//Title:        Consulta de alarmas automaticas de un periodo
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:

package consAlaAutoPer;

import java.io.Serializable;

public class DataIndAuto
    implements Serializable {

  protected String anoD = "";
  protected String semD = "";
  protected String anoH = "";
  protected String semH = "";

  protected String sCod = "";
  protected String sDes = "";
  protected String sAno = "";

  protected String enfermedad = "";
  //protected String nombreEnfermedad = "";

  public DataIndAuto() {
  }

  public DataIndAuto(String cod) {
    sCod = cod;
  }

  public DataIndAuto(String cod, String des, String ano) {
    sCod = cod;
    sDes = des;
    sAno = ano;
  }

  public DataIndAuto(String aD, String sD, String aH, String sH) {
    anoD = aD;
    semD = sD;
    anoH = aH;
    semH = sH;
  }

  public String getCod() {
    return sCod;
  }

  public String getDes() {
    return sDes;
  }

  public String getAnoD() {
    return anoD;
  }

  public String getAnoH() {
    return anoH;
  }

  public String getSemD() {
    return semD;
  }

  public String getSemH() {
    return semH;
  }

  public void setAnoD(String a) {
    anoD = a;
  }

  public void setAnoH(String a) {
    anoH = a;
  }

  public void setSemD(String s) {
    semD = s;
  }

  public void setSemH(String s) {
    semH = s;
  }

  public String getEnfermedad() {
    return enfermedad;
  }

  public void setEnfermedad(String a) {
    enfermedad = a;
  }

}
