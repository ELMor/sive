//Title:        Consulta de alarmas automaticas de un periodo
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:

package consAlaAutoPer;

//import eqNot.*;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.CheckboxControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo.DataCat;
import eapp.DataGraf;
import eapp.GPanel;
import eapp.PanelChart;
import sapp.StubSrvBD;
import utilidades.PanFechas;

public class panConsAlaAutoPer
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  //Modos de operacion del servlet
  final int ALA_AUTO_SEM = 1;

  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/vaciar.gif",
      "images/grafico.gif"};

  protected CCargadorImagen imgs = null;

  protected StubSrvBD stubCliente = new StubSrvBD();

  public parConsAlaAutoPer param;
  protected panInfAlaAutoPer informe = null;

  protected int modoOperacion = modoNORMAL;

  // stub's
  final String strSERVLETInd = "servlet/SrvIndAuto";
  final String strSERVLETEnf = "servlet/SrvEnfVigi";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  XYLayout xYLayout = new XYLayout();

  Label lblDesde = new Label();
  Label lblInd1 = new Label();
  CCampoCodigo txtCodInd1 = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarInd1 = new ButtonControl();
  TextField txtDesInd1 = new TextField();
  PanFechas panelDesde;
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();
  ButtonControl btnInformeGraf = new ButtonControl();

  focusAdapter txtFocusAdapter = new focusAdapter(this);

  txt_keyAdapter txtKeyAdapter = new txt_keyAdapter(this);

  actionListener btnActionListener = new actionListener(this);

  Label lblEnf = new Label();
  CCampoCodigo txtCodEnf = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarEnf = new ButtonControl();
  TextField txtDesEnf = new TextField();
  Label lblHasta = new Label();
  utilidades.PanFechas paneHasta = null;
  Label lblInd2 = new Label();
  CCampoCodigo txtCodInd2 = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarInd2 = new ButtonControl();
  TextField txtDesInd2 = new TextField();
  Label lblInd3 = new Label();
  CCampoCodigo txtCodInd3 = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarInd3 = new ButtonControl();
  XYLayout xYLayout1 = new XYLayout();
  TextField txtDesInd3 = new TextField();
  CheckboxGroup checkboxAgrupar = new CheckboxGroup();
  CheckboxGroup checkboxAgrupar2 = new CheckboxGroup();
  CheckboxControl checkCtrlDS = new CheckboxControl();
  CheckboxControl checkCtrlCoef = new CheckboxControl();
  chkitemAdapter chkItemAdapter = new chkitemAdapter(this);

  // Para conocer la selecci�n de indicadores
  private int iSeleccionIndicadores = 0;

  public panConsAlaAutoPer(CApp a) {
    try {
      setApp(a);
      res = ResourceBundle.getBundle("consAlaAutoPer.Res" + a.getIdioma());

      informe = new panInfAlaAutoPer(a);
      panelDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      paneHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      jbInit();
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(364);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));

    btnInforme.setLabel(res.getString("btnInforme.Label"));
    btnInformeGraf.setLabel(res.getString("btnInformeGraf.Label"));

    lblEnf.setText(res.getString("lblEnf.Text"));
    lblInd1.setText(res.getString("lblInd1.Text"));
    lblInd2.setText(res.getString("lblInd1.Text"));
    lblHasta.setText(res.getString("lblHasta.Text"));
    lblInd3.setText(res.getString("lblInd1.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));

    txtCodEnf.addKeyListener(txtKeyAdapter);
    txtCodEnf.addFocusListener(txtFocusAdapter);
    txtCodEnf.setName("txtCodEnf");
    txtCodEnf.setBackground(new Color(255, 255, 150));
    txtDesEnf.setEditable(false);
    txtDesEnf.setEnabled(false);

    btnCtrlBuscarEnf.setActionCommand("buscarEnf");
    btnCtrlBuscarInd1.setActionCommand("buscarInd1");
    btnCtrlBuscarInd2.setActionCommand("buscarInd2");
    btnCtrlBuscarInd3.setActionCommand("buscarInd3");
    btnInforme.setActionCommand("generar");
    btnInformeGraf.setActionCommand("generar_grafico");
    btnLimpiar.setActionCommand("limpiar");

    // gestores de eventos

    btnCtrlBuscarInd1.addActionListener(btnActionListener);
    btnCtrlBuscarInd2.addActionListener(btnActionListener);
    btnCtrlBuscarInd3.addActionListener(btnActionListener);
    btnCtrlBuscarEnf.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);
    btnInformeGraf.addActionListener(btnActionListener);

    txtCodInd1.addKeyListener(txtKeyAdapter);
    txtCodInd1.addFocusListener(txtFocusAdapter);
    txtDesInd1.setEditable(false);
    txtDesInd1.setEnabled(false);

    txtCodInd1.setName("txtCodInd1");
    txtCodInd1.setBackground(new Color(255, 255, 150));

    txtCodInd2.addKeyListener(txtKeyAdapter);
    txtCodInd2.addFocusListener(txtFocusAdapter);
    txtCodInd2.setName("txtCodInd2");
    txtCodInd2.setBackground(Color.white);
    txtDesInd2.setEditable(false);
    txtDesInd2.setEnabled(false);

    txtCodInd3.addKeyListener(txtKeyAdapter);
    txtCodInd3.addFocusListener(txtFocusAdapter);
    txtCodInd3.setName("txtCodInd3");
    txtCodInd3.setBackground(Color.white);
    txtDesInd3.setEditable(false);
    txtDesInd3.setEnabled(false);

    xYLayout1.setHeight(301);
    xYLayout1.setWidth(596);

    checkCtrlDS.setCheckboxGroup(checkboxAgrupar2);
    checkCtrlDS.setLabel(res.getString("checkCtrlDS.Label"));
    checkCtrlCoef.setCheckboxGroup(checkboxAgrupar2);
    checkCtrlCoef.setLabel(res.getString("checkCtrlCoef.Label"));

    checkCtrlDS.addItemListener(chkItemAdapter);
    checkCtrlCoef.addItemListener(chkItemAdapter);

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnCtrlBuscarInd1.setImage(imgs.getImage(0));
    btnCtrlBuscarInd3.setImage(imgs.getImage(0));
    btnCtrlBuscarInd2.setImage(imgs.getImage(0));
    btnCtrlBuscarEnf.setImage(imgs.getImage(0));
    btnLimpiar.setImage(imgs.getImage(1));

    btnInforme.setImage(imgs.getImage(2));
    btnInformeGraf.setImage(imgs.getImage(2));

    xYLayout.setWidth(532);

    this.setLayout(xYLayout);

    this.add(lblDesde, new XYConstraints(26, 42, 46, -1));
    this.add(panelDesde, new XYConstraints(40, 42, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 82, 51, -1));
    this.add(paneHasta, new XYConstraints(40, 82, -1, -1));

    this.add(lblEnf, new XYConstraints(26, 122, -1, -1));
    this.add(txtCodEnf, new XYConstraints(111, 122, 107, -1));
    this.add(btnCtrlBuscarEnf, new XYConstraints(226, 122, -1, -1));
    this.add(txtDesEnf, new XYConstraints(255, 122, 252, -1));

    this.add(lblInd1, new XYConstraints(26, 162, -1, -1));
    this.add(txtCodInd1, new XYConstraints(111, 162, 107, -1));
    this.add(btnCtrlBuscarInd1, new XYConstraints(226, 162, -1, -1));
    this.add(txtDesInd1, new XYConstraints(255, 162, 253, -1));
    this.add(checkCtrlDS, new XYConstraints(115, 282, -1, -1));
    this.add(checkCtrlCoef, new XYConstraints(194, 282, -1, -1));

    this.add(lblInd2, new XYConstraints(26, 202, -1, -1));
    this.add(txtCodInd2, new XYConstraints(111, 202, 107, -1));
    this.add(btnCtrlBuscarInd2, new XYConstraints(226, 202, -1, -1));
    this.add(txtDesInd2, new XYConstraints(255, 202, 252, -1));
    this.add(lblInd3, new XYConstraints(26, 242, -1, -1));
    this.add(txtCodInd3, new XYConstraints(111, 242, 107, -1));
    this.add(btnCtrlBuscarInd3, new XYConstraints(226, 242, -1, -1));
    this.add(txtDesInd3, new XYConstraints(255, 242, 252, -1));
    this.add(btnLimpiar, new XYConstraints(271, 312, -1, -1));
    this.add(btnInforme, new XYConstraints(424, 312, -1, -1));
    this.add(btnInformeGraf, new XYConstraints(349, 312, -1, -1));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (panelDesde.txtAno.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    if ( (panelDesde.txtCodSem.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    if ( (txtDesEnf.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    if ( (txtDesInd1.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    if ( (txtDesInd2.getText().length() == 0) &&
        (txtDesInd3.getText().length() == 0)) {
      if ( (!checkCtrlCoef.isChecked()) && (!checkCtrlDS.isChecked())) {
        bDatosCompletos = false;
      }
    }

    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtCodEnf.setEnabled(true);
        btnCtrlBuscarEnf.setEnabled(true);

        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);
        btnInformeGraf.setEnabled(true);

        checkCtrlCoef.setEnabled(true);
        checkCtrlDS.setEnabled(true);

        if (!txtDesEnf.getText().equals("")) {
          btnCtrlBuscarInd1.setEnabled(true);
          txtCodInd1.setEnabled(true);
        }
        else {
          btnCtrlBuscarInd1.setEnabled(false);
          txtCodInd1.setEnabled(false);
        }

        if (!txtDesInd1.getText().equals("")) {
          btnCtrlBuscarInd2.setEnabled(true);
          txtCodInd2.setEnabled(true);
        }
        else {
          btnCtrlBuscarInd2.setEnabled(false);
          txtCodInd2.setEnabled(false);
        }

        if (!txtDesInd2.getText().equals("")) {
          btnCtrlBuscarInd3.setEnabled(true);
          txtCodInd3.setEnabled(true);
          checkCtrlCoef.setChecked(false);
          checkCtrlDS.setChecked(false);
          checkCtrlCoef.setEnabled(false);
          checkCtrlDS.setEnabled(false);

        }
        else {
          btnCtrlBuscarInd3.setEnabled(false);
          txtCodInd3.setEnabled(false);
          checkCtrlCoef.setEnabled(true);
          checkCtrlDS.setEnabled(true);
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCodInd1.setEnabled(false);
        btnCtrlBuscarInd1.setEnabled(false);

        btnCtrlBuscarInd2.setEnabled(false);
        txtCodInd2.setEnabled(false);

        btnCtrlBuscarInd3.setEnabled(false);
        txtCodInd3.setEnabled(false);

        txtCodEnf.setEnabled(false);
        btnCtrlBuscarEnf.setEnabled(false);

        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        btnInformeGraf.setEnabled(false);

        checkCtrlCoef.setEnabled(false);
        checkCtrlDS.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  // busca la enfermedad
  void btnCtrlBuscarEnf_actionPerformed(ActionEvent evt) {
    DataCat datosPantalla = null;
    CMessage msgBox = null;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      CListaEnfEDO lista = new CListaEnfEDO(app,
                                            res.getString("msg3.Text"),
                                            stubCliente,
                                            strSERVLETEnf,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      datosPantalla = (DataCat) lista.getComponente();

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (datosPantalla != null) {
      txtCodEnf.removeKeyListener(txtKeyAdapter);
      txtCodEnf.setText(datosPantalla.getCod());
      txtDesEnf.setText(datosPantalla.getDes());
      txtCodEnf.addKeyListener(txtKeyAdapter);
      //AIC
      txtCodInd1.setText("");
      txtDesInd1.setText("");
      txtCodInd2.setText("");
      txtDesInd2.setText("");
      txtCodInd3.setText("");
      txtDesInd3.setText("");
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // Buscar indicadores (ademas deben ser distintos)
  void btnCtrlBuscarInd_actionPerformed(ActionEvent evt) {
    DataIndAuto datosPantalla = null;
    CMessage msgBox = null;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      CListaInd lista = new CListaInd(this.getApp(),
                                      res.getString("msg4.Text"),
                                      stubCliente,
                                      strSERVLETInd,
                                      servletOBTENER_X_CODIGO,
                                      servletOBTENER_X_DESCRIPCION,
                                      servletSELECCION_X_CODIGO,
                                      servletSELECCION_X_DESCRIPCION,
                                      panelDesde.txtAno.getText(),
                                      paneHasta.txtAno.getText(),
                                      txtCodEnf.getText());
      lista.show();
      datosPantalla = (DataIndAuto) lista.getComponente();

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (datosPantalla != null) {
      if (evt.getActionCommand().equals("buscarInd1")) {
        txtCodInd1.removeKeyListener(txtKeyAdapter);
        txtCodInd1.setText(datosPantalla.getCod());
        txtDesInd1.setText(datosPantalla.getDes());
        txtCodInd1.addKeyListener(txtKeyAdapter);
        chkEstado_itemStateChanged();
      }
      else if (evt.getActionCommand().equals("buscarInd2")) {
        txtCodInd2.removeKeyListener(txtKeyAdapter);
        txtCodInd2.setText(datosPantalla.getCod());
        txtDesInd2.setText(datosPantalla.getDes());
        txtCodInd2.addKeyListener(txtKeyAdapter);
      }
      else if (evt.getActionCommand().equals("buscarInd3")) {
        txtCodInd3.removeKeyListener(txtKeyAdapter);
        txtCodInd3.setText(datosPantalla.getCod());
        txtDesInd3.setText(datosPantalla.getDes());
        txtCodInd3.addKeyListener(txtKeyAdapter);
      }
    }

    modoOperacion = modoNORMAL;
    Inicializar();

  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    panelDesde.txtAno.setText("");
    panelDesde.txtCodSem.setText("");
    panelDesde.txtFecSem.setText("");

    paneHasta.txtAno.setText("");
    paneHasta.txtCodSem.setText("");
    paneHasta.txtFecSem.setText("");

    txtCodInd1.setText("");
    txtDesInd1.setText("");
    txtCodInd2.setText("");
    txtDesInd2.setText("");
    txtCodInd3.setText("");
    txtDesInd3.setText("");

    txtCodEnf.setText("");
    txtDesEnf.setText("");
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //******************************************************************//
  void btnGenerarGrafico_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    param = new parConsAlaAutoPer();
    CLista listaParametros = null;
    CLista listaResultados = null;

    final String strSERVLET = "servlet/SrvIndAuto";
    final int servletCONSULTA = 7;
    final int servletCONSULTA_GRAFICO = 700;

    if (bDatosCompletos()) {
      // consulta en modo espera
      modoOperacion = modoESPERA;
      Inicializar();

      param = cargarParametro();
      param.tipoPres = "g";
      param.bInformeCompleto = true;

      listaParametros = new CLista();

      listaParametros.setIdioma(getApp().getIdioma());
      listaParametros.setPerfil(getApp().getPerfil());
      listaParametros.setLogin(getApp().getLogin());

      listaParametros.addElement(param);

      try {
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
        listaResultados = (CLista) stubCliente.doPost(servletCONSULTA,
            listaParametros);
        //  listaResultados = (CLista) stubCliente.doPost(servletCONSULTA_GRAFICO, listaParametros);
//SrvIndAuto srv = new SrvIndAuto();
//listaResultados = srv.doPrueba(servletCONSULTA_GRAFICO, listaParametros);
//srv = null;

        if (listaResultados != null) {
          pintaGrafico( (Vector) listaResultados.firstElement(), param);
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                                res.getString("msg5.Text"));
          msgBox.show();
          msgBox = null;
        }
      }
      catch (Exception e) {
        // System_out.println(e.getMessage());
      }

      modoOperacion = modoNORMAL;
      Inicializar();

    }

  }

  void pintaGrafico(Vector puntos, parConsAlaAutoPer datos) {
    String strCriterio1 = null;
    String strCriterio2 = null;
    String strCriterio3 = null;

    DataConsIndAuto dciaTemp = null;

    double rawData[][] = null;
    int filasRawData = 0;

    String series_X[] = null;
    String series_Y[] = null; // {"casos", "media", "m�ximo", "m�nimo"};

    switch (iSeleccionIndicadores) {
      case 1:
        filasRawData = 5;
        series_Y = new String[4];
        series_Y[0] = "casos";
        series_Y[1] = getNombreIndicador(1);
        series_Y[2] = "umbral inferior";
        series_Y[3] = "umbral superior";
        break;
      case 2:
        filasRawData = 4;
        series_Y = new String[3];
        series_Y[0] = "casos";
        series_Y[1] = getNombreIndicador(1);
        series_Y[2] = getNombreIndicador(2);
        break;
      case 3:
        filasRawData = 5;
        series_Y = new String[4];
        series_Y[0] = "casos";
        series_Y[1] = getNombreIndicador(1);
        series_Y[2] = getNombreIndicador(2);
        series_Y[3] = getNombreIndicador(3);
        break;
    }

    series_X = new String[puntos.size()];
    rawData = new double[filasRawData][puntos.size()];

    for (int i = 0; i < puntos.size(); i++) {
      dciaTemp = (DataConsIndAuto) puntos.elementAt(i);

      series_X[i] = (dciaTemp.SEMANA).substring(6, 8); // (((DataConsIndAuto) puntos.elementAt(i)).SEMANA).substring(6, 8);

      rawData[0][i] = i + 1;
      rawData[1][i] = dciaTemp.NM_CASOS; //((DataConsIndAuto) puntos.elementAt(i)).NM_CASOS;
      rawData[2][i] = dciaTemp.NM_VALOR1; //((DataConsIndAuto) puntos.elementAt(i)).NM_VALOR1;

      switch (iSeleccionIndicadores) {
        case 1:
          rawData[3][i] = dciaTemp.UMBRAL_INF; // ((DataConsIndAuto) puntos.elementAt(i)).UMBRAL_INF;
          rawData[4][i] = dciaTemp.UMBRAL_SUP; // ((DataConsIndAuto) puntos.elementAt(i)).UMBRAL_SUP;
          break;
        case 2:
          rawData[3][i] = dciaTemp.NM_VALOR2; // ((DataConsIndAuto) puntos.elementAt(i)).NM_VALOR2;
          break;
        case 3:
          rawData[3][i] = dciaTemp.NM_VALOR2; // ((DataConsIndAuto) puntos.elementAt(i)).NM_VALOR2;
          rawData[4][i] = dciaTemp.NM_VALOR3; // ((DataConsIndAuto) puntos.elementAt(i)).NM_VALOR3;
          break;
      }
    }

    DataGraf data = new DataGraf(res.getString("msg6.Text"), rawData, series_Y,
                                 series_X);

    GPanel panel = new GPanel(this.app, PanelChart.CURVA_EPIDEMICA);

    panel.chart.setDatosLabels(data, series_X);

    strCriterio1 = new String(res.getString("msg7.Text") + datos.anoDesde
                              + res.getString("msg8.Text") + datos.semDesde +
                              res.getString("msg9.Text") + datos.anoHasta
                              + res.getString("msg10.Text") + datos.semHasta +
                              ".");

    strCriterio2 = new String(res.getString("msg11.Text") + datos.enfermedad +
                              " - "
                              + this.txtDesEnf.getText() + ".");

    if (iSeleccionIndicadores == 1) {
      if (this.checkCtrlDS.getState()) {
        strCriterio3 = res.getString("msg12.Text")
            + res.getString("msg13.Text");
      }
      else {
        strCriterio3 = res.getString("msg12.Text")
            + res.getString("msg14.Text");
      }
    }
    else {
      strCriterio3 = new String();
    }

    panel.chart.setCriterios(strCriterio3, strCriterio2, strCriterio1);

    panel.chart.setTitulo(res.getString("this.chart.Titulo"));
    panel.chart.setTituloEje(res.getString("this.chart.TituloEje"),
                             res.getString("this1.chart.TituloEje"));

    panel.show();

  }

  private String getNombreIndicador(int posicion) {
    String strValor = res.getString("msg15.Text");
    String strTemp = "";

    switch (posicion) {
      case 1:
        strTemp = txtCodInd1.getText();
        break;
      case 2:
        strTemp = txtCodInd2.getText();
        break;
      case 3:
        strTemp = txtCodInd3.getText();
        break;
    }

    if (strTemp.endsWith("1")) {
      strValor = res.getString("msg16.Text");
    }
    else if (strTemp.endsWith("2")) {
      strValor = res.getString("msg17.Text");
    }
    else {
      strValor = res.getString("msg18.Text");
    }
    return strValor;
  }

  //***************************************************************//

  boolean bDatosCompletos() {
    CMessage msgBox;
    boolean bValor = true;

    if (isDataValid()) {
      iSeleccionIndicadores = 0;
      // Comprobar que solo se selecciona la desv. estandar
      // con un indicador de media
      if ( (checkCtrlDS.isChecked()) &&
          (!txtCodInd1.getText().endsWith("1"))) {
        if (txtDesInd2.getText().length() == 0) { // Para que s�lo investigue el valor si hay un indicador
          msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                                res.getString("msg19.Text"));
          msgBox.show();
          msgBox = null;
          //return;
          bValor = false;
        }
      }
      // Comprobar que los tres indicadores son distintos
      if ( (! (txtDesInd1.getText().length() == 0) &&
            ! (txtDesInd2.getText().length() == 0) &&
            txtDesInd1.getText().equals(txtDesInd2.getText()))
          ||
          (! (txtDesInd1.getText().length() == 0) &&
           ! (txtDesInd2.getText().length() == 0) &&
           ! (txtDesInd3.getText().length() == 0) &&
           (txtDesInd1.getText().equals(txtDesInd2.getText()) ||
            txtDesInd1.getText().equals(txtDesInd3.getText()) ||
            txtDesInd2.getText().equals(txtDesInd3.getText())))) {
        msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                              res.getString("msg20.Text"));
        msgBox.show();
        msgBox = null;
        //return;
        bValor = false;
      }

      if (bValor) {
        if (txtDesInd3.getText().length() != 0) {
          iSeleccionIndicadores = 3;
        }
        else if (txtDesInd2.getText().length() != 0) {
          iSeleccionIndicadores = 2;
        }
        else {
          iSeleccionIndicadores = 1;
        }
      }
    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg21.Text"));
      msgBox.show();
      msgBox = null;
      bValor = false;
    }
    return bValor;
  }

  parConsAlaAutoPer cargarParametro() {
    parConsAlaAutoPer parametro = new parConsAlaAutoPer();

    if (!txtDesInd1.getText().equals("")) {
      parametro.ind1 = txtCodInd1.getText();
      parametro.nombreInd1 = txtDesInd1.getText();
    }

    if (txtDesInd2.getText().equals("") &&
        txtDesInd3.getText().equals("")) {
      if (checkCtrlCoef.isChecked()) {
        parametro.coef_ds = "c";
      }
      else {
        parametro.coef_ds = "d";
      }
    }

    if (!txtDesInd2.getText().equals("")) {
      parametro.ind2 = txtCodInd2.getText();
      parametro.nombreInd2 = txtDesInd2.getText();
    }

    if (!txtDesInd3.getText().equals("")) {
      parametro.ind3 = txtCodInd3.getText();
      parametro.nombreInd3 = txtDesInd3.getText();
    }

    if (!txtDesEnf.getText().equals("")) {
      parametro.enfermedad = txtCodEnf.getText();
      parametro.nombreEnfermedad = txtDesEnf.getText();
    }

    parametro.anoDesde = panelDesde.txtAno.getText();
    if (panelDesde.txtCodSem.getText().length() == 1) {
      parametro.semDesde = "0" + panelDesde.txtCodSem.getText();
    }
    else {
      parametro.semDesde = panelDesde.txtCodSem.getText();

    }
    parametro.anoHasta = paneHasta.txtAno.getText();
    if (paneHasta.txtCodSem.getText().length() == 1) {
      parametro.semHasta = "0" + paneHasta.txtCodSem.getText();
    }
    else {
      parametro.semHasta = paneHasta.txtCodSem.getText();

    }
    return parametro;
  }

  //***************************************************************//

  void btnGenerar_actionPerformed(ActionEvent evt) {
    param = null;
    CLista par = null;
    CLista resul = null;

    if (bDatosCompletos()) {

      param = cargarParametro();

      param.tipoPres = "n";
      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();

      informe.setEnabled(true);
      informe.parCons = param;
      informe.numeroIndicadores(! ( (txtCodInd2.getText().length() > 0) ||
                                   (txtCodInd3.getText().length() > 0)));

      boolean hay = informe.GenerarInforme();
      if (hay) {
        informe.show();

      }
      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    // datos de envio
    DataCat enf;
    DataIndAuto ind;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtCodEnf")) &&
        (txtCodEnf.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataCat(txtCodEnf.getText()));
      strServlet = strSERVLETEnf;
      modoServlet = servletOBTENER_X_CODIGO;
    }
    else if (txt.getName().equals("txtCodInd1") &&
             (txtCodInd1.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      DataIndAuto dato = new DataIndAuto(txtCodInd1.getText());
      dato.setAnoD(panelDesde.txtAno.getText());
      dato.setAnoH(paneHasta.txtAno.getText());
      dato.setEnfermedad(txtCodEnf.getText());
      param.addElement(dato);
      strServlet = strSERVLETInd;
      modoServlet = servletOBTENER_X_CODIGO;
    }
    else if (txt.getName().equals("txtCodInd2") &&
             (txtCodInd2.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      DataIndAuto dato = new DataIndAuto(txtCodInd2.getText());
      dato.setAnoD(panelDesde.txtAno.getText());
      dato.setAnoH(paneHasta.txtAno.getText());
      dato.setEnfermedad(txtCodEnf.getText());
      param.addElement(dato);
      strServlet = strSERVLETInd;
      modoServlet = servletOBTENER_X_CODIGO;
    }
    else if (txt.getName().equals("txtCodInd3") &&
             (txtCodInd3.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      DataIndAuto dato = new DataIndAuto(txtCodInd3.getText());
      dato.setAnoD(panelDesde.txtAno.getText());
      dato.setAnoH(paneHasta.txtAno.getText());
      dato.setEnfermedad(txtCodEnf.getText());
      param.addElement(dato);
      strServlet = strSERVLETInd;
      modoServlet = servletOBTENER_X_CODIGO;
    }

    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        param.setIdioma(getApp().getIdioma());
        param.setPerfil(getApp().getPerfil());
        param.setLogin(getApp().getLogin());

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        // modificacion momentanea para debug
        /*SrvIndAuto srv = new SrvIndAuto();
                 param = srv.doPrueba(modoServlet, param);
         srv = null; */

        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtCodEnf")) {

            enf = (DataCat) param.firstElement();
            txtCodEnf.removeKeyListener(txtKeyAdapter);
            txtCodEnf.setText(enf.getCod());
            txtDesEnf.setText(enf.getDes());
            txtCodEnf.addKeyListener(txtKeyAdapter);
            //AIC
            txtCodInd1.setText("");
            txtDesInd1.setText("");
            txtCodInd2.setText("");
            txtDesInd2.setText("");
            txtCodInd3.setText("");
            txtDesInd3.setText("");
          }
          else if (txt.getName().equals("txtCodInd1")) {

            ind = (DataIndAuto) param.firstElement();
            txtCodInd1.removeKeyListener(txtKeyAdapter);
            txtCodInd1.setText(ind.getCod());
            txtDesInd1.setText(ind.getDes());
            txtCodInd1.addKeyListener(txtKeyAdapter);
            chkEstado_itemStateChanged();
          }
          else if (txt.getName().equals("txtCodInd2")) {

            ind = (DataIndAuto) param.firstElement();
            txtCodInd2.removeKeyListener(txtKeyAdapter);
            txtCodInd2.setText(ind.getCod());
            txtDesInd2.setText(ind.getDes());
            txtCodInd2.addKeyListener(txtKeyAdapter);
          }
          else if (txt.getName().equals("txtCodInd3")) {

            ind = (DataIndAuto) param.firstElement();
            txtCodInd3.removeKeyListener(txtKeyAdapter);
            txtCodInd3.setText(ind.getCod());
            txtDesInd3.setText(ind.getDes());
            txtCodInd3.addKeyListener(txtKeyAdapter);
          }
        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg22.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  void imprimeVector(Vector v) {
    Vector aux = new Vector();
    for (int s = 0; s < v.size(); s++) {
      try {
        aux = (Vector) v.elementAt(s);
        imprimeVector(aux);
      }
      catch (ClassCastException ex) {
      }
    }
  }

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtCodEnf"))) {
      txtDesEnf.setText("");

    }
    else if ( (txt.getName().equals("txtCodInd1"))) {
      txtDesInd1.setText("");
    }
    else if ( (txt.getName().equals("txtCodInd2"))) {
      txtDesInd2.setText("");
      txtCodInd3.setText("");
      txtDesInd3.setText("");
    }
    else if ( (txt.getName().equals("txtCodInd3"))) {
      txtDesInd3.setText("");
    }

    Inicializar();
  }

  void chkEstado_itemStateChanged() {
    String cod = txtCodInd1.getText().trim();

    boolean no = false;
    if (!txtDesInd1.getText().trim().equals("") &&
        txtDesInd2.getText().trim().equals("")
        && txtDesInd3.getText().trim().equals("")) {
      if (cod.indexOf("A") == -1) {
        no = true;
      }
      else if (!cod.endsWith("001")) {
        no = true;

      }
      if (no) {
        checkCtrlCoef.setChecked(true);
        checkCtrlDS.setChecked(false);
        checkCtrlCoef.setEnabled(false);
        checkCtrlCoef.setEnabled(false);
      }
    }

  }

} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  panConsAlaAutoPer adaptee = null;
  ActionEvent e = null;

  public actionListener(panConsAlaAutoPer adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarEnf")) {
      adaptee.btnCtrlBuscarEnf_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarInd1")) {
      adaptee.btnCtrlBuscarInd_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarInd2")) {
      adaptee.btnCtrlBuscarInd_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarInd3")) {
      adaptee.btnCtrlBuscarInd_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar_grafico")) {
      adaptee.btnGenerarGrafico_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  panConsAlaAutoPer adaptee;
  FocusEvent event;

  focusAdapter(panConsAlaAutoPer adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

// listener
class chkitemAdapter
    implements java.awt.event.ItemListener {
  panConsAlaAutoPer adaptee;
  chkitemAdapter(panConsAlaAutoPer adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkEstado_itemStateChanged();
  }
}

////////////////////// Clases para listas

class CListaEnfEDO
    extends CListaValores {

  public CListaEnfEDO(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataCat(s);

  }

  public String getCodigo(Object o) {
    return ( ( (DataCat) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCat) o).getDes());
  }
}

class CListaInd
    extends CListaValores {
  String anoD = "";
  String anoH = "";
  String enfer = "";

  public CListaInd(CApp a,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion,
                   String aD, String aH, String enf) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    anoD = aD;
    anoH = aH;
    enfer = enf;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    DataIndAuto dato = new DataIndAuto(s);
    dato.setAnoD(anoD);
    dato.setAnoH(anoH);
    dato.setEnfermedad(enfer);
    return dato;
  }

  public String getCodigo(Object o) {
    return ( (DataIndAuto) o).getCod();
  }

  public String getDescripcion(Object o) {
    return ( (DataIndAuto) o).getDes();
  }
}

class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  panConsAlaAutoPer adaptee;

  txt_keyAdapter(panConsAlaAutoPer adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}
/* C�digo que obtiene el Canal Epid�mico
  void btnGenerarGraficoCanalEpidemico_actionPerformed(ActionEvent evt){
    CMessage msgBox;
    final String strSERVLET = "servlet/SrvIndAuto";
    final int servletCONSULTA_GRAFICO = 700;
    DataIndAuto parametro = new DataIndAuto();
    CLista listaParametros = null;
    CLista listaResultados = null;
    if (isDataGraficosValid()) {
      modoOperacion = modoESPERA;
      Inicializar();
      parametro.setEnfermedad(txtCodEnf.getText());
      parametro.setAnoD(panelDesde.txtAno.getText());
      if (panelDesde.txtCodSem.getText().length() == 1)
        parametro.setSemD("0" + panelDesde.txtCodSem.getText());
      else
        parametro.setSemD(panelDesde.txtCodSem.getText());
      parametro.setAnoH(paneHasta.txtAno.getText());
      if (paneHasta.txtCodSem.getText().length() == 1)
        parametro.setSemH("0" + paneHasta.txtCodSem.getText());
      else
        parametro.setSemH(paneHasta.txtCodSem.getText());
      listaParametros = new CLista();
      listaParametros.setIdioma(getApp().getIdioma() );
      listaParametros.setPerfil(getApp().getPerfil() );
      listaParametros.setLogin(getApp().getLogin());
      listaParametros.addElement(parametro);
      try {
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
        listaResultados = (CLista) stubCliente.doPost(servletCONSULTA_GRAFICO, listaParametros);
        if (listaResultados != null) {
          pintaGraficoCanalEpidemico((Vector) listaResultados.firstElement(), parametro, ((String) listaResultados.elementAt(1)).equals("*"));
        } else {
          msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA, "No hay datos que visualizar.");
          msgBox.show();
          msgBox = null;
        }
      } catch (Exception e) {
        System_out.println(e.getMessage());
      }
      modoOperacion = modoNORMAL;
      Inicializar();
    } else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA, "Faltan campos obligatorios.");
      msgBox.show();
      msgBox = null;
    }
  }
  // se pinta el gr�fico
  private void pintaGraficoCanalEpidemico(Vector puntos, DataIndAuto datos, boolean bCriterio3) {
    String strCriterio1 = null;
    String strCriterio2 = null;
    String strCriterio3 = null;
    double rawData[][] = null;
    String series_X[] = null;
    String series_Y[] = {"casos", "media", "m�ximo", "m�nimo"};
    series_X = new String[puntos.size()];
    rawData = new double[5][puntos.size()];
    for (int i = 0; i < puntos.size(); i++) {
      series_X[i] = ((DataGrafIndAuto) puntos.elementAt(i)).strSemana;
      rawData[0][i] = i + 1;
      rawData[1][i] = ((DataGrafIndAuto) puntos.elementAt(i)).tasa;
      rawData[2][i] = ((DataGrafIndAuto) puntos.elementAt(i)).media;
      rawData[3][i] = ((DataGrafIndAuto) puntos.elementAt(i)).maximo;
      rawData[4][i] = ((DataGrafIndAuto) puntos.elementAt(i)).minimo;
    }
    DataGraf data = new DataGraf("Leyenda", rawData, series_Y, series_X);
    GPanel panel = new GPanel(this.app, PanelChart.CURVA_EPIDEMICA);
    panel.chart.setDatosLabels(data, series_X);
    strCriterio1 = new String("Desde a�o: " + datos.getAnoD()
     + " Semana: " + datos.getSemD() + ", Hasta a�o: " + datos.getAnoH()
                   + " Semana: " + datos.getSemH() + ".");
    strCriterio2 = new String("Enfermedad: " + datos.getEnfermedad() + " - "
                   + this.txtDesEnf.getText() + ".");
    if (bCriterio3) {
      strCriterio3 = new String("NOTA: los c�lculos se han efectuado a partir "
                                + "de la poblaci�n menor de 15 a�os.");
    } else {
      strCriterio3 = new String();
    }
    panel.chart.setCriterios(strCriterio3, strCriterio2, strCriterio1);
    panel.chart.setTitulo("Gr�fico de indicadores de alarma de un per�odo");
    panel.chart.setTituloEje("semanas", "tasas");
    panel.show();
  }
  // valida datos m�nimos de envio para obtener el gr�fico
  public boolean isDataGraficosValid() {
    boolean bDatosCompletos = true;
    // Comprobacion de las fechas
    if ((panelDesde.txtAno.getText().length() == 0))  {
      bDatosCompletos = false;
    }
    if ((panelDesde.txtCodSem.getText().length() == 0))  {
      bDatosCompletos = false;
    }
    if ((txtDesEnf.getText().length() == 0))  {
      bDatosCompletos = false;
    }
    return bDatosCompletos;
  }
 */
