//Title:        Consulta de alarmas automaticas de un periodo
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:

package consAlaAutoPer;

import java.io.Serializable;
import java.util.Vector;

public class parConsAlaAutoPer
    implements Serializable {

  public String anoDesde = new String();
  public String semDesde = new String();
  public String anoHasta = new String();
  public String semHasta = new String();

  public String enfermedad = new String();
  public String nombreEnfermedad = new String();
  public String ind1 = new String();
  public String nombreInd1 = new String();
  public String coef_ds = new String();

  public String ind2 = new String();
  public String nombreInd2 = new String();
  public String ind3 = new String();
  public String nombreInd3 = new String();
  public String tipoPres = new String();

  public int numPagina = 0;
  public boolean bInformeCompleto = false;
  public Vector primSem = new Vector();

  public parConsAlaAutoPer() {
  }

  public parConsAlaAutoPer(String anoD, String semD, String anoH, String semH,
                           String i1, String enf, String co, String pres) {
    anoDesde = anoD;
    semDesde = semD;
    anoHasta = anoH;
    semHasta = semH;
    ind1 = i1;
    enfermedad = enf;
    coef_ds = co;
    tipoPres = pres;
  }
}
