//Title:        Consulta de alarmas automaticas de una semana epidemilogica.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Se presentara la relacion de enfermedades
//notificadas en la semana elegida con el numero
//de casos y los indicadoes superados con su umbral.

package consAlaAutoPer;

import java.util.ResourceBundle;

import capp.CApp;

public class ConsAlaAutoPer
    extends CApp {

  ResourceBundle res;

  public ConsAlaAutoPer() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("consAlaAutoPer.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    //E System_out.println("SrvIndAuto: en applet perfil" + a.getPerfil());
    //E System_out.println("SrvIndAuto: en applet login" + a.getLogin());
    panConsAlaAutoPer panelCons = new panConsAlaAutoPer(a);
    VerPanel(res.getString("msg2.Text"), panelCons, false);
    VerPanel(res.getString("msg2.Text"));

  }

}
