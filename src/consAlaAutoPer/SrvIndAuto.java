//Title:        Consulta de alarmas automaticas de un periodo
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:

package consAlaAutoPer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import capp.CLista;
import sapp.DBServlet;

//import jdbcpool.*; // $$$ modo debug

public class SrvIndAuto
    extends DBServlet {

  // modos de operaci�n
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  final int servletCONSULTA = 7;
  final int servletCONSULTA_GRAFICO = 700;

  final double CTE_TASAS = 100000.0;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    int iEstado;

    int j = 0;

    Vector resConsulta = new Vector();
    int totalCasos = 0;

    String sCondPerfil = "";
    String periodo = "";
    Vector ultSem = new Vector();

    // Para la obtenci�n de gr�ficos
    String strEnfermedad = null;
    String strCD_ENFERMEDAD = null;
    String strGuion = null;

    String strCDMedia = null;
    String strCDMaximo = null;
    String strCDMinimo = null;

    String strQuery = null;
    String strCDActual = null;

    int ku = 0;
    int iPoblacionCA = 0;
    int iEdadPoblacion = 0;

    double dValor = 0;

    // Devoluci�n de datos estad�sticos
    DataGrafIndAuto dgiaDatos = null;
    Vector datosEstadisticos = null;

    // objetos de datos
    CLista data = new CLista();
    DataIndAuto indAuto = null;
    parConsAlaAutoPer parCons = null;
    parConsAlaAutoPer parConsGRAF = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    if (opmode != servletCONSULTA && opmode != servletCONSULTA_GRAFICO) {
      indAuto = (DataIndAuto) param.firstElement();

      switch (param.getPerfil()) {
        case 1:
        case 2: // ccaa
          sCondPerfil = sCondPerfil;
          break;
        case 3: // niv 1
          sCondPerfil = sCondPerfil +
              "and b.CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?) ";
          break;
        case 4: // niv 2
          sCondPerfil = sCondPerfil +
              "and b.CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?) " +
              "and b.CD_NIVEL_2 in (select CD_NIVEL_2 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?) ";
          break;
      }
      periodo = " and (CD_ANOEPI >= ? and CD_ANOEPI <= ? )";
    }
    else {
      if (opmode != servletCONSULTA_GRAFICO) {
        parCons = (parConsAlaAutoPer) param.firstElement();

        if (parCons.anoDesde.equals(parCons.anoHasta)) {
          periodo = " and CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ";
        }
        else {
          periodo = " and ((CD_ANOEPI = ? and CD_SEMEPI>= ? )or "
              + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
              + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))";
        }
      }
      else {
        /*indAuto = (DataIndAuto) param.firstElement();
                 if (indAuto.getAnoD().equals(indAuto.getAnoH())) {
             periodo = " and CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ";
                 } else {
          periodo =  " and ((CD_ANOEPI = ? and CD_SEMEPI>= ? )or "
                     + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
                     + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))";
                 } */
        parConsGRAF = (parConsAlaAutoPer) param.firstElement();

        if (parConsGRAF.anoDesde.equals(parConsGRAF.anoHasta)) {
          periodo = " and CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ";
        }
        else {
          periodo = " and ((CD_ANOEPI = ? and CD_SEMEPI>= ? )or "
              + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
              + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))";
        }
        strEnfermedad = parConsGRAF.enfermedad;

        strGuion = new String();

        for (ku = strEnfermedad.length() + 1; ku <= 6; ku++) {
          strGuion += "-";
        }

        //strCDMedia = "CC--A" + strGuion + strEnfermedad + "001";
        //strCDMaximo = "CC--A" + strGuion + strEnfermedad + "002";
        //strCDMinimo = "CC--A" + strGuion + strEnfermedad + "004";
        strCDMedia = "CC--A" + strGuion + strEnfermedad + "002";
        strCDMaximo = "CC--A" + strGuion + strEnfermedad + "003";
        strCDMinimo = "CC--A" + strGuion + strEnfermedad + "001";
      }
    }

    Vector semanas = null;
    Vector regSem = null;
    int casos = 0;
    String sCasos = "";

    // modos de operaci�n
    switch (opmode) {

      case servletCONSULTA_GRAFICO:

        // se han hecho modificaciones de prueba para entrar en
        // es esta modo, pero el bueno es el otro
        trazaLog("estoy en consultagrafico");

        /*query = " select sum(nm_casos), cd_anoepi, cd_semepi "
              + " from sive_resumen_edos "
              + " where cd_enfcie = ? "
              + periodo
              + " group by cd_anoepi, cd_semepi "
              + " order by cd_anoepi, cd_semepi ";*/

        query = " select sum(nm_casos), cd_anoepi, cd_semepi  "
            + " from sive_resumen_edos "
            + "where cd_enfcie = ? "
            + periodo
            + "group by cd_anoepi, cd_semepi "
            //    order by cd_anoepi, cd_semepi
            + " UNION "
            + "  SELECT 0, CD_ANOEPI, CD_SEMEPI "
            + "  FROM SIVE_SEMANA_EPI "
            +
            "  WHERE CD_SEMEPI NOT IN (SELECT CD_SEMEPI FROM SIVE_RESUMEN_EDOS "
            + "  WHERE CD_ENFCIE = ?  "
            + periodo
            + " ) "
            + periodo
            + " order by cd_anoepi, cd_semepi ";

        trazaLog("query" + query);

        st = con.prepareStatement(query);
        //st.setString(1, indAuto.getEnfermedad());
        st.setString(1, parConsGRAF.enfermedad);

        /*if (indAuto.getAnoD().equals(indAuto.getAnoH())){
          st.setString(2, indAuto.getSemD());
          st.setString(3, indAuto.getSemH());
          st.setString(4, indAuto.getAnoD());
                 }
                 else {
          st.setString(2, indAuto.getAnoD());
          st.setString(3, indAuto.getSemD());
          st.setString(4, indAuto.getAnoD());
          st.setString(5, indAuto.getAnoH());
          st.setString(6, indAuto.getAnoH());
          st.setString(7, indAuto.getSemH());
                 } */
        if (parConsGRAF.anoDesde.equals(parConsGRAF.anoHasta)) {
          st.setString(2, parConsGRAF.semDesde);
          st.setString(3, parConsGRAF.semHasta);
          st.setString(4, parConsGRAF.anoDesde);
          ////////////////////
          st.setString(5, parConsGRAF.enfermedad);
          st.setString(6, parConsGRAF.semDesde);
          st.setString(7, parConsGRAF.semHasta);
          st.setString(8, parConsGRAF.anoDesde);
          st.setString(9, parConsGRAF.semDesde);
          st.setString(10, parConsGRAF.semHasta);
          st.setString(11, parConsGRAF.anoDesde);
          ////////////////////
        }
        else {
          st.setString(2, parConsGRAF.anoDesde);
          st.setString(3, parConsGRAF.semDesde);
          st.setString(4, parConsGRAF.anoDesde);
          st.setString(5, parConsGRAF.anoHasta);
          st.setString(6, parConsGRAF.anoHasta);
          st.setString(7, parConsGRAF.semHasta);
          //////////////////////
          st.setString(8, parConsGRAF.enfermedad);
          st.setString(9, parConsGRAF.anoDesde);
          st.setString(10, parConsGRAF.semDesde);
          st.setString(11, parConsGRAF.anoDesde);
          st.setString(12, parConsGRAF.anoHasta);
          st.setString(13, parConsGRAF.anoHasta);
          st.setString(14, parConsGRAF.semHasta);
          st.setString(15, parConsGRAF.anoDesde);
          st.setString(16, parConsGRAF.semDesde);
          st.setString(17, parConsGRAF.anoDesde);
          st.setString(18, parConsGRAF.anoHasta);
          st.setString(19, parConsGRAF.anoHasta);
          st.setString(20, parConsGRAF.semHasta);
          //////////////////////
        }

        rs = st.executeQuery();

        // Se obtiene el vector de semanas
        semanas = new Vector();

        // Se almacena en strCD_ENFERMEDAD el c�digo general de la enfermedad
        //strCD_ENFERMEDAD = new String(infcata.SrvInfEDOcata.getCDEnfermedad(con, indAuto.getEnfermedad()));
        strCD_ENFERMEDAD = new String(infcata.SrvInfEDOcata.getCDEnfermedad(con,
            parConsGRAF.enfermedad));

        // ... y se compara con la enfermedad 'par�lisis fl�cida...'
        // Si lo es, los c�lculos se realizan a partir de la poblaci�n menor de
        // 15 a�os.
        if (strCD_ENFERMEDAD.equals(infcata.SrvInfEDOcata.CD_PARALISIS)) {
          iEdadPoblacion = 15;
        }
        else {
          iEdadPoblacion = -1;
        }

        // Se obtiene la poblaci�n de la CA durante el primer a�o. Se emplear�
        // para todos los c�lculos.
        //iPoblacionCA = infcata.SrvInfEDOcata.getPoblacion(con, indAuto.getAnoD(), "", "", iEdadPoblacion);
        iPoblacionCA = infcata.SrvInfEDOcata.getPoblacion(con,
            parConsGRAF.anoDesde, "", "", iEdadPoblacion);

        while (rs.next()) {
          dgiaDatos = new DataGrafIndAuto();

          dgiaDatos.strA�o = rs.getString(2);
          dgiaDatos.strSemana = rs.getString(3);
          sCasos = rs.getString(1);
          dgiaDatos.tasa = (new Integer(sCasos)).intValue() * CTE_TASAS /
              iPoblacionCA;

          semanas.addElement(dgiaDatos);
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        datosEstadisticos = new Vector();

        for (ku = 0; ku < semanas.size(); ku++) {
          strQuery = new String(
              " select CD_INDALAR, NM_VALOR from SIVE_ALARMA "
              + " where CD_ANOEPI = ? and CD_SEMEPI = ? "
              + " and CD_INDALAR in (?,?,?)");

          st = con.prepareStatement(strQuery);

          st.setString(1, ( (DataGrafIndAuto) semanas.elementAt(ku)).strA�o);
          st.setString(2, ( (DataGrafIndAuto) semanas.elementAt(ku)).strSemana);
          st.setString(3, strCDMedia);
          st.setString(4, strCDMaximo);
          st.setString(5, strCDMinimo);

          rs = st.executeQuery();

          while (rs.next()) {
            strCDActual = new String(rs.getString(1));
            dValor = (new Double(rs.getString(2))).doubleValue();

            if (strCDActual.equals(strCDMedia)) {
              ( (DataGrafIndAuto) semanas.elementAt(ku)).media = dValor *
                  CTE_TASAS / iPoblacionCA;
            }
            else if (strCDActual.equals(strCDMaximo)) {
              ( (DataGrafIndAuto) semanas.elementAt(ku)).maximo = dValor *
                  CTE_TASAS / iPoblacionCA;
            }
            else if (strCDActual.equals(strCDMinimo)) {
              ( (DataGrafIndAuto) semanas.elementAt(ku)).minimo = dValor *
                  CTE_TASAS / iPoblacionCA;
            }
          }
          datosEstadisticos.addElement( (DataGrafIndAuto) semanas.elementAt(ku));

          rs.close();
          rs = null;
          st.close();
          st = null;
        }

        data.addElement(datosEstadisticos);
        data.addElement(iEdadPoblacion == -1 ? "" : "*");

        break;
// ************************************************************************** //
// ************************************************************************** //
// ************************************************************************** //
      case servletCONSULTA:

        // Vector semanas = new Vector();
        semanas = new Vector();
        /*
                query = " select sum(nm_casos), cd_anoepi, cd_semepi "
                      + " from sive_resumen_edos "
                      + " where cd_enfcie = ? "
                      + periodo
                      + " group by cd_anoepi, cd_semepi "
                      + " order by cd_anoepi, cd_semepi ";
         */
        //////////////////////////7

        query = " select sum(nm_casos), cd_anoepi, cd_semepi  "
            + " from sive_resumen_edos "
            + "where cd_enfcie = ? "
            + periodo
            + "group by cd_anoepi, cd_semepi "
            //    order by cd_anoepi, cd_semepi
            + " UNION "
            + "  SELECT 0, CD_ANOEPI, CD_SEMEPI "
            + "  FROM SIVE_SEMANA_EPI "
            +
            "  WHERE CD_SEMEPI NOT IN (SELECT CD_SEMEPI FROM SIVE_RESUMEN_EDOS "
            + "  WHERE CD_ENFCIE = ?  "
            + periodo
            + " ) "
            + periodo
            + " order by cd_anoepi, cd_semepi ";

        trazaLog("query" + query);

        ///////////////////
        st = con.prepareStatement(query);
        st.setString(1, parCons.enfermedad);

        if (parCons.anoDesde.equals(parCons.anoHasta)) {
          st.setString(2, parCons.semDesde);
          st.setString(3, parCons.semHasta);
          st.setString(4, parCons.anoDesde);
          ////////////////////
          st.setString(5, parCons.enfermedad);
          st.setString(6, parCons.semDesde);
          st.setString(7, parCons.semHasta);
          st.setString(8, parCons.anoDesde);
          st.setString(9, parCons.semDesde);
          st.setString(10, parCons.semHasta);
          st.setString(11, parCons.anoDesde);
          ////////////////////
        }
        else {
          st.setString(2, parCons.anoDesde);
          st.setString(3, parCons.semDesde);
          st.setString(4, parCons.anoDesde);
          st.setString(5, parCons.anoHasta);
          st.setString(6, parCons.anoHasta);
          st.setString(7, parCons.semHasta);
          //////////////////////
          st.setString(8, parCons.enfermedad);
          st.setString(9, parCons.anoDesde);
          st.setString(10, parCons.semDesde);
          st.setString(11, parCons.anoDesde);
          st.setString(12, parCons.anoHasta);
          st.setString(13, parCons.anoHasta);
          st.setString(14, parCons.semHasta);
          st.setString(15, parCons.anoDesde);
          st.setString(16, parCons.semDesde);
          st.setString(17, parCons.anoDesde);
          st.setString(18, parCons.anoHasta);
          st.setString(19, parCons.anoHasta);
          st.setString(20, parCons.semHasta);
          //////////////////////
        }

        rs = st.executeQuery();

        // Se obtiene el vector de semanas
        // int casos = 0;
        // String sCasos = "";
        casos = 0;
        sCasos = "";

        // Vector regSem = new Vector();
        // regSem = new Vector();

        while (rs.next()) {
          regSem = new Vector();
          regSem.addElement(rs.getString(2));
          regSem.addElement(rs.getString(3));
          sCasos = rs.getString(1);
          casos = (new Integer(sCasos)).intValue();
          regSem.addElement(new Integer(sCasos));
          semanas.addElement(regSem);
          totalCasos = totalCasos + casos;
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        //Si solo se ha seleccionado un indicador
        if ( (parCons.ind2.equals(new String())) &&
            (parCons.ind3.equals(new String()))) {

          if (parCons.coef_ds.equals("d")) { //desv. estandar

            for (int i = 0; i < semanas.size(); i++) {

              regSem = (Vector) semanas.elementAt(i);
              if ( (resConsulta.size() > DBServlet.maxSIZE) &&
                  (! (parCons.bInformeCompleto))) {
                data.setState(CLista.listaINCOMPLETA);
                ultSem.addElement( (String) regSem.elementAt(0));
                ultSem.addElement( (String) regSem.elementAt(1));
                break;
              }
              // control de estado
              if (data.getState() == CLista.listaVACIA) {
                data.setState(CLista.listaLLENA);

              }
              query = " select nm_valor, nm_dstandar "
                  + " from sive_alarma "
                  + " where cd_anoepi = ? and cd_semepi = ? "
                  + " and cd_indalar = ? ";

              st = con.prepareStatement(query);

              st.setString(1, (String) regSem.elementAt(0));
              st.setString(2, (String) regSem.elementAt(1));
              st.setString(3, parCons.ind1);

              rs = st.executeQuery();

              float valor;
              float ds;
              if (rs.next()) {
                String anoSem = (String) regSem.elementAt(0) + "  "
                    + (String) regSem.elementAt(1);
                valor = (new Float(rs.getString(1))).floatValue();
                ds = (new Float(rs.getString(2))).floatValue();
                DataConsIndAuto dato = new DataConsIndAuto(anoSem,
                    ( (Integer) regSem.elementAt(2)).intValue(),
                    valor, 1,
                    valor - (float) 2 * ds,
                    valor + (float) 2 * ds);
                resConsulta.addElement(dato);
              }
              rs.close();
              rs = null;
              st.close();
              st = null;
            } // for para las semanas
          } // desv estandar
          else { //coeficiente

            for (int i = 0; i < semanas.size(); i++) {
              regSem = (Vector) semanas.elementAt(i);

              if ( (resConsulta.size() > DBServlet.maxSIZE) &&
                  (! (parCons.bInformeCompleto))) {
                data.setState(CLista.listaINCOMPLETA);
                ultSem.addElement( (String) regSem.elementAt(0));
                ultSem.addElement( (String) regSem.elementAt(1));
                break;
              }
              // control de estado
              if (data.getState() == CLista.listaVACIA) {
                data.setState(CLista.listaLLENA);

              }
              query = " select nm_valor, nm_coef"
                  + " from sive_alarma a , sive_ind_ano i"
                  + " where a.cd_anoepi = ? and a.cd_semepi = ? "
                  + " and a.cd_indalar = ? "
                  + " and a.cd_indalar = i.cd_indalar "
                  + " and a.cd_anoepi = i.cd_anoepi ";

              st = con.prepareStatement(query);

              st.setString(1, (String) regSem.elementAt(0));
              st.setString(2, (String) regSem.elementAt(1));
              st.setString(3, parCons.ind1);

              rs = st.executeQuery();

              float valor;
              float coef;
              float sup;
              float inf;
              if (rs.next()) {
                String anoSem = (String) regSem.elementAt(0) + "  "
                    + (String) regSem.elementAt(1);
                valor = (new Float(rs.getString(1))).floatValue();
                coef = (new Float(rs.getString(2))).floatValue();
                sup = coef * valor;
                inf = valor - (sup - valor);
                DataConsIndAuto dato = new DataConsIndAuto(anoSem,
                    ( (Integer) regSem.elementAt(2)).intValue(), valor, 1,
                    inf, sup);
                resConsulta.addElement(dato);
              }
              rs.close();
              rs = null;
              st.close();
              st = null;
            } // for para las semanas
          } // coeficiente
        } //solo un indicador
        else { // mas de un indicador
          // NOTA:
          // puede haber tres casos segun el numero de indicadores
          // (esto se controla en panconsAlaAutoPer en inicializar()
          // 1.- solo el primer indicador
          // 2.- el primero y el segundo
          // 3.- los tres indicadores

          int inds = 0;
          if (parCons.ind3.length() > 0) {
            inds = 3;
          }
          else {
            inds = 2;

          }
          for (int i = 0; i < semanas.size(); i++) {
            regSem = (Vector) semanas.elementAt(i);
            if ( (resConsulta.size() > DBServlet.maxSIZE) &&
                (! (parCons.bInformeCompleto))) {
              data.setState(CLista.listaINCOMPLETA);
              ultSem.addElement( (String) regSem.elementAt(0));
              ultSem.addElement( (String) regSem.elementAt(1));
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);

            }
            query = " select nm_valor, cd_indalar "
                + " from sive_alarma "
                + " where cd_anoepi = ? and cd_semepi = ? ";

            if (inds == 2) {
              query = query + " and (cd_indalar = ? or cd_indalar = ? )";
            }
            else {
              query = query +
                  " and (cd_indalar = ? or cd_indalar = ? or cd_indalar = ? )";

            }
            st = con.prepareStatement(query);

            st.setString(1, (String) regSem.elementAt(0));
            st.setString(2, (String) regSem.elementAt(1));
            st.setString(3, parCons.ind1);
            if (inds == 2) {
              st.setString(4, parCons.ind2);
            }
            else {
              st.setString(4, parCons.ind2);
              st.setString(5, parCons.ind3);
            }

            rs = st.executeQuery();

            String cd_ind = new String();
            float v1 = 0;
            float v2 = 0;
            float v3 = 0;
            String anoSem = (String) regSem.elementAt(0) + "  "
                + (String) regSem.elementAt(1);
            if (inds == 2) {

              if (rs.next()) {
                cd_ind = rs.getString(2);
                if (cd_ind.equals(parCons.ind1)) {
                  v1 = (new Float(rs.getString(1))).floatValue();
                }
                else if (cd_ind.equals(parCons.ind2)) {
                  v2 = (new Float(rs.getString(1))).floatValue();
                }
              }
              if (rs.next()) {
                cd_ind = rs.getString(2);
                if (cd_ind.equals(parCons.ind1)) {
                  v1 = (new Float(rs.getString(1))).floatValue();
                }
                else if (cd_ind.equals(parCons.ind2)) {
                  v2 = (new Float(rs.getString(1))).floatValue();
                }
              }

              rs.close();
              rs = null;
              st.close();
              st = null;

              DataConsIndAuto dato = new DataConsIndAuto(anoSem,
                  ( (Integer) regSem.elementAt(2)).intValue(), 2,
                  v1, v2);

              resConsulta.addElement(dato);

            }
            else {
              if (rs.next()) {
                cd_ind = rs.getString(2);
                if (cd_ind.equals(parCons.ind1)) {
                  v1 = (new Float(rs.getString(1))).floatValue();
                }
                else if (cd_ind.equals(parCons.ind2)) {
                  v2 = (new Float(rs.getString(1))).floatValue();
                }
                else if (cd_ind.equals(parCons.ind3)) {
                  v3 = (new Float(rs.getString(1))).floatValue();
                }
              }
              if (rs.next()) {
                cd_ind = rs.getString(2);
                if (cd_ind.equals(parCons.ind1)) {
                  v1 = (new Float(rs.getString(1))).floatValue();
                }
                else if (cd_ind.equals(parCons.ind2)) {
                  v2 = (new Float(rs.getString(1))).floatValue();
                }
                else if (cd_ind.equals(parCons.ind3)) {
                  v3 = (new Float(rs.getString(1))).floatValue();
                }
              }
              if (rs.next()) {
                cd_ind = rs.getString(2);
                if (cd_ind.equals(parCons.ind1)) {
                  v1 = (new Float(rs.getString(1))).floatValue();
                }
                else if (cd_ind.equals(parCons.ind2)) {
                  v2 = (new Float(rs.getString(1))).floatValue();
                }
                else if (cd_ind.equals(parCons.ind3)) {
                  v3 = (new Float(rs.getString(1))).floatValue();
                }
              }
              rs.close();
              rs = null;
              st.close();
              st = null;
              DataConsIndAuto dato = new DataConsIndAuto(anoSem,
                  ( (Integer) regSem.elementAt(2)).intValue(),
                  v1, v2, v3);

              resConsulta.addElement(dato);
            }

          } // for para las semanas

        } //mas de un indicador
        data.addElement(resConsulta);
        data.addElement(new Integer(totalCasos));
        if (data.getState() == CLista.listaINCOMPLETA) {
          data.addElement(ultSem);

        }
        break;

        // b�squeda de Indicadores Informados
        // ARG: (13/5/02)
      case servletSELECCION_X_CODIGO:
      case servletSELECCION_X_DESCRIPCION:

        // peticion de tramas did�stintas a la primera
        if (param.getFilter().length() > 0) {

          if (opmode == servletSELECCION_X_CODIGO) { //codigo

            query = "select distinct a.CD_INDALAR, b.DS_INDALAR " //, a.CD_ANOEPI "
                + "from SIVE_IND_ANO a, SIVE_INDICADOR b "
                + "WHERE  a.CD_INDALAR = b.CD_INDALAR AND"
                +
                " a.CD_INDALAR like ? and  a.CD_INDALAR like ? and a.CD_INDALAR > ? "
                + sCondPerfil
                + periodo
                + " group by a.cd_indalar, b.DS_INDALAR "
                + " order by a.CD_INDALAR";
          }
          else { //descripcion

            query = "select a.CD_INDALAR, b.DS_INDALAR " //, a.CD_ANOEPI "
                + "from SIVE_IND_ANO a, SIVE_INDICADOR b "
                + "WHERE  a.CD_INDALAR = b.CD_INDALAR AND"
                + " upper(b.DS_INDALAR) like upper(?) and  a.CD_INDALAR like ? and upper(b.DS_INDALAR) > upper(?) "
                + sCondPerfil
                + periodo
                + " group by a.cd_indalar, b.DS_INDALAR "
                + " order by b.DS_INDALAR";
          }

          // peticion de la primera trama
        }
        else {

          if (opmode == servletSELECCION_X_CODIGO) { //codigo

            query = "select a.CD_INDALAR, b.DS_INDALAR " //, a.CD_ANOEPI "
                + "from SIVE_IND_ANO a, SIVE_INDICADOR b "
                + "WHERE  a.CD_INDALAR = b.CD_INDALAR "
                + " and a.CD_INDALAR like ? and  a.CD_INDALAR like ? "
                + sCondPerfil
                + periodo
                + " group by a.cd_indalar, b.DS_INDALAR "
                + " order by a.CD_INDALAR";
          }
          else { //descripcion

            query = "select a.CD_INDALAR, b.DS_INDALAR " //, a.CD_ANOEPI "
                + "from SIVE_IND_ANO a, SIVE_INDICADOR b "
                + "WHERE  a.CD_INDALAR = b.CD_INDALAR "
                +
                " and upper(b.DS_INDALAR) like upper(?) and  a.CD_INDALAR like ? "
                + sCondPerfil
                + periodo
                + " group by a.cd_indalar, b.DS_INDALAR "
                + " order by b.DS_INDALAR";
          }
        }

        // lanza la query
        st = con.prepareStatement(query);
        // filtro
        int par = 1;
        if (opmode == servletSELECCION_X_CODIGO) {
          st.setString(par, indAuto.getCod().trim() + "%");
        }
        else {
          st.setString(par, "%" + indAuto.getCod().trim() + "%");

        }
        par++;
        /*
                 if (ind.getEnfermedad().length()>5)
             st.setString(par,  "CC__A" + ind.getEnfermedad() + "%");  //caja entrada
                 else
             st.setString(par,  "CC__A_" + ind.getEnfermedad() + "%");  //caja entrada
         */
        //st.setString(par,  "CC__" + "%" + indAuto.getEnfermedad() + "%");  //caja entrada
        //st.setString(par,  "CC__" + "%" + indAuto.getEnfermedad() + "___");  //caja entrada
        // modificacion JLT 14/05/01
        if (sCondPerfil == "" || sCondPerfil == null) {
          st.setString(par, "CC--" + "%" + indAuto.getEnfermedad() + "___"); //caja entrada
        }
        else {
          st.setString(par, "____" + "%" + indAuto.getEnfermedad() + "___"); //caja entrada
        }

        par++;

        // paginaci�n (el codigo)
        if (param.getFilter().length() > 0) {
          //C�digo para paginacion
          st.setString(par, param.getFilter().trim());
          par++;
          //Datos de autorizaciones

          switch (param.getPerfil()) {
            case 1:
            case 2: // ccaa
              st.setString(par, indAuto.getAnoD());
              par++;
              st.setString(par, indAuto.getAnoH());
              par++;

              break;
            case 3: // niv 1
              st.setString(par, param.getLogin().trim());
              par++;
              st.setString(par, indAuto.getAnoD());
              par++;
              st.setString(par, indAuto.getAnoH());
              par++;
              break;
            case 4: // niv 2
              st.setString(par, param.getLogin().trim());
              par++;
              st.setString(par, param.getLogin().trim());
              par++;
              st.setString(par, indAuto.getAnoD());
              par++;
              st.setString(par, indAuto.getAnoH());
              par++;

              break;
          }
        }

        else {
          //Datos de autorizaciones

          switch (param.getPerfil()) {
            case 1:
            case 2: // ccaa
              st.setString(par, indAuto.getAnoD());
              par++;
              st.setString(par, indAuto.getAnoH());
              par++;

              break;
            case 3: // niv 1
              st.setString(par, param.getLogin().trim());
              par++;
              st.setString(par, indAuto.getAnoD());
              par++;
              st.setString(par, indAuto.getAnoH());
              par++;

              break;
            case 4: // niv 2
              st.setString(par, param.getLogin().trim());
              par++;
              st.setString(par, param.getLogin().trim());
              par++;
              st.setString(par, indAuto.getAnoD());
              par++;
              st.setString(par, indAuto.getAnoH());
              par++;

              break;
          }
        }

        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {
          // control de tama�o
          if (j > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataIndAuto) data.lastElement()).getCod());
            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          // a�ade un nodo
          data.addElement(new DataIndAuto
                          (rs.getString("CD_INDALAR"),
                           rs.getString("DS_INDALAR"),
                           ""));
          j++;
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

      case servletOBTENER_X_CODIGO:
      case servletOBTENER_X_DESCRIPCION:

        // prepara la query
        if (opmode == servletOBTENER_X_CODIGO) { //codigo
          query = "select a.CD_INDALAR, b.DS_INDALAR " //, a.CD_ANOEPI "
              + "from SIVE_IND_ANO a, SIVE_INDICADOR b "
              + "WHERE  a.CD_INDALAR = b.CD_INDALAR AND"
              + " a.CD_INDALAR = ? and a.CD_INDALAR like ? "
              + sCondPerfil
              + periodo
              + " group by a.cd_indalar, b.DS_INDALAR ";
        }
        else { //descripcion
          query = "select a.CD_INDALAR, b.DS_INDALAR " //, a.CD_ANOEPI "
              + "from SIVE_IND_ANO a, SIVE_INDICADOR b "
              + "WHERE  a.CD_INDALAR = b.CD_INDALAR AND "
              + " b.DS_INDALAR = ? and a.CD_INDALAR like ? "
              + sCondPerfil
              + periodo
              + " group by a.cd_indalar, b.DS_INDALAR ";

          // lanza la query
        }
        st = con.prepareStatement(query);
        int p = 1;
        st.setString(p, indAuto.getCod().trim());
        p++;
        /*
                 if (ind.getEnfermedad().length()>5)
          st.setString(p,  "CC__A" + ind.getEnfermedad() + "%");
                 else
          st.setString(p,  "CC__A_" + ind.getEnfermedad() + "%");
         */
        //st.setString(p,  "CC__" + "%" + indAuto.getEnfermedad() + "%");  //caja entrada
        //st.setString(p,  "CC__" + "%" + indAuto.getEnfermedad() + "___");  //caja entrada
        // modificacion JLT 14/05/01
        if (sCondPerfil == "" || sCondPerfil == null) {
          st.setString(p, "CC--" + "%" + indAuto.getEnfermedad() + "___"); //caja entrada
        }
        else {
          st.setString(p, "____" + "%" + indAuto.getEnfermedad() + "___"); //caja entrada
        }

        p++;

        switch (param.getPerfil()) {
          case 1:
          case 2: // ccaa
            st.setString(p, indAuto.getAnoD());
            p++;
            st.setString(p, indAuto.getAnoH());
            p++;

            break;
          case 3: // niv 1
            st.setString(p, param.getLogin().trim());
            p++;
            st.setString(p, indAuto.getAnoD());
            p++;
            st.setString(p, indAuto.getAnoH());
            p++;

            break;
          case 4: // niv 2
            st.setString(p, param.getLogin().trim());
            p++;
            st.setString(p, param.getLogin().trim());
            p++;
            st.setString(p, indAuto.getAnoD());
            p++;
            st.setString(p, indAuto.getAnoH());
            p++;

            break;
        }

        rs = st.executeQuery();

        // extrae el registro encontrado
        while (rs.next()) {
          // a�ade un nodo
          data.addElement(new DataIndAuto
                          (rs.getString("CD_INDALAR"),
                           rs.getString("DS_INDALAR"),
                           ""));
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        break;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    data.trimToSize();
    return data;
  }

  /////// modificacion para debug
  /*
    public CLista doPrueba(int operacion, CLista parametros) throws Exception {
      jdcdriver = new JDCConnectionDriver ("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@10.160.5.149:1521:ORCL",
                             "pista",
                             "loteb98");
      Connection con = null;
      con = openConnection();
      return doWork(operacion, parametros);
    }
   */

}