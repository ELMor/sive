
package suca2;

import java.util.Hashtable;

public class datasuca
    extends Hashtable {

  public datasuca() {
  }

  //ARS 31-05-01, para si los datos son nulos, devuelva un blanco.
  public String getString(String key) {
    String sValue = (String) get(key);
    if (sValue == null) {
      sValue = new String("");
    }
    return sValue;
  }

}
