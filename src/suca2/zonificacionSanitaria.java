
package suca2;

public interface zonificacionSanitaria {

  public void setZonificacionSanitaria(String cdmun);

  public void cambiaModoTramero(boolean b);

  public int ponerEnEspera();

  public void ponerModo(int modo);

  public void vialInformado();
}
