package cn;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import catalogo2.CListaCat2;
import catalogo2.DataCat2;
import eqNot.DataAutoriza;
import fechas.conversorfechas;
import notdata.DataMunicipioEDO;
import sapp.StubSrvBD;
import zbs.DataZBS2;

public class Dial_CN
    extends CDialog {

//TIEMPOS
  long lIniDoPost = 0;
  long lFinDoPost = 0;

  // variables usadas para saber provincia y municipio actuales
  protected String codProvi = "";
  ResourceBundle res = ResourceBundle.getBundle("cn.Res" + app.getIdioma());
  protected String codMuni = "";

  //Modos de operaci�n de la ventana
  public static final int modoALTA = 0;
  public static final int modoMODIFICAR = 1;
  public static final int modoESPERA = 2;

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2; //CREo sobra`^^^^^^^^^^^^^^^^^
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIVASIS_X_CODIGO = 7;
  final int servletOBTENER_NIVASIS_X_CODIGO = 8;
  final int servletSELECCION_NIVASIS_X_DESCRIPCION = 9;
  final int servletOBTENER_NIVASIS_X_DESCRIPCION = 10;
  final int servletGENERA_COBERTURA = 10;
  // Autorizaciones
  final int servletOBTENER_PERFIL_3_X_DESCRIPCION = 9;
  final int servletOBTENER_PERFIL_4_X_DESCRIPCION = 10;
  final int servletSELECCION_PERFIL_3_X_DESCRIPCION = 11;
  final int servletSELECCION_PERFIL_4_X_DESCRIPCION = 12;
  final int servletOBTENER_PERFIL_3_X_CODIGO = 13;
  final int servletOBTENER_PERFIL_4_X_CODIGO = 14;
  final int servletSELECCION_PERFIL_3_X_CODIGO = 15;
  final int servletSELECCION_PERFIL_4_X_CODIGO = 16;
  final int servletSELECCION_PERFIL_2_X_CODIGO = 17;
  final int servletSELECCION_PERFIL_2_X_DESCRIPCION = 18;
  final int servletOBTENER_PERFIL_2_X_CODIGO = 19;
  final int servletOBTENER_PERFIL_2_X_DESCRIPCION = 20;

  // modos de operaci�n del servlet SrvMaestroEDO
  final int servletSELECCION_MUNICIPIO_X_CODIGO = 9;
  final int servletOBTENER_MUNICIPIO_X_CODIGO = 10;
  final int servletSELECCION_MUNICIPIO_X_DESCRIPCION = 11;
  final int servletOBTENER_MUNICIPIO_X_DESCRIPCION = 12;

  //Constantes del panel
  final String imgLUPA = "images/Magnify.gif";
  final String imgACEPTAR = "images/aceptar.gif";
  final String imgCANCELAR = "images/cancelar.gif";
  final String strSERVLETcn = "servlet/SrvCN";
  final String strSERVLETNiv1 = "servlet/SrvCat2";
  final String strSERVLETzbs2 = "servlet/SrvZBS2";
  final String strSERVLET_MAESTRO = "servlet/SrvMaestroEDO";

  //Para municipio
  final String strSERVLETMun = "servlet/SrvMunicipio";

  public DataCN datoA�adido = null;

  // par�metros
  protected int modoOperacion = modoALTA;
  protected int modoAnt = modoALTA;

  // listas de resultados
  protected CLista listaNivAsis = null;
  protected CLista listaCentro = null;

  // Informaci�n del usuario
  public DataAutoriza perfil_Usuario = null;
  public String perfil_nivel1 = "";
  public String perfil_nivel2 = "";
  public CLista lRes = null;

  //protected boolean cobertura = false;

  // Stub's
  protected StubSrvBD stubCentro = null;
  protected StubSrvBD stubNivel1 = null;
  protected StubSrvBD stubZBS2 = null;
  protected StubSrvBD stubNivAsis = null;
  protected StubSrvBD stubMun = null;

  protected int m_opcion;

  // cobertura
  public boolean bCobertura = false;

  XYLayout xYLayout1 = new XYLayout();
  Label lblCentro = new Label();
  CCampoCodigo txtCentro = new CCampoCodigo();
  TextField txtCentroDesc = new TextField();
  Label lblProvincia = new Label();
  CCampoCodigo txtProvincia = new CCampoCodigo();
  ButtonControl btnProvincia = new ButtonControl();
  ButtonControl btnMunicipio = new ButtonControl();
  Label lblMunicipio = new Label();
  CCampoCodigo txtMunicipio = new CCampoCodigo();
  TextField txtCP = new TextField();
  Label lblCP = new Label();
  TextField txtCalle = new TextField();
  Label lblCalle = new Label();
  TextField txtNumero = new TextField();
  Label lblNumero = new Label();
  TextField txtPiso = new TextField();
  Label lblPiso = new Label();
  Label lblTelefono = new Label();
  TextField txtTelefono = new TextField();
  Label lblFax = new Label();
  TextField txtFax = new TextField();
  Label lblNivel = new Label();
  Choice choiceNivel = new Choice();
  Checkbox checkCobertura = new Checkbox();
  Label lblAnyo = new Label();
  TextField txtSemanaDesde = new TextField();
  Label lblSemana = new Label();
  TextField txtAnyoDesde = new TextField();
  Checkbox checkBaja = new Checkbox();
  ButtonControl btnModificar = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();
  TextField txtDesProvincia = new TextField();
  TextField txtDesMunicipio = new TextField();

  Dial_CNTextFocusListener focusListener = null;

  public Dial_CN(CApp a, PanelBusca panBus, String titulo, int a_modo,
                 DataCN a_CN) {
    super(a);
    URL u;
    CLista datan;
    DataTA datosAux;
    CMessage msgBox = null;
    CLista data = null;

    try {
      setTitle(titulo);
      m_opcion = a_modo;

      listaNivAsis = new CLista();
      jbInit();
      listaNivAsis.setState(CLista.listaNOVALIDA);
      u = app.getCodeBase();
      stubCentro = new StubSrvBD(new URL(this.app.getURL() + strSERVLETcn));
      stubNivAsis = new StubSrvBD(new URL(this.app.getURL() + strSERVLETcn));
      stubNivel1 = new StubSrvBD(new URL(this.app.getURL() + strSERVLETNiv1));
      stubZBS2 = new StubSrvBD(new URL(this.app.getURL() + strSERVLETzbs2));
      stubMun = new StubSrvBD(new URL(this.app.getURL() + strSERVLETMun));
      btnMunicipio.setEnabled(false);

      // ahora leemos la tabla de nivel asistencial
      datan = new CLista();
      datan.setFilter("");
      datan.setState(CLista.listaVACIA);
      DataCN datosAuxil = null;
      btnMunicipio.setFocusAware(false);
      btnProvincia.setFocusAware(false);
      txtDesMunicipio.setEditable(false);
      txtDesMunicipio.setEnabled(false);
      txtDesProvincia.setEditable(false);
      txtDesProvincia.setEnabled(false);
      txtCentro.requestFocus();
      /*if (a_CN == null){
           datosAuxil = new DataCN("","","","","","","","","","","","","","","","","");
         datan.addElement(datosAuxil);
             }else{
         datosAuxil = new DataCN(a_CN.getCodCentro(),a_CN.getProv(),a_CN.getMun(),"","","","","","","","","","","","","","");
         datosAuxil.setDesMun(a_CN.getDesMun());
         datan.addElement(datosAuxil);
             }*/

      datosAuxil = new DataCN("", "", "", "", "", "", "", "", "", "", "", "",
                              "", "", "", "", "");
      datan.addElement(datosAuxil);
      datan.setIdioma(app.getIdioma());
      listaNivAsis = new CLista();

      listaNivAsis = (CLista) stubNivAsis.doPost(
          servletSELECCION_NIVASIS_X_CODIGO, datan);

      // rellena el choice de SIVE_NIV_ASIST
      for (int va = 0; va < listaNivAsis.size(); va++) {
        //# System.Out.println("UD");
        datosAux = (DataTA) listaNivAsis.elementAt(va);
        choiceNivel.addItem(datosAux.getDesc());
        //# System.Out.println(datosAux.getDesc());
      }

      modoOperacion = modoALTA;
      // JRM
      bCobertura = false;
      // no straemos los datos del centro
      // y repintamos la pantalla
      if (a_CN != null) {
        datan = null;
        datan = new CLista();
        datosAuxil = new DataCN(a_CN.getCodCentro(), a_CN.getProv(),
                                a_CN.getMun(), "", "", "", "", "", "", "", "",
                                "", "", "", "", "", "");
        datosAuxil.setDesMun(a_CN.getDesMun());
        datan.addElement(datosAuxil);

        listaCentro = (CLista) stubCentro.doPost(servletOBTENER_X_CODIGO, datan);

        /*
                 cn.SrvCN srv = new cn.SrvCN();
                 srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                             "dba_edo",
                             "manager");
                 listaCentro = srv.doDebug(servletOBTENER_X_CODIGO,datan);
         */

        rellenarPantalla();
      }
      Inicializar();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // aspecto de la ventana
  private void jbInit() throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        imgLUPA,
        imgACEPTAR,
        imgCANCELAR};
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    setSize(564, 350);

    xYLayout1.setHeight(307); //370);
    xYLayout1.setWidth(558);
    this.setLayout(xYLayout1);

    focusListener = new Dial_CNTextFocusListener(this);

    btnProvincia.setImage(imgs.getImage(0));
    btnMunicipio.setImage(imgs.getImage(0));
    btnAceptar.setImage(imgs.getImage(1));
    btnModificar.setImage(imgs.getImage(2));

    lblCentro.setText(res.getString("lblCentro.Text"));
    txtCentro.setBackground(new Color(255, 255, 150));
    txtCentroDesc.setBackground(new Color(255, 255, 150));
    txtSemanaDesde.setBackground(new Color(255, 255, 150));
    txtAnyoDesde.setBackground(new Color(255, 255, 150));
    lblProvincia.setText(res.getString("lblProvincia.Text"));
    txtProvincia.setBackground(new Color(255, 255, 150));
    btnProvincia.addActionListener(new Dial_CN_btnProvincia_actionAdapter(this));
    btnMunicipio.addActionListener(new Dial_CN_btnMunicipio_actionAdapter(this));
    txtMunicipio.setName("municipio");
    txtProvincia.setName("provincia");
    lblMunicipio.setText(res.getString("lblMunicipio.Text"));
    txtMunicipio.setBackground(new Color(255, 255, 150));
    txtCP.setBackground(new Color(255, 255, 150));
    txtMunicipio.addKeyListener(new Dial_CN_txtMunicipio_keyAdapter(this));
    txtProvincia.addKeyListener(new Dial_CN_txtProvincia_keyAdapter(this));
    lblCP.setText(res.getString("lblCP.Text"));
    txtCalle.setBackground(new Color(255, 255, 150));
    lblCalle.setText(res.getString("lblCalle.Text"));
    lblNumero.setText(res.getString("lblNumero.Text"));
    lblPiso.setText(res.getString("lblPiso.Text"));
    lblTelefono.setText(res.getString("lblTelefono.Text"));
    lblFax.setText(res.getString("lblFax.Text"));
    lblNivel.setText(res.getString("lblNivel.Text"));
    checkCobertura.setLabel(res.getString("checkCobertura.Label"));
    checkCobertura.addItemListener(new Dial_CN_checkCobertura_itemAdapter(this));
    lblAnyo.setText(res.getString("lblAnyo.Text"));
    lblSemana.setText(res.getString("lblSemana.Text"));
    checkBaja.setLabel(res.getString("checkBaja.Label"));
    btnModificar.setLabel(res.getString("btnModificar.Label"));
    btnModificar.addActionListener(new Dial_CN_btnModificar_actionAdapter(this));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnAceptar.addActionListener(new Dial_CN_btnAceptar_actionAdapter(this));

    txtMunicipio.addFocusListener(focusListener);
    txtProvincia.addFocusListener(focusListener);

    this.add(lblCentro, new XYConstraints(25, 27, 43, -1));
    this.add(txtCentro, new XYConstraints(83, 27, 151, -1));
    this.add(txtCentroDesc, new XYConstraints(299, 27, 240, -1));
    this.add(lblProvincia, new XYConstraints(25, 58, 56, -1));
    this.add(txtProvincia, new XYConstraints(83, 58, 151, -1));
    this.add(btnProvincia, new XYConstraints(241, 58, -1, -1));
    this.add(txtDesProvincia, new XYConstraints(299, 58, 240, -1));
    this.add(lblMunicipio, new XYConstraints(24, 88, 56, -1));
    this.add(txtMunicipio, new XYConstraints(83, 88, 151, -1));
    this.add(btnMunicipio, new XYConstraints(241, 88, -1, -1));
    this.add(txtDesMunicipio, new XYConstraints(299, 88, 240, -1));
    this.add(txtCP, new XYConstraints(83, 119, 151, -1));
    this.add(lblCP, new XYConstraints(25, 119, 30, -1));
    this.add(txtCalle, new XYConstraints(299, 119, 241, -1));
    this.add(lblCalle, new XYConstraints(241, 119, 41, -1));
    this.add(txtNumero, new XYConstraints(83, 149, 37, -1));
    this.add(lblNumero, new XYConstraints(25, 149, 54, -1));
    this.add(txtPiso, new XYConstraints(180, 149, 54, -1));
    this.add(lblPiso, new XYConstraints(139, 149, 33, -1));
    this.add(lblTelefono, new XYConstraints(241, 149, 56, -1));
    this.add(txtTelefono, new XYConstraints(299, 149, 83, -1));
    this.add(lblFax, new XYConstraints(419, 149, 29, -1));
    this.add(txtFax, new XYConstraints(456, 149, 84, -1));
    this.add(lblNivel, new XYConstraints(25, 178, 97, -1));
    this.add(choiceNivel, new XYConstraints(139, 178, 244, -1));
    this.add(checkCobertura, new XYConstraints(25, 208, 247, -1));
    this.add(lblAnyo, new XYConstraints(374, 208, 25, -1));
    this.add(lblSemana, new XYConstraints(456, 208, 51, -1));
    this.add(txtAnyoDesde, new XYConstraints(400, 208, 52, -1));
    this.add(txtSemanaDesde, new XYConstraints(509, 208, 31, -1));
    this.add(checkBaja, new XYConstraints(25, 238, 105, -1));
    this.add(btnModificar, new XYConstraints(458, 263, 80, 26));
    this.add(btnAceptar, new XYConstraints(373, 263, 80, 26));
  }

  /** para la sincronizaci�n */
  protected boolean sinBloquear = true;

  /** ete m�todo sirve para mantener una sincronizaci�n en los eventos */
  public synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoOperacion = modoESPERA;
      Inicializar();

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  /** este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    modoOperacion = m_opcion;
    Inicializar();

    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:
        codProvi = txtProvincia.getText();
        txtCentro.setEditable(true);
        txtCentro.setEnabled(true);
        txtCentroDesc.setEditable(true);
        txtCentroDesc.setEnabled(true);
        txtProvincia.setEditable(true);
        txtProvincia.setEnabled(true);
        txtDesProvincia.setEditable(false);
        btnProvincia.setEnabled(true);
        txtDesMunicipio.setEditable(false);

        txtCalle.setEnabled(true);
        txtCP.setEnabled(true);
        txtFax.setEnabled(true);
        txtNumero.setEnabled(true);
        txtPiso.setEnabled(true);
        txtTelefono.setEnabled(true);

        btnAceptar.setEnabled(true);
        btnModificar.setEnabled(true);
        if (txtDesProvincia.getText().length() > 0) {
          txtMunicipio.setEditable(true);
          txtMunicipio.setEnabled(true);
          btnMunicipio.setEnabled(true);
        }
        else {
          txtMunicipio.setEditable(false);
          txtMunicipio.setEnabled(false);
          btnMunicipio.setEnabled(false);
        }
        lblAnyo.setVisible(false);
        txtAnyoDesde.setVisible(false);
        lblSemana.setVisible(false);
        txtSemanaDesde.setVisible(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:
        codProvi = txtProvincia.getText();
        txtCentro.setEditable(false);
        txtCentro.setEnabled(true);
        txtCentroDesc.setEditable(true);
        txtCentroDesc.setEnabled(true);
        txtProvincia.setEditable(true);
        txtProvincia.setEnabled(true);
        btnProvincia.setEnabled(true);
        txtDesProvincia.setEditable(false);
        txtDesMunicipio.setEditable(false);
        if (txtDesProvincia.getText().length() > 0) {
          btnMunicipio.setEnabled(true);
          txtMunicipio.setEditable(true);
          txtMunicipio.setEnabled(true);
        }
        else {
          txtMunicipio.setEditable(false);
          txtMunicipio.setEnabled(false);
          btnMunicipio.setEnabled(false);
        }
        btnAceptar.setEnabled(true);
        btnModificar.setEnabled(true);
        txtAnyoDesde.setEnabled(true);
        txtCalle.setEnabled(true);
        txtCP.setEnabled(true);
        txtFax.setEnabled(true);
        txtNumero.setEnabled(true);
        txtPiso.setEnabled(true);
        txtTelefono.setEnabled(true);

//        if (checkCobertura.getState() != bCobertura){
        // JRM
        if (bCobertura) {
          txtAnyoDesde.setEnabled(true);
          txtSemanaDesde.setEnabled(true);
          lblAnyo.setVisible(true);
          txtAnyoDesde.setVisible(true);
          lblSemana.setVisible(true);
          txtSemanaDesde.setVisible(true);
        }
        else {
          lblAnyo.setVisible(false);
          txtAnyoDesde.setVisible(false);
          //txtAnyoDesde.setText("");
          lblSemana.setVisible(false);
          txtSemanaDesde.setVisible(false);
          //txtSemanaDesde.setText("");
        }

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnProvincia.setEnabled(false);
        btnMunicipio.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnModificar.setEnabled(false);
        txtAnyoDesde.setEnabled(false);
        txtCalle.setEnabled(false);
        txtCentro.setEnabled(false);
        txtCentroDesc.setEnabled(false);
        txtCP.setEnabled(false);
        txtFax.setEnabled(false);
        txtMunicipio.setEnabled(false);
        txtNumero.setEnabled(false);
        txtPiso.setEnabled(false);
        txtProvincia.setEnabled(false);
        txtSemanaDesde.setEnabled(false);
        txtTelefono.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  /** pierde el foco el texto dela provincia */
  public void obtenerProvincia() {
    String strCodProv = txtProvincia.getText();
    String strDesProv = txtDesProvincia.getText();
    CLista result, data;
    // vamos a por la descripci�n de la provincia
    if (strCodProv != null && strCodProv.trim().length() > 0 &&
        (strDesProv == null || strDesProv.trim().length() == 0)) {
      try {
        data = new CLista();
        data.addElement(new DataCat2(strCodProv));
        data.setState(CLista.listaINCOMPLETA);
        lIniDoPost = System.currentTimeMillis(); //^^^^^^^^^^^^
        data.setIdioma(app.getIdioma());
        result = (CLista) stubNivel1.doPost(servletOBTENER_X_CODIGO +
                                            catalogo.Catalogo.catCCAA, data);
        lFinDoPost = System.currentTimeMillis(); //^^^^^^^^^^^^

        /*
           CMessage msgTiempos= new CMessage(getCApp(),CMessage.msgAVISO,"Tiempo doPost Prov"+ ( (lFinDoPost-lIniDoPost)/1000)  );
           msgTiempos.show();
           msgTiempos= null;
         */

        if (result != null && result.size() > 0) {
          strDesProv = ( (DataCat2) result.firstElement()).getDes();
        }
        else {
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                         res.getString("msg2.Text"));
          msgBox.show();
          msgBox = null;
        }
      }
      catch (Exception exc) {
        //# System.Out.println("Error leyendo Provincia "+ exc.toString());
      }
      if (strDesProv != null) {
        txtDesProvincia.setText(strDesProv);
      }
      if (txtDesProvincia.getText().length() > 0) {
        txtMunicipio.setText(""); //****************Estas 3 inst  creo deben ir en el if anterior
        txtDesMunicipio.setText("");
        btnMunicipio.setEnabled(true);
      }
    }
  }

  public void obtenerMunicipio() {
    String strCodMun = txtMunicipio.getText();
    String strDesMun = txtDesMunicipio.getText();
    CLista result, data;
    DataMunicipioEDO datmun;
    // vamos a por la descripci�n del municipio
    if (strCodMun != null && strCodMun.trim().length() > 0 &&
        (strDesMun == null || strDesMun.trim().length() == 0)) {
      try {

        data = new CLista();
        data.setIdioma(app.getIdioma());
        data.addElement(new DataMunicipioEDO(txtMunicipio.getText(),
                                             txtProvincia.getText()));
        stubMun.setUrl(new URL(app.getURL() + strSERVLETMun));

        lIniDoPost = System.currentTimeMillis(); //^^^^^^^^^^^^
        result = (CLista)this.stubMun.doPost(servletOBTENER_MUNICIPIO_X_CODIGO,
                                             data);
        lFinDoPost = System.currentTimeMillis(); //^^^^^^^^^^^^
        /*
            CMessage msgTiempos= new CMessage(getCApp(),CMessage.msgAVISO,"Tiempo doPost Mun"+ ( (lFinDoPost-lIniDoPost)/1000)  );
            msgTiempos.show();
            msgTiempos= null;
         */

        if ( (result != null) &&
            (result.size() > 0)) {
          datmun = (DataMunicipioEDO) result.firstElement();
          codMuni = datmun.getCodMun();
          txtDesMunicipio.setText(datmun.getDesMun());
          txtMunicipio.setText(codMuni);
        }
        else {
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                         res.getString("msg3.Text"));
          msgBox.show();
          msgBox = null;
        }
      }
      catch (Exception exc) {
        //# System.Out.println("Error leyendo Muncipio "+ exc.toString());
      }
    }
  }

  public void rellenarPantalla() {
    String strDesProv = "", strDesMun = "";
    CLista data, result;
    DataTA datosAux = new DataTA("", "");

    if (listaCentro != null && listaCentro.size() > 0) {
      DataCN datoCen = (DataCN) listaCentro.firstElement();
      txtCentro.setText(datoCen.getCodCentro());
      txtCentroDesc.setText(datoCen.getCentroDesc());
      txtProvincia.setText(datoCen.getProv());
      codProvi = datoCen.getProv();
      txtMunicipio.setText(datoCen.getMun());
      txtDesMunicipio.setText(datoCen.getDesMun()); //strDesMun);
      codMuni = datoCen.getMun();
      txtCP.setText(datoCen.getCPostal());
      txtCalle.setText(datoCen.getDireccion());
      txtNumero.setText(datoCen.getNum());
      txtPiso.setText(datoCen.getPiso());
      txtTelefono.setText(datoCen.getTelefono());
      txtFax.setText(datoCen.getFax());
      for (int rf = 0; rf < listaNivAsis.size(); rf++) {
        datosAux = (DataTA) listaNivAsis.elementAt(rf);
        if (datosAux.getCodigo().equals(datoCen.getNivAsis())) {
          choiceNivel.select(rf);
        }
      }
      txtAnyoDesde.setText(datoCen.getAnDesde());
      txtSemanaDesde.setText(datoCen.getSemDesde());
      if (datoCen.getCobertura().compareTo("S") == 0) {
        checkCobertura.setState(true);
        bCobertura = true;
      }
      else {
        checkCobertura.setState(false);
        bCobertura = false;
      }

      if (datoCen.getBaja().compareTo("S") == 0) {
        checkBaja.setState(true);
      }
      else {
        checkBaja.setState(false);
      }
      DataTA datosTA;
      for (int i = 0; i < listaNivAsis.size(); i++) {
        datosTA = (DataTA) listaNivAsis.elementAt(i);
        if (datosTA.getCodigo().equals(datoCen.getNivAsis())) {
          choiceNivel.select(i);
          break;
        }
      }
      // busco la descripci�n del municipio y la provincia
      obtenerMunicipio();
      obtenerProvincia();
      modoOperacion = modoMODIFICAR;
      Inicializar();
    }
  }

  /** devuelve true si la fecha es mala */
  protected boolean erroenaFecha(String anio, String sem) {
    boolean result = false;
    try {
      conversorfechas convDesde = new conversorfechas(anio, app);
      String s = convDesde.getFec(Integer.parseInt(sem));
      if (s.trim().length() == 0) {
        result = true;
      }
    }
    catch (Exception exc) {
    }

    return result;
  }

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;

    boolean fallo = false;

    if (txtAnyoDesde.isVisible() &&
        erroenaFecha(txtAnyoDesde.getText(), txtSemanaDesde.getText())) {
      msg = res.getString("msg4.Text");
      fallo = true;
    }

    // comprueba que esten informados los campos obligatorios
    if (txtCentro.getText().length() <= 0) {
      msg = res.getString("msg5.Text");
      fallo = true;
    }
    else if (txtCentro.getText().length() > 6) {
      msg = res.getString("msg6.Text");
      fallo = true;
    }

    if (txtProvincia.getText().length() <= 0) {
      msg = res.getString("msg7.Text");
      fallo = true;
    }
    else if (txtProvincia.getText().length() > 2) {
      msg = res.getString("msg8.Text");
      fallo = true;
    }
    else {
      codProvi = txtProvincia.getText();
    }

    if (txtMunicipio.getText().length() <= 0) {
      msg = "El municipio es obligatorio";
      fallo = true;
    }
    else if (txtMunicipio.getText().length() > 3) {
      msg = "El c�digo de municipio tienen un m�ximo de 3 caracteres";
      fallo = true;
    }
    else {
      codMuni = txtMunicipio.getText();
    }

    if (txtCentroDesc.getText().length() <= 0) {
      msg = "La descripci�n es obligatorio";
      fallo = true;
    }
    else if (txtCentroDesc.getText().length() > 50) {
      msg = "La descripci�n tienen un m�ximo de 50 caracteres";
      fallo = true;
    }

    if (txtCalle.getText().length() <= 0) {
      msg = "La direcci�n es obligatoria";
      fallo = true;
    }
    else if (txtCalle.getText().length() > 40) {
      msg = "La direcci�n tienen un m�ximo de 40 caracteres";
      fallo = true;
    }

    if (txtCP.getText().length() <= 0) {
      msg = "El c�digo postal es obligatorio";
      fallo = true;
    }
    else if (txtCP.getText().length() > 5) {
      msg = "El c�digo postal tienen un m�ximo de 5 caracteres";
      fallo = true;
    }

    if (txtCentro.getText().length() > 6) {
      fallo = true;
      msg = "Se ha superado el tama�o de los campos.";
      txtCentro.selectAll();
    }

    if (txtFax.getText().length() > 9) {
      fallo = true;
      msg = "Se ha superado el tama�o de los campos.";
      txtFax.selectAll();
    }
    if (txtNumero.getText().length() > 5) {
      fallo = true;
      msg = "Se ha superado el tama�o de los campos.";
      txtNumero.selectAll();
    }
    if (txtPiso.getText().length() > 5) {
      fallo = true;
      msg = "Se ha superado el tama�o de los campos.";
      txtPiso.selectAll();
    }
    if (txtTelefono.getText().length() > 14) {
      fallo = true;
      msg = "Se ha superado el tama�o de los campos.";
      txtTelefono.selectAll();
    }

    if (choiceNivel.getItemCount() == 0) {
      fallo = true;
      msg = "Seleccionar un nivel asistencial.";
    }

    if (fallo) {
      msgBox = new CMessage(app, CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }
    return!fallo;
  }

  protected boolean realiza_actionPerformed(ActionEvent evt) {
    if (m_opcion == modoALTA) {
      return btnAceptar_actionPerformed(evt);
    }
    else if (m_opcion == modoMODIFICAR) {
      return btnModificar_actionPerformed(evt);
    }
    else {
      return false;
    }
  }

  protected boolean btnAceptar_actionPerformed(ActionEvent e) {
    CLista datos = null;
    CLista ln1 = null;
    CMessage mensaje = null;
    String esBaja = null;
    String esCobertura = null;
    DataTA elDato = null;
    String elNivel = null;
    boolean result = false;

    ln1 = new CLista();
    modoAnt = modoOperacion;

    if (isDataValid()) {
      if (checkBaja.getState()) {
        esBaja = "S";
      }
      else {
        esBaja = "N";
      }
      if (checkCobertura.getState()) {
        esCobertura = "S";
      }
      else {
        esCobertura = "N";
      }
      elDato = (DataTA) listaNivAsis.elementAt(this.choiceNivel.
                                               getSelectedIndex());
      elNivel = elDato.getCodigo();
      // Aqu� insertar�amos la informaci�n
      try {
        if ( (m_opcion == modoALTA)) {

          datos = new CLista();
          datos.setLogin(this.app.getLogin());
          datos.setFilter("");
          datoA�adido = new DataCN(txtCentro.getText().toUpperCase(),
                                   codProvi.toUpperCase(), codMuni.toUpperCase(),
                                   txtCentroDesc.getText(), txtCalle.getText(),
                                   txtNumero.getText(),
                                   txtPiso.getText(), txtCP.getText(),
                                   txtTelefono.getText(), txtFax.getText(),
                                   elNivel, esCobertura, "", this.app.getLogin(),
                                   esBaja, txtAnyoDesde.getText(),
                                   txtSemanaDesde.getText());
          datoA�adido.setDesMun(txtDesMunicipio.getText());
          datoA�adido.setDesProv(txtDesProvincia.getText());
          datoA�adido.setDesLProv(txtDesProvincia.getText());
          datos.addElement(datoA�adido);
          lIniDoPost = System.currentTimeMillis(); //^^^^^^^^^^^^
          ln1 = (CLista)this.stubCentro.doPost(servletALTA, datos);
          lFinDoPost = System.currentTimeMillis(); //^^^^^^^^^^^^
          /*
              CMessage msgTiempos= new CMessage(getCApp(),CMessage.msgAVISO,"Tiempo doPost ALTA"+ ( (lFinDoPost-lIniDoPost)/1000)  );
              msgTiempos.show();
              msgTiempos= null;
           */

          result = true;
          datos = null;
        }
        //writeListaModelo();
      }
      catch (Exception excepc) {
        excepc.printStackTrace();
        mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
        mensaje.show();
        datoA�adido = null;
      }
    }
    return result;
  }

  protected boolean btnModificar_actionPerformed(ActionEvent e) {
    CLista datos = null;
    CLista ln1 = null;
    CMessage mensaje = null;
    String esBaja = null;
    String esCobertura = null;
    DataTA elDato = null;
    String elNivel = null;
    boolean result = false;

    ln1 = new CLista();
    modoAnt = modoOperacion;

    if (isDataValid()) {
      if (checkBaja.getState()) {
        esBaja = "S";
        //# System.Out.println("Baja");
      }
      else {
        esBaja = "N";
        //# System.Out.println("No Baja");
      }
      if (checkCobertura.getState()) {
        esCobertura = "S";
        //# System.Out.println("Cobertura");
      }
      else {
        esCobertura = "N";
        //# System.Out.println("No Cobertura");
      }
      elDato = (DataTA) listaNivAsis.elementAt(this.choiceNivel.
                                               getSelectedIndex());
      elNivel = elDato.getCodigo();
      // Aqu� insertar�amos la informaci�n
      try {
        datos = new CLista();
        datos.setLogin(this.app.getLogin());
        datos.setFilter("");
        datoA�adido = new DataCN(txtCentro.getText().toUpperCase(),
                                 codProvi.toUpperCase(), codMuni.toUpperCase(),
                                 txtCentroDesc.getText(), txtCalle.getText(),
                                 txtNumero.getText(),
                                 txtPiso.getText(), txtCP.getText(),
                                 txtTelefono.getText(), txtFax.getText(),
                                 elNivel, esCobertura, "", this.app.getLogin(),
                                 esBaja, txtAnyoDesde.getText(),
                                 txtSemanaDesde.getText());
        datoA�adido.setDesMun(txtDesMunicipio.getText());
        datoA�adido.setDesProv(txtDesProvincia.getText());
        datoA�adido.setDesLProv(txtDesProvincia.getText());
        datos.addElement(datoA�adido);
        lIniDoPost = System.currentTimeMillis(); //^^^^^^^^^^^^
        ln1 = (CLista)this.stubCentro.doPost(servletMODIFICAR, datos);

        /*
                 cn.SrvCN srv = new cn.SrvCN();
                 srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                             "dba_edo",
                             "manager");
                 ln1 = srv.doDebug(servletMODIFICAR,datos);
         */

        lFinDoPost = System.currentTimeMillis(); //^^^^^^^^^^^^
        /*
            CMessage msgTiempos= new CMessage(getCApp(),CMessage.msgAVISO,"Tiempo doPost MOD"+ ( (lFinDoPost-lIniDoPost)/1000)  );
            msgTiempos.show();
            msgTiempos= null;
         */

        //# System.Out.println("despu�s");
        result = true;
        //datos=null;
      }
      catch (Exception excepc) {
        excepc.printStackTrace();
        mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
        mensaje.show();
        modoOperacion = modoALTA;
      }
    }
    return result;
  }

  void btnProvincia_actionPerformed(ActionEvent e) {

    DataCat2 data;
    int modo = modoOperacion;
    CMessage msgBox = null;

    try {
      CListaCat2 lista = new CListaCat2(app,
                                        res.getString("msg9.Text"),
                                        stubNivel1,
                                        strSERVLETNiv1,
                                        servletOBTENER_X_CODIGO +
                                        catalogo.Catalogo.catCCAA,
                                        servletOBTENER_X_DESCRIPCION +
                                        catalogo.Catalogo.catCCAA,
                                        servletSELECCION_X_CODIGO +
                                        catalogo.Catalogo.catCCAA,
                                        servletSELECCION_X_DESCRIPCION +
                                        catalogo.Catalogo.catCCAA);
      lista.show();
      data = (DataCat2) lista.getComponente();

      if (data != null) {
        //borraTodo();
        txtProvincia.setText(data.getCod());
        txtDesProvincia.setText(data.getDes());
        codProvi = data.getCod();
        //areaDesc.setText(data.getDes());
        if (txtDesProvincia.getText().length() > 0) {
          btnMunicipio.setEnabled(true);
        }
        else {
          btnMunicipio.setEnabled(false);
        }
      }
      modoOperacion = modo;
      Inicializar();
      //txtDesProvincia.setText(data.getDes());
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
      modoOperacion = modo;
    }

  }

  void btnMunicipio_actionPerformed(ActionEvent e) {

    DataMunicipioEDO data;
    CMessage msgBox;

    try {

      CListaMun3 lista = new CListaMun3(this,
                                        res.getString("msg10.Text"),
                                        stubMun,
                                        strSERVLETMun,
                                        servletOBTENER_MUNICIPIO_X_CODIGO,
                                        servletOBTENER_MUNICIPIO_X_DESCRIPCION,
                                        servletSELECCION_MUNICIPIO_X_CODIGO,
          servletSELECCION_MUNICIPIO_X_DESCRIPCION);
      lista.show();
      data = (DataMunicipioEDO) lista.getComponente();
      if (data != null) {
        codMuni = data.getCodMun();
        txtDesMunicipio.setText(data.getDesMun());
        txtMunicipio.setText(codMuni);
        btnMunicipio.setEnabled(true);
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

  }

  void txtProvincia_keyPressed(KeyEvent e) {
    codProvi = "";
    btnMunicipio.setEnabled(false);
    codMuni = "";
    txtMunicipio.setText("");
    txtDesMunicipio.setText("");
    txtDesProvincia.setText("");
  }

  void txtMunicipio_keyPressed(KeyEvent e) {
    codMuni = "";
    txtDesMunicipio.setText("");
  }
}

class Dial_CNTextFocusListener
    implements java.awt.event.FocusListener, Runnable {
  Dial_CN adaptee;
  FocusEvent e = null;

  Dial_CNTextFocusListener(Dial_CN adaptee) {
    this.adaptee = adaptee;
  }

  public void focusGained(FocusEvent e) {
  }

  public void focusLost(FocusEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void run() {
    String name2 = ( (Component) e.getSource()).getName();

    if (name2.equals("provincia")) {
      adaptee.obtenerProvincia();
    }
    else if (name2.equals("municipio")) {
      adaptee.obtenerMunicipio();
    }
    adaptee.desbloquea();

  }

} //_______________________________________________ END_CLASS

class Dial_CN_checkCobertura_itemAdapter
    implements java.awt.event.ItemListener {
  Dial_CN adaptee;

  Dial_CN_checkCobertura_itemAdapter(Dial_CN adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.bCobertura = adaptee.checkCobertura.getState();
    adaptee.Inicializar();
  }
}

class Dial_CN_btnAceptar_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Dial_CN adaptee;
  ActionEvent e;

  Dial_CN_btnAceptar_actionAdapter(Dial_CN adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void run() {
    if (adaptee.realiza_actionPerformed(e)) {
      adaptee.dispose();
    }
    adaptee.desbloquea();
  }
}

// cancelar
class Dial_CN_btnModificar_actionAdapter
    implements java.awt.event.ActionListener {
  Dial_CN adaptee;
  ActionEvent e;

  Dial_CN_btnModificar_actionAdapter(Dial_CN adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    adaptee.dispose();
  }
}

class Dial_CN_btnProvincia_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Dial_CN adaptee;
  ActionEvent e;

  Dial_CN_btnProvincia_actionAdapter(Dial_CN adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void run() {
    adaptee.btnProvincia_actionPerformed(e);
    adaptee.desbloquea();
  }
}

class Dial_CN_btnMunicipio_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Dial_CN adaptee;
  ActionEvent e;

  Dial_CN_btnMunicipio_actionAdapter(Dial_CN adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void run() {
    adaptee.btnMunicipio_actionPerformed(e);
    adaptee.desbloquea();
  }
}

class Dial_CN_txtProvincia_keyAdapter
    extends java.awt.event.KeyAdapter {
  Dial_CN adaptee;

  Dial_CN_txtProvincia_keyAdapter(Dial_CN adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtProvincia_keyPressed(e);

  }
}

class Dial_CN_txtMunicipio_keyAdapter
    extends java.awt.event.KeyAdapter {
  Dial_CN adaptee;

  Dial_CN_txtMunicipio_keyAdapter(Dial_CN adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtMunicipio_keyPressed(e);
  }
}

// lista de valores

// lista de valores
class CListaZBS3
    extends CListaValores {

  protected Dial_CN panel;

  public CListaZBS3(Dial_CN p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getCApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtProvincia.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

// lista de valores
class CListaMun3
    extends CListaValores {

  protected Dial_CN panel;

  public CListaMun3(Dial_CN p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descripcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getCApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataMunicipioEDO(s, panel.txtProvincia.getText().trim());
  }

  public String getCodigo(Object o) {
    return ( ( (DataMunicipioEDO) o).getCodMun());
  }

  public String getDescripcion(Object o) {
    //ARG: No hace falta a�adir la descripcion de la provincia
    //return ( ((DataMunicipioEDO)o).getDesMun() + " "+ ((DataMunicipioEDO)o).getDesProv() );

    return ( ( (DataMunicipioEDO) o).getDesMun());
  }
}
