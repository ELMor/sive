package cn;

import java.io.Serializable;

public class DataTA
    implements Serializable {

  protected String cod = "";
  protected String desc = "";

  public DataTA(String co, String de) {
    cod = co;
    desc = de;
  }

  public String getCodigo() {
    return cod;
  }

  public String getDesc() {
    return desc;
  }
}