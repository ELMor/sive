package suca;

import java.util.Enumeration;
import java.util.Hashtable;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import capp.CTabla;
//Im�genes
import comun.Common;
import jclass.bwt.JCActionEvent;
import sapp.StubSrvBD;

public class DialPortalSuca
    extends CDialog {

//__________________________________________________ MODOS
  //modos de operaci�n de la ventana
  public final int modoINICIO = 0;
  public final int modoESPERA = 1;

  /** indica en que estado est� el frame */
  protected int modoOperacion = 0;
  protected int modoAnterior = 0;

  /** esta es la lista que contiene los datos que se muestran en la tabla */
  CLista listaPortal = null;

  /** esta lista son los datos del enfermos seleccionado */
  CLista listaPortalSeleccionado = null;

  /** Portal seleccionado */
  CLista listaDatosBusqueda = null;

  //protected boolean bPerNom = true;

  /** Par�metros de carga de la tabla */
  protected String cdvial = "";
  protected String cdmuni = "";

  protected String cdtpnum = "";
  protected String dscalnum = "";
  protected String dsnumportal = "";

  protected String prov = "";

  /** Permite saber si ha habido datos tras una b�squeda */
  protected boolean bHayDatos = false;

  /** Para saber el modo de llamada al servlet */
  protected int modoLlamada = 0;

  /** Cadena para el mensaje 'no hay datos' */
  private final String strNoHayDatos =
      "No hay portales que respondan a esas caracter�sticas";

// SINCRONIZACION
  /** esta variable se pone a true cuando no se inhiben los eventos
   *  y a false se rechazan todos los eventos
   */
  protected boolean sinBloquear = true;

  public StubSrvBD stubCliente = new StubSrvBD();

  //protected panelsuca pnlSuca = null;

//__________________________________________________ COMP GRAFICOS
//  BevelPanel pnl = new BevelPanel();
  XYLayout xYLayout = new XYLayout();
  CTabla tablaPortal = new CTabla();
  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();
  GroupBox pnlEnfermedad = new GroupBox();

  //______________________________________________ GESTOR de EVENTOS
  BtnPortalActionListener btnActionListener = new BtnPortalActionListener(this);
  PortalTableAdapter tableAdapter = new PortalTableAdapter(this);

  //______________________________________________ CONSTRUCTOR
  public DialPortalSuca(CApp app) {
    super(app);
    setTitle("Di�logo de selecci�n de portales");

    try {
      CLista parametros = null, result = null;
      jbInit();
      pack();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /** este m�todo sirve para mantener una sincronizaci�n en los eventos */
  public synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modoOperacion;
      modoOperacion = modoESPERA;
      //Inicializar();
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  /** este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    if (modoOperacion == modoESPERA) {
      modoOperacion = modoAnterior;
    }
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  void jbInit() throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        Common.imgLUPA,
        Common.imgBUSCAR,
        Common.imgLIMPIAR,
        Common.imgACEPTAR,
        Common.imgCANCELAR};
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    // fijamos las dimensiones del di�logo
    setSize(600, 250);
    xYLayout.setHeight(275);
    xYLayout.setWidth(600);

    setLayout(xYLayout);

    // Im�genes de los botones
    btnAceptar.setImage(imgs.getImage(3));
    btnCancelar.setImage(imgs.getImage(4));

    tablaPortal.setColumnButtonsStrings(jclass.util.JCUtilConverter.
                                        toStringList(new String(
        "Numeracion\nPortal\nCalificador"), '\n'));
    tablaPortal.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new
        String("160\n60\n330"), '\n'));
    tablaPortal.setNumColumns(3);

    btnCancelar.setLabel("Cancelar");
    btnAceptar.setLabel("Aceptar");

    // a�adimos el nombre de los botones
    btnCancelar.setActionCommand("btnCancelar");
    btnAceptar.setActionCommand("btnAceptar");

    // a�adimos al panel todos los componentes
    // primero a�adimos las listas y despu�s el resto de componentes
    // panel enfermedad
    this.add(tablaPortal, new XYConstraints(15, 20, 570, 178));
    this.add(btnCancelar, new XYConstraints(504, 210, -1, -1));
    this.add(btnAceptar, new XYConstraints(427, 210, -1, -1));

    // gesti�n de eventos de botones
    btnCancelar.addActionListener(btnActionListener);
    btnAceptar.addActionListener(btnActionListener);

    //gesti�n de eventos de la tabla
    tablaPortal.addActionListener(tableAdapter);

    // se inicializan los botones a false o true
    this.modoOperacion = modoINICIO;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      case modoINICIO:
        break;
      case modoESPERA:
        break;
    }
  }

  /**
   *  Esta funci�n devuelve el idioma
   */
  public int getIdioma() {
    return app.getIdioma();
  }

  public CLista getListaDatosPortal() {
    return listaPortalSeleccionado;
  }

  public void setListaDatosPortal(CLista lista) {
    listaPortalSeleccionado = lista;
  }

  protected void lanza_busqueda() {
    // rellenamos la hashtable solo con los datos que tengan valores utiles
    datasuca datosPortal = new datasuca();

    bHayDatos = true;

    String filtro = null;
    int indice = 0;
    CLista result = null;

    listaDatosBusqueda = new CLista();
    listaDatosBusqueda.setIdioma(getIdioma());

    datosPortal.put("CDMUNI", cdmuni);
    datosPortal.put("CDVIAL", cdvial);
    datosPortal.put("CDTPNUM", cdtpnum);
    datosPortal.put("DSNMPORTAL", dsnumportal);
    datosPortal.put("DSCALNUM", dscalnum);
    datosPortal.put("CDPROV", prov);

    listaDatosBusqueda.addElement(datosPortal);

    listaPortal = Common.traerDatos(app, stubCliente, panelsuca.strSERVLET_SUCA,
                                    modoLlamada, listaDatosBusqueda);

    /*
         // debug
         srvsuca srv = new srvsuca();
         // par�metros jdbc
         srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                "jdbc:oracle:thin:@192.168.0.13:1521:ICM",
                                "dba_edo",
                                "manager");
         listaPortal = srv.doDebug(modoLlamada, listaDatosBusqueda);
     */

    if (listaPortal != null) {
      if (listaPortal.size() == 0) {
        CMessage msgBox = new CMessage(app, CMessage.msgAVISO, strNoHayDatos);
        msgBox.show();
        msgBox = null;
        bHayDatos = false;
      }
      else {
        escribirTabla(listaPortal);
      }
    }
  }

  /**
   *
   */
  boolean tabla_actionPerformed(JCActionEvent e) {
    int indice = tablaPortal.getSelectedIndex();
    Hashtable h;
    Object code = null;
    String descrip = null;
    int indLista = -1;

    if (indice < 0) {
      return false;
    }

    if (indice == listaPortal.size()) {
      /// se ha pinchado el elemento M�s...
      listaDatosBusqueda.setFilter(listaPortal.getFilter());
      CLista result = Common.traerDatos(app, stubCliente,
                                        panelsuca.strSERVLET_SUCA, modoLlamada,
                                        listaDatosBusqueda);
      listaPortal.appendData(result);
      escribirTabla(listaPortal);
    }
    else {
      /// se buscan los datos del Portal pinchado y se vuelve
      h = (Hashtable) (listaPortal.elementAt(indice));

      datasuca dPortal = new datasuca();

      dPortal.put("DSTNUM", h.get("DSTNUM"));
      dPortal.put("DSNMPORTAL", h.get("DSNMPORTAL"));
      dPortal.put("DSDSCALNUM", h.get("DSDSCALNUM"));
      dPortal.put("CDPOSTAL", h.get("CDPOSTAL"));
      listaPortalSeleccionado = new CLista();
      listaPortalSeleccionado.setIdioma(app.getIdioma());
      listaPortalSeleccionado.addElement(dPortal);
    }
    return true;
  }

  // rellena la tabla
  void escribirTabla(CLista lista) {
    datasuca datUnaLinea;
    StringBuffer datLineaEscribir = new StringBuffer();
    String dato = null;
    int tam;

    //Borramos la tabla que hab�a
    tam = tablaPortal.countItems();
    if (tam > 0) {
      tablaPortal.deleteItems(0, tam - 1);
    }

    if (lista == null || (lista.size() == 0)) {
      CMessage msgBox = new CMessage(app, CMessage.msgAVISO, strNoHayDatos);
      msgBox.show();
      msgBox = null;
      bHayDatos = false;
      return;
    }

    Enumeration enum = lista.elements();

    while (enum.hasMoreElements()) {
      //Recogemos los datos de una l�nea
      datUnaLinea = (datasuca) (enum.nextElement());
      //ponemos a cero el buffer
      datLineaEscribir.setLength(0);

      Object o = (datUnaLinea.get("DSTNUM"));
      if (o != null) {
        datLineaEscribir.append(o.toString());
      }
      datLineaEscribir.append("&");

      //Aqu� se a�ade el resto de los datos
      dato = (String) datUnaLinea.get("DSNMPORTAL");
      if (dato != null) {
        datLineaEscribir.append(dato);
      }

      datLineaEscribir.append("&");

      dato = (String) datUnaLinea.get("DSDSCALNUM");
      if (dato != null) {
        datLineaEscribir.append(dato);
      }

      datLineaEscribir.append(" ");

      //A�adimos la l�nea a la tabla
      tablaPortal.addItem(datLineaEscribir.toString(), '&'); //'&' es el car�cter separador de datos (cada datos va a una columnna)
    }

    // miramos si hay que a�adir Mas ...
    if (lista.getState() == CLista.listaINCOMPLETA) {
      //  como la lista est� incompleta ponemos lo de m�s
      tablaPortal.addItem("M�s & ... &&", '&');
    }
  }

  /**
   *  esta funci�n fija los parametros para que se muestren los datos
   */
  public void setData(CLista a_dat) {

  }

  /**
   *  esta funci�n devuelve los valores calculados
   */
  public CLista getData() {
    return listaPortalSeleccionado;
  }
}

/**
 *  esta clase recoge los eventos de pinchar en las lupas
 *  cuando se pincha a una lupa se llama a un servlet para pedir los datos
 *  se deshabilitan el resto de los botones lupa
 *  y se manda ejecutar la acci�n correspondiente
 */
// action listener de evento en bot�nes
class BtnPortalActionListener
    implements ActionListener, Runnable {
  DialPortalSuca adaptee = null;
  ActionEvent e = null;

  public BtnPortalActionListener(DialPortalSuca adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    // deshabilita los escuchadores de cambios de c�digo
    String name = e.getActionCommand();
    String name2 = ( (Component) e.getSource()).getName();
    CLista lalista = null;
    String campo = null;

    adaptee.modoOperacion = adaptee.modoESPERA;
    adaptee.Inicializar();

    /// buscamos que bot�n es el que se ha pinchado y lo ejecutamos
    if (name.equals("btnAceptar")) {
      if (adaptee.tabla_actionPerformed(null)) {
        adaptee.dispose();
      }
    }
    else if (name.equals("btnCancelar")) {
      /// nos salimos y borramos todo
      // le grabamos los datos pedidos
      adaptee.listaPortalSeleccionado = null;
      adaptee.dispose();
    }
    adaptee.Inicializar();
    // desbloquea la recepci�n  de eventos
    adaptee.desbloquea();
  }
}

// escuchador de los click en la tabla
class PortalTableAdapter
    implements jclass.bwt.JCActionListener, Runnable {
  DialPortalSuca adaptee;
  JCActionEvent e;

  PortalTableAdapter(DialPortalSuca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    adaptee.modoOperacion = adaptee.modoESPERA;
    adaptee.Inicializar();

    ////#// System_out.println("Evento de la tabla " + e.toString());
    adaptee.tabla_actionPerformed(e);
    adaptee.dispose();
    adaptee.desbloquea();
  }
}
//________________________________________________ END CLASS
