
package suca;

public interface zonificacionSanitaria {

  public void setZonificacionSanitaria(String cdmun, String cdprov);

  public void cambiaModoTramero(boolean b);

  public int ponerEnEspera();

  public void ponerModo(int modo);

  public void vialInformado();
}
