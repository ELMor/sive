
package suca;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import capp.CLista;
import sapp.DBServlet;

//para el modo de municipios
//import vCasIn.*;

public class srvsuca
    extends DBServlet {

  /** tablas maestras del componente SUCA */
  public static final int modoLISTAMAESTRAS = 1;
  public static final int modoPROVINCIAS = 2;

  public static final int servletMUN_OBTENER_X_CODIGO = 3;
  public static final int servletMUN_OBTENER_X_DESCRIPCION = 4;
  public static final int servletMUN_SELECCION_X_CODIGO = 5;
  public static final int servletMUN_SELECCION_X_DESCRIPCION = 6;

  public static final int modoMUNICIPIO = 7;
  /** obtenci�n de zonificaci�n sanitaria de un municipio */
  public static final int modoZBS = 8;
  /** obtener el vial */
  public static final int modoOBTENER_VIAL = 9;
  public static final int prueba = 10;
  public static final int modoLISTA_VIALES = 11;
  public static final int modoOBTENER_CDPOSTAL = 12;
  public static final int modoLISTAMAESTRAS_SIN_TRAMERO = 13;
  public static final int modoPROVINCIAS_SIN_TRAMERO = 14;
  public static final int modoPORTAL_I = 15;
  public static final int modoPORTAL_II = 16;

  /** N� m�ximo de viales */
  public static final int maxVIALES = 50;

  // metodos operativos
  protected CLista doWork(int opmode, CLista param) throws Exception {

    /** conexi�n que se mantiene cada vez en el servidor */
    Connection con = null;

    /** statement de la sentencia SQL */
    PreparedStatement st = null;

    /** resultados de la query */
    ResultSet rs = null;

    /** sentencia */
    String query = "";

    Hashtable hash;

    /** Saber si es modo tramero */
    boolean bTramero = true;

    /** Conocer el idioma */
    boolean bIdiomaPorDefecto = true;

    /** Parametrizaci�n de las consultas de municipios */
    String strTABLA;
    String strCDPROV;
    String strCDMUNI;
    String strDSMUNI;

    //resultado
    CLista listaRes = null;

    con = openConnection();

    try {
// System_out.println("@@@@@@@SRVSUCA: opmode " + opmode);

      // modos de operaci�n
      switch (opmode) {

        case modoPORTAL_I:
        case modoPORTAL_II:

          // Entrada: Lista con un elemento que contiene un datasuca con CDMUNI,
          // CDVIAL, [CDTPNUM], [DSCALNUM] y [DSNMPORTAL].
          // Salida: CLista con datasucas conteniendo DSTNUM, DSNMPORTAL, DSDSCALNUM

          String strDSDSCALNUM = null;

          String strCDTPNUM = null;
          String strDSNMPORTAL = null;
          String strDSCALNUM = null;

          int iNumCampo = 0;

          boolean hayRegistros = false;

          listaRes = new CLista();

          datasuca datoBuscarPORTAL = (datasuca) param.firstElement();
          datasuca datoPORTAL = null;

          strCDTPNUM = (String) datoBuscarPORTAL.get("CDTPNUM");
          strDSNMPORTAL = (String) datoBuscarPORTAL.get("DSNMPORTAL");
          strDSCALNUM = (String) datoBuscarPORTAL.get("DSCALNUM");

          query = " select sp.DSNMPORTAL, stn.DSTNUM, sc.DSDSCALNUM "
              +
              " from SUCA_PORTAL sp, SUCA_TIPO_NUMERACION stn, SUCA_CALIFICADOR sc "
              + " WHERE sp.CDTPNUM=stn.CDTNUM and sp.DSCALNUM=sc.DSCALNUM "
              + " and CDVIAL = ? and CDMUNI = ? ";

          if (!strCDTPNUM.equals("")) {
            query = new String(query + " and CDTPNUM = ? ");
          }

          if (!strDSNMPORTAL.equals("")) {
            query = new String(query + " and DSNMPORTAL = ? ");
          }

          // JRM: A�ade el par�metro DSCALNUM de otra forma a como se viene
          // haciendo normalmente por EDO.
          if (opmode == modoPORTAL_I) {
            query = new String(query + " and sp.DSCALNUM like '" + strDSCALNUM +
                               "%'");
          }
          else {
            if (!strDSCALNUM.equals("")) {
              query = new String(query + " and sp.DSCALNUM = '" + strDSCALNUM);
            }
          }

          st = con.prepareStatement(query);

          st.setString(1, (String) datoBuscarPORTAL.get("CDVIAL"));
          st.setString(2, (String) datoBuscarPORTAL.get("CDMUNI"));

          iNumCampo = 3;

          if (!strCDTPNUM.equals("")) {
            st.setString(iNumCampo, strCDTPNUM);
            iNumCampo++;
          }

          if (!strDSNMPORTAL.equals("")) {
            st.setString(iNumCampo, strDSNMPORTAL);
            iNumCampo++;
          }

          /*
                     JRM no introduce los par�metros de esa forma
                     if (opmode == modoPORTAL_I) {
            st.setString(iNumCampo, strDSCALNUM + "%");
                     } else {
            if (!strDSCALNUM.equals("")) {
              st.setString(iNumCampo, strDSCALNUM);
            }
                     }
           */

          rs = st.executeQuery();

          //// System_out.println("suca: " + query);

          while (rs.next()) {
            hayRegistros = true;
            datoPORTAL = new datasuca();

            strDSDSCALNUM = rs.getString(3);

            datoPORTAL.put("DSNMPORTAL", rs.getString(1));
            datoPORTAL.put("DSTNUM", rs.getString(2));

            if (strDSDSCALNUM != null) {
              datoPORTAL.put("DSDSCALNUM", strDSDSCALNUM);
            }
            else {
              datoPORTAL.put("DSDSCALNUM", "");
            }
//          }  Metemos la b�squeda del portal, dentro del 'while'
// para que busque cada portal con su c�digo postal.
// Correcci�n ARS 8-6-01

            //// System_out.println ("antes obtener_cdpostal_portal ");
            // ahora busco el codigo postal
            if (hayRegistros) {
              CLista lisCDPostal = obtener_cdpostal_portal
                  ( (String) datoBuscarPORTAL.get("CDPROV"),
                   (String) datoBuscarPORTAL.get("CDMUNI"),
                   (String) datoBuscarPORTAL.get("CDVIAL"),
                   (String) datoPORTAL.get("DSNMPORTAL"),
                   (String) datoPORTAL.get("DSDSCALNUM"),
                   (String) datoBuscarPORTAL.get("CDTPNUM"),
                   con);
              datasuca aux = (datasuca) lisCDPostal.firstElement();

              String auxS = (String) aux.get("CDPOSTAL");
              // System_out.println (" despues obtener_cdpostal_portal " + auxS);
              if (auxS != null) {
                datoPORTAL.put("CDPOSTAL", auxS);
              }
              else {
                datoPORTAL.put("CDPOSTAL", "");
              }

              if (datoPORTAL != null) {
                listaRes.addElement(datoPORTAL);
              }
            }
          } // Esta ser�a la llave del bucle 'while' de atr�s.

          // Fin de correcci�n ARS 8-6-01
          rs.close();
          rs = null;
          st.close();
          st = null;
          break;
        case modoLISTAMAESTRAS_SIN_TRAMERO:

          // Entrada: Lista con un elemento que indica si el idioma es el predeterminado.
          // Salida: CLista de Clistas (una por cada tabla maestra),
          //         pero s�lo contendr�n datos las de Pa�ses y Comunidades.

          listaRes = new CLista();
          bIdiomaPorDefecto = ( (String) param.elementAt(0)).equals("S");

          // Lista maestra del pais
          // //#// System_out.println("SRVSUCA: tabla maestra del PAIS / Sin tramero");
          CLista pais_ST = new CLista();
          datasuca dato_ST = null;
          String cd_ST = "";
          String ds_ST = "";

          query = " select CD_PAIS, DS_PAIS from SIVE_PAISES ";
          st = con.prepareStatement(query);
          rs = st.executeQuery();

          while (rs.next()) {
            dato_ST = new datasuca();

            cd_ST = rs.getString("CD_PAIS");
            ds_ST = rs.getString("DS_PAIS");

            dato_ST.put(panelsuca.CD_PAIS, cd_ST);
            dato_ST.put(panelsuca.DS_PAIS, ds_ST);

            pais_ST.addElement(dato_ST);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          listaRes.addElement(pais_ST);

          // CA
          CLista ca_ST = new CLista();
          String strDSL_CA = null;

          query = " select CD_CA, DS_CA, DSL_CA from SIVE_COM_AUT";
          st = con.prepareStatement(query);
          rs = st.executeQuery();

          while (rs.next()) {

            dato_ST = new datasuca();
            dato_ST.put(panelsuca.CD_CA, rs.getString("CD_CA"));

            if (bIdiomaPorDefecto) {
              dato_ST.put(panelsuca.DS_CA, rs.getString("DS_CA"));
            }
            else {
              strDSL_CA = rs.getString("DSL_CA");
              if (strDSL_CA != null) {
                dato_ST.put(panelsuca.DS_CA, strDSL_CA);
              }
              else {
                dato_ST.put(panelsuca.DS_CA, rs.getString("DS_CA"));
              }
            }

            ca_ST.addElement(dato_ST);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          listaRes.addElement(ca_ST);

          // Vial
          listaRes.addElement(new CLista());

          // Numeros
          listaRes.addElement(new CLista());

          // Calificadores
          listaRes.addElement(new CLista());

          break;
        case modoLISTAMAESTRAS:

          //Entrada: nada
          //Salida: CLista de Clistas (una por cada tabla maestra)
          //        y cada una de estas contiene datasucas (una por
          //        cada elemento de la tabla mauestra)

          listaRes = new CLista();

          //Lista maestra del pais

          ////#// System_out.println("SRVSUCA: tabla maestra del PAIS");
          CLista pais = new CLista();

          query = " select CDPAIS, DSPAIS from SUCA_PAIS ";

          st = con.prepareStatement(query);
          rs = st.executeQuery();

          datasuca dato = null;
          String cd = "";
          String ds = "";
          while (rs.next()) {
            dato = new datasuca();
            cd = rs.getString("CDPAIS");
            ds = rs.getString("DSPAIS");

            ////#// System_out.println("SRVSUCA: pais " + cd + " " + ds);
            dato.put(panelsuca.CD_PAIS, cd);
            dato.put(panelsuca.DS_PAIS, ds);

            pais.addElement(dato);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          listaRes.addElement(pais);

          //Lista maestra de la comunidad autonoma

          ////#// System_out.println("SRVSUCA: tabla maestra del AUTONOMIA");
          CLista ca = new CLista();

          query = " select CDCOMU, DSCOMU from SUCA_AUTONOMIAS";

          st = con.prepareStatement(query);

          rs = st.executeQuery();

          dato = null;
          while (rs.next()) {
            dato = new datasuca();
            dato.put(panelsuca.CD_CA, rs.getString("CDCOMU"));
            dato.put(panelsuca.DS_CA, rs.getString("DSCOMU"));

            ca.addElement(dato);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          listaRes.addElement(ca);

          //Lista maestra del tipo de vial
          CLista tpVial = new CLista();

          query = " select CDTVIA, DSTVIA, DSTVABR from SUCA_TIPO_VIAL";

          st = con.prepareStatement(query);

          rs = st.executeQuery();

          dato = null;
          String dstv = "";
          while (rs.next()) {
            dato = new datasuca();
            dato.put("CDTVIA", rs.getString("CDTVIA"));
            dato.put("DSTVIA", rs.getString("DSTVIA"));
            dstv = rs.getString("DSTVABR");
            if (dstv != null) {
              dato.put("DSTVABR", dstv);

            }
            tpVial.addElement(dato);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          listaRes.addElement(tpVial);

          //Lista maestra del tipo de numeracion

          ////#// System_out.println("SRVSUCA: tabla maestra del TIPO NUM");
          CLista tpNum = new CLista();

          query = " select CDTNUM, DSTNUM, DSABREV from SUCA_TIPO_NUMERACION";

          st = con.prepareStatement(query);

          rs = st.executeQuery();

          dato = null;
          while (rs.next()) {
            dato = new datasuca();
            dato.put("CDTNUM", rs.getString("CDTNUM"));
            dato.put("DSTNUM", rs.getString("DSTNUM"));
            dstv = rs.getString("DSABREV");
            if (dstv != null) {
              dato.put("DSABREV", dstv);

            }
            tpNum.addElement(dato);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          listaRes.addElement(tpNum);

          //Lista maestra del calificador

          ////#// System_out.println("SRVSUCA: tabla maestra del CALIFICADOR");
          CLista calificador = new CLista();

          query =
              " select DSCALNUM, DSDSCALNUM, DSABREV from SUCA_CALIFICADOR ";

          ////#// System_out.println("SRVSUCA: despues de query");

          st = con.prepareStatement(query);

          ////#// System_out.println("SRVSUCA: despues de  con.prepareStatement(query)");

          rs = st.executeQuery();

          ////#// System_out.println("SRVSUCA: despues de rs = st.executeQuery rs " );

          String dsc = "", dsds = "", dsa = "";
          while (rs.next()) {
            dato = new datasuca();
            dsc = rs.getString("DSCALNUM");
            dsds = rs.getString("DSDSCALNUM");
            dsa = rs.getString("DSABREV");

            ////#// System_out.println("SRVSUCA: " + dsc + " " + dsds+ " " + dsa );

            dato.put("DSCALNUM", dsc);
            if (dsds != null) {
              dato.put("DSDSCALNUM", dsds);
            }
            if (dsds != null) {
              dato.put("DSABREV", dsa);

            }
            calificador.addElement(dato);
          }

          ////#// System_out.println("SRVSUCA: despues del while");

          rs.close();
          rs = null;
          st.close();
          st = null;

          ////#// System_out.println("SRVSUCA: despues de cerrar");

          listaRes.addElement(calificador);

          ////#// System_out.println("SRVSUCA: despues de a�adir a listares");

          break;

//*****************************************************************
        case modoPROVINCIAS_SIN_TRAMERO:

          //Entrada: 0 - datasuca con el c�digo de la comu. auto.
          //         1 - Cadena que informa de si se usa
          //         el idioma por defecto.
          //Salida: Clista con datasucas (una por provincia)

          datasuca parCA_ST = (datasuca) param.elementAt(0);
          bIdiomaPorDefecto = ( (String) param.elementAt(1)).equals("S");

          datasuca datoCA_ST = null;
          String strDSL_PROV = null;

          listaRes = new CLista();

          query = " select CD_PROV, DS_PROV, DSL_PROV "
              + " from SIVE_PROVINCIA "
              + " where CD_CA = ? ";

          st = con.prepareStatement(query);
          st.setString(1, (String) parCA_ST.get(panelsuca.CD_CA));
          rs = st.executeQuery();

          while (rs.next()) {
            datoCA_ST = new datasuca();
            datoCA_ST.put(panelsuca.CD_PROV, rs.getString("CD_PROV"));

            if (bIdiomaPorDefecto) {
              datoCA_ST.put(panelsuca.DS_PROV, rs.getString("DS_PROV"));
            }
            else {
              strDSL_PROV = rs.getString("DSL_PROV");
              if (strDSL_PROV != null) {
                datoCA_ST.put(panelsuca.DS_PROV, strDSL_PROV);
              }
              else {
                datoCA_ST.put(panelsuca.DS_PROV, rs.getString("DS_PROV"));
              }
            }

            listaRes.addElement(datoCA_ST);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          break;
//*****************************************************************
        case modoPROVINCIAS:

          //Entrada: datasuca con el c�digo de la comu. auto.
          //Salida: Clista con datasucas (una por provincia)

          datasuca parCA = (datasuca) param.firstElement();
          listaRes = new CLista();

          query = " select CDPROV, DSPROV "
              + " from SUCA_PROVINCIA "
              + " where CDCOMU = ? ";

          st = con.prepareStatement(query);
          st.setString(1, (String) parCA.get(panelsuca.CD_CA));

          rs = st.executeQuery();

          dato = null;
          while (rs.next()) {
            dato = new datasuca();
            dato.put(panelsuca.CD_PROV, rs.getString("CDPROV"));
            dato.put(panelsuca.DS_PROV, rs.getString("DSPROV"));
            listaRes.addElement(dato);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          break;

        case servletMUN_OBTENER_X_CODIGO:
        case servletMUN_OBTENER_X_DESCRIPCION:

          // Entrada:  datasuca con: la provincia y
          //           el modo ("S" tramero / "N" no tramero).
          // Salida: CLista de datasuca de municipios

          // de aqui sacare la provincia y el c�digo o la
          // descripcion del municipio
          datasuca parProv = (datasuca) param.firstElement();

          // Para conocer el modo
          bTramero = ( (String) parProv.get("TRAMERO")).equals("S");
          ////#// System_out.println("SRVSUCA: Tramero ");

          strTABLA = bTramero ? "suca_municipio" : "sive_municipio";
          strCDPROV = bTramero ? "CDPROV" : "CD_PROV";
          strCDMUNI = bTramero ? "CDMUNI" : "CD_MUN";
          strDSMUNI = bTramero ? "DSMUNI" : "DS_MUN";

          int i = 0;

          if (opmode == servletMUN_OBTENER_X_CODIGO) {
            query = "select " + strCDMUNI + ", " + strDSMUNI + " from " +
                strTABLA
                + " where " + strCDPROV + " = ? and "
                + strCDMUNI + " = ? order by " + strCDMUNI;
          }
          else {
            query = "select " + strCDMUNI + ", " + strDSMUNI + " from " +
                strTABLA
                + " where " + strCDPROV + " = ? and "
                + strDSMUNI + " = ? order by " + strCDMUNI;

            // prepara la lista de resultados
          }
          listaRes = new CLista();

          ////#// System_out.println("SRVSUCA: " + query);

          // lanza la query
          st = con.prepareStatement(query);
          // c�digo provincia
          st.setString(1, (String) parProv.get(panelsuca.CD_PROV)); //datos.getProvincia().trim() );
          // c�digo o descripcion municipio
          if (opmode == servletMUN_OBTENER_X_CODIGO) {
            st.setString(2, (String) parProv.get(panelsuca.CD_MUN)); //datos.getMunicipio().trim());
          }
          else {
            st.setString(2, (String) parProv.get(panelsuca.DS_MUN)); //datos.getMunicipio().trim());

          }
          rs = st.executeQuery();

          dato = null;
          // extrae la p�gina requerida

          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              listaRes.setState(CLista.listaINCOMPLETA);
              dato = new datasuca();
              dato = (datasuca) listaRes.lastElement();
              listaRes.setFilter( (String) dato.get(panelsuca.CD_MUN));
              break;
            }
            // control de estado
            if (listaRes.getState() == CLista.listaVACIA) {
              listaRes.setState(CLista.listaLLENA);
            }
            // a�ade un nodo
            dato = new datasuca();
            cd = rs.getString(strCDMUNI);
            ds = rs.getString(strDSMUNI);

            ////#// System_out.println("SRVSUCA: municipio cd " + cd + " ds " + ds);

            dato.put(panelsuca.CD_MUN, cd);
            dato.put(panelsuca.DS_MUN, ds);
            listaRes.addElement(dato);
            i++;
          }
          rs.close();
          st.close();

          break;

        case servletMUN_SELECCION_X_CODIGO:
        case servletMUN_SELECCION_X_DESCRIPCION:

          //Entrada: datasuca con la provincia) y
          //         el modo ("S" tramero / "N" no tramero).
          //Salida: CLista de datasuca de municipios

          //de aqui sacare la provincia y el c�digo o la
          //descripcion del municipio

          parProv = (datasuca) param.firstElement();

          // Para conocer el modo
          bTramero = ( (String) parProv.get("TRAMERO")).equals("S");
          ////#// System_out.println("SRVSUCA: Tramero ");

          strTABLA = bTramero ? "suca_municipio " : "sive_municipio ";
          strCDPROV = bTramero ? "CDPROV" : "CD_PROV";
          strCDMUNI = bTramero ? "CDMUNI" : "CD_MUN";
          strDSMUNI = bTramero ? "DSMUNI" : "DS_MUN";

              //DSR: filtro por C.A
              /*
                         String strCDCA = bTramero ? "CDCOMU" : "CD_CA";
                         String Join = bTramero ?
                  "suca_municipio.CDPROV = suca_provincia.CDPROV"
                  :
                  "sive_municipio.CD_PROV = sive_provincia.CD_PROV";
               */

          i = 0;

          // prepara la query
          // ARG: upper (13/5/02)
          if (param.getFilter().length() > 0) {
            if (opmode == servletMUN_SELECCION_X_CODIGO) {
              query = "select " + strCDMUNI + ", " + strDSMUNI + " from " +
                  strTABLA +
                  " where " + strCDPROV + " = ? and "
                  //Join + " and " + strCDCA + " = " + parProv.get(panelsuca.CD_CA) + " and " +
                  + strCDMUNI + " like ? and " + strCDMUNI + " > ? order by " +
                  strCDMUNI;
            }
            else {
              query = "select " + strCDMUNI + ", " + strDSMUNI + " from " +
                  strTABLA +
                  " where " + strCDPROV + " = ? and " +
                  //Join + " and " + strCDCA + " = " + parProv.get(panelsuca.CD_CA) + " and " +
                  "upper(" + strDSMUNI + ") like upper(?) and upper(" +
                  strCDMUNI + ") > upper(?) order by " + strCDMUNI;
            }
          }
          else {
            if (opmode == servletMUN_SELECCION_X_CODIGO) {
              query = "select " + strCDMUNI + ", " + strDSMUNI + " from " +
                  strTABLA +
                  " where " + strCDPROV + " = ? and " +
                  //Join + " and " + strCDCA + " = " + parProv.get(panelsuca.CD_CA) + " and " +
                  strCDMUNI + " like ? order by " + strCDMUNI;

            }
            else {
              query = "select " + strCDMUNI + ", " + strDSMUNI + " from " +
                  strTABLA +
                  " where " + strCDPROV + " = ? and " +
                  //Join + " and " + strCDCA + " = " + parProv.get(panelsuca.CD_CA) + " and " +
                  "upper(" + strDSMUNI + ") like upper(?) order by " +
                  strCDMUNI;

            }
          }

          // prepara la lista de resultados
          listaRes = new CLista();

          // lanza la query
          st = con.prepareStatement(query);

          // c�digo provincia
          st.setString(1, (String) parProv.get(panelsuca.CD_PROV)); //datos.getProvincia().trim() );
          // c�digo municipio
          if (opmode == servletMUN_SELECCION_X_CODIGO) {
            st.setString(2, (String) parProv.get(panelsuca.CD_MUN) + "%"); //datos.getMunicipio().trim() + "%");
          }
          else {
            st.setString(2, "%" + (String) parProv.get(panelsuca.DS_MUN) + "%");
            // paginaci�n
          }
          if (param.getFilter().length() > 0) {
            st.setString(3, param.getFilter().trim());
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              listaRes.setState(CLista.listaINCOMPLETA);
              dato = new datasuca();
              dato = (datasuca) listaRes.lastElement();
              listaRes.setFilter( (String) dato.get(panelsuca.CD_MUN));
              break;
            }
            // control de estado
            if (listaRes.getState() == CLista.listaVACIA) {
              listaRes.setState(CLista.listaLLENA);
            }
            // a�ade un nodo
            dato = new datasuca();
            dato.put(panelsuca.CD_MUN, rs.getString(strCDMUNI));
            dato.put(panelsuca.DS_MUN, rs.getString(strDSMUNI));
            listaRes.addElement(dato);
            i++;
          }

          rs.close();
          st.close();

          break;

        case modoZBS:

          //Entrada: codigo del municipio en un datasuca
          //Salida: tres datasucas (nivel1, nivel2 y zbs
          //        obtenidos de sive_municipio)

          datasuca parMun = (datasuca) param.firstElement();

          ////#// System_out.println("SRVSUCA: modo ZBS " + (String) parMun.get("CDMUNI"));

          listaRes = new CLista();

          //primero obtengo los c�digos
          query = " select CD_NIVEL_1, CD_NIVEL_2, CD_ZBS "
              + " from SIVE_MUNICIPIO "
              + " where CD_MUN = ? ";

          st = con.prepareStatement(query);
          st.setString(1, (String) parMun.get(panelsuca.CD_MUN));
          rs = st.executeQuery();

          String nivel1 = "";
          String nivel2 = "";
          String zbs = "";
          while (rs.next()) {
            nivel1 = rs.getString("CD_NIVEL_1");
            nivel2 = rs.getString("CD_NIVEL_2");
            zbs = rs.getString("CD_ZBS");
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          ////#// System_out.println("SRVSUCA: n1, n2, zbs " + nivel1 + " " + nivel2 + " " + zbs);
          //ahora obtengo las descripciones (ds y dsl) para nivel1
          query = " select DS_NIVEL_1, DSL_NIVEL_1 "
              + " from SIVE_NIVEL1_S "
              + " where CD_NIVEL1 = ? ";

          st = con.prepareStatement(query);
          st.setString(1, nivel1);
          rs = st.executeQuery();

          while (rs.next()) {
            dato = new datasuca();
            dato.put("CD_NIVEL_1", nivel1);
            dato.put("DS_NIVEL_1", rs.getString("DS_NIVEL_1"));
            dato.put("DSL_NIVEL_1", rs.getString("DSL_NIVEL_1"));

            listaRes.addElement(dato);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //ahora obtengo las descripciones (ds y dsl) para nivel2
          query = " select DS_NIVEL_2, DSL_NIVEL_2 "
              + " from SIVE_NIVEL2_S "
              + " where CD_NIVEL2 = ? ";

          st = con.prepareStatement(query);
          st.setString(1, nivel2);
          rs = st.executeQuery();

          while (rs.next()) {
            dato = new datasuca();
            dato.put("CD_NIVEL_2", nivel2);
            dato.put("DS_NIVEL_2", rs.getString("DS_NIVEL_2"));
            dato.put("DSL_NIVEL_2", rs.getString("DSL_NIVEL_2"));
            listaRes.addElement(dato);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //ahora obtengo las descripciones (ds y dsl) para zbs
          query = " select DS_ZBS, DSL_ZBS "
              + " from SIVE_ZONA_BASICA "
              + " where CD_ZBS = ? ";

          st = con.prepareStatement(query);
          st.setString(1, zbs);
          rs = st.executeQuery();

          while (rs.next()) {
            dato = new datasuca();
            dato.put("CD_ZBS", zbs);
            dato.put("DS_ZBS", rs.getString("DS_ZBS"));
            dato.put("DSL_ZBS", rs.getString("DSL_ZBS"));
            listaRes.addElement(dato);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          break;

        case modoOBTENER_VIAL:

          //Entrada: datasuca con el CDMUNI, el DSVIANORM y
          //         tal vez CDTVIA
          //Salida: Clista con datasucas una para cada vial

          datasuca parVial = (datasuca) param.firstElement();
          ////#// System_out.println("SRVSUCA: modo OBTENER VIAL " );

          ////#// System_out.println("SRVSUCA: DSVIANORM " + (String)parVial.get("DSVIANORM"));
          ////#// System_out.println("SRVSUCA: CDMUNI " + (String)parVial.get("CDMUNI"));

          String cdtvia = "";
          cdtvia = (String) parVial.get("CDTVIA");

          ////#// System_out.println("SRVSUCA: CDTVIA " + (String)parVial.get("CDTVIA"));

          String vialFilt = nombre_vial_filtrado( (String) parVial.get(
              "DSVIANORM"), con);

          listaRes = new CLista();

          ////#// System_out.println("SRVSUCA: nombre vial filtrado " + vialFilt );

          //primero busco en SUCA_VIAL
          query = " select CDVIAL, CDTVIA, DSVIAOFIC "
              + " from SUCA_VIAL "
              + " where CDESTADO <> ? and "
              + " CDMUNI = ? and DSVIANORM = ? ";
          if (!cdtvia.equals("")) {
            query = query + " and CDTVIA = ? ";

            ////#// System_out.println("SRVSUCA: query " + query);

          }
          st = con.prepareStatement(query);
          st.setString(1, "6");
          st.setString(2, (String) parVial.get(panelsuca.CD_MUN));
          st.setString(3, vialFilt);
          if (!cdtvia.equals("")) {
            st.setString(4, (String) parVial.get("CDTVIA"));
          }
          rs = st.executeQuery();

          datasuca res = null;
          String buscar = "";
          if (rs.next()) {
            res = new datasuca();
            res.put("CDVIAL", rs.getString("CDVIAL"));
            res.put("CDTVIA", rs.getString("CDTVIA"));
            res.put("DSVIAOFIC", rs.getString("DSVIAOFIC"));
            if (rs.next()) {
              //hay mas de un vial y debe buscar en suca_v_vial
              buscar = "SUCA_V_VIAL";
              ////#// System_out.println("SRVSUCA:hay mas de un vial y debe buscar en suca_v_vial");
            }
            else {
              //devuelve el dato que ha encontrado
              ////#// System_out.println("SRVSUCA: devuelve el dato que ha encontrado");
              listaRes.addElement(res);
              break;
            }
          }
          else {
            //hay que buscar en suca_sinonimo_via
            buscar = "SUCA_SINONIMO_VIA";
            ////#// System_out.println("SRVSUCA: hay que buscar en suca_sinonimo_via");
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          if (buscar.equals("SUCA_V_VIAL")) {
            query = " select CDVIAL, CDTVIA, DSVIAOFIC, ITSINONIMO "
                + " from SUCA_V_VIAL "
                + " where CDMUNI = ? and DSVIANORM = ? ";
            if (!cdtvia.equals("")) {
              query = query + " and CDTVIA = ? ";

              ////#// System_out.println("SRVSUCA: SUCA_V_VIAL " + query);

            }
            st = con.prepareStatement(query);
            st.setString(1, (String) parVial.get(panelsuca.CD_MUN));
            st.setString(2, vialFilt);
            if (!cdtvia.equals("")) {
              st.setString(3, (String) parVial.get("CDTVIA"));
            }
            rs = st.executeQuery();

            res = null;
            buscar = "";
            int encontrados = 0;
            while (rs.next()) {
              if (encontrados == maxVIALES) {
                break;
              }
              // que hago con el IT_SINONIMO????????
              res = new datasuca();
              res.put("CDVIAL", rs.getString("CDVIAL"));
              res.put("CDTVIA", rs.getString("CDTVIA"));
              res.put("DSVIAOFIC", rs.getString("DSVIAOFIC"));
              listaRes.addElement(res);
              encontrados++;
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

          }
          else if (buscar.equals("SUCA_SINONIMO_VIA")) {
            query = " select CDVIAL, DSVIAOFIC " // CDTVIA,
                + " from SUCA_SINONIMO_VIA "
                + " where CDMUNI = ? and DSVIANORM = ? ";

            query = query + " order by DSVIANORM ";

            ////#// System_out.println("SRVSUCA: SUCA_SINONIMO_VIA " + query);

            st = con.prepareStatement(query);
            st.setString(1, (String) parVial.get(panelsuca.CD_MUN));
            st.setString(2, vialFilt);
            rs = st.executeQuery();

            res = null;
            buscar = "";
            String cdvial = "";
            if (rs.next()) {
              res = new datasuca();
              cdvial = rs.getString("CDVIAL");
              res.put("CDVIAL", cdvial);
              res.put("CDTVIA", "");
              res.put("DSVIAOFIC", rs.getString("DSVIAOFIC"));

              if (rs.next()) {
                //hay mas de un vial y debe devolver hasta maxVIALES
                ////#// System_out.println("SRVSUCA: en sinonimo hay mas de un vial y debe devolver hasta maxVIALES");
                int encontrados = 1;
                listaRes.addElement(res);
                res = new datasuca();
                res.put("CDVIAL", rs.getString("CDVIAL"));
                res.put("CDTVIA", "");
                res.put("DSVIAOFIC", rs.getString("DSVIAOFIC"));
                listaRes.addElement(res);
                encontrados++;

                while (rs.next()) {
                  if (encontrados == maxVIALES) {
                    break;
                  }
                  // que hago con el IT_SINONIMO????????
                  res = new datasuca();
                  res.put("CDVIAL", rs.getString("CDVIAL"));
                  res.put("CDTVIA", "");
                  res.put("DSVIAOFIC", rs.getString("DSVIAOFIC"));
                  listaRes.addElement(res);
                  encontrados++;
                }
                break;
              }
              else {
                //solo hay un sinonimo y se busca en SUCA_VIA por
                //esa clave (cdvial)
                buscar = "SUCA_VIAL";
                ////#// System_out.println("SRVSUCA:solo hay un sinonimo y se busca en SUCA_VIA por esa clave (cdvial)");
              }
            }
            else {
              //hay que buscar en suca_v_vial
              buscar = "SUCA_V_VIAL";
              ////#// System_out.println("SRVSUCA:  hay que buscar en suca_v_vial");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

            if (buscar.equals("SUCA_VIAL")) {
              query = " select CDVIAL, CDTVIA, DSVIAOFIC "
                  + " from SUCA_VIAL "
                  + " where CDVIAL = ? and "
                  + " CDMUNI = ? ";
              //no tiene en cuenta el estado?????????
              if (!cdtvia.equals("")) {
                query = query + " and CDTVIA = ? ";

                ////#// System_out.println("SRVSUCA: SUCA_VIAL " + query);

              }
              st = con.prepareStatement(query);
              st.setString(1, cdvial);
              st.setString(2, (String) parVial.get(panelsuca.CD_MUN));

              ////#// System_out.println("SRVSUCA: pars " + cdvial +
              //                   " " + (String) parVial.get("CDMUNI") +
              //                   " *" + vialFilt);

              if (!cdtvia.equals("")) {
                st.setString(3, (String) parVial.get("CDTVIA"));
              }
              rs = st.executeQuery();

              res = null;
              buscar = "";
              if (rs.next()) {
                res = new datasuca();
                res.put("CDVIAL", rs.getString("CDVIAL"));
                res.put("CDTVIA", rs.getString("CDTVIA"));
                res.put("DSVIAOFIC", rs.getString("DSVIAOFIC"));
                listaRes.addElement(res);
                ////#// System_out.println("SRVSUCA: he encontrado el sinonimo en suca_vial");
                break;
              }
              else {
                ////#// System_out.println("SRVSUCA: NO he encontrado el sinonimo en suca_vial");
              }
              rs.close();
              rs = null;
              st.close();
              st = null;

            }
            else if (buscar.equals("SUCA_V_VIAL")) {
              //Si encuentra mas de uno los envia hasta maxVIALES
              //si no encuentra ninguno envia lista vacia

              query = " select CDVIAL, CDTVIA, DSVIAOFIC, ITSINONIMO "
                  + " from SUCA_V_VIAL "
                  + " where CDMUNI = ? and DSVIANORM like ? ";
              if (!cdtvia.equals("")) {
                query = query + " and CDTVIA = ? ";
              }
              query = query + " order by DSVIANORM ";

              ////#// System_out.println("SRVSUCA: SUCA_V_VIAL " + query);

              st = con.prepareStatement(query);
              st.setString(1, (String) parVial.get(panelsuca.CD_MUN));
              st.setString(2, "%" + vialFilt + "%");
              if (!cdtvia.equals("")) {
                st.setString(3, (String) parVial.get("CDTVIA"));
              }
              rs = st.executeQuery();

              int encontrados = 0;
              String tvia = "", viaofic = "";
              while (rs.next()) {
                if (encontrados == maxVIALES) {
                  break;
                }

                // que hago con el IT_SINONIMO????????
                res = new datasuca();
                res.put("CDVIAL", rs.getString("CDVIAL"));
                tvia = rs.getString("CDTVIA");
                if (tvia != null) {
                  res.put("CDTVIA", tvia);
                }
                else {
                  res.put("CDTVIA", "");
                }
                viaofic = rs.getString("DSVIAOFIC");
                if (viaofic != null) {
                  res.put("DSVIAOFIC", viaofic);
                }
                else {
                  res.put("DSVIAOFIC", "");

                }
                listaRes.addElement(res);
                encontrados++;
              }
              ////#// System_out.println("SRVSUCA: encontrados " + encontrados);

              rs.close();
              rs = null;
              st.close();
              st = null;
            }
          }

          break;

        case modoLISTA_VIALES:

          //Entrada: datasuca con  CDMUNI obligatorio
          //         y DSVIANORM y CDTVIA opcionales (si no tienen
          //         valor se rellenan con la cadena vacia en local)
          //Salida: Clista con datasucas una para cada vial

          listaRes = new CLista();
          parVial = (datasuca) param.firstElement();
          cdtvia = "";
          cdtvia = (String) parVial.get("CDTVIA");
          if ( ( (String) parVial.get("DSVIANORM")).equals("")) {
            vialFilt = "";
          }
          else {
            vialFilt = nombre_vial_filtrado( (String) parVial.get("DSVIANORM"),
                                            con);

          }
          query = " select CDVIAL, CDTVIA, DSVIAOFIC, ITSINONIMO "
              + " from SUCA_V_VIAL "
              + " where CDMUNI = ? and DSVIANORM like ? ";
          if (!cdtvia.equals("")) {
            query = query + " and CDTVIA = ? ";
          }
          query = query + " order by DSVIANORM ";

          ////#// System_out.println("SRVSUCA: SUCA_V_VIAL en LISTA_VIALES " + query);

          st = con.prepareStatement(query);
          st.setString(1, (String) parVial.get(panelsuca.CD_MUN));
          st.setString(2, "%" + vialFilt + "%");
          if (!cdtvia.equals("")) {
            st.setString(3, (String) parVial.get("CDTVIA"));
          }
          rs = st.executeQuery();

          int encontrados = 0;
          String tvia = "", viaofic = "";
          while (rs.next()) {
            if (encontrados == maxVIALES) {
              break;
            }

            // que hago con el IT_SINONIMO????????
            res = new datasuca();
            res.put("CDVIAL", rs.getString("CDVIAL"));
            tvia = rs.getString("CDTVIA");
            if (tvia != null) {
              res.put("CDTVIA", tvia);
            }
            else {
              res.put("CDTVIA", "");
            }
            viaofic = rs.getString("DSVIAOFIC");
            if (viaofic != null) {
              res.put("DSVIAOFIC", viaofic);
            }
            else {
              res.put("DSVIAOFIC", "");

            }
            listaRes.addElement(res);
            encontrados++;
          }

          ////#// System_out.println("SRVSUCA: encontrados " + encontrados);

          rs.close();
          rs = null;
          st.close();
          st = null;

          break; // fin modoLISTA_VIALES

        case modoOBTENER_CDPOSTAL:

          //Entrada: tres casos  (los datos en datasuca)
          //  1: CDPROV y CDMUNI
          //  2: CDPROV, CDMUNI y CDVIAL
          //  3: CDPROV, CDMUNI, CDVIAL, DSNMPORTAL, DSCALNUM, CDTPNUM
          // (los campos que no se envian se ponen a "")

          //Salida:
          //  CLista de datasucas con los CDPOSTAL
          //  si no se ha encontrado nada va vacia pero inicializada

          listaRes = new CLista();
          datasuca parPostal = (datasuca) param.firstElement();
          String cdprov = (String) parPostal.get(panelsuca.CD_PROV);
          String cdmuni = (String) parPostal.get(panelsuca.CD_MUN);
          String cdvial = (String) parPostal.get("CDVIAL");
          String dsnmportal = (String) parPostal.get("DSNMPORTAL");
          String dscalnum = (String) parPostal.get("DSCALNUM");
          String cdtpnum = (String) parPostal.get("CDTPNUM");

          dato = new datasuca();
          CLista lista = new CLista();
          cd = "";
          //veo en que caso estoy (1, 2 o 3)
          if (!dsnmportal.equals("")) {
            //caso 3
            listaRes = obtener_cdpostal_portal(cdprov, cdmuni, cdvial,
                                               dsnmportal,
                                               dscalnum, cdtpnum, con);
          }
          else if (!cdvial.equals("")) {
            //caso 2
            listaRes = obtener_cdpostal_vial(cdprov, cdmuni, cdvial, con);
          }
          else {
            //caso 1
            listaRes = obtener_cdpostal_municipio(cdprov, cdmuni, con);
          }

          break; //fin modoOBTENER_CDPOSTAL

      }

    }
    catch (Exception exc) {
      //E throw new ServletException(strError);
      exc.printStackTrace();
      throw exc;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (listaRes != null) {
      listaRes.trimToSize();
    }

    return listaRes;

  }

  private CLista obtener_cdpostal_municipio(String prov, String muni,
                                            Connection con) throws Exception {

    CLista resul = new CLista();

// //#// System_out.println("SRVSUCA: caso 1 obtener_cdpostal_municipio ");

    String cd_postal = "";

    PreparedStatement st = null;
    ResultSet rs = null;
    String query = "";

    //obtengo el cd_postal
    query = " select CDPOSTAL "
        + " from SUCA_CDPOST "
        + " where CDPROV = ? and CDMUNI = ? ";
    st = con.prepareStatement(query);

    st.setString(1, prov);
    st.setString(2, muni);

    rs = st.executeQuery();

    datasuca dato = new datasuca();
    while (rs.next()) {
      dato = new datasuca();
      dato.put("CDPOSTAL", rs.getString("CDPOSTAL"));
      resul.addElement(dato);
    }

    rs.close();
    rs = null;
    st.close();
    st = null;

    return resul;
  }

  private CLista obtener_cdpostal_vial(String prov,
                                       String muni, String vial,
                                       Connection con) throws Exception {

    CLista resul = new CLista();

////#// System_out.println("SRVSUCA: caso 2 obtener_cdpostal_vial ");
    String cd_postal = "";

    PreparedStatement st = null;
    ResultSet rs = null;
    String query = "";

    //obtengo el cd_postal
    query = " select distinct CDPOSTAL "
        + " from SUCA_VIAL "
        + " where CDMUNI = ? and CDVIAL = ? ";
    st = con.prepareStatement(query);

//    //#// System_out.println("SRVSUCA: muni " + muni + " vial " + vial);

    st.setString(1, muni);
    st.setString(2, vial);

//    //#// System_out.println("SRVSUCA: despues de setstring ");

    rs = st.executeQuery();

//    //#// System_out.println("SRVSUCA: despues de executeQuery ");

    datasuca dato = new datasuca();
    while (rs.next()) {
      dato = new datasuca();
      cd_postal = rs.getString("CDPOSTAL");
      if (cd_postal != null) {
        dato.put("CDPOSTAL", cd_postal);
        resul.addElement(dato);
      }
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    if (resul.size() == 0) {
//      //#// System_out.println("SRVSUCA: no he encontrado cdpostal_vial");
      resul = obtener_cdpostal_municipio(prov, muni, con);
      return resul;
    }
    else {
      return resul;
    }
  }

  private CLista obtener_cdpostal_portal(String prov, String muni,
                                         String vial, String dsnm,
                                         String dsca, String cdtp,
                                         Connection con) throws Exception {
    // devuelve una CLista vector de datasucas que contienen los cdzona

////#// System_out.println("SRVSUCA: caso 3 obtener_cdpostal_portal ");

    CLista resul = new CLista();

    String cd_postal = "";

    PreparedStatement st = null;
    ResultSet rs = null;
    String query = "";

    //obtengo el cd_postal
    // ARG: (5/6/02) De momento no se filtra por DSCALNUM
    //query = " select min(CDZONA) as CDZONA"
    query = " select distinct(CDZONA)"
        + " from SUCA_PORTAL_ZONA "
        + " where CDMUNI = ? and "
        + "       CDVIAL = ? and "
        + "       DSNMPORTAL = ? and "
//          + "       DSCALNUM = ? and "
        + "       CDTPNUM = ? and "
        + "       CDTPZONA = ? ";
    st = con.prepareStatement(query);

    st.setString(1, muni);
    st.setString(2, vial);
    st.setString(3, dsnm);
//    st.setString(4, dsca);
//    st.setString(5, cdtp);
//    st.setString(6, "1");

    st.setString(4, cdtp);
    st.setString(5, "1");

    rs = st.executeQuery();

    datasuca dato = new datasuca();
    while (rs.next()) {
      dato = new datasuca();
      dato.put("CDPOSTAL", rs.getString("CDZONA"));
      resul.addElement(dato);
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    if (resul.size() == 0) {
//      //#// System_out.println("SRVSUCA: no encontrado cdpostal_portal");
      resul = obtener_cdpostal_vial(prov, muni, vial, con);
      //se devuelve con un dato o vacia
      return resul;
    }
    else {
//      //#// System_out.println("SRVSUCA: cdpostales_portal: " + resul.size());
      return resul;
    }
  }

  private String espacio_delante_de_mas(String cad) {
    boolean hayMas = (cad.indexOf('+') != -1);
    String aux = "";
    if (!hayMas) {
      aux = cad;
    }
    int i = 0;
    while (hayMas) {
      aux = aux + cad.substring(i, cad.indexOf('+', i)) + " +";
      i = cad.indexOf('+', i) + 1;
      hayMas = (cad.indexOf('+', i) != -1);
    }
    return aux;
  }

  private String replace_con_cadenas(String vial,
                                     String auxp, String auxn) {
    //Devuelve vial en la que se han sustituido las ocurrecias de
    //auxp por auxn

    String ini = auxp.substring(0, 1);
    String aux = vial;
    String sub = "";
    int indFrom = 0;
    int indIni = vial.indexOf(ini, indFrom);
    boolean hayVial = (indIni != -1);
    while (hayVial) {
      if (indIni + auxp.length() < vial.length()) {
        sub = vial.substring(indIni, indIni + auxp.length());

        if (auxp.equals(sub)) {
          //he encontrado la cadena
          vial = vial.substring(0, indIni) + auxn
              + vial.substring(indIni + auxp.length());
          ////#// System_out.println(vial);

        }
        else { //solo era el principio pero no coincidia toda
        }
        //sigo buscando
        indFrom = indIni + 1;
        if (indFrom < vial.length()) {
          indIni = vial.indexOf(ini, indFrom);
          hayVial = (indIni != -1); //para ver si hay mas ocurrencias
          //de la cadena
        }
        else {
          hayVial = false; //se ha llegado al final de vial
        }
      }
      else {
        hayVial = false;
      }
    }
    return vial;

  }

  private String quitar_caracter(String cad, char ch) {
    String aux = "";
    int ind = cad.indexOf(ch);
    while (ind != -1) {
      cad = cad.substring(0, ind) + cad.substring(ind + 1);
      ind++;
      if (ind < cad.length()) {
        ind = cad.indexOf(ch, ind);
      }
      else {
        ind = -1; // se ha llegado al final de la cadena
      }
    }
    return cad;
  }

  private String trim_dcha(String cad) {
    while (cad.endsWith(" ")) {
      cad = cad.substring(0, cad.length() - 1);
      ////#// System_out.println("*" + cad + "*");
    }
    return cad;
  }

  private String nombre_vial_filtrado(String vial, Connection con) throws
      Exception {

    PreparedStatement st = null;
    ResultSet rs = null;
    String query = "";

    Vector dspartic = new Vector();
    Vector dsnueva = new Vector();

    // a mayusculas
    String vialMay = vial.toUpperCase(new Locale("es", "ES"));

    ////#// System_out.println("SRVSUCA: vialMay " +vialMay);

    //obtengo las particulas
    query = " select DSPARTIC, DSNUEVA "
        + " from SUCA_PARTICULA order by NMPRIORIDAD";
    st = con.prepareStatement(query);
    rs = st.executeQuery();
    String partic = "";
    int ind = 0;

    while (rs.next()) {
      partic = rs.getString("DSPARTIC");
      if (partic.endsWith("+")) {
        partic = " " + partic;
        partic = espacio_delante_de_mas(partic);
      }
      dspartic.insertElementAt(partic, ind);
      dsnueva.insertElementAt(rs.getString("DSNUEVA"), ind);
      ind++;
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    ////#// System_out.println("SRVSUCA: particulas " + dspartic.size() + " " + dsnueva.size());

//inicializo el resultado
    String vialFilt = " " + trim_dcha(vialMay);

    //recorro las particulas
    String auxp = "";
    String auxn = "";
    for (int i = 0; i < dspartic.size(); i++) {
      auxp = (String) dspartic.elementAt(i);
      auxp = trim_dcha(auxp);
      auxn = (String) dsnueva.elementAt(i);
      auxn = trim_dcha(auxn);
      if (auxp.endsWith("+")) {
        vialFilt = replace_con_cadenas(vialFilt,
                                       auxp.substring(0, auxp.length() - 1),
                                       " " + auxn + " ");
      }
      else {
        vialFilt = replace_con_cadenas(vialFilt, auxp, auxn);
      }
    }

    ////#// System_out.println("SRVSUCA: despues de sustituir las cadenas " + vialFilt);

    //vialFilt = vialFilt.replace('+', '');
    vialFilt = quitar_caracter(vialFilt, '+');

    ////#// System_out.println("SRVSUCA: despues de quitar los + " + vialFilt);

    for (int j = 0; j < vialFilt.length(); j++) {
      vialFilt = replace_con_cadenas(vialFilt, "  ", " ");
    }

    ////#// System_out.println("SRVSUCA: despues de quitar los espacios " + vialFilt);

    return vialFilt;

  }

}
