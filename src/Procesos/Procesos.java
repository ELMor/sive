package Procesos;

import java.util.ResourceBundle;

import capp.CApp;

public class Procesos
    extends CApp {

  // Procesoss
  public static final int catPROCESOS = 90;
  //Obtenci�n Recursos para Strings
  ResourceBundle res;

  // modos de operaci�n del servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // constantes del panel
  final String strSERVLET = "servlet/SrvCat";

  // cat�logo
  int iProcesos;

  // parametros
  int iMaxCod;
  int iMaxDes;
  String sLabel;
  String sEtiqueta;
  String sEtCod;

  int maxLongDes; //Longitud m�x del campo descripci�n

  public void init() {
    super.init();
    res = ResourceBundle.getBundle("Procesos.Res" + this.getIdioma());
    iProcesos = catPROCESOS;
    sEtCod = res.getString("msg6.Text");
    sEtiqueta = res.getString("msg7.Text");
    sLabel = res.getString("msg8.Text");
    iMaxCod = 6;
    iMaxDes = 40;

  }

  public void start() {
    setTitulo(sEtiqueta);
    VerPanel("", new CListaProcesos(this));
  }
}
