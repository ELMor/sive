package Procesos;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import catalogo.DataCat;
import enfcie.DataEnfCie;
import sapp.StubSrvBD;

public class PanelPro
    extends CDialog {

  //Objeto para obtener Strings de fichero de recursos
  ResourceBundle res;

  // modos de operaci�n de la ventana
  final static int modoALTA = 0;
  final static int modoMODIFICAR = 1;
  final static int modoBAJA = 2;
  final static int modoESPERA = 5;

  // modos de operaci�n del servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;

  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // constantes del panel
  final String strSERVLETEnf = "servlet/SrvEnfCie";
  protected StubSrvBD stubEnf;

  final String strSERVLET = "servlet/SrvCat";
  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/Magnify.gif"};

  // par�metros
  protected int modoOperacion;
  public boolean bAceptar = false;
  protected int iProcesos;
  protected String sTitulo;
  protected StubSrvBD stubCliente;
  protected CCargadorImagen imgs = null;

  //longitudes m�ximas de los campos. Se obtienen del applet
  int iMaxCod;
  int iMaxDes;
  // controles
  XYLayout xyLyt = new XYLayout();
  Panel pnl = new Panel();
  Label lbl1 = new Label();
  CCampoCodigo txtCod = new CCampoCodigo();
  Label lbl2 = new Label();
  TextField txtDes = new TextField();
  Label lbl3 = new Label();
  TextField txtDesL = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  catActionListener btnActionListener = new catActionListener(this);
  FlowLayout flowLayout1 = new FlowLayout();

  ButtonControl btnCtrlBuscar = new ButtonControl();
  focusAdapter txtFocusAdapter = new focusAdapter(this);

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:

        // modo alta
        btnCtrlBuscar.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtCod.setEnabled(true);
        txtDes.setEnabled(true);
        txtDesL.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:

        // modo modificaci�n y baja

        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtCod.setEnabled(false);
        btnCtrlBuscar.setEnabled(false);
        txtDes.setEnabled(true);
        txtDesL.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:

        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtCod.setEnabled(false);
        btnCtrlBuscar.setEnabled(false);
        txtDes.setEnabled(false);
        txtDesL.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnCtrlBuscar.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        txtCod.setEnabled(false);
        txtDes.setEnabled(false);
        txtDesL.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  // contructor
  public PanelPro(CApp a, int modo, int cat, String s) {
    super(a);

    try {
      res = ResourceBundle.getBundle("Procesos.Res" + this.app.getIdioma());
      iMaxCod = ( (Procesos) a).iMaxCod;
      iMaxDes = ( (Procesos) a).iMaxDes;
      sTitulo = s;
      iProcesos = cat;
      modoOperacion = modo;
      stubEnf = new StubSrvBD(new URL(a.getURL() + strSERVLETEnf));
      stubCliente = new StubSrvBD(new URL(a.getURL() + strSERVLET));
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
      bAceptar = false;
    }
  }

  // aspecto de la ventana
  private void jbInit() throws Exception {
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    txtCod.addFocusListener(txtFocusAdapter);
    txtCod.setName("txtEnfer");
    btnCtrlBuscar.addActionListener(btnActionListener);
    btnCtrlBuscar.setLabel("");
    btnCtrlBuscar.setActionCommand("buscarEnfer");
    btnCtrlBuscar.setImage(imgs.getImage(2));

    this.setLayout(flowLayout1);
    xyLyt.setHeight(157);
    xyLyt.setWidth(384);
    this.setSize(384, 180);
    pnl.setLayout(xyLyt);
    lbl1.setText(res.getString("lbl1.Text"));
    txtCod.setBackground(new Color(255, 255, 150));
    lbl2.setText(res.getString("lbl2.Text"));
    txtDes.setBackground(new Color(255, 255, 150));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");
    pnl.add(lbl1, new XYConstraints(2, 1, 94, 25));
    pnl.add(txtCod, new XYConstraints(96, 1, 77, -1));
    pnl.add(btnCtrlBuscar, new XYConstraints(180, 1, -1, -1));
    pnl.add(lbl2, new XYConstraints(2, 38, -1, -1));
    pnl.add(txtDes, new XYConstraints(96, 40, 275, -1));
//    pnl.add(lbl3, new XYConstraints(22, 111, -1, -1));
    pnl.add(lbl3, new XYConstraints(2, 80, -1, -1));
    pnl.add(txtDesL, new XYConstraints(172, 80, 201, -1));
    pnl.add(btnAceptar, new XYConstraints(196, 122, 80, 26));
    pnl.add(btnCancelar, new XYConstraints(294, 122, 80, 26));
    this.add(pnl);

    setTitle(sTitulo);

    // establece los escuchadores
    btnAceptar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);

    // idioma local no es visible si no tengo
    if (app.getIdiomaLocal().length() == 0) {
      txtDesL.setVisible(false);
      lbl3.setVisible(false);
    }
    else {
      lbl3.setText(res.getString("lbl3.Text") + app.getIdiomaLocal() + "):");
    }

    // establece el modo de operaci�n
    Inicializar();
  }

  // busca la descripcion de la enfermedad
  void btnCtrlBuscar_actionPerformed(ActionEvent evt) {
    DataEnfCie datosPantalla = null;
    CMessage msgBox = null;

    int antModo = modoOperacion;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      CListaEnfEDO lista = new CListaEnfEDO(app,
                                            res.getString("msg1.Text"),
                                            stubEnf,
                                            strSERVLETEnf,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      datosPantalla = (DataEnfCie) lista.getComponente();

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      bAceptar = false;
    }

    if (datosPantalla != null) {
      txtCod.removeKeyListener(txtCod);
      txtCod.setText(datosPantalla.getCod());
      txtDes.setText(datosPantalla.getDes());
      txtCod.addKeyListener(txtCod);
    }

    modoOperacion = antModo;
    Inicializar();
  }

  // perdida de foco de cajas de c�digos
  String sBkCod = "";
  void focusLost(FocusEvent e) {

    // datos de envio
    DataEnfCie enf;

    CLista param = null;
    CMessage msg;
    //String strServlet = null;
    int modoServlet = 0;

    if (sBkCod.equals(txtCod.getText())) {
      return;
    }
    sBkCod = txtCod.getText();

    int antModo = modoOperacion;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtEnfer")) && (txtCod.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataEnfCie(txtCod.getText()));
      //strServlet = strSERVLETEnf;
      modoServlet = servletOBTENER_X_CODIGO;
    }
    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        param = (CLista) stubEnf.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtEnfer")) {
            enf = (DataEnfCie) param.firstElement();
            txtCod.removeKeyListener(txtCod);
            txtCod.setText(enf.getCod());
            txtDes.setText(enf.getDes());
            txtCod.addKeyListener(txtCod);

          }
        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg2.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
        bAceptar = false;
      }

      modoOperacion = antModo;
      Inicializar();
    }

  }

  // procesa opci�n a�adir al cat�logo
  public void Anadir() {
    CMessage msgBox = null;
    CLista data = null;

    // comprueba la validad de los datos
    if (this.isDataValid()) {
      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        // prepara los datos y los envia
        data = new CLista();
        data.addElement(new DataCat(txtCod.getText(), txtDes.getText(),
                                    txtDesL.getText()));

        //#// System_out.println ("iPtocesos " + iProcesos);
        this.stubCliente.doPost(servletALTA + iProcesos, data);
        //this.stubCliente.doPost(servletALTA, data);
        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoALTA;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        bAceptar = false;
      }
    }
  }

  // procesa opci�n modificar
  public void Modificar() {
    CMessage msgBox = null;
    CLista data = null;

    // comprueba la validad de los datos
    if (this.isDataValid()) {
      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        // prepara y envia los datos
        data = new CLista();
        data.addElement(new DataCat(txtCod.getText(), txtDes.getText(),
                                    txtDesL.getText()));
        this.stubCliente.doPost(servletMODIFICAR + iProcesos, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoMODIFICAR;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        bAceptar = false;
      }
    }
  }

  // procesa opci�n borrar
  public void Borrar() {
    CMessage msgBox = null;
    CLista data = null;

    msgBox = new CMessage(app, CMessage.msgPREGUNTA, res.getString("msg3.Text"));
    msgBox.show();

    if (msgBox.getResponse()) {

      msgBox = null;
      // borra el item del cat�logo
      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        data = new CLista();
        data.addElement(new DataCat(txtCod.getText()));
        this.stubCliente.doPost(servletBAJA + iProcesos, data);
        //this.stubCliente.doPost(servletBAJA, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoBAJA;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        bAceptar = false;
      }
    }
    else {
      msgBox = null;
    }
  }

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = "";

    boolean b = false;

    // comprueba que esten informados los campos obligatorios
    if ( (txtCod.getText().length() > 0) && (txtDes.getText().length() > 0)) {
      b = true;

      // comprueba la longitud m�xima de los campos
      if (txtCod.getText().length() > iMaxCod) {
        b = false;
        msg = res.getString("msg4.Text");
        txtCod.selectAll();
      }

      if (txtDes.getText().length() > iMaxDes) {
        b = false;
        msg = res.getString("msg4.Text");
        txtDes.selectAll();
      }

      if (txtDesL.getText().length() > iMaxDes) {
        b = false;
        msg = res.getString("msg4.Text");
        txtDesL.selectAll();
      }

      // advertencia de requerir datos
    }
    else {
      msg = res.getString("msg5.Text");
    }

    if (!b) {
      msgBox = new CMessage(app, CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }
    bAceptar = b;
    return b;
  }
}

// action listener
class catActionListener
    implements ActionListener, Runnable {
  PanelPro adaptee = null;
  ActionEvent e = null;

  public catActionListener(PanelPro adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    if (e.getActionCommand() == "aceptar") { // aceptar
      adaptee.bAceptar = true;

      // realiza la operaci�n
      switch (adaptee.modoOperacion) {
        case PanelPro.modoALTA:
          adaptee.Anadir();
          break;
        case PanelPro.modoMODIFICAR:
          adaptee.Modificar();
          break;
        case PanelPro.modoBAJA:
          adaptee.Borrar();
          break;
      }

    }
    else if (e.getActionCommand() == "cancelar") { // cancelar
      adaptee.bAceptar = false;
      adaptee.dispose();

    }
    else if (e.getActionCommand() == "buscarEnfer") { // cancelar
      adaptee.btnCtrlBuscar_actionPerformed(e);
    }

  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanelPro adaptee;
  FocusEvent event;

  focusAdapter(PanelPro adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

//clase para lista de enfermedades (para buscar)
class CListaEnfEDO
    extends CListaValores {

  public CListaEnfEDO(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    //Lanzar primera query
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEnfCie(s);

  }

  public String getCodigo(Object o) {
    return ( ( (DataEnfCie) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEnfCie) o).getDes());
  }
}
