/* Clase que extiende de CSolapa del paquete capp para sobreescribir el
 // metodo mousedown que controla el evento sobre la pesta�a de
 // un tabpanel y asi poder obtener para cada notificador su protocolo
 // autor: JLT
 // fecha: 13/06/2000
 */

package tuberculosis.cliente.diaconcasotub;

import java.awt.Event;

import capp.CSolapa;
// modificacion 12/07/2000
import comun.Common;
import infproto.PanelTuberCon;
import tuberculosis.cliente.pansupcon.PanAux;

public class CSolapaExt
    extends CSolapa {

  //Eventos sobre el canvas.
  public boolean mouseDown(Event evt, int x, int y) {

    if (name.size() == 0) {
      return false;
    }

    chosen = x / (getBounds().width / name.size());

    if ( ( (String) name.elementAt(chosen)).equals("Protocolo")) {

      try {
        PanelTuberCon miPanel = null;
        miPanel = ( (PanelTuberCon) ( (CTabPanelExtC) getParent()).
                   getPanelActual());
        paint(getGraphics());
        ( (CTabPanelExtC) getParent()).VerPanel( (String) name.elementAt(chosen));
      }
      catch (Exception ex) {

        PanAux miPanelAux = null;
        miPanelAux = ( (PanAux) ( (CTabPanelExtC) getParent()).getPanelActual());
        Common.ShowWarning(miPanelAux.a,
            "El protocolo no se puede ver hasta que no se grabe el contacto");
        paint(getGraphics());
        ( (CTabPanelExtC) getParent()).VerPanelAnterior();

      }

    }
    else {

      paint(getGraphics());
      ( (CTabPanelExtC) getParent()).VerPanel( (String) name.elementAt(chosen));
    }

    return true;
  } // fin metodo

}