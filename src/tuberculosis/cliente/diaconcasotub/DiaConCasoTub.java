package tuberculosis.cliente.diaconcasotub;

//modificacion momentanea
import java.net.URL;
import java.util.Hashtable;

import java.awt.Cursor;
import java.awt.Panel;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import comun.Common;
import comun.Comunicador;
import comun.DialogoGeneral;
import comun.constantes;
import infproto.DataProtocolo;
import infproto.PanelTuberCon;
import sapp.StubSrvBD;
import tuberculosis.cliente.mantcontub.PanConTub;
import tuberculosis.cliente.pancabecera.PanCabecera;
import tuberculosis.cliente.pansupcon.PanAux;
import tuberculosis.cliente.pansupcon.PanSupCon;
import tuberculosis.datos.mantcontub.DatConCasTub;
import tuberculosis.datos.mantcontub.DatConTub;

//import tuberculosis.datos.notiftub.*;   // $$$
//import tuberculosis.servidor.mantcontub.*; // $$$

public class DiaConCasoTub
    extends DialogoGeneral {

  // Metamodos de funcionamiento
  private static final int servletSELECT_NORMAL = 1;
  private static final int servletSELECT_SUCA = 2;

  //nombre de tabs
  public String sCabecera = "Datos de filiaci�n";
  public String sNulo = "Protocolo";

  //Ya hubo msg en protocolo
  public boolean YaSalioMsgEnferSinProtocolo = false;

//  public static final String strSERVLET_CONCASTUB = "servlet/SrvConCasTub";
  public static final String strSERVLET_CONCASTUB = constantes.
      strSERVLET_CONCASTUB;

  //Error en la construccion del dialogo
  public boolean bError = false;

  // contenedor de im�genes
  protected CCargadorImagen imgs = null;

  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

  // Modos de operaci�n del panel: paquete comun.constantes
  final int modoESPERA = constantes.modoESPERA;
  final int modoALTA = constantes.modoALTA;
  final int modoMODIFICACION = constantes.modoMODIFICACION;
  final int modoBAJA = constantes.modoBAJA;
  final int modoCONSULTA = constantes.modoCONSULTA;
  final int modoSOLODECLARANTES = constantes.modoSOLODECLARANTES;

  // modos de operaci�n del servlet SrvEnfermo
//  final int modoDATOSENFERMO = 10;
//  final int modoDATOSENFERMOTRAMERO = 320;

  //Parametros que se pasan al Panel principal
  private String sAno = null;
  private String sReg = null;
  private String sNom = null;

  // Bloqueo de RTBC
  private String CD_OPE_RTBC = null;
  private java.sql.Timestamp FC_ULTACT_RTBC = null;

  // 'Coordenadas' del protocolo del caso con el que
  // est� relacionado el contacto
  private String CD_NIVEL_1_EDOIND = null;
  private String CD_NIVEL_2_EDOIND = null;

  // Modo de entrada al panel

  // modificacion 27/04/00
  //protected int modoOperacion = constantes.modoALTA;

  public StubSrvBD stubCliente = null;

  // Modo de salida del dialog (Aceptar=0, Cancelar=-1)
  //Aceptar == repetir query al volver
  //Cancelar == nada
  public int iOut = -1;

  CApp capp;

  // controles
  XYLayout xYLayout = new XYLayout();
  XYLayout layoutBotones = new XYLayout();
  XYLayout layoutSuperior = new XYLayout();

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  Panel panelBotones = new Panel();
  StatusBar barra = new StatusBar();

  //public CTabPanel tabPanel = new CTabPanel();
  // modificacion 12/07/2000
  public CTabPanelExtC tabPanel = new CTabPanelExtC();

  //variables globales de entrada
  public Hashtable hashListas_completa = null;

  //public String sNM_EDO = "";
  private String sANO;
  private String sREG;
  private String sCodEnfermo = "";

  public DatConTub datcasostub = new DatConTub();
  public DatConCasTub datconcastub = new DatConCasTub();
  public DatConTub dtc = new DatConTub();

  //paneles
  public PanSupCon pansup = null;
  public PanCabecera panCabecera = null;
  //public PanCabeceraCon panCabecera = null;
  public PanAux panaux = null;
  public PanConTub pancontub = null;
  public PanelTuberCon pProtocolo = null;

  //Registro Seleccionado
  public DatConTub RegSel = null;

  // M�todos para disponer de interfaz con otros componentes

  // 'Coordenadas' de EDOIND para el protocolo
  public String getCD_NIVEL_1_EDOIND() {
    return CD_NIVEL_1_EDOIND;
  }

  public String getCD_NIVEL_2_EDOIND() {
    return CD_NIVEL_2_EDOIND;
  }

  public void setCD_NIVEL_1_EDOIND(String s) {
    CD_NIVEL_1_EDOIND = s;
  }

  public void setCD_NIVEL_2_EDOIND(String s) {
    CD_NIVEL_2_EDOIND = s;
  }

  // Datos de caso de RTBC
  public String getAno_RTBC() {
    return sANO;
  }

  public String getReg_RTBC() {
    return sREG;
  }

  public int getContacto() {
    return new Integer(sCodEnfermo).intValue();
  }

  //

  public String getCD_OPE_RTBC() {
    return CD_OPE_RTBC;
  }

  public java.sql.Timestamp getFC_ULTACT_RTBC() {
    return FC_ULTACT_RTBC;
  }

  public void setCD_OPE_RTBC(String s) {
    CD_OPE_RTBC = s;
  }

  public void setFC_ULTACT_RTBC(java.sql.Timestamp ts) {
    FC_ULTACT_RTBC = ts;
  }

  public void setAno(String sAno) {
    pansup.setAno(sAno);
  }

  public void setReg(String sReg) {
    pansup.setReg(sReg);
  }

  public void setNom(String sNom) {
    pansup.setNom(sNom);
  }

  public String getCodEnf() {
    RegSel = (DatConTub) pancontub.listaRegConTub.elementAt(pancontub.tabla.
        getSelectedIndex());
    return (RegSel.getCD_ENFERMO().trim());
  }

  public DiaConCasoTub(PanConTub pct, CApp a, Hashtable hashListas, int modo,
                       DatConTub datos, Hashtable hEdoind) {
    super(a);
    try {

      final int modoCargar = modo;

      if (hEdoind != null) {
        //setCD_NIVEL_1_EDOIND((String)hEdoind.get("CD_NIVEL_1_EDOIND"));
        //setCD_NIVEL_2_EDOIND((String)hEdoind.get("CD_NIVEL_2_EDOIND"));

        // modificacion 09/06/2000
        // CUIDADO, utilizo los metodos EDOIND para recuperar
        // los niveles GE, y recuperar los niveles 1 y 2
        setCD_NIVEL_1_EDOIND( (String) hEdoind.get("CD_NIVEL_1_GE"));
        setCD_NIVEL_2_EDOIND( (String) hEdoind.get("CD_NIVEL_2_GE"));
      }

      capp = a;
      pancontub = pct;

      modoOperacion = modoCargar;
      stubCliente = new StubSrvBD();
      hashListas_completa = (Hashtable) hashListas.clone();

      YaSalioMsgEnferSinProtocolo = false;

      if (modoOperacion != modoALTA) {
        sCodEnfermo = datos.getCD_ENFERMO();
        sANO = datos.getCD_ANO();
        sREG = datos.getCD_REG();
      }

      //paneles
      pansup = new PanSupCon(this, modoCargar, hashListas_completa);
      panaux = new PanAux(this, modoCargar, hashListas_completa);
      panaux.setBorde(false);

      panCabecera = new PanCabecera(this, modoCargar, hashListas_completa, false);
      //panCabecera = new PanCabeceraCon(this, modoCargar, hashListas_completa, false);
      jbInit();
      pack();
      modoOperacion = modoCargar;

      //ALTA: cabecera + notificadores + superior
      if (modoOperacion == modoALTA) {
        tabPanel.InsertarPanel(sCabecera, panCabecera);
        //panel tabla -----------
        //panTabla.rellena(null);
        //-----------
        tabPanel.InsertarPanel(sNulo, panaux);
        tabPanel.VerPanel(sCabecera);

      }

      //MODIF: cabecera+notificadores+superior +caso + protocolo +otros
      if ( (modoOperacion == modoMODIFICACION)
          || (modoOperacion == modoBAJA) || (modoOperacion == modoCONSULTA)) {

        Inicializar(modoESPERA);

        //*E tabPanel.InsertarPanel(sCabecera, panCabecera);

         //tabPanel.InsertarPanel(sNulo, panaux);
         //*E tabPanel.VerPanel(sCabecera);

          //*E Inicializar(modoESPERA);

           //RellenaDatosTodosPaneles (datos);
        RellenaDatosTodosPaneles();

      } //fin de modos

      modoOperacion = modo;
      Inicializar(modo);
    }
    catch (Exception ex) {
      ex.printStackTrace();
      bError = true;
    }
  }

  //modificacion 12/02/2001
  // constructor para modo altoa
  public DiaConCasoTub(PanConTub pct, CApp a, Hashtable hashListas, int modo,
                       Hashtable hRTBC, Hashtable hEdoind) {
    super(a);
    try {

      final int modoCargar = modo;

      if (hEdoind != null) {
        //setCD_NIVEL_1_EDOIND((String)hEdoind.get("CD_NIVEL_1_EDOIND"));
        //setCD_NIVEL_2_EDOIND((String)hEdoind.get("CD_NIVEL_2_EDOIND"));

        // modificacion 09/06/2000
        // CUIDADO, utilizo los metodos EDOIND para recuperar
        // los niveles GE, y recuperar los niveles 1 y 2
        setCD_NIVEL_1_EDOIND( (String) hEdoind.get("CD_NIVEL_1_GE"));
        setCD_NIVEL_2_EDOIND( (String) hEdoind.get("CD_NIVEL_2_GE"));
      }

      capp = a;
      pancontub = pct;

      modoOperacion = modoCargar;
      stubCliente = new StubSrvBD();
      hashListas_completa = (Hashtable) hashListas.clone();

      YaSalioMsgEnferSinProtocolo = false;

      sANO = (String) hRTBC.get("CD_ARTBC");
      sREG = (String) hRTBC.get("CD_NRTBC");

      //paneles
      pansup = new PanSupCon(this, modoCargar, hashListas_completa);
      panaux = new PanAux(this, modoCargar, hashListas_completa);
      panaux.setBorde(false);

      panCabecera = new PanCabecera(this, modoCargar, hashListas_completa, false);
      jbInit();
      pack();
      modoOperacion = modoCargar;

      //ALTA: cabecera + notificadores + superior
      if (modoOperacion == modoALTA) {
        // Le pasamos el a�o y el n�mero de registro
        panCabecera.cargaAnioNumReg(sANO, sREG);
        tabPanel.InsertarPanel(sCabecera, panCabecera);
        //panel tabla -----------
        //panTabla.rellena(null);
        //-----------
        tabPanel.InsertarPanel(sNulo, panaux);
        tabPanel.VerPanel(sCabecera);

      }

      //MODIF: cabecera+notificadores+superior +caso + protocolo +otros
      if ( (modoOperacion == modoMODIFICACION)
          || (modoOperacion == modoBAJA) || (modoOperacion == modoCONSULTA)) {

        Inicializar(modoESPERA);

        //*E tabPanel.InsertarPanel(sCabecera, panCabecera);

         //tabPanel.InsertarPanel(sNulo, panaux);
         //*E tabPanel.VerPanel(sCabecera);

          //*E Inicializar(modoESPERA);

           //RellenaDatosTodosPaneles (datos);
        RellenaDatosTodosPaneles();

      } //fin de modos

      modoOperacion = modo;
      Inicializar(modo);
    }
    catch (Exception ex) {
      ex.printStackTrace();
      bError = true;
    }
  }

  //public void RellenaDatosTodosPaneles(DatRT datos) throws Exception {
  public void RellenaDatosTodosPaneles() throws Exception {
    IraPorDatos(); //completa la hash: hashListas_completa

    //CLista listado = (CLista) hashListas_completa.get("DATOS");
    //CLista listadatos = (CLista) hashListas_completa.get(constantes.DATOS);

    //pansup.recogerDatos(datconcastub);

    tabPanel.InsertarPanel(sCabecera, panCabecera);
    tabPanel.VerPanel(sCabecera);

    panCabecera.rellenaDatosCon(datconcastub);

    //protocolo
    if (pProtocolo != null) {
      borrarProtocolo();
    }
    addProtocolo();

    //AUTORIZACIONES();
  }

  /*
     void AUTORIZACIONES(){
     //abrir en un modo u otro segun autorizaciones (cambiando modoOperacion)
     }
   */

  void jbInit() throws Exception {

    // carga las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));

    // t�tulo
    this.setTitle("Contactos de Tuberculosis");

    // layout - controles
    xYLayout.setHeight(530);
    xYLayout.setWidth(784);
    setSize(784, 530);
    setLayout(xYLayout);

    if ( (modoOperacion == modoBAJA)
        || (modoOperacion == modoCONSULTA)) {
      btnAceptar.setLabel("Aceptar");
    }
    else {
      btnAceptar.setLabel("Grabar");

    }
    btnCancelar.setLabel("Salir");

    layoutBotones.setHeight(44);
    layoutBotones.setWidth(780);
    barra.setBevelOuter(BevelPanel.LOWERED);
    barra.setBevelInner(BevelPanel.LOWERED);
    panelBotones.setLayout(layoutBotones);
    panelBotones.add(barra, new XYConstraints(5, 10, 400, -1));
    panelBotones.add(btnAceptar, new XYConstraints(530, 10, 100, -1)); //520
    panelBotones.add(btnCancelar, new XYConstraints(645, 10, 100, -1)); //635

    add(pansup, new XYConstraints(10, 3, 770, 65));

    add(tabPanel, new XYConstraints(10, 120 - 40, 770, 286 + 100)); //10, 106, 755, 300
    add(panelBotones, new XYConstraints(10, 409 - 20 + 80, 770, 44 + 30)); //10, 409, 755, 44

    DiaActionAdapter actionAdapter = new DiaActionAdapter(this);

    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    panCabecera.setVisible(false);
    panCabecera.doLayout();
    panCabecera.setVisible(true);
    panCabecera.repaint();
  }

  private void IraPorDatos() {
    try {
      DatConCasTub dcct = new DatConCasTub();

      int iModoServlet = 0;
      if (capp.getIT_TRAMERO().equals("S")) {
        iModoServlet = servletSELECT_SUCA;
      }
      else {
        iModoServlet = servletSELECT_NORMAL;

        //sCodEnfermo = dct.getCD_ENFERMO(getCodEnf());
      }
      CLista data = new CLista();

      dcct.setMODO_SUCA(new Integer(iModoServlet));
      dcct.setCD_ENFERMO(new Integer(getCodEnf()).intValue());
      dcct.setCD_ANO_RTBC(sANO);
      dcct.setCD_REG_RTBC(sREG);

      //comun.DataEnfermo hash = new comun.DataEnfermo("CD_ENFERMO");
      //hash.put("CD_ENFERMO", getCodEnf());

      data.addElement(dcct);

      stubCliente = new StubSrvBD();
      stubCliente.setUrl(new URL(capp.getURL() + strSERVLET_CONCASTUB));

      CLista resultado = null;

      resultado = (CLista) stubCliente.doPost(constantes.modoSELECT, data);

      /*SrvConCasTub srv = new SrvConCasTub();
               resultado = srv.doPrueba(constantes.modoSELECT, data);*/

      if (resultado != null) {
        datconcastub = (DatConCasTub) resultado.firstElement();
      }

      // modificacion 06/07/2000
      CD_OPE_RTBC = datconcastub.getCD_OPE_RTBC();
      FC_ULTACT_RTBC = datconcastub.getFC_ULTACT_RTBC();
      // hashListas_completa.put("DATOS", resultado);

    }
    catch (Exception e) {
      System.out.println(e.getMessage());
      Common.ShowWarning(capp, "Error en la conexi�n rellenando los paneles");
      //dispose(); //no funciona, continua
      //return false;
    }
    //return true;

  }

  void btnCancelar_actionPerformed() {
    iOut = -1;
    dispose();
  }

  public StatusBar getBarra() {
    return barra;
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {

      case modoESPERA:
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        if (pansup != null) {
          pansup.Inicializar(modoOperacion);
        }
        if (panCabecera != null) {
          panCabecera.Inicializar(modoOperacion);
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.deshabilitarCampos(Cursor.WAIT_CURSOR);

        }
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        if (panaux != null) {
          panaux.setVisible(false);
        }
        if (pansup != null) {
          pansup.Inicializar(modoOperacion);
        }
        if (panCabecera != null) {
          panCabecera.Inicializar(modoOperacion);

        }
        if (pProtocolo != null) {
          pProtocolo.pnl.habilitarCampos();

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICACION:

        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        if (panaux != null) {
          panaux.setVisible(false);
        }
        if (pansup != null) {
          pansup.Inicializar(modoOperacion);
        }
        if (panCabecera != null) {
          panCabecera.Inicializar(modoOperacion);
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.habilitarCampos();

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        break;

      case modoBAJA:
      case modoCONSULTA:

        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        if (pansup != null) {
          pansup.Inicializar(modoOperacion);
        }
        if (panCabecera != null) {
          panCabecera.Inicializar(modoOperacion);
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.deshabilitarCampos(Cursor.DEFAULT_CURSOR);

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        break;
    }

    //botones -----------------------
    if ( (modoOperacion == modoBAJA)
        || (modoOperacion == modoCONSULTA)) {
      btnAceptar.setLabel("Aceptar");
    }
    else {
      btnAceptar.setLabel("Grabar");
    }
    btnCancelar.setLabel("Cancelar");
    if (modoOperacion == modoCONSULTA) {
      btnAceptar.setEnabled(false);
      //----------------------------------

    }
    doLayout();
  }

  void btnAceptar_actionPerformed() {
    /*
     if (bError){
         comun.ShowWarning(this.app, "Error al cargar datos. Salga por Cancelar");
        return;
     }
     */

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();

    if (modo == constantes.modoBAJA) {

      borrarConTub();
      return;

    }

    else if (modo == constantes.modoMODIFICACION) {

      if (!panCabecera.validarDatos()) {
        tabPanel.VerPanel(sCabecera);
        Inicializar(modo);
        return;
      }

      modificarConTub();
      return;

    }

    else if (modo == constantes.modoALTA) {

      if (!panCabecera.validarDatos()) {
        tabPanel.VerPanel(sCabecera);
        Inicializar(modo);
        return;
      }

      insertarConTub();
      //dispose();
      return;

    } //fin alta

  } //fin funcion ACEPTAR *****************************************

// Realiza la comunicaci�n con el servlet strSERVLET_CONCASTUB
  CLista hazAccion(StubSrvBD stubCliente, int modo, CLista parametros) throws
      Exception {
    CLista result = null;

    // Invocacion del servlet para que haga el insert
    result = Comunicador.Communicate(this.getCApp(),
                                     stubCliente,
                                     modo,
                                     strSERVLET_CONCASTUB,
                                     parametros);

    // modificacion momentanea
    // SOLO DESARROLLO

    /*    SrvConCasTub srv = new SrvConCasTub();
        parametros.setLogin(this.app.getLogin());
        result = srv.doPrueba(modo, parametros);*/

    return result;
  } // Fin hazAccion()

  private void borrarConTub() {
    //entra en ESPERA

    //recoger los datos de los paneles
    DatConCasTub datosCont = null;

    //
    if (datconcastub == null) {
      datosCont = new DatConCasTub();
    }
    else {
      datosCont = datconcastub;
    }

    //datosCont = pansup.recogerDatos(datosCont);//MACHACAR: ano, a G d G

    //datosCaso.setFC_INIRTBC(this.getCApp().getFC_ACTUAL());

    datosCont = panCabecera.recogerDatContacto(datosCont); //MACHACAR

    CLista listaSalida = new CLista();
    CLista parametros = new CLista();
    parametros.addElement(datosCont);

    boolean bOK = false;

    try {
      listaSalida = hazAccion(stubCliente, constantes.modoBAJA, parametros);
      bOK = true;
    }
    catch (Exception e) { //acceso a base de datos mal o bloqueo
      if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
        if (Common.ShowPregunta(this.app, constantes.DATOS_BLOQUEADOS)) {
          try {
            listaSalida = hazAccion(stubCliente, constantes.modoBAJA_SB,
                                    parametros);
            bOK = true;
          }
          catch (Exception ex) {
            bOK = false;
            Common.ShowWarning(this.app,
                               "Error al realizar el borrado del caso");
          }
        }
        else { //no quiere sobreescribir
          bOK = false;
        }
      }
      else { //error normal
        bOK = false;
        Common.ShowWarning(this.app, "Error al realizar el borrado del caso");
      }
    } //fin del catch

    if (bOK) {
      datconcastub = (DatConCasTub) listaSalida.firstElement();

      CD_OPE_RTBC = datconcastub.getCD_OPE_RTBC();
      FC_ULTACT_RTBC = datconcastub.getFC_ULTACT_RTBC();

      dispose();
    }
    else {
      iOut = -1;
      modoOperacion = modoBAJA;
      this.btnCancelar.requestFocus();
      Inicializar();
      return;
    }

    modoOperacion = modoBAJA;
    Inicializar();
  }

  private void insertarConTub() {

    //entra en ESPERA

    //recoger los datos de los paneles
    DatConCasTub datosCont = null;
    //LA INICIAL (puede contener datos del enfermo. Normalmente: vacia)
    if (datconcastub == null) {
      datosCont = new DatConCasTub();
      datosCont.setCD_ENFERMO( -1);
    }
    else {
      datosCont = datconcastub;
    }

    datosCont = pansup.recogerDatos(datosCont); //MACHACAR: ano, a G d G
    datosCont = panCabecera.recogerDatContacto(datosCont); //MACHACAR

    datosCont.setCD_OPE_RTBC(CD_OPE_RTBC);
    datosCont.setFC_ULTACT_RTBC(FC_ULTACT_RTBC);

    CLista listaSalida = new CLista();
    CLista parametros = new CLista();
    parametros.addElement(datosCont);
    //parametros.addElement(vTabla);

    boolean bOK = false;

    try {
      listaSalida = hazAccion(stubCliente, constantes.modoALTA, parametros);
      bOK = true;
    }
    catch (Exception e) { //acceso a base de datos mal o bloqueo
      if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
        if (Common.ShowPregunta(this.app, constantes.DATOS_BLOQUEADOS)) {
          try {
            listaSalida = hazAccion(stubCliente, constantes.modoALTA_SB,
                                    parametros);
            bOK = true;
          }
          catch (Exception ex) {
            bOK = false;
            Common.ShowWarning(this.app,
                               "Error al realizar la inserci�n del contacto");
          }
        }
        else { //no quiere sobreescribir
          bOK = false;
        }
      }
      else { //error normal
        bOK = false;
        Common.ShowWarning(this.app,
                           "Error al realizar la inserci�n del contacto");
      }
    } //fin del catch

    if (bOK) {
      datconcastub = (DatConCasTub) listaSalida.elementAt(0);

      CLista lista = new CLista();
      lista.addElement(datconcastub);
      hashListas_completa.put("DATOS", lista);

      pansup.txtReg.setText(datconcastub.getCD_REG_RTBC());

      // Para cargar el n�mero de enfermo/contacto
      panCabecera.txtCodEnfermo.setText(String.valueOf(datconcastub.
          getCD_ENFERMO()));

      listaSalida = null;
      this.btnCancelar.requestFocus();
      iOut = 0;

      //sNM_EDO = datconcastub.getNM_EDO();
      sCodEnfermo = String.valueOf(datconcastub.getCD_ENFERMO());
      sANO = datconcastub.getCD_ANO_RTBC();
      sREG = datconcastub.getCD_REG_RTBC();

      modoOperacion = modoMODIFICACION;
    }
    else {
      iOut = -1;
      modoOperacion = modoALTA;
      this.btnCancelar.requestFocus();
      Inicializar();
      return;
    }

//  Inicializar();

    //insertar protocolo si estaba abto
    if (pProtocolo != null) {
      //actualizar el panelInforme con nm_edo
      //pProtocolo.pnl.Cargar_nm_edo(sNM_EDO);
      //GRABAR EL PROTOCOLO, porque puede haber metido algo!!!!!
      boolean b = pProtocolo.pnl.ValidarObligatorios();
      if (b) {
        pProtocolo.pnl.Insertar(); //Inserta datos en la tabla respedo
        //dispose(); //Enrique quiere que se quede abto
        // estaba en la pesta�a de protocolo, continuara alli
      }
    }

    // Quito panaux

    if (panaux != null) {
      tabPanel.BorrarSolapa("Protocolo");
      panaux = null;
    }

    //abro Protocolo si cerrado
    if (pProtocolo == null) {
      addProtocolo();

      /*
         //continuamos abriendo Otros
           panOtros = new PanOtros(this, constantes.modoMODIFICACION, hashListas_completa);
         tabPanel.InsertarPanel("Otros", panOtros);
         panOtros.doLayout();
         tabPanel.VerPanel(sCabecera);
       */

    }
    Inicializar();

  } //fin de insertar_caso

  /*
   private void borrarCasoTub(){
    //entra en ESPERA
    //recoger los datos de los paneles
    DatCasosTub datosCaso =  null;
    //LA INICIAL (puede contener datos del enfermo. Normalmente: vacia)
    if (datcasostub == null) {
        datosCaso = new DatCasosTub();
    }
    else {
       datosCaso = datcasostub;
    }
    datosCaso = pansup.recogerDatos(datosCaso);//MACHACAR: ano, a G d G
    //RT
    datosCaso.setFC_INIRTBC(this.getCApp().getFC_ACTUAL());
    datosCaso = panCabecera.recogerDatos(datosCaso);//MACHACAR
    if (panCaso != null)
      datosCaso = panCaso.recogerDatos(datosCaso);
    Vector vTabla = panTabla.getVectorNotificadores();
    CLista listaSalida = new CLista();
    CLista parametros = new CLista();
    parametros.addElement(datosCaso);
    parametros.addElement(vTabla);
    boolean bOK = false;
    try {
        listaSalida = hazAccion(stubCliente, constantes.modoBAJA, parametros);
        bOK = true;
    } catch (Exception e) { //acceso a base de datos mal o bloqueo
        if(e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1){
          if(comun.ShowPregunta(this.app,constantes.DATOS_BLOQUEADOS)) {
            try {
       listaSalida = hazAccion(stubCliente, constantes.modoBAJA_SB, parametros);
              bOK = true;
            }catch(Exception ex) {
              bOK = false;
       comun.ShowWarning(this.app, "Error al realizar el borrado del caso");
            }
          }else { //no quiere sobreescribir
             bOK = false;}
        }
        else {//error normal
          bOK = false;
          comun.ShowWarning(this.app, "Error al realizar el borrado del caso");
        }
    }//fin del catch
    if (bOK){
      dispose();
    }
    else{
      iOut = -1;
      modoOperacion = modoBAJA;
      this.btnCancelar.requestFocus();
      Inicializar();
      return;
    }
    Inicializar();
    /*CLista param = null;
     CLista resul = null;
     CMessage msgBox = null;
     //entra en ESPERA
     //recoger los datos de los paneles  //conserva cd_ope, fc_ultact
     //no tengo que recoger los datos de los paneles porque en
     //modo borrar no se puede modificar nada
     //**********************************
      //TRY GENERAL
      try {
        param = new CLista();
        datosAlerta.insert("CD_SITALERBRO","1");
        param.addElement(datosAlerta);
        param.addElement(datosBrote);
        //va bien: listaSalida = (true) o                //bien
        //         listaSalida = (true, mensaje pregunta) //modif
        //va mal: listaSalida = null y exc
        //nota: si YA ha sido borrado, devuelve TRUE . No constaras como cd_ope
        resul = Comunicador.Communicate(this.getCApp(),
                                stubCliente,
                                servletCOMP_BORR,
                                strSERVLET_MODIF,
                                param);
//ha pasado algo ie  MODIF por algun otro
     if (resul.size() == 2){
          String pre = (String) resul.elementAt(1);
          msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                              pre);
          msgBox.show();
          if (msgBox.getResponse()){  //SI
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            msgBox = null;
            //va bien: listaSalida.size()=0
            //va mal: listaSalida = null y exc
            resul = Comunicador.Communicate(this.getCApp(),
                      stubCliente,
                      servletBORRADO,
                      strSERVLET_MODIF,
                      param);
            if (resul.size() == 0){
              //ShowWarning("El brote ha sido borrado");
               iOut = 0; //repetir query con datos buenos de db
               dispose();
               return;
              //NO INICIALIZAMOS. NO HACE FALTA!!!!
            }
          }
          else{//NO quiere sobreescribir
               iOut = 0;//Como Aceptar repetir query con datos buenos de db
               this.btnCancelar.requestFocus();
               modoOperacion = modoBAJA;
               Inicializar();
               return;
          }
        }
//SI HA IDO BIEN
     else if (resul.size() == 1){
          // todo ha ido bien y lo que trae es un true
          //ShowWarning("El brote ha sido borrado");
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          iOut = 0;
          dispose();
          return;
          //NO INICIALIZAMOS. NO HACE FALTA!!!!
     }
      // error en el proceso
      } catch (Exception e) {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            e.printStackTrace();
            msgBox = new CMessage(this.app, CMessage.msgERROR, "Error al realizar el borrado del brote");
            msgBox.show();
            msgBox = null;
            iOut = -1;//Como cancelar
            this.btnCancelar.requestFocus();
            modoOperacion = modoBAJA;
            Inicializar();
      }
     */

    /*
     }
     */

    private void modificarConTub() {
      //entra en ESPERA

      //recoger los datos de los paneles
      DatConCasTub datosCont = null;

      //
      if (datconcastub == null) {
        datosCont = new DatConCasTub();
      }
      else {
        datosCont = datconcastub;
      }

      //datosCont = pansup.recogerDatos(datosCont);//MACHACAR: ano, a G d G

// Le meto los valores del ope y ultact por si en alta no le
// hubieran llegado
      if ( (datosCont.getCD_OPE_RTBC() == null) ||
          (datosCont.getCD_OPE_RTBC().trim().length() == 0)) {
        datosCont.setCD_OPE_RTBC(CD_OPE_RTBC);
      }
      if ( (datosCont.getFC_ULTACT_RTBC() == null) ||
          (datosCont.getFC_ULTACT_RTBC().getTime() == 0)) {
        datosCont.setFC_ULTACT_RTBC(FC_ULTACT_RTBC);
//System.out.println(datosCont.getCD_ANO_RTBC()+";"+datosCont.getCD_REG_RTBC())
      }
      datosCont = panCabecera.recogerDatContacto(datosCont); //MACHACAR

      CLista listaSalida = new CLista();
      CLista parametros = new CLista();
      parametros.addElement(datosCont);
      //parametros.addElement(vTabla);

      boolean bOK = false;

      try {
        listaSalida = hazAccion(stubCliente, constantes.modoMODIFICACION,
                                parametros);
        bOK = true;
      }
      catch (Exception e) { //acceso a base de datos mal o bloqueo
        if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
          if (Common.ShowPregunta(this.app, constantes.DATOS_BLOQUEADOS)) {
            try {
              listaSalida = hazAccion(stubCliente,
                                      constantes.modoMODIFICACION_SB,
                                      parametros);
              bOK = true;
            }
            catch (Exception ex) {
              bOK = false;
              Common.ShowWarning(this.app,
                                 "Error al realizar la modificaci�n del caso");
            }
          }
          else { //no quiere sobreescribir
            bOK = false;
          }
        }
        else { //error normal
          bOK = false;
          Common.ShowWarning(this.app,
                             "Error al realizar la modificaci�n del caso");
        }
      } //fin del catch

      if (bOK) {
        datconcastub = (DatConCasTub) listaSalida.elementAt(0);

        CLista lista = new CLista();
        lista.addElement(datconcastub);
        hashListas_completa.put("DATOS", lista);
        pansup.txtReg.setText(datconcastub.getCD_REG_RTBC());

        // Para cargar el n�mero de enfermo/contacto
        panCabecera.txtCodEnfermo.setText(String.valueOf(datconcastub.
            getCD_ENFERMO()));

        listaSalida = null;
        this.btnCancelar.requestFocus();
        iOut = 0;

        sANO = datconcastub.getCD_ANO_RTBC();
        sREG = datconcastub.getCD_REG_RTBC();

        CD_OPE_RTBC = datconcastub.getCD_OPE_RTBC();
        FC_ULTACT_RTBC = datconcastub.getFC_ULTACT_RTBC();

        //EXISTIA PROTOCOLO PARA LA ENFERMEDAD
        if (pProtocolo != null) {
          // Se actualizan datos para no autobloquear la grabaci�n del protocolo
          //GRABAR EL PROTOCOLO, porque puede haber metido algo!!!!!
          boolean b = pProtocolo.pnl.ValidarObligatorios();
          if (b) {
            pProtocolo.pnl.Insertar(); //Inserta datos en la tabla respcontacto
          }
        }
      }
      else {
        iOut = -1;
        //modoOperacion = modoMODIFICACION;
        this.btnCancelar.requestFocus();
        //Inicializar();
        //return;
      }

      modoOperacion = modoMODIFICACION;
      Inicializar();
    }

  // Devuelve una Hashtable conteniendo los datos de bloqueo
  // de las tablas SIVE_REGISTROTBC y SIVE_ENFERMO
  public Hashtable getDatosBloqueo() {
    Hashtable h = new Hashtable();

    DatConCasTub dcct = new DatConCasTub();

    dcct.setCD_OPE(datconcastub.getCD_OPE());
    dcct.setFC_ULTACT(datconcastub.getFC_ULTACT());
    dcct.setCD_OPE_RTBC(datconcastub.getCD_OPE_RTBC());
    dcct.setFC_ULTACT_RTBC(datconcastub.getFC_ULTACT_RTBC());

    h.put(constantes.DATOSBLOQUEO, dcct);
    return h;
  }

  public void borrarProtocolo() {
    tabPanel.BorrarSolapa("Protocolo");
    tabPanel.VerPanel(sCabecera);
  }

  public void addProtocolo() {
    DataProtocolo data = new DataProtocolo();
    //data.sCodigo = constantes.TUBERCULOSIS;
    // modificacion 12/02/2001
    data.sCodigo = this.app.getParametro("CD_TUBERCULOSIS");
    data.sDescripcion = "TUBERCULOSIS";

    // Para que el contacto disponga del mismo protocolo
    // que el caso con el que est� relacionado
    data.sNivel1 = getCD_NIVEL_1_EDOIND(); //panCabecera.txtCodNivel1.getText().trim();
    data.sNivel2 = getCD_NIVEL_2_EDOIND(); //panCabecera.txtCodNivel2.getText().trim();

    //data.NumCaso = sNM_EDO;
    //data.NumCaso = "999999"; // Se pasa un caso de pega
    // modificacion 05/07/2000
    //data.NumCaso = (String) (new Integer(getContacto())).toString(); LO QUE HABIA

    //si venimos del caso en que se lanza la busqueda de enfermo
    // el codigo de enfermo viene en esta variable
    if (sCodEnfermo.equals("") || sCodEnfermo == null) {
      sCodEnfermo = (String) panCabecera.sStringCodEnf;

    }
    data.NumCaso = (String) (new Integer(getContacto())).toString();

    data.sCa = app.getCA();

    // borramos la solapa por si acaso exist�a; as� actualizamos
    tabPanel.BorrarSolapa("Protocolo");
    pProtocolo = null;
    pProtocolo = new PanelTuberCon(capp, data, this); //MIO!!!!!!!

    if (!pProtocolo.pnl.getNoSeEncontraronDatos()) {
      YaSalioMsgEnferSinProtocolo = false;
      tabPanel.InsertarPanel("Protocolo", pProtocolo);
      pProtocolo.doLayout();
      tabPanel.VerPanel(sCabecera);
    }
    else {
      if (!YaSalioMsgEnferSinProtocolo) {
        Common.ShowWarning(this.app,
                           "La enfermedad no tiene un protocolo definido.");

      }
      YaSalioMsgEnferSinProtocolo = true;
      pProtocolo = null;
    }
  }

  /*    Ya asteriscado Ped
     CLista param = null;
     CLista resul = null;
     CMessage msgBox = null;
     String cd_sitAntes = datosAlerta.getCD_SITALERBRO();
     //entra en ESPERA
     //recoger los datos de los paneles  //conserva cd_ope, fc_ultact
     DataBrote datosPantalla = new DataBrote();
     datosPantalla.insert( "CD_OPE", datosBrote.getCD_OPE());
     datosPantalla.insert( "FC_ULTACT", datosBrote.getFC_ULTACT());
     panelSuperior.recogerDatos(datosPantalla);
     pDatos.recogerDatos(datosPantalla);
     pColectivo.recogerDatos(datosPantalla);
     pCuadro.recogerDatos(datosPantalla);
     DataAlerta datosPantAler = new DataAlerta();
     datosPantAler = recogerDatos(datosPantAler, modoMODIFICACION); //dlg
     //**********************************
      //TRY GENERAL
      try {
     param = new CLista();
     param.addElement(datosPantAler);
     param.addElement(datosPantalla);
     //va bien: listaSalida = (true, DataAlerta, DataBrote) o   - catch
     //         listaSalida = (true, mensaje pregunta) - try
     //va mal: listaSalida = null y exc
     resul = Comunicador.Communicate(this.getCApp(),
                             stubCliente,
                             servletCOMP_MOD,
                             strSERVLET_MODIF,
                             param);
     setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
     try {
       //pregunta si se quiere sobreescribir
       String pre = (String) resul.elementAt(1);
       if (pre.indexOf("borrado")!= -1){
         //el brote hab�a sido modificado
         msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                           pre);
         msgBox.show();
         if (msgBox.getResponse()){  //SI
           setCursor(new Cursor(Cursor.WAIT_CURSOR));
           msgBox = null;
           resul = Comunicador.Communicate(this.getCApp(),
                   stubCliente,
                   servletMODIFICACION,
                   strSERVLET_MODIF,
                   param);
           //va bien: listaSalida (DataAlerta, DataBrote)
           //va mal: listaSalida = null y exc
           if (resul.size() == 2){
             //ShowWarning("El brote ha sido modificada");
        datosAlerta = (DataAlerta) resul.elementAt(0); // no sirve para nada!
             datosBrote = (DataBrote) resul.elementAt(1);
             modoOperacion = modoMODIFICACION;
             Parche_datosAlerta(datosAlerta, cd_sitAntes);
             //actualiza it_prim, fc_prim
             RellenaCheck(datosAlerta);
             this.btnCancelar.requestFocus();
             iOut = 0; //aceptar, ptt se hara de nuevo la query
             modoOperacion = modoMODIFICACION;
             //Inicializar(); continuamos en espera
           }
         }
         else{//NO quiere sobreescribir
            iOut = 0;//Como Aceptar repetir query con datos buenos de db
            this.btnCancelar.requestFocus();
            modoOperacion = modoMODIFICACION;
            Inicializar();
            return;
         }
       }
       else{ //el brote habia sido borrado
         ShowWarning("El informe del brote ha sido borrado.No podr� hacer ninguna operaci�n sobre �l");
         iOut = -1;
         this.btnCancelar.requestFocus();
         modoOperacion = modoMODIFICACION;
         Inicializar();
         return;
       }
     }
     catch (ClassCastException e) {
       // todo ha ido bien y lo que trae es el brote modificado
       //ShowWarning("El brote ha sido modificada");
       datosAlerta = (DataAlerta) resul.elementAt(1);
       datosBrote = (DataBrote) resul.elementAt(2);
       modoOperacion = modoMODIFICACION;
       Parche_datosAlerta(datosAlerta, cd_sitAntes);
       //actualiza it_prim, fc_prim
       RellenaCheck(datosAlerta);
       iOut = 0;//Como Aceptar repetir query con datos buenos de db
       this.btnCancelar.requestFocus();
       modoOperacion = modoMODIFICACION;
       //Inicializar(); continuamos en espera
     }
      // error en el proceso
      } catch (Exception e) {
         setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
         e.printStackTrace();
         msgBox = new CMessage(this.app, CMessage.msgERROR, "Error al realizar la modificaci�n del brote");
         msgBox.show();
         msgBox = null;
         iOut = -1;//Como cancelar
         this.btnCancelar.requestFocus();
         modoOperacion = modoMODIFICACION;
         Inicializar();
         return;
      }
      Inicializar();
      //-----------------------------   */

   /*
    }
    */

   /*
// retorna una clista con un elemento de tipo DataBusqueda
// para  a�adir  (solo hace falta que lleve la clave )
    public Object getComponente() {
       Object o = null;
      if (iOut != -1) {
         CLista lista = new CLista();
         //SIRVE PARA EL ALTA
         DatRT datosDevolver = new DatRT();
           datosDevolver.put ("CD_ARTBC", "pendiente");
           datosDevolver.put ("CD_NRTBC", "pendiente");
           datosDevolver.put ("NM_EDO", "pendiente");
         lista.addElement(datosDevolver);
         o = lista.elementAt(0);
       }
       return o;
     }
    */

   /*
     // Devuelve una Hashtable conteniendo los datos del notificador
     // seleccionado en panTabla
     public Hashtable getNotificador() {
       Hashtable h = new Hashtable();
       h.put(constantes.DATOSNOTIFICADOR, this.panTabla.getNotificador());
       return h;
     }
     //
     public void addProtocolo(){
       DataProtocolo data = new DataProtocolo();
       data.sCodigo = constantes.TUBERCULOSIS;
       data.sDescripcion = "TUBERCULOSIS";
       data.sNivel1 = panCabecera.txtCodNivel1.getText().trim();
       data.sNivel2 = panCabecera.txtCodNivel2.getText().trim();
       data.NumCaso = sNM_EDO;
       data.sCa = app.getCA();
       // borramos la solapa por si acaso exist�a; as� actualizamos
       tabPanel.BorrarSolapa("Protocolo");
       pProtocolo = null;
       pProtocolo = new PanelTuber(capp, data, this);//MIO!!!!!!!
       if (!pProtocolo
       .pnl.getNoSeEncontraronDatos()){
           YaSalioMsgEnferSinProtocolo = false;
           tabPanel.InsertarPanel("Protocolo", pProtocolo);
           pProtocolo.doLayout();
           tabPanel.VerPanel(sCabecera);
       } else {
         if (!YaSalioMsgEnferSinProtocolo)
        comun.ShowWarning(this.app, "La enfermedad no tiene un protocolo definido.");
           YaSalioMsgEnferSinProtocolo = true;
           pProtocolo = null;
       }
     }
     //
     public void addCaso(){
       tabPanel.BorrarSolapa("Caso");
       panCaso = null;
       panCaso = new PanCaso(this, modoOperacion, hashListas_completa);
       tabPanel.InsertarPanel("Caso", panCaso);
       panCaso.doLayout();
       tabPanel.VerPanel(sCabecera);
     }
     public void borrarProtocolo(){
        tabPanel.BorrarSolapa("Protocolo");
        tabPanel.VerPanel(sCabecera);
     }
     public void borrarCaso(){
        tabPanel.BorrarSolapa("Caso");
        tabPanel.VerPanel(sCabecera);
     }
    */
} //FIN CLASE

class DiaActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaConCasoTub adaptee;
  ActionEvent evt;

  DiaActionAdapter(DiaConCasoTub adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if (evt.getActionCommand().equals("aceptar")) {
      adaptee.btnAceptar_actionPerformed();
    }
    else if (evt.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed();
    }
  }
} //fin class
