package tuberculosis.cliente.pansup;

import capp.CApp;
import comun.constantes;
import tuberculosis.cliente.diatuber.DialogTub;

public class AppPanSup
    extends CApp {

  public void init() {
    super.init();
    setTitulo("");
  }

  public void start() {
    int modo = constantes.modoMODIFICACION;
    DialogTub dlg = new DialogTub(this, null, 0, null);
    VerPanel("", new PanSup(dlg, modo, null));
  }
} // endclass AppPanMod1RegTub
