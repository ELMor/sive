/**
 * Clase: PanSup
 * Paquete: tuberculosis.cliente.diatuber
 * Hereda: CPanel
 * Autor: M Luisa Mora
 * Fecha Inicio: 22/11/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberulosis
 * Descripcion: Panel Superior del dialogo. Clave: Registro de Tuberculosis
 */

package tuberculosis.cliente.pansup;

import java.util.Calendar;
import java.util.Hashtable;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.FocusEvent;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CPanel;
import comun.Common;
import comun.constantes;
import panniveles.panelNiveles;
import panniveles.usaPanelNiveles;
import tuberculosis.cliente.diatuber.DialogTub;
import tuberculosis.datos.notiftub.DatCasosTub;
import tuberculosis.datos.notiftub.DatRT;

public class PanSup
    extends CPanel
    implements usaPanelNiveles {

  // Modos de operaci�n del panel: paquete comun.constantes
  final int modoESPERA = constantes.modoESPERA;
  final int modoALTA = constantes.modoALTA;
  final int modoMODIFICACION = constantes.modoMODIFICACION;
  final int modoBAJA = constantes.modoBAJA;
  final int modoCONSULTA = constantes.modoCONSULTA;
  final int modoSOLODECLARANTES = constantes.modoSOLODECLARANTES;

  // Modo de entrada al panel
  protected int modoOperacion = constantes.modoALTA;

  // Dialog que contiene a este panel
  DialogTub dlg;

  // Hashtable con los datos
  Hashtable hs = null;

  //CApp
//  CApp a = null;

  /**************** Componentes del panel *********************/

  // Organizacion del panel
  XYLayout xYLayout = new XYLayout();

  private Label lblAnoReg = new Label("A�o/Registro:");
  public TextField txtAno = new TextField();
  private Label lblBarra = new Label();
  public TextField txtReg = new TextField();
  Label lblNotificador = new Label();
  Label lblZonadeGestion = new Label();
  public panelNiveles panNiv = null;

  // Constructor
  public PanSup(DialogTub dlgPpal, int modo, Hashtable hsEntrada) {
    try {

      dlg = dlgPpal;
      //a = dlg.getCApp();
      this.app = dlg.getCApp();
      jbInit();

      modoOperacion = modo;
      if (modoOperacion == constantes.modoALTA) {
        if (!this.app.getANYO_DEFECTO().equals("")) {
          txtAno.setText(this.app.getANYO_DEFECTO());

          this.panNiv.setCDNivel1(this.app.getCD_NIVEL1_DEFECTO());
          this.panNiv.txtFocusNivel1(); // E 16/02/2000

          //Thread.sleep(100);

          this.panNiv.setCDNivel2(this.app.getCD_NIVEL2_DEFECTO());
          this.panNiv.setDSNivel2(this.app.getDS_NIVEL2_DEFECTO());

        }
        else {
          int iAnoActual = 0;
          Calendar cal = Calendar.getInstance();
          iAnoActual = cal.get(cal.YEAR);
          txtAno.setText(new Integer(iAnoActual).toString());
        }
      }
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    this.setSize(new Dimension(745, 80));
    xYLayout.setHeight(82);
    lblBarra.setText("/");
    xYLayout.setWidth(755);
    this.setLayout(xYLayout);
    //setBorde(false);

    diatuberFocusAdapter escucha = new diatuberFocusAdapter(this);
    lblNotificador.setText("");
    lblZonadeGestion.setText("Zona de Gesti�n: ");
    txtAno.setName("ano");
    txtAno.addFocusListener(escucha);

    // A�o/Registro del registro de tuberculosis. A�o actual por defecto
    this.add(lblAnoReg, new XYConstraints(13, 5, 82, -1));
    this.add(txtAno, new XYConstraints(121, 5, 40, -1));
    this.add(lblBarra, new XYConstraints(167, 5, 13, 25));
    this.add(txtReg, new XYConstraints(182, 5, 62, -1));

    this.add(lblNotificador, new XYConstraints(260, 5, 482, -1));

    this.add(lblZonadeGestion, new XYConstraints(13, 42, 100, -1));
    panNiv = new panelNiveles(this.app, this,
                              panelNiveles.MODO_INICIO,
                              panelNiveles.TIPO_NIVEL_3,
                              panelNiveles.LINEAS_1, true);
    //el ultimo argumento tiene que ser true
    //para que filtre por las autorizaciones
    //false: no filtra
    this.add(panNiv, new XYConstraints(121, 38, 655, 40));
    //obligados
    panNiv.setNivelObligatorio(1, true);

    switch (this.app.getPerfil()) {
      case 4:
      case 5:
        panNiv.setNivelObligatorio(2, true);
        break;
      case 1:
      case 2:
      case 3:
        break;
    }

    txtAno.setBackground(new Color(255, 255, 150));

    //modificacion 12/02/2001 , no quieren este campo en amarillo
    //txtReg.setBackground(new Color(255, 255, 150));
  }

  public String getAno() {
    return (txtAno.getText());
  }

  public void setNotificador(String sNot) {
    lblNotificador.setText(sNot);
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {
    switch (modoOperacion) {
      case constantes.modoESPERA:
        txtAno.setEnabled(false);
        txtReg.setEnabled(false);
        panNiv.setModoEspera();
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
      case constantes.modoALTA:
        panNiv.setModoNormal();
        txtAno.setEnabled(true);
        txtReg.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
      case constantes.modoBAJA:
      case constantes.modoCONSULTA:
      case constantes.modoMODIFICACION:
        lblZonadeGestion.setVisible(false); // E 19/01/2000
        panNiv.setVisible(false);
        panNiv.setModoDeshabilitado(); // E 19/01/2000
        txtAno.setEnabled(false);
        txtReg.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
  }

  public void rellenarDatos(DatRT data) {
    txtAno.setText(data.getCD_ARTBC());
    txtReg.setText(data.getCD_NRTBC());
  }

  public boolean validarDatos() {

    boolean correcto = true;

    //1. perdidas de foco

    if (!panNiv.getCDNivel1().equals("")) {
      if (panNiv.getDSNivel1().equals("")) {
        panNiv.setDSNivel1("");
        panNiv.requestFocus();
        return false;
      }
    }
    if (!panNiv.getCDNivel2().equals("")) {
      if (panNiv.getDSNivel2().equals("")) {
        panNiv.setDSNivel2("");
        panNiv.requestFocus();
        return false;
      }
    }
    //2. longitudes
    //suca (numericos y longitudes)
    //3. Valores numericos
    //Campos obligatorios
    if (panNiv.getCDNivel1().trim().equals("")) {
      Common.ShowError(this.getApp(),
                       "Es necesario introducir el campo " + this.app.getNivel1() +
                       "");
      panNiv.requestFocus();
      return false;
    }
    switch (this.getApp().getPerfil()) {
      case 4:
      case 5:
        if (panNiv.getCDNivel2().trim().equals("")) {
          Common.ShowError(this.getApp(),
                           "Es necesario introducir el campo " +
                           this.app.getNivel2() + "");
          panNiv.requestFocus();
          return false;
        }
        break;
      case 1:
      case 2:
      case 3:
        break;
    }

    return correcto;

  }

  // Recoge los datos que hay por los componentes y los introduce en un DatCasosTub
  public DatCasosTub recogerDatos(DatCasosTub resul) {

    resul.setCD_ARTBC(this.getAno());
    resul.setCD_NIVEL_1_TBC(this.panNiv.getCDNivel1());
    resul.setCD_NIVEL_2_TBC(this.panNiv.getCDNivel2());

    return resul;
  } // Fin recogerDatos()

  public void txtAnoFocusLost() {

    int modo = modoOperacion;
    dlg.Inicializar(constantes.modoESPERA);

    // Obtiene el foco
    boolean bFocus = false;

    int iAnoActual = 0;
    Calendar cal = Calendar.getInstance();
    iAnoActual = cal.get(cal.YEAR);

    String sReserva = this.app.getANYO_DEFECTO(); //por si hay un valor por defecto
    if (sReserva.equals("")) {
      sReserva = new Integer(iAnoActual).toString();

      // Comprobaci�n del valor de txtAno
      // El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo
    }
    try {
      Integer f = new Integer(txtAno.getText());
      int ano = f.intValue();
      if ( (ano < 1000) || (ano > 9999)) {
        Common.ShowWarning(this.app,
            "El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo.");
        txtAno.setText(sReserva);
        bFocus = true;
      }
    }
    catch (Exception e) {
      Common.ShowWarning(this.app,
          "El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo.");
      txtAno.setText(sReserva);
      bFocus = true;
    }

    if (bFocus) {
      txtAno.requestFocus();

    }
    dlg.Inicializar(modo);
  }

  //interface usaPanelNiveles
  public void cambioNivelAntesInformado(int nivel) {
  }

  public int ponerEnEspera() {
    int m = modoOperacion;
    modoOperacion = modoESPERA;
    dlg.Inicializar(modoOperacion);
    return m;
  }

  public void ponerModo(int modo) {
    modoOperacion = modo;
    dlg.Inicializar(modoOperacion);
  }
  //-----------interface-------------

} // Fin clase PanSup

// Perdidas de foco
class diatuberFocusAdapter
    implements java.awt.event.FocusListener, Runnable {
  PanSup adaptee;
  FocusEvent evt;

  diatuberFocusAdapter(PanSup adaptee) { // Constructor
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  } // Fin focusLost()

  public void focusGained(FocusEvent e) {
  }

  // Implementacion de run()
  public void run() {
    if ( ( (TextField) evt.getSource()).getName().equals("ano")) {
      adaptee.txtAnoFocusLost();
    }
  } // Fin run()

} // Fin clase foco
