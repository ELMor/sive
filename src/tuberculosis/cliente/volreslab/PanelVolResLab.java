package tuberculosis.cliente.volreslab;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
//import java.applet.*;
//import capp2.CPanel;
import capp2.CApp;
import capp2.CFileName;
import capp2.CInicializar;
import capp2.CPanel;
import comun.constantes; //EditorFicheros
import sapp2.Data;
import sapp2.Lista;
//BDatos
//PanAnoSemFecha
import tuberculosis.cliente.componentes.ContPanAnoSemFecha;
import tuberculosis.cliente.componentes.PanAnoSemFecha;
import util_ficheros.DataCampos;
import util_ficheros.EditorFichero;

/**
 * Panel de volcado de Resultados de laboratorio.
 *
 * @author JRM&AIC
 * @version  1.1 24/03/2000
 */
/*
 * Clase : tuberculosis.cliente.volTratamientos.PanelVolResLab
 *   Autor    Fecha        Accion
 * JRM&AIC  24/03/2000   La implementa
 */
public class PanelVolResLab
    extends CPanel
    implements CInicializar, ContPanAnoSemFecha {
  XYLayout xyLayout = new XYLayout();

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int ALTOPANEL = 48;

  //Constantes para seleccionar el tipo de consulta
  final int MUESTRAS = 0;
  final int RESISTENCIA = 1;

  //componentes q conforman el panel
  Label lDesde = null;
  Label lHasta = null;
  PanAnoSemFecha panDesde = null;
  PanAnoSemFecha panHasta = null;
  ButtonControl btnAceptar = null;

  CFileName panFicheroM = null;
  CFileName panFicheroR = null;

  Vector vCampos = null;
  Vector descripcionCampos = null;

  DataCampos descripCampos;
  /**
   * Constructor de la clase
   */

  /*
   * M�todo : PanelVolResLab()
   *   Autor    Fecha        Accion
   * JRM&AIC  24/03/2000   La implementa
   */
  public PanelVolResLab(CApp Contenedor) {
    super(Contenedor);
    try {
      lDesde = new Label();
      lHasta = new Label();
      panDesde = new PanAnoSemFecha(this, panDesde.modoVACIO, false);
      panHasta = new PanAnoSemFecha(this, panHasta.modoVACIO, false);
      panFicheroM = new capp2.CFileName(Contenedor); //, "Fich. Muestras");
      panFicheroR = new capp2.CFileName(Contenedor); //, "Fich. Resistencias");
      panFicheroM.setTitulo("Fich. Muestras:");
      panFicheroR.setTitulo("Fich. Resistencias:");
      btnAceptar = new ButtonControl();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Inicializaci�n de los controles.
   */

  /*
   * M�todo : PanelVolResLab.jbInit()
   *   Autor    Fecha        Accion
   * JRM&AIC  24/03/2000   La implementa
   */
  void jbInit() throws Exception {
    final String imgAceptar = "images/aceptar.gif";

    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().CargaImagenes();

    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnAceptar.setLabel("Aceptar");
    btnAceptar.addActionListener(new Eventos(this));

    xyLayout.setHeight(300);
    xyLayout.setWidth(300);
    this.setLayout(xyLayout);

    lDesde.setText("Desde:");
    lHasta.setText("Hasta:");

    // inicializo con diferentes nombres los ficheros
    panFicheroM.txtFile.setText("c:\\data1.txt");
    panFicheroR.txtFile.setText("c:\\data2.txt");

    // A�adimos los escuchadores...
//    btnCancelar.addActionListener(new PanExpEnfermDeclar_btnCancelar_actionAdapter(this));

    this.add(lDesde, new XYConstraints(MARGENIZQ, MARGENSUP, 110, ALTO));
    this.add(panDesde,
             new XYConstraints(MARGENIZQ + 110 + INTERHOR, MARGENSUP, 515, 34));
    this.add(lHasta,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 110,
                               ALTO));
    this.add(panHasta,
             new XYConstraints(MARGENIZQ + 110 + INTERHOR,
                               MARGENSUP + ALTO + INTERVERT, 515, 34));

    this.add(panFicheroM,
             new XYConstraints(MARGENIZQ - 13,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 515, 38));
    this.add(panFicheroR,
             new XYConstraints(MARGENIZQ - 13,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT + ALTOPANEL,
                               515, 38));

    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 70 + 515 - 2 * 88 - 20,
                               MARGENSUP + 2 * ALTO + 4 * INTERVERT +
                               2 * ALTOPANEL, 88, 29));

  }

  /**
   * Genera una estructura data con la consulta y los nombres
   * de los campos para enviarla al servlet
   */

  /*
   * M�todo : PanelVolResLab.paqueteConsulta(int Origen)
   *   Autor    Fecha        Accion
   * JRM&AIC  24/03/2000   La implementa
   */
  Data paqueteConsulta(int Origen) {
    Data paquete = new Data();
    String strConsulta = null;
    int anyoIni = Integer.parseInt(panDesde.getCodAno());
    int anyoFin = Integer.parseInt(panHasta.getCodAno());
    int semanaIni = Integer.parseInt(panDesde.getCodSem());
    int semanaFin = Integer.parseInt(panDesde.getCodSem());
    descripcionCampos = new Vector();

    switch (Origen) {
      case MUESTRAS: {
        descripcionCampos.addElement("REGISTRO");
        descripcionCampos.addElement("FC_INIRTBC");
        descripcionCampos.addElement("NM_RESLAB");
        descripcionCampos.addElement("FC_MUESTRA");
        descripcionCampos.addElement("CD_MUESTRA");
        descripcionCampos.addElement("CD_TTECLAB");
        descripcionCampos.addElement("CD_VMUESTRA");
        descripcionCampos.addElement("CD_TMICOB");
        descripcionCampos.addElement("IT_RESISTENTE");

        // Construimos la select. Ponemos nvl para que el servlet no
        // devuelva null en el campo sino ' '
        strConsulta = " Select " +
            " R.CD_ARTBC||R.CD_NRTBC as REGISTRO, " +
            " nvl(TO_CHAR(R.FC_INIRTBC,'DD/MM/YYYY'),' ') as FC_INIRTBC, " +
            " L.NM_RESLAB, " +
            " nvl(TO_CHAR(L.FC_MUESTRA,'DD/MM/YYYY'),' ') as FC_MUESTRA, " +
            " L.CD_MUESTRA, L.CD_TTECLAB, L.CD_VMUESTRA, L.CD_TMICOB, " +
            " L.IT_RESISTENTE " +
            " from SIVE_EDOIND I, SIVE_REGISTROTBC R, " +
            " SIVE_RESLAB L " +
            " where I.NM_EDO = R.NM_EDO and " +
            " I.NM_EDO = L.NM_EDO and " +
            " ((I.CD_ANOEPI = '" + panDesde.getCodAno() + "' and " +
            " I.CD_SEMEPI >= '" + panDesde.getCodSem() + "') or " +
            "  (I.CD_ANOEPI > '" + panDesde.getCodAno() + "' and " +
            " I.CD_ANOEPI  < '" + panHasta.getCodAno() + "') or " +
            "  (I.CD_ANOEPI = '" + panHasta.getCodAno() + "' and " +
            " I.CD_SEMEPI <= '" + panHasta.getCodSem() + "')) " +
            " order by  I.NM_EDO, I.CD_ANOEPI, I.CD_SEMEPI ";
        break;
      }
      case RESISTENCIA: {
        descripcionCampos.addElement("REGISTRO");
        descripcionCampos.addElement("FC_INIRTBC");
        descripcionCampos.addElement("NM_RESLAB");
        descripcionCampos.addElement("CD_ESTREST");

        // Construimos la select. Ponemos nvl para que el servlet no
        // devuelva null en el campo sino ' '
        strConsulta = " Select " +
            " R.CD_ARTBC||R.CD_NRTBC as REGISTRO, " +
            " nvl(TO_CHAR(R.FC_INIRTBC,'DD/MM/YYYY'),' ') as FC_INIRTBC, " +
            " L.NM_RESLAB, RR.CD_ESTREST" +
            " from SIVE_EDOIND I, SIVE_REGISTROTBC R, " +
            " SIVE_RESLAB L, SIVE_RES_RESISTENCIA RR " +
            " where I.NM_EDO = R.NM_EDO and " +
            " I.NM_EDO = L.NM_EDO and " +
            " RR.NM_RESLAB = L.NM_RESLAB and " +
            " ((I.CD_ANOEPI = '" + panDesde.getCodAno() + "' and " +
            " I.CD_SEMEPI >= '" + panDesde.getCodSem() + "') or " +
            "  (I.CD_ANOEPI > '" + panDesde.getCodAno() + "' and " +
            " I.CD_ANOEPI  < '" + panHasta.getCodAno() + "') or " +
            "  (I.CD_ANOEPI = '" + panHasta.getCodAno() + "' and " +
            " I.CD_SEMEPI <= '" + panHasta.getCodSem() + "')) " +
            " order by  I.NM_EDO, I.CD_ANOEPI, I.CD_SEMEPI ";
        break;
      }
    }
    // Campos
    paquete.put("CAMPOS", descripcionCampos);
    paquete.put("CONSULTA", strConsulta);

    return paquete;
  }

  /**
   * Indicar� los campos y la longitud de cada de uno de ellos para
   * escribirlos en el fichero de texto
   */

  /*
   * M�todo : PanelVolResLab.rellenarVector(int Origen)
   *   Autor    Fecha        Accion
   * JRM&AIC  24/03/2000   La implementa
   */
  void rellenarVector(int Origen) {
    vCampos = new Vector();
    switch (Origen) {
      case MUESTRAS: {
        descripCampos = new DataCampos("REGISTRO", 9);
        vCampos.addElement(descripCampos);
        descripCampos = new DataCampos("FC_INIRTBC", 10);
        vCampos.addElement(descripCampos);
        descripCampos = new DataCampos("NM_RESLAB", 6);
        vCampos.addElement(descripCampos);
        descripCampos = new DataCampos("FC_MUESTRA", 10);
        vCampos.addElement(descripCampos);
        descripCampos = new DataCampos("CD_MUESTRA", 2);
        vCampos.addElement(descripCampos);
        descripCampos = new DataCampos("CD_TTCLAB", 2);
        vCampos.addElement(descripCampos);
        descripCampos = new DataCampos("CD_VMUESTRA", 1);
        vCampos.addElement(descripCampos);
        descripCampos = new DataCampos("CD_TMICOB", 1);
        vCampos.addElement(descripCampos);
        descripCampos = new DataCampos("IT_RESISTENTE", 1);
        vCampos.addElement(descripCampos);
        break;
      }

      case RESISTENCIA: {
        descripCampos = new DataCampos("REGISTRO", 9);
        vCampos.addElement(descripCampos);
        descripCampos = new DataCampos("FC_INIRTBC", 10);
        vCampos.addElement(descripCampos);
        descripCampos = new DataCampos("NM_RESLAB", 6);
        vCampos.addElement(descripCampos);
        descripCampos = new DataCampos("CD_ESTREST", 2);
        break;
      }
    }
  }

  public void Inicializar() {
  }

//_____IMPLEMENTACION INTERFAZ CInicializar_____________________________________
  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        panDesde.Inicializate(CInicializar.ESPERA);
        panHasta.Inicializate(CInicializar.ESPERA);
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        panDesde.Inicializate(CInicializar.NORMAL);
        panHasta.Inicializate(CInicializar.NORMAL);
        this.setEnabled(true);
    }
  }

//_____IMPLEMENTACION INTERFAZ CONTPANANOSEMFECHA_______________________________

  /* Para obtener el applet*/
  public CApp getMiCApp() {
    return this.app;
  };

  /* M�todos ser�n llamados desde el PanAnoSemFecha despu�s de que el propio PanAnoSemFecha
   * ejecute su codigo en la p�rdida de foco */

  public void alPerderFocoAno() {}

  public void alPerderFocoSemana() {}

  public void alPerderFocoFecha() {}

  /**
   * Comprobaci�n de datos.
   */

  /*
   * M�todo : PanelVolResLab.ValidarCampos()
   *   Autor    Fecha        Accion
   * JRM&AIC  24/03/2000   La implementa
   */
  boolean ValidarCampos() {
    if ( (panDesde.getCodAno().equals("")) ||
        (panDesde.getCodSem().equals("")) ||
        (panDesde.getFecSem().equals("")) ||
        (panHasta.getCodAno().equals("")) ||
        (panHasta.getCodSem().equals("")) ||
        (panHasta.getFecSem().equals("")) ||
        (panFicheroM.txtFile.getText().length() == 0) ||
        (panFicheroR.txtFile.getText().length() == 0)) {
      this.getApp().showAdvise(
          "Se deben rellenar todos los campos obligatoriamente");
      return false;
    }

    if (panFicheroM.txtFile.getText().equals(panFicheroR.txtFile.getText())) {
      this.getApp().showAdvise(
          "Los ficheros deben de salida deber ser distintos");
      return false;
    }

    return comprobarRangoFechas();
  }

  /**
   * Comprueba que la fecha l�mite hasta sea mayor que la de desde.
   */

  /*
   * M�todo : PanelVolResLab.comprobarRangoFechas()
   *   Autor    Fecha        Accion
   * JRM&AIC  24/03/2000   La implementa
   */
  boolean comprobarRangoFechas() {
    boolean b;
    int sAnoDesde = Integer.parseInt(panDesde.getCodAno());
    int sSemDesde = Integer.parseInt(panDesde.getCodSem());
    int sAnoHasta = Integer.parseInt(panHasta.getCodAno());
    int sSemHasta = Integer.parseInt(panHasta.getCodSem());
    if (sAnoHasta > sAnoDesde) {
      b = true;
    }
    else {
      if (sAnoHasta < sAnoDesde) {
        this.getApp().showAdvise(
            "El a�o l�mite superior es menor que el inferior");
        b = false;
      }
      else {
        if (sSemHasta < sSemDesde) {
          this.getApp().showAdvise(
              "La semana l�mite superior es menor que la inferior para un mismo a�o");
          b = false;
        }
        else {
          b = true;
        }
      }
    }
    return b;
  }

  /**
   * Click en el bot�n Aceptar del panel
   */

  /*
   * M�todo : PanelVolResLab.bAceptar_OnClick(ActionEvent e)
   *   Autor    Fecha        Accion
   * JRM&AIC  24/03/2000   La implementa
   */
  void bAceptar_OnClick(ActionEvent e) {
    if (ValidarCampos()) {
      //1� llamar al servlet y mandar ejecutar la operacion->dev lista
      Inicializar(CInicializar.ESPERA);
      CrearFichero(MUESTRAS);
      CrearFichero(RESISTENCIA);

      Inicializar(CInicializar.NORMAL);
    } //end if isDataValid
  }

  /**
   * Se encarga de comunicar con el servlet y con los datos que le pasa
   * construir los ficheros resultado.
   */

  /*
   * M�todo : PanelVolResLab.CrearFichero(int Origen)
   *   Autor    Fecha        Accion
   * JRM&AIC  24/03/2000   La implementa
   */
  void CrearFichero(int Origen) {
    final String servlet = constantes.strSERVLET_SQLTUB; // "servlet/ServSQLTub";
    //final String servlet = "servlet/SrvExportaciones";
    Lista lResultado = null;
    Data dtDatos = new Data();
    Lista lDatos = new Lista();
    rellenarVector(Origen);
    dtDatos = paqueteConsulta(Origen);
    lDatos.addElement(dtDatos);
    String localiz;
    try {
      //establece la url del servlet al que vamos a consultar.
      getApp().getStub().setUrl(servlet);
      lResultado = (Lista) getApp().getStub().doPost(1, lDatos);
      //tuberculosis.servidor.voltratamientos.SrvSQLTub miServ =
      //   new tuberculosis.servidor.voltratamientos.SrvSQLTub();
      //Indica como conectarse a la b.datos
      //miServ.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
      //                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
      //                 "sive_desa",
      //                "sive_desa");

      //lResultado = (Lista) miServ.doDebug(2, lDatos);

    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      ex.printStackTrace();
    }
    //mandar escribir el fichero
    if (Origen == MUESTRAS) {
      localiz = this.panFicheroM.txtFile.getText();
    }
    else {
      localiz = this.panFicheroR.txtFile.getText();

    }
    EditorFichero edFic = new EditorFichero();
    edFic.escribirFichero(vCampos, getApp(), lResultado, localiz);
  }

} // class panelExportaciones

/**
 * Manejador de eventos para el panel.
 *
 * @author JRM&AIC
 * @version  1.1 24/03/2000
 */
/*
 * Clase : tuberculosis.cliente.volTratamientos.Eventos
 *   Autor    Fecha        Accion
 * JRM&AIC  24/03/2000   La implementa
 */
class Eventos
    implements java.awt.event.ActionListener {
  PanelVolResLab generador;

  Eventos(PanelVolResLab generador) {
    this.generador = generador;
  }

  public void actionPerformed(ActionEvent e) {
    generador.bAceptar_OnClick(e);
  }
}
