package tuberculosis.cliente.diaprot;

//import java.net.*;
//import java.awt.*;
//import java.awt.event.*;
//import com.borland.jbcl.control.*;
//import com.borland.jbcl.layout.*;
//import capp.*;
//import sapp.*;
//import java.util.*;
//import notindiv.*;
//import bidial.*;

// configuración del árbol
class Nodo {
  protected int y;
  protected String s;

  public Nodo(String s, int y) {
    this.y = y;
    this.s = s;
  }

  String getDescripcion() {
    return s;
  }

  int getY() {
    return y;
  }
}
