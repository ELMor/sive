package tuberculosis.cliente.voltratamientos;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CFileName;
import capp2.CInicializar;
import capp2.CPanel;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
//BDatos
//PanAnoSemFecha
import tuberculosis.cliente.componentes.ContPanAnoSemFecha;
import tuberculosis.cliente.componentes.PanAnoSemFecha;
//EditorFicheros
import util_ficheros.DataCampos;
import util_ficheros.EditorFichero;

/**
 * Exportaciones tuberculosis
 *
 * @author        AIC&JRM
 * @version       %I%,%G%
 */
public class PanelVolTratamientos
    extends CPanel
    implements CInicializar, ContPanAnoSemFecha {
  XYLayout xyLayout = new XYLayout();

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;

//constantes tama�o campos
  final public int tamCDENFCIE = 6;
  final public int tamCDPCENTI = 6;
  final public int tamCDMEDCEN = 6;
  final public int tamCDANOEPI = 4;
  final public int tamCDSEMEPI = 2;
  final public int tamNMCASOSDEC = 2;

  //componentes q conforman el panel
  Label lDesde = null;
  Label lHasta = null;
  PanAnoSemFecha panDesde = null;
  PanAnoSemFecha panHasta = null;
  ButtonControl btnAceptar = null;
  CFileName panFichero = null; ;

  Vector vCampos = null;
  Vector descripcionCampos = null;

  /**
   * Constructor de la clase
   *
   * @author JRM@AIC
   * @version %I%,%G%
   */
  public PanelVolTratamientos(CApp Contenedor) {
    super(Contenedor);
    try {
      lDesde = new Label();
      lHasta = new Label();
      panDesde = new PanAnoSemFecha(this, panDesde.modoVACIO, false);
      panHasta = new PanAnoSemFecha(this, panHasta.modoVACIO, false);
      panFichero = new capp2.CFileName(Contenedor);
      btnAceptar = new ButtonControl();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Coloca los controles en el panel
   *
   * @author JRM@AIC
   * @version %I%,%G%
   */
  void jbInit() throws Exception {
    final String imgAceptar = "images/aceptar.gif";

    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().CargaImagenes();

    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnAceptar.setLabel("Aceptar");
    btnAceptar.addActionListener(new Eventos(this));

    xyLayout.setHeight(300);
    xyLayout.setWidth(300);
    this.setLayout(xyLayout);

    lDesde.setText("Desde:");
    lHasta.setText("Hasta:");

    // A�adimos los escuchadores...
//    btnCancelar.addActionListener(new PanExpEnfermDeclar_btnCancelar_actionAdapter(this));

    this.add(lDesde, new XYConstraints(MARGENIZQ, MARGENSUP, 110, ALTO));
    this.add(panDesde,
             new XYConstraints(MARGENIZQ + 110 + INTERHOR, MARGENSUP, 515, 34));
    this.add(lHasta,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 110,
                               ALTO));
    this.add(panHasta,
             new XYConstraints(MARGENIZQ + 110 + INTERHOR,
                               MARGENSUP + ALTO + INTERVERT, 515, 34));
    this.add(panFichero,
             new XYConstraints(MARGENIZQ - 13,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 515, 38));
    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 70 + 515 - 2 * 88 - 20,
                               MARGENSUP + 3 * ALTO + 4 * INTERVERT, 88, 29));

  }

  /**
   * Genera una estructura data con la consulta y los nombres
   * de los campos para enviarla al servlet
   *
   * @author        AIC&JRM
   * @version       %I%,%G%
   * @return        Valores empaquetados en un Data.
   */
  Data paqueteConsulta() {
    Data paquete = new Data();
    String strConsulta = null;

    int anyoIni = Integer.parseInt(panDesde.getCodAno());
    int anyoFin = Integer.parseInt(panHasta.getCodAno());
    int semanaIni = Integer.parseInt(panDesde.getCodSem());
    int semanaFin = Integer.parseInt(panDesde.getCodSem());

    descripcionCampos = new Vector();
    descripcionCampos.addElement("NM_EDO");
    descripcionCampos.addElement("CD_ANOEPI");
    descripcionCampos.addElement("CD_SEMEPI");
    descripcionCampos.addElement("REGISTRO");
    descripcionCampos.addElement("FC_INIRTBC");
    descripcionCampos.addElement("FC_SALRTBC");
    descripcionCampos.addElement("CD_MOTRATINI");
    descripcionCampos.addElement("FC_INITRAT");
    descripcionCampos.addElement("CD_MOTRATFIN");
    descripcionCampos.addElement("FC_FINTRAT");
    descripcionCampos.addElement("FC_INITRAT");

    // Construimos la select. Ponemos nvl para que el servlet no
    // devuelva null en el campo sino ' '
    strConsulta = " Select I.NM_EDO, I.CD_ANOEPI, I.CD_SEMEPI, " +
        " R.CD_ARTBC||R.CD_NRTBC as REGISTRO, " +
        " nvl(TO_CHAR(R.FC_INIRTBC,'DD/MM/YYYY'),' ') as FC_INIRTBC, " +
        " nvl(TO_CHAR(R.FC_SALRTBC,'DD/MM/YYYY'),' ') as FC_SALRTBC, " +
        " T.CD_MOTRATINI, " +
        " nvl(TO_CHAR(T.FC_INITRAT,'DD/MM/YYYY'),' ') as FC_INITRAT, " +
        " T.CD_MOTRATFIN, " +
        " nvl(TO_CHAR(t.FC_FINTRAT,'DD/MM/YYYY'),' ') as FC_FINTRAT " +
        " from SIVE_EDOIND I, SIVE_REGISTROTBC R, " +
        " SIVE_TRATAMIENTOS T " +
        " where I.NM_EDO = R.NM_EDO and " +
        " I.NM_EDO = T.NM_EDO and " +
        " ((I.CD_ANOEPI = '" + panDesde.getCodAno() + "' and " +
        " I.CD_SEMEPI >= '" + panDesde.getCodSem() + "') or " +
        "  (I.CD_ANOEPI > '" + panDesde.getCodAno() + "' and " +
        " I.CD_ANOEPI  < '" + panHasta.getCodAno() + "') or " +
        "  (I.CD_ANOEPI = '" + panHasta.getCodAno() + "' and " +
        " I.CD_SEMEPI <= '" + panHasta.getCodSem() + "')) " +
        " order by  NM_EDO, CD_ANOEPI, CD_SEMEPI ";
    // Campos
    paquete.put("CAMPOS", descripcionCampos);
    paquete.put("CONSULTA", strConsulta);

    return paquete;
  }

  /**
   * Indicar� los campos y la longitud de cada de uno de ellos para
   * escribirlos en el fichero de texto
   *
   * @author        AIC&JRM
   * @version       %I%,%G%
   */
  void rellenarVector() {
    vCampos = new Vector();
    DataCampos descripCampos = new DataCampos("NM_EDO", 6);
    vCampos.addElement(descripCampos);
    descripCampos = new DataCampos("CD_ANOEPI", 4);
    vCampos.addElement(descripCampos);
    descripCampos = new DataCampos("CD_SEMEPI", 2);
    vCampos.addElement(descripCampos);
    descripCampos = new DataCampos("REGISTRO", 9);
    vCampos.addElement(descripCampos);
    descripCampos = new DataCampos("FC_INIRTBC", 10);
    vCampos.addElement(descripCampos);
    descripCampos = new DataCampos("FC_SALRTBC", 10);
    vCampos.addElement(descripCampos);
    descripCampos = new DataCampos("CD_MOTRATINI", 1);
    vCampos.addElement(descripCampos);
    descripCampos = new DataCampos("FC_INITRAT", 10);
    vCampos.addElement(descripCampos);
    descripCampos = new DataCampos("CD_MOTRATFIN", 1);
    vCampos.addElement(descripCampos);
    descripCampos = new DataCampos("FC_FINTRAT", 10);
    vCampos.addElement(descripCampos);
    descripCampos = new DataCampos("FC_INITRAT", 10);
    vCampos.addElement(descripCampos);

  }

  public void Inicializar() {
  }

//_____IMPLEMENTACION INTERFAZ CInicializar_____________________________________
  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        panDesde.Inicializate(CInicializar.ESPERA);
        panHasta.Inicializate(CInicializar.ESPERA);
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        panDesde.Inicializate(CInicializar.NORMAL);
        panHasta.Inicializate(CInicializar.NORMAL);
        this.setEnabled(true);
    }
  }

//_____IMPLEMENTACION INTERFAZ CONTPANANOSEMFECHA_______________________________

  /* Para obtener el applet*/
  public CApp getMiCApp() {
    return this.app;
  };

  /* M�todos ser�n llamados desde el PanAnoSemFecha despu�s de que el propio PanAnoSemFecha
   * ejecute su codigo en la p�rdida de foco */

  public void alPerderFocoAno() {}

  public void alPerderFocoSemana() {}

  public void alPerderFocoFecha() {}

  /**
   * Comprobaci�n de datos.
   *
   * @author JRM&AIC
   * @version %I%,%G%
   */
  boolean ValidarCampos() {
    if ( (panDesde.getCodAno().equals("")) ||
        (panDesde.getCodSem().equals("")) ||
        (panDesde.getFecSem().equals("")) ||
        (panHasta.getCodAno().equals("")) ||
        (panHasta.getCodSem().equals("")) ||
        (panHasta.getFecSem().equals("")) ||
        (panFichero.txtFile.getText().length() == 0)) {
      this.getApp().showAdvise(
          "Se deben rellenar todos los campos obligatoriamente");
      return false;
    }
    else {
      return comprobarRangoFechas();
    }
  }

  /**
   * Comprueba que la fecha l�mite hasta sea mayor que la de desde.
   *
   * @author JRM&AIC
   * @version %I%,%G%
   */
  boolean comprobarRangoFechas() {
    boolean b;
    int sAnoDesde = Integer.parseInt(panDesde.getCodAno());
    int sSemDesde = Integer.parseInt(panDesde.getCodSem());
    int sAnoHasta = Integer.parseInt(panHasta.getCodAno());
    int sSemHasta = Integer.parseInt(panHasta.getCodSem());
    if (sAnoHasta > sAnoDesde) {
      b = true;
    }
    else {
      if (sAnoHasta < sAnoDesde) {
        this.getApp().showAdvise(
            "El a�o l�mite superior es menor que el inferior");
        b = false;
      }
      else {
        if (sSemHasta < sSemDesde) {
          this.getApp().showAdvise(
              "La semana l�mite superior es menor que la inferior para un mismo a�o");
          b = false;
        }
        else {
          b = true;
        }
      }
    }
    return b;
  }

  /**
   * Click en el bot�n Aceptar del panel
   *
   * @author JRM&AIC
   * @version %I%,%G%
   */
  void bAceptar_OnClick(ActionEvent e) {
    final String servlet = constantes.strSERVLET_SQLTUB; // "servlet/ServSQLTub";
    //final String servlet = "servlet/SrvExportaciones";
    Lista lResultado = null;
    Data dtDatos = new Data();
    Lista lDatos = new Lista();
    if (ValidarCampos()) {
      //1� llamar al servlet y mandar ejecutar la operacion->dev lista
      Inicializar(CInicializar.ESPERA);
      rellenarVector();
      dtDatos = paqueteConsulta();
      lDatos.addElement(dtDatos);
      try {
        //establece la url del servlet al que vamos a consultar.
        getApp().getStub().setUrl(servlet);
        //lResultado = (Lista)getApp().getStub().doPost(1,lDatos);

        ///////////////77
        //BDatos.ejecutaSQL(false,this.getApp(),servlet,1,lDatos);
        tuberculosis.servidor.voltratamientos.SrvSQLTub miServ = new
            tuberculosis.servidor.voltratamientos.SrvSQLTub();

        //Indica como conectarse a la b.datos
        miServ.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                  "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                  "sive_desa",
                                  "sive_desa");

        lResultado = (Lista) miServ.doDebug(2, lDatos);

      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
        ex.printStackTrace();
      }
      Inicializar(CInicializar.NORMAL);
      //mandar escribir el fichero
      String localiz = this.panFichero.txtFile.getText();
      EditorFichero edFic = new EditorFichero();
      edFic.escribirFichero(vCampos, getApp(), lResultado, localiz);
    } //end if isDataValid
  }
} // class panelExportaciones

/**
 * Eventos
 *
 * @author        AIC&JRM
 * @version       %I%,%G%
 */
class Eventos
    implements java.awt.event.ActionListener {
  PanelVolTratamientos generador;

  Eventos(PanelVolTratamientos generador) {
    this.generador = generador;
  }

  public void actionPerformed(ActionEvent e) {
    generador.bAceptar_OnClick(e);
  }
}
