/**
 * Clase: PanMues
 * Paquete: tuberculosis.cliente.panmues
 * Hereda: CPanel
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 19/11/1999
 * Descripcion: Implementacion del panel que ense�a una tabla con las
 *   muestras asociadas a un notificador. Desde este panel
 *   se puede acceder al alta/modificacion/baja de muestras.
 */

package tuberculosis.cliente.panmues;

import java.util.Hashtable;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CPanel;
import capp.CTabla;
import comun.Common;
import comun.Comunicador;
import comun.constantes;
import jclass.util.JCVector;
import sapp.StubSrvBD;
//para dar altas/bajas/modif
import tuberculosis.cliente.diamues.DiaMues;
import tuberculosis.cliente.diaresul.DiaOtrosMuestra;
import tuberculosis.datos.panmues.DatCasMuesCS;
import tuberculosis.datos.panmues.DatCasMuesSC;

public class PanMues
    extends CPanel {

  // Applet asociado a este panel: SOLO DESARROLLO
  CApp appMues;

  // Hashtable de entrada
  Hashtable hsMues = null;

  // Lista de los parametros de busqueda
  public CLista listaBusqueda = null;

  // Lista de Muestras asociadas a un Notificador
  public CLista listaMues = null;

  // Lista de Tecnicas de Laboratorio
  public CLista LTLab = null;

  // Lista Muestras Laboratorio
  public CLista LMuesLab = null;

  // Lista Valores Muestras
  public CLista LVMues = null;

  // Lista Tipos de Micobacterias
  public CLista LTMicob = null;

  // Lista Estudios de Resistencias
  public CLista LEstRes = null;

  // Sincronizaci�n de eventos
  protected boolean sinBloquear = true;
  protected int modoAnterior = 0;

  // Variable estado del panel
  protected int modoOperacion = constantes.modoALTA;
  protected int modoEntrada = constantes.modoALTA;

//  final String strSERVLET_MUES = "servlet/SrvCasMues";
  final String strSERVLET_MUES = constantes.strSERVLET_MUES;

//  final String strSERVLET_RESIS = "servlet/SrvCasResis"; // E 27/01/2000
  final String strSERVLET_RESIS = constantes.strSERVLET_RESIS;

  // Cargadores de im�genes visibles en todo el m�dulo
  private CCargadorImagen imgs = null;

  final String imgNAME_mantenimiento[] = {
      constantes.imgALTA2,
      constantes.imgMODIFICAR2,
      constantes.imgBAJA2};

  final String imgNAME_tabla[] = {
      constantes.imgPRIMERO, constantes.imgANTERIOR,
      constantes.imgSIGUIENTE, constantes.imgULTIMO};

  /******************** Componentes **********************/

  // Organizacion del panel
  XYLayout lyXYLayout = new XYLayout();

  // Controles para la navegacion entre los elementos de la tabla
  ButtonControl btnAlta = null;
  ButtonControl btnModificar = null;
  ButtonControl btnBaja = null;
  ButtonControl btnPrimero = null;
  ButtonControl btnAnterior = null;
  ButtonControl btnSiguiente = null;
  ButtonControl btnUltimo = null;

  // Tabla que visualiza y permite navegar entre los tratamientos
  public CTabla tabla = new CTabla();

  public DiaOtrosMuestra dlgOtrosM = null;

  // Constructor
  // Debe ir el panel en vez de Capp
  public PanMues(CApp a, int modo, Hashtable hsEntrada,
                 DatCasMuesCS dataBusq, DiaOtrosMuestra dlgOtros) {
    try {
      appMues = a;
      setApp(a);
      hsMues = hsEntrada;
      dlgOtrosM = dlgOtros;

      // Lista de Tecnicas de Laboratorio
      LTLab = (CLista) hsMues.get(constantes.TIPO_TECNICALAB);

      // Lista Muestras Laboratorio
      LMuesLab = (CLista) hsMues.get(constantes.MUESTRA_LAB);

      // Lista Valores Muestras
      LVMues = (CLista) hsMues.get(constantes.VALOR_MUESTRA);

      // Lista Tipos de Micobacterias
      LTMicob = (CLista) hsMues.get(constantes.TMICOBACTERIA);

      // Lista Estudios de Resistencias
      LEstRes = (CLista) hsMues.get(constantes.ESTUDIORESIS);

      // Datos de busqueda
      listaBusqueda = new CLista();
      listaBusqueda.addElement(dataBusq);

      // Inicializacion
      modoOperacion = modo;
      modoEntrada = modo;
      jbInit();
      if (modoEntrada == constantes.modoALTA) {
        listaMues = new CLista();
      }
      else {
        listaMues = buscarDatos();

      }
      escribirTabla();

      Inicializar();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicializacion en la creacion de un objeto
  void jbInit() throws Exception {
    // Carga de imagenes
    CCargadorImagen imgs_mantenimiento = new CCargadorImagen(app,
        imgNAME_mantenimiento);
    imgs_mantenimiento.CargaImagenes();
    CCargadorImagen imgs_tabla = new CCargadorImagen(app, imgNAME_tabla);
    imgs_tabla.CargaImagenes();

    // Medidas del panel
    this.setSize(new Dimension(780, 315));
    lyXYLayout.setHeight(315);
    lyXYLayout.setWidth(780);
    this.setLayout(lyXYLayout);
    this.setBorde(false);

    // Botones de mantenimiento de la tabla
    btnAlta = new ButtonControl(imgs_mantenimiento.getImage(0));
    btnModificar = new ButtonControl(imgs_mantenimiento.getImage(1));
    btnBaja = new ButtonControl(imgs_mantenimiento.getImage(2));
    this.add(btnAlta, new XYConstraints(20, 155, 25, 25));
    this.add(btnModificar, new XYConstraints(50, 155, 25, 25));
    this.add(btnBaja, new XYConstraints(80, 155, 25, 25));

    // Botones de navegacion por la tabla
    btnPrimero = new ButtonControl(imgs_tabla.getImage(0));
    btnAnterior = new ButtonControl(imgs_tabla.getImage(1));
    btnSiguiente = new ButtonControl(imgs_tabla.getImage(2));
    btnUltimo = new ButtonControl(imgs_tabla.getImage(3));
    this.add(btnPrimero, new XYConstraints(505 + 105, 155, 25, 25));
    this.add(btnAnterior, new XYConstraints(545 + 105, 155, 25, 25));
    this.add(btnSiguiente, new XYConstraints(583 + 105, 155, 25, 25));
    this.add(btnUltimo, new XYConstraints(621 + 105, 155, 25, 25));

    // Se establecen los parametros de la tabla
    pintarTabla();
    this.add(tabla, new XYConstraints(15, 15, 755, 120));

    // Escuchadores: Botones de navegacion
    btnTablaActionListener alBotonesTabla = new btnTablaActionListener(this);
    btnPrimero.setActionCommand("primero");
    btnPrimero.addActionListener(alBotonesTabla);
    btnAnterior.setActionCommand("anterior");
    btnAnterior.addActionListener(alBotonesTabla);
    btnSiguiente.setActionCommand("siguiente");
    btnSiguiente.addActionListener(alBotonesTabla);
    btnUltimo.setActionCommand("ultimo");
    btnUltimo.addActionListener(alBotonesTabla);

    // Escuchadores: Botones Mantenimiento
    btnAMBActionListener alBotonesAMB = new btnAMBActionListener(this);
    btnAlta.setActionCommand("alta");
    btnAlta.addActionListener(alBotonesAMB);
    btnModificar.setActionCommand("modificar");
    btnModificar.addActionListener(alBotonesAMB);
    btnBaja.setActionCommand("baja");
    btnBaja.addActionListener(alBotonesAMB);

    // Tabla de tratamientos
    jcalTablaActionListener jcalTabla = new jcalTablaActionListener(this);
    tabla.addActionListener(jcalTabla);

  }

  // Bloquear eventos
  /** Este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized boolean bloquea() {
    if (sinBloquear) {
      // No hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modoOperacion;
      modoOperacion = constantes.modoESPERA;
      dlgOtrosM.Inicializar(modoOperacion);

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // Ya est� bloqueado
      return false;
    }
  }

  /** Este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    modoOperacion = modoAnterior;
    dlgOtrosM.Inicializar(modoOperacion);
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // Para devolver los datos del Notificador actualizado
  public DatCasMuesCS getComponente() {
    return (DatCasMuesCS) listaBusqueda.elementAt(0);
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar()

  // Implementacion del metodo abstracto de CPanel
  //  modoALTA: habilitado solo el boton de alta
  //  modoMODIFICACION: habilitados los 3 botones
  //  modoBAJA: habilitado solo el boton de baja
  //  modoCONSULTA: habilitado solo el boton de modificacion
  public void Inicializar() {
    boolean state;
    switch (modoOperacion) {
      case constantes.modoESPERA:
        tabla.setEnabled(false);
        btnAlta.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBaja.setEnabled(false);
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
      case constantes.modoALTA:
        if (tabla.countItems() > 0) {
          state = true;
        }
        else {
          state = false;
        }
        tabla.setEnabled(state);
        btnAlta.setEnabled(true);
        btnModificar.setEnabled(false);
        btnBaja.setEnabled(false);
        btnPrimero.setEnabled(state);
        btnAnterior.setEnabled(state);
        btnSiguiente.setEnabled(state);
        btnUltimo.setEnabled(state);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
      case constantes.modoMODIFICACION:
        if (tabla.countItems() > 0) {
          state = true;
        }
        else {
          state = false;
        }
        tabla.setEnabled(state);
        btnAlta.setEnabled(true);
        btnModificar.setEnabled(state);
        btnBaja.setEnabled(state);
        btnPrimero.setEnabled(state);
        btnAnterior.setEnabled(state);
        btnSiguiente.setEnabled(state);
        btnUltimo.setEnabled(state);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
      case constantes.modoBAJA:
        if (tabla.countItems() > 0) {
          state = true;
        }
        else {
          state = false;
        }
        tabla.setEnabled(state);
        btnAlta.setEnabled(false);
        btnModificar.setEnabled(state); // E 27/01/2000
        btnBaja.setEnabled(false); // E 27/01/2000
        btnPrimero.setEnabled(state);
        btnAnterior.setEnabled(state);
        btnSiguiente.setEnabled(state);
        btnUltimo.setEnabled(state);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
      case constantes.modoCONSULTA:
        if (tabla.countItems() > 0) {
          state = true;
        }
        else {
          state = false;
        }
        tabla.setEnabled(state);
        btnAlta.setEnabled(false);
        btnModificar.setEnabled(state); // E 27/01/2000
        btnBaja.setEnabled(false);
        btnPrimero.setEnabled(state);
        btnAnterior.setEnabled(state);
        btnSiguiente.setEnabled(state);
        btnUltimo.setEnabled(state);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
    this.doLayout();
  }

  // Establece el numero de columnas de la tabla y las cabeceras de las mismas
  public void pintarTabla() {
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "95\n180\n180\n100\n180"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(
        "Fecha Muestra\nMuestra\nTipo T�cnica\nValor Muestra\nTipo Micobacteria"),
        '\n'));
    tabla.setNumColumns(5);
  } //fin PintarTabla()

  // A partir de los datos de un notificador DatCasMuesCS, devuelve una lista
  //  con las muestras asociadas al notificador (lista de DatCasMuesSC).
  public CLista buscarDatos() {
    CLista parametros = listaBusqueda;
    CLista listaSalida = null;
    StubSrvBD stubCliente = new StubSrvBD();

    try {
      listaSalida = Comunicador.Communicate(this.app,
                                            stubCliente,
                                            0,
                                            strSERVLET_MUES,
                                            parametros);

      if (listaSalida != null) {
        if (listaSalida.size() > 0) {
          // Se devuelven la lista de muestras
          return listaSalida;
        }
        else {
          //comun.ShowWarning(getApp(),"No existen muestras asociadas.");
          return listaSalida;
        }
      }
      else {
        Common.ShowWarning(getApp(),
            "Error al recuperar los resultados de lab. asociados al notificador.");
        return null;
      }
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      Common.ShowWarning(this.getApp(),
          "Error al recuperar los resultados de lab. asociados al notificador.");
      return null;
    }
  } // Fin buscarDatos()

  // A partir de los datos de una muestra DatCasMuesSC, devuelve una lista
  //  con sus resistencias asociadas (lista de DatResis).
  public CLista buscarResistencias(DatCasMuesSC m) {
    CLista parametros = new CLista();
    parametros.addElement(m);
    CLista listaSalida = null;
    StubSrvBD stubCliente = new StubSrvBD();

    try {
      listaSalida = Comunicador.Communicate(this.app,
                                            stubCliente,
                                            0,
                                            strSERVLET_RESIS,
                                            parametros);

      if (listaSalida != null) {
        return listaSalida;
      }
      else {
        Common.ShowWarning(getApp(),
            "Error al recuperar las resistencias asociadas a los resultados.");
        return null;
      }
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      Common.ShowWarning(this.getApp(),
          "Error al recuperar las resistencias asociadas a los resultados.");
      return null;
    }
  } // Fin buscarResistencias()

  // Implementaci�n del m�todo de rellenado
  public void escribirTabla() {

    if (listaMues == null) {
      resetearTabla();
      return;
    }
    if (listaMues.size() <= 0) {
      resetearTabla();
      return;
    }

    // Fila
    JCVector row = null;
    // Matriz de filas
    JCVector items = new JCVector();

    // Reseteo de la tabla
    resetearTabla();

    // Datos de una fila de la tabla
    DatCasMuesSC dataResult = null;

    for (int i = 0; i < listaMues.size(); i++) {
      dataResult = (DatCasMuesSC) listaMues.elementAt(i);

      // Nuevo JCVector
      row = new JCVector();

      // Fecha Muestra
      row.addElement(dataResult.getFC_MUESTRA().trim());

      // Muestra Laboratorio
      String cdMLab = dataResult.getCD_MUESTRA().trim();
      String dsMLab = dataResult.getDS_MUESTRA().trim();
      if (!cdMLab.equals("")) {
        row.addElement(dsMLab);
      }
      else {
        row.addElement("");

        // T�cnica de Laboratorio
      }
      String cdTLab = dataResult.getCD_TTECLAB().trim();
      String dsTLab = dataResult.getDS_TTECLAB().trim();
      if (!cdTLab.equals("")) {
        row.addElement(dsTLab);
      }
      else {
        row.addElement("");

        // Valor Muestra
      }
      String cdVM = dataResult.getCD_VMUESTRA().trim();
      String dsVM = dataResult.getDS_VMUESTRA().trim();
      if (!cdVM.equals("")) {
        row.addElement(dsVM);
      }
      else {
        row.addElement("");

        // Tipo Micobacteria
      }
      String cdTMico = dataResult.getCD_TMICOB().trim();
      String dsTMico = dataResult.getDS_TMICOB().trim();
      if (!cdTMico.equals("")) {
        row.addElement(dsTMico);
      }
      else {
        row.addElement("");

        // Se a�ade una fila a la matriz
      }
      items.addElement(row);
    } // Fin for()

    // Se establecen los  items
    tabla.setItems(items);

    // Se selecciona el primer registro de la tabla
    tabla.select(0);
    tabla.setTopRow(0);

    // Se repinta la tabla
    tabla.repaint();
  }

  public void resetearTabla() {
    tabla.clear();
    tabla.repaint();
    btnModificar.setEnabled(false);
    btnPrimero.setEnabled(false);
    btnAnterior.setEnabled(false);
    btnSiguiente.setEnabled(false);
    btnUltimo.setEnabled(false);
  } // Fin resetearTabla()

  /********************* Manejadores ************************/

  // Manejador btnPrimero
  protected void btnTablaPrimero() {
    if (tabla.countItems() > 0) {
      tabla.select(0);
      tabla.setTopRow(0);
    }
  }

  // Manejador btnAnterior
  protected void btnTablaAnterior() {
    if (tabla.countItems() > 0) {
      int index = tabla.getSelectedIndex();
      if (index > 0) {
        index--;
        tabla.select(index);
        if (index - tabla.getTopRow() <= 0) {
          tabla.setTopRow(tabla.getTopRow() - 1);
        }
      }
      else {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    }
  }

  // Manejador btnSiguiente
  protected void btnTablaSiguiente() {
    if (tabla.countItems() > 0) {
      int ultimo = tabla.countItems() - 1;
      int index = tabla.getSelectedIndex();

      if (index >= 0 && index < ultimo) {
        index++;
        tabla.select(index);
        if (index - tabla.getTopRow() >= 4) {
          tabla.setTopRow(tabla.getTopRow() + 1);
        }
      }
      else {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    }
  }

  // Manejador btnUltimo
  protected void btnTablaUltimo() {
    int index = 0;
    int ultimo = tabla.countItems() - 1;
    if (ultimo >= 0) {
      index = ultimo;
      tabla.select(index);
      if (tabla.countItems() >= 5) {
        tabla.setTopRow(tabla.countItems() - 5);
      }
      else {
        tabla.setTopRow(0);
      }
    }
  }

  // Manejador btnAlta
  protected void btnAlta() {
    // Relleno de los datos de un notificador
    DatCasMuesCS notif = (DatCasMuesCS) listaBusqueda.firstElement();

    // Relleno de los datos de una muestra (modoALTA)
    DatCasMuesSC mues = new DatCasMuesSC("", "", "", "", "", "", "", "", "", "",
                                         "", "", "");

    // No hay resistencias en el alta de una muestra
    CLista resis = new CLista();

    // Dialogo para dar de alta una nueva muestra
    DiaMues dlg = new DiaMues(this.getApp(), constantes.modoALTA,
                              construirHashCat(), notif, mues, resis);
    dlg.show();

    // Si ha habido exito en la insercion de una  nueva muestra
    //  se a�ade la misma al final de la tabla de muestras, se
    //  pinta la tabla de muestras y se actualiza el operario/fultact
    //  de la notificacion
    if (dlg.accionOK) {
      listaMues.addElement(dlg.muesNueva);
      escribirTabla();
      ( (DatCasMuesCS) listaBusqueda.firstElement()).setCD_OPE(dlg.muesNueva.
          getCD_OPE());
      ( (DatCasMuesCS) listaBusqueda.firstElement()).setFC_ULTACT(dlg.muesNueva.
          getFC_ULTACT());
    }

    dlg = null;
    return;
  } // Fin btnAlta()

  // Manejador btnModificar
  protected void btnModificar() {
    // Relleno de los datos de un notificador
    DatCasMuesCS notif = (DatCasMuesCS) listaBusqueda.firstElement();

    // Relleno de los datos de una muestra (modoMODIFICACION)
    DatCasMuesSC mues = (DatCasMuesSC) listaMues.elementAt(tabla.
        getSelectedIndex());

    // Buscamos las resistencias asociadas a la muestra
    CLista resis = buscarResistencias(mues);

    // Dialogo para modificar/consultar una muestra
    DiaMues dlg = null;
    if (modoEntrada == constantes.modoCONSULTA ||
        modoEntrada == constantes.modoBAJA) {
      dlg = new DiaMues(this.getApp(), constantes.modoCONSULTA,
                        construirHashCat(), notif, mues, resis);
    }
    else {
      dlg = new DiaMues(this.getApp(), constantes.modoMODIFICACION,
                        construirHashCat(), notif, mues, resis);

    }
    dlg.show();

    // Si ha habido exito en la modificacion de una muestra
    //  se sustituye la misma en la tabla de muestras, se
    //  pinta la tabla de muestras y se actualiza el operario/fultact
    //  de la notificacion
    if (dlg.accionOK) {
      listaMues.setElementAt(dlg.muesNueva, tabla.getSelectedIndex());
      escribirTabla();
      ( (DatCasMuesCS) listaBusqueda.firstElement()).setCD_OPE(dlg.muesNueva.
          getCD_OPE());
      ( (DatCasMuesCS) listaBusqueda.firstElement()).setFC_ULTACT(dlg.muesNueva.
          getFC_ULTACT());
    }

    dlg = null;
    return;
  }

  // Manejador btnBaja
  protected void btnBaja() {
    // Relleno de los datos de un notificador
    DatCasMuesCS notif = (DatCasMuesCS) listaBusqueda.firstElement();

    // Relleno de los datos de una muestra (modoBAJA)
    DatCasMuesSC mues = (DatCasMuesSC) listaMues.elementAt(tabla.
        getSelectedIndex());

    // Buscamos las resistencias asociadas a la muestra
    CLista resis = buscarResistencias(mues);

    // Dialogo para dar de baja una muestra
    DiaMues dlg = new DiaMues(this.getApp(), constantes.modoBAJA,
                              construirHashCat(), notif, mues, resis);
    dlg.show();

    // Si ha habido exito en la eliminacion de una muestra
    //  se elimina la misma de la tabla de muestras, se
    //  pinta la tabla de muestras y se actualiza el operario/fultact
    //  de la notificacion
    if (dlg.accionOK) {
      listaMues.removeElementAt(tabla.getSelectedIndex());
      escribirTabla();
      ( (DatCasMuesCS) listaBusqueda.firstElement()).setCD_OPE(dlg.muesNueva.
          getCD_OPE());
      ( (DatCasMuesCS) listaBusqueda.firstElement()).setFC_ULTACT(dlg.muesNueva.
          getFC_ULTACT());
    }

    dlg = null;
    return;
  }

  // Construye la hash con las listas catalogos par el dialogo Muetras
  Hashtable construirHashCat() {
    Hashtable res = new Hashtable();

    res.put("TIPO_TECNICALAB", LTLab);
    res.put("MUESTRA_LAB", LMuesLab);
    res.put("VALOR_MUESTRA", LVMues);
    res.put("TMICOBACTERIA", LTMicob);
    res.put("ESTUDIORESIS", LEstRes);

    return res;
  } // Fin construirHashCat()

  // Acciones sobre la tabla
  protected void tablaActionPerformed(jclass.bwt.JCActionEvent e) {
    switch (modoEntrada) {
      case constantes.modoALTA:

        // btnAlta();
        break;
      case constantes.modoMODIFICACION:
        btnModificar();
        break;
      case constantes.modoBAJA:
        btnBaja();
        break;
      case constantes.modoCONSULTA:
        btnModificar();
        break;
    }
  } // Fin tablaActionPerformed

} // endclass PanMues

// Gesti�n de eventos sobre los botones de la tabla
class btnTablaActionListener
    implements java.awt.event.ActionListener {
  PanMues adaptee;

  public btnTablaActionListener(PanMues ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      if (e.getActionCommand() == "primero") {
        adaptee.btnTablaPrimero();
      }
      else if (e.getActionCommand() == "anterior") {
        adaptee.btnTablaAnterior();
      }
      else if (e.getActionCommand() == "siguiente") {
        adaptee.btnTablaSiguiente();
      }
      else {
        adaptee.btnTablaUltimo();
      }

      adaptee.desbloquea();
    }
  }
} // endclass btnTablaActionListener

// Gesti�n de eventos sobre los botones A�ADIR, MODIFICAR, BORRAR
class btnAMBActionListener
    implements java.awt.event.ActionListener {
  PanMues adaptee;

  public btnAMBActionListener(PanMues ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      if (e.getActionCommand() == "alta") {
        adaptee.btnAlta();
      }
      else if (e.getActionCommand() == "modificar") {
        adaptee.btnModificar();
      }
      else if (e.getActionCommand() == "baja") {
        adaptee.btnBaja();
      }
      adaptee.desbloquea();
    }
  }
} // endclass btnAMBActionListener

class jcalTablaActionListener
    implements jclass.bwt.JCActionListener {
  PanMues adaptee;

  public jcalTablaActionListener(PanMues ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(jclass.bwt.JCActionEvent e) {
    if (adaptee.bloquea()) {
      adaptee.tablaActionPerformed(e);
      adaptee.desbloquea();
    }
  }
} // endclass jcalTablaActionListener
