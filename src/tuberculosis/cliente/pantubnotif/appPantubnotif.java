package tuberculosis.cliente.pantubnotif;

import java.net.URL; // pruebas
import java.util.Hashtable;

import capp.CApp;
import capp.CLista;
import comun.constantes;
import sapp.StubSrvBD;
import tuberculosis.datos.notiftub.DatCasosTub;
import tuberculosis.datos.notiftub.DatNotifTub;
import tuberculosis.servidor.notiftub.SrvNotifTub;

public class appPantubnotif
    extends CApp {

  // Para pruebas
  // Conexi�n con el servlet
  private StubSrvBD stubCliente;
  private CLista data = new CLista();
  private CLista resultado = null;
  private DatCasosTub dct = new DatCasosTub();

  private Hashtable hsEnvio = new Hashtable();

  int modoESPERA = constantes.modoESPERA;
  int modoALTA = constantes.modoALTA;
  int modoMODIFICACION = constantes.modoMODIFICACION;
  int modoBAJA = constantes.modoBAJA;
  int modoCONSULTA = constantes.modoCONSULTA;

  Hashtable hs = new Hashtable();

  public void init() {
    super.init();
    setTitulo("");

    // Pruebas
    try {
      dct.put("META_MODO", new Integer(SrvNotifTub.servletSELECT_SUCA));
      dct.put("CD_ARTBC", "1999");
      dct.put("CD_NRTBC", "00001");

      data.addElement(dct);

      stubCliente = new StubSrvBD();
//      stubCliente.setUrl(new URL (getURL() + "servlet/SrvNotifTub"));
      stubCliente.setUrl(new URL(getURL() + constantes.strSERVLET_TUBNOTIF));

      resultado = (CLista) stubCliente.doPost(constantes.modoMODIFICACION, data);
      hs.put("DATOS", resultado);

    }
    catch (Exception e) {
      System.out.println(e.toString());
    }
    finally {

    }
    // Fin Pruebas

  }

  public void start() {
    CApp a = (CApp)this;

    //este es el de prueba
    PanTablaNotif panel = null;
    //PanTablaNotif panel = new PanTablaNotif(a , modoMODIFICACION, null);

    // Pruebas
    resultado = null;
    resultado = (CLista) hs.get("DATOS");

    for (int i = 1; i < resultado.size(); i++) {
      hsEnvio.put(String.valueOf(i), (DatNotifTub) resultado.elementAt(i));
    }

    panel.rellena(hsEnvio);
    // fin Pruebas

    VerPanel("", panel);
  }

}
