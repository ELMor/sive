/**
 * Clase: AppConTub
 * Paquete: tuberculosis.cliente.mantcontub
 * Hereda: CApp
 * Autor: Pedro Antonio D�az Pardo (PDP)
 * Fecha Inicio: 25/01/2000
 * Analisis Funcional:  Mantenimiento Contactos con enfermos de Tuberculosis.
 * Descripcion: Applet de prueba para visualizar el panel PanConTub
 */

package tuberculosis.cliente.mantcontub;

import capp.CApp;
import comun.constantes;

//applet para  Mantenimiento de Contactos con enfermos de Tuberculosis
public class AppConTub
    extends CApp {

  public void init() {
    super.init();
    setTitulo("Mantenimiento de Contactos de Casos de Tuberculosis");
  }

  public void start() {
    int modo = constantes.modoMANTENIMIENTO;
    VerPanel("", new PanConTub(this, modo));
  }
} // endclass AppConTub
