package tuberculosis.cliente.diaresul;

import java.util.Hashtable;

import java.awt.Cursor;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CCargadorImagen;
import capp.CDialog;
import comun.constantes;
import tuberculosis.cliente.pantrat.PanTrat;
import tuberculosis.cliente.pantubnotif.PanTablaNotif;
import tuberculosis.datos.pantrat.DatCasTratCS;

public class DiaOtrosTratamientos
    extends CDialog {

  // Modo de salida del dialog (Aceptar=0, Cancelar=-1)
  //Aceptar == repetir query al volver
  //Cancelar == nada
  public int iOut = -1;

  final String imgNAME[] = {
      "images/aceptar.gif"};

  public int modoOperacion = constantes.modoALTA;

  ScrollPane panelScroll = new ScrollPane();
  XYLayout xYLayout1 = new XYLayout();
  ButtonControl btnSalir = new ButtonControl("Aceptar");

  PanTrat pT = null;
  DatCasTratCS datNotif = null;
  Hashtable hs = null;

  // constructor
  public DiaOtrosTratamientos(PanTablaNotif panNotif,
                              int modo, Hashtable hsComp) {

    super(panNotif.capp);

    try {
      datNotif = panNotif.getDatosNotificadorT();
      hs = hsComp;
      jbInit();
      Inicializar(modo);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void jbInit() throws Exception {
    btnSalir.setActionCommand("cancelar");
    // carga las imagenes
    CCargadorImagen imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnSalir.setImage(imgs.getImage(0));
    xYLayout1.setWidth(785);
    xYLayout1.setHeight(265);

    this.setLayout(xYLayout1);
    this.setSize(785, 265);

    DialogOTactionAdapter actionAdapter = new DialogOTactionAdapter(this);
    this.setTitle("Evolución");
    this.add(btnSalir, new XYConstraints( (785 - 80) / 2, 203, -1, -1));
    btnSalir.addActionListener(actionAdapter);
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar()

  {
    switch (modoOperacion) {

      case constantes.modoALTA:
      case constantes.modoMODIFICACION:
      case constantes.modoBAJA:
      case constantes.modoCONSULTA:
        btnSalir.setEnabled(true);
        if (pT != null) {
          pT.Inicializar(modoOperacion);

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case constantes.modoESPERA:
        btnSalir.setEnabled(false);
        if (pT != null) {
          pT.Inicializar(modoOperacion);

        }
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();

  }

  // Para recuperar los datos del Notificador actualizado
  public DatCasTratCS getComponente() {
    return pT.getComponente();
  }

  //SOLAPA PROTOCOLO
  public void addPanelTratamientos() {

    int modo = modoOperacion;
    this.Inicializar(constantes.modoESPERA);

    pT = null;
    pT = new PanTrat(this.app,
                     modo,
                     hs, datNotif, this);

    pT.setBorde(false);

    this.add(pT, new XYConstraints(2, 2, 780, 200));
    pT.doLayout();

    this.Inicializar(modo);

  }

  void btnCancelar_actionPerformed() {
    //hay que recoger cd_opE y fc_ultact
    dispose();
  }

} //fin clase

class DialogOTactionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaOtrosTratamientos adaptee;
  ActionEvent evt;

  DialogOTactionAdapter(DiaOtrosTratamientos adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if (evt.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed();
    }
  }
} //fin class
