/*Clase
 * Autor: Luis Rivera
 * Fecha Inicio: 13/01/2000
 * Descripcion: Interfaz que debe implementar cualquier contenedor del PanAnoSemFecha
 * Modificaciones:
 *
 *
 **/

package tuberculosis.cliente.componentes;

import capp2.CApp;
import capp2.CInicializar;

public interface ContPanAnoSemFecha
    extends CInicializar {

  /* Para obtener el applet*/
  public CApp getMiCApp();

  /* M�todos ser�n llamados desde el PanAnoSemFecha despu�s de que el propio PanAnoSemFecha
   * ejecute su codigo en la p�rdida de foco */

  public void alPerderFocoAno();

  public void alPerderFocoSemana();

  public void alPerderFocoFecha();

}
