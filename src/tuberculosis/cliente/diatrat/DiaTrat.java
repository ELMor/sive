/**
 * Clase: DiaMntRegTub
 * Paquete: tuberculosis.cliente.diatrat.DiaTrat
 * Hereda: CDialog
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 23/11/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: Implementacion del dialogo que permite dar de alta
 *   baja, modificar y consultar tratamientos de un notificador.
 */

package tuberculosis.cliente.diatrat;

import java.text.SimpleDateFormat;
import java.util.Locale;

import java.awt.Choice;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.TextAreaControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import comun.Common;
import comun.Comunicador;
import comun.constantes;
import sapp.StubSrvBD;
import tuberculosis.datos.pantrat.DatCasTratCS;
import tuberculosis.datos.pantrat.DatCasTratSC;

//import tuberculosis.servidor.diatrat.*;

public class DiaTrat
    extends CDialog {

  // Mensaje en caso de bloqueo
  private final String DATOS_BLOQUEADOS =
      "Los datos han sido modificados. �Desea sobrescribirlos?";

  // Estado: indica si se ha llevado a cabo correctamente la operacion deseada
  public boolean accionOK = false;
  public DatCasTratSC tratNuevo = null;

  // Localizacion del servlet (datos de un registro tub. individual)
//  final String strSERVLET_DIATRAT = "servlet/SrvDiaTrat";
  final String strSERVLET_DIATRAT = constantes.strSERVLET_DIATRAT;

  // Variable estado del panel
  protected int modoOperacion = constantes.modoALTA;
  protected int modoEntrada = constantes.modoALTA;

  // Sincronizaci�n de eventos
  protected boolean sinBloquear = true;
  protected int modoAnterior;
  ;

  // Datos del Notificador
  DatCasTratCS diaNotif = null;

  // Datos del tratamiento a modificar
  DatCasTratSC TratInicial = null;

  // Lista de Motivos de Tratamiento
  CLista LMotTrat = null;

  // Imagenes
  protected CCargadorImagen imgs = null;
  final String imgNAME[] = {
      constantes.imgACEPTAR,
      constantes.imgCANCELAR};

  /****************** Componentes del panel ********************/

  // Organizacion del panel
  private XYLayout xYLayout = new XYLayout();

  // Fecha Inicio
  private Label lblFIni = new Label("Fecha Reinicio:");
  // Cambio 22-05-01 ARS, poner barras autom�ticamente
//  private CFechaSimple cfsFIni = new CFechaSimple("N");
  private fechas.CFecha cfsFIni = new fechas.CFecha("N");

  // Motivo Tratamiento Inicio
  private Label lblMotIni = new Label("Motivo Reinicio:");
  private Choice choMotIni = new Choice();

  // Fecha Fin
  private Label lblFFin = new Label("Fecha Fin:");
  // Cambio 22-05-01 ARS, poner barras autom�ticamente
//  private CFechaSimple cfsFFin = new CFechaSimple("N");
  private fechas.CFecha cfsFFin = new fechas.CFecha("N");

  // Motivo Tratamiento Fin
  private Label lblMotFin = new Label("Motivo Fin:");
  private Choice choMotFin = new Choice();

  // Observaciones
  private Label lblObserv = new Label("Observaciones:");
  //private TextArea txtaObserv = new TextArea("",2,30,TextArea.SCROLLBARS_NONE);
  private TextAreaControl txtaObserv = new TextAreaControl();

  // Botones de aceptar y cancelar
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores
  DiaTratActionAdapter actionAdapter = null;
  DiaTratFocusAdapter focusAdapter = null;

  // Constructor
  // CApp sera luego el panel del que procede: PanTratMues
  // @param listaMotTrat: lista de Motivos de Tratamiento
  // @param notif: contiene los datos del Notificador al que pertenece el tratamiento
  // @param trat: lleva los datos de un tratamiento en caso de modificaci�n/baja/consulta
  public DiaTrat(CApp a, int modo, CLista listaMotTrat,
                 DatCasTratCS notif, DatCasTratSC trat) {
    super(a);
    try {
      // Variables globales
      LMotTrat = listaMotTrat;
      diaNotif = notif;
      TratInicial = trat;

      // Inicializacion
      modoOperacion = modo;
      modoEntrada = modo;
      jbInit();

      if (modoEntrada == constantes.modoMODIFICACION ||
          modoEntrada == constantes.modoBAJA ||
          modoEntrada == constantes.modoCONSULTA) {
        rellenarDatos(TratInicial);

      }
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    // Organizacion del panel
    this.setSize(new Dimension(500, 280));
    xYLayout.setHeight(280);
    xYLayout.setWidth(500);
    this.setLayout(xYLayout);
    this.setTitle("Evoluci�n");

    // Carga de las im�genes
    imgs = new CCargadorImagen(getCApp(), imgNAME);
    imgs.CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaTratActionAdapter(this);
    focusAdapter = new DiaTratFocusAdapter(this);

    // Fecha Inicio
    this.add(lblFIni, new XYConstraints(10, 15, 90, 25));
    this.add(cfsFIni, new XYConstraints(10 + 90 + 10, 15, 75, 25));
    if (modoEntrada == constantes.modoALTA) {
      SimpleDateFormat formUltact = new SimpleDateFormat("dd/MM/yyyy",
          new Locale("es", "ES"));
      //cfsFIni.setText(formUltact.format(new java.util.Date()));
    }
    cfsFIni.setName("cfsFIni");
    cfsFIni.addFocusListener(focusAdapter);

    // Motivo Tratamiento Inicio
    this.add(lblMotIni,
             new XYConstraints(10 + 90 + 10 + 75 + 15, 15, 75 + 10, 25));
    Common.writeChoiceDataGeneralCDDS(choMotIni, LMotTrat, true, true, true);
    this.add(choMotIni,
             new XYConstraints(10 + 90 + 10 + 75 + 15 + 75 + 10 + 10, 15,
                               140 + 50, 25));

    // Fecha Fin
    this.add(lblFFin, new XYConstraints(10, 15 + 30 + 15, 90, 25));
    this.add(cfsFFin, new XYConstraints(10 + 90 + 10, 15 + 30 + 15, 75, 25));
    cfsFFin.setName("cfsFFin");
    cfsFFin.addFocusListener(focusAdapter);

    // Motivo Tratamiento Fin
    this.add(lblMotFin,
             new XYConstraints(10 + 90 + 10 + 75 + 15, 15 + 30 + 15, 75 + 10,
                               25));
    Common.writeChoiceDataGeneralCDDS(choMotFin, LMotTrat, true, true, true);
    this.add(choMotFin,
             new XYConstraints(10 + 90 + 10 + 75 + 15 + 75 + 10 + 10,
                               15 + 30 + 15, 140 + 50, 25));

    // Observaciones
    this.add(lblObserv, new XYConstraints(10, 15 + 45 + 45, 90, 25));
    this.add(txtaObserv,
             new XYConstraints(10 + 5, 15 + 45 + 45 + 30, 400 + 50, 75));

    // Boton de Aceptar
    btnAceptar.setImage(imgs.getImage(0));
    btnAceptar.setLabel("Aceptar");
    this.add(btnAceptar,
             new XYConstraints(10 + 90 + 10 + 75 + 15 + 75 - 20 + 50,
                               15 + 45 + 45 + 30 + 50 + 30, 80, -1));
    if (modoOperacion == constantes.modoCONSULTA) {
      btnAceptar.setEnabled(false);
    }
    else {
      btnAceptar.setActionCommand("Aceptar");
      btnAceptar.addActionListener(actionAdapter);
    }

    // Boton de Cancelar
    btnCancelar.setImage(imgs.getImage(1));
    btnCancelar.setLabel("Cancelar");
    this.add(btnCancelar,
             new XYConstraints(10 + 90 + 10 + 75 + 15 + 75 + 10 + 60 + 50,
                               15 + 45 + 45 + 30 + 50 + 30, 80, -1));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);
  } // Fin jbInit()

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {
    switch (modoOperacion) {
      case constantes.modoESPERA:
        enableControls(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case constantes.modoALTA:
        enableControls(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case constantes.modoMODIFICACION:
        enableControls(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case constantes.modoCONSULTA:
        enableControls(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        btnCancelar.setEnabled(true);
        break;

      case constantes.modoBAJA:
        enableControls(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        btnCancelar.setEnabled(true);
        btnAceptar.setEnabled(true);
        break;
    }
  } // Fin Inicializar()

  private void enableControls(boolean en) {

    btnAceptar.setEnabled(en);
    btnCancelar.setEnabled(en);

    // Fecha Inicio
    cfsFIni.setEnabled(en);

    // Motivo Tratamiento Inicio
    choMotIni.setEnabled(en);

    // Fecha Fin
    cfsFFin.setEnabled(en);

    // Motivo Tratamiento Fin
    choMotFin.setEnabled(en);

    // Observaciones
    txtaObserv.setEnabled(en);

  } // Fin enableControls()

  public DatCasTratSC recogerDatos() {
    DatCasTratSC resul = null;

    String sNumTrat = TratInicial.getNM_TRATRTBC();
    String sFIni = cfsFIni.getText();
    String sMotIni = Common.getChoiceCDDataGeneralCDDS(choMotIni, LMotTrat, true, true, true);
    String sDsMotIni = Common.getChoiceDSDataGeneralCDDS(choMotIni, LMotTrat, true, true, true);
    String sFFin = cfsFFin.getText();
    String sMotFin = Common.getChoiceCDDataGeneralCDDS(choMotFin, LMotTrat, true, true, true);
    String sDsMotFin = Common.getChoiceDSDataGeneralCDDS(choMotFin, LMotTrat, true, true, true);
    String sObserv = txtaObserv.getText();
    String sOpe = this.getCApp().getLogin(); // Operario que realiza el alta

    resul = new DatCasTratSC(sNumTrat, sFIni, sMotIni, sDsMotIni, sFFin,
                             sMotFin, sDsMotFin, sObserv, sOpe, "");
    return resul;
  } // Fin recogerDatos()

  public boolean validarDatos() {
    // 1. PERDIDAS DE FOCO: No hay campos CD-Button-DS

    // 2. LONGITUDES

    // txtaObserv
    if (txtaObserv.getText().length() > 2000) {
      Common.ShowError(this.getCApp(),
          "La longitud del texto de Observaciones excede de 2000 caracteres");
      txtaObserv.setText("");
      txtaObserv.requestFocus();
      return false;
    }

    // 3. DATOS NUMERICOS: No hay

    // 4. CAMPOS OBLIGATORIOS: No hay

    /*// cfsFIni
        if(cfsFIni.getText().length() <= 0) {
        comun.ShowError(this.getCApp(),"La Fecha de Inicio es un campo obligatorio");
     cfsFIni.requestFocus();
     return false;
        } */

   //MLM: como no es ninguno obligatorio, forzamos a que al menos informen algo
   if (cfsFIni.getText().length() <= 0 && cfsFFin.getText().length() <= 0
       && txtaObserv.getText().length() <= 0
       &&
       Common.getChoiceCDDataGeneralCDDS(choMotIni, LMotTrat, true, true, true).
       equals("")
       &&
       Common.getChoiceDSDataGeneralCDDS(choMotFin, LMotTrat, true, true, true).
       equals("")
       ) {
     Common.ShowError(this.getCApp(), "Debe informar al menos un campo");
     cfsFIni.requestFocus();
     return false;

   }

    // Si llega aqui todo ha ido bien
    return true;
  } // Fin validarDatos()

  public void rellenarDatos(DatCasTratSC data) {
    // Fecha Inicio
    cfsFIni.setText(data.getFC_INITRAT());
    cfsFIni.ValidarFecha();
    if (cfsFIni.getValid().equals("N")) {
      cfsFIni.setText("");

      // Motivo Tratamiento Inicio
    }
    Common.selectChoiceElement(choMotIni, LMotTrat, true, data.getCD_MOTRATINI());

    // Fecha Fin
    cfsFFin.setText(data.getFC_FINTRAT());
    cfsFFin.ValidarFecha();
    if (cfsFFin.getValid().equals("N")) {
      cfsFFin.setText("");

      // Motivo Tratamiento Fin
    }
    Common.selectChoiceElement(choMotFin, LMotTrat, true, data.getCD_MOTRATFIN());

    // Observaciones
    txtaObserv.setText(data.getDS_OBSERV());

  } // Fin rellenarDatos()

  // Compara si los valores de 2 DatCasTratSC son iguales (true)
  private boolean compararCampos(DatCasTratSC a, DatCasTratSC b) {
    if (a == null || b == null) {
      return false;
    }

    if (!a.getFC_INITRAT().equals(b.getFC_INITRAT())) {
      return false;
    }

    if (!a.getCD_MOTRATINI().equals(b.getCD_MOTRATINI())) {
      return false;
    }

    if (!a.getFC_FINTRAT().equals(b.getFC_FINTRAT())) {
      return false;
    }

    if (!a.getCD_MOTRATFIN().equals(b.getCD_MOTRATFIN())) {
      return false;
    }

    if (!a.getDS_OBSERV().equals(b.getDS_OBSERV())) {
      return false;
    }

    return true;
  } // Fin  compararCampos()

  // Bloquear eventos
  /** Este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized boolean bloquea() {
    if (sinBloquear) {
      // No hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modoOperacion;
      modoOperacion = constantes.modoESPERA;
      Inicializar();

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // Ya est� bloqueado
      return false;
    }
  } // Fin bloquea()

  /** Este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    modoOperacion = modoAnterior;
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  } // Fin desbloquea()

  // Realiza la comunicaci�n con el servlet strSERVLET_DIAMED
  CLista hazAccion(StubSrvBD stubCliente, int modo, CLista parametros) throws
      Exception {
    CLista result = null;

    // Invocacion del servlet para que haga el insert
    result = Comunicador.Communicate(this.getCApp(),
                                     stubCliente,
                                     modo,
                                     strSERVLET_DIATRAT,
                                     parametros);

    // SOLO DESARROLLO
    /*SrvDiaTrat srv = new SrvDiaTrat();
         result = srv.doDebug(modo,parametros); */

    return result;
  } // Fin hazAccion()

  /******************** Manejadores *************************/

  void btnAceptarActionPerformed() {

    DatCasTratSC TratActual = null;
    CLista parametros = new CLista();
    CLista listaSalida = new CLista();
    StubSrvBD stubCliente = new StubSrvBD();
    CLista newTrat = new CLista();

    switch (modoEntrada) {
      case constantes.modoALTA:
        if (validarDatos()) {
          // Recogida de datos en un DatCasTratSC
          TratActual = recogerDatos();

          // Establecimiento de los parametros
          parametros.addElement(diaNotif);
          parametros.addElement(TratActual);

          try {
            // Invocacion del servlet para que haga el insert
            newTrat = hazAccion(stubCliente, constantes.modoALTA, parametros);
            accionOK = true;
          }
          catch (Exception e) {
            if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
              if (Common.ShowPregunta(getCApp(), DATOS_BLOQUEADOS)) {
                try {
                  newTrat = hazAccion(stubCliente, constantes.modoALTA_SB,
                                      parametros);
                  accionOK = true;
                }
                catch (Exception ex) {
                  ex.printStackTrace();
                  Common.ShowWarning(this.getCApp(),
                                     "Error al dar de alta del tratamiento.");
                  accionOK = false;
                }
              }
              else {
                accionOK = false;
              }
            }
            else {
              e.printStackTrace();
              Common.ShowWarning(this.getCApp(),
                                 "Error al dar de alta del tratamiento.");
              accionOK = false;
            }
          }

          if (accionOK) {
            // Establecemos en el tratam actual el numero de tratam que se acaba de insertar
            tratNuevo = (DatCasTratSC) newTrat.firstElement();

            // Cerramos el dialogo
            this.dispose();
          }
        } // validarDatos() ya muestra mensajes de error
        break;

      case constantes.modoMODIFICACION:
        if (validarDatos()) {
          // Recogida de datos en un DatCasTratSC
          TratActual = recogerDatos();

          if (compararCampos(TratActual, TratInicial)) {
            ;
          }
          else {
            // Establecimiento de los parametros
            parametros.addElement(diaNotif);
            parametros.addElement(TratActual);

            try {
              newTrat = hazAccion(stubCliente, constantes.modoMODIFICACION,
                                  parametros);
              accionOK = true;
            }
            catch (Exception e) {

              if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
                if (Common.ShowPregunta(getCApp(), DATOS_BLOQUEADOS)) {
                  try {
                    newTrat = hazAccion(stubCliente,
                                        constantes.modoMODIFICACION_SB,
                                        parametros);
                    accionOK = true;
                  }
                  catch (Exception ex) {
                    ex.printStackTrace();
                    Common.ShowWarning(this.getCApp(),
                                       "Error al modificar el tratamiento.");
                    accionOK = false;
                  }
                }
                else {
                  accionOK = false;
                }
              }
              else {
                e.printStackTrace();
                Common.ShowWarning(this.getCApp(),
                                   "Error al modificar el tratamiento.");
                accionOK = false;
              }
            }

            if (accionOK) {
              // Establecemos en el tratam actual el numero de tratam que se acaba de modif
              tratNuevo = (DatCasTratSC) newTrat.firstElement();
              // Cerramos el dialogo
              this.dispose();
            }
          }
        } // validarDatos() ya muestra mensajes de error

        break;

      case constantes.modoBAJA:

        // Recogida de datos en un DatCasTratSC
        TratActual = recogerDatos();

        // Establecimiento de los parametros
        parametros.addElement(diaNotif);
        parametros.addElement(TratActual);

        try {
          // Invocacion del servlet para que haga el delete
          newTrat = hazAccion(stubCliente, constantes.modoBAJA, parametros);
          accionOK = true;
        }
        catch (Exception e) {

          if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
            if (Common.ShowPregunta(getCApp(), DATOS_BLOQUEADOS)) {
              try {
                newTrat = hazAccion(stubCliente, constantes.modoBAJA_SB,
                                    parametros);
                accionOK = true;
              }
              catch (Exception ex) {
                ex.printStackTrace();
                Common.ShowWarning(this.getCApp(),
                                   "Error al borrar el tratamiento.");
                accionOK = false;
              }
            }
            else {
              accionOK = false;
            }
          }
          else {
            e.printStackTrace();
            Common.ShowWarning(this.getCApp(),
                               "Error al borrar el tratamiento.");
            accionOK = false;
          }
        }

        if (accionOK) {
          // Establecemos en el tratam actual el numero de tratam que se acaba de modif
          tratNuevo = (DatCasTratSC) newTrat.firstElement();
          // Cerramos el dialogo
          this.dispose();
        }

        break;
    } // Fin switch

  } // Fin btnAceptarActionPerformed()

  void btnCancelarActionPerformed() {
    dispose();
  } // Fin btnCancelarActionPerformed()

  void cfsFFinFocusLost() {
    // Validacion de la fecha
    cfsFFin.ValidarFecha();

    // Carga de la fecha
    cfsFFin.setText(cfsFFin.getFecha());

  }

  void cfsFIniFocusLost() {
    // Validacion de la fecha
    cfsFIni.ValidarFecha();

    // Carga de la fecha
    cfsFIni.setText(cfsFIni.getFecha());

  }

} // endclass DiaTrat

/******************* ESCUCHADORES **********************/

// Botones
class DiaTratActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaTrat adaptee;
  ActionEvent evt;

  DiaTratActionAdapter(DiaTrat adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    if (adaptee.bloquea()) {
      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      adaptee.desbloquea();
    }
  }
} // Fin clase DiaTratActionAdapter

// Perdidas de foco
class DiaTratFocusAdapter
    implements java.awt.event.FocusListener, Runnable {
  DiaTrat adaptee;
  FocusEvent evt;

  DiaTratFocusAdapter(DiaTrat adaptee) { // Constructor
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  } // Fin focusLost()

  public void focusGained(FocusEvent e) {
  }

  // Implementacion de run()
  public void run() {
    if (adaptee.bloquea()) {
      if ( ( (TextField) evt.getSource()).getName().equals("cfsFIni")) {
        adaptee.cfsFIniFocusLost();
      }
      else if ( ( (TextField) evt.getSource()).getName().equals("cfsFFin")) {
        adaptee.cfsFFinFocusLost();
      }
    }
    adaptee.desbloquea();
  } // Fin run()

} // Fin clase
