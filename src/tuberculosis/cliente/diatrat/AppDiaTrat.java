/**
 * Clase: AppDiaTrat
 * Paquete: tuberculosis.cliente.diatrat
 * Hereda: CApp
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 23/11/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: Applet de prueba para visualizar el dialogo DiaTrat
 */

package tuberculosis.cliente.diatrat;

import capp.CApp;
import capp.CLista;
import comun.DataGeneralCDDS;
import comun.constantes;
import tuberculosis.datos.pantrat.DatCasTratCS;
import tuberculosis.datos.pantrat.DatCasTratSC;

public class AppDiaTrat
    extends CApp {

  CLista LMotTratamiento = new CLista();

  public void init() {
    setTitulo("Dialogo de actualizacion de Tratamientos");
    super.init();
  }

  public void start() {

    // Listas de catalogos rellenadas a pelo

    // Relleno a pelo de la lista de motivos de tratamiento
    CLista LMotSalida = new CLista();
    DataGeneralCDDS d1 = new DataGeneralCDDS("01", "Fallecimiento");
    DataGeneralCDDS d2 = new DataGeneralCDDS("02", "Inicio Sintomas");
    DataGeneralCDDS d3 = new DataGeneralCDDS("03", "Evolucion Lenta");
    DataGeneralCDDS d4 = new DataGeneralCDDS("04", "Cura");
    LMotTratamiento.addElement(d1);
    LMotTratamiento.addElement(d2);
    LMotTratamiento.addElement(d3);
    LMotTratamiento.addElement(d4);

    // Relleno de los datos de un caso
    DatCasTratCS caso = new DatCasTratCS("H01A", "1999", "14", "1",
                                         "17/11/1999", "11/11/1999", "1", "",
                                         "D Mora", "N", "ADMON", "17/11/1999");
    // Relleno de los datos de un tratamiento (modoALTA)
    //DatCasTratSC trat = null;
    // Relleno de los datos de un tratamiento (modoMODIFICACION)
    DatCasTratSC trat = new DatCasTratSC("5", "31/12/1999", "", "", "", "", "",
                                         "", "", "");

    // Dialogo
    DiaTrat dlg = new DiaTrat(this, constantes.modoALTA, LMotTratamiento, caso,
                              trat);
    dlg.show();

    dlg = null;

    return;
  }
} // endclass AppDiaTrat