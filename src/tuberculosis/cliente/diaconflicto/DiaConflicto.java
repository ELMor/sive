/**
 * Clase: DiaConflicto
 * Paquete: tuberculosis.cliente.diaconflicto
 * Hereda: CDialog
 * Autor: Emilio Postigo Riancho
 * Fecha Inicio: 23/12/99
 * Descripcion: Implementaci�n del di�logo encargado de mostrar
               conflictos de una pregunta
 */

package tuberculosis.cliente.diaconflicto;

import java.util.Hashtable;
import java.util.Vector;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Label;
import java.awt.TextField;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CTabla;
import comun.Common;

import comun.Comunicador; //import comun.CFechaSimple; import comun.CHora; import comun.CListaNiveles1; import comun.Comunicador; import comun.constantes; import comun.Data; import comun.DataAutoriza; import comun.DataCasIn; import comun.DataEnferedo; import comun.DataEnfermo; import comun.DataEntradaEDO; import comun.DataEqNot; import comun.DataGeneralCDDS; import comun.DataIndivEnfermedad; import comun.DataLongValid; import comun.DataMun; import comun.DataMunicipioEDO; import comun.DataNivel1; import comun.DataNotifEDO; import comun.DataNotifSem; import comun.DataRegistroEDO; import comun.DataValoresEDO; import comun.DataZBS; import comun.DataZBS2; import comun.DatoBase; import comun.datosParte; import comun.datosPreg; import comun.DialogoGeneral; import comun.DialogRegistroEDO; import comun.DialogSelDomi; import comun.DialogSelEnfermo; import comun.Fechas; import comun.IntContenedor; import comun.PanAnoSemFech; import comun.SrvDebugGeneral; import comun.SrvDlgBuscarRegistro; import comun.SrvEnfermedad; import comun.SrvEntradaEDO; import comun.SrvMaestroEDO; import comun.SrvMun; import comun.SrvMunicipioCont; import comun.SrvNivel1; import comun.SrvZBS2; import comun.UtilEDO; import comun.comun;
import comun.Fechas;
import comun.constantes;
import jclass.util.JCVector;
import sapp.StubSrvBD;
import tuberculosis.datos.diaconflicto.DatResConflicto;
import tuberculosis.datos.panconflicto.ConDatConflicto;
import tuberculosis.datos.panconflicto.DatConflicto;

public class DiaConflicto
    extends CDialog {

  // Cola de eventos
  EventQueue eqClase = new EventQueue();

  // Para marcar l�mites en las dimensiones de la tabla
  private final int NUMERO_MAXIMO = 5;

  // dimensiones del di�logo
  private final int iANCHO = 760;
  private final int iALTO = 400;

  // Mensaje en caso de consolidaci�n
  private final String CONSOLIDAR_RESPUESTA =
      "�Desea establecer como respuesta correcta la seleccionada?";

  // Estados posibles del di�logo
  protected final int modoESPERA = constantes.modoESPERA;
  protected final int modoALTA = constantes.modoALTA;
  protected final int modoMODIFICACION = constantes.modoMODIFICACION;
  protected final int modoBAJA = constantes.modoBAJA;
  protected final int modoCONSULTA = constantes.modoCONSULTA;

  // Mensaje en caso de bloqueo

  // Variables utilizadas para gestionar los estados del di�logo
  protected int modoOperacion = 0;
  protected int modoAnterior = 0;

  // Para sincronizar eventos
  private boolean sinBloquear = true;

  // Conexi�n con servlets
  private StubSrvBD stubCliente;

  // Para controlar cambios de controles

  // Para almacenar los valores iniciales (Resoluci�n de conflictos)
  private DatConflicto dcInicio = null;

  // Para devolver valores
  private DatConflicto dcRetorno = null;

  // Hashtable encargada de almacenar los datos enviados por el contenedor
  private Hashtable hDatos = null;

  // Datos recuperados como par�metros
  private Vector vNotificadoresConflicto = null;
  private String s_Pregunta_Conflicto = null;

  // Variables recuperadas del applet

  // Operadores y fechas de actualizaci�n
  // Iniciales

  // Datos usados de forma habitual

  // Contenedor de im�genes para los botones
  protected CCargadorImagen imgs = null;

  // Contenedor de im�genes para los botones de la tabla
  protected CCargadorImagen imgsTabla = null;

  // Imagenes utilizadas
  final String imgNAME_tabla[] = {
      "images/primero.gif",
      "images/anterior.gif",
      "images/siguiente.gif",
      "images/ultimo.gif"};

  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/aceptar.gif",
      "images/cancelar.gif"};

  // Layout del panel
  XYLayout lyXYLayout = new XYLayout();

  // Tabla de notificadores en conflicto
  CTabla tbNotificadores = null;

  // Botones para navegar por la tabla
  ButtonControl btnPrimero = null;
  ButtonControl btnAnterior = null;
  ButtonControl btnSiguiente = null;
  ButtonControl btnUltimo = null;

  // Etiquetas
  Label lblPregunta = null;
  Label lblNotificador = null;
  Label lblRespuesta = null;
  Label lblConsolidar = null;

  // Cajas de texto
  TextField txtPregunta = null;
  TextField txtNotificador = null;
  TextField txtRespuesta = null;

  // CheckBox
  Checkbox chkConsolidar = null;

  // Bot�n Grabar
  ButtonControl btnGrabar = new ButtonControl();

  // Bot�n Cancelar
  ButtonControl btnCancelar = new ButtonControl();

  // Objeto gestor de eventos en Cancelar
  DiaConflicto_ActionAdapter dcaaGestorEventos = new DiaConflicto_ActionAdapter(this);

  // Objeto gestor de eventos Ganancia/P�rdida de foco en las cajas de texto
  //DiaConflicto_FocusAdapter dcfaGestorEventos = new DiaConflicto_FocusAdapter(this);

  // Objeto gestor de eventos en acciones (doble click) sobre la tabla
  DiaConflicto_TablaActionListener dctalGestorEventos = new
      DiaConflicto_TablaActionListener(this);

  // Objeto gestor de eventos en acciones (click o barra espaciadora) sobre la tabla
  DiaConflicto_TablaItemListener dctilGestorEventos = new
      DiaConflicto_TablaItemListener(this);

  // Objeto gestor de eventos en acciones (checkbox )
  DiaConflicto_chk_itemAdapter dcciaGestorEventos = new
      DiaConflicto_chk_itemAdapter(this);

  // Constructor
  public DiaConflicto(CApp a) {
    super(a);
    try {
      // Inicializaci�n de los controles
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicializacion en la creacion de un objeto
  private void jbInit() throws Exception {

    // t�tulo
    this.setTitle("");

    // Dimensionado del di�logo
    this.setSize(iANCHO, iALTO);

    // Parametros del di�logo
    this.setLayout(lyXYLayout);
    lyXYLayout.setHeight(iALTO);
    lyXYLayout.setWidth(iANCHO);
    this.setBackground(Color.lightGray);

    // Crea el objeto stub
    stubCliente = new StubSrvBD();

    // Carga de las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    imgsTabla = new CCargadorImagen(app, imgNAME_tabla);
    imgsTabla.CargaImagenes();

    // Se establecen los par�metros de la tabla
    tbNotificadores = new CTabla();
    pintaTabla();

    this.add(tbNotificadores, new XYConstraints(10, 10, iANCHO - 20, 150));

    // Eventos tabla (click y barra espaciadora)
    tbNotificadores.addItemListener(dctilGestorEventos);

    // Botones para navegar por la tabla
    btnPrimero = new ButtonControl(imgsTabla.getImage(0));
    btnAnterior = new ButtonControl(imgsTabla.getImage(1));
    btnSiguiente = new ButtonControl(imgsTabla.getImage(2));
    btnUltimo = new ButtonControl(imgsTabla.getImage(3));

    btnPrimero.setActionCommand("primero");
    btnAnterior.setActionCommand("anterior");
    btnSiguiente.setActionCommand("siguiente");
    btnUltimo.setActionCommand("ultimo");

    this.add(btnPrimero, new XYConstraints(450 + 170, 170, 25, 25));
    this.add(btnAnterior, new XYConstraints(485 + 170, 170, 25, 25));
    this.add(btnSiguiente, new XYConstraints(520 + 170, 170, 25, 25));
    this.add(btnUltimo, new XYConstraints(555 + 170, 170, 25, 25));

    // Eventos botones tabla
    btnPrimero.addActionListener(dcaaGestorEventos);
    btnAnterior.addActionListener(dcaaGestorEventos);
    btnSiguiente.addActionListener(dcaaGestorEventos);
    btnUltimo.addActionListener(dcaaGestorEventos);

    // Pregunta cuyas respuestas est�n en conflicto
    lblPregunta = new Label("Pregunta:");
    this.add(lblPregunta, new XYConstraints(15, 170, 70, -1));

    txtPregunta = new TextField();
    this.add(txtPregunta, new XYConstraints(85, 170, 460, -1));
    txtPregunta.setEditable(false);

    // Notificador seleccionado
    lblNotificador = new Label("Notificador:");
    this.add(lblNotificador, new XYConstraints(15, 210, 70, -1));

    txtNotificador = new TextField();
    this.add(txtNotificador, new XYConstraints(85, 210, 460, -1));
    txtNotificador.setEditable(false);

    // Respuesta seleccionada
    lblRespuesta = new Label("Respuesta:");
    this.add(lblRespuesta, new XYConstraints(15, 250, 70, -1));

    txtRespuesta = new TextField();
    this.add(txtRespuesta, new XYConstraints(85, 250, 460, -1));
    txtRespuesta.setEditable(false);

    // �Consolidar?
    lblConsolidar = new Label("Consolidar:");
    this.add(lblConsolidar, new XYConstraints(15, 290, 70, -1));

    chkConsolidar = new Checkbox();
    this.add(chkConsolidar, new XYConstraints(85, 292, 15, -1));

    chkConsolidar.addItemListener(dcciaGestorEventos);

    // Botones Grabar y Cancelar
    btnGrabar.setLabel("Aceptar");
    btnGrabar.setImage(imgs.getImage(1));
    this.add(btnGrabar, new XYConstraints(540, 310 + 30, 100, -1));
    btnGrabar.setActionCommand("grabar");

    // Eventos bot�n Grabar
    btnGrabar.addActionListener(dcaaGestorEventos);

    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(imgs.getImage(2));
    this.add(btnCancelar, new XYConstraints(650, 310 + 30, 100, -1));
    btnCancelar.setActionCommand("cancelar");

    // Eventos bot�n Cancelar
    btnCancelar.addActionListener(dcaaGestorEventos);

    // Se establecen los modos de funcionamiento iniciales
    modoOperacion = modoESPERA;
    modoAnterior = modoESPERA;

    // Inicializaci�n
    Inicializar();
  } // Fin jbinit()

  // Sistema de bloqueo de eventos
  /** Este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized boolean bloquea() {
    int modo = 0;
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;

      modo = ponerEnEspera();
      if (modo != modoESPERA) {
        modoAnterior = modo;
      }

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  /** Este m�todo desbloquea el sistema */
  protected synchronized void desbloquea() {
    sinBloquear = true;
    if (modoOperacion == modoESPERA) {
      modoOperacion = modoAnterior;
    }
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // Implementacion del metodo abstracto de la clase CPanel
  public void Inicializar() {
    /*
         switch (modoOperacion) {
      case modoESPERA:
        break;
      case modoALTA:
        break;
      case modoMODIFICACION:
        break;
         }
     */
    // Se repinta
    this.validate();
  }

  // Para devolver un valor a su contenedor
  public Object getComponente() {
    return dcRetorno;
  }

  // M�todos PRIVADOS

  // Para decidir si una excepci�n ha sido originada por un
  // bloqueo
  private CLista resolverBloqueo(Exception e, String servlet, int modo,
                                 CLista cl) {
    CLista clValor = null;

    if (bContiene(e.getMessage(), constantes.PRE_BLOQUEO)) {
      if (Common.ShowPregunta(this.app, constantes.DATOS_BLOQUEADOS)) {
        try {
          clValor = hazAccion(servlet, modo, cl);
        }
        catch (Exception exp) {
          Common.ShowWarning(this.app, exp.getMessage());
        }
      }
    }
    else {
      Common.ShowWarning(this.app, e.getMessage());
    }

    return clValor;
  }

  // Efect�a la llamada al servlet con el modo de operaci�n
  // y los datos necesarios
  private CLista hazAccion(String servlet, int modo, CLista cl) throws
      Exception {
    CLista clDatos;
    CLista clResultado;

    clDatos = cl;

    clResultado = Comunicador.Communicate(this.app,
                                          stubCliente,
                                          modo,
                                          servlet,
                                          clDatos);

    return clResultado;
  }

  // Devuelve false si no hay ning�n elemento con IT_CONSOLIDADO = true
  // en el vector
  private boolean hayConsolidado(Vector v) {
    boolean b = false;

    for (int i = 0; i < v.size() && b == false; i++) {
      if ( ( (DatResConflicto) v.elementAt(i)).getIT_CONSOLIDADO()) {
        b = true;
      }
    }

    return b;
  }

  // Pone a false los IT_CONSOLIDADO de todos los elementos del vector
  // dado
  private void limpiaConsolidados(Vector v) {
    DatResConflicto drc = null;

    for (int i = 0; i < v.size(); i++) {
      drc = (DatResConflicto) v.elementAt(i);
      drc.setIT_CONSOLIDADO(false);
    }
  }

  // Introduce el valor indicado en el componente indicado
  // del vector
  private void setConsolidado(boolean b, Vector v, int i) {
    DatResConflicto drc = null;

    if (i >= 0 && i < v.size()) {
      drc = (DatResConflicto) v.elementAt(i);
      drc.setIT_CONSOLIDADO(b);
    }

  }

  // Obtiene un booleano con el estado de consolidaci�n
  private boolean getConsolidado(Vector v, int i) {
    DatResConflicto drc = null;
    boolean b = false;

    if (i >= 0 && i < v.size()) {
      drc = (DatResConflicto) v.elementAt(i);
      b = drc.getIT_CONSOLIDADO();
    }

    return b;
  }

  // Obtiene una cadena con la respuesta dada por el notificador seleccionado
  private String getRespuesta(Vector v, int i) {
    DatResConflicto drc = null;
    String s = "";

    if (i >= 0 && i < v.size()) {
      drc = (DatResConflicto) v.elementAt(i);
      s = drc.getDS_RESPUESTA_MRE();
    }

    return s;
  }

  // Obtiene una cadena con los datos que forman la clave del
  // notificador a partir de un vector y el elemento del mismo
  private String getClaveNotificador(Vector v, int i) {
    DatResConflicto drc = null;
    String s = "";

    if (i >= 0 && i < v.size()) {
      drc = (DatResConflicto) v.elementAt(i);
      s = drc.getCD_ANOEPI_MRE() + "-" + drc.getCD_SEMEPI_MRE() +
          ", " + d2s(drc.getFC_FECNOTIF_MRE()) + "-" + d2s(drc.getFC_RECEP_MRE()) +
          ", " + drc.getCD_E_NOTIF_MRE() +
          ", " + drc.getCD_NIVEL_1_EQ() + "-" + drc.getCD_NIVEL_2_EQ() +
          ", " + drc.getDS_DECLARANTE_SNE() +
          ", " + drc.getCD_FUENTE_MRE();
    }

    return s;
  }

  // Simplifica la llamada a la funci�n de conversi�n
  private String d2s(java.sql.Date d) {
    return (Fechas.date2String(d)).trim();
  }

  // Carga la tabla a partir del vector pasado como
  // par�metro
  private void cargaTablaConflictos(Vector v) {
    JCVector jcvTabla = new JCVector();
    JCVector jcvLinea = null;

    DatResConflicto drc = null;

    tbNotificadores.clear();

    for (int i = 0; i < v.size(); i++) {
      drc = (DatResConflicto) v.elementAt(i);

      jcvLinea = new JCVector();

      jcvLinea.addElement(drc.getCD_ANOEPI_MRE() + "-" + drc.getCD_SEMEPI_MRE());
      jcvLinea.addElement(d2s(drc.getFC_FECNOTIF_MRE()) + "-" +
                          d2s(drc.getFC_RECEP_MRE()));
      jcvLinea.addElement(drc.getCD_E_NOTIF_MRE());
      jcvLinea.addElement(drc.getCD_NIVEL_1_EQ() + "-" + drc.getCD_NIVEL_2_EQ());
      jcvLinea.addElement(drc.getDS_DECLARANTE_SNE());
      jcvLinea.addElement(drc.getCD_FUENTE_MRE());
      jcvLinea.addElement(drc.getDS_RESPUESTA_MRE());
      jcvLinea.addElement(d2s(drc.getFC_ASIGN_MRE()));

      jcvTabla.addElement(jcvLinea);
    }

    tbNotificadores.setItems(jcvTabla);
  }

  // Establece el n�mero de columnas de la tabla y las cabeceras de las mismas
  private void pintaTabla() {
    tbNotificadores.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new
        String("65\n137\n50\n45\n145\n25\n182\n72"), '\n'));
    tbNotificadores.setColumnButtonsStrings(jclass.util.JCUtilConverter.
                                            toStringList(new String(
        "A�o-Sem\nF.Notif-F.Recep\nEquipo\nZonas\nDeclarante\nF.\nRespuesta\nF.Asignac."),
        '\n'));
    tbNotificadores.setNumColumns(8);
  } //fin pintaTabla()

  // Devuelve true si el objeto 'contenedor' contiene la cadena especificada
  private boolean bContiene(String contenedor, String contenido) {
    return contenedor.indexOf(contenido) != -1;
  }

  protected void lanzaEvento() {
    java.awt.AWTEvent e = null;
    try {
      e = eqClase.peekEvent();
    }
    catch (Exception exc) {
    }
    finally {
      e = null;
    }
  }

  // M�todos est�ndard

  // Permite al contenedor proporcionar datos al di�logo
  public void rellena(Hashtable h) {
    hDatos = h;
    if (hDatos != null) {
      if (h.containsKey(ConDatConflicto.NOTIFICADORES_CONFLICTO)) {
        vNotificadoresConflicto = (Vector) h.get(ConDatConflicto.
                                                 NOTIFICADORES_CONFLICTO);
        cargaTablaConflictos(vNotificadoresConflicto);
        tbNotificadores.select(0);
      }
      if (h.containsKey(ConDatConflicto.PREGUNTA_CONFLICTO)) {
        s_Pregunta_Conflicto = (String) h.get(ConDatConflicto.
                                              PREGUNTA_CONFLICTO);
        this.setTitle(s_Pregunta_Conflicto);
        txtPregunta.setText(s_Pregunta_Conflicto);
      }
      if (h.containsKey(ConDatConflicto.DATOS_CONFLICTO)) {
        dcInicio = (DatConflicto) h.get(ConDatConflicto.DATOS_CONFLICTO);
      }
    }
  }

  // Gesti�n de eventos

  // CheckBox
  protected void chkControlador_itemStateChanged() {
    limpiaConsolidados(vNotificadoresConflicto);
    setConsolidado(chkConsolidar.getState(), vNotificadoresConflicto,
                   tbNotificadores.getSelectedIndex());
  }

  // Tabla
  protected void tablaActionPerformed() {
    txtNotificador.setText(getClaveNotificador(vNotificadoresConflicto,
                                               tbNotificadores.getSelectedIndex()));
    txtRespuesta.setText(getRespuesta(vNotificadoresConflicto,
                                      tbNotificadores.getSelectedIndex()));
    chkConsolidar.setState(getConsolidado(vNotificadoresConflicto,
                                          tbNotificadores.getSelectedIndex()));
  }

  // Botones

  // Grabar
  protected void btnGrabar_actionPerformed() {
    CLista clLista = null;
    CLista clResultado = null;

    if (!hayConsolidado(vNotificadoresConflicto)) {
      Common.ShowWarning(this.app, "Debe consolidarse una respuesta");
      return;
    }

    if (Common.ShowPregunta(this.app, CONSOLIDAR_RESPUESTA)) {
      clLista = new CLista();
      clLista.addElement( (DatResConflicto) vNotificadoresConflicto.elementAt(
          tbNotificadores.getSelectedIndex()));
      clLista.addElement(dcInicio);

      try {
        clResultado = hazAccion(constantes.strSERVLET_RES_CONFLICTOS,
                                constantes.modoMODIFICACION, clLista);
      }
      catch (Exception e) {
        //comun.ShowError(this.app, e.getMessage());
        clResultado = resolverBloqueo(e, constantes.strSERVLET_RES_CONFLICTOS,
                                      constantes.modoMODIFICACION_SB, clLista);
      }
      finally {
        if (clResultado != null) {
          dcRetorno = new DatConflicto();
          dispose();
        }
      }
    }
  }

  // Cancelar
  protected void btnCancelar_actionPerformed() {
    dcRetorno = null;
    dispose();
  }

  // Botones de la tabla
  protected void btnTablaPrimero() {
    tbNotificadores.select(0);
    tbNotificadores.setTopRow(0);
  }

  protected void btnTablaAnterior() {
    int index = tbNotificadores.getSelectedIndex();
    if (index > 0) {
      index--;
      tbNotificadores.select(index);
      if (index - tbNotificadores.getTopRow() <= 0) {
        tbNotificadores.setTopRow(tbNotificadores.getTopRow() - 1);
      }
    }
    else {
      tbNotificadores.select(0);
      tbNotificadores.setTopRow(0);
    }
  }

  protected void btnTablaSiguiente() {
    int ultimo = tbNotificadores.countItems() - 1;
    int index = tbNotificadores.getSelectedIndex();
    if (index < ultimo && index >= 0) {
      index++;
      tbNotificadores.select(index);
      if (index - tbNotificadores.getTopRow() >= NUMERO_MAXIMO - 1) {
        tbNotificadores.setTopRow(tbNotificadores.getTopRow() + 1);
      }
    }
    else {
      if (index != ultimo) {
        tbNotificadores.select(0);
        tbNotificadores.setTopRow(0);
      }
    }
  }

  protected void btnTablaUltimo() {
    int index = 0;
    int ultimo = tbNotificadores.countItems() - 1;
    if (ultimo >= 0) {
      index = ultimo;
      tbNotificadores.select(index);
      if (tbNotificadores.countItems() >= NUMERO_MAXIMO) {
        tbNotificadores.setTopRow(tbNotificadores.countItems() - NUMERO_MAXIMO);
      }
      else {
        tbNotificadores.setTopRow(0);
      }
    }
  }

  // Fin gesti�n de eventos

  // Exigido por la interfaz ...
  public void cambioNivelAntesInformado(int niv) {
    //System.out.println("cambioNivelAntesInformado " + niv);
  }

  public int ponerEnEspera() {
    int modo = modoOperacion;
    if (modoOperacion != modoESPERA) {
      modoOperacion = modoESPERA;
      Inicializar();
    }
    //System.out.println("ponerEnEspera ");
    return modo;
  }

  public void ponerModo(int modo) {
    if (modoOperacion != modo) {
      modoOperacion = modo;
      Inicializar();
    }
    //System.out.println("ponerModo " + modo);
  }

  // Exigido por IntContenedor
  public CApp getMiCApp() {
    return this.app;
  }

} // endclass DiaConflicto

/******************** ESCUCHADORES DE EVENTOS *********************/

// Eventos sobre el checkbox
class DiaConflicto_chk_itemAdapter
    implements java.awt.event.ItemListener {
  DiaConflicto adaptee;

  public DiaConflicto_chk_itemAdapter(DiaConflicto adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(java.awt.event.ItemEvent e) {
//    if(adaptee.bloquea()) {
    adaptee.chkControlador_itemStateChanged();
//      adaptee.desbloquea();
//    }
  }
}

// Eventos (click o barra espaciadora) sobre la tabla
class DiaConflicto_TablaItemListener
    implements jclass.bwt.JCItemListener {
  DiaConflicto adaptee;

  public DiaConflicto_TablaItemListener(DiaConflicto dcDialogo) {
    adaptee = dcDialogo;
  }

  public void itemStateChanged(jclass.bwt.JCItemEvent e) {
    adaptee.tablaActionPerformed();
  }
}

// Eventos (doble click) sobre la tabla
class DiaConflicto_TablaActionListener
    implements jclass.bwt.JCActionListener {
  DiaConflicto adaptee;

  public DiaConflicto_TablaActionListener(DiaConflicto dcDialogo) {
    adaptee = dcDialogo;
  }

  public void actionPerformed(jclass.bwt.JCActionEvent e) {
    //adaptee.modoOperacion = adaptee.modoESPERA;
    //adaptee.Inicializar(adaptee.modoOperacion );

    adaptee.tablaActionPerformed();

    //adaptee.desbloquea();
  }

} // Fin eventos tabla

// Eventos sobre los botones
class DiaConflicto_ActionAdapter
    implements java.awt.event.ActionListener {
  DiaConflicto adaptee;

  DiaConflicto_ActionAdapter(DiaConflicto adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(java.awt.event.ActionEvent e) {
    if (adaptee.bloquea()) {
      if (e.getActionCommand().equals("cancelar")) {
        adaptee.btnCancelar_actionPerformed();
      }
      else if (e.getActionCommand().equals("grabar")) {
        adaptee.btnGrabar_actionPerformed();
      }
      else if (e.getActionCommand().equals("primero")) {
        adaptee.btnTablaPrimero();
      }
      else if (e.getActionCommand().equals("anterior")) {
        adaptee.btnTablaAnterior();
      }
      else if (e.getActionCommand().equals("siguiente")) {
        adaptee.btnTablaSiguiente();
      }
      else if (e.getActionCommand().equals("ultimo")) {
        adaptee.btnTablaUltimo();
      }
      adaptee.desbloquea();
    }
  }
} // Fin DiaConflicto_ActionAdapter
