/* Bloqueo: Comprobaremos que nadie haya alterao el RT,
  pero nosotros actualizamos nm_edo y RT
 */
package tuberculosis.cliente.mantautom;

import java.net.URL;

import java.awt.Cursor;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import capp.CTabla;
import comun.Common;
import comun.Comunicador;
import comun.constantes;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import jclass.util.JCVector;
import sapp.StubSrvBD;
import tuberculosis.datos.mantautom.DataAuto;

public class PanAuto
    extends CPanel {

  protected CCargadorImagen imgs = null;

  // Estado: indica si se ha llevado a cabo correctamente la operacion deseada
  public boolean accionOK = false;
  // Mensaje en caso de bloqueo
  private final String DATOS_BLOQUEADOS =
      "El registro de tuberculosis ha sido modificado. �Desea sobrescribirlo?";
  private final String DATOS_BLOQUEADOS_EN_MASIVO =
      "Alg�n registro de tuberculosis ha sido modificado. �Desea sobrescribir?";
  //Modos de operaci�n de la ventana
  final int modoALTA = 0;
  final int modoESPERA = 2;

//  final String strSERVLET = "servlet/SrvAutoCierre";
  final String strSERVLET = constantes.strSERVLET_AUTOCIERRE;

  XYLayout xYLayout1 = new XYLayout();

  PanAuto_btn_actionAdapter btn_actionAdapter = null;
  //Label lbl1 = new Label();

  ButtonControl btnCerrarRT = new ButtonControl();
  ButtonControl btnCerrarMasivo = new ButtonControl();
  ButtonControl btnBuscar = new ButtonControl();

  ButtonControl btnPrimero = null;
  ButtonControl btnAnterior = null;
  ButtonControl btnSiguiente = null;
  ButtonControl btnUltimo = null;

  public CTabla tabla = new CTabla();

  // Stub's
  protected StubSrvBD stubCliente = null;

  // par�metros
  protected int modoOperacion = modoALTA;
  protected int modoAnt = modoALTA;

  public CLista listaRegistros = null;

  //Label lbl2 = new Label();
  //Label lbl3 = new Label();
  //Label lbl4 = new Label();

  public PanAuto(CApp a) {
    try {
      this.app = (CApp) a;
      listaRegistros = new CLista();
      stubCliente = new StubSrvBD(new URL(this.app.getURL() + strSERVLET));
      jbInit();

    }
    catch (Exception e) {
      ;
    }
  }

  void jbInit() throws Exception {

    this.setBorde(false);
    final String imgNAME[] = {
        "images/refrescar.gif",
        "images/baja2.gif",
        "images/primero.gif",
        "images/anterior.gif",
        "images/siguiente.gif",
        "images/ultimo.gif"};

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    xYLayout1.setHeight(250); //278
    xYLayout1.setWidth(648);
    this.setLayout(xYLayout1);

    btnBuscar = new ButtonControl(imgs.getImage(0));
    btnCerrarRT = new ButtonControl(imgs.getImage(1));
    btnCerrarMasivo = new ButtonControl(imgs.getImage(1));
    btnPrimero = new ButtonControl(imgs.getImage(2));
    btnAnterior = new ButtonControl(imgs.getImage(3));
    btnSiguiente = new ButtonControl(imgs.getImage(4));
    btnUltimo = new ButtonControl(imgs.getImage(5));

    btnBuscar.setActionCommand("buscar");
    btnCerrarRT.setActionCommand("cerrarRT");
    btnCerrarMasivo.setActionCommand("cerrarMasivo");

    btnPrimero.setActionCommand("primero");
    btnAnterior.setActionCommand("anterior");
    btnSiguiente.setActionCommand("siguiente");
    btnUltimo.setActionCommand("ultimo");

    btn_actionAdapter = new PanAuto_btn_actionAdapter(this);
    btnBuscar.addActionListener(btn_actionAdapter);
    btnBuscar.setLabel("Buscar");
    btnCerrarMasivo.setLabel("CerrarMasivo");
    btnCerrarRT.setLabel("CerrarRT");

    btnBuscar.addActionListener(btn_actionAdapter);
    btnCerrarRT.addActionListener(btn_actionAdapter);
    btnCerrarMasivo.addActionListener(btn_actionAdapter);
    btnPrimero.addActionListener(btn_actionAdapter);
    btnAnterior.addActionListener(btn_actionAdapter);
    btnSiguiente.addActionListener(btn_actionAdapter);
    btnUltimo.addActionListener(btn_actionAdapter);

    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "100\n100\n100\n100\n100\n100"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String("Registro\nF. Actual\nMax (F.Ini.Trat.)\nMin (F.Ini.Trat.)\nMax (F.FinTrat.)\nMin (F.FinTrat.)"),
        '\n'));
    tabla.setNumColumns(6);

    /*
         lbl1.setText("Registros que s�lo poseen fechas de inicio de Tratamiento y han pasado " +
         "2 a�os desde la �ltima fecha de inicio.");
         lbl2.setText("Registros que poseen fecha de inicio y fecha fin en sus Tratamientos " +
         "y ha pasado 1 a�o desde la �ltima fecha de fin.");
         lbl3.setText("El resto de registros si ha pasado 1 a�o desde la �ltima fecha de " +
         "fin de Tratamiento");
         lbl4.setText("y 2 a�os desde la �ltima fecha de inicio de Tratamiento");
     */

    tabla.addActionListener(new PanAuto_tabla_dobleClick(this));
    this.add(btnBuscar, new XYConstraints(10, 10, -1, -1));
    this.add(tabla, new XYConstraints(10, 45, 618, 124));
    this.add(btnCerrarMasivo, new XYConstraints(260, 176, -1, -1));
    this.add(btnPrimero, new XYConstraints(502, 176, 25, 25));
    this.add(btnAnterior, new XYConstraints(536, 176, 25, 25));
    this.add(btnSiguiente, new XYConstraints(570, 176, 25, 25));
    this.add(btnUltimo, new XYConstraints(604, 176, 25, 25));

    /*this.add(lbl1, new XYConstraints(10, 13, -1, -1));
         this.add(lbl2, new XYConstraints(11, 37, 642, 25));
         this.add(lbl3, new XYConstraints(10, 64, 650, 25));
         this.add(lbl4, new XYConstraints(9, 91, 369, 20));*/

    this.add(btnCerrarRT, new XYConstraints(10, 176, -1, -1));

    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

    modoOperacion = modoALTA;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:
        btnBuscar.setEnabled(true);

        if (tabla.countItems() > 0) {
          btnCerrarRT.setEnabled(true);
          btnCerrarMasivo.setEnabled(true);
          btnPrimero.setEnabled(true);
          btnAnterior.setEnabled(true);
          btnSiguiente.setEnabled(true);
          btnUltimo.setEnabled(true);
        }
        else {
          btnCerrarRT.setEnabled(false);
          btnCerrarMasivo.setEnabled(false);
          btnPrimero.setEnabled(false);
          btnAnterior.setEnabled(false);
          btnSiguiente.setEnabled(false);
          btnUltimo.setEnabled(false);
        }
        tabla.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.doLayout();
        break;

      case modoESPERA:

        btnCerrarRT.setEnabled(false);
        btnCerrarMasivo.setEnabled(false);
        btnBuscar.setEnabled(false);
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        tabla.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.doLayout();
        break;

    }
    this.doLayout();
  }

  public CLista hazAccion(StubSrvBD stubCliente,
                          int modo,
                          CLista parametros) throws Exception {

    CLista result = null;

    // Invocacion del servlet
    result = Comunicador.Communicate(this.app,
                                     stubCliente,
                                     modo,
                                     strSERVLET,
                                     parametros);

    /*  SrvAutoCierre srv = new SrvAutoCierre();
      srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@194.140.66.208:1521:ORCL", "sive_desa", "sive_desa");
      result = srv.doDebug(modo, parametros);*/

    return result;
  } // Fin hazAccion()

  public void btnCerrarRT_actionPerformed() {

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();
    int iSel = tabla.getSelectedIndex();

    if (iSel != BWTEnum.NOTFOUND) {
      // se solicita la confirmaci�n
      CMessage msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
          "�Desea cerrar el Registro de Tuberculosis seleccionado?");

      msgBox.show();

      if (msgBox.getResponse()) {

        DataAuto dataEntrada = new DataAuto();
        dataEntrada = (DataAuto) listaRegistros.elementAt(iSel);
        CLista listaEntrada = new CLista();
        listaEntrada.addElement(dataEntrada);
        CLista RTCerrar = new CLista();

        try {
          RTCerrar = hazAccion(stubCliente, constantes.modoBORRARUNO,
                               listaEntrada);
          accionOK = true;
        }
        catch (Exception e) {

          //ERROR DE BLOQUEO --------------------------------------
          if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
            if (Common.ShowPregunta(this.app, DATOS_BLOQUEADOS)) {
              try {
                RTCerrar = hazAccion(stubCliente, constantes.modoBORRARUNO_SB,
                                     listaEntrada);
                accionOK = true;
              }
              catch (Exception ex) {
                ex.printStackTrace();
                Common.ShowWarning(this.app, "Error al cerrar el registro.");
                accionOK = false;
              }
            }
            else {
              accionOK = false;
            }
          }
          //ERROR GENERAL --------------------------------------
          else {
            e.printStackTrace();
            Common.ShowWarning(this.app, "Error al cerrar el registro.");
            accionOK = false;
          }
        } // Fin try

        if (accionOK) {
          btnBuscar_actionPerformed(true);
        }

      } //SI RESPONDE QUE QUIERE CERRAR EL RT

    }
    else {
      Common.ShowWarning(this.app,
                         "Seleccione previamente una l�nea de la lista");
    }
    modoOperacion = modo;
    Inicializar();
  }

  public void btnCerrarMasivo_actionPerformed() {

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();

    // se solicita la confirmaci�n
    CMessage msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
        "�Desea cerrar todos los Registros de Tuberculosis visualizados?");

    msgBox.show();

    if (msgBox.getResponse()) {
      CLista listaEntrada = new CLista();
      for (int j = 0; j < listaRegistros.size(); j++) {
        DataAuto dataEntrada = (DataAuto) listaRegistros.elementAt(j);
        listaEntrada.addElement(dataEntrada);
      }
      CLista RTCerrar = new CLista();

      try {
        RTCerrar = hazAccion(stubCliente, constantes.modoBORRARMASIVO,
                             listaEntrada);
        accionOK = true;
      }
      catch (Exception e) {

        //ERROR DE BLOQUEO --------------------------------------
        if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
          if (Common.ShowPregunta(this.app, DATOS_BLOQUEADOS_EN_MASIVO)) {
            try {
              RTCerrar = hazAccion(stubCliente, constantes.modoBORRARMASIVO_SB,
                                   listaEntrada);
              accionOK = true;
            }
            catch (Exception ex) {
              ex.printStackTrace();
              Common.ShowWarning(this.app, "Error al cerrar los registros.");
              accionOK = false;
            }
          }
          else {
            accionOK = false;
          }
        }
        //ERROR GENERAL --------------------------------------
        else {
          e.printStackTrace();
          Common.ShowWarning(this.app, "Error al cerrar los registros.");
          accionOK = false;
        }
      } // Fin try

      if (accionOK) {
        btnBuscar_actionPerformed(true);
      }

    } //SI RESPONDE QUE QUIERE CERRAR EL RT

    modoOperacion = modo;
    Inicializar();
  } //fin de cerrar masivo

// Resetea la tabla: borrado y repintado
  public void ResetearTabla() {
    tabla.clear();
    tabla.repaint();
    btnCerrarRT.setEnabled(false);
    btnCerrarMasivo.setEnabled(false);
    btnPrimero.setEnabled(false);
    btnAnterior.setEnabled(false);
    btnSiguiente.setEnabled(false);
    btnUltimo.setEnabled(false);
  } // Fin ResetearTabla()

  void btnBuscar_actionPerformed(boolean bsinmsg) {
    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    Inicializar();
    CLista parametros = new CLista();
    listaRegistros = new CLista();
    ResetearTabla();

    try {
      listaRegistros = hazAccion(stubCliente,
                                 constantes.modoSELECT,
                                 parametros);

      if (listaRegistros != null) {
        if (listaRegistros.size() > 0) {
          writelistaRegistros();
        }
        else {
          if (!bsinmsg) {
            Common.ShowWarning(this.app, "No se encontraron datos.");
          }
        }
      }
      else {
        Common.ShowWarning(this.app, "Error al realizar la b�squeda");
      }

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      Common.ShowWarning(this.app,
                         "Error al recuperar los datos de los registros");
    }

    modoOperacion = modo;
    Inicializar();
  }

  public void writelistaRegistros() {

    DataAuto data = new DataAuto();
    JCVector row1 = new JCVector();
    JCVector items = new JCVector();
    tabla.clear();

    String cd_ano = "";
    String cd_registro = "";
    String fc_actual = "";
    String maxini = "";
    String minini = "";
    String maxfin = "";
    String minfin = "";
    String fc_ultact_nmedo = "";
    String cd_ope_nmedo = "";

    // vuelca la lista
    for (int j = 0; j < listaRegistros.size(); j++) {
      data = (DataAuto) listaRegistros.elementAt(j);
      row1 = new JCVector();

      cd_ano = data.getCD_ARTBC();
      cd_registro = data.getCD_NRTBC();
      fc_actual = data.getFC_ACTUAL();
      if (data.getMAX_FC_INITRAT() == null) {
        maxini = "";
      }
      else {
        maxini = data.getMAX_FC_INITRAT().trim();

      }
      if (data.getMIN_FC_INITRAT() == null) {
        minini = "";
      }
      else {
        minini = data.getMIN_FC_INITRAT().trim();

      }
      if (data.getMAX_FC_FINTRAT() == null) {
        maxfin = "";
      }
      else {
        maxfin = data.getMAX_FC_FINTRAT().trim();

      }
      if (data.getMIN_FC_FINTRAT() == null) {
        minfin = "";
      }
      else {
        minfin = data.getMIN_FC_FINTRAT().trim();

      }
      fc_ultact_nmedo = data.getFC_ULTACT_RT();
      cd_ope_nmedo = data.getCD_OPE_RT();

      row1.addElement(cd_ano + "-" + cd_registro);
      row1.addElement(fc_actual);
      row1.addElement(maxini);
      row1.addElement(minini);
      row1.addElement(maxfin);
      row1.addElement(minfin);
      items.addElement(row1);

    }
    tabla.setItems(items);
    tabla.repaint();

  }

} // END CLASS

class PanAuto_btn_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanAuto adaptee;
  ActionEvent e;

  PanAuto_btn_actionAdapter(PanAuto adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent evt) {
    e = evt;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    int index = 0;

    if (e.getActionCommand() == "buscar") { // lista principal
      adaptee.btnBuscar_actionPerformed(false);
    }
    else if (e.getActionCommand() == "cerrarRT") {
      adaptee.btnCerrarRT_actionPerformed();
    }
    else if (e.getActionCommand() == "cerrarMasivo") {
      adaptee.btnCerrarMasivo_actionPerformed();
    }
    else if (e.getActionCommand() == "primero") {
      if (adaptee.tabla.countItems() > 0) {
        adaptee.tabla.select(0);
        adaptee.tabla.setTopRow(0);
      }
    }
    else if ( (e.getActionCommand() == "anterior")
             && (adaptee.tabla.countItems() > 0)) {
      index = adaptee.tabla.getSelectedIndex();
      if (index > 0) {
        index--;
        adaptee.tabla.select(index);
        if (index - adaptee.tabla.getTopRow() <= 0) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() - 1);
        }
      }
      else {
        adaptee.tabla.select(0);
        adaptee.tabla.setTopRow(0);
      }
    }
    else if ( (e.getActionCommand() == "siguiente")
             && ( (adaptee.tabla.countItems() > 0))) {
      int ultimo = adaptee.tabla.countItems() - 1;
      index = adaptee.tabla.getSelectedIndex();
      if (index < ultimo && index >= 0) {
        index++;
        adaptee.tabla.select(index);
        if (index - adaptee.tabla.getTopRow() >= 4) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() + 1);
        }
      }
      else {
        adaptee.tabla.select(0);
        adaptee.tabla.setTopRow(0);
      }
    }
    else if ( (e.getActionCommand() == "ultimo")
             && (adaptee.tabla.countItems() > 0)) {
      int ultimo = adaptee.tabla.countItems() - 1;
      if (ultimo >= 0) {
        index = ultimo;
        adaptee.tabla.select(index);
        if (adaptee.tabla.countItems() >= 4) {
          adaptee.tabla.setTopRow(adaptee.tabla.countItems() - 4);
        }
        else {
          adaptee.tabla.setTopRow(0);
        }
      }
    }
  }
}

// escuchador de los click en la tabla
class PanAuto_tabla_dobleClick
    implements jclass.bwt.JCActionListener, Runnable {
  PanAuto adaptee;
  JCActionEvent e;

  PanAuto_tabla_dobleClick(PanAuto adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent evt) {
    e = evt;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btnCerrarRT_actionPerformed();
  }
}
