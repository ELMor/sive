/**
 * Clase: PanModRegTub
 * Paquete: SP_Tuberculosis.cliente.mantregtub.DiaMntRegTub
 * Hereda: CPanel
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 14/10/1999
 * Analisis Funcional: Punto 2. Mantenimiento Registro Tuberculosis.
 * Descripcion: Implementacion del panel que permite visualizar los datos
    principales de un registro de tuberculosis para poder ser modificados.
 * JMT (29/10/99): adicion del panel suca (interfaz zonificacionSanitaria)
 */

package tuberculosis.cliente.diamntregtub;

import java.util.Hashtable;
import java.util.Vector;

import java.awt.Choice;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import comun.Comunicador;
import comun.Fechas;
import comun.constantes;
import panniveles.panelNiveles;
import panniveles.usaPanelNiveles;
import sapp.StubSrvBD;
import suca.panelsuca;
import suca.zonificacionSanitaria;
import tuberculosis.datos.diamntregtub.DatDiaMntRegTub;
import tuberculosis.datos.diamntregtub.DatEqPrim;
import tuberculosis.datos.mantregtub.DatCatRegTub;

public class PanModRegTub
    extends CPanel
    implements zonificacionSanitaria, usaPanelNiveles {

  protected boolean bEquipoValid = false;

  StubSrvBD stubCliente = new StubSrvBD();

  // Listas con los catalogos
  CLista LSuca = new CLista(); // Lista para el constructor del suca
  CLista LPaises = null;
  CLista LCA = null;
  CLista LSexos = null;

//  final String strSERVLET = "servlet/SrvEqPrimaria";
  final String strSERVLET = constantes.strSERVLET_EQ_PRIMARIA;

  // Modo de operaci�n del panel
  static final int modoESPERA = constantes.modoESPERA;
  static final int modoMODIFICACION = constantes.modoMODIFICACION;
  static final int modoCONSULTA = constantes.modoCONSULTA;
  int modoOperacion = modoMODIFICACION;
  static final int modoDESHABILITADO = constantes.modoDESHABILITADO;

  // Contenedor de im�genes
  protected CCargadorImagen imgs = null;
  final String imgNAME[] = {
      "images/Magnify.gif"};

  /* -------------------- CONTROLES ------------------------- */

  // Organizacion del panel
  private XYLayout lyXYLayout = new XYLayout();

  // Codigo enfermo
  private Label lblCodEnfermo = new Label("Cod. Enfermo:");
  private TextField txtCodEnfermo = new TextField();

  // Apellidos y nombre
  private Label lblApeNombre = new Label("Apells. y Nombre:");
  private TextField txtApe1 = new TextField();
  private TextField txtApe2 = new TextField();
  private TextField txtNombre = new TextField();

  // Fecha nacimiento, Edad, Sexo y Telefono
  private Label lblFechaNac = new Label("F. Nacimiento:");
  // Correcci�n barras fechas autom�ticas (ARS 22-05-01)
//  private CFechaSimple txtFechaNac = new CFechaSimple("N");
  private fechas.CFecha txtFechaNac = new fechas.CFecha("N");
  private Label lblReq = new Label("");
  private Label lblEdad = new Label("Edad:");
  private TextField txtEdad = new TextField();
  private Choice choEdad = new Choice();
  private Label lblSexo = new Label("Sexo:");
  private Choice choSexo = new Choice();
  private Label lblTelefono = new Label("Telefono:");
  private TextField txtTelefono = new TextField();

  // Panel suca
  panelsuca psuca = null;

  // Equipo Notificador
  private Label lblEquipoNotif = new Label("Eq. Primaria:");
  private CCampoCodigo txtEquipoNotif = new CCampoCodigo();
  private ButtonControl btnLupaEquipoNotif = new ButtonControl();
  private TextField txtDescEquipoNotif = new TextField();

  // Medico Facultativo
  private Label lblMedFac = new Label("Nom. Facultativo:");
  private TextField txtMedFac = new TextField();

  // Observaciones
  private Label lblObserv = new Label("Observaciones:");
  private TextArea txtaObserv = new TextArea("", 2, 30,
                                             TextArea.SCROLLBARS_VERTICAL_ONLY);

  //niveles
  public panelNiveles panNiv = null;
  private Label lblZonaGestion = new Label("Zona de Gesti�n:");

  private DiaMntRegTub dlg = null;

  //Escuchadores
  Panel_btn_actionAdapter actionAdapter = null;
  PanelfocusAdapter focusAdapter = null;
  PaneltextAdapter textAdapter = null;

  public PanModRegTub(DiaMntRegTub dialogo, Hashtable catalogos) {
    try {

      dlg = dialogo;
      setApp(dlg.getCApp());
      setBorde(false);

      // Catalogos
      LPaises = (CLista) catalogos.get("PAISES");
      LCA = (CLista) catalogos.get("CA");
      LSexos = (CLista) catalogos.get("SEXOS");

      jbInit();

      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    // Vars. para colocar los componentes
    int pX = 0;
    int pY = 0;
    int longX = 0;
    int longY = 0;

    int modoSuca;

    // Medidas y organizacion del panel
    this.setSize(new Dimension(680, 380));
    lyXYLayout.setHeight(400);
    lyXYLayout.setWidth(680);
    this.setLayout(lyXYLayout);

    // Carga de las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    // Codigo del enfermo
    pX = 15;
    longX = 80;
    pY += 15;
    longY = -1;
    this.add(lblCodEnfermo, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 25;
    longX = 60;
    txtCodEnfermo.setEditable(false);
    txtCodEnfermo.setEnabled(false);
    this.add(txtCodEnfermo, new XYConstraints(pX, pY, longX, longY));

    // Apellidos y nombre
    pX = 15;
    longX = 100;
    pY += 30;
    longY = -1;
    this.add(lblApeNombre, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 150 + 4;
    txtApe1.setEditable(false);
    txtApe1.setEnabled(false);
    this.add(txtApe1, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 150 + 3;
    txtApe2.setEditable(false);
    txtApe2.setEnabled(false);
    this.add(txtApe2, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 150 + 3;
    txtNombre.setEditable(false);
    txtNombre.setEnabled(false);
    this.add(txtNombre, new XYConstraints(pX, pY, longX, longY));

    // Fecha nacimiento, Edad, Sexo y Telefono
    pX = 15;
    longX = 90;
    pY += 30;
    longY = -1;
    this.add(lblFechaNac, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 15;
    longX = 74;
    txtFechaNac.setEditable(false);
    txtFechaNac.setEnabled(false);
    this.add(txtFechaNac, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 25;
    this.add(lblReq, new XYConstraints(pX, pY, longX, longY));

    pX += longX + 10 - 4;
    longX = 32;
    this.add(lblEdad, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 28;
    txtEdad.setEditable(false);
    txtEdad.setEnabled(false);
    this.add(txtEdad, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 60;
    choEdad.setEnabled(false);
    choEdad.addItem("A�os");
    choEdad.addItem("Meses");
    this.add(choEdad, new XYConstraints(pX, pY, longX, longY));

    pX += longX + 10 - 2;
    longX = 30;
    this.add(lblSexo, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5 - 2;
    longX = 60;
    choSexo.setEnabled(false);
    choSexo.addItem("");
    for (int i = 0; i < LSexos.size(); i++) {
      DatCatRegTub sexo = (DatCatRegTub) LSexos.elementAt(i);
      choSexo.addItem(sexo.getDS());
    }
    choSexo.select(0);
    this.add(choSexo, new XYConstraints(pX, pY, longX, longY));

    pX += longX + 10 - 2;
    longX = 55;
    this.add(lblTelefono, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5 - 4;
    longX = 75;
    txtTelefono.setEditable(false);
    txtTelefono.setEnabled(false);
    this.add(txtTelefono, new XYConstraints(pX, pY, longX, longY));

    // Panel suca
    boolean bTramero = true; // E 17/02/2000
    boolean bSeleccionPais = false;
    LSuca.addElement(LPaises);

    // Trampa --> Madrid viene la 1� y falla
    //datasuca d = (datasuca) LCA.firstElement();
    //LCA.removeElementAt(0);
    //LCA.insertElementAt(d, 12);
    //
    LSuca.addElement(LCA);
    LSuca.addElement(new CLista());
    LSuca.addElement(new CLista());
    LSuca.addElement(new CLista());

    modoSuca = panelsuca.modoINICIO;
    if (this.getApp().getIT_FG_ENFERMO().equals("N")) {
      modoSuca = panelsuca.modoCONFIDENCIAL;

    }
    psuca = new panelsuca(this.getApp(), bTramero, modoSuca, bSeleccionPais,
                          LSuca, this);

    // E 17/02/2000 psuca = new panelsuca(this.getApp(),bTramero,modoSuca,bSeleccionPais,this);
    psuca.setModoDeshabilitado();
    pX = 15;
    longX = 596;
    pY += 30;
    longY = 116;
    this.add(psuca, new XYConstraints(pX, pY, longX, longY));

    // Equipo Notificador
    pX = 15;
    longX = 70;
    pY += 116;
    longY = -1;
    this.add(lblEquipoNotif, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 55;
    this.add(txtEquipoNotif, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnLupaEquipoNotif, new XYConstraints(pX, pY, longX, longY));
    btnLupaEquipoNotif.setImage(imgs.getImage(0));
    pX += longX + 10;
    longX = 120;
    this.add(txtDescEquipoNotif, new XYConstraints(pX, pY, longX, longY));

    actionAdapter = new Panel_btn_actionAdapter(this);
    focusAdapter = new PanelfocusAdapter(this);
    textAdapter = new PaneltextAdapter(this);
    txtEquipoNotif.setName("equipo");
    txtEquipoNotif.addFocusListener(focusAdapter);
    txtEquipoNotif.addKeyListener(textAdapter);
    btnLupaEquipoNotif.addActionListener(actionAdapter);

    // Medico Facultativo
    pX += longX + 10;
    longX = 93;
    longY = -1;
    this.add(lblMedFac, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 183;
    this.add(txtMedFac, new XYConstraints(pX, pY, longX, longY));

    //nivel1 - 2
    pX = 15;
    longX = 93;
    longY = -1;
    pY += 35;
    this.add(lblZonaGestion, new XYConstraints(pX, pY, longX, longY));
    panNiv = new panelNiveles(this.app, this,
                              panelNiveles.MODO_INICIO,
                              panelNiveles.TIPO_NIVEL_3,
                              panelNiveles.LINEAS_1, true);
    //el ultimo argumento tiene que ser true
    //para que filtre por las autorizaciones
    //false: no filtra
    pX = 15;
    longX = 655;
    pY += 25;
    longY = 40;
    this.add(panNiv, new XYConstraints(pX, pY, longX, longY));
    //obligados
    panNiv.setNivelObligatorio(1, true);

    switch (this.app.getPerfil()) {
      case 4:
      case 5:
        panNiv.setNivelObligatorio(2, true);
        break;
      case 1:
      case 2:
      case 3:
        break;
    }

    // Observaciones
    pX = 15;
    longX = 90;
    pY += 35;
    longY = -1;
    this.add(lblObserv, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 491;
    pY += 5;
    longY = 75;
    this.add(txtaObserv, new XYConstraints(pX, pY, longX, longY));
  } // Fin jbInit()

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar(modo)

  // Implementacion del metodo abstracto de Cpanel
  public void Inicializar() {

    switch (modoOperacion) {
      case modoMODIFICACION:

        // Habilitar componentes del panel

        // Equipo Notificador
        txtEquipoNotif.setEnabled(true);
        txtEquipoNotif.setEditable(true);
        btnLupaEquipoNotif.setEnabled(true);
        txtDescEquipoNotif.setEnabled(true);
        txtDescEquipoNotif.setEditable(true);
        txtEquipoNotif.doLayout();

        // Medico Facultativo
        txtMedFac.setEnabled(true);
        txtMedFac.setEditable(true);

        // Observaciones
        txtaObserv.setEnabled(true);
        txtaObserv.setEditable(true);

        // Habilitar componentes del panel suca
        psuca.setModoDeshabilitado();
        panNiv.setModoNormal();
        // Cursor normal
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoDESHABILITADO:

        // DesHabilitar componentes del panel

        // Equipo Notificador
        txtEquipoNotif.setEnabled(false);
        txtEquipoNotif.setEditable(false);
        btnLupaEquipoNotif.setEnabled(false);
        txtDescEquipoNotif.setEnabled(false);
        txtDescEquipoNotif.setEditable(false);
        txtEquipoNotif.doLayout();

        // Medico Facultativo
        txtMedFac.setEnabled(false);
        txtMedFac.setEditable(false);

        // Observaciones
        txtaObserv.setEnabled(false);
        txtaObserv.setEditable(false);

        // Habilitar componentes del panel suca
        psuca.setModoDeshabilitado();
        panNiv.setModoDeshabilitado();
        break;

      case modoESPERA:

        // Deshabilitar componentes del panel

        // Equipo Notificador
        txtEquipoNotif.setEnabled(false);
        txtEquipoNotif.setEditable(false);
        btnLupaEquipoNotif.setEnabled(false);
        txtDescEquipoNotif.setEnabled(false);
        txtDescEquipoNotif.setEditable(false);

        // Medico Facultativo
        txtMedFac.setEnabled(false);
        txtMedFac.setEditable(false);

        // Observaciones
        txtaObserv.setEnabled(false);
        txtaObserv.setEditable(false);

        // Deshabilitar componentes del panel suca
        psuca.setModoEspera();
        panNiv.setModoEspera();

        // Cursor de espera
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }

    // Repintado de pantalla
    this.doLayout();
  } // Fin Inicializar()

// ******** M�todo de la interfaz de suca  (28-03-01) **********

  // b�squeda de la zonificaci�n sanitaria de un municipio
  public void setZonificacionSanitaria(String mun, String prov) {}

// *****************************

  void txtEquipo_keyPressed() {

    txtDescEquipoNotif.setText("");
    bEquipoValid = false;

  }

  public void txtEquipo_focusLost() {

    DatEqPrim data;
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    dlg.Inicializar(modoOperacion);

    if (bEquipoValid) {
      modoOperacion = modo;
      dlg.Inicializar(modoOperacion);
      return;
    }

    if (txtEquipoNotif.getText().trim().equals("")) {

      bEquipoValid = false;
      modoOperacion = modo;
      dlg.Inicializar(modoOperacion);
      return;
    }
    // Comprueba que datos introducidos son v�lidos si no se han
    // seleccionado de los popup
    CLista listaSalida = null;
    data = new DatEqPrim("",
                         "",
                         txtEquipoNotif.getText().trim(), "",
                         (Vector)this.getApp().getCD_NIVEL_1_AUTORIZACIONES(),
                         (Vector)this.getApp().getCD_NIVEL_2_AUTORIZACIONES());

    CLista parametros = new CLista();
    parametros.addElement(data);

    try {

      listaSalida = Comunicador.Communicate(this.app,
                                            stubCliente,
                                            constantes.sOBTENER_X_CODIGO,
                                            strSERVLET,
                                            parametros);

      if (listaSalida.size() == 0) {
        ShowWarning("No hay datos con el criterio informado.");
        txtEquipoNotif.setText("");
        bEquipoValid = false;

        modoOperacion = modo;
        dlg.Inicializar(modoOperacion);

        return;
      }

    }
    catch (Exception e) { //acceso a base de datos mal
      e.printStackTrace();
      ShowWarning(e.getMessage());
      bEquipoValid = false;

      //reseteo
      txtEquipoNotif.setText("");
      txtEquipoNotif.select(txtEquipoNotif.getText().length(),
                            txtEquipoNotif.getText().length());
      txtEquipoNotif.requestFocus();

      modoOperacion = modo;
      dlg.Inicializar(modoOperacion);
      return;
    } //fin del try

    //si todo fue bien
    data = (DatEqPrim) listaSalida.elementAt(0);

    txtEquipoNotif.setText(data.getCD_E_NOTIF().trim());
    txtDescEquipoNotif.setText(data.getDS_E_NOTIF().trim());

    // si no ha salido por ninguna de las condiciones anteriores,
    // el equipo el v�lido, y se pone su boolean a true
    bEquipoValid = true;
    modoOperacion = modo;
    dlg.Inicializar(modoOperacion);

  } //fin focus de equipo

  void btnEquipo_actionPerformed() {
    DatEqPrim data;
    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    dlg.Inicializar(modoOperacion);

    try {
      CListaEquipos lista = new CListaEquipos(this,
                                              "Selecci�n de Equipo de Primaria",
                                              stubCliente,
                                              strSERVLET,
                                              constantes.sOBTENER_X_CODIGO,
                                              constantes.sOBTENER_X_DESCRIPCION,
                                              constantes.sSELECCION_X_CODIGO,
                                              constantes.
                                              sSELECCION_X_DESCRIPCION);

      if (lista.getHayDatos() == 0) {
        modoOperacion = modo;
        dlg.Inicializar(modoOperacion);
        return;
      }

      lista.show();
      data = (DatEqPrim) lista.getComponente();

      if (data != null) {

        txtEquipoNotif.setText(data.getCD_E_NOTIF().trim());
        txtDescEquipoNotif.setText(data.getDS_E_NOTIF().trim());
        bEquipoValid = true;
      }

    }
    catch (Exception er) {
      er.printStackTrace();
      ShowWarning(er.getMessage());
      txtEquipoNotif.setText("");
      txtDescEquipoNotif.setText("");
      bEquipoValid = false;
    }

    modoOperacion = modo;
    dlg.Inicializar(modoOperacion);

  }

  public void setModoModificacion() {
    Inicializar(modoMODIFICACION);
  } // Fin setModoModificacion()

  public void setModoDeshabilitado() {
    Inicializar(modoDESHABILITADO);
  } // Fin setModoDeshabilitado()

  //interface usaPanelNiveles
  public void cambioNivelAntesInformado(int nivel) {
    //ya no es necesario que cuando haya cambios en los niveles
    //se resetee el suca
    /*
           suca.setmodoOperacion(suca.modoINICIO);
           suca.setCD_CA("");
     */
  }

  // Recibe los datos que debe pintar en los campos
  public void RellenarDatos(DatDiaMntRegTub data) {
    String sexo = null;

    // CodEnfermo, apellidos, nombre
    txtCodEnfermo.setText(data.getCD_ENFERMO());
    txtApe1.setText(data.getDS_APE1());
    txtApe2.setText(data.getDS_APE2());
    txtNombre.setText(data.getDS_NOMBRE());

    // Fecha de nacimiento + Edad
    txtFechaNac.setText(data.getFC_NAC());
    java.util.Date dFecha = Fechas.string2Date(txtFechaNac.getText());
    if (dFecha != null) {
      if (data.getIT_FG_CALC().equals("S")) {
        lblReq.setText("calc");
      }
      else if (data.getIT_FG_CALC().equals("N")) {
        lblReq.setText("real");
      }
    }
    if (dFecha != null) {
      if (Fechas.edadTipo(dFecha)) { // Edad en a�os
        choEdad.select(0);
        txtEdad.setText( (new Integer(Fechas.edadAnios(dFecha))).toString());
      }
      else { // Edad en meses
        choEdad.select(1);
        txtEdad.setText( (new Integer(Fechas.edadMeses(dFecha))).toString());
      }
    }

    // Sexo
    sexo = data.getCD_SEXO();
    if (!sexo.equals("")) {
      // Seleccion del sexo en el choice
      selectSexo(sexo);
    }

    // Telefono
    txtTelefono.setText(data.getDS_TELEF());

    // Suca
    psuca.setCD_CA(data.getDS_CA());

    try {
      Thread.sleep(500);
    }
    catch (Exception exp) {
    }

    psuca.setCD_PROV(data.getDS_PROV());
    psuca.setCD_MUN(data.getCD_MUN());
    psuca.setDS_MUN(data.getDS_MUN());
    psuca.setDS_DIREC(data.getDS_DIREC());
    psuca.setDS_NUM(data.getDS_NUM());
    psuca.setDS_PISO(data.getDS_PISO());
    psuca.setCD_POSTAL(data.getCD_POSTAL());

    /////niveles
    String cdNivel1 = "";
    String cdNivel2 = "";
    String dsNivel1 = "";
    String dsNivel2 = "";

    if (data.getCD_NIVEL_1_GE() != null) {
      cdNivel1 = data.getCD_NIVEL_1_GE();
      panNiv.setModoOperacion(panNiv.MODO_N1);
    }
    panNiv.setCDNivel1(cdNivel1);

    if (data.getDS_NIVEL_1_GE() != null) {
      dsNivel1 = data.getDS_NIVEL_1_GE();
    }
    panNiv.setDSNivel1(dsNivel1);

    if (data.getCD_NIVEL_2_GE() != null) {
      cdNivel2 = data.getCD_NIVEL_2_GE();
      panNiv.setModoOperacion(panNiv.MODO_N2);
    }
    panNiv.setCDNivel2(cdNivel2);

    if (data.getDS_NIVEL_2_GE() != null) {
      dsNivel2 = data.getDS_NIVEL_2_GE();
    }
    panNiv.setDSNivel2(dsNivel2);

    /////

    // Equipo Notificador (codigo y descripcion)
    txtEquipoNotif.setText(data.getCD_E_NOTIF());
    txtDescEquipoNotif.setText(data.getDS_E_NOTIF());
    if (!txtDescEquipoNotif.getText().equals("")) {
      bEquipoValid = true;

      // Nombre facultativo
    }
    txtMedFac.setText(data.getDS_MEDICO());

    // Observaciones
    txtaObserv.setText(data.getDS_OBSERV());

    // Confidencialidad
    if (this.getApp().getIT_FG_ENFERMO().equals("N")) {
      Confidencial(data.getSIGLAS());
    }
  } // Fin RellenarDatos()

  // Rellena el sexo
  void selectSexo(String sexo) {
    if (sexo.equals("")) {
      choSexo.select(0);
    }
    else {
      for (int i = 0; i < LSexos.size(); i++) {
        DatCatRegTub sex = (DatCatRegTub) LSexos.elementAt(i);
        if (sex.getCD().trim().equals(sexo.trim())) {
          choSexo.select(i + 1);
          break;
        }
      }
    }
  } // Fin  selectSexo()

  // Obtiene el valor de txtEquipoNotif
  public String getEquipoNotif() {
    String res = txtEquipoNotif.getText();
    if (res != null) {
      return res;
    }
    else {
      return "";
    }
  } // Fin getEquipoNotif()

  // Obtiene el valor de txtMedFac
  public String getMedFac() {
    String res = txtMedFac.getText();
    if (res != null) {
      return res;
    }
    else {
      return "";
    }
  } // Fin getMedFac()

  // Obtiene el valor de txtaObserv
  public String getObserv() {
    String res = txtaObserv.getText();
    if (res != null) {
      return res;
    }
    else {
      return "";
    }
  } // Fin getObserv()

  // Restricci�n de confidencialidad
  // si el usuario accede al modo modificaci�n y bFlagEnfermo = false:
  // no puede ver el nombre / apellidos, se ver�n las iniciales
  public void Confidencial(String siglas) {
    txtApe2.setVisible(false);
    txtNombre.setVisible(false);
    txtApe1.setText(siglas);

    lblTelefono.setVisible(false);
    txtTelefono.setVisible(false);
  } // Confidencial()

  // Metodos de la interfaz zonificacionSanitaria
  public void setZonificacionSanitaria(String mun) {}

  // No implementado: siempre va a estar en modo sin tramero
  public void cambiaModoTramero(boolean b) {}

  public int ponerEnEspera() {
    int m = modoOperacion;
    //iAntesDeZona = m; //para saber en setZona(suca)...si alta o modif
    modoOperacion = modoESPERA;
    dlg.Inicializar(modoOperacion); //mlm nuevo
    return m;
  }

  public void ponerModo(int modo) {
    modoOperacion = modo;
    dlg.Inicializar(modoOperacion); //mlm nuevo
  }

  public void vialInformado() {}

  // Fin Metodos de la interfaz zonificacionSanitaria

  public void activaObservaciones() {
    txtaObserv.setEnabled(true);
    txtaObserv.setEditable(true);
  }

  public boolean validarDatos() {

    boolean correcto = true;

    //1. perdidas de foco
    if (!txtEquipoNotif.getText().trim().equals("") &&
        !bEquipoValid) {
      return (false);
    }
    if (!panNiv.getCDNivel1().equals("")) {
      if (panNiv.getDSNivel1().equals("")) {
        panNiv.setDSNivel1("");
        panNiv.requestFocus();
        return false;
      }
    }
    if (!panNiv.getCDNivel2().equals("")) {
      if (panNiv.getDSNivel2().equals("")) {
        panNiv.setDSNivel2("");
        panNiv.requestFocus();
        return false;
      }
    }
    //2. longitudes
    //suca (numericos y longitudes)
    //3. Valores numericos
    //Campos obligatorios
    if (panNiv.getCDNivel1().trim().equals("")) {
      dlg.ShowError("Es necesario introducir el campo " + this.app.getNivel1() +
                    "");
      panNiv.requestFocus();
      return false;
    }
    switch (this.app.getPerfil()) {
      case 4:
      case 5:
        if (panNiv.getCDNivel2().trim().equals("")) {
          dlg.ShowError("Es necesario introducir el campo " +
                        this.app.getNivel2() + "");
          panNiv.requestFocus();
          return false;
        }
        break;
      case 1:
      case 2:
      case 3:
        break;
    }

    return correcto;

  }

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  public void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  } // Fin ShowWarning()

} // endclass PanModRegTub

//EQUIPOS *****
class CListaEquipos
    extends CListaValores {

  protected PanModRegTub panel;

  public CListaEquipos(PanModRegTub p,
                       String title,
                       StubSrvBD stub,
                       String servlet,
                       int obtener_x_codigo,
                       int obtener_x_descripcion,
                       int seleccion_x_codigo,
                       int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    //sin autorizaciones (de momento: 9/12/99)
    return new DatEqPrim("",
                         "",
                         s, "",
                         (Vector) panel.getApp().getCD_NIVEL_1_AUTORIZACIONES(),
                         (Vector) panel.getApp().getCD_NIVEL_2_AUTORIZACIONES());
  }

  public String getCodigo(Object o) {
    return ( ( (DatEqPrim) o).getCD_E_NOTIF());
  }

  public String getDescripcion(Object o) {
    return ( ( (DatEqPrim) o).getDS_E_NOTIF());
  }
} //EQUIPOS

class Panel_btn_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanModRegTub adaptee;
  ActionEvent e;

  Panel_btn_actionAdapter(PanModRegTub adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent evt) {
    e = evt;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btnEquipo_actionPerformed();
  }
}

// escuchador para las cajas de texto
class PaneltextAdapter
    extends java.awt.event.KeyAdapter {
  PanModRegTub adaptee;

  PaneltextAdapter(PanModRegTub adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    if ( ( (TextField) e.getSource()).getName().equals("equipo")) {
      adaptee.txtEquipo_keyPressed();
    }
  }
}

class PanelfocusAdapter
    implements java.awt.event.FocusListener, Runnable {
  PanModRegTub adaptee;
  FocusEvent evt;

  PanelfocusAdapter(PanModRegTub adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void focusGained(FocusEvent e) {

  }

  public void run() {
    if ( ( (TextField) evt.getSource()).getName().equals("equipo")) {
      adaptee.txtEquipo_focusLost();
    }
  }
}
