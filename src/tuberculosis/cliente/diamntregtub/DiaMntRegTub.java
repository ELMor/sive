/**
 * Clase: DiaMntRegTub
 * Paquete: SP_Tuberculosis.cliente.diamntregtub.DiaMntRegTub
 * Hereda: CDialog
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 04/11/1999
 * Analisis Funcional: Punto 2. Mantenimiento Registro Tuberculosis.
 * Descripcion: Implementacion del dialogo que permite visualizar los datos
    principales de un registro de tuberculosis para poder ser modificados.
    Incluye los paneles PanAnoReg y PanModRegTub, m�s componentes propios.
 * Modificaciones:
    29/12/1999 - Emilio Postigo Riancho
    - Se tiene en cuenta el manejo de bloqueos en lo referente
      a la actualizaci�n de la tabla SIVE_REGISTROTBC
     - Se cambia, si procede, el concepto de 'entrada en el registro de tuberculosis'
      por 'fecha de inicio tratamiento'
 */

package tuberculosis.cliente.diamntregtub;

import java.util.Hashtable;

import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import comun.Common;
import comun.Comunicador;
import comun.constantes;
import sapp.StubSrvBD;
import tuberculosis.datos.diamntregtub.DatDiaMntRegTub;
import tuberculosis.datos.mantregtub.DatCatRegTub;

//import tuberculosis.servidor.diamntregtub.*;//quitar

public class DiaMntRegTub
    extends CDialog {

  // Para almacenar el �rea y distrito del caso pasada como par�metro
  // en el alta de caso
  String sCdAreaCaso = null;
  String sDsAreaCaso = null;
  String sCdDistCaso = null;
  String sDsDistCaso = null;

  public int iERROR = -1; //0: error

  // Localizacion del servlet (datos de un registro tub. individual)

  //  private final String strSERVLET_DIAREGTUB = "servlet/SrvDiaMntRegTub";
  private final String strSERVLET_DIAREGTUB = constantes.strSERVLET_DIAREGTUB;

  // Modos de operacion del servlet SrvDiaMntRegTub
  private final int servletSELECT = constantes.modoSELECT;
  private final int servletUPDATE = constantes.modoMODIFICACION;
  private final int servletUPDATE_SB = constantes.modoMODIFICACION_SB;

  // Datos de un registro de tuberculosis
  DatDiaMntRegTub dataParametros = null;
  DatDiaMntRegTub dataResultado = null;

  // Registro sobre el que se trabaja
  String ano;
  String registro;

  // Listas de catalogos
  Hashtable catalogos = null;

  // CLista de Motivos de salida:
  CLista LMotSalida = null;

  // Contenedor de im�genes
  protected CCargadorImagen imgs = null;

  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

  // Inclusion de paneles
  public PanAnoReg pAnoReg = null;
  public PanModRegTub pModRegTub = null;

  // Applet del que procede este dialogo
  //CApp capp;

  // Modo del dialogo
  private static final int modoMANTENIMIENTO = constantes.modoMANTENIMIENTO;
  private static final int modoCERRAR = constantes.modoCERRAR;
  private static final int modoDESHACERCIERRE = constantes.modoDESHACERCIERRE;
  private static final int modoALTADESDECASO = constantes.modoALTADESDECASO;
  private int modoEntrada = modoMANTENIMIENTO;

  // Modo de operaci�n del di�logo
  private static final int modoESPERA = constantes.modoESPERA;
  private static final int modoMODIFICACION = constantes.modoMODIFICACION;
  private static final int modoCONSULTA = constantes.modoCONSULTA;
  private static final int modoALTA = constantes.modoALTA;
  private int modoOperacion = modoMODIFICACION;

  // Constantes de visibilidad y habilitacion de los datos del cierre
  final boolean VISIBLE = true;
  final boolean NOVISIBLE = false;
  final boolean HABILITADO = true;
  final boolean NOHABILITADO = false;

  /* -------------------- CONTROLES ------------------------- */

  // Organizacion del panel
  private XYLayout lyXYLayout = new XYLayout();

  // Cierre
  Label lblCierre = new Label("Cierre");
  Checkbox chkCierre = new Checkbox();

  // Fecha salida
  Label lblFechaSalida = new Label("F.Salida");
  // Cambio barras autom�ticas (22-05-01 ARS)
//  CFechaSimple txtFechaSalida = new CFechaSimple("N");
  fechas.CFecha txtFechaSalida = new fechas.CFecha("N");

  // Motivo de salida
  Label lblMotSalida = new Label("Mot. Salida");
  Choice choMotSalida = new Choice();

  // Botones de grabar y cancelar
  ButtonControl btnGrabar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores
  DiaMntRegTubActionAdapter actionAdapter = null;

  // Constructor usado para crear una instancia del di�logo
  // en el momento de dar de alta el caso
  // Se pasa como par�metro, dentro de la Hashtable datos, el
  // �rea asociada al caso.
  public DiaMntRegTub(CApp a, int modoVentana,
                      String cd_ano, String cd_reg, Hashtable cat,
                      int modoOpe, String fInicioTratamiento, Hashtable datos) {

    this(a, modoVentana, cd_ano, cd_reg, cat, modoOpe, fInicioTratamiento);

    if (datos != null) {

      sCdAreaCaso = (String) datos.get("CD_AREA_CASO");
      sDsAreaCaso = (String) datos.get("DS_AREA_CASO");
      sCdDistCaso = (String) datos.get("CD_DISTRITO_CASO");
      sDsDistCaso = (String) datos.get("DS_DISTRITO_CASO");

      if (sCdAreaCaso != null) {
        pModRegTub.panNiv.setCDNivel1(sCdAreaCaso);
      }
      if (sDsAreaCaso != null) {
        pModRegTub.panNiv.setDSNivel1(sDsAreaCaso);
      }
      if (sDsDistCaso != null) {
        pModRegTub.panNiv.setDSNivel2(sDsDistCaso);
      }
      if (sCdDistCaso != null) {
        pModRegTub.panNiv.setCDNivel2(sCdDistCaso);
      }

    }
  }

  // Constructor
  public DiaMntRegTub(CApp a, int modoVentana,
                      String cd_ano, String cd_reg, Hashtable cat,
                      int modoOpe, String fInicioTratamiento) {

    super(a);
    try {
      this.app = a;
      modoEntrada = modoVentana;
      ano = cd_ano;
      registro = cd_reg;
      catalogos = cat;
      modoOperacion = modoOpe;

      // Paneles
      pAnoReg = new PanAnoReg(this, ano, registro);
      pAnoReg.setModoDeshabilitado();
      pModRegTub = new PanModRegTub(this, catalogos);

      // Titulo: se obtiene a partir del modo de entrada
      switch (modoEntrada) {
        case modoMANTENIMIENTO:
        case modoALTADESDECASO:
          this.setTitle("Modificaci�n de Registros de Tuberculosis");
          break;
        case modoDESHACERCIERRE:
          this.setTitle("Apertura de Registros de Tuberculosis");
          break;
        case modoCERRAR:
          this.setTitle("Cierre de Registros de Tuberculosis");
          break;
      }

      jbInit();

      String FConf;
      if ( (this.getCApp().getIT_FG_ENFERMO()).equals("S")) {
        FConf = "N";
      }
      else {
        FConf = "S";

        // Carga de los datos del registro con el que se abre el di�logo
      }
      dataParametros = new DatDiaMntRegTub(cd_ano, cd_reg, "", "", "", "", "",
                                           "", "", "", "", "", "", "", "", "",
                                           "", "", "", "", "", "",
                                           "", "", "", "", "", FConf, "", "",
                                           "", "", "");

      dataResultado = CargarRegTub(dataParametros); // E 18/02/2000
      if (modoEntrada != modoALTADESDECASO) {
        // dataResultado = CargarRegTub(dataParametros);
        // Relleno de la fecha de entrada del panel PanAnoReg
        pAnoReg.setFechaEntrada(dataResultado.getFC_INIRTBC()); // E 17/02/2000
        RellenarDatos(dataResultado);
      }
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
      this.dispose();
      this.iERROR = 0;
    }

  } // Fin constructor

  void jbInit() throws Exception {

    // Constantes de pintado del di�logo
    int iANCHO = 625;
    int iALTO = 500;

    // Vars. para colocar los componentes
    int pX = 0;
    int pY = 0;
    int longX = 0;
    int longY = 0;

    // Medidas y organizacion del panel
    this.setSize(new Dimension(iANCHO, iALTO)); //625, 465
    lyXYLayout.setHeight(iALTO);
    lyXYLayout.setWidth(iANCHO);
    this.setLayout(lyXYLayout);

    // Carga de las im�genes
    imgs = new CCargadorImagen(getCApp(), imgNAME);
    imgs.CargaImagenes();

    // Escuchadores
    chkCierre.addItemListener(new DiaMntRegTub_chkCierre_itemAdapter(this));
    actionAdapter = new DiaMntRegTubActionAdapter(this);

    // Panel A�oReg
    pX = 0;
    longX = iANCHO;
    pY += 0;
    longY = 40;
    this.add(pAnoReg, new XYConstraints(pX, pY, longX, longY));

    // Panel ModRegTub
    pX = 0;
    longX = iANCHO;
    pY += longY;
    longY = 400;
    this.add(pModRegTub, new XYConstraints(pX, pY, longX, longY));

    // Cierre
    pX = 15;
    longX = 33;
    pY += longY;
    longY = -1;
    this.add(lblCierre, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = -1;
    this.add(chkCierre, new XYConstraints(pX, pY, longX, longY));

    // Fecha de salida
    pX += 29;
    longX = 44;
    this.add(lblFechaSalida, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 75;
    this.add(txtFechaSalida, new XYConstraints(pX, pY, longX, longY));

    // Motivo de salida
    pX += longX + 5;
    longX = 60;
    this.add(lblMotSalida, new XYConstraints(pX, pY, longX, longY));

    LMotSalida = (CLista) catalogos.get("MOTIVOS_SALIDA");
    choMotSalida.addItem("");
    for (int i = 0; i < LMotSalida.size(); i++) {
      DatCatRegTub motivo = (DatCatRegTub) LMotSalida.elementAt(i);
      choMotSalida.addItem(motivo.getCD() + " - " + motivo.getDS());
    }

    // Se obliga al usuario a introducir un motivo
    choMotSalida.setBackground(new Color(255, 255, 150));

    //choMotSalida.select(El que venga del servlet);
    pX += longX + 3;
    longX = 151;
    this.add(choMotSalida, new XYConstraints(pX, pY, longX, longY));

    // Boton de grabar
    btnGrabar.setImage(imgs.getImage(0));
    btnGrabar.setLabel("Grabar");
    pX += longX + 10;
    longX = 80;
    this.add(btnGrabar, new XYConstraints(pX, pY, longX, longY));
    btnGrabar.setActionCommand("Grabar");
    btnGrabar.addActionListener(actionAdapter);

    // Boton de cancelar
    btnCancelar.setImage(imgs.getImage(1));
    btnCancelar.setLabel("Cancelar");
    pX += longX + 10 - 2;
    longX = 80;
    this.add(btnCancelar, new XYConstraints(pX, pY, longX, longY));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);

  } // Fin jbInit()

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar(modo)

  public void Inicializar() {
    switch (modoOperacion) {

      case modoESPERA:
        pModRegTub.setModoDeshabilitado();
        pAnoReg.setModoDeshabilitado();

        chkCierre.setEnabled(false);
        if (chkCierre.getState()) {
          setVisibleDatosCierre(VISIBLE, NOHABILITADO);

        }
        btnGrabar.setEnabled(false);
        btnCancelar.setEnabled(false);

        // Cursor de espera
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoMODIFICACION:
        if (modoEntrada == modoCERRAR
            || modoEntrada == modoDESHACERCIERRE) {
          pModRegTub.setModoDeshabilitado();
          // Activamos la parte de observaciones.
          if (modoEntrada == modoCERRAR) {
            pModRegTub.activaObservaciones();

          }
          pAnoReg.setModoDeshabilitado();

          chkCierre.setVisible(true);
          chkCierre.setEnabled(false);

          if (modoEntrada == modoDESHACERCIERRE) { // ModoDESHACERCIERRE
            chkCierre.setState(false);
            setVisibleDatosCierre(VISIBLE, NOHABILITADO);
          }
          else { // ModoCERRAR
            chkCierre.setState(true);
            txtFechaSalida.setText(this.app.getFC_ACTUAL());
            setVisibleDatosCierre(VISIBLE, HABILITADO);
          }

        }
        else { // ModoMANTENIMIENTO
          lblCierre.setVisible(false);
          chkCierre.setVisible(false);
          chkCierre.setEnabled(false);

          setVisibleDatosCierre(NOVISIBLE, NOHABILITADO);
          pModRegTub.setModoModificacion();
          pAnoReg.setModoModificacion();
        }

        btnGrabar.setEnabled(true);
        btnCancelar.setEnabled(true);

        // Cursor normal
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoCONSULTA:
        if (modoEntrada == modoCERRAR
            || modoEntrada == modoDESHACERCIERRE) { //No se debe entrar, solo admin

          pModRegTub.setModoDeshabilitado();
          pAnoReg.setModoDeshabilitado();

          chkCierre.setVisible(false);
          chkCierre.setEnabled(false);

          if (chkCierre.getState()) { // ModoDESHACERCIERRE
            setVisibleDatosCierre(VISIBLE, NOHABILITADO);
          }
          else { // ModoCERRAR
            setVisibleDatosCierre(VISIBLE, NOHABILITADO);
          }
        }
        else { // ModoMANTENIMIENTO
          lblCierre.setVisible(false);
          chkCierre.setVisible(false);
          chkCierre.setEnabled(false);
          setVisibleDatosCierre(NOVISIBLE, NOHABILITADO);
          pModRegTub.setModoDeshabilitado();
          pAnoReg.setModoDeshabilitado();
        }

        btnGrabar.setEnabled(false);
        btnCancelar.setEnabled(true);

        // Cursor normal
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

    } // Fin switch

    //botones
    if (modoOperacion == modoCONSULTA) {
      btnGrabar.setLabel("Aceptar");
    }
    else {
      btnGrabar.setLabel("Grabar");

    }
    btnCancelar.setLabel("Cancelar");
  } // Fin Inicializar()

  // Establece la visibilidad de los componentes del cierre
  void setVisibleDatosCierre(boolean visible, boolean habilitado) {
    if (visible) {
      lblFechaSalida.setVisible(true);
      txtFechaSalida.setVisible(true);
      lblMotSalida.setVisible(true);
      choMotSalida.setVisible(true);
      if (habilitado) {
        txtFechaSalida.setEnabled(true);
        txtFechaSalida.setEditable(true);
        choMotSalida.setEnabled(true);
      }
      else {
        txtFechaSalida.setEnabled(false);
        txtFechaSalida.setEditable(false);
        choMotSalida.setEnabled(false);
      }
    }
    else {
      lblFechaSalida.setVisible(false);
      txtFechaSalida.setVisible(false);
      lblMotSalida.setVisible(false);
      choMotSalida.setVisible(false);
    }
  } // Fin setVisibleDatosCierre()

  private DatDiaMntRegTub CargarRegTub(DatDiaMntRegTub dataParametros) throws
      Exception {
    CLista parametros = new CLista();
    CLista listaSalida = new CLista();
    StubSrvBD stubCliente = new StubSrvBD();

    parametros.addElement(dataParametros);
    try {
      listaSalida = Comunicador.Communicate(this.app,
                                            stubCliente,
                                            servletSELECT,
                                            strSERVLET_DIAREGTUB,
                                            parametros);

      /*SrvDiaMntRegTub srv = new SrvDiaMntRegTub();
             listaSalida = srv.doPrueba(servletSELECT, parametros);*/

      if (listaSalida != null) {
        if (listaSalida.size() > 0) {
          // Se devuelven los datos del registro
          return ( (DatDiaMntRegTub) listaSalida.elementAt(0));
        }
        else {
          ShowWarning("No existe el registro de tuberculosis indicado.");
        }
      }
      else {
        ShowWarning(
            "Error al recuperar los datos del registro de tuberculosis.");
      }

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      ShowWarning("Error al recuperar los datos del registro de tuberculosis.");
      this.dispose();
    }

    // Si no se puede recuperar el registro elegido se sale de la pantalla
    this.dispose();

    // Se devuelve null
    return null;
  } // Fin CargarRegTub()

  void RellenarDatos(DatDiaMntRegTub data) {
    // Relleno de los datos del panel PanModRegTub
    pModRegTub.RellenarDatos(data);

    // Relleno de los componentes propios del dialogo (parte inferior)

    String fSal = null;
    String motSal = null;

    switch (modoEntrada) {
      case modoMANTENIMIENTO:

        // Los campos inferiores no se pintan
        break;
      case modoCERRAR:

        // Se chequea el chek de Cierre
        chkCierre.setState(false);

        break;
      case modoDESHACERCIERRE:

        // Se chequea el chek de Cierre
        chkCierre.setState(true);

        // Se rellena la fecha de salida
        fSal = data.getFC_SALRTBC();
        if (!fSal.equals("")) {
          txtFechaSalida.setText(fSal);
        }

        // Se rellena el motivo de salida
        motSal = data.getCD_MOTSALRTBC();
        if (!motSal.equals("")) {
          // Seleccion del motivo de salida en el choice
          selectMotivoSalida(motSal);
        }

        break;
    } // Fin switch
  } // Fin RellenarDatos()

  void selectMotivoSalida(String motSal) {
    if (motSal.equals("")) {
      choMotSalida.select(0);
    }
    else {
      for (int i = 0; i < LMotSalida.size(); i++) {
        DatCatRegTub motivo = (DatCatRegTub) LMotSalida.elementAt(i);
        if (motivo.getCD().trim().equals(motSal.trim())) {
          choMotSalida.select(i + 1);
          break;
        }
      }
    }
  } // Fin  selectMotivoSalida()

  /*************** FUNCIONES AUXILIARES ***********************/

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  public void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  } // Fin ShowWarning()

  // Llama a CMessage para mostrar el mensaje de error sMessage
  public void ShowError(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgERROR, sMessage);
    msgBox.show();
    msgBox = null;
  } // Fin ShowError()

  /******************** Manejadores *************************/

  // Manejador de chkCierre
  void chkCierre_itemStateChanged(ItemEvent e) {
    if (chkCierre.getState()) {
      // Habilitar resto campos cierre
      txtFechaSalida.setText("");
      txtFechaSalida.setEnabled(true);
      txtFechaSalida.setEditable(true);
      txtFechaSalida.setText(this.app.getFC_ACTUAL());
      choMotSalida.setEnabled(true);
    }
    else {
      // DesHabilitar resto campos cierre
      txtFechaSalida.setText("");
      txtFechaSalida.setEnabled(false);
      txtFechaSalida.setEditable(false);
      choMotSalida.setEnabled(false);
      choMotSalida.select(0);
    }
  }

  void btnGrabarActionPerformed() {
    // Variables de relleno de la lista de parametros
    String sEqNotif = "";
    String sMedFac = "";
    String sObserv = "";
    String sFSalida = "";
    String sMotSalida = "";
    String sFEntrada = "";
    String sN1 = "";
    String sN2 = "";

    // Para saber si todo ha ido bien
    boolean bOK = false;

    // Modo ESPERA
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    if (!pModRegTub.validarDatos()) {
      Inicializar(modo);
      return;
    }

    // Se comprueba la fecha de entrada
    if (pAnoReg.chequearFechaEntrada() == null) {
      Common.ShowError(this.app, "Introduzca una fecha de entrada v�lida");
      Inicializar(modo);
      return;
    }
    else {
      if ( (new java.util.Date()).before(pAnoReg.chequearFechaEntrada())) {
        Common.ShowError(this.app,
                         "La fecha de entrada no puede ser menor que la actual");
        Inicializar(modo);
        return;
      }
    }

    // Para comprobar que se ha introducido un motivo
    // de salida

    if (modoEntrada == modoCERRAR) {
      if (choMotSalida.getSelectedIndex() == 0) {
        Common.ShowWarning(this.app, "Debe indicarse un motivo de salida");
        Inicializar(modo);
        return;
      }
    }

    switch (modoEntrada) {
      case modoALTADESDECASO:
      case modoMANTENIMIENTO: // Solo EquipoNotif, MedicFac, Observaciones

        // Zona de Gestion, FEntrada
        sEqNotif = pModRegTub.getEquipoNotif(); // FALTA COMPROBAR ESTE VALOR
        sMedFac = pModRegTub.getMedFac();
        sObserv = pModRegTub.getObserv();
        sFEntrada = pAnoReg.getFechaEntrada();
        sN1 = pModRegTub.panNiv.getCDNivel1().trim();
        sN2 = pModRegTub.panNiv.getCDNivel2().trim();
        sFSalida = "";
        sMotSalida = "";

        break;
      case modoCERRAR:

        // Si se queria cerrar y el check de cierre esta chequeado: update
        if (chkCierre.getState()) {
          // Debe haber Fecha de Salida
          txtFechaSalida.ValidarFecha();
          if (txtFechaSalida.getValid().equals("S")) {
            sFSalida = txtFechaSalida.getText();
          }
          else {
            ShowWarning(
                "Es necesaria una fecha de salida para dejar el registro cerrado.");
            txtFechaSalida.requestFocus();

            // Reestablecemos el modo de operacion
            modoOperacion = modo;
            Inicializar(modoOperacion);
            return;
          }

          // Puede haber Motivo de Salida
          if (choMotSalida.getSelectedIndex() > 0) {
            sMotSalida = ( (DatCatRegTub) LMotSalida.elementAt(choMotSalida.
                getSelectedIndex() - 1)).getCD().trim();

            // Resto de campos se quedan tal y como vinieron
          }
          sEqNotif = dataResultado.getCD_E_NOTIF();
          sMedFac = dataResultado.getDS_MEDICO();
          //sObserv = dataResultado.getDS_OBSERV();
          //Leemos las observaciones de la caja de texto.
          sObserv = pModRegTub.getObserv();

          sFEntrada = pAnoReg.getFechaEntrada();
          sN1 = pModRegTub.panNiv.getCDNivel1().trim();
          sN2 = pModRegTub.panNiv.getCDNivel2().trim();
        }
        else {
          // Reestablecemos el modo de operacion
          modoOperacion = modo;
          Inicializar(modoOperacion);
          return;
        }

        break;
      case modoDESHACERCIERRE:

        // check de Cierre = on
        if (chkCierre.getState()) {
          // Debe haber Fecha de Salida
          txtFechaSalida.ValidarFecha();
          if (txtFechaSalida.getValid().equals("S")) {
            sFSalida = txtFechaSalida.getText();
          }
          else {
            ShowWarning(
                "Es necesaria una fecha de salida para dejar el registro cerrado.");
            txtFechaSalida.requestFocus();

            // Reestablecemos el modo de operacion
            modoOperacion = modo;
            Inicializar(modoOperacion);
            return;
          }

          // Puede haber Motivo de Salida
          if (choMotSalida.getSelectedIndex() > 0) {
            sMotSalida = ( (DatCatRegTub) LMotSalida.elementAt(choMotSalida.
                getSelectedIndex() - 1)).getCD().trim();

          }
        }
        else { // Check de Cierre = off
          sFSalida = "";
          sMotSalida = "";
        }

        // Resto de campos se quedan tal y como vinieron
        sEqNotif = dataResultado.getCD_E_NOTIF();
        sMedFac = dataResultado.getDS_MEDICO();
        sObserv = dataResultado.getDS_OBSERV();
        sFEntrada = dataResultado.getFC_INIRTBC();
        sN1 = dataResultado.getCD_NIVEL_1_GE();
        sN2 = dataResultado.getCD_NIVEL_2_GE();
        break;
    } // Fin switch

    // Codigo de operario y Fecha Actualizacion que vinieron de la BD
    String sCodOpe = dataResultado.getCD_OPE();
    String sFUltact = dataResultado.getFC_ULTACT();

    // Datos que se actualizan: EquipoNotif, MedicFac, Observaciones, fecha, n1,n2
    DatDiaMntRegTub dataParametros = new DatDiaMntRegTub(ano, registro,
        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
        sEqNotif, "",
        sMedFac, sObserv, sFSalida, sMotSalida, sCodOpe, sFUltact, "",
        sFEntrada, sN1, sN2, "", "");

    CLista parametros = new CLista();
    CLista listaSalida = new CLista();
    StubSrvBD stubCliente = new StubSrvBD();

    parametros.setLogin(this.getCApp().getParameter("login", ""));
    parametros.addElement(dataParametros);
    try {
      listaSalida = Comunicador.Communicate(this.app,
                                            stubCliente,
                                            servletUPDATE,
                                            strSERVLET_DIAREGTUB,
                                            parametros);
      bOK = true;
      // No hay tratamiento del valor devuelto (no devuelve nada) ...
    }
    catch (Exception excepc) {
      // Salvo si se ha producido una excepci�n:

      // Se debe comprobar si es de bloqueo o no
      if (excepc.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
        if (Common.ShowPregunta(this.app, constantes.DATOS_BLOQUEADOS)) {
          try {
            listaSalida = Comunicador.Communicate(this.app,
                                                  stubCliente,
                                                  servletUPDATE_SB,
                                                  strSERVLET_DIAREGTUB,
                                                  parametros);
            bOK = true;
          }
          catch (Exception e) {
            Common.ShowError(this.app, e.getMessage());
          }
        }
      }
      else {
        excepc.printStackTrace();
        ShowWarning(
            "Error al actualizar los datos del registro de tuberculosis.");
        this.dispose();
      }
    }
    finally {
      // Reestablecemos el modo de operacion
      modoOperacion = modo;
      Inicializar(modoOperacion);
      if (bOK) {
        dispose();
      }
    }
  } // Fin btnGrabarActionPerformed()

  void btnCancelarActionPerformed() {
    dispose();
  } // Fin btnCancelarActionPerformed()

} // endclass DiaMntRegTub

/******************* ESCUCHADORES **********************/

// Checks
class DiaMntRegTub_chkCierre_itemAdapter
    implements java.awt.event.ItemListener {
  DiaMntRegTub adaptee;

  DiaMntRegTub_chkCierre_itemAdapter(DiaMntRegTub adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkCierre_itemStateChanged(e);
  }
} // Fin clase DiaMntRegTub_chkCierre_itemAdapter

// Botones
class DiaMntRegTubActionAdapter
    implements java.awt.event.ActionListener {
  DiaMntRegTub adaptee;
  ActionEvent evt;

  DiaMntRegTubActionAdapter(DiaMntRegTub adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    //new Thread(this).start();
    if (evt.getActionCommand().equals("Grabar")) {
      adaptee.btnGrabarActionPerformed();
    }
    else if (evt.getActionCommand().equals("Cancelar")) {
      adaptee.btnCancelarActionPerformed();
    }
  }

  /*  public void run(){
      if (evt.getActionCommand().equals("Grabar")){
        adaptee.btnGrabarActionPerformed();
      } else if (evt.getActionCommand().equals("Cancelar")){
        adaptee.btnCancelarActionPerformed();
      }
    }*/
} // Fin clase DiaMntRegTubActionAdapter
