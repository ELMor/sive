/**
 * Clase: PanAnoReg
 * Paquete: SP_Tuberculosis.cliente.DiaMntRegTub
 * Hereda: CPanel
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 02/11/1999
 * Analisis Funcional: Punto 2. Mantenimiento Registro Tuberculosis.
 * Descripcion: Implementacion del panel que permite visualizar los datos
 *  principales de un registro de tuberculosis: A�o y Registro.
 * JMT (02/11/99):  Se a�ade un constructor que requiere los valores del
 *  a�o y del registro (en forma de String).
 * Modificaciones:
   29/12/1999 - Emilio Postigo Riancho
   - Se cambia lo relativo a 'fecha de entrada en el RT' por
     'fecha de inicio de tratamiento' en la interfaz de usuario
 */

package tuberculosis.cliente.diamntregtub;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.FocusEvent;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CPanel;
import comun.Fechas;
import comun.constantes;

public class PanAnoReg
    extends CPanel {

  // Modo de operaci�n del panel
  static final int modoESPERA = constantes.modoESPERA;
  static final int modoMODIFICACION = constantes.modoMODIFICACION;
  static final int modoCONSULTA = constantes.modoCONSULTA;
  private int modoOperacion = modoMODIFICACION;

  // Modos de operacion del panel
  static final int modoDESHABILITADO = constantes.modoDESHABILITADO;

  /* -------------------- CONTROLES ------------------------- */

  // Organizacion del panel
  private XYLayout lyXYLayout = new XYLayout();

  // A�o y numero del registro de tuberculosis
  private Label lblRegistro = new Label("Registro:");
  private TextField txtAno = new TextField();
  private Label lblBarra = new Label("/");
  private TextField txtReg = new TextField();

  // Fecha de entrada --> Fecha Inicio Tratamiento
  private Label lblFEntrada = new Label("Fecha Inicio Tratamiento:");
  // Correcci�n barras autom�ticas (ARS 22-05-01)
//  private CFechaSimple txtFEntradaR = new CFechaSimple("S");
  private fechas.CFecha txtFEntradaR = new fechas.CFecha("S");
  PanAnoRegFocusAdapter focusAdapter = null;

  private DiaMntRegTub dlg = null;

  public PanAnoReg(DiaMntRegTub dialogo) {
    try {
      dlg = dialogo;
      setApp(dlg.getCApp());
      setBorde(false);

      jbInit();

      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public PanAnoReg(DiaMntRegTub dialogo, String Ano, String Reg) {
    try {
      dlg = dialogo;
      setApp(dlg.getCApp());
      setBorde(false);

      jbInit();

      txtAno.setText(Ano);
      txtReg.setText(Reg);

      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    // Vars. para colocar los componentes
    int pX = 0;
    int pY = 0;
    int longX = 0;
    int longY = 0;

    // Medidas y organizacion del panel
    this.setSize(new Dimension(680, 330));
    lyXYLayout.setHeight(330);
    lyXYLayout.setWidth(680);
    this.setLayout(lyXYLayout);

    // A�o
    pX = 15;
    longX = 90;
    pY += 15;
    longY = -1;
    this.add(lblRegistro, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10 + 5;
    longX = 40;
    this.add(txtAno, new XYConstraints(pX, pY, longX, longY));

    // Registro
    pX += longX + 5;
    longX = 5;
    this.add(lblBarra, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 50;
    this.add(txtReg, new XYConstraints(pX, pY, longX, longY));

    // Fecha de Inicio Tratamiento
    pX += longX + 135;
    longX = 140;
    this.add(lblFEntrada, new XYConstraints(pX, pY, longX, longY));

    pX += longX + 25;
    longX = 75;
    this.add(txtFEntradaR, new XYConstraints(pX, pY, longX, longY));
    focusAdapter = new PanAnoRegFocusAdapter(this);
    txtFEntradaR.addFocusListener(focusAdapter);

  } // Fin jbInit()

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar(modo)

  // Implementacion del metodo abstracto de Cpanel
  public void Inicializar() {

    switch (modoOperacion) {
      case modoMODIFICACION:

        // Habilitar componentes del panel
        txtAno.setEnabled(false);
        txtAno.setEditable(false);
        txtReg.setEnabled(false);
        txtReg.setEditable(false);

        txtFEntradaR.setEnabled(true);
        txtFEntradaR.setEditable(true);

        // Cursos normal
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoDESHABILITADO:

        // Deshabilitar componentes del panel
        txtAno.setEnabled(false);
        txtAno.setEditable(false);
        txtReg.setEnabled(false);
        txtReg.setEditable(false);
        txtFEntradaR.setEnabled(false);
        txtFEntradaR.setEditable(false);
        break;

      case modoESPERA:

        // Deshabilitar componentes del panel
        txtAno.setEnabled(false);
        txtAno.setEditable(false);
        txtReg.setEnabled(false);
        txtReg.setEditable(false);
        txtFEntradaR.setEnabled(false);
        txtFEntradaR.setEditable(false);
        // Cursor de espera
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }

    // Repintado de pantalla
    this.doLayout();
  } // Fin Inicializar()

  public void setModoDeshabilitado() {
    Inicializar(modoDESHABILITADO);
  }

  public void setModoModificacion() {
    Inicializar(modoMODIFICACION);
  }

  public java.util.Date chequearFechaEntrada() {
    java.util.Date jud = null;

    try {
      jud = Fechas.string2Date(txtFEntradaR.getText().trim());
    }
    catch (Exception e) {

    }

    return jud;
  }

  public void setFechaEntrada(String f) {
    txtFEntradaR.setText(f);
  } // Fin setFechaEntrada()

  public String getFechaEntrada() {
    return (txtFEntradaR.getText());
  }

  void txtFIniFocusLost() {

    txtFEntradaR.ValidarFecha();
    txtFEntradaR.setText(txtFEntradaR.getFecha());

    //if(txtFEntradaR.getText().equals("")) // E 18/02/2000
    //  this.setFechaEntrada(dlg.dataResultado.getFC_INIRTBC());
  }
} // endclass PanAnoReg

// Perdidas de foco
class PanAnoRegFocusAdapter
    implements java.awt.event.FocusListener {
  PanAnoReg adaptee;
  FocusEvent evt;

  PanAnoRegFocusAdapter(PanAnoReg adaptee) { // Constructor
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    //Thread th = new Thread(this);
    //th.start();
    adaptee.txtFIniFocusLost();
  }

  public void focusGained(FocusEvent e) {
  }

  /*  public void run() {
      adaptee.txtFIniFocusLost();
    }*/
}
