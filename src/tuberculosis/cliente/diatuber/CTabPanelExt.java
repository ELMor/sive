/* Clase que extiende de CTabPanel del paquete capp para poder
 // utilizar la clase extendida CSolapaext
 // autor: JLT
 // fecha: 13/06/2000
 */

package tuberculosis.cliente.diatuber;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;

import capp.CPanelSolapa;
import capp.CTabPanel;

public class CTabPanelExt

    extends CTabPanel {

  public CTabPanelExt() {
    setBackground(Color.lightGray);
    setLayout(new BorderLayout());

    solapa = new CSolapaext();
    solapa.setBackground(Color.lightGray);
    solapa.setVisible(false); // No se muestra hasta que tenga mas de uno
    disp = new CPanelSolapa();
    disp.setBackground(Color.lightGray);
    disp.setLayout(new CardLayout());

    add("North", solapa);
    add("Center", disp);
  } //end CTabPanel

} //endclass CTabPanel
