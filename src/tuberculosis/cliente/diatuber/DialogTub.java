package tuberculosis.cliente.diatuber;

// modificacion
import java.net.URL;
import java.util.Hashtable;
import java.util.Vector;

import java.awt.Cursor;
import java.awt.Panel;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import comun.Common;
import comun.Comunicador;
import comun.DialogoGeneral;
import comun.constantes;
import infproto.DataProtocolo;
import infproto.PanelTuber;
import sapp.StubSrvBD;
import tuberculosis.cliente.diamntregtub.DiaMntRegTub;
import tuberculosis.cliente.pancabecera.PanCabecera;
import tuberculosis.cliente.pancaso.PanCaso;
import tuberculosis.cliente.panotros.PanOtros;
import tuberculosis.cliente.pansup.PanSup;
import tuberculosis.cliente.pantubnotif.PanTablaNotif;
import tuberculosis.datos.notiftub.DatCasosTub;
import tuberculosis.datos.notiftub.DatNotifTub;
import tuberculosis.datos.notiftub.DatRT;

public class DialogTub
    extends DialogoGeneral {

  private String sFC_ULTACT_EDOIND = null;
  private String sCD_OPE_EDOIND = null;

  //nombre de tabs
  public String sCabecera = "Datos de filiaci�n";

  //Ya hubo msg en protocolo
  public boolean YaSalioMsgEnferSinProtocolo = false;

  // Metamodos de funcionamiento
  public static final int servletSELECT_NORMAL = 1;
  public static final int servletSELECT_SUCA = 2;

  public static final String strSERVLET_NOTIFTUB = constantes.
      strSERVLET_NOTIFTUB;

  //Error en la construccion del dialogo
  public boolean bError = false;

  // variable que controla que se haya cambiado el protocolo
  // para mostrar showpregunta
  public boolean bChanging = false;

  // variable para controlar que cuando se grabe un protocolo y
  // se pulse grabar, si estamos en el panel del protocolo, se
  // grabe dicho protocolo
  public boolean bGrabarProt = false;

  // contenedor de im�genes
  protected CCargadorImagen imgs = null;

  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

  // Modos de operaci�n del panel: paquete comun.constantes
  final int modoESPERA = constantes.modoESPERA;
  final int modoALTA = constantes.modoALTA;
  final int modoMODIFICACION = constantes.modoMODIFICACION;
  final int modoBAJA = constantes.modoBAJA;
  final int modoCONSULTA = constantes.modoCONSULTA;
  final int modoSOLODECLARANTES = constantes.modoSOLODECLARANTES;

  // Modo de entrada al panel
  // modificacion momentanea
  //protected int modoOperacion = constantes.modoALTA;

  public StubSrvBD stubCliente = null;

  // Modo de salida del dialog (Aceptar=0, Cancelar=-1)
  //Aceptar == repetir query al volver
  //Cancelar == nada
  public int iOut = -1;

  public String s1 = null;
  public String s2 = null;

  //CApp capp;

  // controles
  XYLayout xYLayout = new XYLayout();
  XYLayout layoutBotones = new XYLayout();
  XYLayout layoutSuperior = new XYLayout();

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  Panel panelBotones = new Panel();
  StatusBar barra = new StatusBar();

  // modificacion 19/06/2000
  //public CTabPanel tabPanel = new CTabPanel();
  public CTabPanelExt tabPanel = new CTabPanelExt();

  //variables globales de entrada
  public Hashtable hashListas_completa = null;

  public String sNM_EDO = "";
  public String sANO;
  public String sREG;

  // modificacion 10/06/2000
  public String sCD_NIVEL1;
  public String sCD_NIVEL2;

  public DatCasosTub datcasostub = new DatCasosTub();

  //paneles
  public PanSup pansup = null;
  public PanCaso panCaso = null; //NO EN ALTA
  public PanCabecera panCabecera = null;
  public PanOtros panOtros = null;
  public PanTablaNotif panTabla = null;
  public PanelTuber pProtocolo = null;

  // constructor
  public DialogTub(CApp a,
                   Hashtable hashListas,
                   int modo,
                   DatRT datos) {
    super(a);
    try {

      //final int modoCargar = modo;
      this.app = a;
      modoOperacion = modo; //modoCargar;
      stubCliente = new StubSrvBD();
      hashListas_completa = (Hashtable) hashListas.clone();

      YaSalioMsgEnferSinProtocolo = false;

      if (modoOperacion != modoALTA) {
        sNM_EDO = datos.getNM_EDO();
        sANO = datos.getCD_ARTBC();
        sREG = datos.getCD_NRTBC();

        // modificacion 09/06/2000
        // recupero los valores del nivel 1 y 2 (los GE)
        sCD_NIVEL1 = datos.getCD_NIVEL1();
        sCD_NIVEL2 = datos.getCD_NIVEL2();

      }

      //paneles
      pansup = new PanSup(this, modo, hashListas_completa);

      panCabecera = new PanCabecera(this, modo, hashListas_completa, true);
      panTabla = new PanTablaNotif(this, modo, hashListas_completa);
      jbInit();
      pack();
      modoOperacion = modo;

      //ALTA: cabecera + notificadores + superior
      if (modoOperacion == modoALTA) {
        tabPanel.InsertarPanel(sCabecera, panCabecera);
        //panel tabla -----------
        panTabla.rellena(null);
        //-----------
        tabPanel.InsertarPanel("Notificadores", panTabla);
        tabPanel.VerPanel(sCabecera);
      }

      //MODIF: cabecera+notificadores+superior +caso + protocolo +otros
      if ( (modoOperacion == modoMODIFICACION)
          || (modoOperacion == modoBAJA)
          || (modoOperacion == modoCONSULTA)) {

        Inicializar(modoESPERA);
        RellenaDatosTodosPaneles(datos);

      } //fin de modos

      modoOperacion = modo;
      Inicializar(modo);
    }
    catch (Exception ex) {
      ex.printStackTrace();
      bError = true;
    }
  }

  public void RellenaDatosTodosPaneles(DatRT datos) throws Exception {
    if (!IraPorDatos()) { //completa la hash: hashListas_completa
      Exception e = new Exception();
      throw e;
    }

    CLista listadatos = (CLista) hashListas_completa.get(constantes.DATOS);
    datcasostub = (DatCasosTub) listadatos.elementAt(0);
    pansup.rellenarDatos(datos); //DatRT

    //* E
    if (tabPanel.getNumSolapas() < 2) { // No procedemos de un alta
      tabPanel.InsertarPanel(sCabecera, panCabecera);
      tabPanel.InsertarPanel("Notificadores", panTabla);
      tabPanel.VerPanel(sCabecera);
    }

    panCabecera.rellenaDatos(datcasostub);

    //panel tabla -----------
    CLista resultado = (CLista) hashListas_completa.get("DATOS");
    Hashtable hsEnvio = new Hashtable();

    // E 17/01/2000
    // Los notificadores se devuelven dentro de un vector en la posici�n 1
    // de 'resultado'
    Vector vNotificadores = (Vector) resultado.elementAt(1);
    //
    for (int i = 0; i < vNotificadores.size(); i++) {
      hsEnvio.put(String.valueOf(i), (DatNotifTub) vNotificadores.elementAt(i));
    }
    // E 17/01/2000
    // Se ha cambiado en el 'for' lo relativo a resultado
    panTabla.rellena(hsEnvio);
    //-----------

    // Para que cada notificador disponga de esos datos de la tabla SIVE_EDOIND
    this.panTabla.setCdOpe_FcUltAct_EDOIND_Notificadores(datcasostub.
        getCD_OPE_EDOIND(), datcasostub.getFC_ULTACT_EDOIND());

    /*        //otros:
            panOtros = new PanOtros(this, modoOperacion, hashListas_completa);
            tabPanel.InsertarPanel("Otros", panOtros);
            panOtros.doLayout();
            tabPanel.VerPanel(sCabecera);*/

    //caso:
    if (panCaso != null) {
      borrarCaso();
    }
    addCaso();
    panCaso.rellenarDatos(datcasostub);

    //protocolo
    if (pProtocolo != null) {
      borrarProtocolo();
    }
    addProtocolo();
    //modificacion 23/06/2000
    pProtocolo.repaint();

    //otros     //PDP 06/03/2000
    if (panOtros != null) {
      borrarOtros();
    }
    addOtros();

    /*  //PDP 06/03/2000
             //otros:
             panOtros = new PanOtros(this, modoOperacion, hashListas_completa);
             tabPanel.InsertarPanel("Otros", panOtros);
             panOtros.doLayout();
             tabPanel.VerPanel(sCabecera);
     */
    AUTORIZACIONES();
  }

  void AUTORIZACIONES() {
    //abrir en un modo u otro segun autorizaciones (cambiando modoOperacion)
  }

  void jbInit() throws Exception {

    // carga las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));

    // t�tulo
    this.setTitle("Casos de Tuberculosis");

    // layout - controles
    xYLayout.setHeight(530);
    xYLayout.setWidth(784);
    setSize(784, 530);
    setLayout(xYLayout);

    if ( (modoOperacion == modoBAJA)
        || (modoOperacion == modoCONSULTA)) {
      btnAceptar.setLabel("Aceptar");
    }
    else {
      btnAceptar.setLabel("Grabar");

    }
    btnCancelar.setLabel("Salir");

    layoutBotones.setHeight(44);
    layoutBotones.setWidth(780);
    barra.setBevelOuter(BevelPanel.LOWERED);
    barra.setBevelInner(BevelPanel.LOWERED);
    panelBotones.setLayout(layoutBotones);
    panelBotones.add(barra, new XYConstraints(5, 10, 400, -1));
    panelBotones.add(btnAceptar, new XYConstraints(530, 10, 100, -1)); //520
    panelBotones.add(btnCancelar, new XYConstraints(645, 10, 100, -1)); //635

    add(pansup, new XYConstraints(10, 3, 770, 88));

    add(tabPanel, new XYConstraints(10, 120 - 20, 770, 286 + 100)); //10, 106, 755, 300
    add(panelBotones, new XYConstraints(10, 409 - 20 + 95, 770, 44 + 30)); //10, 409, 755, 44

    DiaActionAdapter actionAdapter = new DiaActionAdapter(this);

    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    // modificacion 27/06/2000
    this.doLayout();
    this.repaint();

  }

  public String getAnopanSup() {
    return (this.pansup.txtAno.getText());
  }

  private boolean IraPorDatos() {
    try {

      DatCasosTub dct = new DatCasosTub();
      int iModoServlet = 0;
      if (this.app.getIT_TRAMERO().equals("S")) {
        iModoServlet = servletSELECT_SUCA;
      }
      else {
        iModoServlet = servletSELECT_NORMAL;

      }
      dct.put("META_MODO", new Integer(iModoServlet));
      dct.put("CD_ARTBC", sANO);
      dct.put("CD_NRTBC", sREG);

      CLista data = new CLista();
      data.addElement(dct); //hash con los 3 datos

      stubCliente = new StubSrvBD();
      stubCliente.setUrl(new URL(this.app.getURL() + "servlet/SrvNotifTub"));

      CLista resultado = null;
      // E 17/01/2000 resultado = (CLista) stubCliente.doPost(constantes.modoMODIFICACION, data);
      resultado = (CLista) stubCliente.doPost(constantes.modoLEERDATOS, data);
      hashListas_completa.put("DATOS", resultado);

    }
    catch (Exception e) {
      System.out.println(e.getMessage());
      Common.ShowWarning(this.app,
                         "Error en la conexi�n rellenando los paneles");
      //dispose(); //no funciona, continua
      return false;
    }
    return true;

  }

  void btnCancelar_actionPerformed() {
    iOut = -1;
    dispose();
  }

  public StatusBar getBarra() {
    return barra;
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {

      case modoESPERA:
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        if (pansup != null) {
          pansup.Inicializar(modoOperacion);
        }
        if (panCaso != null) {
          panCaso.Inicializar(modoOperacion);
        }
        if (panCabecera != null) {
          panCabecera.Inicializar(modoOperacion);
        }
        if (panTabla != null) {
          panTabla.Inicializar(modoOperacion);
        }
        if (panOtros != null) {
          panOtros.Inicializar(modoOperacion);
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.deshabilitarCampos(Cursor.WAIT_CURSOR);

        }
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        if (pansup != null) {
          pansup.Inicializar(modoOperacion);
        }
        if (panCaso != null) {
          panCaso.Inicializar(modoOperacion);
        }
        if (panCabecera != null) {
          panCabecera.Inicializar(modoOperacion);
        }
        if (panTabla != null) {
          panTabla.Inicializar(modoOperacion);
        }
        if (panOtros != null) {
          panOtros.Inicializar(modoOperacion);
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.habilitarCampos();

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICACION:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        if (pansup != null) {
          pansup.Inicializar(modoOperacion);
        }
        if (panCaso != null) {
          panCaso.Inicializar(modoOperacion);
        }
        if (panCabecera != null) {
          panCabecera.Inicializar(modoOperacion);
        }
        if (panTabla != null) {
          panTabla.Inicializar(modoOperacion);
        }
        if (panOtros != null) {
          panOtros.Inicializar(modoOperacion);
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.habilitarCampos();

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
      case modoCONSULTA:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        if (pansup != null) {
          pansup.Inicializar(modoOperacion);
        }
        if (panCaso != null) {
          panCaso.Inicializar(modoOperacion);
        }
        if (panCabecera != null) {
          panCabecera.Inicializar(modoOperacion);
        }
        if (panTabla != null) {
          panTabla.Inicializar(modoOperacion);
        }
        if (panOtros != null) {
          panOtros.Inicializar(modoOperacion);
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.deshabilitarCampos(Cursor.DEFAULT_CURSOR);

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }

    //botones -----------------------
    if ( (modoOperacion == modoBAJA)
        || (modoOperacion == modoCONSULTA)) {
      btnAceptar.setLabel("Aceptar");
    }
    else {
      btnAceptar.setLabel("Grabar");
    }
    btnCancelar.setLabel("Cancelar");
    if (modoOperacion == modoCONSULTA) {
      btnAceptar.setEnabled(false);
      //----------------------------------

    }
    doLayout();
  }

  void btnAceptar_actionPerformed() {

    int modo = modoOperacion;

    if (modo == constantes.modoALTA) {
      if (!Common.ShowPregunta(this.app,
                               "Esta acci�n grabar� el caso. �Desea continuar?")) {
        return;
      }
    }

    if (bError) {
      Common.ShowWarning(this.app, "Error al cargar datos. Salga por Cancelar");
      return;
    }

    modoOperacion = modoESPERA;
    Inicializar();

    if (modo == constantes.modoBAJA) {
      borrarCasoTub();
      return;
    }
    else if (modo == constantes.modoMODIFICACION) {

      //al menos un notificador
      if (panTabla.getVectorNotificadores().size() == 0) {
        Common.ShowWarning(this.app, "Debe introducir al menos un Notificador");
        Inicializar(modo);
        return;
      }

      if (!panCabecera.validarDatos()) {
        tabPanel.VerPanel(sCabecera);
        Inicializar(modo);
        return;
      }

      if (!panCaso.validarDatos()) {
        tabPanel.VerPanel("Caso");
        Inicializar(modo);
        return;
      }

      modificarCasoTub();

      return;
    }
    else if (modo == constantes.modoALTA) {
      //al menos un notificador
      if (panTabla.getVectorNotificadores().size() == 0) {
        Common.ShowWarning(this.app, "Debe introducir al menos un Notificador");
        Inicializar(modo);
        return;
      }

      if (!pansup.validarDatos()) {
        Inicializar(modo);
        return;
      }
      if (!panCabecera.validarDatos()) {
        tabPanel.VerPanel(sCabecera);
        Inicializar(modo);
        return;
      }

      insertarCasoTub();

      // modificacion 11/07/2000 arriesgada
      modo = constantes.modoMODIFICACION;

      return;

    } //fin alta

  } //fin funcion ACEPTAR *****************************************

// Realiza la comunicaci�n con el servlet strSERVLET_DIAMED
  CLista hazAccion(StubSrvBD stubCliente, int modo, CLista parametros) throws
      Exception {
    CLista result = null;

    // Invocacion del servlet para que haga el insert

    result = Comunicador.Communicate(this.getCApp(),
                                     stubCliente,
                                     modo,
                                     strSERVLET_NOTIFTUB,
                                     parametros);

    // SOLO DESARROLLO
    // modificacion mometanea

    //SrvNotifTub srv = new SrvNotifTub();
    //parametros.setLogin(this.app.getLogin());
    //result = srv.doPrueba(modo,parametros);

    return result;

  } // Fin hazAccion()

  private void insertarCasoTub() {

    //entra en ESPERA

    //recoger los datos de los paneles
    DatCasosTub datosCaso = null;
    //LA INICIAL (puede contener datos del enfermo. Normalmente: vacia)
    if (datcasostub == null) {
      datosCaso = new DatCasosTub();
    }
    else {
      datosCaso = datcasostub;
    }
    datosCaso = pansup.recogerDatos(datosCaso); //MACHACAR: ano, a G d G
    //RT
    datosCaso.setFC_INIRTBC(this.getCApp().getFC_ACTUAL());

    datosCaso = panCabecera.recogerDatos(datosCaso); //MACHACAR
    if (panCaso != null) {
      datosCaso = panCaso.recogerDatos(datosCaso);

    }
    Vector vTabla = panTabla.getVectorNotificadores();

    CLista listaSalida = new CLista();
    CLista parametros = new CLista();
    parametros.addElement(datosCaso);
    parametros.addElement(vTabla);

    boolean bOK = false;

    /*DiaMntRegTub dlgRT = new DiaMntRegTub(this.app, 0, sANO, sREG, hashListas_completa, constantes.modoMODIFICACION, datcasostub.getFC_INIRTBC());
           if (dlgRT.iERROR != 0) {
      dlgRT.show();
           }
           dlgRT = null;*/

    try {
      listaSalida = hazAccion(stubCliente, constantes.modoALTA, parametros);
      bOK = true;
    }
    catch (Exception e) { //acceso a base de datos mal o bloqueo
      if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
        if (Common.ShowPregunta(this.app, constantes.DATOS_BLOQUEADOS)) {
          try {
            listaSalida = hazAccion(stubCliente, constantes.modoALTA_SB,
                                    parametros);
            bOK = true;
          }
          catch (Exception ex) {
            bOK = false;
            Common.ShowWarning(this.app,
                               "Error al realizar la inserci�n del caso");
          }
        }
        else { //no quiere sobreescribir
          bOK = false;
        }
      }
      else { //error normal
        bOK = false;
        Common.ShowWarning(this.app, "Error al realizar la inserci�n del caso");
      }
    } //fin del catch

    if (bOK) {
      datcasostub = (DatCasosTub) listaSalida.elementAt(0);
      Vector vNotificadores = (Vector) listaSalida.elementAt(1);
      CLista lista = new CLista();
      lista.addElement(datcasostub);
      hashListas_completa.put("DATOS", lista);
      pansup.txtReg.setText(datcasostub.getCD_NRTBC());

      // Para cargar el n�mero de enfermo
      panCabecera.txtCodEnfermo.setText(datcasostub.getCD_ENFERMO());

      listaSalida = null;
      this.btnCancelar.requestFocus();
      iOut = 0;

      sNM_EDO = datcasostub.getNM_EDO();
      sANO = datcasostub.getCD_ARTBC();
      sREG = datcasostub.getCD_NRTBC();

      //modificacion arriesgada 11/07/2000
      this.panTabla.setNM_EDO(sNM_EDO);

      // E 27/01/2000
      // Para que cada notificador disponga de n� de caso edo
      this.panTabla.setEdoNotificadores(new Integer(sNM_EDO).intValue());
      // Para que cada notificador disponga de CD_OPE y FC_ULTACT
      this.panTabla.setCdOpe_FcUltAct_Notificadores( ( (DatNotifTub)
          vNotificadores.elementAt(0)).getCD_OPE_EDOI(),
          ( (DatNotifTub) vNotificadores.elementAt(0)).getFC_ULTACT_EDOI());

      // Para que cada notificador disponga de esos datos de la tabla SIVE_EDOIND
      this.panTabla.setCdOpe_FcUltAct_EDOIND_Notificadores(datcasostub.
          getCD_OPE_EDOIND(), datcasostub.getFC_ULTACT_EDOIND());

      modoOperacion = constantes.modoMODIFICACION; //pero no inicializamos
      //seguimos en espera
      // ... y mostramos el di�logo de RT en modo ALTADESDECASO
      // Para pasar par�metros de �ltima hora
      Hashtable hDatos = new Hashtable();

      // MODIFICACION 05/04/2000
      hDatos.put("CD_AREA_CASO", this.pansup.panNiv.getCDNivel1());
      hDatos.put("DS_AREA_CASO", this.pansup.panNiv.getDSNivel1());
      hDatos.put("CD_DISTRITO_CASO", this.pansup.panNiv.getCDNivel2());
      hDatos.put("DS_DISTRITO_CASO", this.pansup.panNiv.getDSNivel2());
      // hDatos.put("CD_AREA_CASO", this.panCabecera.txtCodNivel1.getText().trim());

      DiaMntRegTub dlgRT = new DiaMntRegTub(this.app,
                                            constantes.modoALTADESDECASO, sANO,
                                            sREG, hashListas_completa,
                                            constantes.modoMODIFICACION,
                                            datcasostub.getFC_INIRTBC(), hDatos);
      if (dlgRT.iERROR != 0) {
        dlgRT.show();
      }
      dlgRT = null;
    }
    else {
      iOut = -1;
      modoOperacion = modoALTA;
      this.btnCancelar.requestFocus();
      Inicializar();
      return;
    }

    //insertar protocolo si estaba abto
    // modificacion 10/07/2000
    /*
      if (pProtocolo != null){
        //actualizar el panelInforme con nm_edo
        pProtocolo.pnl.Cargar_nm_edo(sNM_EDO);
        //GRABAR EL PROTOCOLO, porque puede haber metido algo!!!!!
        boolean b = pProtocolo.pnl.ValidarObligatorios();
        if (b) {
          pProtocolo.pnl.Insertar();//Inserta datos en la tabla respedo
          //dispose(); //Enrique quiere que se quede abto
          // estaba en la pesta�a de protocolo, continuara alli
        }
      }
     */

    //abro caso si cerrado
    if (panCaso == null) {
      addCaso();
      //abro Protocolo si cerrado
    }
    if (pProtocolo == null) {
      addProtocolo();
      //modificacion 23/06/2000
    }
    pProtocolo.repaint();

    //continuamos abriendo Otros
    panOtros = new PanOtros(this, constantes.modoMODIFICACION,
                            hashListas_completa, sNM_EDO);
    tabPanel.InsertarPanel("Otros", panOtros);
    panOtros.doLayout();
    tabPanel.VerPanel(sCabecera);

    Inicializar();

  } //fin de insertar_caso

  private void borrarCasoTub() {
    //entra en ESPERA

    //recoger los datos de los paneles
    DatCasosTub datosCaso = null;
    //LA INICIAL (puede contener datos del enfermo. Normalmente: vacia)
    if (datcasostub == null) {
      datosCaso = new DatCasosTub();
    }
    else {
      datosCaso = datcasostub;
    }
    datosCaso = pansup.recogerDatos(datosCaso); //MACHACAR: ano, a G d G
    //RT
    datosCaso.setFC_INIRTBC(this.getCApp().getFC_ACTUAL());

    datosCaso = panCabecera.recogerDatos(datosCaso); //MACHACAR
    if (panCaso != null) {
      datosCaso = panCaso.recogerDatos(datosCaso);

    }
    Vector vTabla = panTabla.getVectorNotificadores();

    //System.out.println( ((Integer)datosCaso.get("NM_ENVOTRC")).toString() );
    // JLT 27/04/2001
    // esto se hace pues esta dando problemas al recuperar
    // esta valor de la hash, pues es num�rico. Lo metemos en la
    // hash directamente como string
    //datosCaso.put("NM_ENVOTRC", ((Integer)datosCaso.get("NM_ENVOTRC")).toString());

    CLista listaSalida = new CLista();
    CLista parametros = new CLista();
    parametros.addElement(datosCaso);
    parametros.addElement(vTabla);

    boolean bOK = false;

    try {
      listaSalida = hazAccion(stubCliente, constantes.modoBAJA, parametros);
      bOK = true;
    }
    catch (Exception e) { //acceso a base de datos mal o bloqueo
      if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
        if (Common.ShowPregunta(this.app, constantes.DATOS_BLOQUEADOS)) {
          try {
            listaSalida = hazAccion(stubCliente, constantes.modoBAJA_SB,
                                    parametros);
            bOK = true;
          }
          catch (Exception ex) {
            bOK = false;
            Common.ShowWarning(this.app,
                               "Error al realizar el borrado del caso");
          }
        }
        else { //no quiere sobreescribir
          bOK = false;
        }
      }
      else { //error normal
        bOK = false;
        Common.ShowWarning(this.app, "Error al realizar el borrado del caso");
      }
    } //fin del catch

    if (bOK) {
      dispose();
    }
    else {
      iOut = -1;
      modoOperacion = modoBAJA;
      this.btnCancelar.requestFocus();
      Inicializar();
      return;
    }

    Inicializar();

    /*CLista param = null;
       CLista resul = null;
       CMessage msgBox = null;
       //entra en ESPERA
       //recoger los datos de los paneles  //conserva cd_ope, fc_ultact
       //no tengo que recoger los datos de los paneles porque en
       //modo borrar no se puede modificar nada
       //**********************************
        //TRY GENERAL
        try {
       param = new CLista();
       datosAlerta.insert("CD_SITALERBRO","1");
       param.addElement(datosAlerta);
       param.addElement(datosBrote);
       //va bien: listaSalida = (true) o                //bien
       //         listaSalida = (true, mensaje pregunta) //modif
       //va mal: listaSalida = null y exc
       //nota: si YA ha sido borrado, devuelve TRUE . No constaras como cd_ope
       resul = Comunicador.Communicate(this.getCApp(),
                               stubCliente,
                               servletCOMP_BORR,
                               strSERVLET_MODIF,
                               param);
//ha pasado algo ie  MODIF por algun otro
      if (resul.size() == 2){
         String pre = (String) resul.elementAt(1);
         msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                             pre);
         msgBox.show();
         if (msgBox.getResponse()){  //SI
           setCursor(new Cursor(Cursor.WAIT_CURSOR));
           msgBox = null;
           //va bien: listaSalida.size()=0
           //va mal: listaSalida = null y exc
           resul = Comunicador.Communicate(this.getCApp(),
                     stubCliente,
                     servletBORRADO,
                     strSERVLET_MODIF,
                     param);
           if (resul.size() == 0){
             //ShowWarning("El brote ha sido borrado");
              iOut = 0; //repetir query con datos buenos de db
              dispose();
              return;
             //NO INICIALIZAMOS. NO HACE FALTA!!!!
           }
         }
         else{//NO quiere sobreescribir
              iOut = 0;//Como Aceptar repetir query con datos buenos de db
              this.btnCancelar.requestFocus();
              modoOperacion = modoBAJA;
              Inicializar();
              return;
         }
       }
//SI HA IDO BIEN
      else if (resul.size() == 1){
         // todo ha ido bien y lo que trae es un true
         //ShowWarning("El brote ha sido borrado");
         setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
         iOut = 0;
         dispose();
         return;
         //NO INICIALIZAMOS. NO HACE FALTA!!!!
      }
        // error en el proceso
        } catch (Exception e) {
           setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
           e.printStackTrace();
           msgBox = new CMessage(this.app, CMessage.msgERROR, "Error al realizar el borrado del brote");
           msgBox.show();
           msgBox = null;
           iOut = -1;//Como cancelar
           this.btnCancelar.requestFocus();
           modoOperacion = modoBAJA;
           Inicializar();
        }
      */

  }

  private void setFC_ULTACT_EDOIND(String s) {
    sFC_ULTACT_EDOIND = "";
    if (s != null) {
      sFC_ULTACT_EDOIND = s.trim();
    }
  }

  private String getFC_ULTACT_EDOIND() {
    String s = "";
    if (sFC_ULTACT_EDOIND != null) {
      s = sFC_ULTACT_EDOIND;
    }
    return s;
  }

  private void setCD_OPE_EDOIND(String s) {
    sCD_OPE_EDOIND = "";
    if (s != null) {
      sCD_OPE_EDOIND = s.trim();
    }
  }

  private String getCD_OPE_EDOIND() {
    String s = "";
    if (sCD_OPE_EDOIND != null) {
      s = sCD_OPE_EDOIND;
    }
    return s;
  }

  private void modificarCasoTub() {
    //entra en ESPERA

    //recoger los datos de los paneles
    DatCasosTub datosCaso = null;
    //LA INICIAL (puede contener datos del enfermo. Normalmente: vacia)
    if (datcasostub == null) {
      datosCaso = new DatCasosTub();
    }
    else {
      datosCaso = datcasostub;
    }

    datosCaso = pansup.recogerDatos(datosCaso); //MACHACAR: ano, a G d G
    //RT
    datosCaso.setFC_INIRTBC(this.getCApp().getFC_ACTUAL());

    datosCaso = panCabecera.recogerDatos(datosCaso); //MACHACAR
    if (panCaso != null) {
      datosCaso = panCaso.recogerDatos(datosCaso);

    }
    Vector vTabla = panTabla.getVectorNotificadores();
    // modificacion 04/07/2000
    // recupera el valor fc_ultact_edoind para que no salte el bloqueo
    // de sive_edoind
    String s = (String)this.getFC_ULTACT_EDOIND();
    if (s != null && s != "") {
      datosCaso.setFC_ULTACT_EDOIND( (String)this.getFC_ULTACT_EDOIND());

      //System.out.println( ((Integer)datosCaso.get("NM_ENVOTRC")).toString() );
      // JLT 27/04/2001
      // esto se hace pues esta dando problemas al recuperar
      // esta valor de la hash, pues es num�rico. Lo metemos en la
      // hash directamente como string
      //datosCaso.put("NM_ENVOTRC", ((Integer)datosCaso.get("NM_ENVOTRC")).toString());
    }
    CLista listaSalida = new CLista();
    CLista parametros = new CLista();
    parametros.addElement(datosCaso);
    parametros.addElement(vTabla);

    boolean bOK = false;

    try {
      listaSalida = hazAccion(stubCliente, constantes.modoMODIFICACION,
                              parametros);
      bOK = true;
    }
    catch (Exception e) { //acceso a base de datos mal o bloqueo
      if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
        if (Common.ShowPregunta(this.app, constantes.DATOS_BLOQUEADOS)) {
          try {
            listaSalida = hazAccion(stubCliente, constantes.modoMODIFICACION_SB,
                                    parametros);
            bOK = true;
          }
          catch (Exception ex) {
            bOK = false;
            Common.ShowWarning(this.app,
                               "Error al realizar la modificaci�n del caso");
          }
        }
        else { //no quiere sobreescribir
          bOK = false;
        }
      }
      else { //error normal
        bOK = false;
        Common.ShowWarning(this.app,
                           "Error al realizar la modificaci�n del caso");
      }
    }
    finally {
      if (bOK) {
        datcasostub = (DatCasosTub) listaSalida.firstElement();

        // modificacion 11/07/2000  para que no salte el bloqueo
        this.setFC_ULTACT_EDOIND( (String) datcasostub.getFC_ULTACT_EDOIND());
        this.setCD_OPE_EDOIND( (String) datcasostub.getCD_OPE_EDOIND());

      }
    } //fin del catch

    if (bOK) {

      // modificacion 13/07/2000
      // solamente si se pulsa aceptar desde el panel de protocolo,
      // se grabar�n en B.D. las respuestas

      if (bGrabarProt) {

        //EXISTIA PROTOCOLO PARA LA ENFERMEDAD
        if (pProtocolo != null) {
          // Se actualizan datos para no autobloquear la grabaci�n del protocolo
          this.panTabla.setCdOpe_FcUltAct_EDOIND_Notificadores(datcasostub.
              getCD_OPE_EDOIND(), datcasostub.getFC_ULTACT_EDOIND());

          //GRABAR EL PROTOCOLO, porque puede haber metido algo!!!!!
          boolean b = pProtocolo.pnl.ValidarObligatorios();
          if (b) {
            pProtocolo.pnl.Insertar(); //Inserta datos en la tabla respedo

            // modificacion 06/07/2000
            // borramos el notificador nuevo cuyo protocolo
            // ha sido grabado en esta variable auxiliar, para controlar
            // que se vea el protocolo adecuado del notificador
            if (panTabla.vNuevosNotificadoresaux.size() > 0) {
              int ind = pProtocolo.pnl.indice;
              panTabla.vNuevosNotificadoresaux.removeElementAt(ind);
            }

            // modificacion 04/07/2000 para que no salte el bloqueo de
            // sive_edoind
            DatNotifTub dntaux = null;
            CLista lista = pProtocolo.pnl.result;
            dntaux = (DatNotifTub) ( (Hashtable) lista.elementAt(1)).get(
                constantes.DATOSNOTIFICADOR);
            this.setFC_ULTACT_EDOIND( (String) dntaux.getFC_ULTACT_EDOIND());

            // modificacion 24/07/2000
            bGrabarProt = false;
            bChanging = false; // para no detectar cambio de protocolo

          }
          /*
                   // modificacion 04/07/2000 para que no salte el bloqueo de
                   // sive_edoind
                   DatNotifTub dntaux = null;
                   CLista lista = pProtocolo.pnl.result;
                   dntaux = (DatNotifTub) ((Hashtable) lista.elementAt(1)).get(constantes.DATOSNOTIFICADOR);
               this.setFC_ULTACT_EDOIND((String) dntaux.getFC_ULTACT_EDOIND());
           */

          //Inicializar(modoOperacionBk);
        }
        //NO EXISTIA PROTOCOLO PARA LA ENFERMEDAD
        else {
          //Inicializar(modoOperacionBk);
        }

        iOut = 0;
        //dispose();

      } // if de grabarprot
    }
    else {
      iOut = -1;
      modoOperacion = modoMODIFICACION;
      this.btnCancelar.requestFocus();
      Inicializar();
      return;
    }

    //PDP 06/03/2000
    if (panOtros != null) {
      borrarOtros();
      addOtros();
    }

    modoOperacion = modoMODIFICACION;
    Inicializar();

    /*
       CLista param = null;
       CLista resul = null;
       CMessage msgBox = null;
       String cd_sitAntes = datosAlerta.getCD_SITALERBRO();
       //entra en ESPERA
       //recoger los datos de los paneles  //conserva cd_ope, fc_ultact
       DataBrote datosPantalla = new DataBrote();
       datosPantalla.insert( "CD_OPE", datosBrote.getCD_OPE());
       datosPantalla.insert( "FC_ULTACT", datosBrote.getFC_ULTACT());
       panelSuperior.recogerDatos(datosPantalla);
       pDatos.recogerDatos(datosPantalla);
       pColectivo.recogerDatos(datosPantalla);
       pCuadro.recogerDatos(datosPantalla);
       DataAlerta datosPantAler = new DataAlerta();
       datosPantAler = recogerDatos(datosPantAler, modoMODIFICACION); //dlg
       //**********************************
        //TRY GENERAL
        try {
       param = new CLista();
       param.addElement(datosPantAler);
       param.addElement(datosPantalla);
       //va bien: listaSalida = (true, DataAlerta, DataBrote) o   - catch
       //         listaSalida = (true, mensaje pregunta) - try
       //va mal: listaSalida = null y exc
       resul = Comunicador.Communicate(this.getCApp(),
                               stubCliente,
                               servletCOMP_MOD,
                               strSERVLET_MODIF,
                               param);
       setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
       try {
         //pregunta si se quiere sobreescribir
         String pre = (String) resul.elementAt(1);
         if (pre.indexOf("borrado")!= -1){
           //el brote hab�a sido modificado
           msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                             pre);
           msgBox.show();
           if (msgBox.getResponse()){  //SI
             setCursor(new Cursor(Cursor.WAIT_CURSOR));
             msgBox = null;
             resul = Comunicador.Communicate(this.getCApp(),
                     stubCliente,
                     servletMODIFICACION,
                     strSERVLET_MODIF,
                     param);
             //va bien: listaSalida (DataAlerta, DataBrote)
             //va mal: listaSalida = null y exc
             if (resul.size() == 2){
               //ShowWarning("El brote ha sido modificada");
          datosAlerta = (DataAlerta) resul.elementAt(0); // no sirve para nada!
               datosBrote = (DataBrote) resul.elementAt(1);
               modoOperacion = modoMODIFICACION;
               Parche_datosAlerta(datosAlerta, cd_sitAntes);
               //actualiza it_prim, fc_prim
               RellenaCheck(datosAlerta);
               this.btnCancelar.requestFocus();
               iOut = 0; //aceptar, ptt se hara de nuevo la query
               modoOperacion = modoMODIFICACION;
               //Inicializar(); continuamos en espera
             }
           }
           else{//NO quiere sobreescribir
              iOut = 0;//Como Aceptar repetir query con datos buenos de db
              this.btnCancelar.requestFocus();
              modoOperacion = modoMODIFICACION;
              Inicializar();
              return;
           }
         }
         else{ //el brote habia sido borrado
           ShowWarning("El informe del brote ha sido borrado.No podr� hacer ninguna operaci�n sobre �l");
           iOut = -1;
           this.btnCancelar.requestFocus();
           modoOperacion = modoMODIFICACION;
           Inicializar();
           return;
         }
       }
       catch (ClassCastException e) {
         // todo ha ido bien y lo que trae es el brote modificado
         //ShowWarning("El brote ha sido modificada");
         datosAlerta = (DataAlerta) resul.elementAt(1);
         datosBrote = (DataBrote) resul.elementAt(2);
         modoOperacion = modoMODIFICACION;
         Parche_datosAlerta(datosAlerta, cd_sitAntes);
         //actualiza it_prim, fc_prim
         RellenaCheck(datosAlerta);
         iOut = 0;//Como Aceptar repetir query con datos buenos de db
         this.btnCancelar.requestFocus();
         modoOperacion = modoMODIFICACION;
         //Inicializar(); continuamos en espera
       }
        // error en el proceso
        } catch (Exception e) {
           setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
           e.printStackTrace();
           msgBox = new CMessage(this.app, CMessage.msgERROR, "Error al realizar la modificaci�n del brote");
           msgBox.show();
           msgBox = null;
           iOut = -1;//Como cancelar
           this.btnCancelar.requestFocus();
           modoOperacion = modoMODIFICACION;
           Inicializar();
           return;
        }
        Inicializar();
        //-----------------------------   */

  }

  public void grabarProtocolo() {

    this.setCursor(new Cursor(Cursor.WAIT_CURSOR));

    // modificacion 10/07/2000
    if (pProtocolo != null) {
      // Se actualizan datos para no autobloquear la grabaci�n del protocolo
      if ( ( (String)this.getFC_ULTACT_EDOIND()).equals("")) {
        this.panTabla.setCdOpe_FcUltAct_EDOIND_Notificadores(datcasostub.
            getCD_OPE_EDOIND(), datcasostub.getFC_ULTACT_EDOIND());
      }
      else {
        this.panTabla.setCdOpe_FcUltAct_EDOIND_Notificadores(this.
            getCD_OPE_EDOIND(), this.getFC_ULTACT_EDOIND());
      }

      //GRABAR EL PROTOCOLO, porque puede haber metido algo!!!!!
      boolean b = pProtocolo.pnl.ValidarObligatorios();
      if (b) {
        pProtocolo.pnl.Insertar(); //Inserta datos en la tabla respedo

        // modificacion 06/07/2000
        // borramos el notificador nuevo cuyo protocolo
        // ha sido grabado en esta variable auxiliar, para controlar
        // que se vea el protocolo adecuado del notificador
        if (panTabla.vNuevosNotificadoresaux.size() > 0) {
          int ind = pProtocolo.pnl.indice;
          panTabla.vNuevosNotificadoresaux.removeElementAt(ind);
        }

        // modificacion 04/07/2000 para que no salte el bloqueo de
        // sive_edoind
        DatNotifTub dntaux = null;
        CLista lista = pProtocolo.pnl.result;
        dntaux = (DatNotifTub) ( (Hashtable) lista.elementAt(1)).get(constantes.
            DATOSNOTIFICADOR);
        this.setFC_ULTACT_EDOIND( (String) dntaux.getFC_ULTACT_EDOIND());
        this.setCD_OPE_EDOIND( (String) dntaux.getCD_OPE_EDOIND());

      }

      /*
             // modificacion 04/07/2000 para que no salte el bloqueo de
             // sive_edoind
       DatNotifTub dntaux = null;
       CLista lista = pProtocolo.pnl.result;
       dntaux = (DatNotifTub) ((Hashtable) lista.elementAt(1)).get(constantes.DATOSNOTIFICADOR);
       this.setFC_ULTACT_EDOIND((String) dntaux.getFC_ULTACT_EDOIND());
       this.setCD_OPE_EDOIND((String) dntaux.getCD_OPE_EDOIND());
       */

      //Inicializar(modoOperacionBk);
    }
    //NO EXISTIA PROTOCOLO PARA LA ENFERMEDAD
    else {
      //Inicializar(modoOperacionBk);
    }

    this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

  }

// retorna una clista con un elemento de tipo DataBusqueda
// para  a�adir  (solo hace falta que lleve la clave )
  public Object getComponente() {
    Object o = null;

    if (iOut != -1) {
      CLista lista = new CLista();

      //SIRVE PARA EL ALTA
      DatRT datosDevolver = new DatRT();
      datosDevolver.put("CD_ARTBC", "pendiente");
      datosDevolver.put("CD_NRTBC", "pendiente");
      datosDevolver.put("NM_EDO", "pendiente");

      lista.addElement(datosDevolver);
      o = lista.elementAt(0);
    }

    return o;
  }

  // Devuelve una Hashtable conteniendo los datos del notificador
  // seleccionado en panTabla
  public Hashtable getDatosBloqueo() {
    Hashtable h = new Hashtable();
    h.put(constantes.DATOSNOTIFICADOR, this.panTabla.getNotificador());
    return h;
  }

  //
  public void addProtocolo() {
    DataProtocolo data = new DataProtocolo();
    //data.sCodigo = constantes.TUBERCULOSIS;
    // modificacion 12/02/2001
    data.sCodigo = this.app.getParametro("CD_TUBERCULOSIS");
    data.sDescripcion = "TUBERCULOSIS";

    // modificacion 09/06/2000
    // de esta manera conseguimos que aparezcan los protocolos
    // generados por usuarios de area y distrito
    s1 = null;
    s2 = null;

    s1 = sCD_NIVEL1;
    s2 = sCD_NIVEL2;
    if (s1 == null || s2 == null) {
      s1 = this.pansup.panNiv.getCDNivel1();
      s2 = this.pansup.panNiv.getCDNivel2();
    }

    data.sNivel1 = s1;
    data.sNivel2 = s2;
    //data.sNivel1 = panCabecera.txtCodNivel1.getText().trim();
    //data.sNivel2 = panCabecera.txtCodNivel2.getText().trim();

    data.NumCaso = sNM_EDO;
    data.sCa = app.getCA();

    // borramos la solapa por si acaso exist�a; as� actualizamos
    tabPanel.BorrarSolapa("Protocolo Notificador");
    pProtocolo = null;

    // modificacion 19/06/2000
    // se envia a paneltuber el pantablanotif
    //pProtocolo = new PanelTuber(this.app, data, this);//MIO!!!!!!!
    pProtocolo = new PanelTuber(this.app, data, this, panTabla); //MIO!!!!!!!

    if (!pProtocolo.pnl.getNoSeEncontraronDatos()) {
      YaSalioMsgEnferSinProtocolo = false;
      tabPanel.InsertarPanel("Protocolo Notificador", pProtocolo);
      pProtocolo.doLayout();
      tabPanel.VerPanel(sCabecera);
    }
    else {
      if (!YaSalioMsgEnferSinProtocolo) {
        Common.ShowWarning(this.app,
                           "La enfermedad no tiene un protocolo definido.");

      }
      YaSalioMsgEnferSinProtocolo = true;
      pProtocolo = null;
    }
  }

  //
  public void addCaso() {
    tabPanel.BorrarSolapa("Caso");
    panCaso = null;
    panCaso = new PanCaso(this, modoOperacion, hashListas_completa);
    tabPanel.InsertarPanel("Caso", panCaso);
    panCaso.doLayout();
    tabPanel.VerPanel(sCabecera);
  }

  public void addOtros() { //PDP 06/03/2000
    tabPanel.BorrarSolapa("Otros");
    panOtros = null;
    panOtros = new PanOtros(this, modoOperacion, hashListas_completa, sNM_EDO);
    tabPanel.InsertarPanel("Otros", panOtros);
    panOtros.doLayout();
    tabPanel.VerPanel(sCabecera);
  }

  public void borrarProtocolo() {
    tabPanel.BorrarSolapa("Protocolo Notificador");
    tabPanel.VerPanel(sCabecera);
  }

  public void borrarCaso() {
    tabPanel.BorrarSolapa("Caso");
    tabPanel.VerPanel(sCabecera);
  }

  public void borrarOtros() { //PDP 06/03/2000
    tabPanel.BorrarSolapa("Otros");
    tabPanel.VerPanel(sCabecera);
  }

} //FIN CLASE

class DiaActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialogTub adaptee;
  ActionEvent evt;

  DiaActionAdapter(DialogTub adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if (evt.getActionCommand().equals("aceptar")) {
      adaptee.btnAceptar_actionPerformed();
    }
    else if (evt.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed();
    }
  }
} //fin class
