/* Clase que extiende de CSolapa del paquete capp para sobreescribir el
 // metodo mousedown que controla el evento sobre la pesta�a de
 // un tabpanel y asi poder obtener para cada notificador su protocolo
 // autor: JLT
 // fecha: 13/06/2000
 */

package tuberculosis.cliente.diatuber;

import java.awt.Event;

import capp.CSolapa;
import infproto.PanelTuber;
import tuberculosis.cliente.pancabecera.PanCabecera;
import tuberculosis.cliente.pancaso.PanCaso;
import tuberculosis.cliente.panotros.PanOtros;
// nuevos import
import tuberculosis.cliente.pantubnotif.PanTablaNotif;

public class CSolapaext
    extends CSolapa {

  //Eventos sobre el canvas.
  public boolean mouseDown(Event evt, int x, int y) {

    if (name.size() == 0) {
      return false;
    }

    chosen = x / (getBounds().width / name.size());

    if ( ( (String) name.elementAt(chosen)).equals("Protocolo Notificador")) {
      PanelTuber miPanel = null;
      miPanel = ( (PanelTuber) ( (CTabPanelExt) getParent()).getPanelActual());
      miPanel.recuperarProtocolo();

      paint(getGraphics());
      ( (CTabPanelExt) getParent()).VerPanel( (String) name.elementAt(chosen));

    }
    else if ( ( (String) name.elementAt(chosen)).equals("Notificadores")) {
      PanTablaNotif miPanelN = null;
      miPanelN = ( (PanTablaNotif) ( (CTabPanelExt) getParent()).getPanelActual());
      miPanelN.ChangedProtocolo();

      paint(getGraphics());
      ( (CTabPanelExt) getParent()).VerPanel( (String) name.elementAt(chosen));

    }
    else if ( ( (String) name.elementAt(chosen)).equals("Datos de filiaci�n")) {
      PanCabecera miPanelCA = null;
      miPanelCA = ( (PanCabecera) ( (CTabPanelExt) getParent()).getPanelActual());
      miPanelCA.ChangedProtocolo();

      paint(getGraphics());
      ( (CTabPanelExt) getParent()).VerPanel( (String) name.elementAt(chosen));

    }
    else if ( ( (String) name.elementAt(chosen)).equals("Caso")) {
      PanCaso miPanelC = null;
      miPanelC = ( (PanCaso) ( (CTabPanelExt) getParent()).getPanelActual());
      miPanelC.ChangedProtocolo();

      paint(getGraphics());
      ( (CTabPanelExt) getParent()).VerPanel( (String) name.elementAt(chosen));

    }
    else if ( ( (String) name.elementAt(chosen)).equals("Otros")) {
      PanOtros miPanelO = null;
      miPanelO = ( (PanOtros) ( (CTabPanelExt) getParent()).getPanelActual());
      miPanelO.ChangedProtocolo();

      paint(getGraphics());
      ( (CTabPanelExt) getParent()).VerPanel( (String) name.elementAt(chosen));

    }

    return true;
  } // fin metodo

}
