/**
 * Clase: PanMntRegTub
 * Paquete: SP_Tuberculosis.cliente.mantregtub
 * Hereda: CPanel
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 15/10/1999
 * Analisis Funcional: Punto 2. Mantenimiento Registro Tuberculosis.
 * Descripcion: Implementacion del panel que permite visualizar los datos
    principales de registros de tuberculosis. Los registros mostrados dependen
    de los valores suministrados a los campos de filtrado.
    Ademas permite acceder a la pantalla de modificacion/cierre/deshacer cierre
    de los datos asociados a un determinado registro de tuberculosis.
 * 11/11/99 (JMT): Uso del servlet que trae los cat�logos: Motivos de salida,
 * Pa�ses y Comunidades Aut�nomas
  Modificado: 18/01/2000
              Emilio Postigo Riancho
  Descripci�n de los cambios: Se a�aden variables tipo cadena para
  controlar los cambios efectuados en las cajas de texto, de manera que la p�rdida de
  foco s�lo se ejecute en caso de que haya alg�n cambio.
 */

package tuberculosis.cliente.mantregtub;

import java.net.URL;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Vector;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import capp.CTabla;
import capp.USUButtonControl;
import comun.Common;
import comun.Comunicador;
import comun.DataEnfermo;
import comun.constantes;
import enfermotub.DialBusEnfermo;
import jclass.bwt.JCActionEvent;
import jclass.util.JCVector;
import sapp.StubSrvBD;
import tuberculosis.cliente.diamntregtub.DiaMntRegTub;
import tuberculosis.cliente.diatuber.DialogTub;
import tuberculosis.datos.mantregtub.DatMntRegTubCS;
import tuberculosis.datos.mantregtub.DatMntRegTubSC;
import tuberculosis.datos.notiftub.DatRT;

//import tuberculosis.servidor.mantcasotub.*;

//import tuberculosis.servidor.mantregtub.*;

public class PanMntRegTub
    extends CPanel {

  // Modos de entrada al panel
  final int modoMANTENIMIENTO = constantes.modoMANTENIMIENTO;
  final int modoCERRAR = constantes.modoCERRAR;
  final int modoDESHACERCIERRE = constantes.modoDESHACERCIERRE;
  final int modoCASOSTUB = constantes.modoCASOSTUB;

  int modoEntrada = modoMANTENIMIENTO; // Por defecto

  // Modos de operacion del panel
  final int modoESPERA = 0;
  final int modoBUSCAR = 1;
  int modoOperacion = modoBUSCAR; // Por defecto

  // Flags de permisos de usuario (solo existe el permiso de modificacion)
  boolean bAlta = false;
  boolean bMod = false;
  boolean bBaja = false;
  boolean bConsulta = false;

  // Keys Modo Usu
  String keyBtnAnadir = "";
  String keyBtnModificar = "";
  String keyBtnBorrar = "";

  String claveBtnAnadir = "20001";
  String claveBtnModificar = "20002";
  String claveBtnBorrar = "20003";

  // Modo de representacion del nombre del enfermo (segun flag confidencial)
  boolean bNombreCompleto = true;

  // Datos para la busqueda de registros
  private DatMntRegTubCS dataBusqueda = null;

  // Lista de los parametros de busqueda
  public CLista listaBusquedaRegTub = null;

  // Lista de Registros de Tuberculosis/o CASOS
  public CLista listaRegTubs = null;

  // Cat�logos utilizados por este panel o paneles llamados desde �ste
  public Hashtable catalogos = new Hashtable();

  // Stub's
  protected StubSrvBD stubCliente = null;

  // Localizacion del servlet
//  final String strSERVLET_CASOSTUB = "servlet/SrvMntCasos";
  final String strSERVLET_CASOSTUB = constantes.strSERVLET_CASOSTUB;

//  final String strSERVLET_REGTUBS = "servlet/SrvMntRegTub";
  final String strSERVLET_REGTUBS = constantes.strSERVLET_REGTUBS;

//  final String strSERVLET_CATALOGOS = "servlet/SrvCatRegTub";
  final String strSERVLET_CATALOGOS = constantes.strSERVLET_CATALOGOS;

//  final String strSERVLET_CATALOGOS_CASOSTUB = "servlet/SrvCatCasoTub";
  final String strSERVLET_CATALOGOS_CASOSTUB = constantes.
      strSERVLET_CATALOGOS_CASOSTUB;

  // Para gestionar cambios en cajas de texto
  protected String s_Cod_Enfermo = "";
  protected String s_Fecha_Desde = "";
  protected String s_Fecha_Hasta = "";
  protected String s_Ano = "";
  protected String s_Registro = "";

  // Contenedores de im�genes
  protected CCargadorImagen imgs = null;
  protected CCargadorImagen imgs_mantenimiento = null;
  protected CCargadorImagen imgs_tabla = null;
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/refrescar.gif",
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/declaracion.gif",
      "images/declaracion2.gif"};

  final String imgNAME_mantenimiento[] = {
      "images/alta2.gif",
      "images/modificacion2.gif",
      "images/baja2.gif"};
  final String imgNAME_tabla[] = {
      "images/primero.gif",
      "images/anterior.gif",
      "images/siguiente.gif",
      "images/ultimo.gif"};

  /* -------------------- CONTROLES ------------------------ */

  // Organizacion del panel
  private XYLayout lyXYLayout = new XYLayout();

  // A�o/Registro
  private Label lblAnoReg = new Label("A�o/Registro:");
  private TextField txtAno = new TextField();
  private boolean btxtAno = true;
  private Label lblBarra = new Label("/");
  private TextField txtReg = new TextField();
  private boolean btxtReg = true;

  // Codigo enfermo
  private Label lblCodEnfermo = new Label("Cod. Enfermo:");
  private TextField txtCodEnfermo = new TextField();
  private boolean btxtCodEnfermo = true;
  private ButtonControl btnLupaEnfermo = new ButtonControl();
  private boolean bbtnLupaEnfermo = true;

  // Fecha entrada Desde-Hasta
  private Label lblFechaEnt = new Label("Fecha Entrada");
  private Label lblDesde = new Label("Desde:");
  // Fechas corregidas. Ponen barras autom�ticamente.
  // ARS 23-05-01
//  private CFechaSimple txtFechaDesde = new CFechaSimple("N");
//  private CFechaSimple txtFechaHasta = new CFechaSimple("N");
  fechas.CFecha txtFechaDesde = new fechas.CFecha("N");
  fechas.CFecha txtFechaHasta = new fechas.CFecha("N");
  private boolean btxtFechaDesde = true;
  private Label lblHasta = new Label("Hasta:");
  private boolean btxtFechaHasta = false;

  // Boton Buscar
  private ButtonControl btnBuscar = new ButtonControl();

  // Tabla que visualiza y permite navegar entre los registros tuberculosis
  public CTabla tabla = new CTabla();

  // Controles para el mantenimiento y navegacion entre los registros de la tabla
  //private ButtonControl btnAlta = null;
  //private ButtonControl btnModificar = null;
  //private ButtonControl btnBaja = null;
  private USUButtonControl btnAlta = null;
  private USUButtonControl btnModificar = null;
  private USUButtonControl btnBaja = null;

  private ButtonControl btnPrimero = null;
  private ButtonControl btnAnterior = null;
  private ButtonControl btnSiguiente = null;
  private ButtonControl btnUltimo = null;

  // Botones de cierre y deshacer cierre del registro de tuberculosis
  private ButtonControl btnCerrar = new ButtonControl();
  private ButtonControl btnDeshacerCierre = new ButtonControl();

  // Escuchadores
  PanMntRegTubFocusAdapter focusAdapter = null;
  PanMntRegTubActionAdapter actionAdapter = null;
  PanMntRegTubTablaDobleClick tablaDobleClickListener = null;
  PanMntRegTubTablaClick tablaClickListener = null;

  /* CONSTRUCTOR */
  public PanMntRegTub(CApp a, int modoVent) {
    try {

      setApp(a);
      setBorde(false);

      modoEntrada = modoVent;
      stubCliente = new StubSrvBD(new URL(this.app.getURL() +
                                          strSERVLET_REGTUBS));

      //Gesti�n claves de Usu
      keyBtnAnadir = claveBtnAnadir;
      keyBtnModificar = claveBtnModificar;
      keyBtnBorrar = claveBtnBorrar;
      jbInit();

      switch (modoEntrada) {
        case modoCASOSTUB:
          btnCerrar.setVisible(false);
          btnDeshacerCierre.setVisible(false);
          btnAlta.setVisible(true);
          btnBaja.setVisible(true);
          btnModificar.setVisible(true);
          break;
        case modoMANTENIMIENTO:
          btnCerrar.setVisible(false);
          btnDeshacerCierre.setVisible(false);
          btnAlta.setVisible(false);
          btnBaja.setVisible(false);
          btnModificar.setVisible(true);
          break;
        case modoCERRAR:
          btnCerrar.setVisible(true);
          btnDeshacerCierre.setVisible(false);
          btnAlta.setVisible(false);
          btnBaja.setVisible(false);
          btnModificar.setVisible(false);
          break;
        case modoDESHACERCIERRE:
          btnCerrar.setVisible(false);
          btnDeshacerCierre.setVisible(true);
          btnAlta.setVisible(false);
          btnBaja.setVisible(false);
          btnModificar.setVisible(false);
          break;
      }
      // Carga de los cat�logos desde la BD (Motivos de salida, Pa�ses y CAs)
      // Son cargados en la Hashtable denominada catalogos
      CargarListas();

      if (!this.app.getANYO_DEFECTO().equals("")) {
        txtAno.setText(a.getANYO_DEFECTO());

      }
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // Funcion de inicializacion de los controles del panel
  void jbInit() throws Exception {

    // Vars. para colocar los componentes
    int pX = 0; // Posicion en X
    int pY = 0; // Posicion en Y
    int longX = 0; // Longitud en X de un componente
    int longY = 0; // Longitud en Y de un componente

    // Obtencion del a�o actual
    Calendar cal = Calendar.getInstance();
    int iAnoActual;

    // Clase escuchadora de los eventos relacionados con el foco
    focusAdapter = new PanMntRegTubFocusAdapter(this);
    actionAdapter = new PanMntRegTubActionAdapter(this);
    tablaDobleClickListener = new PanMntRegTubTablaDobleClick(this);
    tablaClickListener = new PanMntRegTubTablaClick(this);

    // Medidas y organizacion del panel
    this.setSize(new Dimension(608, 304));
    lyXYLayout.setHeight(370);
    lyXYLayout.setWidth(680);
    this.setLayout(lyXYLayout);

    // Carga de las imagenes generales (lupa, buscar)
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    // A�o/Registro del registro de tuberculosis. A�o actual por defecto
    pX = 15;
    longX = 90;
    pY = 15;
    longY = -1;
    this.add(lblAnoReg, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 40;
    txtAno.setBackground(new Color(255, 255, 150));
    this.add(txtAno, new XYConstraints(pX, pY, longX, longY));
    iAnoActual = cal.get(cal.YEAR);
    txtAno.setText(new Integer(iAnoActual).toString());
    txtAno.setName("Ano");
    txtAno.addFocusListener(focusAdapter);
    txtAno.requestFocus();

    pX += longX + 5;
    longX = 5;
    this.add(lblBarra, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 50;
    this.add(txtReg, new XYConstraints(pX, pY, longX, longY));
    txtReg.setName("Registro");
    txtReg.addFocusListener(focusAdapter);

    // Codigo del enfermo
    pX = 15;
    longX = 80;
    pY += 30;
    longY = -1;
    this.add(lblCodEnfermo, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 20;
    longX = 60;
    this.add(txtCodEnfermo, new XYConstraints(pX, pY, longX, longY));
    btnLupaEnfermo.setImage(imgs.getImage(0));
    pX += longX + 10;
    longX = -1;
    this.add(btnLupaEnfermo, new XYConstraints(pX, pY, longX, longY));
    txtCodEnfermo.setName("CodEnfermo");
    txtCodEnfermo.addFocusListener(focusAdapter);
    btnLupaEnfermo.setActionCommand("LupaEnfermo");
    btnLupaEnfermo.addActionListener(actionAdapter);

    // Fecha Entrada:  Desde-Hasta
    pX = 15;
    longX = 95;
    pY += 30;
    longY = -1;
    this.add(lblFechaEnt, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 5;
    longX = 39;
    this.add(lblDesde, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 75;
    this.add(txtFechaDesde, new XYConstraints(pX, pY, longX, longY));
    txtFechaDesde.setName("FechaDesde");
    txtFechaDesde.addFocusListener(focusAdapter);
    pX += longX + 20;
    longX = 35;
    this.add(lblHasta, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 75;
    this.add(txtFechaHasta, new XYConstraints(pX, pY, longX, longY));
    txtFechaHasta.setName("FechaHasta");
    txtFechaHasta.addFocusListener(focusAdapter);

    // Boton de busqueda de registros
    btnBuscar.setImage(imgs.getImage(1));
    btnBuscar.setLabel("Buscar");
    btnBuscar.setActionCommand("Buscar");
    btnBuscar.addActionListener(actionAdapter);
    pX += longX + 100 + 75;
    longX = -1;
    this.add(btnBuscar, new XYConstraints(pX, pY, longX, longY));

    // Se establecen los parametros de la tabla
    PintarTabla();
    pX = 15;
    pY += 45;
    longX = 557 + 75;
    longY = 125;
    this.add(tabla, new XYConstraints(pX, pY, longX, longY));
    tabla.addActionListener(tablaDobleClickListener);
    tabla.addItemListener(tablaClickListener);

    // Carga de las im�genes de mantenimiento
    imgs_mantenimiento = new CCargadorImagen(app, imgNAME_mantenimiento);
    imgs_mantenimiento.CargaImagenes();

    // Creacion de los botones de mantenimiento de los registros de la tabla
    //btnAlta =  new ButtonControl(imgs_mantenimiento.getImage(0));
    //btnModificar  = new ButtonControl(imgs_mantenimiento.getImage(1));
    //btnBaja = new ButtonControl(imgs_mantenimiento.getImage(2));
    btnAlta = new USUButtonControl(keyBtnAnadir, app);
    btnAlta.setImage(imgs_mantenimiento.getImage(0));
    btnModificar = new USUButtonControl(keyBtnModificar, app);
    btnModificar.setImage(imgs_mantenimiento.getImage(1));
    btnBaja = new USUButtonControl(keyBtnBorrar, app);
    btnBaja.setImage(imgs_mantenimiento.getImage(2));

    // Asignaci�n a variables booleanas
    bAlta = btnAlta.isEnabled();
    bBaja = btnBaja.isEnabled();
    bMod = btnModificar.isEnabled();
    bConsulta = ! (bAlta || bBaja || bMod);

    btnModificar.setActionCommand("Modificar");
    btnModificar.addActionListener(actionAdapter);
    btnAlta.setActionCommand("Alta");
    btnAlta.addActionListener(actionAdapter);
    btnBaja.setActionCommand("Baja");
    btnBaja.addActionListener(actionAdapter);

    pX = 15;
    longX = 25;
    pY += longY + 10;
    longY = -1;
    this.add(btnAlta, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnModificar, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnBaja, new XYConstraints(pX, pY, longX, longY));

    // Carga de las im�genes de navegacion
    imgs_tabla = new CCargadorImagen(app, imgNAME_tabla);
    imgs_tabla.CargaImagenes();

    // Creacion de los botones de navegacion por los registros de la tabla
    btnPrimero = new ButtonControl(imgs_tabla.getImage(0));
    btnPrimero.setActionCommand("Primero");
    btnPrimero.addActionListener(actionAdapter);
    btnAnterior = new ButtonControl(imgs_tabla.getImage(1));
    btnAnterior.setActionCommand("Anterior");
    btnAnterior.addActionListener(actionAdapter);
    btnSiguiente = new ButtonControl(imgs_tabla.getImage(2));
    btnSiguiente.setActionCommand("Siguiente");
    btnSiguiente.addActionListener(actionAdapter);
    btnUltimo = new ButtonControl(imgs_tabla.getImage(3));
    btnUltimo.setActionCommand("Ultimo");
    btnUltimo.addActionListener(actionAdapter);
    pX = 440 + 75;
    longX = 25;
    longY = 25;
    this.add(btnPrimero, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnAnterior, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnSiguiente, new XYConstraints(pX, pY, longX, longY));
    pX += longX + 10;
    longX = 25;
    this.add(btnUltimo, new XYConstraints(pX, pY, longX, longY));

    // Botones de cerrar o deshacer cierre del registro
    btnCerrar = new ButtonControl(imgs.getImage(4));
    btnCerrar.setLabel("Cerrar");
    btnCerrar.setActionCommand("Cerrar");
    btnCerrar.addActionListener(actionAdapter);
    btnDeshacerCierre = new ButtonControl(imgs.getImage(5));
    btnDeshacerCierre.setLabel("Deshacer Cierre");
    btnDeshacerCierre.setActionCommand("Deshacer Cierre");
    btnDeshacerCierre.addActionListener(actionAdapter);
    switch (modoEntrada) {
      case modoMANTENIMIENTO:

        /*System.out.println("Modo MANTENIMIENTO");
                 btnCerrar.setVisible(false);
                 btnDeshacerCierre.setVisible(false);*/
        break;
      case modoCERRAR:

        //System.out.println("Modo CERRAR");
        pX = 150;
        longX = 100;
        this.add(btnCerrar, new XYConstraints(pX, pY, longX, longY));
        break;
      case modoDESHACERCIERRE:

        //System.out.println("Modo DESHACERCIERRE");
        pX = 150;
        longX = 125;
        this.add(btnDeshacerCierre, new XYConstraints(pX, pY, longX, longY));
        break;
    }

  } // Fin jbInit()

  /*// Recogida los flags de usuario del .html
   public void GetPermisosUsuario() {
     // Boton de alta
     //if  (this.getApp().getIT_AUTALTA().equals("S"))
       bAlta = true;
     //else
     //  bAlta = false;
     // Boton de baja
     //if  (this.getApp().getIT_AUTBAJA().equals("S"))
       bBaja = true;
     //else
     //  bBaja = false;
     // Boton de modificacion
     //if  (this.getApp().getIT_AUTMOD().equals("S"))
       bMod = true;
     //else
     //  bMod = false;
   }// Fin GetPermisosUsuario()   */

 public void Permisos() {
   // habilitar los botones btnAlta, btnBaja, btnModificacion
   // en funci�n de los flags de usuario y de sus autorizaciones
   btnAlta.setEnabled(bAlta);
   btnBaja.setEnabled(bBaja);
   if (!bMod) { // Se prueba modo consulta
     btnModificar.setEnabledForzado(bConsulta);
   }
   else { // debe ser true
     btnModificar.setEnabled(true);
   }
 }

  // Establece el numero de columnas de la tabla y las cabeceras de las mismas
  public void PintarTabla() {
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "100\n100\n350\n55"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String("Registro\nF.Entrada\nEnfermo\nCerrado"), '\n'));
    tabla.setNumColumns(4);
    tabla.setRowHeight(20);
    tabla.setScrollbarDisplay(jclass.bwt.BWTEnum.DISPLAY_VERTICAL_ONLY);
    tabla.setInsets(new Insets(5, 5, 5, 5));
  } // fin PintarTabla()

  private CLista CargarListas() {

    boolean b = false;

    CLista parametros = new CLista();
    CLista listaSalida = new CLista();
    try {
      if (modoEntrada == modoCASOSTUB) {
        listaSalida = Comunicador.Communicate(this.app,
                                              stubCliente,
                                              0,
                                              strSERVLET_CATALOGOS_CASOSTUB,
                                              parametros);
      }
      else {
        listaSalida = Comunicador.Communicate(this.app,
                                              stubCliente,
                                              0,
                                              strSERVLET_CATALOGOS,
                                              parametros);

      }
      if (listaSalida != null) {
        if (listaSalida.size() > 0) {
          if (modoEntrada == modoCASOSTUB) {
            catalogos.put(constantes.CLASIFICACIONES,
                          (CLista) listaSalida.elementAt(0));
            catalogos.put(constantes.PAISES, (CLista) listaSalida.elementAt(1));
            catalogos.put(constantes.CA, (CLista) listaSalida.elementAt(2));
            catalogos.put(constantes.TRAMEROS, (CLista) listaSalida.elementAt(3));
            catalogos.put(constantes.NUMEROS, (CLista) listaSalida.elementAt(4));
            catalogos.put(constantes.CALIFICADORES,
                          (CLista) listaSalida.elementAt(5));
            catalogos.put(constantes.SEXO, (CLista) listaSalida.elementAt(6));
            catalogos.put(constantes.TDOC, (CLista) listaSalida.elementAt(7));
            catalogos.put(constantes.FUENTESNOTIF,
                          (CLista) listaSalida.elementAt(8));
            catalogos.put(constantes.MOTIVOS_TRATAMIENTO,
                          (CLista) listaSalida.elementAt(9));
            // Para muestras (resultados de laboratorio)
            catalogos.put(constantes.TIPO_TECNICALAB,
                          (CLista) listaSalida.elementAt(10));
            catalogos.put(constantes.MUESTRA_LAB,
                          (CLista) listaSalida.elementAt(11));
            catalogos.put(constantes.VALOR_MUESTRA,
                          (CLista) listaSalida.elementAt(12));
            catalogos.put(constantes.TMICOBACTERIA,
                          (CLista) listaSalida.elementAt(13));
            catalogos.put(constantes.ESTUDIORESIS,
                          (CLista) listaSalida.elementAt(14));
          }
          else {
            catalogos.put("MOTIVOS_SALIDA", (CLista) listaSalida.elementAt(0));
            catalogos.put(constantes.PAISES, (CLista) listaSalida.elementAt(1));
            catalogos.put(constantes.CA, (CLista) listaSalida.elementAt(2));
            catalogos.put("SEXOS", (CLista) listaSalida.elementAt(3));
          }
          // Si se est� dando de alta, debe mostrarse el di�logo de RT
          // que necesita estos datos
          if (modoEntrada == modoCASOSTUB) {
            parametros = new CLista();
            listaSalida = new CLista();
            listaSalida = Comunicador.Communicate(this.app,
                                                  stubCliente,
                                                  0,
                                                  strSERVLET_CATALOGOS,
                                                  parametros);

            if (listaSalida != null) {
              if (listaSalida.size() > 0) {
                catalogos.put("MOTIVOS_SALIDA",
                              (CLista) listaSalida.elementAt(0));
                catalogos.put("SEXOS", (CLista) listaSalida.elementAt(3));
              }
            }

          }
        }
        else {
          ShowWarning("No existen datos para rellenar los cat�logos.");
          b = true;
        }
      }
      else {
        ShowWarning("Error al recuperar los cat�logos.");
        b = true;
      }

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      ShowWarning("Error al recuperar los cat�logos.");
      b = true;
    }

    // Si no se pueden recuperar los cat�logos semuestra la p�gina HTML anterior
    if (b) {
      try {
        System.runFinalization();
        System.gc();
        app.getAppletContext().showDocument(new URL(app.getCodeBase(),
            "default.html"), "_self");
      }
      catch (Exception excepc) {
        ;
      }
    }

    return (listaSalida);
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar(modo)

  // Implementacion del metodo abstracto de Cpanel
  public void Inicializar() {
    //boolean bFechaHasta;

    switch (modoOperacion) {
      case modoBUSCAR:

        // Registro, C�digo enfermo y Fechas desde y hasta
        txtAno.setEnabled(btxtAno);
        txtAno.setEditable(btxtAno);
        txtReg.setEnabled(btxtReg);
        txtReg.setEditable(btxtReg);
        txtCodEnfermo.setEnabled(btxtCodEnfermo);
        txtCodEnfermo.setEditable(btxtCodEnfermo);
        btnLupaEnfermo.setEnabled(bbtnLupaEnfermo);
        txtFechaDesde.setEnabled(btxtFechaDesde);
        txtFechaDesde.setEditable(btxtFechaDesde);
        txtFechaHasta.setEnabled(btxtFechaHasta);
        txtFechaHasta.setEditable(btxtFechaHasta);
        btnBuscar.setEnabled(true);

        btnAlta.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBaja.setEnabled(false);

        // Si hay datos en la tabla se habilitan los controles de la misma
        if (tabla.countItems() > 0) {
          // Botones de navegacion por la table
          btnPrimero.setEnabled(true);
          btnAnterior.setEnabled(true);
          btnSiguiente.setEnabled(true);
          btnUltimo.setEnabled(true);

          // Botones de Cerrar y DeshacerCierre
          btnCerrar.setEnabled(true);
          btnDeshacerCierre.setEnabled(true);

          // Botones de alta/baja/mod de un registro de la tabla (segun las
          // autorizaciones ya establecidas)
          Permisos();

          // Se habilita la tabla
          tabla.setEnabled(true);
        }
        else { //no hay en la tabla
          Permisos(); //si procede, alta habilitada

          btnModificar.setEnabled(false);
          btnBaja.setEnabled(false);
          btnCerrar.setEnabled(false);
          btnDeshacerCierre.setEnabled(false);

          // Deshabilitamos los botones de navegacion por la tabla
          btnPrimero.setEnabled(false);
          btnAnterior.setEnabled(false);
          btnSiguiente.setEnabled(false);
          btnUltimo.setEnabled(false);

          tabla.setEnabled(false);

        } // Fin if(tabla.countItems() > 0)

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:

        // Registro, C�digo enfermo y Fechas desde y hasta
        txtAno.setEnabled(false);
        txtAno.setEditable(false);
        txtReg.setEnabled(false);
        txtReg.setEditable(false);
        txtCodEnfermo.setEnabled(false);
        txtCodEnfermo.setEditable(false);
        btnLupaEnfermo.setEnabled(false);
        txtFechaDesde.setEnabled(false);
        txtFechaDesde.setEditable(false);
        txtFechaHasta.setEnabled(false);
        txtFechaHasta.setEditable(false);
        btnBuscar.setEnabled(false);

        // Botones de modificaci�n/navegaci�n por la tabla
        btnAlta.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBaja.setEnabled(false);
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);

        // Botones de Cerrar y DeshacerCierre
        btnCerrar.setEnabled(false);
        btnDeshacerCierre.setEnabled(false);

        // Deshabilitamos la tabla
        tabla.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }

    // Repintado de la pantalla
    this.doLayout();
  } // Fin Inicializar()

  // Ganancia de foco de txtAno
  void txtAnoFocusGained() {
    s_Ano = new String(txtAno.getText().trim());
  }

  // Ganancia de foco de txtReg
  void txtRegFocusGained() {
    s_Ano = new String(txtReg.getText().trim());
  }

  // Ganancia de foco de txtCodEnfermo
  void txtCodEnfermoFocusGained() {
    s_Ano = new String(txtCodEnfermo.getText().trim());
  }

  // Ganancia de foco de txtFechaDesde
  void txtFechaDesdeFocusGained() {
    s_Fecha_Desde = new String(txtFechaDesde.getText().trim());
  }

  // Ganancia de foco de txtFechaHasta
  void txtFechaHastaFocusGained() {
    s_Fecha_Hasta = new String(txtFechaHasta.getText().trim());
  }

  // P�rdida de foco de txtAno
  void txtAnoFocusLost() {
    // Se ajusta el contenido
    txtAno.setText(txtAno.getText().trim());

    // Obtiene el foco
    boolean bFocus = false;

    int iAnoActual = 0;
    Calendar cal = Calendar.getInstance();
    iAnoActual = cal.get(cal.YEAR);

    String sReserva = this.app.getANYO_DEFECTO(); //por si hay un valor por defecto
    if (sReserva.equals("")) {
      sReserva = new Integer(iAnoActual).toString();

      // Comprobaci�n del valor de txtAno
      // El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo
    }
    try {
      Integer f = new Integer(txtAno.getText());
      int ano = f.intValue();
      if ( (ano < 1000) || (ano > 9999)) {
        Common.ShowWarning(this.app,
            "El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo.");
        txtAno.setText(sReserva);
        bFocus = true;
      }
    }
    catch (Exception e) {
      Common.ShowWarning(this.app,
          "El a�o es un valor obligatorio, num�rico, de 4 d�gitos y positivo.");
      txtAno.setText(sReserva);
      bFocus = true;
    }

    // Se resetea la tabla (el a�o puede ser diferente)
    if (!txtAno.getText().equals(s_Ano)) {
      ResetearTabla();
    }

    if (bFocus) {
      txtAno.requestFocus();
    }
  } // Fin txtA�oFocusLost()

  // Perdida de foco de txtReg
  void txtRegFocusLost() {
    // Se ajusta el contenido
    txtReg.setText(txtReg.getText().trim());

    // Anterior modo
    int modo = modoOperacion;

    // Seguir la comprobacion del campo txtReg
    boolean bSeguirCompr = true;

    // Obtiene el foco
    boolean bFocus = false;

    // Si no contiene nada no se hacen mas comprobaciones
    if (txtReg.getText().equals("")) {
      bSeguirCompr = false;

      btxtCodEnfermo = true;
      bbtnLupaEnfermo = true;
      btxtFechaDesde = true;
      if (txtFechaDesde.getText().equals("")) {
        btxtFechaHasta = false;
      }
      else {
        btxtFechaHasta = true;

      }
      Inicializar(modoBUSCAR);
      txtCodEnfermo.requestFocus();
    }

    // Existe texto en el txtReg: Comprobaci�n del valor de txtReg
    // El registro es un valor num�rico, <= 5 d�gitos y positivo
    if (bSeguirCompr) {
      try {
        Integer f = new Integer(txtReg.getText());
        int reg = f.intValue();
        if ( (reg < 0) || (reg > 99999)) {
          ShowWarning(
              "El registro es un valor num�rico, menor o igual que 5 d�gitos y positivo.");
          txtReg.setText("");
          bSeguirCompr = false;
          bFocus = true;
        }
      }
      catch (Exception e) {
        ShowWarning(
            "El registro es un valor num�rico, menor o igual que 5 d�gitos y positivo.");
        txtReg.setText("");
        txtReg.requestFocus();
        bSeguirCompr = false;
        bFocus = true;
      }
    }

    // Se resetea la tabla (el numero registro puede ser diferente)
    if (!txtReg.getText().equals(s_Registro)) {
      ResetearTabla();
    }

    // Existe algo en txtReg y es correcto: deshabilitamos el resto de campos de filtro
    if (bSeguirCompr) {
      txtCodEnfermo.setText("");
      btxtCodEnfermo = false;
      bbtnLupaEnfermo = false;
      txtFechaDesde.setText("");
      btxtFechaDesde = false;
      txtFechaHasta.setText("");
      btxtFechaHasta = false;

      btnBuscar.requestFocus();
    }

    Inicializar(modoBUSCAR);

    if (bFocus) {
      txtReg.requestFocus();

      // Reestablecer el mod oanterior
    }
    Inicializar(modo);
  } // Fin txtRegFocusLost()

  // Perdida de foco de txtCodEnfermo
  void txtCodEnfermoFocusLost() {
    // Se ajusta el contenido
    txtCodEnfermo.setText(txtCodEnfermo.getText().trim());

    // Seguir la comprobacion del campo txtReg
    boolean bSeguirCompr = true;

    // Obtiene el foco
    boolean bFocus = false;

    // Si no contiene nada no se hacen mas comprobaciones
    if (txtCodEnfermo.getText().equals("")) {
      bSeguirCompr = false;

      // Existe texto en el txtCodEfermo: Comprobaci�n del valor de txtCodEnfermo
      // El registro es un valor num�rico, <= 6 d�gitos y positivo
    }
    if (bSeguirCompr) {
      try {
        Integer f = new Integer(txtCodEnfermo.getText());
        int reg = f.intValue();
        if ( (reg < 0) || (reg > 999999)) {
          ShowWarning("El c�digo de enfermo es un valor num�rico, menor o igual que 6 d�gitos y positivo.");
          txtCodEnfermo.setText("");
          bSeguirCompr = false;
          bFocus = true;
        }
      }
      catch (Exception e) {
        ShowWarning("El c�digo de enfermo es un valor num�rico, menor o igual que 6 d�gitos y positivo.");
        txtCodEnfermo.setText("");
        txtCodEnfermo.requestFocus();
        bSeguirCompr = false;
        bFocus = true;
      }
    }

    // Se resetea la tabla (el a�o puede ser diferente)
    if (!txtCodEnfermo.getText().equals(s_Cod_Enfermo)) {
      ResetearTabla();
    }

    if (bFocus) {
      txtCodEnfermo.requestFocus();

    }
  } // Fin txtCodEnfermoFocusLost()

  // Perdida de foco de txtFechaDesde
  void txtFechaDesdeFocusLost() {
    // Se ajusta el contenido
    txtFechaDesde.setText(txtFechaDesde.getText().trim());

    // Anterior modo
    int modo = modoOperacion;

    // Seguir la comprobacion del campo txtFechaDesde
    boolean bSeguirCompr = true;

    // Si no contiene nada no se hacen mas comprobaciones
    if (txtFechaDesde.getText().equals("")) {
      bSeguirCompr = false;
      txtFechaHasta.setText("");
      btxtFechaHasta = false;
      Inicializar(modoBUSCAR);
      btnBuscar.requestFocus();
    }

    // Comprobacion de formato valido de la fecha
    if (bSeguirCompr) {
      // Validacion de la fecha
      txtFechaDesde.ValidarFecha();

      // Carga de la fecha
      txtFechaDesde.setText(txtFechaDesde.getFecha());

      // Si la fecha es valida se habilita la Fecha Hasta
      if (txtFechaDesde.getValid().equals("S")) {
        btxtFechaHasta = true;
        Inicializar(modoBUSCAR);
        //txtFechaHasta.requestFocus();
      }
      else {
        txtFechaHasta.setText("");
        btxtFechaHasta = false;
        Inicializar(modoBUSCAR);
        //btnBuscar.requestFocus();
      }
    }

    // Transferencia del foco
    //txtFechaDesde.transferFocus();

    // Se resetea la tabla (el a�o puede ser diferente)
    if (!txtFechaDesde.getText().equals(s_Fecha_Desde)) {
      ResetearTabla();
    }

    // Reestablecer el modo
    Inicializar(modo);
  } // Fin txtFechaDesdeFocusLost()

  // Perdida de foco de txtFechaHasta
  void txtFechaHastaFocusLost() {
    // Se ajusta el contenido
    txtFechaHasta.setText(txtFechaHasta.getText().trim());

    // Seguir la comprobacion del campo txtFechaDesde
    boolean bSeguirCompr = true;

    // Si no contiene nada no se hacen mas comprobaciones
    if (txtFechaHasta.getText().equals("")) {
      bSeguirCompr = false;

      // Comprobacion de formato valido de la fecha
    }
    if (bSeguirCompr) {

      // Validacion de la fecha
      txtFechaHasta.ValidarFecha();

      // Carga de la fecha
      txtFechaHasta.setText(txtFechaHasta.getFecha());
    }

    // Se resetea la tabla (el a�o puede ser diferente)
    if (txtFechaHasta.getText().equals(s_Fecha_Hasta)) {
      ResetearTabla();
    }

  } // Fin txtFechaHastaFocusLost()

  // Manejador del Boton de Enfermos
  public void btnLupaEnfermoActionPerformed() {
    // Requiere el foco
    btnLupaEnfermo.requestFocus();

    CLista listaDeEnfermos = new CLista();
    Object o = new Object();
    DataEnfermo miEnfermo = new DataEnfermo("CD_ENFERMO");

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    // Abrimos el dialogo del enfermo para elegir el codigo de uno de ellos
    // y colocamos en el TextField el codigo del enfermo elegido
    DialBusEnfermo dial = new enfermotub.DialBusEnfermo(this.app,
        constantes.sSoloEnfermos, true);
    dial.show();
    listaDeEnfermos = (CLista) dial.getListaDatosEnfermo();
    if (listaDeEnfermos != null) {
      if (listaDeEnfermos.size() > 0) {
        miEnfermo = (DataEnfermo) listaDeEnfermos.firstElement();
        o = miEnfermo.get("CD_ENFERMO");
        txtCodEnfermo.setText(o.toString());
      }
    }

    // Eliminamos el dialogo del enfermo
    dial.dispose();
    dial = null;

    // Se resetea la tabla (ha podido cambiar el codigo del enfermo)
    ResetearTabla();

    // Reestablecemos el modo de operacion
    modoOperacion = modo;
    Inicializar(modoOperacion);
  } // Fin btnLupaEnfermoActionPerformed()

  // Manejador del Boton Buscar
  public void btnBuscarActionPerformed(boolean bMensaje) {
    // Primero se requiere el foco
    btnBuscar.requestFocus();

    // Modo ESPERA
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    // Creacion y relleno de la lista con los parametros de busqueda
    listaBusquedaRegTub = new CLista();
    CargarListaBusqueda();

    // Creacion de la lista de registros de tuberculosis, para que
    // acto seguido sea llamado el servlet
    listaRegTubs = new CLista();

    // Llamada al servlet: Intento de obtencion de los registros de tuberculosis
    try {

      if (modoEntrada == modoCASOSTUB) {
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_CASOSTUB));
      }
      else {
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_REGTUBS));

      }
      listaRegTubs = (CLista) stubCliente.doPost(0, listaBusquedaRegTub);
      listaRegTubs.trimToSize();

      // Escritura de los datos obtenidos en la tabla
      if (listaRegTubs != null) {
        if (listaRegTubs.size() > 0) {
          EscribirTabla();
        }
        else {
          if (bMensaje) {
            ShowWarning("No se encontraron registros de tuberculosis");
          }
        }
      }
      else {
        ShowWarning(
            "Error al realizar la b�squeda de registros de tuberculosis");
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      ShowWarning(
          "Error al recuperar los datos sobre registros de tuberculosis");
    }

    // Reestablecemos el modo de operacion
    modoOperacion = modo;
    Inicializar(modoOperacion);

  } // Fin btnBuscarActionPerformed()

  // Rellena la lista de busqueda con los parametros apropiados del panel
  void CargarListaBusqueda() {
    String Ano = txtAno.getText().trim();
    String Reg = txtReg.getText().trim();
    String CodEnfermo = txtCodEnfermo.getText().trim();
    String FDesde = txtFechaDesde.getText().trim();
    String FHasta = txtFechaHasta.getText().trim();
    String FCerrado = "";
    String FConfidencial = "";

    // Se establece FCerrado
    if (modoEntrada == modoMANTENIMIENTO ||
        modoEntrada == modoCERRAR) {
      FCerrado = "N";
    }
    else if (modoEntrada == modoDESHACERCIERRE) {
      FCerrado = "S";
    }
    else if (modoEntrada == modoCASOSTUB) {
      FCerrado = "";

      // Se establece FConfidencial
    }
    bNombreCompleto = (this.getApp().getIT_FG_ENFERMO()).equals("S");
    if (bNombreCompleto) {
      FConfidencial = "N";
    }
    else {
      FConfidencial = "S";

      // Creamos una estructura de datos de busqueda y la rellenamos
    }
    dataBusqueda = new DatMntRegTubCS(Ano, Reg, CodEnfermo, FDesde, FHasta,
                                      FCerrado, FConfidencial);

    // A�adimos la estructura a la lista de datos de busqueda
    listaBusquedaRegTub.addElement(dataBusqueda);
  } // Fin CargarListaBusqueda()

  //
  protected boolean esNivel1Autorizado(String cd_nivel_1) {
    boolean bValor = false;

    Vector vNivel1 = this.app.getCD_NIVEL_1_AUTORIZACIONES();

    for (int i = 0; i < vNivel1.size() && bValor == false; i++) {
      bValor = ( (String) vNivel1.elementAt(i)).equals(cd_nivel_1);
    }

    return bValor;
  }

  //
  protected boolean esNivel2Autorizado(String cd_nivel_2) {
    boolean bValor = false;

    Vector vNivel2 = this.app.getCD_NIVEL_2_AUTORIZACIONES();

    for (int i = 0; i < vNivel2.size() && bValor == false; i++) {
      bValor = ( (String) vNivel2.elementAt(i)).equals(cd_nivel_2);
    }

    return bValor;
  }

  // Devuelve true si el usuario tiene permisos sobre la fila
  // seleccionada (false en caso contrario)
  protected boolean hayPermiso(int indice) {
    boolean bValor = true;

    int iPerfil = this.app.getPerfil();

    if (iPerfil == 3 || iPerfil == 4) {
      bValor = esNivel1Autorizado(getNivel1Gestion(indice));
      if (bValor && iPerfil == 4) {
        bValor = esNivel2Autorizado(getNivel2Gestion(indice));
      }
    }

    return bValor;
  }

  // Para recuperar el c�digo de nivel 1 de un elemento de la tabla
  protected String getNivel1Gestion(int indice) {
    String s = null;

    if (indice >= 0 && indice < listaRegTubs.size()) {
      s = ( (DatMntRegTubSC) listaRegTubs.elementAt(indice)).getCD_NIVEL_1_GE();
    }

    return s;
  }

  // Para recuperar el c�digo de nivel 2 de un elemento de la tabla
  protected String getNivel2Gestion(int indice) {
    String s = null;

    if (indice >= 0 && indice < listaRegTubs.size()) {
      s = ( (DatMntRegTubSC) listaRegTubs.elementAt(indice)).getCD_NIVEL_2_GE();
    }

    return s;
  }

  // Cada registro de tuberculosis que proviene del sevlet se
  // introduce en la tabla
  public void EscribirTabla() {
    // Fila (registro tuberculosis)
    JCVector row = null;
    // Matriz de filas (registros de tuberculosis
    JCVector items = new JCVector();

    // Reseteo de la tabla
    ResetearTabla();

    // Datos de una fila de la tabla
    DatMntRegTubSC dataResult = null;

    for (int i = 0; i < listaRegTubs.size(); i++) {
      dataResult = (DatMntRegTubSC) listaRegTubs.elementAt(i);

      // Nuevo JCVector
      row = new JCVector();

      // A�o/Registro
      row.addElement(dataResult.getCD_ANO().trim() +
                     "/" +
                     dataResult.getCD_REG().trim());

      // F.Entrada
      row.addElement(dataResult.getFC_FECHAENTRADA().trim());

      // Enfermo (en funcion del flag de confidencialidad)
      if (bNombreCompleto) {
        row.addElement(dataResult.getCD_CODENFERMO().trim() + " - " +
                       dataResult.getDS_APE1ENFERMO().trim() + " " +
                       dataResult.getDS_APE2ENFERMO().trim() + " " +
                       dataResult.getDS_NOMBREENFERMO().trim());
      }
      else {
        row.addElement(dataResult.getCD_CODENFERMO().trim() + " - " +
                       dataResult.getDS_SIGLAS().trim());

      }

      if (dataResult.getIT_CERRADO().trim().equals("S")) {
        row.addElement(imgs.getImage(2));
      }
      else if (dataResult.getIT_CERRADO().trim().equals("N")) {
        row.addElement(imgs.getImage(3));

        /*// Cerrado
              switch(modoEntrada) {
         case modoCASOSTUB:
           row.addElement(imgs.getImage(3)); //NO cerrado
           break;
         case modoMANTENIMIENTO:
           row.addElement(imgs.getImage(3));
           break;
         case modoCERRAR:
           row.addElement(imgs.getImage(3));
           break;
         case modoDESHACERCIERRE:
           row.addElement(imgs.getImage(2)); //si cerrado
           break;
              } // Fin switch()   */

       // Se a�ade una fila a la matriz
      }
      items.addElement(row);
    } // Fin for()

    // Se establecen los  items
    tabla.setItems(items);

    // Verifica que sea la �ltima trama
    if (listaRegTubs.getState() == CLista.listaINCOMPLETA) {
      tabla.addItem("M�s datos ...");

      // Se selecciona el primer registro de la tabla
    }
    tabla.select(0);
    tabla.setTopRow(0);

    // Se repinta la tabla
    tabla.repaint();
  } // Fin EscribirTabla()

  // Resetea la tabla: borrado y repintado
  public void ResetearTabla() {
    tabla.clear();
    tabla.repaint();
    btnModificar.setEnabled(false);
    btnPrimero.setEnabled(false);
    btnAnterior.setEnabled(false);
    btnSiguiente.setEnabled(false);
    btnUltimo.setEnabled(false);
  } // Fin ResetearTabla()

  // Manejador del boton Primero
  void btnPrimeroActionPerformed() {
    if (tabla.countItems() > 0) {
      tabla.select(0);
      tabla.setTopRow(0);
    }
  } // Fin btnPrimeroActionPerformed()

  // Manejador del boton Anterior
  void btnAnteriorActionPerformed() {
    if (tabla.countItems() > 0) {
      int index = tabla.getSelectedIndex();
      if (index > 0) {
        index--;
        tabla.select(index);
        if (index - tabla.getTopRow() <= 0) {
          tabla.setTopRow(tabla.getTopRow() - 1);
        }
      }
      else {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    }
  } // Fin btnAnteriorActionPerformed()

  // Manejador del boton Siguiente
  void btnSiguienteActionPerformed() {
    if (tabla.countItems() > 0) {
      int ultimo = tabla.countItems() - 1;
      int index = tabla.getSelectedIndex();

      if (index >= 0 && index < ultimo) {
        index++;
        tabla.select(index);
        if (index - tabla.getTopRow() >= 4) {
          tabla.setTopRow(tabla.getTopRow() + 1);
        }
      }
      else {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    }
  } // Fin btnSiguienteActionPerformed()

  // Manejador del boton Ultimo
  void btnUltimoActionPerformed() {
    if (tabla.countItems() > 0) {
      int ultimo = tabla.countItems() - 1;

      if (ultimo >= 0) {
        tabla.select(ultimo);
        if (tabla.countItems() >= 4) {
          tabla.setTopRow(tabla.countItems() - 4);
        }
        else {
          tabla.setTopRow(0);
        }
      }
    }
  } // Fin btnUltimoActionPerformed()

  void btnAltaActionPerformed() {
    // Modo espera
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    // Creaci�n del dialogo
    MostrarCaso(constantes.modoALTA);

    // Se vac�a la tabla
    ResetearTabla();

    // Se recarga la tabla
    btnBuscarActionPerformed(false);

    // Se reestablece el modo
    modoOperacion = modo;
    Inicializar(modoOperacion);
  }

  void btnBajaActionPerformed() {
    // Modo espera
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    // Creaci�n del dialogo
    MostrarCaso(constantes.modoBAJA);

    // Se vac�a la tabla
    ResetearTabla();

    // Se recarga la tabla
    btnBuscarActionPerformed(false);

    // Se reestablece el modo
    modoOperacion = modo;
    Inicializar(modoOperacion);
  }

  void btnModificarActionPerformed() {

    // Modo espera
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    // Creaci�n del dialogo de modificaci�n
    if (modoEntrada == modoMANTENIMIENTO) {
      MostrarDialogoModificacion(modoMANTENIMIENTO);
    }
    else if (modoEntrada == modoCASOSTUB) {
      MostrarCaso(constantes.modoMODIFICACION);

      // Se reestablece el modo
    }
    modoOperacion = modo;
    Inicializar(modoOperacion);
  } // Fin btnModificarActionPerformed()

  // Manejador del click sobre una fila de la tabla
  void tablaClickPerformed() {
    bBaja = hayPermiso(tabla.getSelectedIndex());
    Inicializar();
  }

  // Manejador del doble-click en un registro de la tabla
  void TablaDobleClickActionPerformed() {

    boolean bModificar = btnModificar.isEnabled();
    if (bModificar) {

      // Modo espera
      int modo = modoOperacion;
      modoOperacion = modoESPERA;
      Inicializar(modoOperacion);

      // Si presionamos al "Mas Datos" se trae otro bloque de datos
      if ( (tabla.getSelectedIndex() == (tabla.countItems() - 1)) &&
          (listaRegTubs.getState() == CLista.listaINCOMPLETA)) {
        try {
          String sFiltro = listaRegTubs.getFilter();
          listaBusquedaRegTub.setFilter(sFiltro);
          CLista listaParcial = null;

          if (modoEntrada == modoCASOSTUB) {
            stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_CASOSTUB));
          }
          else {
            stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_REGTUBS));

          }
          listaParcial = (CLista) stubCliente.doPost(0, listaBusquedaRegTub);

          listaRegTubs.appendData(listaParcial);
          EscribirTabla();

        }
        catch (Exception e) {
          e.printStackTrace();
          ShowWarning(
              "Error al recuperar los datos sobre registros de tuberculosis");
        }

      }
      else { // Creaci�n del dialogo de modificaci�n
        if (modoEntrada == modoMANTENIMIENTO) {
          btnModificarActionPerformed();
        }
        else if (modoEntrada == modoCERRAR) {
          btnCerrarActionPerformed();
        }
        else if (modoEntrada == modoDESHACERCIERRE) {
          btnDeshacerCierreActionPerformed();
        }
        else if (modoEntrada == modoCASOSTUB) {
          btnModificarActionPerformed();
        }
      }

      // Se reestablece el modo
      modoOperacion = modo;
      Inicializar(modoOperacion);
    }
  } // Fin TablaDobleClickActionPerformed()

  private void MostrarDialogoModificacion(int modoVentana) {
    // Obtenci�n del registro seleccionado el a�o y el registro
    JCVector row = new JCVector();
    row = (JCVector) tabla.getSelectedItem();
    String anoreg = (String) row.getFirst();
    String pano = anoreg.substring(0, 4);
    String preg = anoreg.substring(5, anoreg.length());

    // Creaci�n del dialogo de modificaci�n
    int modoOpe = 0;
    if (!hayPermiso(tabla.getSelectedIndex()) || bConsulta) {
      modoOpe = constantes.modoCONSULTA;
    }
    else {
      modoOpe = constantes.modoMODIFICACION;

    }
    DatMntRegTubSC RegSel = (DatMntRegTubSC) listaRegTubs.elementAt(tabla.
        getSelectedIndex());
    String fentrada = RegSel.getFC_FECHAENTRADA();

    DiaMntRegTub dlg = new DiaMntRegTub(this.getApp(),
                                        modoVentana, pano, preg, catalogos,
                                        modoOpe, fentrada);

    if (dlg.iERROR != 0) { //0: error
      dlg.show();

      // Vuelta del dialogo
    }
    dlg = null;

    // Para ver s�lo los activos
    ResetearTabla();
    btnBuscarActionPerformed(false);

  } // Fin MostrarDialogo()

  private void MostrarCaso(int modo) { //alta, modif, borrar

    DatMntRegTubSC data = null;
    DatRT datosEntrada = null;

    if (modo != constantes.modoALTA) {
      data = (DatMntRegTubSC) listaRegTubs.elementAt(tabla.getSelectedIndex());
      datosEntrada = new DatRT();
      datosEntrada.put("CD_ARTBC", data.getCD_ANO());
      datosEntrada.put("CD_NRTBC", data.getCD_REG());
      datosEntrada.put("NM_EDO", data.getNM_EDO());

      //modificacion 09/06/2000
      // modificacion para poder visualizar los protocolos
      // de usuarios tanto de area como de distrito
      // de esta manera recuperamos los valores del nivel 1 y nivel 2
      datosEntrada.put("CD_NIVEL1", data.getCD_NIVEL_1_GE());
      datosEntrada.put("CD_NIVEL2", data.getCD_NIVEL_2_GE());

    }
    else { //alta (lo envio vacio, ya se encarga pSup de pintar a�o defecto)
      datosEntrada = new DatRT();
    }

    int modoOpe = 0;
    if ( (!hayPermiso(tabla.getSelectedIndex()) && modo != constantes.modoALTA) ||
        bConsulta) {
      modoOpe = constantes.modoCONSULTA;
    }
    else {
      modoOpe = modo;

      //cerrado, solo en modo consulta
    }
    if (modo != constantes.modoALTA &&
        data.getIT_CERRADO().trim().equals("S")) {
      modoOpe = constantes.modoCONSULTA;

    }
    DialogTub dlg = new DialogTub(this.app, catalogos,
                                  modoOpe, datosEntrada);

    dlg.show();

    if (dlg.iOut == -1) { //cancelar
      dlg = null;
      return;
    }
    else { //aceptar
      // Para ver s�lo los activos
      ResetearTabla();

      //alta ense�amos solo en insertado
      if (modoOpe == constantes.modoALTA) {
        /*DatRT dataalta = (DatRT) dlg.getComponente();
                 txtAno.setText(dataalta.getCD_ARTBC());
                 txtReg.setText(dataalta.getCD_NRTBC());
                 txtRegFocusLost();*/
        dlg = null;
        btnBuscarActionPerformed(true); //mata la lista etc
      }
      else {
        dlg = null;
        btnBuscarActionPerformed(true);
      }

    }

  } // Fin MostrarCaso()

  // Pasa a la pantalla de modificacion con el click de Cierre (off)
  void btnCerrarActionPerformed() {
    // Modo espera
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    // Creaci�n del dialogo de modificaci�n
    MostrarDialogoModificacion(modoCERRAR);

    // Se reestablece el modo
    modoOperacion = modo;
    Inicializar(modoOperacion);
  } // Fin btnCerrarActionPerformed()

  // Pasa a la pantalla de modficacion con el click de cierre (on) + resto de campos
  void btnDeshacerCierreActionPerformed() {
    // Modo espera
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar(modoOperacion);

    // Creaci�n del dialogo de modificaci�n
    MostrarDialogoModificacion(modoDESHACERCIERRE);

    // Se reestablece el modo
    modoOperacion = modo;
    Inicializar(modoOperacion);
  } // Fin btnDeshacerCierreActionPerformed()

  /*************** FUNCIONES AUXILIARES ***********************/

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  public void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  } // Fin ShowWarning()

  // Llama a CMessage para mostrar el mensaje de error sMessage
  public void ShowError(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgERROR, sMessage);
    msgBox.show();
    msgBox = null;
  } // Fin ShowError()

} // endclass PanMntRegTub

/* --------------- ESCUCHADORES ------------------- */

// Botones
class PanMntRegTubActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanMntRegTub adaptee;
  ActionEvent evt;

  PanMntRegTubActionAdapter(PanMntRegTub adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    if (evt.getActionCommand().equals("LupaEnfermo")) {
      adaptee.btnLupaEnfermoActionPerformed();
    }
    else if (evt.getActionCommand().equals("Buscar")) {
      adaptee.btnBuscarActionPerformed(true);
    }
    else if (evt.getActionCommand().equals("Primero")) {
      adaptee.btnPrimeroActionPerformed();
    }
    else if (evt.getActionCommand().equals("Anterior")) {
      adaptee.btnAnteriorActionPerformed();
    }
    else if (evt.getActionCommand().equals("Siguiente")) {
      adaptee.btnSiguienteActionPerformed();
    }
    else if (evt.getActionCommand().equals("Ultimo")) {
      adaptee.btnUltimoActionPerformed();
    }
    else if (evt.getActionCommand().equals("Cerrar")) {
      adaptee.btnCerrarActionPerformed();
    }
    else if (evt.getActionCommand().equals("Deshacer Cierre")) {
      adaptee.btnDeshacerCierreActionPerformed();
    }
    else if (evt.getActionCommand().equals("Modificar")) {
      adaptee.btnModificarActionPerformed();
    }
    else if (evt.getActionCommand().equals("Alta")) {
      adaptee.btnAltaActionPerformed();
    }
    else if (evt.getActionCommand().equals("Baja")) {
      adaptee.btnBajaActionPerformed();
    }
  }
} // Fin clase PanMntRegTubActionAdapter

// Perdidas de foco
class PanMntRegTubFocusAdapter
    implements java.awt.event.FocusListener, Runnable {
  PanMntRegTub adaptee;
  FocusEvent evt;

  PanMntRegTubFocusAdapter(PanMntRegTub adaptee) { // Constructor
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  } // Fin focusLost()

  public void focusGained(FocusEvent e) {
    if ( ( (TextField) e.getSource()).getName().equals("Ano")) {
      adaptee.txtAnoFocusGained();
    }
    else if ( ( (TextField) e.getSource()).getName().equals("Registro")) {
      adaptee.txtRegFocusGained();
    }
    else if ( ( (TextField) e.getSource()).getName().equals("CodEnfermo")) {
      adaptee.txtCodEnfermoFocusGained();
    }
    else if ( ( (TextField) e.getSource()).getName().equals("FechaDesde")) {
      adaptee.txtFechaDesdeFocusGained();
    }
    else if ( ( (TextField) e.getSource()).getName().equals("FechaHasta")) {
      adaptee.txtFechaHastaFocusGained();
    }
  }

  // Implementacion de run()
  public void run() {
    if ( ( (TextField) evt.getSource()).getName().equals("Ano")) {
      adaptee.txtAnoFocusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("Registro")) {
      adaptee.txtRegFocusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("CodEnfermo")) {
      adaptee.txtCodEnfermoFocusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("FechaDesde")) {
      adaptee.txtFechaDesdeFocusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("FechaHasta")) {
      adaptee.txtFechaHastaFocusLost();
    }
  } // Fin run()

} // Fin clase PanMntRegTubFocusAdapter

// Doble-click en un registro de la tabla
class PanMntRegTubTablaDobleClick
    implements jclass.bwt.JCActionListener, Runnable {
  PanMntRegTub adaptee;

  PanMntRegTubTablaDobleClick(PanMntRegTub adaptee) { // Constructor
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    new Thread(this).start();
  }

  public void run() {
    adaptee.TablaDobleClickActionPerformed();
  }

} // Fin clase PanMntRegTubTablaDobleClick

// Eventos (click o barra espaciadora) sobre la tabla
class PanMntRegTubTablaClick
    implements jclass.bwt.JCItemListener {
  PanMntRegTub adaptee;

  public PanMntRegTubTablaClick(PanMntRegTub panel) {
    adaptee = panel;
  }

  public void itemStateChanged(jclass.bwt.JCItemEvent e) {
    adaptee.tablaClickPerformed();
  }
} // Fin clase PanMntRegTubTablaClick
