/**
 * Clase: AppPanMntRegTub
 * Paquete: SP_Tuberculosis.cliente.mantregtub
 * Hereda: CApp
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 15/10/1999
 * Analisis Funcional: Punto 2. Mantenimiento Registro Tuberculosis.
 * Descripcion: Applet de prueba para visualizar el panel PanMntRegTub
 */

package tuberculosis.cliente.mantregtub;

import capp.CApp;
import comun.constantes;

//applet para  Mantenimiento de Registros de Tuberculosis
public class AppPanMntRegTub
    extends CApp {

  public void init() {
    super.init();
    setTitulo("Mantenimiento de Registros de Tuberculosis");
  }

  public void start() {
    int modo = constantes.modoMANTENIMIENTO;
    VerPanel("", new PanMntRegTub(this, modo));
  }
} // endclass AppPanMod1RegTub
