/**
 * Clase: PanSupCon
 * Paquete: tuberculosis.cliente.diaconcasotub
 * Hereda: CPanel
 * Autor: Pedro Antonio D�az
 * Fecha Inicio: 03/02/2000
 * Analisis Funcional:  Mantenimiento Contactos de Tuberulosis
 * Descripcion: Panel Superior del dialogo. Clave: Registro de contacto de Tuberculosis
 */

package tuberculosis.cliente.pansupcon;

import java.util.Hashtable;

import java.awt.Label;
import java.awt.TextField;

import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CPanel;
import comun.constantes;
import tuberculosis.cliente.diaconcasotub.DiaConCasoTub;

public class PanAux
    extends CPanel {

  // Modos de operaci�n del panel: paquete comun.constantes
  final int modoESPERA = constantes.modoESPERA;
  final int modoALTA = constantes.modoALTA;
  final int modoMODIFICACION = constantes.modoMODIFICACION;
  final int modoBAJA = constantes.modoBAJA;
  final int modoCONSULTA = constantes.modoCONSULTA;
  final int modoSOLODECLARANTES = constantes.modoSOLODECLARANTES;

  // Modo de entrada al panel
  protected int modoOperacion = constantes.modoALTA;

  // Dialog que contiene a este panel
  DiaConCasoTub dlg;

  // Hashtable con los datos
  Hashtable hs = null;

  //CApp
  public CApp a = null;

  /**************** Componentes del panel *********************/

  // Organizacion del panel
  XYLayout xYLayout = new XYLayout();

  private Label lblAnoReg = new Label("A�o/Registro:");
  public TextField txtAno = new TextField();
  private Label lblBarra = new Label();
  public TextField txtReg = new TextField();
  Label lblNombre = new Label();

  // Constructor
  public PanAux(DiaConCasoTub dlgPpal, int modo, Hashtable hsEntrada) {
    try {

      dlg = dlgPpal;
      a = dlg.getCApp();
      this.app = dlg.getCApp();
      jbInit();

      modoOperacion = modo;
      /*
             if (modoOperacion == constantes.modoALTA){
             }
       */
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    /*
      this.setSize(new Dimension(745, 80));
      xYLayout.setHeight(82);
      lblBarra.setText("/");
      xYLayout.setWidth(755);
      this.setLayout(xYLayout);
      //setBorde(false);
      lblNombre.setText("");
      // A�o/Registro del registro de tuberculosis. A�o actual por defecto
      this.add(lblAnoReg, new XYConstraints(13, 5, 82, -1));
      this.add(txtAno, new XYConstraints(121, 5, 40, -1));
      this.add(lblBarra, new XYConstraints(167, 5, 13, 25));
      this.add(txtReg, new XYConstraints(182, 5, 62, -1));
      this.add(lblNombre, new XYConstraints(254, 5, 100, -1));
      txtAno.setBackground(new Color(255, 255, 150));
      txtReg.setBackground(new Color(255, 255, 150));
     */
  }

  public void Inicializar(int modo) {
    //modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {
    /*
     txtAno.setEnabled(false);
     txtReg.setEnabled(false);
     */
  }

  /*
    //Recibe el a�o del panel princiapl
    public void setAno(String sAno){
      txtAno.setText(sAno);
    }
    //Recibe el registro del panel princiapl
    public void setReg(String sReg){
      txtReg.setText(sReg);
    }
    //Recibe el nombre del panel princiapl
    public void setNom(String sNom){
      lblNombre.setText(sNom);
    }
   */

  //-----------interface-------------

} // Fin clase PanSupCon
