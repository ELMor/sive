/**
 * Clase: PanTratMues
 * Paquete: tuberculosis.cliente.pantrat
 * Hereda: CPanel
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 19/11/1999
 * Descripcion: Implementacion del panel que ense�a una tabla con los
 *   tratamientos asociados a un notificador. Desde este panel
 *   se puede acceder al alta/modificacion/baja de tratamientos.
 */

package tuberculosis.cliente.pantrat;

import java.util.Hashtable;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CPanel;
import capp.CTabla;
import comun.Common;
import comun.Comunicador;
import comun.constantes;
import jclass.util.JCVector;
import sapp.StubSrvBD;
import tuberculosis.cliente.diaresul.DiaOtrosTratamientos;
//para dar altas/bajas/modif
import tuberculosis.cliente.diatrat.DiaTrat;
import tuberculosis.datos.pantrat.DatCasTratCS;
import tuberculosis.datos.pantrat.DatCasTratSC;

public class PanTrat
    extends CPanel {

  // Applet asociado a este panel
  CApp appTrats;

  // Hashtable de entrada
  Hashtable hsTrat = null;

  // Lista de los parametros de busqueda
  public CLista listaBusqueda = null;

  // Lista de Tratamientos asociados a un Notificador
  public CLista listaTrats = null;

  // Lista de motivos ed tratamiento
  public CLista LMotTrat = null;

  // Sincronizaci�n de eventos
  protected boolean sinBloquear = true;
  protected int modoAnterior = 0;

  // Variable estado del panel
  protected int modoOperacion = constantes.modoALTA;
  protected int modoEntrada = constantes.modoALTA; //guardamos como entro desde el dlg

//  final String strSERVLET_TRATS = "servlet/SrvCasTrat";
  final String strSERVLET_TRATS = constantes.strSERVLET_TRATS;

  // Cargadores de im�genes visibles en todo el m�dulo
  private CCargadorImagen imgs = null;

  final String imgNAME_mantenimiento[] = {
      constantes.imgALTA2,
      constantes.imgMODIFICAR2,
      constantes.imgBAJA2};

  final String imgNAME_tabla[] = {
      constantes.imgPRIMERO, constantes.imgANTERIOR,
      constantes.imgSIGUIENTE, constantes.imgULTIMO};

  /******************** Componentes **********************/

  // Organizacion del panel
  XYLayout lyXYLayout = new XYLayout();

  // Controles para la navegacion entre los elementos de la tabla
  ButtonControl btnAlta = null;
  ButtonControl btnModificar = null;
  ButtonControl btnBaja = null;
  ButtonControl btnPrimero = null;
  ButtonControl btnAnterior = null;
  ButtonControl btnSiguiente = null;
  ButtonControl btnUltimo = null;

  // Tabla que visualiza y permite navegar entre los tratamientos
  public CTabla tabla = new CTabla();

  public DiaOtrosTratamientos dlgOtrosT = null;

  // Constructor
  public PanTrat(CApp a,
                 int modo,
                 Hashtable hsEntrada,
                 DatCasTratCS dataBusq,
                 DiaOtrosTratamientos dlgOtros) {
    try {
      appTrats = a;
      setApp(appTrats);
      dlgOtrosT = dlgOtros;

      // Lista motivos de tratamiento
      hsTrat = hsEntrada;
      LMotTrat = (CLista) hsTrat.get(constantes.MOTIVOS_TRATAMIENTO);

      // Datos de busqueda
      listaBusqueda = new CLista();
      listaBusqueda.addElement(dataBusq);

      // Inicializacion
      modoOperacion = modo;
      modoEntrada = modo;
      jbInit();
      if (modoEntrada == constantes.modoALTA) {
        listaTrats = new CLista();
      }
      else {
        listaTrats = buscarDatos();

      }
      escribirTabla();

      Inicializar();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicializacion en la creacion de un objeto
  void jbInit() throws Exception {
    // Carga de imagenes
    CCargadorImagen imgs_mantenimiento = new CCargadorImagen(app,
        imgNAME_mantenimiento);
    imgs_mantenimiento.CargaImagenes();
    CCargadorImagen imgs_tabla = new CCargadorImagen(app, imgNAME_tabla);
    imgs_tabla.CargaImagenes();

    // Medidas del panel
    this.setSize(new Dimension(780, 315));
    lyXYLayout.setHeight(305);
    lyXYLayout.setWidth(780);
    this.setLayout(lyXYLayout);

    // Botones de mantenimiento de la tabla
    btnAlta = new ButtonControl(imgs_mantenimiento.getImage(0));
    btnModificar = new ButtonControl(imgs_mantenimiento.getImage(1));
    btnBaja = new ButtonControl(imgs_mantenimiento.getImage(2));
    this.add(btnAlta, new XYConstraints(20, 155, 25, 25));
    this.add(btnModificar, new XYConstraints(50, 155, 25, 25));
    this.add(btnBaja, new XYConstraints(80, 155, 25, 25));

    // Botones de navegacion por la tabla
    btnPrimero = new ButtonControl(imgs_tabla.getImage(0));
    btnAnterior = new ButtonControl(imgs_tabla.getImage(1));
    btnSiguiente = new ButtonControl(imgs_tabla.getImage(2));
    btnUltimo = new ButtonControl(imgs_tabla.getImage(3));
    this.add(btnPrimero, new XYConstraints(505 + 105, 155, 25, 25));
    this.add(btnAnterior, new XYConstraints(545 + 105, 155, 25, 25));
    this.add(btnSiguiente, new XYConstraints(583 + 105, 155, 25, 25));
    this.add(btnUltimo, new XYConstraints(621 + 105, 155, 25, 25));

    // Se establecen los parametros de la tabla
    pintarTabla();
    this.add(tabla, new XYConstraints(15, 15, 755, 120));

    // Escuchadores: Botones de navegacion
    btnTablaActionListener alBotonesTabla = new btnTablaActionListener(this);
    btnPrimero.setActionCommand("primero");
    btnPrimero.addActionListener(alBotonesTabla);
    btnAnterior.setActionCommand("anterior");
    btnAnterior.addActionListener(alBotonesTabla);
    btnSiguiente.setActionCommand("siguiente");
    btnSiguiente.addActionListener(alBotonesTabla);
    btnUltimo.setActionCommand("ultimo");
    btnUltimo.addActionListener(alBotonesTabla);

    // Escuchadores: Botones Mantenimiento
    btnAMBActionListener alBotonesAMB = new btnAMBActionListener(this);
    btnAlta.setActionCommand("alta");
    btnAlta.addActionListener(alBotonesAMB);
    btnModificar.setActionCommand("modificar");
    btnModificar.addActionListener(alBotonesAMB);
    btnBaja.setActionCommand("baja");
    btnBaja.addActionListener(alBotonesAMB);

    // Tabla de tratamientos
    jcalTablaActionListener jcalTabla = new jcalTablaActionListener(this);
    tabla.addActionListener(jcalTabla);

  }

  // Bloquear eventos
  /** Este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized boolean bloquea() {
    if (sinBloquear) {
      // No hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modoOperacion;
      modoOperacion = constantes.modoESPERA;
      dlgOtrosT.Inicializar(modoOperacion);

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // Ya est� bloqueado
      return false;
    }
  }

  /** Este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    modoOperacion = modoAnterior;
    dlgOtrosT.Inicializar(modoOperacion);
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // Para devolver los datos del Notificador actualizado
  public DatCasTratCS getComponente() {
    return (DatCasTratCS) listaBusqueda.elementAt(0);
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar()

  // Implementacion del metodo abstracto de CPanel
  //  modoALTA: habilitado solo el boton de alta
  //  modoMODIFICACION: habilitados los 3 botones
  //  modoBAJA: habilitado solo el boton de baja
  //  modoCONSULTA: habilitado solo el boton de modificacion
  public void Inicializar() {
    boolean state;
    switch (modoOperacion) {

      case constantes.modoESPERA:
        tabla.setEnabled(false);
        btnAlta.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBaja.setEnabled(false);
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case constantes.modoALTA:
        if (tabla.countItems() > 0) {
          state = true;
        }
        else {
          state = false;
        }
        tabla.setEnabled(state);
        btnAlta.setEnabled(true);
        btnModificar.setEnabled(false);
        btnBaja.setEnabled(false);
        btnPrimero.setEnabled(state);
        btnAnterior.setEnabled(state);
        btnSiguiente.setEnabled(state);
        btnUltimo.setEnabled(state);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case constantes.modoMODIFICACION:
        if (tabla.countItems() > 0) {
          state = true;
        }
        else {
          state = false;
        }
        tabla.setEnabled(state);
        btnAlta.setEnabled(true);
        btnModificar.setEnabled(state);
        btnBaja.setEnabled(state);
        btnPrimero.setEnabled(state);
        btnAnterior.setEnabled(state);
        btnSiguiente.setEnabled(state);
        btnUltimo.setEnabled(state);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case constantes.modoBAJA:
        if (tabla.countItems() > 0) {
          state = true;
        }
        else {
          state = false;
        }
        tabla.setEnabled(state);
        btnAlta.setEnabled(false);
        btnModificar.setEnabled(state); // E 28/01/2000
        btnBaja.setEnabled(false); // E 28/01/2000
        btnPrimero.setEnabled(state);
        btnAnterior.setEnabled(state);
        btnSiguiente.setEnabled(state);
        btnUltimo.setEnabled(state);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case constantes.modoCONSULTA:
        if (tabla.countItems() > 0) {
          state = true;
        }
        else {
          state = false;
        }
        tabla.setEnabled(state);
        btnAlta.setEnabled(false);
        btnModificar.setEnabled(state); // E 28/01/2000
        btnBaja.setEnabled(false);
        btnPrimero.setEnabled(state);
        btnAnterior.setEnabled(state);
        btnSiguiente.setEnabled(state);
        btnUltimo.setEnabled(state);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
  }

  // Establece el numero de columnas de la tabla y las cabeceras de las mismas
  public void pintarTabla() {
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "100\n268\n80\n287"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String("Fecha Reinicio\nMotivo Reinicio\nFecha Fin\nMotivo Fin"), '\n'));
    tabla.setNumColumns(4);
  } //fin PintarTabla()

  // A partir de los datos de un notificador DatCasTratCS, devuelve una lista
  // con los tratamientos asociados al notificador (lista de DatCasTratSC).
  public CLista buscarDatos() {
    CLista parametros = listaBusqueda;
    CLista listaSalida = null;
    StubSrvBD stubCliente = new StubSrvBD();

    try {
      listaSalida = Comunicador.Communicate(this.app,
                                            stubCliente,
                                            0,
                                            strSERVLET_TRATS,
                                            parametros);

      if (listaSalida != null) {
        if (listaSalida.size() > 0) {
          // Se devuelven la lista de tratamientos
          return listaSalida;
        }
        else {
          //comun.ShowWarning(this.getApp(),"No existen tratamientos asociados.");
          return listaSalida;
        }
      }
      else {
        Common.ShowWarning(this.getApp(),
            "Error al recuperar los tratamientos asociados al notificador.");
        return null;
      }
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      Common.ShowWarning(this.getApp(),
          "Error al recuperar los tratamientos asociados al notificador.");
      return null;
    }
  } // Fin buscarDatos()

  // Implementaci�n del m�todo de rellenado
  public void escribirTabla() {

    if (listaTrats == null) {
      resetearTabla();
      return;
    }
    if (listaTrats.size() <= 0) {
      resetearTabla();
      return;
    }

    // Fila
    JCVector row = null;
    // Matriz de filas
    JCVector items = new JCVector();

    // Reseteo de la tabla
    resetearTabla();

    // Datos de una fila de la tabla
    DatCasTratSC dataResult = null;

    for (int i = 0; i < listaTrats.size(); i++) {
      dataResult = (DatCasTratSC) listaTrats.elementAt(i);

      // Nuevo JCVector
      row = new JCVector();

      // Fecha Inicio
      row.addElement(dataResult.getFC_INITRAT().trim());

      // Motivo Inicio
      String cdMotIni = dataResult.getCD_MOTRATINI().trim();
      String dsMotIni = dataResult.getDS_MOTRATINI().trim();
      if (!cdMotIni.equals("")) {
        row.addElement(cdMotIni + " - " + dsMotIni);
      }
      else {
        row.addElement("");

        // Fecha Fin
      }
      row.addElement(dataResult.getFC_FINTRAT().trim());

      // Motivo Fin
      String cdMotFin = dataResult.getCD_MOTRATFIN().trim();
      String dsMotFin = dataResult.getDS_MOTRATFIN().trim();
      if (!cdMotFin.equals("")) {
        row.addElement(cdMotFin + " - " + dsMotFin);
      }
      else {
        row.addElement("");

        // Se a�ade una fila a la matriz
      }
      items.addElement(row);
    } // Fin for()

    // Se establecen los  items
    tabla.setItems(items);

    // Se selecciona el primer registro de la tabla
    tabla.select(0);
    tabla.setTopRow(0);

    // Se repinta la tabla
    tabla.repaint();
  }

  public void resetearTabla() {
    tabla.clear();
    tabla.repaint();
    btnModificar.setEnabled(false);
    btnPrimero.setEnabled(false);
    btnAnterior.setEnabled(false);
    btnSiguiente.setEnabled(false);
    btnUltimo.setEnabled(false);
  } // Fin resetearTabla()

  /********************* Manejadores ************************/

  // Manejador btnPrimero
  protected void btnTablaPrimero() {
    if (tabla.countItems() > 0) {
      tabla.select(0);
      tabla.setTopRow(0);
    }
  }

  // Manejador btnAnterior
  protected void btnTablaAnterior() {
    if (tabla.countItems() > 0) {
      int index = tabla.getSelectedIndex();
      if (index > 0) {
        index--;
        tabla.select(index);
        if (index - tabla.getTopRow() <= 0) {
          tabla.setTopRow(tabla.getTopRow() - 1);
        }
      }
      else {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    }
  }

  // Manejador btnSiguiente
  protected void btnTablaSiguiente() {
    if (tabla.countItems() > 0) {
      int ultimo = tabla.countItems() - 1;
      int index = tabla.getSelectedIndex();

      if (index >= 0 && index < ultimo) {
        index++;
        tabla.select(index);
        if (index - tabla.getTopRow() >= 4) {
          tabla.setTopRow(tabla.getTopRow() + 1);
        }
      }
      else {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    }
  }

  // Manejador btnUltimo
  protected void btnTablaUltimo() {
    int index = 0;
    int ultimo = tabla.countItems() - 1;
    if (ultimo >= 0) {
      index = ultimo;
      tabla.select(index);
      if (tabla.countItems() >= 5) {
        tabla.setTopRow(tabla.countItems() - 5);
      }
      else {
        tabla.setTopRow(0);
      }
    }
  }

  // Manejador btnAlta
  protected void btnAlta() {
    // Relleno de los datos de un notificador
    DatCasTratCS notif = (DatCasTratCS) listaBusqueda.firstElement();

    // Relleno de los datos de un tratamiento (modoALTA)
    DatCasTratSC trat = new DatCasTratSC("", "", "", "", "", "", "", "", "", "");

    // Dialogo para dar de alta un nuevo tratamiento
    DiaTrat dlg = new DiaTrat(this.getApp(), constantes.modoALTA, LMotTrat,
                              notif, trat);
    dlg.show();

    // Si ha habido exito en la insercion de un nuevo tratamiento
    //  se a�ade el mismo al final de la tabla de tratamientos, se
    //  pinta la tabla de tratamientos y se actualiza el operario/fultact
    //  de la notificacion
    if (dlg.accionOK) {
      listaTrats.addElement(dlg.tratNuevo);
      escribirTabla();
      ( (DatCasTratCS) listaBusqueda.firstElement()).setCD_OPE(dlg.tratNuevo.
          getCD_OPE());
      ( (DatCasTratCS) listaBusqueda.firstElement()).setFC_ULTACT(dlg.tratNuevo.
          getFC_ULTACT());
    }

    dlg = null;
    return;
  } // Fin btnAlta()

  // Manejador btnModificar
  protected void btnModificar() {
    // Relleno de los datos de un notificador
    DatCasTratCS notif = (DatCasTratCS) listaBusqueda.firstElement();

    // Relleno de los datos de un tratamiento (modoMODIFICACION)
    DatCasTratSC trat = (DatCasTratSC) listaTrats.elementAt(tabla.
        getSelectedIndex());

    // Dialogo para modificar/consultar un tratamiento
    DiaTrat dlg = null;
    if (modoEntrada == constantes.modoCONSULTA ||
        modoEntrada == constantes.modoBAJA) {
      dlg = new DiaTrat(this.getApp(), constantes.modoCONSULTA, LMotTrat, notif,
                        trat);
    }
    else {
      dlg = new DiaTrat(this.getApp(), constantes.modoMODIFICACION, LMotTrat,
                        notif, trat);

    }
    dlg.show();

    // Si ha habido exito en la modificacion de un tratamiento
    //  se sustituye el mismo en la tabla de tratamientos, se
    //  pinta la tabla de tratamientos y se actualiza el operario/fultact
    //  de la notificacion
    if (dlg.accionOK) {
      //DatCasTratSC ntrat = dlg.tratNuevo.clone();
      listaTrats.setElementAt(dlg.tratNuevo, tabla.getSelectedIndex());
      escribirTabla();
      ( (DatCasTratCS) listaBusqueda.firstElement()).setCD_OPE(dlg.tratNuevo.
          getCD_OPE());
      ( (DatCasTratCS) listaBusqueda.firstElement()).setFC_ULTACT(dlg.tratNuevo.
          getFC_ULTACT());
    }

    dlg = null;
    return;
  }

  // Manejador btnBaja
  protected void btnBaja() {
    // Relleno de los datos de un notificador
    DatCasTratCS notif = (DatCasTratCS) listaBusqueda.firstElement();

    // Relleno de los datos de un tratamiento (modoBAJA)
    DatCasTratSC trat = (DatCasTratSC) listaTrats.elementAt(tabla.
        getSelectedIndex());

    // Dialogo para dar de baja un tratamiento
    DiaTrat dlg = new DiaTrat(this.getApp(), constantes.modoBAJA, LMotTrat,
                              notif, trat);
    dlg.show();

    // Si ha habido exito en la eliminacion de un tratamiento
    //  se elimina el mismo de la tabla de tratamientos, se
    //  pinta la tabla de tratamientos y se actualiza el operario/fultact
    //  de la notificacion
    if (dlg.accionOK) {
      listaTrats.removeElementAt(tabla.getSelectedIndex());
      escribirTabla();
      ( (DatCasTratCS) listaBusqueda.firstElement()).setCD_OPE(dlg.tratNuevo.
          getCD_OPE());
      ( (DatCasTratCS) listaBusqueda.firstElement()).setFC_ULTACT(dlg.tratNuevo.
          getFC_ULTACT());
    }

    dlg = null;
    return;
  }

  // Acciones sobre la tabla
  protected void tablaActionPerformed(jclass.bwt.JCActionEvent e) {
    switch (modoEntrada) {
      case constantes.modoALTA:

        // btnAlta();
        break;
      case constantes.modoMODIFICACION:
        btnModificar();
        break;
      case constantes.modoBAJA:
        btnBaja();
        break;
      case constantes.modoCONSULTA:
        btnModificar();
        break;
    }
  } // Fin tablaActionPerformed

} // endclass PanTratMues

// Gesti�n de eventos sobre los botones de la tabla
class btnTablaActionListener
    implements java.awt.event.ActionListener {
  PanTrat adaptee;

  public btnTablaActionListener(PanTrat ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      if (e.getActionCommand() == "primero") {
        adaptee.btnTablaPrimero();
      }
      else if (e.getActionCommand() == "anterior") {
        adaptee.btnTablaAnterior();
      }
      else if (e.getActionCommand() == "siguiente") {
        adaptee.btnTablaSiguiente();
      }
      else {
        adaptee.btnTablaUltimo();
      }

      adaptee.desbloquea();
    }
  }
} // endclass btnTablaActionListener

// Gesti�n de eventos sobre los botones A�ADIR, MODIFICAR, BORRAR
class btnAMBActionListener
    implements java.awt.event.ActionListener {
  PanTrat adaptee;

  public btnAMBActionListener(PanTrat ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      if (e.getActionCommand() == "alta") {
        adaptee.btnAlta();
      }
      else if (e.getActionCommand() == "modificar") {
        adaptee.btnModificar();
      }
      else if (e.getActionCommand() == "baja") {
        adaptee.btnBaja();
      }
      adaptee.desbloquea();
    }
  }
} // endclass btnAMBActionListener

class jcalTablaActionListener
    implements jclass.bwt.JCActionListener {
  PanTrat adaptee;

  public jcalTablaActionListener(PanTrat ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(jclass.bwt.JCActionEvent e) {
    if (adaptee.bloquea()) {
      adaptee.tablaActionPerformed(e);
      adaptee.desbloquea();
    }
  }
} // endclass jcalTablaActionListener
