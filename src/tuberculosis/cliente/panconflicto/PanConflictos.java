/**
 * Clase: PanConflicto
 * Paquete: tuberculosis.cliente.panconflicto
 * Hereda: CPanel
 * Autor: Emilio Postigo Riancho
 * Fecha Inicio: 20/12/1999
 * Descripci�n: Implementaci�n del panel de resoluci�n de conflictos.
 */

package tuberculosis.cliente.panconflicto;

import java.util.Hashtable;
import java.util.Vector;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CPanel;
import capp.CTabla;
import comun.Common;
import comun.Comunicador;
import comun.Fechas;
import comun.IntContenedor;
import comun.constantes;
import jclass.util.JCVector;
import panniveles.panelNiveles;
import panniveles.usaPanelNiveles;
import sapp.StubSrvBD;
import tuberculosis.cliente.diaconflicto.DiaConflicto;
import tuberculosis.datos.panconflicto.ConDatConflicto;
import tuberculosis.datos.panconflicto.DatConflicto;

public class PanConflictos
    extends CPanel
    implements usaPanelNiveles, IntContenedor {

  // Para marcar l�mites en las dimensiones de la tabla
  private final int NUMERO_MAXIMO = 5;

  // Servlets llamados desde el panel
  private final String strSERVLET_CONFLICTOS = constantes.strSERVLET_CONFLICTOS;

  // Estados posibles del di�logo
  protected final int modoESPERA = constantes.modoESPERA;
  protected final int modoBUSCAR = constantes.modoBUSCAR;
  protected final int modoCONSULTA = constantes.modoCONSULTA;

  // Mensaje en caso de bloqueo
  private final String DATOS_BLOQUEADOS =
      "Los datos han sido modificados. �Desea sobrescribirlos?";

  // Variables utilizadas para gestionar los estados del di�logo
  protected int modoOperacion = 0;
  protected int modoAnterior = 0;

  // Para sincronizar eventos
  private boolean sinBloquear = true;

  // Conexi�n con servlets
  private StubSrvBD stubCliente = null;

  // Para controlar cambios de controles
  String sA�o = "";
  String sRegistro = "";
  String sCdNivel1 = "";
  String sCdNivel2 = "";

  // Para almacenar los valores iniciales (Modificaciones y Bajas)

  // Para devolver valores
  DatConflicto dcRetorno = null;

  // Hashtable encargada de almacenar los datos enviados por el contenedor
  private Hashtable hDatos = null;

  // Variables recuperadas del applet
  private String sA�oDefecto = null;
  private String sCD_NIVEL_1 = null;
  private String sDS_NIVEL_1 = null;
  private String sCD_NIVEL_2 = null;
  private String sDS_NIVEL_2 = null;

  // Datos usados de forma habitual

  // Almacena los objetos de tipo DatConflicto mostrados en
  // la tabla
  private Vector vConflictos = null;

  // Almacenan los �ltimos criterios de b�squeda empleados
  private String s_A�o_Criterio = "";
  private String s_Registro_Criterio = "";
  private String s_Cd_Area = "";
  private String s_Cd_Distrito = "";

  // Indica si el nivel 2 es obligatorio
  private boolean bObligatorioNivel2 = false;

  // Indica si el usuario puede resolver conflictos
  // private boolean bResolverConflictos = false;

  // Contenedores de im�genes
  // Para los botones de la tabla
  protected CCargadorImagen imgsTabla = null;

  // Para el resto de los botones
  protected CCargadorImagen imgsBotones = null;

  // Imagenes utilizadas
  final String imgNAME_tabla[] = {
      "images/primero.gif",
      "images/anterior.gif",
      "images/siguiente.gif",
      "images/ultimo.gif"};

  final String imgNAME_botones[] = {
      "images/declaracion2.gif",
      "images/refrescar.gif"};

  // Layout del panel
  XYLayout lyXYLayout = null;

  // A�o/Registro
  Label lblA�oRegistro = null;
  TextField txtA�o = null;
  Label lblBarra = null;
  TextField txtRegistro = null;

  // Zona de Gesti�n
  Label lblZona = null;

  // Panel con �rea, Distrito y descripci�n de �ste
  panelNiveles panAreaDistrito = null;

  // Bot�n buscar
  ButtonControl btnBuscar = null;

  // Tabla de conflictos
  CTabla tbConflictos = null;

  // Botones para navegar por la tabla
  ButtonControl btnPrimero = null;
  ButtonControl btnAnterior = null;
  ButtonControl btnSiguiente = null;
  ButtonControl btnUltimo = null;

  // Bot�n Resolver Conflicto
  ButtonControl btnResolver = null;

  // Objeto gestor de eventos en botones
  PanConflictos_ActionListener pcaaGestorEventos = new
      PanConflictos_ActionListener(this);

  // Objeto gestor de eventos en botones de la tabla
  btnTablaActionListener pcaaGestorEventosBotonesTabla = new
      btnTablaActionListener(this);

  // Objeto gestor de eventos Ganancia/P�rdida de foco en las cajas de texto
  PanConflictos_FocusAdapter pcfaGestorEventos = new PanConflictos_FocusAdapter(this);

  // Objeto gestor de eventos sobre la tabla
  jcalTablaActionListener talGestorEventos = new jcalTablaActionListener(this);

  // Constructor
  public PanConflictos(CApp a) {
    this.app = a;
    try {
      // Recuperacion de par�metros
      sA�oDefecto = a.getANYO_DEFECTO();
      sCD_NIVEL_1 = a.getCD_NIVEL1_DEFECTO();
      sDS_NIVEL_1 = a.getDS_NIVEL1_DEFECTO();
      sCD_NIVEL_2 = a.getCD_NIVEL2_DEFECTO();
      sDS_NIVEL_2 = a.getDS_NIVEL2_DEFECTO();
      //bResolverConflictos = a.getStrIT_FG_RESOLVER_CONFLICTOS().equals("S");
      // Inicializaci�n de los controles
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicializacion en la creacion de un objeto
  private void jbInit() throws Exception {

    // Dimensionado del di�logo
    this.setSize(660, 400);

    // Borde
    this.setBorde(false);

    // Parametros del di�logo
    lyXYLayout = new XYLayout();
    this.setLayout(lyXYLayout);
    lyXYLayout.setHeight(400);
    lyXYLayout.setWidth(660);
    this.setBackground(Color.lightGray);

    // Crea el objeto stub
    stubCliente = new StubSrvBD();

    // Carga de las im�genes
    imgsTabla = new CCargadorImagen(app, imgNAME_tabla);
    imgsTabla.CargaImagenes();

    imgsBotones = new CCargadorImagen(app, imgNAME_botones);
    imgsBotones.CargaImagenes();

    // A�o/Registro
    lblA�oRegistro = new Label("A�o/Registro:");
    this.add(lblA�oRegistro, new XYConstraints(13, 10, 80, -1));

    txtA�o = new TextField();
    this.add(txtA�o, new XYConstraints(93, 10, 40, -1));
    txtA�o.setBackground(new Color(255, 255, 150));

    // Para eventos
    txtA�o.setName("a�o");
    txtA�o.addFocusListener(pcfaGestorEventos);

    lblBarra = new Label("/");
    this.add(lblBarra, new XYConstraints(138, 10, 5, -1));

    txtRegistro = new TextField();
    this.add(txtRegistro, new XYConstraints(145, 10, 70, -1));

    // Para eventos
    txtRegistro.setName("registro");
    txtRegistro.addFocusListener(pcfaGestorEventos);

    // Zona de gesti�n
    lblZona = new Label("Zona de Gesti�n:");
    this.add(lblZona, new XYConstraints(13, 50, 100, -1));

    // Creaci�n del panel �rea de salud/Distrito/Descripci�n
    panAreaDistrito = new panelNiveles(this.app,
                                       this,
                                       panelNiveles.MODO_INICIO,
                                       constantes.TIPO_NIVEL_3,
                                       constantes.LINEAS_1,
                                       true);

    this.add(panAreaDistrito, new XYConstraints(9, 75, 520, -1));

    // Se establece si son obligatorios o no
    // los dos niveles posibles
    panAreaDistrito.setNivelObligatorio(1, true);

    switch (this.app.getPerfil()) {
      case 4:
      case 5:
        panAreaDistrito.setNivelObligatorio(2, true);
        bObligatorioNivel2 = true;
        break;
      case 1:
      case 2:
      case 3:
        break;
    }

    // Se inicializan los campos apropiados con los datos enviados
    // como par�metros del applet
    txtA�o.setText(sA�oDefecto);
    panAreaDistrito.setBuscaCDNivel1(sCD_NIVEL_1);
    panAreaDistrito.setDSNivel1(sDS_NIVEL_1);
    panAreaDistrito.setCDNivel2(sCD_NIVEL_2);
    panAreaDistrito.setDSNivel2(sDS_NIVEL_2);

    // Bot�n Buscar
    btnBuscar = new ButtonControl("Buscar");
    btnBuscar.setImage(imgsBotones.getImage(1));
    this.add(btnBuscar, new XYConstraints(580, 80, 70, -1));

    // Para eventos
    btnBuscar.setActionCommand("buscar");
    btnBuscar.addActionListener(pcaaGestorEventos);

    // Se establecen los par�metros de la tabla
    tbConflictos = new CTabla();
    pintaTabla();

    this.add(tbConflictos, new XYConstraints(13, 125, 634, 125));

    // Para eventos
    tbConflictos.addActionListener(talGestorEventos);

    // Bot�n Resolver Conflicto
    btnResolver = new ButtonControl("Resolver Conflicto",
                                    imgsBotones.getImage(0));

    this.add(btnResolver, new XYConstraints(13, 255, 130, -1));
    btnResolver.setActionCommand("resolver_conflictos");
    btnResolver.addActionListener(pcaaGestorEventos);

    // Botones para navegar por la tabla
    btnPrimero = new ButtonControl(imgsTabla.getImage(0));
    btnAnterior = new ButtonControl(imgsTabla.getImage(1));
    btnSiguiente = new ButtonControl(imgsTabla.getImage(2));
    btnUltimo = new ButtonControl(imgsTabla.getImage(3));

    btnPrimero.setActionCommand("primero");
    btnAnterior.setActionCommand("anterior");
    btnSiguiente.setActionCommand("siguiente");
    btnUltimo.setActionCommand("ultimo");

    this.add(btnPrimero, new XYConstraints(450 + 67, 255, 25, 25));
    this.add(btnAnterior, new XYConstraints(485 + 67, 255, 25, 25));
    this.add(btnSiguiente, new XYConstraints(520 + 67, 255, 25, 25));
    this.add(btnUltimo, new XYConstraints(555 + 67, 255, 25, 25));

    // Se establecen los modos de funcionamiento iniciales
    modoAnterior = modoBUSCAR;

    // Inicializaci�n
    ponerModo(modoBUSCAR);
  } // Fin jbinit()

  // Sistema de bloqueo de eventos
  /** Este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized boolean bloquea() {
    int modo = 0;
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;

      modo = ponerEnEspera();
      if (modo != modoESPERA) {
        modoAnterior = modo;
      }

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  /** Este m�todo desbloquea el sistema */
  protected synchronized void desbloquea() {
    sinBloquear = true;
    if (modoOperacion == modoESPERA) {
      modoOperacion = modoAnterior;
    }
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // Implementacion del metodo abstracto de la clase CPanel
  public void Inicializar() {
    switch (modoOperacion) {
      case modoESPERA:
        panAreaDistrito.setModoEspera();
        txtA�o.setEnabled(false);
        txtRegistro.setEnabled(false);
        panAreaDistrito.setEnabled(false);
        btnBuscar.setEnabled(false);
        tbConflictos.setEnabled(false);
        btnResolver.setEnabled(false);
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        break;
      case modoBUSCAR:
        panAreaDistrito.setModoNormal();
        txtA�o.setEnabled(true);
        txtRegistro.setEnabled(true);
        panAreaDistrito.setEnabled(true);
        btnBuscar.setEnabled(true);
        tbConflictos.setEnabled(true);
        btnResolver.setEnabled(true);
        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);
        break;
      case modoCONSULTA:

        break;
    }
    // Se repinta
    this.validate();
  }

  // M�todos PRIVADOS

  // Para 'limpiar' los datos del �ltimo criterio empleado
  private void inicializaCriterio() {
    s_A�o_Criterio = "";
    s_Registro_Criterio = "";
    s_Cd_Area = "";
    s_Cd_Distrito = "";
  }

  // Para saber si han cambiado los criterios de b�squeda
  private boolean criterioCambiado() {
    boolean bValor = false;

    bValor = !s_A�o_Criterio.equals(getA�o());
    if (!bValor) {
      bValor = !s_Registro_Criterio.equals(getRegistro());
      if (!bValor) {
        bValor = !s_Cd_Area.equals(getCdArea());
        if (!bValor) {
          bValor = !s_Cd_Distrito.equals(getCdDistrito());
        }
      }
    }

    if (bValor) {
      s_A�o_Criterio = getA�o();
      s_Registro_Criterio = getRegistro();
      s_Cd_Area = getCdArea();
      s_Cd_Distrito = getCdDistrito();
    }

    return bValor;
  }

  // Obtiene la pregunta asociada al item seleccionado
  private String getPregunta() {
    return elemento(tbConflictos.getSelectedIndex(), vConflictos).
        getDS_TEXTO_LA();
  }

  // Devuelve un objeto de tipo DatConflicto a partir de
  // su �ndice y el vector donde est� almacenado
  private DatConflicto elemento(int indice, Vector vector) {
    DatConflicto dc = null;
    if (indice >= 0 && indice <= vector.size()) {
      dc = (DatConflicto) vector.elementAt(indice);
    }
    return dc;
  }

  // Limpia la tabla de conflictos
  private void borraTabla() {
    tbConflictos.clear();
  }

  // Carga la tabla a partir del vector pasado como
  // par�metro
  private void cargaTablaConflictos(Vector v) {
    JCVector jcvTabla = new JCVector();
    JCVector jcvLinea = null;

    DatConflicto dc = null;

    tbConflictos.clear();

    for (int i = 0; i < v.size(); i++) {
      dc = (DatConflicto) v.elementAt(i);

      jcvLinea = new JCVector();

      jcvLinea.addElement(dc.getCD_ARTBC_RT() + "/" + dc.getCD_NRTBC_RT());
      jcvLinea.addElement(dc.getCD_NIVEL_1_GE_RT() + "-" +
                          dc.getCD_NIVEL_2_GE_RT());
      jcvLinea.addElement(Fechas.date2String(dc.getFC_INIRTBC_RT()));
      jcvLinea.addElement(dc.getDS_TEXTO_LA());

      jcvTabla.addElement(jcvLinea);
    }

    tbConflictos.setItems(jcvTabla);

    if (v.size() > 0) {
      tbConflictos.select(0);
    }
  }

  // Recupera el a�o seleccionado
  private String getA�o() {
    return txtA�o.getText();
  }

  // Recupera el n� de registro seleccionado
  private String getRegistro() {
    return txtRegistro.getText();
  }

  // Recupera el c�digo del �rea seleccionada
  private String getCdArea() {
    return panAreaDistrito.getCDNivel1();
  }

  // Recupera el c�digo del Distrito seleccionado
  private String getCdDistrito() {
    return panAreaDistrito.getCDNivel2();
  }

  // Informa de si el par�metro es num�rico
  private boolean isNum(String Numero) {
    try {
      Integer I = new Integer(Numero);
      int i = I.intValue();
      if (i < 0) {
        return false;
      }
    }
    catch (Exception e) {
      return false;
    }
    return true;
  }

  // Valida los datos
  private boolean validarDatos() {
    // Comprueba las p�rdidas de foco
    if (panAreaDistrito.getDSNivel1().equals("")) {
      Common.ShowWarning(this.app, "Los datos no son correctos");
      return false;
    }

    // Comprueba las p�rdidas de foco
    if (bObligatorioNivel2 && panAreaDistrito.getDSNivel2().equals("")) {
      Common.ShowWarning(this.app, "Los datos no son correctos");
      return false;
    }

    // Comprueba que el usuario est� autorizado para resolver
    // conflictos
    /*if (!bResolverConflictos) {
      comun.ShowWarning(this.app, "El usuario no est� autorizado para resolver conflictos");
      return false;
         }*/

    // Comprueba la longitud del campo a�o
    if (txtA�o.getText().trim().length() != 4) {
      Common.ShowWarning(this.app, "La longitud del a�o ha de ser de 4 d�gitos");
      return false;
    }

    // Comprueba que el campo a�o es, realmente, num�rico
    if (!isNum(txtA�o.getText().trim())) {
      Common.ShowWarning(this.app, "El a�o ha de ser una expresi�n num�rica");
      return false;
    }

    // Comprueba si se han introducido los datos obligatorios
    if (!bCargadosObligatorios()) {
      Common.ShowWarning(this.app, "Faltan campos obligatorios");
      return false;
    }

    return true;
  }

  // Permite saber si todos los campos obligatorios est�n cargados
  private boolean bCargadosObligatorios() {
    boolean bValor = false;

    bValor = !txtA�o.getText().trim().equals("") &&
        !panAreaDistrito.getCDNivel1().equals("");
    if (bObligatorioNivel2) {
      bValor = bValor && !panAreaDistrito.getCDNivel2().equals("");
    }

    return bValor;
  }

  // Establece el n�mero de columnas de la tabla y las cabeceras de las mismas
  private void pintaTabla() {
    tbConflictos.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new
        String("80\n70\n75\n390"), '\n'));
    tbConflictos.setColumnButtonsStrings(jclass.util.JCUtilConverter.
                                         toStringList(new String(
        "Registro\nZ. Gesti�n\nF. Entrada\nPregunta"), '\n'));
    tbConflictos.setNumColumns(4);
  } //fin pintaTabla()

  // Devuelve true si el objeto 'contenedor' contiene la cadena especificada
  private boolean bContiene(String contenedor, String contenido) {
    return contenedor.indexOf(contenido) != -1;
  }

  // Efect�a la llamada al servlet con el modo de operaci�n
  // y los datos necesarios
  private CLista hazAccion(String servlet, int modo, CLista cl) throws
      Exception {
    CLista clDatos;
    CLista clResultado;

    clDatos = cl;

    clResultado = Comunicador.Communicate(this.app,
                                          stubCliente,
                                          modo,
                                          servlet,
                                          clDatos);

    return clResultado;
  }

  // Este m�todo permite al componente llamador recuperar los
  // valores introducidos.
  public Object getComponente() {
    return dcRetorno;
  }

  // M�todos est�ndard

  // Permite al contenedor proporcionar datos al di�logo
  public void rellena(Hashtable h) {
    hDatos = h;
    if (hDatos != null) {
    }
  }

  // Gesti�n de eventos

  // Cajas de texto

  // P�rdida de foco
  protected void txtA�oFocusLost() {
    txtA�o.setText(txtA�o.getText().trim());
    if (!sA�o.equals(txtA�o.getText())) {
      borraTabla();
      sA�o = txtA�o.getText();
    }
  }

  protected void txtRegistroFocusLost() {
    txtRegistro.setText(txtRegistro.getText().trim());
    if (!sRegistro.equals(txtRegistro.getText())) {
      borraTabla();
      sRegistro = txtRegistro.getText();
    }
  }

  // Ganancia de foco
  protected void txtA�oFocusGained() {
    sA�o = txtA�o.getText().trim();
  }

  protected void txtRegistroFocusGained() {
    sRegistro = txtRegistro.getText().trim();
  }

  // Botones
  protected void btnResolverActionPerformed() {
    Comunicador comConflicto = null;
    CLista clResultado = null;
    CLista clLista = null;
    DiaConflicto dcConflicto = null;

    Hashtable hEnvio = null;

    DatConflicto dc = null;

    dc = elemento(tbConflictos.getSelectedIndex(), vConflictos);

    if (dc != null) { // Si se pudo seleccionar algo...

      clLista = new CLista();
      clLista.addElement(dc);

      try {
        clResultado = hazAccion(constantes.strSERVLET_RES_CONFLICTOS,
                                constantes.modoLEERDATOS, clLista);

        clResultado.trimToSize();

        dcConflicto = new DiaConflicto(this.app);

        dcConflicto.ponerModo(constantes.modoESPERA);

        // Se cargan los par�metros
        hEnvio = new Hashtable();

        hEnvio.put(ConDatConflicto.NOTIFICADORES_CONFLICTO, clResultado);
        hEnvio.put(ConDatConflicto.PREGUNTA_CONFLICTO, getPregunta());
        hEnvio.put(ConDatConflicto.DATOS_CONFLICTO, dc);

        // Se pasan
        dcConflicto.rellena(hEnvio);
        dcConflicto.show();

        // Se comprueba si se ha efectuado la acci�n
        if (dcConflicto.getComponente() != null) {
          // Se refresca el contenido de la tabla
          btnBuscarActionPerformed(false);
        }

      }
      catch (Exception e) {
        Common.ShowError(this.app, e.getMessage());
      }
      finally {

      }
    }

  }

  protected void btnBuscarActionPerformed(boolean bMensaje) {
    Comunicador comRelaciones = null;
    CLista clResultado = null;
    CLista clLista = null;
    DatConflicto dc = null;

    // Se resuelven p�rdidas de foco cr�ticas
    this.txtA�oFocusLost();
    this.txtRegistroFocusLost();

    // Si ha cambiado el criterio de b�squeda o
    // si es necesario refrescar

    //if (criterioCambiado() || !bMensaje) {
    //if (!bMensaje) {
    // Si los datos son correctos
    if (validarDatos()) {
      clLista = new CLista();
      dc = new DatConflicto();

      dc.setCD_ARTBC_RT(getA�o());
      dc.setCD_NRTBC_RT(getRegistro());
      dc.setCD_NIVEL_1_GE_RT(getCdArea());
      dc.setCD_NIVEL_2_GE_RT(getCdDistrito());

      clLista.addElement(dc);

      try {
        clResultado = hazAccion(constantes.strSERVLET_CONFLICTOS,
                                constantes.modoLEERDATOS, clLista);
        vConflictos = (Vector) clResultado;
        cargaTablaConflictos(vConflictos);
        if (vConflictos.size() == 0) {
          if (bMensaje) {
            Common.ShowWarning(this.app, "No se encontraron datos");
          }
        }

      }
      catch (Exception e) {
        Common.ShowError(this.app, e.getMessage());
        // Para poder repetir la consulta
        //inicializaCriterio();
      }
      finally {

      }
    }
    //}
  }

  // Acciones sobre la tabla
  protected void tablaActionPerformed(jclass.bwt.JCActionEvent e) {
    btnResolverActionPerformed();
  }

  // Botones de la tabla
  protected void btnTablaPrimero() {
    tbConflictos.select(0);
    tbConflictos.setTopRow(0);
  }

  protected void btnTablaAnterior() {
    int index = tbConflictos.getSelectedIndex();
    if (index > 0) {
      index--;
      tbConflictos.select(index);
      if (index - tbConflictos.getTopRow() <= 0) {
        tbConflictos.setTopRow(tbConflictos.getTopRow() - 1);
      }
    }
    else {
      tbConflictos.select(0);
      tbConflictos.setTopRow(0);
    }
  }

  protected void btnTablaSiguiente() {
    int ultimo = tbConflictos.countItems() - 1;
    int index = tbConflictos.getSelectedIndex();
    if (index < ultimo && index >= 0) {
      index++;
      tbConflictos.select(index);
      if (index - tbConflictos.getTopRow() >= NUMERO_MAXIMO - 1) {
        tbConflictos.setTopRow(tbConflictos.getTopRow() + 1);
      }
    }
    else {
      if (index != ultimo) {
        tbConflictos.select(0);
        tbConflictos.setTopRow(0);
      }
    }
  }

  protected void btnTablaUltimo() {
    int index = 0;
    int ultimo = tbConflictos.countItems() - 1;
    if (ultimo >= 0) {
      index = ultimo;
      tbConflictos.select(index);
      if (tbConflictos.countItems() >= NUMERO_MAXIMO) {
        tbConflictos.setTopRow(tbConflictos.countItems() - NUMERO_MAXIMO);
      }
      else {
        tbConflictos.setTopRow(0);
      }
    }
  }

  // Fin gesti�n de eventos

  // Exigido por la interfaz ...
  public void cambioNivelAntesInformado(int niv) {
    switch (niv) {
      case 1:
        borraTabla();
        break;
      case 2:
        borraTabla();
        break;
    }
  }

  public int ponerEnEspera() {
    int modo = modoOperacion;
    if (modoOperacion != modoESPERA) {
      //modoOperacion = modoESPERA;
      //Inicializar();
      ponerModo(modoESPERA);
    }
    //System.out.println("ponerEnEspera ");
    return modoBUSCAR; // Se devuelve el estado 'activo' del panel
  }

  public void ponerModo(int modo) {
    if (modoOperacion != modo) {
      modoOperacion = modo;
      Inicializar();
    }
    //System.out.println("ponerModo " + modo);
  }

  // Exigido por IntContenedor
  public CApp getMiCApp() {
    return this.app;
  }

} // endclass PanTuberNotificaciones

/******************** ESCUCHADORES DE EVENTOS *********************/

// Eventos sobre la tabla
class jcalTablaActionListener
    implements jclass.bwt.JCActionListener {
  PanConflictos adaptee;

  public jcalTablaActionListener(PanConflictos ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(jclass.bwt.JCActionEvent e) {
    if (adaptee.bloquea()) {
      adaptee.tablaActionPerformed(e);
      adaptee.desbloquea();
    }
  }

} // Fin eventos tabla

// Gesti�n de eventos sobre los botones de la tabla
class btnTablaActionListener
    implements java.awt.event.ActionListener {
  PanConflictos adaptee;

  public btnTablaActionListener(PanConflictos ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(ActionEvent e) {
    //adaptee.modoOperacion = adaptee.modoESPERA;
    //adaptee.Inicializar(adaptee.modoOperacion);

    if (e.getActionCommand() == "primero") {
      adaptee.btnTablaPrimero();
    }
    else if (e.getActionCommand() == "anterior") {
      adaptee.btnTablaAnterior();
    }
    else if (e.getActionCommand() == "siguiente") {
      adaptee.btnTablaSiguiente();
    }
    else {
      adaptee.btnTablaUltimo();
    }
    //adaptee.desbloquea();
  }

} // Fin eventos botones tabla

// Eventos sobre los botones
class PanConflictos_ActionListener
    implements java.awt.event.ActionListener {
  PanConflictos adaptee;

  PanConflictos_ActionListener(PanConflictos adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(java.awt.event.ActionEvent e) {
    if (adaptee.bloquea()) {
      if (e.getActionCommand().equals("resolver_conflictos")) {
        adaptee.btnResolverActionPerformed();
      }
      else if (e.getActionCommand().equals("buscar")) {
        adaptee.btnBuscarActionPerformed(true);
      }
      adaptee.desbloquea();
    }
  }
} // Fin PanConflictosActionAdapter

// P�rdida de foco en las cajas de texto
class PanConflictos_FocusAdapter
    extends java.awt.event.FocusAdapter {

  PanConflictos adaptee;
  FocusEvent e = null;
  String nombreTxt = null;

  public PanConflictos_FocusAdapter(PanConflictos adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    //adaptee.lanzaEvento(); // E
    //if (adaptee.bloquea()) {

    adaptee.setCursor(new Cursor(Cursor.WAIT_CURSOR));

    this.e = e;
    nombreTxt = ( (Component) e.getSource()).getName();
    if (nombreTxt.equals("a�o")) {
      adaptee.txtA�oFocusLost();
    }
    else if (nombreTxt.equals("registro")) {
      adaptee.txtRegistroFocusLost();
    }

    adaptee.Inicializar();
    adaptee.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    //}
  }

  public void focusGained(FocusEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      nombreTxt = ( (Component) e.getSource()).getName();
      if (nombreTxt.equals("a�o")) {
        adaptee.txtA�oFocusGained();
      }
      else if (nombreTxt.equals("registro")) {
        adaptee.txtRegistroFocusGained();
      }
      adaptee.desbloquea();
    }
  }
}
//********************* Koniec ********************************//
