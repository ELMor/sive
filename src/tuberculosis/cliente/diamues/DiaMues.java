/**
 * Clase: DiaMues
 * Paquete: tuberculosis.cliente.diamues.DiaMues
 * Hereda: CDialog
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 30/11/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: Implementacion del dialogo que permite dar de alta
 *   baja, modificar y consultar muestras de un notificador.
 */

package tuberculosis.cliente.diamues;

import java.util.Hashtable;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Choice;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CTabla;
import comun.Common;
import comun.Comunicador;
import comun.constantes;
import jclass.util.JCVector;
import sapp.StubSrvBD;
import tuberculosis.datos.panmues.DatCasMuesCS;
import tuberculosis.datos.panmues.DatCasMuesSC;
import tuberculosis.datos.panmues.DatCasResis;

public class DiaMues
    extends CDialog {

  // Mensaje en caso de bloqueo
  private final String DATOS_BLOQUEADOS =
      "Los datos han sido modificados. �Desea sobrescribirlos?";

  // Estado: indica si se ha llevado a cabo correctamente la operacion deseada
  public boolean accionOK = false;
  public DatCasMuesSC muesNueva = null;

  // Localizacion del servlet
  final String strSERVLET_DIAMUES = constantes.strSERVLET_DIA_MUES;

  // Variable estado del panel
  protected int modoOperacion = constantes.modoALTA;
  protected int modoEntrada = constantes.modoALTA;

  // Sincronizaci�n de eventos
  protected boolean sinBloquear = true;
  protected int modoAnterior;

  // Datos del Notificador
  DatCasMuesCS diaNotif = null;

  // Datos de la muestra a modificar
  DatCasMuesSC MuesInicial = null;
  CLista ListaResisInicial = null;

  // Resistencias de la muestra actual
  CLista listaResis = null;

  // Lista de Tecnicas de Laboratorio
  CLista LTLab = null;

  // Lista Muestras Laboratorio
  CLista LMuesLab = null;

  // Lista Valores Muestras
  CLista LVMues = null;

  // Lista Tipos de Micobacterias
  CLista LTMicob = null;

  // Lista Estudios de Resistencias
  CLista LEstRes = null;

  // Imagenes
  protected CCargadorImagen imgs = null;
  final String imgNAME[] = {
      constantes.imgACEPTAR,
      constantes.imgCANCELAR};

  final String imgNAME_mantenimiento[] = {
      constantes.imgALTA2,
      constantes.imgMODIFICAR2,
      constantes.imgBAJA2};

  final String imgNAME_tabla[] = {
      constantes.imgPRIMERO, constantes.imgANTERIOR,
      constantes.imgSIGUIENTE, constantes.imgULTIMO};

  /****************** Componentes del panel ********************/

  // Organizacion del panel
  private XYLayout xYLayout = new XYLayout();

  // Fecha Muestra
  private Label lblFMues = new Label("Fecha Muestra:");
  // 22-05-01 (ARS) Cambio poner barras autom�ticamente
//  private CFechaSimple cfsFMues = new CFechaSimple("N");
  private fechas.CFecha cfsFMues = new fechas.CFecha("N");

  // Muestra
  private Label lblMues = new Label("Muestra:");
  private Choice choMues = new Choice();

  // Tipo T�cnica
  private Label lblTTec = new Label("Tipo T�cnica:");
  private Choice choTTec = new Choice();

  // Valor Muestra
  private Label lblVMues = new Label("Valor Muestra:");
  private Choice choVMues = new Choice();

  // Tipo Micobacteria
  private Label lblTMico = new Label("Tipo Micobacteria:");
  private Choice choTMico = new Choice();

  // Sensible/Resistente
  private CheckboxGroup chkgResis = new CheckboxGroup();
  private Checkbox chkSensible = new Checkbox("Sensible", chkgResis, true);
  private Checkbox chkResistente = new Checkbox("Resistente", chkgResis, false);

  // Tabla de resistencias de la muestra
  private CTabla tabla = new CTabla();

  // Controles para la navegacion entre los elementos de la tabla
  private ButtonControl btnAlta = null;
  private ButtonControl btnModificar = null;
  private ButtonControl btnBaja = null;
  private ButtonControl btnPrimero = null;
  private ButtonControl btnAnterior = null;
  private ButtonControl btnSiguiente = null;
  private ButtonControl btnUltimo = null;

  // Botones de aceptar y cancelar
  private ButtonControl btnAceptar = new ButtonControl();
  private ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores
  DiaMuesActionAdapter actionAdapter = null;
  DiaMuesChkSensibleItemAdapter itemAdapter = null;
  DiaMuesFocusAdapter focusAdapter = null;
  jcalTablaActionListener jcalTabla = null;
  btnAMBActionListener alBotonesAMB = null;
  btnTablaActionListener alBotonesTabla = null;

  // Constructor
  // CApp sera luego el panel del que procede: PanMuesMues
  // @param hsCatalogos: hash con los 5 catalogos que utiliza este dialogo
  // @param notif: contiene los datos del Notificador al que pertenece la muestra
  // @param mues: lleva los datos de una muestra en caso de modificaci�n/baja/consulta
  // @param resistencias: resistencias de la muestra mues (CLista de DatCasResis)
  public DiaMues(CApp a, int modo, Hashtable hsCatalogos,
                 DatCasMuesCS notif, DatCasMuesSC mues,
                 CLista resistencias) {
    super(a);
    try {

      // Variables globales
      LTLab = (CLista) hsCatalogos.get("TIPO_TECNICALAB");
      LMuesLab = (CLista) hsCatalogos.get("MUESTRA_LAB");
      LVMues = (CLista) hsCatalogos.get("VALOR_MUESTRA");
      LTMicob = (CLista) hsCatalogos.get("TMICOBACTERIA");
      LEstRes = (CLista) hsCatalogos.get("ESTUDIORESIS");
      diaNotif = notif;
      MuesInicial = mues;
      listaResis = resistencias;
      ListaResisInicial = (CLista) listaResis.clone();

      // Inicializacion
      modoOperacion = modo;
      modoEntrada = modo;
      jbInit();

      if (modoEntrada == constantes.modoMODIFICACION ||
          modoEntrada == constantes.modoBAJA ||
          modoEntrada == constantes.modoCONSULTA) {
        rellenarDatos(MuesInicial);

      }
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    // Organizacion del panel
    this.setSize(new Dimension(600, 350));
    xYLayout.setHeight(350);
    xYLayout.setWidth(600);
    this.setLayout(xYLayout);
    this.setTitle("Resultados de Laboratorio");

    // Carga de las im�genes
    imgs = new CCargadorImagen(getCApp(), imgNAME);
    imgs.CargaImagenes();
    CCargadorImagen imgs_mantenimiento = new CCargadorImagen(app,
        imgNAME_mantenimiento);
    imgs_mantenimiento.CargaImagenes();
    CCargadorImagen imgs_tabla = new CCargadorImagen(app, imgNAME_tabla);
    imgs_tabla.CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaMuesActionAdapter(this);
    focusAdapter = new DiaMuesFocusAdapter(this);
    itemAdapter = new DiaMuesChkSensibleItemAdapter(this);
    jcalTabla = new jcalTablaActionListener(this);
    alBotonesAMB = new btnAMBActionListener(this);
    alBotonesTabla = new btnTablaActionListener(this);

    // Fecha Muestra
    this.add(lblFMues, new XYConstraints(15, 15, 85, 25));
    this.add(cfsFMues, new XYConstraints(15 + 100 + 10, 15, 75, 25));

    // modificaci�n realizada el 08/02/2001
    //cfsFMues.setBackground(constantes.CampoTextoObligatorio);

    /* if(modoEntrada == constantes.modoALTA){
      SimpleDateFormat formUltact = new SimpleDateFormat("dd/MM/yyyy",new Locale("es", "ES"));
      cfsFMues.setText(formUltact.format(new java.util.Date()));
         } */

    cfsFMues.setName("cfsFMues");
    cfsFMues.addFocusListener(focusAdapter);

    // Muestra
    this.add(lblMues, new XYConstraints(15, 15 + 30, 50, 25));
    Common.writeChoiceDataGeneralCDDS(choMues, LMuesLab, true, false, true);
    this.add(choMues, new XYConstraints(15 + 100 + 10, 15 + 30, 180, 25));

    choMues.setBackground(constantes.CampoObligatorio);

    // Tipo T�cnica
    this.add(lblTTec, new XYConstraints(15, 15 + 30 + 30, 100, 25));
    Common.writeChoiceDataGeneralCDDS(choTTec, LTLab, true, false, true);
    this.add(choTTec, new XYConstraints(15 + 100 + 10, 15 + 30 + 30, 180, 25));

    choTTec.setBackground(constantes.CampoObligatorio);

    // Valor Muestra
    this.add(lblVMues,
             new XYConstraints(15 + 100 + 10 + 180 + 15, 15 + 30 + 30, 80, 25));
    Common.writeChoiceDataGeneralCDDS(choVMues, LVMues, true, false, true);
    this.add(choVMues,
             new XYConstraints(15 + 100 + 10 + 180 + 15 + 80 + 5, 15 + 30 + 30,
                               180, 25));

    choVMues.setBackground(constantes.CampoObligatorio);

    // Tipo Micobacteria
    this.add(lblTMico, new XYConstraints(15, 15 + 30 + 30 + 30, 110, 25));
    Common.writeChoiceDataGeneralCDDS(choTMico, LTMicob, true, false, true);
    this.add(choTMico,
             new XYConstraints(15 + 100 + 10, 15 + 30 + 30 + 30, 180, 25));

    choTMico.setBackground(constantes.CampoObligatorio);

    // Sensible/Resistente
    this.add(chkSensible, new XYConstraints(335, 180, 90, 25));
    this.add(chkResistente, new XYConstraints(335, 215, 90, 25));
    chkSensible.addItemListener(itemAdapter);
    chkResistente.addItemListener(itemAdapter);

    // Se establecen los parametros de la tabla de resistencias
    pintarTabla();
    this.add(tabla, new XYConstraints(15, 150, 290, 120));
    tabla.addActionListener(jcalTabla);

    // Botones de mantenimiento de la tabla de resistencias
    btnAlta = new ButtonControl(imgs_mantenimiento.getImage(0));
    btnModificar = new ButtonControl(imgs_mantenimiento.getImage(1));
    btnBaja = new ButtonControl(imgs_mantenimiento.getImage(2));
    this.add(btnAlta, new XYConstraints(15, 275, 25, 25));
    this.add(btnModificar, new XYConstraints(45, 275, 25, 25));
    this.add(btnBaja, new XYConstraints(75, 275, 25, 25));
    // Escuchadores: Botones Mantenimiento
    btnAlta.setActionCommand("alta");
    btnAlta.addActionListener(alBotonesAMB);
    btnModificar.setActionCommand("modificar");
    btnModificar.addActionListener(alBotonesAMB);
    btnBaja.setActionCommand("baja");
    btnBaja.addActionListener(alBotonesAMB);

    // Botones de navegacion por la tabla de resistencias
    btnPrimero = new ButtonControl(imgs_tabla.getImage(0));
    btnAnterior = new ButtonControl(imgs_tabla.getImage(1));
    btnSiguiente = new ButtonControl(imgs_tabla.getImage(2));
    btnUltimo = new ButtonControl(imgs_tabla.getImage(3));
    this.add(btnPrimero, new XYConstraints(190, 275, 25, 25));
    this.add(btnAnterior, new XYConstraints(220, 275, 25, 25));
    this.add(btnSiguiente, new XYConstraints(250, 275, 25, 25));
    this.add(btnUltimo, new XYConstraints(280, 275, 25, 25));
    // Escuchadores: Botones de navegacion
    btnPrimero.setActionCommand("primero");
    btnPrimero.addActionListener(alBotonesTabla);
    btnAnterior.setActionCommand("anterior");
    btnAnterior.addActionListener(alBotonesTabla);
    btnSiguiente.setActionCommand("siguiente");
    btnSiguiente.addActionListener(alBotonesTabla);
    btnUltimo.setActionCommand("ultimo");
    btnUltimo.addActionListener(alBotonesTabla);

    // Boton de Aceptar
    btnAceptar.setImage(imgs.getImage(0));
    btnAceptar.setLabel("Aceptar");
    this.add(btnAceptar, new XYConstraints(400, 275, 80, -1));
    if (modoOperacion == constantes.modoCONSULTA) {
      btnAceptar.setEnabled(false);
    }
    else {
      btnAceptar.setActionCommand("Aceptar");
      btnAceptar.addActionListener(actionAdapter);
    }

    // Boton de Cancelar
    btnCancelar.setImage(imgs.getImage(1));
    btnCancelar.setLabel("Cancelar");
    this.add(btnCancelar, new XYConstraints(500, 275, 80, -1));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);
  } // Fin jbInit()

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {
    switch (modoOperacion) {
      case constantes.modoESPERA:
        enableControls(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case constantes.modoALTA:

        // Si es resistente se hace visible la tabla de resistencias
        if (chkResistente.getState()) {
          visibleTablaResistencias(true);
        }
        else {
          visibleTablaResistencias(false);

          // Se habilitan los controles
        }
        enableControls(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case constantes.modoMODIFICACION:

        // Si es resistente se hace visible la tabla de resistencias
        if (chkResistente.getState()) {
          visibleTablaResistencias(true);
        }
        else {
          visibleTablaResistencias(false);

          // Se habilitan los controles
        }
        enableControls(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case constantes.modoCONSULTA:
      case constantes.modoBAJA:

        // Si es resistente se hace visible la tabla de resistencias
        if (chkResistente.getState()) {
          visibleTablaResistencias(true);
        }
        else {
          visibleTablaResistencias(false);

          // Se habilitan los controles
        }
        enableControls(false);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

    }
    this.doLayout();
  } // Fin Inicializar()

  private void enableControls(boolean en) {

    // Fecha Muestra
    cfsFMues.setEnabled(en);

    // Muestra
    choMues.setEnabled(en);

    // Tipo Tecnica
    choTTec.setEnabled(en);

    // Valor Muestra
    choVMues.setEnabled(en);

    // Tipo Micobacteria
    choTMico.setEnabled(en);

    // Sensible/resistente
    chkSensible.setEnabled(en);
    chkResistente.setEnabled(en);

    int tablaSize = tabla.countItems();
    switch (modoEntrada) {
      case constantes.modoALTA:
      case constantes.modoMODIFICACION:
        if (tablaSize != 0) {
          tabla.setEnabled(en);
          // Botonera navegacion
          btnPrimero.setEnabled(en);
          btnAnterior.setEnabled(en);
          btnSiguiente.setEnabled(en);
          btnUltimo.setEnabled(en);
          // Botonera modificacion
          btnAlta.setEnabled(en);
          btnModificar.setEnabled(en);
          btnBaja.setEnabled(en);
        }
        else {
          tabla.setEnabled(false);
          // Botonera navegacion
          btnPrimero.setEnabled(false);
          btnAnterior.setEnabled(false);
          btnSiguiente.setEnabled(false);
          btnUltimo.setEnabled(false);
          // Botonera modificacion
          btnAlta.setEnabled(en);
          btnModificar.setEnabled(false);
          btnBaja.setEnabled(false);
        }

        // Botones de Aceptar/Cancelar
        btnAceptar.setEnabled(en);
        btnCancelar.setEnabled(en);
        break;
      case constantes.modoBAJA:
      case constantes.modoCONSULTA:

        // Botonera modificacion
        btnAlta.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBaja.setEnabled(false);
        if (tablaSize != 0) {
          tabla.setEnabled(true);
          // Botonera navegacion
          btnPrimero.setEnabled(true);
          btnAnterior.setEnabled(true);
          btnSiguiente.setEnabled(true);
          btnUltimo.setEnabled(true);
        }
        else {
          tabla.setEnabled(false);
          // Botonera navegacion
          btnPrimero.setEnabled(false);
          btnAnterior.setEnabled(false);
          btnSiguiente.setEnabled(false);
          btnUltimo.setEnabled(false);
        }

        // Botones de Aceptar/Cancelar
        btnAceptar.setEnabled(true);
        if (modoEntrada == constantes.modoCONSULTA) {
          btnAceptar.setEnabled(false);
        }
        btnCancelar.setEnabled(true);
        break;
    }
  } // Fin enableControls()

  private boolean datosCompletos() {
    boolean bValor = true;

    String sFMues = cfsFMues.getText();
    String sDsTLab = Common.getChoiceDSDataGeneralCDDS(choTTec, LTLab, true, false, true);
    String sDsMues = Common.getChoiceDSDataGeneralCDDS(choMues, LMuesLab, true, false, true);
    String sDsVMues = Common.getChoiceDSDataGeneralCDDS(choVMues, LVMues, true, false, true);
    String sDsTMico = Common.getChoiceDSDataGeneralCDDS(choTMico, LTMicob, true, false, true);

    // modificaci�n 08/02/2001
    //if (sFMues.equals("") || sDsTLab.equals("") || sDsMues.equals("")
    if (sDsTLab.equals("") || sDsMues.equals("")
        || sDsVMues.equals("") || sDsTMico.equals("")) {
      Common.ShowWarning(this.app, "Faltan campos obligatorios");
      bValor = false;
    }

    return bValor;
  }

  public DatCasMuesSC recogerDatos() {
    DatCasMuesSC resul = null;

    String sNumMues = MuesInicial.getNM_RESLAB();
    String sFMues = cfsFMues.getText();
    String sTLab = Common.getChoiceCDDataGeneralCDDS(choTTec, LTLab, true, false, true);
    String sDsTLab = Common.getChoiceDSDataGeneralCDDS(choTTec, LTLab, true, false, true);
    String sMues = Common.getChoiceCDDataGeneralCDDS(choMues, LMuesLab, true, false, true);
    String sDsMues = Common.getChoiceDSDataGeneralCDDS(choMues, LMuesLab, true, false, true);
    String sVMues = Common.getChoiceCDDataGeneralCDDS(choVMues, LVMues, true, false, true);
    String sDsVMues = Common.getChoiceDSDataGeneralCDDS(choVMues, LVMues, true, false, true);
    String sTMico = Common.getChoiceCDDataGeneralCDDS(choTMico, LTMicob, true, false, true);
    String sDsTMico = Common.getChoiceDSDataGeneralCDDS(choTMico, LTMicob, true, false, true);
    String FlResis;
    if (chkResistente.getState()) {
      FlResis = "S";
    }
    else {
      FlResis = "N";
    }
    String sOpe = this.getCApp().getLogin(); // Operario que realiza el alta

    resul = new DatCasMuesSC(sNumMues, sFMues, sTLab, sDsTLab, sMues, sDsMues,
                             sVMues, sDsVMues, sTMico, sDsTMico, FlResis, sOpe,
                             "");
    return resul;
  } // Fin recogerDatos()

  public boolean validarDatos() {
    // 1. PERDIDAS DE FOCO: No hay campos CD-Button-DS

    // 2. LONGITUDES: No hay

    // 3. DATOS NUMERICOS: No hay

    // 4. CAMPOS OBLIGATORIOS: se recogen en datosCompletos

    return datosCompletos();
  } // Fin validarDatos()

  public void rellenarDatos(DatCasMuesSC data) {
    // Fecha Muestra
    cfsFMues.setText(data.getFC_MUESTRA());
    cfsFMues.ValidarFecha();
    if (cfsFMues.getValid().equals("N")) {
      cfsFMues.setText("");

      // Muestra Laboratorio
    }
    Common.selectChoiceElement(choMues, LMuesLab, true, data.getCD_MUESTRA());

    // Tecnica Laboratorio
    Common.selectChoiceElement(choTTec, LTLab, true, data.getCD_TTECLAB());

    // Valor Muestra
    Common.selectChoiceElement(choVMues, LVMues, true, data.getCD_VMUESTRA());

    // Tipo Micobacteria
    Common.selectChoiceElement(choTMico, LTMicob, true, data.getCD_TMICOB());

    // Sensible/Resistente
    if (data.getIT_RESISTENTE().equals("S")) {
      chkgResis.setSelectedCheckbox(chkResistente);
    }
    else {
      chkgResis.setSelectedCheckbox(chkSensible);

      // Si es resistente se rellena la lista de resistencias
    }
    if (chkResistente.getState()) {
      escribirTablaResistencias();
    }
  } // Fin rellenarDatos()

  // Informa de si una cierta clave de resistencia ya est� incluida
  private boolean estaEnListaResis(DatCasResis dcr) {
    boolean bValor = false;

    for (int i = 0; i < listaResis.size() && !bValor; i++) {
      bValor = ( (DatCasResis) listaResis.elementAt(i)).getCD_ESTREST().equals(
          dcr.getCD_ESTREST());
    }

    return bValor;
  }

  // Compara si los valores de 2 DatCasMuesSC son iguales (true)
  private boolean compararCampos(DatCasMuesSC a, DatCasMuesSC b, CLista al,
                                 CLista bl) {
    if (a == null || b == null) {
      return false;
    }

    if (!a.getFC_MUESTRA().equals(b.getFC_MUESTRA())) {
      return false;
    }

    if (!a.getCD_TTECLAB().equals(b.getCD_TTECLAB())) {
      return false;
    }

    if (!a.getCD_MUESTRA().equals(b.getCD_MUESTRA())) {
      return false;
    }

    if (!a.getCD_VMUESTRA().equals(b.getCD_VMUESTRA())) {
      return false;
    }

    if (!a.getCD_TMICOB().equals(b.getCD_TMICOB())) {
      return false;
    }

    if (!a.getIT_RESISTENTE().equals(b.getIT_RESISTENTE())) {
      return false;
    }
    else if (a.getIT_RESISTENTE().equals("S")) { // Comparacion de listas
      if (al == null) {
        if (bl == null) {
          return true;
        }
        else if (bl.size() == 0) {
          return true;
        }
        else {
          return false;
        }
      }
      else if (bl == null) {
        if (al == null) {
          return true;
        }
        else if (al.size() == 0) {
          return true;
        }
        else {
          return false;
        }
      }
      else // al !=null && bl != null
      if (al.size() != bl.size()) {
        return false;
      }
      for (int i = 0; i < al.size(); i++) {
        if (! ( (DatCasResis) al.elementAt(i)).getCD_ESTREST().equals( ( (
            DatCasResis) bl.elementAt(i)).getCD_ESTREST())) {
          return false;
        }
      }
    }

    return true;
  } // Fin  compararCampos()

  // Establece el numero de columnas de la tabla y las cabeceras de las mismas
  public void pintarTabla() {
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "80\n190"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String("Resistencia\nDescripcion"), '\n'));
    tabla.setNumColumns(2);
  } // Fin pintarTabla()

  // Rellena la tabla con los datos de la lista de resistencias
  public void escribirTablaResistencias() {

    if (listaResis == null) {
      resetearTablaResistencias();
      return;
    }
    if (listaResis.size() <= 0) {
      resetearTablaResistencias();
      return;
    }

    // Fila
    JCVector row = null;
    // Matriz de filas
    JCVector items = new JCVector();

    // Reseteo de la tabla
    resetearTablaResistencias();

    // Datos de una fila de la tabla
    DatCasResis dataResult = null;

    for (int i = 0; i < listaResis.size(); i++) {
      dataResult = (DatCasResis) listaResis.elementAt(i);

      // Nuevo JCVector
      row = new JCVector();

      // Codigo Resistencia
      row.addElement(dataResult.getCD_ESTREST().trim());

      // Descripcion Resistencia
      row.addElement(dataResult.getDS_ESTREST().trim());

      // Se a�ade una fila a la matriz
      items.addElement(row);
    } // Fin for()

    // Se establecen los  items
    tabla.setItems(items);

    // Se selecciona el primer registro de la tabla
    tabla.select(0);
    tabla.setTopRow(0);

    // Se repinta la tabla
    tabla.repaint();
  }

  public void resetearTablaResistencias() {
    tabla.clear();
    tabla.repaint();
    btnModificar.setEnabled(false);
    btnPrimero.setEnabled(false);
    btnAnterior.setEnabled(false);
    btnSiguiente.setEnabled(false);
    btnUltimo.setEnabled(false);
  } // Fin resetearTablaResistencias()

  public void visibleTablaResistencias(boolean state) {
    tabla.setVisible(state);
    btnAlta.setVisible(state);
    btnModificar.setVisible(state);
    btnBaja.setVisible(state);
    btnPrimero.setVisible(state);
    btnAnterior.setVisible(state);
    btnSiguiente.setVisible(state);
    btnUltimo.setVisible(state);
  } // Fin habilitarTablaResistencias

  // Bloquear eventos
  /** Este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized boolean bloquea() {
    if (sinBloquear) {
      // No hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modoOperacion;
      modoOperacion = constantes.modoESPERA;
      Inicializar();

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // Ya est� bloqueado
      return false;
    }
  } // Fin bloquea()

  /** Este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    modoOperacion = modoAnterior;
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  } // Fin desbloquea()

  // Realiza la comunicaci�n con el servlet strSERVLET_DIAMUES
  CLista hazAccion(StubSrvBD stubCliente,
                   int modo,
                   CLista parametros) throws Exception {
    CLista result = null;

    // Invocacion del servlet para que haga el insert
    result = Comunicador.Communicate(this.getCApp(),
                                     stubCliente,
                                     modo,
                                     strSERVLET_DIAMUES,
                                     parametros);

    /*
        // SOLO DESARROLLO
        SrvDiaMues srv = new SrvDiaMues();
        result = srv.doPrueba(modo,parametros);
     */

    return result;
  } // Fin hazAccion()

  /******************** Manejadores *************************/

  void btnAceptarActionPerformed() {

    DatCasMuesSC MuesActual = null;
    CLista parametros = new CLista();
    CLista listaSalida = new CLista();
    StubSrvBD stubCliente = new StubSrvBD();
    CLista newMues = new CLista();

    switch (modoEntrada) {
      case constantes.modoALTA:
        if (validarDatos()) {
          // Recogida de datos en un DatCasTratSC
          MuesActual = recogerDatos();

          // Establecimiento de los parametros
          parametros.addElement(diaNotif);
          parametros.addElement(MuesActual);
          if (MuesActual.getIT_RESISTENTE().equals("S")) {
            parametros.addElement(listaResis);
          }
          else {
            parametros.addElement(null);

          }
          try {
            // Invocacion del servlet para que haga el insert
            newMues = hazAccion(stubCliente, constantes.modoALTA, parametros);
            accionOK = true;
          }
          catch (Exception e) {
            if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
              if (Common.ShowPregunta(getCApp(), DATOS_BLOQUEADOS)) {
                try {
                  newMues = hazAccion(stubCliente, constantes.modoALTA_SB,
                                      parametros);
                  accionOK = true;
                }
                catch (Exception ex) {
                  ex.printStackTrace();
                  Common.ShowWarning(this.getCApp(),
                                     "Error al dar de alta el resultado.");
                  accionOK = false;
                }
              }
              else {
                accionOK = false;
              }
            }
            else {
              e.printStackTrace();
              Common.ShowWarning(this.getCApp(),
                                 "Error al dar de alta el resultado.");
              accionOK = false;
            }
          }

          if (accionOK) {
            // Establecemos en la muestra actual el numero de mues que se acaba de insertar
            MuesActual = (DatCasMuesSC) newMues.firstElement();
            muesNueva = MuesActual;
            // Cerramos el dialogo
            this.dispose();
          }
        } // validarDatos() ya muestra mensajes de error
        break;

      case constantes.modoMODIFICACION:
        if (validarDatos()) {
          // Recogida de datos en un DatCasMuesSC
          MuesActual = recogerDatos();

          if (compararCampos(MuesActual, MuesInicial, listaResis,
                             ListaResisInicial)) {
            ;
          }
          else {
            // Establecimiento de los parametros
            parametros.addElement(diaNotif);
            parametros.addElement(MuesActual);
            if (MuesActual.getIT_RESISTENTE().equals("S")) {
              parametros.addElement(listaResis);
            }
            else {
              parametros.addElement(null);

            }
            try {
              newMues = hazAccion(stubCliente, constantes.modoMODIFICACION,
                                  parametros);
              accionOK = true;
            }
            catch (Exception e) {

              if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
                if (Common.ShowPregunta(getCApp(), DATOS_BLOQUEADOS)) {
                  try {
                    newMues = hazAccion(stubCliente,
                                        constantes.modoMODIFICACION_SB,
                                        parametros);
                    accionOK = true;
                  }
                  catch (Exception ex) {
                    ex.printStackTrace();
                    Common.ShowWarning(this.getCApp(),
                                       "Error al modificar el resultado.");
                    accionOK = false;
                  }
                }
                else {
                  accionOK = false;
                }
              }
              else {
                e.printStackTrace();
                Common.ShowWarning(this.getCApp(),
                                   "Error al modificar el resultado.");
                accionOK = false;
              }
            }

            if (accionOK) {
              // Establecemos en el resul actual el numero de resul que se acaba de modif
              MuesActual = (DatCasMuesSC) newMues.firstElement();
              muesNueva = MuesActual;
              // Cerramos el dialogo
              this.dispose();
            }
          }
        } // validarDatos() ya muestra mensajes de error
        break;
      case constantes.modoBAJA:

        // Recogida de datos en un DatCasMuesSC
        MuesActual = recogerDatos();

        // Establecimiento de los parametros
        parametros.addElement(diaNotif);
        parametros.addElement(MuesActual);

        try {
          // Invocacion del servlet para que haga el delete
          newMues = hazAccion(stubCliente, constantes.modoBAJA, parametros);
          accionOK = true;
        }
        catch (Exception e) {

          if (e.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1) {
            if (Common.ShowPregunta(getCApp(), DATOS_BLOQUEADOS)) {
              try {
                newMues = hazAccion(stubCliente, constantes.modoBAJA_SB,
                                    parametros);
                accionOK = true;
              }
              catch (Exception ex) {
                ex.printStackTrace();
                Common.ShowWarning(this.getCApp(),
                                   "Error al borrar el resultado.");
                accionOK = false;
              }
            }
            else {
              accionOK = false;
            }
          }
          else {
            e.printStackTrace();
            Common.ShowWarning(this.getCApp(), "Error al borrar el resultado.");
            accionOK = false;
          }
        }

        if (accionOK) {
          MuesActual = (DatCasMuesSC) newMues.firstElement();
          muesNueva = MuesActual;
          // Cerramos el dialogo
          this.dispose();
        }

        break;
    } // Fin switch
  } // Fin btnAceptarActionPerformed()

  void btnCancelarActionPerformed() {
    dispose();
  } // Fin btnCancelarActionPerformed()

  // Manejador btnPrimero
  protected void btnTablaPrimero() {
    if (tabla.countItems() > 0) {
      tabla.select(0);
      tabla.setTopRow(0);
    }
  }

  // Manejador btnAnterior
  protected void btnTablaAnterior() {
    if (tabla.countItems() > 0) {
      int index = tabla.getSelectedIndex();
      if (index > 0) {
        index--;
        tabla.select(index);
        if (index - tabla.getTopRow() <= 0) {
          tabla.setTopRow(tabla.getTopRow() - 1);
        }
      }
      else {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    }
  }

  // Manejador btnSiguiente
  protected void btnTablaSiguiente() {
    if (tabla.countItems() > 0) {
      int ultimo = tabla.countItems() - 1;
      int index = tabla.getSelectedIndex();

      if (index >= 0 && index < ultimo) {
        index++;
        tabla.select(index);
        if (index - tabla.getTopRow() >= 4) {
          tabla.setTopRow(tabla.getTopRow() + 1);
        }
      }
      else {
        tabla.select(0);
        tabla.setTopRow(0);
      }
    }
  }

  // Manejador btnUltimo
  protected void btnTablaUltimo() {
    int index = 0;
    int ultimo = tabla.countItems() - 1;
    if (ultimo >= 0) {
      index = ultimo;
      tabla.select(index);
      if (tabla.countItems() >= 5) {
        tabla.setTopRow(tabla.countItems() - 5);
      }
      else {
        tabla.setTopRow(0);
      }
    }
  }

  // Manejador btnAlta
  protected void btnAlta() {
    // Relleno de los datos de una resistencia (modoALTA)
    DatCasResis res = new DatCasResis("", "", "", "");

    // Dialogo para dar de alta una nueva resistencia
    DiaResis dlg = new DiaResis(this.getCApp(), constantes.modoALTA, LEstRes,
                                res);
    dlg.show();

    // Si ha habido exito en la insercion de una  nueva resistencia
    //  se a�ade la misma a la tabla de resistencias, se
    //  pinta la tabla de resistencias
    if (dlg.accionOK) {
      res = dlg.resisNueva;
      if (!estaEnListaResis(res)) {
        listaResis.addElement(res);
        escribirTablaResistencias();
      }
      else {
        Common.ShowWarning(this.app, "La resistencia ya est� incluida");
      }
    }

    dlg = null;
    return;
  } // Fin btnAlta()

  // Manejador btnModificar
  protected void btnModificar() {
    // Relleno de los datos de una muestra (modoMODIFICACION)
    DatCasResis res = (DatCasResis) listaResis.elementAt(tabla.getSelectedIndex());

    // Dialogo para modificar/consultar una resistencia
    DiaResis dlg = null;
    if (modoEntrada == constantes.modoCONSULTA) {
      ;
    }
    else {
      dlg = new DiaResis(this.getCApp(), constantes.modoMODIFICACION, LEstRes,
                         res);

    }
    dlg.show();

    // Si ha habido exito en la modificacion de una resistencia
    //  se sustituye la misma en la tabla de resistencias, se
    //  pinta la tabla de resistencias
    if (dlg.accionOK) {
      res = dlg.resisNueva;
      if (!estaEnListaResis(res)) {
        listaResis.setElementAt(res, tabla.getSelectedIndex());
        escribirTablaResistencias();
      }
      else {
        Common.ShowWarning(this.app, "La resistencia ya est� incluida");
      }
    }

    dlg = null;
    return;
  } // Fin btnModificar()

  // Manejador btnBaja
  protected void btnBaja() {
    // No hay dialogo de resistencia. Se da de baja directamente.
    listaResis.removeElementAt(tabla.getSelectedIndex());
    escribirTablaResistencias();
  } // Fin btnBaja()

  protected void tablaActionPerformed(jclass.bwt.JCActionEvent e) {
    switch (modoEntrada) {
      case constantes.modoALTA:
      case constantes.modoMODIFICACION:
        btnModificar();
        break;
      case constantes.modoBAJA:
      case constantes.modoCONSULTA:
        break;
    }
  } // Fin tablaActionPerformed

  void chkboxItemStateChanged(ItemEvent e) {
    // Para que sea inicializado el dialogo
  } // Fin chkboxItemStateChanged()

  void cfsFMuesFocusLost() {
    // Validacion de la fecha
    cfsFMues.ValidarFecha();

    // Carga de la fecha
    cfsFMues.setText(cfsFMues.getFecha());

  }
} // endclass DiaMues

/******************* ESCUCHADORES **********************/

// Botones Aceptar y Cancelar
// class DiaMuesActionAdapter implements java.awt.event.ActionListener, Runnable{
class DiaMuesActionAdapter
    implements java.awt.event.ActionListener {

  DiaMues adaptee;
  ActionEvent evt;

  DiaMuesActionAdapter(DiaMues adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    //new Thread(this).start();
    if (adaptee.bloquea()) {
      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      adaptee.desbloquea();
    }
  }

  /*  public void run(){
      if(adaptee.bloquea()){
        if (evt.getActionCommand().equals("Aceptar")){
          adaptee.btnAceptarActionPerformed();
        } else if (evt.getActionCommand().equals("Cancelar")){
          adaptee.btnCancelarActionPerformed();
        }
        adaptee.desbloquea();
      }
    }*/
} // Fin clase DiaMuesActionAdapter

// Checkboxes
class DiaMuesChkSensibleItemAdapter
    implements java.awt.event.ItemListener {
  DiaMues adaptee;

  DiaMuesChkSensibleItemAdapter(DiaMues adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    if (adaptee.bloquea()) {
      adaptee.chkboxItemStateChanged(e);
      adaptee.desbloquea();
    }
  }
} // Fin DiaMuesChkSensibleItemAdapter

// Gesti�n de eventos sobre los botones de la tabla
class btnTablaActionListener
    implements java.awt.event.ActionListener {
  DiaMues adaptee;

  public btnTablaActionListener(DiaMues ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      if (e.getActionCommand() == "primero") {
        adaptee.btnTablaPrimero();
      }
      else if (e.getActionCommand() == "anterior") {
        adaptee.btnTablaAnterior();
      }
      else if (e.getActionCommand() == "siguiente") {
        adaptee.btnTablaSiguiente();
      }
      else {
        adaptee.btnTablaUltimo();
      }

      adaptee.desbloquea();
    }
  }
} // endclass btnTablaActionListener

// Gesti�n de eventos sobre los botones A�ADIR, MODIFICAR, BORRAR
class btnAMBActionListener
    implements java.awt.event.ActionListener {
  DiaMues adaptee;

  public btnAMBActionListener(DiaMues ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      if (e.getActionCommand() == "alta") {
        adaptee.btnAlta();
      }
      else if (e.getActionCommand() == "modificar") {
        adaptee.btnModificar();
      }
      else if (e.getActionCommand() == "baja") {
        adaptee.btnBaja();
      }
      adaptee.desbloquea();
    }
  }
} // endclass btnAMBActionListener

class jcalTablaActionListener
    implements jclass.bwt.JCActionListener {
  DiaMues adaptee;

  public jcalTablaActionListener(DiaMues ptnPanel) {
    adaptee = ptnPanel;
  }

  public void actionPerformed(jclass.bwt.JCActionEvent e) {
    if (adaptee.bloquea()) {
      adaptee.tablaActionPerformed(e);
      adaptee.desbloquea();
    }
  }
} // endclass jcalTablaActionListener

// Perdidas de foco
class DiaMuesFocusAdapter
    implements java.awt.event.FocusListener, Runnable {
  DiaMues adaptee;
  FocusEvent evt;

  DiaMuesFocusAdapter(DiaMues adaptee) { // Constructor
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  } // Fin focusLost()

  public void focusGained(FocusEvent e) {
  }

  // Implementacion de run()
  public void run() {
    if (adaptee.bloquea()) {
      if ( ( (TextField) evt.getSource()).getName().equals("cfsFIni")) {
        adaptee.cfsFMuesFocusLost();
      }
    }
    adaptee.desbloquea();
  } // Fin run()

} // Fin clase
