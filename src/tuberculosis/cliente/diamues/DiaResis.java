/**
 * Clase: DiaResis
 * Paquete: tuberculosis.cliente.diamues.DiaResis
 * Hereda: CDialog
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 01/12/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: Implementacion del dialogo que permite dar de alta
 *   y modificar resistencias de una muestra.
 */

package tuberculosis.cliente.diamues;

import java.awt.Choice;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import comun.Common;
import comun.constantes;
import tuberculosis.datos.panmues.DatCasResis;

public class DiaResis
    extends CDialog {

  // Estado: indica si se ha llevado a cabo correctamente la operacion deseada
  public boolean accionOK = false;
  public DatCasResis resisNueva = null;

  // Variable estado del panel
  protected int modoOperacion = constantes.modoALTA;
  protected int modoEntrada = constantes.modoALTA;

  // Sincronizaci�n de eventos
  protected boolean sinBloquear = true;
  protected int modoAnterior;
  ;

  // Datos de la resistencia a modificar
  DatCasResis ResisInicial = null;

  // Lista de Estudios de Resistencias
  CLista LEstRest = null;

  // Imagenes
  protected CCargadorImagen imgs = null;
  final String imgNAME[] = {
      constantes.imgACEPTAR,
      constantes.imgCANCELAR};

  /****************** Componentes del panel ********************/

  // Organizacion del panel
  private XYLayout xYLayout = new XYLayout();

  // Estudio de Resistencias
  private Label lblEstRest = new Label("Estudio Resistencias");
  private Choice choEstRest = new Choice();

  // Botones de aceptar y cancelar
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores
  DiaResisActionAdapter actionAdapter = null;

  // Constructor
  // CApp sera luego el panel del que procede: PanTratMues
  // @param listaEstRest: lista de Estudios de Resistencias
  // @param resis: lleva los datos de una resistencia en caso de modificaci�n/baja/consulta
  public DiaResis(CApp a, int modo, CLista listaEstRest,
                  DatCasResis resis) {
    super(a);
    try {
      // Variables globales
      LEstRest = listaEstRest;
      ResisInicial = resis;

      // Inicializacion
      modoOperacion = modo;
      modoEntrada = modo;
      jbInit();

      if (modoEntrada == constantes.modoMODIFICACION ||
          modoEntrada == constantes.modoBAJA ||
          modoEntrada == constantes.modoCONSULTA) {
        rellenarDatos(ResisInicial);

      }
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    // Organizacion del panel
    this.setSize(new Dimension(350, 145));
    xYLayout.setHeight(145);
    xYLayout.setWidth(350);
    this.setLayout(xYLayout);
    this.setTitle("Estudio Resistencias");

    // Carga de las im�genes
    imgs = new CCargadorImagen(getCApp(), imgNAME);
    imgs.CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaResisActionAdapter(this);

    // Estudio de Resistencias
    this.add(lblEstRest, new XYConstraints(15, 15, 120, -1));
    Common.writeChoiceDataGeneralCDDS(choEstRest, LEstRest, false, false, true);
    this.add(choEstRest, new XYConstraints(15 + 120 + 10, 15, 180, 25));

    // Boton de Aceptar
    btnAceptar.setImage(imgs.getImage(0));
    btnAceptar.setLabel("Aceptar");
    this.add(btnAceptar, new XYConstraints(155, 75, 80, -1));
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.addActionListener(actionAdapter);

    // Boton de Cancelar
    btnCancelar.setImage(imgs.getImage(1));
    btnCancelar.setLabel("Cancelar");
    this.add(btnCancelar, new XYConstraints(245, 75, 80, -1));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);
  } // Fin jbInit()

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {
    switch (modoOperacion) {
      case constantes.modoESPERA:
        enableControls(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case constantes.modoALTA:
        enableControls(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case constantes.modoMODIFICACION:
        enableControls(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
  } // Fin Inicializar()

  private void enableControls(boolean en) {
    choEstRest.setEnabled(en);
  } // Fin enableControls()

  public DatCasResis recogerDatos() {
    DatCasResis resul = null;

    String sEstRest = Common.getChoiceCDDataGeneralCDDS(choEstRest, LEstRest, false, false, true);
    String sDsEstRest = Common.getChoiceDSDataGeneralCDDS(choEstRest, LEstRest, false, false, true);
    String sOpe = this.getCApp().getParameter("login", "NO ONE"); // Operario que realiza el alta

    resul = new DatCasResis(sEstRest, sDsEstRest, sOpe, "");
    return resul;
  } // Fin recogerDatos()

  public boolean validarDatos() {
    // 1. PERDIDAS DE FOCO: No hay campos CD-Button-DS

    // 2. LONGITUDES: No hay

    // 3. DATOS NUMERICOS: No hay

    // 4. CAMPOS OBLIGATORIOS: No hay

    // Si llega aqui todo ha ido bien
    return true;
  } // Fin validarDatos()

  public void rellenarDatos(DatCasResis data) {
    // Estudio de Resistencias
    Common.selectChoiceElement(choEstRest, LEstRest, false, data.getCD_ESTREST());
  } // Fin rellenarDatos()

  // Bloquear eventos
  /** Este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized boolean bloquea() {
    if (sinBloquear) {
      // No hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modoOperacion;
      modoOperacion = constantes.modoESPERA;
      Inicializar();

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // Ya est� bloqueado
      return false;
    }
  } // Fin bloquea()

  /** Este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    modoOperacion = modoAnterior;
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  } // Fin desbloquea()

  /******************** Manejadores *************************/

  void btnAceptarActionPerformed() {

    DatCasResis ResisActual = null;

    switch (modoEntrada) {
      case constantes.modoALTA:
      case constantes.modoMODIFICACION:

        // Recogida de datos en un DatCasTratSC
        ResisActual = recogerDatos();

        // Estado de la operacion
        accionOK = true;
        resisNueva = ResisActual;
        this.dispose();
        break;
    } // Fin switch
  } // Fin btnAceptarActionPerformed()

  void btnCancelarActionPerformed() {
    dispose();
  } // Fin btnCancelarActionPerformed()

} // endclass DiaResis

/******************* ESCUCHADORES **********************/

// Botones
class DiaResisActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaResis adaptee;
  ActionEvent evt;

  DiaResisActionAdapter(DiaResis adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    if (adaptee.bloquea()) {
      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      adaptee.desbloquea();
    }
  }
} // Fin clase DiaResisActionAdapter
