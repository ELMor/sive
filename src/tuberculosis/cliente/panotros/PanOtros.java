package tuberculosis.cliente.panotros;

import java.util.Hashtable;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CPanel;
import comun.Common;
import comun.constantes;
import infproto.DataProtocolo;
// modificacion 30/06/2000 para obtener protocolo general
import tuberculosis.cliente.diaprot.DiaProt;
import tuberculosis.cliente.diatuber.DialogTub;
import tuberculosis.datos.panmues.DatCasMuesCS;
import tuberculosis.datos.pantrat.DatCasTratCS;

public class PanOtros
    extends CPanel {

  final int modoESPERA = constantes.modoESPERA;
  final int modoALTA = constantes.modoALTA; //no se dara
  final int modoMODIFICACION = constantes.modoMODIFICACION;
  final int modoBAJA = constantes.modoBAJA;
  final int modoCONSULTA = constantes.modoCONSULTA;

  protected int modoOperacion = modoALTA;

  // dialog que contiene a este panel
  DialogTub dlgB;
  DatCasMuesCS datNotifM = null;
  DatCasTratCS datNotifT = null;

  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

  // controles
  XYLayout xYLayout = new XYLayout();

  public Hashtable hash = null;
  public String nCaso = "";

  ButtonControl btnTotResul = new ButtonControl();
  ButtonControl btnTotEvol = new ButtonControl();

  //modificacion 30/06/2000 para generar un bot�n que permita
  // visualizar en modo consulta el protocolo general
  ButtonControl btnProtGen = new ButtonControl();

  actionAdapter actionAdap = new actionAdapter(this);

  public CApp app = null;

  public PanOtros(DialogTub dlg,
                  int modo,
                  Hashtable hsCompleta, String NM_EDO) {

    try {
      app = dlg.getCApp();
      dlgB = dlg;
      hash = hsCompleta;
      nCaso = NM_EDO;
      jbInit();
      modoOperacion = modo;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    // carga las im�genes
    CCargadorImagen imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnTotResul.setLabel("Todos los Resultados");
    btnTotResul.setActionCommand("resultados");
    btnTotResul.addActionListener(actionAdap);
    btnTotEvol.setLabel("Todos las Evoluciones");
    btnTotEvol.setActionCommand("evoluciones");
    btnTotEvol.addActionListener(actionAdap);
    btnProtGen.setLabel("Protocolo General");
    btnProtGen.setActionCommand("protocolo");
    btnProtGen.addActionListener(actionAdap);

    // Organizacion del panel
    this.setSize(new Dimension(500, 250));
    xYLayout.setHeight(250);
    xYLayout.setWidth(702);
    this.setLayout(xYLayout);
    setBorde(false);

    this.add(btnTotResul, new XYConstraints(296, 67, 144, -1));
    this.add(btnTotEvol, new XYConstraints(296, 102, 144, -1));
    this.add(btnProtGen, new XYConstraints(296, 137, 144, -1));

  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {

      case modoESPERA:
        btnTotResul.setEnabled(false);
        btnTotEvol.setEnabled(false);
        btnProtGen.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA: //no se dar�
        btnTotResul.setEnabled(true);
        btnTotEvol.setEnabled(true);
        btnProtGen.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
      case modoMODIFICACION:
      case modoBAJA:
      case modoCONSULTA:
        btnTotResul.setEnabled(true);
        btnTotEvol.setEnabled(true);
        btnProtGen.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

    }
  }

  // modificacion 10/07/2000
  public void ChangedProtocolo() {

    // variable para controlar que cuando se grabe un protocolo y
    // se pulse grabar, si estamos en el panel del protocolo, se
    // grabe dicho protocolo

    //dlgB.bGrabarProt = false;

    if (dlgB.bChanging) {
      if (Common.ShowPregunta(this.app,
                              "El protocolo ha cambiado. �Desea grabarlo?")) {
        //dlgB.grabarProtocolo();
        dlgB.tabPanel.VerPanel("Protocolo Notificador");
        dlgB.bChanging = false;
      }
      else {
        dlgB.bChanging = false;
      }
    }
  }

  public void btnTotResul_actionPerformed() {
    int modo = modoOperacion;
    dlgB.Inicializar(constantes.modoESPERA);

    DiaTotMues diaTotMuestras = new DiaTotMues(this, modo, nCaso);

    diaTotMuestras.addPanelMuestras();

    diaTotMuestras.show();

    if (diaTotMuestras.iOut == -1) { //cancelar
      diaTotMuestras = null;
    }
    else { //aceptar
      diaTotMuestras = null;
    }

    dlgB.Inicializar(modo);

  }

  public void btnTotEvol_actionPerformed() {
    int modo = modoOperacion;
    dlgB.Inicializar(constantes.modoESPERA);

    DiaTotTrat diaTotTrat = new DiaTotTrat(this, modo, nCaso);

    diaTotTrat.addPanelTrat();

    diaTotTrat.show();

    if (diaTotTrat.iOut == -1) { //cancelar
      diaTotTrat = null;
    }
    else { //aceptar
      diaTotTrat = null;
    }

    dlgB.Inicializar(modo);

  }

  public void btnProtGen_actionPerformed() {

    int modo = modoOperacion;
    dlgB.Inicializar(constantes.modoESPERA);

    DataProtocolo data = new DataProtocolo();
    //data.sCodigo = constantes.TUBERCULOSIS;
    // modificacion 12/02/2001
    data.sDescripcion = this.app.getParametro("CD_TUBERCULOSIS");
    data.sDescripcion = "TUBERCULOSIS";

    // modificacion 09/06/2000
    // de esta manera conseguimos que aparezcan los protocolos
    // generados por usuarios de area y distrito
    String s1 = null;
    String s2 = null;
    //if(modoOperacion == modoALTA) {
    //s1 = this.pansup.panNiv.getCDNivel1();
    //s2 = this.pansup.panNiv.getCDNivel2();
    //} else {
//    s1 = (String) getNivel1Gestion();
//    s2 = (String) getNivel2Gestion();
    //}

//    data.sNivel1 = s1;
//    data.sNivel2 = s2;

    data.sNivel1 = (String) dlgB.s1;
    data.sNivel2 = (String) dlgB.s2;

    //data.sNivel1 = dlgb.panCabecera.txtCodNivel1.getText().trim();
    //data.sNivel2 = dlgb.panCabecera.txtCodNivel2.getText().trim();
    data.NumCaso = dlgB.sNM_EDO;
    data.sCa = app.getCA();
    //sNotif = (DataNotifTub) getNotificador().getCD_E_NOTIF();

    DiaProt diaProt = new DiaProt(this, modo, data, true);

    diaProt.addPanelProt();

    diaProt.show();

    if (diaProt.iOut == -1) { //cancelar
      diaProt = null;
    }
    else { //aceptar
      diaProt = null;
    }

    dlgB.Inicializar(modo);
  }

} //clase

class actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanOtros adaptee;
  ActionEvent evt;

  actionAdapter(PanOtros adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if (evt.getActionCommand().equals("resultados")) {
      adaptee.btnTotResul_actionPerformed();
    }
    else if (evt.getActionCommand().equals("evoluciones")) {
      adaptee.btnTotEvol_actionPerformed();
    }
    else if (evt.getActionCommand().equals("protocolo")) {
      adaptee.btnProtGen_actionPerformed();
    }
  }
} //fin class
