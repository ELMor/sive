/**
 * Clase: DiaConTub
 * Paquete: tuberculosis.cliente.PanConCasTub
 * Hereda: CDialog
 * Autor: Pedro Antonio D�az (PDP)
 * Fecha Inicio: 28/01/2000
 * Analisis Funcional: Mantenimiento Contactos Tuberculosis.
 * Descripcion: Implementacion del dialogo que permite visualizar los datos
    principales de los contactos de enfermos de tuberculosis. Los registros
    mostrados dependen de los valores suministrados a los campos de filtrado.
    Ademas permite acceder a la pantalla de modificacion/cierre/deshacer cierre
    de los datos asociados a un determinado registro de tuberculosis.
 */

package tuberculosis.cliente.diacontub;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CMessage;
import comun.Fechas;
import comun.constantes;
import tuberculosis.datos.mantregtub.DatMntRegTubSC;

public class DiaConTub
    extends CDialog {

  public int iERROR = -1; //0: error

  //Parametros que se pasan al Panel principal
  public String sAno = null;
  public String sReg = null;
  public String sNombre = null;

  public String CD_OPE_RTBC = null;
  public java.sql.Timestamp FC_ULTACT_RTBC = null;

  // Para almacenar el �rea y distrito de gesti�n de un caso
  private String CD_NIVEL_1_GE = null;
  private String CD_NIVEL_2_GE = null;

  // Datos para conocer las 'coordenadas' de EDOIND para
  // el protocolo
  private String CD_NIVEL_1_EDOIND = null;
  private String CD_NIVEL_2_EDOIND = null;

  // Contenedores de im�genes
  protected CCargadorImagen imgs = null;
  protected CCargadorImagen imgs_mantenimiento = null;
  protected CCargadorImagen imgs_tabla = null;

  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/refrescar.gif",
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/declaracion.gif",
      "images/declaracion2.gif"};

  // Inclusion de paneles
  public PanConCasTub pnlCont = null;

  //Datos para el envio de informaci�n al di�logo
  public DatMntRegTubSC RegSel = null;

  public String auxcheck = null;

  // Applet del que procede este dialogo
  //CApp capp;

  // Modo de operaci�n del di�logo
  static final int modoESPERA = constantes.modoESPERA;
  static final int modoNORMAL = constantes.modoNORMAL;

  private int modoOperacion = modoNORMAL;

  // INTERFAZ CON OTROS COMPONENTES

  // Niveles de EDOIND para cargar el protocolo adecuado
  public void setCD_NIVEL_1_EDOIND(String s) {
    CD_NIVEL_1_EDOIND = s;
  }

  public void setCD_NIVEL_2_EDOIND(String s) {
    CD_NIVEL_2_EDOIND = s;
  }

  public String getCD_NIVEL_1_EDOIND() {
    return CD_NIVEL_1_EDOIND;
  }

  public String getCD_NIVEL_2_EDOIND() {
    return CD_NIVEL_2_EDOIND;
  }

  // Niveles de gesti�n
  public void setCD_NIVEL_1_GE(String s) {
    CD_NIVEL_1_GE = s;
  }

  public void setCD_NIVEL_2_GE(String s) {
    CD_NIVEL_2_GE = s;
  }

  public String getCD_NIVEL_1_GE() {
    return CD_NIVEL_1_GE;
  }

  public String getCD_NIVEL_2_GE() {
    return CD_NIVEL_2_GE;
  }

  /* -------------------- CONTROLES ------------------------ */

  // Organizacion del panel
  private XYLayout lyXYLayout = new XYLayout();

  // Botones de grabar y cancelar
  ButtonControl btnGrabar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores
  DiaConTubActionAdapter actionAdapter = null;

  /* CONSTRUCTOR */

  public DiaConTub(CApp a) {
    super(a);
    try {
      this.app = a;
      // Paneles
      pnlCont = new PanConCasTub(this);

      //T�tulo
      this.setTitle("B�squeda de casos de tuberculosis");

      jbInit();

      Inicializar();

    }
    catch (Exception ex) {
      ex.printStackTrace();
      this.dispose();
      this.iERROR = 0;
    }

  } // Fin constructor

  void jbInit() throws Exception {

    // Vars. para colocar los componentes
    int pX = 0;
    int pY = 0;
    int longX = 0;
    int longY = 0;

    // Medidas y organizacion del panel
    this.setSize(new Dimension(666, 430)); //680,370
    lyXYLayout.setHeight(430);
    lyXYLayout.setWidth(666);
    this.setLayout(lyXYLayout);

    // Carga de las im�genes
    imgs = new CCargadorImagen(getCApp(), imgNAME);
    imgs.CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaConTubActionAdapter(this);

    // Panel Contactos
    pX = 0;
    longX = 680;
    pY += 0;
    longY = 350;
    this.add(pnlCont, new XYConstraints(pX, pY, longX, longY));

    // Boton de grabar
    btnGrabar.setImage(imgs.getImage(2));
    btnGrabar.setLabel("Aceptar");
    pX = 477;
    longX = 80;
    pY += longY;
    longY = -1;
    this.add(btnGrabar, new XYConstraints(pX, pY, longX, longY));
    btnGrabar.setActionCommand("Grabar");
    btnGrabar.addActionListener(actionAdapter);

    // Boton de cancelar
    btnCancelar.setImage(imgs.getImage(3));
    btnCancelar.setLabel("Cancelar");
    pX += longX + 10 - 2;
    longX = 80;
    longY = -1;
    this.add(btnCancelar, new XYConstraints(pX, pY, longX, longY));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);
  } // Fin jbInit()

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar(modo)

  public void Inicializar() {
    switch (modoOperacion) {
      case modoESPERA:
        pnlCont.setEnabled(false);
        btnGrabar.setEnabled(false);
        btnCancelar.setEnabled(false);

        // Cursor de espera
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoNORMAL:
        pnlCont.setEnabled(true);
        btnGrabar.setEnabled(true);
        btnCancelar.setEnabled(true);

        // Cursor normal
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
  }

  private void RegSelect() {
    if (pnlCont.tabla.getSelectedIndex() >= 0) {
      RegSel = (DatMntRegTubSC) pnlCont.listaRegTubs.elementAt(pnlCont.tabla.
          getSelectedIndex());
    }
    else {
      RegSel = null;
    }
  }

  public String getCD_OPE_RTBC() {
    return CD_OPE_RTBC;
  }

  public java.sql.Timestamp getFC_ULTACT_RTBC() {
    return FC_ULTACT_RTBC;
  }

  public String getAno() {
    return sAno;
  }

  public String getReg() {
    return sReg;
  }

  public String getNom() {
    return sNombre;
  }

  public void setAno(String sAno) {
    pnlCont.setAno(sAno);
  }

  public void setReg(String sReg) {
    pnlCont.setReg(sReg);
  }

  /******************** Manejadores *************************/

  void btnGrabarActionPerformed() {
    //DatMntRegTubSC RegSel = null;
    if (pnlCont.tabla.getSelectedIndex() >= 0) {
      RegSelect();
      if (RegSel != null) {
        sAno = RegSel.getCD_ANO();
        sReg = RegSel.getCD_REG();
        sNombre = RegSel.getDS_NOMBREENFERMO() + " " +
            RegSel.getDS_APE1ENFERMO() + " " +
            RegSel.getDS_APE2ENFERMO();
        CD_OPE_RTBC = RegSel.getCD_OPE();
        FC_ULTACT_RTBC = Fechas.string2Timestamp(RegSel.getFC_ULTACT());

        setCD_NIVEL_1_EDOIND(RegSel.getCD_NIVEL_1_EDOIND());
        setCD_NIVEL_2_EDOIND(RegSel.getCD_NIVEL_2_EDOIND());

        setCD_NIVEL_1_GE(RegSel.getCD_NIVEL_1_GE());
        setCD_NIVEL_2_GE(RegSel.getCD_NIVEL_2_GE());
      }
      dispose();
    }
    else {
      ShowWarning(
          "Por favor, realice la b�squeda y seleccione una fila de la tabla");
    }

  } // fin btnGrabarActionPerformed()

  /*
     // Rellena la lista de busqueda con los parametros apropiados del panel
     void CargarListaBusqueda() {
    String Reg = txtReg.getText().trim();
    String CodEnfermo = txtReg.getText().trim();
    // Creamos una estructura de datos de busqueda y la rellenamos
    dataBusqueda = new DatConTub(Ano,Reg,null,null,null,null,null,null);
    // A�adimos la estructura a la lista de datos de busqueda
    listaBusquedaConTub.addElement(dataBusqueda);
     } // Fin CargarListaBusqueda()
   */

  void btnCancelarActionPerformed() {
    sAno = null;
    sReg = null;
    sNombre = null;

    dispose();
  } // Fin btnCancelarActionPerformed()

  /*************** FUNCIONES AUXILIARES ***********************/

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  public void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  } // Fin ShowWarning()

  // Botones
  class DiaConTubActionAdapter
      implements java.awt.event.ActionListener, Runnable {
    DiaConTub adaptee;
    ActionEvent evt;

    DiaConTubActionAdapter(DiaConTub adaptee) {
      this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
      this.evt = e;
      new Thread(this).start();
    }

    public void run() {
      if (evt.getActionCommand().equals("Grabar")) {
        adaptee.btnGrabarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
    }
  }
} // Fin clase DiaConTubActionAdapter
