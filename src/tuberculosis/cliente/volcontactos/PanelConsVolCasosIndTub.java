package tuberculosis.cliente.volcontactos;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import comun.constantes; //Para c�digo de la tuberculosis
import sapp.StubSrvBD;
import tuberculosis.datos.volcontactos.DataVolCasInd;
import tuberculosis.datos.volcontactos.ParVolCasosInd;
import utilidades.PanFechas;

public class PanelConsVolCasosIndTub
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;
  final int modoVOLCADO = 8;

  //rutas imagenes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/vaciar.gif",
      "images/salvar.gif"};

  protected StubSrvBD stubCliente = new StubSrvBD();
  public ParVolCasosInd parCons;
  protected int modoOperacion = modoNORMAL;
  protected PanelListaPreg panelListaPreg;
  // servlet
  String sUrlServlet;
  File miFichero = null;
  FileWriter fStream = null;
  // stub's
  final String strSERVLETNivel1 = constantes.strSERVLET_NIVEL1;
  final String strSERVLETNivel2 = constantes.strSERVLET_NIV2;
  final String strSERVLETZona = constantes.strSERVLET_MUN;
  final String strSERVLETMun = constantes.strSERVLET_MUN;
  final String strSERVLETProv = constantes.strSERVLET_CAT2;
  final String strSERVLETEnf = constantes.strSERVLET_ENFVIGI;

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  final int modoVOLCADO_TUBERCULOSIS = 9; //Volcado de tuberculosis

  XYLayout xYLayout = new XYLayout();
  //Comps para fechas
  Label lblHasta = new Label();
  Label lblDesde = new Label();
  PanFechas fechasDesde;
  PanFechas fechasHasta;
  //Componente para lectura de nombre fichero
  capp.CFileName panFichero;

  ButtonControl btnInforme = new ButtonControl();
  //Comps Enfermedad
  Label lblEnfermedad = new Label();

  //Comps separador
  Label lblSeparador = new Label();
  TextField txtSeparador = new TextField();
  //Escucahdores de eventos
  PanelConsVolCasosIndTub_actionListener btnActionListener = new
      PanelConsVolCasosIndTub_actionListener(this);

  protected CCargadorImagen imgs = null;

  //_________________________________________________________

  public PanelConsVolCasosIndTub(CApp a, PanelListaPreg p) {
    try {
      sUrlServlet = constantes.strSERVLET_MODPREGTUB;
      setApp(a);
      res = ResourceBundle.getBundle("tuberculosis.cliente.volcontactos.Res" +
                                     app.getIdioma());
      panelListaPreg = p;
      fechasDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      fechasHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      panFichero = new capp.CFileName(a);
      jbInit();
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  //_________________________________________________________

  void jbInit() throws Exception {

    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(447);

    btnInforme.setLabel(res.getString("btnInforme.Label"));

    // gestores de eventos
    btnInforme.addActionListener(btnActionListener);

    lblSeparador.setText(res.getString("lblSeparador.Text"));
    txtSeparador.setText("$");

    btnInforme.setActionCommand("generar");

    txtSeparador.setName("txtSeparador");

    lblEnfermedad.setText("Enfermedad:  Tuberculosis");
    lblHasta.setText(res.getString("lblHasta.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));

    xYLayout.setWidth(596);
    this.setLayout(xYLayout);

    this.add(lblDesde, new XYConstraints(26, 5, 51, -1));
    this.add(fechasDesde, new XYConstraints(40, 5, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 35, -1, -1));
    this.add(fechasHasta, new XYConstraints(40, 35, -1, -1));
    this.add(lblEnfermedad, new XYConstraints(26, 65, 183, -1));
    this.add(panFichero, new XYConstraints(12, 95, -1, -1));

    this.add(lblSeparador, new XYConstraints(26, 145, 129, -1));
    this.add(txtSeparador, new XYConstraints(165, 145, -1, -1));

    this.add(btnInforme, new XYConstraints(425, 175, -1, -1));

    imgs = new CCargadorImagen(app, imgNAME);
    txtSeparador.setBackground(new Color(255, 255, 150));
    imgs.CargaImagenes();

    btnInforme.setImage(imgs.getImage(2));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  //_________________________________________________________

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (fechasDesde.txtAno.getText().length() == 0) ||
        (fechasHasta.txtAno.getText().length() == 0) ||
        (fechasDesde.txtCodSem.getText().length() == 0) ||
        (fechasHasta.txtCodSem.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    // Comprobacion del separador
    if (txtSeparador.getText().length() == 0) {
      bDatosCompletos = false;

      // Comprobacion del fichero
    }
    if (panFichero.txtFile.getText().length() == 0) {
      bDatosCompletos = false;

    }
    return bDatosCompletos;
  }

  //_________________________________________________________

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:

        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //_________________________________________________________

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //_________________________________________________________

  void btnGenerar_actionPerformed(ActionEvent evt) {

    CMessage msgBox;
    DataVolCasInd par = new DataVolCasInd();

    if (isDataValid()) {
      //par.enfer = constantes.TUBERCULOSIS;
      // modificacion 12/02/2001
      par.enfer = this.app.getParametro("CD_TUBERCULOSIS");
      par.anoD = fechasDesde.txtAno.getText();
      par.anoH = fechasHasta.txtAno.getText();
      if (fechasDesde.txtCodSem.getText().length() == 1) {
        par.semD = "0" + fechasDesde.txtCodSem.getText();
      }
      else {
        par.semD = fechasDesde.txtCodSem.getText();
      }
      if (fechasHasta.txtCodSem.getText().length() == 1) {
        par.semH = "0" + fechasHasta.txtCodSem.getText();
      }
      else {
        par.semH = fechasHasta.txtCodSem.getText();

        // Compruebo si se calculan las tasas
        // tasas si area o todo vacio
        // de momento lo pongo a false
      }
      par.sep = txtSeparador.getText();
      par.tSive = getApp().getTSive();
      par.codsPreg = panelListaPreg.obtenerCodPreg();

      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();
      try {
        CLista data = new CLista();

        // idioma
        data.setIdioma(app.getIdioma());

        // estado
        data.setState(CLista.listaINCOMPLETA);

        data.addElement(par);

        stubCliente.setUrl(new URL(app.getURL() + sUrlServlet));
        data = (CLista) stubCliente.doPost(this.modoVOLCADO_TUBERCULOSIS, data);

        /*tuberculosis.servidor.volcontactos.SrvModPregTub servlet = new tuberculosis.servidor.volcontactos.SrvModPregTub();
                 //Indica como conectarse a la b.datos
                 servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
             data = (CLista) servlet.doDebug(this.modoVOLCADO_TUBERCULOSIS, data);*/

        if (data != null) {
          // Guardar en fichero
          String sLinea = new String();
          if (data.size() > 0) {
            try {
              miFichero = new File(this.panFichero.txtFile.getText());
              fStream = new FileWriter(miFichero);

              for (int j = 0; j < data.size(); j++) {
                sLinea = (String) data.elementAt(j);
                fStream.write(sLinea);
                fStream.write("\r");
                fStream.write("\n");
              }
              fStream.close();
              fStream = null;
              miFichero = null;
              msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                    res.getString("msg14.Text"));
              msgBox.show();
              msgBox = null;
            }
            catch (IOException ioEx) {
              ioEx.printStackTrace();
              msgBox = new CMessage(this.app, CMessage.msgERROR,
                                    res.getString("msg15.Text"));
              msgBox.show();
              msgBox = null;
            }
          }
          else {
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                  res.getString("msg16.Text"));
            msgBox.show();
            msgBox = null;
          }
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                "Error desconocido");
          msgBox.show();
          msgBox = null;
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg17.Text"));
      msgBox.show();
      msgBox = null;
    }

  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    /*
        DataCat  enf;
        DataEnfCenti  enfCenti;
        CLista param = null;
        CMessage msg;
        String strServlet = null;
        int modoServlet = 0;
        TextField txt = (TextField) e.getSource();
        //Centinelas
        if (app.getTSive().equals("C")) {
          // gestion de datos
         if ((txt.getName().equals("txtEnfer")) && (txtEnfer.getText().length() > 0)) {
            param = new CLista();
            param.setIdioma(app.getIdioma());
            param.addElement(new DataEnfCenti(txtEnfer.getText()));
            strServlet = strSERVLET_ENF_CENTI;
            modoServlet = servletOBTENER_X_CODIGO;
          }
        }
        // busca el item
        if (param != null) {
          try {
            // consulta en modo espera
            modoOperacion = modoESPERA;
            Inicializar();
            stubCliente.setUrl(new URL(app.getURL() + strServlet));
            param = (CLista) stubCliente.doPost(modoServlet, param);
            // rellena los datos
            if (param.size() > 0) {
              // realiza el cast y rellena los datos
              if (txt.getName().equals("txtEnfer")) {
                //Centinelas
                if (app.getTSive().equals("C")) {
                  enfCenti = (DataEnfCenti) param.firstElement();
                  txtEnfer.removeKeyListener(txtKeyAdapter);
                  txtEnfer.setText(enfCenti.getCod());
                  txtDesEnfer.setText(enfCenti.getDes());
                  txtEnfer.addKeyListener(txtKeyAdapter);
                }
                //Centinelas
                if (app.getTSive().equals("E")) {
                  enf = (DataCat) param.firstElement();
                  txtEnfer.removeKeyListener(txtKeyAdapter);
                  txtEnfer.setText(enf.getCod());
                  txtDesEnfer.setText(enf.getDes());
                  txtEnfer.addKeyListener(txtKeyAdapter);
                }
              }
            }
            // no hay datos
            else {
         msg = new CMessage(this.app, CMessage.msgAVISO, res.getString("msg18.Text"));
              msg.show();
              msg = null;
            }
          } catch(Exception ex) {
              msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
              msg.show();
              msg = null;
          }
          // consulta en modo normal
          modoOperacion = modoNORMAL;
          Inicializar();
        }
     */
  }

  //_________________________________________________________

} //clase

//_________________________________________________________

// action listener para los botones
class PanelConsVolCasosIndTub_actionListener
    implements ActionListener, Runnable {
  PanelConsVolCasosIndTub adaptee = null;
  ActionEvent e = null;

  public PanelConsVolCasosIndTub_actionListener(PanelConsVolCasosIndTub adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}
////////////////////// Clases para listas
