//Title:        Volcado de casos individuales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Volcado de casos individuales (incluidas preguntas del protocolo) de una enfermedad a fichero ASCII.

package tuberculosis.cliente.volcontactos;

import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CTabla;
import jclass.bwt.JCItemEvent;
import jclass.bwt.JCItemListener;
import tuberculosis.datos.volcontactos.DataInfoPreg;

public class DialogoInfoPreg
    extends CDialog {

  int numCol;
  ResourceBundle res = ResourceBundle.getBundle(
      "tuberculosis.cliente.volcontactos.Res" + app.getIdioma());
  String tamCol;
  String nomCol;
  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;
  // lista
  public CLista lista = null;
  public Vector listaPreg = new Vector();
  // modo de operaci�n
  public int modoOperacion = modoNORMAL;
  // filtro de b�squeda
  protected String sFiltro;
  // contenedor de imagenes
  protected CCargadorImagen imgs = null;
  // im�genes
  final String imgNAME[] = {
      "images/aceptar.gif"};

  // indica ela fila seleccionada en la tabla
  public int m_itemSelecc = -1;

  XYLayout xYLayout1 = new XYLayout();
  CheckboxGroup checkboxGroup1 = new CheckboxGroup();
//  public JCMultiColumnList tabla = new JCMultiColumnList();

  public CTabla tabla = new CTabla();

  //DialogoInfoPreg_tabla_actionAdapter tablaActionListener = new DialogoInfoPreg_tabla_actionAdapter(this);
  DialogoInfoPregItemListener multlstItemListener = new
      DialogoInfoPregItemListener(this);
  Label lblPreguntas = new Label();
  ButtonControl btnSalir = new ButtonControl();

  // constructor
  public DialogoInfoPreg(CApp a) {
    super(a);

    try {
      lista = new CLista();

      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicializa el aspecto del panel
  public void jbInit() throws Exception {

    this.setTitle(res.getString("msg1.Text"));
    xYLayout1.setWidth(730);
    xYLayout1.setHeight(310);
    this.setLayout(xYLayout1);

    this.setSize(new Dimension(700, 330));

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    lblPreguntas.setAlignment(1);
    lblPreguntas.setText(res.getString("lblPreguntas.Text"));
    btnSalir.setLabel(res.getString("btnSalir.Label"));
    btnSalir.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnSalir_actionPerformed(e);
      }
    });
    btnSalir.setImage(imgs.getImage(0));
    //btnSalir.addActionListener(btnNormalActionListener);

    this.add(lblPreguntas, new XYConstraints(10, 20, 680, -1));
    this.add(tabla, new XYConstraints(10, 50, 680, 200));
    this.add(btnSalir, new XYConstraints(600, 270, -1, -1));

    // configuraci�n de la tabla
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tabla.ColumnButtonsStrings")), '\n'));
    tabla.setColumnResizePolicy(jclass.bwt.BWTEnum.RESIZE_NONE);
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "200\n100\n100\n50\n230"), '\n'));
    tabla.setColumnLabelsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tabla2.ColumnButtonsStrings")), '\n'));
    tabla.setNumColumns(5);
    tabla.setAutoSelect(true);

    //tabla.addActionListener(tablaActionListener);
    tabla.addItemListener(multlstItemListener);

    Inicializar();
  }

  public void Inicializar() {
    switch (modoOperacion) {

      case modoNORMAL:

        // modo modificaci�n y baja
        btnSalir.setEnabled(true);
        tabla.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnSalir.setEnabled(false);
        tabla.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  // limpia la pantalla
  void vaciarPantalla() {
    if (tabla.countItems() > 0) {
      tabla.deleteItems(0, tabla.countItems() - 1);
    }
  }

  // doble click en la tabla
  // Se muestra informacion acerca de la pregunta
  //  modelo/FechAlta/FechBaja/Linea/Pregunta
  public void tabla_actionPerformed() {
  }

  void tabla_itemStateChanged(JCItemEvent evt) {

    m_itemSelecc = tabla.getSelectedIndex();

  }

  // pinta la tabla
  public void rellenarTabla(CLista lista) {
    //#System.Out.println("En rellenar tabla de info de la pregunta");
    DataInfoPreg dato;
    tabla.clear();
    for (int i = 0; i < lista.size(); i++) {
      dato = (DataInfoPreg) lista.elementAt(i);
      tabla.addItem(setLinea(dato), '&');
      //#System.Out.println("En rellenar tabla de info nueva fila");
    }
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(DataInfoPreg o) {
    return new String(o.desMod + '&' + o.fechaAlta + '&'
                      + o.fechaBaja + '&' + o.linea + '&'
                      + o.desPreg);
  }

  void btnSalir_actionPerformed(ActionEvent e) {
    this.dispose();
  }

} //fin clase

    /******************************************************************************/

//Tambi�n implementa Runnable por si usuario pulsa m�s..
class DialogoInfoPregItemListener
    implements JCItemListener, Runnable {
  DialogoInfoPreg adaptee = null;
  JCItemEvent e = null;

  public DialogoInfoPregItemListener(DialogoInfoPreg adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(JCItemEvent e) {
    if (e.getStateChange() == JCItemEvent.SELECTED) { //evento seleccion (no deseleccion)
      this.e = e;
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

//    if (((JCMultiColumnList)e.getSource()).getName() == "tabla")
    adaptee.tabla_itemStateChanged(e);
  }
}
