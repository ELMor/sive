/**
 * Clase: ConTubNotif
 * Paquete: tuberculosis.datos.diatubnotif
 * Hereda:
 * Autor: Emilio Postigo Riancho
 * Fecha Inicio: 30/11/1999
 * Descripcion: Clase destinada a almacenar las constantes
             específicas de este paquete
 */

package tuberculosis.datos.diatubnotif;

public class ConTubNotif {

  // Claves para hashtable
  public static final String NUEVO_PRIMERO = "NUEVO_PRIMERO";
  public static final String HAY_COBERTURA = "IT_COBERTURA";

  // Operador y fecha de actualización de equipos
  public static final String CD_OPE_EQUIPOS = "CD_OPE_E";
  public static final String FC_ULTACT_EQUIPOS = "FC_ULTACT_E";

  // Operador y fecha de actualización de semanas
  public static final String CD_OPE_SEMANAS = "CD_OPE_S";
  public static final String FC_ULTACT_SEMANAS = "FC_ULTACT_S";

  // Operador y fecha de actualización de NOTIFEDO
  public static final String CD_OPE_NOTIFEDO = "CD_OPE_N";
  public static final String FC_ULTACT_NOTIFEDO = "FC_ULTACT_N";

  // Operador y fecha de actualización de NOTIF_EDOI
  public static final String CD_OPE_NOTIF_EDOI = "CD_OPE_NE";
  public static final String FC_ULTACT_NOTIF_EDOI = "FC_ULTACT_NE";

  // Campos relativos a notificadores
  public static final String TEORICOS_EQUIPOS = "SEN_TEORICOS";
  public static final String TEORICOS_SEMANAS = "SNS_TEORICOS";
  public static final String TOTAL_SEMANAS = "SNS_TOTAL_REALES";
  public static final String REALES_NOTIFEDO = "SN_REALES";

}