/**
 * Clase: DatMntRegTubCS
 * Paquete: SP_Tuberculosis.datos.mantregtub
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 27/10/1999
 * Analisis Funcional: Punto 2. Mantenimiento Registro Tuberculosis.
 * Descripcion: Estructura de datos que contiene los datos necesarios
 *   para realizar una busqueda de registros de tuberculosis a traves
 *   del servlet SrvMntRegTub, para el panel PanMntRegTub.
 *   Implementa la estructura de comunicaci�n del cliente al servidor.
 */

package tuberculosis.datos.mantregtub;

import java.io.Serializable;

public class DatMntRegTubCS
    implements Serializable {

  // Datos de la busqueda
  private String CD_ANO = "";
  private String CD_REG = "";
  private String CD_CODENFERMO = "";
  private String FC_FECHADESDE = "";
  private String FC_FECHAHASTA = "";
  private String IT_FG_CERRADO = "";
  private String IT_FG_CONFIDENCIAL = "";

  public DatMntRegTubCS(String Ano,
                        String Reg,
                        String CodEnfermo,
                        String FDesde,
                        String FHasta,
                        String FCerrado,
                        String FConf) { // Constructor

    if (Ano != null) {
      CD_ANO = Ano;
    }
    else {
      CD_ANO = "";
    }
    if (Reg != null) {
      CD_REG = Reg;
    }
    else {
      CD_REG = "";
    }
    if (CodEnfermo != null) {
      CD_CODENFERMO = CodEnfermo;
    }
    else {
      CD_CODENFERMO = "";
    }
    if (FDesde != null) {
      FC_FECHADESDE = FDesde;
    }
    else {
      FC_FECHADESDE = "";
    }
    if (FHasta != null) {
      FC_FECHAHASTA = FHasta;
    }
    else {
      FC_FECHAHASTA = "";
    }
    if (FCerrado != null) {
      IT_FG_CERRADO = FCerrado;
    }
    else {
      IT_FG_CERRADO = "";
    }
    if (FConf != null) {
      IT_FG_CONFIDENCIAL = FConf;
    }
    else {
      IT_FG_CONFIDENCIAL = "";
    }
  }

  public String getCD_ANO() {
    return CD_ANO;
  }

  public String getCD_REG() {
    return CD_REG;
  }

  public String getCD_CODENFERMO() {
    return CD_CODENFERMO;
  }

  public String getFC_FECHADESDE() {
    return FC_FECHADESDE;
  }

  public String getFC_FECHAHASTA() {
    return FC_FECHAHASTA;
  }

  public String getIT_FG_CERRADO() {
    return IT_FG_CERRADO;
  }

  public String getIT_FG_CONFIDENCIAL() {
    return IT_FG_CONFIDENCIAL;
  }

} // endclass DatMntRegTubCS
