/** Clase: DatConCasTub
 * Paquete: tuberculosis.datos.mantcontub
 * Hereda:
 * Autor: Pedro Antonio D�az Pardo (PDP)
 * Fecha Inicio: 07/02/2000
 * Analisis Funcional: Mantenimiento Contactos de enfermos de Tuberculosis.
 * Descripcion: Estructura de datos que contiene los datos necesarios
 *   para realizar un alta, modificaci�n y baja de contactos de enfermos de tuberculosis a traves
 *   del servlet SrvConCasTub, para el panel di�logo DiaConCasTub.
 *   Implementa la estructura de comunicaci�n del cliente al servidor.
 */package tuberculosis.datos.mantcontub;

import java.sql.Date;
import java.sql.Timestamp;

import comun.DatoBase;

public class DatConCasTub
    extends DatoBase {

  // Claves para hashtable
  private static final String key_DS_NIVEL_1 = "key_DS_NIVEL_1";
  private static final String key_DS_NIVEL_2 = "key_DS_NIVEL_2";
  private static final String key_DS_ZBS = "key_DS_ZBS";

  private static final String key_DS_MUNICIPIO = "key_DS_MUNICIPIO";
  private static final String key_CD_CA = "key_CD_CA";
  private static final String key_MODO_SUCA = "key_MODO_SUCA";

  private static final String key_CD_ANO_CC = "key_CD_ANO_CC";
  private static final String key_CD_REG_CC = "key_CD_REG_CC";
  private static final String key_CD_ENFERMO_CC = "key_CD_ENFERMO_CC";

  private static final String key_CD_ANO_RTBC = "key_CD_ANO_RTBC";
  private static final String key_CD_REG_RTBC = "key_CD_REG_RTBC";
  private static final String key_CD_ENFERMO = "key_CD_ENFERMO";

  private static final String key_CD_OPE_RTBC = "key_CD_OPE_RTBC";
  private static final String key_FC_ULTACT_RTBC = "key_FC_ULTACT_RTBC";

  //private static final  String key_CD_E_NOTIF = "key_CD_E_NOTIF";
  private static final String key_CD_TDOC = "key_CD_TDOC";
  private static final String key_DS_APE1 = "key_DS_APE1";
  private static final String key_DS_APE2 = "key_DS_APE2";
  private static final String key_SIGLAS = "key_SIGLAS";
  private static final String key_DS_NOMBRE = "key_DS_NOMBRE";
  private static final String key_DS_FONOAPE1 = "key_DS_FONOAPE1";
  private static final String key_DS_FONOAPE2 = "key_DS_FONOAPE2";
  private static final String key_DS_FONONOMBRE = "key_DS_FONONOMBRE";
  private static final String key_IT_CALC = "key_IT_CALC";
  private static final String key_FC_NAC = "key_FC_NAC";
  private static final String key_CD_SEXO = "key_CD_SEXO";
  private static final String key_CD_POSTAL = "key_CD_POSTAL";
  private static final String key_DS_DIREC = "key_DS_DIREC";
  private static final String key_DS_NUM = "key_DS_NUM";
  private static final String key_DS_PISO = "key_DS_PISO";
  private static final String key_DS_TELEF = "key_DS_TELEF";
  private static final String key_CD_NIVEL_1 = "key_CD_NIVEL_1";
  private static final String key_CD_NIVEL_2 = "key_CD_NIVEL_2";
  private static final String key_CD_ZBS = "key_CD_ZBS";
  private static final String key_CD_OPE = "key_CD_OPE";
  private static final String key_FC_ULTACT = "key_FC_ULTACT";
  private static final String key_IT_REVISADO = "key_IT_REVISADO";
  private static final String key_CD_MOTBAJA = "key_CD_MOTBAJA";
  private static final String key_FC_BAJA = "key_FC_BAJA";
  private static final String key_DS_OBSERV = "key_DS_OBSERV";
  private static final String key_CD_PROV2 = "key_CD_PROV2";
  private static final String key_CD_MUNI2 = "key_CD_MUNI2";
  private static final String key_CD_POST2 = "key_CD_POST2";
  private static final String key_DS_DIREC2 = "key_DS_DIREC2";
  private static final String key_DS_NUM2 = "key_DS_NUM2";
  private static final String key_DS_PISO2 = "key_DS_PISO2";
  private static final String key_DS_OBSERV2 = "key_DS_OBSERV2";
  private static final String key_CD_PROV3 = "key_CD_PROV3";
  private static final String key_CD_MUNI3 = "key_CD_MUNI3";
  private static final String key_CD_POST3 = "key_CD_POST3";
  private static final String key_DS_DIREC3 = "key_DS_DIREC3";
  private static final String key_DS_NUM3 = "key_DS_NUM3";
  private static final String key_DS_PISO3 = "key_DS_PISO3";
  private static final String key_DS_TELEF3 = "key_DS_TELEF3";
  private static final String key_DS_OBSERV3 = "key_DS_OBSERV3";
  private static final String key_DS_NDOC = "key_DS_NDOC";
  private static final String key_DS_TELEF2 = "key_DS_TELEF2";
  private static final String key_CDVIAL = "key_CDVIAL";
  private static final String key_CDTVIA = "key_CDTVIA";
  private static final String key_CDTNUM = "key_CDTNUM";
  private static final String key_DSCALNUM = "key_DSCALNUM";
  private static final String key_CD_PROV = "key_CD_PROV";
  private static final String key_CD_MUN = "key_CD_MUN";
  private static final String key_IT_ENFERMO = "key_IT_ENFERMO";
  private static final String key_CD_PAIS = "key_CD_PAIS";
  private static final String key_IT_CONTACTO = "key_IT_CONTACTO";

  /** constructor por defecto */
  public DatConCasTub() {
    super();
  }

  /*
     public DatConCasTub( String Ano,
                       String Reg,
                       String CEnfermo,
                       //String CNotif,
                       String CDoc,
                       String Ape1,
                       String Ape2,
                       String Siglas,
                       String Nombre,
                       String FonoApe1,
                       String FonoApe2,
                       String FonoNom,
                       String ICalc,
                       java.sql.Date FNac,
                       String Sexo,
                       String CPostal,
                       String Direcc,
                       String Num,
                       String Piso,
                       String Telef,
                       String CNiv1,
                       String CNiv2,
                       String CZbs,
                       String Cope,
                       java.sql.Timestamp FUlt,
                       String IRev,
                       String CMotBaj,
                       java.sql.Date FBaj,
                       String Obsv,
                       String Prov2,
                       String Mun2,
                       String Post2,
                       String Direc2,
                       String Num2,
                       String Piso2,
                       String Obsv2,
                       String Prov3,
                       String Mun3,
                       String Post3,
                       String Direc3,
                       String Num3,
                       String Piso3,
                       String Telef3,
                       String Obsv3,
                       String NDoc,
                       String Telef2,
                       String CVial,
                       String CTvia,
                       String CTnum,
                       String Calnum,
                       String CProv,
                       String CMun,
                       String IEnfermo,
                       String CPais,
                       String IContacto)
                                         { // Constructor
   super();
   setCD_ANO(Ano);
   setCD_REG(Reg);
   setCD_ENFERMO(CEnfermo);
   //setCD_E_NOTIF(CNotif);
   setCD_TDOC(CDoc);
   setDS_APE1(Ape1);
   setDS_APE2(Ape2);
   setSIGLAS(Siglas);
   setDS_NOMBRE(Nombre);
   setDS_FONOAPE1(FonoApe1);
   setDS_FONOAPE2(FonoApe2);
   setDS_FONONOMBRE(FonoNom);
   setIT_CALC(ICalc);
   setFC_NAC(FNac);
   setCD_SEXO(Sexo);
   setCD_POSTAL(CPostal);
   setDS_DIREC(Direcc);
   setDS_NUM(Num);
   setDS_PISO(Piso);
   setDS_TELEF(Telef);
   setCD_NIVEL_1(CNiv1);
   setCD_NIVEL_2(CNiv2);
   setCD_ZBS(CZbs);
   setCD_OPE(Cope);
   setFC_ULTACT(FUlt);
   setIT_REVISADO(IRev);
   setCD_MOTBAJA(CMotBaj);
   setFC_BAJA(FBaj);
   setDS_OBSERV(Obsv);
   setCD_PROV2(Prov2);
   setCD_MUNI2(Mun2);
   setCD_POST2(Post2);
   setDS_DIREC2(Direc2);
   setDS_NUM2(Num2);
   setDS_PISO2(Piso2);
   setDS_OBSERV2(Obsv2);
   setCD_PROV3(Prov3);
   setCD_MUNI3(Mun3);
   setCD_POST3(Post3);
   setDS_DIREC3(Direc3);
   setDS_NUM3(Num3);
   setDS_PISO3(Piso3);
   setDS_OBSERV3(Obsv3);
   setDS_TELEF3(Telef3);
   setDS_OBSERV3(Obsv3);
   setDS_NDOC(NDoc);
   setDS_TELEF2(Telef2);
   setCDVIAL(CVial);
   setCDTVIA(CTvia);
   setCDTNUM(CTnum);
   setDSCALNUM(Calnum);
   setCD_PROV(CProv);
   setCD_MUN(CMun);
   setIT_ENFERMO(IEnfermo);
   setCD_PAIS(CPais);
   setIT_CONTACTO(IContacto);
   }*/

  // Get para la descripci�n del nivel 1
  public String getDS_NIVEL_1() {
    return (String) saca(key_DS_NIVEL_1);
  }

  // Get para la descripci�n del nivel 2
  public String getDS_NIVEL_2() {
    return (String) saca(key_DS_NIVEL_2);
  }

  // Get para la descripci�n de la zonificaci�n
  public String getDS_ZBS() {
    return (String) saca(key_DS_ZBS);
  }

  // Get para el municipio del enfermo
  public String getDS_MUN() {
    return (String) saca(key_DS_MUNICIPIO);
  }

  // Get para la comunidad del enfermo
  public String getCD_CA() {
    return (String) saca(key_CD_CA);
  }

  // Get para suca/no suca
  public Integer getMODO_SUCA() {
    return (Integer) saca(key_MODO_SUCA);
  }

  // Get's relativos a SIVE_REGISTROTBC
  public String getCD_ANO_RTBC() {
    return (String) saca(key_CD_ANO_RTBC);
  }; // A�o
  public String getCD_REG_RTBC() {
    return (String) saca(key_CD_REG_RTBC);
  }; //
  public String getCD_OPE_RTBC() {
    return (String) saca(key_CD_OPE_RTBC);
  };
  public Timestamp getFC_ULTACT_RTBC() {
    return (Timestamp) sacaTimestamp(key_FC_ULTACT_RTBC);
  };

  // Get's relativos a SIVE_CASOCONTAC
  public String getCD_ANO_CC() {
    return (String) saca(key_CD_ANO_CC);
  }; // A�o
  public String getCD_REG_CC() {
    return (String) saca(key_CD_REG_CC);
  }; //
  public Integer getCD_ENFERMO_CC() {
    return (Integer) saca(key_CD_ENFERMO_CC);
  };
// public String getCD_E_NOTIF() {return (String) saca(key_CD_E_NOTIF);}; //

  // Get's relativos a SIVE_ENFERMO
  public int getCD_ENFERMO() {
    return ( (Integer) saca(key_CD_ENFERMO)).intValue();
  };
  public String getCD_TDOC() {
    return (String) saca(key_CD_TDOC);
  };
  public String getDS_APE1() {
    return (String) saca(key_DS_APE1);
  };
  public String getDS_APE2() {
    return (String) saca(key_DS_APE2);
  };
  public String getSIGLAS() {
    return (String) saca(key_SIGLAS);
  };
  public String getDS_NOMBRE() {
    return (String) saca(key_DS_NOMBRE);
  };
  public String getDS_FONOAPE1() {
    return (String) saca(key_DS_FONOAPE1);
  };
  public String getDS_FONOAPE2() {
    return (String) saca(key_DS_FONOAPE2);
  };
  public String getDS_FONONOMBRE() {
    return (String) saca(key_DS_FONONOMBRE);
  };
  public String getIT_CALC() {
    return (String) saca(key_IT_CALC);
  };
  public java.sql.Date getFC_NAC() {
    return (Date) sacaDate(key_FC_NAC);
  };
  public String getCD_SEXO() {
    return (String) saca(key_CD_SEXO);
  };
  public String getCD_POSTAL() {
    return (String) saca(key_CD_POSTAL);
  };
  public String getDS_DIREC() {
    return (String) saca(key_DS_DIREC);
  };
  public String getDS_NUM() {
    return (String) saca(key_DS_NUM);
  };
  public String getDS_PISO() {
    return (String) saca(key_DS_PISO);
  };
  public String getDS_TELEF() {
    return (String) saca(key_DS_TELEF);
  };
  public String getCD_NIVEL_1() {
    return (String) saca(key_CD_NIVEL_1);
  };
  public String getCD_NIVEL_2() {
    return (String) saca(key_CD_NIVEL_2);
  };
  public String getCD_ZBS() {
    return (String) saca(key_CD_ZBS);
  };
  public String getCD_OPE() {
    return (String) saca(key_CD_OPE);
  };
  public java.sql.Timestamp getFC_ULTACT() {
    return (Timestamp) sacaTimestamp(key_FC_ULTACT);
  };
  public String getIT_REVISADO() {
    return (String) saca(key_IT_REVISADO);
  };
  public String getCD_MOTBAJA() {
    return (String) saca(key_CD_MOTBAJA);
  };
  public java.sql.Date getFC_BAJA() {
    return (Date) sacaDate(key_FC_BAJA);
  };
  public String getDS_OBSERV() {
    return (String) saca(key_DS_OBSERV);
  };
  public String getCD_PROV2() {
    return (String) saca(key_CD_PROV2);
  };
  public String getCD_MUNI2() {
    return (String) saca(key_CD_MUNI2);
  };
  public String getCD_POST2() {
    return (String) saca(key_CD_POST2);
  };
  public String getDS_DIREC2() {
    return (String) saca(key_DS_DIREC2);
  };
  public String getDS_NUM2() {
    return (String) saca(key_DS_NUM2);
  };
  public String getDS_PISO2() {
    return (String) saca(key_DS_PISO2);
  };
  public String getDS_OBSERV2() {
    return (String) saca(key_DS_OBSERV2);
  };
  public String getCD_PROV3() {
    return (String) saca(key_CD_PROV3);
  };
  public String getCD_MUNI3() {
    return (String) saca(key_CD_MUNI3);
  };
  public String getCD_POST3() {
    return (String) saca(key_CD_POST3);
  };
  public String getDS_DIREC3() {
    return (String) saca(key_DS_DIREC3);
  };
  public String getDS_NUM3() {
    return (String) saca(key_DS_NUM3);
  };
  public String getDS_PISO3() {
    return (String) saca(key_DS_PISO3);
  };
  public String getDS_TELEF3() {
    return (String) saca(key_DS_TELEF3);
  };
  public String getDS_OBSERV3() {
    return (String) saca(key_DS_OBSERV3);
  };
  public String getDS_NDOC() {
    return (String) saca(key_DS_NDOC);
  };
  public String getDS_TELEF2() {
    return (String) saca(key_DS_TELEF2);
  };
  public String getCDVIAL() {
    return (String) saca(key_CDVIAL);
  };
  public String getCDTVIA() {
    return (String) saca(key_CDTVIA);
  };
  public String getCDTNUM() {
    return (String) saca(key_CDTNUM);
  };
  public String getDSCALNUM() {
    return (String) saca(key_DSCALNUM);
  };
  public String getCD_PROV() {
    return (String) saca(key_CD_PROV);
  };
  public String getCD_MUN() {
    return (String) saca(key_CD_MUN);
  };
  public String getIT_ENFERMO() {
    return (String) saca(key_IT_ENFERMO);
  };
  public String getCD_PAIS() {
    return (String) saca(key_CD_PAIS);
  };
  public String getIT_CONTACTO() {
    return (String) saca(key_IT_CONTACTO);
  };

  // Set's

  // Set para la descripci�n del nivel 1
  public void setDS_NIVEL_1(String s) {
    introduce(key_DS_NIVEL_1, s);
  }

  // Set para la descripci�n del nivel 2
  public void setDS_NIVEL_2(String s) {
    introduce(key_DS_NIVEL_2, s);
  }

  // Set para la zonificaci�n
  public void setDS_ZBS(String s) {
    introduce(key_DS_ZBS, s);
  }

  // Set para el municipio del enfermo
  public void setDS_MUN(String s) {
    introduce(key_DS_MUNICIPIO, s);
  }

  // Set para la comunidad del enfermo
  public void setCD_CA(String s) {
    introduce(key_CD_CA, s);
  }

  // Set para suca/no suca
  public void setMODO_SUCA(Integer i) {
    introduce(key_MODO_SUCA, i);
  }

  // Set's relativos a SIVE_REGISTROTBC
  public void setCD_ANO_RTBC(String s) {
    introduce(key_CD_ANO_RTBC, s);
  }; // A�o
  public void setCD_REG_RTBC(String s) {
    introduce(key_CD_REG_RTBC, s);
  }; //
  public void setCD_OPE_RTBC(String s) {
    introduce(key_CD_OPE_RTBC, s);
  };
  public void setFC_ULTACT_RTBC(Timestamp t) {
    introduce(key_FC_ULTACT_RTBC, t);
  };

  // Set's relativos a SIVE_CASOCONTAC
  public void setCD_ANO_CC(String Ano) {
    introduce(key_CD_ANO_CC, Ano);
  }; // A�o
  public void setCD_REG_CC(String Reg) {
    introduce(key_CD_REG_CC, Reg);
  }; // Secuenciador
  public void setCD_ENFERMO_CC(Integer iEnfermo) {
    introduce(key_CD_ENFERMO_CC, iEnfermo);
  };
//  public void setCD_E_NOTIF(String CNotif) {introduce(key_CD_E_NOTIF, CNotif);};
//  Set's relativos a SIVE_ENFERMO

  public void setCD_ENFERMO(int i) {
    introduce(key_CD_ENFERMO, new Integer(i));
  };
  public void setCD_TDOC(String CDoc) {
    introduce(key_CD_TDOC, CDoc);
  };
  public void setDS_APE1(String Ape1) {
    introduce(key_DS_APE1, Ape1);
  };
  public void setDS_APE2(String Ape2) {
    introduce(key_DS_APE2, Ape2);
  };
  public void setSIGLAS(String Siglas) {
    introduce(key_SIGLAS, Siglas);
  };
  public void setDS_NOMBRE(String Nombre) {
    introduce(key_DS_NOMBRE, Nombre);
  };
  public void setDS_FONOAPE1(String FonoApe1) {
    introduce(key_DS_FONOAPE1, FonoApe1);
  };
  public void setDS_FONOAPE2(String FonoApe2) {
    introduce(key_DS_FONOAPE2, FonoApe2);
  };
  public void setDS_FONONOMBRE(String FonoNom) {
    introduce(key_DS_FONONOMBRE, FonoNom);
  };
  public void setIT_CALC(String ICalc) {
    introduce(key_IT_CALC, ICalc);
  };
  public void setFC_NAC(java.sql.Date FNac) {
    introduce(key_FC_NAC, FNac);
  };
  public void setCD_SEXO(String Sexo) {
    introduce(key_CD_SEXO, Sexo);
  };
  public void setCD_POSTAL(String CPostal) {
    introduce(key_CD_POSTAL, CPostal);
  };
  public void setDS_DIREC(String Direcc) {
    introduce(key_DS_DIREC, Direcc);
  };
  public void setDS_NUM(String Num) {
    introduce(key_DS_NUM, Num);
  };
  public void setDS_PISO(String Piso) {
    introduce(key_DS_PISO, Piso);
  };
  public void setDS_TELEF(String Telef) {
    introduce(key_DS_TELEF, Telef);
  };
  public void setCD_NIVEL_1(String CNiv1) {
    introduce(key_CD_NIVEL_1, CNiv1);
  };
  public void setCD_NIVEL_2(String CNiv2) {
    introduce(key_CD_NIVEL_2, CNiv2);
  };
  public void setCD_ZBS(String CZbs) {
    introduce(key_CD_ZBS, CZbs);
  };
  public void setCD_OPE(String Cope) {
    introduce(key_CD_OPE, Cope);
  };
  public void setFC_ULTACT(java.sql.Timestamp FUlt) {
    introduce(key_FC_ULTACT, FUlt);
  };
  public void setIT_REVISADO(String IRev) {
    introduce(key_IT_REVISADO, IRev);
  };
  public void setCD_MOTBAJA(String CMotBaj) {
    introduce(key_CD_MOTBAJA, CMotBaj);
  };
  public void setFC_BAJA(java.sql.Date FBaj) {
    introduce(key_FC_BAJA, FBaj);
  };
  public void setDS_OBSERV(String Obsv) {
    introduce(key_DS_OBSERV, Obsv);
  };
  public void setCD_PROV2(String Prov2) {
    introduce(key_CD_PROV2, Prov2);
  };
  public void setCD_MUNI2(String Mun2) {
    introduce(key_CD_MUNI2, Mun2);
  };
  public void setCD_POST2(String Post2) {
    introduce(key_CD_POST2, Post2);
  };
  public void setDS_DIREC2(String Direc2) {
    introduce(key_DS_DIREC2, Direc2);
  };
  public void setDS_NUM2(String Num2) {
    introduce(key_DS_NUM2, Num2);
  };
  public void setDS_PISO2(String Piso2) {
    introduce(key_DS_PISO2, Piso2);
  };
  public void setDS_OBSERV2(String Obsv2) {
    introduce(key_DS_OBSERV2, Obsv2);
  };
  public void setCD_PROV3(String Prov3) {
    introduce(key_CD_PROV3, Prov3);
  };
  public void setCD_MUNI3(String Mun3) {
    introduce(key_CD_MUNI3, Mun3);
  };
  public void setCD_POST3(String Post3) {
    introduce(key_CD_POST3, Post3);
  };
  public void setDS_DIREC3(String Direc3) {
    introduce(key_DS_DIREC3, Direc3);
  };
  public void setDS_NUM3(String Num3) {
    introduce(key_DS_NUM3, Num3);
  };
  public void setDS_PISO3(String Piso3) {
    introduce(key_DS_PISO3, Piso3);
  };
  public void setDS_TELEF3(String Telef3) {
    introduce(key_DS_TELEF3, Telef3);
  };
  public void setDS_OBSERV3(String Obsv3) {
    introduce(key_DS_OBSERV3, Obsv3);
  };
  public void setDS_NDOC(String NDoc) {
    introduce(key_DS_NDOC, NDoc);
  };
  public void setDS_TELEF2(String Telef2) {
    introduce(key_DS_TELEF2, Telef2);
  };
  public void setCDVIAL(String CVial) {
    introduce(key_CDVIAL, CVial);
  };
  public void setCDTVIA(String CTvia) {
    introduce(key_CDTVIA, CTvia);
  };
  public void setCDTNUM(String CTnum) {
    introduce(key_CDTNUM, CTnum);
  };
  public void setDSCALNUM(String Calnum) {
    introduce(key_DSCALNUM, Calnum);
  };
  public void setCD_PROV(String CProv) {
    introduce(key_CD_PROV, CProv);
  };
  public void setCD_MUN(String CMun) {
    introduce(key_CD_MUN, CMun);
  };
  public void setIT_ENFERMO(String IEnfermo) {
    introduce(key_IT_ENFERMO, IEnfermo);
  };
  public void setCD_PAIS(String CPais) {
    introduce(key_CD_PAIS, CPais);
  };
  public void setIT_CONTACTO(String IContacto) {
    introduce(key_IT_CONTACTO, IContacto);
  };

} //FIN DE LA CLASE
