// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   DataVolCasInd.java

package tuberculosis.datos.volcontactos;

import java.io.Serializable;

public class DataVolCasInd
    implements Serializable {

  public String anoD;
  public String semD;
  public String anoH;
  public String semH;
  public String enfer;
  public String area;
  public String dist;
  public String zbs;
  public String prov;
  public String mun;
  public String sep;
  public String codsPreg;
  public String tSive;

  public DataVolCasInd() {
    anoD = new String();
    semD = new String();
    anoH = new String();
    semH = new String();
    enfer = new String();
    area = new String();
    dist = new String();
    zbs = new String();
    prov = new String();
    mun = new String();
    sep = "$";
    codsPreg = new String();
    tSive = new String();
  }

  public DataVolCasInd(String aD, String sD, String aH, String sH, String e) {
    anoD = new String();
    semD = new String();
    anoH = new String();
    semH = new String();
    enfer = new String();
    area = new String();
    dist = new String();
    zbs = new String();
    prov = new String();
    mun = new String();
    sep = "$";
    codsPreg = new String();
    tSive = new String();
    anoD = aD;
    semD = sD;
    anoH = aH;
    semH = sH;
    enfer = e;
  }
}
