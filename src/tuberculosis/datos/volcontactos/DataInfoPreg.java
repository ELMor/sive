// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   DataInfoPreg.java

package tuberculosis.datos.volcontactos;

import java.io.Serializable;

public class DataInfoPreg
    implements Serializable {

  public String codMod;
  public String desMod;
  public String fechaAlta;
  public String fechaBaja;
  public String linea;
  public String codPreg;
  public String desPreg;
  public String tSive;

  public DataInfoPreg() {
    codMod = new String();
    desMod = new String();
    fechaAlta = new String();
    fechaBaja = new String();
    linea = new String();
    codPreg = new String();
    desPreg = new String();
    tSive = new String();
  }

  public DataInfoPreg(String cMod, String dMod, String fA, String fB, String l,
                      String cPreg, String dPreg,
                      String tS) {
    codMod = new String();
    desMod = new String();
    fechaAlta = new String();
    fechaBaja = new String();
    linea = new String();
    codPreg = new String();
    desPreg = new String();
    tSive = new String();
    codMod = cMod;
    desMod = dMod;
    fechaAlta = fA;
    fechaBaja = fB;
    linea = l;
    codPreg = cPreg;
    desPreg = dPreg;
    tSive = tS;
  }
}
