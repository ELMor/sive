// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   DataCasoTub.java

package tuberculosis.datos.volcontactos;

import java.io.Serializable;
import java.util.Date;

public class DataCasoTub
    implements Serializable {

  int iNumEdo;
  String sCodEnfermo;
  String sCodSex;
  Date dFecNac;
  String sCodArt;
  String sCodNrt;
  Date dFecIniRtbc;
  int iNumCon;

  public DataCasoTub(int numEdo, String codEnfermo) {
    sCodEnfermo = "";
    sCodSex = "";
    dFecNac = null;
    sCodArt = "";
    sCodNrt = "";
    dFecIniRtbc = null;
    iNumCon = 0;
    iNumEdo = numEdo;
    sCodEnfermo = codEnfermo;
  }

  public void setDatosTablaEnfermo(String codSex, Date fecNac) {
    sCodSex = codSex;
    dFecNac = fecNac;
  }

  public void setDatosTablaRegistrotbc(String codArt, String codNrt,
                                       Date fecIniRtbc) {
    sCodArt = codArt;
    sCodNrt = codNrt;
    dFecIniRtbc = fecIniRtbc;
  }

  public void setDatosTablaCasocontac(int numCon) {
    iNumCon = numCon;
  }

  public int getNumEdo() {
    return iNumEdo;
  }

  public String getCodEnfermo() {
    return sCodEnfermo;
  }

  public String getCodSex() {
    return sCodSex;
  }

  public Date getFecNac() {
    return dFecNac;
  }

  public String getCodArt() {
    return sCodArt;
  }

  public String getCodNrt() {
    return sCodNrt;
  }

  public Date getFecIniRtbc() {
    return dFecIniRtbc;
  }

  public int getNumCon() {
    return iNumCon;
  }
}
