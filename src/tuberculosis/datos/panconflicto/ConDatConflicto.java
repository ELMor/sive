// Clase empleada para almacenar las constantes
// específicas de los paquetes panconflicto y diaconflicto

package tuberculosis.datos.panconflicto;

public class ConDatConflicto {

  // Claves para hashtable

  // Mediante ella se pasa un objeto Vector
  public static final String NOTIFICADORES_CONFLICTO =
      "NOTIFICADORES_CONFLICTO";

  // Mediante ella se pasa un objeto String
  public static final String PREGUNTA_CONFLICTO = "PREGUNTA_CONFLICTO";

  // Mediante ella se pasa un objeto DatConflicto
  public static final String DATOS_CONFLICTO = "DATOS_CONFLICTO";
}
