package tuberculosis.datos.notiftub;

import java.util.Enumeration;
import java.util.Hashtable;

public abstract class DatTub
    extends Hashtable {

  /** constructor */
  public DatTub() {
    super();
  }

  /** constructor a partir de Hashtable */
  public DatTub(Hashtable h) {
    super();
    String sKey = null;
    for (Enumeration e = h.keys(); e.hasMoreElements(); ) {
      sKey = (String) e.nextElement();
      this.put(sKey, h.get(sKey));
    }
  }

  //Si es un nulo, no har� NADA, y al hacer el get tendr� null
  //Si no es nulo, har� un put y al hacer el get obtendr� el dato
  public synchronized void insert(Object key, Object value) {
    if ( (key != null) && (value != null)) {
      put(key, value);
    }
  }

  // Este m�todo decide si el objeto DatTub dtParametro est� incluido
  // en la instancia actual, es decir, si para todo par (key, valor)
  // de dtParametro existe el correspondiente par (key, valor) en la
  // misma.

  public boolean contiene(DatTub dtParametro) {
    boolean bValor = true;
    String sClave = null;
    String sValor = null;

    if (dtParametro != null) {
      for (Enumeration e = dtParametro.keys(); e.hasMoreElements() && bValor; ) {
        sClave = (String) e.nextElement();
        bValor = this.containsKey(sClave);
        if (bValor) {
          sValor = (String) dtParametro.get(sClave);
          bValor = ( (String)this.get(sClave)).equals(sValor);
        }
      }
    }
    return bValor;
  }

  // M�todos de asignaci�n de valores
  protected void introduce(String sClave, String sValor) {
    String sTemp = null;
    if (sValor != null) {
      sTemp = sValor.trim();
      put(sClave, sTemp);
    }
  }

  protected void introduceInteger(String sClave, String sValor) {
    Integer iTemp = new Integer("0");
    if (sValor != null) {
      iTemp = new Integer(sValor);
    }
    put(sClave, iTemp);
  }

  // Para recuperar valores
  protected String saca(String s) {
    Object o = null;
    String r = "";

    try {
      o = get(s);
      if (o != null) {
        r = o.toString();
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }

    return r;
  }

}
