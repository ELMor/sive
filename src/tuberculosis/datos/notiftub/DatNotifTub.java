package tuberculosis.datos.notiftub;

import java.util.Hashtable;
import java.util.Vector;

public class DatNotifTub
    extends DatTub {

  /** constructor */
  public DatNotifTub() {
    super();
  }

  /** constructor a partir de Hashtable */
  public DatNotifTub(Hashtable h) {
    super(h);
  }

  //Si es un nulo, no hara NADA, y al hacer el get tendre null
  //Si no es nulo, hara un put y al hacer el get obtendre el dato
  public synchronized void insert(Object key, Object value) {
    if ( (key != null) && (value != null)) {
      put(key, value);
    }
  }

  // Este m�todo decide si la clave del objeto DatNotifTub dtParametro es
  // la misma que la de la instancia actual.
  // Se toman como campos clave los de SIVE_NOTIFEDO

  public boolean contieneClave(DatNotifTub dtParametro) {
    boolean bValor = true;

    if (dtParametro != null) {
      bValor = (this.getCD_E_NOTIF_EDOI().equals(dtParametro.getCD_E_NOTIF_EDOI()));
      bValor = (bValor &&
                this.getCD_ANOEPI_EDOI().equals(dtParametro.getCD_ANOEPI_EDOI()));
      bValor = (bValor && this.getCD_SEMEPI().equals(dtParametro.getCD_SEMEPI()));
      bValor = (bValor && this.getFC_NOTIF().equals(dtParametro.getFC_NOTIF()));
      bValor = (bValor &&
                this.getFC_RECEP_EDOI().equals(dtParametro.getFC_RECEP_EDOI()));
    }

    return bValor;
  }

  // Para bloqueos en SIVE_EDOIND
  public String getCD_OPE_EDOIND() {
    return (String) get("CD_OPE_EDOIND");
  }

  public String getFC_ULTACT_EDOIND() {
    return (String) get("FC_ULTACT_EDOIND");
  }

  public void setCD_OPE_EDOIND(String s) {
    put("CD_OPE_EDOIND", s);
  }

  public void setFC_ULTACT_EDOIND(String s) {
    put("FC_ULTACT_EDOIND", s);
  }

  // Relativo a SIVE_NOTIF_EDOI

  public String getCD_E_NOTIF_EDOI() {
    return (String) get("CD_E_NOTIF_EDOI");
  };
  public String getCD_ANOEPI_EDOI() {
    return (String) get("CD_ANOEPI_EDOI");
  };
  public String getCD_SEMEPI() {
    return (String) get("CD_SEMEPI");
  };

  // E 27/01/2000
  public String getNM_EDO() {
    String s = null;
    try {
      s = (String) get("NM_EDO_EDOI");
    }
    catch (Exception e) {
      s = String.valueOf( (Integer) get("NM_EDO_EDOI"));
    }

    if (s.equals("")) {
      s = "0";
    }
    return s;
  }

  public String getFC_NOTIF() {
    return (String) get("FC_FECNOTIF");
  };
  public String getFC_RECEP_EDOI() {
    return (String) get("FC_RECEP_EDOI");
  };
  public String getCD_FUENTE() {
    return (String) get("CD_FUENTE");
  };
  public String getDS_DECLARANTE() {
    return (String) get("DS_DECLARANTE");
  };
  public String getIT_PRIMERO() {
    return (String) get("IT_PRIMERO");
  };
  public String getCD_OPE_EDOI() {
    return (String) get("CD_OPE_EDOI");
  };
  public String getFC_ULTACT_EDOI() {
    return (String) get("FC_ULTACT_EDOI");
  };
  public String getDS_HISTCLI() {
    return (String) get("DS_HISTCLI");
  };

  // Relativo a SIVE_NOTIFEDO

  public String getCD_E_NOTIF_EDO() {
    return (String) get("CD_E_NOTIF_EDO");
  };
  public String getCD_ANOEPI_EDO() {
    return (String) get("CD_ANOEPI_EDO");
  };
  public String getCD_SEMEPI_EDO() {
    return (String) get("CD_SEMEPI_EDO");
  };
  public String getFC_RECEP_EDO() {
    return (String) get("FC_RECEP_EDO");
  };
  public String getFC_NOTIF_EDO() {
    return (String) get("FC_NOTIF_EDO");
  };
  public String getNM_NNOTIFR() {
    return ( (Integer) get("NM_NNOTIFR")).toString();
  };
  public String getIT_RESSEM() {
    return (String) get("IT_RESSEM");
  };
  public String getCD_OPE_EDO() {
    return (String) get("CD_OPE_EDO");
  };
  public String getFC_ULTACT_EDO() {
    return (String) get("FC_ULTACT_EDO");
  };
  public String getIT_VALIDADA() {
    return (String) get("IT_VALIDADA");
  };
  public String getFC_FECVALID() {
    return (String) get("FC_FECVALID");
  };

  // Relativo a equipos

  public String getCD_E_NOTIF() {
    return (String) get("CD_E_NOTIF");
  };
  public String getDS_E_NOTIF() {
    return (String) get("DS_E_NOTIF");
  };
  public String getCD_ANOEPI_EQ() {
    return (String) get("CD_ANOEPI_EQ");
  };
  public String getCD_SEMEPI_EQ() {
    return (String) get("CD_SEMEPI_EQ");
  };
  public String getCD_CENTRO_EQ() {
    return (String) get("CD_CENTRO_EQ");
  };
  public String getCD_NIVEL_1() {
    return (String) get("CD_NIVEL_1");
  };
  public String getCD_NIVEL_2() {
    return (String) get("CD_NIVEL_2");
  };
  public String getCD_ZBS() {
    return (String) get("CD_ZBS");
  };
  public String getDS_RESPEQUIP() {
    return (String) get("DS_RESPEQUIP");
  };
  public String getDS_TELEF() {
    return (String) get("DS_TELEF");
  };
  public String getDS_FAX() {
    return (String) get("DS_FAX");
  };

  // E 17/01/2000 public String getNM_NOTIFT_EQ() {return (String) get("NM_NOTIFT");};
  public String getNM_NOTIFT_EQ() {
    return get("NM_NOTIFT") == null ? null :
        String.valueOf( (Integer) get("NM_NOTIFT"));
  };

  public String getFC_ALTA() {
    return (String) get("FC_ALTA");
  };
  public String getCD_OPE_EQ() {
    return (String) get("CD_OPE_EQ");
  };
  public String getFC_ULTACT_EQ() {
    return (String) get("FC_ULTACT_EQ");
  };
  public String getIT_BAJA() {
    return (String) get("IT_BAJA");
  };

  // Relativo a centros

  public String getCD_CENTRO() {
    return (String) get("CD_CENTRO");
  };
  public String getCD_PROV() {
    return (String) get("CD_PROV");
  };
  public String getCD_MUN() {
    return (String) get("CD_MUN");
  };
  public String getDS_CENTRO() {
    return (String) get("DS_CENTRO");
  };
  public String getDS_DIREC() {
    return (String) get("DS_DIREC");
  };
  public String getDS_NUM() {
    return (String) get("DS_NUM");
  };
  public String getDS_PISO() {
    return (String) get("DS_PISO");
  };
  public String getCD_POSTAL() {
    return (String) get("CD_POSTAL");
  };
  public String getDS_TELEF_CE() {
    return (String) get("DS_TELEF_CE");
  };
  public String getDS_FAX_CE() {
    return (String) get("DS_FAX_CE");
  };
  public String getCD_NIVASIS() {
    return (String) get("CD_NIVASIS");
  };
  public String getIT_COBERTURA() {
    return (String) get("IT_COBERTURA");
  };
  public String getFC_ALTA_CE() {
    return (String) get("FC_ALTA_CE");
  };
  public String getCD_OPE_CE() {
    return (String) get("CD_OPE_CE");
  };
  public String getFC_ULTACT_CE() {
    return (String) get("FC_ULTACT_CE");
  };
  public String getIT_BAJA_CE() {
    return (String) get("IT_BAJA_CE");
  };

  // Relativo a SIVE_NOTIF_SEM
  //public String getNM_NNOTIFT_SEM() {return String.valueOf(((Integer) get("NM_NNOTIFT_SEM")));};
  // E 21/01/2000
  public String getNM_NNOTIFT_SEM() {
    String s = null;
    try {
      s = (String) get("NM_NNOTIFT_SEM");
    }
    catch (Exception e) {
      s = String.valueOf( (Integer) get("NM_NNOTIFT_SEM"));
    }

    if (s.equals("")) {
      s = "0";
    }
    return s;
  };

  public String getNM_NTOTREAL_SEM() {
    String s = null;
    try {
      s = (String) get("NM_NTOTREAL_SEM");
    }
    catch (Exception e) {
      s = String.valueOf( (Integer) get("NM_NTOTREAL_SEM"));
    }

    if (s.equals("")) {
      s = "0";
    }
    return s;
  };

  public String getCD_OPE_SEM() {
    return (String) get("CD_OPE_SEM");
  };
  public String getFC_ULTACT_SEM() {
    return (String) get("FC_ULTACT_SEM");
  };

  // Relacionado con SIVE_TRATAMIENTOS
  public Vector getTRATAMIENTOS() {
    return (Vector) get("TRATAMIENTOS");
  };

  public void setCD_ANOEPI_EDOI(String s) {
    introduce("CD_ANOEPI_EDOI", s);
  }

  public void setCD_SEMEPI(String s) {
    introduce("CD_SEMEPI", s);
  }

  public void setFC_NOTIF(String s) {
    introduce("FC_FECNOTIF", s);
  }

  public void setFC_RECEP_EDOI(String s) {
    introduce("FC_RECEP_EDOI", s);
  }

  public void setCD_E_NOTIF_EDOI(String s) {
    introduce("CD_E_NOTIF_EDOI", s);
  }

  public void setDS_E_NOTIF(String s) {
    introduce("DS_E_NOTIF", s);
  }

  public void setCD_CENTRO(String s) {
    introduce("CD_CENTRO", s);
  }

  public void setDS_CENTRO(String s) {
    introduce("DS_CENTRO", s);
  }

  public void setCD_ZBS(String s) {
    introduce("CD_ZBS", s);
  }

  public void setDS_DECLARANTE(String s) {
    introduce("DS_DECLARANTE", s);
  }

  public void setDS_HISTCLI(String s) {
    introduce("DS_HISTCLI", s);
  }

  public void setCD_FUENTE(String s) {
    introduce("CD_FUENTE", s);
  }

  public void setIT_PRIMERO(String s) {
    introduce("IT_PRIMERO", s);
  }

  public void setCD_NIVEL_1(String s) {
    introduce("CD_NIVEL_1", s);
  }

  public void setCD_NIVEL_2(String s) {
    introduce("CD_NIVEL_2", s);
  }

  public void setIT_RESSEM(String s) {
    introduce("IT_RESSEM", s);
  }

  public void setNM_NOTIFT_EQ(String s) {
    introduceInteger("NM_NOTIFT", s);
  }

  public void setNM_NNOTIFR(String s) {
    introduceInteger("NM_NNOTIFR", s);
  }

  public void setNM_NNOTIFT_SEM(String s) {
    introduceInteger("NM_NNOTIFT_SEM", s);
  }

  public void setNM_NTOTREAL_SEM(String s) {
    introduceInteger("NM_NTOTREAL_SEM", s);
  }

  public void setCD_OPE_SEM(String s) {
    introduce("CD_OPE_SEM", s);
  }

  public void setFC_ULTACT_SEM(String s) {
    introduce("FC_ULTACT_SEM", s);
  }

  public void setCD_OPE_EQ(String s) {
    introduce("CD_OPE_EQ", s);
  }

  public void setFC_ULTACT_EQ(String s) {
    introduce("FC_ULTACT_EQ", s);
  }

  public void setCD_OPE_EDO(String s) {
    introduce("CD_OPE_EDO", s);
  }

  public void setFC_ULTACT_EDO(String s) {
    introduce("FC_ULTACT_EDO", s);
  }

  public void setCD_OPE_EDOI(String s) {
    introduce("CD_OPE_EDOI", s);
  }

  public void setFC_ULTACT_EDOI(String s) {
    introduce("FC_ULTACT_EDOI", s);
  }

  public void setNM_EDO_EDOI(String s) {
    introduce("NM_EDO_EDOI", s);
  }

  public void setIT_COBERTURA(String s) {
    introduce("IT_COBERTURA", s);
  }

  public void setNM_NOTIFT(String s) {
    introduce("NM_NOTIFT", s);
  }

  // Relacionado con SIVE_TRATAMIENTOS
  public void setTRATAMIENTOS(Vector v) {
    put("TRATAMIENTOS", v);
  };

} //FIN DE LA CLASE
