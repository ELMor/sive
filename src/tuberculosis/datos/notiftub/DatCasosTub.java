package tuberculosis.datos.notiftub;

import java.util.Hashtable;

public class DatCasosTub
    extends DatTub {

  /** constructor por defecto */
  public DatCasosTub() {
    super();
  }

  /** constructor a partir de Hashtable */
  public DatCasosTub(Hashtable h) {
    super(h);
  }

  // Get's relativos a SIVE_REGISTROTBC
  public String getCD_ARTBC() {
    return (String) get("CD_ARTBC");
  }; // A�o
  public String getCD_NRTBC() {
    return (String) get("CD_NRTBC");
  }; // Secuenciador
  public String getFC_INIRTBC() {
    return (String) get("FC_INIRTBC");
  };
  public String getNM_EDO_TBC() {
    return (String) get("NM_EDO_TBC");
  };
  public String getFC_SALRTBC() {
    return (String) get("FC_SALRTBC");
  };
  public String getDS_OBSERV_TBC() {
    return (String) get("DS_OBSERV_TBC");
  };
  public String getCD_MOTSALRTBC() {
    return (String) get("CD_MOTSALRTBC");
  };
  public String getCD_E_NOTIF_TBC() {
    return (String) get("CD_E_NOTIF_TBC");
  };
  public String getDS_MEDICO() {
    return (String) get("DS_MEDICO");
  };
  public String getCD_OPE_TBC() {
    return (String) get("CD_OPE_TBC");
  };
  public String getFC_ULTACT_TBC() {
    return (String) get("FC_ULTACT_TBC");
  };
  public String getCD_NIVEL_1_TBC() {
    return (String) get("CD_NIVEL_1_GE");
  };
  public String getCD_NIVEL_2_TBC() {
    return (String) get("CD_NIVEL_2_GE");
  };

  // Get's relativos a SIVE_ENFERMO
  public String getCD_ENFERMO() {
    return saca("CD_ENFERMO");
  };
  public String getCD_TDOC() {
    return saca("CD_TDOC");
  };
  public String getDS_APE1() {
    return saca("DS_APE1");
  };
  public String getDS_APE2() {
    return saca("DS_APE2");
  };
  public String getSIGLAS() {
    return saca("SIGLAS");
  };
  public String getDS_NOMBRE() {
    return saca("DS_NOMBRE");
  };
  public String getDS_FONOAPE1() {
    return saca("DS_FONOAPE1");
  };
  public String getDS_FONOAPE2() {
    return saca("DS_FONOAPE2");
  };
  public String getDS_FONONOMBRE() {
    return saca("DS_FONONOMBRE");
  };
  public String getIT_CALC() {
    return saca("IT_CALC");
  };
  public String getFC_NAC() {
    return saca("FC_NAC");
  };
  public String getCD_SEXO() {
    return saca("CD_SEXO");
  };
  public String getCD_POSTAL() {
    return saca("CD_POSTAL");
  };
  public String getDS_DIREC() {
    return saca("DS_DIREC");
  };
  public String getDS_NUM() {
    return saca("DS_NUM");
  };
  public String getDS_PISO() {
    return saca("DS_PISO");
  };
  public String getDS_TELEF() {
    return saca("DS_TELEF");
  };
  public String getCD_NIVEL_1() {
    return saca("CD_NIVEL_1");
  };
  public String getCD_NIVEL_2() {
    return saca("CD_NIVEL_2");
  };
  public String getCD_ZBS() {
    return saca("CD_ZBS");
  };
  public String getCD_OPE() {
    return saca("CD_OPE");
  };
  public String getFC_ULTACT() {
    return saca("FC_ULTACT");
  };
  public String getIT_REVISADO() {
    return saca("IT_REVISADO");
  };
  public String getCD_MOTBAJA() {
    return saca("CD_MOTBAJA");
  };
  public String getFC_BAJA() {
    return saca("FC_BAJA");
  };
  public String getDS_OBSERV() {
    return saca("DS_OBSERV");
  };
  public String getCD_PROV2() {
    return saca("CD_PROV2");
  };
  public String getCD_MUNI2() {
    return saca("CD_MUNI2");
  };
  public String getCD_POST2() {
    return saca("CD_POST2");
  };
  public String getDS_DIREC2() {
    return saca("DS_DIREC2");
  };
  public String getDS_NUM2() {
    return saca("DS_NUM2");
  };
  public String getDS_PISO2() {
    return saca("DS_PISO2");
  };
  public String getDS_OBSERV2() {
    return saca("DS_OBSERV2");
  };
  public String getCD_PROV3() {
    return saca("CD_PROV3");
  };
  public String getCD_MUNI3() {
    return saca("CD_MUNI3");
  };
  public String getCD_POST3() {
    return saca("CD_POST3");
  };
  public String getDS_DIREC3() {
    return saca("DS_DIREC3");
  };
  public String getDS_NUM3() {
    return saca("DS_NUM3");
  };
  public String getDS_PISO3() {
    return saca("DS_PISO3");
  };
  public String getDS_TELEF3() {
    return saca("DS_TELEF3");
  };
  public String getDS_OBSERV3() {
    return saca("DS_OBSERV3");
  };
  public String getDS_NDOC() {
    return saca("DS_NDOC");
  };
  public String getDS_TELEF2() {
    return saca("DS_TELEF2");
  };
  public String getCDVIAL() {
    return saca("CDVIAL");
  };
  public String getCDTVIA() {
    return saca("CDTVIA");
  };
  public String getCDTNUM() {
    return saca("CDTNUM");
  };
  public String getDSCALNUM() {
    return saca("DSCALNUM");
  };
  public String getCD_PROV() {
    return saca("CD_PROV");
  };
  public String getCD_MUN() {
    return saca("CD_MUN");
  };
  public String getIT_ENFERMO() {
    return saca("IT_ENFERMO");
  };
  public String getCD_PAIS() {
    return saca("CD_PAIS");
  };
  public String getIT_CONTACTO() {
    return saca("IT_CONTACTO");
  };

  // Get's relativos a SIVE_EDOIND
  // E 19/01/2000
  public Integer getNM_EDO_Integer() {
    return (Integer) get("NM_EDO");
  };
  // E 18/01/2000
  public String getNM_EDO() {
    return (String) get("NM_EDO");
  };

  public String getCD_ANOEPI() {
    return (String) get("CD_ANOEPI");
  };
  public String getCD_SEMEPI() {
    return (String) get("CD_SEMEPI");
  };
  public String getCD_ANOURG() {
    return (String) get("CD_ANOURG");
  };

  public String getNM_ENVIOURGSEM() {
    return (String) get("NM_ENVIOURGSEM");
  };
  // E 19/01/2000
  public Integer getNM_ENVIOURGSEM_Integer() {
    return (Integer) get("NM_ENVIOURGSEM");
  };

  public String getCD_CLASIFDIAG() {
    return (String) get("CD_CLASIFDIAG");
  };
  public String getIT_PENVURG() {
    return (String) get("IT_PENVURG");
  };
  public String getCD_ENFERMO_EDOIND() {
    return (String) get("CD_ENFERMO_EDOIND");
  };
  public String getCD_PROV_EDOIND() {
    return (String) get("CD_PROV_EDOIND");
  };
  public String getCD_MUN_EDOIND() {
    return (String) get("CD_MUN_EDOIND");
  };
  public String getCD_NIVEL_1_EDOIND() {
    return (String) get("CD_NIVEL_1_EDOIND");
  };
  public String getCD_NIVEL_2_EDOIND() {
    return (String) get("CD_NIVEL_2_EDOIND");
  };
  public String getCD_ZBS_EDOIND() {
    return (String) get("CD_ZBS_EDOIND");
  };
  public String getFC_RECEP() {
    return (String) get("FC_RECEP");
  };
  public String getDS_CALLE() {
    return (String) get("DS_CALLE");
  };
  public String getDS_NMCALLE() {
    return (String) get("DS_NMCALLE");
  };
  public String getDS_PISO_EDOIND() {
    return (String) get("DS_PISO_EDOIND");
  };
  public String getCD_POSTAL_EDOIND() {
    return (String) get("CD_POSTAL_EDOIND");
  };
  public String getCD_ANOOTRC() {
    return (String) get("CD_ANOOTRC");
  };
  public String getNM_ENVOTRC() {
    return (String) get("NM_ENVOTRC");
  };
  public String getCD_ENFCIE() {
    return (String) get("CD_ENFCIE");
  };
  public String getFC_FECNOTIF() {
    return (String) get("FC_FECNOTIF");
  };
  public String getIT_DERIVADO() {
    return (String) get("IT_DERIVADO");
  };
  public String getDS_CENTRODER() {
    return (String) get("DS_CENTRODER");
  };
  public String getIT_DIAGCLI() {
    return (String) get("IT_DIAGCLI");
  };
  public String getIT_DIAGMICRO() {
    return (String) get("IT_DIAGMICRO");
  };
  public String getIT_DIAGSERO() {
    return (String) get("IT_DIAGSERO");
  };
  public String getDS_DIAGOTROS() {
    return (String) get("DS_DIAGOTROS");
  };
  public String getFC_INISNT() {
    return (String) get("FC_INISNT");
  };
  public String getDS_COLECTIVO() {
    return (String) get("DS_COLECTIVO");
  };
  public String getIT_ASOCIADO() {
    return (String) get("IT_ASOCIADO");
  };
  public String getDS_ASOCIADO() {
    return (String) get("DS_ASOCIADO");
  };
  public String getCDVIAL_EDOIND() {
    return (String) get("CDVIAL_EDOIND");
  };
  public String getCDTVIA_EDOIND() {
    return (String) get("CDTVIA_EDOIND");
  };
  public String getCDTNUM_EDOIND() {
    return (String) get("CDTNUM_EDOIND");
  };
  public String getDSCALNUM_EDOIND() {
    return (String) get("DSCALNUM_EDOIND");
  };
  public String getCD_OPE_EDOIND() {
    return (String) get("CD_OPE_EDOIND");
  };
  public String getFC_ULTACT_EDOIND() {
    return (String) get("FC_ULTACT_EDOIND");
  };

  public String getDS_NIVEL_1_EDOIND() {
    return (String) get("DS_NIVEL_1_EDOIND");
  }

  public String getDS_NIVEL_2_EDOIND() {
    return (String) get("DS_NIVEL_2_EDOIND");
  }

  public String getDS_ZBS_EDOIND() {
    return (String) get("DS_ZBS_EDOIND");
  }

  // Relativo a modos normal y suca
  public String getDS_MUN() {
    return (String) get("DS_MUN");
  };
  public String getCD_CA() {
    return (String) get("CD_CA");
  };
  public String getDS_ZBS() {
    return (String) get("DS_ZBS");
  };

  public void putDS_MUN(String sValor) {
    put("DS_MUN", sValor);
  };
  public void putCD_CA(String sValor) {
    put("CD_CA", sValor);
  };
  public void putDS_ZBS(String sValor) {
    put("DS_ZBS", sValor);
  };

  // Set's relativos a SIVE_EDOIND
  public void setNM_EDO(String s) {
    introduce("NM_EDO", s);
  };
  public void setCD_ANOEPI(String s) {
    introduce("CD_ANOEPI", s);
  };
  public void setCD_SEMEPI(String s) {
    introduce("CD_SEMEPI", s);
  };
  public void setCD_ANOURG(String s) {
    introduce("CD_ANOURG", s);
  };
  public void setNM_ENVIOURGSEM(String s) {
    introduce("NM_ENVIOURGSEM", s);
  };
  public void setCD_CLASIFDIAG(String s) {
    introduce("CD_CLASIFDIAG", s);
  };
  public void setIT_PENVURG(String s) {
    introduce("IT_PENVURG", s);
  };
  public void setCD_ENFERMO_EDOIND(String s) {
    introduce("CD_ENFERMO_EDOIND", s);
  };
  public void setCD_PROV_EDOIND(String s) {
    introduce("CD_PROV_EDOIND", s);
  };
  public void setCD_MUN_EDOIND(String s) {
    introduce("CD_MUN_EDOIND", s);
  };
  public void setCD_NIVEL_1_EDOIND(String s) {
    introduce("CD_NIVEL_1_EDOIND", s);
  };
  public void setCD_NIVEL_2_EDOIND(String s) {
    introduce("CD_NIVEL_2_EDOIND", s);
  };
  public void setCD_ZBS_EDOIND(String s) {
    introduce("CD_ZBS_EDOIND", s);
  };
  public void setFC_RECEP(String s) {
    introduce("FC_RECEP", s);
  };
  public void setDS_CALLE(String s) {
    introduce("DS_CALLE", s);
  };
  public void setDS_NMCALLE(String s) {
    introduce("DS_NMCALLE", s);
  };
  public void setDS_PISO_EDOIND(String s) {
    introduce("DS_PISO_EDOIND", s);
  };
  public void setCD_POSTAL_EDOIND(String s) {
    introduce("CD_POSTAL_EDOIND", s);
  };
  public void setCD_ANOOTRC(String s) {
    introduce("CD_ANOOTRC", s);
  };
  public void setNM_ENVOTRC(String s) {
    introduce("NM_ENVOTRC", s);
  };
  public void setCD_ENFCIE(String s) {
    introduce("CD_ENFCIE", s);
  };
  public void setFC_FECNOTIF(String s) {
    introduce("FC_FECNOTIF", s);
  };
  public void setIT_DERIVADO(String s) {
    introduce("IT_DERIVADO", s);
  };
  public void setDS_CENTRODER(String s) {
    introduce("DS_CENTRODER", s);
  };
  public void setIT_DIAGCLI(String s) {
    introduce("IT_DIAGCLI", s);
  };
  public void setIT_DIAGMICRO(String s) {
    introduce("IT_DIAGMICRO", s);
  };
  public void setIT_DIAGSERO(String s) {
    introduce("IT_DIAGSERO", s);
  };
  public void setDS_DIAGOTROS(String s) {
    introduce("DS_DIAGOTROS", s);
  };
  public void setFC_INISNT(String s) {
    introduce("FC_INISNT", s);
  };
  public void setDS_COLECTIVO(String s) {
    introduce("DS_COLECTIVO", s);
  };
  public void setIT_ASOCIADO(String s) {
    introduce("IT_ASOCIADO", s);
  };
  public void setDS_ASOCIADO(String s) {
    introduce("DS_ASOCIADO", s);
  };
  public void setCDVIAL_EDOIND(String s) {
    introduce("CDVIAL_EDOIND", s);
  };
  public void setCDTVIA_EDOIND(String s) {
    introduce("CDTVIA_EDOIND", s);
  };
  public void setCDTNUM_EDOIND(String s) {
    introduce("CDTNUM_EDOIND", s);
  };
  public void setDSCALNUM_EDOIND(String s) {
    introduce("DSCALNUM_EDOIND", s);
  };
  public void setCD_OPE_EDOIND(String s) {
    introduce("CD_OPE_EDOIND", s);
  };
  public void setFC_ULTACT_EDOIND(String s) {
    introduce("FC_ULTACT_EDOIND", s);
  };

  public void setDS_NIVEL_1_EDOIND(String s) {
    introduce("DS_NIVEL_1_EDOIND", s);
  }

  public void setDS_NIVEL_2_EDOIND(String s) {
    introduce("DS_NIVEL_2_EDOIND", s);
  }

  public void setDS_ZBS_EDOIND(String s) {
    introduce("DS_ZBS_EDOIND", s);
  }

  // Set's relativos a SIVE_REGISTROTBC
  public void setCD_ARTBC(String s) {
    introduce("CD_ARTBC", s);
  }; // A�o
  public void setCD_NRTBC(String s) {
    introduce("CD_NRTBC", s);
  }; // Secuenciador
  public void setFC_INIRTBC(String s) {
    introduce("FC_INIRTBC", s);
  };
  public void setNM_EDO_TBC(String s) {
    introduce("NM_EDO_TBC", s);
  };
  public void setFC_SALRTBC(String s) {
    introduce("FC_SALRTBC", s);
  };
  public void setDS_OBSERV_TBC(String s) {
    introduce("DS_OBSERV_TBC", s);
  };
  public void setCD_MOTSALRTBC(String s) {
    introduce("CD_MOTSALRTBC", s);
  };
  public void setCD_E_NOTIF_TBC(String s) {
    introduce("CD_E_NOTIF_TBC", s);
  };
  public void setDS_MEDICO(String s) {
    introduce("DS_MEDICO", s);
  };
  public void setCD_OPE_TBC(String s) {
    introduce("CD_OPE_TBC", s);
  };
  public void setFC_ULTACT_TBC(String s) {
    introduce("FC_ULTACT_TBC", s);
  };
  public void setCD_NIVEL_1_TBC(String s) {
    introduce("CD_NIVEL_1_GE", s);
  };
  public void setCD_NIVEL_2_TBC(String s) {
    introduce("CD_NIVEL_2_GE", s);
  };

  // Set's relativos a SIVE_ENFERMO
  public void setCD_ENFERMO(String s) {
    introduce("CD_ENFERMO", s);
  };
  public void setCD_TDOC(String s) {
    introduce("CD_TDOC", s);
  };
  public void setDS_APE1(String s) {
    introduce("DS_APE1", s);
  };
  public void setDS_APE2(String s) {
    introduce("DS_APE2", s);
  };
  public void setSIGLAS(String s) {
    introduce("SIGLAS", s);
  };
  public void setDS_NOMBRE(String s) {
    introduce("DS_NOMBRE", s);
  };
  public void setDS_FONOAPE1(String s) {
    introduce("DS_FONOAPE1", s);
  };
  public void setDS_FONOAPE2(String s) {
    introduce("DS_FONOAPE2", s);
  };
  public void setDS_FONONOMBRE(String s) {
    introduce("DS_FONONOMBRE", s);
  };
  public void setIT_CALC(String s) {
    introduce("IT_CALC", s);
  };
  public void setFC_NAC(String s) {
    introduce("FC_NAC", s);
  };
  public void setCD_SEXO(String s) {
    introduce("CD_SEXO", s);
  };
  public void setCD_POSTAL(String s) {
    introduce("CD_POSTAL", s);
  };
  public void setDS_DIREC(String s) {
    introduce("DS_DIREC", s);
  };
  public void setDS_NUM(String s) {
    introduce("DS_NUM", s);
  };
  public void setDS_PISO(String s) {
    introduce("DS_PISO", s);
  };
  public void setDS_TELEF(String s) {
    introduce("DS_TELEF", s);
  };
  public void setCD_NIVEL_1(String s) {
    introduce("CD_NIVEL_1", s);
  };
  public void setCD_NIVEL_2(String s) {
    introduce("CD_NIVEL_2", s);
  };
  public void setCD_ZBS(String s) {
    introduce("CD_ZBS", s);
  };
  public void setCD_OPE(String s) {
    introduce("CD_OPE", s);
  };
  public void setFC_ULTACT(String s) {
    introduce("FC_ULTACT", s);
  };
  public void setIT_REVISADO(String s) {
    introduce("IT_REVISADO", s);
  };
  public void setCD_MOTBAJA(String s) {
    introduce("CD_MOTBAJA", s);
  };
  public void setFC_BAJA(String s) {
    introduce("FC_BAJA", s);
  };
  public void setDS_OBSERV(String s) {
    introduce("DS_OBSERV", s);
  };
  public void setCD_PROV2(String s) {
    introduce("CD_PROV2", s);
  };
  public void setCD_MUNI2(String s) {
    introduce("CD_MUNI2", s);
  };
  public void setCD_POST2(String s) {
    introduce("CD_POST2", s);
  };
  public void setDS_DIREC2(String s) {
    introduce("DS_DIREC2", s);
  };
  public void setDS_NUM2(String s) {
    introduce("DS_NUM2", s);
  };
  public void setDS_PISO2(String s) {
    introduce("DS_PISO2", s);
  };
  public void setDS_OBSERV2(String s) {
    introduce("DS_OBSERV2", s);
  };
  public void setCD_PROV3(String s) {
    introduce("CD_PROV3", s);
  };
  public void setCD_MUNI3(String s) {
    introduce("CD_MUNI3", s);
  };
  public void setCD_POST3(String s) {
    introduce("CD_POST3", s);
  };
  public void setDS_DIREC3(String s) {
    introduce("DS_DIREC3", s);
  };
  public void setDS_NUM3(String s) {
    introduce("DS_NUM3", s);
  };
  public void setDS_PISO3(String s) {
    introduce("DS_PISO3", s);
  };
  public void setDS_TELEF3(String s) {
    introduce("DS_TELEF3", s);
  };
  public void setDS_OBSERV3(String s) {
    introduce("DS_OBSERV3", s);
  };
  public void setDS_NDOC(String s) {
    introduce("DS_NDOC", s);
  };
  public void setDS_TELEF2(String s) {
    introduce("DS_TELEF2", s);
  };
  public void setCDVIAL(String s) {
    introduce("CDVIAL", s);
  };
  public void setCDTVIA(String s) {
    introduce("CDTVIA", s);
  };
  public void setCDTNUM(String s) {
    introduce("CDTNUM", s);
  };
  public void setDSCALNUM(String s) {
    introduce("DSCALNUM", s);
  };
  public void setCD_PROV(String s) {
    introduce("CD_PROV", s);
  };
  public void setCD_MUN(String s) {
    introduce("CD_MUN", s);
  };
  public void setIT_ENFERMO(String s) {
    introduce("IT_ENFERMO", s);
  };
  public void setCD_PAIS(String s) {
    introduce("CD_PAIS", s);
  };
  public void setIT_CONTACTO(String s) {
    introduce("IT_CONTACTO", s);
  };

} //FIN DE LA CLASE
