package tuberculosis.datos.notiftub;

import java.util.Hashtable;

public class DatRT
    extends DatTub {

  /** constructor */
  public DatRT() {
    super();
  }

  /** constructor a partir de Hashtable */
  public DatRT(Hashtable h) {
    super(h);
  }

  //Si es un nulo, no hara NADA, y al hacer el get tendre null
  //Si no es nulo, hara un put y al hacer el get obtendre el dato
  public synchronized void insert(Object key, Object value) {
    if ( (key != null) && (value != null)) {
      put(key, value);
    }
  }

  //Soporta la clave de Registro de tuberc
  public String getCD_ARTBC() {
    return (String) get("CD_ARTBC");
  };
  public String getCD_NRTBC() {
    return (String) get("CD_NRTBC");
  };

  public String getNM_EDO() {
    return (String) get("NM_EDO");
  };

  // modificacion 09/06/2000
  // modificacion realizada para poder recueperas los valores
  // de los niveles 1 y 2
  public String getCD_NIVEL1() {
    return (String) get("CD_NIVEL1");
  };
  public String getCD_NIVEL2() {
    return (String) get("CD_NIVEL2");
  };

} //FIN DE LA CLASE
