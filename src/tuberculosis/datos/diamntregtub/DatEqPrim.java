/**
 * Clase: DatEqPrim
 * Paquete: tuberculosis.datos.diamntregtub
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 05/11/1999
 * Analisis Funcional: Punto 2. Mantenimiento Registro Tuberculosis.
 * Descripcion: Estructura de datos que contiene los datos necesarios
 *   para realizar una busqueda de un eq de Primaria
 */

package tuberculosis.datos.diamntregtub;

import java.io.Serializable;
import java.util.Vector;

public class DatEqPrim
    implements Serializable {

  private String CD_NIVEL_1 = "";
  private String CD_NIVEL_2 = "";
  private String CD_E_NOTIF = "";
  private String DS_E_NOTIF = "";
  public Vector VN1 = null;
  public Vector VN2 = null;

  public DatEqPrim(String cd_nivel_1,
                   String cd_nivel_2,
                   String cd_e_notif,
                   String ds_e_notif,
                   Vector vn1,
                   Vector vn2) { // Constructor

    if (cd_nivel_1 != null) {
      CD_NIVEL_1 = cd_nivel_1;
    }
    else {
      CD_NIVEL_1 = "";
    }
    if (cd_nivel_2 != null) {
      CD_NIVEL_2 = cd_nivel_2;
    }
    else {
      CD_NIVEL_2 = "";
    }
    if (cd_e_notif != null) {
      CD_E_NOTIF = cd_e_notif;
    }
    else {
      CD_E_NOTIF = "";
    }
    if (ds_e_notif != null) {
      DS_E_NOTIF = ds_e_notif;
    }
    else {
      DS_E_NOTIF = "";
    }
    if (vn1 != null) {
      VN1 = vn1;
    }
    else {
      VN1 = null;
    }
    if (vn2 != null) {
      VN2 = vn2;
    }
    else {
      VN2 = null;
    }
  }

  public String getCD_NIVEL_1() {
    return CD_NIVEL_1;
  }

  public String getCD_NIVEL_2() {
    return CD_NIVEL_2;
  }

  public String getCD_E_NOTIF() {
    return CD_E_NOTIF;
  }

  public String getDS_E_NOTIF() {
    return DS_E_NOTIF;
  }

  public Vector getVN1() {
    return VN1;
  }

  public Vector getVN2() {
    return VN2;
  }

} // endclass DatDiaMntRegTub
