/**
 * Clase: DatDiaMntRegTub
 * Paquete: SP_Tuberculosis.datos.diamntregtub
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 05/11/1999
 * Analisis Funcional: Punto 2. Mantenimiento Registro Tuberculosis.
 * Descripcion: Estructura de datos que contiene los datos necesarios
 *   para realizar una busqueda de un determinado registro de
 *   tuberculosis y almacenar los datos resultado, a traves del
 *   servlet SrvDiaMntRegTub.
 */

package tuberculosis.datos.diamntregtub;

import java.io.Serializable;

public class DatDiaMntRegTub
    implements Serializable {

  // Datos de la busqueda
  private String CD_ARTBC = "";
  private String CD_NRTBC = "";

  // Datos resultado de la busqueda de un unico registro: a�o+n�mero
  private String CD_ENFERMO = "";
  private String DS_APE1 = "";
  private String DS_APE2 = "";
  private String DS_NOMBRE = "";
  private String SIGLAS = "";
  private String FC_NAC = "";
  private String IT_FG_CALC = "";
  private String CD_SEXO = "";
  private String DS_TELEF = "";
  private String DS_CA = "";
  private String DS_PROV = "";
  private String CD_MUN = "";
  private String DS_MUN = "";
  private String DS_DIREC = "";
  private String DS_NUM = "";
  private String DS_PISO = "";
  private String CD_POSTAL = "";
  private String CD_E_NOTIF = "";
  private String DS_E_NOTIF = "";
  private String DS_MEDICO = "";
  private String DS_OBSERV = "";
  private String FC_SALRTBC = "";
  private String CD_MOTSALRTBC = "";
  private String CD_OPE = "";
  private String FC_ULTACT = "";

  private String IT_FG_CONFIDENCIAL = "";

  //otros campos de RT
  private String FC_INIRTBC = "";
  private String CD_NIVEL_1_GE = "";
  private String CD_NIVEL_2_GE = "";
  private String DS_NIVEL_1_GE = "";
  private String DS_NIVEL_2_GE = "";

  public DatDiaMntRegTub(String Ano,
                         String Reg,
                         String CodEnfermo,
                         String Ape1,
                         String Ape2,
                         String Nombre,
                         String Siglas,
                         String FNac,
                         String FEdad,
                         String Sexo,
                         String Telef,
                         String CA,
                         String Prov,
                         String CodMun,
                         String DescMun,
                         String Direc,
                         String Num,
                         String Piso,
                         String CodPostal,
                         String CodEqNotif,
                         String DesEqNotif,
                         String Medico,
                         String Observ,
                         String FSalida,
                         String MotSalida,
                         String CodOpe,
                         String FUltact,
                         String FConf,
                         String fc_inirtbc,
                         String cd_nivel_1_ge,
                         String cd_nivel_2_ge,
                         String ds_nivel_1_ge,
                         String ds_nivel_2_ge) { // Constructor

    if (Ano != null) {
      CD_ARTBC = Ano;
    }
    else {
      CD_ARTBC = "";
    }
    if (Reg != null) {
      CD_NRTBC = Reg;
    }
    else {
      CD_NRTBC = "";
    }
    if (CodEnfermo != null) {
      CD_ENFERMO = CodEnfermo;
    }
    else {
      CD_ENFERMO = "";
    }
    if (Ape1 != null) {
      DS_APE1 = Ape1;
    }
    else {
      DS_APE1 = "";
    }
    if (Ape2 != null) {
      DS_APE2 = Ape2;
    }
    else {
      DS_APE2 = "";
    }
    if (Nombre != null) {
      DS_NOMBRE = Nombre;
    }
    else {
      DS_NOMBRE = "";
    }
    if (Siglas != null) {
      SIGLAS = Siglas;
    }
    else {
      SIGLAS = "";
    }
    if (FNac != null) {
      FC_NAC = FNac;
    }
    else {
      FC_NAC = "";
    }
    if (FEdad != null) {
      IT_FG_CALC = FEdad;
    }
    else {
      IT_FG_CALC = "";
    }
    if (Sexo != null) {
      CD_SEXO = Sexo;
    }
    else {
      CD_SEXO = "";
    }
    if (Telef != null) {
      DS_TELEF = Telef;
    }
    else {
      DS_TELEF = "";
    }
    if (CA != null) {
      DS_CA = CA;
    }
    else {
      DS_CA = "";
    }
    if (Prov != null) {
      DS_PROV = Prov;
    }
    else {
      DS_PROV = "";
    }
    if (CodMun != null) {
      CD_MUN = CodMun;
    }
    else {
      CD_MUN = "";
    }
    if (DescMun != null) {
      DS_MUN = DescMun;
    }
    else {
      DS_MUN = "";
    }
    if (Direc != null) {
      DS_DIREC = Direc;
    }
    else {
      DS_DIREC = "";
    }
    if (Num != null) {
      DS_NUM = Num;
    }
    else {
      DS_NUM = "";
    }
    if (Piso != null) {
      DS_PISO = Piso;
    }
    else {
      DS_PISO = "";
    }
    if (CodPostal != null) {
      CD_POSTAL = CodPostal;
    }
    else {
      CD_POSTAL = "";
    }
    if (CodEqNotif != null) {
      CD_E_NOTIF = CodEqNotif;
    }
    else {
      CD_E_NOTIF = "";
    }
    if (DesEqNotif != null) {
      DS_E_NOTIF = DesEqNotif;
    }
    else {
      DS_E_NOTIF = "";
    }
    if (Medico != null) {
      DS_MEDICO = Medico;
    }
    else {
      DS_MEDICO = "";
    }
    if (Observ != null) {
      DS_OBSERV = Observ;
    }
    else {
      DS_OBSERV = "";
    }
    if (FSalida != null) {
      FC_SALRTBC = FSalida;
    }
    else {
      FC_SALRTBC = "";
    }
    if (MotSalida != null) {
      CD_MOTSALRTBC = MotSalida;
    }
    else {
      CD_MOTSALRTBC = "";
    }
    if (CodOpe != null) {
      CD_OPE = CodOpe;
    }
    else {
      CD_OPE = "";
    }
    if (FUltact != null) {
      FC_ULTACT = FUltact;
    }
    else {
      FC_ULTACT = "";
    }
    if (FConf != null) {
      IT_FG_CONFIDENCIAL = FConf;
    }
    else {
      IT_FG_CONFIDENCIAL = "";
    }
    if (fc_inirtbc != null) {
      FC_INIRTBC = fc_inirtbc;
    }
    else {
      FC_INIRTBC = "";
    }
    if (cd_nivel_1_ge != null) {
      CD_NIVEL_1_GE = cd_nivel_1_ge;
    }
    else {
      CD_NIVEL_1_GE = "";
    }
    if (cd_nivel_2_ge != null) {
      CD_NIVEL_2_GE = cd_nivel_2_ge;
    }
    else {
      CD_NIVEL_2_GE = "";
    }
    if (ds_nivel_1_ge != null) {
      DS_NIVEL_1_GE = ds_nivel_1_ge;
    }
    else {
      DS_NIVEL_1_GE = "";
    }
    if (ds_nivel_2_ge != null) {
      DS_NIVEL_2_GE = ds_nivel_2_ge;
    }
    else {
      DS_NIVEL_2_GE = "";

    }
  }

  public String getCD_ARTBC() {
    return CD_ARTBC;
  }

  public String getCD_NRTBC() {
    return CD_NRTBC;
  }

  public String getCD_ENFERMO() {
    return CD_ENFERMO;
  }

  public String getDS_APE1() {
    return DS_APE1;
  }

  public String getDS_APE2() {
    return DS_APE2;
  }

  public String getDS_NOMBRE() {
    return DS_NOMBRE;
  }

  public String getSIGLAS() {
    return SIGLAS;
  }

  public String getFC_NAC() {
    return FC_NAC;
  }

  public String getIT_FG_CALC() {
    return IT_FG_CALC;
  }

  public String getCD_SEXO() {
    return CD_SEXO;
  }

  public String getDS_TELEF() {
    return DS_TELEF;
  }

  public String getDS_CA() {
    return DS_CA;
  }

  public String getDS_PROV() {
    return DS_PROV;
  }

  public String getCD_MUN() {
    return CD_MUN;
  }

  public String getDS_MUN() {
    return DS_MUN;
  }

  public String getDS_DIREC() {
    return DS_DIREC;
  }

  public String getDS_NUM() {
    return DS_NUM;
  }

  public String getDS_PISO() {
    return DS_PISO;
  }

  public String getCD_POSTAL() {
    return CD_POSTAL;
  }

  public String getCD_E_NOTIF() {
    return CD_E_NOTIF;
  }

  public String getDS_E_NOTIF() {
    return DS_E_NOTIF;
  }

  public String getDS_MEDICO() {
    return DS_MEDICO;
  }

  public String getDS_OBSERV() {
    return DS_OBSERV;
  }

  public String getFC_SALRTBC() {
    return FC_SALRTBC;
  }

  public String getCD_MOTSALRTBC() {
    return CD_MOTSALRTBC;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public String getIT_FG_CONFIDENCIAL() {
    return IT_FG_CONFIDENCIAL;
  }

  public String getFC_INIRTBC() {
    return FC_INIRTBC;
  }

  public String getCD_NIVEL_1_GE() {
    return CD_NIVEL_1_GE;
  }

  public String getCD_NIVEL_2_GE() {
    return CD_NIVEL_2_GE;
  }

  public String getDS_NIVEL_1_GE() {
    return DS_NIVEL_1_GE;
  }

  public String getDS_NIVEL_2_GE() {
    return DS_NIVEL_2_GE;
  }
} // endclass DatDiaMntRegTub
