/**
 * Clase: DatTotTratCS
 * Paquete: tuberculosis.datos.pantotaltrat
 * Hereda:
 * Autor: Pedro Antonio D�az (PDP)
 * Fecha Inicio: 14/03/2000
 * Descripcion: Estructura de datos que contiene los datos asociados
 *   necesarios para buscar los TRATAMIENTOS asociados por notificador.
 */

package tuberculosis.datos.pantotaltrat;

import java.io.Serializable;

public class DatTotTratCS
    implements Serializable {

  // Datos de la busqueda (SIVE_NOTIF_EDOI)
  private String CD_E_NOTIF = "";
  private String CD_ANOEPI = "";
  private String CD_SEMEPI = "";
  private String NM_EDO = "";
  private String FC_FECNOTIF = "";
  private String FC_RECEP = "";
  private String CD_FUENTE = "";
  private String DS_HISTCLI = "";
  private String DS_DECLARANTE = "";
  private String IT_PRIMERO = "";
  private String CD_OPE = "";
  private String FC_ULTACT = "";

  public DatTotTratCS(String Equipo,
                      String Ano,
                      String Sem,
                      String Edo,
                      String FNotif,
                      String FRecep,
                      String Fuente,
                      String HistClin,
                      String Decl,
                      String FlagPrim,
                      String Ope,
                      String FUlt) { // Constructor

    if (Equipo != null) {
      CD_E_NOTIF = Equipo;
    }
    else {
      CD_E_NOTIF = "";
    }
    if (Ano != null) {
      CD_ANOEPI = Ano;
    }
    else {
      CD_ANOEPI = "";
    }
    if (Sem != null) {
      CD_SEMEPI = Sem;
    }
    else {
      CD_SEMEPI = "";
    }
    if (Edo != null) {
      NM_EDO = Edo;
    }
    else {
      NM_EDO = "";
    }
    if (FNotif != null) {
      FC_FECNOTIF = FNotif;
    }
    else {
      FC_FECNOTIF = "";
    }
    if (FRecep != null) {
      FC_RECEP = FRecep;
    }
    else {
      FC_RECEP = "";
    }
    if (Fuente != null) {
      CD_FUENTE = Fuente;
    }
    else {
      CD_FUENTE = "";
    }
    if (HistClin != null) {
      DS_HISTCLI = HistClin;
    }
    else {
      DS_HISTCLI = "";
    }
    if (Decl != null) {
      DS_DECLARANTE = Decl;
    }
    else {
      DS_DECLARANTE = "";
    }
    if (FlagPrim != null) {
      IT_PRIMERO = FlagPrim;
    }
    else {
      IT_PRIMERO = "";
    }
    if (Ope != null) {
      CD_OPE = Ope;
    }
    else {
      CD_OPE = "";
    }
    if (FUlt != null) {
      FC_ULTACT = FUlt;
    }
    else {
      FC_ULTACT = "";
    }
  }

  public String getCD_E_NOTIF() {
    return CD_E_NOTIF;
  }

  public String getCD_ANOEPI() {
    return CD_ANOEPI;
  }

  public String getCD_SEMEPI() {
    return CD_SEMEPI;
  }

  public String getNM_EDO() {
    return NM_EDO;
  }

  public String getFC_FECNOTIF() {
    return FC_FECNOTIF;
  }

  public String getFC_RECEP() {
    return FC_RECEP;
  }

  public String getCD_FUENTE() {
    return CD_FUENTE;
  }

  public String getDS_HISTCLI() {
    return DS_HISTCLI;
  }

  public String getDS_DECLARANTE() {
    return DS_DECLARANTE;
  }

  public String getIT_PRIMERO() {
    return IT_PRIMERO;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }

  public void setNM_EDO(String edo) {
    NM_EDO = edo;
  }

  public void setCD_OPE(String ope) {
    CD_OPE = ope;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public void setFC_ULTACT(String fultact) {
    FC_ULTACT = fultact;
  }

} // endclass DatTotTratCS