/**
 * Clase: DatTotTratSC
 * Paquete: tuberculosis.datos.pantotaltrat
 * Hereda:
 * Autor: Pedro Antonio D�az (PDP)
 * Fecha Inicio: 14/03/2000
 * Descripcion: Estructura de datos que contiene los datos asociados
 *   a un TRATAMIENTO, resultado de la busqueda de tratamientos
 *   por NOTIFICADOR.
 */

package tuberculosis.datos.pantotaltrat;

import java.io.Serializable;

public class DatTotTratSC
    implements Serializable {

  // Datos resultado de la busqueda de un unico tratamiento
  private String NM_TRATRTBC = "";
  private String CD_E_NOTIF = "";
  private String CD_ANOEPI = "";
  private String CD_SEMEPI = "";
  private String FC_INITRAT = "";
  private String CD_MOTRATINI = "";
  private String DS_MOTRATINI = "";
  private String FC_FINTRAT = "";
  private String CD_MOTRATFIN = "";
  private String DS_MOTRATFIN = "";
  private String DS_OBSERV = "";
  private String CD_OPE = "";
  private String FC_ULTACT = "";

  public DatTotTratSC(String NumTrat,
                      String CodNotif,
                      String NumAno,
                      String NumSem,
                      String FInicio,
                      String MotInicio,
                      String DescMotIni,
                      String FFin,
                      String MotFin,
                      String DescMotFin,
                      String Observ,
                      String Ope,
                      String FUlt) { // Constructor

    if (NumTrat != null) {
      NM_TRATRTBC = NumTrat;
    }
    else {
      NM_TRATRTBC = "";
    }
    if (CodNotif != null) {
      CD_E_NOTIF = CodNotif;
    }
    else {
      CD_E_NOTIF = "";
    }
    if (NumAno != null) {
      CD_ANOEPI = NumAno;
    }
    else {
      CD_ANOEPI = "";
    }
    if (NumSem != null) {
      CD_SEMEPI = NumSem;
    }
    else {
      CD_SEMEPI = "";
    }
    if (FInicio != null) {
      FC_INITRAT = FInicio;
    }
    else {
      FC_INITRAT = "";
    }
    if (MotInicio != null) {
      CD_MOTRATINI = MotInicio;
    }
    else {
      CD_MOTRATINI = "";
    }
    if (DescMotIni != null) {
      DS_MOTRATINI = DescMotIni;
    }
    else {
      DS_MOTRATINI = "";
    }
    if (FFin != null) {
      FC_FINTRAT = FFin;
    }
    else {
      FC_FINTRAT = "";
    }
    if (MotFin != null) {
      CD_MOTRATFIN = MotFin;
    }
    else {
      CD_MOTRATFIN = "";
    }
    if (DescMotFin != null) {
      DS_MOTRATFIN = DescMotFin;
    }
    else {
      DS_MOTRATFIN = "";
    }
    if (Observ != null) {
      DS_OBSERV = Observ;
    }
    else {
      DS_OBSERV = "";
    }
    if (Ope != null) {
      CD_OPE = Ope;
    }
    else {
      CD_OPE = "";
    }
    if (FUlt != null) {
      FC_ULTACT = FUlt;
    }
    else {
      FC_ULTACT = "";
    }
  }

  public String getNM_TRATRTBC() {
    return NM_TRATRTBC;
  }

  public void setNM_TRATRTBC(String n) {
    NM_TRATRTBC = n;
  }

  public String getCD_E_NOTIF() {
    return CD_E_NOTIF;
  }

  public String getCD_ANOEPI() {
    return CD_ANOEPI;
  }

  public String getCD_SEMEPI() {
    return CD_SEMEPI;
  }

  public String getFC_INITRAT() {
    return FC_INITRAT;
  }

  public String getCD_MOTRATINI() {
    return CD_MOTRATINI;
  }

  public String getDS_MOTRATINI() {
    return DS_MOTRATINI;
  }

  public String getFC_FINTRAT() {
    return FC_FINTRAT;
  }

  public String getCD_MOTRATFIN() {
    return CD_MOTRATFIN;
  }

  public String getDS_MOTRATFIN() {
    return DS_MOTRATFIN;
  }

  public String getDS_OBSERV() {
    return DS_OBSERV;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }

  public void setCD_OPE(String ope) {
    CD_OPE = ope;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public void setFC_ULTACT(String fultact) {
    FC_ULTACT = fultact;
  }

} // endclass DatTotTratSC