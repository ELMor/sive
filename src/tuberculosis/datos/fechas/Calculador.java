package tuberculosis.datos.fechas;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Calculador {

  public Calculador() {
  }

  //Devuelve num semanas EPIDEMIOLOGICAS que transcurren entre dos fechas dadas
  //Incluyenndo las semanas epid inicial y final.
  //La fechaDesde debe coincidir con una fecha de fn de semana epid (sabado)
  //La semana de ese s�bado tambi�n se incluye en la cuenta
  //Si la fechaHasta es anterior a la fechaDesde devuelve -1
  int getNumSemEntre(String fechaDesde, String fechaHasta) throws Exception {
    long iNumSem; //Cociente
    java.util.Date dFecha1;
    java.util.Date dFecha2;
    Calendar calen = new GregorianCalendar();
    //Obtiene el a�o cronol�gico
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    //Pasa el string de fecha a Date
    dFecha1 = formater.parse(fechaDesde);
    //Pasa el string de fecha a Date
    dFecha2 = formater.parse(fechaHasta);

    //Si las dos fechas son iguales es la misma semana  (una semana)
    if (dFecha2.getTime() == dFecha1.getTime()) {
      return (1);
    }
    //Si la fechaHasta es posterior a la fechaDesde entonces
    else if (dFecha2.getTime() > dFecha1.getTime()) {
      calen.setTime(dFecha2);
      //Se calcula el num de miliseg entre las 2 fechas y se divide entre el num
      // de miliseg. que tiene una semana
      iNumSem = (dFecha2.getTime() - dFecha1.getTime())
          / ( (long) (1000 * 3600 * 24 * 7));
      // Si la segunda fecha pilla en sabado, adem�s se a�aden 1 semanas  (la que finalizo en fechaDesde)
      if (calen.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
        return ( (int) iNumSem + 1);
      }
      //En caso contrario se a�aden 2 semanas  (la que finalizo en fechaDesde y la de fechaHasta)
      else {
        return ( (int) iNumSem + 2);
      }
    }
    //Si la fechaHasta es anterior a la fechaDesde entonces se dev -1
    else {
      return -1;
    }

  }

}