package tuberculosis.datos.fechas;

import java.io.Serializable;
import java.util.Vector;

public class DataConvFec
    implements Serializable {

  //Para generar todas las fechas
  protected String sFecFinAnt = ""; //fecha ult sem a�o epidem anterior
  protected String sAnoIni = ""; //A�o de partida
  protected int iNumAnos; //numero de a�os de los que queremos generar fechas

  //Para las fechas de un a�o
  protected String sAno = ""; //A�o
  protected Vector vFec = new Vector(); //Vector que contiene los FecyNum de ese a�o
  protected boolean bHayFec; //Dice si hay fechas o no

  protected String sComAno; //Fecha comienzo del a�o epid.

  //Para generar fechas
  public DataConvFec(String fecFinAnt, String anoIni, int numAnos) {
    sFecFinAnt = fecFinAnt;
    sAnoIni = anoIni;
    iNumAnos = numAnos;
  }

  //Para pedir las fechas de un a�o
  public DataConvFec(String ano) {
    sAno = ano;
  }

  //Para recibir las fechas de un a�o
  public DataConvFec(String ano, Vector fec, boolean hayFec) {
    sAno = ano;
    vFec = fec;
    bHayFec = hayFec;
  }

  public String getFecFinAnt() {
    return sFecFinAnt;
  }

  public String getAnoIni() {
    return sAnoIni;
  }

  public int getNumAnos() {
    return iNumAnos;
  }

  public String getAno() {
    return sAno;
  }

  public Vector getFec() {
    return vFec;
  }

  public boolean getHayFec() {
    return bHayFec;
  }

  public String getComAno() {
    return sComAno;
  }

  public void setComAno(String comAno) {
    sComAno = comAno;
  }

}
