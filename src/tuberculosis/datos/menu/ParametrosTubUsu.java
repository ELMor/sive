/**
 * Clase: ParametrosTubUsu
 * Paquete: tuberculosis.datos.menu
 * Fecha Inicio: 29/02/2000
 */

package tuberculosis.datos.menu;

import java.io.Serializable;
import java.net.URLEncoder;
import java.util.Hashtable;

// Hashtable con los par�metros de las sesi�n que no son recuperables
// de las tablas GAT
public class ParametrosTubUsu
    extends Hashtable
    implements Serializable {

  // lectura de par�metros de tipo cadena
  public String getString(String s, boolean b) throws Exception {
    String data = (String) get(s);

    // par�metro requerido
    if (b) {

      // falta par�metro
      if (data == null) {
        throw new Exception("Falta par�metro: " + s);
      }
      else {

        // par�metro vacio
        if (data.length() == 0) {
          throw new Exception("Par�metro vacio: " + s);
        }
      }
    }
    else {
      if (data == null) {
        data = "";
      }
    }

    return data;
  }

  // lectura de par�metros de tipo entero
  public Integer getInteger(String s, boolean b) throws Exception {
    Integer data = (Integer) get(s);

    // par�metro requerido
    if (b) {

      // falta par�metro
      if (data == null) {
        throw new Exception("Falta par�metro: " + s);
      }
    }

    return data;
  }

  // tranforma la hash table en par�metros url
  public String toURL(String app) {
    String url;

    try {
      // par�metros de env�o
      url = "?APPLET=" + app +
          "&IDIOMA=" + URLEncoder.encode(getInteger("IDIOMA", true).toString()) +
          "&LOGIN=" + URLEncoder.encode(getString("LOGIN", true)) +
          "&URL_SERVLET=" + URLEncoder.encode(getString("URL_SERVLET", true)) +
          "&FTP_SERVER=" + URLEncoder.encode(getString("FTP_SERVER", true)) +
          "&URL_HTML=" + URLEncoder.encode(getString("URL_HTML", true)) +
          //*E "&DS_EMAIL=" + URLEncoder.encode(getString("DS_EMAIL", false)) +
           "&PERFIL=" + URLEncoder.encode(getString("PERFIL", true)) +
           "&CA=" + URLEncoder.encode(getString("CA", true)) +
           "&NIVEL1=" + URLEncoder.encode(getString("NIVEL1", true)) +
           "&NIVEL2=" + URLEncoder.encode(getString("NIVEL2", true)) +
           "&NIVEL3=" + URLEncoder.encode(getString("NIVEL3", true)) +
           "&TSIVE=" + URLEncoder.encode(getString("TSIVE", true)) +
           //"&IDIOMA_LOCAL=" + URLEncoder.encode(getString("IDIOMA_LOCAL", false)) +
           //*E "&IT_AUTALTA=" + URLEncoder.encode(getString("IT_AUTALTA", true)) +
            //*E "&IT_AUTBAJA=" + URLEncoder.encode(getString("IT_AUTBAJA", true)) +
             //*E "&IT_AUTMOD=" + URLEncoder.encode(getString("IT_AUTMOD", true)) +
              "&IT_FG_ENFERMO=" + URLEncoder.encode(getString("IT_FG_ENFERMO", true)) +
              //"&IT_FG_VALIDAR=" + URLEncoder.encode(getString("IT_FG_VALIDAR", true)) +
              //*E "&IT_MODULO_3=" + URLEncoder.encode(getString("IT_MODULO_3", true)) +
               "&IT_TRAMERO=" + URLEncoder.encode(getString("IT_TRAMERO", true)) +
               //*E "&IT_FG_MNTO=" + URLEncoder.encode(getString("IT_FG_MNTO", true)) +
                //*E "&IT_FG_MNTO_USU=" + URLEncoder.encode(getString("IT_FG_MNTO_USU", true)) +
                 //*E "&IT_FG_TCNE=" + URLEncoder.encode(getString("IT_FG_TCNE", true)) +
                  //*E "&IT_FG_PROTOS=" + URLEncoder.encode(getString("IT_FG_PROTOS", true)) +
                   //"&IT_FG_ALARMAS=" + URLEncoder.encode(getString("IT_FG_ALARMAS", true)) +
                   //"&IT_FG_EXPORT=" + URLEncoder.encode(getString("IT_FG_EXPORT", true)) +
                   //"&IT_FG_GENALAUTO=" + URLEncoder.encode(getString("IT_FG_GENALAUTO", true)) +
                   //"&IT_FG_MNOTIFS=" + URLEncoder.encode(getString("IT_FG_MNOTIFS", true)) +
                   //"&IT_FG_CONSRES=" + URLEncoder.encode(getString("IT_FG_CONSRES", true)) +
                   //*E "&CD_E_NOTIF=" + URLEncoder.encode(getString("CD_E_NOTIF", false)) +
                    //*E "&DS_E_NOTIF=" + URLEncoder.encode(getString("DS_E_NOTIF", false)) +
                     //*E "&CD_NIVEL_1_EQ=" + URLEncoder.encode(getString("CD_NIVEL_1_EQ", false)) +
                      //*E "&CD_NIVEL_2_EQ=" + URLEncoder.encode(getString("CD_NIVEL_2_EQ", false)) +
                       //*E "&DS_NIVEL_1_EQ=" + URLEncoder.encode(getString("DS_NIVEL_1_EQ", false)) +
                        //*E "&DS_NIVEL_2_EQ=" + URLEncoder.encode(getString("DS_NIVEL_2_EQ", false)) +
                         "&CD_NIVEL_1_AUTORIZACIONES=" +
                         URLEncoder.encode(
                         getString("CD_NIVEL_1_AUTORIZACIONES", false)) +
                         "&CD_NIVEL_2_AUTORIZACIONES=" +
                         URLEncoder.encode(
                         getString("CD_NIVEL_2_AUTORIZACIONES", false)) +
                         "&FC_ACTUAL=" +
                         URLEncoder.encode(getString("FC_ACTUAL", true)) +
                         "&CD_NIVEL_1_DEFECTO=" +
                         URLEncoder.encode(getString("CD_NIVEL_1_DEFECTO", false)) +
                         "&CD_NIVEL_2_DEFECTO=" +
                         URLEncoder.encode(getString("CD_NIVEL_2_DEFECTO", false)) +
                         "&DS_NIVEL_1_DEFECTO=" +
                         URLEncoder.encode(getString("DS_NIVEL_1_DEFECTO", false)) +
                         "&DS_NIVEL_2_DEFECTO=" +
                         URLEncoder.encode(getString("DS_NIVEL_2_DEFECTO", false)) +
                         "&ANYO_DEFECTO=" +
                         URLEncoder.encode(getString("ANYO_DEFECTO", false)) +
                         //*E "&IT_FG_RESOLVER_CONFLICTOS=" + URLEncoder.encode(getString("IT_FG_RESOLVER_CONFLICTOS", false)) +
                          // Propios de USU
                          "&IT_USU=" + URLEncoder.encode(getString("IT_USU", false)) +
                          "&COD_APLICACION=" +
                          URLEncoder.encode(getString("COD_APLICACION", false)) +
                          "&CD_TUBERCULOSIS=" +
                          URLEncoder.encode(getString("CD_TUBERCULOSIS", false));

    }
    catch (Exception e) {
      e.printStackTrace();
      url = "";
    }

    //System.out.println(url);

    return url;
  }
}