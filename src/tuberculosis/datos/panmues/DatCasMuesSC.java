/**
 * Clase: DatCasMuesSC
 * Paquete: tuberculosis.datos.panmues
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 30/11/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: Estructura de datos que contiene los datos asociados
 *   a una MUESTRA, resultado de la busqueda de muestras
 *   asociadas a un NOTIFICADOR.
 */

package tuberculosis.datos.panmues;

import java.io.Serializable;

//import capp.*;

public class DatCasMuesSC
    implements Serializable {

  // Datos resultado de la busqueda de un unico tratamiento
  private String NM_RESLAB = "";
  private String FC_MUESTRA = "";
  private String CD_TTECLAB = "";
  private String DS_TTECLAB = "";
  private String CD_MUESTRA = "";
  private String DS_MUESTRA = "";
  private String CD_VMUESTRA = "";
  private String DS_VMUESTRA = "";
  private String CD_TMICOB = "";
  private String DS_TMICOB = "";
  private String IT_RESISTENTE = "";
  private String CD_OPE = "";
  private String FC_ULTACT = "";
  //private CLista RESISTENCIAS = null;

  public DatCasMuesSC(String NumMuestra,
                      String FMuestra,
                      String CodTLab,
                      String DescTLab,
                      String CodMuestra,
                      String DescMuestra,
                      String CodVMuestra,
                      String DescVMuestra,
                      String CodTMicob,
                      String DescTMicob,
                      String FlagResis,
                      String Ope,
                      String FUlt
                      //CLista Resistencias
                      ) { // Constructor

    if (NumMuestra != null) {
      NM_RESLAB = NumMuestra;
    }
    else {
      NM_RESLAB = "";
    }
    if (FMuestra != null) {
      FC_MUESTRA = FMuestra;
    }
    else {
      FC_MUESTRA = "";
    }
    if (CodTLab != null) {
      CD_TTECLAB = CodTLab;
    }
    else {
      CD_TTECLAB = "";
    }
    if (DescTLab != null) {
      DS_TTECLAB = DescTLab;
    }
    else {
      DS_TTECLAB = "";
    }
    if (CodMuestra != null) {
      CD_MUESTRA = CodMuestra;
    }
    else {
      CD_MUESTRA = "";
    }
    if (DescMuestra != null) {
      DS_MUESTRA = DescMuestra;
    }
    else {
      DS_MUESTRA = "";
    }
    if (CodVMuestra != null) {
      CD_VMUESTRA = CodVMuestra;
    }
    else {
      CD_VMUESTRA = "";
    }
    if (DescVMuestra != null) {
      DS_VMUESTRA = DescVMuestra;
    }
    else {
      DS_VMUESTRA = "";
    }
    if (CodTMicob != null) {
      CD_TMICOB = CodTMicob;
    }
    else {
      CD_TMICOB = "";
    }
    if (DescTMicob != null) {
      DS_TMICOB = DescTMicob;
    }
    else {
      DS_TMICOB = "";
    }
    if (FlagResis != null) {
      IT_RESISTENTE = FlagResis;
    }
    else {
      IT_RESISTENTE = "";
    }
    if (Ope != null) {
      CD_OPE = Ope;
    }
    else {
      CD_OPE = "";
    }
    if (FUlt != null) {
      FC_ULTACT = FUlt;
    }
    else {
      FC_ULTACT = "";
      //RESISTENCIAS = Resistencias;
    }
  }

  public String getNM_RESLAB() {
    return NM_RESLAB;
  }

  public void setNM_RESLAB(String n) {
    NM_RESLAB = n;
  }

  public String getFC_MUESTRA() {
    return FC_MUESTRA;
  }

  public String getCD_TTECLAB() {
    return CD_TTECLAB;
  }

  public String getDS_TTECLAB() {
    return DS_TTECLAB;
  }

  public String getCD_MUESTRA() {
    return CD_MUESTRA;
  }

  public String getDS_MUESTRA() {
    return DS_MUESTRA;
  }

  public String getCD_VMUESTRA() {
    return CD_VMUESTRA;
  }

  public String getDS_VMUESTRA() {
    return DS_VMUESTRA;
  }

  public String getCD_TMICOB() {
    return CD_TMICOB;
  }

  public String getDS_TMICOB() {
    return DS_TMICOB;
  }

  public String getIT_RESISTENTE() {
    return IT_RESISTENTE;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }

  public void setCD_OPE(String ope) {
    CD_OPE = ope;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public void setFC_ULTACT(String fultact) {
    FC_ULTACT = fultact;
  }

  /*public CLista getRESISTENCIAS() {
    return RESISTENCIAS;
     }*/

} // endclass DatCasMuesSC
