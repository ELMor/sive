/**
 * Clase: DatCasResis
 * Paquete: tuberculosis.datos.panmues
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 01/12/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: Estructura de datos que contiene los datos asociados
 *   a una RESISTENCIA, resultado de la busqueda de resistencias
 *   asociadas a una MUESTRA.
 */

package tuberculosis.datos.panmues;

import java.io.Serializable;

public class DatCasResis
    implements Serializable {

  // Resistencia
  private String CD_ESTREST = "";
  private String DS_ESTREST = "";
  private String CD_OPE = "";
  private String FC_ULTACT = "";

  public DatCasResis(String CodRes,
                     String DescRes,
                     String Ope,
                     String FUlt) { // Constructor
    if (CodRes != null) {
      CD_ESTREST = CodRes;
    }
    else {
      CD_ESTREST = "";
    }
    if (DescRes != null) {
      DS_ESTREST = DescRes;
    }
    else {
      DS_ESTREST = "";
    }
    if (Ope != null) {
      CD_OPE = Ope;
    }
    else {
      CD_OPE = "";
    }
    if (FUlt != null) {
      FC_ULTACT = FUlt;
    }
    else {
      FC_ULTACT = "";
    }
  }

  public String getCD_ESTREST() {
    return CD_ESTREST;
  }

  public void setCD_ESTREST(String n) {
    CD_ESTREST = n;
  }

  public String getDS_ESTREST() {
    return DS_ESTREST;
  }

  public void setDS_ESTREST(String n) {
    DS_ESTREST = n;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }

  public void setCD_OPE(String ope) {
    CD_OPE = ope;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public void setFC_ULTACT(String fultact) {
    FC_ULTACT = fultact;
  }

} // endclass DatCasResis
