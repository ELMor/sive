/**
 * Clase: DatCasTratSC
 * Paquete: tuberculosis.datos.pantrat
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 22/11/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: Estructura de datos que contiene los datos asociados
 *   a un TRATAMIENTO, resultado de la busqueda de tratamientos
 *   asociados a un NOTIFICADOR.
 */

package tuberculosis.datos.pantrat;

import java.io.Serializable;

public class DatCasTratSC
    implements Serializable {

  // Datos resultado de la busqueda de un unico tratamiento
  private String NM_TRATRTBC = "";
  private String FC_INITRAT = "";
  private String CD_MOTRATINI = "";
  private String DS_MOTRATINI = "";
  private String FC_FINTRAT = "";
  private String CD_MOTRATFIN = "";
  private String DS_MOTRATFIN = "";
  private String DS_OBSERV = "";
  private String CD_OPE = "";
  private String FC_ULTACT = "";

  public DatCasTratSC(String NumTrat,
                      String FInicio,
                      String MotInicio,
                      String DescMotIni,
                      String FFin,
                      String MotFin,
                      String DescMotFin,
                      String Observ,
                      String Ope,
                      String FUlt) { // Constructor

    if (NumTrat != null) {
      NM_TRATRTBC = NumTrat;
    }
    else {
      NM_TRATRTBC = "";
    }
    if (FInicio != null) {
      FC_INITRAT = FInicio;
    }
    else {
      FC_INITRAT = "";
    }
    if (MotInicio != null) {
      CD_MOTRATINI = MotInicio;
    }
    else {
      CD_MOTRATINI = "";
    }
    if (DescMotIni != null) {
      DS_MOTRATINI = DescMotIni;
    }
    else {
      DS_MOTRATINI = "";
    }
    if (FFin != null) {
      FC_FINTRAT = FFin;
    }
    else {
      FC_FINTRAT = "";
    }
    if (MotFin != null) {
      CD_MOTRATFIN = MotFin;
    }
    else {
      CD_MOTRATFIN = "";
    }
    if (DescMotFin != null) {
      DS_MOTRATFIN = DescMotFin;
    }
    else {
      DS_MOTRATFIN = "";
    }
    if (Observ != null) {
      DS_OBSERV = Observ;
    }
    else {
      DS_OBSERV = "";
    }
    if (Ope != null) {
      CD_OPE = Ope;
    }
    else {
      CD_OPE = "";
    }
    if (FUlt != null) {
      FC_ULTACT = FUlt;
    }
    else {
      FC_ULTACT = "";
    }
  }

  public String getNM_TRATRTBC() {
    return NM_TRATRTBC;
  }

  public void setNM_TRATRTBC(String n) {
    NM_TRATRTBC = n;
  }

  public String getFC_INITRAT() {
    return FC_INITRAT;
  }

  public String getCD_MOTRATINI() {
    return CD_MOTRATINI;
  }

  public String getDS_MOTRATINI() {
    return DS_MOTRATINI;
  }

  public String getFC_FINTRAT() {
    return FC_FINTRAT;
  }

  public String getCD_MOTRATFIN() {
    return CD_MOTRATFIN;
  }

  public String getDS_MOTRATFIN() {
    return DS_MOTRATFIN;
  }

  public String getDS_OBSERV() {
    return DS_OBSERV;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }

  public void setCD_OPE(String ope) {
    CD_OPE = ope;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public void setFC_ULTACT(String fultact) {
    FC_ULTACT = fultact;
  }

} // endclass DatCasTratSC