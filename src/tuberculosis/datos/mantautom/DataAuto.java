package tuberculosis.datos.mantautom;

import java.io.Serializable;

public class DataAuto
    implements Serializable {

  public String CD_ARTBC = "";
  public String CD_NRTBC = "";
  public String CD_NIVEL_1_GE = "";
  public String CD_NIVEL_2_GE = "";
  public String DS_NIVEL_1_GE = "";
  public String DS_NIVEL_2_GE = "";
  //notificador  + BLOQUEO
  public String CD_ANOEPI = "";
  public String CD_SEMEPI = "";
  public String FC_RECEP = "";
  public String FC_FECNOTIF = "";
  public String CD_E_NOTIF = "";
  public String DS_CENTRO = "";
  public String CD_FUENTE = "";
  public String NM_EDO = "";
  public String CD_OPE_RT = "";
  public String FC_ULTACT_RT = "";
  //TRATAMIENTO
  public String NM_TRATRTBC = "";
  public String FC_INITRAT = "";
  public String FC_FINTRAT = "";
  public String FC_ACTUAL = "";

  public String MAX_FC_FINTRAT = "";
  public String MAX_FC_INITRAT = "";
  public String MIN_FC_FINTRAT = "";
  public String MIN_FC_INITRAT = "";

  public DataAuto(
      String cd_artbc,
      String cd_nrtbc,
      String cd_anoepi,
      String cd_semepi,
      String fc_recep,
      String fc_fecnotif,
      String cd_e_notif,
      String ds_centro,
      String cd_fuente,
      String nm_edo,
      String cd_ope_rt,
      String fc_ultact_rt,
      String nm_tratrtbc,
      String fc_initrat,
      String fc_fintrat,
      String fc_actual,
      String max_fc_fintrat,
      String max_fc_initrat,
      String min_fc_fintrat,
      String min_fc_initrat) {

    CD_ARTBC = cd_artbc;
    CD_NRTBC = cd_nrtbc;
    CD_ANOEPI = cd_anoepi;
    CD_SEMEPI = cd_semepi;
    FC_RECEP = fc_recep;
    FC_FECNOTIF = fc_fecnotif;
    CD_E_NOTIF = cd_e_notif;
    DS_CENTRO = ds_centro;
    CD_FUENTE = cd_fuente;
    NM_EDO = nm_edo;
    CD_OPE_RT = cd_ope_rt;
    FC_ULTACT_RT = fc_ultact_rt;
    NM_TRATRTBC = nm_tratrtbc;
    FC_INITRAT = fc_initrat;
    FC_FINTRAT = fc_fintrat;
    FC_ACTUAL = fc_actual;
    MAX_FC_FINTRAT = max_fc_fintrat;
    MAX_FC_INITRAT = max_fc_initrat;
    MIN_FC_FINTRAT = min_fc_fintrat;
    MIN_FC_INITRAT = min_fc_initrat;

  }

  public DataAuto() {}

  public String getCD_ARTBC() {
    return CD_ARTBC;
  }

  public String getCD_NRTBC() {
    return CD_NRTBC;
  }

  public String getCD_ANOEPI() {
    return CD_ANOEPI;
  }

  public String getCD_SEMEPI() {
    return CD_SEMEPI;
  }

  public String getFC_RECEP() {
    return FC_RECEP;
  }

  public String getFC_FECNOTIF() {
    return FC_FECNOTIF;
  }

  public String getCD_E_NOTIF() {
    return CD_E_NOTIF;
  }

  public String getDS_CENTRO() {
    return DS_CENTRO;
  }

  public String getCD_FUENTE() {
    return CD_FUENTE;
  }

  public String getNM_EDO() {
    return NM_EDO;
  }

  public String getCD_OPE_RT() {
    return CD_OPE_RT;
  }

  public String getFC_ULTACT_RT() {
    return FC_ULTACT_RT;
  }

  public String getNM_TRATRTBC() {
    return NM_TRATRTBC;
  }

  public String getFC_INITRAT() {
    return FC_INITRAT;
  }

  public String getFC_FINTRAT() {
    return FC_FINTRAT;
  }

  public String getFC_ACTUAL() {
    return FC_ACTUAL;
  }

  public String getCD_NIVEL_1_GE() {
    return CD_NIVEL_1_GE;
  }

  public String getCD_NIVEL_2_GE() {
    return CD_NIVEL_2_GE;
  }

  public String getDS_NIVEL_1_GE() {
    return DS_NIVEL_1_GE;
  }

  public String getDS_NIVEL_2_GE() {
    return DS_NIVEL_2_GE;
  }

  public String getMAX_FC_FINTRAT() {
    return MAX_FC_FINTRAT;
  }

  public String getMAX_FC_INITRAT() {
    return MAX_FC_INITRAT;
  }

  public String getMIN_FC_FINTRAT() {
    return MIN_FC_FINTRAT;
  }

  public String getMIN_FC_INITRAT() {
    return MIN_FC_INITRAT;
  }

}
