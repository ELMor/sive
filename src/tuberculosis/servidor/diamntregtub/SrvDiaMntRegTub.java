/**
 * Clase: SrvDiaMntRegTub
 * Paquete: tuberculosis.servidor.diamntregtub
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 10/11/1999
 * Analisis Funcional: Punto 2. Mantenimiento Registro Tuberculosis.
 * Descripcion: Implementacion del servlet que recupera de la BD los datos
 * de un determinado registro de tuberculosis.
 * Modificado: 29/12/1999 / Emilio Postigo Riancho
 * Descripci�n del cambio: se a�adi� el tratamiento de bloqueos
 */

package tuberculosis.servidor.diamntregtub;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import capp.CLista;
import comun.Fechas;
import comun.constantes;
import jdbcpool.JDCConnectionDriver;
import sapp.DBServlet;
import tuberculosis.datos.diamntregtub.DatDiaMntRegTub;

public class SrvDiaMntRegTub
    extends DBServlet {

  // Modos de operacion de este servlet
  private final int servletSELECT = constantes.modoSELECT;
  private final int servletUPDATE = constantes.modoMODIFICACION;
  private final int servletUPDATE_SB = constantes.modoMODIFICACION_SB;

  Connection conexion = null;

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista();
    CLista listaSalidaprovisional = new CLista();
    DatDiaMntRegTub dataEntrada = null;
    DatDiaMntRegTub dataSalida = null;

    // Querys
    String sQuery = "";
    String sIdentidadEnfermo = "";

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    if (conexion == null) {
      con = openConnection();
    }
    else {
      con = conexion;
    }

    con.setAutoCommit(false);

    // Para gestionar el bloqueo
    boolean bBloqueo = true;

    try {
      // Recuperacion de los datos de busqueda
      dataEntrada = (DatDiaMntRegTub) param.firstElement();

      switch (opmode) {
        case servletSELECT:

          // Dependiendo del flag de confidencialidad se obtienen unos
          //  u otros datos acerca de los enfermos
          boolean bConfidencial = dataEntrada.getIT_FG_CONFIDENCIAL().trim().
              equals("S");
          if (bConfidencial) {
            sIdentidadEnfermo = "e.SIGLAS";
          }
          else {
            sIdentidadEnfermo = "e.DS_APE1, e.DS_APE2, e.DS_NOMBRE";

            // Preparacion del patron de la query
          }
          sQuery = "SELECT r.CD_ARTBC, r.CD_NRTBC, " +
              " r.CD_NIVEL_1_GE, r.CD_NIVEL_2_GE, r.FC_INIRTBC, " +
              " e.CD_ENFERMO, " +
              sIdentidadEnfermo +
              ", e.FC_NAC, e.IT_CALC, e.CD_SEXO, e.DS_TELEF, " +
              "e.CD_PROV, e.CD_MUN, e.DS_DIREC, e.DS_PISO, e.DS_NUM, e.CD_POSTAL, " +
              "r.CD_E_NOTIF, r.DS_MEDICO, r.DS_OBSERV, r.FC_SALRTBC, r.CD_MOTSALRTBC, " +
              "r.CD_OPE, r.FC_ULTACT" +
              " FROM SIVE_REGISTROTBC r, SIVE_EDOIND i, SIVE_ENFERMO e" +
              " WHERE (i.NM_EDO = r.NM_EDO AND e.CD_ENFERMO = i.CD_ENFERMO ";

          // No hay tramado ya que la busqueda devuelve un unico registro de tuberculosis

          // Terminacion de la query en funcion del a�o/registro
          sQuery = sQuery + TerminarPreparacionQuerySELECT() + " )"; ;

          // No hay ordenacion ya que el resultado es un solo registro

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciamos la query con los valores de los datos de entrada
          InstanciarQuerySELECT(dataEntrada, st);

          // Ejecucion de la query, obteniendo un ResultSet

          rs = st.executeQuery();
          String n1 = "";
          String n2 = "";
          String desn1 = "";
          String desn2 = "";
          String CodMun = "";
          String DescMun = "";
          String CodEqNotif = "";
          String DescEqNotif = "";

          // El resultado solo puede ser un registro, por ello el bucle
          // se ejcuta una vez para procesar el registro obtenido de la BD
          while (rs.next()) {

            // Recuperacion de los datos de la consulta
            String Ano = rs.getString("CD_ARTBC");
            String Reg = rs.getString("CD_NRTBC");
            n1 = rs.getString("CD_NIVEL_1_GE");
            n2 = rs.getString("CD_NIVEL_2_GE");
            desn1 = "";
            desn2 = "";

            String FC_INIRTC = UtilDateToString(rs.getDate("FC_INIRTBC"));

            int iCodEnfermo = rs.getInt("CD_ENFERMO");
            String CodEnfermo = (new Integer(iCodEnfermo)).toString();
            String Ape1 = "";
            String Ape2 = "";
            String Nombre = "";
            String Siglas = "";
            String FNac = UtilDateToString(rs.getDate("FC_NAC"));
            String FEdad = "";
            String Sexo = rs.getString("CD_SEXO");
            String Telef = rs.getString("DS_TELEF");
            String CA = "";
            String Prov = rs.getString("CD_PROV");
            CodMun = rs.getString("CD_MUN");
            DescMun = "";
            String Direc = rs.getString("DS_DIREC");
            String Num = rs.getString("DS_NUM");
            String Piso = rs.getString("DS_PISO");
            String CodPostal = rs.getString("CD_POSTAL");
            CodEqNotif = rs.getString("CD_E_NOTIF");
            DescEqNotif = "";
            String Medico = rs.getString("DS_MEDICO");
            String Observ = rs.getString("DS_OBSERV");
            String FSalida = UtilDateToString(rs.getDate("FC_SALRTBC"));
            String MotSalida = rs.getString("CD_MOTSALRTBC");
            String CodOpe = rs.getString("CD_OPE");

            String FUltact = timestamp_a_cadena(rs.getTimestamp("FC_ULTACT"));
            String FConf = "";

            // Flag de calculo de la fecha
            if (rs.getString("IT_CALC").equals("S")) {
              FEdad = "S";
            }
            else {
              FEdad = "N";

              // Segun el flag de confidencialidad se recuperan unos datos u otros
            }
            if (bConfidencial) {
              Siglas = rs.getString("SIGLAS");
            }
            else {
              Ape1 = rs.getString("DS_APE1");
              Ape2 = rs.getString("DS_APE2");
              Nombre = rs.getString("DS_NOMBRE");
            }

            // CA
            CA = getCA(Prov);

            // Relleno del registro de salida correspondiente a esta iteracion por
            //  los registros devueltos por la query
            dataSalida = new DatDiaMntRegTub(Ano, Reg, CodEnfermo,
                                             Ape1, Ape2, Nombre, Siglas, FNac,
                                             FEdad, Sexo, Telef, CA, Prov,
                                             CodMun,
                                             DescMun, Direc, Num, Piso,
                                             CodPostal, CodEqNotif, DescEqNotif,
                                             Medico,
                                             Observ, FSalida, MotSalida, CodOpe,
                                             FUltact, FConf, FC_INIRTC,
                                             n1, n2, desn1, desn2);

            // Adicion del registro obtenido a la lista resultado de salida hacia cliente
            listaSalida.addElement(dataSalida);
          } // Fin for each registro

          // Cierre del ResultSet y del PreparedStatement
          rs.close();
          rs = null;
          st.close();
          st = null;

          for (int i = 0; i < listaSalida.size(); i++) {
            DatDiaMntRegTub data = (DatDiaMntRegTub) listaSalida.elementAt(i);

            // Descripcion Municipio
            if (data.getCD_MUN() == null || data.getCD_MUN().equals("")) {}
            else {
              //sQuery = "select DS_MUN from SIVE_MUNICIPIO where CD_MUN = ?";
              sQuery =
                  "select DSMUNI from SUCA_MUNICIPIO where CDMUNI = ? and CDPROV = ? ";

              st = con.prepareStatement(sQuery);

              st.setString(1, data.getCD_MUN().trim());
              st.setString(2, "28"); // Nuevo

              rs = st.executeQuery();

              rs.next();

              //DescMun =  rs.getString("DS_MUN") ;
              DescMun = rs.getString("DSMUNI");

              rs.close();
              rs = null;
              st.close();
              st = null;
            }
            // Descripcion Equipo Notificador
            if (data.getCD_E_NOTIF() == null ||
                data.getCD_E_NOTIF().equals("")) {}
            else {
              sQuery =
                  "select DS_E_NOTIF from SIVE_E_NOTIF where CD_E_NOTIF = ?";
              st = con.prepareStatement(sQuery);
              st.setString(1, data.getCD_E_NOTIF().trim());
              rs = st.executeQuery();

              rs.next();
              DescEqNotif = rs.getString("DS_E_NOTIF");

              rs.close();
              rs = null;
              st.close();
              st = null;
            }

            //desc n1  -----------------------------
            if (data.getCD_NIVEL_1_GE() == null ||
                data.getCD_NIVEL_1_GE().equals("")) {}
            else {

              sQuery = "SELECT DS_NIVEL_1 " +
                  "FROM SIVE_NIVEL1_S WHERE CD_NIVEL_1 = ?";

              st = con.prepareStatement(sQuery);
              st.setString(1, data.getCD_NIVEL_1_GE().trim());
              rs = st.executeQuery();

              rs.next();
              desn1 = rs.getString("DS_NIVEL_1");

              rs.close();
              rs = null;
              st.close();
              st = null;
            }
            //-----------------------------------
            //desc n2  -----------------------------
            if (data.getCD_NIVEL_2_GE() == null ||
                data.getCD_NIVEL_2_GE().equals("")) {}
            else {

              sQuery = "SELECT DS_NIVEL_2 " +
                  "FROM SIVE_NIVEL2_S WHERE CD_NIVEL_1 = ? and CD_NIVEL_2 = ?";

              st = con.prepareStatement(sQuery);
              st.setString(1, data.getCD_NIVEL_1_GE().trim());
              st.setString(2, data.getCD_NIVEL_2_GE().trim());
              rs = st.executeQuery();

              rs.next();
              desn2 = rs.getString("DS_NIVEL_2");

              rs.close();
              rs = null;
              st.close();
              st = null;
            }
            //-----------------------------------

            DatDiaMntRegTub dataCompleto = new DatDiaMntRegTub(data.getCD_ARTBC(),
                data.getCD_NRTBC(), data.getCD_ENFERMO(),
                data.getDS_APE1(), data.getDS_APE2(),
                data.getDS_NOMBRE(), data.getSIGLAS(),
                data.getFC_NAC(), data.getIT_FG_CALC(), data.getCD_SEXO(),
                data.getDS_TELEF(), data.getDS_CA(),
                data.getDS_PROV(), data.getCD_MUN(),
                DescMun, data.getDS_DIREC(),
                data.getDS_NUM(), data.getDS_PISO(),
                data.getCD_POSTAL(),
                data.getCD_E_NOTIF(), DescEqNotif,
                data.getDS_MEDICO(),
                data.getDS_OBSERV(), data.getFC_SALRTBC(),
                data.getCD_MOTSALRTBC(),
                data.getCD_OPE(), data.getFC_ULTACT(),
                data.getIT_FG_CONFIDENCIAL(), data.getFC_INIRTBC(),
                data.getCD_NIVEL_1_GE(), data.getCD_NIVEL_2_GE(),
                desn1, desn2);

            listaSalidaprovisional.addElement(dataCompleto);
          }
          listaSalida = new CLista();
          for (int i = 0; i < listaSalidaprovisional.size(); i++) {
            listaSalida.addElement( (DatDiaMntRegTub) listaSalidaprovisional.
                                   elementAt(i));
          }

          // Validacion de la transacci�n
          con.commit();

          break;
        case servletUPDATE_SB:
          bBloqueo = false;
        case servletUPDATE:

          // Preparacion del patron de la query
          sQuery = "UPDATE SIVE_REGISTROTBC SET CD_E_NOTIF = ?, " +
              "DS_MEDICO = ?, DS_OBSERV = ?, FC_SALRTBC = ?, CD_MOTSALRTBC = ?, " +
              " FC_INIRTBC = ?, CD_NIVEL_1_GE = ?,  CD_NIVEL_2_GE = ?, " +
              "CD_OPE = ?, FC_ULTACT = ? " +
              "WHERE CD_ARTBC = ? AND CD_NRTBC = ? ";

          // Si hay bloqueo...
          if (bBloqueo) {
            sQuery = sQuery + " and CD_OPE = ? and FC_ULTACT = ? ";
          }

          // No hay tramado ya que es la actualizacion de un registro

          // No hay ordenacion ya que es la actualizacion de un registro

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciamos la query con los valores de los datos de entrada
          InstanciarQueryUPDATE(dataEntrada, st, param.getLogin(), bBloqueo);

          // Ejecucion de la query, obteniendo el numero de filas actualizadas

          int state = st.executeUpdate();

          // Que resultado enviamos al cliente: NINGUNO

          // Pero si se ha tenido en cuenta bloqueo y no se ha grabado ninguno...
          if (state == 0 && bBloqueo) {
            throw new Exception(constantes.PRE_BLOQUEO +
                                constantes.SIVE_REGISTROTBC);
          }

          // Cierre del y del PreparedStatement
          st.close();
          st = null;

          // Validacion de la transacci�n
          con.commit();

          break;
      } // Fin switch

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);

    return listaSalida;
  } // Fin doWork()

  // Termina la preparacion de la query del SELECT
  protected String TerminarPreparacionQuerySELECT() {

    // Registros de un a�o determinado (obligatorio)
    String sQ = " AND r.CD_ARTBC = ? ";

    // Un determinado numero de registro (obligatorio)
    sQ = sQ + " AND r.CD_NRTBC = ? ";

    return sQ;
  } // Fin TerminarPreparacionQuerySELECT()

  // Instanciamos la query (modificando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void InstanciarQuerySELECT(DatDiaMntRegTub dataEntrada,
                                       PreparedStatement st) throws Exception {

    // Contador de posicion del parametro
    int iParN = 1;

    // A�o (obligatorio)
    st.setString(iParN++, dataEntrada.getCD_ARTBC().trim());

    // Registro (unico)
    st.setString(iParN++, dataEntrada.getCD_NRTBC().trim());

  } // Fin InstanciarQuerySELECT()

  // Preparacion de la query del UPDATE (parte del SET)
  protected String PreparacionQueryUPDATE(DatDiaMntRegTub dataEntrada) {

    String sQ = "SET";

    if (dataEntrada.getCD_E_NOTIF().trim().length() > 0) {
      sQ = sQ + " CD_E_NOTIF = ? ,";

    }
    if (dataEntrada.getDS_MEDICO().trim().length() > 0) {
      sQ = sQ + " DS_MEDICO = ? ,";

    }
    if (dataEntrada.getDS_OBSERV().trim().length() > 0) {
      sQ = sQ + " DS_OBSERV = ? ,";

    }
    if (dataEntrada.getFC_SALRTBC().trim().length() > 0) {
      sQ = sQ + " FC_SALRTBC = ? ,";

    }
    if (dataEntrada.getCD_MOTSALRTBC().trim().length() > 0) {
      sQ = sQ + " CD_MOTSALRTBC = ? ,";

    }
    return sQ.substring(0, sQ.length() - 2);
  } // Fin TerminarPreparacionQueryUPDATE()

  // Instanciamos la query (modificando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void InstanciarQueryUPDATE(DatDiaMntRegTub dataEntrada,
                                       PreparedStatement st, String login,
                                       boolean bloqueo) throws Exception {

    // Contador de posicion del parametro
    int iParN = 1;

    // Equipo Notificador
    if (dataEntrada.getCD_E_NOTIF().trim().length() > 0) {
      st.setString(iParN++, dataEntrada.getCD_E_NOTIF().trim());
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      // Medico Facultativo
    }
    if (dataEntrada.getDS_MEDICO().trim().length() > 0) {
      st.setString(iParN++, dataEntrada.getDS_MEDICO().trim());
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      // Observaciones
    }
    if (dataEntrada.getDS_OBSERV().trim().length() > 0) {
      st.setString(iParN++, dataEntrada.getDS_OBSERV().trim());
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      // Fecha de salida
    }
    String FSalida = dataEntrada.getFC_SALRTBC().trim();
    if (FSalida.trim().length() > 0) {
      java.sql.Date fSalida = StringToSQLDate(FSalida);
      st.setDate(iParN++, fSalida);
    }
    else {
      st.setNull(iParN++, java.sql.Types.DATE);

      // Motivo de salida
    }
    if (dataEntrada.getCD_MOTSALRTBC().trim().length() > 0) {
      st.setString(iParN++, dataEntrada.getCD_MOTSALRTBC().trim());
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      // FECHA INI
    }
    java.util.Date dFecha = new java.util.Date();
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    if (dataEntrada.getFC_INIRTBC().trim().length() > 0) {
      dFecha = formater.parse(dataEntrada.getFC_INIRTBC().trim());
      sqlFec = new java.sql.Date(dFecha.getTime());
      st.setDate(iParN++, sqlFec);
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      // Nivel 1
    }
    if (dataEntrada.getCD_NIVEL_1_GE().trim().length() > 0) {
      st.setString(iParN++, dataEntrada.getCD_NIVEL_1_GE().trim());
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);
      // Nivel 2
    }
    if (dataEntrada.getCD_NIVEL_2_GE().trim().length() > 0) {
      st.setString(iParN++, dataEntrada.getCD_NIVEL_2_GE().trim());
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      // C�digo del operario
    }
    st.setString(iParN++, login);

    // Fecha Ultima Actualizacion

    java.util.Date dFechaUltact = new java.util.Date();
    SimpleDateFormat formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
        new Locale("es", "ES"));
    String sFechaUltact = formUltact.format(dFechaUltact);
    java.sql.Timestamp fUltact = cadena_a_timestamp(sFechaUltact);
    st.setTimestamp(iParN++, fUltact);

    // A�o
    st.setString(iParN++, dataEntrada.getCD_ARTBC().trim());

    // Registro
    st.setString(iParN++, dataEntrada.getCD_NRTBC().trim());

    // Si hay bloqueo...
    if (bloqueo) {
      st.setString(iParN++, dataEntrada.getCD_OPE());
      st.setTimestamp(iParN++, Fechas.string2Timestamp(dataEntrada.getFC_ULTACT()));
    }

  } // Fin InstanciarQueryUPDATE()

  // Obtiene el codigo de CA a partir de un codigo de provincia
  String getCA(String Prov) throws Exception {

    if (Prov == null || Prov.equals("")) {
      return "";
    }

    // Objetos JDBC
    Connection conCA = null;
    PreparedStatement stCA = null;
    ResultSet rsCA = null;

    // Query
    //String CAQuery = "select CD_CA from SIVE_PROVINCIA where CD_PROV = ?";
    String CAQuery = "select CDCOMU CD_CA from SUCA_PROVINCIA where CDPROV = ?";

    String res = null;

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    conCA = openConnection();
    conCA.setAutoCommit(false);

    try {
      // Preparacion de la sentencia SQL
      stCA = conCA.prepareStatement(CAQuery);

      // Provincia

      stCA.setString(1, Prov.trim());

      // Ejecucion de la query
      rsCA = stCA.executeQuery();

      // Almacenamos la CA obtenida
      rsCA.next();
      res = rsCA.getString("CD_CA");

      // Cierre del ResultSet y del PreparedStatement
      rsCA.close();
      rsCA = null;
      stCA.close();
      stCA = null;

      // Validacion de la transacci�n
      conCA.commit();

    }
    catch (Exception ex) {
      conCA.rollback();
      throw ex;
    }

    // Cierre de la conexion
    closeConnection(conCA);

    return res;
  } // Fin getCA()

  /**************** Funciones Auxiliares ***********************/

  // Conversor de fecha String a java.sql.Date
  protected java.sql.Date StringToSQLDate(String sFecha) throws Exception {

    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    java.util.Date uFecha = formater.parse(sFecha);
    java.sql.Date sqlFec = new java.sql.Date(uFecha.getTime());
    return sqlFec;
  } // Fin StringToSQLDate()

  // Conversor de fecha java.util.Date a String
  protected String UtilDateToString(java.util.Date uFecha) throws Exception {

    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    String sFecha = "";
    if (uFecha == null) {
      sFecha = "";
    }
    else {
      sFecha = formater.format(uFecha);

    }
    return (sFecha);
  } // Fin UtilDateToString()

  // Conversor de String a Timestamp
  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    //java.sql.Timestamp TSFec = new java.sql.Timestamp(dFecha.getTime());
    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);

    return TSFec;
  } // Fin cadena_a_timestamp()

  // Conversor de Timestamp a String
  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  } // Fin timestamp_a_cadena()

  /** prueba del servlet */
  public CLista doPrueba(int operacion, CLista parametros) throws Exception {
    jdcdriver = new JDCConnectionDriver("oracle.jdbc.driver.OracleDriver",
        "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
        "sive_desa",
        "sive_desa");
    conexion = openConnection();
    return doWork(operacion, parametros);
  }

} // Fin SrvDiaMntRegTub
