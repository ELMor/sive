package tuberculosis.servidor.voltratamientos;

//import util_ficheros.*;//EditorFicheros
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Vector;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;

/**
 * Servlet gen�rico de consulta a al base de datos.
 *
 * @author JRM&AIC
 * @version  1.1 22/03/2000
 */
/*
 * Clase : tuberculosis.servidor.volTratamientos.ServSQL
 *   Autor    Fecha        Accion
 * JRM&AIC  22/03/2000   La implementa
 */

public class SrvSQLTub
    extends DBServlet {

  /**
   * Hilo de ejecuci�n del servlet
   */
  /*
   * M�todo : ServSQL.doWork(int opMode, Lista param)
   *   Autor    Fecha        Accion
   * JRM&AIC  22/03/2000   La implementa
   */

  protected Lista doWork(int opMode, Lista param) throws Exception {
    Statement Query = null;
    ResultSet Resultado = null;
    Data datosConsulta = null;
    Lista listaSalida = null;
    Data resultadoConsulta = null;
    String strConsulta = null;
    Vector Campos = null;

    try {
      con.setAutoCommit(false);
      if (param.size() != 0) {
        datosConsulta = (Data) param.elementAt(0);
        strConsulta = datosConsulta.getString("CONSULTA");
        Campos = (Vector) datosConsulta.get("CAMPOS");
        listaSalida = new Lista();
        Query = con.createStatement();
        Resultado = Query.executeQuery(strConsulta);

        while (Resultado.next()) {
          resultadoConsulta = new Data();
          Enumeration recorrido = Campos.elements();
          while (recorrido.hasMoreElements()) {
            String nombreCampo;
            nombreCampo = (String) recorrido.nextElement();
            resultadoConsulta.put(nombreCampo, Resultado.getString(nombreCampo));
          }
          listaSalida.addElement(resultadoConsulta);
        }

        Resultado.close();
        Resultado = null;

        Query.close();
        Query = null;

        con.commit();
      }
    }
    catch (Exception ex) {
      con.rollback();
      ex.printStackTrace();
      listaSalida = null;
      throw ex;
    }
    closeConnection(con);
    return listaSalida;
  };

}
