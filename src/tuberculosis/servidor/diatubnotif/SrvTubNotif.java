package tuberculosis.servidor.diatubnotif;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import capp.CLista;
import comun.Fechas;
import comun.SrvDebugGeneral;
import comun.constantes;
import infproto.DataConflictoTuber;
import infproto.DataRespTuber;
import tuberculosis.datos.diatubnotif.ConTubNotif;
import tuberculosis.datos.diatubnotif.DatTubNotif;

public class SrvTubNotif
    extends SrvDebugGeneral {

  // modos de operaci�n del servlet
  final int SELECCION_NOTIFICADORES = constantes.modoLEERDATOS;
  final int INSERCION_NOTIFICADORES = constantes.modoALTA;
  final int MODIFICACION_NOTIFICADORES = constantes.modoMODIFICACION;
  final int ELIMINACION_NOTIFICADORES = constantes.modoBAJA;

  final int INSERCION_NOTIFICADORES_SB = constantes.modoALTA_SB;
  final int MODIFICACION_NOTIFICADORES_SB = constantes.modoMODIFICACION_SB;
  final int ELIMINACION_NOTIFICADORES_SB = constantes.modoBAJA_SB;

  public SimpleDateFormat Format_Horas = new SimpleDateFormat(
      "dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));
  public java.sql.Timestamp fecRecu = null;
  public String sfecRecu = "";
  public java.sql.Timestamp fecRecu_notifedo = null;
  public String sfecRecu_notifedo = "";
  public java.sql.Timestamp fecRecu_notif_sem = null;
  public String sfecRecu_notif_sem = "";

  // objetos de datos
  private CLista clDatos = null;
  private Hashtable hBloqueo = null;

  // para saber si se deben tener en cuenta los bloqueos o no
  private boolean bComprobarBloqueo = true;

  /////// modificaci�n
  private Vector vEstado = null;
  private Vector vClavesPreguntas = null;

  // valores del equipo notificador completo
  DatTubNotif dtNotificador = null;
  private boolean bBorrarResp = false;
  private boolean bUpdateResp = false;

  java.sql.Date dFechaAsignacion = null;

  // par�metros
  String sQuery = "", sFechaRecep = "", sFechaNotif = "", sFechaValidacion = "";
  String sfc_ultact_notifedo = "";
  String sfc_ultact_notif_sem = "";

  java.util.Date dFecha;
  java.sql.Date sqlFec;
  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

  // objetos JDBC
  Connection con = null;

  // Constructor
  public SrvTubNotif() {
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Para devolver si hay errores
    java.lang.Exception exp = null;

    // Configuraci�n
    String sQuery = "";

    if (super.con != null) {
      con = super.con; // Se est� en modo debug
    }
    else {
      con = openConnection(); // Modo real
    }

    clDatos = new CLista();
    hBloqueo = null;
    bComprobarBloqueo = true;

    dFechaAsignacion = new java.sql.Date(new java.util.Date().getTime());

    con.setAutoCommit(false);

    try {
      switch (opmode) {
        case MODIFICACION_NOTIFICADORES_SB:
          bComprobarBloqueo = false;
        case MODIFICACION_NOTIFICADORES:
          hBloqueo = new Hashtable();
          modificarNotificadores(param, bComprobarBloqueo);
          break;
        case INSERCION_NOTIFICADORES_SB:
          bComprobarBloqueo = false;
        case INSERCION_NOTIFICADORES:
          hBloqueo = new Hashtable();
          insertarNotificadores(param, bComprobarBloqueo);
          break;
        case SELECCION_NOTIFICADORES:
          clDatos = seleccionarNotificadores(param);
          break;
        case ELIMINACION_NOTIFICADORES_SB:
          bComprobarBloqueo = false;
        case ELIMINACION_NOTIFICADORES:
          hBloqueo = new Hashtable();
          eliminarNotificadores(param, bComprobarBloqueo);
          break;
      }
      con.commit(); // Todo ha ido bien
    }
    catch (Exception e) {
      con.rollback(); // algo va mal
      exp = e; // Se guarda la excepci�n
    }
    finally {
      // cierra la conexion y acaba el procedimiento doWork
      closeConnection(con);
    }

    if (exp != null) {
      throw exp;
    }
    else {
      if (clDatos != null) {
        clDatos.trimToSize();
        if (hBloqueo != null) {
          clDatos.addElement(hBloqueo);
        }
      }
      return clDatos;
    }

  } // Fin doWork

  // M�todos privados

  // Se encarga de crear un registro en SIVE_NOTIF_SEM si
  // el centro carece de cobertura
  private void compruebaSemana(CLista p) throws Exception {
    String sQuery = null;
    PreparedStatement st = null;

    DatTubNotif dtn = null;

    int iActualizados = 0;

    dtn = (DatTubNotif) p.firstElement();

    sQuery = "update SIVE_NOTIF_SEM set CD_SEMEPI = ? " +
        " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ?";

    st = con.prepareStatement(sQuery);

    st.setString(1, dtn.getSemana());
    st.setString(2, dtn.getCodEquipo());
    st.setString(3, dtn.getAnno());
    st.setString(4, dtn.getSemana());

    iActualizados = st.executeUpdate();

    if (iActualizados == 0) { // Debe crearse el registro
      sQuery = "insert into SIVE_NOTIF_SEM (CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, CD_OPE, FC_ULTACT) " +
          " values (?, ?, ?, ?, ?) ";

      st = con.prepareStatement(sQuery);

      st.setString(1, dtn.getCodEquipo());
      st.setString(2, dtn.getAnno());
      st.setString(3, dtn.getSemana());
      st.setString(4, p.getLogin());
      st.setTimestamp(5, new java.sql.Timestamp(new java.util.Date().getTime()));

      st.executeUpdate();
    }

    sQuery = null;
    st = null;
  }

  // Devuelve el registro de SIVE_NOTIF_EDOI que ha sido marcado
  // como primer notificador. En caso de no haber ninguno, devuelve null.
  private DatTubNotif dtnSelectNuevoPrimerEDOI(CLista p, DatTubNotif dtnDatos) throws
      Exception {
    java.sql.Timestamp tsFechaActualizacion = new java.sql.Timestamp(new java.
        util.Date().getTime());

    int iActualizados = 0;

    DatTubNotif dtn = null;
    ResultSet rsNotifEdoi = null;
    PreparedStatement stNotifEdoi = null;

    String sQueryNotifEdoi = null;

    sQueryNotifEdoi = " select * from SIVE_NOTIF_EDOI where " +
        " NM_EDO = ? " +
        " order by FC_FECNOTIF ";

    stNotifEdoi = con.prepareStatement(sQueryNotifEdoi);

    stNotifEdoi.setInt(1, new Integer(dtnDatos.getNM_EDO()).intValue());

    try {
      rsNotifEdoi = stNotifEdoi.executeQuery();
      while (rsNotifEdoi.next()) {
        dtn = new DatTubNotif();

        dtn.setCodEquipo(rsNotifEdoi.getString("CD_E_NOTIF"));
        dtn.setAnno(rsNotifEdoi.getString("CD_ANOEPI"));
        dtn.setSemana(rsNotifEdoi.getString("CD_SEMEPI"));
        dtn.setNM_EDO(String.valueOf(rsNotifEdoi.getInt("NM_EDO")));
        dtn.setFechaNotif(Fechas.date2String(rsNotifEdoi.getDate("FC_FECNOTIF")));
        dtn.setFechaRecep(Fechas.date2String(rsNotifEdoi.getDate("FC_RECEP")));
        dtn.setCD_FUENTE(rsNotifEdoi.getString("CD_FUENTE"));

        dtn.setCD_OPE_NOTIF_EDOI(p.getLogin());
        dtn.setFC_ULTACT_NOTIF_EDOI(Fechas.timestamp2String(
            tsFechaActualizacion));

        // Se escribe
        String query = " update SIVE_NOTIF_EDOI set " +
            " IT_PRIMERO = ?, CD_OPE = ?, FC_ULTACT = ? " +
            " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
            " and NM_EDO = ? and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? ";

        PreparedStatement st = con.prepareStatement(query);

        st.setString(1, "S");
        st.setString(2, p.getLogin());
        st.setTimestamp(3, tsFechaActualizacion);

        st.setString(4, dtn.getCodEquipo());
        st.setString(5, dtn.getAnno());
        st.setString(6, dtn.getSemana());
        st.setInt(7, new Integer(dtn.getNM_EDO()).intValue());
        st.setDate(8,
                   new java.sql.Date(Fechas.string2Date(dtn.getFechaRecep()).getTime()));
        st.setDate(9,
                   new java.sql.Date(Fechas.string2Date(dtn.getFechaNotif()).getTime()));
        st.setString(10, dtn.getCD_FUENTE());

        iActualizados = st.executeUpdate();
        if (iActualizados == 0) {
          // Ya me dir�s...
          throw new Exception();
        }
        //

        // Si todo va bien, se introduce en hBloqueo el conjunto de valores
        // relativo a SIVE_NOTIF_EDOI
        hBloqueo.put(ConTubNotif.CD_OPE_NOTIF_EDOI, dtn.getCD_OPE_NOTIF_EDOI());
        hBloqueo.put(ConTubNotif.FC_ULTACT_NOTIF_EDOI,
                     dtn.getFC_ULTACT_NOTIF_EDOI());

        break; // Se toma s�lo el primero
      }
    }
    catch (Exception e) {
      System.out.println("Excepcion en dtnSelectNuevoPrimerEDOI:" +
                         e.getMessage());
      dtn = null;
      throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_NOTIF_EDOI);
    }
    finally {
      stNotifEdoi.close();
      stNotifEdoi = null;
      rsNotifEdoi = null;
    }

    return dtn;
  }

  private void eliminarNotificadores(CLista p, boolean bBloqueo) throws
      Exception {

    DatTubNotif dataEntradaTub = (DatTubNotif) p.firstElement();
    Hashtable hsEntradaTub = (Hashtable) p.elementAt(1);
    if (hsEntradaTub == null) {
      hsEntradaTub = new Hashtable();
    }
    ;

    try {
      // procesar cambios en SIVE_NOTIF_SEM
      // si procede
      if (hsEntradaTub.containsKey(ConTubNotif.CD_OPE_SEMANAS)) {
        hazSQLSemanas(p, bBloqueo);
      }

      // NOTIFEDO
      hazDeleteSQLNotifedo(p, bBloqueo);

    }
    catch (Exception e) {
      System.out.println("Excepcion en eliminarNotificadores");
      throw e;
    }
    finally {

    }

  }

  private void modificarNotificadores(CLista p, boolean bBloqueo) throws
      Exception {

    DatTubNotif dataEntradaTub = (DatTubNotif) p.firstElement();
    Hashtable hsEntradaTub = (Hashtable) p.elementAt(1);
    if (hsEntradaTub == null) {
      hsEntradaTub = new Hashtable();
    }
    ;

    try {

      // procesar cambios en SIVE_E_NOTIF
      // si procede
      if (hsEntradaTub.containsKey(ConTubNotif.CD_OPE_EQUIPOS)) {
        hazSQLEquipos(p, bBloqueo);
      }

      // procesar cambios en SIVE_NOTIF_SEM
      // si procede
      if (hsEntradaTub.containsKey(ConTubNotif.CD_OPE_SEMANAS)) {
        hazSQLSemanas(p, bBloqueo);
      }

      // NOTIFEDO
      //System.out.println("Voy a Notifedo");
      hazUpdateSQLNotifedo(p, bBloqueo);

      // SIVE_NOTIF_EDOI
      //System.out.println("Voy a Notif_Edoi");
      if (! ( (String) dataEntradaTub.getNM_EDO()).equals("")) {
        //System.out.println("Estoy en Notif_Edoi");
        hazUpdateSQLNotif_Edoi(p, bBloqueo);
      }

    }
    catch (Exception e) {
      System.out.println("Excepci�n en modificarNotificadores");
      throw e;
    }
    finally {

    }
  }

  // Para dar de alta en SIVE_NOTIFEDO
  private void hazInsertSQLNotifedo(CLista p) throws Exception {
    java.sql.Timestamp tsFechaActualizacion = new java.sql.Timestamp(new java.
        util.Date().getTime());

    DatTubNotif dtn = null;
    PreparedStatement stNotifedo = null;

    String sQueryNotifedo = null;

    dtn = (DatTubNotif) p.firstElement();

    sQueryNotifedo =
        " insert into SIVE_NOTIFEDO (CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, " +
        " FC_RECEP, FC_FECNOTIF, NM_NNOTIFR, IT_RESSEM, CD_OPE, FC_ULTACT, FC_FECVALID, IT_VALIDADA) " +
        " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

    stNotifedo = con.prepareStatement(sQueryNotifedo);

    stNotifedo.setString(1, dtn.getCodEquipo());
    stNotifedo.setString(2, dtn.getAnno());
    stNotifedo.setString(3, dtn.getSemana());
    stNotifedo.setDate(4,
                       new java.sql.Date(Fechas.string2Date(dtn.getFechaRecep()).
                                         getTime()));
    stNotifedo.setDate(5,
                       new java.sql.Date(Fechas.string2Date(dtn.getFechaNotif()).
                                         getTime()));
    stNotifedo.setInt(6, new Integer(dtn.getNotifReales()).intValue());
    stNotifedo.setString(7, dtn.getResSem());
    stNotifedo.setString(8, p.getLogin());
    stNotifedo.setTimestamp(9, tsFechaActualizacion);
    stNotifedo.setDate(10, new java.sql.Date(tsFechaActualizacion.getTime()));
    stNotifedo.setString(11, "S");

    try {
      stNotifedo.executeUpdate();
      // Si todo va bien, se introduce en hBloqueo el conjunto de valores
      // relativo a SIVE_NOTIFEDO
      hBloqueo.put(ConTubNotif.CD_OPE_NOTIFEDO, p.getLogin());
      hBloqueo.put(ConTubNotif.FC_ULTACT_NOTIFEDO,
                   Fechas.timestamp2String(tsFechaActualizacion));
    }
    catch (Exception e) {
      System.out.println("Excepcion en hazInsertSQLNotifedo:" + e.getMessage());
      // modificacion 03/07/2000
      // comento esta linea de abajo para poder continuar aunque salte el
      // error al hacer el insert
      //throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_NOTIFEDO);
    }
    finally {
      stNotifedo.close();
      stNotifedo = null;
    }
  }

  // Para dar de alta en SIVE_NOTIF_EDOI
  private void hazInsertSQLNotif_Edoi(CLista p) throws Exception {
    java.sql.Timestamp tsFechaActualizacion = new java.sql.Timestamp(new java.
        util.Date().getTime());

    DatTubNotif dtn = null;
    PreparedStatement stNotifEdoi = null;

    String sQueryNotifEdoi = null;

    dtn = (DatTubNotif) p.firstElement();

    sQueryNotifEdoi =
        " insert into SIVE_NOTIF_EDOI (CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, " +
        " NM_EDO, FC_FECNOTIF, FC_RECEP, CD_FUENTE, DS_HISTCLI, DS_DECLARANTE, IT_PRIMERO, CD_OPE, FC_ULTACT) " +
        " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

    stNotifEdoi = con.prepareStatement(sQueryNotifEdoi);

    stNotifEdoi.setString(1, dtn.getCodEquipo());
    stNotifEdoi.setString(2, dtn.getAnno());
    stNotifEdoi.setString(3, dtn.getSemana());

    stNotifEdoi.setInt(4, new Integer(dtn.getNM_EDO()).intValue());

    stNotifEdoi.setDate(5,
                        new java.sql.Date(Fechas.string2Date(dtn.getFechaNotif()).
                                          getTime()));
    stNotifEdoi.setDate(6,
                        new java.sql.Date(Fechas.string2Date(dtn.getFechaRecep()).
                                          getTime()));

    stNotifEdoi.setString(7, dtn.getCD_FUENTE());
    stNotifEdoi.setString(8, dtn.getDS_HISTCLI());
    stNotifEdoi.setString(9, dtn.getDS_DECLARANTE());
    stNotifEdoi.setString(10, dtn.getIT_PRIMERO());

    stNotifEdoi.setString(11, p.getLogin());
    stNotifEdoi.setTimestamp(12, tsFechaActualizacion);

    try {
      int cnotifedoi = stNotifEdoi.executeUpdate();
      //trazaLog("cnotifedoi " + cnotifedoi);
      // Si todo va bien, se introduce en hBloqueo el conjunto de valores
      // relativo a SIVE_NOTIF_EDOI
      hBloqueo.put(ConTubNotif.CD_OPE_NOTIF_EDOI, p.getLogin());
      hBloqueo.put(ConTubNotif.FC_ULTACT_NOTIF_EDOI,
                   Fechas.timestamp2String(tsFechaActualizacion));

    }
    catch (Exception e) {
      System.out.println("Excepcion en hazInsertSQLNotif_Edoi:" + e.getMessage());
      throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_NOTIF_EDOI);
    }
    finally {
      stNotifEdoi.close();
      stNotifEdoi = null;
    }
  }

  //
  private void hazDeleteSQLNotifedo(CLista p, boolean bBloqueo) throws
      Exception {
    DatTubNotif dtn = null;
    PreparedStatement st = null;

    String sQuery = null;

    String sCD_E_NOTIF = null;
    String sCD_ANOEPI = null;
    String sCD_SEMEPI = null;
    String sCD_FUENTE = null;
    java.sql.Date dFC_RECEP = null;
    java.sql.Date dFC_FECNOTIF = null;
//    Integer iNM_EDO = null;
    int iNM_EDO = 0;

    int iBorrados = 0;

    int iBorrados2 = 0;

    dtn = (DatTubNotif) p.firstElement();

    // Primero: SIVE_NOTIF_EDOI
    // Si se reciben cadenas vac�as, es porque se est� dando de alta el notificador
    // y no existe ning�n registro asociado en SIVE_NOTIF_EDOI
    if (!dtn.getCD_OPE_NOTIF_EDOI().equals("")) {
      // Se cargan las variables que se emplean como par�metros
      sCD_E_NOTIF = dtn.getCodEquipo();
      sCD_ANOEPI = dtn.getAnno();
      sCD_SEMEPI = dtn.getSemana();
      sCD_FUENTE = dtn.getCD_FUENTE();
      dFC_RECEP = new java.sql.Date(Fechas.string2Date(dtn.getFechaRecep()).
                                    getTime());
      dFC_FECNOTIF = new java.sql.Date(Fechas.string2Date(dtn.getFechaNotif()).
                                       getTime());
//      iNM_EDO = new Integer(dtn.getNM_EDO());
      iNM_EDO = Integer.parseInt( (String) (dtn.getNM_EDO()));

      // Aqu� se recogen las respuestas antiguas
      vEstado = cargaEstado(iNM_EDO);

      sQuery = " delete SIVE_MOVRESPEDO " +
          " where CD_E_NOTIF = ? and NM_EDO = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
          " and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? ";

      st = con.prepareStatement(sQuery);

      st.setString(1, sCD_E_NOTIF);
//      st.setInt(2, iNM_EDO.intValue());
      st.setInt(2, iNM_EDO);
      st.setString(3, sCD_ANOEPI);
      st.setString(4, sCD_SEMEPI);
      st.setDate(5, dFC_RECEP);
      st.setDate(6, dFC_FECNOTIF);
      st.setString(7, sCD_FUENTE);

      st.executeUpdate();

      sQuery = null;
      st = null;

      // Se borran movimientos de respuestas de protocolo
      // Y aqui se est�n tratando los conflictos en caso de borrar
      // un notificador

      estableceConflictos(iNM_EDO, dtn);

      // Resuelve los conflictos posibles de manera autom�tica
      resuelveConflictos(iNM_EDO);

      // Borrar Tratamientos
      sQuery = " delete SIVE_TRATAMIENTOS " +
          " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
          " and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? and NM_EDO = ? ";

      st = con.prepareStatement(sQuery);

      st.setString(1, sCD_E_NOTIF);
      st.setString(2, sCD_ANOEPI);
      st.setString(3, sCD_SEMEPI);
      st.setDate(4, dFC_RECEP);
      st.setDate(5, dFC_FECNOTIF);
      st.setString(6, sCD_FUENTE);
//      st.setInt(7, iNM_EDO.intValue());
      st.setInt(7, iNM_EDO);

      st.executeUpdate();

      sQuery = null;
      st = null;

      // Borrar sive_res_resistencia
      sQuery = " delete SIVE_RES_RESISTENCIA " +
          " where NM_RESLAB in " +
          " ( select NM_RESLAB from SIVE_RESLAB " +
          " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
          " and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? and NM_EDO = ? )";

      st = con.prepareStatement(sQuery);

      st.setString(1, sCD_E_NOTIF);
      st.setString(2, sCD_ANOEPI);
      st.setString(3, sCD_SEMEPI);
      st.setDate(4, dFC_RECEP);
      st.setDate(5, dFC_FECNOTIF);
      st.setString(6, sCD_FUENTE);
//      st.setInt(7, iNM_EDO.intValue());
      st.setInt(7, iNM_EDO);

      st.executeUpdate();

      sQuery = null;
      st = null;

      // Borrar sive_reslab
      sQuery = " delete SIVE_RESLAB " +
          " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
          " and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? and NM_EDO = ? ";

      st = con.prepareStatement(sQuery);

      st.setString(1, sCD_E_NOTIF);
      st.setString(2, sCD_ANOEPI);
      st.setString(3, sCD_SEMEPI);
      st.setDate(4, dFC_RECEP);
      st.setDate(5, dFC_FECNOTIF);
      st.setString(6, sCD_FUENTE);
//      st.setInt(7, iNM_EDO.intValue());
      st.setInt(7, iNM_EDO);

      st.executeUpdate();

      sQuery = null;
      st = null;

      // sive_notif_edoi propiamente dicho
      sQuery = " delete SIVE_NOTIF_EDOI " +
          " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
          " and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? and NM_EDO = ? ";

      if (bBloqueo) {
        sQuery += " and CD_OPE = ? and FC_ULTACT = ?";
      }

      st = con.prepareStatement(sQuery);

      // Clave
      st.setString(1, sCD_E_NOTIF);
      st.setString(2, sCD_ANOEPI);
      st.setString(3, sCD_SEMEPI);
      st.setDate(4, dFC_RECEP);
      st.setDate(5, dFC_FECNOTIF);
      st.setString(6, sCD_FUENTE);
//      st.setInt(7, iNM_EDO.intValue());
      st.setInt(7, iNM_EDO);

      // Campos de bloqueo
      if (bBloqueo) {
        st.setString(8, dtn.getCD_OPE_NOTIF_EDOI());
        st.setTimestamp(9, Fechas.string2Timestamp(dtn.getFC_ULTACT_NOTIF_EDOI()));
      }

      try {
        iBorrados = st.executeUpdate();
      }
      catch (Exception e) {
        System.out.println(
            "Excepcion en hazDeleteSQLNotif_edoi: SIVE_NOTIF_EDOI " +
            e.getMessage());
      }
      finally {
        st.close();
        st = null;
        if (iBorrados == 0) {
          throw new Exception(constantes.PRE_BLOQUEO +
                              constantes.SIVE_NOTIF_EDOI);
        }
      }

      if (dtn.getIT_PRIMERO().equals("S")) {
        DatTubNotif dtnNuevoPrimero = dtnSelectNuevoPrimerEDOI(p, dtn);
        if (dtnNuevoPrimero != null) {
          hBloqueo.put(ConTubNotif.NUEVO_PRIMERO, dtnNuevoPrimero);
        }
      }
    }

    // Segundo: SIVE_NOTIFEDO
    // Se anula su borrado: depender� de otro m�dulo
    sQuery = " update SIVE_NOTIFEDO set NM_NNOTIFR = ? " +
        " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
        " and FC_RECEP = ? and FC_FECNOTIF = ? ";

    st = con.prepareStatement(sQuery);

    // Notificadores reales = 0
    st.setInt(1, 0);

    // Clave
    st.setString(2, dtn.getCodEquipo());
    st.setString(3, dtn.getAnno());
    st.setString(4, dtn.getSemana());
    st.setDate(5,
               new java.sql.Date(Fechas.string2Date(dtn.getFechaRecep()).getTime()));
    st.setDate(6,
               new java.sql.Date(Fechas.string2Date(dtn.getFechaNotif()).getTime()));

    try {
      iBorrados = st.executeUpdate();
    }
    catch (Exception e) {
      System.out.println("Excepcion en hazDeleteSQLNotifedo:" + e.getMessage());
    }
    finally {
      st.close();
      st = null;
      if (iBorrados == 0) {
        throw new Exception(constantes.SIVE_NOTIFEDO);
      }
    }

  }

  // Para actualizar SIVE_NOTIFEDO
  private void hazUpdateSQLNotifedo(CLista p, boolean bBloqueo) throws
      Exception {
    java.sql.Timestamp tsFechaActualizacion = new java.sql.Timestamp(new java.
        util.Date().getTime());

    DatTubNotif dtn = null;
    PreparedStatement stNotifedo = null;

    String sQueryNotifedo = null;

    int iActualizados = 0;

    dtn = (DatTubNotif) p.firstElement();

    sQueryNotifedo = " update SIVE_NOTIFEDO set " +
        " NM_NNOTIFR = ?, IT_RESSEM = ?, CD_OPE = ?, FC_ULTACT = ? " +
        " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
        " and FC_RECEP = ? and FC_FECNOTIF = ? ";

    if (bBloqueo) {
      sQueryNotifedo += " and CD_OPE = ? and FC_ULTACT = ?";
    }

    stNotifedo = con.prepareStatement(sQueryNotifedo);

    // Datos
    stNotifedo.setInt(1, new Integer(dtn.getNotifReales()).intValue());
    stNotifedo.setString(2, dtn.getResSem());
    stNotifedo.setString(3, p.getLogin());
    stNotifedo.setTimestamp(4, tsFechaActualizacion);

    // Clave
    stNotifedo.setString(5, dtn.getCodEquipo());
    stNotifedo.setString(6, dtn.getAnno());
    stNotifedo.setString(7, dtn.getSemana());
    stNotifedo.setDate(8,
                       new java.sql.Date(Fechas.string2Date(dtn.getFechaRecep()).
                                         getTime()));
    stNotifedo.setDate(9,
                       new java.sql.Date(Fechas.string2Date(dtn.getFechaNotif()).
                                         getTime()));

    // Campos de bloqueo
    if (bBloqueo) {
      stNotifedo.setString(10, dtn.getCD_OPE_NOTIFEDO());
      stNotifedo.setTimestamp(11,
                              Fechas.string2Timestamp(dtn.getFC_ULTACT_NOTIFEDO()));
    }

    try {
      iActualizados = stNotifedo.executeUpdate();
    }
    catch (Exception e) {
      System.out.println("Excepcion en hazUpdateSQLNotifedo:" + e.getMessage());
    }
    finally {
      stNotifedo.close();
      stNotifedo = null;

      if (iActualizados == 0) {
        throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_NOTIFEDO);
      }
      else {
        hBloqueo.put(ConTubNotif.CD_OPE_NOTIFEDO, p.getLogin());
        hBloqueo.put(ConTubNotif.FC_ULTACT_NOTIFEDO,
                     Fechas.timestamp2String(tsFechaActualizacion));
      }
    }
  }

  // Para actualizar SIVE_NOTIF_EDOI
  private void hazUpdateSQLNotif_Edoi(CLista p, boolean bBloqueo) throws
      Exception {
    java.sql.Timestamp tsFechaActualizacion = new java.sql.Timestamp(new java.
        util.Date().getTime());

    DatTubNotif dtn = null;
    PreparedStatement stNotifEdoi = null;

    String sQueryNotifEdoi = null;

    int iActualizados = 0;

    dtn = (DatTubNotif) p.firstElement();

    sQueryNotifEdoi = " update SIVE_NOTIF_EDOI set " +
        " DS_HISTCLI = ?, DS_DECLARANTE = ?, CD_OPE = ?, FC_ULTACT = ? " +
        " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
        " and NM_EDO = ? and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? ";

    if (bBloqueo) {
      sQueryNotifEdoi += " and CD_OPE = ? and FC_ULTACT = ?";
    }

    stNotifEdoi = con.prepareStatement(sQueryNotifEdoi);

    // Datos
    stNotifEdoi.setString(1, dtn.getDS_HISTCLI());
    stNotifEdoi.setString(2, dtn.getDS_DECLARANTE());

    stNotifEdoi.setString(3, p.getLogin());
    stNotifEdoi.setTimestamp(4, tsFechaActualizacion);

    // Clave
    stNotifEdoi.setString(5, dtn.getCodEquipo());
    stNotifEdoi.setString(6, dtn.getAnno());
    stNotifEdoi.setString(7, dtn.getSemana());
    stNotifEdoi.setInt(8, new Integer(dtn.getNM_EDO()).intValue());
    stNotifEdoi.setDate(9,
                        new java.sql.Date(Fechas.string2Date(dtn.getFechaRecep()).
                                          getTime()));
    stNotifEdoi.setDate(10,
                        new java.sql.Date(Fechas.string2Date(dtn.getFechaNotif()).
                                          getTime()));
    stNotifEdoi.setString(11, dtn.getCD_FUENTE());

    // Campos de bloqueo
    if (bBloqueo) {
      stNotifEdoi.setString(12, dtn.getCD_OPE_NOTIF_EDOI());
      stNotifEdoi.setTimestamp(13,
                               Fechas.string2Timestamp(dtn.getFC_ULTACT_NOTIF_EDOI()));
    }

    try {
      iActualizados = stNotifEdoi.executeUpdate();
    }
    catch (Exception e) {
      System.out.println("Excepcion en hazUpdateSQLNotif_Edoi:" + e.getMessage());
    }
    finally {
      stNotifEdoi.close();
      stNotifEdoi = null;

      if (iActualizados == 0) {
        throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_NOTIF_EDOI);
      }
      else {
        hBloqueo.put(ConTubNotif.CD_OPE_NOTIF_EDOI, p.getLogin());
        hBloqueo.put(ConTubNotif.FC_ULTACT_NOTIF_EDOI,
                     Fechas.timestamp2String(tsFechaActualizacion));
      }
    }
  }

  /*private void hazInsertNotifedo(CLista p) throws Exception {
    // Aqu� se decide si es un alta o una modificaci�n
    try {
      // Intenta la inserci�n
      hazInsertSQLNotifedo(p);
    } catch (Exception e) {
      // Efect�a la modificaci�n
      System.out.println("Excepcion en hazSQLNotifedo:" + e.getMessage());
    } finally {
    }
     } */

  // Actualizar campos de SIVE_NOTIF_SEM
  private void hazSQLSemanas(CLista p, boolean bBloqueo) throws Exception {
    //System.out.println("Entr� en semanas");
    java.sql.Timestamp tsFechaActualizacion = new java.sql.Timestamp(new java.
        util.Date().getTime());

    Hashtable h = null;
    DatTubNotif dtn = null;

    PreparedStatement stSemanas = null;

    String sQuerySemanas = null;
    String sCampos = null;
    String sCdOpeSemanas = null;
    String sFcUltActSemanas = null;

    String sFiltro1 = null;
    String sFiltro2 = null;
    int iContador = 0;

    int iActualizados = 0;

    dtn = (DatTubNotif) p.firstElement();
    h = (Hashtable) p.elementAt(1);

    sCdOpeSemanas = (String) h.get(ConTubNotif.CD_OPE_SEMANAS);
    sFcUltActSemanas = (String) h.get(ConTubNotif.FC_ULTACT_SEMANAS);

    if (h.containsKey(ConTubNotif.TEORICOS_SEMANAS)) {
      sCampos = " NM_NNOTIFT = ? ";
      sFiltro1 = (String) h.get(ConTubNotif.TEORICOS_SEMANAS);
    }

    if (h.containsKey(ConTubNotif.TOTAL_SEMANAS)) {
      if (sCampos == null) {
        sCampos = " NM_NTOTREAL = ? ";
      }
      else {
        sCampos = sCampos + ", NM_NTOTREAL = ? ";
      }
      sFiltro2 = (String) h.get(ConTubNotif.TOTAL_SEMANAS);
    }

    // Al menos hay un valor anterior y sCampos != null
    sCampos = sCampos + ", CD_OPE = ?, FC_ULTACT = ? ";

    sQuerySemanas = " update SIVE_NOTIF_SEM set " + sCampos +
        " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? ";

    if (bBloqueo) {
      sQuerySemanas += " and CD_OPE = ? and FC_ULTACT = ?";
    }

    stSemanas = con.prepareStatement(sQuerySemanas);

    if (sFiltro1 != null) {
      stSemanas.setInt(1, new Integer(sFiltro1).intValue());
      iContador++;
    }

    if (sFiltro2 != null) {
      stSemanas.setInt(iContador + 1, new Integer(sFiltro2).intValue());
      iContador++;
    }

    stSemanas.setString(iContador + 1, p.getLogin());
    stSemanas.setTimestamp(iContador + 2, tsFechaActualizacion);

    stSemanas.setString(iContador + 3, dtn.getCodEquipo());
    stSemanas.setString(iContador + 4, dtn.getAnno());
    stSemanas.setString(iContador + 5, dtn.getSemana());

    if (bBloqueo) {
      stSemanas.setString(iContador + 6, sCdOpeSemanas);
      stSemanas.setTimestamp(iContador + 7,
                             Fechas.string2Timestamp(sFcUltActSemanas));
    }

    iActualizados = stSemanas.executeUpdate();

    stSemanas.close();
    stSemanas = null;

    if (iActualizados == 0) {
      throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_NOTIF_SEM);
    }
    else {
      hBloqueo.put(ConTubNotif.CD_OPE_SEMANAS, p.getLogin());
      hBloqueo.put(ConTubNotif.FC_ULTACT_SEMANAS,
                   Fechas.timestamp2String(tsFechaActualizacion));
      //System.out.println("puse semanas");
    }
  }

  // Actualizar campos de SIVE_E_NOTIF
  private void hazSQLEquipos(CLista p, boolean bBloqueo) throws Exception {
    java.sql.Timestamp tsFechaActualizacion = new java.sql.Timestamp(new java.
        util.Date().getTime());

    Hashtable h = null;
    DatTubNotif dtn = null;

    PreparedStatement stEquipos = null;

    String sQueryEquipos = null;
    String sCampos = null;
    String sCdOpeEquipos = null;
    String sFcUltActEquipos = null;

    String sFiltro1 = null;

    int iContador = 0;
    int iActualizados = 0;

    dtn = (DatTubNotif) p.firstElement();
    h = (Hashtable) p.elementAt(1);

    sCdOpeEquipos = (String) h.get(ConTubNotif.CD_OPE_EQUIPOS);
    sFcUltActEquipos = (String) h.get(ConTubNotif.FC_ULTACT_EQUIPOS);

    if (h.containsKey(ConTubNotif.TEORICOS_EQUIPOS)) {
      sCampos = " NM_NOTIFT = ? ";
      sFiltro1 = (String) h.get(ConTubNotif.TEORICOS_EQUIPOS);
    }

    // Al menos hay un valor anterior y sCampos != null
    sCampos = sCampos + ", CD_OPE = ?, FC_ULTACT = ? ";

    sQueryEquipos = " update SIVE_E_NOTIF set " + sCampos +
        " where CD_E_NOTIF = ? ";

    if (bBloqueo) {
      sQueryEquipos += " and CD_OPE = ? and FC_ULTACT = ?";
    }

    stEquipos = con.prepareStatement(sQueryEquipos);

    if (sFiltro1 != null) {
      stEquipos.setInt(1, new Integer(sFiltro1).intValue());
      iContador++;
    }

    stEquipos.setString(iContador + 1, p.getLogin());
    stEquipos.setTimestamp(iContador + 2, tsFechaActualizacion);

    stEquipos.setString(iContador + 3, dtn.getCodEquipo());

    if (bBloqueo) {
      stEquipos.setString(iContador + 4, sCdOpeEquipos);
      stEquipos.setTimestamp(iContador + 5,
                             Fechas.string2Timestamp(sFcUltActEquipos));
    }

    iActualizados = stEquipos.executeUpdate();

    stEquipos.close();
    stEquipos = null;

    if (iActualizados == 0) {
      throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_E_NOTIF);
    }
    else {
      // Si el proceso ha sido correcto, se introducen en hBloqueo
      // los datos correspondientes a equipos
      hBloqueo.put(ConTubNotif.CD_OPE_EQUIPOS, p.getLogin());
      hBloqueo.put(ConTubNotif.FC_ULTACT_EQUIPOS,
                   Fechas.timestamp2String(tsFechaActualizacion));
    }
  }

  // Inserci�n/Actualizaci�n de datos de Notifedo, Semanas y equipos
  private void insertarNotificadores(CLista p, boolean bBloqueo) throws
      Exception {

    DatTubNotif dataEntradaTub = (DatTubNotif) p.firstElement();
    Hashtable hsEntradaTub = (Hashtable) p.elementAt(1);
    if (hsEntradaTub == null) {
      hsEntradaTub = new Hashtable();
    }
    ;

    //trazaLog("insertarnotificadores");

    try {
      // Por si acaso el centro careciera de cobertura
      compruebaSemana(p);

      //trazaLog("paso compruebasemanas");

      // procesar cambios en SIVE_E_NOTIF
      // si procede
      if (hsEntradaTub.containsKey(ConTubNotif.CD_OPE_EQUIPOS)) {
        //trazaLog("entro en hazSQlequipos");
        hazSQLEquipos(p, bBloqueo);
      }

      // procesar cambios en SIVE_NOTIF_SEM
      // si procede
      if (hsEntradaTub.containsKey(ConTubNotif.CD_OPE_SEMANAS)) {
        //trazaLog("entro en hazsqlsemanas");
        hazSQLSemanas(p, bBloqueo);
      }

      // SIVE_NOTIFEDO
      try {
        //trazaLog("insertnotifedo");
        hazInsertSQLNotifedo(p);
      }
      catch (Exception e) {
        //trazaLog("updatetnotifedo");
        hazUpdateSQLNotifedo(p, bBloqueo);
      }

      // SIVE_NOTIF_EDOI
      if (! ( (String) dataEntradaTub.getNM_EDO()).equals("")) {
        try {
          //trazaLog("insertnotifedoi");
          hazInsertSQLNotif_Edoi(p);
        }
        catch (Exception e) {
          //trazaLog("updatenotifedoi");
          hazUpdateSQLNotif_Edoi(p, bBloqueo);
        }
      }
    }
    catch (Exception e) {
      System.out.println("Excepcion en insertarNotificadores");
      throw e;
    }
    finally {

    }
  }

  // Obtenci�n de los notificadores que cumplen las condiciones
  // establecidas.

  // Si no se encuentran, al menos se devuelven los datos relativos
  // a la semana (si existen)
  private CLista seleccionarNotificadores(CLista p) throws Exception {

    PreparedStatement stNotificador = null;
    ResultSet rsNotificador = null;

    DatTubNotif dataEntradaTub = null;
    DatTubNotif dataTub = null;

    CLista clData = new CLista(); // Siempre se devolver�,
    // aunque sea vac�a

    dataEntradaTub = (DatTubNotif) p.firstElement();

    sQuery = "select a.IT_RESSEM, a.FC_FECNOTIF, a.FC_RECEP," +
        " a.NM_NNOTIFR, a.CD_OPE, a.FC_ULTACT, " +
        " b.NM_NNOTIFT, b.NM_NTOTREAL, " +
        " b.CD_OPE, b.FC_ULTACT, a.IT_VALIDADA, a.FC_FECVALID " +
//              " c.DS_DECLARANTE, c.DS_HISTCLI " +
        " from SIVE_NOTIFEDO a, SIVE_NOTIF_SEM b " +
        " where (a.CD_E_NOTIF = b.CD_E_NOTIF" +
        " and a.CD_ANOEPI = b.CD_ANOEPI" +
        " and a.CD_SEMEPI = b.CD_SEMEPI" +
        //                  " and a.CD_E_NOTIF = c.CD_E_NOTIF and a.CD_ANOEPI = c.CD_ANOEPI and a.CD_SEMEPI = c.CD_SEMEPI and a.FC_FECNOTIF = c.FC_FECNOTIF and a.FC_RECEP = c.FC_RECEP " +
        " and a.CD_E_NOTIF = ? and a.CD_ANOEPI = ? " +
        " and a.CD_SEMEPI = ? )";

    stNotificador = con.prepareStatement(sQuery);
    stNotificador.setString(1, dataEntradaTub.getCodEquipo());
    stNotificador.setString(2, dataEntradaTub.getAnno());
    stNotificador.setString(3, dataEntradaTub.getSemana());
//    stNotificador.setString(4, dataEntradaTub.getCD_FUENTE());

    rsNotificador = stNotificador.executeQuery();

    fecRecu_notifedo = null;
    sfecRecu_notifedo = "";
    fecRecu_notif_sem = null;
    sfecRecu_notif_sem = "";

    while (rsNotificador.next()) {
      sFechaValidacion = "";
      //Estas fechas nunca pueden ser nulas
      //Los numeros se recuperan con getString para que den null, en caso de serlo

      sqlFec = rsNotificador.getDate("FC_FECNOTIF");
      sFechaNotif = formater.format(sqlFec);

      sqlFec = rsNotificador.getDate("FC_RECEP");
      sFechaRecep = formater.format(sqlFec);

      sqlFec = rsNotificador.getDate("FC_FECVALID");
      if (sqlFec != null) {
        sFechaValidacion = formater.format(sqlFec);

      }
      fecRecu_notifedo = rsNotificador.getTimestamp(6);
      sfecRecu_notifedo = Fechas.timestamp2String(fecRecu_notifedo);
      fecRecu_notif_sem = rsNotificador.getTimestamp(10);
      sfecRecu_notif_sem = Fechas.timestamp2String(fecRecu_notif_sem);

      dataTub = new DatTubNotif(rsNotificador.getString("IT_RESSEM"),
                                sFechaNotif,
                                sFechaRecep,
                                rsNotificador.getString("NM_NNOTIFR"),
                                rsNotificador.getString("NM_NNOTIFT"),
                                rsNotificador.getString("NM_NTOTREAL"),
                                dataEntradaTub.getCodEquipo().trim(),
                                dataEntradaTub.getAnno().trim(),
                                dataEntradaTub.getSemana().trim(),
                                rsNotificador.getString(9),
                                sfecRecu_notif_sem,
                                rsNotificador.getString(5),
                                sfecRecu_notifedo,
                                rsNotificador.getString("IT_VALIDADA"),
                                sFechaValidacion, "", "",
                                "", //rsNotificador.getString("DS_DECLARANTE"),
                                "", //rsNotificador.getString("DS_HISTCLI"),
                                dataEntradaTub.getCD_FUENTE());

      clData.addElement(dataTub);
    }

    rsNotificador.close();
    rsNotificador = null;
    stNotificador.close();
    stNotificador = null;

    // Si no hay registros, se devolver�n los datos de la semana
    if (dataTub == null) {
      sQuery = " select CD_OPE, FC_ULTACT, NM_NNOTIFT, NM_NTOTREAL " +
          " from SIVE_NOTIF_SEM " +
          " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ?";

      stNotificador = con.prepareStatement(sQuery);

      stNotificador.setString(1, dataEntradaTub.getCodEquipo());
      stNotificador.setString(2, dataEntradaTub.getAnno());
      stNotificador.setString(3, dataEntradaTub.getSemana());

      rsNotificador = stNotificador.executeQuery();

      fecRecu_notif_sem = null;
      sfecRecu_notif_sem = "";

      while (rsNotificador.next()) {
        dataTub = new DatTubNotif();

        fecRecu_notif_sem = rsNotificador.getTimestamp("FC_ULTACT");
        //sfecRecu_notif_sem = timestamp_a_cadena (fecRecu_notif_sem);

        dataTub.setResSem("X");
        dataTub.setCD_OPE_NOTIF_SEM(rsNotificador.getString("CD_OPE"));
        dataTub.setFC_ULTACT_NOTIF_SEM(Fechas.timestamp2String(
            fecRecu_notif_sem));
        dataTub.setNotifTeor(rsNotificador.getString("NM_NNOTIFT"));
        dataTub.setTotNotifReales(rsNotificador.getString("NM_NTOTREAL"));

        clData.addElement(dataTub);
      }

      rsNotificador.close();
      rsNotificador = null;
      stNotificador.close();
      stNotificador = null;
    }

    return clData;
  }

////////////////////////////// modificaci�n /////////////

  // Recopilaci�n de datos de estado del protocolo
  private Vector cargaEstado(int iNM_EDO) throws Exception {
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    // variables para recuperar fecha recepcion y notificacion
    String sFrecep = null;
    String sFnotif = null;

    // variable que proporciona el numero de preguntas
    int count = 0;

    Vector vEstado = null;
    DataConflictoTuber dct = null;

    DataRespTuber drt = null;

    vEstado = new Vector();

    // Se recuperan las claves de las preguntas del caso
    sQuery = " select CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, DS_RESPUESTA " +
        " from SIVE_RESP_EDO where NM_EDO = ? ";

    st = con.prepareStatement(sQuery);

    st.setInt(1, iNM_EDO);

    rs = st.executeQuery();

    while (rs.next()) {
      dct = new DataConflictoTuber(iNM_EDO,
                                   rs.getString("CD_TSIVE"),
                                   rs.getString("CD_MODELO"),
                                   rs.getInt("NM_LIN"),
                                   rs.getString("CD_PREGUNTA"),
                                   rs.getString("DS_RESPUESTA"));

      vEstado.addElement(dct);

    }

    // Para cada pregunta del protocolo se recuperan sus poseedores (equipo notificador)
    sQuery = " select CD_E_NOTIF, NM_EDO, CD_ANOEPI, CD_SEMEPI, " +
        " FC_RECEP, FC_FECNOTIF, CD_FUENTE, " +
        " IT_CONFLICTO from SIVE_MOVRESPEDO " +
        " where " +
        " NM_EDO = ? and CD_TSIVE = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ? and DS_RESPUESTA = ? ";

    st = con.prepareStatement(sQuery);

    for (int i = 0; i < vEstado.size(); i++) {
      dct = (DataConflictoTuber) vEstado.elementAt(i);

      st.setInt(1, iNM_EDO);
      st.setString(2, dct.getCD_TSIVE());
      st.setString(3, dct.getCD_MODELO());
      st.setInt(4, dct.getNM_LIN());
      st.setString(5, dct.getCD_PREGUNTA());
      st.setString(6, dct.getDS_RESPUESTA());

      rs = st.executeQuery();

      while (rs.next()) {

        sFrecep = null;
        sFnotif = null;
        sFrecep = (String) Fechas.date2String(rs.getDate("FC_RECEP"));
        sFnotif = (String) Fechas.date2String(rs.getDate("FC_FECNOTIF"));

        dct.addEquipoPoseedor(rs.getString("CD_E_NOTIF"));
        dct.addEquipoPoseedor(rs.getString("NM_EDO"));
        dct.addEquipoPoseedor(rs.getString("CD_ANOEPI"));
        dct.addEquipoPoseedor(rs.getString("CD_SEMEPI"));
//        dct.addEquipoPoseedor(rs.getString("FC_RECEP"));
        dct.addEquipoPoseedor(sFrecep);
//        dct.addEquipoPoseedor(rs.getString("FC_FECNOTIF"));
        dct.addEquipoPoseedor(sFnotif);
        dct.addEquipoPoseedor(rs.getString("CD_FUENTE"));
        dct.setIT_CONFLICTO(rs.getString("IT_CONFLICTO"));

      }

    }

    return vEstado;
  }

  // Para establecer los conflictos
  private void estableceConflictos(int iNM_EDO, DatTubNotif dtNotificador) throws
      Exception {

    DataConflictoTuber dctActual = null;
    vClavesPreguntas = new Vector();
    int count = 0;

    for (int i = 0; i < vEstado.size(); i++) {

      dctActual = (DataConflictoTuber) vEstado.elementAt(i);
      // el valor de count esta condicionado por el hecho de haber efectuado
      // ya el delete de sive_movrespedo
      count = calcularcount(dctActual);

      if (dctActual.getEquiposPoseedores().contains(dtNotificador.getNM_EDO()) &&
          dctActual.getEquiposPoseedores().contains(dtNotificador.getCodEquipo()) &&
          dctActual.getEquiposPoseedores().contains(dtNotificador.getAnno()) &&
          dctActual.getEquiposPoseedores().contains(dtNotificador.getSemana()) &&
          dctActual.getEquiposPoseedores().contains(dtNotificador.getFechaNotif()) &&
          dctActual.getEquiposPoseedores().contains(dtNotificador.getFechaRecep()) &&
          dctActual.getEquiposPoseedores().contains(dtNotificador.getCD_FUENTE())) {

        if (count > 0) {
          updateDatosMovimiento(dctActual, "S");
          updateRespEdo(dctActual);

        }
        else if (count == 0) {
          deleteRespEdo(dctActual);
        }

      }

    } //for

  }

  // Borrado de los movimientos asociados a un caso EDOIND y
  // un equipo notificador

  private void deleteRespEdo(DataConflictoTuber dct) throws Exception {
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    // Se borran aquellas respuestas del protocolo que sea necesario
    sQuery = " delete from SIVE_RESP_EDO where " +
        " CD_TSIVE = ? and NM_EDO = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ? ";

    st = con.prepareStatement(sQuery);

    st.setString(1, dct.getCD_TSIVE());
    st.setInt(2, dct.getNM_EDO());
    st.setString(3, dct.getCD_MODELO());
    st.setInt(4, dct.getNM_LIN());
    st.setString(5, dct.getCD_PREGUNTA());

    st.executeUpdate();

    st = null;
  }

  // Modificaci�n de los movimientos asociados a un caso EDOIND y
  // un equipo notificador

  private void updateRespEdo(DataConflictoTuber dct) throws Exception {
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    // Se modifican aquellas respuestas del protocolo que sea necesario
    sQuery = " update SIVE_RESP_EDO set DS_RESPUESTA = ? where " +
        " CD_TSIVE = ? and NM_EDO = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ? ";

    st = con.prepareStatement(sQuery);

    st.setString(1, " ");
    st.setString(2, dct.getCD_TSIVE());
    st.setInt(3, dct.getNM_EDO());
    st.setString(4, dct.getCD_MODELO());
    st.setInt(5, dct.getNM_LIN());
    st.setString(6, dct.getCD_PREGUNTA());

    st.executeUpdate();

    st = null;

  }

  private int calcularcount(DataConflictoTuber dct) throws Exception {
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;
    int COUNTER = 0;

    // calculo el numero de preguntas iguales
    sQuery = " select count(*) COUNTER " +
        " from SIVE_MOVRESPEDO " +
        " where " +
        " NM_EDO = ? and CD_TSIVE = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ? ";

    st = con.prepareStatement(sQuery);

    st.setInt(1, dct.getNM_EDO());
    st.setString(2, dct.getCD_TSIVE());
    st.setString(3, dct.getCD_MODELO());
    st.setInt(4, dct.getNM_LIN());
    st.setString(5, dct.getCD_PREGUNTA());

    rs = st.executeQuery();

    while (rs.next()) {
      COUNTER = 0;
      COUNTER = rs.getInt("COUNTER");

    }

    return COUNTER;

  }

  // Resoluci�n autom�tica de conflictos
  private void resuelveConflictos(int iNM_EDO) throws Exception {
    PreparedStatement st = null;
    PreparedStatement stCorrectas = null;

    ResultSet rs = null;
    ResultSet rsCorrectas = null;

    String sQuery = null;
    String sQueryCorrectas = null;

    Vector vPreguntas = null;
    DataConflictoTuber dct = null;

    vPreguntas = new Vector();

    // Se recuperan las claves de las preguntas del caso
    sQuery = " select DISTINCT re.CD_TSIVE, re.CD_MODELO, re.NM_LIN, re.CD_PREGUNTA, re.DS_RESPUESTA " +
        " from SIVE_RESP_EDO re, SIVE_MOVRESPEDO mre where re.NM_EDO = ? and mre.IT_CONFLICTO = ? and " +
        " re.CD_TSIVE = mre.CD_TSIVE and re.CD_MODELO = mre.CD_MODELO and " +
        " re.NM_LIN = mre.NM_LIN and re.CD_PREGUNTA = mre.CD_PREGUNTA ";

    st = con.prepareStatement(sQuery);

    st.setInt(1, iNM_EDO);
    st.setString(2, "S");

    rs = st.executeQuery();

    while (rs.next()) {
      dct = new DataConflictoTuber(iNM_EDO,
                                   rs.getString("CD_TSIVE"),
                                   rs.getString("CD_MODELO"),
                                   rs.getInt("NM_LIN"),
                                   rs.getString("CD_PREGUNTA"),
                                   rs.getString("DS_RESPUESTA"));

      vPreguntas.addElement(dct);
    }

    if (vPreguntas.size() > 0) {
      // Para cada pregunta del protocolo se recupera el n� de respuestas
      // existentes en SIVE_MOVRESPEDO para la misma
      sQuery = " select Count(*) NM_TOTAL_RESPUESTAS from SIVE_MOVRESPEDO " +
          " where " +
          " NM_EDO = ? and CD_TSIVE = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ? ";

      st = con.prepareStatement(sQuery);

      sQueryCorrectas =
          " select Count(*) NM_TOTAL_RESPUESTAS from SIVE_MOVRESPEDO " +
          " where " +
          " NM_EDO = ? and CD_TSIVE = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ? and " +
          " DS_RESPUESTA = ? ";

      stCorrectas = con.prepareStatement(sQueryCorrectas);

      for (int i = 0; i < vPreguntas.size(); i++) {
        dct = (DataConflictoTuber) vPreguntas.elementAt(i);

        st.setInt(1, iNM_EDO);
        st.setString(2, dct.getCD_TSIVE());
        st.setString(3, dct.getCD_MODELO());
        st.setInt(4, dct.getNM_LIN());
        st.setString(5, dct.getCD_PREGUNTA());

        rs = st.executeQuery();

        stCorrectas.setInt(1, iNM_EDO);
        stCorrectas.setString(2, dct.getCD_TSIVE());
        stCorrectas.setString(3, dct.getCD_MODELO());
        stCorrectas.setInt(4, dct.getNM_LIN());
        stCorrectas.setString(5, dct.getCD_PREGUNTA());
        stCorrectas.setString(6, dct.getDS_RESPUESTA());

        rsCorrectas = stCorrectas.executeQuery();

        if (rs.next() && rsCorrectas.next()) {
          if (rs.getInt("NM_TOTAL_RESPUESTAS") ==
              rsCorrectas.getInt("NM_TOTAL_RESPUESTAS")) {
            // No hay conflicto --> se elimina
            updateDatosMovimiento(dct, "N");
          }
        }
      }
    }

  }

  // Asignaci�n de conflicto a los movimientos de una pregunta
  private void updateDatosMovimiento(DataConflictoTuber dct, String sConflicto) throws
      Exception {
    PreparedStatement st = null;
    String sQuery = null;

    sQuery = " update SIVE_MOVRESPEDO " +
        " set IT_CONFLICTO = ?, FC_ASIGN = ? " +
        " where NM_EDO = ? and CD_TSIVE = ? and CD_MODELO = ? and " +
        " NM_LIN = ? and CD_PREGUNTA = ? ";

    st = con.prepareStatement(sQuery);

    // Datos
    st.setString(1, sConflicto);
    st.setDate(2, dFechaAsignacion);

    // Clave
    st.setInt(3, dct.getNM_EDO());
    st.setString(4, dct.getCD_TSIVE());
    st.setString(5, dct.getCD_MODELO());
    st.setInt(6, dct.getNM_LIN());
    st.setString(7, dct.getCD_PREGUNTA());

    st.executeUpdate();
  }

//////////////////////// fin modificaci�n /////////////////////

  // Para modo debug
  protected CLista doDebug() throws Exception {
    CLista data = new CLista();
    CLista resultado = null;

    DatTubNotif dtn = new DatTubNotif();

    data.setFilter("");
    data.setLogin("A_1");

    dtn.setCodEquipo(" H01B   ");
    dtn.setAnno("   1999");
    dtn.setSemana("  01   ");
    dtn.setFechaNotif("18/12/1999");
    dtn.setFechaRecep("01/01/1999");
    dtn.setNM_EDO("1");
    dtn.setCD_FUENTE("2");

    dtn.setNotifReales("13");
    dtn.setResSem("S");

    dtn.setCD_OPE_NOTIF_EDOI("A_1");
    dtn.setFC_ULTACT_NOTIF_EDOI("10/12/1999");

    dtn.setCD_OPE_NOTIFEDO("A_1");
    dtn.setFC_ULTACT_NOTIFEDO("10/12/1999");

    dtn.setIT_PRIMERO("S");

    data.addElement(dtn);

    Hashtable hs = new Hashtable();

//     hs.put(ConTubNotif.CD_OPE_SEMANAS, "A_1");
//     hs.put(ConTubNotif.FC_ULTACT_SEMANAS, "10/12/1999");

//     hs.put(ConTubNotif.TEORICOS_SEMANAS, "100");
//     hs.put(ConTubNotif.TOTAL_SEMANAS, "23");

//     hs.put(ConTubNotif.CD_OPE_EQUIPOS, "A_1");
//     hs.put(ConTubNotif.FC_ULTACT_EQUIPOS, "10/12/1999");
//     hs.put(ConTubNotif.TEORICOS_EQUIPOS, "100");

    data.addElement(hs);

    return doPrueba(SELECCION_NOTIFICADORES, data);
  }

  /** prueba del servlet */
  public static void main(String[] args) {
    try {
      SrvTubNotif srv = new SrvTubNotif();
      srv.doDebug();
    }
    catch (Exception e) {
      System.out.println(e.toString());
    }
    finally {
      System.exit(0);
    }
  }

}
