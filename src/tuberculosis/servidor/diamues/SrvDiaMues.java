/**
 * Clase: SrvDiaMues
 * Paquete: tuberculosis.servidor.diamues
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 02/12/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: Implementacion del servlet que permite dar de alta,
 *   baja, modificar una muestra.
 */

package tuberculosis.servidor.diamues;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import capp.CLista;
import comun.Fechas;
import comun.constantes;
import jdbcpool.JDCConnectionDriver;
import sapp.DBServlet;
import tuberculosis.datos.panmues.DatCasMuesCS;
import tuberculosis.datos.panmues.DatCasMuesSC;
import tuberculosis.datos.panmues.DatCasResis;

public class SrvDiaMues
    extends DBServlet {
  // E 17/01/2000

  public CLista doPrueba(int opmode, CLista param) throws Exception {

    jdcdriver = new JDCConnectionDriver("oracle.jdbc.driver.OracleDriver",
        "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
        "sive_desa",
        "sive_desa");
    //Connection con = openConnection();

    return doWork(opmode, param);
  }

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista();
    DatCasMuesCS notif = null;
    DatCasMuesSC mues = null;
    DatCasMuesSC result = null;
    CLista resistencias = null;
    java.util.Date dFechaUltact = null;
    SimpleDateFormat formUltact = null;

    String sOpe;
    String sFechaUltact = null;

    // Querys
    String sQuery = "";
    //Indicador de bloqueo
    boolean bBloqueo = true;

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con = openConnection();
    con.setAutoCommit(false);

    // Datos del tratamiento actual
    notif = (DatCasMuesCS) param.elementAt(0);
    mues = (DatCasMuesSC) param.elementAt(1);

    try {

      switch (opmode) {
        case constantes.modoALTA_SB:
          bBloqueo = false;
        case constantes.modoALTA:

          // En caso de que haya que comprobar el bloqueo, si se ha
          //  modificado la notif no se da de alta  la muestra
          if (bBloqueo) {
            if (notificacionModificada(con, notif)) {
              throw (new Exception(constantes.PRE_BLOQUEO +
                                   constantes.SIVE_NOTIF_EDOI));
            }
          }

          // Buscamos en la tabla de secuenciadores el siguiente numero de muestra
          int iNextMues = sapp.Funciones.SecGeneral(con, st, rs, "NM_RESLAB");

          // Establecimiento de la cadena de la query
          sQuery = "INSERT INTO SIVE_RESLAB " +
              " (NM_RESLAB," +
              "CD_E_NOTIF, FC_MUESTRA, " +
              "CD_ANOEPI," +
              "CD_SEMEPI,CD_TTECLAB,FC_RECEP," +
              "NM_EDO,CD_FUENTE,CD_MUESTRA,CD_VMUESTRA," +
              "FC_FECNOTIF, CD_TMICOB, IT_RESISTENTE) " +
              "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciacion de la query
          instanciarInsertQuery(notif, mues, st, iNextMues);

          // Ejecucion del insert
          st.executeUpdate();

          // Cierre del PreparedStatement (el ResultSet no se usa)
          st.close();
          st = null;

          // Se insertan las resistencias asociadas a esta muestra
          resistencias = (CLista) param.elementAt(2);
          insertarResistencias(con, iNextMues, resistencias);

          // Se modifica el CD_OPE y FC_ULTACT de la notificacion
          dFechaUltact = new java.util.Date();
          formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                            new Locale("es", "ES"));
          sFechaUltact = formUltact.format(dFechaUltact);
          sOpe = mues.getCD_OPE();

          if (bBloqueo) {
            modificarNotificacion(con, notif, sOpe, sFechaUltact);
          }
          else {
            modificarNotificacion_SB(con, notif, sOpe, sFechaUltact);

            // Validacion de la transacci�n
          }
          con.commit();

          // Devolvemos la nueva muestra
          result = new DatCasMuesSC( (new Integer(iNextMues)).toString(),
                                    mues.getFC_MUESTRA(),
                                    mues.getCD_TTECLAB(), mues.getDS_TTECLAB(),
                                    mues.getCD_MUESTRA(), mues.getDS_MUESTRA(),
                                    mues.getCD_VMUESTRA(), mues.getDS_VMUESTRA(),
                                    mues.getCD_TMICOB(), mues.getDS_TMICOB(),
                                    mues.getIT_RESISTENTE(), sOpe, sFechaUltact);

          listaSalida.addElement(result);
          break;

        case constantes.modoMODIFICACION_SB:
          bBloqueo = false;
        case constantes.modoMODIFICACION:

          // En caso de que haya que comprobar el bloqueo, si se ha
          //  modificado la notif no se modif la muestra
          if (bBloqueo) {
            if (notificacionModificada(con, notif)) {
              throw (new Exception(constantes.PRE_BLOQUEO +
                                   constantes.SIVE_NOTIF_EDOI));
            }
          }

          // Establecimiento de la cadena de la query
          sQuery = "UPDATE SIVE_RESLAB " +
              "SET FC_MUESTRA = ?,CD_TTECLAB = ?," +
              "CD_MUESTRA = ?,CD_VMUESTRA = ?," +
              "CD_TMICOB = ?, IT_RESISTENTE = ? " +
              "WHERE NM_RESLAB = ? AND CD_E_NOTIF = ? AND " +
              "CD_ANOEPI = ? AND CD_SEMEPI = ? AND " +
              "FC_RECEP = ? AND FC_FECNOTIF = ? AND " +
              "NM_EDO = ? AND CD_FUENTE = ?";

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciacion de la query
          instanciarUpdateQuery(notif, mues, st);

          // Ejecucion del update
          st.executeUpdate();

          // Cierre del PreparedStatement (el ResultSet no se usa)
          st.close();
          st = null;

          // Se eliminan las resistencias asociadas a esta muestra ...
          eliminarResistencias(con, (new Integer(mues.getNM_RESLAB())).intValue());
          // ... y se a�aden las nuevas
          resistencias = (CLista) param.elementAt(2);
          insertarResistencias(con, (new Integer(mues.getNM_RESLAB())).intValue(),
                               resistencias);

          // Se modifica el CD_OPE y FC_ULTACT de la notificacion

          dFechaUltact = new java.util.Date();
          formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                            new Locale("es", "ES"));
          sFechaUltact = formUltact.format(dFechaUltact);
          sOpe = mues.getCD_OPE();

          if (bBloqueo) {
            modificarNotificacion(con, notif, sOpe, sFechaUltact);
          }
          else {
            modificarNotificacion_SB(con, notif, sOpe, sFechaUltact);

            // Validacion de la transacci�n
          }
          con.commit();

          // Devolvemos la nueva muestra
          result = new DatCasMuesSC(mues.getNM_RESLAB(), mues.getFC_MUESTRA(),
                                    mues.getCD_TTECLAB(), mues.getDS_TTECLAB(),
                                    mues.getCD_MUESTRA(), mues.getDS_MUESTRA(),
                                    mues.getCD_VMUESTRA(), mues.getDS_VMUESTRA(),
                                    mues.getCD_TMICOB(), mues.getDS_TMICOB(),
                                    mues.getIT_RESISTENTE(), sOpe, sFechaUltact);

          listaSalida.addElement(result);
          break;

        case constantes.modoBAJA_SB:
          bBloqueo = false;
        case constantes.modoBAJA:

          // En caso de que haya que comprobar el bloqueo, si se ha
          //  modificado la notif no se baja la muestra
          if (bBloqueo) {
            if (notificacionModificada(con, notif)) {
              throw (new Exception(constantes.PRE_BLOQUEO +
                                   constantes.SIVE_NOTIF_EDOI));
            }
          }

          // Se eliminan todas las resistencias asociadas a esta muestra
          eliminarResistencias(con, (new Integer(mues.getNM_RESLAB())).intValue());

          // Establecimiento de la cadena de la query
          sQuery = "DELETE FROM SIVE_RESLAB " +
              "WHERE NM_RESLAB = ? AND CD_E_NOTIF = ? AND " +
              "CD_ANOEPI = ? AND CD_SEMEPI = ? AND " +
              "FC_RECEP = ? AND FC_FECNOTIF = ? AND " +
              "NM_EDO = ? AND CD_FUENTE = ?";

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciacion de la query
          instanciarDeleteQuery(notif, mues, st);

          // Ejecucion del delete
          st.executeUpdate();

          // Cierre del PreparedStatement (el ResultSet no se usa)
          st.close();
          st = null;

          // Se modifica el CD_OPE y FC_ULTACT de la notificacion
          dFechaUltact = new java.util.Date();
          formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                            new Locale("es", "ES"));
          sFechaUltact = formUltact.format(dFechaUltact);
          sOpe = mues.getCD_OPE();

          if (bBloqueo) {
            modificarNotificacion(con, notif, sOpe, sFechaUltact);
          }
          else {
            modificarNotificacion_SB(con, notif, sOpe, sFechaUltact);

            // Validacion de la transacci�n
          }
          con.commit();

          // Devolvemos la nueva muestra
          result = new DatCasMuesSC(mues.getNM_RESLAB(), mues.getFC_MUESTRA(),
                                    mues.getCD_TTECLAB(), mues.getDS_TTECLAB(),
                                    mues.getCD_MUESTRA(), mues.getDS_MUESTRA(),
                                    mues.getCD_VMUESTRA(), mues.getDS_VMUESTRA(),
                                    mues.getCD_TMICOB(), mues.getDS_TMICOB(),
                                    mues.getIT_RESISTENTE(), sOpe, sFechaUltact);

          listaSalida.addElement(result);
          break;

      }

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);

    return listaSalida;
  } // Fin doWork()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void instanciarInsertQuery(DatCasMuesCS notif,
                                       DatCasMuesSC mues,
                                       PreparedStatement st,
                                       int nextMues) throws Exception {

    // Lleva la cuenta de ?
    int iParN = 1;

    // Numero de muestra SECUENCIADOR !!!
    st.setInt(iParN++, nextMues);

    // Codigo del equipo notificador
    st.setString(iParN++, notif.getCD_E_NOTIF());

    // Fecha Muestra
    String FMues = mues.getFC_MUESTRA().trim();
    if (FMues.length() > 0) {
      java.sql.Date fMues = Fechas.StringToSQLDate(FMues);
      st.setDate(iParN++, fMues);
    }
    else {
      st.setNull(iParN++, java.sql.Types.DATE);

      // A�o epidemiologico
    }
    st.setString(iParN++, notif.getCD_ANOEPI());

    // Semana epidemiologica
    st.setString(iParN++, notif.getCD_SEMEPI());

    // Tipo Tecnica Laboratorio
    st.setString(iParN++, mues.getCD_TTECLAB());

    // Fecha recepcion
    String FRecep = notif.getFC_RECEP().trim();
    if (FRecep.length() > 0) {
      java.sql.Date fRecep = Fechas.StringToSQLDate(FRecep);
      st.setDate(iParN++, fRecep);
    }

    // Numero de EDO
    String nmEdo = notif.getNM_EDO().trim();
    st.setInt(iParN++, (new Integer(nmEdo)).intValue());

    // Fuente
    st.setString(iParN++, notif.getCD_FUENTE());

    // Muestra Laboratorio
    st.setString(iParN++, mues.getCD_MUESTRA());

    // Valor Muestra
    st.setString(iParN++, mues.getCD_VMUESTRA());

    // Fecha notificacion
    String FNotif = notif.getFC_FECNOTIF().trim();
    if (FNotif.length() > 0) {
      java.sql.Date fNotif = Fechas.StringToSQLDate(FNotif);
      st.setDate(iParN++, fNotif);
    }

    // Tipo Micobacteria
    st.setString(iParN++, mues.getCD_TMICOB());

    // Flag Resistente
    st.setString(iParN++, mues.getIT_RESISTENTE());
  } // Fin instanciarInsertQuery()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void instanciarUpdateQuery(DatCasMuesCS notif,
                                       DatCasMuesSC mues,
                                       PreparedStatement st) throws Exception {

    // Lleva la cuenta de ?
    int iParN = 1;

    /************** Par�metros del SET ***************/

    // Fecha Muestra
    String FMues = mues.getFC_MUESTRA().trim();
    if (FMues.length() > 0) {
      java.sql.Date fMues = Fechas.StringToSQLDate(FMues);
      st.setDate(iParN++, fMues);
    }
    else {
      st.setNull(iParN++, java.sql.Types.DATE);

      // Tipo Tecnica Laboratorio
    }
    st.setString(iParN++, mues.getCD_TTECLAB());

    // Muestra Laboratorio
    st.setString(iParN++, mues.getCD_MUESTRA());

    // Valor Muestra
    st.setString(iParN++, mues.getCD_VMUESTRA());

    // Tipo Micobacteria
    st.setString(iParN++, mues.getCD_TMICOB());

    // Flag Resistente
    st.setString(iParN++, mues.getIT_RESISTENTE());

    /************** Par�metros del WHERE ****************/

    // Numero de tratamiento SECUENCIADOR !!!
    String iNumMues = mues.getNM_RESLAB().trim();
    st.setInt(iParN++, (new Integer(iNumMues)).intValue());

    // Codigo del equipo notificador
    st.setString(iParN++, notif.getCD_E_NOTIF());

    // A�o epidemiologico
    st.setString(iParN++, notif.getCD_ANOEPI());

    // Semana epidemiologica
    st.setString(iParN++, notif.getCD_SEMEPI());

    // Fecha recepcion
    String FRecep = notif.getFC_RECEP().trim();
    if (FRecep.length() > 0) {
      java.sql.Date fRecep = Fechas.StringToSQLDate(FRecep);
      st.setDate(iParN++, fRecep);
    }

    // Fecha notificacion
    String FNotif = notif.getFC_FECNOTIF().trim();
    if (FNotif.length() > 0) {
      java.sql.Date fNotif = Fechas.StringToSQLDate(FNotif);
      st.setDate(iParN++, fNotif);
    }

    // Numero de EDO
    String nmEdo = notif.getNM_EDO().trim();
    st.setInt(iParN++, (new Integer(nmEdo)).intValue());

    // Fuente
    st.setString(iParN++, notif.getCD_FUENTE());
  } // Fin instanciarUpdateQuery()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void instanciarDeleteQuery(DatCasMuesCS notif,
                                       DatCasMuesSC mues,
                                       PreparedStatement st) throws Exception {

    // Lleva la cuenta de ?
    int iParN = 1;

    // Numero de muestra
    String iNumMues = mues.getNM_RESLAB().trim();
    st.setInt(iParN++, (new Integer(iNumMues)).intValue());

    // Codigo del equipo notificador
    st.setString(iParN++, notif.getCD_E_NOTIF());

    // A�o epidemiologico
    st.setString(iParN++, notif.getCD_ANOEPI());

    // Semana epidemiologica
    st.setString(iParN++, notif.getCD_SEMEPI());

    // Fecha recepcion
    String FRecep = notif.getFC_RECEP().trim();
    if (FRecep.length() > 0) {
      java.sql.Date fRecep = Fechas.StringToSQLDate(FRecep);
      st.setDate(iParN++, fRecep);
    }

    // Fecha notificacion
    String FNotif = notif.getFC_FECNOTIF().trim();
    if (FNotif.length() > 0) {
      java.sql.Date fNotif = Fechas.StringToSQLDate(FNotif);
      st.setDate(iParN++, fNotif);
    }

    // Numero de EDO
    String nmEdo = notif.getNM_EDO().trim();
    st.setInt(iParN++, (new Integer(nmEdo)).intValue());

    // Fuente
    st.setString(iParN++, notif.getCD_FUENTE());

  } // Fin instanciarDeleteQuery()

  // Comprueba el CD_OPE y FC_ULTACT de la notificacion
  //  de la BD con la notificacion del cliente
  boolean notificacionModificada(Connection c, DatCasMuesCS notif) throws
      Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery;
    String ope, fultact;

    try {
      sQuery = "SELECT CD_OPE, FC_ULTACT FROM SIVE_NOTIF_EDOI " +
          "WHERE CD_E_NOTIF = ? AND " +
          "CD_ANOEPI = ? AND CD_SEMEPI = ? AND " +
          "FC_RECEP = ? AND FC_FECNOTIF = ? AND " +
          "NM_EDO = ? AND CD_FUENTE = ? ";

      // Preparacion de la sentencia SQL
      st = c.prepareStatement(sQuery);

      // INSTANCIACION DE LA QUERY

      // Lleva la cuenta de ?
      int iParN = 1;

      // Codigo del equipo notificador
      st.setString(iParN++, notif.getCD_E_NOTIF());

      // A�o epidemiologico
      st.setString(iParN++, notif.getCD_ANOEPI());

      // Semana epidemiologica
      st.setString(iParN++, notif.getCD_SEMEPI());

      // Fecha recepcion
      String FRecep = notif.getFC_RECEP().trim();
      if (FRecep.length() > 0) {
        //java.sql.Date fRecep = Fechas.StringToSQLDate(FRecep);
        java.sql.Date fRecep = new java.sql.Date(Fechas.string2Date(FRecep).
                                                 getTime());
        st.setDate(iParN++, fRecep);
      }

      // Fecha notificacion
      String FNotif = notif.getFC_FECNOTIF().trim();
      if (FNotif.length() > 0) {
        //java.sql.Date fNotif = Fechas.StringToSQLDate(FNotif);
        java.sql.Date fNotif = new java.sql.Date(Fechas.string2Date(FNotif).
                                                 getTime());
        st.setDate(iParN++, fNotif);
      }

      // Numero de EDO
      String nmEdo = notif.getNM_EDO().trim();
      st.setInt(iParN++, (new Integer(nmEdo)).intValue());

      // Fuente
      st.setString(iParN++, notif.getCD_FUENTE());

      // FIN INSTANCIACION DE LA QUERY

      // Ejecucion de la query
      rs = st.executeQuery();

      // Obtenemos CD_OPE y FC_ULTACT
      if (rs.next()) {
        ope = rs.getString("CD_OPE");
        fultact = Fechas.timestamp2String(rs.getTimestamp("FC_ULTACT"));
      }
      else {
        return true;
      }

      // Cierre del PreparedStatement y del ResultSet
      rs.close();
      rs = null;
      st.close();
      st = null;

    }
    catch (Exception ex) {
      c.rollback();
      return true;
    }

    if (!ope.equals(notif.getCD_OPE()) || !fultact.equals(notif.getFC_ULTACT())) {
      return true;
    }
    else {
      return false;
    }

  } // Fin notificacionModificada()

  // Modifica el CD_OPE y FC_ULTACT de la notificacion
  void modificarNotificacion(Connection c, DatCasMuesCS notif,
                             String sOpe, String sFechaUltact) throws Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery;

    try {
      sQuery = "UPDATE SIVE_NOTIF_EDOI SET CD_OPE = ?,FC_ULTACT = ? " +
          "WHERE CD_E_NOTIF = ? AND " +
          "CD_ANOEPI = ? AND CD_SEMEPI = ? AND " +
          "FC_RECEP = ? AND FC_FECNOTIF = ? AND " +
          "NM_EDO = ? AND CD_FUENTE = ? AND CD_OPE = ? AND FC_ULTACT = ?";

      // Preparacion de la sentencia SQL
      st = c.prepareStatement(sQuery);

      // INSTANCIACION DE LA QUERY

      // Lleva la cuenta de ?
      int iParN = 1;

      // CD_OPE NUEVO
      st.setString(iParN++, sOpe);
      // FC_ULTACT  NUEVO
      java.sql.Timestamp fUltact = Fechas.string2Timestamp(sFechaUltact);
      st.setTimestamp(iParN++, fUltact);

      // Codigo del equipo notificador
      st.setString(iParN++, notif.getCD_E_NOTIF());

      // A�o epidemiologico
      st.setString(iParN++, notif.getCD_ANOEPI());

      // Semana epidemiologica
      st.setString(iParN++, notif.getCD_SEMEPI());

      // Fecha recepcion
      String FRecep = notif.getFC_RECEP().trim();
      if (FRecep.length() > 0) {
        java.sql.Date fRecep = Fechas.StringToSQLDate(FRecep);
        st.setDate(iParN++, fRecep);
      }

      // Fecha notificacion
      String FNotif = notif.getFC_FECNOTIF().trim();
      if (FNotif.length() > 0) {
        java.sql.Date fNotif = Fechas.StringToSQLDate(FNotif);
        st.setDate(iParN++, fNotif);
      }

      // Numero de EDO
      String nmEdo = notif.getNM_EDO().trim();
      st.setInt(iParN++, (new Integer(nmEdo)).intValue());

      // Fuente
      st.setString(iParN++, notif.getCD_FUENTE());

      //cdopeantiguo
      st.setString(iParN++, notif.getCD_OPE());
      //fcultact antiguo
      st.setTimestamp(iParN++, Fechas.string2Timestamp(notif.getFC_ULTACT()));

      // FIN INSTANCIACION DE LA QUERY
      // Ejecucion del update
      int iActualizados = st.executeUpdate();

      //PREGUNTAR si ha modificado algo (ver de nuevo el bloqueo)
      if (iActualizados == 0) {
        throw (new Exception(constantes.PRE_BLOQUEO +
                             constantes.SIVE_NOTIF_EDOI));
      }

      // Cierre del PreparedStatement
      st.close();
      st = null;
    }
    catch (Exception ex) {
      throw ex;
    }
  } // Fin modificarNotificacion()

  // Modifica el CD_OPE y FC_ULTACT de la notificacion sin bloqueo
  void modificarNotificacion_SB(Connection c, DatCasMuesCS notif,
                                String sOpe, String sFechaUltact) throws
      Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery;

    try {
      sQuery = "UPDATE SIVE_NOTIF_EDOI SET CD_OPE = ?,FC_ULTACT = ? " +
          "WHERE CD_E_NOTIF = ? AND " +
          "CD_ANOEPI = ? AND CD_SEMEPI = ? AND " +
          "FC_RECEP = ? AND FC_FECNOTIF = ? AND " +
          "NM_EDO = ? AND CD_FUENTE = ? ";

      // Preparacion de la sentencia SQL
      st = c.prepareStatement(sQuery);

      // INSTANCIACION DE LA QUERY

      // Lleva la cuenta de ?
      int iParN = 1;

      // CD_OPE NUEVO
      st.setString(iParN++, sOpe);
      // FC_ULTACT  NUEVO
      java.sql.Timestamp fUltact = Fechas.string2Timestamp(sFechaUltact);
      st.setTimestamp(iParN++, fUltact);

      // Codigo del equipo notificador
      st.setString(iParN++, notif.getCD_E_NOTIF());

      // A�o epidemiologico
      st.setString(iParN++, notif.getCD_ANOEPI());

      // Semana epidemiologica
      st.setString(iParN++, notif.getCD_SEMEPI());

      // Fecha recepcion
      String FRecep = notif.getFC_RECEP().trim();
      if (FRecep.length() > 0) {
        java.sql.Date fRecep = Fechas.StringToSQLDate(FRecep);
        st.setDate(iParN++, fRecep);
      }

      // Fecha notificacion
      String FNotif = notif.getFC_FECNOTIF().trim();
      if (FNotif.length() > 0) {
        java.sql.Date fNotif = Fechas.StringToSQLDate(FNotif);
        st.setDate(iParN++, fNotif);
      }

      // Numero de EDO
      String nmEdo = notif.getNM_EDO().trim();
      st.setInt(iParN++, (new Integer(nmEdo)).intValue());

      // Fuente
      st.setString(iParN++, notif.getCD_FUENTE());

      // FIN INSTANCIACION DE LA QUERY
      // Ejecucion del update
      st.executeUpdate();

      // Cierre del PreparedStatement
      st.close();
      st = null;
    }
    catch (Exception ex) {
      throw ex;
    }
  } // Fin modificarNotificacion_SB()

  // Insercion de resistencias de una muestra
  void insertarResistencias(Connection c, int nm_reslab, CLista res) throws
      Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    String sQuery;

    if (res == null) {
      return;
    }
    if (res.size() == 0) {
      return;
    }

    DatCasResis r = null;
    for (int i = 0; i < res.size(); i++) {
      r = (DatCasResis) res.elementAt(i);
      try {
        sQuery = "INSERT INTO SIVE_RES_RESISTENCIA " +
            "(NM_RESLAB, CD_ESTREST) VALUES (?,?)";

        // Preparacion de la sentencia SQL
        st = c.prepareStatement(sQuery);

        // INSTANCIACION DE LA QUERY

        // Numero Muestra
        st.setInt(1, nm_reslab);

        // CD_ESTREST
        st.setString(2, r.getCD_ESTREST());

        // FIN INSTANCIACION DE LA QUERY

        // Ejecucion del update
        st.executeUpdate();

        // Cierre del PreparedStatement
        st.close();
        st = null;

        // NO se valida aqui la transaccion sino que se devuelve para ser validada tras insertar la muestra completa
        // con.commit();

      }
      catch (Exception ex) {
        //c.rollback();
        throw ex;
      }
    } // Fin for
  } // Fin insertarResistencias()

  // Eliminacion de todas las resistencias asociadas a una muestra
  void eliminarResistencias(Connection c, int nm_reslab) throws Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    String sQuery;

    try {
      sQuery = "DELETE FROM SIVE_RES_RESISTENCIA " +
          "WHERE NM_RESLAB = ?";

      // Preparacion de la sentencia SQL
      st = c.prepareStatement(sQuery);

      // INSTANCIACION DE LA QUERY: Numero Muestra
      st.setInt(1, nm_reslab);

      // Ejecucion del update
      st.executeUpdate();

      // Cierre del PreparedStatement
      st.close();
      st = null;

    }
    catch (Exception ex) {
      //c.rollback();
      throw ex;
    }

  } // Fin eliminarResistencias

} // Fin SrvDiaTrat
