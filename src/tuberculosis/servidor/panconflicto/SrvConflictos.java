package tuberculosis.servidor.panconflicto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import comun.SrvDebugGeneral;
import comun.constantes;
import tuberculosis.datos.panconflicto.DatConflicto;

public class SrvConflictos
    extends SrvDebugGeneral {
  // Modos de funcionamiento del servlet
  private final int modoLEERDATOS = constantes.modoLEERDATOS;

  // objetos JDBC
  Connection conexion = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {
    // Lista de valores de salida
    CLista clRetorno = null;

    if (super.con != null) {
      conexion = super.con; // Se est� en modo debug
    }
    else {
      conexion = openConnection(); // Modo real
    }

    conexion.setAutoCommit(false);

    try {
      switch (opmode) {
        case modoLEERDATOS:
          clRetorno = clSelectConflictos(param);
          break;
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
    finally {
      closeConnection(conexion);
    }

    return clRetorno;
  }

  // m�todos privados
  private CLista clSelectConflictos(CLista p) throws Exception {
    //
    //java.sql.Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
    //System.out.println(Fechas.timestamp2String(ts));
    //
    CLista clResultado = null;
    CLista clTemporal = null;

    String sQuery = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    DatConflicto dc = null;

    String sA�o = null;
    String sRegistro = null;
    String sCdArea = null;
    String sCdDistrito = null;

    String sQueryA�o = "";
    String sQueryA�oEDO = "";
    String sQueryRegistro = "";
    String sQueryCdArea = "";
    String sQueryCdDistrito = "";

    int iContador = 0;

    // Recuperamos los par�metros de b�squeda
    dc = (DatConflicto) p.firstElement();

    sA�o = dc.getCD_ARTBC_RT();
    sRegistro = dc.getCD_NRTBC_RT();
    sCdArea = dc.getCD_NIVEL_1_GE_RT();
    sCdDistrito = dc.getCD_NIVEL_2_GE_RT();

    if (!sA�o.equals("")) {
      sQueryA�o = " and CD_ARTBC = ? ";
      sQueryA�oEDO = " CD_ARTBC = ? ";
    }

    if (!sRegistro.equals("")) {
      sQueryRegistro = " and CD_NRTBC = ? ";
    }

    if (!sCdArea.equals("")) {
      sQueryCdArea = " and CD_NIVEL_1_GE = ? ";
    }

    if (!sCdDistrito.equals("")) {
      sQueryCdDistrito = " and CD_NIVEL_2_GE = ? ";
    }

    // Recuperamos los datos apropiados de SIVE_MOVRESPEDO
    sQuery = " select NM_EDO, CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA " +
        " from SIVE_MOVRESPEDO where IT_CONFLICTO = ? and " +
        " NM_EDO in " +
        " (select distinct NM_EDO from SIVE_REGISTROTBC where " +
        sQueryA�oEDO + sQueryRegistro + sQueryCdArea + sQueryCdDistrito +
        " )" +
        " group by  NM_EDO, CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA ";

    st = conexion.prepareStatement(sQuery);
    st.setString(1, "S");

    iContador = 2;

    if (!sQueryA�oEDO.equals("")) {
      st.setString(iContador, sA�o);
      iContador++;
    }

    if (!sQueryRegistro.equals("")) {
      st.setString(iContador, sRegistro);
      iContador++;
    }

    if (!sQueryCdArea.equals("")) {
      st.setString(iContador, sCdArea);
      iContador++;
    }

    if (!sQueryCdDistrito.equals("")) {
      st.setString(iContador, sCdDistrito);
    }

    rs = st.executeQuery();

    clResultado = new CLista();
    clTemporal = new CLista();

    // Se almacenan los conflictos obtenidos en clTemporal
    while (rs.next()) {
      dc = new DatConflicto();

      dc.setNM_EDO_MRE(rs.getInt("NM_EDO"));
      dc.setCD_TSIVE_MRE(rs.getString("CD_TSIVE"));
      dc.setCD_MODELO_MRE(rs.getString("CD_MODELO"));
      dc.setNM_LIN_MRE(rs.getInt("NM_LIN"));
      dc.setCD_PREGUNTA_MRE(rs.getString("CD_PREGUNTA"));

      clTemporal.addElement(dc);
    }

    sQuery = null;
    st = null;
    rs = null;

    // Para todos los conflictos detectados, recuperamos los datos restantes
    sQuery =
        " select rt.CD_ARTBC, rt.CD_NRTBC, rt.CD_NIVEL_1_GE, rt.CD_NIVEL_2_GE, " +
        " rt.FC_INIRTBC, l.DS_TEXTO " +
        " from SIVE_REGISTROTBC rt, SIVE_LINEASM l " +
        " where rt.NM_EDO = ? and l.CD_TSIVE = ? and l.CD_MODELO = ? and l.NM_LIN = ? " +
        sQueryA�o +
        sQueryRegistro +
        sQueryCdArea +
        sQueryCdDistrito;

    st = conexion.prepareStatement(sQuery);

    for (int i = 0; i < clTemporal.size(); i++) {
      dc = (DatConflicto) clTemporal.elementAt(i);

      st.setInt(1, dc.getNM_EDO_MRE());
      st.setString(2, dc.getCD_TSIVE_MRE());
      st.setString(3, dc.getCD_MODELO_MRE());
      st.setInt(4, dc.getNM_LIN_MRE());

      iContador = 5;

      if (!sQueryA�o.equals("")) {
        st.setString(iContador, sA�o);
        iContador++;
      }

      if (!sQueryRegistro.equals("")) {
        st.setString(iContador, sRegistro);
        iContador++;
      }

      if (!sQueryCdArea.equals("")) {
        st.setString(iContador, sCdArea);
        iContador++;
      }

      if (!sQueryCdDistrito.equals("")) {
        st.setString(iContador, sCdDistrito);
      }

      rs = st.executeQuery();

      if (rs.next()) {
        //dc = (DatConflicto) clTemporal.elementAt(i);

        dc.setCD_ARTBC_RT(rs.getString("CD_ARTBC"));
        dc.setCD_NRTBC_RT(rs.getString("CD_NRTBC"));
        dc.setCD_NIVEL_1_GE_RT(rs.getString("CD_NIVEL_1_GE"));
        dc.setCD_NIVEL_2_GE_RT(rs.getString("CD_NIVEL_2_GE"));
        dc.setFC_INIRTBC_RT(rs.getDate("FC_INIRTBC"));
        dc.setDS_TEXTO_LA(rs.getString("DS_TEXTO"));

        //break; // Innecesario te�ricamente
      }

      clResultado.addElement(dc);

    }

    // Devolvemos...
    return clResultado;
  }

  // Para modo debug
  protected CLista doDebug() throws Exception {
    CLista data = new CLista();
    DatConflicto dc = new DatConflicto();

    dc.setCD_ARTBC_RT("1999");
    dc.setCD_NRTBC_RT("108");
    dc.setCD_NIVEL_1_GE_RT("1");
    dc.setCD_NIVEL_2_GE_RT("");

    data.addElement(dc);

    return doPrueba(modoLEERDATOS, data);
  }

  // prueba del servlet
  public static void main(String[] args) {
    try {
      SrvConflictos srv = new SrvConflictos();
      srv.doDebug();
    }
    catch (Exception e) {
      System.out.println(e.toString());
    }
    finally {
      System.exit(0);
    }
  }

}