/**
 * Clase: SrvCasResis
 * Paquete: tuberculosis.servidor.panmues
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 01/12/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: Implementacion del servlet que recupera de la BD los datos
 *   a mostrar en la tabla del DiaMues: resistencias de una muestra.
 */

package tuberculosis.servidor.panmues;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import sapp.DBServlet;
import tuberculosis.datos.panmues.DatCasMuesSC;
import tuberculosis.datos.panmues.DatCasResis;

public class SrvCasResis
    extends DBServlet {

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista();
    DatCasMuesSC dataEntrada = null;
    DatCasResis dataSalida = null;

    // Querys
    String sQuery = "";

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con = openConnection();
    con.setAutoCommit(false);

    try {
      // Recuperacion de los datos de busqueda
      dataEntrada = (DatCasMuesSC) param.firstElement();

      // Preparacion del patron de la query
      sQuery = "SELECT CD_ESTREST " +
          "FROM SIVE_RES_RESISTENCIA " +
          "WHERE NM_RESLAB = ?";

      // Preparacion de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Instanciamos la query con los valores de los datos de entrada
      st.setString(1, dataEntrada.getNM_RESLAB());

      // Ejecucion de la query, obteniendo un ResultSet
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      while (rs.next()) {

        // Recuperacion del CD_ESTREST a partir del ResultSet de
        //  la query realizada sobre la BD
        String CodResis = rs.getString("CD_ESTREST");
        String DescResis = getDescResis(con, CodResis);
        String Ope = "";
        String FUlt = "";

        // Relleno del registro de salida correspondiente a esta iteracion por
        //  los registros devueltos por la query
        dataSalida = new DatCasResis(CodResis, DescResis, Ope, FUlt);

        // Adicion del registro obtenido a la lista resultado de salida hacia cliente
        listaSalida.addElement(dataSalida);
      } // Fin for each registro

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);

    return listaSalida;
  } // Fin doWork()

  // Obtiene la descr. de una resistencia
  String getDescResis(Connection c, String CodResis) throws Exception {
    if (CodResis == null || CodResis.equals("")) {
      return "";
    }

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Query
    String Query =
        "select DS_ESTREST from SIVE_ESTUDIORESIS where CD_ESTREST = ?";
    String res = null;

    try {
      // Preparacion de la sentencia SQL
      st = c.prepareStatement(Query);

      // CodResis
      st.setString(1, CodResis.trim());

      // Ejecucion de la query
      rs = st.executeQuery();

      // Almacenamos la DS_ESTREST obtenida
      if (rs.next()) {
        res = rs.getString("DS_ESTREST");

        // Cierre del ResultSet y del PreparedStatement
      }
      rs.close();
      rs = null;
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }

    return res;
  } // Fin getDescResis()

} // Fin SrvCasMues
