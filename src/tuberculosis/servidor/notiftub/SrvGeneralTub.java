package tuberculosis.servidor.notiftub;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.StringTokenizer;

import capp.CLista;
import comun.Fechas;
// E
import jdbcpool.JDCConnectionDriver;
import sapp.DBServlet;

/**
 *  Esta clase est� en el servidor recibe una CLista
 *  recoge de ella el nombre de la clase de datos y los par�metros
 *  ejecuta la sentencia SQL
 *  rellena una CLISTA con objetos de datos
 *  y finaliza la conexi�n
 */
public abstract class SrvGeneralTub
    extends DBServlet {

  // modos de operaci�n
  public static final int servletALTA = 0;
  public static final int servletMODIFICAR = 1;
  public static final int servletBAJA = 2;
  public static final int servletOBTENER_X_CODIGO = 3;
  public static final int servletOBTENER_X_DESCRIPCION = 4;
  public static final int servletSELECCION_X_CODIGO = 5;
  public static final int servletSELECCION_X_DESCRIPCION = 6;

  /** para reenviar los errores */
  String strError = null;

  /** conexi�n que se mantiene cada vez en el servidor */
  Connection con = null;

  /** statement de la sentencia SQL */
  PreparedStatement st = null;

  /** resultados de la query */
  ResultSet rs = null;

  /** sentencia sql que se est� ejecutando */
  StringBuffer strSQL = null;

  /** son los datos de los parametros */
  CLista param = null;

  /**  indica el n�mero de datos que hay que traer */
  int iNumReg = 0;

  /** indic asi es prueba o real */
  public boolean breal = true;

  //_______ L RIVERA _________________
  /*indica si en la consulta actual es necesario tener en cuenta los idiomas*/
  protected boolean bTratarIdiomas = false;
  //Hashtable para guardar parejas de campos en los que hay que tratar idiomas
  //P. ejemplo ("DS_NIVEL_1", "DSL_NIVEL_1")
  protected Hashtable hashIdiomas = new Hashtable();

  //__________________________________

  /// funciones abstractas para que las implementes los hijos
  //________________________________________________________________________
  /** devuelve la sentencia de SQL pedida
   *  @param a_imodo es el n�mero que indica la sentencia SQL
   *  @param a_data es el tipo de datos que tiene los parametros
   *  @param a_result  es la cadena que se va a devolver con la sentencia SQL
   *  @countEltos  devuelve el n�mero de registros que se tienen que leer
   */
//  protected abstract int prepareSQLSentencia  ( int a_imodo, Object a_data, StringBuffer  a_result);
  // a_result es el n�mero de sentencias y adem�s se le pasa la conexi�n
  // devuelve el stratement
  public abstract PreparedStatement prepareSQLSentencia(int a_imodo,
      Object a_data, StringBuffer a_result, StringBuffer countEltos,
      Connection con);

  //L RIVERA
  /* devuelve un booleano que indica si es necesario hacer tratamiento de idiomas o no en la consulta
   * hashIdiomas es la variable hashIdiomas que recoge las parejas de nombres de campos DS_CAMPO y DSL_CAMPO
   **  @param a_imodo es el n�mero que indica la sentencia SQL
   *Si hay que hacer tratamiento de idiomas se llamar� luego en cada registro recogido a la funci�n tratarIdiomas
   */

  //public abstract boolean prepararTratamientoIdiomas(int a_imodo, Hashtable hashIdiomas);

  //________________________________________________________________________

  protected CLista doWork(int opmode, CLista param) throws Exception {
    CLista result = null;
    this.param = param;

    if (param == null) {
      // no nos han pasado datos iniciales
      return null;
    }

    // establece la conexi�n con la base de datos
    if (breal) {
      con = openConnection();
    }

    // establecemos las propiedades indicadas
    con.setAutoCommit(false);

    result = realiza_SQL(opmode, param);
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return result;
  }

  //________________________________________________________________________

  public CLista doPrueba(int opmode, CLista param) throws Exception {
    breal = false;
    // establece la conexi�n con la base de datos
    /*      Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
          con  = DriverManager.getConnection("jdbc:odbc:thin:@194.140.66.208:1521:ORCL", "sive_desa", "sive_desa");
     */
    jdcdriver = new JDCConnectionDriver("oracle.jdbc.driver.OracleDriver",
        "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
        "sive_desa",
        "sive_desa");
    con = openConnection();
    return doWork(opmode, param);
  }

  //________________________________________________________________________

  // funcionalidad del servlet
  /**
   *  sevlet generico recibe los parametros de entrada en la CLista
   *  y devuelve una CLista con los resultados
   */
  protected CLista realiza_SQL(int opmode, CLista a_param) throws Exception {

    // objetos de datos
    CLista result_data = null;

    /** parametros de entrada */
    CLista param = a_param;
    int iValor = 1;

    // prepara la sentencia con sus par�metros
    strSQL = new StringBuffer();
    StringBuffer numEltos = new StringBuffer();

    st = prepareSQLSentencia(opmode, param.firstElement(), strSQL, numEltos,
                             con);

    //_____________

    if (strSQL.length() <= 0) {
      return null;
    }
    else {
      iNumReg = Integer.parseInt(numEltos.toString());
    }

    if (iNumReg > 0) {
      // lo que se ha lanzado es una select
      rs = st.executeQuery();
      result_data = recogerDatos();
      rs.close();
      rs = null;
    }
    else {
      // se est� modificando la base de datos
      st.executeUpdate();
      // siempre se devuelve una lista aunque sea vacia
      result_data = new CLista();
    }

    // cerramos el cursor
    st.close();
    st = null;

    if (result_data != null) {
      result_data.trimToSize();
    }

    return result_data;
  }

  //________________________________________________________________________

  public CLista recogerDatos() {
    CLista v = new CLista();
    Hashtable h;
    Object o;
    StringTokenizer parserSQL = null;
    String token;
    int num_datos = 0;
    Object campo_filtro = null;
    int i;
    String fecha;

    String fc_ultact = "";

    try {
      //Se recorren todos los registros del ResultSet rs
      // llevando cuenta de cuantos llevamos
      for (i = 1; rs.next(); i++) { //Cambiado i de 0 a 1*****
        h = new Hashtable(); //(Hashtable) param.firstElement();

        //Se tokeniza la sent. SQL para saber a partir de ella los campos
        //que sea necesario recoger de cada registro del resultSet rs
        parserSQL = new StringTokenizer(strSQL.toString(), ", ");
        token = parserSQL.nextToken();

        //Primer token es el "SELECT" : lo saltamos siempre
        token = sSinPunto(parserSQL.nextToken());

        //Entre el segundo token (actual ) hasta encontrar el "FROM"
        //Estar�n los campos buscados
        for (; ! (token.equals("FROM")); ) {

          // Si hay un join quitamos la marca del join
          //para luego poder hacer p.ejemplo getObject("CD_NIVEL_1")
          //en vez de  un getObject("a.CDNIVEL1")
          //if (token.charAt(1) == '.') {
          //   token = token.substring(2);
          //}

          //else
          if (token.equals("DISTINCT")) {
            token = parserSQL.nextToken();
            continue;
          }

          /*if (token.startsWith("FC_ULTACT")) {
             try {
               fc_ultact = timestamp_a_cadena(rs.getTimestamp(token));
               if (fc_ultact != null) {
                 h.put(token, fc_ultact);
               } else {
                 h.put(token, "");
               }
             } catch (SQLException e) {
               System.out.println(e.getMessage());
             }
                              } */

          //En los tokens correspondientes a campos que haya que consultar
          //Se recoge el objeto (puede ser de cualquier clase) de la base de datos
          else {
            try {
              o = rs.getObject(token);
              if (o != null) { // si es null ...

                // convierte las fechas a cadena
                if (o.getClass().getName().equals("java.sql.Timestamp")) {
                  // E 17/01/2000
                  if (token.startsWith("FC_ULTACT")) {
                    fecha = Fechas.timestamp2String( (java.sql.Timestamp) o);
                  }
                  else {
                    fecha = (String) Fechas.date2String( (java.util.Date) o);
                  }

                  h.put(token, fecha);

                }
                else if (o.getClass().getName().equals("java.sql.Date")) {

                  fecha = (String) Fechas.date2String( (java.util.Date) o);
                  h.put(token, fecha);

                }
                else if (o.getClass().getName().equals("java.math.BigDecimal")) {
                  //cod = new Integer( ((Number) o).intValue() );
                  h.put(token, new Integer( ( (Number) o).intValue()));
                }
                else {
                  h.put(token, (String) o.toString());
                }
              }
              else { // Se inserta con una cadena vac�a
                h.put(token, "");
              }
            }
            catch (SQLException e) {
              o = null;
            }
          }

          token = sSinPunto(parserSQL.nextToken());
        }

        v.addElement(h);
      } //Fin for de recorrido registros

      // vemos si est� llena la lista
      if (i <= iNumReg) {
        v.setState(CLista.listaLLENA);
      }

      return v;
    }

    catch (SQLException exc) {
      exc.printStackTrace();
      strError = exc.toString();
      return null;
    }
  }

  // Quita, en una expresi�n del tipo de 'str.CD_ENFERMO'
  // los caracteres a la izda. del punto, incluido �ste
  private String sSinPunto(String sCadena) {
    int iPosicion = sCadena.indexOf('.') + 1;
    return sCadena.substring(iPosicion);
  }

} //________________________________________________ END_CLASS
