package tuberculosis.servidor.notiftub;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;

import capp.CLista;
import comun.Fechas;
import comun.constantes;
import jdbcpool.JDCConnectionDriver;
import tuberculosis.datos.notiftub.DatCasosTub;
import tuberculosis.datos.notiftub.DatNotifTub;

public class SrvNotifTub
    extends SrvGeneralTub {
  /** hashtable que contiene los datos */
  Hashtable parameter = null;

  // Metamodos de funcionamiento
  public static final int servletSELECT_NORMAL = 1;
  public static final int servletSELECT_SUCA = 2;

  // Estados que corresponden a cada sentencia SQL
  // son los modos de operaci�n
  public final int modoLEERDATOS = constantes.modoLEERDATOS;
  public final int modoMODIFICACION = constantes.modoMODIFICACION;
  public final int modoALTA = constantes.modoALTA;
  public final int modoBAJA = constantes.modoBAJA;

  public final int modoMODIFICACION_SB = constantes.modoMODIFICACION_SB;
  public final int modoALTA_SB = constantes.modoALTA_SB;
  public final int modoBAJA_SB = constantes.modoBAJA_SB;

  // Opciones
  private final int iDatosETE = 1;
  private final int iDatosNotificadores = 2;
  private final int iDatosTratamientos = 3;

  // Metamodo actual
  private int iMetaModo = 0;

  // Modo actual
  private int iModo = 0;

  // Para recuperar el registro de tuberculosis
  private String sCD_ARTBC = null;
  private String sCD_NRTBC = null;

  // Para saber con qu� caso se est� trabajando
  private Integer intNM_EDO = null;

  // Para saber con qu� notificador se est� trabajando
  private String sCD_E_NOTIF = null;
  private String sCD_ANOEPI = null;
  private String sCD_SEMEPI = null;
  private java.sql.Date dFC_RECEP = null;
  private java.sql.Date dFC_FECNOTIF = null;
  private String sCD_FUENTE = null;

  // Cadenas de consulta

  // SIVE_REGISTROTBC
  private static final String sSQL_TBC =
      " srt.CD_ARTBC, srt.CD_NRTBC, srt.FC_INIRTBC, srt.NM_EDO NM_EDO_TBC, " +
      "srt.FC_SALRTBC, srt.DS_OBSERV DS_OBSERV_TBC, srt.CD_MOTSALRTBC, srt.CD_E_NOTIF CD_E_NOTIF_TBC, " +
      "srt.DS_MEDICO, srt.CD_OPE CD_OPE_TBC, srt.FC_ULTACT FC_ULTACT_TBC, " +
      "srt.CD_NIVEL_1_GE, srt.CD_NIVEL_2_GE ";

  // SIVE_EDOIND
  private static final String sSQL_EDOIND =
      " sed.NM_EDO, sed.CD_ANOEPI, sed.CD_SEMEPI, sed.CD_ANOURG, " +
      "sed.NM_ENVIOURGSEM, sed.CD_CLASIFDIAG, sed.IT_PENVURG, sed.CD_ENFERMO CD_ENFERMO_EDOIND, " +
      "sed.CD_PROV CD_PROV_EDOIND, sed.CD_MUN CD_MUN_EDOIND, sed.CD_NIVEL_1 CD_NIVEL_1_EDOIND, sed.CD_NIVEL_2 CD_NIVEL_2_EDOIND, sed.CD_ZBS CD_ZBS_EDOIND, " +
      "sed.FC_RECEP, sed.DS_CALLE, sed.DS_NMCALLE, sed.DS_PISO DS_PISO_EDOIND, sed.CD_POSTAL CD_POSTAL_EDOIND, " +
      "sed.CD_ANOOTRC, sed.NM_ENVOTRC, sed.CD_ENFCIE, sed.FC_FECNOTIF, sed.IT_DERIVADO, " +
      "sed.DS_CENTRODER, sed.IT_DIAGCLI, sed.IT_DIAGMICRO, sed.IT_DIAGSERO, " +
      "sed.DS_DIAGOTROS, sed.FC_INISNT, sed.DS_COLECTIVO, sed.IT_ASOCIADO, " +
      "sed.DS_ASOCIADO, sed.CDVIAL CDVIAL_EDOIND, sed.CDTVIA CDTVIA_EDOIND, sed.CDTNUM CDTNUM_EDOIND, sed.DSCALNUM DSCALNUM_EDOIND, " +
      "sed.CD_OPE CD_OPE_EDOIND, sed.FC_ULTACT FC_ULTACT_EDOIND ";

  // SIVE_ENFERMO
  private static final String sSQL_ENFERMO =
      " se.CD_ENFERMO, se.CD_TDOC, se.DS_APE1, se.DS_APE2, se.SIGLAS, se.DS_NOMBRE, " +
      "se.DS_FONOAPE1, se.DS_FONOAPE2, se.DS_FONONOMBRE, se.IT_CALC, se.FC_NAC, " +
      "se.CD_SEXO, se.CD_POSTAL, se.DS_DIREC, se.DS_NUM, se.DS_PISO, se.DS_TELEF, " +
      "se.CD_NIVEL_1, se.CD_NIVEL_2, se.CD_ZBS, se.CD_OPE, se.FC_ULTACT, " +
      "se.IT_REVISADO, se.CD_MOTBAJA, se.FC_BAJA, se.DS_OBSERV, se.CD_PROV2, " +
      "se.CD_MUNI2, se.CD_POST2, se.DS_DIREC2, se.DS_NUM2, se.DS_PISO2, se.DS_OBSERV2, " +
      "se.CD_PROV3, se.CD_MUNI3, se.CD_POST3, se.DS_DIREC3, se.DS_NUM3, " +
      "se.DS_PISO3, se.DS_TELEF3, se.DS_OBSERV3, se.DS_NDOC, se.DS_TELEF2, " +
      "se.CDVIAL, se.CDTVIA, se.CDTNUM, se.DSCALNUM, se.CD_PROV, se.CD_MUN, " +
      "se.IT_ENFERMO, se.CD_PAIS, se.IT_CONTACTO ";

  // SIVE_NOTIF_EDOI
  private static final String sSQL_NOTIF_EDOI =
      " sne.CD_E_NOTIF CD_E_NOTIF_EDOI, sne.CD_ANOEPI CD_ANOEPI_EDOI, sne.CD_SEMEPI CD_SEMEPI_EDOI, " +
      "sne.NM_EDO NM_EDO_EDOI, sne.FC_FECNOTIF, " +
      "sne.FC_RECEP FC_RECEP_EDOI, sne.CD_FUENTE, sne.DS_DECLARANTE, sne.IT_PRIMERO, sne.CD_OPE CD_OPE_EDOI, " +
      "sne.FC_ULTACT FC_ULTACT_EDOI, sne.DS_HISTCLI ";

  // SIVE_NOTIFEDO
  private static final String sSQL_NOTIFEDO =
      " sn.CD_E_NOTIF CD_E_NOTIF_EDO, sn.CD_ANOEPI CD_ANOEPI_EDO, sn.CD_SEMEPI, " +
      "sn.FC_RECEP FC_RECEP_EDO, sn.FC_FECNOTIF FC_NOTIF_EDO, " +
      "sn.NM_NNOTIFR, sn.IT_RESSEM, sn.CD_OPE CD_OPE_EDO, sn.FC_ULTACT FC_ULTACT_EDO, " +
      "sn.IT_VALIDADA, sn.FC_FECVALID ";

  // SIVE_E_NOTIF
  private static final String sSQL_E_NOTIF =
      " en.DS_E_NOTIF, en.CD_NIVEL_1, en.CD_NIVEL_2, en.CD_ZBS, " +
      " en.CD_OPE CD_OPE_EQ, en.FC_ULTACT FC_ULTACT_EQ ";

  // SIVE_C_NOTIF
  private static final String sSQL_C_NOTIF =
      " c.CD_CENTRO, c.DS_CENTRO, c.IT_COBERTURA ";

  // SIVE_NOTIF_SEM
  private static final String sSQL_S_NOTIF =
      " sns.NM_NNOTIFT NM_NNOTIFT_SEM, sns.NM_NTOTREAL NM_NTOTREAL_SEM, " +
      " sns.CD_OPE CD_OPE_SEM, sns.FC_ULTACT FC_ULTACT_SEM ";

  // SIVE_TRATAMIENTOS
  private static final String sSQL_TRATAMIENTOS =
      " st.NM_TRATRTBC, st.CD_E_NOTIF CD_E_NOTIF_TRA, st.CD_ANOEPI CD_ANOEPI_TRA, " +
      " st.CD_SEMEPI CD_SEMEPI_TRA, st.FC_RECEP FC_RECEP_TRA, st.FC_FECNOTIF FC_FECNOTIF_TRA, " +
      " st.NM_EDO NM_EDO_TRA, st.CD_FUENTE CD_FUENTE_TRA, st.FC_INITRAT, st.CD_MOTRATINI, " +
      " st.FC_FINTRAT, st.CD_MOTRATFIN, st.DS_OBSERV ";

  // Para recuperar datos de CA, MUN y ZBS
  private static final String sSQL_C_NORMAL =
      "SELECT CD_CA FROM SIVE_PROVINCIA WHERE CD_PROV = ?";
  private static final String sSQL_C_SUCA =
      "SELECT CDCOMU FROM SUCA_PROVINCIA WHERE CDPROV = ?";
  private static final String sSQL_M_NORMAL =
      "SELECT DS_MUN FROM SIVE_MUNICIPIO WHERE CD_PROV = ? AND CD_MUN = ?";
  private static final String sSQL_M_SUCA =
      "SELECT DSMUNI FROM SUCA_MUNICIPIO WHERE CDPROV = ? AND CDMUNI = ?";

  private static final String sSQL_ZBS_NORMAL = "SELECT DS_ZBS FROM SIVE_ZONA_BASICA WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ? AND CD_ZBS = ?";

  /** constructor por defecto */
  public SrvNotifTub() {
  }

  /** devuelven las clases de datos propias de este servlet */
  public Object getNewDatCasosTub() {
    return new DatCasosTub();
  }

  public Object getNewDatNotifTub() {
    return new DatNotifTub();
  }

  //________________________________________________________________________

  protected CLista doWork(int opmode, CLista param) throws Exception {
    //protected CLista doPrueba(int opmode, CLista param) throws Exception {
    String descrip = null;
    Hashtable hash = null;
    CLista result = null;
    this.param = param;

    boolean bComprobarBloqueo = true;
    //Para contar los registros que se llevan introducidos
    //int numRegistros = 1;

    if (param == null) {
      // no nos han pasado datos iniciales
      return null;
    }

    parameter = (DatCasosTub) param.firstElement();

    // Se recuperan los par�metros b�sicos para comenzar a
    // trabajar
    iModo = opmode;
    if (parameter.get("META_MODO") != null) {
      iMetaModo = ( (Integer) parameter.get("META_MODO")).intValue();

    }
    sCD_ARTBC = (String) parameter.get("CD_ARTBC");
    sCD_NRTBC = (String) parameter.get("CD_NRTBC");

    // establece la conexi�n con la base de datos
    if (breal) {
      con = openConnection();
    }

    con.setAutoCommit(false);

    try {
      switch (opmode) {
        case modoLEERDATOS:
          con.setAutoCommit(true);
          result = clSelectDatos(param);
          break;
        case modoMODIFICACION_SB:
          bComprobarBloqueo = false;
        case modoMODIFICACION:
          result = clUpdateDatos(param, bComprobarBloqueo);
          //if (!breal) con.rollback();
          break;
        case modoALTA_SB:
          bComprobarBloqueo = false;
        case modoALTA:
          result = clInsertDatos(param, bComprobarBloqueo);
          //if (!breal) con.rollback();
          break;
        case modoBAJA_SB:
          bComprobarBloqueo = false;
        case modoBAJA:
          result = clDeleteDatos(param, bComprobarBloqueo);
          //if (!breal) con.rollback();
          break;
      }
      con.commit();
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
      con.rollback();
      result = null;
      throw e;
    }
    finally {
      closeConnection(con);
    }

    return result;
  }

  /** Bloque de c�digo espec�fico del servlet */
  // Inserci�n de datos que pueden ser nulos
  private void insertaDato(int columna, PreparedStatement statement, int tipo,
                           String valor) throws Exception {
    if (valor != null && !valor.equals("")) {
      switch (tipo) {
        case java.sql.Types.INTEGER:
          statement.setInt(columna, new Integer(valor).intValue());
          break;
        case java.sql.Types.VARCHAR:
          statement.setString(columna, valor);
          break;
        case java.sql.Types.TIMESTAMP:
          statement.setTimestamp(columna, Fechas.string2Timestamp(valor));
          break;
        case java.sql.Types.DATE:
          statement.setDate(columna,
                            new java.sql.Date(Fechas.string2Date(valor).getTime()));
          break;
      }
    }
    else {
      statement.setNull(columna, java.sql.Types.VARCHAR);
    }
  }

  // Actualizaci�n de los datos asociados
  private CLista clUpdateDatos(CLista clLista, boolean bBloqueo) throws
      Exception {
    // Fecha actual
    java.sql.Timestamp tsFechaActual = new java.sql.Timestamp( (new java.util.
        Date()).getTime());

    // modificacion
    String tsFechaActualString = null;
    tsFechaActualString = Fechas.timestamp2String(tsFechaActual);

    java.sql.Timestamp tsFechabuena = null;
    tsFechabuena = Fechas.string2Timestamp(tsFechaActualString);

    // Objetos para almacenar los datos recibidos
    DatCasosTub dctCaso = null;

    dctCaso = (DatCasosTub) clLista.firstElement();

    // Aqu� se preguntar� si hay algo modificado
    //modificarEnfermo(dctCaso, new Integer(dctCaso.getCD_ENFERMO()).intValue(), clLista.getLogin(), tsFechaActual, bBloqueo);
    modificarEnfermo(dctCaso, new Integer(dctCaso.getCD_ENFERMO()).intValue(),
                     clLista.getLogin(), tsFechabuena, bBloqueo);

    //System.out.println(dctCaso.getNM_EDO_Integer());
    //modificarCasoEDOIND(dctCaso, dctCaso.getNM_EDO_Integer().intValue(), new Integer(dctCaso.getCD_ENFERMO()).intValue(), clLista.getLogin(), tsFechaActual, bBloqueo);
    modificarCasoEDOIND(dctCaso, dctCaso.getNM_EDO_Integer().intValue(),
                        new Integer(dctCaso.getCD_ENFERMO()).intValue(),
                        clLista.getLogin(), tsFechabuena, bBloqueo);

    //
    return clLista;
  }

  // Borrado masivo de los datos asociados
  private CLista clDeleteDatos(CLista clLista, boolean bBloqueo) throws
      Exception {
    // Fecha actual
    java.sql.Timestamp tsFechaActual = new java.sql.Timestamp( (new java.util.
        Date()).getTime());

    // Objetos para almacenar los datos recibidos
    DatCasosTub dctCaso = null;
    Vector vNotificadores = null;

    dctCaso = (DatCasosTub) clLista.firstElement();
    vNotificadores = (Vector) clLista.elementAt(1);

    // Se mueven a las tablas de bajas correspondientes los
    // registros de SIVE_EDOIND y SIVE_REGISTROTBC
    insertarBajasRegistros(dctCaso);

    // Borrar f�sicamente registro de tuberculosis, llev�ndolo a
    // BAJAS_...
    borrarRegistroTBC(dctCaso, clLista.getLogin(), tsFechaActual, bBloqueo);

    // Este proceso ha de ser posterior, forzosamente, al borrado del
    // registro de tuberculosis
    // Borrar f�sicamente EDOIND
    borrarCasoEDOIND(dctCaso, vNotificadores, clLista.getLogin(), tsFechaActual,
                     bBloqueo);

    // Borrar SIVE_ENFERMO
    borrarEnfermo(dctCaso, clLista.getLogin(), tsFechaActual, bBloqueo);

    return clLista;
  } // Fin clDeleteDatos

  // Inserci�n en tablas de bajas
  private void insertarBajasRegistros(DatCasosTub dct) throws Exception {

    /*
        String sQuery = null;
        PreparedStatement st = null;
        // Se pasa la informaci�n a la tabla
        // SIVE_BAJASEDOIND
        sQuery = " insert into SIVE_BAJASEDOIND " +
                 " select " +
         " NM_EDO, CD_ANOEPI, CD_NIVEL_1, CD_NIVEL_2, CD_SEMEPI, CD_ANOURG, " +
         " NM_ENVIOURGSEM, CD_CLASIFDIAG, IT_PENVURG, CD_ENFERMO, CD_PROV, " +
         " CD_MUN, CD_ZBS, FC_RECEP, DS_CALLE, DS_NMCALLE, DS_PISO, CD_POSTAL, " +
         " CD_ANOOTRC, NM_ENVOTRC, CD_ENFCIE, FC_FECNOTIF, IT_DERIVADO, " +
         " DS_CENTRODER, IT_DIAGCLI, IT_DIAGMICRO, IT_DIAGSERO, DS_DIAGOTROS, " +
                 " FC_INISNT, DS_COLECTIVO, IT_ASOCIADO, DS_ASOCIADO, CDVIAL, CDTVIA, CDTNUM, DSCALNUM, CD_OPE, FC_ULTACT " +
                 " from SIVE_EDOIND " +
                 " where NM_EDO = ? ";
        st = con.prepareStatement(sQuery);
        st.setInt(1, dct.getNM_EDO_Integer().intValue() );
        st.executeUpdate();
     */

    // modificacion 25/07/2000
    // en vez de utilizar directamente el select para el insert,
    // primero hago el select metiendo el resultado en un data,
    // y luego insertamos

    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    DatCasosTub dctaux = null;

    sQuery = " select " +
        " NM_EDO, CD_ANOEPI, CD_NIVEL_1, CD_NIVEL_2, CD_SEMEPI, CD_ANOURG, " +
        " NM_ENVIOURGSEM, CD_CLASIFDIAG, IT_PENVURG, CD_ENFERMO, CD_PROV, " +
        " CD_MUN, CD_ZBS, FC_RECEP, DS_CALLE, DS_NMCALLE, DS_PISO, CD_POSTAL, " +
        " CD_ANOOTRC, NM_ENVOTRC, CD_ENFCIE, FC_FECNOTIF, IT_DERIVADO, " +
        " DS_CENTRODER, IT_DIAGCLI, IT_DIAGMICRO, IT_DIAGSERO, DS_DIAGOTROS, " +
        " FC_INISNT, DS_COLECTIVO, IT_ASOCIADO, DS_ASOCIADO, CDVIAL, CDTVIA, CDTNUM, DSCALNUM, CD_OPE, FC_ULTACT " +
        " from SIVE_EDOIND " +
        " where NM_EDO = ? ";

    st = con.prepareStatement(sQuery);

    st.setInt(1, dct.getNM_EDO_Integer().intValue());

    rs = st.executeQuery();

    while (rs.next()) {
      dctaux = new DatCasosTub();
      dctaux.setNM_EDO(new Integer(rs.getInt("NM_EDO")).toString());
      dctaux.setCD_ANOEPI(rs.getString("CD_ANOEPI"));
      dctaux.setCD_NIVEL_1_EDOIND(rs.getString("CD_NIVEL_1"));
      dctaux.setCD_NIVEL_2_EDOIND(rs.getString("CD_NIVEL_2"));
      dctaux.setCD_SEMEPI(rs.getString("CD_SEMEPI"));
      dctaux.setCD_ANOURG(rs.getString("CD_ANOURG"));
      dctaux.setNM_ENVIOURGSEM(new Integer(rs.getInt("NM_ENVIOURGSEM")).
                               toString());
      dctaux.setCD_CLASIFDIAG(rs.getString("CD_CLASIFDIAG"));
      dctaux.setIT_PENVURG(rs.getString("IT_PENVURG"));
      dctaux.setCD_ENFERMO_EDOIND(rs.getString("CD_ENFERMO"));
      dctaux.setCD_PROV_EDOIND(rs.getString("CD_PROV"));
      dctaux.setCD_MUN_EDOIND(rs.getString("CD_MUN"));
      dctaux.setCD_ZBS_EDOIND(rs.getString("CD_ZBS"));
      String sferecp = (String) Fechas.date2String(rs.getDate("FC_RECEP"));
      dctaux.setFC_RECEP(sferecp);
      dctaux.setDS_CALLE(rs.getString("DS_CALLE"));
      dctaux.setDS_NMCALLE(rs.getString("DS_NMCALLE"));
      dctaux.setDS_PISO_EDOIND(rs.getString("DS_PISO"));
      dctaux.setCD_POSTAL_EDOIND(rs.getString("CD_POSTAL"));
      dctaux.setCD_ANOOTRC(rs.getString("CD_ANOOTRC"));
      dctaux.setNM_ENVOTRC(new Integer(rs.getInt("NM_ENVOTRC")).toString());
      dctaux.setCD_ENFCIE(rs.getString("CD_ENFCIE"));
      String sfenot = (String) Fechas.date2String(rs.getDate("FC_FECNOTIF"));
      dctaux.setFC_FECNOTIF(sfenot);
      dctaux.setIT_DERIVADO(rs.getString("IT_DERIVADO"));
      dctaux.setDS_CENTRODER(rs.getString("DS_CENTRODER"));
      dctaux.setIT_DIAGCLI(rs.getString("IT_DIAGCLI"));
      dctaux.setIT_DIAGMICRO(rs.getString("IT_DIAGMICRO"));
      dctaux.setIT_DIAGSERO(rs.getString("IT_DIAGSERO"));
      dctaux.setDS_DIAGOTROS(rs.getString("DS_DIAGOTROS"));
      String sfinis = (String) Fechas.date2String(rs.getDate("FC_INISNT"));
      dctaux.setFC_INISNT(sfinis);
      dctaux.setDS_COLECTIVO(rs.getString("DS_COLECTIVO"));
      dctaux.setIT_ASOCIADO(rs.getString("IT_ASOCIADO"));
      dctaux.setDS_ASOCIADO(rs.getString("DS_ASOCIADO"));
      dctaux.setCDVIAL_EDOIND(rs.getString("CDVIAL"));
      dctaux.setCDTVIA_EDOIND(rs.getString("CDTVIA"));
      dctaux.setCDTNUM_EDOIND(rs.getString("CDTNUM"));
      dctaux.setDSCALNUM_EDOIND(rs.getString("DSCALNUM"));
      dctaux.setCD_OPE_EDOIND(rs.getString("CD_OPE"));
      String sfultact = (String) Fechas.timestamp2String(rs.getTimestamp(
          "FC_ULTACT"));
      dctaux.setFC_ULTACT_EDOIND(sfultact);

    }

    rs.close();
    rs = null;
    st.close();
    st = null;

    sQuery = " insert into SIVE_BAJASEDOIND " +
        " (NM_EDO, CD_ANOEPI, CD_SEMEPI, CD_ANOURG, " +
        " NM_ENVIOURGSEM, CD_CLASIFDIAG, IT_PENVURG, CD_PROV, " +
        " CD_MUN, CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, FC_RECEP, DS_CALLE, DS_NMCALLE, DS_PISO, CD_POSTAL, " +
        " CD_OPE, FC_ULTACT, CD_ANOOTRC, NM_ENVOTRC, CD_ENFCIE, FC_FECNOTIF, IT_DERIVADO, " +
        " DS_CENTRODER, IT_DIAGCLI, IT_DIAGMICRO, IT_DIAGSERO, DS_DIAGOTROS, " +
        " FC_INISNT, DS_COLECTIVO, IT_ASOCIADO, DS_ASOCIADO, CDVIAL, CDTVIA, CDTNUM, DSCALNUM, CD_ENFERMO) " +
        " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, " +
        " ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";

    st = con.prepareStatement(sQuery);

    st.setInt(1, new Integer(dctaux.getNM_EDO()).intValue());
    st.setString(2, dctaux.getCD_ANOEPI());
    st.setString(3, dctaux.getCD_SEMEPI());
    st.setString(4, dctaux.getCD_ANOURG());

    if (dct.getNM_ENVIOURGSEM() != null) {
      st.setInt(5, new Integer(dctaux.getNM_ENVIOURGSEM()).intValue());
    }
    else {
      st.setNull(5, java.sql.Types.INTEGER);
    }

    st.setString(6, dctaux.getCD_CLASIFDIAG());
    st.setString(7, dctaux.getIT_PENVURG());

    st.setString(8, dctaux.getCD_PROV_EDOIND());
    // modificacion
    //String smun = (String) dct.getCD_MUN_EDOIND();
    st.setString(9, dctaux.getCD_MUN_EDOIND());

    st.setString(10, dctaux.getCD_NIVEL_1_EDOIND());
    st.setString(11, dctaux.getCD_NIVEL_2_EDOIND());

    st.setString(12, dctaux.getCD_ZBS_EDOIND());

    st.setDate(13,
               new java.sql.Date(Fechas.string2Date(dctaux.getFC_RECEP()).getTime()));

    st.setString(14, dctaux.getDS_CALLE());
    st.setString(15, dctaux.getDS_NMCALLE());
    st.setString(16, dctaux.getDS_PISO_EDOIND());
    st.setString(17, dctaux.getCD_POSTAL_EDOIND());

    st.setString(18, dctaux.getCD_OPE_EDOIND());
    st.setTimestamp(19, Fechas.string2Timestamp(dctaux.getFC_ULTACT_EDOIND()));

    st.setString(20, dctaux.getCD_ANOOTRC());

    if (dctaux.getNM_ENVOTRC() != null) {
      st.setInt(21, new Integer(dctaux.getNM_ENVOTRC()).intValue());
    }
    else {
      st.setNull(21, java.sql.Types.INTEGER);
    }

    st.setString(22, dctaux.getCD_ENFCIE());

    st.setDate(23,
               new java.sql.Date(Fechas.string2Date(dctaux.getFC_FECNOTIF()).getTime()));

    st.setString(24, dctaux.getIT_DERIVADO());
    st.setString(25, dctaux.getDS_CENTRODER());
    st.setString(26, dctaux.getIT_DIAGCLI());
    st.setString(27, dctaux.getIT_DIAGMICRO());
    st.setString(28, dctaux.getIT_DIAGSERO());
    st.setString(29, dctaux.getDS_DIAGOTROS());

    if (dct.getFC_INISNT() != null && !dct.getFC_INISNT().equals("")) {
      st.setDate(30,
                 new java.sql.Date(Fechas.string2Date(dctaux.getFC_INISNT()).getTime()));
    }
    else {
      st.setNull(30, java.sql.Types.DATE);
    }

    st.setString(31, dctaux.getDS_COLECTIVO());
    st.setString(32, dctaux.getIT_ASOCIADO());
    st.setString(33, dctaux.getDS_ASOCIADO());
    st.setString(34, dctaux.getCDVIAL_EDOIND());
    st.setString(35, dctaux.getCDTVIA_EDOIND());
    st.setString(36, dctaux.getCDTNUM_EDOIND());
    st.setString(37, dctaux.getDSCALNUM_EDOIND());

    st.setInt(38, new Integer(dctaux.getCD_ENFERMO_EDOIND()).intValue());

    st.executeUpdate();

    st = null;
    //sQuery = null;

    //////////////////////////////////////
    // Movimiento a SIVE_BREGISTROTBC
    sQuery = " insert into SIVE_BREGISTROTBC " +
        " select " +
        " CD_ARTBC, CD_NRTBC, CD_NIVEL_1_GE, FC_INIRTBC, CD_NIVEL_2_GE, " +
        " NM_EDO, FC_SALRTBC, DS_OBSERV, CD_MOTSALRTBC, CD_E_NOTIF, DS_MEDICO, CD_OPE, FC_ULTACT " +
        " from SIVE_REGISTROTBC " +
        " where CD_ARTBC = ? and CD_NRTBC = ? ";

    st = con.prepareStatement(sQuery);

    st.setString(1, dct.getCD_ARTBC());
    st.setString(2, dct.getCD_NRTBC());

    st.executeUpdate();

  }

  // Borra el enfermo, si no est� asociado con m�s casos, as� como los
  // movimientos asociados al mismo (SIVE_MOV_ENFERMO)
  private void borrarEnfermo(DatCasosTub dct, String sLogin,
                             java.sql.Timestamp tsFecha, boolean bBloqueo) throws
      Exception {
    String sQuery = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    boolean bHayCasos = false;

    sQuery = " select count(*) as casos from sive_edoind where CD_ENFERMO = ? ";

    st = con.prepareStatement(sQuery);

    st.setInt(1, new Integer(dct.getCD_ENFERMO()).intValue());

    rs = st.executeQuery(sQuery);

    // Nos aseguramos de que, al menos, hay dos casos antes de
    // asignar a bHayCasos el valor 'true'.
    rs.next();
    if (rs.getInt("casos") > 0) {
      bHayCasos = true; // Hay al menos dos
    }

    rs.close();
    rs = null;

    if (!bHayCasos) { // Si el caso que estamos borrando es el �nico
      // de este enfermo
      // Se borran sus movimientos
      sQuery = " delete SIVE_MOV_ENFERMO where CD_ENFERMO = ? ";
      st = con.prepareStatement(sQuery);

      st.setInt(1, new Integer(dct.getCD_ENFERMO()).intValue());
      int j = st.executeUpdate();

      // Se borra el enfermo
      sQuery = " delete SIVE_ENFERMO where CD_ENFERMO = ? ";
      st = con.prepareStatement(sQuery);

      st.setInt(1, new Integer(dct.getCD_ENFERMO()).intValue());
      try {
        st.executeUpdate();
      }
      catch (java.sql.SQLException se) {

      }

    }

  }

  // Borra el caso y los datos que dependen del mismo
  private void borrarCasoEDOIND(DatCasosTub dct, Vector v, String sLogin,
                                java.sql.Timestamp tsFecha, boolean bBloqueo) throws
      Exception {
    String sQuery = null;
    PreparedStatement st = null;

    String sQueryBloqueo = null;

    int iActualizados = 0;

    // Se borran notificadores
    for (int i = 0; i < v.size(); i++) {
      borrarNotificadores( (DatNotifTub) v.elementAt(i), bBloqueo);
    }

    // Se borran respuestas de protocolo
    sQuery = " delete SIVE_RESP_EDO where NM_EDO = ? ";

    st = con.prepareStatement(sQuery);

    st.setInt(1, dct.getNM_EDO_Integer().intValue());

    iActualizados = st.executeUpdate();

    // Borrado propiamente dicho
    sQuery = " delete SIVE_EDOIND " +
        " where NM_EDO = ? ";

    if (bBloqueo) {
      sQueryBloqueo = " and CD_OPE = ? and FC_ULTACT = ? ";
    }
    else {
      sQueryBloqueo = "";
    }

    st = con.prepareStatement(sQuery + sQueryBloqueo);

    st.setInt(1, dct.getNM_EDO_Integer().intValue());

    if (bBloqueo) {
      st.setString(2, dct.getCD_OPE_EDOIND());
      st.setTimestamp(3, Fechas.string2Timestamp(dct.getFC_ULTACT_EDOIND()));
    }

    iActualizados = st.executeUpdate();

    if (iActualizados == 0) {
      throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_EDOIND);
    }

  } // Fin borrarCasoEDOIND

  // Borra SIVE_NOTIF_EDOI  y los datos asociados
  private void borrarNotificadores(DatNotifTub dnt, boolean bBloqueo) throws
      Exception {
    PreparedStatement st = null;

    String sQuery = null;

    java.sql.Date dFC_RECEP = null;
    java.sql.Date dFC_FECNOTIF = null;
    int iNmEdo = 0;

    int iBorrados = 0;

    // Se cargan los datos 'engorrosos'
    dFC_RECEP = new java.sql.Date(Fechas.string2Date(dnt.getFC_RECEP_EDOI()).
                                  getTime());
    dFC_FECNOTIF = new java.sql.Date(Fechas.string2Date(dnt.getFC_NOTIF()).
                                     getTime());
    iNmEdo = new Integer(dnt.getNM_EDO()).intValue();

    // Se borran movimientos de respuestas de protocolo
    sQuery = " delete SIVE_MOVRESPEDO " +
        " where CD_E_NOTIF = ? and NM_EDO = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
        " and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? ";

    st = con.prepareStatement(sQuery);

    st.setString(1, dnt.getCD_E_NOTIF_EDOI());
    st.setInt(2, iNmEdo);
    st.setString(3, dnt.getCD_ANOEPI_EDOI());
    st.setString(4, dnt.getCD_SEMEPI());
    st.setDate(5, dFC_RECEP);
    st.setDate(6, dFC_FECNOTIF);
    st.setString(7, dnt.getCD_FUENTE());

    st.executeUpdate();

    sQuery = null;
    st = null;

    // Borrar Tratamientos
    sQuery = " delete SIVE_TRATAMIENTOS " +
        " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
        " and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? and NM_EDO = ? ";

    st = con.prepareStatement(sQuery);

    st.setString(1, dnt.getCD_E_NOTIF_EDOI());
    st.setString(2, dnt.getCD_ANOEPI_EDOI());
    st.setString(3, dnt.getCD_SEMEPI());
    st.setDate(4, dFC_RECEP);
    st.setDate(5, dFC_FECNOTIF);
    st.setString(6, dnt.getCD_FUENTE());
    st.setInt(7, iNmEdo);

    st.executeUpdate();

    sQuery = null;
    st = null;

    // Borrar sive_res_resistencia
    sQuery = " delete SIVE_RES_RESISTENCIA " +
        " where NM_RESLAB in " +
        " ( select NM_RESLAB from SIVE_RESLAB " +
        " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
        " and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? and NM_EDO = ? )";

    st = con.prepareStatement(sQuery);

    st.setString(1, dnt.getCD_E_NOTIF_EDOI());
    st.setString(2, dnt.getCD_ANOEPI_EDOI());
    st.setString(3, dnt.getCD_SEMEPI());
    st.setDate(4, dFC_RECEP);
    st.setDate(5, dFC_FECNOTIF);
    st.setString(6, dnt.getCD_FUENTE());
    st.setInt(7, iNmEdo);

    st.executeUpdate();

    sQuery = null;
    st = null;

    // Borrar sive_reslab
    sQuery = " delete SIVE_RESLAB " +
        " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
        " and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? and NM_EDO = ? ";

    st = con.prepareStatement(sQuery);

    st.setString(1, dnt.getCD_E_NOTIF_EDOI());
    st.setString(2, dnt.getCD_ANOEPI_EDOI());
    st.setString(3, dnt.getCD_SEMEPI());
    st.setDate(4, dFC_RECEP);
    st.setDate(5, dFC_FECNOTIF);
    st.setString(6, dnt.getCD_FUENTE());
    st.setInt(7, iNmEdo);

    st.executeUpdate();

    sQuery = null;
    st = null;

    // sive_notif_edoi propiamente dicho
    sQuery = " delete SIVE_NOTIF_EDOI " +
        " where CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
        " and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? and NM_EDO = ? ";

    if (bBloqueo) {
      sQuery += " and CD_OPE = ? and FC_ULTACT = ?";
    }

    st = con.prepareStatement(sQuery);

    // Clave
    st.setString(1, dnt.getCD_E_NOTIF_EDOI());
    st.setString(2, dnt.getCD_ANOEPI_EDOI());
    st.setString(3, dnt.getCD_SEMEPI());
    st.setDate(4, dFC_RECEP);
    st.setDate(5, dFC_FECNOTIF);
    st.setString(6, dnt.getCD_FUENTE());
    st.setInt(7, iNmEdo);

    // modificacion 07/07/2000
    String snotif = (String) dnt.getCD_E_NOTIF_EDOI();
    String sanoepi = (String) dnt.getCD_ANOEPI_EDOI();
    String ssemepi = (String) dnt.getCD_SEMEPI();
    String sfecrecep = (String) Fechas.date2String(dFC_RECEP);
    String sfecnotif = (String) Fechas.date2String(dFC_FECNOTIF);
    String sfuente = (String) dnt.getCD_FUENTE();
    String sope = (String) dnt.getCD_OPE_EDOI();
    //java.sql.Timestamp sfc = Fechas.string2Timestamp(dnt.getFC_ULTACT_EDOI());
    //trazaLog("sope " + sope);
    //trazaLog("sfc " + sfc);

    // Campos de bloqueo
    if (bBloqueo) {
      st.setString(8, dnt.getCD_OPE_EDOI());
      st.setTimestamp(9, Fechas.string2Timestamp(dnt.getFC_ULTACT_EDOI()));
    }

    try {
      iBorrados = st.executeUpdate();
    }
    catch (Exception e) {
      System.out.println("Excepcion en hazDeleteSQLNotifedo: SIVE_NOTIF_EDOI " +
                         e.getMessage());
    }
    finally {
      st.close();
      st = null;
      if (iBorrados == 0) {
        throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_NOTIF_EDOI);
      }
    }

  } // Fin borrarNotificadores

  // Borra los contactos y sus datos dependientes a partir de la
  // clave del registro de tuberculosis
  private void borrarContactosRegistroTBC(String sArtbc, String sNrtbc) throws
      Exception {
    String sQuery = null;
    String sQueryMovimientos = null;

    PreparedStatement st = null;
    PreparedStatement stMovimientos = null;

    ResultSet rs = null;

    Vector vCdEnfermo = new Vector();

    // Se recuperan los CD_ENFERMO de todos los casos asociados con ese
    // caso de tuberculosis, para intentar su borrado tras la eliminaci�n
    // del caso.
    sQuery =
        " select CD_ENFERMO from SIVE_CASOCONTAC where CD_ARTBC = ? and CD_NRTBC = ? ";

    st = con.prepareStatement(sQuery);

    st.setString(1, sArtbc);
    st.setString(2, sNrtbc);

    rs = st.executeQuery();

    while (rs.next()) {
      vCdEnfermo.addElement(rs.getString("CD_ENFERMO"));
    }

    rs = null;

    // Borrado de respuestas de protocolo de todos los contactos
    // asociados a ese caso
    sQuery = " delete SIVE_RESPCONTACTO where CD_ARTBC = ? and CD_NRTBC = ? ";

    st = con.prepareStatement(sQuery);

    st.setString(1, sArtbc);
    st.setString(2, sNrtbc);

    st.executeUpdate();

    // Borrado en s�
    sQuery = " delete SIVE_CASOCONTAC where CD_ARTBC = ? and CD_NRTBC = ? ";

    st = con.prepareStatement(sQuery);

    st.setString(1, sArtbc);
    st.setString(2, sNrtbc);

    st.executeUpdate();

    // Se intentan borrar los registros de SIVE_ENFERMO asociados a los
    // registros de SIVE_CASOCONTAC eliminados

    sQuery = " delete SIVE_ENFERMO where CD_ENFERMO = ? ";
    st = con.prepareStatement(sQuery);

    // Se borran sus movimientos
    sQueryMovimientos = " delete SIVE_MOV_ENFERMO where CD_ENFERMO = ? ";
    stMovimientos = con.prepareStatement(sQueryMovimientos);

    for (int i = 0; i < vCdEnfermo.size(); i++) {
      try {
        stMovimientos.setInt(1,
                             new Integer( (String) vCdEnfermo.elementAt(i)).intValue());
        stMovimientos.executeUpdate();

        st.setInt(1, new Integer( (String) vCdEnfermo.elementAt(i)).intValue());
        st.executeUpdate();

      }
      catch (java.sql.SQLException se) {
      }
    }

  } // fin borrarContactosRegistroTBC

  // Borra el registro de tuberculosis
  // El proceso consta de dos pasos:
  //  - Se pasa la informaci�n del registro a la tabla SIVE_BREGISTROTBC
  private void borrarRegistroTBC(DatCasosTub dct, String sLogin,
                                 java.sql.Timestamp tsFecha, boolean bBloqueo) throws
      Exception {
    String sQuery = null;
    PreparedStatement st = null;

    // Borrado de sus contactos
    borrarContactosRegistroTBC(dct.getCD_ARTBC(), dct.getCD_NRTBC());

    // Borrado en s�
    sQuery = " delete SIVE_REGISTROTBC where CD_ARTBC = ? and CD_NRTBC = ? ";

    st = con.prepareStatement(sQuery);

    st.setString(1, dct.getCD_ARTBC());
    st.setString(2, dct.getCD_NRTBC());

    st.executeUpdate();

  } // Fin borrarRegistroTBC

  // Para actualizar el caso
  //private void modificarCasoEDOIND(DatCasosTub dct, int nmEdo, int nmEnfermo, String sLogin, java.sql.Date tsFecha, boolean bBloqueo) throws Exception {
  private void modificarCasoEDOIND(DatCasosTub dct, int nmEdo, int nmEnfermo,
                                   String sLogin, java.sql.Timestamp tsFecha,
                                   boolean bBloqueo) throws Exception {
    String sQuery = null;
    PreparedStatement st = null;

    String sQueryBloqueo = null;
    int iActualizados = 0;

    sQuery = "update SIVE_EDOIND set " +
        " CD_ANOEPI = ?, CD_SEMEPI = ?, CD_ANOURG = ?, " +
        " NM_ENVIOURGSEM = ?, CD_CLASIFDIAG = ?, IT_PENVURG = ?, CD_ENFERMO = ?, " +
        " CD_PROV = ?, CD_MUN = ?, CD_NIVEL_1 = ?, CD_NIVEL_2 = ?, CD_ZBS = ?, " +
        " FC_RECEP = ?, DS_CALLE = ?, DS_NMCALLE = ?, DS_PISO = ?, CD_POSTAL = ?, " +
        " CD_ANOOTRC = ?, NM_ENVOTRC = ?, CD_ENFCIE = ?, FC_FECNOTIF = ?, IT_DERIVADO = ?, " +
        " DS_CENTRODER = ?, IT_DIAGCLI = ?, IT_DIAGMICRO = ?, IT_DIAGSERO = ?, " +
        " DS_DIAGOTROS = ?, FC_INISNT = ?, DS_COLECTIVO = ?, IT_ASOCIADO = ?, " +
        " DS_ASOCIADO = ?, CDVIAL = ?, CDTVIA = ?, CDTNUM = ?, DSCALNUM = ?, " +
        " CD_OPE = ?, FC_ULTACT = ? " +
        " where NM_EDO = ? ";

    if (bBloqueo) {
      sQueryBloqueo = " and CD_OPE = ? and FC_ULTACT = ? ";
    }
    else {
      sQueryBloqueo = "";
    }

    st = con.prepareStatement(sQuery + sQueryBloqueo);

    // Datos
    st.setString(1, dct.getCD_ANOEPI());
    st.setString(2, dct.getCD_SEMEPI());
    st.setString(3, dct.getCD_ANOURG());

    if (dct.getNM_ENVIOURGSEM() != null && !dct.getNM_ENVIOURGSEM().equals("")) {
      st.setInt(4, new Integer(dct.getNM_ENVIOURGSEM()).intValue());
    }
    else {
      st.setNull(4, java.sql.Types.INTEGER);
    }
    // st.setInt(4, dct.getNM_ENVIOURGSEM_Integer().intValue());

    st.setString(5, dct.getCD_CLASIFDIAG());
    st.setString(6, dct.getIT_PENVURG());
    st.setInt(7, nmEnfermo);
    st.setString(8, dct.getCD_PROV_EDOIND());
    st.setString(9, dct.getCD_MUN_EDOIND());
    st.setString(10, dct.getCD_NIVEL_1_EDOIND());
    st.setString(11, dct.getCD_NIVEL_2_EDOIND());
    st.setString(12, dct.getCD_ZBS_EDOIND());
    st.setDate(13,
               new java.sql.Date(Fechas.string2Date(dct.getFC_RECEP()).getTime()));
    st.setString(14, dct.getDS_CALLE());
    st.setString(15, dct.getDS_NMCALLE());
    st.setString(16, dct.getDS_PISO_EDOIND());
    st.setString(17, dct.getCD_POSTAL_EDOIND());
    st.setString(18, dct.getCD_ANOOTRC());

    // new Integer(rs.getInt("NM_EDO")).toString()
    System.out.println( (String) dct.getNM_ENVOTRC());
    //if (dct.getNM_ENVOTRC() != null  && (!dct.getNM_ENVOTRC().equals(""))) {
    //if ( !(  Integer.toString(dct.getNM_ENVOTRC())).equals(""))
    if (new Integer(dct.getNM_ENVOTRC()).toString() != null) {

      st.setInt(19, new Integer(dct.getNM_ENVOTRC()).intValue());
    }
    else {
      st.setNull(19, java.sql.Types.INTEGER);
    }

    st.setString(20, dct.getCD_ENFCIE());
    st.setDate(21,
               new java.sql.Date(Fechas.string2Date(dct.getFC_FECNOTIF()).getTime()));
    st.setString(22, dct.getIT_DERIVADO());
    st.setString(23, dct.getDS_CENTRODER());
    st.setString(24, dct.getIT_DIAGCLI());
    st.setString(25, dct.getIT_DIAGMICRO());
    st.setString(26, dct.getIT_DIAGSERO());
    st.setString(27, dct.getDS_DIAGOTROS());

    if (!dct.getFC_INISNT().equals("") && dct.getFC_INISNT() != null) {
      st.setDate(28,
                 new java.sql.Date(Fechas.string2Date(dct.getFC_INISNT()).getTime()));
    }
    else {
      st.setNull(28, java.sql.Types.DATE);
    }

    st.setString(29, dct.getDS_COLECTIVO());
    st.setString(30, dct.getIT_ASOCIADO());
    st.setString(31, dct.getDS_ASOCIADO());
    st.setString(32, dct.getCDVIAL_EDOIND());
    st.setString(33, dct.getCDTVIA_EDOIND());
    st.setString(34, dct.getCDTNUM_EDOIND());
    st.setString(35, dct.getDSCALNUM_EDOIND());
    st.setString(36, sLogin);
    st.setTimestamp(37, tsFecha);
    // Clave
    st.setInt(38, nmEdo);

    // Bloqueo
    if (bBloqueo) {
      st.setString(39, dct.getCD_OPE_EDOIND());
      st.setTimestamp(40, Fechas.string2Timestamp(dct.getFC_ULTACT_EDOIND()));
    }

    iActualizados = st.executeUpdate();

    if (iActualizados == 0) {
      throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_EDOIND);
    }
    else {
      // Datos para devolver al cliente
      dct.setCD_OPE_EDOIND(sLogin);
      dct.setFC_ULTACT_EDOIND(Fechas.timestamp2String(tsFecha));
    }

    sQuery = null;
    st = null;

  } // Fin modificarCasoEDOIND

  // Actualiza los datos del enfermo
  private void modificarEnfermo(DatCasosTub dct, int nmEnfermo, String sLogin,
                                java.sql.Timestamp tsFecha, boolean bBloqueo) throws
      Exception {
    //private void modificarEnfermo(DatCasosTub dct, int nmEnfermo, String sLogin, java.sql.Date tsFecha, boolean bBloqueo) throws Exception {
    String sQuery = null;

    // Objetos necesarios para el uso del secuenciador general
    ResultSet rs = null;
    PreparedStatement st = null;

    // Secuenciador actual
    int iNM_MOVENF = sapp.Funciones.SecGeneral(con, st, rs,
                                               constantes.COLUMNA_MOVENF);

    String sQueryBloqueo = null;

    // Movimiento a SIVE_MOV_ENFERMO
    sQuery = " insert into SIVE_MOV_ENFERMO " +
        " select " + String.valueOf(iNM_MOVENF) +
        " NM_MOVENF, CD_ENFERMO, CD_PAIS, CD_TDOC, CD_PROV, CD_MUN, DS_APE1, DS_APE2, " +
        " SIGLAS, DS_NOMBRE, " +
        " DS_FONOAPE1, DS_FONOAPE2, DS_FONONOMBRE, IT_CALC, FC_NAC, " +
        " CD_SEXO, CD_POSTAL, DS_DIREC, DS_NUM, DS_PISO, DS_TELEF, " +
        " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, CD_OPE, FC_ULTACT, " +
        " IT_REVISADO, CD_MOTBAJA, FC_BAJA, DS_OBSERV, CD_PROV2, " +
        " CD_MUNI2, CD_POST2, DS_DIREC2, DS_NUM2, DS_PISO2, DS_OBSERV2, " +
        " CD_PROV3, CD_MUNI3, CD_POST3, DS_DIREC3, DS_NUM3, " +
        " DS_PISO3, DS_TELEF3, DS_OBSERV3, DS_NDOC, DS_TELEF2, " +
        " CDVIAL, CDTVIA, CDTNUM, " +
        " IT_ENFERMO, DSCALNUM, IT_CONTACTO " +
        " from SIVE_ENFERMO " +
        " where CD_ENFERMO = ? ";

    st = con.prepareStatement(sQuery);

    st.setInt(1, new Integer(dct.getCD_ENFERMO()).intValue());

    st.executeUpdate();

    // Actualizaci�n propiamente dicha
    int iActualizados = 0;

    sQuery = " update SIVE_ENFERMO set " +
        " CD_TDOC = ?, DS_APE1 = ?, DS_APE2 = ?, SIGLAS = ?, DS_NOMBRE = ?, " +
        " DS_FONOAPE1 = ?, DS_FONOAPE2 = ?, DS_FONONOMBRE = ?, IT_CALC = ?, FC_NAC = ?, " +
        " CD_SEXO = ?, CD_POSTAL = ?, DS_DIREC = ?, DS_NUM = ?, DS_PISO = ?, DS_TELEF = ?, " +
        " CD_NIVEL_1 = ?, CD_NIVEL_2 = ?, CD_ZBS = ?, CD_OPE = ?, FC_ULTACT = ?, " +
        " DS_OBSERV = ?, CD_PROV2 = ?, " +
        " CD_MUNI2 = ?, CD_POST2 = ?, DS_DIREC2 = ?, DS_NUM2 = ?, DS_PISO2 = ?, DS_OBSERV2 = ?, " +
        " CD_PROV3 = ?, CD_MUNI3 = ?, CD_POST3 = ?, DS_DIREC3 = ?, DS_NUM3 = ?, " +
        " DS_PISO3 = ?, DS_TELEF3 = ?, DS_OBSERV3 = ?, DS_NDOC = ?, DS_TELEF2 = ?, " +
        " CDVIAL = ?, CDTVIA = ?, CDTNUM = ?, DSCALNUM = ?, CD_PROV = ?, CD_MUN = ?, " +
        " IT_ENFERMO = ?, CD_PAIS = ? " + // 47
        " where CD_ENFERMO = ? ";

    if (bBloqueo) {
      sQueryBloqueo = " and CD_OPE = ? and FC_ULTACT = ? ";
    }
    else {
      sQueryBloqueo = "";
    }

    st = con.prepareStatement(sQuery + sQueryBloqueo);

    // Datos
    st.setString(1, dct.getCD_TDOC());
    st.setString(2, dct.getDS_APE1());
    st.setString(3, dct.getDS_APE2());
    st.setString(4, dct.getSIGLAS());
    st.setString(5, dct.getDS_NOMBRE());
    st.setString(6, dct.getDS_FONOAPE1());
    st.setString(7, dct.getDS_FONOAPE2());
    st.setString(8, dct.getDS_FONONOMBRE());
    st.setString(9, dct.getIT_CALC());

    if (dct.getFC_NAC() != null && !dct.getFC_NAC().equals("")) {
      st.setDate(10,
                 new java.sql.Date(Fechas.string2Date(dct.getFC_NAC()).getTime()));
    }
    else {
      st.setNull(10, java.sql.Types.DATE);
    }

    st.setString(11, dct.getCD_SEXO());
    st.setString(12, dct.getCD_POSTAL());
    st.setString(13, dct.getDS_DIREC());
    st.setString(14, dct.getDS_NUM());
    st.setString(15, dct.getDS_PISO());
    st.setString(16, dct.getDS_TELEF());
    st.setString(17, dct.getCD_NIVEL_1());
    st.setString(18, dct.getCD_NIVEL_2());
    st.setString(19, dct.getCD_ZBS());
    st.setString(20, sLogin);
    st.setTimestamp(21, tsFecha);

    //st.setString(22, dct.getIT_REVISADO());

    st.setString(22, dct.getDS_OBSERV());
    st.setString(23, dct.getCD_PROV2());
    st.setString(24, dct.getCD_MUNI2());
    st.setString(25, dct.getCD_POST2());
    st.setString(26, dct.getDS_DIREC2());
    st.setString(27, dct.getDS_NUM2());
    st.setString(28, dct.getDS_PISO2());
    st.setString(29, dct.getDS_OBSERV2());
    st.setString(30, dct.getCD_PROV3());
    st.setString(31, dct.getCD_MUNI3());
    st.setString(32, dct.getCD_POST3());

    st.setString(33, dct.getDS_DIREC3());
    st.setString(34, dct.getDS_NUM3());
    st.setString(35, dct.getDS_PISO3());

    st.setString(36, dct.getDS_TELEF3());
    st.setString(37, dct.getDS_OBSERV3());
    st.setString(38, dct.getDS_NDOC());

    st.setString(39, dct.getDS_TELEF2());

    st.setString(40, dct.getCDVIAL());
    st.setString(41, dct.getCDTVIA());
    st.setString(42, dct.getCDTNUM());
    st.setString(43, dct.getDSCALNUM());
    st.setString(44, dct.getCD_PROV());
    st.setString(45, dct.getCD_MUN());
    st.setString(46, dct.getIT_ENFERMO());
    st.setString(47, dct.getCD_PAIS());

    //st.setString(48, dct.getIT_CONTACTO());

    // Clave
    st.setInt(48, nmEnfermo);

    // Bloqueo
    if (bBloqueo) {
      st.setString(49, dct.getCD_OPE());
      st.setTimestamp(50, Fechas.string2Timestamp(dct.getFC_ULTACT()));
    }

    iActualizados = st.executeUpdate();

    if (iActualizados == 0) {
      throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_ENFERMO);
    }
    else {
      dct.setCD_OPE(sLogin);
      dct.setFC_ULTACT(Fechas.timestamp2String(tsFecha));
    }

    sQuery = null;
    st = null;

  } // Fin modificarEnfermo

  // Inserta los datos del enfermo
  private void insertarEnfermo(DatCasosTub dct, int nmEnfermo, String sLogin,
                               java.sql.Timestamp tsFecha) throws Exception {
    String sQuery = null;
    PreparedStatement st = null;

    sQuery = " insert into SIVE_ENFERMO (" +
        " CD_ENFERMO, CD_TDOC, DS_APE1, DS_APE2, SIGLAS, DS_NOMBRE, " +
        " DS_FONOAPE1, DS_FONOAPE2, DS_FONONOMBRE, IT_CALC, FC_NAC, " +
        " CD_SEXO, CD_POSTAL, DS_DIREC, DS_NUM, DS_PISO, DS_TELEF, " +
        " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, CD_OPE, FC_ULTACT, " +
        " IT_REVISADO, DS_OBSERV, CD_PROV2, " +
        " CD_MUNI2, CD_POST2, DS_DIREC2, DS_NUM2, DS_PISO2, DS_OBSERV2, " +
        " CD_PROV3, CD_MUNI3, CD_POST3, DS_DIREC3, DS_NUM3, " +
        " DS_PISO3, DS_TELEF3, DS_OBSERV3, DS_NDOC, DS_TELEF2, " +
        " CDVIAL, CDTVIA, CDTNUM, DSCALNUM, CD_PROV, CD_MUN, " +
        " IT_ENFERMO, CD_PAIS, IT_CONTACTO ) " + //50
        " values " +
        " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + // 20
        "   ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + // 20
        "   ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) "; // 10

    st = con.prepareStatement(sQuery);

    st.setInt(1, nmEnfermo);
    st.setString(2, dct.getCD_TDOC());
    st.setString(3, dct.getDS_APE1());
    st.setString(4, dct.getDS_APE2());
    st.setString(5, dct.getSIGLAS());
    st.setString(6, dct.getDS_NOMBRE());
    st.setString(7, dct.getDS_FONOAPE1());
    st.setString(8, dct.getDS_FONOAPE2());
    st.setString(9, dct.getDS_FONONOMBRE());
    st.setString(10, dct.getIT_CALC());

    if (!dct.getFC_NAC().equals("")) {
      st.setDate(11,
                 new java.sql.Date(Fechas.string2Date(dct.getFC_NAC()).getTime()));
    }
    else {
      st.setNull(11, java.sql.Types.DATE);
    }

    st.setString(12, dct.getCD_SEXO());
    st.setString(13, dct.getCD_POSTAL());
    st.setString(14, dct.getDS_DIREC());
    st.setString(15, dct.getDS_NUM());
    st.setString(16, dct.getDS_PISO());
    st.setString(17, dct.getDS_TELEF());
    st.setString(18, dct.getCD_NIVEL_1());
    st.setString(19, dct.getCD_NIVEL_2());
    st.setString(20, dct.getCD_ZBS());
    st.setString(21, sLogin);
    st.setTimestamp(22, tsFecha);
    st.setString(23, dct.getIT_REVISADO());
    st.setString(24, dct.getDS_OBSERV());
    st.setString(25, dct.getCD_PROV2());
    st.setString(26, dct.getCD_MUNI2());
    st.setString(27, dct.getCD_POST2());
    st.setString(28, dct.getDS_DIREC2());
    st.setString(29, dct.getDS_NUM2());
    st.setString(30, dct.getDS_PISO2());
    st.setString(31, dct.getDS_OBSERV2());
    st.setString(32, dct.getCD_PROV3());
    st.setString(33, dct.getCD_MUNI3());
    st.setString(34, dct.getCD_POST3());

    st.setString(35, dct.getDS_DIREC3());
    st.setString(36, dct.getDS_NUM3());
    st.setString(37, dct.getDS_PISO3());

    st.setString(38, dct.getDS_TELEF3());
    st.setString(39, dct.getDS_OBSERV3());
    st.setString(40, dct.getDS_NDOC());

    st.setString(41, dct.getDS_TELEF2());

    st.setString(42, dct.getCDVIAL());
    st.setString(43, dct.getCDTVIA());
    st.setString(44, dct.getCDTNUM());
    st.setString(45, dct.getDSCALNUM());
    st.setString(46, dct.getCD_PROV());
    st.setString(47, dct.getCD_MUN());
    st.setString(48, dct.getIT_ENFERMO());
    st.setString(49, dct.getCD_PAIS());
    st.setString(50, dct.getIT_CONTACTO());

    st.executeUpdate();

    // Datos para devolver al cliente
    dct.setCD_ENFERMO(String.valueOf(nmEnfermo));
    dct.setCD_OPE(sLogin);
    dct.setFC_ULTACT(Fechas.timestamp2String(tsFecha));

    st = null;
    sQuery = null;
  } // Fin insertarEnfermo

  private void insertarCasoEDOIND(DatCasosTub dct, int nmEdo, int nmEnfermo,
                                  String sLogin, java.sql.Timestamp tsFecha) throws
      Exception {

    String sQuery = null;
    PreparedStatement st = null;

    sQuery = "insert into SIVE_EDOIND (" +
        " NM_EDO, CD_ANOEPI, CD_SEMEPI, CD_ANOURG, " +
        " NM_ENVIOURGSEM, CD_CLASIFDIAG, IT_PENVURG, CD_ENFERMO, " +
        " CD_PROV, CD_MUN, CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, " +
        " FC_RECEP, DS_CALLE, DS_NMCALLE, DS_PISO, CD_POSTAL, " +
        " CD_ANOOTRC, NM_ENVOTRC, CD_ENFCIE, FC_FECNOTIF, IT_DERIVADO, " +
        " DS_CENTRODER, IT_DIAGCLI, IT_DIAGMICRO, IT_DIAGSERO, " +
        " DS_DIAGOTROS, FC_INISNT, DS_COLECTIVO, IT_ASOCIADO, " +
        " DS_ASOCIADO, CDVIAL, CDTVIA, CDTNUM, DSCALNUM, " +
        " CD_OPE, FC_ULTACT )" +
        " values " +
        " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
        "   ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";

    st = con.prepareStatement(sQuery);

    st.setInt(1, nmEdo);
    st.setString(2, dct.getCD_ANOEPI());
    st.setString(3, dct.getCD_SEMEPI());
    st.setString(4, dct.getCD_ANOURG());

    if (dct.getNM_ENVIOURGSEM() != null) {
      st.setInt(5, new Integer(dct.getNM_ENVIOURGSEM()).intValue());
    }
    else {
      st.setNull(5, java.sql.Types.INTEGER);
    }

    st.setString(6, dct.getCD_CLASIFDIAG());
    st.setString(7, dct.getIT_PENVURG());
    st.setInt(8, nmEnfermo);
    st.setString(9, dct.getCD_PROV_EDOIND());
    // modificacion
    String smun = (String) dct.getCD_MUN_EDOIND();
    st.setString(10, dct.getCD_MUN_EDOIND());
    st.setString(11, dct.getCD_NIVEL_1_EDOIND());
    st.setString(12, dct.getCD_NIVEL_2_EDOIND());
    st.setString(13, dct.getCD_ZBS_EDOIND());

    st.setDate(14,
               new java.sql.Date(Fechas.string2Date(dct.getFC_RECEP()).getTime()));

    st.setString(15, dct.getDS_CALLE());
    st.setString(16, dct.getDS_NMCALLE());
    st.setString(17, dct.getDS_PISO_EDOIND());
    st.setString(18, dct.getCD_POSTAL_EDOIND());
    st.setString(19, dct.getCD_ANOOTRC());

    if (dct.getNM_ENVOTRC() != null) {
      st.setInt(20, new Integer(dct.getNM_ENVOTRC()).intValue());
    }
    else {
      st.setNull(20, java.sql.Types.INTEGER);
    }

    st.setString(21, dct.getCD_ENFCIE());

    st.setDate(22,
               new java.sql.Date(Fechas.string2Date(dct.getFC_FECNOTIF()).getTime()));

    st.setString(23, dct.getIT_DERIVADO());
    st.setString(24, dct.getDS_CENTRODER());
    st.setString(25, dct.getIT_DIAGCLI());
    st.setString(26, dct.getIT_DIAGMICRO());
    st.setString(27, dct.getIT_DIAGSERO());
    st.setString(28, dct.getDS_DIAGOTROS());

    if (dct.getFC_INISNT() != null && !dct.getFC_INISNT().equals("")) {
      st.setDate(29,
                 new java.sql.Date(Fechas.string2Date(dct.getFC_INISNT()).getTime()));
    }
    else {
      st.setNull(29, java.sql.Types.DATE);
    }

    st.setString(30, dct.getDS_COLECTIVO());
    st.setString(31, dct.getIT_ASOCIADO());
    st.setString(32, dct.getDS_ASOCIADO());
    st.setString(33, dct.getCDVIAL_EDOIND());
    st.setString(34, dct.getCDTVIA_EDOIND());
    st.setString(35, dct.getCDTNUM_EDOIND());
    st.setString(36, dct.getDSCALNUM_EDOIND());
    st.setString(37, sLogin);
    st.setTimestamp(38, tsFecha);

    st.executeUpdate();

    // Datos para devolver al cliente
    dct.setNM_EDO(String.valueOf(nmEdo));
    dct.setCD_OPE_EDOIND(sLogin);
    dct.setFC_ULTACT_EDOIND(Fechas.timestamp2String(tsFecha));

    st = null;
    sQuery = null;
  } // Fin insertarCasoEDOIND

  private void insertarRegistroTBC(DatCasosTub dct, int nmEdo, int nmRt,
                                   String sLogin, java.sql.Timestamp tsFecha) throws
      Exception {
    String sQuery = null;
    PreparedStatement st = null;

    sQuery = " insert into SIVE_REGISTROTBC (" +
        " CD_ARTBC, CD_NRTBC, FC_INIRTBC, NM_EDO, " +
        " FC_SALRTBC, DS_OBSERV, CD_MOTSALRTBC, CD_E_NOTIF, " +
        " DS_MEDICO, CD_OPE, FC_ULTACT, CD_NIVEL_1_GE, CD_NIVEL_2_GE ) " +
        " values " +
        " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";

    st = con.prepareStatement(sQuery);

    st.setString(1, dct.getCD_ARTBC());
    st.setString(2, String.valueOf(nmRt));
    st.setDate(3,
               new java.sql.Date(Fechas.string2Date(dct.getFC_INIRTBC()).getTime()));
    st.setInt(4, nmEdo);
    st.setNull(5, java.sql.Types.DATE);
    st.setString(6, dct.getDS_OBSERV_TBC());
    st.setString(7, dct.getCD_MOTSALRTBC());
    st.setString(8, dct.getCD_E_NOTIF_TBC());
    st.setString(9, dct.getDS_MEDICO());
    st.setString(10, sLogin);
    st.setTimestamp(11, tsFecha);
    st.setString(12, dct.getCD_NIVEL_1_TBC());
    st.setString(13, dct.getCD_NIVEL_2_TBC());

    st.executeUpdate();

    // Datos para devolver al cliente
    // ...
    dct.setNM_EDO_TBC(String.valueOf(nmEdo));
    dct.setCD_NRTBC(String.valueOf(nmRt));

    dct.setCD_OPE_TBC(sLogin);
    dct.setFC_ULTACT_TBC(Fechas.timestamp2String(tsFecha));

    st = null;
    sQuery = null;

  } // Fin insertarRegistroTBC

  private void insertarNotificadores(Vector v, int nmEdo, String login,
                                     java.sql.Timestamp tsFecha) throws
      Exception {
    DatNotifTub dnt = null;

    PreparedStatement st = null;
    String sQuery = null;

    sQuery = " insert into SIVE_NOTIF_EDOI (CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, " +
        " NM_EDO, FC_FECNOTIF, FC_RECEP, CD_FUENTE, DS_HISTCLI, DS_DECLARANTE, IT_PRIMERO, CD_OPE, FC_ULTACT) " +
        " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

    for (int i = 0; i < v.size(); i++) {
      dnt = (DatNotifTub) v.elementAt(i);

      st = con.prepareStatement(sQuery);

      st.setString(1, dnt.getCD_E_NOTIF_EDOI());
      st.setString(2, dnt.getCD_ANOEPI_EDOI());
      st.setString(3, dnt.getCD_SEMEPI());

      st.setInt(4, nmEdo);

      st.setDate(5,
                 new java.sql.Date(Fechas.string2Date(dnt.getFC_NOTIF()).getTime()));
      st.setDate(6,
                 new java.sql.Date(Fechas.string2Date(dnt.getFC_RECEP_EDOI()).getTime()));

      st.setString(7, dnt.getCD_FUENTE());
      st.setString(8, dnt.getDS_HISTCLI());
      st.setString(9, dnt.getDS_DECLARANTE());
      st.setString(10, dnt.getIT_PRIMERO());

      st.setString(11, login);
      st.setTimestamp(12, tsFecha);

      st.executeUpdate();

      dnt.setNM_EDO_EDOI(String.valueOf(nmEdo));
      dnt.setCD_OPE_EDOI(login);
      dnt.setFC_ULTACT_EDOI(Fechas.timestamp2String(tsFecha));

      // Insertar tratamientos en SIVE_TRATAMIENTOS
      // insertarTratamientos(dnt, nmEdo, nmTratrtbc, login, tsFecha);
    }

    st = null;
    sQuery = null;
  } // Fin insertarNotificadores

  /*
     private void insertarTratamientos(DatNotifTub dnt, int nmEdo, int nmTratrtbc, String login, java.sql.Timestamp tsFecha)
               throws Exception {
    Hashtable hTratamiento = null;
    String sQuery = null;
    PreparedStatement st = null;
    Vector vTratamientos = null;
    vTratamientos = dnt.getTRATAMIENTOS();
    sQuery = " insert into SIVE_TRATAMIENTOS (NM_TRATRTBC, CD_E_NOTIF, CD_ANOEPI, " +
       " CD_SEMEPI, FC_RECEP, FC_FECNOTIF, NM_EDO, CD_FUENTE, FC_INITRAT, " +
             " CD_MOTRATINI, FC_FINTRAT, CD_MOTRATFIN, DS_OBSERV " +
             " values " +
             " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";
    for (int i = 0; i < vTratamientos.size(); i++) {
      st = con.prepareStatement(sQuery);
      hTratamiento = (Hashtable) vTratamientos.elementAt(i);
      st.setInt(1, nmTratrtbc);
      st.setString(2, (String) hTratamiento.get("CD_E_NOTIF_TRA"));
      st.setString(3, (String) hTratamiento.get("CD_ANOEPI_TRA"));
      st.setString(4, (String) hTratamiento.get("CD_SEMEPI_TRA"));
      st.setDate(5, new java.sql.Date( Fechas.string2Date((String) hTratamiento.get("FC_RECEP_TRA")).getTime() ));
      st.setDate(6, new java.sql.Date( Fechas.string2Date((String) hTratamiento.get("FC_FECNOTIF_TRA")).getTime() ));
      st.setInt(7, nmEdo);
      st.setString(8, (String) hTratamiento.get("CD_FUENTE_TRA"));
      st.setDate(9, new java.sql.Date( Fechas.string2Date((String) hTratamiento.get("FC_INITRAT")).getTime() ));
      st.setString(10, (String) hTratamiento.get("CD_MOTRATINI"));
      st.setDate(11, new java.sql.Date( Fechas.string2Date((String) hTratamiento.get("FC_FINTRAT")).getTime() ));
      st.setString(12, (String) hTratamiento.get("CD_MOTRATFIN"));
      st.setString(13, (String) hTratamiento.get("DS_OBSERV"));
    }
    sQuery = null;
    st = null;
     } // Fin insertarTratamientos
   */

  // Para insertar datos del caso, enfermo, notificadores y tratamientos
  private CLista clInsertDatos(CLista clLista, boolean bBloqueo) throws
      Exception {
    // Fecha actual
    java.sql.Timestamp tsFechaActual = new java.sql.Timestamp( (new java.util.
        Date()).getTime());

    // Objetos para almacenar los datos recibidos
    DatCasosTub dctCaso = null;
    Vector vNotificadores = null;

    // Objetos necesarios para el uso del secuenciador general
    ResultSet rs = null;
    PreparedStatement st = null;

    // Secuenciadores actuales
    int iNM_EDO = sapp.Funciones.SecGeneral(con, st, rs, constantes.COLUMNA_EDO);
    //int iNM_MOVENF = sapp.Funciones.SecGeneral(con, st, rs, constantes.COLUMNA_MOVENF);
//    int iNM_TRATRTBC = sapp.Funciones.SecGeneral(con, st, rs, constantes.COLUMNA_TRATRTBC);

    int iNM_ENFERMO = 0;

    dctCaso = (DatCasosTub) clLista.firstElement();
    vNotificadores = (Vector) clLista.elementAt(1);

    int iNM_NRTBC = sapp.Funciones.SecAnual(con,
                                            st,
                                            rs,
                                            "CD_ANO",
                                            dctCaso.getCD_ARTBC(),
                                            "NM_NRTBC");

    if (dctCaso.getCD_ENFERMO().equals("")) { // Alta de enfermo
      iNM_ENFERMO = sapp.Funciones.SecGeneral(con, st, rs,
                                              constantes.COLUMNA_ENFERMO);
      insertarEnfermo(dctCaso, iNM_ENFERMO, clLista.getLogin(), tsFechaActual);
    }
    else { // Modificaci�n de enfermo
      iNM_ENFERMO = new Integer(dctCaso.getCD_ENFERMO()).intValue();
      // Aqu� se preguntar� si hay algo modificado
      modificarEnfermo(dctCaso, iNM_ENFERMO, clLista.getLogin(), tsFechaActual,
                       bBloqueo);
    }

    // Insertar registro en SIVE_EDOIND
    insertarCasoEDOIND(dctCaso, iNM_EDO, iNM_ENFERMO, clLista.getLogin(),
                       tsFechaActual);

    // Insertar registro en SIVE_REGISTROTBC
    insertarRegistroTBC(dctCaso, iNM_EDO, iNM_NRTBC, clLista.getLogin(),
                        tsFechaActual);

    // Insertar registroS en SIVE_NOTIF_EDOI
    insertarNotificadores(vNotificadores, iNM_EDO, clLista.getLogin(),
                          tsFechaActual);

    return clLista;
  }

  // Para obtener datos del caso, enfermo, notificadores y tratamientos
  private CLista clSelectDatos(CLista clLista) throws Exception {
    CLista clResultado = null;
    CLista clAuxiliar = null;

    Vector vNotificadores = null;
    Vector vTratamientos = null;

    DatNotifTub dntTemp = null;

    String sSQLComunidad = null;
    String sSQLMunicipio = null;

    if (iMetaModo == servletSELECT_NORMAL) {
      sSQLComunidad = sSQL_C_NORMAL;
      sSQLMunicipio = sSQL_M_NORMAL;
    }
    else {
      sSQLComunidad = sSQL_C_SUCA;
      sSQLMunicipio = sSQL_M_SUCA;
    }

//    switch (iModo) {
//      case modoMODIFICACION:
    clAuxiliar = realiza_SQL(iDatosETE, clLista);
    if (clAuxiliar != null) {
      // Datos particulares
      DatCasosTub dctDatos = new DatCasosTub( (Hashtable) clAuxiliar.
                                             firstElement());
      dctDatos.putCD_CA(sSQL_CD_CA(dctDatos, con, sSQLComunidad));
      dctDatos.putDS_MUN(sSQL_DS_MUNICIPIO(dctDatos, con, sSQLMunicipio));
      dctDatos.putDS_ZBS(sSQL_DS_ZBS(dctDatos, con, sSQL_ZBS_NORMAL));
      // Ahora se introducen los datos relativos a las descripciones
      // de �rea, distrito y zona de SIVE_EDOIND
      if (dctDatos.getCD_NIVEL_1_EDOIND() != null) {
        dctDatos.setDS_NIVEL_1_EDOIND(getDS_AREA(dctDatos.getCD_NIVEL_1_EDOIND()));
        if (dctDatos.getCD_NIVEL_2_EDOIND() != null) {
          dctDatos.setDS_NIVEL_2_EDOIND(getDS_DISTRITO(dctDatos.
              getCD_NIVEL_1_EDOIND(), dctDatos.getCD_NIVEL_2_EDOIND()));
          if (dctDatos.getCD_ZBS_EDOIND() != null) {
            dctDatos.setDS_ZBS_EDOIND(getDS_ZBS(dctDatos.getCD_NIVEL_1_EDOIND(),
                                                dctDatos.getCD_NIVEL_2_EDOIND(),
                                                dctDatos.getCD_ZBS_EDOIND()));
          }
        }
      }
      //
      clResultado = new CLista();
      clResultado.addElement(dctDatos);
      try {
        // Se buscan los notificadores del caso
        intNM_EDO = (Integer) ( (DatCasosTub) clResultado.firstElement()).get(
            "NM_EDO");
        clAuxiliar = null;
        clAuxiliar = realiza_SQL(iDatosNotificadores, clLista);
        if (clAuxiliar != null) {
          vNotificadores = new Vector();
          for (int i = 0; i < clAuxiliar.size(); i++) {
            // Se buscan los tratamientos de cada notificador
            dntTemp = new DatNotifTub( (Hashtable) clAuxiliar.elementAt(i));

            sCD_E_NOTIF = dntTemp.getCD_E_NOTIF_EDOI();
            sCD_ANOEPI = dntTemp.getCD_ANOEPI_EDOI();
            sCD_SEMEPI = dntTemp.getCD_SEMEPI();
            dFC_RECEP = new java.sql.Date(Fechas.string2Date(dntTemp.
                getFC_RECEP_EDOI()).getTime());
            dFC_FECNOTIF = new java.sql.Date(Fechas.string2Date(dntTemp.
                getFC_NOTIF()).getTime());
            sCD_FUENTE = dntTemp.getCD_FUENTE();

            //vTratamientos = realiza_SQL(iDatosTratamientos, clLista);
            dntTemp.setTRATAMIENTOS(realiza_SQL(iDatosTratamientos, clLista));
            vNotificadores.addElement(dntTemp);
          }
          clResultado.addElement(vNotificadores);
        }
      }
      catch (Exception e) {
        System.out.println(e.getMessage());
      }
    }
//        break;
//    }

    return clResultado;
  }

  //_______________________________________________________________
  /**
   *  devuelve la sentencia SQL seg�n el modo indicado
   *  @param a_i_modo  indica el modo en que est� el di�logo
   *  @param a_data es el tipo de dato que tiene los par�metros
   *  @param resultSQL es la sentencia SQL a ejecutar
   *  @return devuelve el n�mero de registros que se deben de traer
   */
  public PreparedStatement prepareSQLSentencia(int a_imodo, Object a_data,
                                               StringBuffer sbResultSQL,
                                               StringBuffer sbNumEltos,
                                               Connection cnCon) {
    PreparedStatement stValor = null;
    int iNumReg = 9999;

    try {
      switch (a_imodo) {
        case iDatosETE:
          stValor = getSQLBuscaDatos(sbResultSQL, cnCon);
          break;
        case iDatosNotificadores:
          stValor = getSQLBuscaNotificadores(sbResultSQL, cnCon);
          break;
        case iDatosTratamientos:
          stValor = getSQLBuscaTratamientos(sbResultSQL, cnCon);
      }
    }
    catch (Exception e) {

    }
    finally {

    }

    sbNumEltos.setLength(0);
    sbNumEltos.append(String.valueOf(iNumReg));

    return stValor;
  }

  /** funciones espec�ficas */

  /** Descripci�n del �rea de SIVE_EDOIND */
  private String getDS_AREA(String sCdArea) {
    String sValor = null;

    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    try {
      sQuery = " select DS_NIVEL_1 from SIVE_NIVEL1_S where CD_NIVEL_1 = ? ";
      st = con.prepareStatement(sQuery);
      st.setString(1, sCdArea);
      rs = st.executeQuery();
      while (rs.next()) {
        sValor = rs.getString("DS_NIVEL_1");
        break; // Por si acaso...
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
    finally {
    }

    return sValor;
  }

  /** Descripci�n del distrito de SIVE_EDOIND */
  private String getDS_DISTRITO(String sCdArea, String sCdDistrito) {
    String sValor = null;

    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    try {
      sQuery =
          " select DS_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_1 = ? and CD_NIVEL_2 = ? ";
      st = con.prepareStatement(sQuery);
      st.setString(1, sCdArea);
      st.setString(2, sCdDistrito);
      rs = st.executeQuery();
      while (rs.next()) {
        sValor = rs.getString("DS_NIVEL_2");
        break; // Por si acaso...
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
    finally {
    }

    return sValor;
  }

  /** Descripci�n de la ZBS de SIVE_EDOIND */
  private String getDS_ZBS(String sCdArea, String sCdDistrito, String sCdZbs) {
    String sValor = null;

    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    try {
      sQuery = " select DS_ZBS from SIVE_ZONA_BASICA where CD_NIVEL_1 = ? " +
          " and CD_NIVEL_2 = ? and CD_ZBS = ? ";
      st = con.prepareStatement(sQuery);
      st.setString(1, sCdArea);
      st.setString(2, sCdDistrito);
      st.setString(3, sCdZbs);
      rs = st.executeQuery();
      while (rs.next()) {
        sValor = rs.getString("DS_ZBS");
        break; // Por si acaso...
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
    finally {
    }

    return sValor;
  }

  /** Descripci�n de la zonificaci�n sanitaria */
  private String sSQL_DS_ZBS(DatCasosTub dctDatos, Connection cnCon,
                             String sQuery) {
    String sValor = "";

    PreparedStatement st;
    ResultSet rs;

    try {
      st = cnCon.prepareStatement(sQuery);
      st.setString(1, dctDatos.getCD_NIVEL_1_EDOIND());
      st.setString(2, dctDatos.getCD_NIVEL_2_EDOIND());
      st.setString(3, dctDatos.getCD_ZBS_EDOIND());
      rs = st.executeQuery();
      while (rs.next()) {
        sValor = rs.getString(1);
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
    finally {
    }

    return sValor;
  }

  /** C�digo de la comunidad aut�noma */
  private String sSQL_CD_CA(DatCasosTub dctDatos, Connection cnCon,
                            String sQuery) {
    String sValor = "";

    PreparedStatement st;
    ResultSet rs;

    try {
      st = cnCon.prepareStatement(sQuery);
      st.setString(1, dctDatos.getCD_PROV_EDOIND());
      rs = st.executeQuery();
      while (rs.next()) {
        sValor = rs.getString(1);
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
    finally {
    }

    return sValor;
  }

  /** Descripci�n del municipio */
  private String sSQL_DS_MUNICIPIO(DatCasosTub dctDatos, Connection cnCon,
                                   String sQuery) {
    String sValor = "";

    PreparedStatement st;
    ResultSet rs;

    try {
      st = cnCon.prepareStatement(sQuery);
      st.setString(1, dctDatos.getCD_PROV_EDOIND());
      st.setString(2, dctDatos.getCD_MUN_EDOIND());
      rs = st.executeQuery();
      while (rs.next()) {
        sValor = rs.getString(1);
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
    finally {
    }

    return sValor;
  }

  /** Datos de EDOIND */
  private PreparedStatement getSQLBuscaDatos(StringBuffer sbResult,
                                             Connection cnCon) throws Exception {
    PreparedStatement psValor = null;

    sbResult.append(" SELECT " + sSQL_TBC + ", " + sSQL_EDOIND + ", " +
                    sSQL_ENFERMO +
                    " FROM SIVE_REGISTROTBC srt, " +
                    " SIVE_EDOIND sed, SIVE_ENFERMO se " +
        " WHERE srt.NM_EDO = sed.NM_EDO AND se.CD_ENFERMO = sed.CD_ENFERMO AND " +
        " srt.CD_ARTBC = ? AND srt.CD_NRTBC = ? ");

    psValor = cnCon.prepareStatement(sbResult.toString());

    psValor.setString(1, sCD_ARTBC);
    psValor.setString(2, sCD_NRTBC);

    return psValor;
  }

  /** Datos de Notificadores */
  private PreparedStatement getSQLBuscaNotificadores(StringBuffer sbResult,
      Connection cnCon) throws Exception {
    PreparedStatement psValor = null;

    sbResult.append("SELECT " + sSQL_NOTIFEDO + ", " + sSQL_NOTIF_EDOI + ", " +
                    sSQL_E_NOTIF + ", " +
                    sSQL_C_NOTIF + ", " + sSQL_S_NOTIF +
                    " FROM SIVE_NOTIFEDO sn, SIVE_E_NOTIF en, SIVE_C_NOTIF c, " +
                    " SIVE_NOTIF_EDOI sne, SIVE_NOTIF_SEM sns " +
        " WHERE sn.CD_E_NOTIF = sne.CD_E_NOTIF AND sn.CD_ANOEPI = sne.CD_ANOEPI " +
        " AND sn.CD_SEMEPI = sne.CD_SEMEPI AND sn.FC_RECEP = sne.FC_RECEP AND sn.FC_FECNOTIF = sne.FC_FECNOTIF " +
        " AND sn.CD_E_NOTIF = en.CD_E_NOTIF " +
        " and sns.CD_E_NOTIF = sn.CD_E_NOTIF and sns.CD_ANOEPI = sn.CD_ANOEPI and sns.CD_SEMEPI = sn.CD_SEMEPI " +
        " AND en.CD_CENTRO = c.CD_CENTRO AND sne.NM_EDO = ? ");

    psValor = cnCon.prepareStatement(sbResult.toString());

    psValor.setInt(1, intNM_EDO.intValue());

    return psValor;
  }

  /** Datos de Tratamientos */
  private PreparedStatement getSQLBuscaTratamientos(StringBuffer sbResult,
      Connection cnCon) throws Exception {
    PreparedStatement psValor = null;

    sbResult.append("SELECT " + sSQL_TRATAMIENTOS +
                    " FROM SIVE_TRATAMIENTOS st " +
                    " WHERE st.CD_E_NOTIF = ? AND st.CD_ANOEPI = ? " +
        " AND st.CD_SEMEPI = ? AND st.FC_RECEP = ? AND st.FC_FECNOTIF = ? " +
        " AND st.NM_EDO = ? AND st.CD_FUENTE = ? ");

    psValor = cnCon.prepareStatement(sbResult.toString());

    psValor.setString(1, sCD_E_NOTIF);
    psValor.setString(2, sCD_ANOEPI);
    psValor.setString(3, sCD_SEMEPI);
    psValor.setDate(4, dFC_RECEP);
    psValor.setDate(5, dFC_FECNOTIF);
    psValor.setInt(6, intNM_EDO.intValue());
    psValor.setString(7, sCD_FUENTE);

    return psValor;
  }

  /** prueba del servlet */
  static public void main(String[] args) {
    try {

      CLista data = new CLista();
      Vector vNotificadores = new Vector();

      CLista resultado = null;
      DatCasosTub hs = new DatCasosTub();

      SrvNotifTub srv = new SrvNotifTub();

      hs.put("META_MODO", new Integer(servletSELECT_SUCA));

      //*************************************************************

       // SIVE_EDOIND
      hs.setCD_ANOEPI("2000");
      hs.setCD_ANOOTRC("");
      hs.setCD_ANOURG("");
      hs.setCD_CLASIFDIAG("");
      hs.setCD_ENFCIE("005.1");
      hs.setCD_ENFERMO_EDOIND("3");
      hs.setCD_MUN_EDOIND("");
      hs.setCD_NIVEL_1_EDOIND("");
      hs.setCD_NIVEL_2_EDOIND("");
      hs.setCD_OPE_EDOIND("ADMON");
      hs.setCD_POSTAL_EDOIND("");
      hs.setCD_PROV_EDOIND("");
      hs.setCD_SEMEPI("50");
      hs.setCD_ZBS_EDOIND("");
      hs.setCDTNUM_EDOIND("");
      hs.setCDTVIA_EDOIND("");
      hs.setCDVIAL_EDOIND("");
      hs.setDS_ASOCIADO("");
      hs.setDS_CALLE("");
      hs.setDS_CENTRODER("");
      hs.setDS_COLECTIVO("");
      hs.setDS_DIAGOTROS("");
      hs.setDS_NMCALLE("");
      hs.setDS_PISO_EDOIND("");
      hs.setDSCALNUM_EDOIND("");
      hs.setFC_FECNOTIF("17/10/1999");
      hs.setFC_INISNT("01/01/1999");
      hs.setFC_RECEP("01/01/1999");
      hs.setFC_ULTACT_EDOIND("15/10/1999");
      hs.setIT_ASOCIADO("");
      hs.setIT_DERIVADO("");
      hs.setIT_DIAGCLI("");
      hs.setIT_DIAGMICRO("");
      hs.setIT_DIAGSERO("");
      hs.setIT_PENVURG("N");
      hs.put("NM_EDO", new Integer("9999"));
      hs.setNM_ENVIOURGSEM("1");
      hs.setNM_ENVOTRC("1");

      // SIVE_REGISTROTBC
      hs.setCD_ARTBC("1999");
      hs.setCD_NRTBC("");
      hs.setFC_INIRTBC("01/01/1999");
      hs.setFC_SALRTBC("");
      hs.setDS_OBSERV_TBC("");
      hs.setCD_MOTSALRTBC("");
      hs.setCD_E_NOTIF_TBC("");
      hs.setDS_MEDICO("");
      hs.setCD_NIVEL_1_TBC("1");
      hs.setCD_NIVEL_2_TBC("");

      // SIVE_ENFERMO
      hs.setCD_ENFERMO("");
      hs.setCD_TDOC("1");
      hs.setDS_APE1("Povedilla");
      hs.setSIGLAS("P");
      hs.setDS_FONOAPE1("PE");
      hs.setFC_NAC("01/10/1950");
      hs.setCD_OPE("A_1");
      hs.setFC_ULTACT("01/12/1999");
      hs.setIT_REVISADO("N");
      hs.setIT_ENFERMO("S");
      hs.setIT_CONTACTO("N");

      // SIVE_NOTIF_EDOI
      DatNotifTub dnt = new DatNotifTub();

      dnt.put("NM_EDO_EDOI", new Integer("9999"));
      dnt.setCD_E_NOTIF_EDOI("H01A");
      dnt.setCD_ANOEPI_EDOI("1999");
      dnt.setCD_SEMEPI("01");
      dnt.setFC_NOTIF("09/01/1999");
      dnt.setFC_RECEP_EDOI("10/01/1999");
      dnt.setCD_FUENTE("E");

      dnt.setDS_DECLARANTE("Povedilla & Mortimer");
      dnt.setDS_HISTCLI("DA1");
      dnt.setIT_PRIMERO("S");

      dnt.setCD_OPE_EDOI("A_1");
      dnt.setFC_ULTACT_EDOI("14/12/1999");

      // SIVE_TRATAMIENTOS
      Vector v = new Vector();
      Hashtable hT = new Hashtable();
      /*
            hT.put("CD_E_NOTIF_TRA", dnt.getCD_E_NOTIF_EDOI());
            hT.put("CD_ANOEPI_TRA", dnt.getCD_ANOEPI_EDOI());
            hT.put("CD_SEMEPI_TRA", dnt.getCD_SEMEPI());
            hT.put("FC_RECEP_TRA", dnt.getFC_RECEP_EDOI());
            hT.put("FC_FECNOTIF_TRA", dnt.getFC_NOTIF());
            hT.put("CD_FUENTE_TRA", dnt.getCD_FUENTE());
            hT.put("FC_INITRAT", "01/12/1999");
            hT.put("CD_MOTRATINI", "");
            hT.put("FC_FINTRAT", "01/12/2000");
            hT.put("CD_MOTRATFIN", "");
            hT.put("DS_OBSERV", "Ni�o tonto y rebelde");
            v.addElement(hT);
            dnt.setTRATAMIENTOS(v); */
      vNotificadores.addElement(dnt);

      //*************************************************************

      data.addElement(hs);

      data.addElement(vNotificadores);

      data.setLogin("A_1");

      resultado = srv.doPrueba(constantes.modoLEERDATOS, data);
      resultado = null;
    }
    catch (Exception e) {
      System.out.println(e.toString());
    }
    finally {
      System.exit(0);
    }
  }

  /** prueba del servlet */

  public CLista doPrueba(int operacion, CLista parametros) throws Exception {
    jdcdriver = new JDCConnectionDriver("oracle.jdbc.driver.OracleDriver",
        "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
        "sive_desa",
        "sive_desa");
    Connection con = null;
    con = openConnection();
    return doWork(operacion, parametros);
  }

} //fin de la clase
