/**
 * Clase: SrvMntCasos
 * Paquete: SP_Tuberculosis.servidor.mantregtub
 * Hereda: DBServlet
 * Autor: M Luisa Mora (JMT)
 * Fecha Inicio: 28/10/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos de Tuberculosis.
 * Descripcion: Implementacion del servlet que recupera de la BD los datos
 *   a mostrar en la tabla del PanMntRegTub: casos de tuberculosis.
 */

package tuberculosis.servidor.mantregtub;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import capp.CLista;
import comun.Fechas;
import sapp.DBServlet;
import tuberculosis.datos.mantregtub.DatMntRegTubCS;
import tuberculosis.datos.mantregtub.DatMntRegTubSC;

public class SrvMntCasos
    extends DBServlet {

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista();
    DatMntRegTubCS dataEntrada = null;
    DatMntRegTubSC dataSalida = null;
    boolean bFiltro = false;
    int iValorFiltro = 0;

    // Querys
    String sQuery = "";
    String sDatosEnfermo = "";

    // Lleva la cuenta del numero de registros a�adidos para el tramado
    int iNRegs = 1;

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con = openConnection();
    con.setAutoCommit(false);

    try {
      // Recuperacion de los datos de busqueda
      dataEntrada = (DatMntRegTubCS) param.firstElement();

      // Dependiendo del flag de confidencialidad se obtienen unos
      //  u otros datos acerca de los enfermos
      boolean bConfidencial = dataEntrada.getIT_FG_CONFIDENCIAL().trim().equals(
          "S");
      if (bConfidencial) {
        sDatosEnfermo = "e.SIGLAS";
      }
      else {
        sDatosEnfermo = "e.DS_APE1, e.DS_APE2, e.DS_NOMBRE";

        // Preparacion del patron de la query
      }
      sQuery = "SELECT r.CD_ARTBC, r.CD_NRTBC, " +
          " r.CD_NIVEL_1_GE, r.CD_NIVEL_2_GE, " +
          " i.NM_EDO, i.CD_NIVEL_1 CD_NIVEL_1_EDOIND, i.CD_NIVEL_2 CD_NIVEL_2_EDOIND, r.FC_INIRTBC, r.FC_SALRTBC, r.CD_OPE, r.FC_ULTACT, " +
          " e.CD_ENFERMO, " + sDatosEnfermo +
          " FROM SIVE_REGISTROTBC r, SIVE_EDOIND i, SIVE_ENFERMO e" +
          " WHERE (i.NM_EDO = r.NM_EDO AND e.CD_ENFERMO = i.CD_ENFERMO ";

      // Terminacion de la query en funcion de los parametros de busqueda que
      //  vienen en dataEntrada
      // Siguiente registro a leer
      if (param.getFilter().length() > 0) {
        bFiltro = true;
        iValorFiltro = (new Integer(param.getFilter().trim())).intValue();
      }
      sQuery = sQuery + TerminarPreparacionQuery(dataEntrada, bFiltro);

      // Ordenar los registros
      sQuery = sQuery + " ) ORDER BY r.CD_ARTBC DESC, r.CD_NRTBC DESC ";

      // Preparacion de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Instanciamos la query con los valores de los datos de entrada
      InstanciarQuery(dataEntrada, st, bFiltro, iValorFiltro);

      // Ejecucion de la query, obteniendo un ResultSet
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      while (rs.next()) {
        // Si iNRegs es > que el maximo numero de registros por peticion
        //  se sale del bucle dejando la lista en estado INCOMPLETA
        if (iNRegs > DBServlet.maxSIZE) {
          listaSalida.setState(CLista.listaINCOMPLETA);
          listaSalida.setFilter( ( (DatMntRegTubSC) listaSalida.lastElement()).
                                getCD_REG());
          break;
        }

        // Recuperacion del a�o, registro, fecha de entrada y codEnfermo
        //  a partir del ResultSet de la query realizada sobre la BD
        String Ano = rs.getString("CD_ARTBC");
        String Reg = rs.getString("CD_NRTBC");
        String FEntrada = UtilDateToString(rs.getDate("FC_INIRTBC"));
        int iCodEnfermo = rs.getInt("CD_ENFERMO");
        String CodEnfermo = (new Integer(iCodEnfermo)).toString();
        String Siglas = "";
        String Ape1Enfermo = "";
        String Ape2Enfermo = "";
        String NombreEnfermo = "";

        String n1 = "";
        String n2 = "";

        n1 = rs.getString("CD_NIVEL_1_GE"); //SERA OBLIGATORIO
        if (n1 == null) {
          n1 = "";
        }
        n2 = rs.getString("CD_NIVEL_2_GE");
        if (n2 == null) {
          n2 = "";

          // Segun el flag de confidencialidad se recuperan unos datos u otros
        }
        if (bConfidencial) {
          Siglas = rs.getString("SIGLAS");
          if (Siglas == null) {
            Siglas = "";
          }
        }
        else {
          Ape1Enfermo = rs.getString("DS_APE1");
          Ape2Enfermo = rs.getString("DS_APE2");
          NombreEnfermo = rs.getString("DS_NOMBRE");
          if (Ape1Enfermo == null) {
            Ape1Enfermo = "";
          }
          if (Ape2Enfermo == null) {
            Ape2Enfermo = "";
          }
          if (NombreEnfermo == null) {
            NombreEnfermo = "";
          }
        }

        String it_cerrado = "";
        java.util.Date dFC_SALRTBC;
        dFC_SALRTBC = rs.getDate("FC_SALRTBC");
        if (dFC_SALRTBC == null) {
          it_cerrado = "N";
        }
        else {
          it_cerrado = "S";

        }
        Integer Inm_edo = new Integer(rs.getInt("NM_EDO")); ;
        String nm_edo = Inm_edo.toString();

        // Datos para controlar el bloqueo sobre RTBC
        String cd_ope = rs.getString("CD_OPE");
        String fc_ultact = Fechas.timestamp2String(rs.getTimestamp("FC_ULTACT"));

        // Datos para conocer las 'coordenadas' de EDOIND para el protocolo
        String cd_nivel_1_edoind = rs.getString("CD_NIVEL_1_EDOIND");
        String cd_nivel_2_edoind = rs.getString("CD_NIVEL_2_EDOIND");

        // Relleno del registro de salida correspondiente a esta iteracion por
        //  los registros devueltos por la query

        dataSalida = new DatMntRegTubSC(Ano, Reg, FEntrada, CodEnfermo,
                                        Ape1Enfermo, Ape2Enfermo, NombreEnfermo,
                                        Siglas,
                                        it_cerrado, nm_edo, n1, n2, cd_ope,
                                        fc_ultact, cd_nivel_1_edoind,
                                        cd_nivel_2_edoind);

        // Adicion del registro obtenido a la lista resultado de salida hacia cliente
        listaSalida.addElement(dataSalida);

        // Se aumenta el numero de registros a�adidos
        iNRegs++;
      } // Fin for each registro

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);

    return listaSalida;
  } // Fin doWork()

  // Termina la preparacion de la query
  protected String TerminarPreparacionQuery(DatMntRegTubCS dataEntrada,
                                            boolean bFiltro) {

    // Registros de un a�o determinado (obligatorio)
    String sQ = " AND r.CD_ARTBC = ? ";

    // Un determinado numero de registro (unico)
    if (dataEntrada.getCD_REG().trim().length() > 0) {
      sQ = sQ + " AND r.CD_NRTBC = ? ";
      return (sQ);
    }

    // Tramado
    if (bFiltro) {
      sQ = sQ + " AND r.CD_NRTBC < ? ";

      // Cerrado o no cerrado: depende de si existe fecha de salida
    }
    if (dataEntrada.getIT_FG_CERRADO().trim().equals("S")) {
      sQ = sQ + " AND r.FC_SALRTBC IS NOT NULL ";
    }
    else if (dataEntrada.getIT_FG_CERRADO().trim().equals("N")) {
      sQ = sQ + " AND r.FC_SALRTBC IS NULL ";
    }
    else { //todos  (entraran los de casostub)
      sQ = sQ + "";

      // Un determinado codigo de enfermo
    }
    if (dataEntrada.getCD_CODENFERMO().trim().length() > 0) {
      sQ = sQ + " AND i.CD_ENFERMO = ? ";

      // Mayor o igual que un determinada fecha de entrada
    }
    if (dataEntrada.getFC_FECHADESDE().trim().length() > 0) {
      sQ = sQ + " AND r.FC_INIRTBC >= ? ";

      // Menor o igual que un determinada fecha de entrada
      if (dataEntrada.getFC_FECHAHASTA().trim().length() > 0) {
        sQ = sQ + " AND r.FC_INIRTBC <= ? ";
      }
    }

    return sQ;
  } // Fin TerminarPreparacionQuery()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void InstanciarQuery(DatMntRegTubCS dataEntrada,
                                 PreparedStatement st, boolean bFiltro,
                                 int iValorFiltro) throws Exception {

    // Contador de posicion del parametro
    int iParN = 1;

    // A�o (obligatorio)
    st.setString(iParN++, dataEntrada.getCD_ANO().trim());

    // Registro (unico)
    String Reg = dataEntrada.getCD_REG().trim();
    if (Reg.length() > 0) {
      st.setString(iParN++, Reg);
      return;
    }

    // Tramado
    if (bFiltro) {
      st.setInt(iParN++, iValorFiltro);

      // Codigo de enfermo
    }
    String CodEnfermo = dataEntrada.getCD_CODENFERMO().trim();
    if (CodEnfermo.length() > 0) {
      st.setInt(iParN++, (new Integer(CodEnfermo)).intValue());

      // Mayor o igual que un determinada fecha de entrada
    }
    String FDesde = dataEntrada.getFC_FECHADESDE().trim();
    String FHasta = dataEntrada.getFC_FECHAHASTA().trim();
    if (FDesde.length() > 0) {
      java.sql.Date fDesde = StringToSQLDate(FDesde);
      st.setDate(iParN++, fDesde);

      // Menor o igual que un determinada fecha de salida
      if (FHasta.length() > 0) {
        java.sql.Date fHasta = StringToSQLDate(FHasta);
        st.setDate(iParN++, fHasta);
      }
    }
  } // Fin InstanciarQuery()

  // Conversor de fecha String a java.sql.Date
  protected java.sql.Date StringToSQLDate(String sFecha) throws Exception {

    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    java.util.Date uFecha = formater.parse(sFecha);
    java.sql.Date sqlFec = new java.sql.Date(uFecha.getTime());
    return sqlFec;
  } // Fin StringToSQLDate()

  // Conversor de fecha java.util.Date a String
  protected String UtilDateToString(java.util.Date uFecha) throws Exception {

    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    String sFecha = "";
    if (uFecha == null) {
      sFecha = "";
    }
    else {
      sFecha = formater.format(uFecha);

    }
    return (sFecha);
  } // Fin UtilDateToString()

} // Fin SrvMntRegTub
