package tuberculosis.servidor.diaconflicto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import comun.SrvDebugGeneral;
import comun.constantes;
import tuberculosis.datos.diaconflicto.DatResConflicto;
import tuberculosis.datos.panconflicto.DatConflicto;

public class SrvResConflictos
    extends SrvDebugGeneral {
  // Modos de funcionamiento del servlet
  private final int modoLEERDATOS = constantes.modoLEERDATOS;
  private final int modoMODIFICACION = constantes.modoMODIFICACION;
  private final int modoMODIFICACION_SB = constantes.modoMODIFICACION_SB;

  // objetos JDBC
  Connection conexion = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {
    // Lista de valores de salida
    CLista clRetorno = null;

    // Para saber si se bloquea o no
    boolean bBloqueo = true;

    if (super.con != null) {
      conexion = super.con; // Se est� en modo debug
    }
    else {
      conexion = openConnection(); // Modo real
    }

    conexion.setAutoCommit(false);

    try {
      switch (opmode) {
        case modoLEERDATOS:
          clRetorno = clSelectConflictos(param);
          break;
        case modoMODIFICACION_SB:
          bBloqueo = false;
        case modoMODIFICACION:
          clRetorno = clUpdateConflictos(param, bBloqueo);
          break;
      }
      conexion.commit();
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
      conexion.rollback();
      throw e;
    }
    finally {
      closeConnection(conexion);
    }

    return clRetorno;
  }

  // m�todos privados
  private CLista clUpdateConflictos(CLista p, boolean bBloqueo) throws
      Exception {
    java.sql.Timestamp tsFechaActual = new java.sql.Timestamp(new java.util.
        Date().getTime());

    DatConflicto dc = null;
    DatResConflicto drc = null;

    String sQuery = null;
    PreparedStatement st = null;

    int iActualizados = 0;

    drc = (DatResConflicto) p.firstElement();
    dc = (DatConflicto) p.elementAt(1);

    // Se actualizan los datos de SIVE_MOVRESPEDO
    sQuery = " update SIVE_MOVRESPEDO set IT_CONFLICTO = ? " +
        " where " +
        " NM_EDO = ? and CD_TSIVE = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ? ";

    st = conexion.prepareStatement(sQuery);

    // Datos
    st.setString(1, "N");

    // Clave o where
    st.setInt(2, dc.getNM_EDO_MRE());
    st.setString(3, dc.getCD_TSIVE_MRE());
    st.setString(4, dc.getCD_MODELO_MRE());
    st.setInt(5, dc.getNM_LIN_MRE());
    st.setString(6, dc.getCD_PREGUNTA_MRE());

    st.executeUpdate();

    // Se actualizan los datos de SIVE_RESP_EDO
    sQuery = " update SIVE_RESP_EDO set DS_RESPUESTA = ? " +
        " where " +
        " NM_EDO = ? and CD_TSIVE = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ? ";

    st = conexion.prepareStatement(sQuery);

    // Datos
    st.setString(1, drc.getDS_RESPUESTA_MRE());

    // Clave o where
    st.setInt(2, dc.getNM_EDO_MRE());
    st.setString(3, dc.getCD_TSIVE_MRE());
    st.setString(4, dc.getCD_MODELO_MRE());
    st.setInt(5, dc.getNM_LIN_MRE());
    st.setString(6, dc.getCD_PREGUNTA_MRE());

    st.executeUpdate();

    // Se comprueba que no se ha modificado el caso
    // desde que se cargaron los datos, a la vez que
    // se actualizan los datos del mismo
    sQuery = " update SIVE_EDOIND set CD_OPE = ?, FC_ULTACT = ? " +
        " where " +
        " NM_EDO = ? ";

    if (bBloqueo) {
      sQuery = sQuery + " and CD_OPE = ? and FC_ULTACT = ? ";
    }

    st = conexion.prepareStatement(sQuery);

    // Datos
    st.setString(1, p.getLogin());
    st.setTimestamp(2, tsFechaActual);

    // Clave
    st.setInt(3, dc.getNM_EDO_MRE());

    // Bloqueo
    if (bBloqueo) {
      st.setString(4, drc.getCD_OPE_EDOIND());
      st.setTimestamp(5, drc.getFC_ULTACT_EDOIND());
    }

    iActualizados = st.executeUpdate();

    if (iActualizados == 0) {
      throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_EDOIND);
    }

    return new CLista();
  }

  private CLista clSelectConflictos(CLista p) throws Exception {
    //
    //java.sql.Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());
    //System.out.println(Fechas.timestamp2String(ts));
    //
    CLista clResultado = null;
    tuberculosis.datos.panconflicto.DatConflicto dcDatos = null;
    tuberculosis.datos.diaconflicto.DatResConflicto drc = null;

    String sQuery = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // Para recibir los datos de bloqueo de SIVE_EDOIND
    ResultSet rs_Edoind = null;
    java.sql.Timestamp ts_Fc_Ultact_Edoind = null;
    String s_Cd_Ope_Edoind = null;

    clResultado = new CLista();

    dcDatos = (DatConflicto) p.firstElement();

    //*****
    sQuery =
        " select smr.NM_EDO, smr.CD_E_NOTIF, smr.CD_ANOEPI, smr.CD_SEMEPI, " +
        " smr.FC_RECEP, smr.FC_FECNOTIF, smr.CD_FUENTE, smr.CD_TSIVE, " +
        " smr.CD_MODELO, smr.NM_LIN, smr.CD_PREGUNTA, smr.DS_RESPUESTA, smr.IT_CONFLICTO, smr.FC_ASIGN, " +
        " sne.DS_DECLARANTE, sen.CD_NIVEL_1, sen.CD_NIVEL_2 " +
        " from SIVE_MOVRESPEDO smr, SIVE_NOTIF_EDOI sne, SIVE_E_NOTIF sen " +
        " where smr.NM_EDO = ? and smr.CD_TSIVE = ? and smr.CD_MODELO = ? and smr.NM_LIN = ? and smr.CD_PREGUNTA = ? " +
        " and smr.NM_EDO = sne.NM_EDO and smr.CD_E_NOTIF = sne.CD_E_NOTIF " +
        " and smr.CD_ANOEPI = sne.CD_ANOEPI and smr.CD_SEMEPI = sne.CD_SEMEPI  " +
        " and smr.FC_RECEP = sne.FC_RECEP and smr.FC_FECNOTIF = sne.FC_FECNOTIF and smr.CD_FUENTE = sne.CD_FUENTE " +
        " and smr.CD_E_NOTIF = sen.CD_E_NOTIF " +
        " order by smr.FC_ASIGN DESC  ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, dcDatos.getNM_EDO_MRE());
    st.setString(2, dcDatos.getCD_TSIVE_MRE());
    st.setString(3, dcDatos.getCD_MODELO_MRE());
    st.setInt(4, dcDatos.getNM_LIN_MRE());
    st.setString(5, dcDatos.getCD_PREGUNTA_MRE());

    rs = st.executeQuery(sQuery);

    // Se obtienen los datos de bloqueo de SIVE_EDOIND
    sQuery = " select CD_OPE, FC_ULTACT from SIVE_EDOIND " +
        " where NM_EDO = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, dcDatos.getNM_EDO_MRE());

    rs_Edoind = st.executeQuery();

    rs_Edoind.next();

    ts_Fc_Ultact_Edoind = rs_Edoind.getTimestamp("FC_ULTACT");
    s_Cd_Ope_Edoind = rs_Edoind.getString("CD_OPE");

    //****

     while (rs.next()) {
      drc = new DatResConflicto();

      drc.setCD_E_NOTIF_MRE(rs.getString("CD_E_NOTIF"));
      drc.setDS_DECLARANTE_SNE(rs.getString("DS_DECLARANTE"));
      drc.setCD_FUENTE_MRE(rs.getString("CD_FUENTE"));
      drc.setDS_RESPUESTA_MRE(rs.getString("DS_RESPUESTA"));
      drc.setCD_ANOEPI_MRE(rs.getString("CD_ANOEPI"));
      drc.setCD_SEMEPI_MRE(rs.getString("CD_SEMEPI"));
      drc.setFC_FECNOTIF_MRE(rs.getDate("FC_FECNOTIF"));
      drc.setFC_RECEP_MRE(rs.getDate("FC_RECEP"));
      drc.setFC_ASIGN_MRE(rs.getDate("FC_ASIGN"));
      drc.setCD_NIVEL_1_EQ(rs.getString("CD_NIVEL_1"));
      drc.setCD_NIVEL_2_EQ(rs.getString("CD_NIVEL_2"));

      // Datos de bloqueo
      drc.setCD_OPE_EDOIND(s_Cd_Ope_Edoind);
      drc.setFC_ULTACT_EDOIND(ts_Fc_Ultact_Edoind);

      clResultado.addElement(drc);
    }

    // Devolvemos...
    return clResultado;
  }

  // Para modo debug
  protected CLista doDebug() throws Exception {
    CLista data = new CLista();

    return doPrueba(modoLEERDATOS, data);
  }

  /** prueba del servlet */
  public static void main(String[] args) {
    try {
      SrvResConflictos srv = new SrvResConflictos();
      srv.doDebug();
    }
    catch (Exception e) {
      System.out.println(e.toString());
    }
    finally {
      System.exit(0);
    }
  }

}