/**
 * @autor Pedro Antonio D�az Pardo (PDP)  / Emilio Postigo Riancho
 * @version 1.0
 *
 * Da de alta un nuevo registro de contacto de tuberculosis
 * Tabla:    SIVE_ENFERMO
 *
 * Tabla:    SIVE_CASOCONTAC
 *
 */

package tuberculosis.servidor.mantcontub;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Hashtable;

import capp.CLista;
import comun.constantes;
import sapp.DBServlet;
//import jdbcpool.*;
import tuberculosis.datos.mantcontub.DatConCasTub;

public class SrvConCasTub
    extends DBServlet {

  // Para obtener datos

  // Para recuperar datos de CA, MUN y ZBS
  private static final String sSQL_C_NORMAL =
      "SELECT CD_CA FROM SIVE_PROVINCIA WHERE CD_PROV = ?";
  private static final String sSQL_C_SUCA =
      "SELECT CDCOMU FROM SUCA_PROVINCIA WHERE CDPROV = ?";
  private static final String sSQL_M_NORMAL =
      "SELECT DS_MUN FROM SIVE_MUNICIPIO WHERE CD_PROV = ? AND CD_MUN = ?";
  private static final String sSQL_M_SUCA =
      "SELECT DSMUNI FROM SUCA_MUNICIPIO WHERE CDPROV = ? AND CDMUNI = ?";

  private static final String sSQL_ZBS_NORMAL = "SELECT DS_ZBS FROM SIVE_ZONA_BASICA WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ? AND CD_ZBS = ?";

  // SIVE_ENFERMO
  private static final String sSQL_ENFERMO =
      " se.CD_ENFERMO, se.CD_TDOC, se.DS_APE1, se.DS_APE2, se.SIGLAS, se.DS_NOMBRE, " +
      "se.DS_FONOAPE1, se.DS_FONOAPE2, se.DS_FONONOMBRE, se.IT_CALC, se.FC_NAC, " +
      "se.CD_SEXO, se.CD_POSTAL, se.DS_DIREC, se.DS_NUM, se.DS_PISO, se.DS_TELEF, " +
      "se.CD_NIVEL_1, se.CD_NIVEL_2, se.CD_ZBS, se.CD_OPE, se.FC_ULTACT, " +
      "se.IT_REVISADO, se.CD_MOTBAJA, se.FC_BAJA, se.DS_OBSERV, se.CD_PROV2, " +
      "se.CD_MUNI2, se.CD_POST2, se.DS_DIREC2, se.DS_NUM2, se.DS_PISO2, se.DS_OBSERV2, " +
      "se.CD_PROV3, se.CD_MUNI3, se.CD_POST3, se.DS_DIREC3, se.DS_NUM3, " +
      "se.DS_PISO3, se.DS_TELEF3, se.DS_OBSERV3, se.DS_NDOC, se.DS_TELEF2, " +
      "se.CDVIAL, se.CDTVIA, se.CDTNUM, se.DSCALNUM, se.CD_PROV, se.CD_MUN, " +
      "se.IT_ENFERMO, se.CD_PAIS, se.IT_CONTACTO ";

  private static final String sSQL_RTBC =
      " rt.CD_OPE CD_OPE_RTBC, rt.FC_ULTACT FC_ULTACT_RTBC ";

  // modos de operaci�n del servlet
  private final int servletCONCASTUB = 1;

  private final int modoMODIFICACION = constantes.modoMODIFICACION;
  private final int modoMODIFICACION_SB = constantes.modoMODIFICACION_SB;

  private final int modoALTA = constantes.modoALTA;
  private final int modoALTA_SB = constantes.modoALTA_SB;

  private final int modoBAJA = constantes.modoBAJA;
  private final int modoBAJA_SB = constantes.modoBAJA_SB;

  private final int modoSELECT = constantes.modoSELECT;

  // modificacion momentanea
  //Connection conexion = null;

  protected CLista doWork(int opmode, CLista param) throws Exception {
    //protected CLista doPrueba(int opmode, CLista param) throws Exception {
    /** hashtable que contiene los datos */
    //Hashtable parameter = null;
    DatConCasTub parameter = null;

    /** para reenviar los errores */
    String strError = null;

    /** conexi�n que se mantiene cada vez en el servidor */
    Connection con = null;

    /** statement de la sentencia SQL */
    PreparedStatement st = null;

    /** resultados de la query */
    ResultSet rs = null;

    /** sentencia sql que se est� ejecutando */
    StringBuffer strSQL = null;

    /** son los datos de los parametros */
    //CLista param = null;

    /**  indica el n�mero de datos que hay que traer */
    int iNumReg = 0;

    // Modo actual
    int iModo = 0;

    String descrip = null;
    Hashtable hash = null;
    CLista result = null;

    boolean bComprobarBloqueo = true;

    //Para contar los registros que se llevan introducidos
    //int numRegistros = 1;

    if (param == null) {
      // no nos han pasado datos iniciales
      return null;
    }

    parameter = (DatConCasTub) param.firstElement();

    // Se recuperan los par�metros b�sicos para comenzar a
    // trabajar
    iModo = opmode;

    // establece la conexi�n con la base de datos

//  if (conexion == null) {
    con = openConnection();
//  } else {
//    con = conexion;
//  }
    con.setAutoCommit(false);

    try {
      switch (opmode) {
        case modoSELECT:
          result = clSelectDatos(con, param);
          break;
        case modoALTA_SB:
          bComprobarBloqueo = false;
        case modoALTA:
          result = clInsertDatos(con, param, bComprobarBloqueo);
          break;
        case modoMODIFICACION_SB:
          bComprobarBloqueo = false;
        case modoMODIFICACION:
          result = clUpdateDatos(con, param, bComprobarBloqueo);
          break;
        case modoBAJA_SB:
          bComprobarBloqueo = false;
        case modoBAJA:
          result = clDeleteDatos(con, param, bComprobarBloqueo);
          break;
      }
      con.commit();
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
      con.rollback();
      result = null;
      throw e;
    }
    finally {
      closeConnection(con);
    }

    return result;
  }

  /** Descripci�n del �rea */
  private String getDS_AREA(DatConCasTub dctDatos, Connection con) {
    String sValor = null;

    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    try {
      sQuery = " select DS_NIVEL_1 from SIVE_NIVEL1_S where CD_NIVEL_1 = ? ";
      st = con.prepareStatement(sQuery);
      st.setString(1, dctDatos.getCD_NIVEL_1());
      rs = st.executeQuery();
      while (rs.next()) {
        sValor = rs.getString("DS_NIVEL_1");
        break; // Por si acaso...
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
    finally {
    }

    return sValor;
  }

  /** Descripci�n del distrito */
  private String getDS_DISTRITO(DatConCasTub dctDatos, Connection con) {
    String sValor = null;

    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    try {
      sQuery =
          " select DS_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_1 = ? and CD_NIVEL_2 = ? ";
      st = con.prepareStatement(sQuery);
      st.setString(1, dctDatos.getCD_NIVEL_1());
      st.setString(2, dctDatos.getCD_NIVEL_2());
      rs = st.executeQuery();
      while (rs.next()) {
        sValor = rs.getString("DS_NIVEL_2");
        break; // Por si acaso...
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
    finally {
    }

    return sValor;
  }

  /** Descripci�n de la ZBS */
  /*  private String getDS_ZBS(DatConCasTub dctDatos, Connection con) {
      String sValor = null;
      PreparedStatement st = null;
      ResultSet rs = null;
      String sQuery = null;
      try {
        sQuery = " select DS_ZBS from SIVE_ZONA_BASICA where CD_NIVEL_1 = ? " +
                 " and CD_NIVEL_2 = ? and CD_ZBS = ? ";
        st = con.prepareStatement(sQuery);
        st.setString(1, sCdArea);
        st.setString(2, sCdDistrito);
        st.setString(3, sCdZbs);
        rs = st.executeQuery();
        while (rs.next()) {
          sValor = rs.getString("DS_ZBS");
          break; // Por si acaso...
        }
      } catch (Exception e) {
        System.out.println(e.getMessage());
      } finally {
      }
      return sValor;
    }*/

  /** Descripci�n de la zonificaci�n sanitaria */
  private String sSQL_DS_ZBS(DatConCasTub dctDatos, Connection cnCon,
                             String sQuery) {
    String sValor = "";

    PreparedStatement st;
    ResultSet rs;

    try {
      st = cnCon.prepareStatement(sQuery);
      st.setString(1, dctDatos.getCD_NIVEL_1());
      st.setString(2, dctDatos.getCD_NIVEL_2());
      st.setString(3, dctDatos.getCD_ZBS());
      rs = st.executeQuery();
      while (rs.next()) {
        sValor = rs.getString(1);
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
    finally {
    }

    return sValor;
  }

  /** Descripci�n del municipio */
  private String sSQL_DS_MUNICIPIO(DatConCasTub dctDatos, Connection cnCon,
                                   String sQuery) {
    String sValor = "";

    PreparedStatement st;
    ResultSet rs;

    try {
      st = cnCon.prepareStatement(sQuery);
      st.setString(1, dctDatos.getCD_PROV());
      st.setString(2, dctDatos.getCD_MUN());
      rs = st.executeQuery();
      while (rs.next()) {
        sValor = rs.getString(1);
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
    finally {
    }

    return sValor;
  }

  /** C�digo de la comunidad aut�noma */
  private String sSQL_CD_CA(DatConCasTub dctDatos, Connection cnCon,
                            String sQuery) {
    String sValor = "";

    PreparedStatement st;
    ResultSet rs;

    try {
      st = cnCon.prepareStatement(sQuery);
      st.setString(1, dctDatos.getCD_PROV());
      rs = st.executeQuery();
      while (rs.next()) {
        sValor = rs.getString(1);
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
    finally {
    }

    return sValor;
  }

  // Para recuperar datos del contacto
  private CLista clSelectDatos(Connection conexion, CLista clLista) throws
      Exception {
    // Para devolver los datos
    CLista clRetorno = new CLista();

    // Objetos para almacenar los datos recibidos
    DatConCasTub dctCont = null;

    String sQuery = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    int iModoSuca = 0;

    String sSQLComunidad = null;
    String sSQLMunicipio = null;

    dctCont = (DatConCasTub) clLista.firstElement();

    iModoSuca = dctCont.getMODO_SUCA().intValue();

    if (iModoSuca == constantes.servletSELECT_NORMAL) {
      sSQLComunidad = sSQL_C_NORMAL;
      sSQLMunicipio = sSQL_M_NORMAL;
    }
    else {
      sSQLComunidad = sSQL_C_SUCA;
      sSQLMunicipio = sSQL_M_SUCA;
    }

    sQuery = " select " + sSQL_RTBC + ", " + sSQL_ENFERMO +
        " from SIVE_ENFERMO se, SIVE_REGISTROTBC rt, SIVE_CASOCONTAC cc " +
        " where " +
        " se.CD_ENFERMO = cc.CD_ENFERMO and " +
        " rt.CD_ARTBC = cc.CD_ARTBC and " +
        " rt.CD_NRTBC = cc.CD_NRTBC and " +
        " cc.CD_ENFERMO = ? and cc.CD_ARTBC = ? and cc.CD_NRTBC = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, dctCont.getCD_ENFERMO());
    st.setString(2, dctCont.getCD_ANO_RTBC());
    st.setString(3, dctCont.getCD_REG_RTBC());

    rs = st.executeQuery();

    if (rs.next()) {
      // Enfermo
      dctCont.setCD_TDOC(rs.getString("CD_TDOC"));
      dctCont.setDS_NDOC(rs.getString("DS_NDOC"));

      dctCont.setDS_NOMBRE(rs.getString("DS_NOMBRE"));
      dctCont.setDS_APE1(rs.getString("DS_APE1"));
      dctCont.setDS_APE2(rs.getString("DS_APE2"));

      dctCont.setDS_FONONOMBRE(rs.getString("DS_FONONOMBRE"));
      dctCont.setDS_FONOAPE1(rs.getString("DS_FONOAPE1"));
      dctCont.setDS_FONOAPE2(rs.getString("DS_FONOAPE2"));

      dctCont.setSIGLAS(rs.getString("SIGLAS"));

      dctCont.setIT_CALC(rs.getString("IT_CALC"));
      dctCont.setFC_NAC(rs.getDate("FC_NAC"));
      dctCont.setCD_SEXO(rs.getString("CD_SEXO"));
      dctCont.setCD_POSTAL(rs.getString("CD_POSTAL"));
      dctCont.setDS_DIREC(rs.getString("DS_DIREC"));
      dctCont.setDS_NUM(rs.getString("DS_NUM"));
      dctCont.setDS_PISO(rs.getString("DS_PISO"));
      dctCont.setDS_TELEF(rs.getString("DS_TELEF"));

      dctCont.setCD_NIVEL_1(rs.getString("CD_NIVEL_1"));
      dctCont.setCD_NIVEL_2(rs.getString("CD_NIVEL_2"));
      dctCont.setCD_ZBS(rs.getString("CD_ZBS"));

      dctCont.setIT_REVISADO(rs.getString("IT_REVISADO"));
      dctCont.setCD_MOTBAJA(rs.getString("CD_MOTBAJA"));
      dctCont.setFC_BAJA(rs.getDate("FC_BAJA"));
      dctCont.setDS_OBSERV(rs.getString("DS_OBSERV"));

      dctCont.setCD_PROV2(rs.getString("CD_PROV2"));
      dctCont.setCD_MUNI2(rs.getString("CD_MUNI2"));

      dctCont.setCD_POST2(rs.getString("CD_POST2"));
      dctCont.setDS_DIREC2(rs.getString("DS_DIREC2"));
      dctCont.setDS_NUM2(rs.getString("DS_NUM2"));
      dctCont.setDS_PISO2(rs.getString("DS_PISO2"));
      dctCont.setDS_TELEF2(rs.getString("DS_TELEF2"));
      dctCont.setDS_OBSERV2(rs.getString("DS_OBSERV2"));

      dctCont.setCD_PROV3(rs.getString("CD_PROV3"));
      dctCont.setCD_MUNI3(rs.getString("CD_MUNI3"));

      dctCont.setCD_POST3(rs.getString("CD_POST3"));
      dctCont.setDS_DIREC3(rs.getString("DS_DIREC3"));
      dctCont.setDS_NUM3(rs.getString("DS_NUM3"));
      dctCont.setDS_PISO3(rs.getString("DS_PISO3"));
      dctCont.setDS_TELEF3(rs.getString("DS_TELEF3"));
      dctCont.setDS_OBSERV3(rs.getString("DS_OBSERV3"));

      dctCont.setCD_PROV(rs.getString("CD_PROV"));
      dctCont.setCD_MUN(rs.getString("CD_MUN"));

      dctCont.setCDVIAL(rs.getString("CDVIAL"));
      dctCont.setCDTVIA(rs.getString("CDTVIA"));
      dctCont.setCDTNUM(rs.getString("CDTNUM"));
      dctCont.setDSCALNUM(rs.getString("DSCALNUM"));

      dctCont.setIT_ENFERMO(rs.getString("IT_ENFERMO"));
      dctCont.setCD_PAIS(rs.getString("CD_PAIS"));
      dctCont.setIT_CONTACTO(rs.getString("IT_CONTACTO"));

      dctCont.setCD_OPE(rs.getString("CD_OPE"));
      dctCont.setFC_ULTACT(rs.getTimestamp("FC_ULTACT"));

      // RTBC
      dctCont.setCD_OPE_RTBC(rs.getString("CD_OPE_RTBC"));
      dctCont.setFC_ULTACT_RTBC(rs.getTimestamp("FC_ULTACT_RTBC"));

    }

    // Datos de otras tablas
    dctCont.setCD_CA(sSQL_CD_CA(dctCont, conexion, sSQLComunidad));
    dctCont.setDS_MUN(sSQL_DS_MUNICIPIO(dctCont, conexion, sSQLMunicipio));
    dctCont.setDS_ZBS(sSQL_DS_ZBS(dctCont, conexion, sSQL_ZBS_NORMAL));

    dctCont.setDS_NIVEL_1(getDS_AREA(dctCont, conexion));
    dctCont.setDS_NIVEL_2(getDS_DISTRITO(dctCont, conexion));

    clRetorno.addElement(dctCont);

    return clRetorno;
  }

  // Para modificar datos del contacto
  private CLista clUpdateDatos(Connection conexion, CLista clLista,
                               boolean bBloqueo) throws Exception {
    // Devuelve datos
    CLista clRetorno = new CLista();

    // Datos de entrada
    DatConCasTub dcct = (DatConCasTub) clLista.firstElement();

    int iNmEnfermo = dcct.getCD_ENFERMO();

    // Fecha actual
    java.sql.Timestamp tsFechaActual = new java.sql.Timestamp( (new java.util.
        Date()).getTime());

    modificarEnfermo(conexion, dcct, iNmEnfermo, clLista.getLogin(),
                     tsFechaActual, bBloqueo);
    clRetorno.addElement(dcct);

    clRetorno = bloqueoRTBC(conexion, clRetorno, clLista.getLogin(),
                            tsFechaActual, bBloqueo);

    return clRetorno;
  }

  // Para borrar datos del contacto
  private CLista clDeleteDatos(Connection conexion, CLista clLista,
                               boolean bBloqueo) throws Exception {
    // Fecha actual
    java.sql.Timestamp tsFechaActual = new java.sql.Timestamp( (new java.util.
        Date()).getTime());

    // Para devolver datos actualizados
    CLista clRetorno = new CLista();

    // Datos recibidos
    DatConCasTub dcct = (DatConCasTub) clLista.firstElement();

    String sQuery = null;
    PreparedStatement st = null;

    // Borrado de respuestas de protocolo del contacto
    sQuery = " delete SIVE_RESPCONTACTO where CD_ARTBC = ? and CD_NRTBC = ? " +
        " and CD_ENFERMO = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setString(1, dcct.getCD_ANO_RTBC());
    st.setString(2, dcct.getCD_REG_RTBC());
    st.setInt(3, dcct.getCD_ENFERMO());

    st.executeUpdate();

    // Borrado en s�
    sQuery = " delete SIVE_CASOCONTAC where CD_ARTBC = ? and CD_NRTBC = ? " +
        " and CD_ENFERMO = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setString(1, dcct.getCD_ANO_RTBC());
    st.setString(2, dcct.getCD_REG_RTBC());
    st.setInt(3, dcct.getCD_ENFERMO());

    st.executeUpdate();

    // Se comprueba bloqueo de RTBC
    if (bBloqueo) {
      clRetorno = bloqueoRTBC(conexion, clLista, clLista.getLogin(),
                              tsFechaActual, bBloqueo);
    }

    // Se borran los movimientos del registro de enfermo
    sQuery = " delete SIVE_MOV_ENFERMO where CD_ENFERMO = ? ";
    st = conexion.prepareStatement(sQuery);

    st.setInt(1, dcct.getCD_ENFERMO());

    st.executeUpdate();

    // Se comprueba que el enfermo no ha sido
    // modificado
    if (bBloqueo) {
      bloqueoEnfermo(conexion, clLista);
    }

    // Se intenta borrar el registro de SIVE_ENFERMO
    // fallar� si est� asociado a otros casos (EDOIND o RTBC)
    sQuery = " delete SIVE_ENFERMO where CD_ENFERMO = ? ";
    st = conexion.prepareStatement(sQuery);

    st.setInt(1, dcct.getCD_ENFERMO());

    try {
      st.executeUpdate();
    }
    catch (java.sql.SQLException exc) {
      // Basta con no generar una excepci�n para
      // mantener controlado este caso
    }

    return clRetorno;
  }

  // Para insertar datos del contacto
  private CLista clInsertDatos(Connection conexion, CLista clLista,
                               boolean bBloqueo) throws Exception {

    // Para devolver datos actualizados
    CLista clRetorno = new CLista();

    // Fecha actual
    java.sql.Timestamp tsFechaActual = new java.sql.Timestamp( (new java.util.
        Date()).getTime());

    // Objetos para almacenar los datos recibidos
    DatConCasTub dctCont = null;

    // Objetos necesarios para el uso del secuenciador general
    ResultSet rs = null;
    PreparedStatement st = null;

    // Secuenciadores actuales
    //int iNM_EDO = sapp.Funciones.SecGeneral(con, st, rs, constantes.COLUMNA_EDO);
    //int iNM_MOVENF = sapp.Funciones.SecGeneral(con, st, rs, constantes.COLUMNA_MOVENF);
//    int iNM_TRATRTBC = sapp.Funciones.SecGeneral(con, st, rs, constantes.COLUMNA_TRATRTBC);

    int iNM_ENFERMO = 0;

    dctCont = (DatConCasTub) clLista.firstElement();

    if (dctCont.getCD_ENFERMO() == -1) { // Alta de contacto de con enfermo de tuberculosis
      iNM_ENFERMO = sapp.Funciones.SecGeneral(conexion, st, rs,
                                              constantes.COLUMNA_ENFERMO);
      insertarEnfermo(conexion, dctCont, iNM_ENFERMO, clLista.getLogin(),
                      tsFechaActual);
      // Insertar registro en SIVE_CASOCONTAC
      insertarCasoCont(conexion, dctCont, iNM_ENFERMO, clLista.getLogin());
    }
    else { // Modificaci�n de enfermo
      iNM_ENFERMO = dctCont.getCD_ENFERMO();
      // Aqu� se preguntar� si hay algo modificado
      modificarEnfermo(conexion, dctCont, iNM_ENFERMO, clLista.getLogin(),
                       tsFechaActual, bBloqueo);
      // Insertar registro en SIVE_CASOCONTAC
      insertarCasoCont(conexion, dctCont, iNM_ENFERMO, clLista.getLogin());
    }

    clRetorno.addElement(dctCont);
    clRetorno = bloqueoRTBC(conexion, clRetorno, clLista.getLogin(),
                            tsFechaActual, bBloqueo);

    return clRetorno;
  }

  // Inserta los datos del enfermo
  private void insertarEnfermo(Connection conexion, DatConCasTub dct,
                               int nmEnfermo, String sLogin,
                               java.sql.Timestamp tsFecha) throws Exception {

    String sQuery = null;
    PreparedStatement st = null;

    sQuery = " insert into SIVE_ENFERMO (" +
        " CD_ENFERMO, CD_TDOC, DS_APE1, DS_APE2, SIGLAS, DS_NOMBRE, " +
        " DS_FONOAPE1, DS_FONOAPE2, DS_FONONOMBRE, IT_CALC, FC_NAC, " +
        " CD_SEXO, CD_POSTAL, DS_DIREC, DS_NUM, DS_PISO, DS_TELEF, " +
        " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, CD_OPE, FC_ULTACT, " +
        " IT_REVISADO, DS_OBSERV, CD_PROV2, " +
        " CD_MUNI2, CD_POST2, DS_DIREC2, DS_NUM2, DS_PISO2, DS_OBSERV2, " +
        " CD_PROV3, CD_MUNI3, CD_POST3, DS_DIREC3, DS_NUM3, " +
        " DS_PISO3, DS_TELEF3, DS_OBSERV3, DS_NDOC, DS_TELEF2, " +
        " CDVIAL, CDTVIA, CDTNUM, DSCALNUM, CD_PROV, CD_MUN, " +
        " IT_ENFERMO, CD_PAIS, IT_CONTACTO ) " + //50
        " values " +
        " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + // 20
        "   ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + // 20
        "   ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) "; // 10

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, nmEnfermo);
    st.setString(2, dct.getCD_TDOC());
    st.setString(3, dct.getDS_APE1());
    st.setString(4, dct.getDS_APE2());
    st.setString(5, dct.getSIGLAS());
    st.setString(6, dct.getDS_NOMBRE());
    st.setString(7, dct.getDS_FONOAPE1());
    st.setString(8, dct.getDS_FONOAPE2());
    st.setString(9, dct.getDS_FONONOMBRE());
    st.setString(10, dct.getIT_CALC());

    if (dct.getFC_NAC() != null && !dct.getFC_NAC().equals("")) {
      //st.setDate(11, new java.sql.Date(  Fechas.string2Date( dct.getFC_NAC() ).getTime()  ) );
      st.setDate(11, dct.getFC_NAC());
    }
    else {
      st.setNull(11, java.sql.Types.DATE);
    }

    st.setString(12, dct.getCD_SEXO());
    st.setString(13, dct.getCD_POSTAL());
    st.setString(14, dct.getDS_DIREC());
    st.setString(15, dct.getDS_NUM());
    st.setString(16, dct.getDS_PISO());
    st.setString(17, dct.getDS_TELEF());
    st.setString(18, dct.getCD_NIVEL_1());
    st.setString(19, dct.getCD_NIVEL_2());
    st.setString(20, dct.getCD_ZBS());
    st.setString(21, sLogin);
    st.setTimestamp(22, tsFecha);
    st.setString(23, dct.getIT_REVISADO());
    st.setString(24, dct.getDS_OBSERV());
    st.setString(25, dct.getCD_PROV2());
    st.setString(26, dct.getCD_MUNI2());
    st.setString(27, dct.getCD_POST2());
    st.setString(28, dct.getDS_DIREC2());
    st.setString(29, dct.getDS_NUM2());
    st.setString(30, dct.getDS_PISO2());
    st.setString(31, dct.getDS_OBSERV2());
    st.setString(32, dct.getCD_PROV3());
    st.setString(33, dct.getCD_MUNI3());
    st.setString(34, dct.getCD_POST3());

    st.setString(35, dct.getDS_DIREC3());
    st.setString(36, dct.getDS_NUM3());
    st.setString(37, dct.getDS_PISO3());

    st.setString(38, dct.getDS_TELEF3());
    st.setString(39, dct.getDS_OBSERV3());
    st.setString(40, dct.getDS_NDOC());

    st.setString(41, dct.getDS_TELEF2());

    st.setString(42, dct.getCDVIAL());
    st.setString(43, dct.getCDTVIA());
    st.setString(44, dct.getCDTNUM());
    st.setString(45, dct.getDSCALNUM());
    st.setString(46, dct.getCD_PROV());
    st.setString(47, dct.getCD_MUN());
    st.setString(48, dct.getIT_ENFERMO());
    st.setString(49, dct.getCD_PAIS());
    st.setString(50, dct.getIT_CONTACTO());

    st.executeUpdate();

    // Datos para devolver al cliente
    dct.setCD_ENFERMO(nmEnfermo);
    dct.setCD_OPE(sLogin);
    dct.setFC_ULTACT(tsFecha);

    st = null;
    sQuery = null;

  } // Fin insertarEnfermo

  // Actualiza los datos del enfermo
  private void modificarEnfermo(Connection conexion, DatConCasTub dct,
                                int nmEnfermo, String sLogin,
                                java.sql.Timestamp tsFecha, boolean bBloqueo) throws
      Exception {

    // Consulta sql
    String sQuery = null;

    // Objetos necesarios para el uso del secuenciador general
    ResultSet rs = null;
    PreparedStatement st = null;

    // Secuenciador actual
    int iNM_MOVENF = sapp.Funciones.SecGeneral(conexion, st, rs,
                                               constantes.COLUMNA_MOVENF);

    String sQueryBloqueo = null;

    // Movimiento a SIVE_MOV_ENFERMO
    sQuery = " insert into SIVE_MOV_ENFERMO " +
        " select " + String.valueOf(iNM_MOVENF) +
        " NM_MOVENF, CD_ENFERMO, CD_PAIS, CD_TDOC, CD_PROV, CD_MUN, DS_APE1, DS_APE2, " +
        " SIGLAS, DS_NOMBRE, " +
        " DS_FONOAPE1, DS_FONOAPE2, DS_FONONOMBRE, IT_CALC, FC_NAC, " +
        " CD_SEXO, CD_POSTAL, DS_DIREC, DS_NUM, DS_PISO, DS_TELEF, " +
        " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, CD_OPE, FC_ULTACT, " +
        " IT_REVISADO, CD_MOTBAJA, FC_BAJA, DS_OBSERV, CD_PROV2, " +
        " CD_MUNI2, CD_POST2, DS_DIREC2, DS_NUM2, DS_PISO2, DS_OBSERV2, " +
        " CD_PROV3, CD_MUNI3, CD_POST3, DS_DIREC3, DS_NUM3, " +
        " DS_PISO3, DS_TELEF3, DS_OBSERV3, DS_NDOC, DS_TELEF2, " +
        " CDVIAL, CDTVIA, CDTNUM, " +
        " IT_ENFERMO, DSCALNUM, IT_CONTACTO " +
        " from SIVE_ENFERMO " +
        " where CD_ENFERMO = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, new Integer(dct.getCD_ENFERMO()).intValue());

    st.executeUpdate();

    // Actualizaci�n propiamente dicha
    int iActualizados = 0;

    sQuery = " update SIVE_ENFERMO set " +
        " CD_TDOC = ?, DS_APE1 = ?, DS_APE2 = ?, SIGLAS = ?, DS_NOMBRE = ?, " +
        " DS_FONOAPE1 = ?, DS_FONOAPE2 = ?, DS_FONONOMBRE = ?, IT_CALC = ?, FC_NAC = ?, " +
        " CD_SEXO = ?, CD_POSTAL = ?, DS_DIREC = ?, DS_NUM = ?, DS_PISO = ?, DS_TELEF = ?, " +
        " CD_NIVEL_1 = ?, CD_NIVEL_2 = ?, CD_ZBS = ?, CD_OPE = ?, FC_ULTACT = ?, " +
        " DS_OBSERV = ?, " +
        " DS_NDOC = ?, " +
        " CDVIAL = ?, CDTVIA = ?, CDTNUM = ?, DSCALNUM = ?, CD_PROV = ?, CD_MUN = ?, " +
        " CD_PAIS = ? " +
        " where CD_ENFERMO = ? ";

    if (bBloqueo) {
      sQueryBloqueo = " and CD_OPE = ? and FC_ULTACT = ? ";
    }
    else {
      sQueryBloqueo = "";
    }

    st = conexion.prepareStatement(sQuery + sQueryBloqueo);

    // Datos
    st.setString(1, dct.getCD_TDOC());
    st.setString(2, dct.getDS_APE1());
    st.setString(3, dct.getDS_APE2());
    st.setString(4, dct.getSIGLAS());
    st.setString(5, dct.getDS_NOMBRE());
    st.setString(6, dct.getDS_FONOAPE1());
    st.setString(7, dct.getDS_FONOAPE2());
    st.setString(8, dct.getDS_FONONOMBRE());
    st.setString(9, dct.getIT_CALC());

    if (dct.getFC_NAC() != null && !dct.getFC_NAC().equals("")) {
      //st.setDate(10, new java.sql.Date(  Fechas.string2Date( dct.getFC_NAC() ).getTime()  ) );
      st.setDate(10, dct.getFC_NAC());
    }
    else {
      st.setNull(10, java.sql.Types.DATE);
    }

    st.setString(11, dct.getCD_SEXO());
    st.setString(12, dct.getCD_POSTAL());
    st.setString(13, dct.getDS_DIREC());
    st.setString(14, dct.getDS_NUM());
    st.setString(15, dct.getDS_PISO());
    st.setString(16, dct.getDS_TELEF());
    st.setString(17, dct.getCD_NIVEL_1());
    st.setString(18, dct.getCD_NIVEL_2());
    st.setString(19, dct.getCD_ZBS());

    st.setString(20, sLogin);
    st.setTimestamp(21, tsFecha);

    st.setString(22, dct.getDS_OBSERV());

    st.setString(23, dct.getDS_NDOC());

    st.setString(24, dct.getCDVIAL());
    st.setString(25, dct.getCDTVIA());
    st.setString(26, dct.getCDTNUM());
    st.setString(27, dct.getDSCALNUM());
    st.setString(28, dct.getCD_PROV());
    st.setString(29, dct.getCD_MUN());

    st.setString(30, dct.getCD_PAIS());

    // Clave
    st.setInt(31, nmEnfermo);
    // Bloqueo
    if (bBloqueo) {
      st.setString(32, dct.getCD_OPE());
      st.setTimestamp(33, dct.getFC_ULTACT());
    }
    /*
         System.out.println("------------fecha ultact/ultact_rtbc-------------------");
     System.out.println(dct.getCD_OPE_RTBC()+" --- "+dct.getFC_ULTACT_RTBC());
     System.out.println(dct.getCD_OPE()+" --- "+dct.getFC_ULTACT());
     System.out.println("-------------------------------");
     */
    iActualizados = st.executeUpdate();

    if (iActualizados == 0) {
      throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_ENFERMO);
    }
    else {
      dct.setCD_OPE(sLogin);
      dct.setFC_ULTACT(tsFecha);
    }

    sQuery = null;
    st = null;

  } // Fin modificarEnfermo

  // Inserta los CasoCont
  private void insertarCasoCont(Connection conexion, DatConCasTub dct,
                                int nmEnfermo, String sLogin) throws Exception {

    String sQuery = null;
    PreparedStatement st = null;

    sQuery = " insert into SIVE_CASOCONTAC (" +
        " CD_ENFERMO, CD_ARTBC, CD_NRTBC) " +
        " values " +
        " ( ?, ?, ? ) ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, nmEnfermo);
    st.setString(2, dct.getCD_ANO_RTBC());
    st.setString(3, dct.getCD_REG_RTBC());

    st.executeUpdate();

    /*
         // Datos para devolver al cliente
         dct.setCD_ENFERMO(String.valueOf(nmEnfermo));
         dct.setCD_OPE(sLogin);
         dct.setFC_ULTACT(Fechas.timestamp2String(tsFecha));
     */

    st = null;
    sQuery = null;
  } // Fin insertarEnfermo

  // Bloqueo de ENFERMO
  // lanza una excepci�n si el enfermo ha sido actualizado.
  private void bloqueoEnfermo(Connection conexion, CLista clLista) throws
      Exception {
    DatConCasTub dcct = null;

    String sQuery = null;
    PreparedStatement st = null;

    int iActualizados = 0;

    dcct = (DatConCasTub) clLista.firstElement();

    sQuery = " update SIVE_ENFERMO set CD_OPE = ? " +
        " where " +
        " CD_OPE = ? and FC_ULTACT = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setString(1, dcct.getCD_OPE());
    st.setString(2, dcct.getCD_OPE());
    st.setTimestamp(3, dcct.getFC_ULTACT());

    iActualizados = st.executeUpdate();

    if (iActualizados == 0) {
      throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_ENFERMO);
    }

  }

  // Bloqueo de RTBC
  private CLista bloqueoRTBC(Connection conexion, CLista clLista, String sLogin,
                             Timestamp ts, boolean bBloqueo) throws Exception {
    CLista clResultado = new CLista();
    DatConCasTub dcct = null;

    String sQuery = null;
    PreparedStatement st = null;

    int iActualizados = 0;

    dcct = (DatConCasTub) clLista.firstElement();

    sQuery = " update SIVE_REGISTROTBC " +
        " set CD_OPE = ?, FC_ULTACT = ? " +
        " where " +
        " CD_ARTBC = ? and CD_NRTBC = ? ";

    if (bBloqueo) {
      sQuery += " and CD_OPE = ? and FC_ULTACT = ? ";
    }

    st = conexion.prepareStatement(sQuery);

    String sano = (String) dcct.getCD_ANO_RTBC();
    String sreg = (String) dcct.getCD_REG_RTBC();
    /*
     System.out.println("------------String-------------------");
     System.out.println(sano+" --- "+sreg);
     System.out.println("-------------------------------");
     */
    st.setString(1, sLogin);
    st.setTimestamp(2, ts);
    st.setString(3, dcct.getCD_ANO_RTBC());
    st.setString(4, dcct.getCD_REG_RTBC());
    /*
     System.out.println("--------------dcct-----------------");
     System.out.println(dcct.getCD_ANO_RTBC()+" --- "+dcct.getCD_REG_RTBC());
     System.out.println("-------------------------------");
     */
    //java.sql.Timestamp fcwhere = (java.sql.Timestamp) dcct.getFC_ULTACT_RTBC();
    //trazaLog("fcwhere " + fcwhere);
    /*
     System.out.println("------------fecha ultact-------------------");
         System.out.println(dcct.getCD_OPE_RTBC()+" --- "+dcct.getFC_ULTACT_RTBC());
     System.out.println("-------------------------------");
     */

    if (bBloqueo) {
      st.setString(5, dcct.getCD_OPE_RTBC());
      st.setTimestamp(6, dcct.getFC_ULTACT_RTBC());
    }

    iActualizados = st.executeUpdate();

    if (iActualizados == 0) {
      throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_REGISTROTBC);
    }
    else {
      dcct.setCD_OPE_RTBC(sLogin);
      dcct.setFC_ULTACT_RTBC(ts);
    }

    clResultado.addElement(dcct);
    return clResultado;
  }

  /** prueba del servlet */
  /*
    public CLista doPrueba(int operacion, CLista parametros) throws Exception {
      jdcdriver = new JDCConnectionDriver ("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                             "sive_desa",
                             "sive_desa");
      Connection con = null;
      con = openConnection();
      return doWork(operacion, parametros);
    }
   */

} //fin de la clase
