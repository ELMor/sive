/**
 * Clase: SrvCasTrat
 * Paquete: tuberculosis.servidor.pantrat
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 22/11/1999
 * Analisis Funcional: Punto 1. Mantenimiento Casos Tuberculosis.
 * Descripcion: Implementacion del servlet que recupera de la BD los datos
 *   a mostrar en la tabla del PanCasTrat: tratamientos de un notificador.
 */

package tuberculosis.servidor.pantrat;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import capp.CLista;
import sapp.DBServlet;
import tuberculosis.datos.pantrat.DatCasTratCS;
import tuberculosis.datos.pantrat.DatCasTratSC;

public class SrvCasTrat
    extends DBServlet {

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista();
    DatCasTratCS dataEntrada = null;
    DatCasTratSC dataSalida = null;

    // Querys
    String sQuery = "";

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con = openConnection();
    con.setAutoCommit(false);

    try {
      // Recuperacion de los datos de busqueda
      dataEntrada = (DatCasTratCS) param.firstElement();

      // Preparacion del patron de la query
      sQuery = "SELECT NM_TRATRTBC, FC_INITRAT, CD_MOTRATINI, " +
          "FC_FINTRAT, CD_MOTRATFIN, DS_OBSERV " +
          "FROM SIVE_TRATAMIENTOS " +
          "WHERE NM_EDO = ? AND CD_ANOEPI = ? AND CD_SEMEPI = ? " +
          "AND CD_E_NOTIF = ? AND CD_FUENTE = ? AND " +
          "FC_RECEP = ? AND FC_FECNOTIF = ?";

      // Ordenamiento
      sQuery = sQuery + " ORDER BY NM_TRATRTBC ASC";

      // Preparacion de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Instanciamos la query con los valores de los datos de entrada
      InstanciarQuery(dataEntrada, st);

      // Ejecucion de la query, obteniendo un ResultSet
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      while (rs.next()) {

        // Recuperacion del NM_TRATRTBC, FC_INITRAT, CD_MOTRATINI, FC_FINTRAT,
        //  CD_MOTRATFIN,DS_OBSERV
        //  a partir del ResultSet de la query realizada sobre la BD
        int iNTrat = rs.getInt("NM_TRATRTBC");
        String NTrat = (new Integer(iNTrat)).toString();
        String FIni = UtilDateToString(rs.getDate("FC_INITRAT"));
        String MotIni = rs.getString("CD_MOTRATINI");
        String DescMotIni = getDescMotTrat(con, MotIni);
        String FFin = UtilDateToString(rs.getDate("FC_FINTRAT"));
        String MotFin = rs.getString("CD_MOTRATFIN");
        String DescMotFin = getDescMotTrat(con, MotFin);
        String Observ = rs.getString("DS_OBSERV"); ;
        String Ope = "";
        String FUlt = "";

        // Relleno del registro de salida correspondiente a esta iteracion por
        //  los registros devueltos por la query
        dataSalida = new DatCasTratSC(NTrat, FIni, MotIni, DescMotIni,
                                      FFin, MotFin, DescMotFin, Observ, Ope,
                                      FUlt);

        // Adicion del registro obtenido a la lista resultado de salida hacia cliente
        listaSalida.addElement(dataSalida);
      } // Fin for each registro

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);

    return listaSalida;
  } // Fin doWork()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void InstanciarQuery(DatCasTratCS dataEntrada,
                                 PreparedStatement st) throws Exception {

    // Contador de posicion del parametro
    int iParN = 1;

    // Numero de EDO
    String nmEdo = dataEntrada.getNM_EDO().trim();
    st.setInt(iParN++, (new Integer(nmEdo)).intValue());

    // A�o epidemiologico
    st.setString(iParN++, dataEntrada.getCD_ANOEPI());

    // Semana epidemiologica
    st.setString(iParN++, dataEntrada.getCD_SEMEPI());

    // Codigo del equipo notificador
    st.setString(iParN++, dataEntrada.getCD_E_NOTIF());

    // Fuente
    st.setString(iParN++, dataEntrada.getCD_FUENTE());

    // Fecha recepcion
    String FRecep = dataEntrada.getFC_RECEP().trim();
    if (FRecep.length() > 0) {
      java.sql.Date fRecep = StringToSQLDate(FRecep);
      st.setDate(iParN++, fRecep);
    }

    // Fecha notificacion
    String FNotif = dataEntrada.getFC_FECNOTIF().trim();
    if (FNotif.length() > 0) {
      java.sql.Date fNotif = StringToSQLDate(FNotif);
      st.setDate(iParN++, fNotif);
    }

  } // Fin InstanciarQuery()

  // Conversor de fecha String a java.sql.Date
  protected java.sql.Date StringToSQLDate(String sFecha) throws Exception {

    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    java.util.Date uFecha = formater.parse(sFecha);
    java.sql.Date sqlFec = new java.sql.Date(uFecha.getTime());
    return sqlFec;
  } // Fin StringToSQLDate()

  // Conversor de fecha java.util.Date a String
  protected String UtilDateToString(java.util.Date uFecha) throws Exception {

    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    String sFecha = "";
    if (uFecha == null) {
      sFecha = "";
    }
    else {
      sFecha = formater.format(uFecha);

    }
    return (sFecha);
  } // Fin UtilDateToString()

  // Obtiene la descr. de un motivo de tratamiento
  String getDescMotTrat(Connection c, String CodMotTrat) throws Exception {
    if (CodMotTrat == null || CodMotTrat.equals("")) {
      return "";
    }

    // Objetos JDBC
    PreparedStatement stMotTrat = null;
    ResultSet rsMotTrat = null;

    // Query
    String MotQuery =
        "select DS_MOTRAT from SIVE_MOTIVO_TRAT where CD_MOTRAT = ?";
    String res = null;

    try {
      // Preparacion de la sentencia SQL
      stMotTrat = c.prepareStatement(MotQuery);

      // CodMun
      stMotTrat.setString(1, CodMotTrat.trim());

      // Ejecucion de la query
      rsMotTrat = stMotTrat.executeQuery();

      // Almacenamos la DS_MOTRAT obtenida
      rsMotTrat.next();
      res = rsMotTrat.getString("DS_MOTRAT");

      // Cierre del ResultSet y del PreparedStatement
      rsMotTrat.close();
      rsMotTrat = null;
      stMotTrat.close();
      stMotTrat = null;

    }
    catch (Exception ex) {
      throw ex;
    }

    return res;
  } // Fin getDescMotTrat()

} // Fin SrvMntRegTub
