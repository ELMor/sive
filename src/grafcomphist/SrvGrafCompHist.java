//Title:        Grafico Comparativo Historico
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina
//Company:      Norsistemas S.A.
//Description:

package grafcomphist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;

import capp.CLista;
import sapp.DBServlet;

public class SrvGrafCompHist
    extends DBServlet {

  // modos de operaci�n
  final int GRAFICO_HISTORICO = 0;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    //// System_out.println("SRVGRAFCOMPHIST: entri en doWork");

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    // objetos de datos
    CLista data = new CLista();
    Vector salida = new Vector();
    dataConsGrafHis datos = null;
    parConsGrafHis par = null;

    //auxiliares
    Vector enfermedades = new Vector();
    Enfermedad enfermedad = new Enfermedad();
    CuatriSemana guiaAct = new CuatriSemana();
    CuatriSemana guiaAnt = new CuatriSemana();
    CuatriSemana guiaPos = new CuatriSemana();

    //enfermedades para el gr�fico
    String enfs = "";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    par = (parConsGrafHis) param.firstElement();

    CLista listEnf = par.lista;

    //// System_out.println("SRVGRAFCOMPHIST: lista enfermedades " + listEnf);

    Hashtable elem = new Hashtable();

    for (int i = 0; i < listEnf.size(); i++) {
      elem = (Hashtable) listEnf.elementAt(i);
      if (elem.get("CD_ENFCIE") != null) {
        enfs = enfs + "'" + (String) elem.get("CD_ENFCIE") + "', ";
      }
    }
    if (enfs.lastIndexOf(',') != -1) {
      enfs = " ( " + enfs.substring(0, enfs.lastIndexOf(',')) + " ) ";
    }
    else {
      enfs = " ( " + " ) ";

      //// System_out.println("SRVGRAFCOMPHIST: " + enfs);

      // modos de operaci�n
    }
    switch (opmode) {

      // casos o tasas por cien mil habitantes por area
      case GRAFICO_HISTORICO:

        // guia de las cuatrisemanas actuales
        query = " select cd_semepi, cd_anoepi from sive_semana_epi "
            + " where ((cd_semepi<= ? and cd_anoepi = ?) or (cd_anoepi = ?) ) "
            + " order by  cd_anoepi desc, cd_semepi desc ";

        //// System_out.println("SRVGRAFCOMPHIST: guia de las cuatrisemanas actuales " + query);

        st = con.prepareStatement(query);

        st.setString(1, par.semH);
        st.setString(2, par.anoH);
        Integer a = new Integer(par.anoH);
        a = new Integer(a.intValue() - 1);
        st.setString(3, a.toString());

        rs = st.executeQuery();
        int sems = 0;
        while (rs.next()) {
          if (sems > 3) {
            break;
          }
          guiaAct.sems[sems] = rs.getString("cd_semepi");
          guiaAct.anos[sems] = rs.getString("cd_anoepi");
          sems++;
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        par.anoD = guiaAct.anos[3];
        par.semD = guiaAct.sems[3];

        //// System_out.println("SRVGRAFCOMPHIST: guiaAct.sems " + guiaAct.sems[0] + " " + guiaAct.sems[1] + " " +guiaAct.sems[2]+ " " +guiaAct.sems[3]);
        //// System_out.println("SRVGRAFCOMPHIST: guiaAct.anos " + guiaAct.anos[0] + " " + guiaAct.anos[1] + " " +guiaAct.anos[2]+ " " +guiaAct.anos[3]);
        // la query para la cuatrisemana actual,
        // del a�o actual
        // calculo la cuatrisemana 0
        query = " select sum(nm_casos), cd_enfcie"
            + " from sive_resumen_edos "
            + " where ((cd_semepi = ? and cd_anoepi = ?) or (cd_semepi = ? and cd_anoepi = ?) or (cd_semepi = ? and cd_anoepi = ?) or (cd_semepi = ? and cd_anoepi = ?) )"
            + " and cd_enfcie in "
            + enfs
            + " group by cd_enfcie "
            + " order by cd_enfcie ";

        //// System_out.println("SRVGRAFCOMPHIST: cuatrisemana actual " + query);

        st = con.prepareStatement(query);

        st.setString(1, guiaAct.sems[0]);
        st.setString(2, guiaAct.anos[0]);
        st.setString(3, guiaAct.sems[1]);
        st.setString(4, guiaAct.anos[1]);
        st.setString(5, guiaAct.sems[2]);
        st.setString(6, guiaAct.anos[2]);
        st.setString(7, guiaAct.sems[3]);
        st.setString(8, guiaAct.anos[3]);

        rs = st.executeQuery();
        int actual = 0;
        String enf = "";

        //AIC
        Hashtable listaEnfermedades = new Hashtable();
        Vector vEnferm = new Vector();
        for (int i = 0; i < listEnf.size(); i++) {
          listaEnfermedades = (Hashtable) listEnf.elementAt(i);
          if (listaEnfermedades.get("CD_ENFCIE") != null) {
            enfermedad = new Enfermedad();
            enfermedad.cd_enf = (String) listaEnfermedades.get("CD_ENFCIE");
            enfermedad.actual = 0;
            vEnferm.addElement(enfermedad);
          }
        }

        while (rs.next()) {
          enf = rs.getString("cd_enfcie");

          //AIC
          for (int i = 0; i < vEnferm.size(); i++) {
            enfermedad = (Enfermedad) vEnferm.elementAt(i);
            if (enfermedad.cd_enf.equals(enf)) {
              enfermedad.actual = rs.getInt(1);
              break;
            }
          }
          //enfermedad = new Enfermedad();
          //enfermedad.cd_enf = enf;
          //enfermedad.actual = rs.getInt(1);
          //// System_out.println("SRVGRAFCOMPHIST: enf " + enf + " casos " + enfermedad.actual);
          //enfermedades.addElement(enfermedad);
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        //AIC
        for (int i = 0; i < vEnferm.size(); i++) {
          enfermedades.addElement(vEnferm.elementAt(i));

          //// System_out.println("SRVGRAFCOMPHIST: enfemedades " + enfermedades.size());

          // la query para las cuatrisemanas actuales,
          // de los cinco a�os anteriores

          // pongo las semanas que aparecen el la guia que he
          // obtenido en la consulta anterior
        }
        query = " select sum(nm_casos), cd_enfcie"
            + " from sive_resumen_edos "
            + " where ((cd_semepi = ? and cd_anoepi = ?) or (cd_semepi = ? and cd_anoepi = ?) or (cd_semepi = ? and cd_anoepi = ?) or (cd_semepi = ? and cd_anoepi = ?) )"
            + " and cd_enfcie in " + enfs
            + " group by cd_enfcie "
            + " order by cd_enfcie ";

        //// System_out.println("SRVGRAFCOMPHIST: cuatrisemanas actuales " + query);

        Integer aux = null;
        for (int i = 1; i <= 5; i++) {
          //// System_out.print("SRVGRAFCOMPHIST: anos i=" + i );
          st = con.prepareStatement(query);
          st.setString(1, guiaAct.sems[0]);
          aux = new Integer(guiaAct.anos[0]);
          aux = new Integer(aux.intValue() - i);
          //// System_out.print(" " + aux.toString());
          st.setString(2, aux.toString());
          st.setString(3, guiaAct.sems[1]);
          aux = new Integer(guiaAct.anos[1]);
          aux = new Integer(aux.intValue() - i);
          //// System_out.print(" " + aux.toString());
          st.setString(4, aux.toString());
          st.setString(5, guiaAct.sems[2]);
          aux = new Integer(guiaAct.anos[2]);
          aux = new Integer(aux.intValue() - i);
          //// System_out.print(" " + aux.toString());
          st.setString(6, aux.toString());
          st.setString(7, guiaAct.sems[3]);
          aux = new Integer(guiaAct.anos[3]);
          aux = new Integer(aux.intValue() - i);
          //// System_out.println(" " + aux.toString());
          st.setString(8, aux.toString());

          rs = st.executeQuery();
          int indEnf = 0;

          while (rs.next()) {

            enf = rs.getString("cd_enfcie");
            indEnf = buscarEnfermedad(enfermedades, enf);
            int nada = 0;
            if (indEnf != -1) {
              enfermedad = (Enfermedad) enfermedades.elementAt(indEnf);
              nada = rs.getInt(1);
              enfermedad.valoresAct[i - 1] = nada;
              enfermedades.setElementAt(enfermedad, indEnf);
              //// System_out.println("SRVGRAFCOMPHIST: enf " + enf + " valoresAct[i-1] " + nada);
            }
          }
          rs.close();
          rs = null;
          st.close();
          st = null;
        }

        // guia de las cuatrisemanas anteriores
        query = " select cd_semepi, cd_anoepi from sive_semana_epi "
            + " where ((cd_semepi< ? and cd_anoepi = ?) or (cd_anoepi = ?) ) "
            + " order by  cd_anoepi desc, cd_semepi desc ";

        //// System_out.println("SRVGRAFCOMPHIST: guia de las cuatrisemanas anteriores " + query);

        st = con.prepareStatement(query);

        st.setString(1, par.semD);
        st.setString(2, par.anoD);
        a = new Integer(par.anoD);
        a = new Integer(a.intValue() - 1);
        st.setString(3, a.toString());

        rs = st.executeQuery();
        sems = 0;
        while (rs.next()) {
          if (sems > 3) {
            break;
          }
          guiaAnt.sems[sems] = rs.getString("cd_semepi");
          guiaAnt.anos[sems] = rs.getString("cd_anoepi");
          sems++;

        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        //// System_out.println("SRVGRAFCOMPHIST: guiaAnt " + guiaAnt.sems + " " + guiaAnt.anos);
        //// System_out.println("SRVGRAFCOMPHIST: guiaAnt.sems " + guiaAnt.sems[0] + " " + guiaAnt.sems[1] + " " +guiaAnt.sems[2]+ " " +guiaAnt.sems[3]);
        //// System_out.println("SRVGRAFCOMPHIST: guiaAnt.anos " + guiaAnt.anos[0] + " " + guiaAnt.anos[1] + " " +guiaAnt.anos[2]+ " " +guiaAnt.anos[3]);

        // la query para las cuatrisemanas anteriores,
        // de los cinco a�os anteriores

        query = " select sum(nm_casos), cd_enfcie "
            + " from sive_resumen_edos "
            + " where ((cd_semepi = ? and cd_anoepi = ?) or (cd_semepi = ? and cd_anoepi = ?) or (cd_semepi = ? and cd_anoepi = ?) or (cd_semepi = ? and cd_anoepi = ?) )"
            + " and cd_enfcie in "
            + enfs
            + " group by cd_enfcie "
            + " order by cd_enfcie ";

        //// System_out.println("SRVGRAFCOMPHIST: cuatrisemanas anteriores " + query);

        for (int i = 0; i < 5; i++) {
          //// System_out.print("SRVGRAFCOMPHIST: anos i=" + i );
          st = con.prepareStatement(query);
          st.setString(1, guiaAnt.sems[0]);
          aux = new Integer(guiaAnt.anos[0]);
          aux = new Integer(aux.intValue() - i - 1);
          //// System_out.println(" " + aux.toString());
          st.setString(2, aux.toString());
          st.setString(3, guiaAnt.sems[1]);
          aux = new Integer(guiaAnt.anos[1]);
          aux = new Integer(aux.intValue() - i - 1);
          //// System_out.println(" " + aux.toString());
          st.setString(4, aux.toString());
          st.setString(5, guiaAnt.sems[2]);
          aux = new Integer(guiaAnt.anos[2]);
          aux = new Integer(aux.intValue() - i - 1);
          //// System_out.println(" " + aux.toString());
          st.setString(6, aux.toString());
          st.setString(7, guiaAnt.sems[3]);
          aux = new Integer(guiaAnt.anos[3]);
          aux = new Integer(aux.intValue() - i - 1);
          //// System_out.println(" " + aux.toString());
          st.setString(8, aux.toString());

          rs = st.executeQuery();
          int indEnf = 0;
          int nada = 0;
          while (rs.next()) {
            enf = rs.getString("cd_enfcie");
            indEnf = buscarEnfermedad(enfermedades, enf);
            if (indEnf != -1) {
              enfermedad = (Enfermedad) enfermedades.elementAt(indEnf);
              nada = rs.getInt(1);
              enfermedad.valoresAnt[i] = nada;
              enfermedades.setElementAt(enfermedad, indEnf);
              //// System_out.println("SRVGRAFCOMPHIST: enf " + enf + " valoresAnt[i] " + nada);
            }
          }
          rs.close();
          rs = null;
          st.close();
          st = null;
        }

        // guia de las cuatrisemanas posteriores
        query = " select cd_semepi, cd_anoepi from sive_semana_epi "
            + " where ((cd_semepi> ? and cd_anoepi = ?) or (cd_anoepi = ?) ) "
            + " order by  cd_anoepi asc, cd_semepi asc ";

        //// System_out.println("SRVGRAFCOMPHIST: cuatrisemanas posteriores " + query);

        st = con.prepareStatement(query);

        st.setString(1, par.semH);
        st.setString(2, par.anoH);
        a = new Integer(par.anoH);
        a = new Integer(a.intValue() + 1);
        st.setString(3, a.toString());

        rs = st.executeQuery();
        sems = 0;
        while (rs.next()) {
          if (sems > 3) {
            break;
          }
          guiaPos.sems[sems] = rs.getString("cd_semepi");
          guiaPos.anos[sems] = rs.getString("cd_anoepi");
          sems++;

        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        //// System_out.println("SRVGRAFCOMPHIST: guiaPos " + guiaPos);
        //// System_out.println("SRVGRAFCOMPHIST: guiaPos.sems " + guiaPos.sems[0] + " " + guiaPos.sems[1] + " " +guiaPos.sems[2]+ " " +guiaPos.sems[3]);
        //// System_out.println("SRVGRAFCOMPHIST: guiaPos.anos " + guiaPos.anos[0] + " " + guiaPos.anos[1] + " " +guiaPos.anos[2]+ " " +guiaPos.anos[3]);

        // la query para las cuatrisemanas posteriores,
        // de los cinco a�os anteriores
        query = " select sum(nm_casos), cd_enfcie"
            + " from sive_resumen_edos "
            + " where ((cd_semepi = ? and cd_anoepi = ?) or (cd_semepi = ? and cd_anoepi = ?) or (cd_semepi = ? and cd_anoepi = ?) or (cd_semepi = ? and cd_anoepi = ?) )"
            + " and cd_enfcie in "
            + enfs
            + " group by cd_enfcie "
            + " order by cd_enfcie ";

        //// System_out.println("SRVGRAFCOMPHIST: uatrisemanas posteriores " + query);

        for (int i = 0; i < 5; i++) {
          //// System_out.print("SRVGRAFCOMPHIST: anos i=" + i );
          st = con.prepareStatement(query);
          st.setString(1, guiaPos.sems[0]);
          aux = new Integer(guiaPos.anos[0]);
          aux = new Integer(aux.intValue() - i - 1);
          //// System_out.println(" " + aux.toString());
          st.setString(2, aux.toString());
          st.setString(3, guiaPos.sems[1]);
          aux = new Integer(guiaPos.anos[1]);
          aux = new Integer(aux.intValue() - i - 1);
          //// System_out.println(" " + aux.toString());
          st.setString(4, aux.toString());
          st.setString(5, guiaPos.sems[2]);
          aux = new Integer(guiaPos.anos[2]);
          aux = new Integer(aux.intValue() - i - 1);
          //// System_out.println(" " + aux.toString());
          st.setString(6, aux.toString());
          st.setString(7, guiaPos.sems[3]);
          aux = new Integer(guiaPos.anos[3]);
          aux = new Integer(aux.intValue() - i - 1);
          //// System_out.println(" " + aux.toString());
          st.setString(8, aux.toString());

          rs = st.executeQuery();
          int indEnf = 0;
          int nada = 0;
          while (rs.next()) {
            enf = rs.getString("cd_enfcie");
            indEnf = buscarEnfermedad(enfermedades, enf);
            enfermedad = (Enfermedad) enfermedades.elementAt(indEnf);
            if (indEnf != -1) {
              enfermedad = (Enfermedad) enfermedades.elementAt(indEnf);
              nada = rs.getInt(1);
              enfermedad.valoresPos[i] = nada;
              enfermedades.setElementAt(enfermedad, indEnf);
              //// System_out.println("SRVGRAFCOMPHIST: enf " + enf + " valoresPos[i] " + nada);
            }
          }
          rs.close();
          rs = null;
          st.close();
          st = null;
        }

        //obtener la media de los casos por semana
        //con los 15 valores que se han obtenido
        int auxMed = 0;

        salida = new Vector();
        dataConsGrafHis dato = new dataConsGrafHis();
        float auxDS = (float) 0.0;
        for (int i = 0; i < enfermedades.size(); i++) {
          dato = new dataConsGrafHis();
          enfermedad = (Enfermedad) enfermedades.elementAt(i);
          //// System_out.println("SRVGRAFCOMPHIST: enfermedad " + enfermedad.cd_enf );
          auxMed = 0;
          for (int j = 0; j < 5; j++) {
            auxMed = auxMed + enfermedad.valoresAct[j] +
                enfermedad.valoresAnt[j] + enfermedad.valoresPos[j];
            //// System_out.println("SRVGRAFCOMPHIST: act " + enfermedad.valoresAct[j] + " ant " +enfermedad.valoresAnt[j] + " pos " + enfermedad.valoresPos[j] );
          }
          dato.cd_enf = enfermedad.cd_enf;
          dato.ds_enf = descEnfermedad(enfermedad.cd_enf, par);

          dato.media = (float) auxMed / (float) 15;
          //// System_out.println(dato.ds_enf + "  SRVGRAFCOMPHIST: media " + dato.media);
          //// System_out.println(dato.ds_enf + "  SRVGRAFCOMPHIST: valor " + enfermedad.actual);

          //AIC
          if ( (float) dato.media == (float) enfermedad.actual) {
            dato.razon = 1;
          }
          else {
            if ( (float) dato.media == 0.0) {
              dato.razon = enfermedad.actual;
            }
            else {
              dato.razon = (float) enfermedad.actual / dato.media;
            }
          }

          //// System_out.println("SRVGRAFCOMPHIST: razon " + dato.razon);

          dato.casos = enfermedad.actual;
          //// System_out.println("SRVGRAFCOMPHIST: casos " + dato.casos);

          auxDS = desvEstandar(enfermedad, dato.media);
          dato.umbral_inf = 1 - (2 * auxDS / dato.media);
          //// System_out.println("SRVGRAFCOMPHIST: umbral_inf " + dato.umbral_inf);

          dato.umbral_sup = 1 + (2 * auxDS / dato.media);
          //// System_out.println("SRVGRAFCOMPHIST: umbral_sup " + dato.umbral_sup);
          salida.addElement(dato);
        }

        //// System_out.println("SRVGRAFCOMPHIST: HOLA SOY EL NUEVO");

        break;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    data.addElement(salida);
    data.addElement(par);
    data.trimToSize();

    return data;
  }

  private float desvEstandar(Enfermedad enf, float med) {
    //// System_out.println("SRVGRAFCOMPHIST: calcular desv standar");
    float sum = (float) 0.0;
    for (int i = 0; i < 5; i++) {
      //// System_out.println("SRVGRAFCOMPHIST: ant " + enf.valoresAnt[i]);
      sum = sum +
          (float) Math.pow( (double) enf.valoresAnt[i] - (double) med, 2);
    }
    for (int i = 0; i < 5; i++) {
      //// System_out.println("SRVGRAFCOMPHIST: act " + enf.valoresAct[i]);
      sum = sum +
          (float) Math.pow( (double) enf.valoresAct[i] - (double) med, 2);
    }
    for (int i = 0; i < 5; i++) {
      //// System_out.println("SRVGRAFCOMPHIST: pos " + enf.valoresPos[i]);
      sum = sum +
          (float) Math.pow( (double) enf.valoresPos[i] - (double) med, 2);
    }
    sum = sum / (float) 15;
    sum = (float) Math.sqrt(sum);
    //// System_out.println("SRVGRAFCOMPHIST: desv st " + sum);
    return sum;
  }

  private String descEnfermedad(String cd, parConsGrafHis par) {
    Hashtable elem = new Hashtable();
    for (int i = 0; i < par.lista.size(); i++) {
      elem = (Hashtable) par.lista.elementAt(i);
      if (elem.get("CD_ENFCIE") != null) {
        if (cd.equals( (String) elem.get("CD_ENFCIE"))) {
          if (elem.get("DS_PROCESO") != null) {
            return (String) elem.get("DS_PROCESO");
          }
          else if (elem.get("DSL_PROCESO") != null) {
            return (String) elem.get("DSL_PROCESO");
          }
          else {
            return "";
          }

        }
      }
    }
    return "";
  }

  private int buscarEnfermedad(Vector enfermedades, String enf) {
    Enfermedad enfer = new Enfermedad();
    for (int i = 0; i < enfermedades.size(); i++) {
      enfer = (Enfermedad) enfermedades.elementAt(i);
      if (enf.equals(enfer.cd_enf)) {
        return i;
      }
    }
    return -1;
  }

}

class CuatriSemana {
  public String anos[] = {
      "", "", "", ""};
  public String sems[] = {
      "", "", "", ""};
  public int casos = 0;

  public CuatriSemana() {}
}

class Enfermedad {
  public String cd_enf = "";
  public int valoresAct[] = {
      0, 0, 0, 0, 0};
  public int valoresAnt[] = {
      0, 0, 0, 0, 0};
  public int valoresPos[] = {
      0, 0, 0, 0, 0};
  public int actual = 0;
  public float media = (float) 0.0;
  public float razon = (float) 0.0;

  public Enfermedad() {}
}
