//Title:        Grafico Comparativo Historico
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina
//Company:      Norsistemas S.A.
//Description:

package grafcomphist;

import capp.CApp;

public class grafcomphist
    extends CApp {

  public panelConsulta parametros;

  public grafcomphist() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    setTitulo("Gr�fico comparativo hist�rico");
    CApp a = (CApp)this;

    parametros = new panelConsulta(a);
    VerPanel("", parametros);
  }

}
