//Title:        Grafico Comparativo Historico
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina
//Company:      Norsistemas S.A.
//Description:

package grafcomphist;

import java.io.Serializable;

import capp.CLista;

public class parConsGrafHis
    implements Serializable {

  public String anoD = "";
  public String semD = "";
  public String anoH = "";
  public String semH = "";

  public CLista lista = null;

  public int numPagina = 0;
  public boolean bInformeCompleto = false;
  public String ultEnf = "";

  public parConsGrafHis(String a, String s) {

    anoH = a;
    semH = s;
    lista = new CLista();
  }

  public parConsGrafHis() {
    lista = new CLista();
  }

}