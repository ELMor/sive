package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

// LORTAD
import Registro.RegistroConsultas;
import capp.CApp;
import capp.CLista;
import notdata.DataEdoind;
import notdata.DataEnfCaso;
import notdata.DataEnfermo;
import notdata.DataNotif;
import notdata.DataNotifZona;
import notdata.DataTab;
import sapp.DBServlet;

public class SrvIndivSelect
    extends DBServlet {

  //ESTRUCTURA DE LA LISTA de entrada:
  //Enfermo y Caso , aunque solo uso: caso

  final int servletSELECT_EDO_INVIDIDUALIZADA = 0;
  final int servletSELECT_EDO_INVIDIDUALIZADA_SUCA = 1;

  public SimpleDateFormat Format_Horas = new SimpleDateFormat(
      "dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));
  public java.sql.Timestamp fecRecu = null;
  public String sfecRecu = "";

  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    DataEnfCaso dataEntrada = null;
    CLista listaSalida = new CLista();
    DataTab dataSalida = null;

    CLista listaCaso = new CLista();
    DataEdoind dataCaso = null;
    DataEdoind dataCaso2 = null;

    //descripciones
    String codCadef = null;
    String desProvdef = null;
    String desMunicipio = null;
    String desNivel1 = null;
    String desNivel2 = null;
    String desZB = null;
    String desVialite = null;
    String desVial = null;

    CLista listaEnfermo = new CLista();
    DataEnfermo dataEnfermo = null;
    DataEnfermo dataEnfermo2 = null;

    CLista listaNotif = new CLista();
    DataNotif dataNotif = null;

    CLista listaNotifZona = new CLista();
    DataNotifZona dataNotifZona = null;

    //querys
    String sQuery = "";

    //fechas
    java.util.Date dFecha;
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    /****************** APLICACION DE LORTAD ********************************/
    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;
          /****************** APLICACION DE LORTAD ********************************/

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);
    con.setAutoCommit(false);

    try {

      switch (opmode) {

        // con SUCA cambia la seleccion de descripciones de pais, ca, prov y mun
        case servletSELECT_EDO_INVIDIDUALIZADA:
        case servletSELECT_EDO_INVIDIDUALIZADA_SUCA:

          /*Recuperar el enfermo del caso, el caso, y sus notif_edoi (nm_edo)*/
          /*Habra tantos registros como notif_edoi*/
          /*El caso puede tener enfermo asociado  o no*/
          dataEntrada = (DataEnfCaso) param.firstElement();

//!!!!!!!!!!!!!!!!!!!!!!
//!!!!EDOIND!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!
          //selecciono el caso de edoind
          sQuery = Pregunta_Select_Edoind();
          st = con.prepareStatement(sQuery);

          st.setInt(1, (new Integer(dataEntrada.getNM_EDO().trim())).intValue());

          registroConsultas.insertarParametro(dataEntrada.getNM_EDO().trim());
          registroConsultas.registrar("SIVE_EDOIND",
                                      sQuery,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvIndivSelect",
                                      true);

          rs = st.executeQuery();

          String FC_INISNT = "";
          java.util.Date dFC_INISNT;
          fecRecu = null;
          sfecRecu = "";

          // extrae el registro encontrado
          while (rs.next()) {

            //campos criticos:-----------------------------------
            dFC_INISNT = rs.getDate("FC_INISNT");
            if (dFC_INISNT == null) {
              FC_INISNT = null;
            }
            else {
              FC_INISNT = formater.format(dFC_INISNT);

            }
            fecRecu = rs.getTimestamp("FC_ULTACT");
            sfecRecu = timestamp_a_cadena(fecRecu);

            // a�ade un nodo
            dataCaso = new DataEdoind(
                dataEntrada.getNM_EDO().trim(),
                rs.getString("CD_ANOEPI"),
                rs.getString("CD_SEMEPI"),
                rs.getString("CD_ANOURG"),
                rs.getString("NM_ENVIOURGSEM"), //devuelve nulo si lo fuera
                rs.getString("CD_CLASIFDIAG"),
                rs.getString("IT_PENVURG"),
                rs.getString("CD_ENFERMO"), //devuelve nulo si lo fuera
                rs.getString("CD_PROV"), "", "",
                rs.getString("CD_MUN"), "",
                rs.getString("CD_NIVEL_1"), "",
                rs.getString("CD_NIVEL_2"), "",
                rs.getString("CD_ZBS"), "",
                formater.format(rs.getDate("FC_RECEP")),
                rs.getString("DS_CALLE"),
                rs.getString("DS_NMCALLE"),
                rs.getString("DS_PISO"),
                rs.getString("CD_POSTAL"),
                rs.getString("CD_OPE"),
                sfecRecu,
                rs.getString("CD_ANOOTRC"),
                rs.getString("NM_ENVOTRC"), //devuelve nulo si lo fuera
                rs.getString("CD_ENFCIE"),
                formater.format(rs.getDate("FC_FECNOTIF")),
                rs.getString("IT_DERIVADO"),
                rs.getString("DS_CENTRODER"),
                rs.getString("IT_DIAGCLI"),
                FC_INISNT,
                rs.getString("DS_COLECTIVO"),
                rs.getString("IT_ASOCIADO"),
                rs.getString("IT_DIAGMICRO"),
                rs.getString("DS_ASOCIADO"),
                rs.getString("IT_DIAGSERO"),
                rs.getString("DS_DIAGOTROS"),
                rs.getString("CDVIAL"), rs.getString("CDTVIA"),
                rs.getString("CDTNUM"), rs.getString("DSCALNUM"));

          } //fin del while

          rs.close();
          rs = null;
          st.close();
          st = null;

          //completamos las descripciones
          //CLASIFICADO:

          // MUN y CA
          // con SUCA
          if (opmode == servletSELECT_EDO_INVIDIDUALIZADA_SUCA) {

            if (dataCaso.getCD_PROV_EDOIND() != null &&
                dataCaso.getCD_MUN_EDOIND() != null) {
              sQuery = Pregunta_Select_Municipio_SUCA();

              st = con.prepareStatement(sQuery);
              st.setString(1, dataCaso.getCD_PROV_EDOIND().trim());
              st.setString(2, dataCaso.getCD_MUN_EDOIND().trim());
              rs = st.executeQuery();
              while (rs.next()) {
                desMunicipio = rs.getString("DSMUNI");
              }
              rs.close();
              rs = null;
              st.close();
              st = null;

            }

            // recupera el c�digo de la comunidad aut�noma
            if (dataCaso.getCD_PROV_EDOIND() != null) {
              sQuery = "select CDCOMU from SUCA_PROVINCIA where CDPROV=?";

              st = con.prepareStatement(sQuery);
              st.setString(1, dataCaso.getCD_PROV_EDOIND().trim());
              rs = st.executeQuery();
              while (rs.next()) {
                codCadef = rs.getString("CDCOMU");
              }
              rs.close();
              rs = null;
              st.close();
              st = null;
            }

            // sin SUCA
          }
          else {

            if (dataCaso.getCD_PROV_EDOIND() != null &&
                dataCaso.getCD_MUN_EDOIND() != null) {
              sQuery = Pregunta_Select_Municipio();

              st = con.prepareStatement(sQuery);
              st.setString(1, dataCaso.getCD_PROV_EDOIND().trim());
              st.setString(2, dataCaso.getCD_MUN_EDOIND().trim());
              rs = st.executeQuery();
              while (rs.next()) {
                desMunicipio = rs.getString("DS_MUN");
              }
              rs.close();
              rs = null;
              st.close();
              st = null;

            }
            // recupera el c�digo de la comunidad aut�noma
            if (dataCaso.getCD_PROV_EDOIND() != null) {
              sQuery = "select CD_CA from SIVE_PROVINCIA where CD_PROV=?";

              st = con.prepareStatement(sQuery);
              st.setString(1, dataCaso.getCD_PROV_EDOIND().trim());
              rs = st.executeQuery();
              while (rs.next()) {
                codCadef = rs.getString("CD_CA");
              }
              rs.close();
              rs = null;
              st.close();
              st = null;
            }
          }

          //nivel 1
          if (dataCaso.getCD_NIVEL_1_EDOIND() != null) {
            sQuery = Pregunta_Select_Nivel1();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataCaso.getCD_NIVEL_1_EDOIND().trim());
            rs = st.executeQuery();
            String desN1 = "";
            String desLN1 = "";

            while (rs.next()) {
              desN1 = rs.getString("DS_NIVEL_1");
              desLN1 = rs.getString("DSL_NIVEL_1");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desLN1 != null)) {
                desNivel1 = desLN1;
              }
              else {
                desNivel1 = desN1;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

          }

          //nivel2
          if (dataCaso.getCD_NIVEL_1_EDOIND() != null &&
              dataCaso.getCD_NIVEL_2_EDOIND() != null) {
            sQuery = Pregunta_Select_Nivel2();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataCaso.getCD_NIVEL_1_EDOIND().trim());
            st.setString(2, dataCaso.getCD_NIVEL_2_EDOIND().trim());
            rs = st.executeQuery();
            String desN2 = "";
            String desLN2 = "";

            while (rs.next()) {
              desN2 = rs.getString("DS_NIVEL_2");
              desLN2 = rs.getString("DSL_NIVEL_2");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (desLN2 != null)) {
                desNivel2 = desLN2;
              }
              else {
                desNivel2 = desN2;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

          }

          //Zona Basica
          if (dataCaso.getCD_NIVEL_1_EDOIND() != null &&
              dataCaso.getCD_NIVEL_2_EDOIND() != null &&
              dataCaso.getCD_ZBS_EDOIND() != null) {
            sQuery = Pregunta_Select_ZonaBasica();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataCaso.getCD_NIVEL_1_EDOIND().trim());
            st.setString(2, dataCaso.getCD_NIVEL_2_EDOIND().trim());
            st.setString(3, dataCaso.getCD_ZBS_EDOIND().trim());
            rs = st.executeQuery();
            String desZona = "";
            String desLZona = "";

            while (rs.next()) {
              desZona = rs.getString("DS_ZBS");
              desLZona = rs.getString("DSL_ZBS");

              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                  && (rs.getString("DSL_ZBS") != null)) {
                desZB = desLZona;
              }
              else {
                desZB = desZona;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

          }

          //Reconstruimos dataCaso con dataCaso2
          if (dataCaso != null) {
            dataCaso2 = new DataEdoind(
                dataCaso.getNM_EDO(),
                dataCaso.getCD_ANOEPI(),
                dataCaso.getCD_SEMEPI(),
                dataCaso.getCD_ANOURG(),
                dataCaso.getNM_ENVIOURGSEM(), //puede ser nulo
                dataCaso.getCD_CLASIFDIAG(),
                dataCaso.getIT_PENVURG(),
                dataCaso.getCD_ENFERMO(), //puede ser nulo
                dataCaso.getCD_PROV_EDOIND(), desProvdef,
                codCadef,
                dataCaso.getCD_MUN_EDOIND(),
                desMunicipio,
                dataCaso.getCD_NIVEL_1_EDOIND(),
                desNivel1,
                dataCaso.getCD_NIVEL_2_EDOIND(),
                desNivel2,
                dataCaso.getCD_ZBS_EDOIND(),
                desZB,
                dataCaso.getFC_RECEP(),
                dataCaso.getDS_CALLE(),
                dataCaso.getDS_NMCALLE(),
                dataCaso.getDS_PISO_EDOIND(),
                dataCaso.getCD_POSTAL_EDOIND(),
                dataCaso.getCD_OPE_EDOIND(),
                dataCaso.getFC_ULTACT_EDOIND(),
                dataCaso.getCD_ANOOTRC(),
                dataCaso.getNM_ENVOTRC(),
                dataCaso.getCD_ENFCIE(),
                dataCaso.getFC_FECNOTIF(),
                dataCaso.getIT_DERIVADO(),
                dataCaso.getDS_CENTRODER(),
                dataCaso.getIT_DIAGCLI(),
                dataCaso.getFC_INISNT(), //puede ser nulo
                dataCaso.getDS_COLECTIVO(),
                dataCaso.getIT_ASOCIADO(),
                dataCaso.getIT_DIAGMICRO(),
                dataCaso.getDS_ASOCIADO(),
                dataCaso.getIT_DIAGSERO(),
                dataCaso.getDS_DIAGOTROS(),
                dataCaso.getCDVIAL(),
                dataCaso.getCDTVIA(),
                dataCaso.getCDTNUM(),
                dataCaso.getDSCALNUM());

            //cargo en la lista
            //NOTA: si el caso no existe, se pasa null
          }
          listaCaso.addElement(dataCaso2); //fuera bucle, es UNA

          //-------------------------------------------------------
//!!!!!!!!!!!!!!!!!!!!!!
//!!!!ENFERMO!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!

          if (dataCaso.getCD_ENFERMO() != null) {

            sQuery = Pregunta_Select_Enfermo();
            st = con.prepareStatement(sQuery);
            st.setInt(1, new Integer(dataCaso.getCD_ENFERMO().trim()).intValue());

            registroConsultas.insertarParametro(dataCaso.getCD_ENFERMO().trim());
            registroConsultas.registrar("SIVE_ENFERMO",
                                        sQuery,
                                        "CD_ENFERMO",
                                        dataCaso.getCD_ENFERMO().trim(),
                                        "SrvIndivSelect",
                                        true);

            rs = st.executeQuery();

            String FC_NAC = "";
            java.util.Date dFC_NAC;
            String FC_BAJA = "";
            java.util.Date dFC_BAJA;

            fecRecu = null;
            sfecRecu = "";

            while (rs.next()) {
              fecRecu = rs.getTimestamp("FC_ULTACT");
              sfecRecu = timestamp_a_cadena(fecRecu);

              //campos criticos:-----------------------------------
              dFC_NAC = rs.getDate("FC_NAC");
              if (dFC_NAC == null) {
                FC_NAC = null;
              }
              else {
                FC_NAC = formater.format(dFC_NAC);

              }

              dFC_BAJA = rs.getDate("FC_BAJA");
              if (dFC_BAJA == null) {
                FC_BAJA = null;
              }
              else {
                FC_BAJA = formater.format(dFC_BAJA);
                //---------------------------------------------------

                // a�ade un nodo
              }
              dataEnfermo = new DataEnfermo(
                  dataCaso.getCD_ENFERMO().trim(),
                  rs.getString("CD_TDOC"),
                  rs.getString("DS_APE1"), rs.getString("DS_APE2"),
                  rs.getString("DS_NOMBRE"), rs.getString("DS_FONOAPE1"),
                  rs.getString("DS_FONOAPE2"), rs.getString("DS_FONONOMBRE"),
                  rs.getString("IT_CALC"),
                  FC_NAC,
                  rs.getString("CD_SEXO"), rs.getString("DS_TELEF"),
                  rs.getString("CD_OPE"),
                  sfecRecu,
                  rs.getString("IT_REVISADO"), rs.getString("CD_MOTBAJA"),
                  FC_BAJA,
                  rs.getString("DS_NDOC"), rs.getString("SIGLAS"));

            } //fin del while
            rs.close();
            rs = null;
            st.close();
            st = null;

            //reconstruimos Enfermo
            if (dataEnfermo != null) {
              dataEnfermo2 = new DataEnfermo(
                  dataCaso.getCD_ENFERMO().trim(),
                  dataEnfermo.getCD_TDOC(),
                  dataEnfermo.getDS_APE1(),
                  dataEnfermo.getDS_APE2(),
                  dataEnfermo.getDS_NOMBRE(),
                  dataEnfermo.getDS_FONOAPE1(),
                  dataEnfermo.getDS_FONOAPE2(),
                  dataEnfermo.getDS_FONONOMBRE(),
                  dataEnfermo.getIT_CALC(),
                  dataEnfermo.getFC_NAC(),
                  dataEnfermo.getCD_SEXO(),
                  dataEnfermo.getDS_TELEF(),
                  dataEnfermo.getCD_OPE(),
                  dataEnfermo.getFC_ULTACT(),
                  dataEnfermo.getIT_REVISADO(),
                  dataEnfermo.getCD_MOTBAJA(),
                  dataEnfermo.getFC_BAJA(),
                  dataEnfermo.getDS_NDOC(),
                  dataEnfermo.getSIGLAS());

            }

            listaEnfermo.addElement(dataEnfermo2); //fuera bucle, es UNA

          } //fin de si existe el enfermo: AL FINAL RESULTA QUE ES OBLIGATORIO!!!

//!!!!!!!!!!!!!!!!!!!!!!
//!!!! notif_edoi !!!!!!
//!!!!!!!!!!!!!!!!!!!!!! Todos los de el caso!
          sQuery = Pregunta_Select_Notif_Edoi();
          st = con.prepareStatement(sQuery);
          st.setInt(1, (new Integer(dataEntrada.getNM_EDO().trim())).intValue());
          rs = st.executeQuery();
          fecRecu = null;
          sfecRecu = "";
          while (rs.next()) {
            fecRecu = rs.getTimestamp("FC_ULTACT");
            sfecRecu = timestamp_a_cadena(fecRecu);
            // a�ade un nodo

            dataNotif = new DataNotif(
                dataEntrada.getNM_EDO().trim(),
                rs.getString("CD_E_NOTIF"),
                rs.getString("CD_ANOEPI"),
                rs.getString("CD_SEMEPI"),
                formater.format(rs.getDate("FC_RECEP")),
                rs.getString("DS_DECLARANTE"),
                rs.getString("IT_PRIMERO"),
                rs.getString("CD_OPE"),
                sfecRecu,
                formater.format(rs.getDate("FC_FECNOTIF"))); //rs.getString("FC_FECNOTIF") );

            listaNotif.addElement(dataNotif); //dentro del bucle, son varias

          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          ////////////
          //query para recuperar el proceso
          sQuery = "SELECT DS_PROCESO FROM SIVE_PROCESOS WHERE " +
              "CD_ENFCIE = ? ";

          String proceso = new String();

          st = con.prepareStatement(sQuery);
          st.setString(1, dataCaso.getCD_ENFCIE());
          rs = st.executeQuery();

          while (rs.next()) {
            proceso = rs.getString("DS_PROCESO");
          }

          //-----------------

          ////////////

          //zonas para cada Equipo
          for (i = 0; i < listaNotif.size(); i++) {

            dataNotif = (DataNotif) listaNotif.elementAt(i);
            sQuery = Pregunta_Select_Equipo_Notificador();

            st = con.prepareStatement(sQuery);
            st.setString(1, dataNotif.getCD_E_NOTIF());
            rs = st.executeQuery();

            while (rs.next()) {

              dataNotifZona = new DataNotifZona(
                  dataNotif.getNM_EDO(),
                  dataNotif.getCD_E_NOTIF(),
                  dataNotif.getCD_ANOEPI_NOTIF_EDOI(),
                  dataNotif.getCD_SEMEPI_NOTIF_EDOI(),
                  dataNotif.getFC_RECEP_NOTIF_EDOI(),
                  dataNotif.getDS_DECLARANTE(),
                  dataNotif.getIT_PRIMERO(),
                  dataNotif.getCD_OPE_NOTIF_EDOI(),
                  dataNotif.getFC_ULTACT_NOTIF_EDOI(),
                  dataNotif.getFC_FECNOTIF_NOTIF_EDOI(),
                  rs.getString("CD_NIVEL_1"),
                  rs.getString("CD_NIVEL_2"),
                  rs.getString("CD_ZBS"),
                  rs.getString("CD_CENTRO"), "");

            }
            rs.close();
            rs = null;
            st.close();
            st = null;

            //query para centros
            sQuery = "SELECT DS_CENTRO FROM SIVE_C_NOTIF WHERE " +
                "CD_CENTRO = ? ";

            st = con.prepareStatement(sQuery);
            st.setString(1, dataNotifZona.getCD_CENTRO_E_NOTIF());
            rs = st.executeQuery();

            while (rs.next()) {
              dataNotifZona.DS_CENTRO_E_NOTIF = rs.getString("DS_CENTRO");
            }
            //-----------------

            listaNotifZona.addElement(dataNotifZona);
            listaSalida.addElement(dataNotifZona);

          } //fin for

          //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          //COMPONER LA LISTA DE SALIDA!!!!!!!!!!!
          //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          //Modificamos el primer registro de listaSalida (listaNotifZona)
          //dataCaso, dataEnfermo, dataNotifZona
          if (listaNotifZona.size() > 0) {
            dataNotifZona = (DataNotifZona) listaNotifZona.elementAt(0); //cargo el primero
          }
          else {
            //dataNotifZona esta con todo a null
            //No debe darse. Todo Caso tiene al menos un declarante
          }

          //RECOMPONEMOS EL PRIMER ELEMENTO DE LA LISTA SALIDA*******
          listaSalida.removeElementAt(0);

          //en el elemento cero, debo a�adir los datos
          dataSalida = new DataTab();
          dataSalida.insert( (String) "CD_ENFERMO", dataEnfermo2.getCD_ENFERMO());
          dataSalida.insert( (String) "CD_TDOC", dataEnfermo2.getCD_TDOC());
          dataSalida.insert( (String) "DS_APE1", dataEnfermo2.getDS_APE1());
          dataSalida.insert( (String) "DS_APE2", dataEnfermo2.getDS_APE2());
          dataSalida.insert( (String) "DS_NOMBRE", dataEnfermo2.getDS_NOMBRE());
          dataSalida.insert( (String) "DS_FONOAPE1",
                            dataEnfermo2.getDS_FONOAPE1());
          dataSalida.insert( (String) "DS_FONOAPE2",
                            dataEnfermo2.getDS_FONOAPE2());
          dataSalida.insert( (String) "DS_FONONOMBRE",
                            dataEnfermo2.getDS_FONONOMBRE());
          dataSalida.insert( (String) "IT_CALC", dataEnfermo2.getIT_CALC());
          dataSalida.insert( (String) "FC_NAC", dataEnfermo2.getFC_NAC());
          dataSalida.insert( (String) "CD_SEXO", dataEnfermo2.getCD_SEXO());
          dataSalida.insert( (String) "DS_TELEF", dataEnfermo2.getDS_TELEF());
          dataSalida.insert( (String) "CD_OPE", dataEnfermo2.getCD_OPE());
          dataSalida.insert( (String) "FC_ULTACT", dataEnfermo2.getFC_ULTACT());
          dataSalida.insert( (String) "IT_REVISADO",
                            dataEnfermo2.getIT_REVISADO());
          dataSalida.insert( (String) "CD_MOTBAJA", dataEnfermo2.getCD_MOTBAJA());
          dataSalida.insert( (String) "FC_BAJA", dataEnfermo2.getFC_BAJA());
          dataSalida.insert( (String) "DS_NDOC", dataEnfermo2.getDS_NDOC());
          dataSalida.insert( (String) "SIGLAS", dataEnfermo2.getSIGLAS());
          //caso
          dataSalida.insert( (String) "NM_EDO", dataCaso2.getNM_EDO());
          dataSalida.insert( (String) "CD_ANOEPI", dataCaso2.getCD_ANOEPI());
          dataSalida.insert( (String) "CD_SEMEPI", dataCaso2.getCD_SEMEPI());
          dataSalida.insert( (String) "CD_ANOURG", dataCaso2.getCD_ANOURG());
          dataSalida.insert( (String) "NM_ENVIOURGSEM",
                            dataCaso2.getNM_ENVIOURGSEM());
          dataSalida.insert( (String) "CD_CLASIFDIAG",
                            dataCaso2.getCD_CLASIFDIAG());
          dataSalida.insert( (String) "IT_PENVURG", dataCaso2.getIT_PENVURG());
          dataSalida.insert( (String) "CD_PROV_EDOIND",
                            dataCaso2.getCD_PROV_EDOIND());
          dataSalida.insert( (String) "CD_CA_EDOIND", dataCaso2.getCD_CA_EDOIND());
          dataSalida.insert( (String) "CD_MUN_EDOIND",
                            dataCaso2.getCD_MUN_EDOIND());
          dataSalida.insert( (String) "DS_MUN_EDOIND",
                            dataCaso2.getDS_MUN_EDOIND());
          dataSalida.insert( (String) "CD_NIVEL_1_EDOIND",
                            dataCaso2.getCD_NIVEL_1_EDOIND());
          dataSalida.insert( (String) "DS_NIVEL_1_EDOIND",
                            dataCaso2.getDS_NIVEL_1_EDOIND());
          dataSalida.insert( (String) "CD_NIVEL_2_EDOIND",
                            dataCaso2.getCD_NIVEL_2_EDOIND());
          dataSalida.insert( (String) "DS_NIVEL_2_EDOIND",
                            dataCaso2.getDS_NIVEL_2_EDOIND());
          dataSalida.insert( (String) "CD_ZBS_EDOIND",
                            dataCaso2.getCD_ZBS_EDOIND());
          dataSalida.insert( (String) "DS_ZBS_EDOIND",
                            dataCaso2.getDS_ZBS_EDOIND());
          dataSalida.insert( (String) "FC_RECEP", dataCaso2.getFC_RECEP());
          dataSalida.insert( (String) "DS_CALLE", dataCaso2.getDS_CALLE());
          dataSalida.insert( (String) "DS_NMCALLE", dataCaso2.getDS_NMCALLE());
          dataSalida.insert( (String) "DS_PISO_EDOIND",
                            dataCaso2.getDS_PISO_EDOIND());
          dataSalida.insert( (String) "CD_POSTAL_EDOIND",
                            dataCaso2.getCD_POSTAL_EDOIND());
          dataSalida.insert( (String) "CD_OPE_EDOIND",
                            dataCaso2.getCD_OPE_EDOIND());
          dataSalida.insert( (String) "FC_ULTACT_EDOIND",
                            dataCaso2.getFC_ULTACT_EDOIND());

          dataSalida.insert( (String) "CD_ANOOTRC", dataCaso2.getCD_ANOOTRC());
          dataSalida.insert( (String) "NM_ENVOTRC", dataCaso2.getNM_ENVOTRC());
          dataSalida.insert( (String) "CD_ENFCIE", dataCaso2.getCD_ENFCIE());
          // modificacion jlt 15/12/2001
          // recuperamos el proceso
          dataSalida.insert( (String) "DS_PROCESO", proceso);
          dataSalida.insert( (String) "FC_FECNOTIF", dataCaso2.getFC_FECNOTIF());
          dataSalida.insert( (String) "IT_DERIVADO", dataCaso2.getIT_DERIVADO());
          dataSalida.insert( (String) "DS_CENTRODER", dataCaso2.getDS_CENTRODER());
          dataSalida.insert( (String) "IT_DIAGCLI", dataCaso2.getIT_DIAGCLI());
          dataSalida.insert( (String) "FC_INISNT", dataCaso2.getFC_INISNT());
          dataSalida.insert( (String) "DS_COLECTIVO", dataCaso2.getDS_COLECTIVO());
          dataSalida.insert( (String) "IT_ASOCIADO", dataCaso2.getIT_ASOCIADO());
          dataSalida.insert( (String) "IT_DIAGMICRO", dataCaso2.getIT_DIAGMICRO());
          dataSalida.insert( (String) "DS_ASOCIADO", dataCaso2.getDS_ASOCIADO());
          dataSalida.insert( (String) "IT_DIAGSERO", dataCaso2.getIT_DIAGSERO());
          dataSalida.insert( (String) "DS_DIAGOTROS", dataCaso2.getDS_DIAGOTROS());

          // suca
          dataSalida.insert( (String) "CDVIAL", dataCaso2.getCDVIAL());
          dataSalida.insert( (String) "CDTVIA", dataCaso2.getCDTVIA());
          dataSalida.insert( (String) "CDTNUM", dataCaso2.getCDTNUM());
          dataSalida.insert( (String) "DSCALNUM", dataCaso2.getDSCALNUM());

          //notif_edoi (si fueran nulos es que no hay)
          dataSalida.insert( (String) "CD_E_NOTIF", dataNotifZona.getCD_E_NOTIF());
          dataSalida.insert( (String) "CD_ANOEPI_NOTIF_EDOI",
                            dataNotifZona.getCD_ANOEPI_NOTIF_EDOI());
          dataSalida.insert( (String) "CD_SEMEPI_NOTIF_EDOI",
                            dataNotifZona.getCD_SEMEPI_NOTIF_EDOI());
          dataSalida.insert( (String) "FC_RECEP_NOTIF_EDOI",
                            dataNotifZona.getFC_RECEP_NOTIF_EDOI());
          dataSalida.insert( (String) "FC_FECNOTIF_NOTIF_EDOI",
                            dataNotifZona.getFC_FECNOTIF_NOTIF_EDOI());
          dataSalida.insert( (String) "DS_DECLARANTE",
                            dataNotifZona.getDS_DECLARANTE());
          dataSalida.insert( (String) "IT_PRIMERO", dataNotifZona.getIT_PRIMERO());
          dataSalida.insert( (String) "CD_OPE_NOTIF_EDOI",
                            dataNotifZona.getCD_OPE_NOTIF_EDOI());
          dataSalida.insert( (String) "FC_ULTACT_NOTIF_EDOI",
                            dataNotifZona.getFC_ULTACT_NOTIF_EDOI());
          //Zonas
          dataSalida.insert( (String) "CD_NIVEL_1_E_NOTIF",
                            dataNotifZona.getCD_NIVEL_1_E_NOTIF());
          dataSalida.insert( (String) "CD_NIVEL_2_E_NOTIF",
                            dataNotifZona.getCD_NIVEL_2_E_NOTIF());
          dataSalida.insert( (String) "CD_ZBS_E_NOTIF",
                            dataNotifZona.getCD_ZBS_E_NOTIF());
          dataSalida.insert( (String) "CD_CENTRO_E_NOTIF",
                            dataNotifZona.getCD_CENTRO_E_NOTIF());
          dataSalida.insert( (String) "DS_CENTRO_E_NOTIF",
                            dataNotifZona.getDS_CENTRO_E_NOTIF());

          listaSalida.insertElementAt(dataSalida, 0);

          //ARRREGLO DEL RESTO DE A LISTA DE SALIDA //desde el 1-fin
          if (listaNotifZona.size() > 0) {
            for (i = 1; i < listaNotifZona.size(); i++) {
              dataNotifZona = (DataNotifZona) listaNotifZona.elementAt(i);
              listaSalida.removeElementAt(i);
              //en el elemento, debo a�adir los datos de NotifZona
              dataSalida = new DataTab();
              dataSalida.insert( (String) "CD_E_NOTIF",
                                dataNotifZona.getCD_E_NOTIF());
              dataSalida.insert( (String) "CD_ANOEPI_NOTIF_EDOI",
                                dataNotifZona.getCD_ANOEPI_NOTIF_EDOI());
              dataSalida.insert( (String) "CD_SEMEPI_NOTIF_EDOI",
                                dataNotifZona.getCD_SEMEPI_NOTIF_EDOI());
              dataSalida.insert( (String) "FC_RECEP_NOTIF_EDOI",
                                dataNotifZona.getFC_RECEP_NOTIF_EDOI());
              dataSalida.insert( (String) "FC_FECNOTIF_NOTIF_EDOI",
                                dataNotifZona.getFC_FECNOTIF_NOTIF_EDOI());
              dataSalida.insert( (String) "DS_DECLARANTE",
                                dataNotifZona.getDS_DECLARANTE());
              dataSalida.insert( (String) "IT_PRIMERO",
                                dataNotifZona.getIT_PRIMERO());
              dataSalida.insert( (String) "CD_OPE_NOTIF_EDOI",
                                dataNotifZona.getCD_OPE_NOTIF_EDOI());
              dataSalida.insert( (String) "FC_ULTACT_NOTIF_EDOI",
                                dataNotifZona.getFC_ULTACT_NOTIF_EDOI());
              //Zonas
              dataSalida.insert( (String) "CD_NIVEL_1_E_NOTIF",
                                dataNotifZona.getCD_NIVEL_1_E_NOTIF());
              dataSalida.insert( (String) "CD_NIVEL_2_E_NOTIF",
                                dataNotifZona.getCD_NIVEL_2_E_NOTIF());
              dataSalida.insert( (String) "CD_ZBS_E_NOTIF",
                                dataNotifZona.getCD_ZBS_E_NOTIF());
              dataSalida.insert( (String) "CD_CENTRO_E_NOTIF",
                                dataNotifZona.getCD_CENTRO_E_NOTIF());
              dataSalida.insert( (String) "DS_CENTRO_E_NOTIF",
                                dataNotifZona.getDS_CENTRO_E_NOTIF());

              listaSalida.insertElementAt(dataSalida, i);
            } //for
          } //if

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;

    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;
  } //fin doWork

  public String Pregunta_Select_Edoind() {

    return ("select "
            + " CD_ANOEPI,  CD_SEMEPI, " //3
            + " CD_ANOURG, NM_ENVIOURGSEM, CD_CLASIFDIAG, " //4
            + " IT_PENVURG, CD_ENFERMO, CD_PROV,CD_MUN, " //4
            + " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, FC_RECEP, DS_CALLE, " //5
            + " DS_NMCALLE, DS_PISO, CD_POSTAL, CD_OPE, FC_ULTACT, " //5
            + " CD_ANOOTRC, NM_ENVOTRC, CD_ENFCIE,  " //3
            + " FC_FECNOTIF,  IT_DERIVADO, DS_CENTRODER, " // 3+
            + " IT_DIAGCLI,  FC_INISNT, DS_COLECTIVO, " // 3+
            + " IT_ASOCIADO,  IT_DIAGMICRO, DS_ASOCIADO, "
            + " IT_DIAGSERO, DS_DIAGOTROS, CDVIAL, CDTVIA, CDTNUM, DSCALNUM "
            + " from SIVE_EDOIND where NM_EDO = ? ");
  }

  public String Pregunta_Select_Municipio() {
    return ("select DS_MUN "
            + " FROM SIVE_MUNICIPIO WHERE CD_PROV = ? AND CD_MUN = ? ");
  }

  public String Pregunta_Select_Municipio_SUCA() {
    return ("select DSMUNI "
            + " FROM SUCA_MUNICIPIO WHERE CDPROV = ? AND CDMUNI = ? ");
  }

  public String Pregunta_Select_Nivel1() {
    return ("select "
            + " DS_NIVEL_1, DSL_NIVEL_1 "
            + " FROM  SIVE_NIVEL1_S  WHERE CD_NIVEL_1 = ? ");
  }

  public String Pregunta_Select_Nivel2() {
    return ("select "
            + " DS_NIVEL_2, DSL_NIVEL_2 "
            + " FROM  SIVE_NIVEL2_S  WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ?");
  }

  public String Pregunta_Select_ZonaBasica() {
    return ("select "
            + " DS_ZBS, DSL_ZBS "
            + " FROM  SIVE_ZONA_BASICA"
            + "  WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ? AND CD_ZBS = ? ");

  }

  /*
   public String Pregunta_Select_Calle(){
      return ("select "
          + " a.DS_VIALITE, b.DS_VIAL  "
          + " FROM  SIVE_CALLE a, SIVE_TVIAL b "
          + "  WHERE a.CD_PROV = ? AND a.CD_MUN = ? "
          + " AND a.CD_VIA = ? AND a.CD_PARTVIA = b.CD_VIAL");
   }
   */
  public String Pregunta_Select_Enfermo() {
    //enfermo: no selecciono el ds_telef2 ??
    return ("select "
            + " CD_PROV, CD_PAIS, CD_MUN, CD_TDOC, "
            + " DS_APE1,DS_APE2,DS_NOMBRE , DS_FONOAPE1, DS_FONOAPE2, "
            + " DS_FONONOMBRE, IT_CALC , FC_NAC, CD_SEXO, CD_POSTAL, "
            + " DS_DIREC,DS_NUM,DS_PISO , DS_TELEF, CD_NIVEL_1, CD_NIVEL_2, "
            + " CD_ZBS, CD_OPE, FC_ULTACT,IT_REVISADO, CD_MOTBAJA, FC_BAJA, "
        /*+ " DS_OBSERV, CD_PROV2, CD_MUNI2, CD_POST2, DS_DIREC2, DS_NUM2,  "
        + " DS_PISO2, DS_OBSERV2,CD_PROV3,CD_MUNI3, CD_POST3, DS_DIREC3, "
                  + " DS_NUM3, DS_PISO3, DS_TELEF3, DS_OBSERV3,*/
        + " DS_NDOC,SIGLAS , CDVIAL, CDTVIA, CDTNUM, DSCALNUM"
        + " FROM SIVE_ENFERMO WHERE CD_ENFERMO = ? ");

  }

  public String Pregunta_Select_Notif_Edoi() {
    return ("select "
            + " CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI,"
            + " FC_RECEP, DS_DECLARANTE, IT_PRIMERO, CD_OPE, "
            + " FC_ULTACT, FC_FECNOTIF "
            + " from SIVE_NOTIF_EDOI "
            + " where NM_EDO = ? and CD_FUENTE = 'E' order by FC_FECNOTIF");
  }

  public String Pregunta_Select_Equipo_Notificador() {
    return ("select "
            + " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, CD_CENTRO "
            + " from SIVE_E_NOTIF "
            + " where CD_E_NOTIF = ?");

  }
} //fin de la clase
