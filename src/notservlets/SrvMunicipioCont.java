package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import notdata.DataMunicipioEDO;
import sapp.DBServlet;

public class SrvMunicipioCont
    extends DBServlet {

  // modos de operaci�n del servlet
  final int servletSELECCION_MUNICIPIO_X_CODIGO = 9;
  final int servletOBTENER_MUNICIPIO_X_CODIGO = 10;
  final int servletSELECCION_MUNICIPIO_X_DESCRIPCION = 11;
  final int servletOBTENER_MUNICIPIO_X_DESCRIPCION = 12;

  // par�metros
  DataMunicipioEDO dataMunLinea = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Configuraci�n
    String sQuery = "", sFiltro = "";

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int iValor = 1;

    // objetos de datos
    CLista data = new CLista();
    DataMunicipioEDO dataMun = null;
    DataMunicipioEDO entMun = null;

    String cd_prov = "";
    String cd_mun = "";
    String cd_nivel_1 = "";
    String cd_nivel_2 = "";
    String cd_zbs = "";

    String des = "";
    String desL = "";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {
// System_out.println("@@@notservlets->SrvMunicipioCont...");

      for (int t = 0; t < param.size(); t++) {

        dataMun = (DataMunicipioEDO) param.elementAt(t);

        cd_prov = dataMun.getCodProv();
        cd_nivel_1 = dataMun.getCodNivel1();
        cd_nivel_2 = dataMun.getCodNivel2();
        cd_zbs = dataMun.getCodZBS();
        cd_mun = dataMun.getCodMun();

// System_out.println("@@@notservlets->SrvMunicipioCont; CD_PROV:" + cd_prov);
        //des de provincia y ca
        if (!cd_prov.equals("")) {
          sQuery = " SELECT CD_CA, DS_PROV, DSL_PROV "
              + " FROM SIVE_PROVINCIA a "
              + " WHERE CD_PROV = ? ";

          st = con.prepareStatement(sQuery);
          st.setString(1, cd_prov);
          rs = st.executeQuery();

          while (rs.next()) {
            des = rs.getString("DS_PROV");
            desL = rs.getString("DSL_PROV");

            //provincia
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                && (desL != null)) {
              dataMun.sDesProv = desL;
            }
            else {
              dataMun.sDesProv = des;

              //ca
            }
            dataMun.sCodCa = rs.getString("CD_CA");

          }
          rs.close();
          rs = null;
          st.close();
          st = null;
        } //FIN DE IF PROV - CA

        //nivel1, nivel2 y zbs de un municipio
        if (!cd_mun.equals("")) {
          sQuery = " SELECT CD_NIVEL_1, CD_NIVEL_2, CD_ZBS "
              + " FROM SIVE_MUNICIPIO a "
              + " WHERE CD_MUN = ? "
              + "       AND CD_PROV = ? "; //DSR: filtro por provincia

          st = con.prepareStatement(sQuery);
          st.setString(1, cd_mun);
          st.setString(2, cd_prov); //DSR: filtro por provincia
// System_out.println("@@@notservlets->SrvMunicipioCont; cd_mun=" + cd_mun);
// System_out.println("@@@notservlets->SrvMunicipioCont; cd_prov=" + cd_prov);
          rs = st.executeQuery();

          while (rs.next()) {
            cd_nivel_1 = rs.getString("CD_NIVEL_1");
            cd_nivel_2 = rs.getString("CD_NIVEL_2");
            cd_zbs = rs.getString("CD_ZBS");

            //impide nulls
            if (cd_nivel_1 == null) {
              cd_nivel_1 = "";
            }
            if (cd_nivel_2 == null) {
              cd_nivel_2 = "";
            }
            if (cd_zbs == null) {
              cd_zbs = "";

            }
            dataMun.sCodNivel1 = cd_nivel_1;
            dataMun.sCodNivel2 = cd_nivel_2;
            dataMun.sCodZBS = cd_zbs;

          }
          rs.close();
          rs = null;
          st.close();
          st = null;
        } //FIN DE IF MUN

        //desc de nivel 1
        if (!cd_nivel_1.equals("")) {
          sQuery = "SELECT DS_NIVEL_1, DSL_NIVEL_1 "
              + "FROM SIVE_NIVEL1_S "
              + "WHERE CD_NIVEL_1 = ? ";

          st = con.prepareStatement(sQuery);
          st.setString(1, cd_nivel_1);
          rs = st.executeQuery();

          while (rs.next()) {
            des = rs.getString("DS_NIVEL_1");
            desL = rs.getString("DSL_NIVEL_1");
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                && (desL != null)) {
              dataMun.sDesNivel1 = desL;
            }
            else {
              dataMun.sDesNivel1 = des;
            }
          }
          rs.close();
          rs = null;
          st.close();
          st = null;
        } //FIN DE NIVEL 1

        //desc de nivel 2
        if ( (!cd_nivel_1.equals("")) && (!cd_nivel_2.equals(""))) {
          sQuery = "SELECT DS_NIVEL_2, DSL_NIVEL_2 "
              + "FROM SIVE_NIVEL2_S "
              + "WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ?";

          st = con.prepareStatement(sQuery);
          st.setString(1, cd_nivel_1);
          st.setString(2, cd_nivel_2);
          rs = st.executeQuery();

          while (rs.next()) {
            des = rs.getString("DS_NIVEL_2");
            desL = rs.getString("DSL_NIVEL_2");
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                && (desL != null)) {
              dataMun.sDesNivel2 = desL;
            }
            else {
              dataMun.sDesNivel2 = des;

            }
          }
          rs.close();
          rs = null;
          st.close();
          st = null;
        } //FIN DE NIVEL 2

        //desc de ZONA BASICA
        if ( (!cd_nivel_1.equals("")) && (!cd_nivel_2.equals(""))
            && (!cd_zbs.equals(""))) {

          sQuery = "SELECT DS_ZBS, DSL_ZBS "
              + "FROM SIVE_ZONA_BASICA "
              + "WHERE CD_NIVEL_1 = ? AND CD_NIVEL_2 = ? "
              + "AND CD_ZBS = ?";

          st = con.prepareStatement(sQuery);
          st.setString(1, cd_nivel_1);
          st.setString(2, cd_nivel_2);
          st.setString(3, cd_zbs);

          rs = st.executeQuery();

          while (rs.next()) {
            des = rs.getString("DS_ZBS");
            desL = rs.getString("DSL_ZBS");
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                && (desL != null)) {
              dataMun.sDesZBS = desL;
            }
            else {
              dataMun.sDesZBS = des;

            }
          }
          rs.close();
          rs = null;
          st.close();
          st = null;
        } //FIN DE zona basica

      } //fin del for

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      data = null;
      throw ex;

    }

    // cierra la conexi�n y acaba el procedimiento doWork
    closeConnection(con);
    param.trimToSize();

    return param;
  }

}