package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

// LORTAD
import Registro.RegistroConsultas;
import capp.CLista;
import notdata.DataBorrarIndiv;
import notdata.DataClave;
import sapp.DBServlet;

//Entrada: DataBorrarIndiv de donde uso el caso, enfermo, operador
public class SrvIndivB
    extends DBServlet {

  final int servletBORRAR_INDIVIDUALIZADA = 0;

  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  public java.sql.Timestamp DeString_ATimeStamp(String sFecha) {

    String sFecha_larga = sFecha;
    //partirla!!
    int sdd = (new Integer(sFecha_larga.substring(0, 2))).intValue();
    int smm = (new Integer(sFecha_larga.substring(3, 5))).intValue();
    int syyyy = (new Integer(sFecha_larga.substring(6, 10))).intValue();
    int shh = (new Integer(sFecha_larga.substring(11, 13))).intValue();
    int smi = (new Integer(sFecha_larga.substring(14, 16))).intValue();
    int sss = (new Integer(sFecha_larga.substring(17, 19))).intValue();

    java.sql.Timestamp TSFecha = new java.sql.Timestamp(syyyy - 1900,
        smm - 1,
        sdd, shh, smi, sss, 0);
    return (TSFecha);

  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 0;

    // objetos de datos
    DataBorrarIndiv dataEntrada = null;

    CLista listaAlmacen = new CLista();
    DataClave dataAlmacen = null;
    CLista listaBorrare = new CLista();
    DataClave dataBorrare = null;

    DataBorrarIndiv dataSalida = null; //NO SIRVE PARA NADA
    CLista listaSalida = new CLista(); //NO SIRVE PARA NADA

    //Datos de Entrada
    String sCaso = "";
    String CodEnf = "";
    String sOpe = "";

    //querys
    String sQuery = "";

    //fechas
    java.util.Date dFecha;
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat Format_Horas = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
        new Locale("es", "ES"));

    //fecha actual-----------------------------------------------------------
    //para devolver (sFecha_Actual), para insertar (st.setTimestamp(9, TSFec))
    java.util.Date dFecha_Actual = new java.util.Date();
    String sFecha_Actual = Format_Horas.format(dFecha_Actual); //string
    //partirla!!
    int sdd = (new Integer(sFecha_Actual.substring(0, 2))).intValue();
    int smm = (new Integer(sFecha_Actual.substring(3, 5))).intValue();
    int syyyy = (new Integer(sFecha_Actual.substring(6, 10))).intValue();
    int shh = (new Integer(sFecha_Actual.substring(11, 13))).intValue();
    int smi = (new Integer(sFecha_Actual.substring(14, 16))).intValue();
    int sss = (new Integer(sFecha_Actual.substring(17, 19))).intValue();

    java.sql.Timestamp TSFec = new java.sql.Timestamp(syyyy - 1900, smm - 1,
        sdd, shh, smi, sss, 0);
    //st.setTimestamp(9, TSFec);
    // --------------------------------------------------------------------

    //para pasar
    String cd_anoepi = null;
    String cd_semepi = null;
    String cd_anourg = null;

    String nm_enviourgsem = null;
    String cd_clasifdiag = null;
    String it_penvurg = null;
    String cd_enfermo = null;
    String cd_prov = null;
    String cd_mun = null;
    String cd_nivel_1 = null;
    String cd_nivel_2 = null;
    String cd_zbs = null;
    String fc_recep = null;
    String ds_calle = null;
    String ds_nmcalle = null;
    String ds_piso = null;
    String cd_postal = null;
    String cd_ope = null;
    String fc_ultact = null;
    String cd_anootrc = null;
    String nm_envotrc = null;
    String cd_enfcie = null;
    String fc_fecnotif = null;
    String it_derivado = null;
    String ds_centroder = null;
    String it_diagcli = null;
    String fc_inisnt = null;
    String FC_INISNT = "";
    String ds_colectivo = null;
    String it_asociado = null;
    String it_diagmicro = null;
    String ds_asociado = null;
    //String cd_via=   null;
    //String cd_vial= null;
    String it_diagsero = null;
    String ds_diagotros = null;
    String nm_edo = null;
    // suca
    String cdvial = null;
    String cdtvia = null;
    String cdtnum = null;
    String dscalnum = null;

    /****************** APLICACION DE LORTAD ********************************/
    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;
          /****************** APLICACION DE LORTAD ********************************/

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);
    con.setAutoCommit(false);

    try {

      switch (opmode) {

        case servletBORRAR_INDIVIDUALIZADA:

          //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          //INDIVIDUALIZADA
          //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          //borro resp_edo,
          //�queda suelto enfermo?
          //�queda suelto notifedo?
          //borro notif_edoi
          ///////////////////////////////

          //capturo datos****************************
// System_out.println("@@@notservlets->SrvIndivB Traza 1...");
          dataEntrada = (DataBorrarIndiv) param.firstElement();
          sCaso = dataEntrada.getNM_EDO().trim();
          CodEnf = dataEntrada.getCD_ENFERMO().trim();
          sOpe = dataEntrada.getCD_OPE().trim();

          //borrar registros de resp_edo para ese nm_edo
          sQuery = Borrar_resp_edo();

          st = con.prepareStatement(sQuery);
          st.setInt(1, (new Integer(sCaso.trim())).intValue());

          st.executeUpdate();
          st.close();
          st = null;

// System_out.println("@@@notservlets->SrvIndivB Traza 2...");
          //�queda suelto el enfermo, sin caso?:
          //Select sobre edoind si hay otro caso para ese enfermo
          sQuery = Select_enfermo_edoind();
          boolean Existe_elenfermo_paraotro_caso = false;

          st = con.prepareStatement(sQuery);
          st.setInt(1, (new Integer(CodEnf.trim())).intValue());
          st.setInt(2, (new Integer(sCaso.trim())).intValue());

          registroConsultas.insertarParametro(CodEnf.trim());
          registroConsultas.insertarParametro(sCaso.trim());

          registroConsultas.registrar("SIVE_EDOIND",
                                      sQuery,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvIndivB",
                                      true);

          rs = st.executeQuery();
          while (rs.next()) {
            Existe_elenfermo_paraotro_caso = true;
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

// System_out.println("@@@notservlets->SrvIndivB Traza 3...");
          //Rpto a Notifedo!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          /*�existe otro registro en notif_edoi con igual equipo, a�o,
                 semana, fec recep, fec notif?
                 �existe otro registro en edonum con igual equipo, a�o,
                 semana, fec recep, fec notif?*/

          /*Busco las claves que voy a borrar en notif_edoi*/
          sQuery = Select_Claves_notif_edoi();
          st = con.prepareStatement(sQuery);
          st.setInt(1, (new Integer(sCaso.trim())).intValue());

          rs = st.executeQuery();
          while (rs.next()) {

            dataAlmacen = new DataClave(
                rs.getString("CD_E_NOTIF"),
                rs.getString("CD_ANOEPI"),
                rs.getString("CD_SEMEPI"),
                formater.format(rs.getDate("FC_RECEP")),
                formater.format(rs.getDate("FC_FECNOTIF")));

            listaAlmacen.addElement(dataAlmacen);

          } //fin del while

          rs.close();
          rs = null;
          st.close();
          st = null;

// System_out.println("@@@notservlets->SrvIndivB Traza 4...");
          //para cada clave, recorro notif_edoi y edonum
          boolean ExisteUNO = false;
          for (i = 0; i < listaAlmacen.size(); i++) {
            dataAlmacen = (DataClave) listaAlmacen.elementAt(i);

            sQuery = "SELECT NM_EDO FROM SIVE_NOTIF_EDOI"
                + " WHERE (CD_E_NOTIF = ? AND CD_ANOEPI = ? "
                + " AND CD_SEMEPI = ? AND FC_RECEP = ? "
                + " AND FC_FECNOTIF = ? AND NM_EDO <> ? AND CD_FUENTE = 'E' )";

            st = con.prepareStatement(sQuery);
            st.setString(1, dataAlmacen.getCD_E_NOTIF());
            st.setString(2, dataAlmacen.getCD_ANOEPI());
            st.setString(3, dataAlmacen.getCD_SEMEPI());
            dFecha = formater.parse(dataAlmacen.getFC_RECEP().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(4, sqlFec);
            dFecha = formater.parse(dataAlmacen.getFC_FECNOTIF().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(5, sqlFec);
            st.setInt(6, (new Integer(sCaso.trim())).intValue());

            rs = st.executeQuery();
            while (rs.next()) {
              ExisteUNO = true;
              break;
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

// System_out.println("@@@notservlets->SrvIndivB Traza 5...");
            if (!ExisteUNO) {

              sQuery = "SELECT CD_ENFCIE FROM SIVE_EDONUM"
                  + " WHERE (CD_E_NOTIF = ? AND CD_ANOEPI = ? "
                  + " AND CD_SEMEPI = ? AND FC_RECEP = ? "
                  + " AND FC_FECNOTIF = ? )";

              st = con.prepareStatement(sQuery);
              st.setString(1, dataAlmacen.getCD_E_NOTIF());
              st.setString(2, dataAlmacen.getCD_ANOEPI());
              st.setString(3, dataAlmacen.getCD_SEMEPI());
              dFecha = formater.parse(dataAlmacen.getFC_RECEP().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(4, sqlFec);
              dFecha = formater.parse(dataAlmacen.getFC_FECNOTIF().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(5, sqlFec);

              rs = st.executeQuery();
              while (rs.next()) {
                ExisteUNO = true;
                break;
              }
              rs.close();
              rs = null;
              st.close();
              st = null;
            } //fin if

// System_out.println("@@@notservlets->SrvIndivB Traza 6...");
            //para esta clave, no existe enlace, ptt la borrare
            if (!ExisteUNO) {

              dataBorrare = new DataClave(
                  dataAlmacen.getCD_E_NOTIF(),
                  dataAlmacen.getCD_ANOEPI(),
                  dataAlmacen.getCD_SEMEPI(),
                  dataAlmacen.getFC_RECEP().trim(),
                  dataAlmacen.getFC_FECNOTIF().trim());

              listaBorrare.addElement(dataBorrare);
            }

            ExisteUNO = false; //reseteo para la siguiente clave
          } //fin for

          //borrar registros de notif_edoi para ese nm_edo
          sQuery = Borrar_notif_edoi();

          st = con.prepareStatement(sQuery);
          st.setInt(1, (new Integer(sCaso.trim())).intValue());

          st.executeUpdate();
          st.close();
          st = null;

// System_out.println("@@@notservlets->SrvIndivB Traza 7...");
          //si notifedo queda sin Indiv o numericas colgando,
          //borrare varios notifedo modificando primero SUs notif_sem
          //selecciono notifedo.nm_nnotifr, modifico notif_sem, borro notifedo
          String nm_nnotifr = "";
          String nmTotal = "";
          String it_ressem = "";
          String nmTeoricos = "";
          for (i = 0; i < listaBorrare.size(); i++) {

            dataBorrare = (DataClave) listaBorrare.elementAt(i);

            //Selecciono el nm
            sQuery = "SELECT NM_NNOTIFR, IT_RESSEM FROM SIVE_NOTIFEDO"
                + " WHERE (CD_E_NOTIF = ? AND CD_ANOEPI = ? "
                + " AND CD_SEMEPI = ? AND FC_RECEP = ? "
                + " AND FC_FECNOTIF = ? )";

            st = con.prepareStatement(sQuery);
            st.setString(1, dataBorrare.getCD_E_NOTIF());
            st.setString(2, dataBorrare.getCD_ANOEPI());
            st.setString(3, dataBorrare.getCD_SEMEPI());
            dFecha = formater.parse(dataBorrare.getFC_RECEP().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(4, sqlFec);
            dFecha = formater.parse(dataBorrare.getFC_FECNOTIF().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(5, sqlFec);

            rs = st.executeQuery();

            while (rs.next()) {
              nm_nnotifr = rs.getString("NM_NNOTIFR"); // si en null ira a null
              it_ressem = rs.getString("IT_RESSEM");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

// System_out.println("@@@notservlets->SrvIndivB Traza 8...");
            sQuery = "select NM_NTOTREAL, NM_NNOTIFT FROM SIVE_NOTIF_SEM  " +
                " WHERE (CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ?)";

            st = con.prepareStatement(sQuery);

            st.setString(1, dataBorrare.getCD_E_NOTIF());
            st.setString(2, dataBorrare.getCD_ANOEPI());
            st.setString(3, dataBorrare.getCD_SEMEPI());

            rs = st.executeQuery();

            while (rs.next()) {
              nmTeoricos = rs.getString("NM_NNOTIFT"); // si en null ira a null, no puede ser null
              nmTotal = rs.getString("NM_NTOTREAL"); // si en null ira a null, no puede ser null
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

// System_out.println("@@@notservlets->SrvIndivB Traza 9...");
            //procede cobertura, y resumen semanal
            if (nm_nnotifr != null && nmTotal != null &&
                nmTeoricos != null && it_ressem.equals("S")) {

              sQuery = "UPDATE SIVE_NOTIF_SEM SET " +
                  " NM_NTOTREAL = ?, CD_OPE = ?, FC_ULTACT = ?" +
                  " WHERE (CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ?)";

              st = con.prepareStatement(sQuery);

              int inm = (new Integer(nmTotal)).intValue();
              int iQuery = inm - (new Integer(nm_nnotifr)).intValue();
              st.setInt(1, iQuery);
              st.setString(2, sOpe);
              //st.setDate(3, new java.sql.Date( (new java.util.Date()).getTime()));
              st.setTimestamp(3, TSFec);
              st.setString(4, dataBorrare.getCD_E_NOTIF());
              st.setString(5, dataBorrare.getCD_ANOEPI());
              st.setString(6, dataBorrare.getCD_SEMEPI());
              st.executeUpdate();
              st.close();
              st = null;

            }

// System_out.println("@@@notservlets->SrvIndivB Traza 10...");
            //borro en notifedo:
            sQuery = "DELETE FROM SIVE_NOTIFEDO "
                + " WHERE (CD_E_NOTIF = ? AND CD_ANOEPI = ? "
                + " AND CD_SEMEPI = ? AND FC_RECEP = ? "
                + " AND FC_FECNOTIF = ? )";

            st = con.prepareStatement(sQuery);
            st.setString(1, dataBorrare.getCD_E_NOTIF());
            st.setString(2, dataBorrare.getCD_ANOEPI());
            st.setString(3, dataBorrare.getCD_SEMEPI());
            dFecha = formater.parse(dataBorrare.getFC_RECEP().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(4, sqlFec);
            dFecha = formater.parse(dataBorrare.getFC_FECNOTIF().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(5, sqlFec);

            st.executeUpdate();
            st.close();
            st = null;

            //si no procede cobertura, borro notif_sem que ya se creara
            //solo si se queda suelta la notif_sem
            //preguntar a Enrique ???

          } //fin del for de lista para borrar

// System_out.println("@@@notservlets->SrvIndivB Traza 11...");
          //paso a bajasedoind
          //selecciono el caso de edoind
          sQuery = Pasar_Bajas();
          st = con.prepareStatement(sQuery);
          st.setInt(1, (new Integer(sCaso.trim())).intValue());

          registroConsultas.insertarParametro(sCaso.trim());
          registroConsultas.registrar("SIVE_EDOIND",
                                      sQuery,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvIndivB",
                                      true);

          rs = st.executeQuery();

          java.util.Date dFC_INISNT;
          java.sql.Timestamp fecRecu = null;
          String sfecRecu = "";
          // extrae el registro encontrado
          while (rs.next()) {

            //campos criticos:-----------------------------------
            dFC_INISNT = rs.getDate("FC_INISNT");
            if (dFC_INISNT == null) {
              FC_INISNT = null;
            }
            else {
              FC_INISNT = formater.format(dFC_INISNT);

            }
            fecRecu = rs.getTimestamp("FC_ULTACT");
            sfecRecu = timestamp_a_cadena(fecRecu);

            //tenemos los registros
            cd_anoepi = rs.getString("CD_ANOEPI");
            cd_semepi = rs.getString("CD_SEMEPI");
            cd_anourg = rs.getString("CD_ANOURG");

            nm_enviourgsem = rs.getString("NM_ENVIOURGSEM");
            cd_clasifdiag = rs.getString("CD_CLASIFDIAG");
            it_penvurg = rs.getString("IT_PENVURG");
            cd_enfermo = rs.getString("CD_ENFERMO");
            cd_prov = rs.getString("CD_PROV");
            cd_mun = rs.getString("CD_MUN");
            cd_nivel_1 = rs.getString("CD_NIVEL_1");
            cd_nivel_2 = rs.getString("CD_NIVEL_2");
            cd_zbs = rs.getString("CD_ZBS");
            fc_recep = formater.format(rs.getDate("FC_RECEP"));
            ds_calle = rs.getString("DS_CALLE");
            ds_nmcalle = rs.getString("DS_NMCALLE");
            ds_piso = rs.getString("DS_PISO");
            cd_postal = rs.getString("CD_POSTAL");
            cd_ope = rs.getString("CD_OPE");
            fc_ultact = sfecRecu;
            cd_anootrc = rs.getString("CD_ANOOTRC");
            nm_envotrc = rs.getString("NM_ENVOTRC");
            cd_enfcie = rs.getString("CD_ENFCIE");
            fc_fecnotif = formater.format(rs.getDate("FC_FECNOTIF"));
            it_derivado = rs.getString("IT_DERIVADO");
            ds_centroder = rs.getString("DS_CENTRODER");
            it_diagcli = rs.getString("IT_DIAGCLI");
            fc_inisnt = FC_INISNT;
            ds_colectivo = rs.getString("DS_COLECTIVO");
            it_asociado = rs.getString("IT_ASOCIADO");
            it_diagmicro = rs.getString("IT_DIAGMICRO");
            ds_asociado = rs.getString("DS_ASOCIADO");
            //cd_via=   rs.getString ("CD_VIA");
            //cd_vial= rs.getString ("CD_VIAL");
            it_diagsero = rs.getString("IT_DIAGSERO");
            ds_diagotros = rs.getString("DS_DIAGOTROS");
            // suca
            cdvial = rs.getString("CDVIAL");
            cdtvia = rs.getString("CDTVIA");
            cdtnum = rs.getString("CDTNUM");
            dscalnum = rs.getString("DSCALNUM");

            nm_edo = sCaso.trim();

          } //fin del while

          rs.close();
          rs = null;
          st.close();
          st = null;
// System_out.println("@@@notservlets->SrvIndivB Traza 12...");

          //lanzo la query de Bajas
          //Preparamos el insert
          sQuery = Insert_Bajas(); //NM_EDO al final
          st = con.prepareStatement(sQuery);

          st.setString(1, cd_anoepi);

          st.setString(2, cd_semepi);

          if (cd_anourg == null) {
            st.setNull(3, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(3, cd_anourg);

          }

          if (nm_enviourgsem == null) {
            st.setNull(4, java.sql.Types.VARCHAR);
          }
          else {
            st.setInt(4, (new Integer(nm_enviourgsem)).intValue());

          }

          if (cd_clasifdiag == null) {
            st.setNull(5, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(5, cd_clasifdiag);

          }
          if (it_penvurg == null) {
            st.setNull(6, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(6, it_penvurg);

          }
          st.setInt(7, (new Integer(cd_enfermo)).intValue()); //devuelve nulo si lo fuera

          if (cd_prov == null) {
            st.setNull(8, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(8, cd_prov);

          }
          if (cd_mun == null) {
            st.setNull(9, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(9, cd_mun);

          }
          if (cd_nivel_1 == null) {
            st.setNull(10, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(10, cd_nivel_1);

          }
          if (cd_nivel_2 == null) {
            st.setNull(11, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(11, cd_nivel_2);

          }

          if (cd_zbs == null) {
            st.setNull(12, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(12, cd_zbs);

          }
          dFecha = formater.parse(fc_recep.trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(13, sqlFec);

          if (ds_calle == null) {
            st.setNull(14, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(14, ds_calle);

          }
          if (ds_nmcalle == null) {
            st.setNull(15, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(15, ds_nmcalle);

          }
          if (ds_piso == null) {
            st.setNull(16, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(16, ds_piso);
          }
          if (cd_postal == null) {
            st.setNull(17, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(17, cd_postal);

          }

          st.setString(18, cd_ope);

          /*dFecha = formater.parse(fc_ultact.trim());
                 sqlFec = new java.sql.Date(dFecha.getTime());
                 st.setDate(20, sqlFec);*/
          st.setTimestamp(19, DeString_ATimeStamp(fc_ultact));

          if (cd_anootrc == null) {
            st.setNull(20, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(20, cd_anootrc);

          }
          if (nm_envotrc == null) {
            st.setNull(21, java.sql.Types.VARCHAR);
          }
          else {
            st.setInt(21, (new Integer(nm_envotrc)).intValue());

          }

          st.setString(22, cd_enfcie);

          dFecha = formater.parse(fc_fecnotif.trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(23, sqlFec);

          if (it_derivado == null) {
            st.setNull(24, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(24, it_derivado);

          }
          if (ds_centroder == null) {
            st.setNull(25, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(25, ds_centroder);

          }
          if (it_diagcli == null) {
            st.setNull(26, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(26, it_diagcli);

          }
          if (FC_INISNT == null) {
            st.setNull(27, java.sql.Types.VARCHAR);
          }
          else {
            dFecha = formater.parse(FC_INISNT);
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(27, sqlFec);
          }

          if (ds_colectivo == null) {
            st.setNull(28, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(28, ds_colectivo);

          }
          if (it_asociado == null) {
            st.setNull(29, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(29, it_asociado);

          }
          if (it_diagmicro == null) {
            st.setNull(30, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(30, it_diagmicro);

          }
          if (ds_asociado == null) {
            st.setNull(31, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(31, ds_asociado);

            //CD_VIA Y CD_VIAL DE MOMENTO LOS INRITRODUCIMOS A NULL.
            //NOTA: CD_VIA ES CARCHAR DE 8 Y EN BAJAS DE 5 !!!
            /* suca
                   if (cd_via == null)
              st.setNull(33, java.sql.Types.VARCHAR);
                   else
               st.setNull(33, java.sql.Types.VARCHAR);
              //st.setString(33, cd_via);
                   if (cd_vial == null)
              st.setNull(34, java.sql.Types.VARCHAR);
                   else
              st.setNull(34, java.sql.Types.VARCHAR);
              //st.setString(34, cd_vial);
             */
          }
          if (it_diagsero == null) {
            st.setNull(32, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(32, it_diagsero);

          }
          if (ds_diagotros == null) {
            st.setNull(33, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(33, ds_diagotros);

            // suca
          }
          if (cdvial == null) {
            st.setNull(34, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(34, cdvial);

          }
          if (cdtvia == null) {
            st.setNull(35, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(35, cdtvia);

          }
          if (cdtnum == null) {
            st.setNull(36, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(36, cdtnum);

          }
          if (dscalnum == null) {
            st.setNull(37, java.sql.Types.VARCHAR);
          }
          else {
            st.setString(37, dscalnum);

          }
          st.setInt(38, (new Integer(sCaso.trim())).intValue());
          st.executeUpdate();
          st.close();
          st = null;
// System_out.println("@@@notservlets->SrvIndivB Traza 13...");

          //borrar EDOIND para ese nm_edo
          sQuery = "DELETE FROM SIVE_EDOIND WHERE NM_EDO = ?";

          st = con.prepareStatement(sQuery);
          st.setInt(1, (new Integer(sCaso.trim())).intValue());

          st.executeUpdate();
          st.close();
          st = null;
// System_out.println("@@@notservlets->SrvIndivB Traza 14...");

          //borro el enfermo si procede
          if (!Existe_elenfermo_paraotro_caso) {
            sQuery = Borrar_Mov_Enfermo();
            st = con.prepareStatement(sQuery);
            st.setInt(1, (new Integer(CodEnf.trim())).intValue());

            st.executeUpdate();
            st.close();
            st = null;
// System_out.println("@@@notservlets->SrvIndivB Traza 15...");

            sQuery = Borrar_Enfermo();
            st = con.prepareStatement(sQuery);
            st.setInt(1, (new Integer(CodEnf.trim())).intValue());

            st.executeUpdate();
            st.close();
            st = null;

// System_out.println("@@@notservlets->SrvIndivB Traza 16...");

          }

          //LISTA DE SALIDA QUE NO SIRVE PARA NADA---
          //cargar la lista de vuelta (con Cod_Enfermo, Nm_edo)
          dataSalida = new DataBorrarIndiv(sCaso, CodEnf, sOpe);
          listaSalida.addElement(dataSalida);

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;

    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;
  } //fin doWork

  public String Borrar_notif_edoi() {

    return ("delete from SIVE_NOTIF_EDOI "
            + "where NM_EDO = ? and CD_FUENTE = 'E' ");

  }

  public String Borrar_resp_edo() {

    return ("delete from SIVE_RESP_EDO "
            + "where NM_EDO = ?");
  }

  public String Select_enfermo() {

    return ("select CD_ENFERMO from SIVE_EDOIND "
            + "where NM_EDO = ?");
  }

  public String Borrar_Enfermo() {
    return ("delete from SIVE_ENFERMO "
            + "where CD_ENFERMO = ?");
  }

  public String Borrar_Mov_Enfermo() {
    return ("delete from SIVE_MOV_ENFERMO "
            +
            "where CD_ENFERMO = ? and IT_ENFERMO = 'S' and IT_CONTACTO = 'N' ");
  }

  public String Select_enfermo_edoind() {

    return ("select NM_EDO from SIVE_EDOIND "
            + "where CD_ENFERMO = ? and NM_EDO <> ?");
  }

  public String Select_Claves_notif_edoi() {
    return ("select CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, FC_RECEP, "
            + " FC_FECNOTIF from SIVE_NOTIF_EDOI "
            + " where NM_EDO = ? and CD_FUENTE = 'E' ");
  }

  public String Pasar_Bajas() {
    return ("select "
            + " CD_ANOEPI,  CD_SEMEPI, " //3
            + " CD_ANOURG, NM_ENVIOURGSEM, CD_CLASIFDIAG, " //4
            + " IT_PENVURG, CD_ENFERMO, CD_PROV,CD_MUN, " //4
            + " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, FC_RECEP, DS_CALLE, " //5
            + " DS_NMCALLE, DS_PISO, CD_POSTAL, CD_OPE, FC_ULTACT, " //5
            + " CD_ANOOTRC, NM_ENVOTRC, CD_ENFCIE,  " //3
            + " FC_FECNOTIF,  IT_DERIVADO, DS_CENTRODER, " // 3+
            + " IT_DIAGCLI,  FC_INISNT, DS_COLECTIVO, " // 3+
            + " IT_ASOCIADO,  IT_DIAGMICRO, DS_ASOCIADO," // suca CD_VIA, CD_VIAL, "
            + " IT_DIAGSERO, DS_DIAGOTROS, CDVIAL, CDTVIA, CDTNUM, DSCALNUM "
            + " from SIVE_EDOIND where NM_EDO = ? ");

  }

  public String Insert_Bajas() {
    return ("insert into SIVE_BAJASEDOIND "
            + "( CD_ANOEPI,  CD_SEMEPI, " //3
            + " CD_ANOURG, NM_ENVIOURGSEM, CD_CLASIFDIAG, " //4
            + " IT_PENVURG, CD_ENFERMO, CD_PROV,CD_MUN, " //4
            + " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, FC_RECEP, DS_CALLE, " //5
            + " DS_NMCALLE, DS_PISO, CD_POSTAL, CD_OPE, FC_ULTACT, " //5
            + " CD_ANOOTRC, NM_ENVOTRC, CD_ENFCIE,  " //3
            + " FC_FECNOTIF,  IT_DERIVADO, DS_CENTRODER, " // 3+
            + " IT_DIAGCLI,  FC_INISNT, DS_COLECTIVO, " // 3+
            + " IT_ASOCIADO,  IT_DIAGMICRO, DS_ASOCIADO, " //  3 CD_VIA, CD_VIAL, "
            +
        " IT_DIAGSERO, DS_DIAGOTROS , CDVIAL, CDTVIA, CDTNUM, DSCALNUM, NM_EDO  )" // 7
            + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, "
            + " ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
  }

} //fin de la clase
