package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import capp.CLista;
import notdata.DataListaNotifEDO;
import sapp.DBServlet;

public class SrvNotifEDO
    extends DBServlet {

  // modos de operaci�n del servlet
  final int servletSELECCION_LISTA_NOTIF = 3;
  final int servletSELECCION_LISTA_NOTIF_FECHA = 7;
  final int servletSELECCION_DATOS_NOTIF_SEM = 16;
  final int servletSELECCIONVALIDAR = 17;
  final int servletSELECCION_NOTIF_SEM_Y_LISTA_NOTIF = 18;

  public SimpleDateFormat Format_Horas = new SimpleDateFormat(
      "dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));
  public java.sql.Timestamp fecRecu = null;
  public String sfecRecu = "";
  public java.sql.Timestamp fecRecu_notifedo = null;
  public String sfecRecu_notifedo = "";
  public java.sql.Timestamp fecRecu_notif_sem = null;
  public String sfecRecu_notif_sem = "";

  // objetos de datos
  protected CLista data = null;
  DataListaNotifEDO dataNotifEDO = null; //entrada
  DataListaNotifEDO datalistaEDO = null; //salida

  // par�metros
  String sQuery = "", sFechaRecep = "", sFechaNotif = "", sFechaValidacion = "";
  String sfc_ultact_notifedo = "";
  String sfc_ultact_notif_sem = "";

  java.util.Date dFecha;
  java.sql.Date sqlFec;
  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

  // objetos JDBC
  Connection con = null;
  PreparedStatement st = null;
  ResultSet rs = null;

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Configuraci�n
    String sQuery = "";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    data = new CLista();

    switch (opmode) {

      case servletSELECCION_NOTIF_SEM_Y_LISTA_NOTIF:

        dataNotifEDO = new DataListaNotifEDO();
        dataNotifEDO = (DataListaNotifEDO) param.firstElement();
        sQuery = "select " +
            " NM_NNOTIFT, NM_NTOTREAL, " +
            " CD_OPE, FC_ULTACT " +
            " from SIVE_NOTIF_SEM b" +
            " where (CD_E_NOTIF = ? and CD_ANOEPI = ? " +
            " and CD_SEMEPI = ?)";

        st = con.prepareStatement(sQuery);
        st.setString(1, dataNotifEDO.getCodEquipo().trim());
        st.setString(2, dataNotifEDO.getAnno().trim());
        st.setString(3, dataNotifEDO.getSemana().trim());

        rs = st.executeQuery();
        fecRecu = null;
        sfecRecu = "";
        while (rs.next()) {
          fecRecu = rs.getTimestamp("FC_ULTACT");
          sfecRecu = timestamp_a_cadena(fecRecu);

          DataListaNotifEDO datalistaEDO =
              new DataListaNotifEDO("", "", "", "",
                                    rs.getString("NM_NNOTIFT"),
                                    rs.getString("NM_NTOTREAL"),
                                    "", "", "",
                                    rs.getString("CD_OPE"),
                                    sfecRecu,
                                    "", "", "", "", "", "");

          data.addElement(datalistaEDO);
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        // ahora notifEDO
        sQuery = "select a.IT_RESSEM, a.FC_FECNOTIF, a.FC_RECEP," +
            " a.NM_NNOTIFR, a.CD_OPE, a.FC_ULTACT, " +
            " b.NM_NNOTIFT, b.NM_NTOTREAL, " +
            " b.CD_OPE, b.FC_ULTACT, a.IT_VALIDADA, a.FC_FECVALID " +
            " from SIVE_NOTIFEDO a, SIVE_NOTIF_SEM b" +
            " where (a.CD_E_NOTIF= b.CD_E_NOTIF" +
            " and a.CD_ANOEPI = b.CD_ANOEPI" +
            " and a.CD_SEMEPI = b.CD_SEMEPI" +
            " and a.CD_E_NOTIF=? and a.CD_ANOEPI=? " +
            " and a.CD_SEMEPI=?)";

        st = con.prepareStatement(sQuery);
        st.setString(1, dataNotifEDO.getCodEquipo().trim());
        st.setString(2, dataNotifEDO.getAnno().trim());
        st.setString(3, dataNotifEDO.getSemana().trim());

        rs = st.executeQuery();

        fecRecu_notifedo = null;
        sfecRecu_notifedo = "";
        fecRecu_notif_sem = null;
        sfecRecu_notif_sem = "";
        while (rs.next()) {
          sFechaValidacion = "";
          //Estas fechas nunca pueden ser nulas
          //Los numeros se recuperan con getString para que den null, en caso de serlo

          sqlFec = rs.getDate("FC_FECNOTIF");
          sFechaNotif = formater.format(sqlFec);

          sqlFec = rs.getDate("FC_RECEP");
          sFechaRecep = formater.format(sqlFec);

          sqlFec = rs.getDate("FC_FECVALID");
          if (sqlFec != null) {
            sFechaValidacion = formater.format(sqlFec);

          }
          fecRecu_notifedo = rs.getTimestamp(6);
          sfecRecu_notifedo = timestamp_a_cadena(fecRecu_notifedo);
          fecRecu_notif_sem = rs.getTimestamp(10);
          sfecRecu_notif_sem = timestamp_a_cadena(fecRecu_notif_sem);

          datalistaEDO =
              new DataListaNotifEDO(rs.getString("IT_RESSEM"),
                                    sFechaNotif,
                                    sFechaRecep,
                                    rs.getString("NM_NNOTIFR"),
                                    rs.getString("NM_NNOTIFT"),
                                    rs.getString("NM_NTOTREAL"),
                                    dataNotifEDO.getCodEquipo().trim(),
                                    dataNotifEDO.getAnno().trim(),
                                    dataNotifEDO.getSemana().trim(),
                                    rs.getString(9),
                                    sfecRecu_notif_sem,
                                    rs.getString(5),
                                    sfecRecu_notifedo,
                                    rs.getString("IT_VALIDADA"),
                                    sFechaValidacion, "", "");

          data.addElement(datalistaEDO);
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

      case servletSELECCION_DATOS_NOTIF_SEM:
        dataNotifEDO = new DataListaNotifEDO();
        dataNotifEDO = (DataListaNotifEDO) param.firstElement();

        sQuery = "select " +
            " NM_NNOTIFT, NM_NTOTREAL, " +
            " CD_OPE, FC_ULTACT " +
            " from SIVE_NOTIF_SEM b" +
            " where (CD_E_NOTIF = ? and CD_ANOEPI = ? " +
            " and CD_SEMEPI = ?)";

        st = con.prepareStatement(sQuery);
        st.setString(1, dataNotifEDO.getCodEquipo().trim());
        st.setString(2, dataNotifEDO.getAnno().trim());
        st.setString(3, dataNotifEDO.getSemana().trim());

        rs = st.executeQuery();
        fecRecu = null;
        sfecRecu = "";
        if (rs.next()) {
          fecRecu = rs.getTimestamp("FC_ULTACT");
          sfecRecu = timestamp_a_cadena(fecRecu);

          DataListaNotifEDO datalistaEDO =
              new DataListaNotifEDO("", "", "", "",
                                    rs.getString("NM_NNOTIFT"), // si es null va a null
                                    rs.getString("NM_NTOTREAL"),
                                    "", "", "",
                                    rs.getString("CD_OPE"),
                                    sfecRecu,
                                    "", "", "", "", "", "");

          data.addElement(datalistaEDO);
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        // si no existe la cabecera para la semana (es antigua), se genera
        if (data.size() == 0) {
          // calcula los notificadores te�ricos para el equipo
          sQuery = "select NM_NOTIFT " +
              "from SIVE_E_NOTIF where CD_E_NOTIF=?";
          st = con.prepareStatement(sQuery);
          st.setString(1, dataNotifEDO.getCodEquipo().trim());

          rs = st.executeQuery();

          String snm_notift = "";
          if (rs.next()) {
            snm_notift = rs.getString("NM_NOTIFT"); // si nulo lo coge como null
          }
          if (snm_notift == null) {
            snm_notift = "";

          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //fecha actual en formato timestamp
          java.util.Date dFecha_Actual = new java.util.Date();
          String sFecha_Actual = Format_Horas.format(dFecha_Actual); //string
          //partirla!!
          int sdd = (new Integer(sFecha_Actual.substring(0, 2))).intValue();
          int smm = (new Integer(sFecha_Actual.substring(3, 5))).intValue();
          int syyyy = (new Integer(sFecha_Actual.substring(6, 10))).intValue();
          int shh = (new Integer(sFecha_Actual.substring(11, 13))).intValue();
          int smi = (new Integer(sFecha_Actual.substring(14, 16))).intValue();
          int sss = (new Integer(sFecha_Actual.substring(17, 19))).intValue();

          java.sql.Timestamp TSFec = new java.sql.Timestamp(syyyy - 1900,
              smm - 1,
              sdd, shh, smi, sss, 0);
          // inserta la cabecera de la semana
          sQuery = "insert into SIVE_NOTIF_SEM " +
              "(CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, " +
              " NM_NNOTIFT, NM_NTOTREAL, CD_OPE, FC_ULTACT) " +
              "values (?, ?, ?, ?, ?, ?, ?)";

          st = con.prepareStatement(sQuery);
          st.setString(1, dataNotifEDO.getCodEquipo().trim());
          st.setString(2, dataNotifEDO.getAnno().trim());
          st.setString(3, dataNotifEDO.getSemana().trim());

          if (snm_notift.trim().length() > 0) {
            st.setInt(4, new Integer(snm_notift.trim()).intValue());
          }
          else { //no debe darse y ptt no lo metera nulo, sino cero
            st.setInt(4, new Integer(0).intValue());

          }
          st.setInt(5, new Integer(0).intValue());
          st.setString(6, param.getLogin());
          st.setTimestamp(7, TSFec);

          st.executeUpdate();
          st.close();
          st = null;

          if (snm_notift.trim().length() == 0) {
            snm_notift = "0";
          }
          String snm_real = "0";

          // devuelve la cabecera insertada
          DataListaNotifEDO datalistaEDO =
              new DataListaNotifEDO("", "", "", "",
                                    snm_notift,
                                    snm_real,
                                    "", "", "",
                                    param.getLogin(),
                                    sFecha_Actual,
                                    "", "", "", "", "", "");

          data.addElement(datalistaEDO);

        }

        break;

      case servletSELECCIONVALIDAR:
        dataNotifEDO = new DataListaNotifEDO();
        dataNotifEDO = (DataListaNotifEDO) param.firstElement();

        if (!dataNotifEDO.getCodEquipo().trim().equals("")) {
          sQuery = " select a.IT_RESSEM, a.FC_FECNOTIF, a.FC_RECEP," +
              " a.NM_NNOTIFR, a.CD_OPE, a.FC_ULTACT, " +
              " b.NM_NNOTIFT, b.NM_NTOTREAL, " +
              " b.CD_OPE, b.FC_ULTACT, " +
              " a.IT_VALIDADA, a.FC_FECVALID, " +
              " a.CD_SEMEPI, a.CD_E_NOTIF " +
              " from SIVE_NOTIFEDO a, SIVE_NOTIF_SEM b" +
              " where (a.CD_E_NOTIF= b.CD_E_NOTIF" +
              " and a.CD_ANOEPI = b.CD_ANOEPI" +
              " and a.CD_SEMEPI = b.CD_SEMEPI" +
              " and a.CD_E_NOTIF=? and a.CD_ANOEPI=? " +
              //" and a.CD_SEMEPI=? and a.FC_FECNOTIF = ? "+
              " and a.CD_SEMEPI=? " +
              " and a.IT_VALIDADA = ? )";

          st = con.prepareStatement(sQuery);
          st.setString(1, dataNotifEDO.getCodEquipo().trim());
          st.setString(2, dataNotifEDO.getAnno().trim());
          st.setString(3, dataNotifEDO.getSemana().trim());

          /* // fecha de notificaci�n
           dFecha = formater.parse(dataNotifEDO.getFechaNotif().trim());
           sqlFec = new java.sql.Date(dFecha.getTime());
           st.setDate(4, sqlFec);
           ///  */

          st.setString(4, "N");

        }
        else {
          sQuery = " select a.IT_RESSEM,  a.FC_FECNOTIF, a.FC_RECEP, " +
              " a.NM_NNOTIFR, a.CD_OPE, a.FC_ULTACT, " +
              " b.NM_NNOTIFT, b.NM_NTOTREAL, " +
              " b.CD_OPE, b.FC_ULTACT, " +
              " a.IT_VALIDADA, a.FC_FECVALID,  " +
              " a.CD_SEMEPI, a.CD_E_NOTIF " +
              " from SIVE_NOTIFEDO a, SIVE_NOTIF_SEM b " +
              " where (a.CD_E_NOTIF= b.CD_E_NOTIF " +
              " and a.CD_ANOEPI = b.CD_ANOEPI  " +
              " and a.CD_SEMEPI = b.CD_SEMEPI  " +
              " and a.CD_ANOEPI = ? ";

          if (!dataNotifEDO.getSemana().trim().equals("")) {
            sQuery = sQuery + " and a.CD_SEMEPI=? ";
            /* solo por semana...aunque este informada la fecha notif
              if (!dataNotifEDO.getFechaNotif().trim().equals(""))
                sQuery = sQuery +" and a.FC_FECNOTIF = ?  "; */

          }
          sQuery = sQuery + " and a.IT_VALIDADA =? " +
              " and a.CD_E_NOTIF in (SELECT CD_E_NOTIF from SIVE_E_NOTIF WHERE CD_NIVEL_1 = ? ";

          if (!dataNotifEDO.getCD_NIVEL_2().trim().equals("")) {
            sQuery = sQuery + " AND CD_NIVEL_2 = ?  ";

          }
          sQuery = sQuery + " )   ) ";

          int i = 1;

          st = con.prepareStatement(sQuery);
          st.setString(i, dataNotifEDO.getAnno().trim());
          i = i + 1;
          if (!dataNotifEDO.getSemana().trim().equals("")) {
            st.setString(i, dataNotifEDO.getSemana().trim());
            i = i + 1;
          }
          /*if (!dataNotifEDO.getFechaNotif().trim().equals("")){
             // fecha de notificaci�n
             dFecha = formater.parse(dataNotifEDO.getFechaNotif().trim());
             sqlFec = new java.sql.Date(dFecha.getTime());
             st.setDate(i, sqlFec);
             ///
              i = i+1;
           }*/
          st.setString(i, "N");
          i = i + 1;
          st.setString(i, dataNotifEDO.getCD_NIVEL_1().trim());
          i = i + 1;
          if (!dataNotifEDO.getCD_NIVEL_2().trim().equals("")) {
            st.setString(i, dataNotifEDO.getCD_NIVEL_2().trim());

          }
        }

        //continuamos
        rs = st.executeQuery();

        sFechaValidacion = "";
        fecRecu_notifedo = null;
        sfecRecu_notifedo = "";
        fecRecu_notif_sem = null;
        sfecRecu_notif_sem = "";
        while (rs.next()) {
          //Estas fechas nunca pueden ser nulas
          //Los numeros se recuperan con getString para que den null, en caso de serlo

          sqlFec = rs.getDate("FC_FECNOTIF");
          sFechaNotif = formater.format(sqlFec);

          sqlFec = rs.getDate("FC_RECEP");
          sFechaRecep = formater.format(sqlFec);

          sqlFec = rs.getDate("FC_FECVALID");
          if (sqlFec != null) {
            sFechaValidacion = formater.format(sqlFec);

          }
          fecRecu_notifedo = rs.getTimestamp(6);
          sfecRecu_notifedo = timestamp_a_cadena(fecRecu_notifedo);
          fecRecu_notif_sem = rs.getTimestamp(10);
          sfecRecu_notif_sem = timestamp_a_cadena(fecRecu_notif_sem);

          DataListaNotifEDO datalistaEDO =
              new DataListaNotifEDO(rs.getString("IT_RESSEM"),
                                    sFechaNotif,
                                    sFechaRecep,
                                    rs.getString("NM_NNOTIFR"),
                                    rs.getString("NM_NNOTIFT"),
                                    rs.getString("NM_NTOTREAL"),
                                    rs.getString("CD_E_NOTIF"),
                                    dataNotifEDO.getAnno().trim(),
                                    rs.getString("CD_SEMEPI"),
                                    rs.getString(9),
                                    sfecRecu_notif_sem,
                                    rs.getString(5),
                                    sfecRecu_notifedo,
                                    rs.getString("IT_VALIDADA"),
                                    sFechaValidacion, "", "");
          data.addElement(datalistaEDO);
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

      case servletSELECCION_LISTA_NOTIF:

        dataNotifEDO = new DataListaNotifEDO();
        dataNotifEDO = (DataListaNotifEDO) param.firstElement();

        sQuery = "select a.IT_RESSEM, a.FC_FECNOTIF, a.FC_RECEP," +
            " a.NM_NNOTIFR, a.CD_OPE, a.FC_ULTACT, " +
            " b.NM_NNOTIFT, b.NM_NTOTREAL, " +
            " b.CD_OPE, b.FC_ULTACT, a.IT_VALIDADA, a.FC_FECVALID " +
            " from SIVE_NOTIFEDO a, SIVE_NOTIF_SEM b" +
            " where (a.CD_E_NOTIF= b.CD_E_NOTIF" +
            " and a.CD_ANOEPI = b.CD_ANOEPI" +
            " and a.CD_SEMEPI = b.CD_SEMEPI" +
            " and a.CD_E_NOTIF=? and a.CD_ANOEPI=? " +
            " and a.CD_SEMEPI=?)";

        st = con.prepareStatement(sQuery);
        st.setString(1, dataNotifEDO.getCodEquipo().trim());
        st.setString(2, dataNotifEDO.getAnno().trim());
        st.setString(3, dataNotifEDO.getSemana().trim());

        rs = st.executeQuery();

        fecRecu_notifedo = null;
        sfecRecu_notifedo = "";
        fecRecu_notif_sem = null;
        sfecRecu_notif_sem = "";
        while (rs.next()) {
          sFechaValidacion = "";
          //Estas fechas nunca pueden ser nulas
          //Los numeros se recuperan con getString para que den null, en caso de serlo

          sqlFec = rs.getDate("FC_FECNOTIF");
          sFechaNotif = formater.format(sqlFec);

          sqlFec = rs.getDate("FC_RECEP");
          sFechaRecep = formater.format(sqlFec);

          sqlFec = rs.getDate("FC_FECVALID");
          if (sqlFec != null) {
            sFechaValidacion = formater.format(sqlFec);

            /*sqlFec = rs.getDate(6);
                       sfc_ultact_notifedo = formater.format(sqlFec);
                       sqlFec = rs.getDate(10);
                       sfc_ultact_notif_sem = formater.format(sqlFec);*/
          }
          fecRecu_notifedo = rs.getTimestamp(6);
          sfecRecu_notifedo = timestamp_a_cadena(fecRecu_notifedo);
          fecRecu_notif_sem = rs.getTimestamp(10);
          sfecRecu_notif_sem = timestamp_a_cadena(fecRecu_notif_sem);

          DataListaNotifEDO datalistaEDO =
              new DataListaNotifEDO(rs.getString("IT_RESSEM"),
                                    sFechaNotif,
                                    sFechaRecep,
                                    rs.getString("NM_NNOTIFR"),
                                    rs.getString("NM_NNOTIFT"),
                                    rs.getString("NM_NTOTREAL"),
                                    dataNotifEDO.getCodEquipo().trim(),
                                    dataNotifEDO.getAnno().trim(),
                                    dataNotifEDO.getSemana().trim(),
                                    rs.getString(9),
                                    sfecRecu_notif_sem,
                                    rs.getString(5),
                                    sfecRecu_notifedo,
                                    rs.getString("IT_VALIDADA"),
                                    sFechaValidacion, "", "");

          data.addElement(datalistaEDO);
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

      case servletSELECCION_LISTA_NOTIF_FECHA:

        dataNotifEDO = new DataListaNotifEDO();
        dataNotifEDO = (DataListaNotifEDO) param.firstElement();

        sQuery = " select a.IT_RESSEM, a.FC_RECEP," +
            " a.NM_NNOTIFR, a.CD_OPE, a.FC_ULTACT, " +
            " b.NM_NNOTIFT, b.NM_NTOTREAL, " +
            " b.CD_OPE, b.FC_ULTACT, " +
            " a.IT_VALIDADA, a.FC_FECVALID " +
            " from SIVE_NOTIFEDO a, SIVE_NOTIF_SEM b" +
            " where (a.CD_E_NOTIF= b.CD_E_NOTIF" +
            " and a.CD_ANOEPI = b.CD_ANOEPI" +
            " and a.CD_SEMEPI = b.CD_SEMEPI" +
            " and a.CD_E_NOTIF=? and a.CD_ANOEPI=? " +
            " and a.CD_SEMEPI=? and a.FC_FECNOTIF = ?)";

        st = con.prepareStatement(sQuery);

        st.setString(1, dataNotifEDO.getCodEquipo().trim());
        st.setString(2, dataNotifEDO.getAnno().trim());
        st.setString(3, dataNotifEDO.getSemana().trim());
        // fecha de notificaci�n
        dFecha = formater.parse(dataNotifEDO.getFechaNotif().trim());
        sqlFec = new java.sql.Date(dFecha.getTime());
        st.setDate(4, sqlFec);

        rs = st.executeQuery();

        fecRecu_notifedo = null;
        sfecRecu_notifedo = "";
        fecRecu_notif_sem = null;
        sfecRecu_notif_sem = "";
        while (rs.next()) {
          sFechaValidacion = "";
          //Estas fechas nunca pueden ser nulas
          //Los numeros se recuperan con getString para que den null, en caso de serlo

          sqlFec = rs.getDate("FC_RECEP");
          sFechaRecep = formater.format(sqlFec);

          sqlFec = rs.getDate("FC_FECVALID");
          if (sqlFec != null) {
            sFechaValidacion = formater.format(sqlFec);

            /*sqlFec = rs.getDate(5);
                       sfc_ultact_notifedo = formater.format(sqlFec);
                       sqlFec = rs.getDate(9);
                       sfc_ultact_notif_sem = formater.format(sqlFec); */
          }
          fecRecu_notifedo = rs.getTimestamp(5);
          sfecRecu_notifedo = timestamp_a_cadena(fecRecu_notifedo);
          fecRecu_notif_sem = rs.getTimestamp(9);
          sfecRecu_notif_sem = timestamp_a_cadena(fecRecu_notif_sem);

          DataListaNotifEDO datalistaEDO =
              new DataListaNotifEDO(rs.getString("IT_RESSEM"),
                                    dataNotifEDO.getFechaNotif().trim(),
                                    sFechaRecep,
                                    rs.getString("NM_NNOTIFR"),
                                    rs.getString("NM_NNOTIFT"),
                                    rs.getString("NM_NTOTREAL"),
                                    dataNotifEDO.getCodEquipo().trim(),
                                    dataNotifEDO.getAnno().trim(),
                                    dataNotifEDO.getSemana().trim(),
                                    rs.getString(8),
                                    sfecRecu_notif_sem,
                                    rs.getString(4),
                                    sfecRecu_notifedo,
                                    rs.getString("IT_VALIDADA"),
                                    sFechaValidacion, "", "");

          data.addElement(datalistaEDO);

        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    data.trimToSize();
    return data;
  }

}
