package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import capp.CApp;
import capp.CLista;
import notdata.DataEntradaEDO;
import notdata.DataListaEDONum;
import notdata.DataNotifEDO;
import sapp.DBServlet;

public class SrvEDONum
    extends DBServlet {

  // modos de operaci�n del servlet
  final int servletSELECCION_LISTA_EDO_NUM = 3;
  final int servletSELECCION_LISTA_EDO_NUM_FECHA = 8;

  // objetos de datos
  protected CLista data;
  DataEntradaEDO dataEDO = null;
  DataNotifEDO dataNotif = null;
  DataListaEDONum dataNum = null;
  DataListaEDONum dataLinea = null;

  // par�metros
  String sQuery = "";
  java.util.Date dFecha;
  java.sql.Date sqlFec;
  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

  // objetos JDBC
  Connection con = null;
  PreparedStatement st = null;
  ResultSet rs = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Configuraci�n
    String sQuery = "";
    int i = 1;
    int iValor = 1;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    data = new CLista();
    dataNum = new DataListaEDONum();
    dataNum = (DataListaEDONum) param.firstElement();

    switch (opmode) {

      case servletSELECCION_LISTA_EDO_NUM:

        sQuery = "select a.CD_ENFCIE, a.NM_CASOS, a.FC_RECEP," +
            " a.FC_FECNOTIF, b.DS_PROCESO, b.DSL_PROCESO" +
            " from SIVE_EDONUM a, SIVE_PROCESOS b" +
            " where (a.CD_ENFCIE = b.CD_ENFCIE " +
            " and a.CD_E_NOTIF=?" +
            " and a.CD_ANOEPI=? and a.CD_SEMEPI=?)";

        st = con.prepareStatement(sQuery);

        // c�digo de equipo
        st.setString(1, dataNum.getCodEquipo().trim());
        st.setString(2, dataNum.getAnno().trim());
        st.setString(3, dataNum.getSemana().trim());

        rs = st.executeQuery();

        while (rs.next()) {

          // recogemos las fechas y las pasamos a string
          String sFRecep = formater.format(rs.getDate("FC_RECEP"));
          String sFNotif = formater.format(rs.getDate("FC_FECNOTIF"));
          String sDes;

          String desPro = rs.getString("DS_PROCESO");
          String desLPro = rs.getString("DSL_PROCESO");

          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
              && (desLPro != null)) {
            sDes = desLPro;
          }
          else {
            sDes = desPro;

            // a�ade un nodo
          }
          DataListaEDONum datosNum = new DataListaEDONum
              (rs.getString("CD_ENFCIE"),
               sDes,
               rs.getString("NM_CASOS"),
               sFRecep,
               "", //vigi
               dataNum.getCodEquipo(),
               dataNum.getAnno(),
               dataNum.getSemana(), //datos entrada
               "", "",
               sFNotif,
               ""); //partes
          data.addElement(datosNum);
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        //continuo con vigi de cada uno
        for (int ind = 0; ind < data.size(); ind++) {
          DataListaEDONum dataregistros = (DataListaEDONum) data.elementAt(ind);
          sQuery = "select CD_TVIGI " +
              " from SIVE_ENFEREDO " +
              " where (CD_ENFCIE = ? )";

          st = con.prepareStatement(sQuery);
          st.setString(1, dataregistros.getCodEnfer().trim());
          rs = st.executeQuery();

          while (rs.next()) {
            dataregistros.sVigi = rs.getString("CD_TVIGI");
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

        } //forvigi

        //continuo con los partes de cada uno
        int CantidadPartes = 0;
        for (int ind = 0; ind < data.size(); ind++) {

          DataListaEDONum dataregistros = (DataListaEDONum) data.elementAt(ind);
          //PARa las TIPO X: partes de verdad
          if (dataregistros.getVigi().equals("X")) {
            sQuery = "select CD_ENFCIE " +
                " from SIVE_EDO_DADIC " +
                " where (CD_ENFCIE = ? " +
                " and CD_E_NOTIF=? " +
                " and CD_ANOEPI=? " +
                " and CD_SEMEPI=? " +
                " and FC_RECEP = ?" +
                " and FC_FECNOTIF = ?)";

            st = con.prepareStatement(sQuery);
            st.setString(1, dataregistros.getCodEnfer().trim());
            st.setString(2, dataNum.getCodEquipo().trim());
            st.setString(3, dataNum.getAnno().trim());
            st.setString(4, dataNum.getSemana().trim());
            // fecha de recepci�n
            dFecha = formater.parse(dataregistros.getFechaRecep().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(5, sqlFec);

            // fecha de notificaci�n
            dFecha = formater.parse(dataregistros.getFechaNotif().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(6, sqlFec);

            rs = st.executeQuery();

            while (rs.next()) {
              CantidadPartes = CantidadPartes + 1;
            }

            Integer I = new Integer(CantidadPartes);
            dataregistros.sPartes = (String) I.toString();
            CantidadPartes = 0;

            rs.close();
            rs = null;
            st.close();
            st = null;

          }
          /* else if (dataregistros.getVigi().equals("A")){
              sQuery = "select a.nm_edo, a.cd_enfcie "+
                     "from sive_edoind a, sive_notif_edoi b "+
                     "where  (a.nm_edo=b.nm_edo  "+
                     " and a.CD_ENFCIE = ? "+
                     " and b.CD_E_NOTIF = ? "+
                     " and b.CD_ANOEPI= ? "+
                     " and b.CD_SEMEPI =? "+
                     " and b.FC_RECEP = ? "+
                     " and b.FC_FECNOTIF = ?)";
             st = con.prepareStatement(sQuery);
             st.setString(1,dataregistros.getCodEnfer().trim());
             st.setString(2,dataNum.getCodEquipo().trim());
             st.setString(3,dataNum.getAnno().trim());
             st.setString(4,dataNum.getSemana().trim());
             // fecha de recepci�n
             dFecha = formater.parse(dataregistros.getFechaRecep().trim());
             sqlFec = new java.sql.Date(dFecha.getTime());
             st.setDate(5, sqlFec);
             // fecha de notificaci�n
             dFecha = formater.parse(dataregistros.getFechaNotif().trim());
             sqlFec = new java.sql.Date(dFecha.getTime());
             st.setDate(6, sqlFec);
             rs = st.executeQuery();
             while (rs.next()) {
               CantidadPartes = CantidadPartes + 1;
             }
             Integer I = new Integer ( CantidadPartes);
             dataregistros.sPartes = (String)I.toString();
             CantidadPartes = 0;
             rs.close();
             rs = null;
             st.close();
             st = null;
           }*/
          else if (dataregistros.getVigi().equals("N")) {
            dataregistros.sPartes = "";
          }

        } //for

        break;

      case servletSELECCION_LISTA_EDO_NUM_FECHA:

        sQuery = "select a.CD_ENFCIE, a.NM_CASOS, b.DS_PROCESO, b.DSL_PROCESO" +
            " from SIVE_EDONUM a, SIVE_PROCESOS b " +
            " where (a.CD_ENFCIE = b.CD_ENFCIE AND a.CD_E_NOTIF = ?" +
            " and a.CD_ANOEPI = ? and a.CD_SEMEPI = ?" +
            " and a.FC_RECEP = ? and a.FC_FECNOTIF=? )";

        st = con.prepareStatement(sQuery);
        st.setString(1, dataNum.getCodEquipo().trim());
        st.setString(2, dataNum.getAnno().trim());
        st.setString(3, dataNum.getSemana().trim());

        // fecha de recepci�n
        dFecha = formater.parse(dataNum.getFechaRecep().trim());
        sqlFec = new java.sql.Date(dFecha.getTime());
        st.setDate(4, sqlFec);

        // fecha de notificaci�n
        dFecha = formater.parse(dataNum.getFechaNotif().trim());
        sqlFec = new java.sql.Date(dFecha.getTime());
        st.setDate(5, sqlFec);

        rs = st.executeQuery();

        while (rs.next()) {
          String sDes;
          String desPro = rs.getString("DS_PROCESO");
          String desLPro = rs.getString("DSL_PROCESO");

          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
              && (desLPro != null)) {
            sDes = desLPro;
          }
          else {
            sDes = desPro;

            // a�ade un nodo
          }
          DataListaEDONum datosNumDia = new DataListaEDONum
              (rs.getString("CD_ENFCIE"),
               sDes,
               rs.getString("NM_CASOS"),
               dataNum.getFechaRecep(),
               "", //vigi
               dataNum.getCodEquipo(),
               dataNum.getAnno(),
               dataNum.getSemana(), //datos entrada
               "", "",
               dataNum.getFechaNotif(),
               ""); //partes

          data.addElement(datosNumDia);
        }

        rs.close();
        rs = null;
        st.close();
        st = null;
        //continuo con vigi de cada uno
        for (int ind = 0; ind < data.size(); ind++) {
          DataListaEDONum dataregistros = (DataListaEDONum) data.elementAt(ind);
          sQuery = "select CD_TVIGI " +
              " from SIVE_ENFEREDO " +
              " where (CD_ENFCIE = ? )";

          st = con.prepareStatement(sQuery);
          st.setString(1, dataregistros.getCodEnfer().trim());
          rs = st.executeQuery();

          while (rs.next()) {
            dataregistros.sVigi = rs.getString("CD_TVIGI");
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

        } //forvigi

        //continuo con los partes de cada uno
        int Cantidad_Partes = 0;
        for (int index = 0; index < data.size(); index++) {
          DataListaEDONum dataregistros = (DataListaEDONum) data.elementAt(
              index);
          sQuery = "select CD_ENFCIE " +
              " from SIVE_EDO_DADIC " +
              " where (CD_ENFCIE = ? " +
              " and CD_E_NOTIF=? " +
              " and CD_ANOEPI=? " +
              " and CD_SEMEPI=? " +
              " and FC_RECEP = ?" +
              " and FC_FECNOTIF = ? )";

          st = con.prepareStatement(sQuery);
          st.setString(1, dataregistros.getCodEnfer().trim());
          st.setString(2, dataNum.getCodEquipo().trim());
          st.setString(3, dataNum.getAnno().trim());
          st.setString(4, dataNum.getSemana().trim());
          // fecha de recepci�n
          dFecha = formater.parse(dataNum.getFechaRecep().trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(5, sqlFec);

          // fecha de notificaci�n
          dFecha = formater.parse(dataNum.getFechaNotif().trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(6, sqlFec);

          rs = st.executeQuery();
          while (rs.next()) {
            Cantidad_Partes = Cantidad_Partes + 1;
          }
          Integer I = new Integer(Cantidad_Partes);
          dataregistros.sPartes = (String) I.toString();
          Cantidad_Partes = 0;
          rs.close();
          rs = null;
          st.close();
          st = null;

        } //for

        break;

    } //fin switch

    // cierra la conexi�n y acaba el procedimiento doWork
    closeConnection(con);

    data.trimToSize();
    return data;
  }
}