/*
 * No apliacamos LORTAD. Esta clase se utiliza para el bloqueo de registros
 * y no para la consulta de los datos.
 */
package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Vector;

import capp.CLista;
import notdata.DataOpeFc;
import sapp.DBServlet;

public class SrvBloqueo
    extends DBServlet {

  // modos de operaci�n del servlet
  final int servletBLOQUEO_SELECT = 0;

  // par�metros
  DataOpeFc dataEntrada = null;
  DataOpeFc dataCampos = null;

  public java.sql.Timestamp DeString_ATimeStamp(String sFecha) {

    String sFecha_larga = sFecha;
    //partirla!!
    int sdd = (new Integer(sFecha_larga.substring(0, 2))).intValue();
    int smm = (new Integer(sFecha_larga.substring(3, 5))).intValue();
    int syyyy = (new Integer(sFecha_larga.substring(6, 10))).intValue();
    int shh = (new Integer(sFecha_larga.substring(11, 13))).intValue();
    int smi = (new Integer(sFecha_larga.substring(14, 16))).intValue();
    int sss = (new Integer(sFecha_larga.substring(17, 19))).intValue();

    java.sql.Timestamp TSFecha = new java.sql.Timestamp(syyyy - 1900,
        smm - 1,
        sdd, shh, smi, sss, 0);
    return (TSFecha);

  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Configuraci�n
    String sQuery = "";

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // objetos de datos
    CLista data = new CLista();
    DataOpeFc dataSalida = null;

    //fechas
    java.util.Date dFecha = new java.util.Date();
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {

      boolean bSalir = false;

      for (int i = 0; i < param.size(); i++) {

        if (!bSalir) {
          dataEntrada = (DataOpeFc) param.elementAt(i);
          sQuery = "SELECT CD_OPE, FC_ULTACT " +
              " FROM " + dataEntrada.getTABLA() +
              " WHERE ";

          Vector vCampos = dataEntrada.getvCAMPOS();

          //componer el where---
          for (int j = 0; j < vCampos.size(); j++) {
            dataCampos = (DataOpeFc) vCampos.elementAt(j);
            if (j == 0) {
              sQuery = sQuery + dataCampos.getCAMPOBD() + " = ? ";
            }
            else {
              sQuery = sQuery + " AND " + dataCampos.getCAMPOBD() + " = ? ";
            }
          } //WHERE-----

          st = con.prepareStatement(sQuery);

          // valores de campos
          for (int k = 0; k < vCampos.size(); k++) {
            dataCampos = (DataOpeFc) vCampos.elementAt(k);

            if (dataCampos.getTIPO().trim().equals("C")) {
              st.setString(k + 1, dataCampos.getVALOR().trim());

            }
            else if (dataCampos.getTIPO().trim().equals("N")) {
              st.setInt(k + 1,
                        new Integer(dataCampos.getVALOR().trim()).intValue());

            }
            else if (dataCampos.getTIPO().trim().equals("F")) {
              dFecha = formater.parse(dataCampos.getVALOR().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(k + 1, sqlFec);
            }
          } //-------------

          rs = st.executeQuery();

          String CD_OPE = "";
          /*String FC_ULTACT= "";
                 java.util.Date dFC_ULTACT;*/
          java.sql.Timestamp fecRecuTabla = null;
          String sfecRecuTabla = "";

          java.sql.Timestamp fecEnviada = null;
          String sfecEnviada = "";

          fecEnviada = DeString_ATimeStamp(dataEntrada.getFC_ULTACT().trim());

          if (rs.next()) {

            /*dFC_ULTACT = rs.getDate("FC_ULTACT");
                     if (dFC_ULTACT == null)  //campo obligatorio, no sera nulo
              FC_ULTACT = "";
                     else
              FC_ULTACT = formater.format(dFC_ULTACT);*/

            fecRecuTabla = rs.getTimestamp("FC_ULTACT");
            CD_OPE = rs.getString("CD_OPE");

            //comparo con los que me llegan en dataentrada
            if (!dataEntrada.getCD_OPE().trim().equals(CD_OPE) ||
                fecEnviada.getTime() != fecRecuTabla.getTime()) {

              dataSalida = new DataOpeFc(dataEntrada.getTABLA(), false, false);
              data.addElement(dataSalida);
              bSalir = true; //salimos del for al detectar un fallo
            }

          }
          else {
            //borraron la tabla!!
            dataSalida = new DataOpeFc(dataEntrada.getTABLA(), false, true);
            data.addElement(dataSalida);
            bSalir = true; //salimos del for al detectar un fallo
          } //fin del if rs.next

          rs.close();
          rs = null;
          st.close();
          st = null;
        } //fin del if
      } //fin del for de cada query

      if (!bSalir) { //false, todo bien, data esta vacio -> "", true
        dataSalida = new DataOpeFc("", true, false);
        data.addElement(dataSalida);
      }
      else { //true, hubo uno al menos mal, data ya lleno
        //nada
      }

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      data = null;
      throw ex;

    }

    // cierra la conexi�n y acaba el procedimiento doWork
    closeConnection(con);
    data.trimToSize();

    return data;
  }

}