package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

// LORTAD
import Registro.RegistroConsultas;
import capp.CLista;
import notdata.DataBorrarMasivo;
import notdata.DataEdoind;
import notdata.DataEnfCaso;
import notdata.DataNumerica;
import sapp.DBServlet;

//Entrada: DataBorrarMasivo
public class SrvBorrarMasivo
    extends DBServlet {

  //@JLT
  public java.util.Date fcrecep = null;
  public java.util.Date fcnotif = null;
  public String sfcrecep = null;
  public String sfcnotif = null;
  public java.sql.Date sqlfcrecep = null;
  public java.sql.Date sqlfcnotif = null;
  public Hashtable hNotifedoi = null;
  public String sEdo = null;
  public String sqlNotifedoi = null;
  public String sqlEdoind = null;
  public String sPrimero = "S";

  final int servletBORRAR = 0;
  public java.sql.Timestamp fecRecu = null;
  public String sfecRecu = "";
  public SimpleDateFormat Format_Horas = new SimpleDateFormat(
      "dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));

  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  //@
  private String stringdate2stringdate(String fecha) {
    String sfech = new String();
    int sanyo = (new Integer(fecha.substring(0, 4))).intValue();
    int smes = (new Integer(fecha.substring(5, 7))).intValue();
    int sdia = (new Integer(fecha.substring(8, 10))).intValue();
    sfech = new Integer(sdia).toString() + '/' + new Integer(smes).toString() +
        '/' + new Integer(sanyo).toString();

    return sfech;
  }

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  public java.sql.Timestamp DeString_ATimeStamp(String sFecha) {

    String sFecha_larga = sFecha;
    //partirla!!
    int sdd = (new Integer(sFecha_larga.substring(0, 2))).intValue();
    int smm = (new Integer(sFecha_larga.substring(3, 5))).intValue();
    int syyyy = (new Integer(sFecha_larga.substring(6, 10))).intValue();
    int shh = (new Integer(sFecha_larga.substring(11, 13))).intValue();
    int smi = (new Integer(sFecha_larga.substring(14, 16))).intValue();
    int sss = (new Integer(sFecha_larga.substring(17, 19))).intValue();

    java.sql.Timestamp TSFecha = new java.sql.Timestamp(syyyy - 1900,
        smm - 1,
        sdd, shh, smi, sss, 0);
    return (TSFecha);

  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 0;

    // objetos de datos
    DataBorrarMasivo dataEntrada = null;

    CLista listaNumericas = new CLista(); //almaceno las numericas que borrare
    DataNumerica dataNumericas = null;

    CLista listaCasos = new CLista(); //almaceno los casos afectados en las Indivs
    DataEnfCaso dataCasos = null;

    CLista listaCasosBorrar = new CLista(); //almaceno los casos a borrar en las Indivs
    DataEdoind dataCasosBorrar = null;

    CLista listaEnfermosNoBorrar = new CLista(); //almaceno los enfermos que no debo borrar
    DataEnfCaso dataEnfermosNoBorrar = null;

    CLista listaEnfermosBorrar = new CLista(); //almaceno los enfermos a borrar en las Indivs
    DataEnfCaso dataEnfermosBorrar = null;

    DataBorrarMasivo dataSalida = null; // NO SIRVE PARA NADA
    CLista listaSalida = new CLista(); // SIRVE PARA SABER C�MO HA
    // TERMINADO LA EJECUCI�N DEL SERVLET

    Vector listaNM_EDO = null; // Variables utilizadas para conocer
    Vector listaCD_E_NOTIF = null; // los permisos del usuario sobre los datos
    // que se pretende borrar.

    int iPerfil = 0;
    int iInterrogacion = 1;
    int iPrepare = 1;
    String sLogin = null;

    //Datos de Entrada
    String cd_e_notif = "";
    String cd_anoepi = "";
    String cd_semepi = "";
    String fc_recep = "";
    String fc_fecnotif = "";
    String nm_ntotreal = "";
    String cd_ope = "";
    String fc_ultact = "";
    String sCobertura = "";
    String it_actualizar_notif_sem = "";

    //fecha actual-----------------------------------------------------------
    //para devolver (sFecha_Actual), para insertar (st.setTimestamp(9, TSFec))
    java.util.Date dFecha_Actual = new java.util.Date();
    String sFecha_Actual = Format_Horas.format(dFecha_Actual); //string
    //partirla!!
    int sdd = (new Integer(sFecha_Actual.substring(0, 2))).intValue();
    int smm = (new Integer(sFecha_Actual.substring(3, 5))).intValue();
    int syyyy = (new Integer(sFecha_Actual.substring(6, 10))).intValue();
    int shh = (new Integer(sFecha_Actual.substring(11, 13))).intValue();
    int smi = (new Integer(sFecha_Actual.substring(14, 16))).intValue();
    int sss = (new Integer(sFecha_Actual.substring(17, 19))).intValue();

    java.sql.Timestamp TSFec = new java.sql.Timestamp(syyyy - 1900, smm - 1,
        sdd, shh, smi, sss, 0);
    //st.setTimestamp(9, TSFec);
    // --------------------------------------------------------------------
    java.sql.Timestamp fecRecu = null;
    String sfecRecu = "";

    //querys
    String sQuery = "";

    //fechas
    java.util.Date dFecha;
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    /****************** APLICACION DE LORTAD ********************************/
    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;
          /****************** APLICACION DE LORTAD ********************************/

      //# // System_out.println("GUI Borrar masivo: antes de conectar");

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    con.setAutoCommit(false);

    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    try {

      switch (opmode) {

        case servletBORRAR:

          /*System.out.println("entro en el case");
                 String a = "2002-10-20";
                 int an = (new Integer(a.substring(0,4))).intValue();
                 int m = (new Integer(a.substring(5,7))).intValue();
                 int d = (new Integer(a.substring(8,10))).intValue();
                 String f = new Integer(d).toString() + '/' + new Integer(m).toString() + '/' + new Integer(an).toString();
                 fcnotif = formater.parse(f);
                 sqlfcnotif = new java.sql.Date(fcnotif.getTime());
                 System.out.println("sqlfcnotif");
                 System.out.println(sqlfcnotif);
           */

          //capturo datos****************************
          dataEntrada = (DataBorrarMasivo) param.firstElement();

          cd_e_notif = dataEntrada.getCD_E_NOTIF().trim();
          cd_anoepi = dataEntrada.getCD_ANOEPI().trim();
          cd_semepi = dataEntrada.getCD_SEMEPI().trim();
          fc_recep = dataEntrada.getFC_RECEP().trim();
          fc_fecnotif = dataEntrada.getFC_FECNOTIF().trim();
          nm_ntotreal = dataEntrada.getNM_NTOTREAL().trim();
          cd_ope = dataEntrada.getCD_OPE().trim();
          fc_ultact = dataEntrada.getFC_ULTACT().trim();
          sCobertura = dataEntrada.getsCOBERTURA().trim();
          it_actualizar_notif_sem = dataEntrada.getIT_ACTUALIZAR_NOTIF_SEM().
              trim();

          iPerfil = param.getPerfil();
          sLogin = param.getLogin();

          // control de permisos para los casos
          // control de autorizaciones para el borrado

          //0: Selecciono seg�n el perfil.
          switch (iPerfil) {
            case 1:
              break;
            case 2:
              break;
            case 3:
            case 4:
            case 5:

              // 1: se obtiene la lista de casos de esa notificaci�n.
              listaNM_EDO = getlistaNM_EDO(con, cd_e_notif, cd_anoepi,
                                           cd_semepi, fc_recep,
                                           fc_fecnotif);

              if (listaNM_EDO.size() != 0) {
                // Si hay algo...
                // 2: se obtiene, para cada caso, el resto de los equipos declarantes,
                //    incluido el de partida.
                listaCD_E_NOTIF = getlistaCD_E_NOTIF(con, listaNM_EDO);

                if (listaCD_E_NOTIF.size() != 0) {
                  // Si hay algo...
                  if (iPerfil == 3) {
                    if (!getPermisoPerfil_3(con, sLogin, listaCD_E_NOTIF)) {
                      listaSalida.addElement("Sin permisos");
                      //throw new Exception("Sin permisos");
                    }
                  }
                  else if (iPerfil == 4) {
                    if (!getPermisoPerfil_4(con, sLogin, listaCD_E_NOTIF)) {
                      listaSalida.addElement("Sin permisos");
                      //throw new Exception("Sin permisos");
                    }
                  }
                  else if (iPerfil == 5) {
                    if (!getPermisoPerfil_5(con, cd_e_notif, listaCD_E_NOTIF)) {
                      listaSalida.addElement("Sin permisos");
                      //throw new Exception("Sin permisos");
                    }
                  }
                }
              }

              break;
          } //switch parcial

          //# // System_out.println("GUI Comienza el borrado");

          if (listaSalida.size() == 0) { // Si hay permisos

            //# // System_out.println("GUI 0");

            if (sCobertura.equals("S")) {
//!!!!!!!!!!!!!!!!!!!!!!
//Actualizo SU notif_sem
//!!!!!!!!!!!!!!!!!!!!!!

              sQuery = "UPDATE SIVE_NOTIF_SEM SET " +
                  " NM_NTOTREAL = ?, CD_OPE = ?, FC_ULTACT = ?" +
                  " WHERE (CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ?)";

              st = con.prepareStatement(sQuery);
              if (nm_ntotreal.equals("")) {
                st.setNull(1, java.sql.Types.VARCHAR);
              }
              else {
                int inm = (new Integer(nm_ntotreal)).intValue();
                st.setInt(1, inm);
              }
              st.setString(2, cd_ope);
              st.setTimestamp(3, TSFec);
              st.setString(4, cd_e_notif);
              st.setString(5, cd_anoepi);
              st.setString(6, cd_semepi);
              st.executeUpdate();
              st.close();
              st = null;

            } //fin de actualizar

//!!!!!!!!!!!!!!!!!!!!!!
//Numericas
            /*El notifedo tiene asociado una serie de edonum
             que borrare */
//!!!!!!!!!!!!!!!!!!!!!!

            sQuery = "select CD_ENFCIE, NM_CASOS, CD_OPE, FC_ULTACT " +
                " FROM SIVE_EDONUM  " +
                " WHERE (CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
                " and  FC_RECEP = ? and FC_FECNOTIF = ?)";

            st = con.prepareStatement(sQuery);

            st.setString(1, cd_e_notif);
            st.setString(2, cd_anoepi);
            st.setString(3, cd_semepi);

            dFecha = formater.parse(fc_recep.trim());
            sqlFec = new java.sql.Date(dFecha.getTime());

            st.setDate(4, sqlFec);
            dFecha = formater.parse(fc_fecnotif.trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(5, sqlFec);

            rs = st.executeQuery();

            fecRecu = null;
            sfecRecu = "";

            while (rs.next()) {
              fecRecu = rs.getTimestamp("FC_ULTACT");
              sfecRecu = timestamp_a_cadena(fecRecu);

              dataNumericas = new DataNumerica("", "", "",
                                               "", "", "", "", "", "", "",
                                               rs.getString("CD_OPE"),
                                               sfecRecu, "", "",
                                               rs.getString("CD_ENFCIE"),
                                               (new Integer(rs.getInt(
                  "NM_CASOS")).toString()),
                                               "", "", "");

              listaNumericas.addElement(dataNumericas);
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

            //# // System_out.println("GUI 1");

            //INSERTO EN EL HISTORICO
            //previamente, necesito el max de la tabla
            sQuery = "SELECT MAX(NM_SECBAJA) "
                + "FROM SIVE_BAJASEDONUM WHERE NM_SECBAJA < ?";

            st = con.prepareStatement(sQuery);
            Integer NumMax = new Integer(999999);
            st.setInt(1, NumMax.intValue());
            rs = st.executeQuery();

            String sSecuencial = "";
            int iSecuencial = 0;

            if (rs.next()) {
              if (rs == null) {
                iSecuencial = 1;
                sSecuencial = "1";
              }
              else {
                iSecuencial = rs.getInt(1) + 1;
                Integer ISecuencial = new Integer(iSecuencial);
                sSecuencial = ISecuencial.toString();
              }
            }

            rs.close();
            rs = null;
            st.close();
            st = null;

            //insertamos
            int iSecEncadaMomento = 0;
            i = 0;
            for (i = 0; i < listaNumericas.size(); i++) {

              dataNumericas = (DataNumerica) listaNumericas.elementAt(i);
              sQuery = "insert into SIVE_BAJASEDONUM ( NM_SECBAJA, CD_ENFCIE," +
                  " CD_E_NOTIF," +
                  " CD_ANOEPI, CD_SEMEPI, FC_RECEP, FC_FECNOTIF," +
                  " NM_CASOS, CD_OPE, FC_ULTACT)" +
                  " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

              st = con.prepareStatement(sQuery);
              iSecEncadaMomento = iSecuencial + i;

              st.setInt(1, iSecEncadaMomento);
              st.setString(2, dataNumericas.getCD_ENFCIE().trim());
              st.setString(3, cd_e_notif);
              st.setString(4, cd_anoepi);
              st.setString(5, cd_semepi);

              dFecha = formater.parse(fc_recep.trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(6, sqlFec);

              dFecha = formater.parse(fc_fecnotif.trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(7, sqlFec);
              st.setInt(8,
                        (new Integer(dataNumericas.getNM_CASOS().trim())).intValue());
              st.setString(9, dataNumericas.getCD_OPE().trim());
              st.setTimestamp(10,
                              DeString_ATimeStamp(dataNumericas.getFC_ULTACT().
                                                  trim()));

              // lanza la query
              st.executeUpdate();
              st.close();
              st = null;

            } //for

            //# // System_out.println("GUI 2");

            //borramos resp_adic y edo_dadic, a partir de listaNumericas
            for (i = 0; i < listaNumericas.size(); i++) {
              dataNumericas = (DataNumerica) listaNumericas.elementAt(i);
              sQuery = "delete from SIVE_RESP_ADIC " +
                  " where CD_ENFCIE = ? and CD_E_NOTIF = ?" +
                  " and CD_ANOEPI = ? and CD_SEMEPI = ?" +
                  " and FC_RECEP = ? and FC_FECNOTIF = ?";

              st = con.prepareStatement(sQuery);
              st.setString(1, dataNumericas.getCD_ENFCIE().trim());
              st.setString(2, cd_e_notif);
              st.setString(3, cd_anoepi);
              st.setString(4, cd_semepi);
              // fecha de recepci�n
              dFecha = formater.parse(fc_recep.trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(5, sqlFec);
              // fecha de notificaci�n
              dFecha = formater.parse(fc_fecnotif.trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(6, sqlFec);

              st.executeUpdate();
              st.close();
            }

            //# // System_out.println("GUI 3");

            for (i = 0; i < listaNumericas.size(); i++) {
              dataNumericas = (DataNumerica) listaNumericas.elementAt(i);
              sQuery = "delete from SIVE_EDO_DADIC " +
                  " where CD_ENFCIE = ? and CD_E_NOTIF = ?" +
                  " and CD_ANOEPI = ? and CD_SEMEPI = ?" +
                  " and FC_RECEP = ? and FC_FECNOTIF = ?";

              st = con.prepareStatement(sQuery);
              st.setString(1, dataNumericas.getCD_ENFCIE().trim());
              st.setString(2, cd_e_notif);
              st.setString(3, cd_anoepi);
              st.setString(4, cd_semepi);
              // fecha de recepci�n
              dFecha = formater.parse(fc_recep.trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(5, sqlFec);
              // fecha de notificaci�n
              dFecha = formater.parse(fc_fecnotif.trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(6, sqlFec);

              st.executeUpdate();
              st.close();
            }
            //# // System_out.println("GUI 4");
            //Borro edonum las de listaNumericas
            for (i = 0; i < listaNumericas.size(); i++) {
              dataNumericas = (DataNumerica) listaNumericas.elementAt(i);

              sQuery = "delete from SIVE_EDONUM " +
                  " where CD_ENFCIE = ? and CD_E_NOTIF = ?" +
                  " and CD_ANOEPI = ? and CD_SEMEPI = ?" +
                  " and FC_RECEP = ? and FC_FECNOTIF = ?";

              st = con.prepareStatement(sQuery);
              st.setString(1, dataNumericas.getCD_ENFCIE().trim());
              st.setString(2, cd_e_notif);
              st.setString(3, cd_anoepi);
              st.setString(4, cd_semepi);
              dFecha = formater.parse(fc_recep.trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(5, sqlFec);
              dFecha = formater.parse(fc_fecnotif.trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(6, sqlFec);

              st.executeUpdate();
              st.close();
              st = null;
            } //for
            //# // System_out.println("GUI 5");
            //--------------------------------------------------------
//!!!!!!!!!!!!!!!!!!!!!!
//Individualizadas
            /*Borrare varias notif_edoi con varios casos asociados*/
            /*Si los casos se quedan colgados, borrar resp_edo y ver enfermo*/
//!!!!!!!!!!!!!!!!!!!!!!

            sQuery = "select NM_EDO " +
                " FROM SIVE_NOTIF_EDOI  " +
                " WHERE (CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ? " +
                " and  FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = 'E' )";

            st = con.prepareStatement(sQuery);

            st.setString(1, cd_e_notif);
            st.setString(2, cd_anoepi);
            st.setString(3, cd_semepi);
            dFecha = formater.parse(fc_recep.trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(4, sqlFec);
            dFecha = formater.parse(fc_fecnotif.trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(5, sqlFec);

            rs = st.executeQuery();

            while (rs.next()) {

              dataCasos = new DataEnfCaso("",
                                          (new Integer(rs.getInt("NM_EDO")).
                                           toString()));

              listaCasos.addElement(dataCasos);
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

            //# // System_out.println("GUI 6");

            //casos colgados?
            boolean ExisteNOTIF_EDOIconigualCaso = false;
            i = 0;
            for (i = 0; i < listaCasos.size(); i++) {
              dataCasos = (DataEnfCaso) listaCasos.elementAt(i);

              //@JLT

              /*        sQuery = "SELECT CD_E_NOTIF,CD_ANOEPI,CD_SEMEPI, FC_RECEP, FC_FECNOTIF "
                              + " FROM SIVE_NOTIF_EDOI "
                              + " WHERE (CD_E_NOTIF <> ? OR CD_ANOEPI <> ? "
                              + " OR CD_SEMEPI <> ? OR FC_RECEP <> ? "
                   + " OR FC_FECNOTIF <> ?) AND NM_EDO = ? AND CD_FUENTE = 'E' ";
               */
              sQuery =
                  "SELECT CD_E_NOTIF,CD_ANOEPI,CD_SEMEPI, FC_RECEP, FC_FECNOTIF, NM_EDO "
                  + " FROM SIVE_NOTIF_EDOI "
                  + " WHERE (CD_E_NOTIF <> ? OR CD_ANOEPI <> ? "
                  + " OR CD_SEMEPI <> ? OR FC_RECEP <> ? "
                  + " OR FC_FECNOTIF <> ?) AND NM_EDO = ? AND CD_FUENTE = 'E' "
                  + " ORDER BY CD_ANOEPI, CD_SEMEPI, FC_FECNOTIF, FC_RECEP";

              st = con.prepareStatement(sQuery);
              st.setString(1, cd_e_notif);
              st.setString(2, cd_anoepi);
              st.setString(3, cd_semepi);
              dFecha = formater.parse(fc_recep.trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(4, sqlFec);
              dFecha = formater.parse(fc_fecnotif.trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(5, sqlFec);
              st.setInt(6, (new Integer(dataCasos.getNM_EDO().trim())).intValue());

              rs = st.executeQuery();
              while (rs.next()) {
                ExisteNOTIF_EDOIconigualCaso = true;

                //@JLT
                // recupero los valores de cd_e_notif, cd_anoepi, cd_semepi
                // fc_fecnotif, fc_recep del registro m�s antiguo, para modificarlo
                hNotifedoi = new Hashtable();
                hNotifedoi.put("CD_E_NOTIF", rs.getString("CD_E_NOTIF"));
                hNotifedoi.put("CD_ANOEPI", rs.getString("CD_ANOEPI"));
                hNotifedoi.put("CD_SEMEPI", rs.getString("CD_SEMEPI"));
                //fcrecep = rs.getDate("FC_RECEP");
                //fcnotif = rs.getDate("FC_FECNOTIF");
                //hNotifedoi.put("FC_RECEP", (String) fcrecep.toString());
                //hNotifedoi.put("FC_FECNOTIF", (String) fcnotif.toString());
                hNotifedoi.put("FC_RECEP",
                               (java.sql.Date) rs.getDate("FC_RECEP"));
                hNotifedoi.put("FC_FECNOTIF",
                               (java.sql.Date) rs.getDate("FC_FECNOTIF"));
                hNotifedoi.put("NM_EDO",
                               (new Integer(rs.getInt("NM_EDO")).toString()));
                ////////////////////////////////

                //
                break;
              }
              rs.close();
              rs = null;
              st.close();
              st = null;

              //# // System_out.println("GUI 7");

              //caso colgado
              if (!ExisteNOTIF_EDOIconigualCaso) {

                //consultar el enfermo, y los datos de edoind para borrar
                sQuery = "select "
                    + " CD_ANOEPI,  CD_SEMEPI, " //3
                    + " CD_ANOURG, NM_ENVIOURGSEM, CD_CLASIFDIAG, " //4
                    + " IT_PENVURG, CD_ENFERMO, CD_PROV,CD_MUN, " //4
                    + " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, FC_RECEP, DS_CALLE, " //5
                    + " DS_NMCALLE, DS_PISO, CD_POSTAL, CD_OPE, FC_ULTACT, " //5
                    + " CD_ANOOTRC, NM_ENVOTRC, CD_ENFCIE,  " //3
                    + " FC_FECNOTIF,  IT_DERIVADO, DS_CENTRODER, " // 3+
                    + " IT_DIAGCLI,  FC_INISNT, DS_COLECTIVO, " // 3+
                    +
                    " IT_ASOCIADO,  IT_DIAGMICRO, DS_ASOCIADO, CDVIAL, CDTVIA, CDTNUM, DSCALNUM, "
                    + " IT_DIAGSERO, DS_DIAGOTROS "
                    + " from SIVE_EDOIND where NM_EDO = ? ";

                st = con.prepareStatement(sQuery);
                st.setInt(1,
                          (new Integer(dataCasos.getNM_EDO().trim())).intValue());

                // ARG: Se a�aden parametros
                registroConsultas.insertarParametro(dataCasos.getNM_EDO().trim());

                rs = st.executeQuery();

                // ARG: Se registra la consulta
                registroConsultas.registrar("SIVE_EDOIND",
                                            sQuery,
                                            "CD_ENFERMO",
                                            "",
                                            "SrvBorrarMasivo",
                                            true);

                java.util.Date dFC_INISNT;
                String FC_INISNT = "";
                fecRecu = null;
                sfecRecu = "";
                while (rs.next()) {

                  dFC_INISNT = rs.getDate("FC_INISNT");
                  if (dFC_INISNT == null) {
                    FC_INISNT = null;
                  }
                  else {
                    FC_INISNT = formater.format(dFC_INISNT);
                  }
                  fecRecu = rs.getTimestamp("FC_ULTACT");
                  sfecRecu = timestamp_a_cadena(fecRecu);

                  dataCasosBorrar = new DataEdoind(
                      dataCasos.getNM_EDO().trim(),
                      rs.getString("CD_ANOEPI"),
                      rs.getString("CD_SEMEPI"),
                      rs.getString("CD_ANOURG"),
                      rs.getString("NM_ENVIOURGSEM"), //devuelve nulo si lo fuera
                      rs.getString("CD_CLASIFDIAG"),
                      rs.getString("IT_PENVURG"),
                      rs.getString("CD_ENFERMO"), //devuelve nulo si lo fuera
                      rs.getString("CD_PROV"), "", "",
                      rs.getString("CD_MUN"), "",
                      rs.getString("CD_NIVEL_1"), "",
                      rs.getString("CD_NIVEL_2"), "",
                      rs.getString("CD_ZBS"), "",
                      formater.format(rs.getDate("FC_RECEP")),
                      rs.getString("DS_CALLE"),
                      rs.getString("DS_NMCALLE"),
                      rs.getString("DS_PISO"),
                      rs.getString("CD_POSTAL"),
                      rs.getString("CD_OPE"),
                      sfecRecu,
                      rs.getString("CD_ANOOTRC"),
                      rs.getString("NM_ENVOTRC"), //devuelve nulo si lo fuera
                      rs.getString("CD_ENFCIE"),
                      formater.format(rs.getDate("FC_FECNOTIF")),
                      rs.getString("IT_DERIVADO"),
                      rs.getString("DS_CENTRODER"),
                      rs.getString("IT_DIAGCLI"),
                      FC_INISNT,
                      rs.getString("DS_COLECTIVO"),
                      rs.getString("IT_ASOCIADO"),
                      rs.getString("IT_DIAGMICRO"),
                      rs.getString("DS_ASOCIADO"),
                      rs.getString("IT_DIAGSERO"),
                      rs.getString("DS_DIAGOTROS"),
                      rs.getString("CDVIAL"), //devuelve nulo si lo fuera
                      rs.getString("CDTVIA"), //devuelve nulo si lo fuera
                      rs.getString("CDTNUM"), //devuelve nulo si lo fuera
                      rs.getString("DSCALNUM")); //devuelve nulo si lo fuera

                } //while del rs
                listaCasosBorrar.addElement(dataCasosBorrar);

              } //fin del if
              else {
                //@JLT
                // si existen otros notificadores en esas circunstancias, en el
                // notif_edoi m�s antiguo hay que poner it_primero = 'S' y en
                // sive_edoind hay que poner el anyo y la semana del registro que
                // se modifique en sive_notif_edoi
                sqlNotifedoi = new String();
                sqlNotifedoi = "UPDATE SIVE_NOTIF_EDOI SET IT_PRIMERO = ? "
                    + "WHERE CD_E_NOTIF = ? AND CD_ANOEPI = ? AND "
                    + " CD_SEMEPI = ? AND FC_FECNOTIF  = ? AND "
                    + " FC_RECEP = ? and NM_EDO = ? ";

                st = con.prepareStatement(sqlNotifedoi);
                st.setString(1, sPrimero);
                st.setString(2, (String) hNotifedoi.get("CD_E_NOTIF"));
                st.setString(3, (String) hNotifedoi.get("CD_ANOEPI"));
                st.setString(4, (String) hNotifedoi.get("CD_SEMEPI"));

                //sfcnotif = ((String) hNotifedoi.get("FC_FECNOTIF")).trim();
                //sfcnotif = stringdate2stringdate(sfcnotif);
                //sfcrecep = ((String) hNotifedoi.get("FC_RECEP")).trim();
                //sfcrecep = stringdate2stringdate(sfcrecep);

                int iedo;
                iedo = (new Integer( (String) hNotifedoi.get("NM_EDO"))).
                    intValue();

                //fcnotif = formater.parse(sfcnotif);
                //fcrecep = formater.parse(sfcrecep);

                //sqlfcnotif = new java.sql.Date(fcnotif.getTime());
                //st.setDate(5, sqlfcnotif);
                st.setDate(5, (java.sql.Date) hNotifedoi.get("FC_FECNOTIF"));
                //sqlfcrecep = new java.sql.Date(fcrecep.getTime());
                //st.setDate(6, sqlfcrecep);
                st.setDate(6, (java.sql.Date) hNotifedoi.get("FC_FECNOTIF"));
                st.setInt(7, iedo);

                st.executeUpdate();
                st.close();
                st = null;

                sqlEdoind = new String();
                sqlEdoind =
                    "UPDATE SIVE_EDOIND SET CD_ANOEPI = ?, CD_SEMEPI = ? "
                    + "WHERE  NM_EDO = ? ";

                st = con.prepareStatement(sqlEdoind);
                st.setString(1, (String) hNotifedoi.get("CD_ANOEPI"));
                st.setString(2, (String) hNotifedoi.get("CD_SEMEPI"));
                st.setInt(3,
                          (new Integer( (String) hNotifedoi.get("NM_EDO"))).intValue());

                st.executeUpdate();

                st.close();
                st = null;

              } // FIN DE IF @JLT

              ExisteNOTIF_EDOIconigualCaso = false; //reseteo para el siguiente caso
            } //fin for

            //# // System_out.println("GUI 8");

            //ENFERMOS para no borrar, enfermos para Borrar*********************
            if (listaCasosBorrar.size() > 0) {

              sQuery = "select distinct CD_ENFERMO from SIVE_EDOIND "
                  + " where ";

              i = 0;
              for (i = 0; i < listaCasosBorrar.size(); i++) {
                sQuery = sQuery + " NM_EDO <> ? and ";
              }

              sQuery = sQuery + "CD_ENFERMO in (";

              i = 0;
              for (i = 0; i < listaCasosBorrar.size() - 1; i++) {
                sQuery = sQuery + "? ,";
              }
              sQuery = sQuery + " ? ) ";

              st = con.prepareStatement(sQuery);

              // ARG: Se registra la consulta
              registroConsultas.registrar("SIVE_EDOIND",
                                          sQuery,
                                          "CD_ENFERMO",
                                          "",
                                          "SrvBorrarMasivo",
                                          true);

              i = 0;
              iInterrogacion = 1;
              for (i = 0; i < listaCasosBorrar.size(); i++) {
                dataCasosBorrar = (DataEdoind) listaCasosBorrar.elementAt(i);
                st.setInt(iInterrogacion,
                          (new Integer(dataCasosBorrar.getNM_EDO().trim())).
                          intValue());
                iInterrogacion++;
                // ARG: Se a�aden parametros
                registroConsultas.insertarParametro(dataCasosBorrar.getNM_EDO().
                    trim());
              }

              i = 0;
              for (i = 0; i < listaCasosBorrar.size(); i++) {
                dataCasosBorrar = (DataEdoind) listaCasosBorrar.elementAt(i);
                st.setInt(iInterrogacion,
                          (new Integer(dataCasosBorrar.getCD_ENFERMO().trim())).
                          intValue());
                iInterrogacion++;
                // ARG: Se a�aden parametros
                registroConsultas.insertarParametro(dataCasosBorrar.
                    getCD_ENFERMO().trim());
              }

              rs = st.executeQuery();

              String cd_enfermo_ver = "";
              while (rs.next()) {
                cd_enfermo_ver = rs.getString("CD_ENFERMO");
                dataEnfermosNoBorrar = new DataEnfCaso
                    (cd_enfermo_ver, ""); //lo puedo recoger como string

                listaEnfermosNoBorrar.addElement(dataEnfermosNoBorrar);
              }

              rs.close();
              rs = null;
              st.close();
              st = null;

              //# // System_out.println("GUI 9");

              //LISTAENFERMOSBORRAR!!!!!!!!!!!!!!!!!!!!!!!
              //listaCasosBorrar --- listaEnfermosNoBorrar
              String cd_enfermo_afectado = "";
              boolean bCoinciden = false;
              i = 0;
              for (i = 0; i < listaCasosBorrar.size(); i++) {
                dataCasosBorrar = (DataEdoind) listaCasosBorrar.elementAt(i);

                if (!dataCasosBorrar.getCD_ENFERMO().trim().equals(
                    cd_enfermo_afectado.trim())) {
                  cd_enfermo_afectado = dataCasosBorrar.getCD_ENFERMO().trim();
                  bCoinciden = false;
                  for (int j = 0; j < listaEnfermosNoBorrar.size(); j++) {
                    dataEnfermosNoBorrar = (DataEnfCaso) listaEnfermosNoBorrar.
                        elementAt(j);
                    if (cd_enfermo_afectado.trim().equals(dataEnfermosNoBorrar.
                        getCD_ENFERMO().trim())
                        && !bCoinciden) {
                      bCoinciden = true;
                    }
                    if (!bCoinciden) {
                      dataEnfermosBorrar = new DataEnfCaso(cd_enfermo_afectado.
                          trim(), "");

                      listaEnfermosBorrar.addElement(dataEnfermosBorrar);

                    }
                  } //for

                  //Error al ir pegando la lista de enfermos a Borrar:
                  //Podemos tener 113, 112, 113 ie varios repetidos
                  //RECOMPONER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                  if (listaEnfermosBorrar.size() > 0) {

                    int k = 0;
                    int u = 0;

                    DataEnfCaso dataAlmacen = null;
                    CLista listaAlmacen = new CLista(); //para recomponer

                    DataEnfCaso dataUsar = null;
                    CLista listaUsar = new CLista(); //para usar--> pasaremos a listaEnfermosBorrar

                    //copiar en almacen
                    i = 0;
                    for (i = 0; i < listaEnfermosBorrar.size(); i++) {
                      dataAlmacen = (DataEnfCaso) listaEnfermosBorrar.elementAt(
                          i);
                      listaAlmacen.addElement(dataAlmacen);
                    }

                    String sEnfermo = "";
                    i = 0;
                    for (i = 0; i < listaEnfermosBorrar.size(); i++) {
                      dataEnfermosBorrar = (DataEnfCaso) listaEnfermosBorrar.
                          elementAt(i);
                      sEnfermo = dataEnfermosBorrar.getCD_ENFERMO().trim();

                      //PRIMER ELEMENTO ---------------------------
                      if (i == 0) {
                        dataUsar = new DataEnfCaso(sEnfermo, "");
                        listaUsar.addElement(dataUsar);
                      } //0

                      //DEMAS ELEMENTOS ---------------------------
                      else {
                        boolean byaesta = false;
                        for (u = 0; u < listaUsar.size(); u++) {
                          dataUsar = (DataEnfCaso) listaUsar.elementAt(u);
                          if (dataUsar.getCD_ENFERMO().trim().equals(sEnfermo)) {
                            byaesta = true;
                          }
                        }

                        if (byaesta) { //si esta en listaUsar, no se hace nada
                          //registro que ya sobra y esta almacenado
                        }
                        else {
                          //busco otras en Almacen
                          dataUsar = new DataEnfCaso(sEnfermo, "");
                          listaUsar.addElement(dataUsar);
                        } //else

                      } //else

                    } //fin for ppal
                    //volver a copiar en EnfermoBorrar
                    //COPIA   en param, la de usar
                    listaEnfermosBorrar.removeAllElements();
                    i = 0;
                    for (i = 0; i < listaUsar.size(); i++) {
                      dataUsar = (DataEnfCaso) listaUsar.elementAt(i);
                      listaEnfermosBorrar.addElement(dataUsar);
                    }

                  } //fin de if si existen Enfermos a Borrar
                  //FIN RECOMPONER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                } //if
              } //for

            } //if

            //# // System_out.println("GUI 10");
            //borrar notif_edoi
            sQuery = "DELETE from SIVE_NOTIF_EDOI "
                + " WHERE (CD_E_NOTIF = ? AND CD_ANOEPI = ? "
                + " AND CD_SEMEPI = ? AND FC_RECEP = ? "
                + " AND FC_FECNOTIF = ? AND CD_FUENTE = 'E' )";

            st = con.prepareStatement(sQuery);

            st.setString(1, cd_e_notif);
            st.setString(2, cd_anoepi);
            st.setString(3, cd_semepi);
            dFecha = formater.parse(fc_recep.trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(4, sqlFec);
            dFecha = formater.parse(fc_fecnotif.trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(5, sqlFec);

            st.executeUpdate();
            st.close();
            st = null;

//IF!!!!!! si hay casos para borrar
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            if (listaCasosBorrar.size() > 0) {

              //borro resp_edo de los casos que tengo que borrar
              sQuery = "delete from SIVE_RESP_EDO "
                  + "where NM_EDO in (";

              i = 0;
              for (i = 0; i < listaCasosBorrar.size() - 1; i++) {
                sQuery = sQuery + "? ,";
              }
              sQuery = sQuery + " ? )";

              st = con.prepareStatement(sQuery);

              iPrepare = 1;
              i = 0;
              for (i = 0; i < listaCasosBorrar.size(); i++) {
                dataCasosBorrar = (DataEdoind) listaCasosBorrar.elementAt(i);
                st.setInt(iPrepare,
                          (new Integer(dataCasosBorrar.getNM_EDO().trim())).
                          intValue());
                iPrepare = iPrepare + 1;
              }

              st.executeUpdate();
              st.close();
              st = null;

              //# // System_out.println("GUI 11");

              //borro edoind (los casos)
              sQuery = "delete from SIVE_EDOIND "
                  + "where NM_EDO in (";
              i = 0;
              for (i = 0; i < listaCasosBorrar.size() - 1; i++) {
                sQuery = sQuery + "? ,";
              }
              sQuery = sQuery + " ? )";

              st = con.prepareStatement(sQuery);

              i = 0;
              for (i = 0; i < listaCasosBorrar.size(); i++) {
                dataCasosBorrar = (DataEdoind) listaCasosBorrar.elementAt(i);
                st.setInt(i + 1,
                          (new Integer(dataCasosBorrar.getNM_EDO().trim())).intValue());
              }

              st.executeUpdate();
              st.close();
              st = null;
            }
//IF!!!!!!
//!!!!!!!!!

//IF!!!!!! si hay enfermos para borrar
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if (listaEnfermosBorrar.size() > 0) {

              //borro mov_enfermos
              sQuery = "delete from SIVE_MOV_ENFERMO "
                  +
                  "where IT_ENFERMO = 'S' and IT_CONTACTO = 'N' and CD_ENFERMO in (";
              i = 0;
              for (i = 0; i < listaEnfermosBorrar.size() - 1; i++) {
                sQuery = sQuery + "? ,";
              }
              sQuery = sQuery + " ? )";

              st = con.prepareStatement(sQuery);
              i = 0;
              for (i = 0; i < listaEnfermosBorrar.size(); i++) {
                dataEnfermosBorrar = (DataEnfCaso) listaEnfermosBorrar.
                    elementAt(i);
                st.setInt(i + 1,
                          (new Integer(dataEnfermosBorrar.getCD_ENFERMO().trim())).
                          intValue());
              }

              st.executeUpdate();
              st.close();
              st = null;

              //# // System_out.println("GUI 12");

              //borro enfermos
              sQuery = "delete from SIVE_ENFERMO "
                  + "where CD_ENFERMO in (";
              i = 0;
              for (i = 0; i < listaEnfermosBorrar.size() - 1; i++) {
                sQuery = sQuery + "? ,";
              }
              sQuery = sQuery + " ? )";

              st = con.prepareStatement(sQuery);
              i = 0;
              for (i = 0; i < listaEnfermosBorrar.size(); i++) {
                dataEnfermosBorrar = (DataEnfCaso) listaEnfermosBorrar.
                    elementAt(i);
                st.setInt(i + 1,
                          (new Integer(dataEnfermosBorrar.getCD_ENFERMO().trim())).
                          intValue());
              }

              st.executeUpdate();
              st.close();
              st = null;
            }
//IF!!!!!!
//!!!!!!!!!

//HISTORICOS para cada caso
//!!!!!!!!!!!
            i = 0;
            for (i = 0; i < listaCasosBorrar.size(); i++) {
              dataCasosBorrar = (DataEdoind) listaCasosBorrar.elementAt(i);
              sQuery = Insert_Bajas(); //NM_EDO al final
              st = con.prepareStatement(sQuery);

              st.setString(1, dataCasosBorrar.getCD_ANOEPI().trim());

              st.setString(2, dataCasosBorrar.getCD_SEMEPI().trim());

              if (dataCasosBorrar.getCD_ANOURG() == null) {
                st.setNull(3, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(3, dataCasosBorrar.getCD_ANOURG().trim());

              }

              if (dataCasosBorrar.getNM_ENVIOURGSEM() == null) {
                st.setNull(4, java.sql.Types.VARCHAR);
              }
              else {
                st.setInt(4,
                          (new Integer(dataCasosBorrar.getNM_ENVIOURGSEM().trim())).
                          intValue());

              }

              if (dataCasosBorrar.getCD_CLASIFDIAG() == null) {
                st.setNull(5, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(5, dataCasosBorrar.getCD_CLASIFDIAG().trim());

              }

              if (dataCasosBorrar.getIT_PENVURG() == null) {
                st.setNull(6, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(6, dataCasosBorrar.getIT_PENVURG().trim());

              }

              st.setInt(7,
                        (new Integer(dataCasosBorrar.getCD_ENFERMO().trim())).intValue());

              if (dataCasosBorrar.getCD_PROV_EDOIND() == null) {
                st.setNull(8, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(8, dataCasosBorrar.getCD_PROV_EDOIND().trim());

              }

              if (dataCasosBorrar.getCD_MUN_EDOIND() == null) {
                st.setNull(9, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(9, dataCasosBorrar.getCD_MUN_EDOIND().trim());

              }

              if (dataCasosBorrar.getCD_NIVEL_1_EDOIND() == null) {
                st.setNull(10, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(10, dataCasosBorrar.getCD_NIVEL_1_EDOIND().trim());

              }
              if (dataCasosBorrar.getCD_NIVEL_2_EDOIND() == null) {
                st.setNull(11, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(11, dataCasosBorrar.getCD_NIVEL_2_EDOIND().trim());

              }
              if (dataCasosBorrar.getCD_ZBS_EDOIND() == null) {
                st.setNull(12, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(12, dataCasosBorrar.getCD_ZBS_EDOIND().trim());

              }
              dFecha = formater.parse(dataCasosBorrar.getFC_RECEP().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(13, sqlFec);

              if (dataCasosBorrar.getDS_CALLE() == null) {
                st.setNull(14, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(14, dataCasosBorrar.getDS_CALLE().trim());

              }
              if (dataCasosBorrar.getDS_NMCALLE() == null) {
                st.setNull(15, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(15, dataCasosBorrar.getDS_NMCALLE().trim());

              }
              if (dataCasosBorrar.getDS_PISO_EDOIND() == null) {
                st.setNull(16, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(16, dataCasosBorrar.getDS_PISO_EDOIND().trim());

              }
              if (dataCasosBorrar.getCD_POSTAL_EDOIND() == null) {
                st.setNull(17, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(17, dataCasosBorrar.getCD_POSTAL_EDOIND().trim());

              }
              st.setString(18, dataCasosBorrar.getCD_OPE_EDOIND().trim());

              st.setTimestamp(19,
                              DeString_ATimeStamp(dataCasosBorrar.getFC_ULTACT_EDOIND().
                                                  trim()));

              if (dataCasosBorrar.getCD_ANOOTRC() == null) {
                st.setNull(20, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(20, dataCasosBorrar.getCD_ANOOTRC().trim());

              }

              if (dataCasosBorrar.getNM_ENVOTRC() == null) {
                st.setNull(21, java.sql.Types.VARCHAR);
              }
              else {
                st.setInt(21,
                          (new Integer(dataCasosBorrar.getNM_ENVOTRC().trim())).intValue());

              }
              st.setString(22, dataCasosBorrar.getCD_ENFCIE().trim());

              dFecha = formater.parse(dataCasosBorrar.getFC_FECNOTIF().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(23, sqlFec);

              if (dataCasosBorrar.getIT_DERIVADO() == null) {
                st.setNull(24, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(24, dataCasosBorrar.getIT_DERIVADO().trim());

              }

              if (dataCasosBorrar.getDS_CENTRODER() == null) {
                st.setNull(25, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(25, dataCasosBorrar.getDS_CENTRODER().trim());

              }
              if (dataCasosBorrar.getIT_DIAGCLI() == null) {
                st.setNull(26, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(26, dataCasosBorrar.getIT_DIAGCLI().trim());

              }
              if (dataCasosBorrar.getFC_INISNT() == null) {
                st.setNull(27, java.sql.Types.VARCHAR);
              }
              else {
                dFecha = formater.parse(dataCasosBorrar.getFC_INISNT().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(27, sqlFec);
              }

              if (dataCasosBorrar.getDS_COLECTIVO() == null) {
                st.setNull(28, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(28, dataCasosBorrar.getDS_COLECTIVO().trim());

              }
              if (dataCasosBorrar.getIT_ASOCIADO() == null) {
                st.setNull(29, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(29, dataCasosBorrar.getIT_ASOCIADO().trim());

              }

              if (dataCasosBorrar.getIT_DIAGMICRO() == null) {
                st.setNull(30, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(30, dataCasosBorrar.getIT_DIAGMICRO().trim());

              }
              if (dataCasosBorrar.getDS_ASOCIADO() == null) {
                st.setNull(31, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(31, dataCasosBorrar.getDS_ASOCIADO().trim());

              }
              if (dataCasosBorrar.getIT_DIAGSERO() == null) {
                st.setNull(32, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(32, dataCasosBorrar.getIT_DIAGSERO().trim());

              }

              if (dataCasosBorrar.getDS_DIAGOTROS() == null) {
                st.setNull(33, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(33, dataCasosBorrar.getDS_DIAGOTROS().trim());

                // SUCA
              }
              if (dataCasosBorrar.getCDVIAL() == null) {
                st.setNull(34, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(34, dataCasosBorrar.getCDVIAL().trim());

              }
              if (dataCasosBorrar.getCDTVIA() == null) {
                st.setNull(35, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(35, dataCasosBorrar.getCDTVIA().trim());

              }
              if (dataCasosBorrar.getCDTNUM() == null) {
                st.setNull(36, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(36, dataCasosBorrar.getCDTNUM().trim());

              }
              if (dataCasosBorrar.getDSCALNUM() == null) {
                st.setNull(37, java.sql.Types.VARCHAR);
              }
              else {
                st.setString(37, dataCasosBorrar.getDSCALNUM().trim());

              }
              st.setInt(38,
                        (new Integer(dataCasosBorrar.getNM_EDO().trim())).intValue());

              st.executeUpdate();
              st.close();
              st = null;
            } //fin for

//!!!!!!!!!!!
//!!!!!!!!!!!

            //# // System_out.println("GUI 13");

            //NOTIFEDO!!!!
            //borrar notifedo
            sQuery = "DELETE from SIVE_NOTIFEDO "
                + " WHERE (CD_E_NOTIF = ? AND CD_ANOEPI = ? "
                + " AND CD_SEMEPI = ? AND FC_RECEP = ? "
                + " AND FC_FECNOTIF = ? )";

            st = con.prepareStatement(sQuery);

            st.setString(1, cd_e_notif);
            st.setString(2, cd_anoepi);
            st.setString(3, cd_semepi);
            dFecha = formater.parse(fc_recep.trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(4, sqlFec);
            dFecha = formater.parse(fc_fecnotif.trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(5, sqlFec);

            st.executeUpdate();
            st.close();
            st = null;

            //# // System_out.println("GUI 14");

            //Borramos notif_sem, si el equipo no procedia cobertura:
            //cuando se de de alta, se generar�
            //ver con enrique!!!
            /*if (it_actualizar_notif_sem.equals("N")){
              sQuery = "DELETE from SIVE_NOTIF_SEM "
                      + " WHERE (CD_E_NOTIF = ? AND CD_ANOEPI = ? "
                      + " AND CD_SEMEPI = ? )";
                   st = con.prepareStatement(sQuery);
                   st.setString (1, cd_e_notif );
                   st.setString (2, cd_anoepi );
                   st.setString (3, cd_semepi );
                   st.executeUpdate();
                   st.close();
                   st=null;
                   }*/
            //---------------------------------

            //LISTA DE SALIDA QUE NO SIRVE PARA NADA---
            //cargar la lista de vuelta (con lo que entro)
            /*dataSalida = new DataBorrarMasivo (dataEntrada.getsCOBERTURA(),
                              dataEntrada.getIT_ACTUALIZAR_NOTIF_SEM(),
                              cd_e_notif ,  cd_anoepi , cd_semepi ,
                              fc_recep ,  fc_fecnotif ,nm_ntotreal ,
                              cd_ope , fc_ultact);
                 listaSalida.addElement(dataSalida);*/

          } // fin si hay permisos

          break; //ppal

      } //fin switch

      //valida la transacci�n

      con.commit();
      if (listaSalida.size() == 0) {
        listaSalida.addElement("OK");

      }
      String mensaje = (String) listaSalida.firstElement();
      //# // System_out.println("GUI mensaje" +mensaje);

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = new CLista();
      throw ex;
    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return listaSalida;

  } //fin doWork

  public String Insert_Bajas() {
    return ("insert into SIVE_BAJASEDOIND "
            + "( CD_ANOEPI,  CD_SEMEPI, " //3
            + " CD_ANOURG, NM_ENVIOURGSEM, CD_CLASIFDIAG, " //4
            + " IT_PENVURG, CD_ENFERMO, CD_PROV, CD_MUN, " //4
            + " CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, FC_RECEP, DS_CALLE, " //5
            + " DS_NMCALLE, DS_PISO, CD_POSTAL, CD_OPE, FC_ULTACT, " //5
            + " CD_ANOOTRC, NM_ENVOTRC, CD_ENFCIE,  " //3
            + " FC_FECNOTIF,  IT_DERIVADO, DS_CENTRODER, " // 3+
            + " IT_DIAGCLI,  FC_INISNT, DS_COLECTIVO, " // 3+
            + " IT_ASOCIADO,  IT_DIAGMICRO, DS_ASOCIADO, "
            +
        " IT_DIAGSERO, DS_DIAGOTROS, CDVIAL, CDTVIA, CDTNUM, DSCALNUM, NM_EDO )"
            + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, "
            + " ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
  }

//****************************************************************
   private Vector getlistaNM_EDO(Connection con,
                                 String cd_e_notif,
                                 String cd_anoepi,
                                 String cd_semepi,
                                 String fc_recep,
                                 String fc_fecnotif
                                 ) {

     ////# // System_out.println("srvBorrarMasivo: notificaciones - 1");

     Vector listaNM_EDO = new Vector();
     PreparedStatement stAut = null;
     String queryAut = null;
     ResultSet rsAut = null;

     java.util.Date dFecha;
     java.sql.Date sqlFec;
     SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

     queryAut = new String(
         " select NM_EDO from SIVE_NOTIF_EDOI where CD_E_NOTIF = ? " +
         " and CD_ANOEPI = ? and CD_SEMEPI = ? and FC_RECEP = ? " +
         " and FC_FECNOTIF = ? and CD_FUENTE = 'E' ");

     ////# // System_out.println("srvBorrarMasivo: notificaciones - 1 a");

     try {
       stAut = con.prepareStatement(queryAut);
       stAut.setString(1, cd_e_notif);
       stAut.setString(2, cd_anoepi);
       stAut.setString(3, cd_semepi);

       ////# // System_out.println("srvBorrarMasivo: notificaciones - 1 b: " + cd_e_notif + " " + fc_recep);

       dFecha = formater.parse(fc_recep.trim());
       sqlFec = new java.sql.Date(dFecha.getTime());
       stAut.setDate(4, sqlFec);

       ////# // System_out.println("srvBorrarMasivo: notificaciones - 1 c");

       dFecha = formater.parse(fc_fecnotif.trim());
       sqlFec = new java.sql.Date(dFecha.getTime());
       stAut.setDate(5, sqlFec);

       ////# // System_out.println("srvBorrarMasivo: notificaciones - 1 d");

       rsAut = stAut.executeQuery();

       while (rsAut.next()) {
         listaNM_EDO.addElement(new Integer(rsAut.getInt("NM_EDO")));
       }

       // De prueba
       /*String strX = new String("");
            for (int iX = 0; iX < listaNM_EDO.size(); iX++) {
         strX = strX + "," + String.valueOf( ((Integer) listaNM_EDO.elementAt(iX)).intValue() );
            }
            //# // System_out.println(strX);*/

       rsAut.close();
       rsAut = null;

       stAut.close();
       stAut = null;

       ////# // System_out.println("srvBorrarMasivo: notificaciones - 2");

     }
     catch (Exception exc) {
       //# // System_out.println("Error recorriendo NOTIFICACIONES");
       exc.printStackTrace();
     }
     ;

     ////# // System_out.println("srvBorrarMasivo: notificaciones - 3");

     return listaNM_EDO;
   }

  private Vector getlistaCD_E_NOTIF(Connection con, Vector listaNM_EDO) {

    ////# // System_out.println("srvBorrarMasivo: equipos - 1");

    Vector listaCD_E_NOTIF = new Vector();

    StringBuffer sbEquipos = null;
    PreparedStatement stAut = null;
    String queryAut = null;
    ResultSet rsAut = null;

    for (int iNM_EDO = 0; iNM_EDO < listaNM_EDO.size(); iNM_EDO++) {
      sbEquipos = new StringBuffer("");
      queryAut = new String(" select CD_E_NOTIF from SIVE_NOTIF_EDOI " +
                            " where NM_EDO = ? and CD_FUENTE = 'E' ");

      //  //# // System_out.println("srvBorrarMasivo: equipos - 2");

      try {
        stAut = con.prepareStatement(queryAut);
        stAut.setInt(1, ( (Integer) listaNM_EDO.elementAt(iNM_EDO)).intValue());

        rsAut = stAut.executeQuery();

        while (rsAut.next()) {
          sbEquipos.append(rsAut.getString("CD_E_NOTIF") + ",");
        }

        ////# // System_out.println("Equipos de un caso: " + sbEquipos.toString());

        listaCD_E_NOTIF.addElement(sbEquipos.toString());

        rsAut.close();
        rsAut = null;

        stAut.close();
        stAut = null;

      }
      catch (Exception e) {}
    }

    // De prueba
    /*String strX = new String("");
         for (int iX = 0; iX < listaCD_E_NOTIF.size(); iX++) {
      strX = strX + " - " + (String) listaCD_E_NOTIF.elementAt(iX);
         }
         //# // System_out.println("Vector CD_E_NOTIF" + strX);*/

    return listaCD_E_NOTIF;
  }

//*********** Perfil 3
   private boolean getPermisoPerfil_3(Connection con, String strOPE,
                                      Vector listaCD_E_NOTIF) {

     // //# // System_out.println("Borrado en masa: entrada en perfil 3");

     PreparedStatement stAut = null;
     String queryAut = null;
     ResultSet rsAut = null;

     StringTokenizer stkEquipos = null;
     String strInEquipos = null;
     Vector listaClaves = null;
     int j = 0;
     boolean bPermisos = true;

     //String strX = null;

     for (int i = 0; i < listaCD_E_NOTIF.size() && bPermisos; i++) {
       stkEquipos = new StringTokenizer( (String) listaCD_E_NOTIF.elementAt(i),
                                        ",");
       strInEquipos = new String();
       listaClaves = new Vector();

       while (stkEquipos.hasMoreTokens()) {
         listaClaves.addElement(stkEquipos.nextToken());
       }
       /* De prueba
            strX = new String("");
            for (int iX = 0; iX < listaClaves.size(); iX++) {
         strX = strX + ", " + (String) listaClaves.elementAt(iX);
            }
            //# // System_out.println("Equipos: " + strX);
        */

       for (j = 0; j < listaClaves.size() - 1; j++) {
         strInEquipos = strInEquipos + "?,";
       }
       strInEquipos = strInEquipos + "?";

       queryAut = new String(" select CD_NIVEL_1 from SIVE_NIVEL1_S where " +
           " CD_NIVEL_1 in ( select CD_NIVEL_1 from SIVE_E_NOTIF where " +
           " CD_E_NOTIF in (" + strInEquipos + "))" +
           " and " +
           " CD_NIVEL_1 not in ( select CD_NIVEL_1 from SIVE_AUTORIZACIONES_PISTA where " +
           " CD_USUARIO = ? )");

       ////# // System_out.println(queryAut);

       try {
         stAut = con.prepareStatement(queryAut);
         for (j = 0; j < listaClaves.size(); j++) {
           stAut.setString(j + 1, (String) listaClaves.elementAt(j));
         }
         stAut.setString(j + 1, strOPE);

         rsAut = stAut.executeQuery();

         if (rsAut.next()) {
           bPermisos = false;
           // //# // System_out.println("Borrado en masa: salgo de perfil 3 sin permisos");
           //break;
         }

         rsAut.close();
         rsAut = null;

         stAut.close();
         stAut = null;
         // //# // System_out.println("Borrado en masa: salgo de perfil 3 con permisos");
       }
       catch (Exception e) {}

       if (!bPermisos) {
         break;
       }
     }

     /*/# // System_out.println("Borrado en masa: salida de perfil 3 " +
                       (bPermisos ? "con permisos" : "sin permisos"));
     */
    return bPermisos;
   }

//********* Perfil 4
   private boolean getPermisoPerfil_4(Connection con, String strOPE,
                                      Vector listaCD_E_NOTIF) {

     // //# // System_out.println("Borrado en masa: entrada en perfil 4");

     PreparedStatement stAut = null;
     String queryAut = null;
     ResultSet rsAut = null;

     StringTokenizer stkEquipos = null;
     String strInEquipos = null;

     String strCD_NIVEL_1_AUX = null;
     String strCD_NIVEL_2_AUX = null;

     Vector listaClaves = null;
     Vector listaParesNOTIF = null;
     Vector listaParesAUTORIZACION = null;

     int j = 0;
     boolean bPermisos = true;

     for (int i = 0; i < listaCD_E_NOTIF.size() && bPermisos; i++) {
       listaParesNOTIF = new Vector();
       listaParesAUTORIZACION = new Vector();

       stkEquipos = new StringTokenizer( (String) listaCD_E_NOTIF.elementAt(i),
                                        ",");
       strInEquipos = new String();
       listaClaves = new Vector();

       while (stkEquipos.hasMoreTokens()) {
         listaClaves.addElement(stkEquipos.nextToken());
       }

       // De prueba
       /*String strX = new String("");
            for (int iX = 0; iX < listaClaves.size(); iX++) {
         strX = strX + " - " + (String) listaClaves.elementAt(iX);
            }
            //# // System_out.println("Equipos Perfil 4:" + strX);
        */

       for (j = 0; j < listaClaves.size() - 1; j++) {
         strInEquipos = strInEquipos + "?,";
       }
       strInEquipos = strInEquipos + "?";

       queryAut = new String(
           " select CD_NIVEL_1, CD_NIVEL_2 from SIVE_E_NOTIF where " +
           " CD_E_NOTIF in (" + strInEquipos + ")");

       // //# // System_out.println("Borrado en masa: perfil 4 - 1:" + queryAut);

       try {
         stAut = con.prepareStatement(queryAut);
         for (j = 0; j < listaClaves.size(); j++) {
           stAut.setString(j + 1, (String) listaClaves.elementAt(j));
         }

         rsAut = stAut.executeQuery();

         // //# // System_out.println("Perfil 4: Ejecuci�n de la primera query");

         while (rsAut.next()) {
           listaParesNOTIF.addElement(rsAut.getString("CD_NIVEL_1") + "-" +
                                      rsAut.getString("CD_NIVEL_2"));
         }

         rsAut.close();
         rsAut = null;

         stAut.close();
         stAut = null;

         // //# // System_out.println("Borrado en masa: perfil 4 - 2: Antes de la segunda consulta");

         queryAut = new String(
             " select CD_NIVEL_1, CD_NIVEL_2 from SIVE_AUTORIZACIONES_PISTA where " +
             " CD_USUARIO = ? ");

         // //# // System_out.println("Borrado en masa: perfil 4 - 2:" + queryAut);

         stAut = con.prepareStatement(queryAut);
         stAut.setString(1, strOPE);

         rsAut = stAut.executeQuery();

         while (rsAut.next()) {
           strCD_NIVEL_1_AUX = rsAut.getString("CD_NIVEL_1");
           strCD_NIVEL_2_AUX = rsAut.getString("CD_NIVEL_2");
           if (strCD_NIVEL_1_AUX != null && strCD_NIVEL_2_AUX != null) {
             listaParesAUTORIZACION.addElement(strCD_NIVEL_1_AUX + "-" +
                                               strCD_NIVEL_2_AUX);
           }
         }

         rsAut.close();
         rsAut = null;

         stAut.close();
         stAut = null;

         for (j = 0; j < listaParesNOTIF.size() && bPermisos; j++) {
           if (!listaParesAUTORIZACION.contains(listaParesNOTIF.elementAt(j))) {
             bPermisos = false;
           }
         }

       }
       catch (Exception e) {
         //# // System_out.println("Error en perfil 4");
         bPermisos = false;
       }

       // //# // System_out.println("Borrado en masa: estoy en perfil 4 con permisos");
     }

     /*/# // System_out.println("Borrado en masa: salida de perfil 4 " +
                       (bPermisos ? "con permisos" : "sin permisos"));
     */
    return bPermisos;
   }

//****** Perfil 5
   private boolean getPermisoPerfil_5(Connection con, String cd_e_notif,
                                      Vector listaCD_E_NOTIF) {

     StringTokenizer stkEquipos = null;
     String strInEquipos = null;
     boolean bPermisos = true;

     ////# // System_out.println("Borrado en masa: entrada en perfil 5");

     for (int i = 0; i < listaCD_E_NOTIF.size() && bPermisos; i++) {
       stkEquipos = new StringTokenizer( (String) listaCD_E_NOTIF.elementAt(i),
                                        ",");

       if (stkEquipos.countTokens() > 1) {
         bPermisos = false;
       }

       while (stkEquipos.hasMoreTokens() && bPermisos) {
         if (!cd_e_notif.equals(stkEquipos.nextToken())) {
           bPermisos = false;
         }
       }

     }

     /*# // System_out.println("Borrado en masa: salida de perfil 5 " +
                        (bPermisos ? "con permisos" : "sin permisos"));
      */
     return bPermisos;
   }

} //fin de la clase
