package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import capp.CLista;
import notdata.DataNumerica;
import sapp.DBServlet;

public class SrvNumerica
    extends DBServlet {

  //ESTRUCTURA DE LA LISTA:
  //primer registro :
  //datos notifedo , datos primer edonum
  //siguientes registros :
  //NULOS, datos del siguiente edonum

  final int servletINSERT_EDO_NUMERICA = 0;
  final int servletUPDATE_EDO_NUMERICA = 1;
  public SimpleDateFormat Format_Horas = new SimpleDateFormat(
      "dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  public java.sql.Timestamp DeString_ATimeStamp(String sFecha) {

    String sFecha_larga = sFecha;
    //partirla!!
    int sdd = (new Integer(sFecha_larga.substring(0, 2))).intValue();
    int smm = (new Integer(sFecha_larga.substring(3, 5))).intValue();
    int syyyy = (new Integer(sFecha_larga.substring(6, 10))).intValue();
    int shh = (new Integer(sFecha_larga.substring(11, 13))).intValue();
    int smi = (new Integer(sFecha_larga.substring(14, 16))).intValue();
    int sss = (new Integer(sFecha_larga.substring(17, 19))).intValue();

    java.sql.Timestamp TSFecha = new java.sql.Timestamp(syyyy - 1900,
        smm - 1,
        sdd, shh, smi, sss, 0);
    return (TSFecha);

  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 1;
    int k = 0;
    int u = 0;

    // objetos de datos
    DataNumerica dataEntrada = null;
    CLista listaSalida = new CLista();
    DataNumerica dataSalida = null; //para almacenar temporales

    DataNumerica dataStore = null;
    CLista listaStore = new CLista(); //para recomponer

    DataNumerica dataUsar = null;
    CLista listaUsar = new CLista(); //para usar

    DataNumerica dataInsertar = null;
    CLista listaInsertar = new CLista(); //para insertar
    DataNumerica dataModif = null;
    CLista listaModif = new CLista(); //para modificar

    String sEnfermedad = "";
    String NumCasos = "";
    int iNumTotal = 0;

    //querys
    String sQuery = "";

    //fechas
    java.util.Date dFecha;
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    //fecha actual-----------------------------------------------------------
    //para devolver (sFecha_Actual), para insertar (st.setTimestamp(9, TSFec))
    java.util.Date dFecha_Actual = new java.util.Date();
    String sFecha_Actual = Format_Horas.format(dFecha_Actual); //string

    //partirla!!
    int sdd = (new Integer(sFecha_Actual.substring(0, 2))).intValue();
    int smm = (new Integer(sFecha_Actual.substring(3, 5))).intValue();
    int syyyy = (new Integer(sFecha_Actual.substring(6, 10))).intValue();
    int shh = (new Integer(sFecha_Actual.substring(11, 13))).intValue();
    int smi = (new Integer(sFecha_Actual.substring(14, 16))).intValue();
    int sss = (new Integer(sFecha_Actual.substring(17, 19))).intValue();

    java.sql.Timestamp TSFec = new java.sql.Timestamp(syyyy - 1900, smm - 1,
        sdd, shh, smi, sss, 0);
    //st.setTimestamp(9, TSFec);
    // --------------------------------------------------------------------

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {

      switch (opmode) {

        //alta en notifedo ,alta en edonum
        case servletINSERT_EDO_NUMERICA:

          //leo del primero que lleva notifedo
          dataEntrada = (DataNumerica) param.firstElement();
          boolean ExisteNotifSem = false;

          sQuery = Pregunta_Select_notif_sem();
          st = con.prepareStatement(sQuery);
          st.setString(1, dataEntrada.getCD_E_NOTIF().trim());
          st.setString(2, dataEntrada.getCD_ANOEPI().trim());
          st.setString(3, dataEntrada.getCD_SEMEPI().trim());
          rs = st.executeQuery();
          while (rs.next()) {
            ExisteNotifSem = true;
          } //fin while
          rs.close();
          rs = null;
          st.close();
          st = null;

          //Se trata de un equipo con Centro NO en Cobertura: CON NULOS
          if (!ExisteNotifSem) {

            //lo inserto
            sQuery = Pregunta_Alta_notif_sem();
            st = con.prepareStatement(sQuery);
            st.setString(1, dataEntrada.getCD_E_NOTIF().trim());
            st.setString(2, dataEntrada.getCD_ANOEPI().trim());
            st.setString(3, dataEntrada.getCD_SEMEPI().trim());

            //if (dataEntrada.getNM_NNOTIFT_NUEVA().trim().length() > 0)
            // st.setInt(4, (new Integer (dataEntrada.getNM_NNOTIFT_NUEVA().trim()).intValue()));
            //else
            st.setNull(4, java.sql.Types.VARCHAR);

            //if (dataEntrada.getNM_NTOTREAL().trim().length() > 0)
            //  st.setInt(5, (new Integer (dataEntrada.getNM_NTOTREAL().trim()).intValue()));
            //else
            st.setNull(5, java.sql.Types.VARCHAR);

            st.setString(6, dataEntrada.getCD_OPE().trim());

            st.setTimestamp(7, TSFec); //Fecha actual

            st.executeUpdate();
            st.close();

          } //no existe, lo inserto

          else { //si existe el registro en notif_sem

            if (dataEntrada.getIT_ACTUALIZAR_NOTIF_SEM().trim().equals("S")) {

              sQuery = Pregunta_Modif_notif_sem();

              st = con.prepareStatement(sQuery);
              if (dataEntrada.getNM_NNOTIFT_NUEVA().trim().length() > 0) {
                st.setInt(1,
                          (new Integer(dataEntrada.getNM_NNOTIFT_NUEVA().trim()).
                           intValue()));
              }
              else {
                st.setNull(1, java.sql.Types.VARCHAR);

              }
              if (dataEntrada.getNM_NTOTREAL().trim().length() > 0) {
                st.setInt(2,
                          (new Integer(dataEntrada.getNM_NTOTREAL().trim()).intValue()));
              }
              else {
                st.setNull(2, java.sql.Types.VARCHAR);

              }
              st.setString(3, dataEntrada.getCD_OPE().trim());

              /*dFecha = formater.parse(dataEntrada.getFC_ULTACT().trim());
                         sqlFec = new java.sql.Date(dFecha.getTime());
                         st.setDate(4, sqlFec);*/
              st.setTimestamp(4, TSFec); //Fecha actual

              st.setString(5, dataEntrada.getCD_E_NOTIF().trim());
              st.setString(6, dataEntrada.getCD_ANOEPI().trim());
              st.setString(7, dataEntrada.getCD_SEMEPI().trim());
              st.executeUpdate();
              st.close();

              //# // System_out.println("1");

              //�tengo que actualizar tb e_notif?, solo si Nuevo <> Antiguo y 'S'
              if (!dataEntrada.getNM_NNOTIFT_NUEVA().trim().equals(dataEntrada.
                  getNM_NNOTIFT_ANTIGUA().trim())
                  && dataEntrada.getIT_RESSEM().equals("S")) {

                //actualizo e_notif con 3 campos
                sQuery = Pregunta_Modif_e_notif();
                st = con.prepareStatement(sQuery);
                if (dataEntrada.getNM_NNOTIFT_NUEVA().trim().length() > 0) {
                  st.setInt(1,
                            (new Integer(dataEntrada.getNM_NNOTIFT_NUEVA().trim()).
                             intValue()));
                }
                else {
                  st.setNull(1, java.sql.Types.VARCHAR);

                }
                st.setString(2, dataEntrada.getCD_OPE().trim());

                /*dFecha = formater.parse(dataEntrada.getFC_ULTACT().trim());
                               sqlFec = new java.sql.Date(dFecha.getTime());
                               st.setDate(3, sqlFec);*/
                st.setTimestamp(3, TSFec); //Fecha actual

                st.setString(4, dataEntrada.getCD_E_NOTIF().trim());

                st.executeUpdate();
                st.close();
              }

            } //actualiza notif_sem, y puede que e_notif

          } //fin de si existe

          //!!!!! notifedo !!!!!!

          dataEntrada = (DataNumerica) param.firstElement();
          sQuery = Pregunta_Alta_Notifedo();
          st = con.prepareStatement(sQuery);

          st.setString(1, dataEntrada.getCD_E_NOTIF().trim());
          st.setString(2, dataEntrada.getCD_ANOEPI().trim());
          st.setString(3, dataEntrada.getCD_SEMEPI().trim());

          dFecha = formater.parse(dataEntrada.getFC_RECEP().trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(4, sqlFec);
          //reales en notifedo, pasa a null si no es RS
          if (dataEntrada.getIT_RESSEM().trim().equals("N")) {
            dataEntrada.NM_NNOTIFR = "";

          }
          if (dataEntrada.getNM_NNOTIFR().trim().length() > 0) {
            st.setInt(5,
                      (new Integer(dataEntrada.getNM_NNOTIFR().trim())).intValue());
          }
          else {
            st.setNull(5, java.sql.Types.VARCHAR);

            //--------------
          }
          dFecha = formater.parse(dataEntrada.getFC_FECNOTIF().trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(6, sqlFec);

          st.setString(7, dataEntrada.getIT_RESSEM().trim());
          st.setString(8, dataEntrada.getCD_OPE());

          /*dFecha = formater.parse(dataEntrada.getFC_ULTACT().trim());
                     sqlFec = new java.sql.Date(dFecha.getTime());
                     st.setDate(9, sqlFec);*/
          st.setTimestamp(9, TSFec); //Fecha actual

          st.setString(10, dataEntrada.getIT_VALIDADA().trim());

          ////////////
          //# // System_out.println("dataEntrada.getFC_FECVALID() "+dataEntrada.getFC_FECVALID());
          if (dataEntrada.getFC_FECVALID().trim().length() > 0) {
            dFecha = formater.parse(dataEntrada.getFC_FECVALID().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(11, sqlFec);
          }
          else {
            st.setNull(11, java.sql.Types.VARCHAR);
            /////////

            /*
                 System_out.println("CD_E_NOTIF " + dataEntrada.getCD_E_NOTIF().trim());
                 System_out.println("CD_ANOEPI " +  dataEntrada.getCD_ANOEPI().trim());
                 System_out.println("CD_SEMEPI " +  dataEntrada.getCD_SEMEPI ().trim());
                 System_out.println("FC_RECEP " +  dataEntrada.getFC_RECEP ().trim());
                      if (dataEntrada.getNM_NNOTIFR().trim().length() > 0)
                        System_out.println("FC_RECEP "+ (new Integer(dataEntrada.getNM_NNOTIFR().trim())).intValue());
                      else
                        System_out.println("FC_RECEP null");
                 System_out.println("FC_FECNOTIF " +  dataEntrada.getFC_FECNOTIF ().trim());
                 System_out.println("IT_RESSEM " +  dataEntrada.getIT_RESSEM().trim());
                     System_out.println("CD_OPE " +  dataEntrada.getCD_OPE());
                 System_out.println("IT_VALIDADA " +  dataEntrada.getIT_VALIDADA().trim());
                      if (dataEntrada.getFC_FECVALID().trim().length() > 0){
                       System_out.println("FC_FECVALID " +  dataEntrada.getFC_FECVALID ().trim());          }
                      else
                       System_out.println("FC_FECVALID null" );
             */

          }
          st.executeUpdate();
          st.close();

          //# // System_out.println("2");

//hasta aqui, se da de alta notifedo: MAESTRO
          dataEntrada = (DataNumerica) param.firstElement();
          if (dataEntrada.getIT_SOLO_ALTA_NOTIFEDO().trim().equals("N")) {

//!!!!!!!!!!!!!!!!!!!!!!
//RECOMPONER!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!
            //recomponemos la entrada: existen varis numericas para igual enfermedad
            //COPIA
            for (i = 0; i < param.size(); i++) {
              dataStore = (DataNumerica) param.elementAt(i);
              listaStore.addElement(dataStore);
            }

            for (i = 0; i < param.size(); i++) {
              dataEntrada = (DataNumerica) param.elementAt(i);
              sEnfermedad = dataEntrada.getCD_ENFCIE().trim();

              //PRIMER ELEMENTO ---------------------------
              if (i == 0) {
                iNumTotal = (new Integer(dataEntrada.getNM_CASOS())).intValue();

                for (k = i + 1; k < listaStore.size(); k++) {
                  dataStore = (DataNumerica) listaStore.elementAt(k);
                  if (dataStore.getCD_ENFCIE().trim().equals(sEnfermedad)) {
                    iNumTotal = iNumTotal
                        + (new Integer(dataStore.getNM_CASOS())).intValue();
                  }
                } //for

                dataUsar = new DataNumerica("", "", "",
                                            dataEntrada.getCD_E_NOTIF(),
                                            dataEntrada.getCD_ANOEPI(),
                                            dataEntrada.getCD_SEMEPI(),
                                            dataEntrada.getFC_RECEP(), null,
                                            dataEntrada.getFC_FECNOTIF(),
                                            null, dataEntrada.getCD_OPE(),
                                            dataEntrada.getFC_ULTACT(),
                                            dataEntrada.getIT_VALIDADA(),
                                            dataEntrada.getFC_FECVALID(),
                                            sEnfermedad,
                                            new Integer(iNumTotal).toString(), null, null, null);
                listaUsar.addElement(dataUsar);
                iNumTotal = 0;
              } //0

              //DEMAS ELEMENTOS ---------------------------
              else {
                boolean byaesta = false;
                for (u = 0; u < listaUsar.size(); u++) {
                  dataUsar = (DataNumerica) listaUsar.elementAt(u);
                  if (dataUsar.getCD_ENFCIE().trim().equals(sEnfermedad)) {
                    byaesta = true;
                  }
                }

                if (byaesta) { //si esta en listaUsar, no se hace nada
                  //registro que ya sobra y esta almacenado
                }
                else {
                  //busco otras en Store
                  iNumTotal = (new Integer(dataEntrada.getNM_CASOS())).intValue();

                  for (k = i + 1; k < listaStore.size(); k++) {
                    dataStore = (DataNumerica) listaStore.elementAt(k);
                    if (dataStore.getCD_ENFCIE().trim().equals(sEnfermedad)) {
                      iNumTotal = iNumTotal
                          + (new Integer(dataStore.getNM_CASOS())).intValue();
                    }
                  }
                  dataUsar = new DataNumerica("", "", "",
                                              dataEntrada.getCD_E_NOTIF(),
                                              dataEntrada.getCD_ANOEPI(),
                                              dataEntrada.getCD_SEMEPI(),
                                              dataEntrada.getFC_RECEP(), null,
                                              dataEntrada.getFC_FECNOTIF(),
                                              null, dataEntrada.getCD_OPE(),
                                              dataEntrada.getFC_ULTACT(),
                                              dataEntrada.getIT_VALIDADA(),
                                              dataEntrada.getFC_FECVALID(),
                                              sEnfermedad,
                                              new Integer(iNumTotal).toString(), null, null, null);
                  listaUsar.addElement(dataUsar);
                  iNumTotal = 0;

                } //else

              } //else

            } //fin for ppal

            //COPIA   en param, la de usar
            param.removeAllElements();
            for (i = 0; i < listaUsar.size(); i++) {
              dataUsar = (DataNumerica) listaUsar.elementAt(i);
              param.addElement(dataUsar);
            }

//!!!!!!!!!!!!!!!!!!!!!!
//FIN RECOMPONER!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!

            //!!!!!! edonum !!!!!!!!!!
            for (i = 0; i < param.size(); i++) {

              dataEntrada = (DataNumerica) param.elementAt(i);

              sQuery = Pregunta_Alta_Edonum();
              st = con.prepareStatement(sQuery);

              // c�digo de enfermedad
              st.setString(1, dataEntrada.getCD_ENFCIE().trim());
              // c�digo de equipo notificador
              st.setString(2, dataEntrada.getCD_E_NOTIF().trim());
              // a�o
              st.setString(3, dataEntrada.getCD_ANOEPI().trim());
              // semana
              st.setString(4, dataEntrada.getCD_SEMEPI().trim());
              // fecha de recepci�n
              dFecha = formater.parse(dataEntrada.getFC_RECEP().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(5, sqlFec);
              // fecha de notificaci�n
              dFecha = formater.parse(dataEntrada.getFC_FECNOTIF().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(6, sqlFec);

              // n�mero de casos
              st.setInt(7,
                        (new Integer(dataEntrada.getNM_CASOS().trim())).intValue());
              // c�d. ope.
              st.setString(8, dataEntrada.getCD_OPE().trim());
              // fecha �ltima actualizaci�n
              /*dFecha = formater.parse(dataEntrada.getFC_ULTACT().trim());
                         sqlFec = new java.sql.Date(dFecha.getTime());
                         st.setDate(9, sqlFec); */
              st.setTimestamp(9, TSFec); //Fecha actual

              // lanza la query
              st.executeUpdate();
              st.close();

            } //fin del for

            //# // System_out.println("3");

          } //fin de dar de alta tb al menos una notid_edoi

          //# // System_out.println("4");
          break;
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

          //modifico en notifedo , modifico en edonum
        case servletUPDATE_EDO_NUMERICA:

          //leo del primero que lleva notifedo
          dataEntrada = (DataNumerica) param.firstElement();

//notif_sem
          if (dataEntrada.getIT_ACTUALIZAR_NOTIF_SEM().trim().equals("S")) {

            sQuery = Pregunta_Modif_notif_sem();
            st = con.prepareStatement(sQuery);
            if (dataEntrada.getNM_NNOTIFT_NUEVA().trim().length() > 0) {
              st.setInt(1,
                        (new Integer(dataEntrada.getNM_NNOTIFT_NUEVA().trim()).intValue()));
            }
            else {
              st.setNull(1, java.sql.Types.VARCHAR);
            }
            if (dataEntrada.getNM_NTOTREAL().trim().length() > 0) {
              st.setInt(2,
                        (new Integer(dataEntrada.getNM_NTOTREAL().trim()).intValue()));
            }
            else {
              st.setNull(2, java.sql.Types.VARCHAR);

            }
            st.setString(3, dataEntrada.getCD_OPE().trim());

            /*dFecha = formater.parse(dataEntrada.getFC_ULTACT().trim());
                       sqlFec = new java.sql.Date(dFecha.getTime());
                       st.setDate(4, sqlFec);*/
            st.setTimestamp(4, TSFec); //Fecha actual

            st.setString(5, dataEntrada.getCD_E_NOTIF().trim());
            st.setString(6, dataEntrada.getCD_ANOEPI().trim());
            st.setString(7, dataEntrada.getCD_SEMEPI().trim());
            st.executeUpdate();
            st.close();

            //�tengo que actualizar tb e_notif?, solo si Nuevo <> Antiguo y 'S'
            if (!dataEntrada.getNM_NNOTIFT_NUEVA().trim().equals(dataEntrada.
                getNM_NNOTIFT_ANTIGUA().trim())
                && dataEntrada.getIT_RESSEM().equals("S")) {

              //actualizo e_notif con 3 campos
              sQuery = Pregunta_Modif_e_notif();
              st = con.prepareStatement(sQuery);
              if (dataEntrada.getNM_NNOTIFT_NUEVA().trim().length() > 0) {
                st.setInt(1,
                          (new Integer(dataEntrada.getNM_NNOTIFT_NUEVA().trim()).
                           intValue()));
              }
              else {
                st.setNull(1, java.sql.Types.VARCHAR);

              }
              st.setString(2, dataEntrada.getCD_OPE().trim());

              /*dFecha = formater.parse(dataEntrada.getFC_ULTACT().trim());
                             sqlFec = new java.sql.Date(dFecha.getTime());
                             st.setDate(3, sqlFec);*/
              st.setTimestamp(3, TSFec); //Fecha actual

              st.setString(4, dataEntrada.getCD_E_NOTIF().trim());

              st.executeUpdate();
              st.close();

            }
          }

//# // System_out.println("11111");

          //!!!!! notifedo !!!!!!
          dataEntrada = (DataNumerica) param.firstElement();
          sQuery = Pregunta_Modifica_Notifedo();
          st = con.prepareStatement(sQuery);

          // n�mero de notificadores reales
          //no obligatorio
          if (dataEntrada.getNM_NNOTIFR().trim().length() > 0) {
            st.setInt(1,
                      (new Integer(dataEntrada.getNM_NNOTIFR().trim())).intValue());
          }
          else {
            st.setNull(1, java.sql.Types.VARCHAR);

            // resumen semanal
          }
          st.setString(2, dataEntrada.getIT_RESSEM().trim());

          // c�digo de operaci�n
          st.setString(3, dataEntrada.getCD_OPE().trim());

          // fecha de �ltima actualizaci�n
          /*dFecha = formater.parse(dataEntrada.getFC_ULTACT().trim());
                   sqlFec = new java.sql.Date(dFecha.getTime());
                   st.setDate(4, sqlFec);*/
          st.setTimestamp(4, TSFec); //Fecha actual

          st.setString(5, dataEntrada.getIT_VALIDADA().trim());

          ////////////
          if (dataEntrada.getFC_FECVALID().trim().length() > 0) {
            dFecha = formater.parse(dataEntrada.getFC_FECVALID().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(6, sqlFec);
          }
          else {
            st.setNull(6, java.sql.Types.VARCHAR);
            /////////

            //WHERE
            // c�digo del equipo notificador
          }
          st.setString(7, dataEntrada.getCD_E_NOTIF());
          // a�o
          st.setString(8, dataEntrada.getCD_ANOEPI().trim());
          // seman
          st.setString(9, dataEntrada.getCD_SEMEPI());
          // fecha de recepci�n
          dFecha = formater.parse(dataEntrada.getFC_RECEP().trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(10, sqlFec);
          // fecha de notificaci�n
          dFecha = formater.parse(dataEntrada.getFC_FECNOTIF().trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(11, sqlFec);

          // lanza la query
          st.executeUpdate();
          st.close();

          //!!!!! EDONUM !!!!!!
          //leo los que hay en la base de datos
          //y los meto en una estructura

          dataEntrada = (DataNumerica) param.firstElement();

//ACTUALIZAMOS SOLO LA NOTIFICACION o continuo!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          if (dataEntrada.getIT_SOLO_ACTUALIZAR_NOTIFEDO().trim().equals("N")) {

            sQuery = Pregunta_Select_Previa_Edonum();
            st = con.prepareStatement(sQuery);

            //WHERE
            // c�digo de equipo
            st.setString(1, dataEntrada.getCD_E_NOTIF().trim());
            // a�o
            st.setString(2, dataEntrada.getCD_ANOEPI().trim());
            // semana
            st.setString(3, dataEntrada.getCD_SEMEPI().trim());
            // fecha de recepci�n
            dFecha = formater.parse(dataEntrada.getFC_RECEP().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(4, sqlFec);
            // fecha de notificaci�n
            dFecha = formater.parse(dataEntrada.getFC_FECNOTIF().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(5, sqlFec);

            rs = st.executeQuery();

            //cargo en listaHabia lo que habia en edonum
            CLista listaHabia = new CLista();
            DataNumerica dataHabia = null;
            java.sql.Timestamp fecRecu = null;
            String sfecRecu = "";

            while (rs.next()) {
              fecRecu = rs.getTimestamp("FC_ULTACT");
              sfecRecu = timestamp_a_cadena(fecRecu);

              dataHabia = new DataNumerica("", "", "",
                                           dataEntrada.getCD_E_NOTIF().trim(),
                                           dataEntrada.getCD_ANOEPI().trim(),
                                           dataEntrada.getCD_SEMEPI().trim(),
                                           dataEntrada.getFC_RECEP().trim(),
                                           null,
                                           dataEntrada.getFC_FECNOTIF().trim(),
                                           null, rs.getString("CD_OPE"),
                                           sfecRecu, dataEntrada.getIT_VALIDADA(),
                                           dataEntrada.getFC_FECVALID(),
                                           rs.getString("CD_ENFCIE"),
                                           new Integer(rs.getInt("NM_CASOS")).
                                           toString(),
                                           null, null, null);

              listaHabia.addElement(dataHabia);

            } //fin del while

            rs.close();
            rs = null;
            st.close();
            st = null;

            for (int y = 0; y < listaHabia.size(); y++) {
              dataHabia = (DataNumerica) listaHabia.elementAt(y);
              //# // System_out.println("dataHabia.getCD_ENFCIE() "+dataHabia.getCD_ENFCIE());
            }
//vemos si lo que quiere es borrar todas las numericas (getCD_ENFCIE ="")
            if (!dataEntrada.getCD_ENFCIE().trim().equals("")) {

//!!!!!!!!!!!!!!!!!!!!!!
//RECOMPONER!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!
              //recomponemos la entrada: existen varis numericas para igual enfermedad
              //COPIA
              for (i = 0; i < param.size(); i++) {
                dataStore = (DataNumerica) param.elementAt(i);
                listaStore.addElement(dataStore);
              }

              for (i = 0; i < param.size(); i++) {
                dataEntrada = (DataNumerica) param.elementAt(i);
                sEnfermedad = dataEntrada.getCD_ENFCIE().trim();

                //PRIMER ELEMENTO ---------------------------
                if (i == 0) {
                  iNumTotal = (new Integer(dataEntrada.getNM_CASOS())).intValue();

                  for (k = i + 1; k < listaStore.size(); k++) {
                    dataStore = (DataNumerica) listaStore.elementAt(k);
                    if (dataStore.getCD_ENFCIE().trim().equals(sEnfermedad)) {
                      iNumTotal = iNumTotal
                          + (new Integer(dataStore.getNM_CASOS())).intValue();
                    }
                  } //for

                  dataUsar = new DataNumerica("", "", "",
                                              dataEntrada.getCD_E_NOTIF(),
                                              dataEntrada.getCD_ANOEPI(),
                                              dataEntrada.getCD_SEMEPI(),
                                              dataEntrada.getFC_RECEP(), null,
                                              dataEntrada.getFC_FECNOTIF(),
                                              null, dataEntrada.getCD_OPE(),
                                              dataEntrada.getFC_ULTACT(),
                                              dataEntrada.getIT_VALIDADA(),
                                              dataEntrada.getFC_FECVALID(),
                                              sEnfermedad,
                                              new Integer(iNumTotal).toString(), null, null, null);
                  listaUsar.addElement(dataUsar);
                  iNumTotal = 0;
                } //0

                //DEMAS ELEMENTOS ---------------------------
                else {
                  boolean byaesta = false;
                  for (u = 0; u < listaUsar.size(); u++) {
                    dataUsar = (DataNumerica) listaUsar.elementAt(u);
                    if (dataUsar.getCD_ENFCIE().trim().equals(sEnfermedad)) {
                      byaesta = true;
                    }
                  }

                  if (byaesta) { //si esta en listaUsar, no se hace nada
                    //registro que ya sobra y esta almacenado
                  }
                  else {
                    //busco otras en Store
                    iNumTotal = (new Integer(dataEntrada.getNM_CASOS())).
                        intValue();

                    for (k = i + 1; k < listaStore.size(); k++) {
                      dataStore = (DataNumerica) listaStore.elementAt(k);
                      if (dataStore.getCD_ENFCIE().trim().equals(sEnfermedad)) {
                        iNumTotal = iNumTotal
                            + (new Integer(dataStore.getNM_CASOS())).intValue();
                      }
                    }
                    dataUsar = new DataNumerica("", "", "",
                                                dataEntrada.getCD_E_NOTIF(),
                                                dataEntrada.getCD_ANOEPI(),
                                                dataEntrada.getCD_SEMEPI(),
                                                dataEntrada.getFC_RECEP(), null,
                                                dataEntrada.getFC_FECNOTIF(),
                                                null, dataEntrada.getCD_OPE(),
                                                dataEntrada.getFC_ULTACT(),
                                                dataEntrada.getIT_VALIDADA(),
                                                dataEntrada.getFC_FECVALID(),
                                                sEnfermedad,
                                                new Integer(iNumTotal).toString(), null, null, null);
                    listaUsar.addElement(dataUsar);
                    iNumTotal = 0;

                  } //else

                } //else

              } //fin for ppal

              //COPIA   en param, la de usar
              param.removeAllElements();
              for (i = 0; i < listaUsar.size(); i++) {
                dataUsar = (DataNumerica) listaUsar.elementAt(i);
                param.addElement(dataUsar);
              }

//!!!!!!!!!!!!!!!!!!!!!!
//FIN RECOMPONER!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!

              //cargo en listaAlmacen lo que borrare
              CLista listaAlmacen = new CLista();
              DataNumerica dataAlmacen = null;
              DataNumerica dataHabra = null; //lo que llega de param
              boolean bEncontrado = false;

              for (i = 0; i < listaHabia.size(); i++) {
                dataHabia = (DataNumerica) listaHabia.elementAt(i);
                for (int iNueva = 0; iNueva < param.size(); iNueva++) {
                  dataHabra = (DataNumerica) param.elementAt(iNueva);
                  if (dataHabra.getCD_ENFCIE().trim().equals(dataHabia.
                      getCD_ENFCIE().trim())) {
                    bEncontrado = true; //modificacion
                    break;
                  }
                }
                if (!bEncontrado) {
                  dataAlmacen = (DataNumerica) listaHabia.elementAt(i);
                  listaAlmacen.addElement(dataAlmacen);
                }
                bEncontrado = false;
              }

              for (int y = 0; y < listaAlmacen.size(); y++) {
                dataAlmacen = (DataNumerica) listaAlmacen.elementAt(y);
                //# // System_out.println("Borrare "+dataAlmacen.getCD_ENFCIE());
              }

              //listaAlmacen  borrar
              //listaHabia
              //param lo de usar

              for (i = 0; i < param.size(); i++) {
                dataUsar = (DataNumerica) param.elementAt(i);
                String enfermedad = dataUsar.getCD_ENFCIE();
                bEncontrado = false;

                for (int j = 0; j < listaHabia.size(); j++) {
                  dataHabia = (DataNumerica) listaHabia.elementAt(j);

                  if (dataHabia.getCD_ENFCIE().equals(enfermedad)) {
                    //lo modifica
                    if (!dataUsar.getNM_CASOS().equals(dataHabia.getNM_CASOS())) {
                      listaModif.addElement(dataUsar);
                    }
                    //no lo modifica
                    else {
                      //la dejo tal cual
                    }
                    bEncontrado = true;
                    break;
                  }
                } //for usar

                if (!bEncontrado) {
                  listaInsertar.addElement(dataUsar);

                }
              } //for habia

              //listaInsertar
              //listaModif

              //INSERTANDO LOS NUEVOS DATOS
              for (i = 0; i < listaInsertar.size(); i++) {

                dataInsertar = (DataNumerica) listaInsertar.elementAt(i);
                //# // System_out.println("Insertar enfer" + dataInsertar.getCD_ENFCIE());

                sQuery = Pregunta_Alta_Edonum();
                st = con.prepareStatement(sQuery);

                // c�digo de enfermedad
                st.setString(1, dataInsertar.getCD_ENFCIE().trim());
                // c�digo de equipo notificador
                st.setString(2, dataInsertar.getCD_E_NOTIF().trim());
                // a�o
                st.setString(3, dataInsertar.getCD_ANOEPI().trim());
                // semana
                st.setString(4, dataInsertar.getCD_SEMEPI().trim());
                // fecha de recepci�n
                dFecha = formater.parse(dataInsertar.getFC_RECEP().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(5, sqlFec);
                // fecha de notificaci�n
                dFecha = formater.parse(dataInsertar.getFC_FECNOTIF().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(6, sqlFec);

                // n�mero de casos
                st.setInt(7,
                          (new Integer(dataInsertar.getNM_CASOS().trim())).intValue());
                // c�d. ope.
                st.setString(8, dataInsertar.getCD_OPE().trim());
                // fecha �ltima actualizaci�n
                /*dFecha = formater.parse(dataEntrada.getFC_ULTACT().trim());
                           sqlFec = new java.sql.Date(dFecha.getTime());
                           st.setDate(9, sqlFec);*/
                st.setTimestamp(9, TSFec);

                // lanza la query
                st.executeUpdate();
                st.close();
                st = null;
              } //for

              //modificamos LOS DATOS
              for (i = 0; i < listaModif.size(); i++) {
                dataModif = (DataNumerica) listaModif.elementAt(i);

                //# // System_out.println("Modif enfer" + dataModif.getCD_ENFCIE());
                sQuery = Pregunta_Modif_Edonum();
                st = con.prepareStatement(sQuery);

                st.setInt(1,
                          (new Integer(dataModif.getNM_CASOS().trim())).intValue());
                st.setString(2, dataModif.getCD_OPE().trim());
                st.setTimestamp(3, TSFec);

                st.setString(4, dataModif.getCD_ENFCIE().trim());
                st.setString(5, dataModif.getCD_E_NOTIF().trim());
                st.setString(6, dataModif.getCD_ANOEPI().trim());
                st.setString(7, dataModif.getCD_SEMEPI().trim());
                // fecha de recepci�n
                dFecha = formater.parse(dataModif.getFC_RECEP().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(8, sqlFec);
                // fecha de notificaci�n
                dFecha = formater.parse(dataModif.getFC_FECNOTIF().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(9, sqlFec);

                // lanza la query
                st.executeUpdate();
                st.close();
                st = null;
              } //for

              /* DataNumerica data = null;
               for (int y=0; y < param.size(); y++){
                   data = (DataNumerica) param.elementAt(y);
               } */

              //INSERTO EN EL HISTORICO
              //previamente, necesito el max de la tabla
              sQuery = "SELECT MAX(NM_SECBAJA) "
                  + "FROM SIVE_BAJASEDONUM WHERE NM_SECBAJA < ?";

              st = con.prepareStatement(sQuery);
              Integer NumMax = new Integer(999999);
              st.setInt(1, NumMax.intValue());
              rs = st.executeQuery();

              String sSecuencial = "";
              int iSecuencial = 0;

              if (rs.next()) {
                if (rs == null) {
                  iSecuencial = 1;
                  sSecuencial = "1";
                }
                else {
                  iSecuencial = rs.getInt(1) + 1;
                  Integer ISecuencial = new Integer(iSecuencial);
                  sSecuencial = ISecuencial.toString();
                }
              }

              rs.close();
              rs = null;
              st.close();
              st = null;

              //insertamos
              int iSecEncadaMomento = 0;
              for (i = 0; i < listaAlmacen.size(); i++) {
                dataAlmacen = (DataNumerica) listaAlmacen.elementAt(i);
                sQuery = Pregunta_Alta_BajasEdonum();
                st = con.prepareStatement(sQuery);

                iSecEncadaMomento = iSecuencial + i;

                st.setInt(1, iSecEncadaMomento);
                st.setString(2, dataAlmacen.getCD_ENFCIE().trim());
                st.setString(3, dataAlmacen.getCD_E_NOTIF().trim());
                st.setString(4, dataAlmacen.getCD_ANOEPI().trim());
                st.setString(5, dataAlmacen.getCD_SEMEPI().trim());
                dFecha = formater.parse(dataAlmacen.getFC_RECEP().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(6, sqlFec);

                dFecha = formater.parse(dataAlmacen.getFC_FECNOTIF().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(7, sqlFec);

                st.setInt(8,
                          (new Integer(dataAlmacen.getNM_CASOS().trim())).intValue());

                st.setString(9, dataAlmacen.getCD_OPE().trim());

                /*dFecha = formater.parse(dataAlmacen.getFC_ULTACT().trim());
                           sqlFec = new java.sql.Date(dFecha.getTime());
                           st.setDate(10, sqlFec);*/
                st.setTimestamp(10,
                                DeString_ATimeStamp(dataAlmacen.getFC_ULTACT().
                    trim()));

                // lanza la query
                st.executeUpdate();
                st.close();
                st = null;

              } //for

              //borramos resp_adic y edo_dadic, a partir de listaAlmacen
              for (i = 0; i < listaAlmacen.size(); i++) {

                dataAlmacen = (DataNumerica) listaAlmacen.elementAt(i);
                //# // System_out.println("Borrare enfer" + dataAlmacen.getCD_ENFCIE());

                sQuery = "delete from SIVE_RESP_ADIC " +
                    " where CD_ENFCIE = ? and CD_E_NOTIF = ?" +
                    " and CD_ANOEPI = ? and CD_SEMEPI = ?" +
                    " and FC_RECEP = ? and FC_FECNOTIF = ?";

                st = con.prepareStatement(sQuery);
                st.setString(1, dataAlmacen.getCD_ENFCIE().trim());
                st.setString(2, dataAlmacen.getCD_E_NOTIF().trim());
                st.setString(3, dataAlmacen.getCD_ANOEPI().trim());
                st.setString(4, dataAlmacen.getCD_SEMEPI().trim());
                // fecha de recepci�n
                dFecha = formater.parse(dataAlmacen.getFC_RECEP().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(5, sqlFec);
                // fecha de notificaci�n
                dFecha = formater.parse(dataAlmacen.getFC_FECNOTIF().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(6, sqlFec);

                st.executeUpdate();
                st.close();
              }

              for (i = 0; i < listaAlmacen.size(); i++) {
                dataAlmacen = (DataNumerica) listaAlmacen.elementAt(i);
                sQuery = "delete from SIVE_EDO_DADIC " +
                    " where CD_ENFCIE = ? and CD_E_NOTIF = ?" +
                    " and CD_ANOEPI = ? and CD_SEMEPI = ?" +
                    " and FC_RECEP = ? and FC_FECNOTIF = ?";

                st = con.prepareStatement(sQuery);
                st.setString(1, dataAlmacen.getCD_ENFCIE().trim());
                st.setString(2, dataAlmacen.getCD_E_NOTIF().trim());
                st.setString(3, dataAlmacen.getCD_ANOEPI().trim());
                st.setString(4, dataAlmacen.getCD_SEMEPI().trim());
                // fecha de recepci�n
                dFecha = formater.parse(dataAlmacen.getFC_RECEP().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(5, sqlFec);
                // fecha de notificaci�n
                dFecha = formater.parse(dataAlmacen.getFC_FECNOTIF().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(6, sqlFec);

                st.executeUpdate();
                st.close();
              }
              //--------------------------------------------------------

              //borramos las de edonum que procedan, despues de...dadic
              //a partir de lista almacen
              for (i = 0; i < listaAlmacen.size(); i++) {
                dataAlmacen = (DataNumerica) listaAlmacen.elementAt(i);

                sQuery = "delete from SIVE_EDONUM " +
                    " where CD_ENFCIE = ? and CD_E_NOTIF = ?" +
                    " and CD_ANOEPI = ? and CD_SEMEPI = ?" +
                    " and FC_RECEP = ? and FC_FECNOTIF = ?";

                st = con.prepareStatement(sQuery);

                st.setString(1, dataAlmacen.getCD_ENFCIE().trim());
                st.setString(2, dataAlmacen.getCD_E_NOTIF().trim());
                st.setString(3, dataAlmacen.getCD_ANOEPI().trim());
                st.setString(4, dataAlmacen.getCD_SEMEPI().trim());
                // fecha de recepci�n
                dFecha = formater.parse(dataAlmacen.getFC_RECEP().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(5, sqlFec);
                // fecha de notificaci�n
                dFecha = formater.parse(dataAlmacen.getFC_FECNOTIF().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(6, sqlFec);

                st.executeUpdate();
                st.close();
              } //borrar edonum

            }
            //si quiere borrar todas las numericas
            else if (dataEntrada.getCD_ENFCIE().trim().equals("")) {

              //# // System_out.println("Quiero borrar todo");

              //INSERTO EN EL HISTORICO
              //previamente, necesito el max de la tabla
              sQuery = "SELECT MAX(NM_SECBAJA) "
                  + "FROM SIVE_BAJASEDONUM WHERE NM_SECBAJA < ?";

              st = con.prepareStatement(sQuery);
              Integer NumMax = new Integer(999999);
              st.setInt(1, NumMax.intValue());
              rs = st.executeQuery();

              String sSecuencial = "";
              int iSecuencial = 0;

              if (rs.next()) {
                if (rs == null) {
                  iSecuencial = 1;
                  sSecuencial = "1";
                }
                else {
                  iSecuencial = rs.getInt(1) + 1;
                  Integer ISecuencial = new Integer(iSecuencial);
                  sSecuencial = ISecuencial.toString();
                }
              }

              rs.close();
              rs = null;
              st.close();
              st = null;

              //insertamos
              int iSecEncadaMomento = 0;
              for (i = 0; i < listaHabia.size(); i++) {

                dataHabia = (DataNumerica) listaHabia.elementAt(i);
                sQuery = Pregunta_Alta_BajasEdonum();
                st = con.prepareStatement(sQuery);

                iSecEncadaMomento = iSecuencial + i;

                st.setInt(1, iSecEncadaMomento);
                st.setString(2, dataHabia.getCD_ENFCIE().trim());
                st.setString(3, dataHabia.getCD_E_NOTIF().trim());
                st.setString(4, dataHabia.getCD_ANOEPI().trim());
                st.setString(5, dataHabia.getCD_SEMEPI().trim());
                dFecha = formater.parse(dataHabia.getFC_RECEP().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(6, sqlFec);

                dFecha = formater.parse(dataHabia.getFC_FECNOTIF().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(7, sqlFec);

                st.setInt(8,
                          (new Integer(dataHabia.getNM_CASOS().trim())).intValue());

                st.setString(9, dataHabia.getCD_OPE().trim());

                /*dFecha = formater.parse(dataHabia.getFC_ULTACT().trim());
                           sqlFec = new java.sql.Date(dFecha.getTime());
                           st.setDate(10, sqlFec);*/
                st.setTimestamp(10,
                                DeString_ATimeStamp(dataHabia.getFC_ULTACT().trim()));

                // lanza la query
                st.executeUpdate();
                st.close();
                st = null;

              } //for del historico

              //# // System_out.println("voy a borrar");
              //borramos resp_adic y edo_dadic, a partir de listaAlmacen
              for (i = 0; i < listaHabia.size(); i++) {
                dataHabia = (DataNumerica) listaHabia.elementAt(i);

                //# // System_out.println("SIVE_RESP_ADIC dataHabia.getCD_ENFCIE().trim() " + dataHabia.getCD_ENFCIE().trim());

                sQuery = "delete from SIVE_RESP_ADIC " +
                    " where CD_ENFCIE = ? and CD_E_NOTIF = ?" +
                    " and CD_ANOEPI = ? and CD_SEMEPI = ?" +
                    " and FC_RECEP = ? and FC_FECNOTIF = ?";

                st = con.prepareStatement(sQuery);
                st.setString(1, dataHabia.getCD_ENFCIE().trim());
                st.setString(2, dataHabia.getCD_E_NOTIF().trim());
                st.setString(3, dataHabia.getCD_ANOEPI().trim());
                st.setString(4, dataHabia.getCD_SEMEPI().trim());
                // fecha de recepci�n
                dFecha = formater.parse(dataHabia.getFC_RECEP().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(5, sqlFec);
                // fecha de notificaci�n
                dFecha = formater.parse(dataHabia.getFC_FECNOTIF().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(6, sqlFec);

                st.executeUpdate();
                st.close();
              }

              for (i = 0; i < listaHabia.size(); i++) {
                dataHabia = (DataNumerica) listaHabia.elementAt(i);

                //# // System_out.println("SIVE_EDO_DADIC dataHabia.getCD_ENFCIE().trim() " + dataHabia.getCD_ENFCIE().trim());

                sQuery = "delete from SIVE_EDO_DADIC " +
                    " where CD_ENFCIE = ? and CD_E_NOTIF = ?" +
                    " and CD_ANOEPI = ? and CD_SEMEPI = ?" +
                    " and FC_RECEP = ? and FC_FECNOTIF = ?";

                st = con.prepareStatement(sQuery);
                st.setString(1, dataHabia.getCD_ENFCIE().trim());
                st.setString(2, dataHabia.getCD_E_NOTIF().trim());
                st.setString(3, dataHabia.getCD_ANOEPI().trim());
                st.setString(4, dataHabia.getCD_SEMEPI().trim());
                // fecha de recepci�n
                dFecha = formater.parse(dataHabia.getFC_RECEP().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(5, sqlFec);
                // fecha de notificaci�n
                dFecha = formater.parse(dataHabia.getFC_FECNOTIF().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(6, sqlFec);

                st.executeUpdate();
                st.close();
              }

              //Borro en EDONUM!!!!!!!!!!!!!!!!!!!!!
              dataEntrada = (DataNumerica) param.firstElement();
              sQuery = Pregunta_Borra_Edonum();
              st = con.prepareStatement(sQuery);

              //BORRANDO //WHERE

              // c�digo de equipo
              st.setString(1, dataEntrada.getCD_E_NOTIF().trim());
              // a�o
              st.setString(2, dataEntrada.getCD_ANOEPI().trim());
              // semana
              st.setString(3, dataEntrada.getCD_SEMEPI().trim());
              // fecha de recepci�n
              dFecha = formater.parse(dataEntrada.getFC_RECEP().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(4, sqlFec);
              // fecha de notificaci�n
              dFecha = formater.parse(dataEntrada.getFC_FECNOTIF().trim());
              sqlFec = new java.sql.Date(dFecha.getTime());
              st.setDate(5, sqlFec);

              st.executeUpdate();
              st.close();

              //--------------------------------------------------------
            }

          } //fin del if de SOLO MODIFICAR NOTIFICACION

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    //si algo se ha actualizado o dado de alta, ha sido con fecha: sFecha_Actual
    DataNumerica datosdeSalida = new DataNumerica("", "", "", "", "", "", "",
                                                  "", "", "",
                                                  "", sFecha_Actual, "", "", "",
                                                  "", "", "", "");
    listaSalida.addElement(datosdeSalida);
    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;
  } //fin doWork

  public String Pregunta_Alta_Notifedo() {

    return ("insert into SIVE_NOTIFEDO (CD_E_NOTIF, CD_ANOEPI," +
            " CD_SEMEPI, FC_RECEP, NM_NNOTIFR, FC_FECNOTIF," +
            " IT_RESSEM, CD_OPE, FC_ULTACT, IT_VALIDADA, FC_FECVALID)" +
            " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

  }

  public String Pregunta_Alta_Edonum() {
    return ("insert into SIVE_EDONUM (CD_ENFCIE, CD_E_NOTIF," +
            " CD_ANOEPI, CD_SEMEPI, FC_RECEP, FC_FECNOTIF," +
            " NM_CASOS, CD_OPE, FC_ULTACT)" +
            " values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
  }

  public String Pregunta_Modif_Edonum() {

    return ("UPDATE SIVE_EDONUM SET NM_CASOS = ?, " +
            "  CD_OPE = ?, FC_ULTACT = ?" +
            " WHERE (CD_ENFCIE = ? and CD_E_NOTIF = ? and " +
            " CD_ANOEPI = ? and CD_SEMEPI = ? and " +
            " FC_RECEP = ? and FC_FECNOTIF = ?)");

  }

  public String Pregunta_Modifica_Notifedo() {

    return ("UPDATE SIVE_NOTIFEDO SET NM_NNOTIFR = ?, " +
        " IT_RESSEM = ?, CD_OPE = ?, FC_ULTACT = ?, IT_VALIDADA = ?, FC_FECVALID = ? " +
        " WHERE CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ?" +
        " and FC_RECEP= ? and  FC_FECNOTIF = ?");
  }

  public String Pregunta_Borra_Edonum() {

    return ("delete from SIVE_EDONUM " +
            " where CD_E_NOTIF = ?" +
            " and CD_ANOEPI = ? and CD_SEMEPI = ?" +
            " and FC_RECEP = ? and FC_FECNOTIF = ?");
  }

  public String Pregunta_Select_Previa_Edonum() {
//selecciono enfermedad y datos, para esa notificacion

    return ("select CD_ENFCIE, NM_CASOS, CD_OPE, FC_ULTACT from SIVE_EDONUM " +
            " where CD_E_NOTIF = ?" +
            " and CD_ANOEPI = ? and CD_SEMEPI = ?" +
            " and FC_RECEP = ? and FC_FECNOTIF = ?");
  }

  public String Pregunta_Alta_BajasEdonum() {
    return ("insert into SIVE_BAJASEDONUM ( NM_SECBAJA, CD_ENFCIE," +
            " CD_E_NOTIF," +
            " CD_ANOEPI, CD_SEMEPI, FC_RECEP, FC_FECNOTIF," +
            " NM_CASOS, CD_OPE, FC_ULTACT)" +
            " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
  }

  public String Pregunta_Select_notif_sem() {
    return ("select CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, NM_NNOTIFT " +
            " NM_NTOTREAL, CD_OPE, FC_ULTACT " +
            " FROM SIVE_NOTIF_SEM WHERE (CD_E_NOTIF = ? AND " +
            " CD_ANOEPI = ? AND CD_SEMEPI = ?) ");

  }

  public String Pregunta_Alta_notif_sem() {
    return ("insert into SIVE_NOTIF_SEM " +
            " (CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, NM_NNOTIFT, " +
            " NM_NTOTREAL, CD_OPE, FC_ULTACT) " +
            " values (?,?,?,?,?,?,?) ");

  }

  public String Pregunta_Modif_notif_sem() {

    return ("UPDATE SIVE_NOTIF_SEM SET NM_NNOTIFT = ?, " +
            " NM_NTOTREAL = ?, CD_OPE = ?, FC_ULTACT = ?" +
            " WHERE (CD_E_NOTIF = ? and CD_ANOEPI = ? and CD_SEMEPI = ?)");

  }

  public String Pregunta_Modif_e_notif() {

    return ("UPDATE SIVE_E_NOTIF SET NM_NOTIFT = ?, " +
            " CD_OPE = ?, FC_ULTACT = ?" +
            " WHERE CD_E_NOTIF = ? ");

  }
} //fin de la clase
