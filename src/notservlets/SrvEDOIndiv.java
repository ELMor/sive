package notservlets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

// LORTAD
import Registro.RegistroConsultas;
import capp.CApp;
import capp.CLista;
import notdata.DataCasoEDO;
import notdata.DataListaEDOIndiv;
import sapp.DBServlet;

//Entrada: equipo, anno, semana, fecha recep, fecha notif
//Salida: (3), nm_edo, cod_enfermedad, des_enfermedad,
//             cd_enfermo, Iniciales, fecha_nacimiento
//        (7) + fechas recep y notif
public class SrvEDOIndiv
    extends DBServlet {

  // modos de operaci�n del servlet
  final int servletSELECCION_LISTA_EDO_INDIV = 3;
  final int servletSELECCION_LISTA_EDO_FECHA_INDIV = 7;
  final int servletSELECCION_DIALOGO_EDO = 21;
  final int servletSELECCION_LISTA_CLASIFEDO = 23;

  // objetos de datos
  CLista listaSalida = null;
  CLista listaEntrada = new CLista();
  DataListaEDOIndiv dataEntrada = null;
  DataListaEDOIndiv dataSalida = null;
  DataCasoEDO dataDialogoEDO = new DataCasoEDO();
  DataCasoEDO dataExpediente = new DataCasoEDO();

  // par�metros
  String sQuery = ""; // Query para mostrar descripci�n de diagn�stico
  String sQueryNot = ""; //Query para mostrar en la lista el primer declarante
  java.util.Date dFecha;
  java.sql.Date sqlFec;
  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

  // objetos JDBC
  Connection con = null;
  PreparedStatement st = null;
  ResultSet rs = null;
  int i = 0;

  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    //java.sql.Timestamp TSFec = new java.sql.Timestamp(dFecha.getTime());
    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);

    return TSFec;
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    /****************** APLICACION DE LORTAD ********************************/
    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;
          /****************** APLICACION DE LORTAD ********************************/

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);
    con.setAutoCommit(false);
    listaSalida = new CLista();

    //datos
    String sCodEquipo = "";
    String sAnno = "";
    String sSemana = "";
    String sFechaRecep = "";
    String sFechaNotif = "";
    String sExpediente = "";
    String sCodEnfermedad = "";
    String sDesEnfermedad = "";
    String sCodEnfermo = "";
    String sIniciales = "";
    String sFechaNacto = "";

    String FC_NAC = "", FC_INISNT = "";
    java.util.Date dFC_NAC;
    java.util.Date dFC_INISNT;

    String sDesCla = "";
    String sDesClaL = "";

    try {
      switch (opmode) {

        case servletSELECCION_LISTA_EDO_INDIV:

          dataEntrada = (DataListaEDOIndiv) param.firstElement();
          sQuery = "select NM_EDO, FC_RECEP, FC_FECNOTIF" +
              " from SIVE_NOTIF_EDOI " +
              " where CD_E_NOTIF=? and CD_ANOEPI=? " +
              " and CD_SEMEPI=? and CD_FUENTE = 'E' ";

          st = con.prepareStatement(sQuery);

          st.setString(1, dataEntrada.getCodEquipo().trim());
          st.setString(2, dataEntrada.getAnno().trim());
          st.setString(3, dataEntrada.getSemana().trim());

          rs = st.executeQuery();

          // recogida de datos ( en objeto de tipo DataListaEDOIndiv)
          while (rs.next()) {

            dataSalida = new DataListaEDOIndiv(
                dataEntrada.getCodEquipo().trim(),
                dataEntrada.getAnno().trim(),
                dataEntrada.getSemana().trim(),
                formater.format(rs.getDate("FC_RECEP")), //null,
                formater.format(rs.getDate("FC_FECNOTIF")), //null,
                (new Integer(rs.getInt("NM_EDO"))).toString(),
                null, null, null, null, null);

            listaSalida.addElement(dataSalida);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          // recorre la lista, y para cada expediente, recupera:
          // SIVE_EDOIND, la enfermedad y el enfermo
          sQuery = "select CD_ENFCIE, CD_ENFERMO from SIVE_EDOIND " +
              " where NM_EDO = ?";

          for (i = 0; i < listaSalida.size(); i++) {

            dataSalida = (DataListaEDOIndiv) listaSalida.elementAt(i);
            sExpediente = dataSalida.getExpediente().trim();
            // fechas
            sFechaRecep = dataSalida.getFechaRecep().trim();
            sFechaNotif = dataSalida.getFechaNotif().trim();

            st = con.prepareStatement(sQuery);
            st.setInt(1, (new Integer(sExpediente)).intValue());

            if (sExpediente == null) {
              registroConsultas.insertarParametro("null");
            }
            else {
              registroConsultas.insertarParametro(sExpediente);
            }
            registroConsultas.registrar("SIVE_EDOIND",
                                        sQuery,
                                        "CD_ENFERMO",
                                        "",
                                        "SrvEDOIndiv",
                                        true);

            rs = st.executeQuery();

            listaSalida.removeElementAt(i);

            while (rs.next()) { //sera un registro

              dataSalida = new DataListaEDOIndiv(
                  dataEntrada.getCodEquipo().trim(),
                  dataEntrada.getAnno().trim(),
                  dataEntrada.getSemana().trim(),
                  sFechaRecep, //null,
                  sFechaNotif, //null,
                  sExpediente,
                  rs.getString("CD_ENFCIE"),
                  null,
                  rs.getString("CD_ENFERMO"), //(int)si es nulo va a nulo
                  null, null);
              listaSalida.insertElementAt(dataSalida, i);
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

          } //for

          //descripci�n de la enfermedad
          sQuery = "select "
              + " DS_PROCESO, DSL_PROCESO "
              + " FROM  SIVE_PROCESOS"
              + "  WHERE CD_ENFCIE = ?";

          for (i = 0; i < listaSalida.size(); i++) {

            dataSalida = (DataListaEDOIndiv) listaSalida.elementAt(i);

            if (dataSalida.getCodEnfermedad() != null) {

              sExpediente = dataSalida.getExpediente();
              sCodEnfermedad = dataSalida.getCodEnfermedad();
              sCodEnfermo = dataSalida.getCodEnfermo();

              // fechas
              sFechaRecep = dataSalida.getFechaRecep();
              sFechaNotif = dataSalida.getFechaNotif();

              st = con.prepareStatement(sQuery);
              st.setString(1, sCodEnfermedad);
              rs = st.executeQuery();

              listaSalida.removeElementAt(i);
              String des = "";
              String desL = "";
              while (rs.next()) {
                des = rs.getString("DS_PROCESO");
                desL = rs.getString("DSL_PROCESO");
                if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                    && (desL != null)) {
                  dataSalida = new DataListaEDOIndiv(
                      dataEntrada.getCodEquipo().trim(),
                      dataEntrada.getAnno().trim(),
                      dataEntrada.getSemana().trim(),
                      sFechaRecep, //null,
                      sFechaNotif, //null,
                      sExpediente, sCodEnfermedad,
                      desL,
                      sCodEnfermo, null, null);
                }
                else {
                  dataSalida = new DataListaEDOIndiv(
                      dataEntrada.getCodEquipo().trim(),
                      dataEntrada.getAnno().trim(),
                      dataEntrada.getSemana().trim(),
                      sFechaRecep, //null,
                      sFechaNotif, //null,
                      sExpediente, sCodEnfermedad,
                      des,
                      sCodEnfermo, null, null);

                }
                listaSalida.insertElementAt(dataSalida, i);

              }
              rs.close();
              rs = null;
              st.close();
              st = null;

            }

          } //fin for

          //Iniciales del enfermo si existe (!= null)
          sQuery = "select "
              + " SIGLAS, FC_NAC "
              + " FROM  SIVE_ENFERMO"
              + "  WHERE CD_ENFERMO = ?";

          for (i = 0; i < listaSalida.size(); i++) {

            dataSalida = (DataListaEDOIndiv) listaSalida.elementAt(i);
            if (dataSalida.getCodEnfermo() != null) {

              sExpediente = dataSalida.getExpediente();
              sCodEnfermedad = dataSalida.getCodEnfermedad();
              sDesEnfermedad = dataSalida.getDesEnfermedad();
              sCodEnfermo = dataSalida.getCodEnfermo();

              // fechas
              sFechaRecep = dataSalida.getFechaRecep();
              sFechaNotif = dataSalida.getFechaNotif();

              st = con.prepareStatement(sQuery);
              st.setString(1, sCodEnfermo);

              if (sCodEnfermo == null) {
                registroConsultas.insertarParametro("null");
              }
              else {
                registroConsultas.insertarParametro(sCodEnfermo);
              }
              registroConsultas.registrar("SIVE_ENFERMO",
                                          sQuery,
                                          "CD_ENFERMO",
                                          sCodEnfermo,
                                          "SrvEDOIndiv",
                                          true);
              rs = st.executeQuery();

              listaSalida.removeElementAt(i);

              while (rs.next()) {

                dFC_NAC = rs.getDate("FC_NAC");
                if (dFC_NAC == null) {
                  FC_NAC = null;
                }
                else {
                  FC_NAC = formater.format(dFC_NAC);

                }
                dataSalida = new DataListaEDOIndiv(
                    dataEntrada.getCodEquipo().trim(),
                    dataEntrada.getAnno().trim(),
                    dataEntrada.getSemana().trim(),
                    sFechaRecep, //null,
                    sFechaNotif, //null,
                    sExpediente, sCodEnfermedad, sDesEnfermedad,
                    sCodEnfermo, rs.getString("SIGLAS"),
                    FC_NAC);

                listaSalida.insertElementAt(dataSalida, i);

              }
              rs.close();
              rs = null;
              st.close();
              st = null;

            }
          } //fin for

          break;

        case servletSELECCION_LISTA_EDO_FECHA_INDIV:
          java.sql.Timestamp fec = null;

          dataEntrada = (DataListaEDOIndiv) param.firstElement();

          if (MAS.equals("||")) {

            /*  sQuery =  "select NM_EDO " +
                        " from SIVE_NOTIF_EDOI "+
                 " where CD_E_NOTIF  = '" + dataEntrada.getCodEquipo().trim() + "' and " +
                 "       CD_ANOEPI   = '" + dataEntrada.getAnno().trim() + "' and " +
                 "       CD_SEMEPI   = '" + dataEntrada.getSemana().trim() + "' and " +
                        "       FC_RECEP    = TO_DATE('" + dataEntrada.getFechaRecep().trim() + "','DD/MM/YYYY') and " +
                        "       FC_FECNOTIF = TO_DATE('" + dataEntrada.getFechaNotif().trim() + "','DD/MM/YYYY') and " +
                        "       CD_FUENTE = 'E' ";
             */

            // modificacion para recuperarlo con to_char
            sQuery = "select NM_EDO " +
                " from SIVE_NOTIF_EDOI " +
                " where CD_E_NOTIF  = '" + dataEntrada.getCodEquipo().trim() +
                "' and " +
                "       CD_ANOEPI   = '" + dataEntrada.getAnno().trim() +
                "' and " +
                "       CD_SEMEPI   = '" + dataEntrada.getSemana().trim() +
                "' and " +
                "       TO_CHAR(FC_RECEP,'DD/MM/YYYY') = '" +
                dataEntrada.getFechaRecep().trim() + "' and " +
                "       TO_CHAR(FC_FECNOTIF,'DD/MM/YYYY') = '" +
                dataEntrada.getFechaNotif().trim() + "' and " +
                "       CD_FUENTE = 'E' ";

            st = con.prepareStatement(sQuery);
          }
          else {
            sQuery = "select NM_EDO from SIVE_NOTIF_EDOI" +
                " where CD_E_NOTIF = ? and CD_ANOEPI = ?" +
                " and CD_SEMEPI = ? and FC_RECEP = ?" +
                " and FC_FECNOTIF = ? and CD_FUENTE = 'E' ";

            st = con.prepareStatement(sQuery);

            st.setString(1, dataEntrada.getCodEquipo().trim());
            st.setString(2, dataEntrada.getAnno().trim());
            st.setString(3, dataEntrada.getSemana().trim());

            dFecha = formater.parse(dataEntrada.getFechaRecep().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(4, sqlFec);

            dFecha = formater.parse(dataEntrada.getFechaNotif().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(5, sqlFec);
          }

          rs = st.executeQuery();

          // recogida de datos ( en objeto de tipo DataListaEDOIndiv)
          while (rs.next()) {
            dataSalida = new DataListaEDOIndiv(
                dataEntrada.getCodEquipo().trim(),
                dataEntrada.getAnno().trim(),
                dataEntrada.getSemana().trim(),
                dataEntrada.getFechaRecep().trim(),
                dataEntrada.getFechaNotif().trim(),
                (new Integer(rs.getInt("NM_EDO"))).toString(),
                null, null, null, null, null);

            listaSalida.addElement(dataSalida);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          // recorre la lista, y para cada expediente, recupera:
          // SIVE_EDOIND, la enfermedad y el enfermo
          sQuery = "select CD_ENFCIE, CD_ENFERMO from SIVE_EDOIND " +
              " where NM_EDO = ?";

          for (i = 0; i < listaSalida.size(); i++) {

            dataSalida = (DataListaEDOIndiv) listaSalida.elementAt(i);
            sExpediente = dataSalida.getExpediente().trim();

            st = con.prepareStatement(sQuery);
            st.setInt(1, (new Integer(sExpediente)).intValue());

            if (sExpediente == null) {
              registroConsultas.insertarParametro("null");
            }
            else {
              registroConsultas.insertarParametro(sExpediente);

            }
            registroConsultas.registrar("SIVE_EDOIND",
                                        sQuery,
                                        "CD_ENFERMO",
                                        "",
                                        "SrvEDOIndiv",
                                        true);
            rs = st.executeQuery();

            listaSalida.removeElementAt(i);
            while (rs.next()) { //sera un registro
              dataSalida = new DataListaEDOIndiv(
                  dataEntrada.getCodEquipo().trim(),
                  dataEntrada.getAnno().trim(),
                  dataEntrada.getSemana().trim(),
                  dataEntrada.getFechaRecep().trim(),
                  dataEntrada.getFechaNotif().trim(),
                  sExpediente,
                  rs.getString("CD_ENFCIE"),
                  null, rs.getString("CD_ENFERMO"), null,
                  null);
              listaSalida.insertElementAt(dataSalida, i);
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
          } //for

          //decripcion de la enfermedad
          sQuery = "select "
              + " DS_PROCESO, DSL_PROCESO "
              + " FROM  SIVE_PROCESOS"
              + "  WHERE CD_ENFCIE = ?";

          for (i = 0; i < listaSalida.size(); i++) {

            dataSalida = (DataListaEDOIndiv) listaSalida.elementAt(i);
            if (dataSalida.getCodEnfermedad() != null) {
              sExpediente = dataSalida.getExpediente();
              sCodEnfermedad = dataSalida.getCodEnfermedad();
              sCodEnfermo = dataSalida.getCodEnfermo();

              st = con.prepareStatement(sQuery);
              st.setString(1, sCodEnfermedad);
              rs = st.executeQuery();

              listaSalida.removeElementAt(i);
              while (rs.next()) {
                if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                    && (rs.getString("DSL_PROCESO") != null)) {
                  dataSalida = new DataListaEDOIndiv(
                      dataEntrada.getCodEquipo().trim(),
                      dataEntrada.getAnno().trim(),
                      dataEntrada.getSemana().trim(),
                      dataEntrada.getFechaRecep().trim(),
                      dataEntrada.getFechaNotif().trim(),
                      sExpediente, sCodEnfermedad,
                      rs.getString("DSL_PROCESO"),
                      sCodEnfermo, null, null);
                }
                else {
                  dataSalida = new DataListaEDOIndiv(
                      dataEntrada.getCodEquipo().trim(),
                      dataEntrada.getAnno().trim(),
                      dataEntrada.getSemana().trim(),
                      dataEntrada.getFechaRecep().trim(),
                      dataEntrada.getFechaNotif().trim(),
                      sExpediente, sCodEnfermedad,
                      rs.getString("DS_PROCESO"),
                      sCodEnfermo, null, null);

                }
                listaSalida.insertElementAt(dataSalida, i);

              }
              rs.close();
              rs = null;
              st.close();
              st = null;

            }
          } //fin for

          //Iniciales del enfermo si existe (!= null)
          sQuery = "select "
              + " SIGLAS, FC_NAC "
              + " FROM  SIVE_ENFERMO"
              + "  WHERE CD_ENFERMO = ?";

          for (i = 0; i < listaSalida.size(); i++) {

            dataSalida = (DataListaEDOIndiv) listaSalida.elementAt(i);
            if (dataSalida.getCodEnfermo() != null) {

              sExpediente = dataSalida.getExpediente();
              sCodEnfermedad = dataSalida.getCodEnfermedad();
              sDesEnfermedad = dataSalida.getDesEnfermedad();
              sCodEnfermo = dataSalida.getCodEnfermo();

              st = con.prepareStatement(sQuery);
              st.setString(1, sCodEnfermo);

              if (sCodEnfermo == null) {
                registroConsultas.insertarParametro("null");
              }
              else {
                registroConsultas.insertarParametro(sCodEnfermo);
              }
              registroConsultas.registrar("SIVE_ENFERMO",
                                          sQuery,
                                          "CD_ENFERMO",
                                          sCodEnfermo,
                                          "SrvEDOIndiv",
                                          true);

              rs = st.executeQuery();

              listaSalida.removeElementAt(i);
              while (rs.next()) {

                dFC_NAC = rs.getDate("FC_NAC");
                if (dFC_NAC == null) {
                  FC_NAC = null;
                }
                else {
                  FC_NAC = formater.format(dFC_NAC);

                }
                dataSalida = new DataListaEDOIndiv(
                    dataEntrada.getCodEquipo().trim(),
                    dataEntrada.getAnno().trim(),
                    dataEntrada.getSemana().trim(),
                    dataEntrada.getFechaRecep().trim(),
                    dataEntrada.getFechaNotif().trim(),
                    sExpediente, sCodEnfermedad, sDesEnfermedad,
                    sCodEnfermo, rs.getString("SIGLAS"),
                    FC_NAC);

                listaSalida.insertElementAt(dataSalida, i);

              }
              rs.close();
              rs = null;
              st.close();
              st = null;

            }
          } //fin for

          break;

        case servletSELECCION_DIALOGO_EDO:

          //QQ:
          // recogemos los datos de entrada
          dataDialogoEDO = (DataCasoEDO) param.firstElement();

          String sCod_Enfermedad = dataDialogoEDO.sCodEnfermedad;
          // String sDes_Enfermedad= dataDialogoEDO.sDesEnfermedad;

          // Construimos la query.
          // Comprobar igualaci�n de it_primero.

          sQuery = "select a.NM_EDO, a.CD_CLASIFDIAG," +
              " a.FC_FECNOTIF, a.FC_INISNT, b.CD_E_NOTIF" +
              " from SIVE_EDOIND a, SIVE_NOTIF_EDOI b" +
              " where a.CD_ENFERMO = ? and a.CD_ENFCIE = ?" +
              " and b.IT_PRIMERO=? and a.NM_EDO = b.NM_EDO" +
              " and b.CD_FUENTE = 'E' ";

          st = con.prepareStatement(sQuery);
          st.setInt(1,
                    (new Integer(dataDialogoEDO.sCodEnfermo.trim())).intValue());
          st.setString(2, dataDialogoEDO.sCodEnfermedad.trim());
          st.setString(3, "S");

          registroConsultas.insertarParametro(dataDialogoEDO.sCodEnfermo.trim());
          registroConsultas.insertarParametro(dataDialogoEDO.sCodEnfermedad.
                                              trim());
          registroConsultas.insertarParametro("S");

          registroConsultas.registrar("SIVE_EDOIND",
                                      sQuery,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvEDOIndiv",
                                      true);

          rs = st.executeQuery();

          while (rs.next()) {
            // Inicio de s�ntomas
            dFC_INISNT = rs.getDate("FC_INISNT");
            if (dFC_INISNT == null) {
              FC_INISNT = null;
            }
            else {
              FC_INISNT = formater.format(dFC_INISNT);

            }

            listaSalida.addElement(new DataCasoEDO(dataDialogoEDO.sCodEnfermo.
                trim(),
                "", //Antes DS_APE1
                "", //Antes DS_APE2
                "", //Antes DS_NOMBRE
                "",
                rs.getInt("NM_EDO"),
                "", "", true, false, false,
                "", //Antes, fecha nacimiento
                formater.format(rs.getDate("FC_FECNOTIF")),
                rs.getString("CD_CLASIFDIAG"),
                null,
                FC_INISNT,
                "", "", //Codigo, desc centro
                rs.getString("CD_E_NOTIF"), "", //Codigo, desc equipo
                "", "")); //Area, Distrito
            //Ojo, a�adido cd_e_notif.

          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //QUERY DEL JOIN ie DS_CLASIFDIAG
          sQuery = "select "
              + " DS_CLASIFDIAG "
              + " FROM  SIVE_CLASIF_EDO"
              + "  WHERE CD_CLASIFDIAG = ?";

          //Query del primer notificador:
          sQueryNot = " select "
              + "a.DS_CENTRO, b.DS_E_NOTIF, b.CD_NIVEL_1,b.CD_NIVEL_2"
              + " FROM sive_c_notif a, sive_e_notif b"
              + " WHERE b.CD_E_NOTIF=? "
              + " and a.cd_centro=b.cd_centro";

          for (i = 0; i < listaSalida.size(); i++) {
            dataExpediente = (DataCasoEDO) listaSalida.elementAt(i);
            // valores de enfermedad
            // dataExpediente.sCodEnfermedad=sCod_Enfermedad;
            // dataExpediente.sDesEnfermedad=sDes_Enfermedad;

            //clasif
            if (dataExpediente.sCodClasif != null) {
              st = con.prepareStatement(sQuery);
              st.setString(1, dataExpediente.sCodClasif);
              rs = st.executeQuery();
              while (rs.next()) {

                //_______________________

                sDesCla = rs.getString("DS_CLASIFDIAG");

                // obtiene la descripcion auxiliar en funci�n del idioma
              }
              if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
                sDesClaL = rs.getString("DSL_CLASIFDIAG");
                if (sDesClaL != null) {
                  sDesCla = sDesClaL;
                }
              }
              //_______________________
              //         dataExpediente.sDesClasif = rs.getString("DS_CLASIFDIAG");

              dataExpediente.sDesClasif = sDesCla;

              rs.close();
              rs = null;
              st.close();
              st = null;
            }

            //Primer declarante:
            //Ojo, meter en la estructura sCodEquipo
            if (dataExpediente.sCodEquipo != null) {
              st = con.prepareStatement(sQueryNot);
              st.setString(1, dataExpediente.sCodEquipo);
              rs = st.executeQuery();
              int iAuxPrimeros = 0;
              while (rs.next()) {
                dataExpediente.sDesCentro = rs.getString("DS_CENTRO");
                dataExpediente.sDesEquipo = rs.getString("DS_E_NOTIF");
                dataExpediente.sCodN1 = rs.getString("CD_NIVEL_1");
                dataExpediente.sCodN2 = rs.getString("CD_NIVEL_2");
                iAuxPrimeros++;
              }
              if (iAuxPrimeros > 1) { // Por si hubieran aparecido varios primer-declarante
                dataExpediente.sDesCentro = "Err: Varios declarantes";
                dataExpediente.sDesEquipo = "  como declarante 1�.";
                dataExpediente.sCodN1 = "-";
                dataExpediente.sCodN2 = "-";
              }
            }
          } //fin for

          break;

        case servletSELECCION_LISTA_CLASIFEDO:
          dataSalida = new DataListaEDOIndiv();
          dataSalida = (DataListaEDOIndiv) param.firstElement();

          sQuery = "select CD_CLASIFDIAG, DS_CLASIFDIAG" +
              " from SIVE_CLASIF_EDO";

          st = con.prepareStatement(sQuery);

          // ejecuci�n de la query
          rs = st.executeQuery();

          while (rs.next()) {

            DataListaEDOIndiv dataInd = new DataListaEDOIndiv();
            dataInd.sCodEnfermedad = rs.getString("CD_CLASIFDIAG");

//          dataInd.sDesEnfermedad = rs.getString("DS_CLASIFDIAG");
            //_______________________

            sDesCla = rs.getString("DS_CLASIFDIAG");

            // obtiene la descripcion auxiliar en funci�n del idioma
            if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
              sDesClaL = rs.getString("DSL_CLASIFDIAG");
              if (sDesClaL != null) {
                sDesCla = sDesClaL;
              }
            }

            dataInd.sDesEnfermedad = sDesCla;
            //_______________________

            // a�ade un nodo con los datos recogidos
            listaSalida.addElement(dataInd);
          }
          rs.close();
          st.close();

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;

    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (listaSalida != null) {
      listaSalida.trimToSize();
    }
    return listaSalida;
  } //fin doWork
} //fin clase
