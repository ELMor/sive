package bipsuperior;

import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.CheckboxGroup;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

import bidata.DataAlerta;
import bidata.DataBrote;
import bidata.DataCDDS;
import bidial.DiaBIAlt;
import bidial.DialogInvesInfor;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.CheckboxControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import obj.CFechaSimple;
import obj.CHora;
import sapp.StubSrvBD;

public class PanelbiSuperior
    extends CPanel {

  //para contolar el cambio de la cmbo
  private String cd_grupo = "";
  ResourceBundle res;

  final int modoESPERA = 2;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;

  protected int modoOperacion = modoALTA;
  protected StubSrvBD stubCliente = null;

  //para el focus //para las cajas de texto  //para los botones
  focusAdapter focusAdap = new focusAdapter(this);
  actionAdapter actionAdap = new actionAdapter(this);
  itemListener itemList = new itemListener(this);

  // controles
  XYLayout xYLayout = new XYLayout();
  Label lblAno = new Label();
  TextField txtAno = new TextField();
  TextField txtCod = new TextField();
  ButtonControl btnClave = new ButtonControl();
  TextField txtDesc = new TextField();
  Label lblFNotif = new Label();
  CFechaSimple txtFecha = new CFechaSimple("S");
  CHora txtHora = new CHora("S");
  Label lGrupo = new Label();
  Choice choGrupo = new Choice();
  Label lTipoB = new Label();
  Choice choTipoB = new Choice();

  CheckboxGroup checkGroup = new CheckboxGroup();
  CheckboxControl checkAuto = new CheckboxControl();
  CheckboxControl checkManual = new CheckboxControl();

  // dialog que contiene a este panel
  DialogInvesInfor dlgB;
  Hashtable hs = null;

  //listas
  CLista listaGrupo = new CLista();
  CLista listaTBrotes = new CLista();
  Label lblResumen = new Label();

  // constructor
  public PanelbiSuperior(DialogInvesInfor dlg,
                         int modo,
                         Hashtable hsNecesarias) {
    try {

      setApp(dlg.getCApp());
      res = ResourceBundle.getBundle("bipsuperior.Res" + getApp().getIdioma());
      dlgB = dlg;
      hs = (Hashtable) hsNecesarias.clone();

      listaGrupo = (CLista) hs.get("GRUPOS");
      listaTBrotes = (CLista) hs.get("TBROTE");

      jbInit();

      Cargar_Grupo();
      Cargar_Tipo();

      modoOperacion = modo;
      Inicializar();

      cd_grupo = getGrupo();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    final String imgNAME[] = {
        "images/Magnify.gif"};

    CCargadorImagen imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    //obligatorios
    txtAno.setBackground(new Color(255, 255, 150));
    txtCod.setBackground(new Color(255, 255, 150));
    txtDesc.setBackground(new Color(255, 255, 150));
    txtHora.setBackground(new Color(255, 255, 150));
    txtFecha.setBackground(new Color(255, 255, 150));
    choGrupo.setBackground(new Color(255, 255, 150));
    choTipoB.setBackground(new Color(255, 255, 150));

    this.setSize(new Dimension(755, 92));
    xYLayout.setHeight(92);
    txtCod.setText("");
    txtCod.setEditable(false);

    xYLayout.setWidth(755);
    btnClave = new ButtonControl(imgs.getImage(0));
    txtHora.setText("00:00");
    this.setLayout(xYLayout);

    lGrupo.setText(res.getString("lGrupo.Text"));
    lTipoB.setText(res.getString("lTipoB.Text"));

    checkAuto.setCheckboxGroup(checkGroup);
    checkManual.setCheckboxGroup(checkGroup);
    checkAuto.setLabel(res.getString("checkAuto.Label"));
    checkManual.setLabel(res.getString("checkManual.Label"));

    //ordenarlos !!!
    this.add(lblAno, new XYConstraints(14, 9, 108, 19));
    this.add(txtAno, new XYConstraints(124, 9, 37, 19));
    this.add(txtCod, new XYConstraints(164, 9, 63, 19));
    this.add(btnClave, new XYConstraints(233, 9, -1, -1));
    this.add(txtDesc, new XYConstraints(265, 9, 217, 19));
    this.add(lblFNotif, new XYConstraints(494, 9, 119, 19));
    this.add(txtFecha, new XYConstraints(612, 9, 81, 19));
    this.add(txtHora, new XYConstraints(694, 9, -1, 19));
    this.add(lGrupo, new XYConstraints(13, 36, 110, 19));
    this.add(choGrupo, new XYConstraints(126, 36, 248, 19));
    this.add(lTipoB, new XYConstraints(397, 36, 68, 19));
    this.add(choTipoB, new XYConstraints(466, 36, 248, 19));
    this.add(lblResumen, new XYConstraints(11, 63, 116, -1));
    this.add(checkManual, new XYConstraints(135, 63, -1, -1));
    this.add(checkAuto, new XYConstraints(220, 63, -1, -1));

    //nombre
    txtAno.setName("ano");
    txtFecha.setName("fecha");
    txtHora.setName("hora");
    choGrupo.setName("choGrupo");
    choTipoB.setName("choTipo");

    choGrupo.addItemListener(itemList);

    lblResumen.setText(res.getString("lblResumen.Text"));
    txtFecha.setText(this.app.getFC_ACTUAL());
    lblFNotif.setText(res.getString("lblFNotif.Text"));

    txtAno.setText(this.app.getFC_ACTUAL().substring(6, 10));
    lblAno.setText(res.getString("lblAno.Text"));

    //para cambiar la hora
    txtAno.addFocusListener(focusAdap);
    txtFecha.addFocusListener(focusAdap);
    txtHora.addFocusListener(focusAdap);

    //para los botones
    btnClave.addActionListener(actionAdap);
    btnClave.setActionCommand("alta");

  }

  void choGrupo_itemStateChanged() {

    //si no cambia, que no haga nada
    if (cd_grupo.equals(getGrupo())) {
      return;
    }

    Cargar_Tipo();
    //actuar sobre pdatos
    //0: tia  -> N� Manipulares
    //1: Vacunables -> Estado Vacunal
    //2: Otras  _> nada
    if (dlgB.tabPanel.getSolapaActual().equals(res.getString("Datos_B_sicos"))) {
      dlgB.tabPanel.VerPanel(res.getString("Colectivo"));
      dlgB.pDatos.habilitaEV_Manip(dlgB.panelSuperior.getGrupo().trim(), true);
      dlgB.tabPanel.VerPanel(res.getString("Datos_B_sicos"));
    }
    else {
      dlgB.pDatos.habilitaEV_Manip(dlgB.panelSuperior.getGrupo().trim(), true);
    }
    cd_grupo = getGrupo();
  }

  public void Cargar_Grupo() {
    DataCDDS aux = null;
    for (int i = 0; i < listaGrupo.size(); i++) {
      aux = (DataCDDS) listaGrupo.elementAt(i);
      choGrupo.addItem(aux.getCD() + "   " + aux.getDS());
    }
    choGrupo.select(0);
  }

//devuelve el cod de grupo
  public String getDSGrupo() {

    String iG = "";
    iG = choGrupo.getSelectedItem().trim().substring(2);

    return (iG.trim());
  } //getGrupo

//devuelve el cod de grupo
  public String getGrupo() {

    String iG = "";
    iG = choGrupo.getSelectedItem().trim().substring(0, 1);

    return (iG.trim());
  } //getGrupo

//devuelve el cod del tipo de brote
  public String getTipo() {

    String iG = "";
    iG = choTipoB.getSelectedItem().trim().substring(0, 1);

    return (iG);
  } //getGrupo

  public void Cargar_Tipo() {
    choTipoB.removeAll();
    choTipoB.doLayout();

    DataCDDS aux = null;
    String cdGrupo = getGrupo();

    for (int i = 0; i < listaTBrotes.size(); i++) {
      aux = (DataCDDS) listaTBrotes.elementAt(i);
      if (cdGrupo.equals(aux.getCDpadre())) {
        choTipoB.addItem(aux.getCD() + "   " + aux.getDS());
      }
    }

  }

//dado un cod, situar la Choice en ese registro
//(es obligatorio-> select (i))
  public void selectGrupo(String cd) {
    if (cd.equals("")) {
      choGrupo.select(0);
    }
    else {
      for (int i = 0; i < listaGrupo.size(); i++) {
        DataCDDS data = (DataCDDS) listaGrupo.elementAt(i);
        if (data.getCD().trim().equals(cd.trim())) {
          choGrupo.select(i);
          break;
        }
      }
    }
  }

//dado un cod, situar la Choice en ese registro
//(es obligatorio-> select (i))
  public void selectTipo(String cd) {
    if (cd.equals("")) {
      choTipoB.select(0);
    }
    else {
      for (int i = 0; i < listaTBrotes.size(); i++) {
        DataCDDS data = (DataCDDS) listaTBrotes.elementAt(i);
        if (data.getCD().trim().equals(cd.trim())) {
          choTipoB.select(i);
          break;
        }
      }
    }
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      case modoESPERA:
        txtCod.setEnabled(false);
        txtAno.setEnabled(false);
        txtDesc.setEnabled(false);
        txtFecha.setEnabled(false);
        txtHora.setEnabled(false);
        btnClave.setEnabled(false);
        choGrupo.setEnabled(false);
        choTipoB.setEnabled(false);
        checkAuto.setEnabled(false);
        checkManual.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA:
        txtCod.setEnabled(false);
        if (txtCod.getText().trim().equals("")) {
          txtAno.setEnabled(true);
          btnClave.setEnabled(true);
        }
        else {
          txtAno.setEnabled(false);
          btnClave.setEnabled(false);
        }
        txtDesc.setEnabled(true);
        txtFecha.setEnabled(true);
        txtHora.setEnabled(true);
        choGrupo.setEnabled(true);
        choTipoB.setEnabled(true);
        checkAuto.setEnabled(true);
        checkManual.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
      case modoMODIFICACION:
        txtCod.setEnabled(false);
        txtAno.setEnabled(false);
        txtDesc.setEnabled(true);
        txtFecha.setEnabled(true);
        txtHora.setEnabled(true);
        btnClave.setEnabled(false);
        choGrupo.setEnabled(false); //deshabilitado
        choTipoB.setEnabled(true);
        checkAuto.setEnabled(true);
        checkManual.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
      case modoCONSULTA:
        txtCod.setEnabled(false);
        txtAno.setEnabled(false);
        txtDesc.setEnabled(false);
        txtFecha.setEnabled(false);
        txtHora.setEnabled(false);
        btnClave.setEnabled(false);
        choGrupo.setEnabled(false);
        choTipoB.setEnabled(false);
        checkAuto.setEnabled(false);
        checkManual.setEnabled(false);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
    checkAuto.setVisible(false);
    checkManual.setVisible(false);
    lblResumen.setVisible(false);
    checkManual.setChecked(true);
    checkAuto.setChecked(false);

    this.doLayout();
  }

//rellena el panel con datos que le llegan (modo MODIFICACION)
  public void rellena(DataBrote data) {

    String nmAlerBro = "";
    String fcFechaHoraF = "";
    String fcFechaHoraH = "";
    String cdAno = "";
    String dsBrote = "";
    String grupo = "";
    String tipo = "";
    String resumen = "";

    if (data.getCD_ANO() != null) {
      cdAno = data.getCD_ANO();
    }
    txtAno.setText(cdAno);

    if (data.getNM_ALERBRO() != null) {
      nmAlerBro = data.getNM_ALERBRO();
    }
    txtCod.setText(nmAlerBro);

    if (data.getDS_BROTE() != null) {
      dsBrote = data.getDS_BROTE();
    }
    txtDesc.setText(dsBrote);

    if (data.getFC_FECHAHORA_F() != null) {
      fcFechaHoraF = data.getFC_FECHAHORA_F();
    }
    txtFecha.setText(fcFechaHoraF);

    if (data.getFC_FECHAHORA_H() != null) {
      fcFechaHoraH = data.getFC_FECHAHORA_H();
    }
    txtHora.setText(fcFechaHoraH);

    if (data.getCD_GRUPO() != null) {
      grupo = data.getCD_GRUPO();
      choGrupo.removeItemListener(itemList);
      selectGrupo(grupo);
      Cargar_Tipo(); //lista de valores
      selectTipo(data.getCD_TBROTE());
      choGrupo.addItemListener(itemList);
    }

    if (data.getIT_RESCALC() != null) {
      resumen = data.getIT_RESCALC();
    }
    if (resumen.equals("S")) { //automatico
      checkAuto.setChecked(true);
      checkManual.setChecked(false);
    }
    else {
      checkManual.setChecked(true);
      checkAuto.setChecked(false);
    }

    cd_grupo = getGrupo();

  } //fin

  public DataBrote recogerDatos(DataBrote resul) {

    resul.insert("CD_ANO", txtAno.getText().trim());
    resul.insert("NM_ALERBRO", txtCod.getText().trim());
    resul.insert("DS_BROTE", txtDesc.getText().trim());
    resul.insert("FC_FECHAHORA_F", txtFecha.getText().trim());
    resul.insert("FC_FECHAHORA_H", txtHora.getText().trim());

    resul.insert("CD_GRUPO", getGrupo());
    resul.insert("CD_TBROTE", getTipo());

    if (checkAuto.isChecked()) {
      resul.insert("IT_RESCALC", "S");
    }
    else {
      resul.insert("IT_RESCALC", "N");

    }
    return resul;

  }

//Validar Numericos
  public boolean isNum(TextField txt) {

    boolean b = true; //Es Numerico

    try {
      Integer f = new Integer(txt.getText());
      int i = f.intValue();
      if (i < 0) {
        b = false;

      }
    }
    catch (Exception e) {
      b = false;
    }
    return b;
  }

  public String getCD_ANO() {
    return (txtAno.getText().trim());
  }

  public String getNM_ALERBRO() {
    return (txtCod.getText().trim());
  }

  public boolean validarDatos(int modo) {
    boolean correcto = true;

    //1. perdidas de foco
    //2. longitudes
    if (txtAno.getText().length() > 4) {
      ShowError(res.getString("msg.1"));
      txtAno.setText("");
      txtAno.requestFocus();
      return false;
    }
    if (txtAno.getText().length() < 4) {
      ShowError(res.getString("msg.2"));
      txtAno.setText("");
      txtAno.requestFocus();
      return false;
    }

    if (txtCod.getText().length() > 7) {
      ShowError(res.getString("msg.3"));
      txtCod.setText("");
      txtCod.requestFocus();
      return false;
    }
    if (txtDesc.getText().length() > 50) {
      ShowError(res.getString("msg.4"));
      txtDesc.setText("");
      txtDesc.requestFocus();
      return false;
    }
    //3. numericos
    //en alta, puedo escribir el a�o
    if (modo == modoALTA) {
      if (!isNum(txtAno)) {
        ShowError(res.getString("msg.5"));
        txtAno.requestFocus();
        return false;
      }
    }
    //4. obligatorios //no saltara ninguno
    //en alta
    if (txtCod.getText().equals("")) {
      ShowError(res.getString("msg.6"));
      txtCod.requestFocus();
      return false;
    }
    if (txtAno.getText().equals("")) {
      ShowError(res.getString("msg.7"));
      txtAno.requestFocus();
      return false;
    }
    if (txtDesc.getText().equals("")) {
      ShowError(res.getString("msg.8"));
      txtDesc.requestFocus();
      return false;
    }
    if (txtFecha.getText().equals("")) {
      ShowError(res.getString("msg.9"));
      txtFecha.requestFocus();
      return false;
    }
    //Este caso no se da porque se inicializa a 00:00
    if (txtHora.getText().equals("")) {
      ShowError(res.getString("msg.10"));
      txtHora.requestFocus();
      return false;
    }

    return correcto;
  }

// Llama a CMessage para mostrar el mensaje de error sMessage
  public void ShowError(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgERROR, sMessage);
    msgBox.show();
    msgBox = null;
  }

  private void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

  public void txtFecha_focusLost() {

    txtFecha.ValidarFecha();
    if (modoOperacion == modoALTA) {
      if (txtFecha.getValid().equals("N")) {
        txtFecha.setText(this.app.getFC_ACTUAL());
        txtAno.setText(this.app.getFC_ACTUAL().substring(6, 10));
      }
      else if (txtFecha.getValid().equals("S")) {
        txtFecha.setText(txtFecha.getFecha());
        txtAno.setText(txtFecha.getText().substring(6, 10));
      }
      txtHora.setText("00:00");
    } //alta

    if (modoOperacion == modoMODIFICACION) {
      DataBrote datosIniciales = (DataBrote) dlgB.datosBrote;

      if (txtFecha.getValid().equals("N")) {
        txtFecha.setText(datosIniciales.getFC_FECHAHORA_F());
      }
      else if (txtFecha.getValid().equals("S")) {
        txtFecha.setText(txtFecha.getFecha());
        if (!txtFecha.getText().substring(6, 10).equals(txtAno.getText())) {
          txtFecha.setText(datosIniciales.getFC_FECHAHORA_F());
        }
        else {
          txtFecha.setText(txtFecha.getFecha());
        }
      }
    }

  }

  void txtAno_focusLost() {
    if (modoOperacion == modoALTA) { //solo en alta
      //si la fecha se corresponde con otro a�o
      if (!txtFecha.getText().substring(6, 10).equals(txtAno.getText())) {
        String dia = txtFecha.getText().substring(0, 2);
        String mes = txtFecha.getText().substring(3, 5);
        txtFecha.setText(dia + "/" + mes + "/" + txtAno.getText());
        txtFecha_focusLost();
      }
    }
  }

  void hora_focusLost() {
    txtHora.ValidarFecha(); //validamos el dato
    if (txtHora.getValid().equals("N")) {
      txtHora.setText(txtHora.getFecha());
    }
    else if (txtHora.getValid().equals("S")) {
      txtHora.setText(txtHora.getFecha());
    }
  }

  public CApp getMiCApp() {
    return (this.app);
  }

  void btnClave_actionPerformed() {

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    dlgB.Inicializar(modoOperacion);

    DiaBIAlt dlg = new DiaBIAlt(this.getApp(),
                                res.getString("msg.11"),
                                txtAno.getText().trim());
    if (!dlg.bError) { //sin error y con datos

      //puede ser que se de un error en la recuperacion de la
      //lista de alertas y brotes
      dlg.show();
    }
    else { //con error  o sin datos
      if (dlg.hayDatos != 0) { //con error y no sin datos
        ShowWarning(res.getString("msg.12"));
      }
    }

    //si sali por alta(aceptar) en el dialogo
    if (dlg.iOut != -1) {
      CLista listaAlta = dlg.getComponente();
      if (listaAlta.size() > 0) {
        //relleno los paneles
        if (listaAlta.size() == 1) {
          //solo es un DataAlerta "CD_ANO",
          //"NM_ALERBRO", "CD_SITALERBRO", "FC_ALERBRO",
          DataAlerta data = (DataAlerta) listaAlta.elementAt(0);
          txtAno.setText(data.getCD_ANO());
          txtCod.setText(data.getNM_ALERBRO());
          this.dlgB.RellenaCheck(data);

        }
        else if (listaAlta.size() == 2) {
          //un DataAlerta y un DataBrote
          DataAlerta dataA = (DataAlerta) listaAlta.elementAt(0);
          DataBrote dataB = (DataBrote) listaAlta.elementAt(1);
          this.rellena(dataB);
          this.dlgB.RellenaCheck(dataA);
          dlgB.pColectivo.rellena(dataB);
          dlgB.pCuadro.rellena(dataB);
          dlgB.pDatos.rellena(dataB);

        }

      }
    }
    //si sale por cancelar, nada
    dlg = null;
    modoOperacion = modo;
    dlgB.Inicializar(modoOperacion);

  }

} //clase

//para las perdidas de foco
class focusAdapter
    implements java.awt.event.FocusListener, Runnable {
  PanelbiSuperior adaptee;
  FocusEvent evt;

  focusAdapter(PanelbiSuperior adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void focusGained(FocusEvent e) {

  }

  public void run() {

    // System_out.println("foco " + ((TextField)evt.getSource()).getName() );

    if ( ( (TextField) evt.getSource()).getName().equals("ano")) {
      adaptee.txtAno_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("fecha")) {
      adaptee.txtFecha_focusLost();
    }
    else if ( ( (CHora) evt.getSource()).getName().equals("hora")) {
      adaptee.hora_focusLost();
    }

  }

}

//acciones de los botones
class actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelbiSuperior adaptee;
  ActionEvent e;

  actionAdapter(PanelbiSuperior adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent evt) {
    e = evt;
    Thread th = new Thread(this);
    th.start();

  }

  public void run() {
    int index = 0;
    if (e.getActionCommand().equals("alta")) {
      adaptee.btnClave_actionPerformed();
    }
  }
}

class itemListener
    implements java.awt.event.ItemListener, Runnable {
  PanelbiSuperior adaptee;
  ItemEvent e = null;

  itemListener(PanelbiSuperior adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent evt) {
    e = evt;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    String name = ( (Component) e.getSource()).getName();

    if (name.equals("choGrupo")) {
      adaptee.choGrupo_itemStateChanged();
    }

  }
} //class
