//Title:        Volcado de casos individuales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Volcado de casos individuales (incluidas preguntas del protocolo) de una enfermedad a fichero ASCII.

package volCasosIndCent;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo.DataCat;
import centinelas.cliente.c_comuncliente.nombreservlets;
import enfcentinela.CListaEnfCenti;
import enfcentinela.DataEnfCenti;
import sapp.StubSrvBD;
import utilidades.PanFechas;

public class PanelConsVolCasosInd
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;
  final int modoVOLCADO = 8;

  //rutas imagenes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/vaciar.gif",
      "images/salvar.gif"};

  protected StubSrvBD stubCliente = new StubSrvBD();
  public ParVolCasosInd parCons;
  protected int modoOperacion = modoNORMAL;
  protected PanelListaPreg panelListaPreg;
  // servlet
  String sUrlServlet;
  File miFichero = null;
  FileWriter fStream = null;
  // stub's
  /*
    final String strSERVLETNivel1 = "servlet/SrvNivel1";
    final String strSERVLETNivel2 = "servlet/SrvZBS2";
    final String strSERVLETZona = "servlet/SrvMun";
    final String strSERVLETMun = "servlet/SrvMun";
    final String strSERVLETProv = "servlet/SrvCat2";
   */
  final String strSERVLETEnf = "servlet/SrvEnfVigi";

  //Para centinelas
//  final String strSERVLET_ENF_CENTI = "servlet/SrvEnfCenti";
  final String strSERVLET_ENF_CENTI = nombreservlets.strSERVLET_ENF_CENTI;

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  XYLayout xYLayout = new XYLayout();
  //Comps para fechas
  Label lblHasta = new Label();
  Label lblDesde = new Label();
  PanFechas fechasDesde;
  PanFechas fechasHasta;
  //Componente para lectura de nombre fichero
  capp.CFileName panFichero;

  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();
  //Comps Enfermedad
  Label lblEnfermedad = new Label();

  CCampoCodigo txtEnfer = new CCampoCodigo();

  TextField txtDesEnfer = new TextField();
  ButtonControl btnCtrlBuscarEnfer = new ButtonControl();
  //Comps separador
  Label lblSeparador = new Label();
  TextField txtSeparador = new TextField();
  //Escucahdores de eventos
  focusAdapter txtFocusAdapter = new focusAdapter(this);
  txt_keyAdapter txtKeyAdapter = new txt_keyAdapter(this);
  actionListener btnActionListener = new actionListener(this);

  protected CCargadorImagen imgs = null;

  //_________________________________________________________

  public PanelConsVolCasosInd(CApp a, PanelListaPreg p) {
    try {
//      sUrlServlet = "servlet/SrvModPregCent";
      sUrlServlet = nombreservlets.strSERVLET_MOD_PREG_CENT;

      setApp(a);
      res = ResourceBundle.getBundle("volCasosIndCent.Res" + app.getIdioma());
      panelListaPreg = p;

      if (app.getParametro("CD_ANO") == null) {
        fechasDesde = new PanFechas(this, PanFechas.modoVACIO, false);
      }
      else {
        fechasDesde = new PanFechas(this, false, app.getParametro("CD_ANO"), false);

      }
      if (app.getParametro("CD_ANO") == null) {
        fechasHasta = new PanFechas(this, PanFechas.modoVACIO, false);
      }
      else {
        fechasHasta = new PanFechas(this, false, app.getParametro("CD_ANO"), false);

        /*
               fechasDesde = new PanFechas(this, PanFechas.modoVACIO, false);
               fechasHasta = new PanFechas(this, PanFechas.modoVACIO, false);
         */
      }
      panFichero = new capp.CFileName(a);
      jbInit();
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  //_________________________________________________________

  void jbInit() throws Exception {

    this.setSize(new Dimension(569, 350));
    xYLayout.setHeight(350);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));

    // gestores de eventos
    btnCtrlBuscarEnfer.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    txtEnfer.addKeyListener(txtKeyAdapter);
    txtEnfer.addFocusListener(txtFocusAdapter);

    lblSeparador.setText(res.getString("lblSeparador.Text"));
    txtSeparador.addKeyListener(txtKeyAdapter);
    txtSeparador.addFocusListener(txtFocusAdapter);
    txtSeparador.setText("$");

    btnCtrlBuscarEnfer.setActionCommand("buscarEnfer");
    txtDesEnfer.setEditable(false);
    txtDesEnfer.setEnabled(false);

    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    txtEnfer.setName("txtEnfer");
    txtSeparador.setName("txtSeparador");

    lblEnfermedad.setText(res.getString("lblEnfermedad.Text"));
    lblHasta.setText(res.getString("lblHasta.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));

    txtEnfer.setBackground(new Color(255, 255, 150));
    xYLayout.setWidth(596);

    this.setLayout(xYLayout);

    this.add(lblDesde, new XYConstraints(26, 5, 51, -1));
    this.add(fechasDesde, new XYConstraints(40, 5, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 35, -1, -1));
    this.add(fechasHasta, new XYConstraints(40, 35, -1, -1));
    this.add(lblEnfermedad, new XYConstraints(26, 65, 75, -1));
    this.add(txtEnfer, new XYConstraints(143, 65, 77, -1));
    this.add(btnCtrlBuscarEnfer, new XYConstraints(225, 65, 24, 24));
    this.add(txtDesEnfer, new XYConstraints(254, 65, 287, -1));

    this.add(panFichero, new XYConstraints(12, 95, -1, -1));
    this.add(lblSeparador, new XYConstraints(26, 145, 129, -1));
    this.add(txtSeparador, new XYConstraints(165, 145, -1, -1));
    this.add(btnLimpiar, new XYConstraints(325, 175, -1, -1));
    this.add(btnInforme, new XYConstraints(425, 175, -1, -1));

    imgs = new CCargadorImagen(app, imgNAME);
    txtSeparador.setBackground(new Color(255, 255, 150));
    imgs.CargaImagenes();

    btnCtrlBuscarEnfer.setImage(imgs.getImage(0));
    btnLimpiar.setImage(imgs.getImage(1));
    btnInforme.setImage(imgs.getImage(2));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  //_________________________________________________________

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (fechasDesde.txtAno.getText().length() == 0) ||
        (fechasHasta.txtAno.getText().length() == 0) ||
        (fechasDesde.txtCodSem.getText().length() == 0) ||
        (fechasHasta.txtCodSem.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    // Comprobacion de la descripcion de la enfermedad
    if (txtDesEnfer.getText().length() == 0) {
      bDatosCompletos = false;

      // Comprobacion del separador
    }
    if (txtSeparador.getText().length() == 0) {
      bDatosCompletos = false;

      // Comprobacion del fichero
    }
    if (panFichero.txtFile.getText().length() == 0) {
      bDatosCompletos = false;

    }
    return bDatosCompletos;
  }

  //_________________________________________________________

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtEnfer.setEnabled(true);
        btnCtrlBuscarEnfer.setEnabled(true);
        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtEnfer.setEnabled(false);
        btnCtrlBuscarEnfer.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //_________________________________________________________

  //busca la enfermedad
  void btnCtrlBuscarEnfer_actionPerformed(ActionEvent evt) {
    DataCat datosPantallaTub = null; //Para tuberculosis
    DataEnfCenti dataCenti = null; //Para centinelas
    CMessage msgBox = null;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      //tuberculosis
      if (app.getTSive().equals("E")) {
        CListaEnfEDO lista = new CListaEnfEDO(app,
                                              res.getString("msg9.Text"),
                                              stubCliente,
                                              strSERVLETEnf,
                                              servletOBTENER_X_CODIGO,
                                              servletOBTENER_X_DESCRIPCION,
                                              servletSELECCION_X_CODIGO,
                                              servletSELECCION_X_DESCRIPCION);
        lista.show();
        datosPantallaTub = (DataCat) lista.getComponente();
      }

      //Centinelas
      else if (app.getTSive().equals("C")) {

        // apunta al servlet auxiliar
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_ENF_CENTI));

        CListaEnfCenti lista = new CListaEnfCenti(app,
                                                  res.getString("msg10.Text"),
                                                  stubCliente,
                                                  strSERVLET_ENF_CENTI,
                                                  servletOBTENER_X_CODIGO,
                                                  servletOBTENER_X_DESCRIPCION,
                                                  servletSELECCION_X_CODIGO,
            servletSELECCION_X_DESCRIPCION);

        lista.show();
        dataCenti = (DataEnfCenti) lista.getComponente();
        //he traido datos
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

    //tuberculosis
    if (app.getTSive().equals("E")) {
      if (datosPantallaTub != null) {
        txtEnfer.removeKeyListener(txtKeyAdapter);
        txtEnfer.setText(datosPantallaTub.getCod());
        txtDesEnfer.setText(datosPantallaTub.getDes());
        txtEnfer.addKeyListener(txtKeyAdapter);
      }
    }
    //Centinelas
    else if (app.getTSive().equals("C")) {
      if (dataCenti != null) {
        txtEnfer.removeKeyListener(txtKeyAdapter);
        txtEnfer.setText(dataCenti.getCod());
        txtDesEnfer.setText(dataCenti.getDes());
        txtEnfer.addKeyListener(txtKeyAdapter);
      }
    }
    modoOperacion = modoNORMAL;
    Inicializar();
    // Borramos la otra solapa.
    panelListaPreg.vaciarPantalla();
    // Env�o el c�digo a la otra solapa.
    if (dataCenti != null) {
      panelListaPreg.pasaWhere(dataCenti.getCod());
    }
  }

  //_________________________________________________________

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    txtEnfer.setText("");
    txtDesEnfer.setText("");
    panelListaPreg.vaciarPantalla();
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //_________________________________________________________

  void btnGenerar_actionPerformed(ActionEvent evt) {

    CMessage msgBox;
    DataVolCasInd par = new DataVolCasInd();

    if (isDataValid()) {
      if (!txtDesEnfer.getText().equals("")) {
        par.enfer = txtEnfer.getText();
      }
      par.anoD = fechasDesde.txtAno.getText();
      par.anoH = fechasHasta.txtAno.getText();
      if (fechasDesde.txtCodSem.getText().length() == 1) {
        par.semD = "0" + fechasDesde.txtCodSem.getText();
      }
      else {
        par.semD = fechasDesde.txtCodSem.getText();
      }
      if (fechasHasta.txtCodSem.getText().length() == 1) {
        par.semH = "0" + fechasHasta.txtCodSem.getText();
      }
      else {
        par.semH = fechasHasta.txtCodSem.getText();

        // Compruebo si se calculan las tasas
        // tasas si area o todo vacio
        // de momento lo pongo a false
      }
      par.sep = txtSeparador.getText();
      par.tSive = getApp().getTSive();
      par.codsPreg = panelListaPreg.obtenerCodPreg();

      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();
      try {
        CLista data = new CLista();

        // idioma
        data.setIdioma(app.getIdioma());

        // estado
        data.setState(CLista.listaINCOMPLETA);

        data.addElement(par);

        stubCliente.setUrl(new URL(app.getURL() + sUrlServlet));
        data = (CLista) stubCliente.doPost(this.modoVOLCADO, data);

        /*
                    SrvModPregCent servlet = new SrvModPregCent();
                   //Indica como conectarse a la b.datos
             servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                    data = (CLista) servlet.doDebug(this.modoVOLCADO, data);
         */

        // Guardar en fichero
        String sLinea = new String();
        if (data.size() > 0) {
          try {
            miFichero = new File(this.panFichero.txtFile.getText());
            fStream = new FileWriter(miFichero);

            for (int j = 0; j < data.size(); j++) {
              sLinea = (String) data.elementAt(j);
              fStream.write(sLinea);
              fStream.write("\r");
              fStream.write("\n");
            }
            fStream.close();
            fStream = null;
            miFichero = null;
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                  res.getString("msg14.Text"));
            msgBox.show();
            msgBox = null;
            // Borramos la otra solapa.
            panelListaPreg.vaciarPantalla();
            // Borramos esta pantalla.
            fechasDesde.sCodSemBk = "";
            fechasDesde.sFecSemBk = "";
            fechasDesde.txtCodSem.setText("");
            fechasDesde.txtFecSem.setText("");
            fechasHasta.sCodSemBk = "";
            fechasHasta.sFecSemBk = "";
            fechasHasta.txtCodSem.setText("");
            fechasHasta.txtFecSem.setText("");
            txtEnfer.setText("");
            txtDesEnfer.setText("");
            panFichero.txtFile.setText("c:\\data.txt");
          }
          catch (IOException ioEx) {
            ioEx.printStackTrace();
            msgBox = new CMessage(this.app, CMessage.msgERROR,
                                  res.getString("msg15.Text"));
            msgBox.show();
            msgBox = null;
          }
        } //if
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg16.Text"));
          msgBox.show();
          msgBox = null;
        }

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg17.Text"));
      msgBox.show();
      msgBox = null;
    }

  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    DataCat enf;
    DataEnfCenti enfCenti;
    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    // Esta variable la ponemos para comprobar que
    // no cambia el contenido de la caja de texto y as�
    // no tener que borrar la otra solapa.
    String sEnferAntigua = txtEnfer.getText();

    TextField txt = (TextField) e.getSource();

    //Centinelas
    if (app.getTSive().equals("C")) {
      // gestion de datos
      if ( (txt.getName().equals("txtEnfer")) &&
          (txtEnfer.getText().length() > 0)) {
        param = new CLista();
        param.setIdioma(app.getIdioma());
        param.addElement(new DataEnfCenti(txtEnfer.getText()));
        strServlet = strSERVLET_ENF_CENTI;
        modoServlet = servletOBTENER_X_CODIGO;
      }
    }

    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtEnfer")) {

            //Centinelas
            if (app.getTSive().equals("C")) {

              enfCenti = (DataEnfCenti) param.firstElement();
              txtEnfer.removeKeyListener(txtKeyAdapter);
              txtEnfer.setText(enfCenti.getCod());
              txtDesEnfer.setText(enfCenti.getDes());
              txtEnfer.addKeyListener(txtKeyAdapter);
            }

            //Centinelas
            if (app.getTSive().equals("E")) {

              enf = (DataCat) param.firstElement();
              txtEnfer.removeKeyListener(txtKeyAdapter);
              txtEnfer.setText(enf.getCod());
              txtDesEnfer.setText(enf.getDes());
              txtEnfer.addKeyListener(txtKeyAdapter);
            }
            // Borramos la otra solapa s�lo si cambia el contenido
            // de la caja de texto
            if (!txtEnfer.getText().equals(sEnferAntigua)) {
              panelListaPreg.vaciarPantalla();
            }
          }

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg18.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  //_________________________________________________________

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtEnfer")) &&
        (txtDesEnfer.getText().length() > 0)) {
      txtEnfer.setText("");
      txtDesEnfer.setText("");
      // Borramos la otra solapa.
      panelListaPreg.vaciarPantalla();
    }

    Inicializar();
  }

} //clase

//_________________________________________________________

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  PanelConsVolCasosInd adaptee = null;
  ActionEvent e = null;

  public actionListener(PanelConsVolCasosInd adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarEnfer")) {
      adaptee.btnCtrlBuscarEnfer_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanelConsVolCasosInd adaptee;
  FocusEvent event;

  focusAdapter(PanelConsVolCasosInd adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

// cambio en una caja de codigos
class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  PanelConsVolCasosInd adaptee;

  txt_keyAdapter(PanelConsVolCasosInd adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}

////////////////////// Clases para listas

class CListaEnfEDO
    extends CListaValores {

  public CListaEnfEDO(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataCat(s);

  }

  public String getCodigo(Object o) {
    return ( ( (DataCat) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCat) o).getDes());
  }
}
