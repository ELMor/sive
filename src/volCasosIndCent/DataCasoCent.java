//Estructura que representa los datos de un caso centinela

package volCasosIndCent;

import java.io.Serializable;
import java.util.Date;

public class DataCasoCent
    implements Serializable {

  String sCodEnf = ""; //Enfermedad
  String sPunCen = ""; //Pto Centinela
  String sMedCen = ""; //Med Cent
  String sCodAno = ""; //A�o
  String sCodSem = ""; //Semana
  int iNumOrd; //N�mero de orden
  String sCodSex = ""; //cod Sexo
  Date dFecNac = null; //Fecha de nacimiento

  public DataCasoCent() {
  }

  public DataCasoCent(String codEnf, String punCen, String medCen,
                      String codAno, String codSem, int numOrden,
                      String codSex, java.util.Date fecNac) {
    sCodEnf = codEnf;
    sPunCen = punCen;
    sMedCen = medCen;
    sCodAno = codAno;
    sCodSem = codSem;
    iNumOrd = numOrden;
    sCodSex = codSex;
    dFecNac = fecNac;
  }

  String getCodEnf() {
    return sCodEnf;
  }

  String getPunCen() {
    return sPunCen;
  }

  String getMedCen() {
    return sMedCen;
  }

  String getCodAno() {
    return sCodAno;
  }

  String getCodSem() {
    return sCodSem;
  }

  int getNumOrd() {
    return iNumOrd;
  }

  String getCodSex() {
    return sCodSex;
  }

  java.util.Date getFecNac() {
    return dFecNac;
  }

} //CLASE
