//Title:        Volcado de casos individuales
//Author:       Cristina Gayan Asensio y Luis Rivera
//Description:  Servlet para Volcado de casos individuales (incluidas preguntas del protocolo) de una enfermedad a fichero ASCII.

// Tiene una parte comun a centinelas y tuberculosis , la de seleccion de preguntas, informaci�n de preguntas, etc
// Los modos de volcado de datos son distintos en centinelas y tuberculosis

package volCasosIndCent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.Vector;

import capp.CLista;
import sapp.DBServlet;

public class SrvModPregCent
    extends DBServlet {

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3; //Para obtener un modelo, una pregunta o una pregunta de un modelo
  final int servletOBTENER_X_DESCRIPCION = 4; //Para obtener un modelo, una pregunta o una pregunta de un modelo
  final int servletSELECCION_X_CODIGO = 5; //Para seleccionar varios modelos,  preguntas o preguntas de un modelo
  final int servletSELECCION_X_DESCRIPCION = 6; //Para seleccionar varios modelos,  preguntas o preguntas de un modelo
  final int modoINFOPREG = 7;
  final int modoVOLCADO = 8; //Volcado de centinelas
  final int modoVOLCADO_TUBERCULOSIS = 9; //Volcado de tuberculosis

  final int MODELO = 10;
  final int PREGUNTA = 11;
  final int MOD_Y_PREG = 12;

  final int servletBUSQUEDA_CON_FILTRO = 13; // Para buscar enmodelos para una �nica enfermedad.

  // Para el volcado de casos individuales sin preguntas
  final String NO_ENFER = "()";
  final String NO_PREGS = "('')";

  //_______________________________________________________

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {
    // variables temporales
    String CD_TSIVE = "";
    String CD_MODELO = "";
    String DS_MODELO = "";
    String DSL_MODELO = "";
    String CD_NIVEL_1 = "";
    String CD_NIVEL_2 = "";
    String CD_CA = "";
    String IT_OK = "";
    String CD_ENFCIE = "";
    java.sql.Date FC_BAJAM = null;
    int numAux = 0;

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    PreparedStatement stAux = null;
    ResultSet rsAux = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;
    int j = 1;
    // objetos de datos
    CLista data = new CLista();
    DataModPreg dModPreg = null;
    DataInfoPreg dInfoPreg = null;
    DataVolCasInd dVolCas = null;
    String fechaCadena = null;

    String sep = ""; //Separador caracteres fichero
    int par; //Para recorrer los par�metros de sent. JDBC
    Integer aux = null; //Para conversiones datos

    //para el formato de las fechas
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    data.setState(CLista.listaVACIA);
    data = null;
    try {

      // modos de operaci�n
      switch (opmode) {

        //_________________________________________________________

        case servletBUSQUEDA_CON_FILTRO:
          dModPreg = (DataModPreg) param.elementAt(0);
          String cd_enfcie = (String) param.elementAt(1);
          int caso = 0;

          //#// System_out.println("SrvModPreg: servletSELECCION_X_CODIGO");

          switch (dModPreg.modo) {
            case MODELO:

              //#// System_out.println("SrvModPreg: MODELO");
              query = "select CD_MODELO, DS_MODELO from SIVE_MODELO "
                  + " where CD_TSIVE = ? and CD_ENFCIE = ? and";
              if (param.getFilter().length() > 0) {
                query = query +
                    " CD_MODELO like ? and CD_MODELO > ? order by CD_MODELO";
              }
              else {
                query = query + " CD_MODELO like ? order by CD_MODELO";
              }
              break;
            case PREGUNTA:

              //#// System_out.println("SrvModPreg: PREGUNTA");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and CD_ENFCIE = ? and";
              if (param.getFilter().length() > 0) {
                query = query +
                    " CD_PREGUNTA like ? and CD_PREGUNTA > ? order by CD_PREGUNTA";
              }
              else {
                query = query + " CD_PREGUNTA like ? order by CD_PREGUNTA";
              }
              break;
            case MOD_Y_PREG:

              //#// System_out.println("SrvModPreg: MOD_Y_PREG");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and CD_ENFCIE = ? and"
                  + " CD_PREGUNTA in (select CD_PREGUNTA from SIVE_LINEA_ITEM where CD_MODELO = ?) and ";
              if (param.getFilter().length() > 0) {
                query = query +
                    " CD_PREGUNTA like ? and CD_PREGUNTA > ? order by CD_PREGUNTA";
              }
              else {
                query = query + " CD_PREGUNTA like ? order by CD_PREGUNTA";
              }
              break;
          }

          // prepara la lista de resultados
          data = new CLista();

          //#// System_out.println("SrvModPreg : " + query);

          // lanza la query
          st = con.prepareStatement(query);
          j = 1;
          st.setString(j, dModPreg.tSive);
          //#// System_out.println("SrvModPreg : tSive " + dModPreg.tSive + " " + j);
          j++;
          st.setString(j, cd_enfcie);
          //#// System_out.println("SrvModPreg : codMod " + dModPreg.codMod + '%' + " " + j);
          j++;

          // Da valor a los parametros

          switch (dModPreg.modo) {
            case MODELO:
              st.setString(j, dModPreg.codMod + '%');
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.codMod + '%' + " " + j);
              j++;

              break;
            case PREGUNTA:
              st.setString(j, dModPreg.codPreg + '%');
              //#// System_out.println("SrvModPreg : codPreg " + dModPreg.codPreg + '%' + " " + j);
              j++;
              break;
            case MOD_Y_PREG:
              st.setString(j, dModPreg.codMod);
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.codMod + " " + j);
              j++;
              st.setString(j, dModPreg.codPreg + '%');
              //#// System_out.println("SrvModPreg : codPreg " + dModPreg.codPreg + '%' + " " + j);
              j++;
              break;
          }

          if (param.getFilter().length() > 0) {
            st.setString(j, param.getFilter().trim());
            j++;
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          i = 1;

          while (rs.next()) {

            ////#// System_out.println("SrvModPreg: entro en el while");

            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (dModPreg.modo == MODELO) {
                data.setFilter( ( (DataModPreg) data.lastElement()).codMod);
              }
              else {
                data.setFilter( ( (DataModPreg) data.lastElement()).codPreg);
              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            CD_MODELO = (rs.getString(1));
            DS_MODELO = (rs.getString(2));

            ////#// System_out.println("SrvModPreg: CD y DS " + CD_MODELO + " " + DS_MODELO);

            if (dModPreg.modo == MODELO) {
              data.addElement(new DataModPreg(CD_MODELO, DS_MODELO,
                                              "", "", dModPreg.tSive));
              i++;
            }
            else {
              data.addElement(new DataModPreg("", "",
                                              CD_MODELO, DS_MODELO,
                                              dModPreg.tSive));
              i++;
            }
          }
          rs.close();
          //st.close();
          rs = null;
          st = null;
          break;

          //_______________________________________________________________

        case modoVOLCADO:

          dVolCas = (DataVolCasInd) param.firstElement();
          sep = dVolCas.sep;

          if (dVolCas.enfer.equals(NO_ENFER)) {

            // No se ha rellenado en cliente el campo enfermedad
            // Se seleccionan todas las enfermedades Centinelas
            //Para formar con ellas una lista separada por comas
            String enfCentinelas = new String();
            String queryEDO = "select CD_ENFCIE from SIVE_ENF_CENTI ";
            st = con.prepareStatement(queryEDO);
            rs = st.executeQuery();
            enfCentinelas = "(";
            while (rs.next()) {
              enfCentinelas += "'" + rs.getString("CD_ENFCIE") + "',";
            }
            rs.close();
            //st.close();
            rs = null;
            st = null;

            enfCentinelas = enfCentinelas.substring(0,
                enfCentinelas.lastIndexOf(',')) + ")";

            query = "select * from SIVE_CASOS_CENT  "
                + " where CD_ENFCIE in " + enfCentinelas + " and ";
          }
          else {
            //Se coge una sola enfermedad
            query = "select * from SIVE_CASOS_CENT "
                + " where CD_ENFCIE = ? and ";
          }

          if (dVolCas.anoD.equals(dVolCas.anoH)) {
            query = query +
                " CD_ANOEPI = ? and CD_SEMEPI >= ? and CD_SEMEPI <= ? ";
          }
          else {
            query = query + " ((CD_ANOEPI = ? and CD_SEMEPI >= ?) or (CD_ANOEPI > ? and CD_ANOEPI < ?) or (CD_ANOEPI = ? and CD_SEMEPI <= ?)) ";

            // prepara la lista de resultados
          }
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);
          par = 1;
          if (!dVolCas.enfer.equals(NO_ENFER)) {
            st.setString(par, dVolCas.enfer);
            par++;
          }
          if (dVolCas.anoD.equals(dVolCas.anoH)) {
            st.setString(par, dVolCas.anoD);
            par++;
            st.setString(par, dVolCas.semD);
            par++;
            st.setString(par, dVolCas.semH);
            par++;
          }
          else {
            st.setString(par, dVolCas.anoD);
            par++;
            st.setString(par, dVolCas.semD);
            par++;
            st.setString(par, dVolCas.anoD);
            par++;
            st.setString(par, dVolCas.anoH);
            par++;
            st.setString(par, dVolCas.anoH);
            par++;
            st.setString(par, dVolCas.semH);
            par++;
          }

          rs = st.executeQuery();

          // extrae el vector de casos
          i = 1;
          Vector vCasos = new Vector();

          while (rs.next()) {

            vCasos.addElement(
                new DataCasoCent(rs.getString("CD_ENFCIE"),
                                 rs.getString("CD_PCENTI"),
                                 rs.getString("CD_MEDCEN"),
                                 rs.getString("CD_ANOEPI"),
                                 rs.getString("CD_SEMEPI"),
                                 rs.getInt("NM_ORDEN"),
                                 rs.getString("CD_SEXO"),
                                 rs.getDate("FC_NAC")));

          } //Fin while del rs
          rs.close();
          rs = null;
          st = null;

          //_______________________________________________________________

          //Ya tenemos un vector de casos y en cada caso est�:
          //Pto, m�dico,a�o, semana, edad, sexo
          //Ahora se va a obtener las respuestas a preguntas seleccionadas del caso
          //Y luego se forman las lineas

          rsAux = null;
          stAux = null;

          // Se buscan las respuestas del caso a las preguntas seleccionadas
          if (!dVolCas.codsPreg.equals(NO_PREGS)) {

            for (int xi = 0; xi < vCasos.size(); xi++) {

              //Se reciogen datos del casso
              DataCasoCent datCasoCent = (DataCasoCent) (vCasos.elementAt(xi));
              String linea = new String(); //Linea que se va a a�adir al fichero

              //CAlculo de la edad
              // Para la edad en anos
              String edadA = new String("");
//          String edadM = new String("");

              java.util.Date dateNac = datCasoCent.getFecNac();

              if (dateNac != null) {
                java.util.Date hoy = new java.util.Date();
                long mili_anio = 31557600000L;
                long mili_mes = 2626560000L;
                long edad_mili = hoy.getTime() - dateNac.getTime();

                if (edad_mili > 0) {
                  aux = new Integer( (int) (edad_mili / mili_anio));
                }
                else {
                  aux = new Integer(0);
                }
                edadA = aux.toString();

              }
              else {
                edadA = "";

              } //else

              //C�lculo del sexo
              String cd_sexo = datCasoCent.getCodSex();
              if (cd_sexo == null) {
                cd_sexo = "";

              }
              linea = linea +
                  datCasoCent.getPunCen() + sep +
                  datCasoCent.getMedCen() + sep +
                  datCasoCent.getCodAno() + sep +
                  datCasoCent.getCodSem() + sep +
                  edadA + sep +
                  cd_sexo + sep;

              //HAce consulta de resptas del caso y a�ade lineas de resptas
              String queryPreg = "select DS_RESPUESTA from SIVE_RESP_CENTI "
                  + "where CD_TSIVE = ? "
                  + "and CD_ENFCIE = ? and CD_PCENTI = ? and CD_MEDCEN = ? and CD_ANOEPI = ? and CD_SEMEPI = ? "
                  + "and NM_ORDEN = ? "
                  + "and CD_PREGUNTA in " + dVolCas.codsPreg;

              stAux = con.prepareStatement(queryPreg);
//            stAux.setString(1,dVolCas.tSive);
//            stAux.setString(2, (String)vCasos.elementAt(xi));

              stAux.setString(1, dVolCas.tSive);

              stAux.setString(2, datCasoCent.getCodEnf());
              stAux.setString(3, datCasoCent.getPunCen());
              stAux.setString(4, datCasoCent.getMedCen());

              stAux.setString(5, datCasoCent.getCodAno());
              stAux.setString(6, datCasoCent.getCodSem());
              stAux.setInt(7, datCasoCent.getNumOrd());

              rsAux = stAux.executeQuery();

              String preg = new String();
//            String lin = (String) data.elementAt(xi);

              while (rsAux.next()) {
                preg = rsAux.getString("DS_RESPUESTA");
                if (preg != null) {
                  linea = linea + preg + sep;
                }
                else {
                  linea = linea + " " + sep;
                }
              }

//            data.setElementAt(lin,xi);

              data.addElement(linea);

              rsAux.close();
              stAux = null;
              rsAux = null;

            }

          }
          break;

          //____________________________________________________________________

        case modoINFOPREG:
          dInfoPreg = (DataInfoPreg) param.firstElement();
          // Se obtiene informacion de una pregunta
          //#// System_out.println("SrvModPreg: modoINFOPREG");
          query = "select distinct a.CD_MODELO, a.DS_MODELO, a.FC_ALTAM, a.FC_BAJAM, b.NM_LIN, c.DS_TEXTO "
              + " from SIVE_MODELO a, SIVE_LINEA_ITEM b, SIVE_LINEASM c "
              + " where a.CD_TSIVE = ? and "
              + " a.CD_MODELO = b.CD_MODELO and "
              + " b.CD_PREGUNTA = ? and "
              + " b.NM_LIN = c.NM_LIN ";

          if (param.getFilter().length() > 0) {
            query = query + " and b.CD_MODELO > ? ";
          }

          // prepara la lista de resultados
          data = new CLista();

          //#// System_out.println("SrvModPreg : " + query);

          // lanza la query
          st = con.prepareStatement(query);
          st.setString(1, dInfoPreg.tSive);
          //#// System_out.println("SrvModPreg : tSive " + dInfoPreg.tSive);
          st.setString(2, dInfoPreg.codPreg);
          //#// System_out.println("SrvModPreg : tSive " + dInfoPreg.codPreg);

          if (param.getFilter().length() > 0) {
            st.setString(3, dInfoPreg.codPreg);
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          i = 1;

          while (rs.next()) {

            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              data.setFilter( ( (DataInfoPreg) data.lastElement()).codMod);
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            SimpleDateFormat dateF = new SimpleDateFormat("dd/MM/yyyy");
            StringBuffer sBF = new StringBuffer();
            String fA = new String();
            String fB = new String();
            java.util.Date dateAux = rs.getDate("FC_ALTAM");
            if (dateAux != null) {
              sBF = dateF.format(dateAux, sBF, new FieldPosition(0));
              fA = sBF.toString();
            }
            else {
              fA = "-";

            }
            sBF = new StringBuffer();
            dateAux = rs.getDate("FC_BAJAM");
            if (dateAux != null) {
              sBF = dateF.format(dateAux, sBF, new FieldPosition(0));
              fB = sBF.toString();
            }
            else {
              fB = "-";
            }
            data.addElement(new DataInfoPreg(rs.getString(1), rs.getString(2),
                                             fA, fB, //rs.getString(3),rs.getString(4),
                                             rs.getString(5), dInfoPreg.codPreg,
                                             rs.getString(6), dInfoPreg.tSive));

          }
          rs.close();
          //st.close();
          rs = null;
          st = null;
          break;

          //____________________________________________________________________

          // b�squeda de modelo
        case servletSELECCION_X_CODIGO:
          dModPreg = (DataModPreg) param.firstElement();
          caso = 0;
          //#// System_out.println("SrvModPreg: servletSELECCION_X_CODIGO");

          switch (dModPreg.modo) {
            case MODELO:

              //#// System_out.println("SrvModPreg: MODELO");
              query = "select CD_MODELO, DS_MODELO from SIVE_MODELO "
                  + " where CD_TSIVE = ? and ";
              if (param.getFilter().length() > 0) {
                query = query +
                    " CD_MODELO like ? and CD_MODELO > ? order by CD_MODELO";
              }
              else {
                query = query + " CD_MODELO like ? order by CD_MODELO";
              }
              break;
            case PREGUNTA:

              //#// System_out.println("SrvModPreg: PREGUNTA");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and ";
              if (param.getFilter().length() > 0) {
                query = query +
                    " CD_PREGUNTA like ? and CD_PREGUNTA > ? order by CD_PREGUNTA";
              }
              else {
                query = query + " CD_PREGUNTA like ? order by CD_PREGUNTA";
              }
              break;
            case MOD_Y_PREG:

              //#// System_out.println("SrvModPreg: MOD_Y_PREG");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and "
                  + " CD_PREGUNTA in (select CD_PREGUNTA from SIVE_LINEA_ITEM where CD_MODELO = ?) and ";
              if (param.getFilter().length() > 0) {
                query = query +
                    " CD_PREGUNTA like ? and CD_PREGUNTA > ? order by CD_PREGUNTA";
              }
              else {
                query = query + " CD_PREGUNTA like ? order by CD_PREGUNTA";
              }
              break;
          }

          // prepara la lista de resultados
          data = new CLista();

          //#// System_out.println("SrvModPreg : " + query);

          // lanza la query
          st = con.prepareStatement(query);
          j = 1;
          st.setString(j, dModPreg.tSive);
          //#// System_out.println("SrvModPreg : tSive " + dModPreg.tSive + " " + j);
          j++;

          // Da valor a los parametros

          switch (dModPreg.modo) {
            case MODELO:
              st.setString(j, dModPreg.codMod + '%');
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.codMod + '%' + " " + j);
              j++;
              break;
            case PREGUNTA:
              st.setString(j, dModPreg.codPreg + '%');
              //#// System_out.println("SrvModPreg : codPreg " + dModPreg.codPreg + '%' + " " + j);
              j++;
              break;
            case MOD_Y_PREG:
              st.setString(j, dModPreg.codMod);
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.codMod + " " + j);
              j++;
              st.setString(j, dModPreg.codPreg + '%');
              //#// System_out.println("SrvModPreg : codPreg " + dModPreg.codPreg + '%' + " " + j);
              j++;
              break;
          }

          if (param.getFilter().length() > 0) {
            st.setString(j, param.getFilter().trim());
            j++;
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          i = 1;

          while (rs.next()) {

            ////#// System_out.println("SrvModPreg: entro en el while");

            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (dModPreg.modo == MODELO) {
                data.setFilter( ( (DataModPreg) data.lastElement()).codMod);
              }
              else {
                data.setFilter( ( (DataModPreg) data.lastElement()).codPreg);
              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            CD_MODELO = (rs.getString(1));
            DS_MODELO = (rs.getString(2));

            ////#// System_out.println("SrvModPreg: CD y DS " + CD_MODELO + " " + DS_MODELO);

            if (dModPreg.modo == MODELO) {
              data.addElement(new DataModPreg(CD_MODELO, DS_MODELO,
                                              "", "", dModPreg.tSive));
              i++;
            }
            else {
              data.addElement(new DataModPreg("", "",
                                              CD_MODELO, DS_MODELO,
                                              dModPreg.tSive));
              i++;
            }
          }
          rs.close();
          //st.close();
          rs = null;
          st = null;
          break;

          //____________________________________________________________________

        case servletSELECCION_X_DESCRIPCION:
          dModPreg = (DataModPreg) param.firstElement();
          //#// System_out.println("SrvModPreg: servletSELECCION_X_DESCRIPCION");
          caso = 0;

          switch (dModPreg.modo) {
            case MODELO:

              //#// System_out.println("SrvModPreg: MODELO");
              query = "select CD_MODELO, DS_MODELO from SIVE_MODELO "
                  + " where CD_TSIVE = ? and ";
              if (param.getFilter().length() > 0) {
                query = query +
                    " DS_MODELO like ? and DS_MODELO > ? order by DS_MODELO";
              }
              else {
                query = query + " DS_MODELO like ? order by DS_MODELO";
              }
              break;
            case PREGUNTA:

              //#// System_out.println("SrvModPreg: PREGUNTA ");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and ";
              if (param.getFilter().length() > 0) {
                query = query +
                    " DS_PREGUNTA like ? and DS_PREGUNTA > ? order by DS_PREGUNTA";
              }
              else {
                query = query + " DS_PREGUNTA like ? order by DS_PREGUNTA";
              }
              break;
            case MOD_Y_PREG:

              //#// System_out.println("SrvModPreg: MOD_Y_PREG");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and "
                  + " CD_PREGUNTA in (select CD_PREGUNTA from SIVE_LINEA_ITEM where CD_MODELO = ?) and ";
              if (param.getFilter().length() > 0) {
                query = query +
                    " DS_PREGUNTA like ? and DS_PREGUNTA > ? order by DS_PREGUNTA";
              }
              else {
                query = query + " DS_PREGUNTA like ? order by DS_PREGUNTA";
              }
              break;
          }

          // prepara la lista de resultados
          data = new CLista();

          //#// System_out.println("SrvModPreg : " + query);

          // lanza la query
          st = con.prepareStatement(query);
          j = 1;
          st.setString(j, dModPreg.tSive);
          //#// System_out.println("SrvModPreg : tSive " + dModPreg.tSive + " " + j);
          j++;

          // Da valor a los parametros

          switch (dModPreg.modo) {
            case MODELO:
              st.setString(j, dModPreg.desMod + '%');
              //#// System_out.println("SrvModPreg : desMod " + dModPreg.desMod + '%'+ " " + j);
              j++;
              break;
            case PREGUNTA:
              st.setString(j, dModPreg.desPreg + '%');
              //#// System_out.println("SrvModPreg : desPreg " + dModPreg.desPreg + '%'+ " " + j);
              j++;
              break;
            case MOD_Y_PREG:
              st.setString(j, dModPreg.codMod);
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.desMod + " " + j);
              j++;
              st.setString(j, dModPreg.desPreg + '%');
              //#// System_out.println("SrvModPreg : desPreg " + dModPreg.desPreg + '%'+ " " + j);
              j++;
              break;
          }

          if (param.getFilter().length() > 0) {
            st.setString(j, param.getFilter().trim());
            j++;
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          i = 1;

          while (rs.next()) {
            ////#// System_out.println("SrvModPreg : entro en el while ");
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (dModPreg.modo == MODELO) {
                data.setFilter( ( (DataModPreg) data.lastElement()).desMod);
              }
              else {
                data.setFilter( ( (DataModPreg) data.lastElement()).desPreg);
              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            CD_MODELO = (rs.getString(1));
            DS_MODELO = (rs.getString(2));

            ////#// System_out.println("SrvModPreg: CD y DS " + CD_MODELO + " " + DS_MODELO);

            if (dModPreg.modo == MODELO) {
              data.addElement(new DataModPreg(CD_MODELO, DS_MODELO,
                                              "", "", dModPreg.tSive));
              i++;
            }
            else {
              data.addElement(new DataModPreg("", "",
                                              CD_MODELO, DS_MODELO,
                                              dModPreg.tSive));
              i++;
            }
          }
          rs.close();
          //st.close();
          rs = null;
          st = null;
          break;

          //_________________________________________________________

        case servletOBTENER_X_CODIGO:
          dModPreg = (DataModPreg) param.firstElement();
          //#// System_out.println("SrvModPreg: servletOBTENER_X_CODIGO");
          caso = 0;

          switch (dModPreg.modo) {
            case MODELO:

              //#// System_out.println("SrvModPreg: MODELO");
              caso = MODELO;
              query = "select CD_MODELO, DS_MODELO from SIVE_MODELO "
                  + " where CD_TSIVE = ? and ";
              query = query + " CD_MODELO = ? order by CD_MODELO";
              break;
            case PREGUNTA:

              //#// System_out.println("SrvModPreg: PREGUNTA");
              caso = PREGUNTA;
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and ";
              query = query + " CD_PREGUNTA = ? order by CD_PREGUNTA";
              break;
            case MOD_Y_PREG:

              //#// System_out.println("SrvModPreg: MOD_Y_PREG");
              caso = MOD_Y_PREG;
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and "
                  + " CD_PREGUNTA in (select CD_PREGUNTA from SIVE_LINEA_ITEM where CD_MODELO = ?) and ";
              query = query + " CD_PREGUNTA = ? order by CD_PREGUNTA";
              break;
          }

          // prepara la lista de resultados
          data = new CLista();

          //#// System_out.println("SrvModPreg : " + query);

          // lanza la query
          st = con.prepareStatement(query);
          j = 1;
          st.setString(j, dModPreg.tSive);
          j++;

          // Da valor a los parametros

          switch (dModPreg.modo) {
            case MODELO:
              st.setString(j, dModPreg.codMod);
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.codMod);
              j++;
              break;
            case PREGUNTA:
              st.setString(j, dModPreg.codPreg);
              //#// System_out.println("SrvModPreg : codPreg " + dModPreg.codPreg);
              j++;
              break;
            case MOD_Y_PREG:
              st.setString(j, dModPreg.codMod);
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.codMod);
              j++;
              st.setString(j, dModPreg.codPreg);
              //#// System_out.println("SrvModPreg : codPreg " + dModPreg.codPreg);
              j++;
              break;
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          i = 1;

          while (rs.next()) {
            ////#// System_out.println("SrvModPreg: dentro de while (rs.next())");

            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            CD_MODELO = (rs.getString(1));
            DS_MODELO = (rs.getString(2));

            ////#// System_out.println("SrvModPreg: cd y ds " + CD_MODELO + " " + DS_MODELO);

            if (dModPreg.modo == MODELO) {
              data.addElement(new DataModPreg(CD_MODELO, DS_MODELO,
                                              "", "", dModPreg.tSive));
              i++;
            }
            else {
              data.addElement(new DataModPreg("", "",
                                              CD_MODELO, DS_MODELO,
                                              dModPreg.tSive));
              i++;
            }
          }
          rs.close();
          //st.close();
          rs = null;
          st = null;
          break;

          //_________________________________________________________

        case servletOBTENER_X_DESCRIPCION:
          dModPreg = (DataModPreg) param.firstElement();
          caso = 0;
          //#// System_out.println("SrvModPreg: OBTENER_X_DESCRIPCIOM");

          switch (dModPreg.modo) {
            case MODELO:
              caso = MODELO;
              //#// System_out.println("SrvModPreg: MODELO");
              query = "select CD_MODELO, DS_MODELO from SIVE_MODELO "
                  + " where CD_TSIVE = ? and ";
              query = query + " DS_MODELO = ? order by DS_MODELO";
              break;
            case PREGUNTA:
              caso = PREGUNTA;
              //#// System_out.println("SrvModPreg: PREGUNTA");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and ";
              query = query + " DS_PREGUNTA = ? order by DS_PREGUNTA";
              break;
            case MOD_Y_PREG:
              caso = MOD_Y_PREG;
              //#// System_out.println("SrvModPreg: MOD_Y_PREG ");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and "
                  + " CD_PREGUNTA in (select CD_PREGUNTA from SIVE_LINEA_ITEM where CD_MODELO = ?) and ";
              query = query + " DS_PREGUNTA = ? order by DS_PREGUNTA";
              break;
          }

          // prepara la lista de resultados
          data = new CLista();

          //#// System_out.println("SrvModPreg : " + query);

          // lanza la query
          st = con.prepareStatement(query);
          j = 1;
          st.setString(j, dModPreg.tSive);
          //#// System_out.println("SrvModPreg : tSive " + dModPreg.tSive + " " + j);
          j++;

          // Da valor a los parametros

          switch (dModPreg.modo) {
            case MODELO:
              st.setString(j, dModPreg.desMod);
              //#// System_out.println("SrvModPreg : desMod " + dModPreg.desMod + " " + j);
              j++;
              break;
            case PREGUNTA:
              st.setString(j, dModPreg.desPreg);
              //#// System_out.println("SrvModPreg : desPreg " + dModPreg.desPreg + " " + j);
              j++;
              break;
            case MOD_Y_PREG:
              st.setString(j, dModPreg.codMod);
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.codMod + " " + j);
              j++;
              st.setString(j, dModPreg.desPreg);
              //#// System_out.println("SrvModPreg : desPreg " + dModPreg.desPreg + " " + j);
              j++;
              break;
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          i = 1;

          while (rs.next()) {
            ////#// System_out.println("SrvModPreg : entro en while");

            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            CD_MODELO = (rs.getString(1));
            DS_MODELO = (rs.getString(2));

            ////#// System_out.println("SrvModPreg: CD y DS " + CD_MODELO + " " + DS_MODELO);

            if (dModPreg.modo == MODELO) {
              data.addElement(new DataModPreg(CD_MODELO, DS_MODELO,
                                              "", "", dModPreg.tSive));
              i++;
            }
            else {
              data.addElement(new DataModPreg("", "",
                                              CD_MODELO, DS_MODELO,
                                              dModPreg.tSive));
              i++;
            }
          }
          rs.close();
          //st.close();
          rs = null;
          st = null;
          break;

      }
      //con.commit();
    }
    catch (Exception exs) {
      //con.rollback();

      String mensa = exs.getMessage();
      //if (mensa.indexOf("ORA-00000")==-1){
      exs.printStackTrace();
      throw exs;
      //}
    }
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      //#// System_out.println("SrvModPreg: data no es null " + data.size());
      data.trimToSize();
    }

    return data;
  }
}
