package infedoind;

import java.io.Serializable;

public class DataRespCompInfedoind
    implements Serializable {
  protected String Caso = "";
  protected String CodModelo = "";
  protected String Num = "";
  protected String CodPregunta = "";
  protected String Nivel = "";
  protected String IT_OK = "";
  protected String Des = ""; //V
  protected String ValorLista = ""; //varon
  protected String TipoPregunta = ""; // C, N, F, B, L

  public DataRespCompInfedoind(String caso,
                               String codM, String num,
                               String pregunta, String nivel, String activo,
                               String des,
                               String valorlista,
                               String tipoPreg) {

    Caso = caso;
    CodModelo = codM;
    Num = num;
    CodPregunta = pregunta;
    Nivel = nivel;
    IT_OK = activo;
    Des = des;
    ValorLista = valorlista;
    TipoPregunta = tipoPreg;

  } //fin construct

  public String getIT_OK() {
    return IT_OK;
  }

  public String getNivel() {
    return Nivel;
  }

  public String getCodModelo() {
    return CodModelo;
  }

  public String getCaso() {
    return Caso;
  }

  public String getNumLinea() {
    return Num;
  }

  public String getCodPregunta() {
    return CodPregunta;
  }

  public String getDesPregunta() {
    return Des;
  }

  public String getValorLista() {
    return ValorLista;
  }

  public String getTipoPregunta() {
    return TipoPregunta;
  }

}
