//Para pedir las lineas de UN CASO con sus repuestas

package infedoind;

import java.io.Serializable;

//soporta los datos para hacer la query base
public class DataPetLineas
    implements Serializable {

  //Datos recogidos del caso
  protected String sCodEnf = "";
  protected String sCA = "";
  protected String sN1 = "";
  protected String sN2 = "";
  protected String sNumCas = "";
  //Datos recogidos del applet (Strings que indican niv1 y niv2 en cada comunidad)
  protected String sNiv1App = "";
  protected String sNiv2App = "";

  public DataPetLineas(String CodEnf,
                       String sComunidad, String Nivel1, String Nivel2,
                       String numCas, String niv1App, String niv2App) {

    sCodEnf = CodEnf;
    sCA = sComunidad;
    sN1 = Nivel1;
    sN2 = Nivel2;
    sNumCas = numCas;
    sNiv1App = niv1App;
    sNiv2App = niv2App;
  }

  public String getCodEnfermedad() {
    return sCodEnf;
  }

  public String getComunidad() {
    return sCA;
  }

  public String getNivelUNO() {
    return sN1;
  }

  public String getNivelDOS() {
    return sN2;
  }

  public String getNM_EDO() {
    return sNumCas;
  }

  //Datos del applet
  public String getNiv1App() {
    return sNiv1App;
  }

  public String getNiv2App() {
    return sNiv2App;
  }

}
