//Title:        Gr�fico comparativo de una enfermedad
//Version:
//Copyright:    Copyright (c) 1999
//Author:
//Company:      NorSistemas
//Description:

package GrafComEnf;

import java.io.Serializable;

public class parGrafCom
    implements Serializable {

  public String anoDesde = null;
  public String semDesde = null;
  public String anoHasta = null;
  public String semHasta = null;

  public String CD_ENFERMEDAD = null;
  public String DS_ENFERMEDAD = null;

  public String CD_CA = null;

  public parGrafCom() {
  }
}
