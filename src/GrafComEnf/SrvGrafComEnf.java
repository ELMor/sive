package GrafComEnf;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

// LORTAD
import Registro.RegistroConsultas;
import capp.CLista;
import sapp.DBServlet;

public class SrvGrafComEnf
    extends DBServlet {

  // informes
  final int GRAFICO_COMUNIDAD = 100;
  final double CTE_TASAS = 100000.0;

  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  protected CLista doWork(int opmode, CLista param) throws Exception {
    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // buffers
    parGrafCom parametro = null;

    // Auxiliares
    String strQuery = null;

    String strCD_ENFERMEDAD_ENFERE = null;
    String strCD_ENFERMEDAD = null;
    String strDS_ENFERMEDAD = null;
    String strCD_TVIGI = null;

    int elValor = 0;
    int iPoblacionCA = 0;
    int iEdadPoblacion = -1;

    double dTasaCA = 0;
    double dTasaArea = 0;

    /****************** APLICACION DE LORTAD ********************************/
    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;
          /****************** APLICACION DE LORTAD ********************************/

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);
    con.setAutoCommit(true);

    // Recupera el par�metro
    parametro = (parGrafCom) param.firstElement();

    // Devoluci�n de valores
    CLista data = new CLista();
    Vector vectorTasasAreas = null;
    parGrafComCA pgccaDatos = null;
    parGrafComDesv pgcdDatos = null;

    // modos de operaci�n
    switch (opmode) {

      case GRAFICO_COMUNIDAD:

        try {
          // Se obtiene el nombre de la CA
          strQuery = new String(
              " select CD_CA, DS_CA, DSL_CA from SIVE_COM_AUT "
              + " where CD_CA = ?");

          st = con.prepareStatement(strQuery);

          st.setString(1, parametro.CD_CA);

          rs = st.executeQuery();

          pgccaDatos = new parGrafComCA();
          if (rs.next()) {
            pgccaDatos.dsCA = rs.getString(2);
          }

          rs.close();
          st.close();
          rs = null;
          st = null;

          // Se recuperan los datos de la enfermedad
          strQuery = new String(
              " select a.CD_ENFCIE, b.DS_PROCESO, a.CD_TVIGI " +
              " from SIVE_ENFEREDO a, SIVE_PROCESOS b " +
              " where a.CD_ENFCIE = b.CD_ENFCIE and " +
              " a.CD_ENFCIE = ? ");

          st = con.prepareStatement(strQuery);
          st.setString(1, parametro.CD_ENFERMEDAD);

          rs = st.executeQuery();

          if (rs.next()) {
            strCD_ENFERMEDAD = new String(rs.getString(1));
            strDS_ENFERMEDAD = new String(rs.getString(2));
            strCD_TVIGI = new String(rs.getString(3));
          }

          rs.close();
          st.close();
          rs = null;
          st = null;

          // Se obtienen los casos de la enfermedad

          //AIC
          //// System_out.println("srvgrafcomenf  " + strCD_TVIGI);
          //// System_out.println(strCD_TVIGI.equals("I"));

          if (strCD_TVIGI.equals("I")) {
            strQuery = new String(
                "select count(NM_EDO) from sive_edoind where CD_ENFCIE = ? ");
          }
          else {
            strQuery = new String(
                "select sum(NM_CASOS) from sive_edonum where CD_ENFCIE = ? ");
          }

          if (parametro.anoDesde.equals(parametro.anoHasta)) {
            strQuery = new String(strQuery +
                "and CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ");
          }
          else {
            strQuery = new String(strQuery +
                " and ((CD_ANOEPI = ? and CD_SEMEPI >= ? )or "
                + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
                + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))");
          }

          st = con.prepareStatement(strQuery);

          st.setString(1, strCD_ENFERMEDAD);
          if (strCD_TVIGI.equals("I")) {
            registroConsultas.insertarParametro(strCD_ENFERMEDAD);
          }

          if (parametro.anoDesde.equals(parametro.anoHasta)) {
            // semana desde
            st.setString(2, parametro.semDesde);
            // semana hasta
            st.setString(3, parametro.semHasta);
            // a�o
            st.setString(4, parametro.anoDesde);

            if (strCD_TVIGI.equals("I")) {
              registroConsultas.insertarParametro(parametro.semDesde);
              registroConsultas.insertarParametro(parametro.semHasta);
              registroConsultas.insertarParametro(parametro.anoDesde);

            }
          }
          else {
            // a�o inicial
            st.setString(2, parametro.anoDesde);
            // semana inicial
            st.setString(3, parametro.semDesde);
            st.setString(4, parametro.anoDesde);
            // a�o final
            st.setString(5, parametro.anoHasta);
            st.setString(6, parametro.anoHasta);
            // semana final
            st.setString(7, parametro.semHasta);

            if (strCD_TVIGI.equals("I")) {
              registroConsultas.insertarParametro(parametro.anoDesde);
              registroConsultas.insertarParametro(parametro.semDesde);
              registroConsultas.insertarParametro(parametro.anoDesde);
              registroConsultas.insertarParametro(parametro.anoHasta);
              registroConsultas.insertarParametro(parametro.anoHasta);
              registroConsultas.insertarParametro(parametro.semHasta);
            }
          }

          if (strCD_TVIGI.equals("I")) {
            registroConsultas.registrar("SIVE_EDOIND",
                                        strQuery,
                                        "CD_ENFERMO",
                                        "",
                                        "SrvGrafComEnf",
                                        true);
          }

          rs = st.executeQuery();

          if (rs.next()) {
            elValor = rs.getInt(1);
            // // System_out.println("El valor es " + String.valueOf(elValor));
          }

          rs.close();
          st.close();
          rs = null;
          st = null;

          // Se obtiene la poblaci�n de la CA y se calcula su tasa

          // Si la enfermedad es CD_PARALISIS, la edad de la poblaci�n ha de ser
          // menor de 15 a�os.

          strCD_ENFERMEDAD_ENFERE = new String(infcata.SrvInfEDOcata.
                                               getCDEnfermedad(con,
              strCD_ENFERMEDAD));
          if (strCD_ENFERMEDAD_ENFERE.equals(infcata.SrvInfEDOcata.CD_PARALISIS)) {
            iEdadPoblacion = 15;
          }
          else {
            iEdadPoblacion = -1;
          }

          iPoblacionCA = infcata.SrvInfEDOcata.getPoblacion(con,
              parametro.anoDesde, "", "", iEdadPoblacion);

          dTasaCA = elValor * CTE_TASAS / iPoblacionCA;

          pgccaDatos.tasaCA = dTasaCA;

          //AIC
          //// System_out.println("SrvGrafComEnf: " + strQuery + "...." + String.valueOf(dTasaCA) + " - " + String.valueOf(iPoblacionCA));

          // Se obtienen los mismos datos para cada �rea,
          // m�s el c�digo y la descripci�n de las mismas.

          if (strCD_TVIGI.equals("I")) {
            strQuery = new String(
                "select count(NM_EDO), sn.CD_NIVEL_1, DS_NIVEL_1 from "
                + " sive_edoind se, sive_nivel1_s sn"
                + " where se.CD_NIVEL_1=sn.CD_NIVEL_1 and CD_ENFCIE = ? ");
          }
          else {
            strQuery = new String(
                "select sum(NM_CASOS), sn.CD_NIVEL_1, DS_NIVEL_1 from "
                + " sive_edonum se, sive_e_notif st, sive_nivel1_s sn"
                + " where se.CD_E_NOTIF=st.CD_E_NOTIF and "
                + " sn.CD_NIVEL_1=st.CD_NIVEL_1 and "
                + " CD_ENFCIE = ? ");
          }

          if (parametro.anoDesde.equals(parametro.anoHasta)) {
            strQuery = new String(strQuery +
                " and se.CD_SEMEPI >= ? and se.CD_SEMEPI <= ? and se.CD_ANOEPI = ? "
                + " group by sn.CD_NIVEL_1, DS_NIVEL_1");
          }
          else {
            strQuery = new String(strQuery +
                " and ((se.CD_ANOEPI = ? and se.CD_SEMEPI>= ? )or "
                + " (se.CD_ANOEPI > ? and se.CD_ANOEPI < ? )or "
                + " (se.CD_ANOEPI = ? and se.CD_SEMEPI <= ? ))"
                + " group by sn.CD_NIVEL_1, DS_NIVEL_1");
          }

          //AIC
          //// System_out.println(strQuery);

          st = con.prepareStatement(strQuery);
          st.setString(1, strCD_ENFERMEDAD);
          if (strCD_TVIGI.equals("I")) {
            registroConsultas.insertarParametro(strCD_ENFERMEDAD);
          }

          if (parametro.anoDesde.equals(parametro.anoHasta)) {
            // semana desde
            st.setString(2, parametro.semDesde);
            // semana hasta
            st.setString(3, parametro.semHasta);
            // a�o
            st.setString(4, parametro.anoDesde);
            if (strCD_TVIGI.equals("I")) {
              registroConsultas.insertarParametro(parametro.semDesde);
              registroConsultas.insertarParametro(parametro.semHasta);
              registroConsultas.insertarParametro(parametro.anoDesde);

            }

          }
          else {
            // a�o inicial
            st.setString(2, parametro.anoDesde);
            // semana inicial
            st.setString(3, parametro.semDesde);
            st.setString(4, parametro.anoDesde);
            // a�o final
            st.setString(5, parametro.anoHasta);
            st.setString(6, parametro.anoHasta);
            // semana final
            st.setString(7, parametro.semHasta);

            if (strCD_TVIGI.equals("I")) {
              registroConsultas.insertarParametro(parametro.anoDesde);
              registroConsultas.insertarParametro(parametro.semDesde);
              registroConsultas.insertarParametro(parametro.anoDesde);
              registroConsultas.insertarParametro(parametro.anoHasta);
              registroConsultas.insertarParametro(parametro.anoHasta);
              registroConsultas.insertarParametro(parametro.semHasta);
            }
          }

          if (strCD_TVIGI.equals("I")) {
            registroConsultas.registrar("SIVE_EDOIND",
                                        strQuery,
                                        "CD_ENFERMO",
                                        "",
                                        "SrvGrafComEnf",
                                        true);
          }
          rs = st.executeQuery();

          vectorTasasAreas = new Vector();

          while (rs.next()) {
            pgcdDatos = new parGrafComDesv();

            pgcdDatos.cdArea = rs.getString(2);
            pgcdDatos.dsArea = rs.getString(3);

            dTasaArea = CTE_TASAS * rs.getInt(1) /
                infcata.SrvInfEDOcata.getPoblacion(con, parametro.anoDesde,
                pgcdDatos.cdArea, "", iEdadPoblacion);

            pgcdDatos.tasaArea = dTasaArea;

            //AIC
            //// System_out.println(dTasaArea);

            if (dTasaCA != 0) {
              pgcdDatos.porcentaje = (dTasaArea - dTasaCA) * 100.0 / dTasaCA;
            }
            vectorTasasAreas.addElement(pgcdDatos);
          }

          rs.close();
          st.close();
          rs = null;
          st = null;

          //*********************

        }
        catch (Exception e) {
          // // System_out.println("srvGrafComEnf: " + e.getMessage());
          // e.printStackTrace();
          throw e;
        }

        break;
    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    if (vectorTasasAreas.size() > 0) {
      vectorTasasAreas.trimToSize();
      data.addElement(vectorTasasAreas);
      data.addElement(pgccaDatos);
      data.addElement(iEdadPoblacion == -1 ? "" : "*");
      // registros totales
      data.trimToSize();
    }
    else {
      data = null;
    }

    // // System_out.println("srvGrafComEnf: Saliendo");

    return data;
  }
}
