//Title:        Gr�fico comparativo de una enfermedad
//Version:
//Copyright:    Copyright (c) 1999
//Author:
//Company:      NorSistemas
//Description:

package GrafComEnf;

import java.net.URL;
import java.util.Vector;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo.DataCat;
import eapp.DataGraf;
import eapp.GPanel;
import eapp.PanelChart;
import sapp.StubSrvBD;
import utilidades.PanFechas;

public class panGrafCom
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  final int modoESPERA = 2;

  //Modos de operacion del servlet
  final int ALA_AUTO_SEM = 1;

  private final String imgNAME[] = {
      "images/Magnify.gif",
      "images/vaciar.gif",
      "images/grafico.gif"};

  protected CCargadorImagen imgs = null;

  protected StubSrvBD stubCliente = new StubSrvBD();

  public parGrafCom parametro;

  protected int modoOperacion = modoNORMAL;

  // stub's
  final String strSERVLETGraf = "servlet/SrvGrafComEnf";
  final String strSERVLETEnf = "servlet/SrvEnfVigi";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  final int GRAFICO_COMUNIDAD = 100;

  XYLayout xYLayout = new XYLayout();

  Label lblDesde = new Label();

  PanFechas panelDesde;
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  focusAdapter txtFocusAdapter = new focusAdapter(this);

  txt_keyAdapter txtKeyAdapter = new txt_keyAdapter(this);

  actionListener btnActionListener = new actionListener(this);

  Label lblEnf = new Label();
  CCampoCodigo txtCodEnf = new CCampoCodigo();
  ButtonControl btnCtrlBuscarEnf = new ButtonControl();
  TextField txtDesEnf = new TextField();
  Label lblHasta = new Label();
  utilidades.PanFechas panelHasta = null;

  XYLayout xyFondo = new XYLayout();

  public panGrafCom(CApp a) {
    try {
      setApp(a);
      panelDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      panelHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      jbInit();
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xyFondo.setHeight(240);
    btnLimpiar.setLabel("Limpiar");
    btnInforme.setLabel("Consultar");

    lblEnf.setText("Enfermedad:");
    lblHasta.setText("Hasta:");
    lblDesde.setText("Desde:");

    txtCodEnf.addKeyListener(txtKeyAdapter);
    txtCodEnf.addFocusListener(txtFocusAdapter);
    txtCodEnf.setName("txtCodEnf");
    txtCodEnf.setBackground(new Color(255, 255, 150));
    txtDesEnf.setEditable(false);
    txtDesEnf.setEnabled(false);

    btnCtrlBuscarEnf.setActionCommand("buscarEnf");
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    // gestores de eventos

    btnCtrlBuscarEnf.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    xyFondo.setHeight(301);
    xyFondo.setWidth(596);

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnCtrlBuscarEnf.setImage(imgs.getImage(0));
    btnLimpiar.setImage(imgs.getImage(1));
    btnInforme.setImage(imgs.getImage(2));

    xYLayout.setWidth(607);

    this.setLayout(xYLayout);

    this.add(lblDesde, new XYConstraints(26, 42, 46, -1));
    this.add(panelDesde, new XYConstraints(40, 42, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 82, 51, -1));
    this.add(panelHasta, new XYConstraints(40, 82, -1, -1));

    this.add(lblEnf, new XYConstraints(26, 122, -1, -1));
    this.add(txtCodEnf, new XYConstraints(111, 122, 107, -1));
    this.add(btnCtrlBuscarEnf, new XYConstraints(226, 122, -1, -1));
    this.add(txtDesEnf, new XYConstraints(255, 122, 252, -1));
    this.add(btnLimpiar, new XYConstraints(388, 186, -1, -1));
    this.add(btnInforme, new XYConstraints(474, 186, -1, -1));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (panelDesde.txtAno.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    if ( (panelDesde.txtCodSem.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    if ( (txtDesEnf.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtCodEnf.setEnabled(true);
        btnCtrlBuscarEnf.setEnabled(true);
        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:

        txtCodEnf.setEnabled(false);
        btnCtrlBuscarEnf.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }

    this.doLayout();
  }

  //busca la enfermedad
  void btnCtrlBuscarEnf_actionPerformed(ActionEvent evt) {
    DataCat datosPantalla = null;
    CMessage msgBox = null;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      CListaEnfEDO lista = new CListaEnfEDO(app,
                                            "Selecci�n de enfermedad EDO",
                                            stubCliente,
                                            strSERVLETEnf,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      datosPantalla = (DataCat) lista.getComponente();

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (datosPantalla != null) {
      txtCodEnf.removeKeyListener(txtKeyAdapter);
      txtCodEnf.setText(datosPantalla.getCod());
      txtDesEnf.setText(datosPantalla.getDes());
      txtCodEnf.addKeyListener(txtKeyAdapter);
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    panelDesde.txtAno.setText("");
    panelDesde.txtCodSem.setText("");
    panelDesde.txtFecSem.setText("");

    panelHasta.txtAno.setText("");
    panelHasta.txtCodSem.setText("");
    panelHasta.txtFecSem.setText("");

    txtCodEnf.setText("");
    txtDesEnf.setText("");

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    CMessage msgBox;

    parametro = new parGrafCom();

    CLista listaParametros = null;
    CLista listaResultados = null;

    if (isDataValid()) {

      if (!txtDesEnf.getText().equals("")) {
        parametro.CD_ENFERMEDAD = txtCodEnf.getText();
        parametro.DS_ENFERMEDAD = txtDesEnf.getText();
      }

      parametro.anoDesde = panelDesde.txtAno.getText();
      if (panelDesde.txtCodSem.getText().length() == 1) {
        parametro.semDesde = "0" + panelDesde.txtCodSem.getText();
      }
      else {
        parametro.semDesde = panelDesde.txtCodSem.getText();

      }
      parametro.anoHasta = panelHasta.txtAno.getText();

      if (panelHasta.txtCodSem.getText().length() == 1) {
        parametro.semHasta = "0" + panelHasta.txtCodSem.getText();
      }
      else {
        parametro.semHasta = panelHasta.txtCodSem.getText();

      }
      parametro.CD_CA = this.app.getCA();

      modoOperacion = modoESPERA;
      Inicializar();

      listaParametros = new CLista();

      listaParametros.setIdioma(getApp().getIdioma());
      listaParametros.setPerfil(getApp().getPerfil());
      listaParametros.setLogin(getApp().getLogin());

      listaParametros.addElement(parametro);

      try {
        stubCliente.setUrl(new URL(app.getURL() + strSERVLETGraf));

        listaParametros.setLogin(app.getLogin());
        listaParametros.setLortad(app.getParameter("LORTAD"));

        listaResultados = (CLista) stubCliente.doPost(GRAFICO_COMUNIDAD,
            listaParametros);
        if (listaResultados != null) {
          pintaGrafico( (Vector) listaResultados.firstElement(), parametro,
                       ( (String) listaResultados.elementAt(2)).equals("*"));
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                                "No hay datos que visualizar.");
          msgBox.show();
          msgBox = null;
        }
      }
      catch (Exception e) {
        // System_out.println(e.getMessage());
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            "Faltan campos obligatorios.");
      msgBox.show();
      msgBox = null;
    }

  }

  // se pinta el gr�fico
  private void pintaGrafico(Vector puntos, parGrafCom datos, boolean bCriterio3) {

    String strCriterio1 = null;
    String strCriterio2 = null;
    String strCriterio3 = null;

    double rawData[][] = null;

    String series_Y[] = null;
    String series_X[] = {
        "porcentaje"};

    series_Y = new String[puntos.size()];
    rawData = new double[2][puntos.size()];

    for (int i = 0; i < puntos.size(); i++) {
      series_Y[i] = ( (parGrafComDesv) puntos.elementAt(i)).dsArea;
      rawData[0][i] = i + 1;
      rawData[1][i] = ( (parGrafComDesv) puntos.elementAt(i)).porcentaje;
    }

    DataGraf data = new DataGraf("Leyenda", rawData, series_X, series_Y);

    GPanel panel = new GPanel(this.app, PanelChart.BARRAS_INVERTIDAS);
    panel.chart.setDatosLabels(data, series_Y);

    strCriterio1 = new String("Desde a�o: " + datos.anoDesde
                              + " Semana: " + datos.semDesde + ", Hasta a�o: " +
                              datos.anoHasta
                              + " Semana: " + datos.semHasta + ".");

    strCriterio2 = new String("Enfermedad: " + datos.CD_ENFERMEDAD + " - "
                              + datos.DS_ENFERMEDAD + ".");

    if (bCriterio3) {
      strCriterio3 = new String("NOTA: los c�lculos se han efectuado a partir "
                                + "de la poblaci�n menor de 15 a�os.");
    }
    else {
      strCriterio3 = new String();
    }

    panel.chart.setCriterios(strCriterio3, strCriterio2, strCriterio1);

    panel.chart.setTitulo("GR�FICO COMPARATIVO DE UNA ENFERMEDAD");
    panel.chart.setTituloEje("�reas", "%");

    panel.show();
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    // datos de envio
    DataCat enf;
    //DataIndAuto ind;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtCodEnf")) &&
        (txtCodEnf.getText().length() > 0)) {
      param = new CLista();
      param.addElement(new DataCat(txtCodEnf.getText()));
      strServlet = strSERVLETEnf;
      modoServlet = servletOBTENER_X_CODIGO;
    }

    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        param.setIdioma(getApp().getIdioma());
        param.setPerfil(getApp().getPerfil());
        param.setLogin(getApp().getLogin());

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtCodEnf")) {
            enf = (DataCat) param.firstElement();
            txtCodEnf.removeKeyListener(txtKeyAdapter);
            txtCodEnf.setText(enf.getCod());
            txtDesEnf.setText(enf.getDes());
            txtCodEnf.addKeyListener(txtKeyAdapter);
          }
        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO, "No hay datos.");
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtCodEnf"))) {
      txtDesEnf.setText("");

    }

    Inicializar();
  }
} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  panGrafCom adaptee = null;
  ActionEvent e = null;

  public actionListener(panGrafCom adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarEnf")) {
      adaptee.btnCtrlBuscarEnf_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  panGrafCom adaptee;
  FocusEvent event;

  focusAdapter(panGrafCom adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

////////////////////// Clases para listas

class CListaEnfEDO
    extends CListaValores {

  public CListaEnfEDO(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataCat(s);

  }

  public String getCodigo(Object o) {
    return ( ( (DataCat) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCat) o).getDes());
  }
}

class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  panGrafCom adaptee;

  txt_keyAdapter(panGrafCom adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}
