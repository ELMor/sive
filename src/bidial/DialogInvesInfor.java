package bidial;

import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import bidata.DataAlerta;
import bidata.DataBrote;
import bidata.DataBusqueda;
import bipcolectivo.PanelBIColectivo;
import bipcuadro.PanelBICuadro;
import bipdatos.PanelBIDatos;
//import bipotros.*;
import bipsuperior.PanelbiSuperior;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import capp.CTabPanel;
import infprotoedo.DataProtocolo;
import infprotoedo.PanelBrote;
import notutil.Comunicador;
import obj.CFechaSimple;
import sapp.StubSrvBD;

public class DialogInvesInfor
    extends CDialog {

  //Ya hubo msg en protocolo
  public boolean YaSalioMsgEnferSinProtocolo = false;
  ResourceBundle res;

  //Error en la construccion del dialogo
  public boolean bError = false;

  // contenedor de im�genes
  protected CCargadorImagen imgs = null;

  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

  final int modoESPERA = 2;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;

  final int modoINFORME = 600;
  final int modoINVESTIGACION = 601;

  final String strSERVLET_SELECT = "servlet/SrvBISelect";
  final int servletSELECT = 0;
  final String strSERVLET_INSERT = "servlet/SrvBIInsert";
  final int servletINSERT = 0;
  final String strSERVLET_MODIF = "servlet/SrvBIModif";
  final int servletCOMP_MOD = 3; // modificacion con comprobacion
  final int servletCOMP_BORR = 4; // borrado con comprobacion
  final int servletMODIFICACION = 5; // modificacion
  final int servletBORRADO = 6; //borrado

  protected StubSrvBD stubCliente = null;

  //valores Prim de check y fecha
  public String IT_PRIM = "";
  public String FC_PRIM = "";

  // Modo de salida del dialog (Aceptar=0, Cancelar=-1)
  //Aceptar == repetir query al volver
  //Cancelar == nada
  public int iOut = -1;

  //paneles
  public PanelBIDatos pDatos = null;
  public PanelBICuadro pCuadro = null;
  public PanelBIColectivo pColectivo = null;
  //public PanelBIOtros pOtros = null;
  public PanelbiSuperior panelSuperior = null;
  public PanelBrote pProtocolo = null;
  DataProtocolo dataProtocol;

  CApp capp;

  // modo de operaci�n del dialog
  private int modoOperacion = modoALTA;
  // modo de apertura del dialog
  public int modoVentana = modoINFORME;

  // controles
  XYLayout xYLayout = new XYLayout();
  XYLayout layoutBotones = new XYLayout();
  XYLayout layoutSuperior = new XYLayout();

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  Checkbox chkEstado = new Checkbox();
  Label lblFechaEstado = new Label();
  CFechaSimple txtFecEstado = new CFechaSimple("N");

  Panel panelBotones = new Panel();

  public CTabPanel tabPanel = new CTabPanel();

  // listeners
  DialogIactionAdapter actionAdapter = new DialogIactionAdapter(this);
  DialogIchkitemAdapter chkItemAdapter = new DialogIchkitemAdapter(this);

  //variables globales de entrada
  public String ano = "";
  public String nm = "";
  public Hashtable hashListas_completa = null;

  //modif
  public DataBrote datosBrote = null;
  public DataAlerta datosAlerta = null;

  // constructor
  public DialogInvesInfor(CApp a,
                          Hashtable hashListas,
                          int modo,
                          String cd_ano,
                          String nm_alerbro,
                          int InformeInvestig) {
    super(a);
    try {
      capp = a;
      res = ResourceBundle.getBundle("bidial.Res" + capp.getIdioma());
      modoOperacion = modo;
      modoVentana = InformeInvestig;
      ano = cd_ano;
      nm = nm_alerbro;
      stubCliente = new StubSrvBD();
      hashListas_completa = (Hashtable) hashListas.clone();

      //paneles
      panelSuperior = new PanelbiSuperior(this, modoOperacion,
                                          hashListas_completa);
      pDatos = new PanelBIDatos(this, modoOperacion, hashListas_completa);
      pCuadro = new PanelBICuadro(this, modoOperacion, null);
      pColectivo = new PanelBIColectivo(this, modoOperacion,
                                        hashListas_completa);
      //pOtros = new PanelBIOtros(this, modoOperacion, hashListas_completa);

      jbInit();
      pack();

      if ( (modoOperacion == modoMODIFICACION)
          || (modoOperacion == modoBAJA)
          || (modoOperacion == modoCONSULTA)) {

        Inicializar(modoESPERA);

        if (!RellenaDatos()) {
          Exception e = new Exception();
          throw e;
        }
        panelSuperior.rellena(datosBrote);
        pDatos.rellena(datosBrote);
        pCuadro.rellena(datosBrote);
        pColectivo.rellena(datosBrote);
        //pOtros.rellena(new DataBrote());
        if (pProtocolo != null) {
          borrarProtocolo(res.getString("msg1.Text"));

        }
        addProtocolo(res.getString("msg1.Text")); //esta en espera
      }

      modoOperacion = modo;
      RellenaCheck(datosAlerta);
      Inicializar(modo);

    }
    catch (Exception ex) {
      ex.printStackTrace();
      bError = true;
    }
  }

  void jbInit() throws Exception {

    // carga las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));

    // t�tulo
    if (modoVentana == modoINFORME) {
      this.setTitle(res.getString("this.Title"));
    }

    // layout - controles
    xYLayout.setHeight(450);
    xYLayout.setWidth(780);
    setSize(780, 450);
    setLayout(xYLayout);

    if ( (modoOperacion == modoBAJA)
        || (modoOperacion == modoCONSULTA)) {
      btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    }
    else {
      btnAceptar.setLabel(res.getString("btnAceptar.Label1"));

    }
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));

    chkEstado.setLabel(res.getString("chkEstado.Label"));
    lblFechaEstado.setText(res.getString("lblFechaEstado.Text"));
    txtFecEstado.setText("");

    layoutBotones.setHeight(44);
    layoutBotones.setWidth(755);

    panelBotones.setLayout(layoutBotones);
    panelBotones.add(btnAceptar, new XYConstraints(530, 10, 100, -1)); //520
    panelBotones.add(btnCancelar, new XYConstraints(645, 10, 100, -1)); //635
    panelBotones.add(chkEstado, new XYConstraints(10, 5, 158, 21));
    panelBotones.add(lblFechaEstado, new XYConstraints(172, 5, 39, 21));
    panelBotones.add(txtFecEstado, new XYConstraints(219, 5, 100, 21));

    add(panelSuperior, new XYConstraints(10, 3, 755, 84));
    add(tabPanel, new XYConstraints(10, 120 - 20, 755, 286)); //10, 106, 755, 300
    add(panelBotones, new XYConstraints(10, 409 - 20, 755, 44 + 30)); //10, 409, 755, 44

    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    chkEstado.addItemListener(chkItemAdapter);
    chkEstado.setName("estado");

    tabPanel.InsertarPanel(res.getString("msg1.Text"), pDatos);
    tabPanel.InsertarPanel(res.getString("tabPanel.Text2"), pColectivo);
    tabPanel.InsertarPanel(res.getString("msg2.Text"), pCuadro);
    //tabPanel.InsertarPanel("Protocolo", pOtros);
    tabPanel.VerPanel(res.getString("msg1.Text"));
  }

  public void RellenaCheck(DataAlerta datos) {

    boolean pintarfecha = false;

    if (modoOperacion == modoALTA) {
      IT_PRIM = "N";
      FC_PRIM = "";
      pintarfecha = false;
    } //fin ALTA

    else { //BAJA, consulta, modif
      //brote confirmado (4)
      if (datos.getCD_SITALERBRO().equals("4")) {
        IT_PRIM = "S";
        FC_PRIM = datos.getFC_ALERBRO();
        pintarfecha = true;
      }
      else {
        //brote confirmado (3)
        IT_PRIM = "N";
        //habra una fecha que no pintamos, pero que guardamos
        FC_PRIM = datos.getFC_ALERBRO();
        pintarfecha = false;
      }
    }
    //objetos
    if (IT_PRIM.equals("N")) {
      chkEstado.setState(false);
    }
    else {
      chkEstado.setState(true);
    }
    if (pintarfecha) {
      txtFecEstado.setText(datos.getFC_ALERBRO());

    }
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  void Inicializar() {

    switch (modoOperacion) {

      case modoESPERA:
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        chkEstado.setEnabled(false);
        if (panelSuperior != null) {
          panelSuperior.Inicializar(modoOperacion);
        }
        if (pDatos != null) {
          pDatos.Inicializar(modoOperacion);
        }
        if (pCuadro != null) {
          pCuadro.Inicializar(modoOperacion);
        }
        if (pColectivo != null) {
          pColectivo.Inicializar(modoOperacion);
          /*if (pOtros != null)
            pOtros.Inicializar(modoOperacion);*/
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.deshabilitarCampos(Cursor.WAIT_CURSOR);

        }
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        chkEstado.setEnabled(true);

        if (panelSuperior != null) {
          panelSuperior.Inicializar(modoOperacion);
        }
        if (pDatos != null) {
          pDatos.Inicializar(modoOperacion);
        }
        if (pCuadro != null) {
          pCuadro.Inicializar(modoOperacion);
        }
        if (pColectivo != null) {
          pColectivo.Inicializar(modoOperacion);
          /*if (pOtros != null)
            pOtros.Inicializar(modoOperacion);*/
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.habilitarCampos();

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICACION:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        chkEstado.setEnabled(true);

        if (panelSuperior != null) {
          panelSuperior.Inicializar(modoOperacion);
        }
        if (pDatos != null) {
          pDatos.Inicializar(modoOperacion);
        }
        if (pCuadro != null) {
          pCuadro.Inicializar(modoOperacion);
        }
        if (pColectivo != null) {
          pColectivo.Inicializar(modoOperacion);
          /*if (pOtros != null)
            pOtros.Inicializar(modoOperacion);*/
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.habilitarCampos();

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
      case modoCONSULTA:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        chkEstado.setEnabled(false);

        if (panelSuperior != null) {
          panelSuperior.Inicializar(modoOperacion);
        }
        if (pDatos != null) {
          pDatos.Inicializar(modoOperacion);
        }
        if (pCuadro != null) {
          pCuadro.Inicializar(modoOperacion);
        }
        if (pColectivo != null) {
          pColectivo.Inicializar(modoOperacion);
          /*if (pOtros != null)
            pOtros.Inicializar(modoOperacion);*/
        }
        if (pProtocolo != null) {
          pProtocolo.pnl.deshabilitarCampos(Cursor.DEFAULT_CURSOR);

        }

        if (modoOperacion == modoCONSULTA) {
          btnAceptar.setEnabled(false);

        }

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }

    //fechas
    txtFecEstado.setEditable(false);
    txtFecEstado.setEnabled(false);

    //botones
    if ( (modoOperacion == modoBAJA)
        || (modoOperacion == modoCONSULTA)) {
      btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    }
    else {
      btnAceptar.setLabel(res.getString("btnAceptar.Label1"));

    }
    btnCancelar.setLabel(res.getString("btnCancelar.Label1"));

    doLayout();
  }

// Rellena los datos de las solapas
  boolean RellenaDatos() {
    try {

      CLista result = new CLista();
      CLista parametros = new CLista();

      // par�metros que se le pasan a DataAlerta: CD_ANO, NM_ALERBRO
      DataBrote data = new DataBrote();
      data.insert("CD_ANO", ano);
      data.insert("NM_ALERBRO", nm);
      parametros.addElement(data);

      //sin tramero NOTA
      result = Comunicador.Communicate(this.getCApp(),
                                       stubCliente,
                                       servletSELECT,
                                       strSERVLET_SELECT,
                                       parametros);

      // comprueba que hay datos
      if (result.size() == 0) {
        ShowWarning(res.getString("msg3.Text"));
        //dispose(); //no funciona, continua
        return false;
      }
      else {
        //tengo los datos de todos los paneles
        datosBrote = (DataBrote) result.elementAt(1);
        datosAlerta = (DataAlerta) result.elementAt(0);
      }
    }
    catch (Exception e) {
      ShowWarning(res.getString("msg4.Text"));
      //dispose(); //no funciona, continua
      return false;
    }
    return true;
  } //fin de rellenadatos

  public void chkEstado_itemStateChanged(ItemEvent e) {

    //vale para alta y para modif
    //e.getStateChange()-> SI 2: DESELECCIONADO, 1:SELECCIONADO
    int AcaboDe = 0;
    AcaboDe = e.getStateChange();

    if (AcaboDe == 1) { //seleccionado
      chkEstado.setState(true);
    }
    else {
      chkEstado.setState(false);

    }
    if (IT_PRIM.equals("S")) {
      if (chkEstado.getState()) {
        txtFecEstado.setText(FC_PRIM); //la guardada
      }
      else {
        txtFecEstado.setText("");
      }
    }
    else if (IT_PRIM.equals("N")) {
      if (chkEstado.getState()) {
        txtFecEstado.setText(this.app.getFC_ACTUAL());
      }
      else {
        txtFecEstado.setText("");
      }
    }
  }

  void btnAceptar_actionPerformed() { //alta, modif, baja

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();

    if (modo == modoBAJA) {
      borrarBrote();
      return;
    }
    else if (modo == modoMODIFICACION) {

      if (!panelSuperior.validarDatos(modoMODIFICACION)) {
        Inicializar(modo);
        return;
      }

      if (!pDatos.validarDatos()) {
        tabPanel.VerPanel(res.getString("tabPanel.Text1"));
        Inicializar(modo);
        return;
      }
      if (!pColectivo.validarDatos()) {
        tabPanel.VerPanel(res.getString("tabPanel.Text2"));
        Inicializar(modo);
        return;
      }
      if (!pCuadro.validarDatos()) {
        tabPanel.VerPanel(res.getString("tabPanel.Text3"));
        Inicializar(modo);
        return;
      }
      modificarBrote();
      return;
    }
    else if (modo == modoALTA) {

      if (!panelSuperior.validarDatos(modoALTA)) {
        Inicializar(modo);
        return;
      }
      if (!pDatos.validarDatos()) {
        tabPanel.VerPanel(res.getString("tabPanel.Text1"));
        Inicializar(modo);
        return;
      }
      if (!pColectivo.validarDatos()) {
        tabPanel.VerPanel(res.getString("tabPanel.Text2"));
        Inicializar(modo);
        return;
      }
      if (!pCuadro.validarDatos()) {
        tabPanel.VerPanel(res.getString("tabPanel.Text3"));
        Inicializar(modo);
        return;
      }
      insertarBrote();
      return;
    } //fin alta

  } //fin funcion ACEPTAR *****************************************

  private void borrarBrote() {
    CLista param = null;
    CLista resul = null;
    CMessage msgBox = null;

    //entra en ESPERA

    //recoger los datos de los paneles  //conserva cd_ope, fc_ultact
    //no tengo que recoger los datos de los paneles porque en
    //modo borrar no se puede modificar nada
    //**********************************

     //TRY GENERAL
    try {
      param = new CLista();

      datosAlerta.insert("CD_SITALERBRO", "1");
      param.addElement(datosAlerta);
      param.addElement(datosBrote);

      //va bien: listaSalida = (true) o                //bien
      //         listaSalida = (true, mensaje pregunta) //modif
      //va mal: listaSalida = null y exc
      //nota: si YA ha sido borrado, devuelve TRUE . No constaras como cd_ope
      resul = Comunicador.Communicate(this.getCApp(),
                                      stubCliente,
                                      servletCOMP_BORR,
                                      strSERVLET_MODIF,
                                      param);

//ha pasado algo ie  MODIF por algun otro
      if (resul.size() == 2) {

        String pre = (String) resul.elementAt(1);
        msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                              pre);
        msgBox.show();
        if (msgBox.getResponse()) { //SI
          setCursor(new Cursor(Cursor.WAIT_CURSOR));
          msgBox = null;

          //va bien: listaSalida.size()=0
          //va mal: listaSalida = null y exc
          resul = Comunicador.Communicate(this.getCApp(),
                                          stubCliente,
                                          servletBORRADO,
                                          strSERVLET_MODIF,
                                          param);

          if (resul.size() == 0) {
            //ShowWarning(res.getString("msg5.Text"));
            iOut = 0; //repetir query con datos buenos de db
            dispose();
            return;
            //NO INICIALIZAMOS. NO HACE FALTA!!!!

          }

        }
        else { //NO quiere sobreescribir
          iOut = 0; //Como Aceptar repetir query con datos buenos de db
          this.btnCancelar.requestFocus();
          modoOperacion = modoBAJA;
          Inicializar();
          return;
        }
      }

//SI HA IDO BIEN
      else if (resul.size() == 1) {
        // todo ha ido bien y lo que trae es un true
        //ShowWarning(res.getString("msg5.Text"));
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        iOut = 0;
        dispose();
        return;
        //NO INICIALIZAMOS. NO HACE FALTA!!!!
      }
      // error en el proceso
    }
    catch (Exception e) {
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR,
                            res.getString("msg6.Text"));
      msgBox.show();
      msgBox = null;
      iOut = -1; //Como cancelar
      this.btnCancelar.requestFocus();
      modoOperacion = modoBAJA;
      Inicializar();
    }

  }

  public void modificarBrote() {

    CLista param = null;
    CLista resul = null;
    CMessage msgBox = null;

    //entra en ESPERA

    //recoger los datos de los paneles  //conserva cd_ope, fc_ultact
    DataBrote datosPantalla = new DataBrote();
    datosPantalla.insert("CD_OPE", datosBrote.getCD_OPE());
    datosPantalla.insert("FC_ULTACT", datosBrote.getFC_ULTACT());

    panelSuperior.recogerDatos(datosPantalla);
    pDatos.recogerDatos(datosPantalla);
    pColectivo.recogerDatos(datosPantalla);
    pCuadro.recogerDatos(datosPantalla);
    DataAlerta datosPantAler = new DataAlerta();
    datosPantAler = recogerDatos(datosPantAler, modoMODIFICACION); //dlg
    //**********************************

     //TRY GENERAL
    try {
      param = new CLista();
      param.addElement(datosPantAler);
      param.addElement(datosPantalla);

      //va bien: listaSalida = (true, DataAlerta, DataBrote) o   - catch
      //         listaSalida = (true, mensaje pregunta) - try
      //va mal: listaSalida = null y exc
      resul = Comunicador.Communicate(this.getCApp(),
                                      stubCliente,
                                      servletCOMP_MOD,
                                      strSERVLET_MODIF,
                                      param);

      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      try {
        //pregunta si se quiere sobreescribir
        String pre = (String) resul.elementAt(1);
        if (pre.indexOf("borrado") != -1) {
          //el brote hab�a sido modificado
          msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                                pre);
          msgBox.show();

          if (msgBox.getResponse()) { //SI
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            msgBox = null;
            resul = Comunicador.Communicate(this.getCApp(),
                                            stubCliente,
                                            servletMODIFICACION,
                                            strSERVLET_MODIF,
                                            param);
            //va bien: listaSalida (DataAlerta, DataBrote)
            //va mal: listaSalida = null y exc

            if (resul.size() == 2) {
              //ShowWarning(res.getString("msg7.Text"));
              datosAlerta = (DataAlerta) resul.elementAt(0); // no sirve para nada!
              datosBrote = (DataBrote) resul.elementAt(1);
              modoOperacion = modoMODIFICACION;
              Parche_datosAlerta(datosAlerta);
              RellenaCheck(datosAlerta); //actualiza it_prim, fc_prim
              this.btnCancelar.requestFocus();
              iOut = 0; //aceptar, ptt se hara de nuevo la query
              modoOperacion = modoMODIFICACION;
              //Inicializar(); continuamos en espera
            }
          }
          else { //NO quiere sobreescribir
            iOut = 0; //Como Aceptar repetir query con datos buenos de db
            this.btnCancelar.requestFocus();
            modoOperacion = modoMODIFICACION;
            Inicializar();
            return;
          }
        }
        else { //el brote habia sido borrado
          ShowWarning(res.getString("msg8.Text"));
          iOut = -1;
          this.btnCancelar.requestFocus();
          modoOperacion = modoMODIFICACION;
          Inicializar();
          return;
        }
      }
      catch (ClassCastException e) {
        // todo ha ido bien y lo que trae es el brote modificado
        //ShowWarning(res.getString("msg7.Text"));
        datosAlerta = (DataAlerta) resul.elementAt(1);
        datosBrote = (DataBrote) resul.elementAt(2);
        modoOperacion = modoMODIFICACION;
        Parche_datosAlerta(datosAlerta);
        RellenaCheck(datosAlerta); //actualiza it_prim, fc_prim
        iOut = 0; //Como Aceptar repetir query con datos buenos de db
        this.btnCancelar.requestFocus();
        modoOperacion = modoMODIFICACION;
        //Inicializar(); continuamos en espera
      }
      // error en el proceso
    }
    catch (Exception e) {
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR,
                            res.getString("msg9.Text"));
      msgBox.show();
      msgBox = null;
      iOut = -1; //Como cancelar
      this.btnCancelar.requestFocus();
      modoOperacion = modoMODIFICACION;
      Inicializar();
      return;
    }
    //continuamos con el protocolo
    if (pProtocolo != null) {
      //GRABAR EL PROTOCOLO, porque puede haber metido algo!!!!!
      boolean b = pProtocolo.pnl.ValidarObligatorios();
      if (b) {
        pProtocolo.pnl.Insertar(); //Inserta datos en la tabla respedo
        //dispose(); //Enrique quiere que se quede abto
        // estaba en la pesta�a de protocolo, continuara alli
      }
    }
    Inicializar();
    //-----------------------------

  }

//completa el datosAlerta
  private void Parche_datosAlerta(DataAlerta dat) {
    //para hacer el insert a lo que falta
    //3->3
    if (IT_PRIM.equals("N") && !chkEstado.getState()) {
      dat.insert("CD_SITALERBRO", "3");
      dat.insert("FC_ALERBRO", FC_PRIM);
    }
    //4->4
    if (IT_PRIM.equals("S") && chkEstado.getState()) {
      dat.insert("CD_SITALERBRO", "4");
      dat.insert("FC_ALERBRO", FC_PRIM);
    }

    datosAlerta = (DataAlerta) dat.clone();
  }

  private DataAlerta recogerDatos(DataAlerta result, int modo) {
    //alta
    if (modo == modoALTA) {
      if (!chkEstado.getState()) {
        result.insert("CD_SITALERBRO", "3");
        result.insert("FC_ALERBRO", capp.getFC_ACTUAL());
      }
      else {
        result.insert("CD_SITALERBRO", "4");
        result.insert("FC_ALERBRO", capp.getFC_ACTUAL());
      }
    }
    //modif
    else {
      //3-> 3
      if (IT_PRIM.equals("N") && !chkEstado.getState()) {
        //no cambia, no envio nada  (cris no lo actualiza)
        result.insert("CD_SITALERBRO", null);
        result.insert("FC_ALERBRO", null);
      }
      //3-> 4
      if (IT_PRIM.equals("N") && chkEstado.getState()) {
        result.insert("CD_SITALERBRO", "4");
        result.insert("FC_ALERBRO", txtFecEstado.getText().trim());
      }
      //4-> 4
      if (IT_PRIM.equals("S") && chkEstado.getState()) {
        //no cambia, no envio nada  (cris no lo actualiza)
        result.insert("CD_SITALERBRO", null);
        result.insert("FC_ALERBRO", null);
      }
      //4-> 3
      if (IT_PRIM.equals("S") && !chkEstado.getState()) {
        result.insert("CD_SITALERBRO", "3");
        result.insert("FC_ALERBRO", capp.getFC_ACTUAL());
      }
    }

    return result;
  }

  private void insertarBrote() {
    //entra en ESPERA

    //recoger los datos de los paneles
    DataBrote datosPantBrote = new DataBrote();
    datosPantBrote.insert("CD_OPE", this.app.getLogin());

    datosPantBrote = panelSuperior.recogerDatos(datosPantBrote);
    datosPantBrote = pDatos.recogerDatos(datosPantBrote);
    datosPantBrote = pColectivo.recogerDatos(datosPantBrote);
    datosPantBrote = pCuadro.recogerDatos(datosPantBrote);
    DataAlerta datosPantAler = new DataAlerta();
    datosPantAler = recogerDatos(datosPantAler, modoALTA); //dlg
    datosPantAler.insert( (String) "CD_ANO", datosPantBrote.getCD_ANO().trim());
    datosPantAler.insert( (String) "NM_ALERBRO",
                         datosPantBrote.getNM_ALERBRO().trim());
    //llamada al servlet con el dataAlerta

    CLista listaSalida = new CLista();
    CLista parametros = new CLista();
    parametros.addElement(datosPantAler);
    parametros.addElement(datosPantBrote);

    try {

      listaSalida = Comunicador.Communicate(this.app,
                                            stubCliente,
                                            servletINSERT,
                                            strSERVLET_INSERT,
                                            parametros);

      if (listaSalida.size() == 2) {
        //ShowWarning(res.getString("msg10.Text"));
        /*listaAlerta = new CLista();
             listaAlerta.addElement((DataAlerta)listaSalida.firstElement());
             listaAlerta.addElement((DataBrote)listaSalida.elementAt(1));*/
        datosBrote = (DataBrote) listaSalida.elementAt(1);
        //Cris - DataAlerta datos = (DataAlerta)listaSalida.elementAt(0);
        datosAlerta = (DataAlerta) listaSalida.elementAt(0);
        listaSalida = null;

        /*datos = ( DataAlerta ) listaAlerta.firstElement();
                   panelSuperior.txtCod.setText(datos.getNM_ALERBRO());*/
        Parche_datosAlerta(datosAlerta);
        RellenaCheck(datosAlerta); //actualiza it_prim, fc_prim
        this.btnCancelar.requestFocus();
        iOut = 0;
        modoOperacion = modoMODIFICACION; //pero no inicializamos
        //seguimos en espera
      }

    }
    catch (Exception e) { //acceso a base de datos mal
      iOut = -1;
      e.printStackTrace();
      ShowWarning(res.getString("msg11.Text"));
      modoOperacion = modoALTA;
      this.btnCancelar.requestFocus();
      Inicializar();
      return;
    } //fin del catch

    //continuamos abriendo el PROTOCOLO
    addProtocolo(res.getString("msg1.Text"));
    Inicializar();

  } //fin de insertar_brote

  void btnCancelar_actionPerformed() {
    dispose();
  }

//PROTOCOLO
  public void borrarProtocolo(String sPantallaMostrar) {

    tabPanel.BorrarSolapa(res.getString("tabPanel.Text4"));
    tabPanel.VerPanel(sPantallaMostrar);

  }

//SOLAPA PROTOCOLO (solo se abre en modif)
  public void addProtocolo(String sPantallaMostrar) {

    DataProtocolo data = new DataProtocolo();
    data.sCodigo = "B" + panelSuperior.getGrupo();

    data.sDescripcion = panelSuperior.getDSGrupo();
    data.sNivel1 = pColectivo.panNivExp.getCDNivel1().trim();
    data.sNivel2 = pColectivo.panNivExp.getCDNivel2().trim();
    data.NumCaso = panelSuperior.getCD_ANO() + panelSuperior.getNM_ALERBRO();
    data.sCa = app.getCA();

    // borramos la solapa por si acaso exist�a; as� actualizamos
    tabPanel.BorrarSolapa(res.getString("tabPanel.Text4"));
    pProtocolo = null;
    pProtocolo = new PanelBrote(capp, data, this); //MIO!!!!!!!

    if (!pProtocolo.pnl.getNoSeEncontraronDatos()) {
      YaSalioMsgEnferSinProtocolo = false;
      tabPanel.InsertarPanel(res.getString("tabPanel.Text4"), pProtocolo);
      pProtocolo.doLayout();
      tabPanel.VerPanel(sPantallaMostrar);

    }
    else {
      if (!YaSalioMsgEnferSinProtocolo) {
        ShowWarning(res.getString("msg12.Text"));

      }
      YaSalioMsgEnferSinProtocolo = true;
      pProtocolo = null;
    }

  } //ADDPROTOCOLO***********************************************

// retorna una clista con un elemento de tipo DataBusqueda
// para  a�adir  (solo hace falta que lleve la clave )
  public Object getComponente() {
    Object o = null;

    if (iOut != -1) {
      CLista lista = new CLista();

      //sacar databusqueda a partir de listaBrote
      DataBusqueda datosDevolver = new DataBusqueda(datosBrote.getCD_ANO(),
          datosBrote.getNM_ALERBRO(), "", "",
          "", "", "", "",
          "", "", "",
          "", "", "", "", "", "",
          "", "", -1,
          null, null);

      lista.addElement(datosDevolver);
      o = lista.elementAt(0);

    }

    return o;
  }

  public void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

} //FIN CLASE

class DialogIactionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialogInvesInfor adaptee;
  ActionEvent evt;

  DialogIactionAdapter(DialogInvesInfor adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if (evt.getActionCommand().equals("aceptar")) {
      adaptee.btnAceptar_actionPerformed();
    }
    else if (evt.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed();
    }
  }
} //fin class

// listener
class DialogIchkitemAdapter
    implements java.awt.event.ItemListener {
  DialogInvesInfor adaptee;
  DialogIchkitemAdapter(DialogInvesInfor adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkEstado_itemStateChanged(e);
  }
}
