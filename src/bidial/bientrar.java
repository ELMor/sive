package bidial;

import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.Color;

import bidata.DataAlerta;
import bidata.DataCDDS;
import capp.CApp;
import capp.CLista;

public class bientrar
    extends CApp {

  CApp a;
  ResourceBundle res = ResourceBundle.getBundle("bidial.Res");
  final int modoINFORME = 600;
  final int modoINVESTIGACION = 601;

  final int modoESPERA = 2;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;

  public void init() {
    super.init();
    this.setBackground(Color.lightGray);
  }

  public void start() {

    setTitulo(res.getString("msg13.Text"));
    a = (CApp)this;

    VerEntradaEDO();

  }

  public void VerEntradaEDO() {

    Hashtable hs = new Hashtable();
    /*
       protected  CLista listaAlerta =  new CLista();// datos de los paneles
       protected  CLista listaGrupo =  new CLista();
       protected  CLista listaTBrotes =  new CLista();
       protected  CLista listaTColectivo =  new CLista();
       protected  CLista listaTNotificador =  new CLista();
       protected  CLista listaPaises =  new CLista();
       protected  CLista listaCA =  new CLista();
       protected  CLista listaMECTRANS =  new CLista();*/

    CLista lis = new CLista();
    DataAlerta datos = new DataAlerta();
    datos.insert("CD_ANO", "1999");
    datos.insert("NM_ALERBRO", "7");
    datos.insert("FC_FECHAHORA_F", "15/09/1999");
    datos.insert("FC_FECHAHORA_H", "00:00");
    datos.insert("CD_NIVEL_1", "1");
    datos.insert("DS_NIVEL_1", "Area Centro");

    lis.addElement(datos);
    hs.put("ALERTA", lis);

    lis = new CLista();
    DataCDDS dat = new DataCDDS("0", "Vacunables");
    lis.addElement(dat);
    dat = new DataCDDS("1", "Infecciosos");
    lis.addElement(dat);
    dat = new DataCDDS("2", "T�xico-Alimentarios");
    lis.addElement(dat);
    hs.put("GRUPOS", lis);

    lis = new CLista();
    dat = new DataCDDS("1", "1", "ds_tbrote1");
    lis.addElement(dat);
    dat = new DataCDDS("1", "2", "tipo2");
    lis.addElement(dat);
    hs.put("TBROTE", lis);

    lis = new CLista();
    dat = new DataCDDS("1", "ds_tipocol1");
    lis.addElement(dat);
    dat = new DataCDDS("2", "ds_tipocol2");
    lis.addElement(dat);
    hs.put("TCOLECTIVO", lis);

    lis = new CLista();
    dat = new DataCDDS("1", "Trabajo");
    lis.addElement(dat);
    dat = new DataCDDS("2", "Parque Recreativo");
    lis.addElement(dat);
    hs.put("TNOTIFICADOR", lis);

    lis = new CLista();
    hs.put("PAISES", lis);
    hs.put("CA", lis);
    lis = new CLista();
    dat = new DataCDDS("1", "mec1");
    lis.addElement(dat);
    hs.put("MECTRANS", lis);

    DialogInvesInfor dlg = new DialogInvesInfor(a, hs, modoALTA, "1999", "7",
                                                modoINFORME);

    dlg.show();
    /*data = (DataListaEDONum) dlg.getComponente();
         if (data != null) {
         } */
    dlg = null;

    return;
  }

} // END CLASS Applet
