package infedopac;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import enfermo.DataEnfermo;
import enfermo.DialBusEnfermo;
import notutil.Comunicador;
import sapp.StubSrvBD;

public class Pan_EDOPac
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  //rutas imagenes
  final String imgLUPA = "images/Magnify.gif";
  final String imgLIMPIAR = "images/vaciar.gif";
  final String imgGENERAR = "images/grafico.gif";

  protected StubSrvBD stubCliente = new StubSrvBD();

  //public ParamC11_12 paramC2;

  // modos de operaci�n del servlet SrvEnfermo
  final int modoDATOSENFERMO = 10;
  // servlet
  final String strSERVLET_ENFERMO = "servlet/SrvEnfermo";

  public String sStringBk = "";
  CLista data = new CLista();
  protected CLista listaEnfermo = null;

  protected int modoOperacion = modoNORMAL;

  protected String fechaNac = "";
  protected DataInfEDOPac paramC2 = null;

  protected Pan_InfEDOPac informe;
  // datos del enfermo seleccionado
  DataEnfermo elEnfermo = new DataEnfermo("CD_ENFERMO");
  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETMun = "servlet/SrvMun";
  final String strSERVLETProv = "servlet/SrvCat2";
  final String strSERVLETEquipo = "servlet/SrvEqNot";
  final String strSERVLETCentro = "servlet/SrvCN";
  final String strSERVLETEnf = "servlet/SrvEnfVigi";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  XYLayout xYLayout = new XYLayout();

  Label lblCodEnf = new Label();
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();
  ButtonControl btnEnfermo = new ButtonControl();

  actionListener btnActionListener = new actionListener(this);
  CheckboxGroup checkboxAgrupar = new CheckboxGroup();
  TextField txtEnfermo = new CCampoCodigo();
  Label label1 = new Label();
  TextField txtNombre = new TextField();
  Label label2 = new Label();
  TextField txtFechaNac = new TextField();

  public Pan_EDOPac(CApp a) {
    try {

      setApp(a);
      res = ResourceBundle.getBundle("infedopac.Res" + a.getIdioma());
      informe = new Pan_InfEDOPac(a);
      //panelFecha = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      jbInit();
      informe.setEnabled(false);
      btnEnfermo.setFocusAware(false);
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(301);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));
    txtEnfermo.setBackground(new Color(255, 255, 150));
    txtEnfermo.addKeyListener(new Pan_EDOPac_txtEnfermo_keyAdapter(this));
    txtEnfermo.addFocusListener(new Pan_EDOPac_txtEnfermo_focusAdapter(this));
    btnEnfermo.setLabel("");
    label1.setText(res.getString("label1.Text"));
    txtNombre.setEditable(false);
    label2.setText(res.getString("label2.Text"));
    txtFechaNac.setEditable(false);

    // gestores de eventos

    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    //ARG
//    btnEnfermo.addActionListener(btnActionListener);

    //btnCtrlBuscarEnfer.setLabel("btnCtrlBuscarPro1");
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    //ARG
//    btnLimpiar.setActionCommand("enfermo");

    lblCodEnf.setText(res.getString("lblFecha.Text"));

    xYLayout.setWidth(596);

    this.setLayout(xYLayout);

    this.add(lblCodEnf, new XYConstraints(26, 42, 100, -1));
    this.add(btnLimpiar, new XYConstraints(389, 162, -1, -1));
    this.add(btnInforme, new XYConstraints(475, 162, -1, -1));
    this.add(txtEnfermo, new XYConstraints(149, 42, 89, -1));
    this.add(btnEnfermo, new XYConstraints(246, 42, -1, -1));
    this.add(label1, new XYConstraints(26, 77, 117, -1));
    this.add(txtNombre, new XYConstraints(149, 77, 271, -1));
    this.add(label2, new XYConstraints(26, 112, 116, -1));
    this.add(txtFechaNac, new XYConstraints(149, 112, 134, -1));

    btnLimpiar.setImageURL(new URL(app.getCodeBase(), imgLIMPIAR));
    btnInforme.setImageURL(new URL(app.getCodeBase(), imgGENERAR));
    btnEnfermo.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    //ARG
    btnEnfermo.addActionListener(new Pan_EDOPac_btnEnfermo_actionAdapter(this));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas

    if ( (this.txtEnfermo.getText().length() == 0)) {
      bDatosCompletos = false;
    }
    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:

        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);
        btnEnfermo.setEnabled(true);
        txtEnfermo.setEnabled(true);
        txtFechaNac.setEnabled(true);
        txtNombre.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:

        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        btnEnfermo.setEnabled(false);
        txtEnfermo.setEnabled(false);
        txtFechaNac.setEnabled(false);
        txtNombre.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //Busca los niveles1 (area de salud)
  void btnCtrlbuscarArea_actionPerformed(ActionEvent evt) {
    /*DataNivel1 data = null;
         CMessage mensaje = null;
         modoOperacion=modoESPERA;
         Inicializar();
         try{
      CListaNivel1 lista = new CListaNivel1(app,
                                          "Selecci�n de "+this.app.getNivel1(),
                                          stubCliente,
                                          strSERVLETNivel1,
                                          servletOBTENER_X_CODIGO,
                                          servletOBTENER_X_DESCRIPCION,
                                          servletSELECCION_X_CODIGO,
                                          servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();
         }catch (Exception excepc){
      excepc.printStackTrace();
      mensaje=new CMessage(this.app,CMessage.msgERROR,excepc.getMessage());
      mensaje.show();
         }
         if (data != null) {
      txtCodArea.removeTextListener(txtTextAdapter);
      txtCodArea.setText(data.getCod());
      txtDesArea.setText(data.getDes());
      txtCodArea.addTextListener(txtTextAdapter);
         }
         modoOperacion = modoNORMAL;
         Inicializar();
     */
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {

    txtEnfermo.setText("");
    txtFechaNac.setText("");
    txtNombre.setText("");

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    CMessage msgBox;

    boolean eq = false;
    boolean cen = false;
    boolean tasas = false;

    if (isDataValid()) {
      if (!txtEnfermo.getText().equals("")) {
        // habilita el panel
        modoOperacion = modoESPERA;
        Inicializar();
        informe.setEnabled(true);
        /*informe.paramC1 = new DataInfEDOPac(paramC2.DS_ENF,paramC2.DS_SEMEPI,
          paramC2.DS_CENTRO,paramC2.EDAD,paramC2.DS_ZBS,paramC2.CLASIF);
//E        //#// System_out.println(paramC2.EDAD);
                 informe.paramC1.CD_N1 = paramC2.CD_N1;
                 informe.paramC1.CD_N2 = paramC2.CD_N2;*/
        informe.paramC1 = paramC2;
        if (informe.GenerarInforme()) { // JRM2 Aqu� genera el informe
          informe.show();
        }
        modoOperacion = modoNORMAL;
        Inicializar();
      }
    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg3.Text"));
      msgBox.show();
      msgBox = null;
    }

  }

  void btnEnfermo_actionPerformed(ActionEvent e) {
    CLista listaEnfermo = null;
    DataEnfermo datos = null;
    String cadena = "";

    modoOperacion = modoESPERA;
    Inicializar();

    DialBusEnfermo dial = new DialBusEnfermo(this.app);

    dial.show();
    modoOperacion = modoNORMAL;
    Inicializar();
    listaEnfermo = (CLista) dial.getListaDatosEnfermo();
    dial = null;

    //ARG: la comprobaci�n debe ser esta y no if (listaEnfermo.size() > 0)
    if (listaEnfermo != null) {
      datos = (DataEnfermo) listaEnfermo.firstElement();
      txtEnfermo.setText(datos.get("CD_ENFERMO").toString());
      sStringBk = datos.get("CD_ENFERMO").toString();
      cadena = " ";
      if (datos.get("DS_APE1") != null) {
        cadena += datos.get("DS_APE1").toString() + " ";
      }
      if (datos.get("DS_APE2") != null) {
        cadena += datos.get("DS_APE2").toString();
        //#// System_out.println("f");
      }
      if (datos.get("DS_NOMBRE") != null) {
        cadena += ", " + datos.get("DS_NOMBRE").toString();
        //#// System_out.println("g");
      }
      txtNombre.setText(cadena);
      //#// System_out.println("h");
      cadena = "";
      fechaNac = "";
      //#// System_out.println("i");
      if (datos.get("FC_NAC") != null) {

        // cadena  = Fechas.date2String( (java.util.Date) datos.get("FC_NAC") );
        cadena = (String) datos.get("FC_NAC");

        fechaNac = cadena;
        //#// System_out.println("l");
        if (datos.get("IT_CALC") != null) {
          //#// System_out.println("m");
          if (datos.get("IT_CALC").toString().compareTo("S") == 0) {
            cadena += "(C)";

          }
        }
      }
      txtFechaNac.setText(cadena);
      paramC2 = new DataInfEDOPac(txtNombre.getText(), txtEnfermo.getText(), "",
                                  fechaNac, "", "");
      if (datos.get("CD_NIVEL_1") != null) {
        paramC2.CD_N1 = datos.get("CD_NIVEL_1").toString();
      }
      if (datos.get("CD_NIVEL_2") != null) {
        paramC2.CD_N2 = datos.get("CD_NIVEL_2").toString();
      }
    }
  }

  void txtEnfermo_focusLost(FocusEvent e) {
    String sStringDespues = txtEnfermo.getText();
    String cadena = "";
    CMessage msgBox;

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();
//E    //#// System_out.println("A");
//JRM2 Quita por incidencia 159  if (sStringBk.equals(sStringDespues))
//JRM2 Quita por incidencia 159     return;
//E    //#// System_out.println("B");
    if (txtEnfermo.getText().equals("")) {
//E      //#// System_out.println("C");
      txtEnfermo.setText(sStringBk);
//E      //#// System_out.println("D");
      modoOperacion = modo;
//E      //#// System_out.println("E");
      Inicializar();
//E      //#// System_out.println("F");
      return;
    }
//E    //#// System_out.println("G");
    //COMPROBAR NUMERICO ****
    boolean bEnf = true;
    try {
//E      //#// System_out.println("H");
      Integer iCodEnfermo = new Integer(txtEnfermo.getText());
//E      //#// System_out.println("I");
      if (iCodEnfermo.intValue() < 0) {
        bEnf = false;
//E      //#// System_out.println("J");
      }
      if (iCodEnfermo.intValue() > 999999) {
        bEnf = false;
//E      //#// System_out.println("K");
      }
    }
    catch (Exception erf) {
      bEnf = false;
    }

    if (!bEnf) {
      msgBox = new CMessage(this.app,
                            CMessage.msgAVISO, res.getString("msg4.Text"));
      msgBox.show();
      msgBox = null;
      txtEnfermo.selectAll();
      txtEnfermo.setText(sStringBk);
      //Reset_Enfermo();
      modoOperacion = modo;
      Inicializar();
      return;
    }

    data = new CLista();

    DataEnfermo hash = new DataEnfermo("CD_ENFERMO");

    hash.put("CD_ENFERMO", txtEnfermo.getText());

    data.addElement(hash);

    try {

      listaEnfermo = Comunicador.Communicate(this.getApp(),
                                             stubCliente,
                                             modoDATOSENFERMO,
                                             strSERVLET_ENFERMO,
                                             data);

      if (listaEnfermo != null) {

        if (listaEnfermo.size() == 0) {
          CMessage elMensaje = new CMessage(this.app, CMessage.msgERROR,
                                            res.getString("msg5.Text"));
          elMensaje.show();
          txtEnfermo.setText(sStringBk);

        }
        else {
          elEnfermo = (DataEnfermo) listaEnfermo.firstElement();
          cadena = elEnfermo.getDS_APE1();
          if (elEnfermo.get("DS_APE2") != null) {
            cadena += " " + elEnfermo.get("DS_APE2").toString();
          }
          if (elEnfermo.get("DS_NOMBRE") != null) {
            cadena += ", " + elEnfermo.get("DS_NOMBRE").toString();
          }
          txtNombre.setText(cadena);
          cadena = "";
          fechaNac = "";
          if (elEnfermo.get("FC_NAC") != null) {
            txtFechaNac.setText(elEnfermo.getFechaNacimiento());
            cadena = elEnfermo.getFechaNacimiento();
            fechaNac = cadena;
            if (elEnfermo.get("IT_CALC") != null) {
              if (elEnfermo.get("IT_CALC").toString().compareTo("S") == 0) {
                cadena += "(C)";
                //E //#// System_out.println("A5");
              }
            }
          }
          //E //#// System_out.println("A6");
          txtEnfermo.setText(elEnfermo.get("CD_ENFERMO").toString());
          txtFechaNac.setText(cadena);
          //E //#// System_out.println("A7");
          sStringBk = txtEnfermo.getText();
          //E //#// System_out.println("A8");

          paramC2 = new DataInfEDOPac(txtNombre.getText(), txtEnfermo.getText(),
                                      "",
                                      fechaNac, "", "");
          //E //#// System_out.println("A9");
          if (elEnfermo.get("CD_NIVEL_1") != null) {
            paramC2.CD_N1 = elEnfermo.get("CD_NIVEL_1").toString();
            //E //#// System_out.println("A10");
          }
          if (elEnfermo.get("CD_NIVEL_2") != null) {
            paramC2.CD_N2 = elEnfermo.get("CD_NIVEL_2").toString();
            //E //#// System_out.println("A11");
          }
        }
      }
    }
    catch (Exception erf) {
      erf.printStackTrace();
      txtEnfermo.setText(sStringBk);
    }

    sStringBk = txtEnfermo.getText(); //grabamos
    modoOperacion = modo;
    Inicializar();
  }

  void txtEnfermo_keyPressed(KeyEvent e) {
    txtFechaNac.setText("");
    txtNombre.setText("");
  }
} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  Pan_EDOPac adaptee = null;
  ActionEvent e = null;

  public actionListener(Pan_EDOPac adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    if (e.getActionCommand().equals("buscarArea")) {
      adaptee.btnCtrlbuscarArea_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) { // JRM2 Aqu� pasa si se pulsa el btn Obtener
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
//    else if (e.getActionCommand().equals("enfermo"))
//      adaptee.btnEnfermo_actionPerformed(e);
    }
  }
}

class Pan_EDOPac_btnEnfermo_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_EDOPac adaptee;
  ActionEvent e;

  Pan_EDOPac_btnEnfermo_actionAdapter(Pan_EDOPac adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnEnfermo_actionPerformed(e);
  }
}

class Pan_EDOPac_txtEnfermo_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_EDOPac adaptee;
  FocusEvent e;

  Pan_EDOPac_txtEnfermo_focusAdapter(Pan_EDOPac adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.txtEnfermo_focusLost(e);
  }
}

class Pan_EDOPac_txtEnfermo_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_EDOPac adaptee;

  Pan_EDOPac_txtEnfermo_keyAdapter(Pan_EDOPac adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtEnfermo_keyPressed(e);
  }
}
