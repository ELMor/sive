package infedopac;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class Pan_InfEDOPac
    extends EPanel {
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 1;

  final String strSERVLET = "servlet/SrvInfEDOPac";
  final String strLOGO_CCAA = "images/ccaa.gif";

  // estructuras de datos
  protected Vector vCasos;
  protected Vector vTotales;
  protected CLista lista;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  CApp app = null;

  final int erwCASOS_EDO = 1;

  public int erwCASO_ACTUAL = 2;

  public DataInfEDOPac paramC1 = null;

  public Pan_InfEDOPac(CApp a) {
    super(a);
    app = a;
    res = ResourceBundle.getBundle("infedopac.Res" + a.getIdioma());
    try {
      stub = new StubSrvBD();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + "erw/ERWEDOPac.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;

    //E //#// System_out.println(lista.getState());
    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        PrepararInforme();
        param.setFilter(lista.getFilter());
        // obtiene los datos del servidor
        paramC1.iPagina++;
        param.addElement(paramC1);
        param.setFilter("");
        param.setIdioma(app.getIdioma());
        param.trimToSize();
        stub.setUrl(new URL(app.getURL() + strSERVLET));

        param.setLogin(app.getLogin());
        param.setLortad(app.getParameter("LORTAD"));

        lista = (CLista) stub.doPost(erwCASOS_EDO, param);
        v = (Vector) lista.elementAt(0);
        vTotales = (Vector) lista.elementAt(1);

        for (int j = 0; j < v.size(); j++) {
          vCasos.addElement(v.elementAt(j));

          // control de registros
        }
        Integer tot = (Integer) lista.elementAt(1);
        this.setTotalRegistros(tot.toString());
        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // repintado
        erwClient.refreshReport(true);

      }
      catch (Exception e) {
        e.printStackTrace();
        //AIC
        msgBox = new CMessage(this.app, CMessage.msgERROR, "Error en mostrar()");
        //msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());

        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // informe comleto
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // genera el primer informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  protected boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    CLista param = new CLista();
    DataInfEDOPac datosPar = null;
    boolean elBoolean = true;

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      //E //#// System_out.println("Antes prepara");
      PrepararInforme();
      //E //#// System_out.println("Despu�s prepara");
      // obtiene los datos del servidor
      paramC1.iPagina = 0;
      //E //#// System_out.println("Antes llama");
      datosPar = new DataInfEDOPac(paramC1.DS_ENF, paramC1.DS_SEMEPI,
                                   paramC1.DS_CENTRO,
                                   paramC1.EDAD, paramC1.DS_ZBS, paramC1.CLASIF);
      // �todos los datos?
      //E //#// System_out.println("despu�s llama");
      datosPar.bInformeCompleto = conTodos;

      //param.addElement(paramC1);
      param.addElement(datosPar);
      param.setIdioma(app.getIdioma());

      param.trimToSize();
      stub.setUrl(new URL(app.getURL() + strSERVLET));
      lista = new CLista();
      lista.setState(CLista.listaVACIA);
      //E //#// System_out.println("Antes stub");

      param.setLogin(app.getLogin());
      param.setLortad(app.getParameter("LORTAD"));

//      lista = (CLista) stub.doPost(erwCASOS_EDO, param);

      SrvInfEDOPac srv = new SrvInfEDOPac();
      srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@192.168.0.13:1521:ICM",
                             "dba_edo",
                             "manager");
      lista = srv.doDebug(erwCASOS_EDO, param);

      //E //#// System_out.println("desp. stub");

      // control de registros
      if (lista == null || lista.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg6.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
        elBoolean = false;
      }
      else {
        //E //#// System_out.println("Estoy dentro");
        vCasos = (Vector) lista.elementAt(0);
        //E //#// System_out.println("leo sig.");
        //vTotales = (Vector) lista.elementAt(1);
        Integer tot = (Integer) lista.elementAt(1);
        this.setTotalRegistros(tot.toString());
        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // establece las matrices de datos
        Vector retval = new Vector();
        retval.addElement("DS_ENF = DS_ENF");
        retval.addElement("DS_SEMEPI = DS_SEMEPI");
        retval.addElement("DS_CENTRO = DS_CENTRO");
        retval.addElement("EDAD = EDAD");
        retval.addElement("DS_ZBS = DS_ZBS");
        retval.addElement("CLASIF = CLASIF");
        dataHandler.RegisterTable(vCasos, "SIVE_C4", retval, null);
        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        elBoolean = true;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      //AIC
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR,
                            "Error en LlamadaInforme");
      //gBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());

      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();
      elBoolean = false;
    }

    modoOperacion = modoNORMAL;
    Inicializar();
    return elBoolean;
  }

  // este informe no tiene grafica
  public void MostrarGrafica() {
  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;
    int iEtiqueta = 0;

    // plantilla
    TemplateManager tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
    tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

    // carga los citerios
    /*
        tm.SetLabel("LAB005", "C�digo Enfermo: " + paramC1.DS_SEMEPI);
        tm.SetLabel("LAB007", "Apellidos y Nombre: " + paramC1.DS_ENF);
        tm.SetLabel("LAB008", "Fecha Nacimiento: " + paramC1.EDAD);
        tm.SetLabel("LAB001", "C�digo: " + paramC1.DS_SEMEPI
          +" Enfermo: " + paramC1.DS_ENF
          +" Fec. Nac.: " + paramC1.EDAD);
     */
    tm.SetLabel("CRITERIO1", res.getString("msg7.Text") + paramC1.DS_SEMEPI
                + res.getString("msg8.Text") + paramC1.DS_ENF);

    tm.SetLabel("CRITERIO2", res.getString("msg9.Text") + paramC1.EDAD);

  }
}
