
package infedopac;

// ARG: (3/6/2002) Para el calculo de la edad
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;

// LORTAD
import Registro.RegistroConsultas;
import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvInfEDOPac
    extends DBServlet {

  // informes
  final int erwCASOS_EDO = 1;

  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    DataInfEDOPac datosEq = null;

    // control
    int i = 1;
    String sCodigos = null;
    CLista sDescripciones = null;
    String sTempo = null;
    int totalRegistros = 0;

    // buffers
    String sCod;
    int pDes = 0;
    int iNum;
    DataInfEDOPac dat = null;

    // objetos de datos
    CLista data = new CLista();
    data.setState(CLista.listaVACIA);

    Vector registros = new Vector();
    Vector registrosAux = new Vector();
    Vector totales = new Vector();
    int totReal = 0;
    int notifTotal = 0;
    float cober = 0;
    String cadi = "";
    int primerRegistro = 0;

    // c�lculo de edad del enfermo
    java.util.Date laFecha;
    String fechaNac = "";
    String fechaNot = "";
    int anyoNac = 0;
    int anyoNot = 0;
    int mesNac = 0;
    int mesNot = 0;
    int anyos = 0;
    int meses = 0;
    Calendar cal = new GregorianCalendar();
    String laEdad = "";

    java.util.Date fecNacimiento = null;
    java.util.Date fecReferencia = null;

    //Para ver qu� descripci�n se coge (si es local o no)
    String sDesPro = "";
    String sDesProL = "";
    String sDesClaL = "";
    String sDesZbsL = "";

    /****************** APLICACION DE LORTAD ********************************/
    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;
          /****************** APLICACION DE LORTAD ********************************/

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);
    con.setAutoCommit(true);
    //E //#// System_out.println("1");
    datosEq = (DataInfEDOPac) param.firstElement();
    //E //#// System_out.println("2");
    fechaNac = datosEq.EDAD;
    //E //#// System_out.println("3");
    if (fechaNac.length() > 0) {
      SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
      //E //#// System_out.println("4");
      laFecha = formateador.parse(fechaNac);
      fecNacimiento = formateador.parse(fechaNac);
      //E //#// System_out.println("5");
      cal.setTime(laFecha);
      //E //#// System_out.println(laFecha);
      //E //#// System_out.println("6");
      anyoNac = cal.get(Calendar.YEAR);
      //E //#// System_out.println("7");
      mesNac = cal.get(Calendar.MONTH);
      //E //#// System_out.println("8");
    }
    // modos de operaci�n
    switch (opmode) {

      // l�neas del modelo
      case erwCASOS_EDO:

        //E //#// System_out.println(mesNac);
        //E //#// System_out.println(anyoNac);

        data.setFilter(param.getFilter());

        // registros totales
        query = "select count(*) from sive_edoind a, sive_procesos b " +
            "where a.CD_ENFCIE=b.CD_ENFCIE and a.CD_ENFERMO=?";
        st = con.prepareStatement(query);
        // c�digo del enfermo
        st.setString(1, datosEq.DS_SEMEPI);
        // LORTAD
        registroConsultas.insertarParametro(datosEq.DS_SEMEPI);
        registroConsultas.registrar("SIVE_EDOIND",
                                    query,
                                    "CD_ENFERMO",
                                    datosEq.DS_SEMEPI,
                                    "SrvInfEDOPac",
                                    true);

        rs = st.executeQuery();
        totalRegistros = 0;
        if (rs.next()) {
          totalRegistros = rs.getInt(1);
        }
        rs.close();
        st.close();
        rs = null;
        st = null;
        // Primer registro a buscar
        primerRegistro = datosEq.iPagina * DBServlet.maxSIZE;
        query = "select a.NM_EDO, a.CD_ENFCIE, a.CD_ANOEPI, a.CD_SEMEPI, " +
            "a.FC_FECNOTIF, a.CD_ZBS, a.CD_CLASIFDIAG, b.DS_PROCESO, b.DSL_PROCESO, a.CD_NIVEL_1, a.CD_NIVEL_2 " +
            "from sive_edoind a, sive_procesos b where a.CD_ENFCIE=b.CD_ENFCIE" +
            " and a.CD_ENFERMO=? order by a.CD_ANOEPI, a.CD_SEMEPI";
        // lanza la query
        st = con.prepareStatement(query);
        // c�digo del enfermo
        st.setString(1, datosEq.DS_SEMEPI);
        // LORTAD
        registroConsultas.insertarParametro(datosEq.DS_SEMEPI);
        registroConsultas.registrar("SIVE_EDOIND",
                                    query,
                                    "CD_ENFERMO",
                                    datosEq.DS_SEMEPI,
                                    "SrvInfEDOPac",
                                    true);
        rs = st.executeQuery();

        i = 0;
        // extrae la p�gina requerida
        if (param.getFilter().length() > 0) {
          for (int j = 0; j < Integer.parseInt(param.getFilter()); j++) {
            rs.getString("NM_EDO");
          }
        }
        while (rs.next()) {
          // control de tama�o
          if ( (i > DBServlet.maxSIZE) && (! (datosEq.bInformeCompleto))) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter(Integer.toString(i));

            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
            //Recogemos datos modelo
          }
          if (fechaNac.length() > 0) {
            laFecha = new java.util.Date();
            laFecha = rs.getDate("FC_FECNOTIF");

            fecReferencia = rs.getDate("FC_FECNOTIF");
            fechaNot = laFecha.toString();
            cal.setTime(laFecha);
            anyoNot = cal.get(Calendar.YEAR);
            mesNot = cal.get(Calendar.MONTH);
            if (mesNot > mesNac) {
              anyos = anyoNot - anyoNac;
              meses = mesNot - mesNac;
            }
            else if (mesNot == mesNac) {
              anyos = anyoNot - anyoNac;
              meses = 0;
            }
            else {
              anyos = (anyoNot - anyoNac) - 1;
              meses = (mesNot + 12) - mesNac;
            }
            if (anyos <= 0) {

              //laEdad = Integer.toString(meses) + " m";
              // ARG: Nuevo calculo de la edad
              laEdad = Integer.toString(enfermo.Fechas.edadMesesConRef(
                  fecNacimiento, fecReferencia)) + " m";
            }
            else {

              //laEdad = Integer.toString(anyos);
              // ARG: Nuevo calculo de la edad
              laEdad = Integer.toString(enfermo.Fechas.edadAniosConRef(
                  fecNacimiento, fecReferencia));
            }
          }
          else {
            laEdad = "";
          }

          //____________________________________

          sDesPro = rs.getString("DS_PROCESO");
          // obtiene la descripcion auxiliar en funci�n del idioma
          if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
            sDesProL = rs.getString("DSL_PROCESO");
            if (sDesProL != null) {
              sDesPro = sDesProL;
            }
          }
          dat = new DataInfEDOPac(sDesPro,
                                  rs.getString("CD_ANOEPI") + "-" +
                                  rs.getString("CD_SEMEPI"),
                                  rs.getObject("NM_EDO").toString(),
                                  laEdad,
                                  rs.getString("CD_ZBS"),
                                  rs.getString("CD_CLASIFDIAG"));

          //____________________________________
          dat.CD_N1 = rs.getString("CD_NIVEL_1");
          dat.CD_N2 = rs.getString("CD_NIVEL_2");
          registros.addElement(dat);
          i++;
        } //while que recorre el ResultSet

        rs.close();
        rs = null;
        st.close();
        st = null;

        if (registros.size() > 0) {
          registrosAux = new Vector();
          for (int r = 0; r < registros.size(); r++) {

            dat = (DataInfEDOPac) registros.elementAt(r);

            // totales
            query = "select DS_CENTRO from sive_c_notif where CD_CENTRO in(" +
                "select CD_CENTRO from sive_e_notif where CD_E_NOTIF in(" +
                "select cd_e_notif from sive_notif_edoi where NM_EDO=? and IT_PRIMERO='S' and CD_FUENTE = 'E' ))";
            // lanza la query
            st = con.prepareStatement(query);
            // n�mero caso EDO
            st.setString(1, dat.DS_CENTRO);
            rs = st.executeQuery();

            // a�ado los datos que faltan
            i = 0;

            if (rs.next()) {
              dat.DS_CENTRO = rs.getString("DS_CENTRO");
            }
            registrosAux.addElement(dat);
            i++;

            rs.close();
            rs = null;
            st.close();
            st = null;
          }
        }

        // ahora vamos a por las descripciones
        // descripciones de los diagn�sticos
        registros = new Vector();
        for (int j = 0; j < registrosAux.size(); j++) {
          dat = (DataInfEDOPac) registrosAux.elementAt(j);
          if ( (dat.CLASIF != null) && (dat.CLASIF.compareTo("") != 0)) {
            query = "select * from SIVE_CLASIF_EDO where CD_CLASIFDIAG=?";
            // lanza la query
            st = con.prepareStatement(query);
            // a�o epidemiol�gico
            st.setString(1, dat.CLASIF);

            rs = st.executeQuery();

            // a�ado los datos que faltan
            i = 0;

            if (rs.next()) {

              dat.CLASIF = rs.getString("DS_CLASIFDIAG");
              //____________________________________
              // obtiene la descripcion auxiliar en funci�n del idioma
              if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
                sDesClaL = rs.getString("DSL_CLASIFDIAG");
                if (sDesClaL != null) {
                  dat.CLASIF = sDesClaL;
                }
              }
              //____________________________________

            }
            rs.close();
            rs = null;
            st.close();
            st = null;
          }
          else {
            dat.CLASIF = "";
          }
          registros.addElement(dat);
          i++;

        }

        // descripciones de las zonas b�sicas
        registrosAux = new Vector();
        for (int j = 0; j < registros.size(); j++) {
          dat = (DataInfEDOPac) registros.elementAt(j);
          if ( (dat.DS_ZBS != null) && (dat.CD_N1 != null) && (dat.CD_N2 != null) &&
              (dat.DS_ZBS.compareTo("") != 0) && (dat.CD_N1.compareTo("") != 0) &&
              (dat.CD_N2.compareTo("") != 0)) {
            query =
                "select * from SIVE_ZONA_BASICA where CD_ZBS=? and CD_NIVEL_1=? " +
                "and CD_NIVEL_2=?";
            // lanza la query
            st = con.prepareStatement(query);
            // zona b�sica
            st.setString(1, dat.DS_ZBS);
            // nivel 1
            st.setString(2, dat.CD_N1);
            // nivel 2
            st.setString(3, dat.CD_N2);

            rs = st.executeQuery();

            // a�ado los datos que faltan
            i = 0;

            if (rs.next()) {
              dat.DS_ZBS = rs.getString("DS_ZBS");

              //____________________________________
              // obtiene la descripcion auxiliar en funci�n del idioma
              if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
                sDesZbsL = rs.getString("DSL_ZBS");
                if (sDesZbsL != null) {
                  dat.DS_ZBS = sDesZbsL;
                }
              }
              //____________________________________

            }
            rs.close();
            rs = null;
            st.close();
            st = null;

          }
          else {
            dat.DS_ZBS = "";
          }
          registrosAux.addElement(dat);
          i++;

        }

        /*
                  // descripciones de las enfermedades
                  registros = new Vector();
                  for (int j=0;j<registrosAux.size();j++){
                    dat = (DataInfEDOPac)registrosAux.elementAt(j);
                    if ((dat.DS_ENF != null)){
             query = "select DS_ENFERCIE from SIVE_ENFER_CIE where CD_ENFCIE=?";
                      // lanza la query
                      st = con.prepareStatement(query);
                      // c�digo enfermedad
                      st.setString(1, dat.DS_ENF);
                      rs = st.executeQuery();
                      // a�ado los datos que faltan
                      i = 0;
                      if (rs.next()){
                        dat.DS_ENF = rs.getString("DS_ENFERCIE");
                      }
                      rs.close();
                      rs = null;
                      st.close();
                      st = null;
                    }else{
                      dat.DS_ENF = "";
                    }
                    registros.addElement(dat);
                    i++;
                  }
         */
        registros = registrosAux;
        break;
    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    // prepara los datos
    if (registros.size() > 0) {
      registros.trimToSize();
      data.addElement(registros);
      data.addElement(new Integer(totalRegistros));
      data.trimToSize();
    }
    else {
      data = null;
      //data.setState(CLista.listaVACIA);
    }

    return data;
  }
}
