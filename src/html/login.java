package html;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CDialog;
import capp.CImage;
//PDP 03/05/2000
import cifrado.Cifrar;

public class login
    extends CDialog {
  //modos de pantalla
  public static final int modoNORMAL = 0;
  ResourceBundle res;
  public static final int modoESPERA = 5;

  //Constantes del panel
  final String imgACEPTAR = "images/aceptar.gif";
  final String imgSEGURIDAD = "images/seguridad.gif";

  // imagen
  protected CImage image = null;

  // par�metros
  protected int modoOperacion = modoNORMAL;
  protected URL url = null;
  private parametros hash = null;

  /**
   * Controla si la ventana es cerrada con el aspa
   */
  private boolean cerradaConAspa = false;

  // controles
  XYLayout xyLyt = new XYLayout();
  Label lblUsuario = new Label();
  TextField txtUID = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  Label lblContrase�a = new Label();
  TextField txtPWD = new TextField();
  StatusBar status = new StatusBar();
  actionListener btnActionListener = new actionListener(this);

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        btnAceptar.setEnabled(true);
        txtUID.setEnabled(true);
        txtPWD.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        status.setText("");
        break;
      case modoESPERA:
        btnAceptar.setEnabled(false);
        txtUID.setEnabled(false);
        txtPWD.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        status.setText(res.getString("status.Text"));
        break;
    }
  }

  public login(CApp a, String u, String t) {

    super(a);

    try {
      res = ResourceBundle.getBundle("html.Res" + a.getIdioma());
      this.setTitle(t);

      // modificacion jlt para recuperar el url_servlet
      String ur = new String();
      String ur2 = new String();
      String ur3 = new String();
      String ur4 = new String();
      int ind = 0;
      int indb = 0;
      ur = (String) a.getDocumentBase().toString();
      ind = ur.lastIndexOf("/");
      ur2 = (String) ur.substring(0, ind + 1);
      ur3 = (String) ur2.substring(0, ind);
      indb = ur3.lastIndexOf("/");
      ur4 = (String) ur3.substring(0, indb + 1);
      // System_out.println("urlista2" + ur2);
      // System_out.println("urlista2" + ur4);
      /////////////////

      if (u.equals("") || u.equals("/") || u.equals(null)) {

        this.url = new URL(ur4 + "servlet/AutServlet");
      }
      else {

        this.url = new URL(u + "servlet/AutServlet");
      }

      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // aspecto de la ventana
  private void jbInit() throws Exception {
    setSize(290, 170);
    xyLyt.setHeight(170);
    xyLyt.setWidth(290);
    lblUsuario.setText(res.getString("lblUsuario.Text"));
    txtUID.setBackground(new Color(255, 255, 150));
    status.setBevelOuter(BevelPanel.LOWERED);
    status.setBevelInner(BevelPanel.LOWERED);
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnAceptar.setImageURL(new URL(app.getCodeBase(), imgACEPTAR));
    lblContrase�a.setText(res.getString("lblContrasena.Text"));
    txtPWD.setBackground(new Color(255, 255, 150));
    txtPWD.setEchoChar('*');
    btnAceptar.setActionCommand("aceptar");
    this.setLayout(xyLyt);
    this.add(lblUsuario, new XYConstraints(81, 11, -1, 17));
    this.add(txtUID, new XYConstraints(177, 5, 100, -1));
    this.add(txtPWD, new XYConstraints(177, 39, 100, -1));
    this.add(btnAceptar, new XYConstraints(197, 78, 80, 26));
    this.add(lblContrase�a, new XYConstraints(81, 45, 74, 17));
    this.add(status, new XYConstraints(3, 116, 277, 25));

    // establece los escuchadores
    btnAceptar.addActionListener(btnActionListener);

    this.addWindowListener(new EventosVentana(this));

    // imagen
    try {
      image = new CImage(new URL(app.getCodeBase(), imgSEGURIDAD), 60, 60);
      this.add(image, new XYConstraints(10, 10, 70, 70));
    }
    catch (Exception e) {
      image = null;
    }

    // establece el modo de operaci�n
    Inicializar();
  }

  // comprueba el login introducido
  protected void verificarLogin() {

    parametros uid = null;
    Object data = null;
    URLConnection con = null;
    ObjectInputStream in = null;
    ObjectOutputStream out = null;
    String pwdCifrada = ""; //PDP 03/05/2000
    if ( (txtUID.getText().length() > 0) &&
        (txtPWD.getText().length() > 0)) {

      modoOperacion = modoESPERA;
      Inicializar();

      try {
        // abre la conexi�n con el servlet de autentificaci�n
        con = url.openConnection();
        con.setRequestProperty("Content-type", "application/octet-stream");
        con.setDoOutput(true);
        con.setUseCaches(false);

        // cifrado de password //PDP 03/05/2000
        pwdCifrada = Cifrar.cifraClave(txtPWD.getText());

        // graba los par�metros para autentificar
        uid = new parametros();
        uid.put("LOGIN", txtUID.getText());

        uid.put("PASSWORD", pwdCifrada); //PDP 03/05/2000
        //uid.put("PASSWORD", txtPWD.getText());
        uid.put("IDIOMA", new Integer(app.getIdioma()));

        // invoca el modo de operaci�n del servlet
        out = new ObjectOutputStream(con.getOutputStream());

        // envia usuario y password
        out.writeObject(uid);
        out.flush();
        out.close();

        // lee los par�metros de retorno
        in = new ObjectInputStream( (InputStream) con.getInputStream());
        data = in.readObject();

        // cierra los streams
        in.close();

        // levanta la excepci�n producida en el servlet
        if (data != null) {
          if (data.getClass().getName().equals("java.lang.Exception")) {
            throw (Exception) data;
          }
        }

        // graba los datos de la sesi�n
        hash = (parametros) data;

        // a�ade login, url_html e idioma
        hash.put("LOGIN", txtUID.getText());
        hash.put("TSIVE", "E");

        // modificacion jlt para recuperar el url_servlet
        String ur = new String();
        String ur2 = new String();
        String ur3 = new String();
        String ur4 = new String();
        int ind = 0;
        int indb = 0;
        ur = (String) app.getDocumentBase().toString();
        ind = ur.lastIndexOf("/");
        ur2 = (String) ur.substring(0, ind + 1);
        ur3 = (String) ur2.substring(0, ind);
        indb = ur3.lastIndexOf("/");
        ur4 = (String) ur3.substring(0, indb + 1);
        // System_out.println("urlista2" + ur2);
        // System_out.println("urlista2" + ur4);
        /////////////////

        if (app.getREF().equals("") || app.getREF().equals("/")
            || app.getREF().equals(null)) {

          hash.put("URL_HTML", ur2);
        }
        else {

          hash.put("URL_HTML", app.getREF());
        }
        ///////////////////////////////////
        //hash.put("URL_HTML", app.getREF());
        hash.put("IDIOMA", new Integer(app.getIdioma()));

        modoOperacion = modoNORMAL;
        Inicializar();

      }
      catch (Exception e) {
        e.printStackTrace();
        hash = null;
        modoOperacion = modoNORMAL;
        Inicializar();
        if ( (e.toString()).indexOf("ORA-01017") != -1) { //PDP 03/05/2000
          status.setText("Password o Usuario no v�lido");
        }
        else if ( (e.toString()).indexOf("ORA-00942") != -1) {
          status.setText("Password o Usuario no v�lido");
        }
        else {
          status.setText(e.getMessage());
        }
      }

      // si la sesi�n es v�lida, entra en ella
      if (hash != null) {
        dispose();
      }

    }
    else {
      status.setText(res.getString("status.Text1"));
    }

  }

  /**
   * Determina si la ventana es cerrada con el aspa
   * @return true si la ventana ha sido cerrada con el aspa
   */
  public boolean cancelada() {
    return cerradaConAspa;
  }

  /** Cierra el objeto gr�fico activo.
   *
   */
  protected void this_windowClosing() {
    cerradaConAspa = true;
    dispose(); // Se cierra con la cruz
  } //end this_windowClosing

  public parametros getHash() {
    return hash;
  }
}

/** Clase que se encarga de la gesti�n de los eventos de la ventana.
 *
 * @Author  LARG
 * @version 06/06/2000
 */

class EventosVentana
    implements WindowListener {
  login ventana = null;

  /** La ventana actual se actualiza en el constructor
   *
   */
  public EventosVentana(login ventana) {
    this.ventana = ventana;
  }

  /** Se cierra la ventana con el aspa.
   *
   */
  public void windowClosing(WindowEvent e) {
    ventana.this_windowClosing();
  }

  public void windowDeactivated(WindowEvent e) {
    ;
  }

  public void windowActivated(WindowEvent e) {
    ;
  }

  public void windowDeiconified(WindowEvent e) {
    ;
  }

  public void windowIconified(WindowEvent e) {
    ;
  }

  public void windowClosed(WindowEvent e) {
    ;
  }

  public void windowOpened(WindowEvent e) {
    ;
  }

}

// action listener
class actionListener
    implements ActionListener, Runnable {
  login adaptee = null;
  ActionEvent e = null;

  public actionListener(login adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  // hilo de ejecuci�n
  // Si no se pulsa el bot�n aceptar no se verifica el Login
  // LARG
  public void run() {
    if (e.getActionCommand().equals("aceptar")) {
      adaptee.verificarLogin();
    }
  }
} // actionListener
