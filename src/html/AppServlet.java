//Atenci�n: vesi�n de registro s�lo para explorer Febrero 2000
// JRM: 31/05/2000
// Modifica lo necesario para que en el entorno de desarrollo no se
// tiren de los zips sino de los applets, en si. Es decir, se comentan los
// sArchive.
//
package html;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import comun.constantes; //LRG Compatibilidad con USU de paquetes compartidos con ICM

public class AppServlet
    extends HttpServlet {

  // applets
  protected final int appletEDO = 1;
  protected final int appletSISTEMA = 2;
  protected final int appletINDICADORES = 3;
  protected final int appletMODELOS = 4;
  protected final int appletPREG = 5;
  protected final int appletLISTAS = 6;
  protected final int appletEDO_CNE = 8;
  protected final int appletCIE = 9;
  protected final int appletPROCESOS = 10;
  protected final int appletNIVEL1 = 11;
  protected final int appletNIVEL2 = 12;
  protected final int appletZBS = 13;
  protected final int appletCCAA = 14;
  protected final int appletPROV = 15;
  protected final int appletESPEC = 16;
  protected final int appletTLINEA = 17;
  protected final int appletTPREG = 18;
  protected final int appletTSIVE = 19;
  protected final int appletTVIGI = 20;
  protected final int appletMOTBAJA = 21;
  protected final int appletNIVASIS = 22;
  protected final int appletSEXO = 23;
  protected final int appletTASISTENCIA = 24;
  protected final int appletALARMAS = 25;
  protected final int appletNOTIFICACION = 26;
  protected final int appletENFERMO = 27;
  protected final int appletC1 = 28;
  protected final int appletC2 = 29;
  protected final int appletC3 = 30; // QQ Consulta de pacientesXenfermedad
  protected final int applet711 = 31; // QQ  Cobertura por semana
  protected final int applet712 = 32; // QQ  Cobertura por per�odo
  protected final int appletCEN = 33;
  protected final int appletEQP = 34;
  protected final int appletUSR = 35;
  protected final int appletTRV = 36; // Trasvase de datos
  //QQ Ayudas:
  protected final int ayudaPP = 37;
  protected final int ayudaAcerca = 38;
  protected final int appletEPI = 39; // Servicio de Epidemiolog�a
  protected final int applet_VEDO = 40; // Applets de volcados:
  protected final int applet_VCENTROS = 41;
  protected final int applet_VCASOS = 42;
  protected final int applet_VNOTIF = 43;
  protected final int applet_GENANO = 44; // Generaci�n del a�o epidemiol�gico
  protected final int applet_VCASOSI = 45; // Volcado 8.4: Casos individuales (sin preguntas)
  protected final int applet_ENVNUM = 46; // Envio de casos num�ricos
  protected final int applet_CEdoPac = 47; // Consulta de relaci�n de casos EDO de un paciente.
  protected final int applet_ALAUTO = 48; // Generaci�n de Alarmas autom�ticas
  //---
  protected final int applet_consdemres = 49;
  protected final int applet_infnoteq = 50;
  protected final int applet_infedonoma = 51;
  protected final int applet_consconsenf = 52;
  protected final int applet_distcasnivres = 53;
  protected final int applet_disnotasis = 54;
  protected final int applet_infedovarnot = 55;
  protected final int applet_conscasoper = 73; // Casos notificados por un periodo clasificado por centro/notificador
  protected final int applet_VALARMA = 56;
  protected final int applet_CONSALAUTO = 57; //Consulta alarmas autom�ticas de una semana
  protected final int applet_CONSALAUTO_PER = 58; //Consulta alarmas autom�ticas de una semana
  protected final int applet_CASOSTASAS_AREA = 59;
  protected final int applet_CASOSTASASCIENAREA = 72; // Casos o tasas por 100000 habitantes
  protected final int applet_ENFERMO_EDO = 60;
  protected final int appletDIAGNOSTICO = 61;
  // JRM: Dos nuevas opciones de consulta: consulta de �ndices y env�o de cartas
  // con retrasos
  protected final int applet_INDICES = 100;
  protected final int applet_RETRASOS = 101;

  //QQ:Pista 06
  protected final int applet_CARGA_T = 62; //Carga de Tablas
  protected final int applet_DESCARGA_T = 63; //Descarga de Tablas
  protected final int applet_CARGA_MCNE = 64; //Carga Modelo del CNE
  protected final int applet_DESCARGA_M = 65; //Descarga de Modelos
  protected final int applet_ENVUR = 66; //Env�os urgentes
  protected final int applet_ENVRESSEM = 67; //Env�o de resumen semanal

  //QQ:Acceso al CNE
  protected final int applet_CATA = 68; // Acceso al cat�logo de tablas
  protected final int applet_DICCIO = 69; // Acceso al Diccionario Central de Protocolos
  protected final int applet_FTP = 70; // M�dulo FTP
  protected final int appletMANTENIMIENTO_CASOS_EDO = 71;

  //Nuevos
  protected final int applet_grafcomenf = 74; // Gr�fico comparativo de una enfermedad
  protected final int applet_grafcomphist = 75; // Gr�fico comparativo hist�rico
  protected final int appletLIMPIAR_NOTI = 77; // notlimpia
  protected final int applet_ENVOTRCOM = 78;

  // brotes
  protected final int applet_ALERTA = 80;
  protected final int applet_BROTES = 81;
  protected final int applet_MNTO_GRUPO_BROTE = 82;
  protected final int applet_MNTO_TIPO_NOTIFICADOR = 83;
  protected final int applet_MNTO_MECANISMOS = 84;
  protected final int applet_MNTO_COLECTIVOS = 85;
  protected final int applet_MNTO_SITUACION = 86;
  protected final int applet_MNTO_TIPO_BROTE = 87;

  protected final int appletMODELOS_BROTE = 88;
  protected final int appletPREG_BROTE = 89;

  // servicio
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {

    // streams
    //ObjectInputStream in = null;
    ServletOutputStream out;

    // par�metros del apllet
    int iCat = -1;
    int iCatAux = -1;
    int iCatPral = -1;
    String sWidth = "";
    String sHeight = "";
    String sCatalogoW = ""; //QQ Se establcen m�s abajo.
    String sCatalogoH = "";
    String sConsW = "";
    String sConsH = "";
    String sVolcW = "";
    String sVolcH = "";
    String sClass = "";
    String sLogin = "", sRef = "";
    String sAyuda = "ayuda_pp";
    boolean AppletConsulta = false; //DSR: control de los applets de consulta

    //Parametro para segundo applet si existe
    String sClassSegundoApplet = "";

    // QQ: Atenci�n esta variable solo vale para Explorer (INTERFAZ.CAB)
    final String sArchiveDefault = "EDO_util.zip";
    // Pruebas 2000 QQ: final String sArchiveDefault = "capp.zip, sapp.zip";
    //--------------

    String sArchive = "";
    String sTSIVE = "E";

    // comunicaciones servlet-applet
    try {

      // lee el modo de operaci�n
      //in = new ObjectInputStream(request.getInputStream());

      // lee los par�metros
      //hash = (sesion) in.readObject();

      // par�metros recibidos
      parametros hash = new parametros();
      loadTable(hash, request);

      // abre el stream de salida
      response.setContentType("text/html");
      out = response.getOutputStream();

      // cabecera
      out.println("<HTML><HEAD>");
      out.println("<TITLE>PISTA</TITLE>");
      out.println("<BASE HREF=\"" + hash.getString("URL_HTML", true) + "\">");
      out.println("</HEAD><BODY BGCOLOR=\"#FFFFFF\">");

      // determina el tama�o y el nombre de la clase
      sCatalogoW = "625"; //Tama�o para applets de mantenimiento.
      sCatalogoH = "325";
      sConsW = "640"; // Tama�o para applets de Consultas
      sConsH = "600";
      sVolcW = "502"; //Tama�o para applets de volcados (Exportaciones)
      sVolcH = "230";

      switch (new Integer(hash.getString("APPLET", true)).intValue()) {

        case appletEDO:
          sClass = "enfedo.MantEnferEDO.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + ", enfedo.zip, enfcie.zip";
          break;

        case appletLIMPIAR_NOTI:
          sClass = "notlimpiar.notcomenzar.class";
          sWidth = "680";
          sHeight = "400";
          sAyuda = "ayuda_lmpnoti";
          sArchive = sArchiveDefault + ", notlimpiar.zip";
          break;

        case appletSISTEMA:
          sClass = "param.parametros.class";
          sWidth = "570";
          sHeight = "390";
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + ", param.zip";
          break;

        case appletINDICADORES:
          sClass = "alarmas.ApAlarmas.class";
          sWidth = "615";
          sHeight = "320";
          sAyuda = "ayuda_parametrizacion";
          // JRM2
          sArchive = sArchiveDefault +
              ",sapp.zip,catalogo.zip,obj.zip,comun.zip, enfcentinela.zip";
          break;

        case appletMODELOS_BROTE:
          sTSIVE = "B";
        case appletMODELOS:
          sClass = "confprot.ApConfProt.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_parametrizacion";
          sArchive = " enfcentinela.zip,confprot.zip, capp.zip,sapp.zip,comun.zip,cargamodelo.zip,preguntas.zip,listas.zip,catalogo.zip";
          break;

        case appletPREG_BROTE:
          sTSIVE = "B";
        case appletPREG:
          sClass = "preguntas.MantDePreguntas.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_parametrizacion";
          sArchive = sArchiveDefault + ", listas.zip, preguntas.zip";
          break;

        case appletLISTAS:
          sClass = "listas.mantlis.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_parametrizacion";
          sArchive = sArchiveDefault + ", listas.zip";
          break;

        case appletCIE:
          sClass = "enfcie.EnfCie.class";
          sWidth = sCatalogoW; // "660";
          sHeight = sCatalogoH; // "380";
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + ", enfcie.zip";
          break;

        case appletNIVEL1:
          sClass = "nivel1.Nivel1.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletNIVEL2:
          iCatPral = 20;
          iCatAux = 130;
          sClass = "catalogo2.Catalogo2.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletPROV:
          iCatPral = 10;
          iCatAux = 10;
          sClass = "catalogo2.Catalogo2.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletZBS:
          sClass = "zbs.ZBS.class";
          sWidth = sCatalogoW;
          sHeight = "430";
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletEDO_CNE:
          iCat = 20;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletPROCESOS:
          sClass = "Procesos.Procesos.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + ", procesos.zip, enfcie.zip";
          break;

        case appletCCAA:
          iCat = 10;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletESPEC:
          iCat = 120;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletTLINEA:
          iCat = 30;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletTPREG:
          iCat = 40;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletTSIVE:
          iCat = 50;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletTVIGI:
          iCat = 60;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletMOTBAJA:
          iCat = 70;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletNIVASIS:
          iCat = 80;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletSEXO:
          iCat = 100;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletTASISTENCIA:
          iCat = 110;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletDIAGNOSTICO:
          iCat = 180;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case appletNOTIFICACION:
          sClass = "notentrada.NotificacionEDO.class";
          sWidth = "700";
          sHeight = "480";
          sAyuda = "ayuda_notif";
          // JRM: Esto fue lo que coment�.
          // ARG: Comentarlo para probarlo en desarrollo
          sArchive = sArchiveDefault + ", notificacion.zip";

          /* Versi�n sin registro
                     sArchive = sArchiveDefault + ", infproto.zip "
                   + ", notadic.zip, notalarmas.zip"
                   + ", notduplic.zip, notindiv.zip, cartas.zip "
               + ", notentrada.zip, infedoind.zip, eapp.zip, bidial.zip, mantbi.zip";
                   // QQ: Pruebas de rendimiento (quitar erw) + ", notentrada.zip, infedoind.zip, eapp.zip, erw.zip, erwclient.zip, erwcommon.zip, bidial.zip, mantbi.zip";
           */
          break;

        case appletENFERMO:
          sClass = "enfermo.ApPruEnfermo.class";
          sWidth = "640";
          sHeight = "543";
          sAyuda = "ayuda_notif";
          sArchive = sArchiveDefault + ", notificacion.zip ";
          break;

        case applet_INDICES:

          // Para par�metro de los paneles de fechas.
          AppletConsulta = true;
          sClass = "indices.AppIndices.class";
          sWidth = sConsW;
          sHeight = "360";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ", indices.zip";
          break;

        case applet_RETRASOS:

          // Para par�metro de los paneles de fechas.
          AppletConsulta = true;
          sClass = "retrasos.AppRetrasos.class";
          sWidth = sConsW;
          sHeight = "360";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ", retrasos.zip";
          break;

        case appletC1:
          sClass = "consedo.CasosEDO.class";
          sWidth = sConsW;
          sHeight = "360";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + ""
              + ""
              + ", vcasin.zip, consedo.zip";
          break;

        case appletC2:
          sClass = "consEdoAgru.consEdoAgru.class";
          sWidth = sConsW;
          sHeight = "415";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + ""
              + ""
              + ", consedoagru.zip";
          break;
        case appletC3:
          sClass = "conspacenf.conspacenf.class";
          sWidth = sConsW;
          sHeight = "386";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + ""
              + ""
              + ", conspacenf.zip";
          break;

        case applet711:
          sClass = "infcobsem.CasosCobSem.class";
          sWidth = sConsW;
          sHeight = "340";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + " "
              + ", infcobper.zip"
              + ", infcobsem.zip";
          break;

        case applet712:
          sClass = "infcobper.CasosCobPer.class";
          sWidth = sConsW;
          sHeight = "330";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + ""
              + ", infcobper.zip";
          break;

        case appletCEN:
          sClass = "cn.cn.class";
          sWidth = sCatalogoW;
          sHeight = "370";
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + ""
              + "";
          break;

        case appletEQP:
          sClass = "eqNot.eqNot.class";
          sWidth = "670"; //"415";
          sHeight = "360"; //"425";
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + ""
              + "";
          break;

        case appletUSR:
          sClass = "mantus.ApMantUs.class";
          sWidth = sCatalogoW;
          sHeight = "345";
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault +
              ",notificacion.zip , capp2.zip, sapp2.zip "
              + ", mantus.zip";
          sClassSegundoApplet = "mantus.PsApMantUsu.class";
          break;

        case appletEPI:
          sClass = "mantdatos.mantdatos.class";
          sWidth = "565";
          sHeight = "380";
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + ", mantdatos.zip, param.zip";
          break;

        case applet_VEDO:
          sClass = "genfich.genfichedonum.class";
          sWidth = sVolcW;
          sHeight = sVolcH;
          sAyuda = "ayuda_trv";
          sArchive = sArchiveDefault + ""
              + ", genfich.zip";
          break;

        case applet_VCENTROS:
          sClass = "genfich.genfichcnotif.class";
          sWidth = sVolcW;
          sHeight = sVolcH;
          sAyuda = "ayuda_trv";
          sArchive = sArchiveDefault + ""
              + ", genfich.zip";
          break;

        case applet_VCASOS:
          sClass = "genfich.genfichnotificaciones.class";
          sWidth = sVolcW;
          sHeight = sVolcH;
          sAyuda = "ayuda_trv";
          sArchive = sArchiveDefault + ""
              + ", genfich.zip";
          break;

        case applet_VNOTIF:
          sClass = "volCasosInd.volCasosInd.class";
          sWidth = "613";
          sHeight = "420";
          sAyuda = "ayuda_trv";
          sArchive = sArchiveDefault + " "
              + ""
              + ", vcasin.zip, volcasosind.zip";
          break;

        case applet_GENANO:
          sClass = "genano.genano.class";
          sWidth = "420";
          sHeight = "198";
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + ", genano.zip";
          break;

        case applet_VCASOSI:
          sClass = "volCasosSP.volCasosSP.class";
          sWidth = "600";
          sHeight = "394";
          sAyuda = "ayuda_trv";
          sArchive = sArchiveDefault + " "
              + ", vcasin.zip "
              + ", volcasosind.zip, volcasossp.zip";
          break;

        case applet_CEdoPac:
          sClass = "infedopac.CasosEDOPac.class";
          sWidth = sConsW;
          sHeight = "250";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + ""
              + ""
              + ", infedopac.zip";
          break;

        case applet_ALAUTO:
          sClass = "genAlaAuto.genAlaAuto.class";
          sWidth = sVolcW;
          sHeight = sVolcH;
          sAyuda = "ayuda_parametrizacion";
          sArchive = sArchiveDefault + ""
              + ", genalaauto.zip";
          break;

          //----Bater�a de consultas 23/4/99
        case applet_consdemres:
          sClass = "consdemres.consdemres.class";
          sWidth = sConsW;
          sHeight = "384";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ", conspacenf.zip "
              + ""
              + ""
              + ", consdemres.zip";
          break;

        case applet_infnoteq:
          sClass = "infnoteq.CasosNotEq.class";
          sWidth = sConsW;
          sHeight = "283";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + ""
              + ", infnoteq.zip";
          break;

        case applet_grafcomenf:
          sClass = "GrafComEnf.LanzaGrafCom.class";
          sWidth = sConsW;
          sHeight = "283";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + " "
              + ", grafcomenf.zip";
          break;

        case applet_grafcomphist:
          sClass = "grafcomphist.grafcomphist.class";
          sWidth = sConsW;
          sHeight = "350";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + ", grafcomphist.zip";
          break;

        case applet_infedonoma:
          sClass = "infedonoma.CasosEDOnoMA.class";
          sWidth = sConsW;
          sHeight = "330";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + ", infedonoma.zip";
          break;

        case applet_consconsenf:
          sClass = "consconsenf.consconsenf.class";
          sWidth = sConsW;
          sHeight = "215";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ", conspacenf.zip"
              + ", consconsenf.zip";
          break;

        case applet_distcasnivres:
          sClass = "distcasnivres.disCasNivRes.class";
          sWidth = sConsW;
          sHeight = "275";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + " "
              + " "
              + ", distcasnivres.zip";
          break;

        case applet_disnotasis:
          sClass = "disnotasis.disnotasis.class";
          sWidth = sConsW;
          sHeight = "320";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ", conspacenf.zip"
              + ""
              + ""
              + ", disnotasis.zip";
          break;

        case applet_infedovarnot:
          sClass = "infedovarnot.CasosEDOvarNot.class";
          sWidth = sConsW;
          sHeight = "286";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + ""
              + ", infedovarnot.zip";
          break;

        case applet_conscasoper:
          sClass = "conscasoper.conscasoper.class";
          sWidth = sConsW;
          sHeight = "386";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + " "
              + ""
              + ", vcasin.zip, conscasoper.zip";
          break;

          //----Fin bater�a consultas 23/4/99----
        case applet_VALARMA:
          sClass = "genfich.genfichalarmas.class";
          sWidth = "500";
          sHeight = "300";
          sAyuda = "ayuda_trv";
          sArchive = sArchiveDefault + ""
              + ", genfich.zip";
          break;

        case applet_CONSALAUTO:
          sClass = "ConsAlaAuto.ConsAlaAuto.class";
          sWidth = sConsW;
          sHeight = "240";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + " "
              + ""
              + ", consalaauto.zip";
          break;

        case applet_CONSALAUTO_PER:
          sClass = "consAlaAutoPer.ConsAlaAutoPer.class";
          sWidth = sConsW;
          sHeight = "390";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + ""
              + ", consalaautoper.zip";
          break;

        case applet_CASOSTASAS_AREA:
          sClass = "infcata.CasosEDOcata.class";
          sWidth = sConsW;
          sHeight = "360";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + ""
              + ", infcata.zip";
          break;

        case applet_CASOSTASASCIENAREA:
          sClass = "casostasascienarea.casosTasasCienArea.class";
          sWidth = sConsW;
          sHeight = "360";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + ""
              + ", casostasascienarea.zip";
          break;

        case applet_ENFERMO_EDO:
          sClass = "infconsenf.CasosConsEnf.class";
          sWidth = sConsW;
          sHeight = "190";
          sAyuda = "ayuda_cons";
          AppletConsulta = true;
          sArchive = sArchiveDefault + ""
              + ""
              + ", infconsenf.zip";
          break;

          //QQ: Pista 06

        case applet_CARGA_T:
          sClass = "carga.Carga.class";
          sWidth = "483";
          sHeight = "250";
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + ", carga.zip";
          break;

        case applet_DESCARGA_T:
          sClass = "descarga.Descarga.class";
          sWidth = sVolcW;
          sHeight = sVolcH;
          sAyuda = "ayuda_trv";
          sArchive = sArchiveDefault + ", descarga.zip";
          break;

        case applet_CARGA_MCNE:
          sClass = "cargamodelo.CargaModelo.class";
          sWidth = "483";
          sHeight = "250";
          sAyuda = "ayuda_parametrizacion";
          sArchive = sArchiveDefault + ", cargamodelo.zip";
          break;

        case applet_DESCARGA_M:
          sClass = "descargamodelos.DesMod.class";
          sWidth = sVolcW;
          sHeight = sVolcH;
          sAyuda = "ayuda_trv";
          sArchive = sArchiveDefault + ", confprot.zip"
              + ", listas.zip, preguntas.zip, descargamodelos.zip";
          break;

        case applet_ENVUR:
          sClass = "envressem.EnvUrg.class";
          sWidth = "665";
          sHeight = "370";
          sAyuda = "ayuda_trv";
          sArchive = sArchiveDefault + ""
              + ", envressem.zip, envotrcom.zip, infedoind.zip, mantdatos.zip";
          break;

        case applet_ENVOTRCOM:
          sClass = "envotrcom.EnvOtrCom.class";
          sWidth = "665";
          sHeight = "400";
          sAyuda = "ayuda_trv";
          sArchive = sArchiveDefault + ""
              + ", envressem.zip, envotrcom.zip, infedoind.zip, mantdatos.zip";
          break;

        case applet_ENVRESSEM:
          sClass = "envressem.envressem.class";
          sWidth = "500";
          sHeight = "440";
          sAyuda = "ayuda_trv";
          sArchive = sArchiveDefault + ""
              + ", envressem.zip";
          break;

        case appletMANTENIMIENTO_CASOS_EDO:
          sClass = "notmantenimiento.notmanten.class";
          sWidth = "574";
          sHeight = "317";
          sAyuda = "ayuda_notif";
          sArchive = sArchiveDefault + ", notificacion.zip";
          break;

        case applet_ALERTA:
          sTSIVE = "B";
          sClass = "mantbrotes.mbcomenzar.class";
          sWidth = "670";
          sHeight = "400";
          sAyuda = "ayuda_notif";
          sArchive = sArchiveDefault + ", brotesEDO.zip";
          break;

        case applet_BROTES:
          sTSIVE = "B";
          sClass = "mantbi.mbi.class";
          sWidth = "670";
          sHeight = "400";
          sAyuda = "ayuda_notif";
          sArchive = sArchiveDefault + ", notificacion.zip, brotesEDO.zip";
          break;

        case applet_MNTO_GRUPO_BROTE:
          sTSIVE = "B";
          iCat = 190;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case applet_MNTO_TIPO_NOTIFICADOR:
          sTSIVE = "B";
          iCat = 200;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case applet_MNTO_MECANISMOS:
          sTSIVE = "B";
          iCat = 210;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case applet_MNTO_COLECTIVOS:
          sTSIVE = "B";
          iCat = 220;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case applet_MNTO_SITUACION:
          sTSIVE = "B";
          iCat = 230;
          sClass = "catalogo.Catalogo.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          sArchive = sArchiveDefault + "";
          break;

        case applet_MNTO_TIPO_BROTE:
          sTSIVE = "B";
          iCatPral = 30;
          iCatAux = 190;
          sClass = "catalogo2.Catalogo2.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_mnto";
          // JRM: Faltaba Catalogo2
          sArchive = sArchiveDefault + ", catalogo2.zip";
          break;
      }

      out.println("<TABLE WIDTH=\"100%\" HEIGHT=\"100%\" BORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"\"0>");
      out.println("<TR><TD ALIGN=\"CENTER\" VALIGN=\"MIDDLE\">");

      // applet
      out.println("<APPLET");
      out.println(" MAYSCRIPT");

      // ZIP
      // System_out.println(sArchive);
      if (sArchive.equals("")) {
        out.println(" CODEBASE = \"applet/\"");
      }
      else {
        out.println(" CODEBASE = \"zip/\"");
        out.println(" ARCHIVE = \"" + sArchive + "\"");
      }
      out.println(" CODE     = \"" + sClass + "\"");
      out.println(" NAME     = \"PISTA" + hash.getString("APPLET", true) + "\"");
      out.println(" WIDTH    = \"" + sWidth + "\"");
      out.println(" HEIGHT   = \"" + sHeight + "\"");
      out.println(" HSPACE   = \"0\"");
      out.println(" VSPACE   = \"0\"");
      out.println(" ALIGN   = \"CENTER\"");
      out.println(">");

      out.println("<PARAM NAME=\"CA\" VALUE=\"" + hash.getString("CA", true) +
                  "\">");
      out.println("<PARAM NAME=\"URL_SERVLET\" VALUE=\"" +
                  hash.getString("URL_SERVLET", true) + "\">");
      out.println("<PARAM NAME=\"FTP_SERVER\" VALUE=\"" +
                  hash.getString("FTP_SERVER", true) + "\">");
      out.println("<PARAM NAME=\"DS_EMAIL\" VALUE=\"" +
                  hash.getString("DS_EMAIL", false) + "\">");
      out.println("<PARAM NAME=\"NIVEL1\" VALUE=\"" + hash.getString("NIVEL1", true) +
                  "\">");
      out.println("<PARAM NAME=\"NIVEL2\" VALUE=\"" + hash.getString("NIVEL2", true) +
                  "\">");
      out.println("<PARAM NAME=\"NIVEL3\" VALUE=\"" + hash.getString("NIVEL3", true) +
                  "\">");
      out.println("<PARAM NAME=\"IDIOMA\" VALUE=\"" +
                  hash.getInteger("IDIOMA", true).toString() + "\">");
      out.println("<PARAM NAME=\"LOGIN\" VALUE=\"" + hash.getString("LOGIN", true) +
                  "\">");
      out.println("<PARAM NAME=\"TSIVE\" VALUE=\"" + sTSIVE + "\">");
      out.println("<PARAM NAME=\"IDIOMA_LOCAL\" VALUE=\"" +
                  hash.getString("IDIOMA_LOCAL", false) + "\">");
      out.println("<PARAM NAME=\"CATALOGO\" VALUE=\"" + Integer.toString(iCat) +
                  "\">");
      out.println("<PARAM NAME=\"CATAUX\" VALUE=\"" + Integer.toString(iCatAux) +
                  "\">");
      out.println("<PARAM NAME=\"CATPRAL\" VALUE=\"" +
                  Integer.toString(iCatPral) + "\">");
      out.println("<PARAM NAME=\"PERFIL\" VALUE=\"" + hash.getString("PERFIL", true) +
                  "\">");
      out.println("<PARAM NAME=\"AYUDA\" VALUE=\"" + "ayuda/" +
                  hash.getInteger("IDIOMA", true).toString() + "/" + sAyuda +
                  ".html\">");
      out.println("<PARAM NAME=\"IT_AUTALTA\" VALUE=\"" +
                  hash.getString("IT_AUTALTA", true) + "\">");
      out.println("<PARAM NAME=\"IT_AUTBAJA\" VALUE=\"" +
                  hash.getString("IT_AUTBAJA", true) + "\">");
      out.println("<PARAM NAME=\"IT_AUTMOD\" VALUE=\"" +
                  hash.getString("IT_AUTMOD", true) + "\">");
      out.println("<PARAM NAME=\"IT_FG_ENFERMO\" VALUE=\"" +
                  hash.getString("IT_FG_ENFERMO", true) + "\">");
      out.println("<PARAM NAME=\"IT_MODULO_3\" VALUE=\"" +
                  hash.getString("IT_MODULO_3", true) + "\">");
      out.println("<PARAM NAME=\"IT_TRAMERO\" VALUE=\"" +
                  hash.getString("IT_TRAMERO", true) + "\">");
      out.println("<PARAM NAME=\"CD_E_NOTIF\" VALUE=\"" +
                  hash.getString("CD_E_NOTIF", false) + "\">");
      out.println("<PARAM NAME=\"DS_E_NOTIF\" VALUE=\"" +
                  hash.getString("DS_E_NOTIF", false) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL_1_EQ\" VALUE=\"" +
                  hash.getString("CD_NIVEL_1_EQ", false) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL_2_EQ\" VALUE=\"" +
                  hash.getString("CD_NIVEL_2_EQ", false) + "\">");
      out.println("<PARAM NAME=\"CD_ZBS_EQ\" VALUE=\"" +
                  hash.getString("CD_ZBS_EQ", false) + "\">");
      out.println("<PARAM NAME=\"DS_NIVEL_1_EQ\" VALUE=\"" +
                  hash.getString("DS_NIVEL_1_EQ", false) + "\">");
      out.println("<PARAM NAME=\"DS_NIVEL_2_EQ\" VALUE=\"" +
                  hash.getString("DS_NIVEL_2_EQ", false) + "\">");
      out.println("<PARAM NAME=\"DS_ZBS_EQ\" VALUE=\"" +
                  hash.getString("DS_ZBS_EQ", false) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL_1_AUTORIZACIONES\" VALUE=\"" +
                  hash.getString("CD_NIVEL_1_AUTORIZACIONES", false) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL_2_AUTORIZACIONES\" VALUE=\"" +
                  hash.getString("CD_NIVEL_2_AUTORIZACIONES", false) + "\">");
      out.println("<PARAM NAME=\"FC_ACTUAL\" VALUE=\"" +
                  hash.getString("FC_ACTUAL", true) + "\">");
      out.println("<PARAM NAME=\"IT_FG_VALIDAR\" VALUE=\"" +
                  hash.getString("IT_FG_VALIDAR", true) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL1_DEFECTO\" VALUE=\"" +
                  hash.getString("CD_NIVEL_1_DEFECTO", false) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL2_DEFECTO\" VALUE=\"" +
                  hash.getString("CD_NIVEL_2_DEFECTO", false) + "\">");
      out.println("<PARAM NAME=\"DS_NIVEL1_DEFECTO\" VALUE=\"" +
                  hash.getString("DS_NIVEL_1_DEFECTO", false) + "\">");
      out.println("<PARAM NAME=\"DS_NIVEL2_DEFECTO\" VALUE=\"" +
                  hash.getString("DS_NIVEL_2_DEFECTO", false) + "\">");
      out.println("<PARAM NAME=\"ANYO_DEFECTO\" VALUE=\"" +
                  hash.getString("ANYO_DEFECTO", false) + "\">");
//      out.println("<PARAM NAME=\"IT_FG_RESOLVER_CONFLICTOS\" VALUE=\"" + hash.getString("IT_FG_RESOLVER_CONFLICTOS", false) + "\">");

      //LRG Parametros para compatibilidad con USU de ICM
      out.println("<PARAM NAME=\"IT_USU\" VALUE=\"" +
                  constantes.keyBtnAnadir + ", ," + constantes.keyBtnModificar +
                  ", ,"
                  + constantes.keyBtnBorrar + ", ," + constantes.keyBtnAux +
                  ", ,"
                  + "\">");

      //DSR: Si se trata de un applet de consulta a�adimos par�metro IT_FCCONS
      if (AppletConsulta) {
        out.println("<PARAM NAME=\"IT_FCCONS\" VALUE=\"" +
                    hash.getString("IT_FCCONS", false) + "\">");
      }

      // ARG: Se a�ade el parametro ORIGEN
      out.println("<PARAM NAME=\"ORIGEN\" VALUE=\"" + hash.getString("ORIGEN", false) +
                  "\">");

      // JRM (28/06/01): A�adimos el par�metro LORTAD
      out.println("<PARAM NAME=\"LORTAD\" VALUE=\"" + hash.getString("LORTAD", false) +
                  "\">");

      out.println("</APPLET>");
      //___________

      //Para el caso de mantenimiento de usuarios se carga otro applet
      //que va a ser invisible pero va a ser necesario para asumir funciones
      //que el principal no puede (ser� un capp2.CApp)
      //El applet no tiene parametros height ni width
      switch (new Integer(hash.getString("APPLET", true)).intValue()) {

        case appletUSR:

          // applet
          out.println("<APPLET");
          out.println(" MAYSCRIPT");

          // ZIP
          if (sArchive.equals("")) {
            out.println(" CODEBASE = \"applet/\"");
          }
          else {
            out.println(" CODEBASE = \"zip/\"");
            out.println(" ARCHIVE = \"" + sArchive + "\"");
          }
          out.println(" CODE     = \"" + sClassSegundoApplet + "\"");
          // JRM: establece nombre correcto de applet.
          out.println(" NAME     = \"PsApMantUsu" + "\"");
          //out.println(" NAME     = \"PISTA" + hash.getString("APPLET", true) + "\"");
          out.println(" WIDTH    = \"" + "0" + "\"");
          out.println(" HEIGHT   = \"" + "0" + "\"");
          out.println(" HSPACE   = \"0\"");
          out.println(" VSPACE   = \"0\"");
          out.println(" ALIGN   = \"CENTER\"");
          out.println(">");

          out.println("<PARAM NAME=\"CA\" VALUE=\"" + hash.getString("CA", true) +
                      "\">");
          out.println("<PARAM NAME=\"URL_SERVLET\" VALUE=\"" +
                      hash.getString("URL_SERVLET", true) + "\">");
          out.println("<PARAM NAME=\"FTP_SERVER\" VALUE=\"" +
                      hash.getString("FTP_SERVER", true) + "\">");
          out.println("<PARAM NAME=\"DS_EMAIL\" VALUE=\"" +
                      hash.getString("DS_EMAIL", false) + "\">");
          out.println("<PARAM NAME=\"NIVEL1\" VALUE=\"" +
                      hash.getString("NIVEL1", true) + "\">");
          out.println("<PARAM NAME=\"NIVEL2\" VALUE=\"" +
                      hash.getString("NIVEL2", true) + "\">");
          out.println("<PARAM NAME=\"NIVEL3\" VALUE=\"" +
                      hash.getString("NIVEL3", true) + "\">");
          out.println("<PARAM NAME=\"IDIOMA\" VALUE=\"" +
                      hash.getInteger("IDIOMA", true).toString() + "\">");
          out.println("<PARAM NAME=\"LOGIN\" VALUE=\"" + hash.getString("LOGIN", true) +
                      "\">");
          out.println("<PARAM NAME=\"TSIVE\" VALUE=\"" + sTSIVE + "\">");
          out.println("<PARAM NAME=\"IDIOMA_LOCAL\" VALUE=\"" +
                      hash.getString("IDIOMA_LOCAL", false) + "\">");
          out.println("<PARAM NAME=\"CATALOGO\" VALUE=\"" +
                      Integer.toString(iCat) + "\">");
          out.println("<PARAM NAME=\"CATAUX\" VALUE=\"" +
                      Integer.toString(iCatAux) + "\">");
          out.println("<PARAM NAME=\"CATPRAL\" VALUE=\"" +
                      Integer.toString(iCatPral) + "\">");
          out.println("<PARAM NAME=\"PERFIL\" VALUE=\"" +
                      hash.getString("PERFIL", true) + "\">");
          out.println("<PARAM NAME=\"AYUDA\" VALUE=\"" + "ayuda/" +
                      hash.getInteger("IDIOMA", true).toString() + "/" + sAyuda +
                      ".html\">");
          out.println("<PARAM NAME=\"IT_AUTALTA\" VALUE=\"" +
                      hash.getString("IT_AUTALTA", true) + "\">");
          out.println("<PARAM NAME=\"IT_AUTBAJA\" VALUE=\"" +
                      hash.getString("IT_AUTBAJA", true) + "\">");
          out.println("<PARAM NAME=\"IT_AUTMOD\" VALUE=\"" +
                      hash.getString("IT_AUTMOD", true) + "\">");
          out.println("<PARAM NAME=\"IT_FG_ENFERMO\" VALUE=\"" +
                      hash.getString("IT_FG_ENFERMO", true) + "\">");
          out.println("<PARAM NAME=\"IT_MODULO_3\" VALUE=\"" +
                      hash.getString("IT_MODULO_3", true) + "\">");
          out.println("<PARAM NAME=\"IT_TRAMERO\" VALUE=\"" +
                      hash.getString("IT_TRAMERO", true) + "\">");
          out.println("<PARAM NAME=\"CD_E_NOTIF\" VALUE=\"" +
                      hash.getString("CD_E_NOTIF", false) + "\">");
          out.println("<PARAM NAME=\"DS_E_NOTIF\" VALUE=\"" +
                      hash.getString("DS_E_NOTIF", false) + "\">");
          out.println("<PARAM NAME=\"CD_NIVEL_1_EQ\" VALUE=\"" +
                      hash.getString("CD_NIVEL_1_EQ", false) + "\">");
          out.println("<PARAM NAME=\"CD_NIVEL_2_EQ\" VALUE=\"" +
                      hash.getString("CD_NIVEL_2_EQ", false) + "\">");
          out.println("<PARAM NAME=\"DS_NIVEL_1_EQ\" VALUE=\"" +
                      hash.getString("DS_NIVEL_1_EQ", false) + "\">");
          out.println("<PARAM NAME=\"DS_NIVEL_2_EQ\" VALUE=\"" +
                      hash.getString("DS_NIVEL_2_EQ", false) + "\">");
          out.println("<PARAM NAME=\"CD_NIVEL_1_AUTORIZACIONES\" VALUE=\"" +
                      hash.getString("CD_NIVEL_1_AUTORIZACIONES", false) +
                      "\">");
          out.println("<PARAM NAME=\"CD_NIVEL_2_AUTORIZACIONES\" VALUE=\"" +
                      hash.getString("CD_NIVEL_2_AUTORIZACIONES", false) +
                      "\">");
          out.println("<PARAM NAME=\"FC_ACTUAL\" VALUE=\"" +
                      hash.getString("FC_ACTUAL", true) + "\">");
          out.println("<PARAM NAME=\"IT_FG_VALIDAR\" VALUE=\"" +
                      hash.getString("IT_FG_VALIDAR", true) + "\">");
          out.println("<PARAM NAME=\"CD_NIVEL1_DEFECTO\" VALUE=\"" +
                      hash.getString("CD_NIVEL_1_DEFECTO", false) + "\">");
          out.println("<PARAM NAME=\"CD_NIVEL2_DEFECTO\" VALUE=\"" +
                      hash.getString("CD_NIVEL_2_DEFECTO", false) + "\">");
          out.println("<PARAM NAME=\"DS_NIVEL1_DEFECTO\" VALUE=\"" +
                      hash.getString("DS_NIVEL_1_DEFECTO", false) + "\">");
          out.println("<PARAM NAME=\"DS_NIVEL2_DEFECTO\" VALUE=\"" +
                      hash.getString("DS_NIVEL_2_DEFECTO", false) + "\">");
          out.println("<PARAM NAME=\"ANYO_DEFECTO\" VALUE=\"" +
                      hash.getString("ANYO_DEFECTO", false) + "\">");
//      out.println("<PARAM NAME=\"IT_FG_RESOLVER_CONFLICTOS\" VALUE=\"" + hash.getString("IT_FG_RESOLVER_CONFLICTOS", false) + "\">");

          //LRG Parametros para compatibilidad con USU de ICM
          out.println("<PARAM NAME=\"IT_USU\" VALUE=\"" +
                      constantes.keyBtnAnadir + ", ," +
                      constantes.keyBtnModificar + ", ,"
                      + constantes.keyBtnBorrar + ", ," + constantes.keyBtnAux +
                      ", ,"
                      + "\">");

          // ARG: Se a�ade el parametro ORIGEN
          out.println("<PARAM NAME=\"ORIGEN\" VALUE=\"" +
                      hash.getString("ORIGEN", false) + "\">");

          // JRM (28/06/01): A�adimos par�metro de la LORTAD
          out.println("<PARAM NAME=\"LORTAD\" VALUE=\"" +
                      hash.getString("LORTAD", false) + "\">");

          out.println("</APPLET>");
      }

      //___________

      out.println("</TD></TR>");
      out.println("</TABLE></BODY></HTML>");
      out.flush();
      out.close();

      // envia la excepci�n
    }
    catch (Exception e) {
      e.printStackTrace();

      // abre el stream de salida
      response.setContentType("text/html");
      out = response.getOutputStream();
      out.println("<HTML><HEAD>");
      out.println("<TITLE>PISTA</TITLE>");
      out.println("</HEAD><BODY BGCOLOR=\"#FFFFFF\">");
      out.println(
          "<STRONG><FONT SIZE=2>Petici�n no autorizada.</FONT></STRONG></BODY></HTML>");
      out.flush();
      out.close();
    }
  }

  // CARGA LA TABLA CON LOS DATOS DEL OBJETO REQUEST
  public void loadTable(parametros param, HttpServletRequest request) {
    param.put("APPLET", request.getParameter("APPLET"));
    param.put("IDIOMA", new Integer(request.getParameter("IDIOMA")));
    param.put("LOGIN", request.getParameter("LOGIN"));
    param.put("URL_SERVLET", request.getParameter("URL_SERVLET"));
    param.put("FTP_SERVER", request.getParameter("FTP_SERVER"));
    param.put("DS_EMAIL", request.getParameter("DS_EMAIL"));
    param.put("URL_HTML", request.getParameter("URL_HTML"));
    param.put("PERFIL", request.getParameter("PERFIL"));
    param.put("CA", request.getParameter("CA"));
    param.put("NIVEL1", request.getParameter("NIVEL1"));
    param.put("NIVEL2", request.getParameter("NIVEL2"));
    param.put("NIVEL3", request.getParameter("NIVEL3"));
    param.put("TSIVE", request.getParameter("TSIVE"));
    param.put("IDIOMA_LOCAL", request.getParameter("IDIOMA_LOCAL"));
    param.put("IT_AUTALTA", request.getParameter("IT_AUTALTA"));
    param.put("IT_AUTALTA", request.getParameter("IT_AUTALTA"));
    param.put("IT_AUTBAJA", request.getParameter("IT_AUTBAJA"));
    param.put("IT_AUTMOD", request.getParameter("IT_AUTMOD"));
    param.put("IT_FG_ENFERMO", request.getParameter("IT_FG_ENFERMO"));
    param.put("IT_FG_VALIDAR", request.getParameter("IT_FG_VALIDAR"));
    param.put("IT_MODULO_3", request.getParameter("IT_MODULO_3"));
    param.put("IT_TRAMERO", request.getParameter("IT_TRAMERO"));
    param.put("IT_FG_MNTO", request.getParameter("IT_FG_MNTO"));
    param.put("IT_FG_MNTO_USU", request.getParameter("IT_FG_MNTO_USU"));
    param.put("IT_FG_TCNE", request.getParameter("IT_FG_TCNE"));
    param.put("IT_FG_PROTOS", request.getParameter("IT_FG_PROTOS"));
    param.put("IT_FG_ALARMAS", request.getParameter("IT_FG_ALARMAS"));
    param.put("IT_FG_EXPORT", request.getParameter("IT_FG_EXPORT"));
    param.put("IT_FG_GENALAUTO", request.getParameter("IT_FG_GENALAUTO"));
    param.put("IT_FG_MNOTIFS", request.getParameter("IT_FG_MNOTIFS"));
    param.put("IT_FG_CONSRES", request.getParameter("IT_FG_CONSRES"));
    param.put("CD_E_NOTIF", request.getParameter("CD_E_NOTIF"));
    param.put("DS_E_NOTIF", request.getParameter("DS_E_NOTIF"));
    param.put("CD_NIVEL_1_EQ", request.getParameter("CD_NIVEL_1_EQ"));
    param.put("CD_NIVEL_2_EQ", request.getParameter("CD_NIVEL_2_EQ"));
    param.put("CD_ZBS_EQ", request.getParameter("CD_ZBS_EQ"));
    param.put("DS_NIVEL_1_EQ", request.getParameter("DS_NIVEL_1_EQ"));
    param.put("DS_NIVEL_2_EQ", request.getParameter("DS_NIVEL_2_EQ"));
    param.put("DS_ZBS_EQ", request.getParameter("DS_ZBS_EQ"));
    param.put("CD_NIVEL_1_AUTORIZACIONES",
              request.getParameter("CD_NIVEL_1_AUTORIZACIONES"));
    param.put("CD_NIVEL_2_AUTORIZACIONES",
              request.getParameter("CD_NIVEL_2_AUTORIZACIONES"));
    param.put("FC_ACTUAL", request.getParameter("FC_ACTUAL"));
    param.put("CD_NIVEL_1_DEFECTO", request.getParameter("CD_NIVEL_1_DEFECTO"));
    param.put("CD_NIVEL_2_DEFECTO", request.getParameter("CD_NIVEL_2_DEFECTO"));
    param.put("DS_NIVEL_1_DEFECTO", request.getParameter("DS_NIVEL_1_DEFECTO"));
    param.put("DS_NIVEL_2_DEFECTO", request.getParameter("DS_NIVEL_2_DEFECTO"));
    param.put("ANYO_DEFECTO", request.getParameter("ANYO_DEFECTO"));
    // JRM: IT de fechas de consultas
    param.put("IT_FCCONS", request.getParameter("IT_FCCONS"));
    // JRM: Para el origen
    param.put("ORIGEN", request.getParameter("ORIGEN"));
    // JRM (28/06/01): Par�metro de LORTAD
    param.put("LORTAD", request.getParameter("LORTAD"));
  }
}
