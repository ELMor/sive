package html;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import capp.CApp;
import capp.CMessage;
import panelnivanedo.DlgNivAn;

public class navmenu
    extends CApp {

  // applets
  static final int appletEDO = 1;
  ResourceBundle res;
  static final int appletSISTEMA = 2;
  static final int appletINDICADORES = 3;
  static final int appletMODELOS = 4;
  static final int appletPREG = 5;
  static final int appletLISTAS = 6;
  static final int appletEDO_CNE = 8;
  static final int appletCIE = 9;
  static final int appletPROCESOS = 10;
  static final int appletNIVEL1 = 11;
  static final int appletNIVEL2 = 12;
  static final int appletZBS = 13;
  static final int appletCCAA = 14;
  static final int appletPROV = 15;
  static final int appletESPEC = 16;
  static final int appletTLINEA = 17;
  static final int appletTPREG = 18;
  static final int appletTSIVE = 19;
  static final int appletTVIGI = 20;
  static final int appletMOTBAJA = 21;
  static final int appletNIVASIS = 22;
  static final int appletSEXO = 23;
  //static final int appletTASISTENCIA = 24;
  static final int appletNOTIFICACION = 26;
  static final int appletENFERMO = 27;
  static final int appletC1 = 28;
  static final int appletC2 = 29;
  static final int appletC3 = 30;
  static final int applet711 = 31; //QQ Cobertura por semana
  static final int applet712 = 32; //QQ Cobertura por per�odo
  static final int appletCEN = 33;
  static final int appletEQP = 34;
  static final int appletUSR = 35;
  static final int appletTRV = 36;
  static final int ayudaPP = 37;
  static final int ayudaAcerca = 38;
  static final int appletEPI = 39; //QQ: Servicio de epidemiolog�a
  static final int applet_VEDO = 40; //Applets de volcados:
  static final int applet_VCENTROS = 41;
  static final int applet_VCASOS = 42;
  static final int applet_VNOTIF = 43;
  static final int applet_GENANO = 44; //Generaci�n del a�o epidemiol�gico.
  static final int applet_VCASOSI = 45; //Volcado Casos Individuales sin Preguntas (8.4)
  static final int applet_ENVNUM = 46; //Envio de casos num�ricos
  static final int applet_CEdoPac = 47; //Consulta de casos EDO de un paciente.
  static final int applet_ALAUTO = 48; //Generaci�n autom�tica de alarmas;
  static final int applet_consdemres = 49;
  static final int applet_infnoteq = 50;
  static final int applet_infedonoma = 51;
  static final int applet_consconsenf = 52;
  static final int applet_distcasnivres = 53;
  static final int applet_disnotasis = 54;
  static final int applet_infedovarnot = 55;
  static final int applet_conscasoper = 73; // Casos notificados por un periodo clasificado por centro/notificador
  static final int applet_VALARMA = 56;
  static final int applet_CONSALAUTO = 57;
  static final int applet_CONSALAUTO_PER = 58;
  static final int applet_CASOSTASAS_AREA = 59;
  static final int applet_CASOSTASASCIENAREA = 72; // Casos o tasas por 100000 habitantes
  static final int applet_ENFERMO_EDO = 60;
  static final int appletDIAGNOSTICO = 61;
  static final int applet_CARGA_T = 62; //Carga de Tablas
  static final int applet_DESCARGA_T = 63; //Descarga de Tablas
  static final int applet_CARGA_MCNE = 64; //Carga Modelo del CNE
  static final int applet_DESCARGA_M = 65; //Descarga de Modelos
  static final int applet_ENVUR = 66; //Env�os urgentes
  static final int applet_ENVRESSEM = 67; //Env�o de resumen semanal
  static final int appletMANTENIMIENTO_CASOS_EDO = 71;
  static final int applet_grafcomenf = 74;
  static final int applet_grafcomphist = 75;
  static final int applet_LIMPIAR_NOTI = 77;
  static final int applet_ENVOTRCOM = 78;

  // JRM: Dos nuevas opciones de consulta: consulta de �ndices y env�o de cartas
  // con retrasos
  protected static final int applet_INDICES = 100;
  protected static final int applet_RETRASOS = 101;

  // brotes

  static final int applet_ALERTA = 80;
  static final int applet_BROTES = 81;
  static final int applet_MNTO_GRUPO_BROTE = 82;
  static final int applet_MNTO_TIPO_NOTIFICADOR = 83;
  static final int applet_MNTO_MECANISMOS = 84;
  static final int applet_MNTO_COLECTIVOS = 85;
  static final int applet_MNTO_SITUACION = 86;
  static final int applet_MNTO_TIPO_BROTE = 87;

  static final int appletMODELOS_BROTE = 88;
  static final int appletPREG_BROTE = 89;

  static final int applet_SEL_ZONIFICACION = 2000;
  //static final int applet_INICIO_SESION = 2001;

  // m�dulo 1
  static final int applet_CATA = 68; // Acceso al cat�logo de tablas
  static final int applet_DICCIO = 69; // Acceso al Diccionario Central de Protocolos
  static final int applet_FTP = 70; // M�dulo FTP

  // p�ginas de ayuda
  static final int ayuda_AD = 1000;
  static final int ayuda_PP = 1001;

  // par�metros de sesion
  public parametros hash = null;

  // MEN�
  MenuPanel mPanel = new MenuPanel();
  MenuBar menuBar = new MenuBar();

  // opciones del men�
  // escribe las etiquetas
  Menu m1;
  Menu m2;
  Menu m4;
  Menu m5;
  Menu m6;
  Menu m7;
  Menu m8;
  Menu m9;

  // notificaciones
  MenuItem mi1_1;
  MenuItem mi1_4;
  MenuItem mi1_5;
  MenuItem mi1_6;
  MenuItem mi1_7;

  // brotes
  // ARG
  //Menu m1_2;
  //MenuItem mi1_21;
  //MenuItem mi1_22;

  // consultas
  MenuItem mi2_1;
  MenuItem mi2_2;
  MenuItem mi2_4;
  MenuItem mi2_5;
  MenuItem mi2_6;
  MenuItem mi2_7;
  MenuItem mi2_8;
  MenuItem mi2_9;
  MenuItem mi2_10;
  MenuItem mi2_11;
  MenuItem mi2_12;
  MenuItem mi2_13;
  MenuItem mi2_14;
  MenuItem mi2_15;
  MenuItem mi2_16; // E
  MenuItem mi2_17; // E
  MenuItem mi2_18; // E
  MenuItem mi2_19;
  MenuItem mi2_20;
  MenuItem mi2_21;

  Menu m2_14;
  Menu m2_15;
  MenuItem mi2_15_1;
  MenuItem mi2_15_2;
  MenuItem mi2_15_3;

  // parametrizaci�n
  Menu m4_2;
  Menu m4_3;

  // alarmas
  MenuItem mi4_2_0;
  MenuItem mi4_2_1;

  // modelos
  MenuItem mi4_3_1;
  MenuItem mi4_3_2;
  MenuItem mi4_3_3;
  MenuItem mi4_3_4;
  MenuItem mi4_3_5;
  Menu m4_31;
  // ARG
  //Menu m4_33;
  //MenuItem mi4_3_6;
  //MenuItem mi4_3_7;

  // mantenimiento
  MenuItem mi5_0;
  MenuItem mi5_00;
  MenuItem mi5_1;
  MenuItem mi5_2;
  MenuItem mi5_3;
  MenuItem mi5_30;
  Menu m5_4;

  // tablas de c�digos
  Menu m5_4_1;
  Menu m5_4_2;
  Menu m5_4_3;
  Menu m5_4_4;
  MenuItem mi5_4_1;
  MenuItem mi5_4_2;
  MenuItem mi5_4_3;
  MenuItem mi5_4_4;
  MenuItem mi5_4_5;
  MenuItem mi5_4_6;
  MenuItem mi5_4_7;
  MenuItem mi5_4_8;
  MenuItem mi5_4_9;
  MenuItem mi5_4_10;
  MenuItem mi5_4_13;
  MenuItem mi5_4_14;
  MenuItem mi5_4_15;
  MenuItem mi5_4_16;
  MenuItem mi5_4_17;
  MenuItem mi5_4_18;
  MenuItem mi5_4_19;
  MenuItem mi5_4_20;
  MenuItem mi5_4_21;

  // ARG
  //MenuItem mi5_4_41;
  MenuItem mi5_4_42;
  MenuItem mi5_4_43;
  MenuItem mi5_4_44;
  MenuItem mi5_4_45;
  // ARG
  //MenuItem mi5_4_46;

  // Trasvase de datos
  MenuItem mi6_1;
  MenuItem mi6_2;
  MenuItem mi6_3;
  MenuItem mi6_35;
  MenuItem mi6_4;
  MenuItem mi6_5;
  MenuItem mi6_6;
  MenuItem mi6_7;

  //Env�os:
  MenuItem mi8_1;
  MenuItem mi8_2;
  MenuItem mi8_3;
  MenuItem mi8_4;

  // Opciones
  MenuItem mi9_1;

  // Ayudas
  MenuItem mi7_1;
  MenuItem mi7_2;

  // escuchador
  menuActionListener escuchador = new menuActionListener(this);

  public void start() {
  }

  public void init() {
    super.init();

    res = ResourceBundle.getBundle("html.Res" + getIdioma());

    // escribe las etiquetas
    m1 = new Menu(res.getString("menu.1"));
    m2 = new Menu(res.getString("menu.2"));
    m4 = new Menu(res.getString("menu.3"));
    m5 = new Menu(res.getString("menu.4"));
    m6 = new Menu(res.getString("menu.5"));
    m7 = new Menu(res.getString("menu.6"));
    m8 = new Menu(res.getString("menu.7"));
    m9 = new Menu(res.getString("menu.8"));

    // notificaciones EDO
    mi1_1 = new MenuItem(res.getString("menu.9"));
    // ARG
    //m1_2 = new Menu(res.getString("menu.10"));
    mi1_4 = new MenuItem(res.getString("menu.11"));
    mi1_5 = new MenuItem(res.getString("menu.12"));
    mi1_6 = new MenuItem(res.getString("menu.13"));

    // notificaciones de brotes
    // ARG
    //mi1_21 = new MenuItem(res.getString("menu.14"));
    //mi1_22 = new MenuItem(res.getString("menu.15"));

    // consultas
    mi2_1 = new MenuItem(res.getString("menu.16"));
    mi2_2 = new MenuItem(res.getString("menu.17"));
    mi2_4 = new MenuItem(res.getString("menu.18"));
    mi2_5 = new MenuItem(res.getString("menu.19"));
    mi2_6 = new MenuItem(res.getString("menu.20"));
    mi2_7 = new MenuItem(res.getString("menu.21"));
    mi2_8 = new MenuItem(res.getString("menu.22"));
    mi2_9 = new MenuItem(res.getString("menu.23"));
    mi2_10 = new MenuItem(res.getString("menu.24"));
    mi2_11 = new MenuItem(res.getString("menu.25"));
    mi2_12 = new MenuItem(res.getString("menu.26"));
    mi2_13 = new MenuItem(res.getString("menu.27"));
    mi2_14 = new MenuItem(res.getString("menu.28"));
    mi2_15 = new MenuItem(res.getString("menu.29"));
    mi2_16 = new MenuItem(res.getString("menu.30")); // E
    mi2_17 = new MenuItem(res.getString("menu.31")); // E
    mi2_18 = new MenuItem(res.getString("menu.32")); // E
    mi2_19 = new MenuItem(res.getString("menu.33")); // E

    // JRM
    mi2_20 = new MenuItem(res.getString("menu.100"));
    mi2_21 = new MenuItem(res.getString("menu.101"));

    m2_14 = new Menu(res.getString("menu.34"));
    m2_15 = new Menu(res.getString("menu.35"));
    mi2_15_1 = new MenuItem(res.getString("menu.36"));
    mi2_15_2 = new MenuItem(res.getString("menu.37"));
    mi2_15_3 = new MenuItem(res.getString("menu.38"));

    // parametrizaci�n
    m4_2 = new Menu(res.getString("menu.39"));
    m4_3 = new Menu(res.getString("menu.40"));

    // alarmas
    mi4_2_0 = new MenuItem(res.getString("menu.41"));
    mi4_2_1 = new MenuItem(res.getString("menu.42"));

    // modelos
    mi4_3_1 = new MenuItem(res.getString("menu.43"));
    mi4_3_2 = new MenuItem(res.getString("menu.44"));
    mi4_3_3 = new MenuItem(res.getString("menu.45"));
    mi4_3_4 = new MenuItem(res.getString("menu.46"));
    mi4_3_5 = new MenuItem(res.getString("menu.47"));
    m4_31 = new Menu(res.getString("menu.48"));
    // ARG
    //m4_33 = new Menu(res.getString("menu.10"));
    //mi4_3_6 = new MenuItem(res.getString("menu.43"));
    //mi4_3_7 = new MenuItem(res.getString("menu.45"));

    // mantenimiento
    mi5_0 = new MenuItem(res.getString("menu.49"));
    mi5_00 = new MenuItem(res.getString("menu.50"));
    mi5_1 = new MenuItem(res.getString("menu.51"));
    mi5_2 = new MenuItem(res.getString("menu.52"));
    mi5_3 = new MenuItem(res.getString("menu.53"));
    mi5_30 = new MenuItem(res.getString("menu.54"));
    m5_4 = new Menu(res.getString("menu.55"));

    // tablas de c�digos
    m5_4_1 = new Menu(res.getString("menu.56"));
    m5_4_2 = new Menu(res.getString("menu.57"));
    m5_4_3 = new Menu(res.getString("menu.58"));
    mi5_4_1 = new MenuItem(res.getString("menu.59"));
    mi5_4_2 = new MenuItem(res.getString("menu.60"));
    mi5_4_3 = new MenuItem(res.getString("menu.61"));
    mi5_4_4 = new MenuItem(res.getString("menu.62"));
    mi5_4_5 = new MenuItem(res.getString("menu.63"));
    mi5_4_6 = new MenuItem(res.getString("menu.64"));
    mi5_4_7 = new MenuItem(res.getString("menu.65"));
    mi5_4_8 = new MenuItem(res.getString("menu.66"));
    mi5_4_9 = new MenuItem(res.getString("menu.67"));
    mi5_4_10 = new MenuItem(res.getString("menu.68"));
    mi5_4_13 = new MenuItem("N1");
    mi5_4_14 = new MenuItem("N2");
    mi5_4_15 = new MenuItem("N3");
    mi5_4_16 = new MenuItem(res.getString("menu.69"));
    mi5_4_17 = new MenuItem(res.getString("menu.70"));
    mi5_4_18 = new MenuItem(res.getString("menu.71"));
    mi5_4_19 = new MenuItem(res.getString("menu.72"));
    mi5_4_20 = new MenuItem(res.getString("menu.73"));
    mi5_4_21 = new MenuItem(res.getString("menu.74"));
    // ARG
    //mi5_4_41 = new MenuItem(res.getString("menu.75"));
    mi5_4_42 = new MenuItem(res.getString("menu.76"));
    mi5_4_43 = new MenuItem(res.getString("menu.77"));
    mi5_4_44 = new MenuItem(res.getString("menu.78"));

    // JRM2: Quito la opcion Situaci�n Alerta del men�
    //mi5_4_45 = new MenuItem(res.getString("menu.79"));

    // ARG
    //mi5_4_46 = new MenuItem(res.getString("menu.80"));

    // Trasvase de datos
    mi6_1 = new MenuItem(res.getString("menu.81"));
    mi6_2 = new MenuItem(res.getString("menu.82"));
    mi6_3 = new MenuItem(res.getString("menu.83"));
    mi6_35 = new MenuItem(res.getString("menu.84"));
    mi6_4 = new MenuItem(res.getString("menu.85"));
    mi6_5 = new MenuItem(res.getString("menu.86"));
    mi6_6 = new MenuItem(res.getString("menu.87"));
    mi6_7 = new MenuItem(res.getString("menu.88"));

    //Env�os:
    mi8_1 = new MenuItem(res.getString("menu.89"));
    mi8_2 = new MenuItem(res.getString("menu.90"));
    mi8_3 = new MenuItem(res.getString("menu.91"));
    mi8_4 = new MenuItem(res.getString("menu.92"));

    // Opciones
    mi9_1 = new MenuItem(res.getString("menu.93"));

    // Ayudas
    mi7_1 = new MenuItem(res.getString("menu.94"));
    mi7_2 = new MenuItem(res.getString("menu.95"));

    // notificaciones
    m1.add(mi1_1);
    mi1_1.setName(Integer.toString(this.appletNOTIFICACION));
    m1.add(mi1_4);
    mi1_4.setName(Integer.toString(this.appletENFERMO));
    m1.add(mi1_5);
    mi1_5.setName(Integer.toString(this.appletMANTENIMIENTO_CASOS_EDO));
    m1.add(mi1_6);
    mi1_6.setName(Integer.toString(this.applet_LIMPIAR_NOTI));

    // ARG
    //m1.addSeparator();
    //m1.add(m1_2);

    // brotes
    // ARG
    //m1_2.add(mi1_21);
    //mi1_21.setName(Integer.toString(this.applet_ALERTA));
    //m1_2.add(mi1_22);
    //mi1_22.setName(Integer.toString(this.applet_BROTES));

    // consultas
    m2.add(mi2_1);
    mi2_1.setName(Integer.toString(this.appletC1));
    m2.add(mi2_2);
    mi2_2.setName(Integer.toString(this.appletC2));
    m2.addSeparator();
    m2.add(mi2_13);
    mi2_13.setName(Integer.toString(this.applet_CONSALAUTO));
    m2.add(mi2_14);
    mi2_14.setName(Integer.toString(this.applet_CONSALAUTO_PER));
    m2.addSeparator();
    m2.add(mi2_6);
    mi2_6.setName(Integer.toString(this.applet_consdemres));
    m2.add(mi2_7);
    mi2_7.setName(Integer.toString(this.applet_infnoteq));
    m2.add(mi2_4);
    mi2_4.setName(Integer.toString(this.applet711));
    m2.add(mi2_5);
    mi2_5.setName(Integer.toString(this.applet712));
    m2.addSeparator();
    m2.add(mi2_10);
    mi2_10.setName(Integer.toString(this.applet_distcasnivres));
    m2.add(mi2_11);
    mi2_11.setName(Integer.toString(this.applet_disnotasis));

    m2.add(mi2_12);
    mi2_12.setName(Integer.toString(this.applet_infedovarnot));

    m2.add(mi2_17); // E
    mi2_17.setName(Integer.toString(this.applet_conscasoper)); // E

    m2.addSeparator();

    m2.add(mi2_16); // E
    mi2_16.setName(Integer.toString(this.applet_CASOSTASASCIENAREA)); // E

    m2.add(mi2_15);
    mi2_15.setName(Integer.toString(this.applet_CASOSTASAS_AREA));

    m2.addSeparator();
    m2.add(mi2_18); // E
    mi2_18.setName(Integer.toString(this.applet_grafcomenf)); // E
    m2.add(mi2_19); // E
    mi2_19.setName(Integer.toString(this.applet_grafcomphist)); // E

    m2.addSeparator();
    m2.add(m2_14);
    m2_14.add(mi2_8);
    mi2_8.setName(Integer.toString(this.applet_infedonoma));
    m2_14.add(mi2_9);
    mi2_9.setName(Integer.toString(this.applet_consconsenf));
    m2.addSeparator();
    //Consultas restringidas
    m2.add(m2_15);
    m2_15.add(mi2_15_1);
    mi2_15_1.setName(Integer.toString(this.appletC3));
    m2_15.add(mi2_15_2);
    mi2_15_2.setName(Integer.toString(this.applet_CEdoPac));
    m2_15.add(mi2_15_3);
    mi2_15_3.setName(Integer.toString(this.applet_ENFERMO_EDO));

    // JRM: Consultas de �ndices y cartas con retrasos; Solo en CyL
    /*
         m2.addSeparator();
         m2.add(mi2_20); // E
         mi2_20.setName(Integer.toString(this.applet_INDICES)); // E
         m2.add(mi2_21); // E
         mi2_21.setName(Integer.toString(this.applet_RETRASOS)); // E
     */

    //---

    // parametrizaci�n
    m4.add(m4_2);
    m4.add(m4_3);

    // modelos
    m4_3.add(m4_31);
    m4_31.add(mi4_3_1);
    mi4_3_1.setName(Integer.toString(this.appletMODELOS));
    m4_31.add(mi4_3_3);
    mi4_3_3.setName(Integer.toString(this.appletPREG));

    // ARG
    //m4_3.add(m4_33);
    //m4_33.add(mi4_3_6);
    //mi4_3_6.setName(Integer.toString(this.appletMODELOS_BROTE));
    //m4_33.add(mi4_3_7);
    //mi4_3_7.setName(Integer.toString(this.appletPREG_BROTE));

    m4_3.add(mi4_3_2);
    mi4_3_2.setName(Integer.toString(this.appletLISTAS));
    m4_3.addSeparator();
    m4_3.add(mi4_3_4);
    mi4_3_4.setName(Integer.toString(this.applet_CARGA_MCNE));
    m4_3.add(mi4_3_5);
    mi4_3_5.setName(Integer.toString(this.applet_DICCIO));

    // alarmas
    m4_2.add(mi4_2_0);
    mi4_2_0.setName(Integer.toString(this.applet_ALAUTO));
    m4_2.addSeparator();
    m4_2.add(mi4_2_1);
    mi4_2_1.setName(Integer.toString(this.appletINDICADORES));

    // mantenimiento
    m5.add(mi5_0);
    mi5_0.setName(Integer.toString(this.appletSISTEMA));
    m5.add(mi5_00);
    mi5_00.setName(Integer.toString(this.appletEPI));
    m5.add(mi5_3);
    mi5_3.setName(Integer.toString(this.appletUSR));
    m5.addSeparator();
    m5.add(mi5_1);
    mi5_1.setName(Integer.toString(this.appletCEN));
    m5.add(mi5_2);
    mi5_2.setName(Integer.toString(this.appletEQP));
    m5.addSeparator();
    m5.add(mi5_30);
    mi5_30.setName(Integer.toString(this.applet_GENANO));
    m5.addSeparator();
    m5.add(m5_4);

    // tablas de c�digos
    // zonas
    m5_4.add(m5_4_1);
    m5_4_1.add(mi5_4_1);
    mi5_4_1.setName(Integer.toString(this.appletCCAA));
    m5_4_1.add(mi5_4_16);
    mi5_4_16.setName(Integer.toString(this.appletPROV));
    m5_4_1.add(mi5_4_13);
    mi5_4_13.setName(Integer.toString(this.appletNIVEL1));
    m5_4_1.add(mi5_4_14);
    mi5_4_14.setName(Integer.toString(this.appletNIVEL2));
    m5_4_1.add(mi5_4_15);
    mi5_4_15.setName(Integer.toString(this.appletZBS));
    m5_4.addSeparator();

    // enfermedades
    m5_4.add(m5_4_2);
    m5_4_2.add(mi5_4_17);
    mi5_4_17.setName(Integer.toString(this.appletCIE));
    m5_4_2.add(mi5_4_10);
    mi5_4_10.setName(Integer.toString(this.appletPROCESOS));
    m5_4_2.add(mi5_4_2);
    mi5_4_2.setName(Integer.toString(this.appletEDO_CNE));
    m5_4_2.add(mi5_4_18);
    mi5_4_18.setName(Integer.toString(this.appletEDO));
    m5_4.addSeparator();

    // miscellaneous
    m5_4.add(mi5_4_5);
    mi5_4_5.setName(Integer.toString(this.appletTSIVE));
    m5_4.add(mi5_4_3);
    mi5_4_3.setName(Integer.toString(this.appletTLINEA));
    m5_4.add(mi5_4_4);
    mi5_4_4.setName(Integer.toString(this.appletTPREG));
    m5_4.add(mi5_4_6);
    mi5_4_6.setName(Integer.toString(this.appletTVIGI));
    m5_4.add(mi5_4_7);
    mi5_4_7.setName(Integer.toString(this.appletMOTBAJA));
    m5_4.add(mi5_4_8);
    mi5_4_8.setName(Integer.toString(this.appletNIVASIS));
    m5_4.add(mi5_4_9);
    mi5_4_9.setName(Integer.toString(this.appletSEXO));
    m5_4.add(mi5_4_19);
    mi5_4_19.setName(Integer.toString(this.appletDIAGNOSTICO));
    m5_4.addSeparator();

    // ARG
    //m5_4.add(mi5_4_41);
    //mi5_4_41.setName(Integer.toString(this.applet_MNTO_GRUPO_BROTE));
    m5_4.add(mi5_4_42);
    mi5_4_42.setName(Integer.toString(this.applet_MNTO_TIPO_NOTIFICADOR));
    m5_4.add(mi5_4_43);
    mi5_4_43.setName(Integer.toString(this.applet_MNTO_MECANISMOS));
    m5_4.add(mi5_4_44);
    mi5_4_44.setName(Integer.toString(this.applet_MNTO_COLECTIVOS));
    //JRM2
    //m5_4.add(mi5_4_45);
    //mi5_4_45.setName(Integer.toString(this.applet_MNTO_SITUACION));

    // ARG
    //m5_4.add(mi5_4_46);
    //mi5_4_46.setName(Integer.toString(this.applet_MNTO_TIPO_BROTE));
    m5_4.addSeparator();

    m5_4.add(m5_4_3);
    m5_4_3.add(mi5_4_20);
    mi5_4_20.setName(Integer.toString(this.applet_CARGA_T));
    m5_4_3.add(mi5_4_21);
    mi5_4_21.setName(Integer.toString(this.applet_CATA));

    // Trasvase de Datos:
    m6.add(mi6_1);
    mi6_1.setName(Integer.toString(this.applet_VEDO));
    m6.add(mi6_2);
    mi6_2.setName(Integer.toString(this.applet_VCENTROS));
    m6.add(mi6_3);
    mi6_3.setName(Integer.toString(this.applet_VCASOS));
    m6.add(mi6_35);
    mi6_35.setName(Integer.toString(this.applet_VCASOSI));
    m6.add(mi6_4);
    mi6_4.setName(Integer.toString(this.applet_VNOTIF));
    m6.add(mi6_5);
    mi6_5.setName(Integer.toString(this.applet_VALARMA));
    m6.addSeparator();
    m6.add(mi6_6);
    mi6_6.setName(Integer.toString(this.applet_DESCARGA_T));
    m6.add(mi6_7);
    mi6_7.setName(Integer.toString(this.applet_DESCARGA_M));

    //Env�os:
    m8.add(mi8_1);
    mi8_1.setName(Integer.toString(this.applet_ENVRESSEM));
    m8.addSeparator();
    m8.add(mi8_2);
    mi8_2.setName(Integer.toString(this.applet_ENVUR));
    m8.addSeparator();
    m8.add(mi8_3);
    mi8_3.setName(Integer.toString(this.applet_ENVOTRCOM));
    m8.addSeparator();
    m8.add(mi8_4);
    mi8_4.setName(Integer.toString(this.applet_FTP));

    // Opciones
    m9.add(mi9_1);
    mi9_1.setName(Integer.toString(this.applet_SEL_ZONIFICACION));

    // Ayudas
    m7.add(mi7_1);
    mi7_1.setName(Integer.toString(this.ayuda_PP));
    m7.addSeparator();
    m7.add(mi7_2);
    mi7_2.setName(Integer.toString(this.ayuda_AD));

    // escuchadores de eventos
    mi1_1.addActionListener(escuchador);
    // ARG
    //mi1_21.addActionListener(escuchador);
    //mi1_22.addActionListener(escuchador);
    mi1_4.addActionListener(escuchador);
    mi1_5.addActionListener(escuchador);
    mi1_6.addActionListener(escuchador);
    mi2_1.addActionListener(escuchador);
    mi2_10.addActionListener(escuchador);
    mi2_11.addActionListener(escuchador);
    mi2_12.addActionListener(escuchador);
    mi2_13.addActionListener(escuchador);
    mi2_14.addActionListener(escuchador);
    mi2_15.addActionListener(escuchador);
    mi2_15_1.addActionListener(escuchador);
    mi2_15_2.addActionListener(escuchador);
    mi2_15_3.addActionListener(escuchador);
    mi2_16.addActionListener(escuchador);
    mi2_17.addActionListener(escuchador);
    mi2_18.addActionListener(escuchador);
    mi2_19.addActionListener(escuchador);
    mi2_20.addActionListener(escuchador);
    mi2_21.addActionListener(escuchador);

    mi2_2.addActionListener(escuchador);
    mi2_4.addActionListener(escuchador);
    mi2_5.addActionListener(escuchador);
    mi2_6.addActionListener(escuchador);
    mi2_7.addActionListener(escuchador);
    mi2_8.addActionListener(escuchador);
    mi2_9.addActionListener(escuchador);
    mi4_2_0.addActionListener(escuchador);
    mi4_2_1.addActionListener(escuchador);
    mi4_3_1.addActionListener(escuchador);
    mi4_3_2.addActionListener(escuchador);
    mi4_3_3.addActionListener(escuchador);
    mi4_3_4.addActionListener(escuchador);
    mi4_3_5.addActionListener(escuchador);
    // ARG
    //mi4_3_6.addActionListener(escuchador);
    //mi4_3_7.addActionListener(escuchador);
    mi5_0.addActionListener(escuchador);
    mi5_00.addActionListener(escuchador);
    mi5_1.addActionListener(escuchador);
    mi5_2.addActionListener(escuchador);
    mi5_3.addActionListener(escuchador);
    mi5_30.addActionListener(escuchador);
    mi5_4_1.addActionListener(escuchador);
    mi5_4_10.addActionListener(escuchador);
    mi5_4_13.addActionListener(escuchador);
    mi5_4_14.addActionListener(escuchador);
    mi5_4_15.addActionListener(escuchador);
    mi5_4_16.addActionListener(escuchador);
    mi5_4_17.addActionListener(escuchador);
    mi5_4_18.addActionListener(escuchador);
    mi5_4_19.addActionListener(escuchador);
    mi5_4_2.addActionListener(escuchador);
    mi5_4_20.addActionListener(escuchador);
    mi5_4_21.addActionListener(escuchador);
    mi5_4_3.addActionListener(escuchador);
    mi5_4_4.addActionListener(escuchador);

    // ARG
    //mi5_4_41.addActionListener(escuchador);
    mi5_4_42.addActionListener(escuchador);
    mi5_4_43.addActionListener(escuchador);
    mi5_4_44.addActionListener(escuchador);
    // JRM2
    //mi5_4_45.addActionListener(escuchador);
    // ARG
    //mi5_4_46.addActionListener(escuchador);

    mi5_4_5.addActionListener(escuchador);
    mi5_4_6.addActionListener(escuchador);
    mi5_4_7.addActionListener(escuchador);
    mi5_4_8.addActionListener(escuchador);
    mi5_4_9.addActionListener(escuchador);
    mi6_1.addActionListener(escuchador);
    mi6_2.addActionListener(escuchador);
    mi6_3.addActionListener(escuchador);
    mi6_35.addActionListener(escuchador);
    mi6_4.addActionListener(escuchador);
    mi6_5.addActionListener(escuchador);
    mi6_6.addActionListener(escuchador);
    mi6_7.addActionListener(escuchador);
    mi7_1.addActionListener(escuchador);
    mi7_2.addActionListener(escuchador);
    mi8_1.addActionListener(escuchador);
    mi8_2.addActionListener(escuchador);
    mi8_3.addActionListener(escuchador);
    mi8_4.addActionListener(escuchador);
    mi9_1.addActionListener(escuchador);

    // opciones del menu
    menuBar.add(m1);
    menuBar.add(m2);
    menuBar.add(m6);
    menuBar.add(m8);
    menuBar.add(m4);
    menuBar.add(m5);
    menuBar.add(m9);
    menuBar.add(m7);

    // iniciar sesion
    iniciarSesion();
  }

  // activa los permisos de acceso de una sesion
  protected void iniciarSesion() {
    try {

      // solicita inicio de sesi�n
      login dLogin = new login(this, getURL(), res.getString("login.Title"));
      dLogin.show();
      if (dLogin.cancelada()) {
        return;
      }
      hash = null;
      hash = dLogin.getHash();
      // JRM: Metemos en el hash el par�metro de la consulta
      hash.put("IT_FCCONS", getParameter("IT_FCCONS"));
      // JRM: Metemos en el hash el origen
      hash.put("ORIGEN", getParameter("ORIGEN"));
      // JRM (26/02/01): Metemos en el hash si se aplica la LORTAD
      // ARG: (6/9/2001) Ahora el parametro LORTAD se lee de la BD
//    hash.put("LORTAD", getParameter("LORTAD"));

      // establece descripciones
      mi5_4_13.setLabel(hash.getString("NIVEL1", true));
      mi5_4_14.setLabel(hash.getString("NIVEL2", true));
      mi5_4_15.setLabel(hash.getString("NIVEL3", true));

      // gestiona perfil de usuario

      //1� Mantenimientos
      if (hash.getString("IT_FG_MNTO", true).equals("S") ||
          hash.getString("IT_FG_MNTO_USU", true).equals("S") ||
          hash.getString("IT_FG_MNOTIFS", true).equals("S")) {
        //Habilitar opci�n primer nivel Mantenimiento
        m5.setEnabled(true);
        //Habiitar/deshabilitar opciones de segundo nivel:
        mi5_0.setEnabled(false);
        mi5_00.setEnabled(false);
        mi5_1.setEnabled(false);
        mi5_2.setEnabled(false);
        mi5_3.setEnabled(false);
        mi5_30.setEnabled(false);
        m5_4.setEnabled(false);
        if (hash.getString("IT_FG_MNTO_USU", true).equals("S")) {
          mi5_3.setEnabled(true);
        }
        if (hash.getString("IT_FG_MNOTIFS", true).equals("S")) {
          mi5_1.setEnabled(true);
          mi5_2.setEnabled(true);
        }
        if (hash.getString("IT_FG_MNTO", true).equals("S")) {
          mi5_0.setEnabled(true); //Configuraci�n CA
          mi5_00.setEnabled(true); //Servicio Epidemiol�gico
          mi5_30.setEnabled(true); //Generar a�o epidemiol�gico
          m5_4.setEnabled(true); //Tablas
        }

      }
      else {
        //No tiene ning�n permiso de mantenimiento.
        m5.setEnabled(false);

      }

      //2�: Parametrizaci�n:
      if (hash.getString("IT_FG_PROTOS", true).equals("S") ||
          hash.getString("IT_FG_ALARMAS", true).equals("S") ||
          hash.getString("IT_FG_GENALAUTO", true).equals("S")) {
        //Alg�n permiso de parametrizaci�n; habilitarlo:
        m4_2.setEnabled(false);
        m4_3.setEnabled(false);
        //Modelos
        if (hash.getString("IT_FG_PROTOS", true).equals("S")) {
//          m4_2.setEnabled(true);
          m4_3.setEnabled(true); //LRG
        }
        //Alarmas
        if ( (hash.getString("IT_FG_ALARMAS", true).equals("S")) ||
            (hash.getString("IT_FG_GENALAUTO", true).equals("S"))) {
          // m4_3.setEnabled(true);
          m4_2.setEnabled(true); //LRG

          mi4_2_0.setEnabled(false);
          mi4_2_1.setEnabled(false);
          if (hash.getString("IT_FG_ALARMAS", true).equals("S")) {
            mi4_2_1.setEnabled(true);
          }
          if (hash.getString("IT_FG_GENALAUTO", true).equals("S")) {
            mi4_2_0.setEnabled(true);
          }
        }
      }
      else {
        //Ning�n permiso de parametrizaci�n
        m4.setEnabled(false);
      }

      //3� Exportaciones:
      if (hash.getString("IT_FG_EXPORT", true).equals("S")) {
        m6.setEnabled(true);
      }
      else {
        m6.setEnabled(true);
      }

      //4� Env�os:
      if (hash.getString("IT_FG_TCNE", true).equals("S")) {
        m8.setEnabled(true);
      }
      else {
        m8.setEnabled(false);
      }

      /* if(hash.getString("PERFIL",true).equals("1") || hash.getString("PERFIL",true).equals("5"))
       {
         //AIC los perfiles 1 y 5 no pueden generar alarmas
         m4_2.setEnabled(false);
       } */

      // Perfil 5
      if (hash.getString("PERFIL", true).equals("5")) {
        mi1_4.setEnabled(false);
        mi1_5.setEnabled(false);
        mi1_6.setEnabled(false);
        // ARG
        //mi1_22.setEnabled(false);
        m2.setEnabled(false);
        m4.setEnabled(false);
        m5.setEnabled(false);
        m6.setEnabled(false);
        m8.setEnabled(false);
        m9.setEnabled(false);
      }
      else {
        mi1_5.setEnabled(true);
        m9.setEnabled(true);
      }

      // Perfiles 5 y 1
      if (hash.getString("PERFIL", true).equals("1") ||
          hash.getString("PERFIL", true).equals("5")) {
        //AIC los perfiles 1 y 5 no pueden generar alarmas
        m4_2.setEnabled(false);

        mi1_6.setEnabled(false);
      }
      else {
        mi1_6.setEnabled(true);
      }

    }
    catch (Exception e) {
      // desactiva las opciones de menu
      for (int h = 0; h < menuBar.getMenuCount(); h++) {
        menuBar.getMenu(h).setEnabled(false);
      }

    }
    removeAll();

    mPanel.setMenuBar(menuBar);
    add("North", mPanel);
    validate();
  }

  // selecciona notificacion
  public void seleccionarZonificacion() {
    String sAnyo;

    try {
      //LRG
      //Se meten en applet par�metros de usario necesarios
      //poeder determinar niveles por defecto teniendo en cuenta autorizaciones
      sLogin = hash.getString("LOGIN", false);
      iPerfil = Integer.parseInt(hash.getString("PERFIL", false));

      if (hash.getString("DS_NIVEL_2_DEFECTO", false).equals("")) {
        sAnyo = hash.getString("FC_ACTUAL", true);
        sAnyo = sAnyo.substring(6, 10);
      }
      else {
        sAnyo = hash.getString("ANYO_DEFECTO", false);
      }

      DlgNivAn zbs = new DlgNivAn(this, sAnyo,
                                  hash.getString("CD_NIVEL_1_DEFECTO", false),
                                  hash.getString("DS_NIVEL_1_DEFECTO", false),
                                  hash.getString("CD_NIVEL_2_DEFECTO", false),
                                  hash.getString("DS_NIVEL_2_DEFECTO", false));

      zbs.show();

      if (zbs.esAceptado) {
        // actualiza los datos
        hash.put("CD_NIVEL_1_DEFECTO", zbs.getCodArea());
        hash.put("CD_NIVEL_2_DEFECTO", zbs.getCodDistrito());
        hash.put("DS_NIVEL_1_DEFECTO", zbs.getDescArea());
        hash.put("DS_NIVEL_2_DEFECTO", zbs.getDescDistrito());
        hash.put("ANYO_DEFECTO", zbs.getAnyo());
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
}

// escuchador del men�
class menuActionListener
    implements ActionListener {
  navmenu adaptee = null;
  ResourceBundle res = ResourceBundle.getBundle("html.Res0");

  public menuActionListener(navmenu a) {
    adaptee = a;
  }

  protected void showAdvise() {
    CMessage msg = new CMessage(adaptee, CMessage.msgAVISO,
                                res.getString("msg1"));
    msg.show();
    msg = null;
  }

  public void actionPerformed(ActionEvent e) {
    URL url;
    String get;

    // modificacion jlt para recuperar el url_servlet
    String ur = new String();
    String ur2 = new String();
    String ur3 = new String();
    String ur4 = new String();
    int ind = 0;
    int indb = 0;
    ur = (String) adaptee.getDocumentBase().toString();
    ind = ur.lastIndexOf("/");
    ur2 = (String) ur.substring(0, ind + 1);
    ur3 = (String) ur2.substring(0, ind);
    indb = ur3.lastIndexOf("/");
    ur4 = (String) ur3.substring(0, indb + 1);
    // System_out.println("urhtml" + ur2);
    // System_out.println("urhtml2" + ur4);
    /////////////////

    if (e.getSource()instanceof MenuItem) {
      MenuItem mi = (MenuItem) e.getSource();
      try {

        // recarga la applet
        switch (Integer.parseInt(mi.getName())) {
          case navmenu.ayuda_AD:
            if (adaptee.getREF().equals("") || adaptee.getREF().equals("/")
                || adaptee.getREF().equals(null)) {

              url = new URL(ur2 + "zip/ayuda/" +
                            adaptee.hash.getInteger("IDIOMA", true).toString() +
                            "/ayuda_ad.html");
            }
            else {

              url = new URL(adaptee.getREF() + "zip/ayuda/" +
                            adaptee.hash.getInteger("IDIOMA", true).toString() +
                            "/ayuda_ad.html");
            }

            ///////////////////////////////////
            //url=new URL(adaptee.getREF() + "zip/ayuda/" + adaptee.hash.getInteger("IDIOMA", true).toString() +"/ayuda_ad.html");
            adaptee.getAppletContext().showDocument(url, "_ayuda");
            break;

          case navmenu.ayuda_PP:

            if (adaptee.getREF().equals("") || adaptee.getREF().equals("/")
                || adaptee.getREF().equals(null)) {

              url = new URL(ur2 + "zip/ayuda/" +
                            adaptee.hash.getInteger("IDIOMA", true).toString() +
                            "/ayuda_pp.html");
            }
            else {

              url = new URL(adaptee.getREF() + "zip/ayuda/" +
                            adaptee.hash.getInteger("IDIOMA", true).toString() +
                            "/ayuda_pp.html");
            }

            ///////////////////////////////////
            //url=new URL(adaptee.getREF() + "zip/ayuda/" + adaptee.hash.getInteger("IDIOMA", true).toString() +"/ayuda_pp.html");
            adaptee.getAppletContext().showDocument(url, "_ayuda");
            break;

            // opciones m�dulo 1
          case navmenu.applet_CATA:
            showAdvise();
            url = new URL(adaptee.getFTP_SERVER() + "catalogo/catlista.asp");
            adaptee.getAppletContext().showDocument(url, "main");
            break;

          case navmenu.applet_DICCIO:
            showAdvise();
            url = new URL(adaptee.getFTP_SERVER() + "repositorio/replista.asp");
            adaptee.getAppletContext().showDocument(url, "main");
            break;

          case navmenu.applet_FTP:
            showAdvise();
            url = new URL(adaptee.getFTP_SERVER() + "transmision.html");
            adaptee.getAppletContext().showDocument(url, "main");
            break;

            // iniciar sesion
            /*          case navmenu.applet_INICIO_SESION:
                        adaptee.iniciarSesion();
                        break;
             */
            // seleccionar zonificaci�n sanitaria
          case navmenu.applet_SEL_ZONIFICACION:
            adaptee.seleccionarZonificacion();
            break;

            // applet de trabajo
          default:

            if (adaptee.getURL().equals("") || adaptee.getURL().equals("/")
                || adaptee.getURL().equals(null)) {

              url = new URL(ur4 + "servlet/AppServlet" +
                            adaptee.hash.toURL(mi.getName()));
            }
            else {

              url = new URL(adaptee.getURL() + "servlet/AppServlet" +
                            adaptee.hash.toURL(mi.getName()));
            }

            adaptee.getAppletContext().showDocument(url, "main");
            break;
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }
}
