package html;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AutServlet
    extends HttpServlet {

  // par�metros jdbc para conexi�n general
  // Tendr� userName y userPwd extraido del servlet.properties,
  //y se la usar�n todos los usuarios)

  protected String dbUrl = "";
  protected String userName = "";
  protected String userPwd = "";
  protected String driverName = ""; //LRG

  public void init(ServletConfig config) throws ServletException {

    // carga las constantes de acceso JDBC
    dbUrl = config.getInitParameter("dbUrl");
    userName = config.getInitParameter("userName");
    userPwd = config.getInitParameter("userPwd");
    driverName = config.getInitParameter("driverName"); //LRG

    try {
      // carga el driver JDBC
//      Class.forName(config.getInitParameter("drivername"));
      Class.forName(driverName);

    }
    catch (Exception e) {
      // error al abrir la conexi�n con la base de datos
      e.printStackTrace();
      throw new ServletException("Error al conectar con la base de datos.");
    }
  }

  // servicio
  public void doPost(HttpServletRequest request,
                     HttpServletResponse response) throws ServletException,
      IOException {

    // par�metros
    parametros hash = null;

    // streams
    ObjectOutputStream out = null;
    ObjectInputStream in = null;

    // objetos de la base de datos
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // buffers
    String sDescripcion = null;

    //Variables para primera conexi�n que valida el password
    //Nota: No confundir con la conexi�n habitual a la base de datos
    //que usaria el mismo userName y userPwd para todos los usuarios
    String userNameEntrada = "";
    String userPwdEntrada = "";
    Connection conEntrada = null;

    java.util.Date dFecha_Actual = new java.util.Date(); //LRG
    SimpleDateFormat Format = new SimpleDateFormat("dd/MM/yyyy"); //LRG
    String sFecha_Actual = Format.format(dFecha_Actual); //string    //LRG

    // comunicaciones servlet-applet
    try {

      // lee los par�metros
      in = new ObjectInputStream(request.getInputStream());
      hash = (parametros) in.readObject();

      userNameEntrada = hash.getString("LOGIN", true); //LRG
      userPwdEntrada = hash.getString("PASSWORD", true); //LRG

      // abre el stream de salida
      response.setContentType("application/octet-stream");
      out = new ObjectOutputStream( (OutputStream) response.getOutputStream());
      out.flush();

      // abre y cierra la conexi�n conEntradapara comprobar que el login y pwd indicados
      //por el usuario est�n dados de alta en la base de datos
      Class.forName(driverName);
      conEntrada = DriverManager.getConnection(dbUrl, userNameEntrada,
                                               userPwdEntrada);
      conEntrada.close();

      // System_out.println( "Validado usuario*******");

      // obtiene los par�metros de la sesion
      // abre la conexi�n general
      con = DriverManager.getConnection(dbUrl, userName, userPwd);
      //PDP 28/04/2000 SIVE_CA_PARAMETROS no est� dada de alta en SIVE_DESA
      st = con.prepareStatement("select * from SIVE_CA_PARAMETROS");
      rs = st.executeQuery();

      // graba los par�metros
      if (rs.next()) {
        hash.put("CA", rs.getString("CD_CA"));
        hash.put("URL_SERVLET", rs.getString("URL"));
        sDescripcion = rs.getString("DS_URL_CNE");
        if (sDescripcion != null) {
          hash.put("FTP_SERVER", sDescripcion);
        }
        sDescripcion = rs.getString("DSL_NIVEL_1");
        if ( (hash.getInteger("IDIOMA", true).intValue() == 0) || (sDescripcion == null)) {
          sDescripcion = rs.getString("DS_NIVEL_1");
        }
        hash.put("NIVEL1", sDescripcion);
        sDescripcion = rs.getString("DSL_NIVEL_2");
        if ( (hash.getInteger("IDIOMA", true).intValue() == 0) || (sDescripcion == null)) {
          sDescripcion = rs.getString("DS_NIVEL_2");
        }
        hash.put("NIVEL2", sDescripcion);
        sDescripcion = rs.getString("DSL_NIVEL_3");
        if ( (hash.getInteger("IDIOMA", true).intValue() == 0) || (sDescripcion == null)) {
          sDescripcion = rs.getString("DS_NIVEL_3");
        }
        hash.put("NIVEL3", sDescripcion);
        sDescripcion = rs.getString("DSL_IDLOCAL");
        if (sDescripcion != null) {
          hash.put("IDIOMA_LOCAL", sDescripcion);
        }
        hash.put("IT_MODULO_3", rs.getString("IT_MODULO_3"));
        hash.put("IT_TRAMERO", rs.getString("IT_TRAMERO"));
        // ARG: (6/9/2001) Ahora el parametro IT_LORTAD se lee de la BD
        sDescripcion = rs.getString("IT_LORTAD");
        if (sDescripcion != null) {
          hash.put("LORTAD", sDescripcion);

        }
      }
      else {
        throw new Exception("El sistema no est� configurado.");
      }

      rs.close();
      st.close();

      // System_out.println( "Consultada CA_PARAMETROS*******");

      // autentifica al usuario
      //st = con.prepareStatement("select DS_EMAIL, DS_PASSWORD, it_perfilusu, it_fg_mnto, it_fg_mnto_usu, it_fg_tcne, it_fg_protos, it_fg_alarmas, it_fg_validar, IT_AUTALTA, IT_AUTBAJA, IT_AUTMOD, IT_FG_ENFERMO, CD_E_NOTIF, IT_FG_EXPORT, IT_FG_GENALAUTO, IT_FG_MNOTIFS, IT_FG_CONSREST from SIVE_USUARIO where CD_USUARIO = ?"); //PDP 28/04/2000

      st = con.prepareStatement("select DS_EMAIL, IT_PERFILUSU, IT_FG_MNTO, IT_FG_MNTO_USU, IT_FG_TCNE, IT_FG_PROTOS, IT_FG_ALARMAS, IT_FG_VALIDAR, IT_AUTALTA, IT_AUTBAJA, IT_AUTMOD, IT_FG_ENFERMO, CD_E_NOTIF, IT_FG_EXPORT, IT_FG_GENALAUTO, IT_FG_MNOTIFS, IT_FG_CONSREST from SIVE_USUARIO_PISTA where CD_USUARIO = ?");
      st.setString(1, hash.getString("LOGIN", true));
      rs = st.executeQuery();
      if (rs.next()) {

        // System_out.println( "Recogiendo datos de sive_usuario_pistaz*******");

        /* //PDP 28/04/2000 ya no tiene sentido validar la password en sive_usuario ya que s�lo se valida la conexi�n JDBC
          // contrase�a incorrecta
             if (!rs.getString("DS_PASSWORD").equals(hash.getString("PASSWORD", true)))
             throw new Exception("La contrase�a que ha introducido no es correcta.");
          else
          {
            hash.put("PERFIL", rs.getString("it_perfilusu"));
            hash.put("IT_FG_MNTO", rs.getString("IT_FG_MNTO"));
            hash.put("IT_FG_MNTO_USU", rs.getString("IT_FG_MNTO_USU"));
            hash.put("IT_FG_TCNE", rs.getString("IT_FG_TCNE"));
            hash.put("IT_FG_PROTOS", rs.getString("IT_FG_PROTOS"));
            hash.put("IT_FG_ALARMAS", rs.getString("IT_FG_ALARMAS"));
            hash.put("IT_AUTALTA", rs.getString("IT_AUTALTA"));
            hash.put("IT_AUTBAJA", rs.getString("IT_AUTBAJA"));
            hash.put("IT_AUTMOD", rs.getString("IT_AUTMOD"));
            hash.put("IT_FG_ENFERMO", rs.getString("IT_FG_ENFERMO"));
            hash.put("IT_FG_VALIDAR", rs.getString("IT_FG_VALIDAR"));
            hash.put("FC_ACTUAL", sFecha_Actual);
            hash.put("IT_FG_EXPORT", rs.getString("IT_FG_EXPORT"));
            hash.put("IT_FG_GENALAUTO", rs.getString("IT_FG_GENALAUTO"));
            hash.put("IT_FG_MNOTIFS", rs.getString("IT_FG_MNOTIFS"));
            hash.put("IT_FG_CONSRES", rs.getString("IT_FG_CONSREST"));
//          hash.put("IT_FG_RESOLVER_CONFLICTOS", rs.getString("IT_FG_RESOLVER_CONFLICTOS"));
            sDescripcion = rs.getString("DS_EMAIL");
            if (sDescripcion != null)
              hash.put("DS_EMAIL", sDescripcion);
            if ( hash.getString("PERFIL", true).equals("5")) {
              hash.put("CD_E_NOTIF", rs.getString("CD_E_NOTIF"));
            }
          }
               // usuario no existe
               } else
          throw new Exception("No est� autorizado a entrar en el sistema.");
         */

        hash.put("PERFIL", rs.getString("IT_PERFILUSU"));
        hash.put("IT_FG_MNTO", rs.getString("IT_FG_MNTO"));
        hash.put("IT_FG_MNTO_USU", rs.getString("IT_FG_MNTO_USU"));
        hash.put("IT_FG_TCNE", rs.getString("IT_FG_TCNE"));
        hash.put("IT_FG_PROTOS", rs.getString("IT_FG_PROTOS"));
        hash.put("IT_FG_ALARMAS", rs.getString("IT_FG_ALARMAS"));
        hash.put("IT_AUTALTA", rs.getString("IT_AUTALTA"));
        hash.put("IT_AUTBAJA", rs.getString("IT_AUTBAJA"));
        hash.put("IT_AUTMOD", rs.getString("IT_AUTMOD"));
        hash.put("IT_FG_ENFERMO", rs.getString("IT_FG_ENFERMO"));
        hash.put("IT_FG_VALIDAR", rs.getString("IT_FG_VALIDAR"));
        hash.put("FC_ACTUAL", sFecha_Actual);
        hash.put("IT_FG_EXPORT", rs.getString("IT_FG_EXPORT"));
        hash.put("IT_FG_GENALAUTO", rs.getString("IT_FG_GENALAUTO"));
        hash.put("IT_FG_MNOTIFS", rs.getString("IT_FG_MNOTIFS"));
        hash.put("IT_FG_CONSRES", rs.getString("IT_FG_CONSREST"));
//          hash.put("IT_FG_RESOLVER_CONFLICTOS", rs.getString("IT_FG_RESOLVER_CONFLICTOS"));
        sDescripcion = rs.getString("DS_EMAIL");
        if (sDescripcion != null) {
          hash.put("DS_EMAIL", sDescripcion);

        }
        if (hash.getString("PERFIL", true).equals("5")) {
          hash.put("CD_E_NOTIF", rs.getString("CD_E_NOTIF"));
        }
      }

      rs.close();
      st.close();

      // System_out.println( "Consultada sive_usuario_pista*******");
      // System_out.println( "Perfil es***"  +  hash.getString(("PERFIL"),true));

      // m�s par�metros si el perfil de usuario es 5
      if (hash.get("CD_E_NOTIF") != null) {
        st = con.prepareStatement(
            "select * from SIVE_E_NOTIF WHERE CD_E_NOTIF = ?");
        st.setString(1, hash.getString("CD_E_NOTIF", true));

        rs = st.executeQuery();

        // graba los par�metros
        if (rs.next()) {
          hash.put("CD_NIVEL_1_EQ", rs.getString("CD_NIVEL_1")); // _EQ
          hash.put("CD_NIVEL_2_EQ", rs.getString("CD_NIVEL_2")); // _EQ
          // JRM a�ade la zona b�sica de salud del notificador
          hash.put("CD_ZBS_EQ", rs.getString("CD_ZBS")); // _EQ
          hash.put("DS_E_NOTIF", rs.getString("DS_E_NOTIF")); // _EQ

        }
        else {
          throw new Exception("El sistema no est� configurado.");
        }

        rs.close();
        st.close();

        if (hash.get("CD_E_NOTIF") != null) {
          // nombre del �rea
          st = con.prepareStatement(
              "select * from SIVE_NIVEL1_S WHERE CD_NIVEL_1 = ?");
          st.setString(1, hash.getString("CD_NIVEL_1_EQ", true));

          rs = st.executeQuery();

          if (rs.next()) {
            sDescripcion = rs.getString("DSL_NIVEL_1");
            if ( (hash.getInteger("IDIOMA", true).intValue() == 0) ||
                (sDescripcion == null)) {
              sDescripcion = rs.getString("DS_NIVEL_1");
            }
            hash.put("DS_NIVEL_1_EQ", sDescripcion);
          }
          else {
            throw new Exception("El sistema no est� configurado.");
          }

          rs.close();
          st.close();

          // nombre del distrito
          st = con.prepareStatement(
              "select * from SIVE_NIVEL2_S WHERE CD_NIVEL_1 = ?");
          st.setString(1, hash.getString("CD_NIVEL_2_EQ", true));

          rs = st.executeQuery();

          if (rs.next()) {
            sDescripcion = rs.getString("DSL_NIVEL_2");
            if ( (hash.getInteger("IDIOMA", true).intValue() == 0) ||
                (sDescripcion == null)) {
              sDescripcion = rs.getString("DS_NIVEL_2");
            }
            hash.put("DS_NIVEL_2_EQ", sDescripcion);
          }
          else {
            throw new Exception("El sistema no est� configurado.");
          }

          rs.close();
          st.close();

          // JRM: A�adimos la zona basica de salud
          String strSelect = "select * from SIVE_ZONA_BASICA " +
              "where CD_NIVEL_1 = '" + hash.getString("CD_NIVEL_1_EQ", true) +
              "' and " +
              "      CD_NIVEL_2 = '" + hash.getString("CD_NIVEL_2_EQ", true) +
              "' and " +
              "      CD_ZBS     = '" + hash.getString("CD_ZBS_EQ", true) + "'";
          st = con.prepareStatement(strSelect);
          rs = st.executeQuery();
          if (rs.next()) {
            hash.put("DS_ZBS_EQ", rs.getString("DS_ZBS"));
          }
          else {
            throw new Exception("El sistema no est� configurado.");
          }

          rs.close();
          st.close();
        }
      }

      // System_out.println( "Consultadas tablas para perfil 5*******");
      // System_out.println( "Perfil es***"  +  hash.getString(("PERFIL"),true));

      // �mbito sanitario
      if (hash.getString("PERFIL", true).equals("3") ||
          hash.getString("PERFIL", true).equals("4")) {

        StringBuffer sbCD_NIVEL_1 = new StringBuffer("");
        StringBuffer sbCD_NIVEL_2 = new StringBuffer("");
        String sN1 = null;
        String sN2 = null;

        st = con.prepareStatement(
            "select * from SIVE_AUTORIZACIONES_PISTA WHERE CD_USUARIO = ?");
        st.setString(1, hash.getString("LOGIN", true));

        rs = st.executeQuery();

        while (rs.next()) {
          sN1 = rs.getString("CD_NIVEL_1");
          if (sN1 != null) {
            sbCD_NIVEL_1.append(sN1 + ",");
            sN2 = rs.getString("CD_NIVEL_2");
            if (sN2 != null) {
              if (hash.getString("PERFIL", true).equals("4")) {
                sbCD_NIVEL_2.append(sN2 + ",");
              }
            }
          }
        }

        rs.close();
        st.close();

        hash.put("CD_NIVEL_1_AUTORIZACIONES", sbCD_NIVEL_1.toString());
        hash.put("CD_NIVEL_2_AUTORIZACIONES", sbCD_NIVEL_2.toString());
      }

      // System_out.println( "Consultadas tablas para autoriz*******");
      // System_out.println( "Perfil es***"  +  hash.getString(("PERFIL"),true));

      // cierra la conexi�n con la base de datos
      con.close();

      // envia los par�metros de la sesi�n
      out.writeObject(hash);
      out.flush();

      // cierra los streams
      out.close();

      // envia la excepci�n
    }
    catch (Exception e) {
      e.printStackTrace();
      out.writeObject(new Exception(e.getMessage()));
      out.flush();
      out.close();
    }
  }

} //CLASE
