package html;

import java.io.Serializable;
import java.net.URLEncoder;
import java.util.Hashtable;

// vector con los par�metros de las sesi�n
public class parametros
    extends Hashtable
    implements Serializable {

  // lectura de par�metros de tipo cadena
  public String getString(String s, boolean b) throws Exception {
    String data = (String) get(s);

    // par�metro requerido
    if (b) {
      // falta par�metro
      if (data == null) {
        throw new Exception("Falta par�metro: " + s);
      }
      else {
        // par�metro vacio
        if (data.length() == 0) {
          throw new Exception("Par�metro vacio: " + s);
        }
      }
    }
    else {
      if (data == null) {
        data = "";
      }
    }

    return data;
  }

  // lectura de par�metros de tipo entero
  public Integer getInteger(String s, boolean b) throws Exception {
    Integer data = (Integer) get(s);

    // par�metro requerido
    if (b) {

      // falta par�metro
      if (data == null) {
        throw new Exception("Falta par�metro: " + s);
      }
    }

    return data;
  }

  // tranforma la hash table en par�metros url
  public String toURL(String app) {
    String url;

    try {
      // par�metros de env�o
      url = "?APPLET=" + app +
          "&IDIOMA=" + URLEncoder.encode(getInteger("IDIOMA", true).toString()) +
          "&LOGIN=" + URLEncoder.encode(getString("LOGIN", true)) +
          "&URL_SERVLET=" + URLEncoder.encode(getString("URL_SERVLET", true)) +
          "&FTP_SERVER=" + URLEncoder.encode(getString("FTP_SERVER", true)) +
          "&URL_HTML=" + URLEncoder.encode(getString("URL_HTML", true)) +
          "&DS_EMAIL=" + URLEncoder.encode(getString("DS_EMAIL", false)) +
          "&PERFIL=" + URLEncoder.encode(getString("PERFIL", true)) +
          "&CA=" + URLEncoder.encode(getString("CA", true)) +
          "&NIVEL1=" + URLEncoder.encode(getString("NIVEL1", true)) +
          "&NIVEL2=" + URLEncoder.encode(getString("NIVEL2", true)) +
          "&NIVEL3=" + URLEncoder.encode(getString("NIVEL3", true)) +
          "&TSIVE=" + URLEncoder.encode(getString("TSIVE", true)) +
          "&IDIOMA_LOCAL=" + URLEncoder.encode(getString("IDIOMA_LOCAL", false)) +
          "&IT_AUTALTA=" + URLEncoder.encode(getString("IT_AUTALTA", true)) +
          "&IT_AUTBAJA=" + URLEncoder.encode(getString("IT_AUTBAJA", true)) +
          "&IT_AUTMOD=" + URLEncoder.encode(getString("IT_AUTMOD", true)) +
          "&IT_FG_ENFERMO=" + URLEncoder.encode(getString("IT_FG_ENFERMO", true)) +
          "&IT_FG_VALIDAR=" + URLEncoder.encode(getString("IT_FG_VALIDAR", true)) +
          "&IT_MODULO_3=" + URLEncoder.encode(getString("IT_MODULO_3", true)) +
          "&IT_TRAMERO=" + URLEncoder.encode(getString("IT_TRAMERO", true)) +
          "&IT_FG_MNTO=" + URLEncoder.encode(getString("IT_FG_MNTO", true)) +
          "&IT_FG_MNTO_USU=" + URLEncoder.encode(getString("IT_FG_MNTO_USU", true)) +
          "&IT_FG_TCNE=" + URLEncoder.encode(getString("IT_FG_TCNE", true)) +
          "&IT_FG_PROTOS=" + URLEncoder.encode(getString("IT_FG_PROTOS", true)) +
          "&IT_FG_ALARMAS=" + URLEncoder.encode(getString("IT_FG_ALARMAS", true)) +
          "&IT_FG_EXPORT=" + URLEncoder.encode(getString("IT_FG_EXPORT", true)) +
          "&IT_FG_GENALAUTO=" + URLEncoder.encode(getString("IT_FG_GENALAUTO", true)) +
          "&IT_FG_MNOTIFS=" + URLEncoder.encode(getString("IT_FG_MNOTIFS", true)) +
          "&IT_FG_CONSRES=" + URLEncoder.encode(getString("IT_FG_CONSRES", true)) +
          "&CD_E_NOTIF=" + URLEncoder.encode(getString("CD_E_NOTIF", false)) +
          "&DS_E_NOTIF=" + URLEncoder.encode(getString("DS_E_NOTIF", false)) +
          "&CD_NIVEL_1_EQ=" + URLEncoder.encode(getString("CD_NIVEL_1_EQ", false)) +
          "&CD_NIVEL_2_EQ=" + URLEncoder.encode(getString("CD_NIVEL_2_EQ", false)) +
          "&CD_ZBS_EQ=" + URLEncoder.encode(getString("CD_ZBS_EQ", false)) +
          "&DS_NIVEL_1_EQ=" + URLEncoder.encode(getString("DS_NIVEL_1_EQ", false)) +
          "&DS_NIVEL_2_EQ=" + URLEncoder.encode(getString("DS_NIVEL_2_EQ", false)) +
          "&DS_ZBS_EQ=" + URLEncoder.encode(getString("DS_ZBS_EQ", false)) +
          "&CD_NIVEL_1_AUTORIZACIONES=" +
          URLEncoder.encode(getString("CD_NIVEL_1_AUTORIZACIONES", false)) +
          "&CD_NIVEL_2_AUTORIZACIONES=" +
          URLEncoder.encode(getString("CD_NIVEL_2_AUTORIZACIONES", false)) +
          "&FC_ACTUAL=" + URLEncoder.encode(getString("FC_ACTUAL", true)) +
          "&CD_NIVEL_1_DEFECTO=" +
          URLEncoder.encode(getString("CD_NIVEL_1_DEFECTO", false)) +
          "&CD_NIVEL_2_DEFECTO=" +
          URLEncoder.encode(getString("CD_NIVEL_2_DEFECTO", false)) +
          "&DS_NIVEL_1_DEFECTO=" +
          URLEncoder.encode(getString("DS_NIVEL_1_DEFECTO", false)) +
          "&DS_NIVEL_2_DEFECTO=" +
          URLEncoder.encode(getString("DS_NIVEL_2_DEFECTO", false)) +
          "&ANYO_DEFECTO=" + URLEncoder.encode(getString("ANYO_DEFECTO", false)) +
          // JRM: Para consultas
          "&IT_FCCONS=" + URLEncoder.encode(getString("IT_FCCONS", false)) +
          // JRM: Para el origen
          "&ORIGEN=" + URLEncoder.encode(getString("ORIGEN", false)) +
          // JRM (26/01/01): Metemos par�metro de LORTAD
          "&LORTAD=" + URLEncoder.encode(getString("LORTAD", false)
                                         );
      //   "&IT_FG_RESOLVER_CONFLICTOS=" + URLEncoder.encode(getString("IT_FG_RESOLVER_CONFLICTOS", false));
    }
    catch (Exception e) {
      e.printStackTrace();
      url = "";
    }

    // System_out.println(url);

    return url;
  }
}
