package html;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

class MenuPanel
    extends Container
    implements MouseListener, MouseMotionListener {
  PopupMenu popList[]; // A PopupMenu for each item in your "menu bar".
  Label lblList[]; // Labels to hold the names of "menu bar" items.
  Color clrDefault; // Default color to use for this control
  int idxHighlight; // The menu item index that has the focus. (-1=none)
  int idxPressed; // Menu item idx that user clicked on
  boolean bMenuUp;

  // Default constructor for the MenuPanel class.
  MenuPanel() {
    setLayout(new FlowLayout(FlowLayout.LEFT, 4, 3));
    clrDefault = SystemColor.menu;
    setBackground(clrDefault);
    addMouseMotionListener(this);
    addMouseListener(this);
    idxHighlight = -1;
    idxPressed = -1;
    bMenuUp = false;
    //# // System_out.println("El men�");
    lblList = new Label[1];
    lblList[0] = new Label("Testing...", Label.CENTER);
    lblList[0].addMouseMotionListener(this);
    lblList[0].addMouseListener(this);

    add(lblList[0]);
    validate();
  }

  // Converts a MenuBar to a set of Label/PopupMenu pairs used by MenuPanel.
  public void setMenuBar(MenuBar mBar) {
    Font f = new Font("Dialog", Font.BOLD, 12);

    clearMouseListeners();
    int menuCount = mBar.getMenuCount();
    lblList = new Label[menuCount];
    popList = new PopupMenu[menuCount];

    // For each Menu in the MenuBar...
    for (int i = 0; i < menuCount; i++) {
      Menu m = mBar.getMenu(i);
      String szName = m.getLabel();
      int itemCount = m.getItemCount();

      // Generate the corresponding Label/PopupMenu object pairs...
      lblList[i] = new Label(szName, Label.CENTER);
      lblList[i].addMouseMotionListener(this);
      lblList[i].addMouseListener(this);
      lblList[i].setFont(f);
      lblList[i].setEnabled(m.isEnabled());

      add(lblList[i]);

      // Build this PopupMenu item by item (assignment won't work).
      popList[i] = new PopupMenu(szName);
      for (int j = 0; j < itemCount; j++) {
        MenuItem mi = m.getItem(0);
        popList[i].add(mi);
      }
      lblList[i].add(popList[i]);
    }

  }

  // Paints any 3D rectangles around the Menu labels.
  public void paint(Graphics g) {
    Rectangle rect = getBounds();

    g.setColor(clrDefault); // Prepares screen wash
    g.fillRect(0, 0, rect.width, rect.height); // fill with bkg color

    // Paint 3D line underneath menus...
    g.draw3DRect(rect.x + 1, rect.y + rect.height - 3, rect.width - 4, 1, false);

    // Paint the Labels and PopupMenus as appropriate...
    for (int i = 0; i < lblList.length; i++) {
      rect = lblList[i].getBounds();
      if (i == idxHighlight) {
        g.draw3DRect(rect.x - 1, rect.y - 1, rect.width + 2, rect.height + 2, true);
      }
      if (i == idxPressed) {
        g.draw3DRect(rect.x - 1, rect.y - 1, rect.width + 2, rect.height + 2, false);
        if (bMenuUp == false) {
          bMenuUp = true;
          popList[i].show(this, rect.x, rect.y + rect.height + 1);
        }
      }
    }
  }

  // Used to clear any Listeners before re-creating them with setMenuBar()
  protected void clearMouseListeners() {
    for (int i = 0; i < lblList.length; i++) {
      remove(lblList[i]);
      lblList[i].removeMouseListener(this);
      lblList[i].removeMouseMotionListener(this);
    }
  }

  // Handle the MouseListener.mouseClicked event.
  public void mousePressed(MouseEvent evt) {
    int oldIdx = idxPressed;

    if (idxPressed >= 0) {
      idxHighlight = idxPressed;
      idxPressed = -1;
      repaint();
      return;
    }

    // Which Label[] index is the mouse hovering over?
    idxPressed = -1; //lblState = 0;
    if (evt.getSource()instanceof Label) {
      Label lbl = (Label) evt.getSource();
      for (int i = 0; i < lblList.length; i++) {
        if (lblList[i] == lbl) {
          idxPressed = i;
          idxHighlight = -1;
          bMenuUp = false;
        }
      }
    }

//		Point evtPoint = evt.getPoint();
//		szInfo = "("+evtPoint.x+","+evtPoint.y+")";
    if (idxPressed != oldIdx) {
      repaint();
    }
  }

  // Handle the MouseListener.mouseExited event.
  public void mouseExited(MouseEvent evt) {
    idxHighlight = -1;
    idxPressed = -1;
    repaint();
  }

  // Handle the MouseListener.mouseReleased event.
  public void mouseReleased(MouseEvent evt) {
    if (evt.getSource()instanceof Label) {
      mouseClicked(evt);
    }
  }

  // Handle the MouseMotionListener.mouseMoved event.
  public void mouseMoved(MouseEvent evt) {
    int oldIdx = idxHighlight;

    // Which Label[] index is the mouse hovering over?
    idxHighlight = -1; //lblState = 0;
    if (evt.getSource()instanceof Label) {
      Label lbl = (Label) evt.getSource();
      for (int i = 0; i < lblList.length; i++) {
        if (lblList[i] == lbl && i != idxPressed) {
          idxHighlight = i;
          if (idxPressed >= 0) {
            idxPressed = i;
          }
        }
      }
    }

//		Point evtPoint = evt.getPoint();
//		szInfo = "("+evtPoint.x+","+evtPoint.y+")";
    if (idxHighlight != oldIdx) {
      repaint();
    }
  }

  // Handle the MouseMotionListener.mouseDragged event.
  public void mouseDragged(MouseEvent evt) {
    mouseMoved(evt);
  }

  // Unused MouseEvents.
  // Not using MouseAdapter & MouseMotionAdapter yields 2 less .class files.
  public void mouseClicked(MouseEvent evt) {}

  public void mouseEntered(MouseEvent evt) {}
}
