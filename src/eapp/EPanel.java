package eapp;

import java.util.ResourceBundle;

import java.awt.BorderLayout;
import java.awt.Dimension;

import capp.CApp;
import capp.CDialog;

public abstract class EPanel
    extends CDialog {
  ControlPanel ctrlPanel;
  ResourceBundle res = ResourceBundle.getBundle("eapp.Res" + app.getIdioma());

  /** abreviaturas de los criterios de b�squeda */
  public static final String ENFERMEDAD = " Enf.";
  public static final String AREA = " Zonif.";
  public static final String DISTRITO = "-";
  public static final String ZBS = "-";
  public static final String EQUIPO = " Eq.";
  public static final String CENTRO = " Cn.";
  public static final String PERIODO = " Per.";
  public static final String SEPARADOR_ANO_SEM = "-";
  public static final String HASTA = " hasta";
  public static final String PROVINCIA = " Prov.";
  public static final String MUNICIPIO = " Mun.";
  public static final String CRITERIO = "CRITERIO:";
  public static final String AGRUPACION = " Ag.";
  public static final String GSEM = " sem.";
  public static final String CODIGO = " Cod.";
  public static final String ENFERMO = " Enfo.";
  public static final String FECHA_NACIMIENTO = " Fec. Nac.";
  public static final String NIVEL_ASISTENCIAL = " Niv.As.";
  public static final String NIVEL_ASISTENCIAL_1 = " N.As.1 ";
  public static final String NIVEL_ASISTENCIAL_2 = " N.As.2 ";
  public static final String CRITERIO_POR_AREA = " Crit. por area";
  public static final String CRITERIO_POR_MUNICIPIO = " Crit. por mun.";

  //Strings que van al di�logo en funci�n del idioma
  protected String sREGISTROS_TOTALES = "";
  protected String sREGISTROS_MOSTRADOS = "";
  protected String sCONSULTANDO = "";

  public EPanel(CApp a) {
    super(a);
    try {
      sREGISTROS_TOTALES = res.getString("msg2.Text");
      sREGISTROS_MOSTRADOS = res.getString("msg3.Text");
      sCONSULTANDO = res.getString("msg4.Text");
      ctrlPanel = new ControlPanel(this);
      jbInit();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(800, 550));
    this.add(ctrlPanel, BorderLayout.NORTH);
  }

  // informe completo
  public abstract void InformeCompleto();

  // mas datos
  public abstract void MasDatos();

  // generar
  public abstract boolean GenerarInforme();

  // mostrar grafica
  public abstract void MostrarGrafica();

  public void setTotalRegistros(String t) {
    ctrlPanel.setStatus1(sREGISTROS_TOTALES + t);
  }

  public void setRegistrosMostrados(String t) {
    ctrlPanel.setStatus2(sREGISTROS_MOSTRADOS + t);
  }

  public void setLimpiarStatus() {
    ctrlPanel.setStatus1("");
    ctrlPanel.setStatus2("");
  }

  public void setGenerandoInforme() {
    ctrlPanel.setStatus1("");
    ctrlPanel.setStatus2(sCONSULTANDO);
  }
}
