package enfcentinela;

import java.io.Serializable;

public class DataEnfCenti
    implements Serializable {
  protected String sCod = "";
  protected String sDes = "";

  public DataEnfCenti() {
  }

  public DataEnfCenti(String cod) {
    sCod = cod;
    sDes = null;
  }

  public DataEnfCenti(String cod, String des) {

    sCod = cod;
    sDes = des;
  } //fin construct

  public String getCod() {
    return sCod;
  }

  public String getDes() {
    return sDes;
  }
}
