
package infcobsem;

import java.io.Serializable;

public class DataInfCobSem
    implements Serializable {

  public String DS_NOMBRE;
  public int NM_NTOTREAL;
  public int NM_NNOTIFT;
  public int NM_COBERTURAI;
  public int NM_COBERTURAR;
  public boolean bInformeCompleto = false;

  public DataInfCobSem(String d,
                       int i, int j, int k, int l) {
    DS_NOMBRE = d;
    NM_NTOTREAL = i;
    NM_NNOTIFT = j;
    NM_COBERTURAI = k;
    NM_COBERTURAR = l;
  }
}
