package infcobsem;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import eqNot.DataEqNot;
import infcobper.ParamC11_12;
import sapp.StubSrvBD;

public class Pan_InfCobSem
    extends EPanel {
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 1;

  final String strSERVLET = "servlet/SrvInfCobSem";
  final String strLOGO_CCAA = "images/ccaa.gif";

  // estructuras de datos
  protected Vector vCasos;
  protected Vector vTotales;
  protected CLista lista;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  final int erwCASOS_EQUIPO = 1;
  final int erwCASOS_CENTRO = 2;
  public int erwCASO_ACTUAL = 2;

  public ParamC11_12 paramC1 = null;

  CApp app = null;

  public Pan_InfCobSem(CApp a) {
    super(a);

    try {
      res = ResourceBundle.getBundle("infcobsem.Res" + a.getIdioma());
      stub = new StubSrvBD();
      app = a;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + "erw/ERWCobSem.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // iniciar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;
    // ARS 17-07-01
    DataEqNot datosPar = null;

    //E //#// System_out.println(lista.getState());
    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        PrepararInforme();
        param.setFilter(lista.getFilter());
        // obtiene los datos del servidor
        paramC1.iPagina++;
// Modificaci�n ARS 17-07-01
// para corregir el null del m�s datos del informe.
// Env�o el n�mero de pagina en el DataEqNot
// en uno de esos par�metros que no se utiliza, el fax.

        if (!paramC1.sEN.equals("")) {
          datosPar = new DataEqNot("", "", "", paramC1.sCN,
                                   paramC1.sEN, paramC1.sSemanaI, paramC1.sAnoI,
                                   "", "",
                                   "", String.valueOf(paramC1.iPagina), 0, "",
                                   "", true);
//	    "","",0,"","",true);
        }
        else {
          datosPar = new DataEqNot("", "", "", paramC1.sCN,
                                   "", paramC1.sSemanaI, paramC1.sAnoI, "", "",
                                   "", String.valueOf(paramC1.iPagina), 0, "",
                                   "", true);
//	    "","",0,"","",true);
        }
        param.addElement(datosPar);

//	param.addElement(paramC1);
// Fin de la correcci�n del m�s datos del informe ARS 17-07-01
        param.setFilter("");
        param.setIdioma(app.getIdioma());
        param.trimToSize();
        stub.setUrl(new URL(app.getURL() + strSERVLET));
//	  if (erwCASO_ACTUAL == erwCASOS_CENTRO)  {
        if (paramC1.sGrupo.compareTo("1") == 0) {
          lista = (CLista) stub.doPost(erwCASOS_EQUIPO, param);
          /*     SrvInfCobSem srv = new SrvInfCobSem();
               srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                  "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                  "pista_desa",
                  "pista_desa");
               lista = srv.doDebug(erwCASOS_EQUIPO, param);*/
        }
        else {
          lista = (CLista) stub.doPost(erwCASOS_CENTRO, param);

          /*	      SrvInfCobSem srv = new SrvInfCobSem();
               srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                  "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                  "pista_desa",
                  "pista_desa");
               lista = srv.doDebug(erwCASOS_CENTRO, param);*/
        }

        v = (Vector) lista.elementAt(0);
//	vTotales = (Vector) lista.elementAt(1); ARS 17-07-01

        for (int j = 0; j < v.size(); j++) {
          vCasos.addElement(v.elementAt(j));

          // control de registros
        }
        Integer tot = (Integer) lista.elementAt(1);
        this.setTotalRegistros(tot.toString());
        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // repintado
        erwClient.refreshReport(true);

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // informe comleto
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // genera el primer informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  protected boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    CLista param = new CLista();
    DataEqNot datosPar = null;

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      PrepararInforme();

      // obtiene los datos del servidor
      paramC1.iPagina = 0;
      if (!paramC1.sEN.equals("")) {
        datosPar = new DataEqNot("", "", "", paramC1.sCN,
                                 paramC1.sEN, paramC1.sSemanaI, paramC1.sAnoI,
                                 "", "",
                                 "", "", 0, "", "", true);
      }
      else {
        datosPar = new DataEqNot("", "", "", paramC1.sCN,
                                 "", paramC1.sSemanaI, paramC1.sAnoI, "", "",
                                 "", "", 0, "", "", true);
      }
      // �todos los datos?
      datosPar.bInformeCompleto = conTodos;

      //param.addElement(paramC1);
      param.addElement(datosPar);
      param.setIdioma(app.getIdioma());
      param.trimToSize();
      stub.setUrl(new URL(app.getURL() + strSERVLET));
      lista = new CLista();
      lista.setState(CLista.listaVACIA);
      if (paramC1.sGrupo.compareTo("1") == 0) {
        lista = (CLista) stub.doPost(erwCASOS_EQUIPO, param);

        /*	  SrvInfCobSem srv = new SrvInfCobSem();
             srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                "jdbc:oracle:thin:@10.160.5.149:1521:ORCL",
                "pista",
                "loteb98");
             lista = srv.doDebug(erwCASOS_EQUIPO, param);
         */
      }
      else {

        lista = (CLista) stub.doPost(erwCASOS_CENTRO, param);

        /*	  SrvInfCobSem srv = new SrvInfCobSem();
             srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                "jdbc:oracle:thin:@10.160.5.149:1521:ORCL",
                "pista",
                "loteb98");
             lista = srv.doDebug(erwCASOS_CENTRO, param);
         */

      }
      //E //#// System_out.println("vuelvo del servlet");

      // control de registros
      if (lista == null) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg7.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return false;
      }
      else {

        vCasos = (Vector) lista.elementAt(0);
        //vTotales = (Vector) lista.elementAt(1);
        Integer tot = (Integer) lista.elementAt(1);
        /*if (lista.getState()==lista.listaINCOMPLETA)
          this.setTotalRegistros(res.getString(" this.TotalRegistros"));
         */
        this.setTotalRegistros(tot.toString());
        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // establece las matrices de datos
        Vector retval = new Vector();
        retval.addElement("DS_NOMBRE = DS_NOMBRE");
        retval.addElement("NM_NTOTREAL = NM_NTOTREAL");
        retval.addElement("NM_NNOTIFT = NM_NNOTIFT");
        retval.addElement("NM_COBERTURAI = NM_COBERTURAI");
        retval.addElement("NM_COBERTURAR = NM_COBERTURAR");
        dataHandler.RegisterTable(vCasos, "C_11_12", retval, null);
        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        modoOperacion = modoNORMAL;
        Inicializar();
        return true;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();

      modoOperacion = modoNORMAL;
      Inicializar();
      return false;
    }

  }

  // este informe no tiene grafica
  public void MostrarGrafica() {
  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;
    int iEtiqueta = 0;
    String textoGrupo;
    String textoEn;

    // plantilla
    TemplateManager tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
    tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

    // carga los citerios
//    tm.SetLabel("LAB005", "A�o epidemiol�gico: " + paramC1.sAnoI);
//    tm.SetLabel("LAB007", "Semana epidemiol�gica: " + paramC1.sSemanaI);
//    tm.SetLabel("LAB008", "Area: " + paramC1.sCN);

    // carga los citerios
    tm.SetLabel("CRITERIO1",
                res.getString("msg8.Text") + paramC1.sAnoI + " " +
                res.getString("msg9.Text") + paramC1.sSemanaI);
    tm.SetLabel("CRITERIO2",
                res.getString("msg10.Text") + paramC1.sCN + " " +
                paramC1.sCNdesc);

    if (paramC1.sEN.compareTo("") != 0) {
//	tm.SetLabel("LAB009", res.getString("msg11.Text") + paramC1.sEN);
      tm.SetLabel("CRITERIO2",
                  tm.GetLabel("CRITERIO2") + res.getString("msg12.Text") +
                  paramC1.sEN + " " + paramC1.sENdesc);
      textoEn = "-" + paramC1.sEN;

    }
    else {
//	tm.SetLabel("LAB009", "");
      textoEn = "";
    }
    if (paramC1.sGrupo.compareTo("1") == 0) {
//	tm.SetLabel("LAB010", "Agrupado por Equipos Notificadores");
      tm.SetLabel("CRITERIO3", res.getString("msg13.Text"));
      tm.SetLabel("LAB000", res.getString("msg14.Text"));
      textoGrupo = res.getString("msg15.Text");

    }
    else {
//	tm.SetLabel("LAB010", "Agrupado por Centros Notificadores");
      tm.SetLabel("CRITERIO3", res.getString("msg16.Text"));
      tm.SetLabel("LAB000", res.getString("msg17.Text"));
      textoGrupo = res.getString("msg18.Text");

    }

    /*tm.SetLabel("LAB014", "A�o: " + paramC1.sAnoI
      +" Semana: " + paramC1.sSemanaI
      +" Area: " + paramC1.sCN
      + textoEn + textoGrupo);*/
//E //#// System_out.println("terminando***************");
  }

}
