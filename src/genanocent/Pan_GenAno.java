/* Solo para Centinelas*/

package genanocent;

// PRUEBA
import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.c_fechas.DataConvFec;
import sapp.StubSrvBD;

public class Pan_GenAno
    extends CPanel {
  XYLayout xYLayout1 = new XYLayout();
  ResourceBundle res;
  Label label1 = new Label();
  TextField txtAnyo = new TextField();
  ButtonControl btnGenerar = new ButtonControl();

  protected CCargadorImagen imgs = null;
  final String imgBROWSER = "images/aceptar.gif";

  final int modoNORMAL = 1;
  final int modoESPERA = 2;

  // comunicaciones
  final int servletGENERAR_FECHAS_DE_UN_A�O = 1;
  final int servletGENERAR_A�O_Y_COPIAR_POBLAC = 2;
  protected StubSrvBD stubClienteParam = null;
//  final String strSERVLETParam= "servlet/SrvConvFec";

//  final String strSERVLETParam= "servlet/SrvConvFecCent";
  final String strSERVLETParam = nombreservlets.strSERVLET_CONV_FEC_CENT;

  int idioma;
  int modoOperacion = modoNORMAL;

  public Pan_GenAno(CApp a) {
    try {
      setApp(a);
      res = ResourceBundle.getBundle("genanocent.Res" + a.getIdioma());
      idioma = app.getIdioma();
      stubClienteParam = new StubSrvBD(new URL(app.getURL() + strSERVLETParam));
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    imgs = new CCargadorImagen(app);
    imgs.setImage(imgBROWSER);
    imgs.CargaImagenes();
    label1.setText(res.getString("label1.Text"));
    txtAnyo.setBackground(new Color(255, 255, 150));
    txtAnyo.addActionListener(new Pan_GenAno_txtAnyo_actionAdapter(this));
    btnGenerar.setLabel(res.getString("btnGenerar.Label"));
    btnGenerar.setImage(imgs.getImage(0));
    btnGenerar.addActionListener(new Pan_GenAno_btnGenerar_actionAdapter(this));
    xYLayout1.setWidth(400);
    xYLayout1.setHeight(200);
    this.setLayout(xYLayout1);
    this.add(label1, new XYConstraints(24, 42, -1, -1));
    this.add(txtAnyo, new XYConstraints(71, 42, 78, -1));
    this.add(btnGenerar, new XYConstraints(280, 109, -1, -1));
  }

  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        btnGenerar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnGenerar.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  public boolean IsDataValid() {
    boolean b = true;
    String msg = "";
    CMessage mensaje = null;

    if (this.txtAnyo.getText().length() == 0) {
      msg = res.getString("msg2.Text");
      b = false;
    }
    else if (this.txtAnyo.getText().length() != 4) {
      msg = res.getString("msg3.Text");
      this.txtAnyo.selectAll();
      b = false;
    }

    if (!b) {
      mensaje = new CMessage(this.app, CMessage.msgERROR, msg);
      mensaje.show();
      mensaje = null;
    }

    return b;
  }

  void txtAnyo_actionPerformed(ActionEvent e) {

  }

  void btnGenerar_actionPerformed(ActionEvent e) {
    CMessage mensaje = null;
    DataConvFec elDato = new DataConvFec("", txtAnyo.getText().trim(), 1);
    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    Inicializar();

    if (IsDataValid()) {
      try {
        CLista lista = new CLista();
        lista.addElement(elDato);
//        this.stubClienteParam.doPost(servletGENERAR_FECHAS_DE_UN_A�O, lista);
        this.stubClienteParam.doPost(servletGENERAR_A�O_Y_COPIAR_POBLAC, lista);

        /**************
                 //  SOLO DESARROLLO
                  SrvConvFecCent srv= new SrvConvFecCent();
                  //Indica como conectarse a la b.datos
                  srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                  srv.doDebug(servletGENERAR_A�O_Y_COPIAR_POBLAC, lista);
//************* */

         //supongo que si vuelve sin lanzar ninguna excepcion es porque
         //se ha generado correctamente
         mensaje = new CMessage(app, CMessage.msgAVISO,
                                res.getString("msg4.Text"));
        mensaje.show();
        mensaje = null;

      }
      catch (Exception erf) {
        String error = erf.getMessage();
        error = error.substring(error.indexOf(':') + 1);
        mensaje = new CMessage(app, CMessage.msgERROR, error);
        mensaje.show();
        mensaje = null;
      }
    }
    modoOperacion = modo;
    Inicializar();
  }

}

class Pan_GenAno_txtAnyo_actionAdapter
    implements java.awt.event.ActionListener {
  Pan_GenAno adaptee;

  Pan_GenAno_txtAnyo_actionAdapter(Pan_GenAno adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.txtAnyo_actionPerformed(e);
  }
}

class Pan_GenAno_btnGenerar_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_GenAno adaptee;
  ActionEvent e;

  Pan_GenAno_btnGenerar_actionAdapter(Pan_GenAno adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnGenerar_actionPerformed(e);
  }
}
