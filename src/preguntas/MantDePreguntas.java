package preguntas;

import java.util.ResourceBundle;

import capp.CApp;
import comun.constantes;

public class MantDePreguntas
    extends CApp {

  // parametros del servlet
  final int servletSELECCION_X_CODIGO = 5;
  ResourceBundle res;
  final int servletSELECCION_X_DESCRIPCION = 6;

  //  final String strSERVLET= "servlet/SrvMantPreguntas";
  final String strSERVLET = constantes.strSERVLET_MANT_PREGUNTAS;

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("preguntas.Res" + this.getIdioma());
    setTitulo(res.getString("msg2.Text"));
//  VerPanel("",new CMantenimientoPreguntas(this));

    //Centinelas: Constructor con el USU
    //if ( this.getTSive().equals("C") ) {
    VerPanel("", new CMantenimientoPreguntas(this, true));
    //}
    //else {
    //  VerPanel("",new CMantenimientoPreguntas(this));
    //}

  }

}
