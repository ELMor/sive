package preguntas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvMantPreguntas
    extends DBServlet {

  // modos de operaci�n
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRICPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRICPCION = 6;
  final int servletPREGUNTA_SELECCIONADA = 7;

  // buffers
  protected String sCodPregunta;
  protected String sDescPregunta;
  protected String sDescLPregunta;
  protected String sListaValores;
  protected String iTipo;
  protected int iLongitud;
  protected int iEnteros;
  protected int iDecimales;
  protected String sDescLista;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    String query;

    // objetos de datos
    CLista data = new CLista();
    int iValor = 1;
    DataMantPreguntas dMantPreguntas;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    dMantPreguntas = (DataMantPreguntas) param.firstElement();

    // modos de operaci�n
    switch (opmode) {

      // alta de lista de valores
      case servletALTA:

        // prepara la query
        query = "insert into SIVE_PREGUNTA (CD_TSIVE, CD_PREGUNTA, CD_LISTA," +
            "DS_PREGUNTA, DSL_PREGUNTA, CD_TPREG, NM_LONG, NM_ENT, NM_DEC, CD_OPE," +
            "FC_ULTACT) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        st = con.prepareStatement(query);
        // codigo SIVE
        st.setString(1, dMantPreguntas.getTSive());
        // codigo pregunta
        st.setString(2, dMantPreguntas.getCodPregunta().trim().toUpperCase());
        // codigo lista
        if (dMantPreguntas.getListaValores().trim().length() > 0) {
          st.setString(3, dMantPreguntas.getListaValores().trim());
        }
        else {
          st.setNull(3, java.sql.Types.VARCHAR);
          // descripci�n pregunta
        }
        st.setString(4, dMantPreguntas.getDescPregunta().trim());
        // descripci�n local pregunta
        if (dMantPreguntas.getDescLPregunta().trim().length() > 0) {
          st.setString(5, dMantPreguntas.getDescLPregunta().trim());
        }
        else {
          st.setNull(5, java.sql.Types.VARCHAR);
          // c�digo TPREG
        }
        st.setString(6, dMantPreguntas.getTipo().trim());
        // nm_long
        if (dMantPreguntas.getLongitud() > 0) {
          st.setInt(7, dMantPreguntas.getLongitud());
        }
        else {
          st.setNull(7, java.sql.Types.INTEGER);
          // nm_ent
        }
        if (dMantPreguntas.getEnteros() > 0) {
          st.setInt(8, dMantPreguntas.getEnteros());
        }
        else {
          st.setNull(8, java.sql.Types.INTEGER);
          // nm_dec
        }
        if (dMantPreguntas.getDecimales() > 0) {
          st.setInt(9, dMantPreguntas.getDecimales());
        }
        else {
          st.setNull(9, java.sql.Types.INTEGER);
          // c�digo operador
        }
        st.setString(10, param.getLogin());
        // fecha �ltima actualizaci�n
        st.setDate(11, new java.sql.Date( (new java.util.Date()).getTime()));
        // lanza la query
        st.executeUpdate();
        st.close();

        break;

        // Baja de lista de valores
      case servletBAJA:
        query = "DELETE FROM SIVE_PREGUNTA WHERE " +
            " CD_TSIVE = ? and CD_PREGUNTA = ?";
        st = con.prepareStatement(query);

        //Pregunta de Brotes o EDO MLM 20-10-99
        st.setString(1, param.getTSive().trim());

        st.setString(2, dMantPreguntas.getCodPregunta().trim());
        st.executeUpdate();
        st.close();

        break;

        // b�squeda de listas de valores
      case servletSELECCION_X_CODIGO:
      case servletSELECCION_X_DESCRICPCION:

        // prepara la query
        // ARG: a�ade los upper (7-5-02)
        if (param.getFilter().length() > 0) {
          if (opmode == servletSELECCION_X_CODIGO) {
            query = "select CD_PREGUNTA, CD_LISTA, DS_PREGUNTA," +
                " DSL_PREGUNTA, CD_TPREG, NM_LONG, NM_ENT, NM_DEC" +
                " from SIVE_PREGUNTA where CD_TSIVE = ? and " +
                " CD_PREGUNTA like ? and CD_PREGUNTA > ? order by CD_PREGUNTA";
          }
          else {
            query = "select CD_PREGUNTA, CD_LISTA, DS_PREGUNTA," +
                " DSL_PREGUNTA, CD_TPREG, NM_LONG, NM_ENT, NM_DEC" +
                " from SIVE_PREGUNTA where CD_TSIVE = ? and " +
                " upper(DS_PREGUNTA) like upper(?) and " +
                " upper(DS_PREGUNTA) > upper(?) order by DS_PREGUNTA";
          }
        }

        //peticion de la primera trama
        else {
          if (opmode == servletSELECCION_X_CODIGO) {
            query = "select CD_PREGUNTA, CD_LISTA, DS_PREGUNTA," +
                " DSL_PREGUNTA, CD_TPREG, NM_LONG, NM_ENT, NM_DEC" +
                " from SIVE_PREGUNTA where CD_TSIVE = ? and " +
                " CD_PREGUNTA like ? order by CD_PREGUNTA";
          }
          else {
            query = "select CD_PREGUNTA, CD_LISTA, DS_PREGUNTA," +
                " DSL_PREGUNTA, CD_TPREG, NM_LONG, NM_ENT, NM_DEC" +
                " from SIVE_PREGUNTA where CD_TSIVE = ? and " +
                " upper(DS_PREGUNTA) like upper(?) order by DS_PREGUNTA";
          }
        }

        // prepara la lista de resultados
        data = new CLista();
        st = con.prepareStatement(query);

        //Pregunta de Brotes o EDO MLM 20-10-99
        st.setString(1, param.getTSive().trim());

        // filtro
        if (opmode == servletSELECCION_X_CODIGO) {
          st.setString(2, dMantPreguntas.getCodPregunta().trim() + "%");
        }
        else {
          st.setString(2, "%" + dMantPreguntas.getCodPregunta().trim() + "%");

          // paginaci�n
        }
        if (param.getFilter().length() > 0) {
          st.setString(3, param.getFilter().trim());
        }
        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {

          // control de tama�o
          if (iValor > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            if (opmode == servletSELECCION_X_CODIGO) {
              data.setFilter( ( (DataMantPreguntas) data.lastElement()).
                             getCodPregunta());
            }
            else {
              data.setFilter( ( (DataMantPreguntas) data.lastElement()).
                             getDescPregunta());

            }
            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }

          // lee el registro
          sCodPregunta = rs.getString("CD_PREGUNTA");
          sDescPregunta = rs.getString("DS_PREGUNTA");
          sDescLPregunta = rs.getString("DSL_PREGUNTA");
          sListaValores = rs.getString("CD_LISTA");
          iTipo = rs.getString("CD_TPREG");
          iLongitud = rs.getInt("NM_LONG");
          iEnteros = rs.getInt("NM_ENT");
          iDecimales = rs.getInt("NM_DEC");

          //____________________

          // obtiene la descripcion principal en funci�n del idioma si el cliente no es una CListaMantenimiento
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (param.getMantenimiento() == false)) {
            if (sDescLPregunta != null) {
              sDescPregunta = sDescLPregunta;
            }
          }

          //____________________

          // A�ade un nodo
          data.addElement(new DataMantPreguntas(sCodPregunta,
                                                sDescPregunta,
                                                sDescLPregunta,
                                                sListaValores,
                                                iTipo,
                                                iLongitud,
                                                iEnteros,
                                                iDecimales));

          iValor++;
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        // recupera las descripciones de las listas
        st = con.prepareStatement(
            "select DS_LISTA, DSL_LISTA from SIVE_LISTAS where CD_LISTA = ?");

        // recorre la lista para recuperar la descripci�n de la lista de valores
        for (int j = 0; j < data.size(); j++) {

          dMantPreguntas = (DataMantPreguntas) data.elementAt(j);
          if (dMantPreguntas.getListaValores().length() > 0) {
            st.setString(1, dMantPreguntas.getListaValores());
            rs = st.executeQuery();
            if (rs.next()) {
              sDescLista = null;

              // toma la descripci�n local
              if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
                sDescLista = rs.getString("DSL_LISTA");

                // toma la descripci�n en castellano
              }
              if (sDescLista == null) {
                sDescLista = rs.getString("DS_LISTA");

              }
              dMantPreguntas.setDescLista(sDescLista);
            }
            rs.close();
            rs = null;
          }
        }

        st.close();

        break;

        // modifica de listas de valores
      case servletMODIFICAR:

        // prepara la query
        query = "update SIVE_PREGUNTA set CD_LISTA=?, " +
            " DS_PREGUNTA=?, DSL_PREGUNTA=?, CD_TPREG=?," +
            " NM_LONG=?, NM_ENT=?, NM_DEC=?, FC_ULTACT=? " +
            " where CD_TSIVE = ? and CD_PREGUNTA =?";

        st = con.prepareStatement(query);
        // c�digo lista
        if (dMantPreguntas.getListaValores().trim().length() > 0) {
          st.setString(1, dMantPreguntas.getListaValores().trim());
        }
        else {
          st.setNull(1, java.sql.Types.VARCHAR);
          // descripci�n pregunta
        }
        st.setString(2, dMantPreguntas.getDescPregunta().trim());
        // descripci�n local pregunta
        if (dMantPreguntas.getDescLPregunta().trim().length() > 0) {
          st.setString(3, dMantPreguntas.getDescLPregunta().trim());
        }
        else {
          st.setNull(3, java.sql.Types.VARCHAR);
          // tipo pregunta
        }
        st.setString(4, dMantPreguntas.getTipo().trim());
        // longitud
        if (dMantPreguntas.getLongitud() > 0) {
          st.setInt(5, dMantPreguntas.getLongitud());
        }
        else {
          st.setNull(5, java.sql.Types.INTEGER);
          // enteros
        }
        if (dMantPreguntas.getEnteros() > 0) {
          st.setInt(6, dMantPreguntas.getEnteros());
        }
        else {
          st.setNull(6, java.sql.Types.INTEGER);
          // decimales
        }
        if (dMantPreguntas.getDecimales() > 0) {
          st.setInt(7, dMantPreguntas.getDecimales());
        }
        else {
          st.setNull(7, java.sql.Types.INTEGER);
          // fecha actualizaci�n
        }
        st.setDate(8, new java.sql.Date( (new java.util.Date()).getTime()));

        //WHERE (PK a modif)
        st.setString(9, dMantPreguntas.getTSive().trim());
        st.setString(10, dMantPreguntas.getCodPregunta().trim());

        st.executeUpdate();
        st.close();

        break;

      case servletOBTENER_X_CODIGO:
      case servletOBTENER_X_DESCRICPCION:
        if (opmode == servletOBTENER_X_CODIGO) {

          // prepara la query
          query = "select CD_PREGUNTA, CD_LISTA, DS_PREGUNTA," +
              " DSL_PREGUNTA, CD_TPREG, NM_LONG, NM_ENT, NM_DEC" +
              " from SIVE_PREGUNTA where CD_TSIVE = ? and CD_PREGUNTA like ?";
          //******* like y no = para optar entre indicar la CA o no en el c�digo
        }
        else {

          // prepara la query
          query = "select CD_PREGUNTA, CD_LISTA, DS_PREGUNTA," +
              " DSL_PREGUNTA, CD_TPREG, NM_LONG, NM_ENT, NM_DEC" +
              " from SIVE_PREGUNTA where CD_TSIVE = ? and DS_PREGUNTA = ?";

          // prepara la lista de resultados
        }
        data = new CLista();
        st = con.prepareStatement(query);

        //Pregunta de Brotes o EDO MLM 20-10-99
        st.setString(1, param.getTSive().trim());

        // c�digo
        if (opmode == servletOBTENER_X_CODIGO) { //*****************
          st.setString(2, dMantPreguntas.getCodPregunta().trim() + "%");
        }
        else {
          st.setString(2, dMantPreguntas.getCodPregunta().trim());
        }
        rs = st.executeQuery();

        // extrae el registro encontrado
        while (rs.next()) {

          // lee el registro
          sCodPregunta = rs.getString("CD_PREGUNTA");
          sDescPregunta = rs.getString("DS_PREGUNTA");
          sDescLPregunta = rs.getString("DSL_PREGUNTA");
          sListaValores = rs.getString("CD_LISTA");
          iTipo = rs.getString("CD_TPREG");
          iLongitud = rs.getInt("NM_LONG");
          iEnteros = rs.getInt("NM_ENT");
          iDecimales = rs.getInt("NM_DEC");

          //____________________
          // obtiene la descripcion principal en funci�n del idioma si el cliente no es una CListaMantenimiento
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (param.getMantenimiento() == false)) {
            if (sDescLPregunta != null) {
              sDescPregunta = sDescLPregunta;
            }
          }
          //____________________

          // A�ade un nodo
          data.addElement(new DataMantPreguntas(sCodPregunta,
                                                sDescPregunta,
                                                sDescLPregunta,
                                                sListaValores,
                                                iTipo,
                                                iLongitud,
                                                iEnteros,
                                                iDecimales));

        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        // recupera las descripciones de las listas
        st = con.prepareStatement(
            "select DS_LISTA, DSL_LISTA from SIVE_LISTAS where CD_LISTA = ?");

        // recorre la lista para recuperar la descripci�n de la lista de valores
        for (int j = 0; j < data.size(); j++) {

          dMantPreguntas = (DataMantPreguntas) data.elementAt(j);
          if (dMantPreguntas.getListaValores().length() > 0) {
            st.setString(1, dMantPreguntas.getListaValores());
            rs = st.executeQuery();
            if (rs.next()) {
              sDescLista = null;

              // toma la descripci�n local
              if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
                sDescLista = rs.getString("DSL_LISTA");

                // toma la descripci�n en castellano
              }
              if (sDescLista == null) {
                sDescLista = rs.getString("DS_LISTA");

              }
              dMantPreguntas.setDescLista(sDescLista);
            }
            rs.close();
            rs = null;
          }
        }

        st.close();

        break;

        // pregunta seleccionada
      case servletPREGUNTA_SELECCIONADA:
        String sIndicador = "N";
        query = "select COUNT(*) from SIVE_LINEA_ITEM where CD_PREGUNTA = ?";
        st = con.prepareStatement(query);
        st.setString(1, dMantPreguntas.getCodPregunta().trim());
        rs = st.executeQuery();

        if (rs.next()) {
          if (rs.getInt(1) > 0) {
            sIndicador = "S";

          }
        }
        rs.close();
        st.close();

        data.addElement(sIndicador);

        break;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();
    }
    return data;
  }
}
