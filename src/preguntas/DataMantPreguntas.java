package preguntas;

import java.io.Serializable;

public class DataMantPreguntas
    implements Serializable {
  protected String sTSive;
  protected String sCodPregunta;
  protected String sDescPregunta;
  protected String sDescLPregunta;
  protected String sListaValores;
  protected String sDescLista;
  protected String iTipo;
  protected int iLongitud;
  protected int iEnteros;
  protected int iDecimales;

  public static final String tipoBOOLEANO = "B";
  public static final String tipoCADENA = "C";
  public static final String tipoNUMERICO = "N";
  public static final String tipoFECHA = "F";
  public static final String tipoLISTA = "L";

  public DataMantPreguntas(String CodPregunta,
                           String DescPregunta,
                           String DescLPregunta,
                           String ListaValores,
                           String Tipo,
                           int Longitud,
                           int Enteros,
                           int Decimales) {
    sTSive = null;
    sCodPregunta = CodPregunta;
    sDescPregunta = DescPregunta;
    sDescLPregunta = DescLPregunta;
    if (sDescLPregunta == null) {
      sDescLPregunta = "";
    }
    sListaValores = ListaValores;
    if (sListaValores == null) {
      sListaValores = "";
    }
    sDescLista = null;
    iTipo = Tipo;
    iLongitud = Longitud;
    iEnteros = Enteros;
    iDecimales = Decimales;
  }

  public DataMantPreguntas(String TSive,
                           String CodPregunta,
                           String DescPregunta,
                           String DescLPregunta,
                           String ListaValores,
                           String Tipo,
                           int Longitud,
                           int Enteros,
                           int Decimales) {
    sTSive = TSive;
    sCodPregunta = CodPregunta;
    sDescPregunta = DescPregunta;
    sDescLPregunta = DescLPregunta;
    if (sDescLPregunta == null) {
      sDescLPregunta = "";
    }
    sListaValores = ListaValores;
    if (sListaValores == null) {
      sListaValores = "";
    }
    sDescLista = null;
    iTipo = Tipo;
    iLongitud = Longitud;
    iEnteros = Enteros;
    iDecimales = Decimales;
  }

  public DataMantPreguntas(String CodPregunta) {
    sCodPregunta = CodPregunta;
    sDescPregunta = null;
    sDescLPregunta = null;
    sListaValores = null;
    sDescLista = null;
    iTipo = null;
    iLongitud = 0;
    iEnteros = 0;
    iDecimales = 0;
  }

  public String getCodPregunta() {
    return sCodPregunta;
  }

  public String getDescPregunta() {
    return sDescPregunta;
  }

  public String getDescLPregunta() {
    return sDescLPregunta;
  }

  public String getListaValores() {
    return sListaValores;
  }

  public String getDescLista() {
    return sDescLista;
  }

  public void setDescLista(String sDes) {
    sDescLista = sDes;
  }

  public int getDecimales() {
    return iDecimales;
  }

  public int getEnteros() {
    return iEnteros;
  }

  public int getLongitud() {
    return iLongitud;
  }

  public String getTipo() {
    return iTipo;
  }

  public String getTSive() {
    return sTSive;
  }
}
