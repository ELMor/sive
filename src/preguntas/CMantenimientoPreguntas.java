package preguntas;

import capp.CApp;
import capp.CListaMantenimiento;
import comun.constantes;
import sapp.StubSrvBD;

public class CMantenimientoPreguntas
    extends CListaMantenimiento {

  public CMantenimientoPreguntas(MantDePreguntas a) {
    super( (CApp) a,
          3,
          "100\n400\n62",
          a.res.getString("msg1.Text"),
          new StubSrvBD(),
          a.strSERVLET,
          a.servletSELECCION_X_CODIGO,
          a.servletSELECCION_X_DESCRIPCION);
    setBorde(false);
  }

  public CMantenimientoPreguntas(MantDePreguntas a, boolean USU) {
    super( (CApp) a,
          3,
          "100\n400\n62",
          a.res.getString("msg1.Text"),
          new StubSrvBD(),
          a.strSERVLET,
          a.servletSELECCION_X_CODIGO,
          a.servletSELECCION_X_DESCRIPCION,
          constantes.keyBtnAnadir,
          constantes.keyBtnModificar,
          constantes.keyBtnBorrar,
          constantes.keyBtnAux
          );
    setBorde(false);
  }

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {

    modoOperacion = modoESPERA;
    Inicializar();
    Pan_MantPreguntas panel = new Pan_MantPreguntas(this.app,
        Pan_MantPreguntas.modoALTA,
        null);
    panel.show();

    if ( (panel.bAceptar) && (panel.valido)) {
      lista.addElement(panel.pregunta);
      RellenaTabla();
    }

    panel = null;

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // modifica l�nea de la lista
  public void btnModificar_actionPerformed(int i) {

    modoOperacion = modoESPERA;
    Inicializar();
    Pan_MantPreguntas panel = new Pan_MantPreguntas(this.app,
        Pan_MantPreguntas.modoMODIFICAR,
        (DataMantPreguntas) lista.elementAt(i));

    panel.show();

    if ( (panel.bAceptar) && (panel.valido)) {
      lista.removeElementAt(i);
      lista.insertElementAt(panel.pregunta, i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed(int i) {

    modoOperacion = modoESPERA;
    Inicializar();
    Pan_MantPreguntas panel = new Pan_MantPreguntas(this.app,
        Pan_MantPreguntas.modoBAJA,
        (DataMantPreguntas) lista.elementAt(i));
    panel.show();

    if (panel.bAceptar) {
      lista.removeElementAt(i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // rellena los par�metros para enviar al servlet
  public Object setComponente(String s) {
    return new DataMantPreguntas(s);
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(Object o) {
    DataMantPreguntas preg = (DataMantPreguntas) o;

    return preg.getCodPregunta() + "&" + preg.getDescPregunta() + "&" +
        preg.getTipo();
  }

}
