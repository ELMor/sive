package conspacenf;

import java.util.ResourceBundle;

import capp.CApp;
import sapp.StubSrvBD;

public class conspacenf
    extends CApp {

  protected StubSrvBD stubCliente = new StubSrvBD();
  ResourceBundle res;

  public conspacenf() {
  }

  public void init() {
    super.init();
  }

  public void start() {

    PnlParamC3 pParamC3 = null;
    CApp a = (CApp)this;

    res = ResourceBundle.getBundle("conspacenf.Res" + this.getIdioma());
    //// System_out.println("Iniciamos applet conspacenf.ApMantu");
    setTitulo(res.getString("msg1.Text"));
    pParamC3 = new PnlParamC3(a);
    VerPanel(res.getString("Pacientes"), pParamC3, false);
    VerPanel(res.getString("msg2.Text"));

  }

  public void stop() {
  }

} //__________________________________________________ END_CLASS
