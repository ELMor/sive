package conspacenf;

import java.util.Vector;

import capp.CLista;
import sapp.DBServlet;

/**
 *   lista de la clasificaci�n del diagn�stico
 */
public class DataClasifDiag
    extends Data {

  /** campos que va a tener */
  public static final int CD_CLASIFDIAG = 1;
  public static final int DS_CLASIFDIAG = 2;

  /** parametros que tiene */
  // n�mero de par�metros de esta sentencia
  public static final int P_NUM_CLASIFDIAG = 1;

  // posici�n de los diferentes par�metrso en la select
  public static final int P_CD_CLASIFDIAG = 1;

  /** modos que tiene */

  public static final String sqlCLASIF = "SELECT " +
      " CD_CLASIFDIAG, DS_CLASIFDIAG FROM SIVE_CLASIF_EDO ";

  public DataClasifDiag(int a_campo_filtro) {
    super(a_campo_filtro);
    // siempre hay que reservar
    arrayDatos = new Object[3];
  }

  public Object getNewData() {
    return new DataClasifDiag(campo_filtro);
  }

  /** devuelve el c�digo de un modo e funcionamiento */
  public String getCode(int a_modo) {
    return "CD_CLASIFDIAG";
  }

  /** devuelve la descripci�n de un modo de funcionamiento */
  public String getDes(int a_modo) {
    return "DS_CLASIFDIAG";
  }

  /** indica si hay que a�adir un WHERE
   *  true si la sentencia no lleva WHERE
   *  FALSE si lo lleva
   */
  public boolean getWhere(int a_modo) {
    return true;
  }

  /** esta funci�n devuelve la sentencia a ejecutar */
  public int prepareSQLSentencia(int opmode, StringBuffer strSQL,
                                 CLista ini_param) {
    strSQL.append(sqlCLASIF);
    //// System_out.println("Report: " + sqlCLASIF);
    return DBServlet.maxSIZE; // por defecto
  }

  public String getCD_CLASIFDIAG() {
    if (arrayDatos.length > CD_CLASIFDIAG) {
      return (String) arrayDatos[CD_CLASIFDIAG];
    }
    else {
      return null;
    }
  }

  public String getDS_CLASIFDIAG() {
    if (arrayDatos.length > DS_CLASIFDIAG) {
      return (String) arrayDatos[DS_CLASIFDIAG];
    }
    else {
      return null;
    }
  }

  /** indica al report como leer los datos */
  public static Vector getFieldVector() {
    Vector retval = new Vector();

    retval.addElement("CD_CLASIFDIAG  = getCD_CLASIFDIAG");
    retval.addElement("DS_CLASIFDIAG  = getDS_CLASIFDIAG");

    return retval;
  }

} //____________________________________ END_CLASS