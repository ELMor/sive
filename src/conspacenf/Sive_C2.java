package conspacenf;

import java.util.Vector;

public class Sive_C2 {

  String DESDE;
  String HASTA;
  long CASOS;
  float TASAS;

  public Sive_C2() {
  }

  public Sive_C2(String desde, String hasta,
                 long casos, float tasas) {
    DESDE = desde;
    HASTA = hasta;
    CASOS = casos;
    TASAS = tasas;
  }

  public String toString() {
    return new String();
  }

  public static Vector getFieldVector() {
    Vector retval = new Vector();

    retval.addElement("DESDE  = getDESDE");
    retval.addElement("HASTA  = getHASTA");
    retval.addElement("CASOS  = getCASOS");
    retval.addElement("TASAS  = getTASAS");

    return retval;
  }

  public String getDESDE() {
    //return DESDE;
    return "1998 11";
  }

  public String getHASTA() {
    //return HASTA;
    return "1998 15";
  }

  public long getCASOS() {
    //return CASOS;
    return 5;
  }

  public float getTASAS() {
    //return TASAS;
    return 6;
  }

} // END_CLASE
