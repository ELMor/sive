package notmantenimiento;

import java.io.Serializable;

public class DataNotManten
    implements Serializable {
  protected int iNM_EDO = 0;
  protected String sFC_FECNOTIF = "";
  protected String sFC_RECEP = "";
  protected String sIT_PRIMERO = "";
  protected String sCD_E_NOTIF = "";
  protected String sCD_ZONA_EQ = "";
  protected String sCD_ZONA_CASO = "";
  protected int iCD_ENFERMO = 0;
  protected String sCD_ENFCIE = "";
  protected String sCD_ANOEPI = "";
  protected String sCD_SEMEPI = "";
  protected String sIT_COBERTURA = "";
  protected String sIT_RESSEM = "";

  public String EnfermedadGuichi = "";

  public DataNotManten(int codExped, String fecNot, String fecRecep,
                       String primero,
                       String codEq, String zonaEq, String zonaCaso,
                       int cdEnfermo, String cdEnfermedad,
                       String ano, String semana, String cobertura,
                       String ressem) {

    iNM_EDO = codExped;
    sFC_FECNOTIF = fecNot;
    sFC_RECEP = fecRecep;
    sIT_PRIMERO = primero;
    sCD_E_NOTIF = codEq;
    sCD_ZONA_EQ = zonaEq;
    sCD_ZONA_CASO = zonaCaso;
    iCD_ENFERMO = cdEnfermo;
    sCD_ENFCIE = cdEnfermedad;
    sCD_ANOEPI = ano;
    sCD_SEMEPI = semana;
    sIT_COBERTURA = cobertura;
    sIT_RESSEM = ressem;
  }

  public int getNM_EDO() {
    return iNM_EDO;
  }

  public String getFECNOTIF() {
    return sFC_FECNOTIF;
  }

  public String getFECRECEP() {
    return sFC_RECEP;
  }

  public boolean esPrimero() {
    if (sIT_PRIMERO.equals("S")) {
      return true;
    }
    else {
      return false;
    }
  }

  public String getEquipo() {
    return sCD_E_NOTIF;
  }

  public String getZonaEq() {
    return sCD_ZONA_EQ;
  }

  public String getZonaCaso() {
    return sCD_ZONA_CASO;
  }

  public int getEnfermo() {
    return iCD_ENFERMO;
  }

  public String getEnfermedad() {
    return sCD_ENFCIE;
  }

  public String getAno() {
    return sCD_ANOEPI;
  }

  public String getSemana() {
    return sCD_SEMEPI;
  }

  public String getCobertura() {
    return sIT_COBERTURA;
  }

  public String getRessem() {
    return sIT_RESSEM;
  }

  public void setNM_EDO(int elEDO) {
    iNM_EDO = elEDO;
  }

  public void setFECNOTIF(String laFec) {
    sFC_FECNOTIF = laFec;
  }

  public void getFECRECEP(String laFec) {
    sFC_RECEP = laFec;
  }

  public void setPrimero(String cad) {
    sIT_PRIMERO = cad;
  }

  public void setEquipo(String cad) {
    sCD_E_NOTIF = cad;
  }

  public void setZonaEq(String cad) {
    sCD_ZONA_EQ = cad;
  }

  public void setZonaCaso(String cad) {
    sCD_ZONA_CASO = cad;
  }

  public void setEnfermo(int num) {
    iCD_ENFERMO = num;
  }

  public void setEnfermedad(String cad) {
    sCD_ENFCIE = cad;
  }

  public void setAno(String cad) {
    sCD_ANOEPI = cad;
  }

  public void setSemana(String cad) {
    sCD_SEMEPI = cad;
  }

  public void setCobertura(String cad) {
    sIT_COBERTURA = cad;
  }

  public void setRessem(String cad) {
    sIT_RESSEM = cad;
  }

} //__________________________________ END CLASS
