package notmantenimiento;

import java.io.Serializable;

public class DataEnfVi
    implements Serializable {
  protected String sCD_ENFCIE = "";
  protected String sDS_ENFCIE = "";

  public DataEnfVi(String cod, String des) {
    sCD_ENFCIE = cod;
    sDS_ENFCIE = des;
  }

  public String getCod() {
    return sCD_ENFCIE;
  }

  public String getDes() {
    return sDS_ENFCIE;
  }
}
