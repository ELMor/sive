package notmantenimiento;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import Registro.RegistroConsultas;
import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvNotManten
    extends DBServlet {

  //Modos de operaci�n del Servlet
  public static final int servletSELECCION_ENFERMEDAD = 2;
  public static final int servletOBTENER_X_CODIGO = 3;
  public static final int servletOBTENER_X_DESCRIPCION = 4;
  public static final int servletSELECCION_X_CODIGO = 5;
  public static final int servletSELECCION_X_DESCRIPCION = 6;

  // ARG:
  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  // objetos JDBC
  protected Connection con = null;
  protected PreparedStatement st = null;
  protected String query = null;
  protected ResultSet rs = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    int i = 0;

    // objetos de datos
    CLista data = new CLista();
    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
    DataNotManten datos = null;
    DataEnfVi enferVi = null;
    String cadena = "";
    String fechaNac = "";
    String fechaRec = "";

    //Descripciones
    String sDesPro = "";
    String sDesProL = "";

    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    con.setAutoCommit(false);
    datos = (DataNotManten) param.firstElement();

    //para las fechas del IT_RESSEM
    java.util.Date dFecha;
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    //filtro para tramado
    //String filtroTrama = " and (NM_EDO > ? or CD_E_NOTIF > ?  or CD_ANOEPI > ? or  CD_SEMEPI > ? or FC_RECEP > ? or FC_FECNOTIF > ? )";
    //String filtroTrama = " and (NM_EDO > ? or CD_E_NOTIF > ?  or CD_ANOEPI > ? or  CD_SEMEPI > ? or FC_RECEP > ? or FC_FECNOTIF > ? )";
    String filtroTrama = " and (A.NM_EDO > ? or A.CD_E_NOTIF > ?  or A.CD_ANOEPI > ? or  A.CD_SEMEPI > ? or A.FC_RECEP > ? or A.FC_FECNOTIF > ? )";

    try {
      // modos de operaci�n
      switch (opmode) {

        // alta
        case servletSELECCION_ENFERMEDAD:

          // prepara la query
          query = "select * from sive_procesos where cd_enfcie in"
              +
              "(select cd_enfcie from sive_enferedo where cd_tvigi='I' or cd_tvigi='A')" +
              " order by DS_PROCESO";
          st = con.prepareStatement(query);
          // lanza la query
          st.executeQuery();
          rs = st.getResultSet();
          while (rs.next()) {

            sDesPro = rs.getString("DS_PROCESO");
            sDesProL = rs.getString("DSL_PROCESO");
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                && (sDesProL != null)) {
              enferVi = new DataEnfVi(rs.getString("CD_ENFCIE"), sDesProL);
            }
            else {
              enferVi = new DataEnfVi(rs.getString("CD_ENFCIE"), sDesPro);

            }

            data.addElement(enferVi);
            enferVi = null;
          }
          st.close();
          rs.close();
          st = null;
          rs = null;

          break;

          // b�squeda
        case servletSELECCION_X_CODIGO:
          boolean miBool = false;
          String n1 = "";
          String n2 = "";
          String zbs = "";

          // prepara la query
          // modificacion jlt 29/10/2001
          // optimizamos la consulta al realizar la b�squeda

          /*query = "select NM_EDO, FC_FECNOTIF, FC_RECEP, IT_PRIMERO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI "
                  +"from SIVE_NOTIF_EDOI ";
           */

          //////////////////////////////

          query =
              "SELECT A.NM_EDO, C.CD_ENFERMO, A.FC_FECNOTIF, A.FC_RECEP, A.IT_PRIMERO, "
              + " A.CD_E_NOTIF, A.CD_ANOEPI, A.CD_SEMEPI, "
              +
              " '' || B.CD_NIVEL_1 || '-' || B.CD_NIVEL_2 || '-' || B.CD_ZBS || '' ZONAEQ, "
              +
              " '' || C.CD_NIVEL_1 || '-' || C.CD_NIVEL_2 || '-' || C.CD_ZBS || '' ZONACASO, "
              + " C.CD_ENFCIE, D.IT_COBERTURA, E.IT_RESSEM "
              + " FROM SIVE_NOTIF_EDOI A,SIVE_E_NOTIF B,SIVE_EDOIND C, "
              + " SIVE_C_NOTIF D,SIVE_NOTIFEDO E ";

          //////////////////////////////

          if (datos.getNM_EDO() != 0) {
            //query += "where NM_EDO=? ";
            query += "where A.NM_EDO=? ";
            miBool = true;
          }
          if (datos.getEnfermo() != 0) {
            if (!miBool) {
              query += "where  ";
              miBool = true;
            }
            else {
              query += "and ";
            }
            //query += " NM_EDO in (select NM_EDO from sive_edoind where CD_ENFERMO=?) ";
            query += "  C.CD_ENFERMO=? ";
          }
          if ( (!datos.getEnfermedad().equals("")) && (datos.getEnfermedad() != null)) {
            if (!miBool) {
              query += "where  ";
              miBool = true;
            }
            else {
              query += "and ";
            }
            //query += " NM_EDO in (select NM_EDO from sive_edoind where CD_ENFCIE=?) ";
            query += "  C.CD_ENFCIE=? ";
          }

          query += " AND A.CD_E_NOTIF = B.CD_E_NOTIF AND "
              + " A.NM_EDO = C.NM_EDO AND "
              + " A.CD_ANOEPI = E.CD_ANOEPI AND "
              + " A.CD_E_NOTIF = E.CD_E_NOTIF AND "
              + " A.CD_SEMEPI = E.CD_SEMEPI AND "
              + " A.FC_RECEP = E.FC_RECEP AND "
              + " A.FC_FECNOTIF = E.FC_FECNOTIF AND "
              + " B.CD_CENTRO = D.CD_CENTRO AND "
              + " A.CD_FUENTE = 'E'  ";

          if (!param.getFilter().equals("")) {
            query = query + filtroTrama;
          }

          //query = query + " and CD_FUENTE = 'E' order by NM_EDO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, FC_RECEP, FC_FECNOTIF ";

          query = query + " ORDER BY A.NM_EDO, A.CD_E_NOTIF, A.CD_ANOEPI, A.CD_SEMEPI, A.FC_RECEP, A.FC_FECNOTIF ";

          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);
          i = 1;

          if (datos.getNM_EDO() != 0) {
            st.setInt(i, datos.getNM_EDO());
            i++;
            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(String.valueOf(datos.getNM_EDO()));
          }

          if (datos.getEnfermo() != 0) {
            st.setInt(i, datos.getEnfermo());
            i++;
            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(String.valueOf(datos.getEnfermo()));
          }

          if ( (!datos.getEnfermedad().equals("")) && (datos.getEnfermedad() != null)) {
            st.setString(i, datos.getEnfermedad());
            i++;
            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(datos.getEnfermedad());
          }
          String auxF = "";
          int ind = 0;
          if (!param.getFilter().equals("")) {
            auxF = param.getFilter();

            //nm_edo

            st.setInt(i,
                      (new Integer(auxF.substring(0, auxF.indexOf(',', ind)).trim())).
                      intValue());
            i++;
            ind = auxF.indexOf(',', ind + 1);
            //cd_e_notif
            st.setString(i,
                         auxF.substring(ind + 1, auxF.indexOf(',', ind + 1)).trim());
            i++;
            ind = auxF.indexOf(',', ind + 1);
            //cd_anoepi
            st.setString(i,
                         auxF.substring(ind + 1, auxF.indexOf(',', ind + 1)).trim());
            i++;
            ind = auxF.indexOf(',', ind + 1);
            //cd_semepi
            st.setString(i,
                         auxF.substring(ind + 1, auxF.indexOf(',', ind + 1)).trim());
            i++;
            ind = auxF.indexOf(',', ind + 1);
            //fc_recep
            dFecha = formater.parse(auxF.substring(ind + 1,
                auxF.indexOf(',', ind + 1)).trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(i, sqlFec);
            i++;
            ind = auxF.indexOf(',', ind + 1);
            // fc_fecnotif
            dFecha = formater.parse(auxF.substring(ind + 1).trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(i, sqlFec);
            i++;

            // ARG: Se a�aden parametros
            //registroConsultas.insertarParametro(String.valueOf(auxF.substring(0,auxF.indexOf(',', ind)).trim()));
            ind = 0;
            registroConsultas.insertarParametro(auxF.substring(0,
                auxF.indexOf(',', ind)).trim());
            ind = auxF.indexOf(',', ind + 1);
            registroConsultas.insertarParametro(auxF.substring(ind + 1,
                auxF.indexOf(',', ind + 1)).trim());
            ind = auxF.indexOf(',', ind + 1);
            registroConsultas.insertarParametro(auxF.substring(ind + 1,
                auxF.indexOf(',', ind + 1)).trim());
            ind = auxF.indexOf(',', ind + 1);
            registroConsultas.insertarParametro(auxF.substring(ind + 1,
                auxF.indexOf(',', ind + 1)).trim());
            ind = auxF.indexOf(',', ind + 1);
            registroConsultas.insertarParametro("TO_DATE('" +
                                                auxF.substring(ind + 1,
                auxF.indexOf(',', ind + 1)).trim() + "','DD/MM/YYYY')");
            ind = auxF.indexOf(',', ind + 1);
            registroConsultas.insertarParametro("TO_DATE('" +
                                                auxF.substring(ind + 1).trim() +
                                                "','DD/MM/YYYY')");
          }

          rs = st.executeQuery();
          if (datos.getEnfermo() != 0) {

            // ARG: Registro LORTAD
            registroConsultas.registrar("SIVE_EDOIND", query, "CD_ENFERMO",
                                        String.valueOf(datos.getEnfermo()),
                                        "SrvNotManten", true);
          }
          else {

            // ARG: Registro LORTAD
            registroConsultas.registrar("SIVE_EDOIND", query, "CD_ENFERMO",
                                        "", "SrvNotManten", true);

            // extrae la p�gina requerida
          }
          i = 1;
          while (rs.next()) {
            // control de tama�o

            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              DataNotManten aux = (DataNotManten) data.lastElement();
              data.setFilter(aux.getNM_EDO() + ", " + aux.getEquipo() + ", " +
                             aux.getAno() + ", " + aux.getSemana() + ", " +
                             aux.getFECRECEP() + ", " + aux.getFECNOTIF());
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // a�ade un nodo
            fechaRec = "";
            fechaNac = "";
            fechaNac = formato.format(rs.getDate("FC_FECNOTIF"));
            fechaRec = formato.format(rs.getDate("FC_RECEP"));

                /*DataNotManten datosWhile = new DataNotManten(rs.getInt("NM_EDO"),fechaNac,
                 fechaRec,rs.getString("IT_PRIMERO"),rs.getString("CD_E_NOTIF"),
                 "","",0,"",rs.getString("CD_ANOEPI"),rs.getString("CD_SEMEPI"),"", "");
             */

            DataNotManten datosWhile = new DataNotManten(rs.getInt("NM_EDO"),
                fechaNac,
                fechaRec, rs.getString("IT_PRIMERO"), rs.getString("CD_E_NOTIF"),
                rs.getString("ZONAEQ"), rs.getString("ZONACASO"),
                rs.getInt("CD_ENFERMO"),
                rs.getString("CD_ENFCIE"), rs.getString("CD_ANOEPI"),
                rs.getString("CD_SEMEPI"),
                rs.getString("IT_COBERTURA"), rs.getString("IT_RESSEM"));

            data.addElement(datosWhile);

            i++;
          }
          i = 1;

          rs.close();
          rs = null;
          st.close();
          st = null;

          // modificacion jlt 29/10/2001
          // optimizamos la consulta
          // ahora recorremos la lista en busca de los equipos
          /* if (data.size() > 0){
             for (int j = 0;j < data.size();j++){
               //query = "select CD_NIVEL_1" + MAS + "'-'" + MAS + "CD_NIVEL_2" + MAS + "'-'" + MAS + "CD_ZBS codigo_z "
               //  +"from sive_e_notif where CD_E_NOTIF=?";
               String codigo_z = new String();
               String cd_n1 = "";
               String cd_n2 = "";
               String cd_zbs = "";
               query = "select CD_NIVEL_1,CD_NIVEL_2, CD_ZBS from sive_e_notif "
                 + " where CD_E_NOTIF = ?";
               st = con.prepareStatement(query);
               datos = null;
               datos = new DataNotManten(0,"","","","","","",0,"","","","", "");
               datos = (DataNotManten) data.elementAt(j);
               st.setString(1, datos.getEquipo());
               rs = st.executeQuery();
               if (rs.next()){
                 //datos.setZonaEq(rs.getString("codigo_z"));
                 cd_n1 = rs.getString("CD_NIVEL_1");
                 cd_n2 = rs.getString("CD_NIVEL_2");
                 cd_zbs = rs.getString("CD_ZBS");
               }
               codigo_z = cd_n1 + "-" + cd_n2 + "-" + cd_zbs;
               datos.setZonaEq(codigo_z);
               data.removeElementAt(j);
               data.insertElementAt(datos, j);
               rs.close();
               rs = null;
               st.close();
               st = null;
               //ahora buscamos la zonificaci�n del caso  y la enfermedad por si acaso
               query = "select CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, CD_ENFCIE "
                 +"from sive_edoind where NM_EDO = ?";
               st = con.prepareStatement(query);
               datos = null;
               datos = new DataNotManten(0,"","","","","","",0,"","","","", "");
               datos = (DataNotManten) data.elementAt(j);
               st.setInt(1, datos.getNM_EDO());
               // ARG: Se a�aden parametros
               registroConsultas.insertarParametro(String.valueOf(datos.getNM_EDO()));
               rs = st.executeQuery();
              // ARG: Registro LORTAD
              registroConsultas.registrar("SIVE_EDOIND",query,"CD_ENFERMO",
                                          "","SrvNotManten",true);
               if (rs.next()){
                 n1 = rs.getString("CD_NIVEL_1");
                 n2 = rs.getString("CD_NIVEL_2");
                 zbs = rs.getString("CD_ZBS");
               }
               if (n1 == null){
                 cadena = "";
               }else{
                 cadena = n1;
                 if (n2 != null){
                   cadena += "-" + n2;
                   if (zbs != null)
                     cadena += "-" + zbs;
                 }
               }
               datos.setZonaCaso(cadena);
               datos.EnfermedadGuichi = rs.getString("CD_ENFCIE");
               data.removeElementAt(j);
               data.insertElementAt(datos, j);
               rs.close();
               rs = null;
               st.close();
               st = null;
               // ahora tengo que obtener el campo IT_COBERTURA
               if (data.size() != 0){
                 for (int t=0;t < data.size();t++){
               query = "select IT_COBERTURA from sive_c_notif where CD_CENTRO in"
               +"(select CD_CENTRO from sive_e_notif where CD_E_NOTIF=?)";
                   st = con.prepareStatement(query);
                   // selecciono el equipo
               st.setString(1, ((DataNotManten)data.elementAt(t)).getEquipo());
                   // lanzo la query
                   rs = st.executeQuery();
                   if (rs.next()){
                     datos = null;
               datos = new DataNotManten(0,"","","","","","",0,"","","","", "");
                     datos = (DataNotManten)data.elementAt(t);
                     datos.setCobertura(rs.getString("IT_COBERTURA"));
                     data.removeElementAt(t);
                     data.insertElementAt(datos, t);
                   }
                   rs.close();
                   st.close();
                   rs = null;
                   st = null;
                 }
               }
               //cris
               // ahora tengo que obtener el campo IT_RESSEM
               if (data.size() != 0){
                 for (int t=0;t < data.size();t++){
                   query = " select IT_RESSEM from sive_notifedo "
               + " where CD_E_NOTIF = ? and CD_ANOEPI = ? and  CD_SEMEPI = ? "
                         + " and FC_RECEP = ? and FC_FECNOTIF = ? ";
                   st = con.prepareStatement(query);
                   // selecciono el equipo
               st.setString(1, ((DataNotManten)data.elementAt(t)).getEquipo());
               st.setString(2, ((DataNotManten)data.elementAt(t)).getAno());
               st.setString(3, ((DataNotManten)data.elementAt(t)).getSemana());
               dFecha = formater.parse(((DataNotManten)data.elementAt(t)).getFECRECEP());
                   sqlFec = new java.sql.Date(dFecha.getTime());
                   st.setDate(4, sqlFec);
               dFecha = formater.parse(((DataNotManten)data.elementAt(t)).getFECNOTIF());
                   sqlFec = new java.sql.Date(dFecha.getTime());
                   st.setDate(5, sqlFec);
                   // lanzo la query
                   rs = st.executeQuery();
                   if (rs.next()){
                     datos = null;
               datos = new DataNotManten(0,"","","","","","",0,"","","","", "");
                     datos = (DataNotManten)data.elementAt(t);
                     datos.setRessem(rs.getString("IT_RESSEM"));
                     data.removeElementAt(t);
                     data.insertElementAt(datos, t);
                   }
                   rs.close();
                   st.close();
                   rs = null;
                   st = null;
                 }
               }
              }
             } */
          break;
      }

      con.commit();
    }
    catch (Exception exs) {
      con.rollback();
      throw exs;

    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    if (data != null) {
      data.trimToSize();
    }
    return data;
  }

} //______________________________________ END CLASS
