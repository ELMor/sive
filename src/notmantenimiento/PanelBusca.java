package notmantenimiento;

import java.net.URL;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Choice;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import capp.CTabla;
import enfermo.DialBusEnfermo;
import enfermo.comun;
import infedoind.DialInfEdoInd;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import jclass.bwt.JCItemEvent;
import jclass.util.JCVector;
import notdata.DataListaEDOIndiv;
import notdata.DataListaNotifEDO;
import notdata.DataOpeFc;
import notindiv.DialogListaEDOIndiv;
import sapp.StubSrvBD;

public class PanelBusca
    extends CPanel {

  // variables usadas para saber provincia y municipio actuales
  protected CLista listaPanelCabecera = null;
  ResourceBundle res;
  protected CLista listaPanelCaso = null;
  protected String codProvi = "";
  protected String codMuni = "";
  protected CCargadorImagen imgs = null;

  //Modos de operaci�n de la ventana
  final int modoALTA = 0;
  final int modoBAJA = 5;
  final int modoMODIFICAR = 1;
  final int modoESPERA = 2;
  final int modoMODIFICACION = 4;
  final int modoCONSULTA = 6;
  final int modoMODIFY = 21;
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_LISTA_ENFER = 9;
  final int servletSELECCION_ENFERMEDAD = 2;

  // modos para la notificacion
  final int servletSELECCION_LISTA_NOTIF = 3;
  final int servletSELECCION_DATOS_NOTIF_SEM = 16;
  final int servletSELECCION_NOTIF_SEM_Y_LISTA_NOTIF = 18;

  final String strSERVLETEnfermedad = "servlet/SrvNotManten";
  final String strSERVLETCaso = "servlet/SrvNotManten";
  final String strSERVLET_EDOIND = "servlet/SrvEDOIndiv";
  final String strSERVLETnotif = "servlet/SrvNotifEDO";

  /** es el dato seleccionado */
  protected DataNotManten datCasoSeleccionado = null;
  protected Hashtable hashNotifEDO = null;

  XYLayout xYLayout1 = new XYLayout();
  Label lblExpediente = new Label();
  Label lblEnfermedad = new Label();
  CCampoCodigo txtExpediente = new CCampoCodigo();
  CCampoCodigo txtEnfermo = new CCampoCodigo();
  ButtonControl btnEnfermo = new ButtonControl();
  Label lblEnfermo = new Label();
  ButtonControl btnBuscar = new ButtonControl();
  ButtonControl btnModificar = null;
  ButtonControl btnBorrar = null;
  ButtonControl btnPrimero = null;
  ButtonControl btnAnterior = null;
  ButtonControl btnSiguiente = null;
  ButtonControl btnUltimo = null;

  ButtonControl btnListado = new ButtonControl();

  focusAdapter txtFocusAdapter = new focusAdapter(this); //cris

  public CTabla tabla = new CTabla();
  URL u;

  //lista seleccionada
  final int lCaso = 0;
  final int lNivel1 = 1;
  final int lZBS2 = 2;

  // Stub's
  protected StubSrvBD stubCaso = null;
  protected StubSrvBD stubEnfermedad = null;
  protected StubSrvBD stubCliente = null;
  protected StubSrvBD stubNotif = null;

  /** para la sincronizaci�n de eventos */
  protected boolean sinBloquear = true;

  // bot�n pulsado
  public boolean acepto = false;

  // par�metros
  protected int modoOperacion = modoALTA;
  protected int modoAnt = modoALTA;

  // listas de resultados
  public CLista listaCaso = null;
  public CLista listaEnf = null;

  PanelBusca_btnBuscar_actionAdapter btn_actionAdapter = null;
  BevelPanel bevelPanel1 = new BevelPanel();
  Choice chEnfermedad = new Choice();

  public PanelBusca(CApp a) {
    CMessage msgBox;
    CLista laList = new CLista();
    DataEnfVi enfermedades = null;
    this.app = (CApp) a;
    res = ResourceBundle.getBundle("notmantenimiento.Res" + a.getIdioma());
    try {
      listaCaso = new CLista();
      jbInit();
      listaCaso.setState(CLista.listaNOVALIDA);

      u = app.getCodeBase();
      stubCaso = new StubSrvBD(new URL(this.app.getURL() + strSERVLETCaso));
      stubEnfermedad = new StubSrvBD(new URL(this.app.getURL() +
                                             strSERVLETEnfermedad));
      listaEnf = new CLista();
      laList.addElement(new DataNotManten(0, "", "", "", "", "", "", 0, "", "",
                                          "", "", ""));
      laList.setIdioma(app.getIdioma());
      listaEnf = (CLista) stubEnfermedad.doPost(servletSELECCION_ENFERMEDAD,
                                                laList);
      // escribimos las enfermedades
      if (listaEnf.size() > 0) {
        chEnfermedad.addItem(""); //cris
      }
      for (int r = 0; r < listaEnf.size(); r++) {
        enfermedades = new DataEnfVi("", "");
        enfermedades = (DataEnfVi) listaEnf.elementAt(r);
        chEnfermedad.addItem(enfermedades.getDes());
        enfermedades = null;
      }
      btnEnfermo.setFocusAware(false);
      txtExpediente.requestFocus();
      stubCliente = new StubSrvBD(new URL(app.getURL() + strSERVLET_EDOIND));
    }
    catch (Exception ex) {
      ex.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
    }
  }

  /** ete m�todo sirve para mantener una sincronizaci�n en los eventos */
  public synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnt = modoOperacion;
      modoOperacion = modoESPERA;
      Inicializar();

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  public void llamaNotif() {
    String n1 = "";
    String n2 = "";
    String miCad = "";

    //limpio la hash
    hashNotifEDO = new Hashtable();

    int modo = modoOperacion;

    modoOperacion = modoESPERA;

    int iSel = tabla.getSelectedIndex();
    if (iSel != BWTEnum.NOTFOUND) {

      DataNotManten dataInd = (DataNotManten) listaCaso.elementAt(tabla.
          getSelectedIndex());
      hashNotifEDO.put("CD_ENFCIE", dataInd.getEnfermedad());
      hashNotifEDO.put("DS_PROCESO", chEnfermedad.getSelectedItem());
      //gui
      String cd = "";
      String ds = "";
      cd = (String) hashNotifEDO.get("CD_ENFCIE");
      if (cd == null) {
        cd = "";
      }
      ds = (String) hashNotifEDO.get("DS_PROCESO");
      if (ds == null) {
        ds = "";
      }
      if (cd.equals("")) {
        hashNotifEDO.put("CD_ENFCIE", dataInd.EnfermedadGuichi);
      }
      hashNotifEDO.put("DS_PROCESO", Coger_Enf(dataInd.EnfermedadGuichi));

      hashNotifEDO.put("NM_EDO", Integer.toString(dataInd.getNM_EDO()));
      miCad = dataInd.getZonaEq();
      n1 = miCad.substring(0, miCad.indexOf('-'));
      n2 = miCad.substring(miCad.indexOf('-') + 1, miCad.lastIndexOf('-'));
      hashNotifEDO.put("CD_NIVEL_1", n1);
      hashNotifEDO.put("CD_NIVEL_2_EQ", n2);
      // autorizaciones
      hashNotifEDO.put("CD_NIVEL_1_AUTORIZACIONES",
                       getApp().getCD_NIVEL_1_AUTORIZACIONES());
      hashNotifEDO.put("CD_NIVEL_2_AUTORIZACIONES",
                       getApp().getCD_NIVEL_2_AUTORIZACIONES());
      hashNotifEDO.put("PERFIL", Integer.toString(getApp().getPerfil()));
      hashNotifEDO.put("CD_ANOEPI", dataInd.getAno());
      hashNotifEDO.put("CD_SEMEPI", dataInd.getSemana());
      hashNotifEDO.put("FC_RECEP", dataInd.getFECRECEP());
      hashNotifEDO.put("FC_FECNOTIF", dataInd.getFECNOTIF());
      hashNotifEDO.put("CD_E_NOTIF", dataInd.getEquipo());
      hashNotifEDO.put("IT_FG_ENFERMO", app.getIT_FG_ENFERMO());

      //puedo entrar como modificacion o consulta o alta = modif
      //borrado maxivo no entra
      int iEstado = 0;

      if (this.app.getIT_AUTMOD().equals("S")) {
        iEstado = modoMODIFICACION;
      }
      else {
        iEstado = modoCONSULTA;

      }
      DialogListaEDOIndiv dlg = new DialogListaEDOIndiv(this.getApp(),
          "Indiv", stubCliente,
          "", iEstado,
          new DataListaEDOIndiv(), hashNotifEDO);

      dlg.setListaPanelCabecera(listaPanelCabecera);
      dlg.setListaPanelCaso(listaPanelCaso);
      dlg.show();
      listaPanelCabecera = dlg.getListaPanelCabecera();
      listaPanelCaso = dlg.getListaPanelCaso();

      if (dlg.iOut != -1) {
        DataListaEDOIndiv data = (DataListaEDOIndiv) dlg.getComponente();

        if (data != null) {
          // refrescamos la tabla
          tabla.clear();
          btnBuscar_actionPerformed(null);
        }
      }

      dlg = null;
    }
    //modoOperacion = modoNORMAL;
    //Inicializar();

    //applet.pMaestroEDO.Inicializar(modoNORMAL);
    modoOperacion = modo;
    //JM Inicializar();
    //applet.pListaEDONum.Inicializar(modo);
    //applet.pListaEDOIndiv.Inicializar(modo);

  }

  /** este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    modoOperacion = modoAnt;
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  void jbInit() throws Exception {

    final String imgNAME[] = {
        comun.imgLUPA,
        comun.imgREFRESCAR,
        "images/baja2.gif",
        comun.imgMODIFICAR2,
        comun.imgPRIMERO,
        comun.imgANTERIOR,
        comun.imgSIGUIENTE,
        comun.imgULTIMO,
        "images/declaracion2.gif",
        "images/grafico.gif"};
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    xYLayout1.setHeight(290);
    xYLayout1.setWidth(563);
    this.setLayout(xYLayout1);

    lblExpediente.setText(res.getString("lblExpediente.Text"));
    lblEnfermedad.setText(res.getString("lblEnfermedad.Text"));
    lblEnfermo.setText(res.getString("lblEnfermo.Text"));

    btnEnfermo = new ButtonControl(imgs.getImage(0));
    btnBuscar = new ButtonControl(imgs.getImage(1));
    btnModificar = new ButtonControl(imgs.getImage(3));
    btnBorrar = new ButtonControl(imgs.getImage(2));
    btnPrimero = new ButtonControl(imgs.getImage(4));
    btnAnterior = new ButtonControl(imgs.getImage(5));
    btnSiguiente = new ButtonControl(imgs.getImage(6));
    btnUltimo = new ButtonControl(imgs.getImage(7));
    btnListado.setImage(imgs.getImage(9));

    btnBuscar.setActionCommand("buscar");
    btnModificar.setActionCommand("modificar");
    btnBorrar.setActionCommand("borrar");
    btnPrimero.setActionCommand("primero");
    btnAnterior.setActionCommand("anterior");
    btnSiguiente.setActionCommand("siguiente");
    btnUltimo.setActionCommand("ultimo");
    btnListado.setActionCommand("GenListado");
    btn_actionAdapter = new PanelBusca_btnBuscar_actionAdapter(this);

    btnBuscar.setLabel(res.getString("btnBuscar.Label"));
    btnListado.setLabel(res.getString("btnListado.Label"));

    btnBuscar.addActionListener(btn_actionAdapter);
    btnModificar.addActionListener(btn_actionAdapter);
    btnBorrar.addActionListener(btn_actionAdapter);
    btnPrimero.addActionListener(btn_actionAdapter);
    btnAnterior.addActionListener(btn_actionAdapter);
    btnSiguiente.addActionListener(btn_actionAdapter);
    btnUltimo.addActionListener(btn_actionAdapter);

    btnEnfermo.addActionListener(new PanelBusca_btnEnfermo_actionAdapter(this));
    btnListado.addActionListener(btn_actionAdapter);

    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "-998\n92\n92\n-998\n-998\n-998\n-998"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tabla.ColumnButtonsStrings")), '\n'));
    tabla.setNumColumns(7);
    bevelPanel1.setBevelOuter(BevelPanel.LOWERED);
    bevelPanel1.setBevelInner(BevelPanel.FLAT);
    tabla.addItemListener(new PanelBusca_tabla_actionAdapter(this));
    tabla.addActionListener(new PanelBusca_tabla_dobleClick(this));

    txtExpediente.addFocusListener(txtFocusAdapter);

    this.add(lblExpediente, new XYConstraints(11, 5, 81, -1));
    this.add(lblEnfermedad, new XYConstraints(11, 67, 77, -1));
    this.add(txtExpediente, new XYConstraints(93, 5, 96, -1));
    this.add(txtEnfermo, new XYConstraints(93, 36, 57, -1));
    this.add(btnEnfermo, new XYConstraints(158, 36, 25, 25));
    this.add(chEnfermedad, new XYConstraints(93, 66, 270, -1));
    this.add(btnBuscar, new XYConstraints(445, 66, -1, -1));
    this.add(tabla, new XYConstraints(11, 117, 514, 124));
    this.add(lblEnfermo, new XYConstraints(11, 36, 79, -1));
    this.add(btnModificar, new XYConstraints(11, 249, 25, 25));
    this.add(btnBorrar, new XYConstraints(41, 249, 25, 25));
    this.add(btnListado, new XYConstraints(230, 249, -1, 26));
    this.add(btnPrimero, new XYConstraints(373, 249, 25, 25));
    this.add(btnAnterior, new XYConstraints(415, 249, 25, 25));
    this.add(btnSiguiente, new XYConstraints(457, 249, 25, 25));
    this.add(btnUltimo, new XYConstraints(499, 249, 25, 25));
    this.add(bevelPanel1, new XYConstraints(11, 101, 512, 4));

    acepto = false;
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

    modoOperacion = modoALTA;
    Inicializar();
  }

  public String Coger_Enf(String cd) {
    String retorno = "";
    for (int i = 0; i < listaEnf.size(); i++) {
      DataEnfVi cdds = (DataEnfVi) listaEnf.elementAt(i);
      if (cdds.getCod().equals(cd)) {
        retorno = cdds.getDes();
      }
    }
    return (retorno);
  }

  void btnEnfermo_actionPerformed(ActionEvent e) {

    CLista listaDeEnfermos = new CLista();
    Object o = new Object();
    enfermo.DataEnfermo miEnfermo = new enfermo.DataEnfermo("CD_ENFERMO");
    modoOperacion = modoESPERA;
    Inicializar();
    DialBusEnfermo dial = new DialBusEnfermo(this.app);
    dial.show();
    listaDeEnfermos = (CLista) dial.getListaDatosEnfermo();
    if (listaDeEnfermos != null) {
      if (listaDeEnfermos.size() > 0) {
        miEnfermo = (enfermo.DataEnfermo) listaDeEnfermos.firstElement();
        o = miEnfermo.get("CD_ENFERMO");
        txtEnfermo.setText(o.toString());
      }
    }
    dial.dispose();
    dial = null;
    modoOperacion = modoMODIFICAR;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:
        HabilitarBotonListado();
        tabla.setEnabled(true);
        btnEnfermo.setEnabled(true);
        btnEnfermo.setSelected(false);
        btnEnfermo.setEnabled(true);
        btnBuscar.setEnabled(true);

        btnModificar.setEnabled(true);
        if (this.app.getIT_AUTBAJA().equals("S")) {
          btnBorrar.setEnabled(true);
        }
        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);
        txtExpediente.setEditable(true);
        txtEnfermo.setEditable(true);
        chEnfermedad.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:
        HabilitarBotonListado();
        tabla.setEnabled(true);
        btnEnfermo.setEnabled(true);
        btnEnfermo.setSelected(false);

        btnModificar.setEnabled(true);
        if (this.app.getIT_AUTBAJA().equals("S")) {
          btnBorrar.setEnabled(true);
        }
        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);
        btnBuscar.setEnabled(true);

        txtExpediente.setEditable(true);
        //btnCentro.setEnabled(true);
        txtEnfermo.setEditable(true);
        chEnfermedad.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnListado.setEnabled(false);
        tabla.setEnabled(false);
        btnEnfermo.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBorrar.setEnabled(false);
        btnBuscar.setEnabled(false);
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        txtExpediente.setEditable(false);
        txtEnfermo.setEditable(false);
        chEnfermedad.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  // comprueba que se ha seleccionado un dato de la tabla
  /// y selecciona el dato a mandar
  protected boolean isDataValid() {
    CMessage msgBox;

    int index = tabla.getSelectedIndex();
    if (listaCaso != null && listaCaso.size() > index && index >= 0) {
      datCasoSeleccionado = (DataNotManten) listaCaso.elementAt(index);
      return true;
    }

    return false;
  }

  void btnCancelar_actionPerformed(ActionEvent e) {
    acepto = false;
  }

  public void btnBorrar_actionPerformed(ActionEvent e) {

    if (tabla.countItems() == 0) {
      return;
    }

    String n1 = "";
    String n2 = "";
    String miCad = "";
    CMessage msgBox = null;
    DataListaNotifEDO listEDO = new DataListaNotifEDO();
    DataListaNotifEDO listSEM = new DataListaNotifEDO();
    CLista listaOpeUltActTabla = new CLista();
    CLista listaNotifSem = new CLista();
    CLista listaNotifNot = new CLista();
    CLista listaPar = new CLista();
    Vector vnotifsem;
    Vector vnotifedo;
    DataOpeFc datosnotifsem;
    DataOpeFc datosnotifedo;

    hashNotifEDO = new Hashtable();
    int modo = modoOperacion;

    modoOperacion = modoESPERA;

    try {
      //si existen datos suficientes para poder borrar ESTA, continuamos
      int iSel = tabla.getSelectedIndex();
      if (iSel != BWTEnum.NOTFOUND) {

        DataNotManten dataInd = (DataNotManten) listaCaso.elementAt(tabla.
            getSelectedIndex());

        // obtenermos los datos de notif_sem
        stubNotif = null;
        stubNotif = new StubSrvBD(new URL(this.app.getURL() + strSERVLETnotif));
        listSEM = new DataListaNotifEDO("", "", "", "", "", "",
                                        dataInd.getEquipo(),
                                        dataInd.getAno(), dataInd.getSemana(),
                                        "", "", "", "", "", "", "", "");
        listaPar.addElement(listSEM);
        listaPar.setIdioma(app.getIdioma());
        listaNotifSem = (CLista) stubNotif.doPost(
            servletSELECCION_NOTIF_SEM_Y_LISTA_NOTIF, listaPar);
        if (listaNotifSem.size() > 0) {
          listSEM = (DataListaNotifEDO) listaNotifSem.elementAt(0);
          listEDO = (DataListaNotifEDO) listaNotifSem.elementAt(1);
        }

        // ahora constru�mos la listaOpe
        listaOpeUltActTabla.addElement(new DataOpeFc());
        // primero NOTIF SEM
        vnotifsem = new Vector();
        datosnotifsem = null;
        datosnotifsem = new DataOpeFc("CD_E_NOTIF", dataInd.getEquipo(), "C");
        vnotifsem.addElement(datosnotifsem);
        datosnotifsem = null;
        datosnotifsem = new DataOpeFc("CD_ANOEPI", dataInd.getAno(), "C");
        vnotifsem.addElement(datosnotifsem);
        datosnotifsem = null;
        datosnotifsem = new DataOpeFc("CD_SEMEPI", dataInd.getSemana(), "C");
        vnotifsem.addElement(datosnotifsem);
        datosnotifsem = null;
        datosnotifsem = new DataOpeFc(listSEM.getCD_OPE_NOTIF_SEM(),
                                      listSEM.getFC_ULTACT_NOTIF_SEM(),
                                      "SIVE_NOTIF_SEM", vnotifsem);
        listaOpeUltActTabla.addElement(datosnotifsem);
        // ahora NOTIFEDO
        vnotifedo = new Vector();
        datosnotifedo = null;
        datosnotifedo = new DataOpeFc("CD_E_NOTIF", dataInd.getEquipo(), "C");
        vnotifedo.addElement(datosnotifedo);
        datosnotifedo = null;
        datosnotifedo = new DataOpeFc("CD_ANOEPI", dataInd.getAno(), "C");
        vnotifedo.addElement(datosnotifedo);
        datosnotifedo = null;
        datosnotifedo = new DataOpeFc("CD_SEMEPI", dataInd.getSemana(), "C");
        vnotifedo.addElement(datosnotifedo);
        datosnotifedo = null;
        datosnotifedo = new DataOpeFc("FC_RECEP", listEDO.getFechaRecep(), "F");
        vnotifedo.addElement(datosnotifedo);
        datosnotifedo = null;
        datosnotifedo = new DataOpeFc("FC_FECNOTIF", listEDO.getFechaNotif(),
                                      "F");
        vnotifedo.addElement(datosnotifedo);
        datosnotifedo = null;
        datosnotifedo = new DataOpeFc(listEDO.getCD_OPE_NOTIFEDO(),
                                      listEDO.getFC_ULTACT_NOTIFEDO(),
                                      "SIVE_NOTIFEDO", vnotifedo);
        listaOpeUltActTabla.addElement(datosnotifedo);

        // ahora preparamos la llamada al di�logo
        hashNotifEDO.put("CD_ENFCIE", dataInd.getEnfermedad());
        hashNotifEDO.put("DS_PROCESO", chEnfermedad.getSelectedItem());
        hashNotifEDO.put("NM_EDO", Integer.toString(dataInd.getNM_EDO()));
        miCad = dataInd.getZonaEq();
        n1 = miCad.substring(0, miCad.indexOf('-'));
        n2 = miCad.substring(miCad.indexOf('-') + 1, miCad.lastIndexOf('-'));
        hashNotifEDO.put("CD_NIVEL_1", n1);
        hashNotifEDO.put("CD_NIVEL_2_EQ", n2);
        // autorizaciones
        hashNotifEDO.put("CD_NIVEL_1_AUTORIZACIONES",
                         getApp().getCD_NIVEL_1_AUTORIZACIONES());
        hashNotifEDO.put("CD_NIVEL_2_AUTORIZACIONES",
                         getApp().getCD_NIVEL_2_AUTORIZACIONES());
        hashNotifEDO.put("PERFIL", Integer.toString(getApp().getPerfil()));
        hashNotifEDO.put("CD_ANOEPI", dataInd.getAno());
        hashNotifEDO.put("CD_SEMEPI", dataInd.getSemana());
        hashNotifEDO.put("FC_RECEP", dataInd.getFECRECEP());
        hashNotifEDO.put("FC_FECNOTIF", dataInd.getFECNOTIF());
        hashNotifEDO.put("CD_E_NOTIF", dataInd.getEquipo());

        // recuperar listas de control de concurrencia para el borrado
        hashNotifEDO.put("IT_COBERTURA", dataInd.getCobertura());
        hashNotifEDO.put("IT_RESSEM", dataInd.getRessem());
        hashNotifEDO.put("listaOpeUltactTabla", listaOpeUltActTabla);

        DialogListaEDOIndiv dlg = new DialogListaEDOIndiv(this.getApp(),
            "Indiv",
            stubCliente,
            "",
            modoBAJA,
            new DataListaEDOIndiv(),
            hashNotifEDO);
        dlg.setListaPanelCabecera(listaPanelCabecera);
        dlg.setListaPanelCaso(listaPanelCaso);
        dlg.show();
        listaPanelCabecera = dlg.getListaPanelCabecera();
        listaPanelCaso = dlg.getListaPanelCaso();

        //si sali por borrar(aceptar) en el dialogo
        if (dlg.iOut != -1) {
          DataListaEDOIndiv data = (DataListaEDOIndiv) dlg.getComponente();

          if (data != null) {
            // refrescamos la tabla
            tabla.clear();
            btnBuscar_actionPerformed(null);

          }
        }
        //si sale por cancelar, nada
        dlg = null;

      }
      //no selcciona nada de la lista
      else {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg2.Text"));
        msgBox.show();
        msgBox = null;
      }

      // desde PanelMaestroEDO se ejecuta el modoINICIO total
      // ( PanelMaestroEDO + PanelListaEDONUm + PanelListaEDOIndiv )

      //old
      //applet.pMaestroEDO.Inicializar(modoINICIO);
    }
    catch (Exception erf) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, erf.getMessage());
      msgBox.show();
      msgBox = null;
    }
    //jm
    modoOperacion = modo;
    //JM Inicializar();

    //applet.pListaEDONum.Inicializar(modo);
    //applet.pListaEDOIndiv.Inicializar(modo);

  } //fin de borrar ****** ***** ***** ***** *****

  public void btnModificar_actionPerformed(ActionEvent e) {

    CMessage msgBox = null;

    if (tabla.countItems() == 0) {
      return;
    }

    // modificacion jlt
    int iSel = tabla.getSelectedIndex();
    if (iSel != BWTEnum.NOTFOUND) {

      llamaNotif();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            res.getString("msg2.Text"));
      msgBox.show();
      msgBox = null;
    }

  }

  boolean alMenosUnoInformado() { //cris
    if ( (!txtExpediente.getText().equals("")) ||
        (!txtEnfermo.getText().equals("")) ||
        (chEnfermedad.getSelectedIndex() > 0)) {
      return true;
    }
    else {
      return false;
    }
  }

  void btnBuscar_actionPerformed(ActionEvent e) {
    CLista datos = null;
    CMessage mensaje = null;
    String cad = new String("");
    DataNotManten daton1 = null;
    int expediente = 0;
    int enfermo = 0;
    String enfermedad = "";

    if (alMenosUnoInformado()) {

      listaCaso.setState(CLista.listaNOVALIDA);
      try {
        if (this.listaCaso.getState() == CLista.listaNOVALIDA) {
          datos = new CLista();

          datos.setFilter("");
          if (txtExpediente.getText().length() > 0) {
            expediente = Integer.parseInt(txtExpediente.getText());
          }
          if (txtEnfermo.getText().length() > 0) {
            enfermo = Integer.parseInt(txtEnfermo.getText());
          }
          if (chEnfermedad.getSelectedIndex() > 0) { //cris
            enfermedad = ( (DataEnfVi) listaEnf.elementAt(chEnfermedad.
                getSelectedIndex() - 1)).getCod();
          }
          else {
            enfermedad = "";

          }
          daton1 = new DataNotManten(expediente, "", "", "", "", "", "",
                                     enfermo, enfermedad, "", "", "", "");
          datos.addElement(daton1);
          datos.setIdioma(app.getIdioma());

          // ARG: Se introduce el login
          datos.setLogin(app.getLogin());
          // ARG: Se introduce la lortad
          datos.setLortad(app.getParameter("LORTAD"));

          listaCaso = (CLista)this.stubCaso.doPost(servletSELECCION_X_CODIGO,
              datos);

          /*      SrvNotManten srv = new SrvNotManten();
               srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
               "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                       "sive_desa",
                                       "sive_desa");
               listaCaso = srv.doDebug(servletSELECCION_X_CODIGO, datos);
           */

//SrvNotManten srv = new SrvNotManten();
//listaCaso = srv.doPrueba(servletSELECCION_X_CODIGO, datos);
//srv = null;

          datos = null;

        }
        writeListaCaso();

      }
      catch (Exception excepc) {
        excepc.printStackTrace();
        mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
        mensaje.show();
      }
    }
    else { //cris
      mensaje = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg3.Text"));
      mensaje.show();
    }

  }

  public void writeListaCaso() {
    DataNotManten data;
    CMessage msgBox;
    String cadLinea = "";
    JCVector row1 = new JCVector();
    JCVector items = new JCVector();

    tabla.clear();
    cadLinea = "";
    if (listaCaso.size() > 0) {

      // vuelca la lista
      for (int j = 0; j < listaCaso.size(); j++) {
        data = (DataNotManten) listaCaso.elementAt(j);
        row1 = new JCVector();
        row1.addElement(Integer.toString(data.getNM_EDO()));
        row1.addElement(data.getFECNOTIF().trim());
        row1.addElement(data.getFECRECEP().trim());
        if (data.esPrimero()) {
          row1.addElement(imgs.getImage(8));
        }
        else {
          row1.addElement(new String(""));
        }
        row1.addElement(data.getEquipo());
        row1.addElement(data.getZonaEq().trim());
        row1.addElement(data.getZonaCaso().trim());
        items.addElement(row1);

      }
      tabla.setItems(items);
      // opci�n m�s datos
      if (listaCaso.getState() == CLista.listaINCOMPLETA) {
        cadLinea = res.getString("msg4.Text") + "&" + "" + "&" + "" + "&" + "" +
            "&" + "" + "&" + "" + "&" + "";
        tabla.addItem(cadLinea, '&'); //'&' es el car�cter separador de datos (cada datos va a una columnna)
      }

      tabla.repaint();

      // mensaje de lista vacia
    }
    else {
      tabla.clear();
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            res.getString("msg5.Text"));
      msgBox.show();
      msgBox = null;
      this.modoOperacion = modoALTA;
    }
  }

  void btnAceptar_actionPerformed(ActionEvent e) {
    acepto = true;
  }

  void tabla_actionPerformed(JCItemEvent evt) {
    CMessage msgBox = null;
    DataNotManten datUsuario = null;
    CLista data = null;

    int tam;
    tam = tabla.countItems();

    //Obtengo el �ndice selccionado de tabla
    int indSel = tabla.getSelectedIndex();

    if (indSel != BWTEnum.NOTFOUND) {

      try {
        // a�ade una p�gina a la lista principal
        if ( (indSel == tam - 1) &&
            (listaCaso.getState() == CLista.listaINCOMPLETA)) {

          tabla.deselect(tabla.getSelectedIndex());

          // prepara los par�metros de llamada al servlet
          data = new CLista();

          // c�digo de filtrado
          /*data.addElement(new DataNotManten(txtCentro.getText().toUpperCase(),codProvi.toUpperCase(),
               codMuni.toUpperCase(),txtCentroDesc.getText(),"","","","","","","","","",
                this.app.getLogin(),"","",""));*/
          // �ltimo c�digo en el cliente

          data.setFilter(listaCaso.getFilter());
          //// System_out.println("filtro" + listaCaso.getFilter());

          // estado incompleto
          data.setState(listaCaso.getState());

          // idioma
          data.setIdioma(app.getIdioma());

          DataNotManten daton1 = null;
          int expediente = 0;
          int enfermo = 0;
          String enfermedad = "";

          if (alMenosUnoInformado()) {

            if (txtExpediente.getText().length() > 0) {
              expediente = Integer.parseInt(txtExpediente.getText());
            }
            if (txtEnfermo.getText().length() > 0) {
              enfermo = Integer.parseInt(txtEnfermo.getText());
            }
            if (chEnfermedad.getSelectedIndex() > 0) { //cris

              //enfermedad = ((DataEnfVi)listaEnf.elementAt(chEnfermedad.getSelectedIndex())).getCod();
              enfermedad = ( (DataEnfVi) listaEnf.elementAt(chEnfermedad.
                  getSelectedIndex() - 1)).getCod();
            }
            else {
              enfermedad = "";
            }
            daton1 = new DataNotManten(expediente, "", "", "", "", "", "",
                                       enfermo, enfermedad, "", "", "", "");
            data.addElement(daton1);
          }
          else { //cris
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                  res.getString("msg3.Text"));
            msgBox.show();
            msgBox = null;
          }
          // a�ade la lista
          data.setIdioma(app.getIdioma());

          // ARG: Se introduce el login
          data.setLogin(app.getLogin());
          // ARG: Se introduce la lortad
          data.setLortad(app.getParameter("LORTAD"));

          CLista aux = (CLista) stubCaso.doPost(servletSELECCION_X_CODIGO, data);

          /*SrvNotManten srv = new SrvNotManten();
               srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
               CLista aux = srv.doDebug(servletSELECCION_X_CODIGO, data);
           */

          listaCaso.appendData(aux);
          listaCaso.setState(aux.getState());
          // visualiza los datos
          writeListaCaso();
        }

        // selecciona el item
        else {
          btnModificar.setEnabled(true);
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

    } //if alguno seleccionado
  }

  //cris
  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {
    //en este caso solo es para el num de expediente
    if (txtExpediente.getText().equals("")) {
      txtEnfermo.setEnabled(true);
      chEnfermedad.setEnabled(true);
      txtEnfermo.requestFocus();
    }
    else {
      txtEnfermo.setEnabled(false);
      chEnfermedad.setEnabled(false);
      chEnfermedad.select(0);
      btnBuscar.requestFocus();
    }
  }

  void HabilitarBotonListado() {

    if (tabla.countItems() > 0) {
      btnListado.setEnabled(true);
    }
    else {
      btnListado.setEnabled(false);

    }
    this.doLayout();
  }

  void btnListado_actionPerformed(ActionEvent evt) {

    DialInfEdoInd dlgInforme = null;
    int indice = tabla.getSelectedIndex();
    CMessage msgBox = null;

    try {

      // ARG
      hashNotifEDO = new Hashtable();

      //DataNotManten dataInd = (DataNotManten)listaCaso.elementAt(tabla.getSelectedIndex());
      //hashNotifEDO.put("DS_PROCESO", Coger_Enf(dataInd.EnfermedadGuichi));

      //Si hay fila elegida
      if (indice != BWTEnum.NOTFOUND) {
        DataNotManten dataInd = (DataNotManten) listaCaso.elementAt(tabla.
            getSelectedIndex());
        hashNotifEDO.put("DS_PROCESO", Coger_Enf(dataInd.EnfermedadGuichi));
        dlgInforme = new DialInfEdoInd(app,
                                       Integer.toString( ( (DataNotManten) (
            listaCaso.elementAt(indice))).getNM_EDO()),
                                       (String) hashNotifEDO.get("DS_PROCESO").
                                       toString());

        dlgInforme.setEnabled(true);
        if (dlgInforme.GenerarInforme()) {
          dlgInforme.show();
        }
      }
      // modificacion jlt
      else {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg2.Text"));
        msgBox.show();
        msgBox = null;
      }

      // error al procesar
    }
    catch (Exception e) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }
  }

} // END CLASS

class PanelBusca_btnEnfermo_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelBusca adaptee;
  ActionEvent e;

  PanelBusca_btnEnfermo_actionAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnEnfermo_actionPerformed(e);
  }
}

class PanelBusca_btnBuscar_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelBusca adaptee;
  ActionEvent e;

  PanelBusca_btnBuscar_actionAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      new Thread(this).start();
    }
  }

  public void run() {
    int index;

    if (e.getActionCommand() == "buscar") { // lista principal
      adaptee.btnBuscar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("GenListado")) {
      adaptee.btnListado_actionPerformed(e);
    }
    else if (e.getActionCommand() == "modificar") {
      adaptee.btnModificar_actionPerformed(e);
    }
    else if (e.getActionCommand() == "borrar") {
      adaptee.btnBorrar_actionPerformed(e);
    }
    else if (e.getActionCommand() == "primero") {
      if (adaptee.tabla.countItems() > 0) {
        adaptee.tabla.select(0);
        adaptee.tabla.setTopRow(0);
      }
    }
    else if ( (e.getActionCommand() == "anterior")
             && (adaptee.tabla.countItems() > 0)) {
      index = adaptee.tabla.getSelectedIndex();
      if (index > 0) {
        index--;
        adaptee.tabla.select(index);
        if (index - adaptee.tabla.getTopRow() <= 0) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() - 1);
        }
      }
      else {
        adaptee.tabla.select(0);
        adaptee.tabla.setTopRow(0);
      }
    }
    else if ( (e.getActionCommand() == "siguiente")
             && ( (adaptee.tabla.countItems() > 0))) {
      int ultimo = adaptee.tabla.countItems() - 1;
      index = adaptee.tabla.getSelectedIndex();
      if (index < ultimo && index >= 0) {
        index++;
        adaptee.tabla.select(index);
        if (index - adaptee.tabla.getTopRow() >= 5) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() + 1);
        }
      }
      else {
        adaptee.tabla.select(0);
        adaptee.tabla.setTopRow(0);
      }
    }
    else if ( (e.getActionCommand() == "ultimo")
             && (adaptee.tabla.countItems() > 0)) {
      int ultimo = adaptee.tabla.countItems() - 1;
      if (ultimo >= 0) {
        index = ultimo;
        adaptee.tabla.select(index);
        if (adaptee.tabla.countItems() >= 5) {
          adaptee.tabla.setTopRow(adaptee.tabla.countItems() - 5);
        }
        else {
          adaptee.tabla.setTopRow(0);
        }
      }
    }
    adaptee.desbloquea();
  }
}

// escuchador de los click en la tabla
class PanelBusca_tabla_dobleClick
    implements jclass.bwt.JCActionListener, Runnable {
  PanelBusca adaptee;

  PanelBusca_tabla_dobleClick(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    if (adaptee.bloquea()) {
      new Thread(this).start();
    }
  }

  public void run() {
    adaptee.btnModificar_actionPerformed(null);
    adaptee.desbloquea();
  }
}

class PanelBusca_tabla_actionAdapter
    implements jclass.bwt.JCItemListener, Runnable {
  PanelBusca adaptee;
  JCItemEvent e;

  PanelBusca_tabla_actionAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(JCItemEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      new Thread(this).start();
    }
  }

  public void run() {
    if (e.getStateChange() == JCItemEvent.SELECTED) { //evento seleccion (no deseleccion)
      adaptee.tabla_actionPerformed(e);
    }
    adaptee.desbloquea();
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanelBusca adaptee;
  FocusEvent event;

  focusAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}
