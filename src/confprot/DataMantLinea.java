package confprot;

import java.io.Serializable;

public class DataMantLinea
    implements Serializable {

  int iLinea = 0;
  String sTipo = null;
  String sDescGeneral = null;
  String sDescLocal = null;
  String sCodPregunta = null;
  boolean bObligatoria = false;
  boolean bCondicionada = false;
  int iCondALinea = 0;
  String sCondAValor = null;

  // parametros de la pregunta
  String sTPreg = null;
  int iLong = 0;
  int iEnt = 0;
  int iDec = 0;

  public final static String tipoETIQUETA = "E";
  public final static String tipoPREGUNTA = "P";

  public DataMantLinea() {
  }

  public DataMantLinea(int Linea,
                       String Tipo,
                       String DescGeneral,
                       String DescLocal,
                       String CodPregunta,
                       boolean Obligatoria,
                       boolean Condicionada,
                       int CondALinea,
                       String CondAValor) {
    iLinea = Linea;
    sTipo = Tipo;
    sDescGeneral = DescGeneral;
    if (DescLocal != null) {
      if (DescLocal.length() > 0) {
        sDescLocal = DescLocal;
      }
    }
    if (CodPregunta != null) {
      if (CodPregunta.length() > 0) {
        sCodPregunta = CodPregunta;
      }
    }
    bObligatoria = Obligatoria;
    bCondicionada = Condicionada;
    iCondALinea = CondALinea;
    sCondAValor = CondAValor;
  }

  public DataMantLinea(int Linea,
                       String Tipo,
                       String DescGeneral,
                       String DescLocal) {
    iLinea = Linea;
    sTipo = Tipo;
    sDescGeneral = DescGeneral;
    sDescLocal = DescLocal;
    sCodPregunta = null;
    bObligatoria = false;
    bCondicionada = false;
    iCondALinea = 0;
    sCondAValor = "";
  }

}
