
package confprot;

import java.io.Serializable;

public class DataModelo
    implements Serializable {

  protected String sCD_TSIVE;
  protected String sCD_MODELO;
  protected String sDS_MODELO;
  protected String sDSL_MODELO;
  protected String sCD_NIVEL_1;
  protected String sCD_NIVEL_2;
  protected String sCD_CA;
  protected String sIT_OK;
  protected String sCD_ENFCIE;
  protected String sPartir;
  protected String sBaja;

  public DataModelo(String tSive, String cdMod, String dsMod, String dslMod,
                    String cdN1, String cdN2, String cdCA, String itOK,
                    String cdEnfcie, String Partir, String deBaja) {

    sCD_TSIVE = tSive;
    sCD_MODELO = cdMod;
    sDS_MODELO = dsMod;
    sDSL_MODELO = dslMod;
    sCD_NIVEL_1 = cdN1;
    sCD_NIVEL_2 = cdN2;
    sCD_CA = cdCA;
    sIT_OK = itOK;
    sCD_ENFCIE = cdEnfcie;
    sPartir = Partir;
    sBaja = deBaja;
  }

  public String getBaja() {
    return sBaja;
  }

  public String getCD_TSIVE() {
    return sCD_TSIVE;
  }

  public String getCD_Modelo() {
    return sCD_MODELO;
  }

  public String getDS_Modelo() {
    return sDS_MODELO;
  }

  public String getDSL_Modelo() {
    return sDSL_MODELO;
  }

  public String getCD_Nivel_1() {
    return sCD_NIVEL_1;
  }

  public String getCD_Nivel_2() {
    return sCD_NIVEL_2;
  }

  public String getCD_CA() {
    return sCD_CA;
  }

  public String getIT_OK() {
    return sIT_OK;
  }

  public String getCD_EnfCie() {
    return sCD_ENFCIE;
  }

  public String getPartir() {
    return sPartir;
  }
}
