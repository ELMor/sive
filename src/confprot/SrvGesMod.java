package confprot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Vector;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvGesMod
    extends DBServlet {

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletNO_OPERATIVO = 10;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {
    // Por si se desea filtrar por una enfermedad en concreto
    String sQueryEnfermedad = "";
    String sCdEnfermedad = null;

    // variables temporales
    String CD_TSIVE = "";
    String CD_MODELO = "";
    String DS_MODELO = "";
    String DSL_MODELO = "";
    String CD_NIVEL_1 = "";
    String CD_NIVEL_2 = "";
    String CD_CA = "";
    String IT_OK = "";
    String CD_ENFCIE = "";
    java.sql.Date FC_BAJAM = null;
    int numAux = 0;

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;
    int j = 1;

    // objetos de datos
    CLista data = new CLista();
    DataModelo dModelo = null;
    DataLineasM dLineas = null;
    DataLineaItem dItems = null;
    String fechaCadena = null;
    SimpleDateFormat formato1 = null;
    SimpleDateFormat formato2 = null;

    //registro que va a ser modificado
    DataModelo anterior = null;
    java.sql.Date bajaAnterior = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    dModelo = (DataModelo) param.firstElement();

    if (param.getCdEnfermedad() != null) {
      if (!param.getCdEnfermedad().equals("")) {
        sQueryEnfermedad = " CD_ENFCIE = ? and ";
        sCdEnfermedad = param.getCdEnfermedad();
      }
    }

    /*mlm:
     lo que habia:
     sAutorizacion1 = " and CD_NIVEL_1 IN (select CD_NIVEL_1 from SIVE_AUTORIZACIONES where CD_USUARIO = ?)";
     sAutorizacion2 = " and CD_NIVEL_2 IN (select CD_NIVEL_2 from SIVE_AUTORIZACIONES where CD_USUARIO = ? and CD_NIVEL_1 = ?)";
     nuevo:
     1,1,2
     2,3,1
     para sAutorizaciones2 debe salir solo 2,3 si nivel_1 era 1 */
    Vector vN1 = (Vector) param.getVN1();
    Vector vN2 = (Vector) param.getVN2();
    String n1 = "";
    String n1Fijo = "";
    int k = 0;
    for (k = 0; k < vN1.size(); k++) {
      n1 = n1 + ", " + (String) vN1.elementAt(k);
    }
    String n2 = "";
    for (k = 0; k < vN2.size(); k++) {
      /*if (nivel.getNivel1() != null)
        n1Fijo = nivel.getNivel1().trim(); */
      //quitar lo que sobra-si el correspondiente 1 es igual al fijo, ok)
      /*if ( ((String) vN1.elementAt(k)).equals(n1Fijo) ) */
      n2 = n2 + ", " + (String) vN2.elementAt(k);
    }
    //quitamos las comas primeras
    if (!n1.trim().equals("")) {
      n1 = n1.substring(1);
    }
    if (!n2.trim().equals("")) {
      n2 = n2.substring(1);
      //------------------------------------------------

    }
    data.setState(CLista.listaVACIA);
    data = null;
    try {

      // modos de operaci�n
      switch (opmode) {

        case servletALTA:

          // insertamos el modelo
          query = "insert into SIVE_MODELO " +
              " (CD_TSIVE, CD_MODELO, DS_MODELO," +
              " CD_NIVEL_1, DSL_MODELO, CD_NIVEL_2, " +
              " CD_CA, FC_ALTAM, IT_OK, FC_BAJAM," +
              " CD_OPE, CD_ENFCIE, FC_ULTACT) " +
              " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

          st = con.prepareStatement(query);
          // codigo TSIVE
          st.setString(1, dModelo.getCD_TSIVE().trim());
          // c�digo modelo
          st.setString(2, dModelo.getCD_Modelo().trim());
          // descripci�n modelo
          st.setString(3, dModelo.getDS_Modelo().trim());

          // c�digo nivel1
          if (dModelo.getCD_Nivel_1().trim().length() > 0) {
            st.setString(4, dModelo.getCD_Nivel_1().trim());
          }
          else {
            st.setNull(4, java.sql.Types.VARCHAR);
          }

          // descripci�n en ingl�s
          if (dModelo.getDSL_Modelo().trim().length() > 0) {
            st.setString(5, dModelo.getDSL_Modelo().trim());

          }
          else {
            st.setNull(5, java.sql.Types.VARCHAR);
          }

          // c�digo nivel2
          if (dModelo.getCD_Nivel_2().trim().length() > 0) {
            st.setString(6, dModelo.getCD_Nivel_2().trim());
          }
          else {
            st.setNull(6, java.sql.Types.VARCHAR);
          }

          // c�digo CCAA
          if (dModelo.getCD_CA().trim().length() > 0) {
            st.setString(7, dModelo.getCD_CA().trim());
          }
          else {
            st.setNull(7, java.sql.Types.VARCHAR);
          }

          // fecha alta
          st.setDate(8, new java.sql.Date( (new java.util.Date()).getTime()));
          // ok
          st.setString(9, dModelo.getIT_OK().trim());
          // fecha baja : alta=NULL
          st.setNull(10, java.sql.Types.VARCHAR);
          // c�digo operador
          st.setString(11, param.getLogin());

          // c�digo enfcie
          st.setString(12, dModelo.getCD_EnfCie().trim());
          // �ltima actualizaci�n
          st.setDate(13, new java.sql.Date( (new java.util.Date()).getTime()));

          st.executeUpdate();
          st.close();
          st = null;

          // Primero comprobamos que existe alg�n modelo a heredar
          if (dModelo.getPartir().compareTo("") != 0) {

            numAux = 1;
            if (numAux != 0) {
              // ahora vamos a crear la estructura de la que partimoS
              query = "SELECT CD_TSIVE, CD_MODELO, " +
                  " NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO " +
                  "FROM SIVE_LINEASM WHERE(CD_TSIVE=? AND CD_MODELO=? )";

              st = con.prepareStatement(query);

              // c�digo TSIVE
              st.setString(1, dModelo.getCD_TSIVE().trim());

              // c�digo modelo
              st.setString(2, dModelo.getPartir().trim());

              // lanza la query
              st.executeQuery();

              rs = st.getResultSet();

              // recorremos el resultSet

              i = 0;
              data = new CLista();
              while (rs.next()) {
                // a�ade un nodo
                data.addElement(new DataLineasM(rs.getString("CD_TSIVE"),
                                                rs.getString("CD_MODELO"),
                                                rs.getInt("NM_LIN"),
                                                rs.getString("CD_TLINEA"),
                                                rs.getString("DS_TEXTO"),
                                                rs.getString("DSL_TEXTO")));
                i++;
              }

              st.close();
              rs.close();
              st = null;
              rs = null;

              if (i != 0) {
                // ahora recorremos la CLista y cambiamos el c�digo
                for (j = 0; j < data.size(); j++) {
                  dLineas = (DataLineasM) data.elementAt(j);
                  // insertamos nuestro c�digo
                  query = "insert into SIVE_LINEASM " +
                      "(CD_TSIVE, CD_MODELO, NM_LIN, CD_TLINEA, " +
                      " DS_TEXTO, DSL_TEXTO ) " +
                      " values(?,?,?,?,?,?)";

                  st = con.prepareStatement(query);
                  // c�digo TSIVE
                  st.setString(1, dModelo.getCD_TSIVE().trim());
                  // c�digo modelo
                  st.setString(2, dModelo.getCD_Modelo());
                  // n�mero de l�neas
                  st.setInt(3, dLineas.getNM_LIN());
                  // tLinea
                  st.setString(4, dLineas.getCD_TLINEA().trim());
                  // Desc. texto
                  st.setString(5, dLineas.getDS_TEXTO().trim());
                  // Desc. texto l.
                  if (dLineas.getDSL_TEXTO() != null) {
                    st.setString(6, dLineas.getDSL_TEXTO().trim());
                  }
                  else {
                    st.setNull(6, java.sql.Types.VARCHAR);
                  }

                  st.executeUpdate();
                  st.close();
                  st = null;
                }

                // hacemos lo mismo con linea_item
                // primero comprobamos que existe alg�n registro
                numAux = 1;
                if (numAux != 0) {

                  query = "select CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, " +
                      " CD_PREGUNTA_PC, IT_OBLIGATORIO, IT_CONDP, " +
                      " NM_LIN_PC, DS_VPREGUNTA_PC " +
                      " from SIVE_LINEA_ITEM " +
                      " where( CD_TSIVE=? and CD_MODELO=? ) " +
                      " order by NM_LIN";

                  st = con.prepareStatement(query);
                  // c�digo TSIVE
                  st.setString(1, dModelo.getCD_TSIVE().trim());
                  // c�digo modelo
                  st.setString(2, dModelo.getPartir().trim());
                  // lanza la query
                  st.executeQuery();
                  rs = null;
                  rs = st.getResultSet();
                  data = new CLista();
                  // recorremos el resultSet
                  i = 0;
                  while (rs.next()) {
                    // a�ade un nodo
                    data.addElement(new DataLineaItem(rs.getString("CD_TSIVE"),
                        rs.getString("CD_MODELO"),
                        rs.getInt("NM_LIN"),
                        rs.getString("CD_PREGUNTA"),
                        rs.getString("CD_PREGUNTA_PC"),
                        rs.getString("IT_OBLIGATORIO"),
                        rs.getString("IT_CONDP"),
                        rs.getInt("NM_LIN_PC"),
                        rs.getString("DS_VPREGUNTA_PC")));
                    i++;
                  }

                  st.close();
                  st = null;
                  rs.close();
                  rs = null;
                  // ahora recorremos la CLista y cambiamos el c�digo

                  if (i != 0) {

                    query = "insert into SIVE_LINEA_ITEM " +
                        "( CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, " +
                        " CD_PREGUNTA_PC, IT_OBLIGATORIO, IT_CONDP, " +
                        " NM_LIN_PC, DS_VPREGUNTA_PC )" +
                        " values(?,?,?,?,?,?,?,?,?)";

                    for (j = 0; j < data.size(); j++) {
                      dItems = (DataLineaItem) data.elementAt(j);
                      st = con.prepareStatement(query);

                      // c�digo TSIVE
                      st.setString(1, dModelo.getCD_TSIVE());

                      // c�digo modelo
                      st.setString(2, dModelo.getCD_Modelo());

                      // num. l�n.
                      st.setInt(3, dItems.getNM_LIN());

                      // cd preg.
                      st.setString(4, dItems.getCD_PREGUNTA().trim());

                      // it. oblig.
                      st.setString(6, dItems.getIT_OBLIGATORIO().trim());

                      // it. condp.
                      st.setString(7, dItems.getIT_CONDP().trim());

                      // num. l�n. pc
                      st.setInt(8, dItems.getNM_LIN_PC());

                      if (dItems.getIT_CONDP().equals("S")) {

                        // cd preg. pc
                        st.setString(5, dItems.getCD_PREGUNTA_PC().trim());

                        // vpreg.
                        st.setString(9, dItems.getDS_VPREGUNTA_PC().trim());

                      }
                      else {
                        st.setNull(5, java.sql.Types.VARCHAR);
                        st.setNull(9, java.sql.Types.VARCHAR);
                      }

                      // lanza la query
                      st.executeUpdate();

                      st.close();
                      st = null;
                    }
                  }
                }
              }
            }
          }
          data = null;
          break;

          // baja de modelo
        case servletBAJA:

          // borra las l�neas
          query = "DELETE FROM SIVE_LINEA_ITEM " +
              " WHERE CD_TSIVE = ? and CD_MODELO = ?";
          st = con.prepareStatement(query);
          st.setString(1, dModelo.getCD_TSIVE());
          st.setString(2, dModelo.getCD_Modelo());
          st.executeUpdate();
          st.close();
          st = null;

          query = "DELETE FROM SIVE_LINEASM " +
              " WHERE CD_TSIVE = ? AND CD_MODELO = ?";
          st = con.prepareStatement(query);
          st.setString(1, dModelo.getCD_TSIVE());
          st.setString(2, dModelo.getCD_Modelo());
          st.executeUpdate();
          st.close();
          st = null;

          // borra el modelo
          query = "DELETE FROM SIVE_MODELO " +
              " WHERE CD_MODELO = ? AND CD_TSIVE = ?";
          st = con.prepareStatement(query);
          st.setString(1, dModelo.getCD_Modelo());
          st.setString(2, dModelo.getCD_TSIVE());
          st.executeUpdate();
          st.close();
          st = null;

          break;

        case servletNO_OPERATIVO:

          // prepara la query
          query = "UPDATE SIVE_MODELO SET IT_OK = ?, FC_BAJAM = ? " +
              " WHERE CD_TSIVE = ? AND CD_MODELO = ?";

          // lanza la query
          st = con.prepareStatement(query);
          st.setString(1, "N");
          st.setDate(2, new java.sql.Date( (new java.util.Date()).getTime()));
          st.setString(3, dModelo.getCD_TSIVE());
          st.setString(4, dModelo.getCD_Modelo());

          st.executeUpdate();
          st.close();
          st = null;

          break;

          // b�squeda de modelo
        case servletSELECCION_X_CODIGO:
        case servletSELECCION_X_DESCRIPCION:

          switch (param.getPerfil()) {
            // ARG: Se cambia la visibilidad de protocolos. Ahora es de abajo a arriba
            case 1: // Super Usuario
              query = "select * from SIVE_MODELO where " + sQueryEnfermedad +
                  " CD_TSIVE = ? and " +
                  "CD_CA is null and CD_NIVEL_1 is null and CD_NIVEL_2 is null ";
              break;
            case 2: // ccaa

              // ARG: El ccaa se va a si mismo y a los de nivel superior
              query = "select * from SIVE_MODELO where " + sQueryEnfermedad +
                  " CD_TSIVE = ? and " +
                  "((CD_CA = ? and CD_NIVEL_1 is null and CD_NIVEL_2 is null) " +
                  "or (CD_CA is null and CD_NIVEL_1 is null and CD_NIVEL_2 is null))";
              /*
                           query = "select * from SIVE_MODELO where " + sQueryEnfermedad + " CD_TSIVE = ? and " +
                   "CD_CA = ? and CD_NIVEL_1 is null and CD_NIVEL_2 is null ";
               */
              break;
            case 3: // niv 1

              // ARG: El niv 1 se va a si mismo y a los de nivel superior
              query = "select * from SIVE_MODELO where " + sQueryEnfermedad +
                  " CD_TSIVE = ? and " +
                  "((CD_CA = ? and CD_NIVEL_1 IN ( " + n1 +
                  " ) and CD_NIVEL_2 is null) " +
                  "or (CD_CA is null and CD_NIVEL_1 is null and CD_NIVEL_2 is null) " +
                  "or (CD_CA is not null and CD_NIVEL_1 is null and CD_NIVEL_2 is null))";
              /*
                           query = "select * from SIVE_MODELO where " + sQueryEnfermedad + " CD_TSIVE = ? and " +
                   "CD_CA = ? and not CD_NIVEL_1 is null and CD_NIVEL_2 is null " +
                      //mlm "and CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES where CD_USUARIO = ?) ";
                      " and CD_NIVEL_1 IN ( " + n1 +" )";
               */
              break;
            case 4: // niv 2

              // ARG: El niv 2 se va a si mismo y a los de nivel superior
              query = "select * from SIVE_MODELO where " + sQueryEnfermedad +
                  " CD_TSIVE = ? and " +
                  "((CD_CA = ? and CD_NIVEL_1 IN ( " + n1 +
                  " ) and CD_NIVEL_2 IN ( " + n2 + " )) " +
                  "or (CD_CA is null and CD_NIVEL_1 is null and CD_NIVEL_2 is null) " +
                  "or (CD_CA is not null and CD_NIVEL_1 is null and CD_NIVEL_2 is null) " +
                  "or (CD_CA is not null and CD_NIVEL_1 is not null and CD_NIVEL_2 is null))";
              /*
                           query = "select * from SIVE_MODELO where " + sQueryEnfermedad + " CD_TSIVE = ? and " +
                   "CD_CA = ? and not CD_NIVEL_1 is null and not CD_NIVEL_2 is null " +
                      //mlm "and CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES where CD_USUARIO = ?) " +
                      //mlm "and CD_NIVEL_2 in (select CD_NIVEL_2 from SIVE_AUTORIZACIONES where CD_USUARIO = ?) ";
                      " and CD_NIVEL_1 IN ( " + n1 +" )" +
                      " and CD_NIVEL_2 IN ( " + n2 +" )";
               */
              break;

          }

          // prepara la query
          // ARG: A�ade los upper (7/5/02)
          if (param.getFilter().length() > 0) {
            if (opmode == servletSELECCION_X_CODIGO) {
              query = query +
                  " and CD_MODELO like ? and CD_MODELO > ? order by CD_MODELO";
            }
            else {
              query = query + " and upper(DS_MODELO) like upper(?) and upper(DS_MODELO) > upper(?) order by DS_MODELO";
            }
          }
          else {
            if (opmode == servletSELECCION_X_CODIGO) {
              query = query + " and CD_MODELO like ? order by CD_MODELO";
            }
            else {
              query = query +
                  " and upper(DS_MODELO) like upper(?) order by DS_MODELO";
            }
          }

          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);

          if (sCdEnfermedad == null) {
            j = 1;
          }
          else {
            st.setString(1, sCdEnfermedad);
            j = 2;
          }

          //MLM 20-10-99
          st.setString(j, param.getTSive());

          j++;

          // perfil usuario
          if (param.getPerfil() != 1) {
            // System_out.println("COM AUT********" + dModelo.getCD_CA());
            st.setString(j, dModelo.getCD_CA());
            j++;
          }

          /* mlm: con el vector de autoriz, esto sobra
                   switch (param.getPerfil()) {
            case 3: // niv 1
              st.setString(j, param.getLogin().trim());
              j++;
              break;
            case 4: // niv 2
              st.setString(j, param.getLogin().trim());
              j++;
              st.setString(j, param.getLogin().trim());
              j++;
              break;
                   } */

          // filtro
          if (opmode == servletSELECCION_X_CODIGO) {
            st.setString(j, dModelo.getCD_Modelo().trim() + "%");
          }
          else {
            st.setString(j, "%" + dModelo.getCD_Modelo().trim() + "%");

          }
          j++;
          // paginaci�n
          if (param.getFilter().length() > 0) {

            st.setString(j, param.getFilter().trim());
          }
          rs = st.executeQuery();

          // extrae la p�gina requerida
          i = 1;
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (opmode == servletSELECCION_X_CODIGO) {
                data.setFilter( ( (DataModelo) data.lastElement()).getCD_Modelo());
              }
              else {
                data.setFilter( ( (DataModelo) data.lastElement()).getDS_Modelo());
              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            CD_TSIVE = (rs.getString("CD_TSIVE"));
            CD_MODELO = (rs.getString("CD_MODELO"));
            DS_MODELO = (rs.getString("DS_MODELO"));
            DSL_MODELO = (rs.getString("DSL_MODELO"));

            //___________________

            // obtiene la descripcion principal en funci�n del idioma si el cliente no es una CListaMantenimiento
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (param.getMantenimiento() == false)) {
              if (DSL_MODELO != null) {
                DS_MODELO = DSL_MODELO;
              }
            }
            //______________________

            CD_NIVEL_1 = (rs.getString("CD_NIVEL_1"));
            CD_NIVEL_2 = (rs.getString("CD_NIVEL_2"));
            CD_CA = (rs.getString("CD_CA"));
            IT_OK = (rs.getString("IT_OK"));
            CD_ENFCIE = (rs.getString("CD_ENFCIE"));
            FC_BAJAM = (rs.getDate("FC_BAJAM"));

            if (FC_BAJAM == null) {
              fechaCadena = "";
            }
            else {
              fechaCadena = FC_BAJAM.toString();
              formato1 = new SimpleDateFormat("dd/MM/yyyy");
              formato2 = new SimpleDateFormat("yyyy-MM-dd");
              java.util.Date miFecha = null;
              miFecha = formato2.parse(fechaCadena);
              fechaCadena = formato1.format(miFecha);
            }
            // a�ade un nodo

            data.addElement(new DataModelo(CD_TSIVE, CD_MODELO,
                                           DS_MODELO, DSL_MODELO, CD_NIVEL_1,
                                           CD_NIVEL_2, CD_CA, IT_OK,
                                           CD_ENFCIE, "", fechaCadena));
            i++;
          }

          rs.close();
          st.close();

          break;

          // modificaci�n de modelo
        case servletMODIFICAR:

//______Se obtienen datos anteriores de Fecha baja y IT_OK ____________________________________________________________________

          query = "select * from SIVE_MODELO " +
              " where CD_TSIVE = ? and CD_MODELO  = ?";

          dModelo = (DataModelo) param.firstElement();
          data = new CLista();
          st = con.prepareStatement(query);

          st.setString(1, param.getTSive());
          st.setString(2, dModelo.getCD_Modelo().trim());

// System_out.println("@***@ antes OK_1");
          rs = st.executeQuery();
// System_out.println("@***@ despu�s OK_1");

          while (rs.next()) {
            CD_TSIVE = (rs.getString("CD_TSIVE"));
            CD_MODELO = (rs.getString("CD_MODELO"));
            DS_MODELO = (rs.getString("DS_MODELO"));
            DSL_MODELO = (rs.getString("DSL_MODELO"));

            //___________________

            // obtiene la descripcion principal en funci�n del idioma si el cliente no es una CListaMantenimiento
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (param.getMantenimiento() == false)) {
              if (DSL_MODELO != null) {
                DS_MODELO = DSL_MODELO;
              }
            }
            //______________________

            CD_NIVEL_1 = (rs.getString("CD_NIVEL_1"));
            CD_NIVEL_2 = (rs.getString("CD_NIVEL_2"));
            CD_CA = (rs.getString("CD_CA"));
            IT_OK = (rs.getString("IT_OK"));
            CD_ENFCIE = (rs.getString("CD_ENFCIE"));
            FC_BAJAM = (rs.getDate("FC_BAJAM"));

            if (FC_BAJAM == null) {
              fechaCadena = "";
            }
            else {
              fechaCadena = FC_BAJAM.toString();
            }
            // a�ade un nodo

            anterior = new DataModelo(CD_TSIVE, CD_MODELO,
                                      DS_MODELO, DSL_MODELO, CD_NIVEL_1,
                                      CD_NIVEL_2, CD_CA, IT_OK,
                                      CD_ENFCIE, "", fechaCadena);

          }
          rs.close();
          st.close();
          st = null;
          rs = null;
//_________________________________________________________________________

          // prepara la query
          // primero vamos a comprobar que no existe otro registro activo
          // para ello comprobaremos que en pantalla esta a SI y el registro a NO
          if (dModelo.getIT_OK().equals("S") && anterior.getIT_OK().equals("N")) {

            query = "update SIVE_MODELO set IT_OK=? , FC_BAJAM = ? " +
                " where CD_TSIVE = ? AND CD_ENFCIE=? and IT_OK = ? ";

            if ( (dModelo.getCD_CA().length() == 0)) {
              query = query +
                  " and CD_CA is NULL and CD_NIVEL_1 is NULL and CD_NIVEL_2 is NULL";
              // ca
            }
            else if ( (dModelo.getCD_Nivel_1().trim().length() == 0) &&
                     (dModelo.getCD_Nivel_2().trim().length() == 0)) {
              query = query +
                  " and CD_CA=? and CD_NIVEL_1 is NULL and CD_NIVEL_2 is NULL";
              // n1
            }
            else if ( (dModelo.getCD_Nivel_1().trim().length() != 0) &&
                     (dModelo.getCD_Nivel_2().trim().length() == 0)) {
              query = query +
                  " and CD_CA=? and CD_NIVEL_1 = ? and CD_NIVEL_2 is NULL";
              // n2
            }
            else if ( (dModelo.getCD_Nivel_1().trim().length() != 0) &&
                     (dModelo.getCD_Nivel_2().trim().length() != 0)) {
              query = query +
                  " and CD_CA=? and CD_NIVEL_1 = ? and CD_NIVEL_2 = ?";

            }
            st = con.prepareStatement(query);

            //Marca de no operativo
            st.setString(1, "N");
            //Fecha de baja : actual
            st.setDate(2, new java.sql.Date( (new java.util.Date()).getTime()));
            //TIPO
            st.setString(3, param.getTSive());
            //Enfermedad
            st.setString(4, dModelo.getCD_EnfCie());
            //Marca de operativo (los que buscamos)
            st.setString(5, "S");

            // ccaa
            if ( (dModelo.getCD_Nivel_1().trim().length() == 0)
                && (dModelo.getCD_Nivel_2().trim().length() == 0)
                && (dModelo.getCD_CA().trim().length() != 0)) {
              st.setString(6, dModelo.getCD_CA());

            }
            // n1
            else if ( (dModelo.getCD_Nivel_1().trim().length() != 0) &&
                     (dModelo.getCD_Nivel_2().trim().length() == 0) &&
                     (dModelo.getCD_CA().trim().length() != 0)) {
              st.setString(6, dModelo.getCD_CA());
              st.setString(7, dModelo.getCD_Nivel_1().trim());

            }
            // n2
            else if ( (dModelo.getCD_Nivel_1().trim().length() != 0) &&
                     (dModelo.getCD_Nivel_2().trim().length() != 0) &&
                     (dModelo.getCD_CA().trim().length() != 0)) {
              st.setString(6, dModelo.getCD_CA());
              st.setString(7, dModelo.getCD_Nivel_1().trim());
              st.setString(8, dModelo.getCD_Nivel_2().trim());

            }

// System_out.println("@***@ antes OK_2");
            st.executeUpdate();
// System_out.println("@***@ despu�s OK_2");
            st.close();
            st = null;
          }

          // ahora modificaremos nuestro registro
          query = "UPDATE SIVE_MODELO SET DS_MODELO=?," +
              " CD_NIVEL_1=?, DSL_MODELO=?, CD_NIVEL_2=?, IT_OK=?," +
              " FC_BAJAM =?, " +
              " CD_OPE=?, CD_ENFCIE=?, FC_ULTACT=? " +
              " WHERE CD_TSIVE = ? AND CD_MODELO = ? ";

          // lanza la query
          st = con.prepareStatement(query);

          // descripci�n modelo
          st.setString(1, dModelo.getDS_Modelo().trim());

          // c�digo nivel1
          if (dModelo.getCD_Nivel_1().trim().length() > 0) {
            st.setString(2, dModelo.getCD_Nivel_1().trim());
          }
          else {
            st.setNull(2, java.sql.Types.VARCHAR);

            // descripci�n en ingl�s
          }
          if (dModelo.getDSL_Modelo().trim().length() > 0) {
            st.setString(3, dModelo.getDSL_Modelo().trim());
          }
          else {
            st.setNull(3, java.sql.Types.VARCHAR);

            // c�digo nivel2
          }
          if (dModelo.getCD_Nivel_2().trim().length() > 0) {
            st.setString(4, dModelo.getCD_Nivel_2().trim());
          }
          else {
            st.setNull(4, java.sql.Types.VARCHAR);

            // activo
          }
          st.setString(5, dModelo.getIT_OK().trim());

          //Fecha de baja. Su valor tambi�n depende, adem�a�as de lo que diaga el usuario, de lo que ten�amos antes
          //Antes no estaba operativo y ahora pasa a  operativo
          if ( (anterior.getIT_OK().compareTo("N") == 0)
              && (dModelo.getIT_OK().trim().compareTo("S") == 0)) {
            st.setNull(6, java.sql.Types.VARCHAR);
          }
          //Antes  estaba operativo y ahora pasa a  NO operativo. Pone fecha actual
          else if ( (anterior.getIT_OK().compareTo("S") == 0)
                   && (dModelo.getIT_OK().trim().compareTo("N") == 0)) {
            st.setDate(6, new java.sql.Date( (new java.util.Date()).getTime()));
          }
          //Antes  estaba operativo y ahora SIGUE a  operativo
          else if ( (anterior.getIT_OK().compareTo("S") == 0)
                   && (dModelo.getIT_OK().trim().compareTo("S") == 0)) {
            st.setNull(6, java.sql.Types.VARCHAR);
          }

          //Antes  estaba NO operativo y ahora SIGUE a  NO operativo . Pone fecha que hab�a, que puede ser null (nunca estuvo operativo ) o no
          else if ( (anterior.getIT_OK().compareTo("N") == 0)
                   && (dModelo.getIT_OK().trim().compareTo("N") == 0)) {
            if (FC_BAJAM == null) {
              st.setNull(6, java.sql.Types.VARCHAR);
            }
            else {
              st.setDate(6, FC_BAJAM);
            }
          }

          // c�digo operador
          st.setString(7, param.getLogin());

          // c�digo enfcie
          st.setString(8, dModelo.getCD_EnfCie().trim());

          // �ltima actualizaci�n
          st.setDate(9, new java.sql.Date( (new java.util.Date()).getTime()));

          // codigo TSIVE
          st.setString(10, dModelo.getCD_TSIVE().trim());

          // c�digo modelo
          st.setString(11, dModelo.getCD_Modelo().trim());

          // lanza la query
// System_out.println("@***@ antes OK_3");
          st.executeUpdate();
// System_out.println("@***@ despu�s OK_3");
          st.close();
          st = null;

          data = null;

          break;

        case servletOBTENER_X_CODIGO:
        case servletOBTENER_X_DESCRIPCION:

          switch (param.getPerfil()) {
            case 1: // Super Usuario
              query = "select * from SIVE_MODELO where CD_TSIVE = ? and " +
                  "CD_CA is null and CD_NIVEL_1 is null and CD_NIVEL_2 is null ";
              break;
            case 2: // ccaa
              query = "select * from SIVE_MODELO where CD_TSIVE = ? and " +
                  "CD_CA = ? and CD_NIVEL_1 is null and CD_NIVEL_2 is null ";
              break;
            case 3: // niv 1
              query = "select * from SIVE_MODELO where CD_TSIVE = ? and " +
                  "CD_CA = ? and not CD_NIVEL_1 is null and CD_NIVEL_2 is null " +
                  //mlm "and CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES where CD_USUARIO = ?) ";
                  " and CD_NIVEL_1 IN ( " + n1 + " )";

              break;
            case 4: // niv 2
              query = "select * from SIVE_MODELO where CD_TSIVE = ? and " +
                  "CD_CA = ? and not CD_NIVEL_1 is null and not CD_NIVEL_2 is null " +
                  //mlm "and CD_NIVEL_1 in (select CD_NIVEL_1 from SIVE_AUTORIZACIONES where CD_USUARIO = ?) " +
                  //mlm "and CD_NIVEL_2 in (select CD_NIVEL_2 from SIVE_AUTORIZACIONES where CD_USUARIO = ?) ";
                  " and CD_NIVEL_1 IN ( " + n1 + " )" +
                  " and CD_NIVEL_2 IN ( " + n2 + " )";

              break;

          }

          dModelo = (DataModelo) param.firstElement();
          data = new CLista();

          if (opmode == servletOBTENER_X_CODIGO) {
            query = query + "and CD_MODELO  like ?"; //*****************
          }
          else {
            query = query + "and DS_MODELO = ?";
          }
          st = con.prepareStatement(query);
          // perfil usuario
          j = 1;

          //MLM 20-10-99
          st.setString(j, param.getTSive());
          j++;

          if (param.getPerfil() != 1) {
            st.setString(j, dModelo.getCD_CA());
            j++;
          }

          /*mlm: con el vector de autoriz, esto sobre
                   switch (param.getPerfil()) {
            case 3: // niv 1
              st.setString(j, param.getLogin().trim());
              j++;
              break;
            case 4: // niv 2
              st.setString(j, param.getLogin().trim());
              j++;
              st.setString(j, param.getLogin().trim());
              j++;
              break;
                   } */
//        st.setString(j, dModelo.getCD_Modelo().trim() );

          if (opmode == servletOBTENER_X_CODIGO) {
            st.setString(j, dModelo.getCD_Modelo().trim() + "%"); //*****************
          }
          else {
            st.setString(j, dModelo.getCD_Modelo().trim());
          }

          rs = st.executeQuery();

          while (rs.next()) {
            CD_TSIVE = (rs.getString("CD_TSIVE"));
            CD_MODELO = (rs.getString("CD_MODELO"));
            DS_MODELO = (rs.getString("DS_MODELO"));
            DSL_MODELO = (rs.getString("DSL_MODELO"));

            //___________________

            // obtiene la descripcion principal en funci�n del idioma si el cliente no es una CListaMantenimiento
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (param.getMantenimiento() == false)) {
              if (DSL_MODELO != null) {
                DS_MODELO = DSL_MODELO;
              }
            }
            //______________________

            CD_NIVEL_1 = (rs.getString("CD_NIVEL_1"));
            CD_NIVEL_2 = (rs.getString("CD_NIVEL_2"));
            CD_CA = (rs.getString("CD_CA"));
            IT_OK = (rs.getString("IT_OK"));
            CD_ENFCIE = (rs.getString("CD_ENFCIE"));
            FC_BAJAM = (rs.getDate("FC_BAJAM"));

            if (FC_BAJAM == null) {
              fechaCadena = "";
            }
            else {
              fechaCadena = FC_BAJAM.toString();
            }
            // a�ade un nodo

            data.addElement(new DataModelo(CD_TSIVE, CD_MODELO,
                                           DS_MODELO, DSL_MODELO, CD_NIVEL_1,
                                           CD_NIVEL_2, CD_CA, IT_OK,
                                           CD_ENFCIE, "", fechaCadena));

          }
          rs.close();
          st.close();
          break;

      }
      con.commit();
    }
    catch (Exception exs) {
      con.rollback();
      throw exs;
    }
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();
    }

    return data;
  }
}
