package confprot;

import java.util.ResourceBundle;

import capp.CApp;

public class ApConfProt
    extends CApp {

  CMantenimientoModelos panel;
  ResourceBundle res;
  String sEtCodigo = "";
  String sEtModelo = "";
  String sEtOperativo = "";

  public ApConfProt() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    ResourceBundle res = ResourceBundle.getBundle("confprot.Res" +
                                                  this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    sEtCodigo = res.getString("msg2.Text");
    sEtModelo = res.getString("msg3.Text");
    sEtOperativo = res.getString("msg4.Text");

    //Centinelas: Constructor con el USU
    //if ( this.getTSive().equals("C") ) {
    //panel = new CMantenimientoModelos(this, true); //������
    //}
    //else {
    panel = new CMantenimientoModelos(this); //������
    //}
    panel.setBorde(false);

    VerPanel("", panel);
  }

  public void stop() {
    panel = null;
    System.gc();
  }
}
