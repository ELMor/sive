package confprot;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.FileDialog;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import cargamodelo.partirFichero;
import cargamodelo.volcarRepositorio;
import comun.DataEnferedo;
import comun.DataEntradaEDO;
import comun.constantes;
import enfcentinela.DataEnfCenti;
import sapp.StubSrvBD;

public class DialMantModelo
    extends CDialog {

  // Para saber si estamos en TUBERCULOSIS
  private boolean bTuberculosis = false;

  //Modos de operaci�n de la ventana
  public static final int modoALTA = 0;
  ResourceBundle res = ResourceBundle.getBundle("confprot.Res" +
                                                this.app.getIdioma());
  public static final int modoMODIFICAR = 1;
  public static final int modoESPERA = 2;
  public static final int modoBAJA = 3;

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;

  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  //
  final static int servletOBTENER_X_CODIGO_MODELOS = 202;
  final static int servletOBTENER_X_DESCRIPCION_MODELOS = 203;
  final static int servletSELECCION_X_CODIGO_MODELOS = 200;
  final static int servletSELECCION_X_DESCRIPCION_MODELOS = 201;

  final int servletNO_OPERATIVO = 10;
  final int servletDES_MOD = 0;

  final int servletOBTENER_NIV1_X_CODIGO = 3;
  final int servletOBTENER_NIV1_X_DESCRIPCION = 4;
  final int servletSELECCION_NIV1_X_CODIGO = 5;
  final int servletSELECCION_NIV1_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;

  final int servletCONSULTAR_RESPUESTAS = 2;
  final int servletCONSULTAR_LINEAS = 4;

  //Constantes del panel
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/repositorio.gif"};

  final String strSERVLETMod = constantes.strSERVLET_MOD;
  final String strSERVLETModelo = constantes.strSERVLET_MODELO;
  final String strSERVLETEnf = constantes.strSERVLET_ENF;
  final String strSERVLETNiv = constantes.strSERVLET_NIV;
  final String strSERVLETdescarga = constantes.strSERVLET_DESCARGA;

  //Para centinelas
  final String strSERVLET_ENF_CENTI = constantes.strSERVLET_ENF_CENTI;

  protected int modoOperacion;

  protected StubSrvBD stubCliente;
  protected StubSrvBD stubClienteRepositorio;

  protected boolean bAceptar = false;

  // control
  protected String sModeloConLineas = "N";
  protected String sModeloConRespuestas = "N";

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  public DataModelo datModelo;

  // gestores de eventos
  botonesActionListener btnActionListener = new botonesActionListener(this);
  itemListener choiceItemListener = new itemListener(this);
  keyAdapter txtKeyListener = new keyAdapter(this);
  focusAdapter txtFocusListener = new focusAdapter(this);

  XYLayout xYLayout = new XYLayout();
  Label lblModelo = new Label();
  Label lblEnf = new Label();
  Label lblGrupo = new Label();
  Label lblNiv1 = new Label();
  Label lblNiv2 = new Label();
  Label lblCopia = new Label();
  Label lblOp = new Label();
  CCampoCodigo modelo = new CCampoCodigo();
  TextField modeloDesc = new TextField();
  TextField txtDescL = new TextField();
  CCampoCodigo enfermedad = new CCampoCodigo();
  CCampoCodigo area = new CCampoCodigo();
  CCampoCodigo distrito = new CCampoCodigo();
  CCampoCodigo partir = new CCampoCodigo();
  Choice choiceOp = new Choice();
  Choice choGrupo = new Choice();
  TextField fecha = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnEnf = new ButtonControl();
  ButtonControl btnNiv1 = new ButtonControl();
  ButtonControl btnNiv2 = new ButtonControl();
  ButtonControl btnCopia = new ButtonControl();
  TextField enfermedadDesc = new TextField();
  TextField areaDesc = new TextField();
  TextField distritoDesc = new TextField();
  TextField partirDesc = new TextField();
  Label lblFecBaja = new Label();
  Label lblDescLocal = new Label();
  Label lblModeloDesc = new Label();
  Panel pnl = new Panel();
  Panel pnl2 = new Panel();
  BorderLayout borderLayout1 = new BorderLayout();
  BorderLayout borderLayout2 = new BorderLayout();
  StatusBar statusBar1 = new StatusBar();
  StatusBar statusBar2 = new StatusBar();
  SimpleDateFormat formato1 = new SimpleDateFormat("dd/MM/yyyy");
  Label lblPartirFichero = new Label();
  TextField txtPartirFichero = new TextField();
  ButtonControl btnFichero = new ButtonControl();
  ButtonControl btnRepositorio = new ButtonControl();

  public DialMantModelo(CApp a,
                        int modo,
                        DataModelo dat) {
    super(a);

    try {

      bTuberculosis = !a.getParametro("CD_TUBERCULOSIS").equals("");

      stubCliente = new StubSrvBD();
      modoOperacion = modo;
      datModelo = dat;
      jbInit();

      lblNiv1.setText(this.app.getNivel1() + ":");
      lblNiv2.setText(this.app.getNivel2() + ":");
      Inicializar();
      enfermedadDesc.setEditable(false);
      enfermedadDesc.setEnabled(false);
      areaDesc.setEditable(false);
      areaDesc.setEnabled(false);
      distritoDesc.setEditable(false);
      distritoDesc.setEnabled(false);
      partirDesc.setEditable(false);
      partirDesc.setEnabled(false);
      btnCopia.setFocusAware(false);
      btnEnf.setFocusAware(false);
      btnNiv1.setFocusAware(false);
      btnNiv2.setFocusAware(false);
      modelo.requestFocus();

      if (modoOperacion == modoALTA) {
        this.area.setText(this.app.getCD_NIVEL1_DEFECTO());
        this.areaDesc.setText(this.app.getDS_NIVEL1_DEFECTO());
        this.distrito.setText(this.app.getCD_NIVEL2_DEFECTO());
        this.distritoDesc.setText(this.app.getDS_NIVEL2_DEFECTO());
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private String getCodEnfGrupo() {
    String sAfectado = "";
    if (this.app.getTSive().equals("E") || this.app.getTSive().equals("T")) {
      sAfectado = this.enfermedad.getText();
    }
    else if (this.app.getTSive().equals("C")) { //Centinelas LRG
      sAfectado = this.enfermedad.getText();
    }
    else if (this.app.getTSive().equals("B")) {
      sAfectado = getGrupo();

    }
    return (sAfectado);
  }

  // validaciones
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;
    boolean b = true;

    // comprueba que esten informados los campos obligatorios

    switch (app.getPerfil()) {

      case 1: // cne
      case 2: // ca
        if ( (this.modelo.getText().length() == 0) ||
            (this.modeloDesc.getText().length() == 0) ||
            (getCodEnfGrupo().length() == 0)) {
          b = false;
        }
        break;
      case 3: // area
        if ( (this.modelo.getText().length() == 0) ||
            (this.modeloDesc.getText().length() == 0) ||
            (getCodEnfGrupo().length() == 0) ||
            (this.areaDesc.getText().length() == 0)) {
          b = false;
        }
        break;
      case 4: // distrito
        if ( (this.modelo.getText().length() == 0) ||
            (this.modeloDesc.getText().length() == 0) ||
            (getCodEnfGrupo().length() == 0) ||
            (this.areaDesc.getText().length() == 0) ||
            (this.distritoDesc.getText().length() == 0)) {
          b = false;
        }
        break;
    }

    if (!b) {
      msg = res.getString("msg5.Text");
    }

    if (b) {

      b = true;
      if ( (modelo.getText().length() > 8) &&
          (modoOperacion == modoALTA)) {
        b = false;
        msg = res.getString("msg6.Text");
        modelo.selectAll();
      }
      if (modeloDesc.getText().length() > 60) {
        b = false;
        msg = res.getString("msg6.Text");
        modeloDesc.selectAll();
      }
      if (txtDescL.getText().length() > 60) {
        b = false;
        msg = res.getString("msg6.Text");
        txtDescL.selectAll();
      }
    }
    if (!b) {
      msgBox = new CMessage(app, CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }
    //Se ajusta la variable que se analiza en panel otra vez por si se cerrase dialogo con aspa !!
    bAceptar = b;
    return b;
  }

  // inicializador
  void jbInit() throws Exception {
    CLista data;
    CLista param;

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnNiv2.setImage(imgs.getImage(0));
    btnNiv1.setImage(imgs.getImage(0));
    btnEnf.setImage(imgs.getImage(0));
    btnCopia.setImage(imgs.getImage(0));
    btnFichero.setImage(imgs.getImage(0));
    btnRepositorio.setLabel(res.getString("btnRepositorio.Label"));
    btnRepositorio.addActionListener(new
        DialMantModelo_btnRepositorio_actionAdapter(this));
    btnRepositorio.setImage(imgs.getImage(3));
    btnAceptar.setImage(imgs.getImage(1));
    btnCancelar.setImage(imgs.getImage(2));

    pnl.setLayout(xYLayout);
    xYLayout.setHeight(470);
    this.setLayout(borderLayout1);
    //this.setSize(new Dimension(400, 470));
    xYLayout.setWidth(466);
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    lblModelo.setText(res.getString("lblModelo.Text"));
    lblEnf.setText(res.getString("lblEnf.Text"));
    lblGrupo.setText("Grupo: ");
    lblNiv1.setText(res.getString("lblNiv1.Text"));
    lblNiv2.setText(res.getString("lblNiv2.Text"));
    lblCopia.setText(res.getString("lblCopia.Text"));
    lblOp.setText(res.getString("lblOp.Text"));
    modelo.setForeground(Color.black);
    modelo.setBackground(new Color(255, 255, 150));
    modeloDesc.setBackground(new Color(255, 255, 150));
    enfermedad.setForeground(Color.black);
    enfermedad.setBackground(new Color(255, 255, 150));
    area.setBackground(new Color(255, 255, 150));
    distrito.setBackground(new Color(255, 255, 150));

    modelo.setName("modelo");
    enfermedad.setName("enfermedad");
    partir.setName("partir");
    partir.addKeyListener(new DialMantModelo_partir_keyAdapter(this));
    area.setName("area");
    distrito.setName("distrito");

    // p�rdida del foco
    enfermedad.addFocusListener(txtFocusListener);
    partir.addFocusListener(txtFocusListener);
    area.addFocusListener(txtFocusListener);
    distrito.addFocusListener(txtFocusListener);

    // cambio de c�digo
    enfermedad.addKeyListener(txtKeyListener);
    partir.addKeyListener(txtKeyListener);
    area.addKeyListener(txtKeyListener);
    distrito.addKeyListener(txtKeyListener);

    // botones
    btnCopia.addActionListener(btnActionListener);
    btnEnf.addActionListener(btnActionListener);
    btnNiv1.addActionListener(btnActionListener);
    btnNiv2.addActionListener(btnActionListener);
    btnAceptar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);

    lblFecBaja.setText(res.getString("lblFecBaja.Text"));
    fecha.setEditable(false);
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    lblDescLocal.setText(res.getString("lblDescLocal.Text"));
    lblModeloDesc.setText(res.getString("lblModeloDesc.Text"));
    statusBar1.setBevelOuter(BevelPanel.LOWERED);
    statusBar1.setBevelInner(BevelPanel.LOWERED);
    statusBar2.setBevelOuter(BevelPanel.LOWERED);
    lblPartirFichero.setText(res.getString("lblPartirFichero.Text"));
    btnFichero.addActionListener(new DialMantModelo_btnFichero_actionAdapter(this));
    txtPartirFichero.addKeyListener(new
                                    DialMantModelo_txtPartirFichero_keyAdapter(this));
    statusBar2.setBevelInner(BevelPanel.LOWERED);
    pnl2.setLayout(borderLayout2);

    btnNiv2.setActionCommand("distrito");
    btnNiv1.setActionCommand("area");
    btnEnf.setActionCommand("enfermedad");
    btnCopia.setActionCommand("partir");
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");

    enfermedadDesc.setEditable(false);
    areaDesc.setEditable(false);
    distritoDesc.setEditable(false);
    partirDesc.setEditable(false);

    pnl.add(modelo, new XYConstraints(128, 8, 93, -1));
    pnl.add(modeloDesc, new XYConstraints(128, 37, 326, -1));
    pnl.add(txtDescL, new XYConstraints(128, 66, 326, -1));

    if (this.app.getTSive().equals("E") || this.app.getTSive().equals("T")) {
      pnl.add(enfermedad, new XYConstraints(128, 98, 93, -1));
    }
    else if (this.app.getTSive().equals("C")) { //Centinelas LRG
      pnl.add(enfermedad, new XYConstraints(128, 98, 93, -1));
    }
    else if (this.app.getTSive().equals("B")) {
      pnl.add(choGrupo, new XYConstraints(128, 98, -1, -1));

    }
    pnl.add(btnEnf, new XYConstraints(224, 98, -1, -1));
    pnl.add(area, new XYConstraints(128, 131, 93, -1));
    pnl.add(btnNiv1, new XYConstraints(224, 131, -1, -1));
    pnl.add(distrito, new XYConstraints(128, 164, 93, -1));
    pnl.add(btnNiv2, new XYConstraints(224, 164, -1, -1));
    pnl.add(partir, new XYConstraints(128, 196, 93, -1));
    pnl.add(btnCopia, new XYConstraints(224, 196, -1, -1));
    pnl.add(choiceOp, new XYConstraints(128, 258, 46, 23));
    pnl.add(fecha, new XYConstraints(363, 258, 92, -1));
    pnl.add(btnAceptar, new XYConstraints(287, 297, 80, 26));
    pnl.add(btnCancelar, new XYConstraints(374, 297, 80, 26));
    pnl.add(lblModelo, new XYConstraints(8, 8, 54, -1));

    if (this.app.getTSive().equals("E") || this.app.getTSive().equals("T")) {
      pnl.add(lblEnf, new XYConstraints(8, 98, 82, -1));
    }
    else if (this.app.getTSive().equals("C")) { //Centinelas
      pnl.add(lblEnf, new XYConstraints(8, 98, 82, -1));
    }
    else if (this.app.getTSive().equals("B")) {
      pnl.add(lblGrupo, new XYConstraints(8, 98, 82, -1));

    }
    pnl.add(lblNiv1, new XYConstraints(8, 131, 92, -1));
    pnl.add(lblNiv2, new XYConstraints(8, 164, -1, -1));
    pnl.add(lblCopia, new XYConstraints(8, 196, 105, -1));
    pnl.add(lblOp, new XYConstraints(8, 258, 80, -1));
    pnl.add(lblDescLocal, new XYConstraints(8, 67, 113, -1));
    pnl.add(enfermedadDesc, new XYConstraints(267, 98, 187, -1));
    pnl.add(areaDesc, new XYConstraints(267, 131, 187, -1));
    pnl.add(partirDesc, new XYConstraints(267, 196, 187, -1));
    pnl.add(distritoDesc, new XYConstraints(267, 164, 187, -1));
    pnl.add(lblFecBaja, new XYConstraints(261, 258, 88, -1));
    pnl.add(lblModeloDesc, new XYConstraints(8, 37, 102, -1));
    pnl.add(lblPartirFichero, new XYConstraints(8, 228, -1, -1));
    pnl.add(txtPartirFichero, new XYConstraints(128, 228, 283, -1));
    pnl.add(btnFichero, new XYConstraints(414, 228, -1, -1));
    if (modoOperacion == modoMODIFICAR) {
      pnl.add(btnRepositorio, new XYConstraints(8, 297, -1, -1));
    }

    choiceOp.addItem(res.getString("msg7.Text"));
    choiceOp.addItem(res.getString("msg8.Text"));
    choiceOp.addItemListener(choiceItemListener);

    setSize(466, 400);
    this.add(pnl, BorderLayout.CENTER);
    pnl2.add(statusBar1, BorderLayout.CENTER);
    pnl2.add(statusBar2, BorderLayout.WEST);
    this.add(pnl2, BorderLayout.SOUTH);

    //Invisible/Visible kit enfermedad o brotes mlm 20-10-99
    choGrupo.addItem("        ");
    if (this.app.getTSive().equals("E") || this.app.getTSive().equals("T")) {
      lblGrupo.setVisible(false);
      choGrupo.setVisible(false);
    }
    else if (this.app.getTSive().equals("C")) { //Centinelas
      lblGrupo.setVisible(false);
      choGrupo.setVisible(false);
    }
    else if (this.app.getTSive().equals("B")) {
      lblEnf.setVisible(false);
      enfermedad.setVisible(false);
      btnEnf.setVisible(false);
      enfermedadDesc.setVisible(false);
      choGrupo.setBackground(new Color(255, 255, 150));
      choGrupo.addItem("B0" + "   " + "Brote T�xico-Alimentario");
      choGrupo.addItem("B1" + "   " + "Brote Vacunable");
      choGrupo.select(0);
    }

    // idioma local no es visible si no tengo
    if (app.getIdiomaLocal().length() == 0) {
      txtDescL.setVisible(false);
      lblDescLocal.setVisible(false);
    }
    else {
      lblDescLocal.setText(res.getString("lblDescLocal.Text") +
                           app.getIdiomaLocal() + "):");
    }

    // ambito del modelo en funcion del epidemi�logo
    switch (app.getPerfil()) {
      case 1: // cne
      case 2: // ca
        area.setVisible(false);
        areaDesc.setVisible(false);
        btnNiv1.setVisible(false);
        lblNiv1.setVisible(false);
        distrito.setVisible(false);
        distritoDesc.setVisible(false);
        btnNiv2.setVisible(false);
        lblNiv2.setVisible(false);
        break;
      case 3: // area
        distrito.setVisible(false);
        distritoDesc.setVisible(false);
        btnNiv2.setVisible(false);
        lblNiv2.setVisible(false);
        break;
    }

    // datos iniciales
    if (datModelo != null) {
      modelo.setText(datModelo.getCD_Modelo());

      modeloDesc.setText(datModelo.getDS_Modelo());
      txtDescL.setText(datModelo.getDSL_Modelo());
      if (this.app.getTSive().equals("E") || this.app.getTSive().equals("T")) {
        // enfermedad // busco descripci�n enfermedad
        enfermedad.setText(datModelo.getCD_EnfCie());
        enfermedad_focusLost();
      }

      else if (this.app.getTSive().equals("C")) { //Centinelas
        // enfermedad // busco descripci�n enfermedad
        enfermedad.setText(datModelo.getCD_EnfCie());
        enfermedad_focusLost();
      }

      else if (this.app.getTSive().equals("B")) {
        selectGrupo(datModelo.getCD_EnfCie());
      }
      // area
      if (datModelo.getCD_Nivel_1() != null) {
        area.setText(datModelo.getCD_Nivel_1());
        // busco descripci�n �rea
        area_focusLost();
      }
      // distrito
      if (datModelo.getCD_Nivel_2() != null) {
        distrito.setText(datModelo.getCD_Nivel_2());
        // busco descripci�n distrito
        distrito_focusLost();
      }

      if (datModelo.getIT_OK().compareTo("S") == 0) {
        choiceOp.select(res.getString("msg7.Text"));
      }
      else {
        choiceOp.select(res.getString("msg8.Text"));
      }

      //Si el modelo est� de baja se pone la fecha de baja
      //(Nota puede estar no operativo aun sin haber sido dado de baja
      if (! (datModelo.getBaja().equals(""))) {
        /*formato1=new SimpleDateFormat("dd/MM/yyyy");
                 formato2=new SimpleDateFormat("yyyy-MM-dd");
                 Date miFecha = null;
                 miFecha = formato2.parse(datModelo.getBaja()); */
        fecha.setText(datModelo.getBaja());
      }

      param = new CLista();
      param.setTSive(this.app.getTSive());
      param.addElement(new DataMantLineasModelo(datModelo.getCD_Modelo(),
                                                app.getTSive()));

      // determina si el modelo tiene l�neas
      param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
      param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
      stubCliente.setUrl(new URL(app.getURL() + strSERVLETModelo));

      data = (CLista) stubCliente.doPost(servletCONSULTAR_LINEAS, param);

      /*
            SrvModelo srv= new SrvModelo();
            data= srv.doDebug(servletCONSULTAR_LINEAS, param);
       */

      if (data.size() != 0) {
        sModeloConLineas = (String) data.firstElement();
      }
      else {
        sModeloConLineas = "S"; // bloquea el modelo

      }
      data = null;

      statusBar2.setText(res.getString("statusBar2.Text") + sModeloConLineas);

      // determina si el modelo tiene respuestas
      param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
      param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
      stubCliente.setUrl(new URL(app.getURL() + strSERVLETModelo));

      data = (CLista) stubCliente.doPost(servletCONSULTAR_RESPUESTAS, param);

      /*
           SrvModelo srvlet= new SrvModelo();
           data= srvlet.doDebug(servletCONSULTAR_RESPUESTAS, param);
       */

      if (data.size() != 0) {
        sModeloConRespuestas = (String) data.firstElement();
      }
      else {
        sModeloConRespuestas = "S"; // bloquea el modelo

      }
      data = null;
      param = null;

      statusBar1.setText(res.getString("statusBar1.Text") +
                         sModeloConRespuestas);
    }

    // Adaptaci�n para Tuberculosis
    if (bTuberculosis) {
      enfermedad.setText(this.app.getParametro("CD_TUBERCULOSIS"));
      enfermedadDesc.setText("TUBERCULOSIS");
    }

    Inicializar();
  }

  public void Inicializar() {

    btnRepositorio.setEnabled(false); // opci�n no sisponible

    switch (modoOperacion) {

      case modoALTA:
        setTitle(res.getString("msg9.Text"));
        modelo.setEnabled(true);
        modeloDesc.setEnabled(true);
        enfermedad.setEnabled(true);
        choGrupo.setEnabled(true);
        area.setEnabled(true);
        distrito.setEnabled(true);
        partir.setEnabled(true);
        txtPartirFichero.setEnabled(true);
        btnFichero.setEnabled(true);
        btnCopia.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnEnf.setEnabled(true);
        btnNiv1.setEnabled(true);
        if (areaDesc.getText().length() > 0) {
          btnNiv2.setEnabled(true);
        }
        else {
          btnNiv2.setEnabled(false);

          // se crea como no operativo
        }
        choiceOp.setEnabled(false);
        choiceOp.select(1);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:
        setTitle(res.getString("msg10.Text"));
        modelo.setEnabled(false);
        modeloDesc.setEnabled(true);
        partir.setVisible(false);
        partirDesc.setVisible(false);
        btnCopia.setVisible(false);
        txtPartirFichero.setVisible(false);
        btnFichero.setVisible(false);
        lblPartirFichero.setVisible(false);
        lblCopia.setVisible(false);
        choiceOp.setEnabled(true);
        fecha.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnAceptar.setEnabled(true);
        // modelo con respuestas u operativo
        if ( (sModeloConRespuestas.equals("S")) ||
            (datModelo.getIT_OK().equals("S"))) {
          enfermedad.setEnabled(false);
          choGrupo.setEnabled(false);
          area.setEnabled(false);
          distrito.setEnabled(false);
          btnNiv1.setEnabled(false);
          btnEnf.setEnabled(false);
          btnNiv2.setEnabled(false);
        }
        else {
          enfermedad.setEnabled(true);
          choGrupo.setEnabled(true);
          area.setEnabled(true);
          distrito.setEnabled(true);
          btnNiv1.setEnabled(true);
          btnEnf.setEnabled(true);
          if (areaDesc.getText().length() > 0) {
            btnNiv2.setEnabled(true);
          }
          else {
            btnNiv2.setEnabled(false);
          }
        }

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        modelo.setEnabled(false);
        modeloDesc.setEnabled(false);
        enfermedad.setEnabled(false);
        choGrupo.setEnabled(false);
        area.setEnabled(false);
        distrito.setEnabled(false);
        partir.setEnabled(false);
        txtPartirFichero.setEnabled(false);
        btnFichero.setEnabled(false);
        choiceOp.setEnabled(false);
        fecha.setEnabled(false);
        btnCancelar.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnCopia.setEnabled(false);
        btnEnf.setEnabled(false);
        btnNiv1.setEnabled(false);
        btnNiv2.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoBAJA:
        setTitle(res.getString("msg11.Text"));
        modelo.setEnabled(false);
        modeloDesc.setEnabled(false);
        enfermedad.setEnabled(false);
        choGrupo.setEnabled(false);
        txtDescL.setEnabled(false);
        area.setEnabled(false);
        distrito.setEnabled(false);
        partir.setVisible(false);
        partirDesc.setVisible(false);
        txtPartirFichero.setVisible(false);
        btnFichero.setVisible(false);
        lblPartirFichero.setVisible(false);
        btnCopia.setVisible(false);
        lblCopia.setVisible(false);
        choiceOp.setEnabled(false);
        fecha.setEnabled(false);
        btnCancelar.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnEnf.setEnabled(false);
        btnNiv1.setEnabled(false);
        btnNiv2.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }

    if (bTuberculosis) {
      enfermedad.setEnabled(false);
      btnEnf.setEnabled(false);
    }

    this.doLayout();
  }

  //devuelve el cod de grupo
  private String getGrupo() {

    String iG = "";
    iG = choGrupo.getSelectedItem().substring(0, 2); //sin trim

    return (iG.trim());
  } //getGrupo

  //dado un cod, situar la Choice en ese registro
  //recordar que es obligatorio pero lleva un blanco
  public void selectGrupo(String cd) {
    if (cd.equals("")) {
      choGrupo.select(0);
    }
    else {
      String cdChoice = "";
      for (int i = 1; i < 3; i++) {
        cdChoice = choGrupo.getItem(i);
        cdChoice = cdChoice.substring(0, 2);
        if (cdChoice.equals(cd.trim())) {
          choGrupo.select(i);
          break;
        }
      }
    }
  }

  // gesti�n del campo fecha de baja
  void choiceOp_itemStateChanged(ItemEvent e) {
    Date today = new Date();
    String dia;
    int mes;
    int anyo;
    String cad;
    //Si es operativo
    if (choiceOp.getSelectedItem().compareTo(res.getString("msg8.Text")) != 0) {
      fecha.setText("");
    }
    //Si no est� operativo
    else {
      formato1 = new SimpleDateFormat("dd/MM/yyyy");
      fecha.setText(formato1.format(today));
    }
  }

  // limpia la ventana
  protected void borraTodo() {
    modeloDesc.setText("");
    txtDescL.setText("");
    enfermedad.setText("");
    choGrupo.select(0);
    enfermedadDesc.setText("");
    area.setText("");
    areaDesc.setText("");
    distrito.setText("");
    distritoDesc.setText("");
    partir.setText("");
    partirDesc.setText("");
    choiceOp.select(res.getString("msg7.Text"));
    fecha.setText("");
    Inicializar();
  }

  // lista de valores de enfermedades
  void btnEnf_actionPerformed(ActionEvent e) {
    DataEnferedo data;
    CMessage mensaje = null;
    int modo = modoOperacion;
    DataEnfCenti dataEnfCenti = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {

      if (app.getTSive().equals("E") || app.getTSive().equals("T")) {
        CListaEnfCie lista = new CListaEnfCie(app, res.getString("msg12.Text"),
                                              stubCliente, strSERVLETEnf,
                                              servletOBTENER_X_CODIGO_MODELOS,
            servletOBTENER_X_DESCRIPCION_MODELOS,
            servletSELECCION_X_CODIGO_MODELOS,
            servletSELECCION_X_DESCRIPCION_MODELOS);

        lista.show();

        data = (DataEnferedo) lista.getComponente();
        if (data != null) {
          enfermedad.setText(data.getCod());
          enfermedadDesc.setText(data.getDes());
        }
      }

      else if (app.getTSive().equals("C")) {
        CListaEnfCenti lista = new CListaEnfCenti(app,
                                                  res.getString("msg12.Text"),
                                                  stubCliente,
                                                  strSERVLET_ENF_CENTI,
                                                  servletOBTENER_X_CODIGO,
                                                  servletOBTENER_X_DESCRIPCION,
                                                  servletSELECCION_X_CODIGO,
            servletSELECCION_X_DESCRIPCION);

        lista.show();

        dataEnfCenti = (DataEnfCenti) lista.getComponente();
        if (dataEnfCenti != null) {
          enfermedad.setText(dataEnfCenti.getCod());
          enfermedadDesc.setText(dataEnfCenti.getDes());
        }
      }

//   strSERVLET_ENF_CENTI

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }
    modoOperacion = modo;
    Inicializar();
  }

  // lista de valores de �reas
  void btnNiv1_actionPerformed(ActionEvent e) {
    DataEntradaEDO data;
    int modo = modoOperacion;
    CMessage mensaje = null;

    CLista lr;
    CLista datos;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaNiv1 lista = new CListaNiv1(app,
                                        res.getString("msg13.Text") +
                                        app.getNivel1(),
                                        stubCliente,
                                        strSERVLETNiv,
                                        servletOBTENER_NIV1_X_CODIGO,
                                        servletOBTENER_NIV1_X_DESCRIPCION,
                                        servletSELECCION_NIV1_X_CODIGO,
                                        servletSELECCION_NIV1_X_DESCRIPCION);
      lista.show();
      data = (DataEntradaEDO) lista.getComponente();
      if (data != null) {
        area.setText(data.getCod());
        areaDesc.setText(data.getDes());
        btnNiv2.setEnabled(true);
      }
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }
    modoOperacion = modo;
    Inicializar();
  }

  // lista de valores de distritos
  void btnNiv2_actionPerformed(ActionEvent e) {
    DataEntradaEDO data;
    CMessage msgBox;
    int modo = modoOperacion;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaNiv2 lista = new CListaNiv2(this,
                                        res.getString("msg13.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLETNiv,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataEntradaEDO) lista.getComponente();
      if (data != null) {
        distrito.setText(data.getCod());
        distritoDesc.setText(data.getDes());
        btnNiv2.setEnabled(true);
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    modoOperacion = modo;
    Inicializar();
  }

  // lista de valores de modelos
  void btnCopia_actionPerformed(ActionEvent e) {

    DataModelo data;
    CMessage mensaje = null;
    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaModelo lista = new CListaModelo(app,
                                            res.getString("msg14.Text"),
                                            stubCliente,
                                            strSERVLETMod,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);

      // modificacion 20/06/2001 para que muestre la informaci�n
      lista.btnSearch_actionPerformed();

      lista.show();
      data = (DataModelo) lista.getComponente();
      if (data != null) {
        partirDesc.setText(data.getDS_Modelo());
        partir.setText(data.getCD_Modelo());
        txtPartirFichero.setText("");
      }
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }
    modoOperacion = modo;
    Inicializar();
  }

// a�adir
  void Anadir() {
    CLista datos = null;
    CMessage mensaje = null;
    CLista lr = null;
    String miCod = "";
    Date dFec = null;

    if (!isDataValid()) {
      return;
    }
    else {

      int miModo = modoOperacion;
      modoOperacion = modoESPERA;
      Inicializar();

      String cad = new String("");
      String tramI = new String("");

      //comprobacion modeloalta <> partir
      if (modelo.getText().compareTo(partir.getText()) != 0) {

        try {
          if (this.choiceOp.getSelectedItem() == res.getString("msg8.Text")) {
            tramI = "N";
          }
          else {
            tramI = "S";
          }
          lr = new CLista();

          datos = new CLista();
          datos.setLogin(this.app.getLogin());
          datos.setFilter("");
          datos.setTSive(this.app.getTSive());

          switch (this.app.getCA().length()) {
            case 0:
              miCod = "_00";
              break;
            case 1:
              miCod = "_0" + this.app.getCA();
              break;
            case 2:
              miCod = "_" + this.app.getCA();
              break;
          }
          switch (app.getPerfil()) {
            //Si es perfil de S.Centrales no se pasa la CAut
            case 1:
              datModelo = new DataModelo(this.app.getTSive(),
                                         modelo.getText().toUpperCase() + miCod,
                                         this.modeloDesc.getText(),
                                         txtDescL.getText(),
                                         "", "", "",
                                         tramI, getCodEnfGrupo(),
                                         partir.getText(), "");
              break;

            case 2:
              datModelo = new DataModelo(this.app.getTSive(),
                                         modelo.getText().toUpperCase() + miCod,
                                         this.modeloDesc.getText(),
                                         txtDescL.getText(),
                                         "", "", this.app.getCA(),
                                         tramI, getCodEnfGrupo(),
                                         partir.getText(), "");
              break;

            case 3:
              datModelo = new DataModelo(this.app.getTSive(),
                                         modelo.getText().toUpperCase() + miCod,
                                         this.modeloDesc.getText(),
                                         txtDescL.getText(),
                                         area.getText(), "", this.app.getCA(),
                                         tramI, getCodEnfGrupo(),
                                         partir.getText(), "");
              break;

            default:
              datModelo = new DataModelo(this.app.getTSive(),
                                         modelo.getText().toUpperCase() + miCod,
                                         this.modeloDesc.getText(),
                                         txtDescL.getText(),
                                         area.getText(), distrito.getText(),
                                         this.app.getCA(),
                                         tramI, getCodEnfGrupo(),
                                         partir.getText(), "");
              break;
          }

          datos.addElement(datModelo);
          datos.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
          datos.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
          stubCliente.setUrl(new URL(app.getURL() + strSERVLETMod));

          lr = (CLista)this.stubCliente.doPost(modoALTA, datos);

          /*
                  lr= (new SrvGesMod()).doDebug(modoALTA,datos);
           */

          datos = null;
          if (txtPartirFichero.getText().length() > 0) {
            partirFichero partFich = new partirFichero(this.app,
                txtPartirFichero.getText().trim(),
                this.modelo.getText().trim() + miCod);
          }
          dispose();
        }
        catch (Exception excepc) {
          excepc.printStackTrace();
          mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
          mensaje.show();
        }
      }
      else { //ni idea
        mensaje = new CMessage(this.app, CMessage.msgERROR,
                               res.getString("msg15.Text"));
        mensaje.show();
      }
    } //else data valid

    modoOperacion = modoALTA;
    Inicializar();
  }

  void Modificar() {
    CLista datos = null;
    CMessage mensaje = null;
    CLista lrm = null;
    String cad = new String("");
    String tramI = new String("");

    Date dFec = null;
    String sFec = "";

    if (isDataValid()) {
      try {
        if (this.choiceOp.getSelectedItem().compareTo(res.getString("msg8.Text")) ==
            0) {
          tramI = "N";
          //Convierte la fecha de baja de formato pantalla a formato de b.datos
          /*formato1=new SimpleDateFormat("dd/MM/yyyy");
                     formato2=new SimpleDateFormat("yyyy-MM-dd");*/
          //Pasa a tipo Date
          //dFec = formato1.parse(fecha.getText());
          //Pasa el Date a String con el formato que viene de b.datos
          //sFec = formato2.format(dFec);
          sFec = fecha.getText();
        }
        else {
          tramI = "S";
        }
        //if (area.getText()
        modoOperacion = modoESPERA;
        Inicializar();
        datos = new CLista();
        lrm = new CLista();
        datos.setLogin(this.app.getLogin());
        datos.setPerfil(this.app.getPerfil());

        switch (app.getPerfil()) {
          //Si es perfil de S.Centrales no se pone la CAut
          case 1:
            datModelo = new DataModelo(this.app.getTSive(), modelo.getText(),
                                       this.modeloDesc.getText(),
                                       txtDescL.getText(), area.getText(),
                                       distrito.getText(),
                                       "", tramI, getCodEnfGrupo(), "", sFec);
            break;

          default:
            datModelo = new DataModelo(this.app.getTSive(), modelo.getText(),
                                       this.modeloDesc.getText(),
                                       txtDescL.getText(), area.getText(),
                                       distrito.getText(),
                                       this.app.getCA(), tramI, getCodEnfGrupo(),
                                       "", sFec);
            break;
        }

        datos.addElement(datModelo);
        datos.setTSive(this.app.getTSive());
        datos.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
        datos.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
        stubCliente.setUrl(new URL(app.getURL() + strSERVLETMod));

        lrm = (CLista)this.stubCliente.doPost(servletMODIFICAR, datos);

        //BEGIN_DSR
        //SrvGesMod srv= new SrvGesMod();
        //srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
        //                       "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
        //                       "dba_edo",
        //                       "manager");
        //lrm = srv.doDebug(servletMODIFICAR, datos);
        //lrm = null;
        //END_DSR

        datos = null;
        dispose();
      }
      catch (Exception excepc) {
        excepc.printStackTrace();
        mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
        mensaje.show();
      }
      try {
        modoOperacion = modoMODIFICAR;
        Inicializar();
      }
      catch (Exception e) {
        e.printStackTrace(); ;
      }
    }
  }

  void Borrar() {
    CLista datos = null;
    CMessage mensaje = null;
    CLista lrm = null;
    String cad = new String("");
    String tramI = new String("");
    int modo = -1;

    try {
      datModelo = new DataModelo(this.app.getTSive(),
                                 modelo.getText(), null, null, null,
                                 null, null, null, null, null, null);

      // el modelo tiene respuestas
      if (sModeloConRespuestas.equals("S")) {
        mensaje = new CMessage(app, CMessage.msgPREGUNTA,
                               res.getString("msg16.Text"));
        mensaje.show();

        // pone el nodo como no operativo
        if ( (mensaje.getResponse()) == true) {
          modo = servletNO_OPERATIVO;
        }
      }
      else if (sModeloConLineas.equals("S")) {
        mensaje = new CMessage(app, CMessage.msgPREGUNTA,
                               res.getString("msg17.Text"));
        mensaje.show();

        // pone el nodo como no operativo
        if ( (mensaje.getResponse()) == true) {
          modo = servletBAJA;
        }
      }
      else {
        mensaje = new CMessage(app, CMessage.msgPREGUNTA,
                               res.getString("msg18.Text"));
        mensaje.show();

        // pone el nodo como no operativo
        if ( (mensaje.getResponse()) == true) {
          modo = servletBAJA;
        }
      }

      //if (area.getText()
      if (modo != -1) {
        modoOperacion = modoESPERA;
        Inicializar();
        datos = new CLista();
        lrm = new CLista();
        datos.setLogin(this.app.getLogin());
        datos.addElement(datModelo);
        datos.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
        datos.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
        stubCliente.setUrl(new URL(app.getURL() + strSERVLETMod));
        lrm = (CLista)this.stubCliente.doPost(modo, datos);
        datos = null;
        dispose();
      }
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }
    modoOperacion = modoBAJA;
    Inicializar();
  }

  // cambio de la caja de enfermedad
  void enfermedad_keyPressed(KeyEvent e) {
    enfermedadDesc.setText("");
  }

  // cambio del area
  void area_keyPressed(KeyEvent e) {
    areaDesc.setText("");
    distrito.setText("");
    distritoDesc.setText("");
    btnNiv2.setEnabled(false);
    distrito.setEnabled(false);
  }

  //cambio del distrito
  void distrito_keyPressed(KeyEvent e) {
    distritoDesc.setText("");
  }

  // cambio en partir
  void partir_keyPressed(KeyEvent e) {
    partirDesc.setText("");
    txtPartirFichero.setText("");
  }

  // perdida del foco en enfermedad
  void enfermedad_focusLost() {
    CLista datos = null;
    CLista lr = null;
    DataEnferedo dataEnf;
    CMessage msgBox;
    int modo = modoOperacion;

    DataEnfCenti dataEnfCenti;

    if (enfermedad.getText().length() > 0) {
      try {

        //Aplicaci�n EDO
        if (app.getTSive().equals("E") || app.getTSive().equals("T")) {

          modoOperacion = modoESPERA;
          Inicializar();
          datos = new CLista();
          datos.setLogin(this.app.getLogin());
          datos.setFilter("");
          datos.addElement(new DataEnferedo(enfermedad.getText()));
          datos.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
          datos.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
          stubCliente.setUrl(new URL(app.getURL() + strSERVLETEnf));
          lr = (CLista)this.stubCliente.doPost(servletOBTENER_X_CODIGO_MODELOS,
                                               datos);
          datos = null;
          if (lr.size() > 0) {
            dataEnf = (DataEnferedo) lr.firstElement();
            enfermedadDesc.setText(dataEnf.getDes());
          }
          else {
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                  res.getString("msg19.Text"));
            msgBox.show();
            msgBox = null;
          }
        } //If de aplicaci�n EDO

        //Aplicaci�n centinelas
        else if (app.getTSive().equals("C")) {

          modoOperacion = modoESPERA;
          Inicializar();
          datos = new CLista();
          datos.setLogin(this.app.getLogin());
          datos.setFilter("");
          datos.addElement(new DataEnfCenti(enfermedad.getText()));
          datos.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
          datos.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
          stubCliente.setUrl(new URL(app.getURL() + strSERVLET_ENF_CENTI));
          lr = (CLista)this.stubCliente.doPost(servletOBTENER_X_CODIGO, datos);
          datos = null;
          if (lr.size() > 0) {
            dataEnfCenti = (DataEnfCenti) lr.firstElement();
            enfermedadDesc.setText(dataEnfCenti.getDes());
          }
          else {
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                  res.getString("msg19.Text"));
            msgBox.show();
            msgBox = null;
          }
        } //If de aplicaci�n centinelas

      }
      catch (Exception erl) {
        erl.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, erl.getMessage());
        msgBox.show();
        msgBox = null;
      }
      modoOperacion = modo;
      Inicializar();
    }
  }

  void area_focusLost() {
    CLista datos = null;
    CLista lr = null;
    DataEntradaEDO dataniv1;
    CMessage msgBox;
    int modo = modoOperacion;

    if (area.getText().length() > 0) {
      try {
        modoOperacion = modoESPERA;
        Inicializar();
        datos = new CLista();
        datos.setLogin(this.getCApp().getLogin());
        datos.setPerfil(this.getCApp().getPerfil());
        datos.addElement(new DataEntradaEDO(area.getText()));
        datos.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
        datos.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
        stubCliente.setUrl(new URL(app.getURL() + strSERVLETNiv));
        lr = (CLista)this.stubCliente.doPost(servletOBTENER_NIV1_X_CODIGO,
                                             datos);
        datos = null;
        if (lr.size() > 0) {
          dataniv1 = (DataEntradaEDO) lr.firstElement();
          areaDesc.setText(dataniv1.getDes());
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg19.Text"));
          msgBox.show();
          msgBox = null;
        }
      }
      catch (Exception erl) {
        erl.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, erl.getMessage());
        msgBox.show();
        msgBox = null;
      }
      modoOperacion = modo;
      Inicializar();
    }
  }

  void distrito_focusLost() {
    CLista datos = null;
    CLista lr = null;
    DataEntradaEDO dataniv2;
    CMessage msgBox;
    int modo = modoOperacion;

    if (distrito.getText().length() > 0) {
      try {
        modoOperacion = modoESPERA;
        Inicializar();
        datos = new CLista();
        datos.setLogin(this.app.getLogin());
        datos.setPerfil(this.getCApp().getPerfil());
        datos.setFilter("");
        // cris datos.addElement(new DataEntradaEDO(area.getText(),"",distrito.getText()));
        datos.addElement(new DataEntradaEDO(distrito.getText(), "",
                                            area.getText())); //cod, des, nivel1
        datos.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
        datos.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
        stubCliente.setUrl(new URL(app.getURL() + strSERVLETNiv));
        lr = (CLista)this.stubCliente.doPost(servletOBTENER_NIV2_X_CODIGO,
                                             datos);
        datos = null;
        if (lr.size() > 0) {
          dataniv2 = (DataEntradaEDO) lr.firstElement();
          distritoDesc.setText(dataniv2.getDes());
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg19.Text"));
          msgBox.show();
          msgBox = null;
        }
      }
      catch (Exception erl) {
        erl.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, erl.getMessage());
        msgBox.show();
        msgBox = null;
      }
      modoOperacion = modo;
      Inicializar();
    }

  }

  void partir_focusLost() {
    CLista datos = null;
    CLista lr = null;
    DataModelo data;
    CMessage msgBox;
    int modo = modoOperacion;

    if (partir.getText().length() > 0) {
      try {
        modoOperacion = modoESPERA;
        Inicializar();
        datos = new CLista();
        datos.setLogin(this.app.getLogin());
        datos.setPerfil(this.app.getPerfil());
        datos.setTSive(this.app.getTSive()); //LRG 29-5-00
        datos.addElement(new DataModelo(this.app.getTSive(), partir.getText(), null, null, null, null,
                                        app.getCA(), null, null, null, null));

        stubCliente.setUrl(new URL(app.getURL() + strSERVLETMod));
        datos.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
        datos.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
        lr = (CLista)this.stubCliente.doPost(servletOBTENER_X_CODIGO, datos);
        datos = null;
        if (lr.size() > 0) {
          data = (DataModelo) lr.firstElement();
          partirDesc.setText(data.getDS_Modelo());
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg19.Text"));
          msgBox.show();
          msgBox = null;
        }
      }
      catch (Exception erl) {
        erl.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, erl.getMessage());
        msgBox.show();
        msgBox = null;
      }
      modoOperacion = modo;
      Inicializar();
    }
  }

  // cambio en la caja del modelo
  void modelo_keyPressed(KeyEvent e) {
    borraTodo();
  }

  void txtPartirFichero_keyPressed(KeyEvent e) {
    partir.setText("");
    partirDesc.setText("");
  }

  void btnFichero_actionPerformed(ActionEvent e) {
    int modoMio = modoOperacion;
    CMessage msgBox = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      FileDialog dialog = new FileDialog(app.getFrame(),
                                         res.getString("msg20.Text"),
                                         FileDialog.LOAD);
      dialog.show();
      if (dialog.getFile() != null) {
        txtPartirFichero.setText(dialog.getDirectory() + dialog.getFile());
        partir.setText("");
      }
    }
    catch (Exception er) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    modoOperacion = modoMio;
    Inicializar();
  }

  void btnRepositorio_actionPerformed(ActionEvent e) {
    CMessage msgBox;
    CLista data;
    CLista elBloque;
    CLista listaFich;
    int j = 0;
    int miModo = modoOperacion;
    int numTok; //Num tokens quedan por recorrer de una linea
    String sLinea;
    // stub's

    try {
      this.modoOperacion = modoESPERA;
      Inicializar();
      data = new CLista();
      listaFich = new CLista();
      data.addElement(new DataModelo(this.app.getTSive(), modelo.getText(), null, null, null, null,
                                     app.getCA(), null, null, null, null));
      // llamamos al servlet de descarga
      data.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
      data.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());
      stubClienteRepositorio = new StubSrvBD();
      stubClienteRepositorio.setUrl(new URL(app.getURL() + strSERVLETdescarga));
      // obtiene la lista con los registros a a�adir
      listaFich = (CLista) stubClienteRepositorio.doPost(servletDES_MOD, data);

      if (listaFich.size() > 0) {
        // insertamos en el repositorio
        volcarRepositorio volcador = new volcarRepositorio(this.app,
            listaFich,
            this.app.getFTP_SERVER());
      }
      else {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg21.Text"));
        msgBox.show();
        msgBox = null;
      }
      // error al procesar
    }
    catch (Exception enc) {
      enc.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, enc.toString());
      msgBox.show();
      msgBox = null;
    }

    this.modoOperacion = miModo;
    Inicializar();

  }

}

// gestor evento botones
class botonesActionListener
    implements ActionListener, Runnable {
  DialMantModelo adaptee = null;
  ResourceBundle res = ResourceBundle.getBundle("confprot.Res0");
  ActionEvent e = null;

  public botonesActionListener(DialMantModelo adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand() == "partir") {
      adaptee.btnCopia_actionPerformed(e);
    }
    else if (e.getActionCommand() == "distrito") {
      adaptee.btnNiv2_actionPerformed(e);
    }
    else if (e.getActionCommand() == "area") {
      adaptee.btnNiv1_actionPerformed(e);
    }
    else if (e.getActionCommand() == "enfermedad") {
      adaptee.btnEnf_actionPerformed(e);
    }

    else if (e.getActionCommand() == "aceptar") { // aceptar
      adaptee.bAceptar = true;

      // realiza la operaci�n
      switch (adaptee.modoOperacion) {
        case DialMantModelo.modoALTA:
          adaptee.Anadir();
          break;
        case DialMantModelo.modoMODIFICAR:
          adaptee.Modificar();
          break;
        case DialMantModelo.modoBAJA:
          adaptee.Borrar();
          break;
      }

    }
    else if (e.getActionCommand() == "cancelar") { // cancelar
      adaptee.bAceptar = false;
      adaptee.dispose();
    }

  }
}

// selecci�n de la choice de modelo operativo
class itemListener
    implements java.awt.event.ItemListener {
  DialMantModelo adaptee;

  itemListener(DialMantModelo adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.choiceOp_itemStateChanged(e);
  }
}

// cambio del texto en una caja de c�digo
class keyAdapter
    extends java.awt.event.KeyAdapter {
  DialMantModelo adaptee;

  keyAdapter(DialMantModelo adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    if (txt.getName().equals("enfermedad")) {
      adaptee.enfermedad_keyPressed(e);
    }
    else if (txt.getName().equals("area")) {
      adaptee.area_keyPressed(e);
    }
    else if (txt.getName().equals("distrito")) {
      adaptee.distrito_keyPressed(e);
    }
    else if (txt.getName().equals("partir")) {
      adaptee.partir_keyPressed(e);
    }
    else if (txt.getName().equals("modelo")) {
      adaptee.modelo_keyPressed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  DialMantModelo adaptee;
  FocusEvent evt;

  focusAdapter(DialMantModelo adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    TextField txt = (TextField) evt.getSource();
    if (txt.getName().equals("enfermedad")) {
      adaptee.enfermedad_focusLost();
    }
    else if (txt.getName().equals("area")) {
      adaptee.area_focusLost();
    }
    else if (txt.getName().equals("distrito")) {
      adaptee.distrito_focusLost();
    }
    else if (txt.getName().equals("partir")) {
      adaptee.partir_focusLost();
    }
  }
}

// listas de valores
class CListaModelo
    extends CListaValores {
  public CListaModelo(CApp a,
                      String title,
                      StubSrvBD stub, String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descripcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a, title, stub, servlet, obtener_x_codigo, obtener_x_descripcion,
          seleccion_x_codigo, seleccion_x_descripcion);

    //btnSearch_actionPerformed();

  }

  public Object setComponente(String s) {
    return new DataModelo(this.app.getTSive(), s, null, null, null, null,
                          app.getCA(), null, null, null, null);
  }

  public String getCodigo(Object o) {
    return ( ( (DataModelo) o).getCD_Modelo());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataModelo) o).getDS_Modelo());
  }

} // fin clase

// lista de valores  enf para aplicaci�n  EDO
class CListaEnfCie
    extends CListaValores {

  public CListaEnfCie(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descripcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEnferedo(s, s); // El servlet se encarga de tomar la cadena
    // oportuna.
  }

  public String getCodigo(Object o) {
    return ( ( (DataEnferedo) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEnferedo) o).getDes());
  }

}

// lista de valores de Enfermedades Centinelas
class CListaEnfCenti
    extends CListaValores {

  public CListaEnfCenti(CApp a,
                        String title,
                        StubSrvBD stub,
                        String servlet,
                        int obtener_x_codigo,
                        int obtener_x_descricpcion,
                        int seleccion_x_codigo,
                        int seleccion_x_descripcion) {

    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEnfCenti(s, s); // El servlet se encarga de tomar la cadena
    // necesaria.
  }

  public String getCodigo(Object o) {
    return ( ( (DataEnfCenti) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEnfCenti) o).getDes());
  }
}

// lista de valores
class CListaNiv1
    extends CListaValores {

  public CListaNiv1(CApp a,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

// lista de valores
class CListaNiv2
    extends CListaValores {

  DialMantModelo dial;

  public CListaNiv2(DialMantModelo a,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(a.getCApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    dial = a;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s, "", dial.area.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

class DialMantModelo_txtPartirFichero_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialMantModelo adaptee;

  DialMantModelo_txtPartirFichero_keyAdapter(DialMantModelo adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtPartirFichero_keyPressed(e);
  }
}

class DialMantModelo_partir_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialMantModelo adaptee;

  DialMantModelo_partir_keyAdapter(DialMantModelo adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.partir_keyPressed(e);
  }
}

class DialMantModelo_btnFichero_actionAdapter
    implements java.awt.event.ActionListener {
  DialMantModelo adaptee;

  DialMantModelo_btnFichero_actionAdapter(DialMantModelo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnFichero_actionPerformed(e);
  }
}

class DialMantModelo_btnRepositorio_actionAdapter
    implements java.awt.event.ActionListener {
  DialMantModelo adaptee;

  DialMantModelo_btnRepositorio_actionAdapter(DialMantModelo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnRepositorio_actionPerformed(e);
  }
}
