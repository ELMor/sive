package confprot;

import java.io.Serializable;

import capp.CLista;

public class DataMantLineasModelo
    implements Serializable {
  String sCodModelo = "";
  String sDescModelo = "";
  String sProceso = "";
  String sTSive = null;
  protected CLista lineasModelo = null;

  public DataMantLineasModelo() {
  }

  public DataMantLineasModelo(String CodModelo) {
    sCodModelo = CodModelo;
    sDescModelo = null;
    sProceso = null;
  }

  public DataMantLineasModelo(String CodModelo,
                              String DescModelo,
                              String Proceso) {
    sCodModelo = CodModelo;
    sDescModelo = DescModelo;
    sProceso = Proceso;
  }

  public DataMantLineasModelo(String CodModelo,
                              String TSive) {
    sCodModelo = CodModelo;
    sDescModelo = null;
    sProceso = null;
    sTSive = TSive;
  }

  public CLista getLineas() {
    return lineasModelo;
  }

  public void setLineas(CLista lineas) {
    lineasModelo = lineas;
  }
}