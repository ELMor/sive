package brotes.servidor.mantmuestras;

//import sapp.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import brotes.datos.comun.Secuenciador;
import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;

public class SrvMantMuestras
    extends DBServlet {

  final protected int servletSEL_MUESTRAS = 1;
  final protected int servletALTA_MUESTRAS = 3;
  final protected int servletALTA_MICROTOX = 2;

  // Funcionalidad del servlet
  protected Lista doWork(int opmode, Lista param) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = null;

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    Lista listaSalida = new Lista();
    Data dataEntrada = null;
    Data dataSalida = null;
    boolean bFiltro = false;
    int iValorFiltro = 0;
    Lista lisPeticion = null;

    // Lleva la cuenta del numero de registros a�adidos para el tramado
    int iNRegs = 1;

    // Preparacion del patron de la query
    String sQuery = "SELECT mu.NM_MUESTRA, mu.CD_MUESTRA, mu.CD_ANO, " +
        "mu.NM_ALERBRO, mu.DS_RMUESTRA, mu.NM_MUESBROTE, " +
        "mu.NM_POSITBROTE, mi.CD_AGENTET, ag.DS_AGENTET, " +
        "ag.CD_GRUPO, mi.DS_RESCUANTI, orig.DS_MUESTRA, mi.NM_MICROTOX " +
        "FROM SIVE_MUESTRAS_BROTE mu, SIVE_MICROTOX_MUESTRAS mi, " +
        "SIVE_ORIGEN_MUESTRA orig, SIVE_AGENTE_TOXICO ag " +
        "WHERE mu.NM_MUESTRA = mi.NM_MUESTRA " +
        "AND mu.CD_MUESTRA= orig.CD_MUESTRA " +
        "AND mi.CD_AGENTET = ag.CD_AGENTET " +
        "AND mu.CD_ANO = ? " +
        "AND mu.NM_ALERBRO= ? " +
        "AND mi.CD_GRUPO= ? ";

    // Ordenamiento
//      sQuery = sQuery + "ORDER BY orig.CD_MUESTRA";
    sQuery = sQuery + "ORDER BY orig.CD_MUESTRA, mu.DS_RMUESTRA,ag.DS_AGENTET";

    final String sALTA_MUESTRAS =
        " insert into SIVE_MUESTRAS_BROTE  "
        + "( NM_MUESTRA, CD_MUESTRA, CD_ANO,"
        + " NM_ALERBRO, DS_RMUESTRA, NM_MUESBROTE,"
        + " NM_POSITBROTE )"
        + " values (?,?,?,?,?,?,?)";

    final String sALTA_MICROTOX =
        " insert into SIVE_MICROTOX_MUESTRAS"
        + "( NM_MICROTOX, NM_MUESTRA, CD_GRUPO,"
        + " CD_AGENTET, DS_MICROTOX, DS_RESCUANTI )"
        + " values (?,?,?,?,?,?)";

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    //  con = openConnection();
    try {
      con.setAutoCommit(false);

      dataEntrada = (Data) param.elementAt(0);

      switch (opmode) {

        case servletSEL_MUESTRAS:

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          st.setString(1, dataEntrada.getString("CD_ANO"));
          st.setInt(2,
                    (new Integer( (String) dataEntrada.get("NM_ALERBRO"))).intValue());
          st.setString(3, dataEntrada.getString("CD_GRUPO"));

          // Ejecucion de la query, obteniendo un ResultSet
          rs = st.executeQuery();

          // Procesamiento de cada registro obtenido de la BD
          while (rs.next()) {
            String NmMuestra = (new Integer(rs.getInt("NM_MUESTRA"))).toString();
            String CdMuestra = rs.getString("CD_MUESTRA");
            String Ano = rs.getString("CD_ANO");
            String NmAlerbro = (new Integer(rs.getInt("NM_ALERBRO"))).toString();
            String DsRMuestra = rs.getString("DS_RMUESTRA");
            String NmMuesbrote = (new Integer(rs.getInt("NM_MUESBROTE"))).
                toString();
            String NmPositbrote = (new Integer(rs.getInt("NM_POSITBROTE"))).
                toString();
            String DsMuestra = rs.getString("DS_MUESTRA");
            String DsRescuanti = null;
            if (rs.getString("DS_RESCUANTI") == null) {
              DsRescuanti = "";
            }
            else {
              DsRescuanti = rs.getString("DS_RESCUANTI");
            }
            String CdAgentet = rs.getString("CD_AGENTET");
            String DsAgentet = rs.getString("DS_AGENTET");
            String CdGrupo = rs.getString("CD_GRUPO");
            String NmMicrotox = (new Integer(rs.getInt("NM_MICROTOX"))).
                toString();

            // Relleno del registro de salida correspondiente a esta iteracion por
            //  los registros devueltos por la query
            dataSalida = new Data();
            dataSalida.put("NM_MUESTRA", NmMuestra);
            dataSalida.put("CD_MUESTRA", CdMuestra);
            dataSalida.put("CD_ANO", Ano);
            dataSalida.put("NM_ALERBRO", NmAlerbro);
            dataSalida.put("DS_RMUESTRA", DsRMuestra);
            dataSalida.put("NM_MUESBROTE", NmMuesbrote);
            dataSalida.put("NM_POSITBROTE", NmPositbrote);
            dataSalida.put("DS_MUESTRA", DsMuestra);
            dataSalida.put("DS_RESCUANTI", DsRescuanti);
            dataSalida.put("CD_AGENTET", CdAgentet);
            dataSalida.put("DS_AGENTET", DsAgentet);
            dataSalida.put("CD_GRUPO", CdGrupo);
            dataSalida.put("NM_MICROTOX", NmMicrotox);

            // Adicion del registro obtenido a la lista resultado de salida hacia cliente
            listaSalida.addElement(dataSalida);

          } // Fin while
          break;

        case servletALTA_MUESTRAS:

          //Secuenciador
          String cod = Secuenciador.getSecuenciador(con, st, rs, "NM_MUESTRA",
              7);
          dataEntrada.put("NM_MUESTRA", cod);

          /*         // prepara la query
                   st = con.prepareStatement(sALTA_MUESTRAS);
                   // completa los valores ?
                   st.setString(1, (String)dataEntrada.get("NM_MUESTRA"));
                   st.setString(2, (String)dataEntrada.get("CD_MUESTRA"));
                   st.setString(3, (String)dataEntrada.get("CD_ANO"));
                   st.setString(4, (String)dataEntrada.get("NM_ALERBRO"));
                   st.setString(5, (String)dataEntrada.get("DS_RMUESTRA"));
                   st.setString(6, (String)dataEntrada.get("NM_MUESBROTE"));
                   st.setString(7, (String)dataEntrada.get("NM_POSITBROTE"));
                   // ejecuta la query
                   st.executeUpdate();*/

          listaSalida.addElement(dataEntrada); //LRG

          break;
        case servletALTA_MICROTOX:

          //Secuenciador
          String cod1 = Secuenciador.getSecuenciador(con, st, rs, "NM_MICROTOX",
              7);
          dataEntrada.put("NM_MICROTOX", cod1);
          dataEntrada.put("DS_MICROTOX", "");

          // prepara la query
          st = con.prepareStatement(sALTA_MICROTOX);
          // completa los valores ?
          st.setString(1, (String) dataEntrada.get("NM_MICROTOX"));
          st.setString(2, (String) dataEntrada.get("NM_MUESTRA"));
          st.setString(3, (String) dataEntrada.get("CD_GRUPO"));
          st.setString(4, (String) dataEntrada.get("DS_AGENTET"));
          st.setString(5, (String) dataEntrada.get("DS_MICROTOX"));
          st.setString(6, (String) dataEntrada.get("DS_RESCUANTI"));

          // ejecuta la query
          st.executeUpdate();

          listaSalida.addElement(dataEntrada); //LRG
          break;
      } //end switch

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      ex.printStackTrace();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);
    return listaSalida;
  } // Fin doWork()

}
