/**
 * Clase: SrvSint
 * Paquete: brotes.servidor.pansint
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 09/12/1999
 * Analisis Funcional: Punto 2. Mantenimiento Brotes.
 * Descripcion: Implementacion del servlet que recupera de la BD los datos
 *   a mostrar en la tabla del PanSint: sintomas de un brote.
 */

package brotes.servidor.pansint;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

// SOLO DESARROLLO
//import jdbcpool.*;
import brotes.datos.pansint.DatSintCS;
import brotes.datos.pansint.DatSintSC;
import capp.CLista;
import sapp.DBServlet;

public class SrvSint
    extends DBServlet {

  // Temporales
  String tmpDescSint = "";
  String tmpFlMasInf = "";

  // SOLODESARROLLO
  /*public CLista doDebug(int opmode, CLista param) throws Exception {
      jdcdriver = new JDCConnectionDriver ("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
      return doWork(opmode, param);
     } */

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista();
    DatSintCS dataEntrada = null;
    DatSintSC dataSalida = null;
    boolean bFiltro = false;
    int iValorFiltro = 0;

    // Lleva la cuenta del numero de registros a�adidos para el tramado
    int iNRegs = 1;

    // Querys
    String sQuery = "";

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con = openConnection();
    con.setAutoCommit(false);

    try {
      // Recuperacion de los datos de busqueda
      dataEntrada = (DatSintCS) param.firstElement();

      // Preparacion del patron de la query
      sQuery = "SELECT CD_ANO, NM_ALERBRO, CD_SINTOMA, " +
          "DS_MASINF, NM_CASOS " +
          "FROM SIVE_SINTOMAS_BROTE " +
          "WHERE CD_ANO = ? AND NM_ALERBRO = ?";

      // Tramado
      if (param.getFilter().length() > 0) {
        bFiltro = true;
        iValorFiltro = (new Integer(param.getFilter().trim())).intValue();
        sQuery = sQuery + " AND CD_SINTOMA > ? ";
      }

      // Ordenamiento
      sQuery = sQuery + "ORDER BY CD_SINTOMA";

      // Preparacion de la sentencia SQL
      st = con.prepareStatement(sQuery);

      // Instanciamos la query con los valores de los datos de entrada
      InstanciarQuery(dataEntrada, st, bFiltro, iValorFiltro);

      // Ejecucion de la query, obteniendo un ResultSet
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      while (rs.next()) {
        // Si iNRegs es > que el maximo numero de registros por peticion
        //  se sale del bucle dejando la lista en estado INCOMPLETA
        if (iNRegs > DBServlet.maxSIZE) {
          listaSalida.setState(CLista.listaINCOMPLETA);
          listaSalida.setFilter( ( (DatSintSC) listaSalida.lastElement()).
                                getCD_SINTOMA());
          break;
        }

        // Recuperacion del CD_ANO, NM_ALERBRO, CD_SINTOMA, DS_MASINF, NM_CASOS
        //  a partir del ResultSet de la query realizada sobre la BD
        String Ano = rs.getString("CD_ANO");
        int iNBrote = rs.getInt("NM_ALERBRO");
        String NBrote = (new Integer(iNBrote)).toString();

        String Sint = rs.getString("CD_SINTOMA");
        getDescSintoma(con, Sint);
        String DescSint = tmpDescSint;
        String FlMasInf = tmpFlMasInf;

        String DescMasInf = rs.getString("DS_MASINF");
        int iNCasos = rs.getInt("NM_CASOS");
        String NCasos = (new Integer(iNCasos)).toString();
        String Ope = "";
        String FUlt = "";

        // Relleno del registro de salida correspondiente a esta iteracion por
        //  los registros devueltos por la query
        dataSalida = new DatSintSC(Ano, NBrote, Sint, DescSint, FlMasInf,
                                   DescMasInf, NCasos, Ope, FUlt);

        // Adicion del registro obtenido a la lista resultado de salida hacia cliente
        listaSalida.addElement(dataSalida);

        // Se aumenta el numero de registros a�adidos
        iNRegs++;
      } // Fin for each registro

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);

    return listaSalida;
  } // Fin doWork()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void InstanciarQuery(DatSintCS dataEntrada,
                                 PreparedStatement st, boolean bFiltro,
                                 int iValorFiltro) throws Exception {

    // Contador de posicion del parametro
    int iParN = 1;

    // A�o
    st.setString(iParN++, dataEntrada.getCD_ANO());

    // Num. Brote
    String nmBrote = dataEntrada.getNM_ALERBRO().trim();
    st.setInt(iParN++, (new Integer(nmBrote)).intValue());

    // Tramado
    if (bFiltro) {
      st.setInt(iParN++, iValorFiltro);

    }
  } // Fin InstanciarQuery()

  // Conversor de fecha String a java.sql.Date
  protected java.sql.Date StringToSQLDate(String sFecha) throws Exception {

    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    java.util.Date uFecha = formater.parse(sFecha);
    java.sql.Date sqlFec = new java.sql.Date(uFecha.getTime());
    return sqlFec;
  } // Fin StringToSQLDate()

  // Conversor de fecha java.util.Date a String
  protected String UtilDateToString(java.util.Date uFecha) throws Exception {

    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    String sFecha = "";
    if (uFecha == null) {
      sFecha = "";
    }
    else {
      sFecha = formater.format(uFecha);

    }
    return (sFecha);
  } // Fin UtilDateToString()

  // Obtiene la descr. de un sintoma y si requiere mas informacion
  void getDescSintoma(Connection c, String CodSint) throws Exception {
    if (CodSint == null || CodSint.equals("")) {
      tmpDescSint = "";
      tmpFlMasInf = "";
      return;
    }

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Query
    String Query =
        "select DS_SINTOMA, IT_MASINF from SIVE_SINTOMAS where CD_SINTOMA = ?";
    String res = null;

    try {
      // Preparacion de la sentencia SQL
      st = c.prepareStatement(Query);

      // CodSintoma
      st.setString(1, CodSint.trim());

      // Ejecucion de la query
      rs = st.executeQuery();

      // Almacenamos la DS_SINTOMA y IT_MASINF obtenida
      rs.next();
      tmpDescSint = rs.getString("DS_SINTOMA");
      tmpFlMasInf = rs.getString("IT_MASINF");

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }
  } // Fin getDescSintoma()

} // Fin SrvSint
