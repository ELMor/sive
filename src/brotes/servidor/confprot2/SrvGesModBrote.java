package brotes.servidor.confprot2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import brotes.datos.confprot2.DataLineaItemBrote;
import brotes.datos.confprot2.DataLineasMBrote;
import brotes.datos.confprot2.DataModeloBrote;
import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvGesModBrote
    extends DBServlet {
  // Por si se desea filtrar por una enfermedad en concreto
  private String sQueryEnfermedad = "";
  private String sCdEnfermedad = null;

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletNO_OPERATIVO = 10;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {
    // variables temporales
    String CD_TSIVE = "";
    String CD_MODELO = "";
    String DS_MODELO = "";
    String DSL_MODELO = "";
    String CD_NIVEL_1 = "";
    String CD_NIVEL_2 = "";
    String CD_ENFCIE = "";
    int NM_ALERBRO = 0;
    String CD_ANO = "";
    String IT_OK = "";
    String CD_CA = "";
    java.sql.Date FC_BAJAM = null;
    int numAux = 0;

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;
    int j = 1;

    // objetos de datos
    CLista data = new CLista();
    DataModeloBrote dModelo = null;
    DataLineasMBrote dLineas = null;
    DataLineaItemBrote dItems = null;
    String fechaCadena = null;
    SimpleDateFormat formato1 = null;
    SimpleDateFormat formato2 = null;

    //registro que va a ser modificado
    DataModeloBrote anterior = null;
    java.sql.Date bajaAnterior = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    dModelo = (DataModeloBrote) param.firstElement();

//quitado1
    /*    if (param.getCdEnfermedad() != null) {
          if (!param.getCdEnfermedad().equals("")) {
            sQueryEnfermedad = " CD_ENFCIE = ? and ";
            sCdEnfermedad = param.getCdEnfermedad();
          }
        }
         /*mlm:
           lo que habia:
           sAutorizacion1 = " and CD_NIVEL_1 IN (select CD_NIVEL_1 from SIVE_AUTORIZACIONES where CD_USUARIO = ?)";
           sAutorizacion2 = " and CD_NIVEL_2 IN (select CD_NIVEL_2 from SIVE_AUTORIZACIONES where CD_USUARIO = ? and CD_NIVEL_1 = ?)";
           nuevo:
           1,1,2
           2,3,1
           para sAutorizaciones2 debe salir solo 2,3 si nivel_1 era 1 */
//quitado2
     /*    Vector vN1 = (Vector)param.getVN1();
         Vector vN2 = (Vector)param.getVN2();
         String n1 = "";
         String n1Fijo = "";
         int k =0;
         for (k=0; k < vN1.size(); k++) {
             n1 = n1 + ", " + (String) vN1.elementAt(k);
         }
         String n2 = "";
         for (k=0; k < vN2.size(); k++) {
           /*if (nivel.getNivel1() != null)
              n1Fijo = nivel.getNivel1().trim(); */
      //quitar lo que sobra-si el correspondiente 1 es igual al fijo, ok)
      /*if ( ((String) vN1.elementAt(k)).equals(n1Fijo) ) */
//quitado 3
      /*        n2 = n2 + ", " +  (String) vN2.elementAt(k);
          }
          //quitamos las comas primeras
          if (!n1.trim().equals("")) n1 = n1.substring(1);
           if (!n2.trim().equals("")) n2 = n2.substring(1);
          //------------------------------------------------
       */
//fin quitado 1,2,3

    data.setState(CLista.listaVACIA);
    data = null;
    try {

      // modos de operaci�n
      switch (opmode) {

        case servletALTA:

          // insertamos el modelo
          query = "insert into SIVE_MODELO " +
              " (CD_TSIVE, CD_MODELO, DS_MODELO," +
              " DSL_MODELO, CD_NIVEL_1, CD_NIVEL_2, " +
              " CD_CA, FC_ALTAM, IT_OK, FC_BAJAM," +
              " CD_OPE, CD_ENFCIE, FC_ULTACT," +
              " CD_ANO, NM_ALERBRO) " +
              " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

          st = con.prepareStatement(query);
          // codigo TSIVE
          st.setString(1, dModelo.getCD_TSIVE().trim());
          // c�digo modelo
          st.setString(2, dModelo.getCD_Modelo().trim());
          // descripci�n modelo
          st.setString(3, dModelo.getDS_Modelo().trim());

          // descripci�n en ingl�s
          if (dModelo.getDSL_Modelo().trim().length() > 0) {
            st.setString(4, dModelo.getDSL_Modelo().trim());
          }
          else {
            st.setNull(4, java.sql.Types.VARCHAR);
          }

          // c�digo nivel1:siempre a null
          st.setNull(5, java.sql.Types.VARCHAR);
          // c�digo nivel2:siempre a null
          st.setNull(6, java.sql.Types.VARCHAR);
          // c�digo CA:siempre a null
          st.setNull(7, java.sql.Types.VARCHAR);
          // fecha alta
          st.setDate(8, new java.sql.Date( (new java.util.Date()).getTime()));
          // ok
          st.setString(9, dModelo.getIT_OK().trim());
          // fecha baja : alta=NULL
          st.setNull(10, java.sql.Types.VARCHAR);
          // c�digo operador
          st.setString(11, param.getLogin());

          // c�digo enfcie:siempre a null
          st.setNull(12, java.sql.Types.VARCHAR);
          // �ltima actualizaci�n
          st.setDate(13, new java.sql.Date( (new java.util.Date()).getTime()));
          //c�digo de a�o de brote
          st.setString(14, dModelo.getCD_Ano().trim());
          //n�mero de alerta del brote
          st.setInt(15, dModelo.getNM_Alerbro());

          st.executeUpdate();
          st.close();
          st = null;

          // Primero comprobamos que existe alg�n modelo a heredar
          if (dModelo.getPartir().compareTo("") != 0) {

            numAux = 1;
            if (numAux != 0) {
              // ahora vamos a crear la estructura de la que partimoS
              query = "SELECT CD_TSIVE, CD_MODELO, " +
                  " NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO " +
                  "FROM SIVE_LINEASM WHERE(CD_TSIVE=? AND CD_MODELO=? )";

              st = con.prepareStatement(query);

              // c�digo TSIVE
              st.setString(1, dModelo.getCD_TSIVE().trim());

              // c�digo modelo
              st.setString(2, dModelo.getPartir().trim());

              // lanza la query
              st.executeQuery();

              rs = st.getResultSet();

              // recorremos el resultSet

              i = 0;
              data = new CLista();
              while (rs.next()) {
                // a�ade un nodo
                data.addElement(new DataLineasMBrote(rs.getString("CD_TSIVE"),
                    rs.getString("CD_MODELO"),
                    rs.getInt("NM_LIN"),
                    rs.getString("CD_TLINEA"),
                    rs.getString("DS_TEXTO"),
                    rs.getString("DSL_TEXTO")));
                i++;
              }

              st.close();
              rs.close();
              st = null;
              rs = null;

              //si se ha introducido alguna l�nea para el nuevo
              //modelo creado (las del modelo del q parte)...
              if (i != 0) {
                // ahora recorremos data(1 CLista) para introducir
                //esos datos como del nuevo modelo creado: cambia cd_modelo
                for (j = 0; j < data.size(); j++) {
                  dLineas = (DataLineasMBrote) data.elementAt(j);
                  // insertamos nuestro c�digo
                  query = "insert into SIVE_LINEASM " +
                      "(CD_TSIVE, CD_MODELO, NM_LIN, CD_TLINEA, " +
                      " DS_TEXTO, DSL_TEXTO ) " +
                      " values(?,?,?,?,?,?)";

                  st = con.prepareStatement(query);
                  // c�digo TSIVE
                  st.setString(1, dModelo.getCD_TSIVE().trim());
                  // c�digo modelo
                  st.setString(2, dModelo.getCD_Modelo());
                  // n�mero de l�neas
                  st.setInt(3, dLineas.getNM_LIN());
                  // tLinea
                  st.setString(4, dLineas.getCD_TLINEA().trim());
                  // Desc. texto
                  st.setString(5, dLineas.getDS_TEXTO().trim());
                  // Desc. texto l.
                  if (dLineas.getDSL_TEXTO() != null) {
                    st.setString(6, dLineas.getDSL_TEXTO().trim());
                  }
                  else {
                    st.setNull(6, java.sql.Types.VARCHAR);
                  }

                  st.executeUpdate();
                  st.close();
                  st = null;
                }

                // hacemos lo mismo con linea_item
                // primero comprobamos que existe alg�n registro
                numAux = 1;
                if (numAux != 0) {

                  query = "select CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, " +
                      " CD_PREGUNTA_PC, IT_OBLIGATORIO, IT_CONDP, " +
                      " NM_LIN_PC, DS_VPREGUNTA_PC " +
                      " from SIVE_LINEA_ITEM " +
                      " where( CD_TSIVE=? and CD_MODELO=? ) " +
                      " order by NM_LIN";

                  st = con.prepareStatement(query);
                  // c�digo TSIVE
                  st.setString(1, dModelo.getCD_TSIVE().trim());
                  // c�digo modelo
                  st.setString(2, dModelo.getPartir().trim());
                  // lanza la query
                  st.executeQuery();
                  rs = null;
                  rs = st.getResultSet();
                  data = new CLista();
                  // recorremos el resultSet
                  i = 0;
                  while (rs.next()) {
                    // a�ade un nodo
                    data.addElement(new DataLineaItemBrote(rs.getString(
                        "CD_TSIVE"),
                        rs.getString("CD_MODELO"),
                        rs.getInt("NM_LIN"),
                        rs.getString("CD_PREGUNTA"),
                        rs.getString("CD_PREGUNTA_PC"),
                        rs.getString("IT_OBLIGATORIO"),
                        rs.getString("IT_CONDP"),
                        rs.getInt("NM_LIN_PC"),
                        rs.getString("DS_VPREGUNTA_PC")));
                    i++;
                  }

                  st.close();
                  st = null;
                  rs.close();
                  rs = null;
                  // ahora recorremos data (CLista) y a�adimos los
                  //valores, pero con el c�digo del modelo actual
                  if (i != 0) {

                    query = "insert into SIVE_LINEA_ITEM " +
                        "( CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, " +
                        " CD_PREGUNTA_PC, IT_OBLIGATORIO, IT_CONDP, " +
                        " NM_LIN_PC, DS_VPREGUNTA_PC )" +
                        " values(?,?,?,?,?,?,?,?,?)";

                    for (j = 0; j < data.size(); j++) {
                      dItems = (DataLineaItemBrote) data.elementAt(j);
                      st = con.prepareStatement(query);

                      // c�digo TSIVE
                      st.setString(1, dModelo.getCD_TSIVE());

                      // c�digo modelo
                      st.setString(2, dModelo.getCD_Modelo());

                      // num. l�n.
                      st.setInt(3, dItems.getNM_LIN());

                      // cd preg.
                      st.setString(4, dItems.getCD_PREGUNTA().trim());

                      // it. oblig.
                      st.setString(6, dItems.getIT_OBLIGATORIO().trim());

                      // it. condp.
                      st.setString(7, dItems.getIT_CONDP().trim());

                      // num. l�n. pc
                      st.setInt(8, dItems.getNM_LIN_PC());

                      if (dItems.getIT_CONDP().equals("S")) {

                        // cd preg. pc
                        st.setString(5, dItems.getCD_PREGUNTA_PC().trim());

                        // vpreg.
                        st.setString(9, dItems.getDS_VPREGUNTA_PC().trim());

                      }
                      else {
                        st.setNull(5, java.sql.Types.VARCHAR);
                        st.setNull(9, java.sql.Types.VARCHAR);
                      }

                      // lanza la query
                      st.executeUpdate();

                      st.close();
                      st = null;
                    }
                  }
                }
              }
            }
          }
          data = null;
          break;

          // baja de modelo
        case servletBAJA:

          // borra las l�neas: sive_linea_item
          query = "DELETE FROM SIVE_LINEA_ITEM " +
              " WHERE CD_TSIVE = ? and CD_MODELO = ?";
          st = con.prepareStatement(query);
          st.setString(1, dModelo.getCD_TSIVE());
          st.setString(2, dModelo.getCD_Modelo());
          st.executeUpdate();
          st.close();
          st = null;

          // borra las l�neas: sive_lineasm
          query = "DELETE FROM SIVE_LINEASM " +
              " WHERE CD_TSIVE = ? AND CD_MODELO = ?";
          st = con.prepareStatement(query);
          st.setString(1, dModelo.getCD_TSIVE());
          st.setString(2, dModelo.getCD_Modelo());
          st.executeUpdate();
          st.close();
          st = null;

          // borra el modelo
          query = "DELETE FROM SIVE_MODELO " +
              " WHERE CD_MODELO = ? AND CD_TSIVE = ?";
          st = con.prepareStatement(query);
          st.setString(1, dModelo.getCD_Modelo());
          st.setString(2, dModelo.getCD_TSIVE());
          st.executeUpdate();
          st.close();
          st = null;

          break;

        case servletNO_OPERATIVO:

          // prepara la query de modificaci�n a no operativo
          query = "UPDATE SIVE_MODELO SET IT_OK = ?, FC_BAJAM = ? " +
              " WHERE CD_TSIVE = ? AND CD_MODELO = ?";

          // lanza la query
          st = con.prepareStatement(query);
          st.setString(1, "N");
          st.setDate(2, new java.sql.Date( (new java.util.Date()).getTime()));
          st.setString(3, dModelo.getCD_TSIVE());
          st.setString(4, dModelo.getCD_Modelo());

          st.executeUpdate();
          st.close();
          st = null;

          break;

          /****************************************************************/

          // b�squeda de modelo
        case servletSELECCION_X_CODIGO:
        case servletSELECCION_X_DESCRIPCION:

              /*        query= "select * from SIVE_MODELO where CD_ANO is not null "+
                           "and NM_ALERBRO is not null "+
               "and exists (select CD_ANO,NM_ALERBRO from SIVE_INVESTIGADOR "+
               "where CD_USUARIO= ? and CD_ANO=SIVE_MODELO.CD_ANO and "+
                            "NM_ALERBRO =SIVE_MODELO.NM_ALERBRO)";
           */
          query = "select * from SIVE_MODELO where " +
              "exists (select CD_ANO,NM_ALERBRO from SIVE_INVESTIGADOR " +
              "where CD_USUARIO= ? and CD_ANO=SIVE_MODELO.CD_ANO and " +
              "NM_ALERBRO =SIVE_MODELO.NM_ALERBRO and CD_CA=?) ";

          // prepara la query
          if (param.getFilter().length() > 0) {
            if (opmode == servletSELECCION_X_CODIGO) {
              query = query +
                  " and CD_MODELO like ? and CD_MODELO > ? order by CD_MODELO";
            }
            else {
              query = query +
                  " and DS_MODELO like ? and DS_MODELO > ? order by DS_MODELO";
            }
          }
          else {
            if (opmode == servletSELECCION_X_CODIGO) {
              query = query + " and CD_MODELO like ? order by CD_MODELO";
            }
            else {
              query = query + " and DS_MODELO like ? order by DS_MODELO";
            }
          }

          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);

          String sca = dModelo.getCD_CA();
          st.setString(1, param.getLogin());
          //AQUI
          st.setString(2, sca);

          // filtro
          st.setString(3, dModelo.getCD_Modelo().trim() + "%");
          // paginaci�n
          if (param.getFilter().length() > 0) {
            st.setString(4, param.getFilter().trim());
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          i = 1;
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (opmode == servletSELECCION_X_CODIGO) {
                data.setFilter( ( (DataModeloBrote) data.lastElement()).
                               getCD_Modelo());
              }
              else {
                data.setFilter( ( (DataModeloBrote) data.lastElement()).
                               getDS_Modelo());
              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            CD_TSIVE = (rs.getString("CD_TSIVE"));
            CD_MODELO = (rs.getString("CD_MODELO"));
            DS_MODELO = (rs.getString("DS_MODELO"));
            DSL_MODELO = (rs.getString("DSL_MODELO"));

            //___________________

            // obtiene la descripcion principal en funci�n del idioma si el cliente no es una CListaMantenimiento
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (param.getMantenimiento() == false)) {
              if (DSL_MODELO != null) {
                DS_MODELO = DSL_MODELO;
              }
            }
            //______________________

            /*          CD_NIVEL_1=(rs.getString("CD_NIVEL_1"));
                      CD_NIVEL_2=(rs.getString("CD_NIVEL_2"));
                      CD_CA=(rs.getString("CD_CA"));*/
            CD_ANO = (rs.getString("CD_ANO"));
            NM_ALERBRO = (rs.getInt("NM_ALERBRO"));
            IT_OK = (rs.getString("IT_OK"));
            CD_ENFCIE = (rs.getString("CD_ENFCIE"));
            FC_BAJAM = (rs.getDate("FC_BAJAM"));
            CD_CA = (rs.getString("CD_CA"));

            if (FC_BAJAM == null) {
              fechaCadena = "";
            }
            else {
              fechaCadena = FC_BAJAM.toString();
              formato1 = new SimpleDateFormat("dd/MM/yyyy");
              formato2 = new SimpleDateFormat("yyyy-MM-dd");
              java.util.Date miFecha = null;
              miFecha = formato2.parse(fechaCadena);
              fechaCadena = formato1.format(miFecha);
            }
            // a�ade un nodo
            data.addElement(new DataModeloBrote(CD_TSIVE, CD_MODELO,
                                                DS_MODELO, DSL_MODELO, CD_ANO,
                                                NM_ALERBRO, IT_OK,
                                                "", fechaCadena, CD_ENFCIE,
                                                CD_CA));
            i++;
          }
          rs.close();
          st.close();

          break;

          /****************************************************************/

          // modificaci�n de modelo
        case servletMODIFICAR:

//______Se obtienen datos anteriores de Fecha baja y IT_OK ____________________________________________________________________

          query = "select * from SIVE_MODELO " +
              " where CD_TSIVE = ? and CD_MODELO  = ?";

          dModelo = (DataModeloBrote) param.firstElement();
          data = new CLista();
          st = con.prepareStatement(query);

          st.setString(1, param.getTSive());
          st.setString(2, dModelo.getCD_Modelo().trim());

          rs = st.executeQuery();

          while (rs.next()) {
            CD_TSIVE = (rs.getString("CD_TSIVE"));
            CD_MODELO = (rs.getString("CD_MODELO"));
            DS_MODELO = (rs.getString("DS_MODELO"));
            DSL_MODELO = (rs.getString("DSL_MODELO"));

            //___________________

            // obtiene la descripcion principal en funci�n del idioma si el cliente no es una CListaMantenimiento
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                && (param.getMantenimiento() == false)) {
              if (DSL_MODELO != null) {
                DS_MODELO = DSL_MODELO;
              }
            }
            //______________________

            CD_NIVEL_1 = (rs.getString("CD_NIVEL_1"));
            CD_NIVEL_2 = (rs.getString("CD_NIVEL_2"));
            CD_CA = (rs.getString("CD_CA"));
            IT_OK = (rs.getString("IT_OK"));
            CD_ENFCIE = (rs.getString("CD_ENFCIE"));
            FC_BAJAM = (rs.getDate("FC_BAJAM"));

            if (FC_BAJAM == null) {
              fechaCadena = "";
            }
            else {
              fechaCadena = FC_BAJAM.toString();
            }
            // a�ade un nodo

            anterior = new DataModeloBrote(CD_TSIVE, CD_MODELO,
                                           DS_MODELO, DSL_MODELO, CD_ANO,
                                           NM_ALERBRO, IT_OK,
                                           "", fechaCadena, CD_ENFCIE, CD_CA);

          }
          rs.close();
          st.close();
          st = null;
          rs = null;
//_________________________________________________________________________

          /* prepara la query: en caso de q el modelo se modifique pas�ndolo
               a opertativo, se debe asegurar q ninguno de los modelos q existen
                   para ese brote est� a su vez operativo*/
          //Si el modelo q se modifica se pone como operativo
          if (dModelo.getIT_OK().equals("S") && anterior.getIT_OK().equals("N")) {

            //se pasan todos los modelos de ese mismo brote a no operativos
            //indicando la fecha en q se han dado de baja de operativos.
            query = "update SIVE_MODELO set IT_OK=? , FC_BAJAM = ? " +
                " where CD_TSIVE = ? AND CD_ANO=? AND NM_ALERBRO=? and IT_OK = ? " +
                " and CD_CA is NULL and CD_NIVEL_1 is NULL and CD_NIVEL_2 is NULL";

            st = con.prepareStatement(query);

            //Marca de no operativo
            st.setString(1, "N");
            //Fecha de baja : actual
            st.setDate(2, new java.sql.Date( (new java.util.Date()).getTime()));
            //TIPO
            st.setString(3, param.getTSive());
            //cd_Ano
            st.setString(4, dModelo.getCD_Ano());
            //nm_Alerbro
            st.setInt(5, dModelo.getNM_Alerbro());
            //Marca de operativo (los que buscamos)
            st.setString(6, "S");

            st.executeUpdate();
            st.close();
            st = null;
          }

          // ahora modificaremos nuestro registro
          /*        query = "insert into SIVE_MODELO "+
                  " (CD_TSIVE, CD_MODELO, DS_MODELO,"+
                  " DSL_MODELO, CD_NIVEL_1, CD_NIVEL_2, "+
                  " CD_CA, FC_ALTAM, IT_OK, FC_BAJAM,"+
                  " CD_OPE, CD_ENFCIE, FC_ULTACT,"+
                  " CD_ANO, NM_ALERBRO) "+
                  " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
           */
          query = "UPDATE SIVE_MODELO SET DS_MODELO=?," +
              " DSL_MODELO=?,IT_OK=?," +
              " FC_BAJAM =?, " +
              " CD_OPE=?, CD_ENFCIE=?, FC_ULTACT=?, " +
              " CD_ANO=?, NM_ALERBRO=? " +
              " WHERE CD_TSIVE = ? AND CD_MODELO = ? ";

          // lanza la query
          st = con.prepareStatement(query);

          // descripci�n modelo
          st.setString(1, dModelo.getDS_Modelo().trim());

          // descripci�n en ingl�s
          if (dModelo.getDSL_Modelo().trim().length() > 0) {
            st.setString(2, dModelo.getDSL_Modelo().trim());
          }
          else {
            st.setNull(2, java.sql.Types.VARCHAR);

            // activo
          }
          st.setString(3, dModelo.getIT_OK().trim());

          //Fecha de baja. Su valor depende de si estaba operativo antes o no
          //Si antes no estaba operativo y ahora pasa a operativo:no hay fecha de baja
          if ( (anterior.getIT_OK().compareTo("N") == 0)
              && (dModelo.getIT_OK().trim().compareTo("S") == 0)) {
            st.setNull(4, java.sql.Types.VARCHAR);
          }
          //Si antes  estaba operativo y ahora pasa a  NO operativo:pone fecha actual
          else if ( (anterior.getIT_OK().compareTo("S") == 0)
                   && (dModelo.getIT_OK().trim().compareTo("N") == 0)) {
            st.setDate(4, new java.sql.Date( (new java.util.Date()).getTime()));
          }
          //Si antes  estaba operativo y ahora SIGUE a  operativo: no hay fecha de baja
          else if ( (anterior.getIT_OK().compareTo("S") == 0)
                   && (dModelo.getIT_OK().trim().compareTo("S") == 0)) {
            st.setNull(4, java.sql.Types.VARCHAR);
          }

          //Si antes  estaba NO operativo y ahora SIGUE a  NO operativo:Pone fecha que hab�a, que puede ser null (nunca estuvo operativo ) o no
          else if ( (anterior.getIT_OK().compareTo("N") == 0)
                   && (dModelo.getIT_OK().trim().compareTo("N") == 0)) {
            if (FC_BAJAM == null) {
              st.setNull(4, java.sql.Types.VARCHAR);
            }
            else {
              st.setDate(4, FC_BAJAM);
            }
          }

          // c�digo operador
          st.setString(5, param.getLogin());
          // c�digo enfcie: a null
          st.setNull(6, java.sql.Types.VARCHAR);
          // �ltima actualizaci�n
          st.setDate(7, new java.sql.Date( (new java.util.Date()).getTime()));
          //cd_Ano
          st.setString(8, dModelo.getCD_Ano().trim());
          //nm_Alerbro
          st.setInt(9, dModelo.getNM_Alerbro());
          // codigo TSIVE
          st.setString(10, dModelo.getCD_TSIVE().trim());
          // c�digo modelo
          st.setString(11, dModelo.getCD_Modelo().trim());

          // lanza la query
          st.executeUpdate();
          st.close();
          st = null;

          data = null;

          break;

        /****************************************************************/
        case servletOBTENER_X_CODIGO:
        case servletOBTENER_X_DESCRIPCION:

          //con esta query selecciono los modelos individuales
          //sin tener en cuenta el investigador q poseen
              /*        query= "select * from SIVE_MODELO where CD_ANO is not null "+
                           "and NM_ALERBRO is not null ";*/

          //obtiene todos los modelos de los brotes q ese usuario investiga
          query = "select * from SIVE_MODELO where CD_ANO is not null " +
              "and NM_ALERBRO is not null " +
              "and exists (select CD_ANO,NM_ALERBRO from SIVE_INVESTIGADOR " +
              "where CD_USUARIO= ? and CD_ANO=SIVE_MODELO.CD_ANO and " +
              "NM_ALERBRO =SIVE_MODELO.NM_ALERBRO and CD_CA=?)";
          dModelo = (DataModeloBrote) param.firstElement();
          data = new CLista();

          if (opmode == servletOBTENER_X_CODIGO) {
            query = query + " and CD_MODELO = ?"; //*****************
//          query = query + " and CD_MODELO  like ?"; //*****************
          }
          else {
            query = query + " and DS_MODELO = ?";
          }
          query = query + " order by CD_MODELO";
          st = con.prepareStatement(query);

          //QUITAR
          String ca = dModelo.getCD_CA();
          st.setString(1, param.getLogin());
          st.setString(2, ca);

          if (opmode == servletOBTENER_X_CODIGO) {
            st.setString(3, dModelo.getCD_Modelo().trim() + "%"); //*****************
          }
          else {
            st.setString(3, dModelo.getCD_Modelo().trim());
          }

          rs = st.executeQuery();

          while (rs.next()) {
            CD_TSIVE = (rs.getString("CD_TSIVE"));
            CD_MODELO = (rs.getString("CD_MODELO"));
            DS_MODELO = (rs.getString("DS_MODELO"));
            DSL_MODELO = (rs.getString("DSL_MODELO"));

            //___________________

            // obtiene la descripcion principal en funci�n del idioma si el cliente no es una CListaMantenimiento
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (param.getMantenimiento() == false)) {
              if (DSL_MODELO != null) {
                DS_MODELO = DSL_MODELO;
              }
            }
            //______________________

            /*          CD_NIVEL_1=(rs.getString("CD_NIVEL_1"));
                      CD_NIVEL_2=(rs.getString("CD_NIVEL_2"));
                      CD_CA=(rs.getString("CD_CA"));*/

            CD_ANO = (rs.getString("CD_ANO"));
            NM_ALERBRO = (rs.getInt("NM_ALERBRO"));
            IT_OK = (rs.getString("IT_OK"));
            CD_ENFCIE = (rs.getString("CD_ENFCIE"));
            FC_BAJAM = (rs.getDate("FC_BAJAM"));
            CD_CA = (rs.getString("CD_CA"));

            if (FC_BAJAM == null) {
              fechaCadena = "";
            }
            else {
              fechaCadena = FC_BAJAM.toString();
            }
            // a�ade un nodo

            data.addElement(new DataModeloBrote(CD_TSIVE, CD_MODELO,
                                                DS_MODELO, DSL_MODELO, CD_ANO,
                                                NM_ALERBRO, IT_OK,
                                                "", fechaCadena, CD_ENFCIE,
                                                CD_CA));

          }
          rs.close();
          st.close();
          break;

      }
      con.commit();
    }
    catch (Exception exs) {
      con.rollback();
      throw exs;
    }
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();
    }
    return data;
  }
}
