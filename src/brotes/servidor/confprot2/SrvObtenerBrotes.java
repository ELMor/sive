package brotes.servidor.confprot2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import brotes.datos.confprot2.DataBroteBasico;
import capp.CLista;
import sapp.DBServlet;

//�ltima versi�n
public class SrvObtenerBrotes
    extends DBServlet {

  public SrvObtenerBrotes() {
  }

  final protected int servletSEL_CASOS = 1;
  final protected int servletSEL_DESCBROTE = 2;
  final protected int servletSEL_DESCBROTELIM = 3;

  protected CLista doWork(int opmode, CLista param) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = null;

    // Objetos JDBC
    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista();
    String anoBrote = null;
    int alerBrote = 0;
    String anoEntrada = null;
    String usuEntrada = null;
    String comAutonoma = null;
    DataBroteBasico dataSalida = null;

    String sCdAno = null;
    String sNmAlerbro = null;
    String sCdGrupo = null;
    String cdTBrote = null;
    String dsBrote = null;

    final String sSEL_BROTES = "SELECT CD_ANO,NM_ALERBRO, " +
        "CD_GRUPO,CD_TBROTE,DS_BROTE FROM SIVE_BROTES " +
        "WHERE CD_ANO=? AND CD_ANO in (select CD_ANO " +
        "from SIVE_INVESTIGADOR where CD_USUARIO=? and CD_CA=?) " +
        "and NM_ALERBRO in (select NM_ALERBRO " +
        "from SIVE_INVESTIGADOR where CD_USUARIO=? " +
        "and CD_ANO=? and CD_CA=?) " +
        "order by NM_ALERBRO";

    final String sSEL_DESCBROTELIM = "SELECT DS_BROTE " +
        "FROM SIVE_BROTES " +
        "WHERE CD_ANO=? AND NM_ALERBRO=? " +
        "AND CD_ANO in (select CD_ANO " +
        "from SIVE_INVESTIGADOR where CD_USUARIO=? and CD_CA=?) " +
        "and NM_ALERBRO in (select NM_ALERBRO " +
        "from SIVE_INVESTIGADOR where CD_USUARIO=? " +
        "and CD_ANO=? and CD_CA=?) ";

    final String sSEL_DESCBROTE = "SELECT DS_BROTE " +
        "FROM SIVE_BROTES " +
        "WHERE CD_ANO=? AND NM_ALERBRO=?";

    try {
      con = openConnection();
      con.setAutoCommit(false);

      switch (opmode) {
        //Data q recibe: anoepini,semepiini,anoepifin,semepifin,cd_enfcie,cd_indalar
        case servletSEL_CASOS:
          if (param.size() != 0) {
            anoEntrada = (String) param.elementAt(0);
            usuEntrada = (String) param.elementAt(1);
            comAutonoma = (String) param.elementAt(2);
          }
          st = con.prepareStatement(sSEL_BROTES);

          st.setString(1, anoEntrada);
          st.setString(2, usuEntrada);
          st.setString(3, comAutonoma);
          st.setString(4, usuEntrada);
          st.setString(5, anoEntrada);
          st.setString(6, comAutonoma);

          rs = st.executeQuery();
          while (rs.next()) {
            sCdAno = (rs.getString("CD_ANO"));
            sNmAlerbro = (new Integer(rs.getInt("NM_ALERBRO"))).toString();
            sCdGrupo = (rs.getString("CD_GRUPO"));
            cdTBrote = (rs.getString("CD_TBROTE"));
            dsBrote = (rs.getString("DS_BROTE"));

            listaSalida.addElement(new DataBroteBasico(sCdAno, sNmAlerbro,
                sCdGrupo, cdTBrote, dsBrote));
          } //end while
          break;
        case servletSEL_DESCBROTE:
          if (param.size() != 0) {
            anoBrote = (String) param.elementAt(0);
            alerBrote = Integer.parseInt( (String) param.elementAt(1));
          }
          st = con.prepareStatement(sSEL_DESCBROTE);

          st.setString(1, anoBrote);
          st.setInt(2, alerBrote);

          rs = st.executeQuery();
          while (rs.next()) {
            dsBrote = (rs.getString("DS_BROTE"));

            listaSalida.addElement(dsBrote);
          } //end while
          break;

        case servletSEL_DESCBROTELIM:
          if (param.size() != 0) {
            anoBrote = (String) param.elementAt(0);
            alerBrote = Integer.parseInt( (String) param.elementAt(1));
            usuEntrada = (String) param.elementAt(2);
            comAutonoma = (String) param.elementAt(3);
          }
          st = con.prepareStatement(sSEL_DESCBROTELIM);

          st.setString(1, anoBrote);
          st.setInt(2, alerBrote);
          st.setString(3, usuEntrada);
          st.setString(4, comAutonoma);
          st.setString(5, usuEntrada);
          st.setString(6, anoBrote);
          st.setString(7, comAutonoma);

          rs = st.executeQuery();
          while (rs.next()) {
            dsBrote = (rs.getString("DS_BROTE"));
            listaSalida.addElement(dsBrote);
          } //end while
          break;
      } //end switch

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      ex.printStackTrace();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);
    return listaSalida;
  } // Fin doWork()
}
