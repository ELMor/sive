package brotes.servidor.confprot2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import brotes.datos.confprot2.DataMantLineaBrote;
import brotes.datos.confprot2.DataMantLineasModBrote;
import capp.CLista;
import sapp.DBServlet;

public class SrvModBrote
    extends DBServlet {

  // modos de operaci�n
  final int servletMODIFICAR = 1;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;
    int j = 1;

    // objetos de datos
    CLista data = new CLista();
    DataMantLineasModBrote datModelo;

    // buffers
    String sCod = "";
    String sDes = "";
    String sDesAux = "";

    String sCodCond = "";
    DataMantLineaBrote datLinea;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    datModelo = (DataMantLineasModBrote) param.firstElement();

    try {

      // modos de operaci�n
      switch (opmode) {

        case servletMODIFICAR:

          //Borramos todas las lineas item con preguntas
          //que hab�a pertenecientes a este modelo
          query = "delete from SIVE_LINEA_ITEM " +
              " where CD_TSIVE = ? and CD_MODELO = ? ";
          st = con.prepareStatement(query);

          st.setString(1, datModelo.sTSive.trim());
          st.setString(2, datModelo.sCodModelo.trim());

          // lanza la query
          st.executeUpdate();
          st.close();
          st = null;

          //Borramos todas las lineasm (descripciones+pregs)
          //que hab�a pertenecientes a este modelo
          query = "delete from SIVE_LINEASM " +
              " where CD_TSIVE = ? and CD_MODELO = ? ";

          st = con.prepareStatement(query);

          st.setString(1, datModelo.sTSive.trim());
          st.setString(2, datModelo.sCodModelo.trim());

          // lanza la query
          st.executeUpdate();
          st.close();
          st = null;

          // inserta en SIVE_LINEASM
          query = "insert into SIVE_LINEASM " +
              " (CD_TSIVE, CD_MODELO, NM_LIN, CD_TLINEA, " +
              " DS_TEXTO, DSL_TEXTO) values (?, ?, ?, ?, ?, ?)";

          for (j = 0; j < datModelo.getLineas().size(); j++) {
            st = con.prepareStatement(query);

            datLinea = (DataMantLineaBrote) datModelo.getLineas().elementAt(j);

            st.setString(1, datModelo.sTSive.trim());
            st.setString(2, datModelo.sCodModelo.trim());
            st.setInt(3, datLinea.iLinea);
            st.setString(4, datLinea.sTipo.trim());
            st.setString(5, datLinea.sDescGeneral.trim());
            if (datLinea.sDescLocal != null) {
              st.setString(6, datLinea.sDescLocal.trim());
            }
            else {
              st.setNull(6, java.sql.Types.VARCHAR);

              // lanza la query
            }
            st.executeUpdate();
            st.close();
            st = null;
          }

          // inserta en SIVE_LINEA_ITEM
          query = "insert into SIVE_LINEA_ITEM " +
              " (CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, " +
              " IT_OBLIGATORIO, IT_CONDP, NM_LIN_PC, CD_PREGUNTA_PC, " +
              " DS_VPREGUNTA_PC) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

          for (j = 0; j < datModelo.getLineas().size(); j++) {
            st = con.prepareStatement(query);

            datLinea = (DataMantLineaBrote) datModelo.getLineas().elementAt(j);

            if (datLinea.sTipo.equals(DataMantLineaBrote.tipoPREGUNTA)) {

              st.setString(1, datModelo.sTSive.trim());
              st.setString(2, datModelo.sCodModelo.trim());
              st.setInt(3, datLinea.iLinea);
              if (datLinea.sCodPregunta != null) {
                st.setString(4, datLinea.sCodPregunta);
              }
              else {
                st.setNull(4, java.sql.Types.VARCHAR);
              }
              if (datLinea.bObligatoria) {
                st.setString(5, "S");
              }
              else {
                st.setString(5, "N");

                //# System.Out.println(datLinea.sCodPregunta);
              }
              if (datLinea.bCondicionada) {
                st.setString(6, "S");
                st.setInt(7, datLinea.iCondALinea);
                for (i = 0; i < j; i++) {
                  if ( ( (DataMantLineaBrote) datModelo.getLineas().elementAt(i)).
                      iLinea == datLinea.iCondALinea) {
                    sCodCond = ( (DataMantLineaBrote) datModelo.getLineas().
                                elementAt(i)).sCodPregunta;
                    break;
                  }
                }
                st.setString(8, sCodCond);
                st.setString(9, datLinea.sCondAValor);
              }
              else {
                st.setString(6, "N");
                st.setNull(7, java.sql.Types.INTEGER);
                st.setNull(8, java.sql.Types.VARCHAR);
                st.setNull(9, java.sql.Types.VARCHAR);
              }

              // lanza la query
              st.executeUpdate();
              st.close();
              st = null;
            } // if esta linea es pregunta
          }

          break;

      } //switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      throw ex;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    if (data != null) {
      data.trimToSize();
    }
    return data;
  } // proc doWork
} //clase
