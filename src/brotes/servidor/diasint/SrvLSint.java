/**
 * Clase: SrvLSint
 * Paquete: brotes.servidor.diasint
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 10/12/1999
 * Analisis Funcional: Punto 2. Mantenimiento Brotes.
 * Descripcion: Obtiene el subconjunto de sintomas que
 *   verifican una cierta condicion. 4 modos de
 *   funcionamiento: busqueda exacta (codigo), busqueda
 *   exacta (descripcion), busqueda aproximada (codigo),
 *   busqueda aproximada (descripcion).
 */

package brotes.servidor.diasint;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import comun.DataGeneralCDDS;
import comun.constantes;
import sapp.DBServlet;

public class SrvLSint
    extends DBServlet {

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Configuraci�n
    String sQuery = "";

    // Objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int iValor = 1;

    // Objetos de datos
    CLista listaSalida = new CLista();
    DataGeneralCDDS ent = null;
    DataGeneralCDDS dataSalida = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {
      switch (opmode) {
        case constantes.sOBTENER_X_CODIGO:
        case constantes.sOBTENER_X_DESCRIPCION:

          // Parametro de busqueda
          ent = (DataGeneralCDDS) param.firstElement();

          // Query
          if (opmode == constantes.sOBTENER_X_CODIGO) {
            sQuery = "select CD_SINTOMA, DS_SINTOMA, IT_MASINF " +
                " from SIVE_SINTOMAS " +
                " where CD_SINTOMA = ?";
          }
          else {
            sQuery = "select CD_SINTOMA, DS_SINTOMA, IT_MASINF" +
                " from SIVE_SINTOMAS " +
                " where DS_SINTOMA = ?";

            // Preparacion del statement
          }
          st = con.prepareStatement(sQuery);

          // Instanciacion del CD_SINTOMA (Cod o Desc)
          st.setString(iValor++, ent.getCD().trim());

          break;

          //TRAMAR ****
        case constantes.sSELECCION_X_CODIGO:
        case constantes.sSELECCION_X_DESCRIPCION:

          ent = (DataGeneralCDDS) param.firstElement();

          // Query
          if (opmode == constantes.sSELECCION_X_CODIGO) {
            sQuery = "select CD_SINTOMA, DS_SINTOMA, IT_MASINF " +
                " from SIVE_SINTOMAS " +
                " where CD_SINTOMA like ?";
            // Tramado
            if (param.getFilter().length() > 0) {
              sQuery = sQuery + " and CD_SINTOMA > ?";
            }
          }
          else {
            sQuery = "select CD_SINTOMA, DS_SINTOMA, IT_MASINF " +
                " from SIVE_SINTOMAS " +
                " where DS_SINTOMA like ?";
            // Tramado
            if (param.getFilter().length() > 0) {
              sQuery = sQuery + " and DS_SINTOMA > ?";
            }
          }

          // Ordenacion
          if (opmode == constantes.sSELECCION_X_CODIGO) {
            sQuery = sQuery + " order by CD_SINTOMA";
          }
          else {
            sQuery = sQuery + " order by DS_SINTOMA";

            // Preparacion del statement
          }
          st = con.prepareStatement(sQuery);

          // Instanciacion del CD_SINTOMA o DS_SINTOMA
          st.setString(iValor++, ent.getCD().trim() + "%");

          // trama
          if (param.getFilter().length() > 0) {
            st.setString(iValor++, param.getFilter().trim());

          }
          break;
      } // Fin switch

      // Ejecucion de la query
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido
      while (rs.next()) {

        // Tramado
        if ( (opmode == constantes.sSELECCION_X_CODIGO) ||
            (opmode == constantes.sSELECCION_X_DESCRIPCION)) {

          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            listaSalida.setState(CLista.listaINCOMPLETA);

            if (opmode == constantes.sSELECCION_X_CODIGO) {
              listaSalida.setFilter( ( (DataGeneralCDDS) listaSalida.
                                      lastElement()).getCD());
            }
            else {
              listaSalida.setFilter( ( (DataGeneralCDDS) listaSalida.
                                      lastElement()).getDS());

            }
            break;
          }

          // Control de estado
          if (listaSalida.getState() == CLista.listaVACIA) {
            listaSalida.setState(CLista.listaLLENA);
          }
        } // Fin Tramado

        dataSalida = new DataGeneralCDDS(rs.getString("IT_MASINF"),
                                         rs.getString("CD_SINTOMA"),
                                         rs.getString("DS_SINTOMA"));

        // A�ade un nodo
        listaSalida.addElement(dataSalida);

        // Siguiente registro
        i++;
      } // Fin while

      rs.close();
      rs = null;
      st.close();
      st = null;

      // Valida la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // cierra la conexi�n y acaba el procedimiento doWork
    closeConnection(con);
    listaSalida.trimToSize();

    return listaSalida;
  }
} // Fin clase SrvLSint
