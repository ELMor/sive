/**
 * Clase: SrvDiaSint
 * Paquete: brotes.servidor.diasint
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 09/12/1999
 * Analisis Funcional: Punto 2. Mantenimiento Brotes
 * Descripcion: Implementacion del servlet que permite dar de alta,
 *   baja, modificar un sintoma.
 */

package brotes.servidor.diasint;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import brotes.datos.pansint.DatSintCS;
import brotes.datos.pansint.DatSintSC;
import capp.CLista;
// SOLO DESARROLLO
//import jdbcpool.*;
import comun.Fechas;
import comun.constantes;
import sapp.DBServlet;

public class SrvDiaSint
    extends DBServlet {

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista();
    DatSintCS brote = null;
    DatSintSC sint = null;
    DatSintSC result = null;
    java.util.Date dFechaUltact = null;
    SimpleDateFormat formUltact = null;
    String sOpe;
    String sFechaUltact = null;

    // Indicador de bloqueo
    boolean bBloqueo = true;

    // Querys
    String sQuery = "";

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con = openConnection();
    con.setAutoCommit(false);

    // Datos del sintoma actual
    brote = (DatSintCS) param.elementAt(0);
    sint = (DatSintSC) param.elementAt(1);

    try {

      switch (opmode) {
        case constantes.modoALTA_SB:
          bBloqueo = false;
        case constantes.modoALTA:

          // En caso de que haya que comprobar el bloqueo, si se ha
          //  modificado el brote no se da de alta el sintoma
          if (bBloqueo) {
            if (broteModificado(con, brote)) {
              throw (new Exception(constantes.PRE_BLOQUEO +
                                   constantes.SIVE_BROTES));
            }
          }

          // Establecimiento de la cadena de la query
          sQuery = "INSERT INTO SIVE_SINTOMAS_BROTE (CD_ANO, NM_ALERBRO, " +
              "CD_SINTOMA, DS_MASINF, NM_CASOS) VALUES (?,?,?,?,?)";

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciacion de la query
          instanciarInsertQuery(sint, st);

          // Ejecucion del insert
          st.executeUpdate();

          // Cierre del PreparedStatement (el ResultSet no se usa)
          st.close();
          st = null;

          // Se modifica el CD_OPE y FC_ULTACT del brote
          dFechaUltact = new java.util.Date();
          formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                            new Locale("es", "ES"));
          sFechaUltact = formUltact.format(dFechaUltact);
          sOpe = sint.getCD_OPE();

          if (bBloqueo) { //si es true es con bloqueo
            modificarBrote(con, brote, sOpe, sFechaUltact);
          }
          else { //si es false es sin bloqueo
            modificarBrote_SB(con, brote, sOpe, sFechaUltact);

            // Validacion de la transacci�n
          }
          con.commit();

          // Devolvemos el sintoma modificado
          result = new DatSintSC(sint.getCD_ANO(), sint.getNM_ALERBRO(),
                                 sint.getCD_SINTOMA(), sint.getDS_SINTOMA(),
                                 sint.getIT_MASINF(), sint.getDS_MASINF(),
                                 sint.getNM_CASOS(),
                                 sOpe, sFechaUltact);

          listaSalida.addElement(result);
          break;

        case constantes.modoMODIFICACION_SB:
          bBloqueo = false;
        case constantes.modoMODIFICACION:

          // En caso de que haya que comprobar el bloqueo, si se ha
          //  modificado el brote no se modifica el sintoma
          if (bBloqueo) {
            if (broteModificado(con, brote)) {
              throw (new Exception(constantes.PRE_BLOQUEO +
                                   constantes.SIVE_BROTES));
            }
          }

          // Establecimiento de la cadena de la query
          sQuery = "UPDATE SIVE_SINTOMAS_BROTE " +
              "SET DS_MASINF = ?, NM_CASOS = ? " +
              "WHERE CD_ANO = ? AND NM_ALERBRO = ? AND CD_SINTOMA = ?";

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciacion de la query
          instanciarUpdateQuery(sint, st);

          // Ejecucion del update
          st.executeUpdate();

          // Cierre del PreparedStatement (el ResultSet no se usa)
          st.close();
          st = null;

          // Se modifica el CD_OPE y FC_ULTACT de la notificacion
          dFechaUltact = new java.util.Date();
          formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                            new Locale("es", "ES"));
          sFechaUltact = formUltact.format(dFechaUltact);
          sOpe = sint.getCD_OPE();
          if (bBloqueo) { //si es true es con bloqueo
            modificarBrote(con, brote, sOpe, sFechaUltact);
          }
          else { //si es false es sin bloqueo
            modificarBrote_SB(con, brote, sOpe, sFechaUltact);

            // Validacion de la transacci�n
          }
          con.commit();

          // Devolvemos el sintoma modificado
          result = new DatSintSC(sint.getCD_ANO(), sint.getNM_ALERBRO(),
                                 sint.getCD_SINTOMA(), sint.getDS_SINTOMA(),
                                 sint.getIT_MASINF(), sint.getDS_MASINF(),
                                 sint.getNM_CASOS(),
                                 sOpe, sFechaUltact);
          listaSalida.addElement(result);

          break;

        case constantes.modoBAJA_SB:
          bBloqueo = false;
        case constantes.modoBAJA:

          // En caso de que haya que comprobar el bloqueo, si se ha
          //  modificado el brote no se da de baja el sintoma
          if (bBloqueo) {
            if (broteModificado(con, brote)) {
              throw (new Exception(constantes.PRE_BLOQUEO +
                                   constantes.SIVE_BROTES));
            }
          }

          // Establecimiento de la cadena de la query
          sQuery = "DELETE FROM SIVE_SINTOMAS_BROTE " +
              "WHERE CD_ANO = ? AND NM_ALERBRO = ? AND CD_SINTOMA = ?";

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciacion de la query
          instanciarDeleteQuery(sint, st);

          // Ejecucion del delete
          st.executeUpdate();

          // Cierre del PreparedStatement (el ResultSet no se usa)
          st.close();
          st = null;

          // Se modifica el CD_OPE y FC_ULTACT de la notificacion
          dFechaUltact = new java.util.Date();
          formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                            new Locale("es", "ES"));
          sFechaUltact = formUltact.format(dFechaUltact);
          sOpe = sint.getCD_OPE();
          if (bBloqueo) { //si es true es con bloqueo
            modificarBrote(con, brote, sOpe, sFechaUltact);
          }
          else { //si es false es sin bloqueo
            modificarBrote_SB(con, brote, sOpe, sFechaUltact);

            // Validacion de la transacci�n
          }
          con.commit();

          // Devolvemos el sintoma modificado
          result = new DatSintSC(sint.getCD_ANO(), sint.getNM_ALERBRO(),
                                 sint.getCD_SINTOMA(), sint.getDS_SINTOMA(),
                                 sint.getIT_MASINF(), sint.getDS_MASINF(),
                                 sint.getNM_CASOS(),
                                 sOpe, sFechaUltact);
          listaSalida.addElement(result);
          break;
        case constantes.modoCONSULTA:
          break;
      }

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);

    return listaSalida;
  } // Fin doWork()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void instanciarInsertQuery(DatSintSC sint,
                                       PreparedStatement st) throws Exception {

    // Lleva la cuenta de ?
    int iParN = 1;

    // A�o
    st.setString(iParN++, sint.getCD_ANO());

    // Numero de Brote
    String nmBrote = sint.getNM_ALERBRO().trim();
    st.setInt(iParN++, (new Integer(nmBrote)).intValue());

    // Sintoma
    st.setString(iParN++, sint.getCD_SINTOMA());

    // Hasta aqui son los campos obligatorios (NOT NULL)

    // Mas Informacion
    String sMasInf = sint.getDS_MASINF();
    if (sint.getIT_MASINF().equals("S")) {
      if (!sMasInf.equals("")) {
        st.setString(iParN++, sMasInf);
      }
      else {
        st.setNull(iParN++, java.sql.Types.VARCHAR);
      }
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      // Numero Casos
    }
    String nmCasos = sint.getNM_CASOS().trim();
    if (!nmCasos.equals("")) {
      st.setInt(iParN++, (new Integer(nmCasos)).intValue());
    }
    else {
      st.setNull(iParN++, java.sql.Types.INTEGER);

    }
  } // Fin instanciarInsertQuery()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void instanciarUpdateQuery(DatSintSC sint,
                                       PreparedStatement st) throws Exception {

    // Lleva la cuenta de ?
    int iParN = 1;

    /************** Par�metros del SET ***************/

    // Mas Informacion
    String sMasInf = sint.getDS_MASINF();
    if (sint.getIT_MASINF().equals("S")) {
      if (!sMasInf.equals("")) {
        st.setString(iParN++, sMasInf);
      }
      else {
        st.setNull(iParN++, java.sql.Types.VARCHAR);
      }
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      // Numero Casos
    }
    String sNumCasos = sint.getNM_CASOS().trim();
    if (!sNumCasos.equals("")) {
      st.setInt(iParN++, (new Integer(sNumCasos)).intValue());
    }
    else {
      st.setNull(iParN++, java.sql.Types.INTEGER);

      /************** Par�metros del WHERE ****************/

      // A�o
    }
    st.setString(iParN++, sint.getCD_ANO());

    // Numero Brote
    String iNumBrote = sint.getNM_ALERBRO().trim();
    st.setInt(iParN++, (new Integer(iNumBrote)).intValue());

    // Codigo Sintoma
    st.setString(iParN++, sint.getCD_SINTOMA());
  } // Fin instanciarUpdateQuery()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void instanciarDeleteQuery(DatSintSC sint,
                                       PreparedStatement st) throws Exception {

    // Lleva la cuenta de ?
    int iParN = 1;

    // A�o
    st.setString(iParN++, sint.getCD_ANO());

    // Numero Brote
    String iNumBrote = sint.getNM_ALERBRO().trim();
    st.setInt(iParN++, (new Integer(iNumBrote)).intValue());

    // Codigo Sintoma
    st.setString(iParN++, sint.getCD_SINTOMA());
  } // Fin instanciarDeleteQuery()

  // Comprueba el CD_OPE y FC_ULTACT del brote
  //  de la BD con el brote del cliente
  boolean broteModificado(Connection c, DatSintCS brote) throws Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery;
    String ope, fultact;

    try {
      sQuery = "SELECT CD_OPE,FC_ULTACT FROM SIVE_BROTES " +
          "WHERE CD_ANO = ? AND NM_ALERBRO = ?";

      // Preparacion de la sentencia SQL
      st = c.prepareStatement(sQuery);

      // INSTANCIACION DE LA QUERY

      // Lleva la cuenta de ?
      int iParN = 1;

      // A�o
      st.setString(iParN++, brote.getCD_ANO());

      // Numero Brote
      String nmBrote = brote.getNM_ALERBRO().trim();
      st.setInt(iParN++, (new Integer(nmBrote)).intValue());

      // FIN INSTANCIACION DE LA QUERY

      // Ejecucion de la query
      rs = st.executeQuery();

      // Obtenemos CD_OPE y FC_ULTACT
      if (rs.next()) {
        ope = rs.getString("CD_OPE");
        fultact = Fechas.timestamp2String(rs.getTimestamp("FC_ULTACT"));
      }
      else {
        return true;
      }

      // Cierre del PreparedStatement y del ResultSet
      rs.close();
      rs = null;
      st.close();
      st = null;

    }
    catch (Exception ex) {
      return true;
    }

    if (!ope.equals(brote.getCD_OPE()) ||
        !fultact.equals(brote.getFC_ULTACT())) {
      return true;
    }
    else {
      return false;
    }

  } // broteModificado()

  // Modifica el CD_OPE y FC_ULTACT del brote (con comprobacion de bloqueo)
  void modificarBrote(Connection c, DatSintCS brote, String sOpe,
                      String sFechaUltact) throws Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    String sQuery;

    try {
      sQuery = "UPDATE SIVE_BROTES SET CD_OPE = ?,FC_ULTACT = ? " +
          "WHERE CD_ANO = ? AND NM_ALERBRO = ? AND CD_OPE = ? AND FC_ULTACT = ?";

      // Preparacion de la sentencia SQL
      st = c.prepareStatement(sQuery);

      // INSTANCIACION DE LA QUERY

      // Lleva la cuenta de ?
      int iParN = 1;

      // Parte del SET
      // CD_OPE NUEVO
      st.setString(iParN++, sOpe);
      // FC_ULTACT NUEVO
      java.sql.Timestamp fUltact = Fechas.string2Timestamp(sFechaUltact);
      st.setTimestamp(iParN++, fUltact);

      // Parte del WHERE (ANO, ALERBRO, CD_OPE, FC_ULTACT)
      // A�o
      st.setString(iParN++, brote.getCD_ANO());
      // Numero de Brote
      String nmBrote = brote.getNM_ALERBRO().trim();
      st.setInt(iParN++, (new Integer(nmBrote)).intValue());
      // CD_OPE ANTIGUO
      st.setString(iParN++, brote.getCD_OPE());
      // FC_ULTACT ANTIGUO
      st.setTimestamp(iParN++, Fechas.string2Timestamp(brote.getFC_ULTACT()));

      // FIN INSTANCIACION DE LA QUERY

      // Ejecucion del update
      int iActualizados = st.executeUpdate();

      //PREGUNTAR si ha modificado algo (ver de nuevo el bloqueo)
      if (iActualizados == 0) {
        throw (new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_BROTES));
      }

      // Cierre del PreparedStatement
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }
  } // Fin modificarBrote()

  // Modifica el CD_OPE y FC_ULTACT del brote (sin consideracion de bloqueo)
  void modificarBrote_SB(Connection c, DatSintCS brote, String sOpe,
                         String sFechaUltact) throws Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    String sQuery;

    try {
      sQuery = "UPDATE SIVE_BROTES SET CD_OPE = ?,FC_ULTACT = ? " +
          "WHERE CD_ANO = ? AND NM_ALERBRO = ?";

      // Preparacion de la sentencia SQL
      st = c.prepareStatement(sQuery);

      // INSTANCIACION DE LA QUERY

      // Lleva la cuenta de ?
      int iParN = 1;

      // CD_OPE NUEVO
      st.setString(iParN++, sOpe);
      // FC_ULTACT NUEVO
      java.sql.Timestamp fUltact = Fechas.string2Timestamp(sFechaUltact);
      st.setTimestamp(iParN++, fUltact);

      // A�o
      st.setString(iParN++, brote.getCD_ANO());
      // Numero de Brote
      String nmBrote = brote.getNM_ALERBRO().trim();
      st.setInt(iParN++, (new Integer(nmBrote)).intValue());

      // FIN INSTANCIACION DE LA QUERY

      // Ejecucion del update
      st.executeUpdate();

      // Cierre del PreparedStatement
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }
  } // Fin modificarBrote_SB()

} // Fin SrvDiaSint
