//Title:        Notificacion datos adicionales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Notificacion de enfermedades EDO de declaracion numerica
//que necesitan datos adicionales.

package brotes.servidor.bisrv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Vector;

import brotes.datos.bidata.DataAlerta;
import brotes.datos.bidata.DataBrote;
import capp.CLista;
import sapp.DBServlet;

public class SrvBIModif
    extends DBServlet {

  final int servletCOMP_MOD = 3; // modificacion con comprobacion
  final int servletCOMP_BORR = 4; // borrado con comprobacion
  final int servletMODIFICACION = 5; // modificacion
  final int servletBORRADO = 6; //borrado

  /*
    final   String sQuery = "SELECT CD_NIVEL_1, CD_NIVEL_2, "
            + " CD_CA , IT_OK FROM SIVE_MODELO WHERE "
            + " CD_TSIVE = ? AND CD_MODELO = ?";
    final String sALTA_CAB ="insert into SIVE_EDO_DADIC "
    + "(CD_ENFCIE, SEQ_CODIGO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, "
    + " FC_FECNOTIF, FC_RECEP, CD_OPE, FC_ULTACT) "
    + " values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    final String sALTA_RESP ="insert into SIVE_RESP_ADIC "
    + "(CD_ENFCIE, CD_E_NOTIF, CD_ANOEPI, CD_TSIVE, CD_SEMEPI, "
    + " FC_FECNOTIF, FC_RECEP, CD_MODELO, SEQ_CODIGO, "
    + " NM_LIN, CD_PREGUNTA, DS_RESPUESTA) "
    + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    final String sBORRAR_RESP = "DELETE FROM SIVE_RESP_ADIC "
            + " WHERE ( CD_ENFCIE = ? and CD_E_NOTIF = ? and "
            + " CD_ANOEPI = ? and CD_TSIVE = ? and CD_SEMEPI = ? "
            + " and FC_FECNOTIF = ? and FC_RECEP = ? and "
            + " SEQ_CODIGO = ? )";
    final String sBORRAR_RESP_TODOS = "DELETE FROM SIVE_RESP_ADIC "
            + " WHERE ( CD_ENFCIE = ? and CD_E_NOTIF = ? and "
            + " CD_ANOEPI = ? and CD_TSIVE = ? and CD_SEMEPI = ? "
            + " and FC_FECNOTIF = ? and FC_RECEP = ? )";
    final String sBORRAR_CAB = "DELETE FROM SIVE_EDO_DADIC "
            + " WHERE ( CD_ENFCIE = ? and SEQ_CODIGO = ? and "
            + " CD_E_NOTIF = ? and  CD_ANOEPI = ? and CD_SEMEPI = ? "
            + " and FC_FECNOTIF = ? and FC_RECEP = ? )";
   final String sSELECT_VALORES =" select "
          + " a.DS_LISTAP, a.DSL_LISTAP "
          + "from SIVE_LISTA_VALORES a, SIVE_PREGUNTA b where "
          + "(b.CD_PREGUNTA = ? and a.CD_LISTAp = ? "
          + " and a.CD_LISTA = b.CD_LISTA)" ;
   */

  //fechas
  java.util.Date dFecha;
  java.sql.Date sqlFec;
  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
  SimpleDateFormat formUltAct = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
      new Locale("es", "ES"));

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    //fechas
    dFecha = new java.util.Date();
    sqlFec = new java.sql.Date(dFecha.getTime());
    formater = new SimpleDateFormat("dd/MM/yyyy");
    formUltAct = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                      new Locale("es", "ES"));

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    int iEstado;

    int j = 0;

    // objetos de datos
    CLista data = new CLista();
    Vector listaPartesAct = new Vector();
    DataAlerta alerta = null;
    DataBrote brote = null;

    // objetos de datos
    CLista listaSalida = new CLista();

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    try {
      // modos de operaci�n
      switch (opmode) {

        //MODIFICAR******************************************
        case servletCOMP_MOD:

          //va bien: listaSalida = (true, DataAlerta, DataBrote) o
          //         listaSalida = (true, mensaje pregunta) o
          //va mal: listaSalida = null y exc

          //param(0): DataAlerta con la alerta que se va a modificar
          //param(1): DataBrote con el brote que se desea modificar

          alerta = (DataAlerta) param.elementAt(0);
          brote = (DataBrote) param.elementAt(1);

          // Comprobar si la alerta que se esta modificando
          // ha sido borrado o modificado
          int aux = -1;

          aux = comprobarModBor(brote, con, st, rs, param.getLogin());
          if (aux == 0) {

            //se lanza el select con la clave para ver si la alerta
            //ha sido modificado o borrado
            String resMod = modificado_o_borrado(brote, con, st, rs);
            if (resMod.equals("M")) {

              //resultado modificado
              listaSalida.addElement(new Boolean(true));
              listaSalida.addElement("Mientras se alteraba el contenido del brote ha sido modificado por otro usuario. �Desea sobreescribir sus cambios?");
            }
            else if (resMod.equals("B")) {

              //resultado borrado
              String mens = "Mientras se alteraba el contenido del brote ha sido borrado por otro usuario. �Desea guardar los cambios?";
              listaSalida.addElement(new Boolean(true));
              listaSalida.addElement(mens);
            }
          } // la alerta habia sido modificado o borrado
          else {

            //MODIFICACION

            Vector alerBro = modificacionAlertaBrote(alerta, brote, con, st, rs,
                param.getLogin());

            listaSalida.addElement(new Boolean(true));
            listaSalida.addElement( (DataAlerta) alerBro.elementAt(0));
            listaSalida.addElement( (DataBrote) alerBro.elementAt(1));
          } //la alerta no habia sido modificado ni borrado

          break;

        case servletCOMP_BORR:

          //param(0): DataAlerta correspondiente
          //param(1): DataBrote que se va a borrar

          //va bien: listaSalida = (true) o
          //         listaSalida = (true, mensaje pregunta)
          //va mal: listaSalida = null y exc

          alerta = (DataAlerta) param.elementAt(0);
          brote = (DataBrote) param.elementAt(1);

          // Comprobar si la alerta que se va a borrar ha sido borrado
          // o modificado

          aux = -1;
          aux = comprobarModBor(brote, con, st, rs, param.getLogin());
          if (aux == 0) {
            //no se ha actualizado ninguna tabla de alerta
            //se lanza el select con la clave para ver si la alerta
            //ha sido modificado o borrado

            String resMod = modificado_o_borrado(brote, con, st, rs);
            if (resMod.equals("M")) {
              //resultado modificado
              listaSalida.addElement(new Boolean(true));
              listaSalida.addElement("El brote ha sido modificado por otro usuario. �Desea borrarlo descartando as� la �ltima modificaci�n?");
            }
            else if (resMod.equals("B")) {
              //resultado: ya habia sido borrado
              listaSalida.addElement(new Boolean(true));
            }
          } // la alerta habia sido modificado o borrado
          else {
            //BORRADO

            //la alerta_brotes se ha modificado en la comprobacion
            borradoAlertaBrote(alerta, brote, con, st, rs,
                               param.getLogin().trim());
            listaSalida.addElement(new Boolean(true));

          }
          break;

        case servletBORRADO:

          //param(0): alerta que se va a borrar

          //va bien: listaSalida.size()=0
          //va mal: listaSalida = null y exc

          alerta = (DataAlerta) param.elementAt(1);
          brote = (DataBrote) param.elementAt(1);
          //BORRADO
          borradoAlertaBrote(alerta, brote, con, st, rs, param.getLogin());

          break;

        case servletMODIFICACION:

          //param(0): alerta que se va a modificar
          //param(1): brote que se va a modificar
          //va bien: listaSalida = (alerta, brote)
          //va mal: listaSalida = null y exc

          alerta = (DataAlerta) param.elementAt(0);
          brote = (DataBrote) param.elementAt(1);
          //MODIFICACION
          //se actualiza sive_alerta_brotes
          Vector alBro = borradoInsercionAlertaBrote(alerta, brote, con, st, rs,
              param.getLogin());

          listaSalida.addElement(alBro.elementAt(0));
          listaSalida.addElement(alBro.elementAt(1));

          break;

      } // fin del switch para los modos de consulta

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;

      throw ex;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;

  }

  private String fecha_mas_hora(String f, String h) {
    f = f.trim();
    h = h.trim();

    //return f + " " + h.substring(0, h.lastIndexOf(':'));
    return f + " " + h + ":00";
  }

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    //java.sql.Timestamp TSFec = new java.sql.Timestamp(dFecha.getTime());
    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);

    return TSFec;
  }

  private Vector modificacionAlertaBrote(DataAlerta alerta,
                                         DataBrote brote,
                                         Connection con,
                                         PreparedStatement st,
                                         ResultSet rs,
                                         String login) throws Exception {

    //1. Modifico la situacion y la fecha de la alerta,
    //si se envia una situacion nueva

    if (alerta.getCD_SITALERBRO() != null) {

      String queryComp = " update SIVE_ALERTA_BROTES "
          + " set CD_OPE = ?, FC_ULTACT = ? "
          + " , CD_SITALERBRO = ? , FC_ALERBRO = ? "
          + " where CD_ANO = ? and NM_ALERBRO = ? ";

      st = con.prepareStatement(queryComp);

      //operador actual
      st.setString(1, login);

      //la fecha de ultima actualizacion no es un date
      //es un timestamp, fecha actual
      dFecha = new java.util.Date();
      String sFecha = formUltAct.format(dFecha); //string

      java.sql.Timestamp ts = cadena_a_timestamp(sFecha);
      st.setTimestamp(2, ts);
      /*System.out.println("update 2");*/

      st.setString(3, alerta.getCD_SITALERBRO().trim());
      /*System.out.println("update 3");*/

      dFecha = formater.parse(alerta.getFC_ALERBRO().trim());
      sqlFec = new java.sql.Date(dFecha.getTime());
      st.setDate(4, sqlFec);
      /*System.out.println("update 4");*/

      st.setString(5, brote.getCD_ANO().trim());
      st.setInt(6, (new Integer(brote.getNM_ALERBRO().trim())).intValue());

      st.executeUpdate();
      st.close();
      st = null;

      alerta.remove("CD_OPE");
      alerta.insert("CD_OPE", login);
      alerta.remove("FC_ULTACT");
      alerta.insert("FC_ULTACT", sFecha);
    }

    //2. Modifico el brote
    String queryComp = " update SIVE_BROTES "
        + " set FC_FSINPRIMC = ? , FC_ISINPRIMC = ? , "
        + " NM_PERINMIN = ? , NM_PERINMAX = ? , NM_PERINMED = ? , "
        + " NM_ENFERMOS = ? , FC_EXPOSICION = ? , NM_INGHOSP = ? , "
        + " NM_DEFUNCION = ? , NM_EXPUESTOS = ? , " //10
        + " CD_TRANSMIS = ? , NM_NVACENF = ? , "
        + " CD_OPE = ? , FC_ULTACT = ? , "
        + " NM_DCUACMIN = ? , NM_DCUACMED = ? , NM_DCUACMAX = ? , "
        + " NM_NVACNENF = ? , NM_VACNENF = ? , NM_VACENF = ? , " //20
        + " DS_OBSERV = ? , CD_TIPOCOL = ? , DS_NOMCOL = ? , DS_DIRCOL = ? , "
        + " CD_POSTALCOL = ? , CD_PROVCOL = ? , "
        + " DS_BROTE = ? , CD_GRUPO = ? , FC_FECHAHORA = ? , NM_MANIPUL = ? , " //30
        +
        " CD_ZBS_LE = ? , CD_NIVEL_2_LE = ? , CD_TNOTIF = ? , IT_RESCALC = ? , "
        +
        " CD_MUNCOL = ? , CD_NIVEL_1_LCA = ? , DS_TELCOL = ? , CD_NIVEL_1_LE = ? , "
        + " CD_NIVEL_2_LCA = ? , CD_ZBS_LCA = ? , " //40
        +
        " IT_PERIN = ? , IT_DCUAC = ? , DS_NMCALLE = ? , CD_TBROTE = ?, DS_PISOCOL = ? " //45
        + " where CD_ANO = ? and NM_ALERBRO = ? "; //47

    st = con.prepareStatement(queryComp);

    if (brote.getFC_FSINPRIMC_F().trim().length() > 0) {
      if (brote.getFC_FSINPRIMC_H().trim().length() > 0) {
        st.setTimestamp(1,
                        cadena_a_timestamp(fecha_mas_hora(brote.getFC_FSINPRIMC_F().
            trim(),
            brote.getFC_FSINPRIMC_H().trim())));
      }
      else {
        st.setTimestamp(1,
                        cadena_a_timestamp(fecha_mas_hora(brote.getFC_FSINPRIMC_F().
            trim(),
            "00:00:00")));
      }
    }
    else {
      st.setNull(1, java.sql.Types.DATE);

    }
    if (brote.getFC_ISINPRIMC_F().trim().length() > 0) {
      if (brote.getFC_ISINPRIMC_H().trim().length() > 0) {
        st.setTimestamp(2,
                        cadena_a_timestamp(fecha_mas_hora(brote.getFC_ISINPRIMC_F().
            trim(),
            brote.getFC_ISINPRIMC_H().trim())));
      }
      else {
        st.setTimestamp(2,
                        cadena_a_timestamp(fecha_mas_hora(brote.getFC_ISINPRIMC_F().
            trim(),
            "00:00:00")));
      }
    }
    else {
      st.setNull(2, java.sql.Types.DATE);

    }
    if (brote.getNM_PERINMIN().trim().length() > 0) {
      st.setInt(3, (new Integer(brote.getNM_PERINMIN().trim())).intValue());
    }
    else {
      st.setNull(3, java.sql.Types.INTEGER);

    }
    if (brote.getNM_PERINMAX().trim().length() > 0) {
      st.setInt(4, (new Integer(brote.getNM_PERINMAX().trim())).intValue());
    }
    else {
      st.setNull(4, java.sql.Types.INTEGER);

    }
    if (brote.getNM_PERINMED().trim().length() > 0) {
      st.setInt(5, (new Integer(brote.getNM_PERINMED().trim())).intValue());
    }
    else {
      st.setNull(5, java.sql.Types.INTEGER);

    }
    if (brote.getNM_ENFERMOS().trim().length() > 0) {
      st.setInt(6, (new Integer(brote.getNM_ENFERMOS().trim())).intValue());
    }
    else {
      st.setNull(6, java.sql.Types.INTEGER);

    }
    if (brote.getFC_EXPOSICION_F().trim().length() > 0) {
      if (brote.getFC_EXPOSICION_H().trim().length() > 0) {
        st.setTimestamp(7,
                        cadena_a_timestamp(fecha_mas_hora(brote.getFC_EXPOSICION_F().
            trim(),
            brote.getFC_EXPOSICION_H().trim())));
      }
      else {
        st.setTimestamp(7,
                        cadena_a_timestamp(fecha_mas_hora(brote.getFC_EXPOSICION_F().
            trim(),
            "00:00:00")));
      }
    }
    else {
      st.setNull(7, java.sql.Types.DATE);

    }
    if (brote.getNM_INGHOSP().trim().length() > 0) {
      st.setInt(8, (new Integer(brote.getNM_INGHOSP().trim())).intValue());
    }
    else {
      st.setNull(8, java.sql.Types.INTEGER);

    }
    if (brote.getNM_DEFUNCION().trim().length() > 0) {
      st.setInt(9, (new Integer(brote.getNM_DEFUNCION().trim())).intValue());
    }
    else {
      st.setNull(9, java.sql.Types.INTEGER);

    }
    if (brote.getNM_EXPUESTOS().trim().length() > 0) {
      st.setInt(10, (new Integer(brote.getNM_EXPUESTOS().trim())).intValue());
    }
    else {
      st.setNull(10, java.sql.Types.INTEGER);

      /*System.out.println("MODIF 10");*/
    }
    if (brote.getCD_TRANSMIS().trim().length() > 0) {
      st.setString(11, brote.getCD_TRANSMIS().trim());
    }
    else {
      st.setNull(11, java.sql.Types.VARCHAR);

    }
    if (brote.getNM_NVACENF().trim().length() > 0) {
      st.setInt(12, (new Integer(brote.getNM_NVACENF().trim())).intValue());
    }
    else {
      st.setNull(12, java.sql.Types.INTEGER);

    }
    st.setString(13, login);

    dFecha = new java.util.Date();
    String sFecha = formUltAct.format(dFecha); //string
    //partirla!!     *************
    String sfecRecuBrote = sFecha;
    st.setTimestamp(14, cadena_a_timestamp(sFecha));

    if (brote.getNM_DCUACMIN().trim().length() > 0) {
      st.setInt(15, (new Integer(brote.getNM_DCUACMIN().trim())).intValue());
    }
    else {
      st.setNull(15, java.sql.Types.INTEGER);

    }
    if (brote.getNM_DCUACMED().trim().length() > 0) {
      st.setInt(16, (new Integer(brote.getNM_DCUACMED().trim())).intValue());
    }
    else {
      st.setNull(16, java.sql.Types.INTEGER);

    }
    if (brote.getNM_DCUACMAX().trim().length() > 0) {
      st.setInt(17, (new Integer(brote.getNM_DCUACMAX().trim())).intValue());
    }
    else {
      st.setNull(17, java.sql.Types.INTEGER);

    }
    if (brote.getNM_NVACNENF().trim().length() > 0) {
      st.setInt(18, (new Integer(brote.getNM_NVACNENF().trim())).intValue());
    }
    else {
      st.setNull(18, java.sql.Types.INTEGER);

    }
    if (brote.getNM_VACNENF().trim().length() > 0) {
      st.setInt(19, (new Integer(brote.getNM_VACNENF().trim())).intValue());
    }
    else {
      st.setNull(19, java.sql.Types.INTEGER);

    }
    if (brote.getNM_VACENF().trim().length() > 0) {
      st.setInt(20, (new Integer(brote.getNM_VACENF().trim())).intValue());
    }
    else {
      st.setNull(20, java.sql.Types.INTEGER);

      /*System.out.println("MODIF 20");*/
    }
    if (brote.getDS_OBSERV().trim().length() > 0) {
      st.setString(21, brote.getDS_OBSERV().trim());
    }
    else {
      st.setNull(21, java.sql.Types.VARCHAR);

    }
    st.setString(22, brote.getCD_TIPOCOL().trim());

    if (brote.getDS_NOMCOL().trim().length() > 0) {
      st.setString(23, brote.getDS_NOMCOL().trim());
    }
    else {
      st.setNull(23, java.sql.Types.VARCHAR);

    }
    if (brote.getDS_DIRCOL().trim().length() > 0) {
      st.setString(24, brote.getDS_DIRCOL().trim());
    }
    else {
      st.setNull(24, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_POSTALCOL().trim().length() > 0) {
      st.setString(25, brote.getCD_POSTALCOL().trim());
    }
    else {
      st.setNull(25, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_PROVCOL().trim().length() > 0) {
      st.setString(26, brote.getCD_PROVCOL().trim());
    }
    else {
      st.setNull(26, java.sql.Types.VARCHAR);

    }
    st.setString(27, brote.getDS_BROTE().trim());

    st.setString(28, brote.getCD_GRUPO().trim());

    if (brote.getFC_FECHAHORA_H().trim().length() > 0) {
      st.setTimestamp(29,
                      cadena_a_timestamp(fecha_mas_hora(brote.getFC_FECHAHORA_F().
          trim(),
          brote.getFC_FECHAHORA_H().trim())));
    }
    else {
      st.setTimestamp(29,
                      cadena_a_timestamp(fecha_mas_hora(brote.getFC_FECHAHORA_F().
          trim(),
          "00:00:00")));

    }
    if (brote.getNM_MANIPUL().trim().length() > 0) {
      st.setInt(30, (new Integer(brote.getNM_MANIPUL().trim())).intValue());
    }
    else {
      st.setNull(30, java.sql.Types.INTEGER);

      /*System.out.println("MODIF 30");*/
    }
    if (brote.getCD_ZBS_LE().trim().length() > 0) {
      st.setString(31, brote.getCD_ZBS_LE().trim());
    }
    else {
      st.setNull(31, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_NIVEL_2_LE().trim().length() > 0) {
      st.setString(32, brote.getCD_NIVEL_2_LE().trim());
    }
    else {
      st.setNull(32, java.sql.Types.VARCHAR);

    }
    st.setString(33, brote.getCD_TNOTIF().trim());

    if (brote.getIT_RESCALC().trim().length() > 0) {
      st.setString(34, brote.getIT_RESCALC().trim());
    }
    else {
      st.setNull(34, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_MUNCOL().trim().length() > 0) {
      st.setString(35, brote.getCD_MUNCOL().trim());
    }
    else {
      st.setNull(35, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_NIVEL_1_LCA().trim().length() > 0) {
      st.setString(36, brote.getCD_NIVEL_1_LCA().trim());
    }
    else {
      st.setNull(36, java.sql.Types.VARCHAR);

    }
    if (brote.getDS_TELCOL().trim().length() > 0) {
      st.setString(37, brote.getDS_TELCOL().trim());
    }
    else {
      st.setNull(37, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_NIVEL_1_LE().trim().length() > 0) {
      st.setString(38, brote.getCD_NIVEL_1_LE().trim());
    }
    else {
      st.setNull(38, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_NIVEL_2_LCA().trim().length() > 0) {
      st.setString(39, brote.getCD_NIVEL_2_LCA().trim());
    }
    else {
      st.setNull(39, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_ZBS_LCA().trim().length() > 0) {
      st.setString(40, brote.getCD_ZBS_LCA().trim());
    }
    else {
      st.setNull(40, java.sql.Types.VARCHAR);

      /*System.out.println("MODIF 40");*/
    }
    if (brote.getIT_PERIN().trim().length() > 0) {
      st.setString(41, brote.getIT_PERIN().trim());
    }
    else {
      st.setNull(41, java.sql.Types.VARCHAR);

    }
    if (brote.getIT_DCUAC().trim().length() > 0) {
      st.setString(42, brote.getIT_DCUAC().trim());
    }
    else {
      st.setNull(42, java.sql.Types.VARCHAR);

    }
    if (brote.getDS_NMCALLE().trim().length() > 0) {
      st.setString(43, brote.getDS_NMCALLE().trim());
    }
    else {
      st.setNull(43, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_TBROTE().trim().length() > 0) {
      st.setString(44, brote.getCD_TBROTE().trim());
    }
    else {
      st.setNull(44, java.sql.Types.VARCHAR);

    }
    if (brote.getDS_PISOCOL().trim().length() > 0) {
      st.setString(45, brote.getDS_PISOCOL().trim());
    }
    else {
      st.setNull(45, java.sql.Types.VARCHAR);

    }
    st.setString(46, brote.getCD_ANO().trim());
    st.setInt(47, (new Integer(brote.getNM_ALERBRO().trim())).intValue());

    /*System.out.println("MODIF 47");*/
    st.executeUpdate();
    st.close();
    st = null;

    brote.remove("CD_OPE");
    brote.insert("CD_OPE", login);
    brote.remove("FC_ULTACT");
    brote.insert("FC_ULTACT", sFecha);

    Vector aux = new Vector();
    aux.addElement(alerta);
    aux.addElement(brote);
    return aux;
  }

  private Vector borradoInsercionAlertaBrote(DataAlerta alerta,
                                             DataBrote brote,
                                             Connection con,
                                             PreparedStatement st,
                                             ResultSet rs,
                                             String login) throws Exception {

    //1. Modifico la situacion y la fecha de la alerta,
    //si se envia una situacion nueva y borro el brote
    //todo esto se hace en borradoAlertaBrote
    borradoAlertaBrote(alerta, brote, con, st, rs, login);

    //2. Inserto de nuevo el brote

    //Cuidado en el caso en el que se borran las entidades
    //que dependen del brote!!!!
    //Como se vuelven a inserta?
    String queryComp = "insert into SIVE_BROTES " //
        + " ( FC_FSINPRIMC, FC_ISINPRIMC, "
        + " NM_PERINMIN, NM_PERINMAX, NM_PERINMED, "
        + " NM_ENFERMOS, FC_EXPOSICION, NM_INGHOSP, "
        + " NM_DEFUNCION, NM_EXPUESTOS, "
        + " CD_TRANSMIS, NM_NVACENF, "
        + " CD_OPE, FC_ULTACT, "
        + " NM_DCUACMIN, NM_DCUACMED, NM_DCUACMAX, "
        + " NM_NVACNENF, NM_VACNENF, NM_VACENF, "
        + " DS_OBSERV, CD_TIPOCOL, DS_NOMCOL, DS_DIRCOL, "
        + " CD_POSTALCOL, CD_PROVCOL,  "
        + " DS_BROTE, CD_GRUPO, FC_FECHAHORA, NM_MANIPUL, "
        + " CD_ZBS_LE, CD_NIVEL_2_LE, CD_TNOTIF, IT_RESCALC, "
        + " CD_MUNCOL, CD_NIVEL_1_LCA, DS_TELCOL, CD_NIVEL_1_LE, "
        + " CD_NIVEL_2_LCA, CD_ZBS_LCA, "
        +
        " IT_PERIN, IT_DCUAC, DS_NMCALLE, CD_TBROTE, DS_PISOCOL, NM_ALERBRO, CD_ANO) "
        + "values ( ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,  ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,  ?,?,?,?,?, ?,?,?,?,?, ?,?)";

    st = con.prepareStatement(queryComp);

    if (brote.getFC_FSINPRIMC_F().trim().length() > 0) {
      if (brote.getFC_FSINPRIMC_H().trim().length() > 0) {
        st.setTimestamp(1,
                        cadena_a_timestamp(fecha_mas_hora(brote.getFC_FSINPRIMC_F().
            trim(),
            brote.getFC_FSINPRIMC_H().trim())));
      }
      else {
        st.setTimestamp(1,
                        cadena_a_timestamp(fecha_mas_hora(brote.getFC_FSINPRIMC_F().
            trim(),
            "00:00:00")));
      }
    }
    else {
      st.setNull(1, java.sql.Types.DATE);

    }
    if (brote.getFC_ISINPRIMC_F().trim().length() > 0) {
      if (brote.getFC_ISINPRIMC_H().trim().length() > 0) {
        st.setTimestamp(2,
                        cadena_a_timestamp(fecha_mas_hora(brote.getFC_ISINPRIMC_F().
            trim(),
            brote.getFC_ISINPRIMC_H().trim())));
      }
      else {
        st.setTimestamp(2,
                        cadena_a_timestamp(fecha_mas_hora(brote.getFC_ISINPRIMC_F().
            trim(),
            "00:00:00")));
      }
    }
    else {
      st.setNull(2, java.sql.Types.DATE);

    }
    if (brote.getNM_PERINMIN().trim().length() > 0) {
      st.setInt(3, (new Integer(brote.getNM_PERINMIN().trim())).intValue());
    }
    else {
      st.setNull(3, java.sql.Types.INTEGER);

    }
    if (brote.getNM_PERINMAX().trim().length() > 0) {
      st.setInt(4, (new Integer(brote.getNM_PERINMAX().trim())).intValue());
    }
    else {
      st.setNull(4, java.sql.Types.INTEGER);

    }
    if (brote.getNM_PERINMED().trim().length() > 0) {
      st.setInt(5, (new Integer(brote.getNM_PERINMED().trim())).intValue());
    }
    else {
      st.setNull(5, java.sql.Types.INTEGER);

    }
    if (brote.getNM_ENFERMOS().trim().length() > 0) {
      st.setInt(6, (new Integer(brote.getNM_ENFERMOS().trim())).intValue());
    }
    else {
      st.setNull(6, java.sql.Types.INTEGER);

    }
    if (brote.getFC_EXPOSICION_F().trim().length() > 0) {
      if (brote.getFC_EXPOSICION_H().trim().length() > 0) {
        st.setTimestamp(7,
                        cadena_a_timestamp(fecha_mas_hora(brote.getFC_EXPOSICION_F().
            trim(),
            brote.getFC_EXPOSICION_H().trim())));
      }
      else {
        st.setTimestamp(7,
                        cadena_a_timestamp(fecha_mas_hora(brote.getFC_EXPOSICION_F().
            trim(),
            "00:00:00")));
      }
    }
    else {
      st.setNull(7, java.sql.Types.DATE);

    }
    if (brote.getNM_INGHOSP().trim().length() > 0) {
      st.setInt(8, (new Integer(brote.getNM_INGHOSP().trim())).intValue());
    }
    else {
      st.setNull(8, java.sql.Types.INTEGER);

    }
    if (brote.getNM_DEFUNCION().trim().length() > 0) {
      st.setInt(9, (new Integer(brote.getNM_DEFUNCION().trim())).intValue());
    }
    else {
      st.setNull(9, java.sql.Types.INTEGER);

    }
    if (brote.getNM_EXPUESTOS().trim().length() > 0) {
      st.setInt(10, (new Integer(brote.getNM_EXPUESTOS().trim())).intValue());
    }
    else {
      st.setNull(10, java.sql.Types.INTEGER);

      /*System.out.println("MODIF 10");*/
    }
    if (brote.getCD_TRANSMIS().trim().length() > 0) {
      st.setString(11, brote.getCD_TRANSMIS().trim());
    }
    else {
      st.setNull(11, java.sql.Types.VARCHAR);

    }
    if (brote.getNM_NVACENF().trim().length() > 0) {
      st.setInt(12, (new Integer(brote.getNM_NVACENF().trim())).intValue());
    }
    else {
      st.setNull(12, java.sql.Types.INTEGER);

    }
    st.setString(13, login);

    dFecha = new java.util.Date();
    String sFecha = formUltAct.format(dFecha); //string
    //partirla!!     *************
    String sfecRecuBrote = sFecha;
    st.setTimestamp(14, cadena_a_timestamp(sFecha));

    if (brote.getNM_DCUACMIN().trim().length() > 0) {
      st.setInt(15, (new Integer(brote.getNM_DCUACMIN().trim())).intValue());
    }
    else {
      st.setNull(15, java.sql.Types.INTEGER);

    }
    if (brote.getNM_DCUACMED().trim().length() > 0) {
      st.setInt(16, (new Integer(brote.getNM_DCUACMED().trim())).intValue());
    }
    else {
      st.setNull(16, java.sql.Types.INTEGER);

    }
    if (brote.getNM_DCUACMAX().trim().length() > 0) {
      st.setInt(17, (new Integer(brote.getNM_DCUACMAX().trim())).intValue());
    }
    else {
      st.setNull(17, java.sql.Types.INTEGER);

    }
    if (brote.getNM_NVACNENF().trim().length() > 0) {
      st.setInt(18, (new Integer(brote.getNM_NVACNENF().trim())).intValue());
    }
    else {
      st.setNull(18, java.sql.Types.INTEGER);

    }
    if (brote.getNM_VACNENF().trim().length() > 0) {
      st.setInt(19, (new Integer(brote.getNM_VACNENF().trim())).intValue());
    }
    else {
      st.setNull(19, java.sql.Types.INTEGER);

    }
    if (brote.getNM_VACENF().trim().length() > 0) {
      st.setInt(20, (new Integer(brote.getNM_VACENF().trim())).intValue());
    }
    else {
      st.setNull(20, java.sql.Types.INTEGER);

      /*System.out.println("MODIF 20");*/
    }
    if (brote.getDS_OBSERV().trim().length() > 0) {
      st.setString(21, brote.getDS_OBSERV().trim());
    }
    else {
      st.setNull(21, java.sql.Types.VARCHAR);

    }
    st.setString(22, brote.getCD_TIPOCOL().trim());

    if (brote.getDS_NOMCOL().trim().length() > 0) {
      st.setString(23, brote.getDS_NOMCOL().trim());
    }
    else {
      st.setNull(23, java.sql.Types.VARCHAR);

    }
    if (brote.getDS_DIRCOL().trim().length() > 0) {
      st.setString(24, brote.getDS_DIRCOL().trim());
    }
    else {
      st.setNull(24, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_POSTALCOL().trim().length() > 0) {
      st.setString(25, brote.getCD_POSTALCOL().trim());
    }
    else {
      st.setNull(25, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_PROVCOL().trim().length() > 0) {
      st.setString(26, brote.getCD_PROVCOL().trim());
    }
    else {
      st.setNull(26, java.sql.Types.VARCHAR);

    }
    st.setString(27, brote.getDS_BROTE().trim());

    st.setString(28, brote.getCD_GRUPO().trim());

    if (brote.getFC_FECHAHORA_H().trim().length() > 0) {
      st.setTimestamp(29,
                      cadena_a_timestamp(fecha_mas_hora(brote.getFC_FECHAHORA_F().
          trim(),
          brote.getFC_FECHAHORA_H().trim())));
    }
    else {
      st.setTimestamp(29,
                      cadena_a_timestamp(fecha_mas_hora(brote.getFC_FECHAHORA_F().
          trim(),
          "00:00:00")));

    }
    if (brote.getNM_MANIPUL().trim().length() > 0) {
      st.setInt(30, (new Integer(brote.getNM_MANIPUL().trim())).intValue());
    }
    else {
      st.setNull(30, java.sql.Types.INTEGER);

      /*System.out.println("MODIF 30");*/
    }
    if (brote.getCD_ZBS_LE().trim().length() > 0) {
      st.setString(31, brote.getCD_ZBS_LE().trim());
    }
    else {
      st.setNull(31, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_NIVEL_2_LE().trim().length() > 0) {
      st.setString(32, brote.getCD_NIVEL_2_LE().trim());
    }
    else {
      st.setNull(32, java.sql.Types.VARCHAR);

    }
    st.setString(33, brote.getCD_TNOTIF().trim());

    if (brote.getIT_RESCALC().trim().length() > 0) {
      st.setString(34, brote.getIT_RESCALC().trim());
    }
    else {
      st.setNull(34, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_MUNCOL().trim().length() > 0) {
      st.setString(35, brote.getCD_MUNCOL().trim());
    }
    else {
      st.setNull(35, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_NIVEL_1_LCA().trim().length() > 0) {
      st.setString(36, brote.getCD_NIVEL_1_LCA().trim());
    }
    else {
      st.setNull(36, java.sql.Types.VARCHAR);

    }
    if (brote.getDS_TELCOL().trim().length() > 0) {
      st.setString(37, brote.getDS_TELCOL().trim());
    }
    else {
      st.setNull(37, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_NIVEL_1_LE().trim().length() > 0) {
      st.setString(38, brote.getCD_NIVEL_1_LE().trim());
    }
    else {
      st.setNull(38, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_NIVEL_2_LCA().trim().length() > 0) {
      st.setString(39, brote.getCD_NIVEL_2_LCA().trim());
    }
    else {
      st.setNull(39, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_ZBS_LCA().trim().length() > 0) {
      st.setString(40, brote.getCD_ZBS_LCA().trim());
    }
    else {
      st.setNull(40, java.sql.Types.VARCHAR);

      /*System.out.println("MODIF 40");*/
    }
    if (brote.getIT_PERIN().trim().length() > 0) {
      st.setString(41, brote.getIT_PERIN().trim());
    }
    else {
      st.setNull(41, java.sql.Types.VARCHAR);

    }
    if (brote.getIT_DCUAC().trim().length() > 0) {
      st.setString(42, brote.getIT_DCUAC().trim());
    }
    else {
      st.setNull(42, java.sql.Types.VARCHAR);

    }
    if (brote.getDS_NMCALLE().trim().length() > 0) {
      st.setString(43, brote.getDS_NMCALLE().trim());
    }
    else {
      st.setNull(43, java.sql.Types.VARCHAR);

    }
    if (brote.getCD_TBROTE().trim().length() > 0) {
      st.setString(44, brote.getCD_TBROTE().trim());
    }
    else {
      st.setNull(44, java.sql.Types.VARCHAR);

    }
    if (brote.getDS_PISOCOL().trim().length() > 0) {
      st.setString(45, brote.getDS_PISOCOL().trim());
    }
    else {
      st.setNull(45, java.sql.Types.VARCHAR);

    }
    st.setInt(46, (new Integer(brote.getNM_ALERBRO().trim())).intValue());
    st.setString(47, brote.getCD_ANO().trim());

    /*System.out.println("MODIF 47");*/
    st.executeUpdate();
    st.close();
    st = null;

    brote.remove("CD_OPE");
    brote.insert("CD_OPE", login);
    brote.remove("FC_ULTACT");
    brote.insert("FC_ULTACT", sFecha);

        /*System.out.println("SrvBIModif saliendo de borradoInsercionAlertaBrote");*/
    Vector aux = new Vector();
    aux.addElement(alerta);
    aux.addElement(brote);
    return aux;
  }

  private void borradoAlertaBrote(DataAlerta alerta,
                                  DataBrote brote,
                                  Connection con,
                                  PreparedStatement st,
                                  ResultSet rs,
                                  String login) throws Exception {

    String cd_ano = brote.getCD_ANO().trim();
    int nm_alerbro = new Integer(brote.getNM_ALERBRO().trim()).intValue();

    //1. Modifico la situacion y la fecha de la alerta

    if (alerta.getCD_SITALERBRO() != null) {
      String queryMod = " update SIVE_ALERTA_BROTES "
          + " set CD_OPE = ?, FC_ULTACT = ? "
          + " , CD_SITALERBRO = ? , FC_ALERBRO = ? "
          + " where CD_ANO = ? and NM_ALERBRO = ? ";

      st = con.prepareStatement(queryMod);

      //operador actual
      st.setString(1, login);

      //la fecha de ultima actualizacion no es un date
      //es un timestamp, fecha actual
      dFecha = new java.util.Date();
      String sFecha = formUltAct.format(dFecha); //string

      java.sql.Timestamp ts = cadena_a_timestamp(sFecha);
      st.setTimestamp(2, ts);

      st.setString(3, alerta.getCD_SITALERBRO().trim());
      dFecha = formater.parse(alerta.getFC_ALERBRO().trim());
      sqlFec = new java.sql.Date(dFecha.getTime());
      st.setDate(4, sqlFec);
      st.setString(5, cd_ano);
      st.setInt(6, nm_alerbro);

      st.executeUpdate();
      st.close();
      st = null;

      alerta.remove("CD_OPE");
      alerta.insert("CD_OPE", login);
      alerta.remove("FC_ULTACT");
      alerta.insert("FC_ULTACT", sFecha);
    }

    //2. borrar todos las ocurrencias dependientes de este brote
    //alrededor de 19 tablas

    /*
         String queryB = "delete from SIVE_AGCAUSAL_BROTE "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1,cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_ALIBROTE "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_BROTES_DISTGEDAD "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_BROTES_FACCONT "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_BROTES_MED "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_FACTOR_RIESGO "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_INVESTIGADOR_BROTE "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_LINEAS_M_BROTE "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_LISTAS_BROTE "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_MODELO_BROTE "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_MUESTRAS_BROTE "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_MUESTRAS_IND "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_PERBROTE "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_PERBROTE_SINT "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_PREGUNTAS_BROTE "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_RESP_BROTES "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_RESP_PERBROTES "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_SINTOMAS_BROTE "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
         queryB = "delete from SIVE_TATAQ_ENFVAC "
            + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
         st = con.prepareStatement(queryB);
         st.setString(1, cd_ano);
         st.setInt(2, nm_alerbro);
         st.executeUpdate();
         st.close();
         st=null;
     */

    String queryB = "delete from SIVE_RESP_BROTES "
        + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
    st = con.prepareStatement(queryB);

    st.setString(1, cd_ano);
    st.setInt(2, nm_alerbro);
    st.executeUpdate();
    st.close();
    st = null;

    //3. Poner los datos en el historico
    InsercionHistoricoBrotes(brote, con, st, rs, login);

    //4.borrar el brote

    queryB = "delete from SIVE_BROTES "
        + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
    st = con.prepareStatement(queryB);

    st.setString(1, brote.getCD_ANO().trim());
    st.setInt(2, new Integer(brote.getNM_ALERBRO().trim()).intValue());
    st.executeUpdate();
    st.close();
    st = null;

  }

  private void InsercionHistoricoBrotes(DataBrote brote,
                                        Connection con,
                                        PreparedStatement st,
                                        ResultSet rs,
                                        String login) throws Exception {

    //de momento el motbaja es nulo
    String queryComp = "insert into SIVE_HIST_BROTES " //
        + " ( FC_FSINPRIMC, FC_ISINPRIMC, "
        + " NM_PERINMIN, NM_PERINMAX, NM_PERINMED, "
        + " NM_ENFERMOS, FC_EXPOSICION, NM_INGHOSP, "
        + " NM_DEFUNCION, NM_EXPUESTOS, "
        + " CD_TRANSMIS, NM_NVACENF, "
        + " CD_OPE, FC_ULTACT, "
        + " NM_DCUACMIN, NM_DCUACMED, NM_DCUACMAX, "
        + " NM_NVACNENF, NM_VACNENF, NM_VACENF, "
        + " DS_OBSERV, CD_TIPOCOL, DS_NOMCOL, DS_DIRCOL, "
        + " CD_POSTALCOL, CD_PROVCOL,  "
        + " DS_BROTE, CD_GRUPO, FC_FECHAHORA, NM_MANIPUL, "
        + " CD_ZBS_LE, CD_NIVEL_2_LE, CD_TNOTIF, IT_RESCALC, "
        + " CD_MUNCOL, CD_NIVEL_1_LCA, DS_TELCOL, CD_NIVEL_1_LE, "
        + " CD_NIVEL_2_LCA, CD_ZBS_LCA, "
        + " IT_PERIN, IT_DCUAC, DS_NMCALLE, CD_TBROTE, DS_PISOCOL, "
        + "  NM_ALERBRO, CD_ANO) "
        + " select FC_FSINPRIMC, FC_ISINPRIMC, "
        + " NM_PERINMIN, NM_PERINMAX, NM_PERINMED, "
        + " NM_ENFERMOS, FC_EXPOSICION, NM_INGHOSP, "
        + " NM_DEFUNCION, NM_EXPUESTOS, "
        + " CD_TRANSMIS, NM_NVACENF, "
        + " CD_OPE, FC_ULTACT, "
        + " NM_DCUACMIN, NM_DCUACMED, NM_DCUACMAX, "
        + " NM_NVACNENF, NM_VACNENF, NM_VACENF, "
        + " DS_OBSERV, CD_TIPOCOL, DS_NOMCOL, DS_DIRCOL, "
        + " CD_POSTALCOL, CD_PROVCOL,  "
        + " DS_BROTE, CD_GRUPO, FC_FECHAHORA, NM_MANIPUL, "
        + " CD_ZBS_LE, CD_NIVEL_2_LE, CD_TNOTIF, IT_RESCALC, "
        + " CD_MUNCOL, CD_NIVEL_1_LCA, DS_TELCOL, CD_NIVEL_1_LE, "
        + " CD_NIVEL_2_LCA, CD_ZBS_LCA, "
        + " IT_PERIN, IT_DCUAC, DS_NMCALLE, CD_TBROTE, DS_PISOCOL, "
        + "  NM_ALERBRO, CD_ANO "
        + " from SIVE_BROTES where cd_ano = ? and nm_alerbro = ? ";

    st = con.prepareStatement(queryComp);

    st.setString(1, brote.getCD_ANO().trim());
    st.setInt(2, (new Integer(brote.getNM_ALERBRO().trim())).intValue());

    st.executeUpdate();
    st.close();
    st = null;

    //ahora corrijo el operador y la fecha de actualizacion
    queryComp = " update SIVE_HIST_BROTES "
        + " set CD_OPE = ?, FC_ULTACT = ? "
        + " where CD_ANO = ? and NM_ALERBRO = ? ";
    st = con.prepareStatement(queryComp);

    //operador actual
    st.setString(1, login);

    //la fecha de ultima actualizacion no es un date
    //es un timestamp, fecha actual
    dFecha = new java.util.Date();
    String sFecha = formUltAct.format(dFecha); //string

    java.sql.Timestamp ts = cadena_a_timestamp(sFecha);
    st.setTimestamp(2, ts);

    st.setString(3, brote.getCD_ANO().trim());
    st.setInt(4, (new Integer(brote.getNM_ALERBRO().trim())).intValue());

    st.executeUpdate();
    st.close();
    st = null;

  }

  private int comprobarModBor(DataBrote brote,
                              Connection con,
                              PreparedStatement st,
                              ResultSet rs,
                              String login) throws Exception {

    String queryComp = " update SIVE_BROTES "
        + " set CD_OPE = ? , FC_ULTACT = ? "
        + " where ( CD_ANO = ? and NM_ALERBRO = ? "
        + " and CD_OPE = ? and FC_ULTACT = ?  )";

    st = con.prepareStatement(queryComp);

    java.sql.Timestamp ts = null;

    //operador actual
    st.setString(1, login);

    //la fecha de ultima actualizacion no es un date
    //es un timestamp, fecha actual
    dFecha = new java.util.Date();
    String sFecha = formUltAct.format(dFecha); //string

    ts = cadena_a_timestamp(sFecha);
    st.setTimestamp(2, ts);

    st.setString(3, brote.getCD_ANO().trim());

    st.setInt(4, new Integer(brote.getNM_ALERBRO().trim()).intValue());

    st.setString(5, brote.getCD_OPE().trim());

    ts = cadena_a_timestamp(brote.getFC_ULTACT().trim());
    st.setTimestamp(6, ts);

    int resUpdate = 0;
    resUpdate = st.executeUpdate();
    st.close();
    st = null;

    return resUpdate;
  }

  private String modificado_o_borrado(DataBrote brote,
                                      Connection con,
                                      PreparedStatement st,
                                      ResultSet rs) throws Exception {

    String resultado = "";

    String queryComp = " select * from SIVE_BROTES "
        + " where CD_ANO = ? and NM_ALERBRO = ? ";
    st = con.prepareStatement(queryComp);
    st.setString(1, brote.getCD_ANO().trim());
    st.setString(2, brote.getNM_ALERBRO().trim());

    rs = st.executeQuery();
    if (rs.next()) {
      //resultado modificado
      resultado = "M";
    }
    else {
      //resultado borrado
      resultado = "B";
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    return resultado;

  }

}