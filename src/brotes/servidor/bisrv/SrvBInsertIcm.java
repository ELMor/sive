package brotes.servidor.bisrv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import brotes.datos.bidata.DataAlerta;
import capp.CLista;
import sapp.DBServlet;

public class SrvBInsertIcm
    extends DBServlet {

  final int servletINSERT = 0;

  public SimpleDateFormat Format_Horas = new SimpleDateFormat(
      "dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));
  public java.sql.Timestamp fecRecu = null;
  public String sfecRecu = "";

  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    //java.sql.Timestamp TSFec = new java.sql.Timestamp(dFecha.getTime());
    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);
    //# System.Out.println("ts : " + TSFec.toString());

    return TSFec;
  }

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  private String fecha_de_fecha(String f) {
    //dd/mm/yyyy hh:mm:ss
    f = f.trim();
    return f.substring(0, f.indexOf(' ', 0));
  }

  private String hora_de_fecha(String f) {
    //dd/mm/yyyy hh:mm:ss
    f = f.trim();
    return f.substring(f.indexOf(' ', 0) + 1);
  }

  private String fecha_mas_hora(String f, String h) {
    f = f.trim();
    h = h.trim();
    if (h.equals("")) {
      h = "00:00";
    }
    System.out.println("SrvBInsertIcm: fecha " + f);
    System.out.println("SrvBInsertIcm: hora " + h);
    System.out.println("SrvBInsertIcm: fecha_mas_hora " + f + " " +
                       h.substring(0, h.lastIndexOf(':')));
    //return f + " " + h.substring(0, h.lastIndexOf(':'));
    return f + " " + h + ":00";
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    DataAlerta dataEntrada = null;
    CLista listaSalida = new CLista();
    DataAlerta dataSalida = new DataAlerta();

    //querys
    String sQuery = "";

    //fechas
    java.util.Date dFecha;
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    //STRING
    String desMunicipio = null;
    String codCadef = null;
    String desNivel1 = null;
    String desNivel2 = null;
    String desZB = null;
    String desNivel1_normal = null;
    String desNivel2_normal = null;
    String desequipo = null;
    String codcentro = null;
    String descentro = null;
    int cdAler = 0;

    try {

      switch (opmode) {

        //con SUCA cambia la seleccion de descripciones de pais, ca, prov y mun
        case servletINSERT:

          dataEntrada = (DataAlerta) param.firstElement();

//!!!!!!!!!!!!!!!!!!!!!!
//!!!!BROTES!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!

          System.out.println("SrvBInsertIcm: " + Pregunta_Select_Cod_Alerta());
          sQuery = Pregunta_Select_Cod_Alerta(); //30 campos
          st = con.prepareStatement(sQuery);

          st.setString(1, dataEntrada.getCD_ANO().trim());
          rs = st.executeQuery();

          if (rs.next()) {
            cdAler = rs.getInt(1) + 1;

          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          System.out.println("SrvBInsertIcm: cod Alerta " + cdAler);

          System.out.println("SrvBInsertIcm: antes Pregunta_Insert_Brotes()");
          sQuery = Pregunta_Insert_Brotes(); //30 campos
          st = con.prepareStatement(sQuery);

          st.setString(1, dataEntrada.getCD_ANO().trim());
          st.setInt(2, cdAler);
          if (dataEntrada.getCD_LEXPROV().trim().length() > 0) {
            st.setString(3, dataEntrada.getCD_LEXPROV().trim());
          }
          else {
            st.setNull(3, java.sql.Types.VARCHAR);
          }
          System.out.println("SrvBInsertIcm: despues parametro 3");

          if (dataEntrada.getCD_LEXMUN().trim().length() > 0) {
            st.setString(4, dataEntrada.getCD_LEXMUN().trim());
          }
          else {
            st.setNull(4, java.sql.Types.VARCHAR);
          }
          System.out.println("SrvBInsertIcm: despues parametro 4");
          st.setTimestamp(5,
                          cadena_a_timestamp(fecha_mas_hora(dataEntrada.getFC_FECHAHORA_F().
              trim(),
              dataEntrada.getFC_FECHAHORA_H().trim())));
          System.out.println("SrvBInsertIcm: despues parametro 5");
          st.setString(6, dataEntrada.getCD_NIVEL_1_EX().trim());
          System.out.println("SrvBInsertIcm: despues parametro 6");
          if (dataEntrada.getCD_NIVEL_2_EX().trim().length() > 0) {
            st.setString(7, dataEntrada.getCD_NIVEL_2_EX().trim());
          }
          else {
            st.setNull(7, java.sql.Types.VARCHAR);
          }
          System.out.println("SrvBInsertIcm: despues parametro 7");
          if (dataEntrada.getCD_ZBS_EX().trim().length() > 0) {
            st.setString(8, dataEntrada.getCD_ZBS_EX().trim());
          }
          else {
            st.setNull(8, java.sql.Types.VARCHAR);
          }
          System.out.println("SrvBInsertIcm: despues parametro 8");
          st.setString(9, dataEntrada.getCD_NIVEL_1().trim());
          System.out.println("SrvBInsertIcm: despues parametro 9");
          if (dataEntrada.getCD_NIVEL_2().trim().length() > 0) {
            st.setString(10, dataEntrada.getCD_NIVEL_2().trim());
          }
          else {
            st.setNull(10, java.sql.Types.VARCHAR);
          }
          System.out.println("SrvBInsertIcm: despues parametro 10");
          if (dataEntrada.getCD_E_NOTIF().trim().length() > 0) {
            st.setString(11, dataEntrada.getCD_E_NOTIF().trim());
          }
          else {
            st.setNull(11, java.sql.Types.VARCHAR);
          }
          System.out.println("SrvBInsertIcm: despues parametro 11");
          st.setString(12, dataEntrada.getDS_ALERTA().trim());
          System.out.println("SrvBInsertIcm: despues parametro 12");
          st.setString(13, dataEntrada.getCD_GRUPO().trim());
          System.out.println("SrvBInsertIcm: despues parametro 13");
          if (dataEntrada.getNM_EXPUESTOS().trim().length() > 0) {
            st.setInt(14,
                      (new Integer(dataEntrada.getNM_EXPUESTOS().trim())).intValue());
          }
          else {
            st.setNull(14, java.sql.Types.INTEGER);
          }
          System.out.println("SrvBInsertIcm: despues parametro 14");
          if (dataEntrada.getNM_ENFERMOS().trim().length() > 0) {
            st.setInt(15,
                      (new Integer(dataEntrada.getNM_ENFERMOS().trim())).intValue());
          }
          else {
            st.setNull(15, java.sql.Types.INTEGER);
          }
          System.out.println("SrvBInsertIcm: despues parametro 15");
          if (dataEntrada.getNM_INGHOSP().trim().length() > 0) {
            st.setInt(16,
                      (new Integer(dataEntrada.getNM_INGHOSP().trim())).intValue());
          }
          else {
            st.setNull(16, java.sql.Types.INTEGER);
          }
          System.out.println("SrvBInsertIcm: despues parametro 16");

          if (dataEntrada.getFC_EXPOSICION_F().trim().length() > 0) {
            if (dataEntrada.getFC_EXPOSICION_H().trim().length() > 0) {
              st.setTimestamp(17,
                              cadena_a_timestamp(fecha_mas_hora(dataEntrada.getFC_EXPOSICION_F().
                  trim(),
                  dataEntrada.getFC_EXPOSICION_H().trim())));
            }
            else {
              st.setTimestamp(17,
                              cadena_a_timestamp(fecha_mas_hora(dataEntrada.getFC_EXPOSICION_F().
                  trim(),
                  "00:00:00")));
            }
          }
          else {
            st.setNull(17, java.sql.Types.DATE);
          }
          System.out.println("SrvBInsertIcm: despues parametro 17");
          if (dataEntrada.getFC_INISINTOMAS_F().trim().length() > 0) {
            if (dataEntrada.getFC_INISINTOMAS_H().trim().length() > 0) {
              st.setTimestamp(18,
                              cadena_a_timestamp(fecha_mas_hora(dataEntrada.getFC_INISINTOMAS_F().
                  trim(),
                  dataEntrada.getFC_INISINTOMAS_H().trim())));
            }
            else {
              st.setTimestamp(18,
                              cadena_a_timestamp(fecha_mas_hora(dataEntrada.getFC_INISINTOMAS_F().
                  trim(),
                  "00:00:00")));
            }
          }
          else {
            st.setNull(18, java.sql.Types.DATE);
          }
          System.out.println("SrvBInsertIcm: despues parametro 18");
          if (dataEntrada.getDS_LEXP().trim().length() > 0) {
            st.setString(19, dataEntrada.getDS_LEXP().trim());
          }
          else {
            st.setNull(19, java.sql.Types.VARCHAR);
          }
          System.out.println("SrvBInsertIcm: despues parametro 19");
          if (dataEntrada.getDS_LEXPDIREC().trim().length() > 0) {
            st.setString(20, dataEntrada.getDS_LEXPDIREC().trim());
          }
          else {
            st.setNull(20, java.sql.Types.VARCHAR);
          }
          System.out.println("SrvBInsertIcm: despues parametro 20");
          if (dataEntrada.getCD_LEXPPOST().trim().length() > 0) {
            st.setString(21, dataEntrada.getCD_LEXPPOST().trim());
          }
          else {
            st.setNull(21, java.sql.Types.VARCHAR);
          }
          System.out.println("SrvBInsertIcm: despues parametro 21");
          if (dataEntrada.getDS_LNMCALLE().trim().length() > 0) {
            st.setString(22, dataEntrada.getDS_LNMCALLE().trim());
          }
          else {
            st.setNull(22, java.sql.Types.VARCHAR);
          }
          System.out.println("SrvBInsertIcm: despues parametro 22");
          if (dataEntrada.getDS_LEXPTELEF().trim().length() > 0) {
            st.setString(23, dataEntrada.getDS_LEXPTELEF().trim());
          }
          else {
            st.setNull(23, java.sql.Types.VARCHAR);
          }
          System.out.println("SrvBInsertIcm: despues parametro 23");
          st.setString(24, dataEntrada.getCD_SITALERBRO().trim());
          System.out.println("SrvBInsertIcm: despues parametro 24");
          dFecha = formater.parse(dataEntrada.getFC_ALERBRO().trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(25, sqlFec);
          System.out.println("SrvBInsertIcm: despues parametro 25");
          if (dataEntrada.getIT_VALIDADA().trim().length() > 0) {
            st.setString(26, dataEntrada.getIT_VALIDADA().trim());
          }
          else {
            st.setNull(26, java.sql.Types.VARCHAR);
          }
          System.out.println("SrvBInsertIcm: despues parametro 26");
          if (dataEntrada.getFC_FECVALID().trim().length() > 0) {
            dFecha = formater.parse(dataEntrada.getFC_FECVALID().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(27, sqlFec);
          }
          else {
            st.setNull(27, java.sql.Types.DATE);
          }
          System.out.println("SrvBInsertIcm: despues parametro 27");
          st.setString(28, dataEntrada.getCD_OPE().trim());
          System.out.println("SrvBInsertIcm: despues parametro 28");

          //new date       *************
          dFecha = new java.util.Date();
          String sFecha = Format_Horas.format(dFecha); //string
          //partirla!!     *************
          sfecRecu = sFecha;
          st.setTimestamp(29, cadena_a_timestamp(sFecha));

          System.out.println("SrvBInsertIcm: despues parametro 29");
          if (dataEntrada.getDS_LEXPPISO().trim().length() > 0) {
            st.setString(30, dataEntrada.getDS_LEXPPISO().trim());
          }
          else {
            st.setNull(30, java.sql.Types.VARCHAR);
          }
          System.out.println("SrvBInsertIcm: despues parametro 30");

          st.executeUpdate();

          st.close();
          st = null;

          System.out.println(
              "SrvBInsertIcm: Pregunta_Insert_Brotes() realizada ");
//!!!!!!!!!!!!!!!!!!!!!!
//!!!!ADIC!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!

          System.out.println("SrvBInsertIcm: antes Pregunta_Insert_Adic()");
          sQuery = Pregunta_Insert_Adic();
          st = con.prepareStatement(sQuery);
          st.setString(1, dataEntrada.getCD_ANO().trim());
          st.setInt(2, cdAler);

          if (dataEntrada.getCD_MINPROV().trim().length() > 0) {
            st.setString(3, dataEntrada.getCD_MINPROV().trim());
          }
          else {
            st.setNull(3, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getCD_MINMUN().trim().length() > 0) {
            st.setString(4, dataEntrada.getCD_MINMUN().trim());
          }
          else {
            st.setNull(4, java.sql.Types.VARCHAR);
          }
          st.setString(5, dataEntrada.getCD_TNOTIF().trim());
          if (dataEntrada.getDS_NOTIFINST().trim().length() > 0) {
            st.setString(6, dataEntrada.getDS_NOTIFINST().trim());
          }
          else {
            st.setNull(6, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getCD_NOTIFPROV().trim().length() > 0) {
            st.setString(7, dataEntrada.getCD_NOTIFPROV().trim());
          }
          else {
            st.setNull(7, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_NOTIFICADOR().trim().length() > 0) {
            st.setString(8, dataEntrada.getDS_NOTIFICADOR().trim());
          }
          else {
            st.setNull(8, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_NOTIFDIREC().trim().length() > 0) {
            st.setString(9, dataEntrada.getDS_NOTIFDIREC().trim());
          }
          else {
            st.setNull(9, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getCD_NOTIFMUN().trim().length() > 0) {
            st.setString(10, dataEntrada.getCD_NOTIFMUN().trim());
          }
          else {
            st.setNull(10, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getCD_NOTIFPOSTAL().trim().length() > 0) {
            st.setString(11, dataEntrada.getCD_NOTIFPOSTAL().trim());
          }
          else {
            st.setNull(11, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_NNMCALLE().trim().length() > 0) {
            st.setString(12, dataEntrada.getDS_NNMCALLE().trim());
          }
          else {
            st.setNull(12, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_NOTIFTELEF().trim().length() > 0) {
            st.setString(13, dataEntrada.getDS_NOTIFTELEF().trim());
          }
          else {
            st.setNull(13, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_MINFPER().trim().length() > 0) {
            st.setString(14, dataEntrada.getDS_MINFPER().trim());
          }
          else {
            st.setNull(14, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_MINFDIREC().trim().length() > 0) {
            st.setString(15, dataEntrada.getDS_MINFDIREC().trim());
          }
          else {
            st.setNull(15, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getCD_MINFPOSTAL().trim().length() > 0) {
            st.setString(16, dataEntrada.getCD_MINFPOSTAL().trim());
          }
          else {
            st.setNull(16, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_MINFTELEF().trim().length() > 0) {
            st.setString(17, dataEntrada.getDS_MINFTELEF().trim());
          }
          else {
            st.setNull(17, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_MNMCALLE().trim().length() > 0) {
            st.setString(18, dataEntrada.getDS_MNMCALLE().trim());
          }
          else {
            st.setNull(18, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_OBSERV().trim().length() > 0) {
            st.setString(19, dataEntrada.getDS_OBSERV().trim());
          }
          else {
            st.setNull(19, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_ALISOSP().trim().length() > 0) {
            st.setString(20, dataEntrada.getDS_ALISOSP().trim());
          }
          else {
            st.setNull(20, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_SINTOMAS().trim().length() > 0) {
            st.setString(21, dataEntrada.getDS_SINTOMAS().trim());
          }
          else {
            st.setNull(21, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_NOTIFPISO().trim().length() > 0) {
            st.setString(22, dataEntrada.getDS_NOTIFPISO().trim());
          }
          else {
            st.setNull(22, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_MINFPISO().trim().length() > 0) {
            st.setString(23, dataEntrada.getDS_MINFPISO().trim());
          }
          else {
            st.setNull(23, java.sql.Types.VARCHAR);

          }
          st.executeUpdate();

          st.close();
          st = null;

          System.out.println("SrvBInsertIcm: Pregunta_Insert_Adic() realizada");
//!!!!!!!!!!!!!!!!!!!!!!
//!!!!COLEC!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!
          System.out.println("SrvBInsertIcm: antes Pregunta_Insert_Colec()");

          sQuery = Pregunta_Insert_Colec();
          st = con.prepareStatement(sQuery);

          st.setString(1, dataEntrada.getCD_ANO().trim());
          st.setInt(2, cdAler);
          if (dataEntrada.getCD_PROV().trim().length() > 0) {
            st.setString(3, dataEntrada.getCD_PROV().trim());
          }
          else {
            st.setNull(3, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getCD_MUN().trim().length() > 0) {
            st.setString(4, dataEntrada.getCD_MUN().trim());
          }
          else {
            st.setNull(4, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_NOMCOL().trim().length() > 0) {
            st.setString(5, dataEntrada.getDS_NOMCOL().trim());
          }
          else {
            st.setNull(5, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_DIRCOL().trim().length() > 0) {
            st.setString(6, dataEntrada.getDS_DIRCOL().trim());
          }
          else {
            st.setNull(6, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_NMCALLE().trim().length() > 0) {
            st.setString(7, dataEntrada.getDS_NMCALLE().trim());
          }
          else {
            st.setNull(7, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getCD_POSTALCOL().trim().length() > 0) {
            st.setString(8, dataEntrada.getCD_POSTALCOL().trim());
          }
          else {
            st.setNull(8, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_TELCOL().trim().length() > 0) {
            st.setString(9, dataEntrada.getDS_TELCOL().trim());
          }
          else {
            st.setNull(9, java.sql.Types.VARCHAR);
          }
          if (dataEntrada.getDS_PISOCOL().trim().length() > 0) {
            st.setString(10, dataEntrada.getDS_PISOCOL().trim());
          }
          else {
            st.setNull(10, java.sql.Types.VARCHAR);

          }
          st.executeUpdate();

          st.close();
          st = null;

          System.out.println(
              "SrvBInsertIcm: Pregunta_Insert_Colec() realizada ");

          //PREPARAR LA LISTASALIDA con DataAlerta
          dataSalida = dataEntrada;
          dataSalida.insert("NM_ALERBRO", (new Integer(cdAler)).toString());
          dataSalida.insert("FC_ULTACT", sfecRecu);

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    //listaSalida.addElement(new Integer(cdAler));
    listaSalida.addElement(dataSalida);

    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;
  } //fin doWork

  public String Pregunta_Select_Cod_Alerta() {
    return ("select "
            + " max(NM_ALERBRO) "
            + " from SIVE_ALERTA_BROTES where CD_ANO = ? ");
  }

  public String Pregunta_Insert_Adic() {
    return ("insert into SIVE_ALERTA_ADIC " //23
            + "(CD_ANO, NM_ALERBRO, "
            + " CD_MINPROV, CD_MINMUN,  "
            + " CD_TNOTIF, DS_NOTIFINST, "
            + " CD_NOTIFPROV, DS_NOTIFICADOR, DS_NOTIFDIREC, "
            + " CD_NOTIFMUN, CD_NOTIFPOSTAL, DS_NNMCALLE, "
            + " DS_NOTIFTELEF, DS_MINFPER, DS_MINFDIREC, CD_MINFPOSTAL,  "
            + " DS_MINFTELEF,  DS_MNMCALLE, DS_OBSERV, "
            + " DS_ALISOSP,  DS_SINTOMAS, "
            + " DS_NOTIFPISO, DS_MINFPISO "
            + " ) "
            + "values ( ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,? )");
  }

  public String Pregunta_Insert_Colec() {
    return ("insert into SIVE_ALERTA_COLEC " //10
            + "( CD_ANO, NM_ALERBRO, "
            + " CD_PROV, CD_MUN,  "
            + " DS_NOMCOL, DS_DIRCOL, "
            + " DS_NMCALLE, CD_POSTALCOL, DS_TELCOL, "
            + " DS_PISOCOL  )"
            + " values ( ?,?,?,?,?, ?,?,?,?,?)");
  }

  public String Pregunta_Insert_Brotes() {

    return ("insert into SIVE_ALERTA_BROTES " //30
            + " ( CD_ANO,  NM_ALERBRO, "
            + " CD_LEXPROV, CD_LEXMUN,  "
            + " FC_FECHAHORA, CD_NIVEL_1_EX, CD_NIVEL_2_EX,  CD_ZBS_EX,"
            + " CD_NIVEL_1, CD_NIVEL_2, CD_E_NOTIF, "
            + " DS_ALERTA, CD_GRUPO, NM_EXPUESTOS, NM_ENFERMOS, NM_INGHOSP, "
            + " FC_EXPOSICION, FC_INISINTOMAS, DS_LEXP,  "
            + " DS_LEXPDIREC,  CD_LEXPPOST, DS_LNMCALLE, "
            + " DS_LEXPTELEF,  CD_SITALERBRO, FC_ALERBRO, "
            + " IT_VALIDADA,  FC_FECVALID, CD_OPE, "
            + " FC_ULTACT, DS_LEXPPISO ) "
            +
        "values ( ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,  ?,?,?,?,?, ?,?,?,?,?)");
  }

} //fin de la clase
