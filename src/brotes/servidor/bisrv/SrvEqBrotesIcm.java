package brotes.servidor.bisrv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import brotes.datos.bidata.DataBusqueda;
import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

//databusqueda
//VN1
//VN2
public class SrvEqBrotesIcm
    extends DBServlet {

  final int servletOBTENER_EQUIPO_X_CODIGO = 4;
  final int servletOBTENER_EQUIPO_X_DESCRIPCION = 5;
  final int servletSELECCION_EQUIPO_X_CODIGO = 6;
  final int servletSELECCION_EQUIPO_X_DESCRIPCION = 7;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Configuraci�n
    String sQuery = "";
    String sFiltro = "";
    String sAutorizacion1 = "";
    String sAutorizacion2 = "";
    Vector vN1 = null;
    Vector vN2 = null;

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int iValor = 1;

    // objetos de datos
    CLista listaSalida = new CLista();
    DataBusqueda ent = null;
    DataBusqueda dataSalida = null;
    DataBusqueda dataLinea = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    String des = "";
    String desL = "";

    try {

      switch (opmode) {

        case servletOBTENER_EQUIPO_X_CODIGO:
        case servletOBTENER_EQUIPO_X_DESCRIPCION:

          ent = (DataBusqueda) param.firstElement();
          vN1 = (Vector) ent.getVN1();
          vN2 = (Vector) ent.getVN2();

          //respecto a Nivel1 y 2 ------------------
          if (!ent.getCD_NIVEL_1().equals("")) {
            sFiltro = " AND CD_NIVEL_1 = ?";
          }
          if (!ent.getCD_NIVEL_2().equals("")) {
            sFiltro = sFiltro + " AND CD_NIVEL_2 = ?";

          }
          if (ent.getCD_NIVEL_1().equals("") && ent.getCD_NIVEL_2().equals("")) {
            switch (param.getPerfil()) {
              case CApp.perfilNIVEL1:
                sAutorizacion1 = " AND CD_NIVEL_1 IN (";
                for (i = 0; i < vN1.size(); i++) {
                  sAutorizacion1 = sAutorizacion1 + " ?" + ",";
                }
                sAutorizacion1 = sAutorizacion1.substring(0,
                    sAutorizacion1.length() - 1) + ")";
                break;

              case CApp.perfilNIVEL2:
                for (i = 0; i < vN1.size(); i++) {
                  sAutorizacion2 = sAutorizacion2 +
                      " (CD_NIVEL_1 = ? AND CD_NIVEL_2 = ?) OR";
                }
                sAutorizacion2 = " AND ( " +
                    sAutorizacion2.substring(0, sAutorizacion2.length() - 2) +
                    ")";
                break;
            }
          }

          //-----------------------------------------

          // query
          if (opmode == servletOBTENER_EQUIPO_X_CODIGO) {
            sQuery = "select CD_E_NOTIF, DS_E_NOTIF, CD_NIVEL_1, CD_NIVEL_2, " +
                " CD_CENTRO from SIVE_E_NOTIF " +
                " where CD_E_NOTIF = ?";
          }
          else {
            sQuery = "select CD_E_NOTIF, DS_E_NOTIF, CD_NIVEL_1, CD_NIVEL_2, " +
                " CD_CENTRO from SIVE_E_NOTIF " +
                " where DS_E_NOTIF = ?";

            //query
          }
          sQuery = sQuery + sFiltro + sAutorizacion1 + sAutorizacion2;
          sQuery = sQuery +
              " AND CD_ANOEPI = ? AND IT_BAJA = ? order by CD_E_NOTIF";

          st = con.prepareStatement(sQuery);

          // filtro normal
          st.setString(iValor, ent.getCD_E_NOTIF().trim()); //sera des o cod
          iValor++;

          //respecto a nivel 1 y 2 ---------------
          if (!ent.getCD_NIVEL_1().equals("")) {
            st.setString(iValor, ent.getCD_NIVEL_1().trim());
            iValor++;
          }
          if (!ent.getCD_NIVEL_2().equals("")) {
            st.setString(iValor, ent.getCD_NIVEL_2().trim());
            iValor++;
          }
          if (ent.getCD_NIVEL_1().equals("") && ent.getCD_NIVEL_2().equals("")) {
            switch (param.getPerfil()) {
              case CApp.perfilNIVEL1:
                for (i = 0; i < vN1.size(); i++) {
                  st.setString(iValor, (String) vN1.elementAt(i));
                  iValor++;
                }
                break;

              case CApp.perfilNIVEL2:
                for (i = 0; i < vN1.size(); i++) {
                  st.setString(iValor, (String) vN1.elementAt(i));
                  iValor++;
                  st.setString(iValor, (String) vN2.elementAt(i));
                  iValor++;
                }
                break;
            }
          }

          //---------------------------------------------

          //a�o e it_baja
          st.setString(iValor, ent.getCD_ANO().trim());
          iValor++;
          st.setString(iValor, "N");
          iValor++;

          break;

          //TRAMAR ****
        case servletSELECCION_EQUIPO_X_CODIGO:
        case servletSELECCION_EQUIPO_X_DESCRIPCION:

          ent = (DataBusqueda) param.firstElement();
          vN1 = (Vector) ent.getVN1();
          vN2 = (Vector) ent.getVN2();

          //respecto a Nivel1 y 2 ------------------
          if (!ent.getCD_NIVEL_1().equals("")) {
            sFiltro = " AND CD_NIVEL_1 = ?";
          }
          if (!ent.getCD_NIVEL_2().equals("")) {
            sFiltro = sFiltro + " AND CD_NIVEL_2 = ?";

          }
          if (ent.getCD_NIVEL_1().equals("") && ent.getCD_NIVEL_2().equals("")) {
            switch (param.getPerfil()) {
              case CApp.perfilNIVEL1:
                sAutorizacion1 = " AND CD_NIVEL_1 IN (";
                for (i = 0; i < vN1.size(); i++) {
                  sAutorizacion1 = sAutorizacion1 + " ?" + ",";
                }
                sAutorizacion1 = sAutorizacion1.substring(0,
                    sAutorizacion1.length() - 1) + ")";
                break;

              case CApp.perfilNIVEL2:
                for (i = 0; i < vN1.size(); i++) {
                  sAutorizacion2 = sAutorizacion2 +
                      " (CD_NIVEL_1 = ? AND CD_NIVEL_2 = ?) OR";
                }
                sAutorizacion2 = " AND ( " +
                    sAutorizacion2.substring(0, sAutorizacion2.length() - 2) +
                    ")";
                break;
            }
          }

          //-----------------------------------------

          // query
          //tramar
          if (param.getFilter().length() > 0) {

            if (opmode == servletSELECCION_EQUIPO_X_CODIGO) {
              sQuery =
                  "select CD_E_NOTIF, DS_E_NOTIF, CD_NIVEL_1, CD_NIVEL_2, " +
                  " CD_CENTRO from SIVE_E_NOTIF " +
                  " where CD_E_NOTIF like ? and CD_E_NOTIF > ? ";

            }
            else {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, CD_NIVEL_1, CD_NIVEL_2," +
                  " CD_CENTRO from SIVE_E_NOTIF " +
                  " where DS_E_NOTIF like ? and DS_E_NOTIF > ? ";
            }
          }
          else {
            if (opmode == servletSELECCION_EQUIPO_X_CODIGO) {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, CD_NIVEL_1, CD_NIVEL_2," +
                  " CD_CENTRO from SIVE_E_NOTIF " +
                  " where CD_E_NOTIF like ? ";

            }
            else {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, CD_NIVEL_1, CD_NIVEL_2," +
                  " CD_CENTRO from SIVE_E_NOTIF " +
                  " where DS_E_NOTIF like ? ";
            }
          }

          sQuery = sQuery + sFiltro + sAutorizacion1 + sAutorizacion2;
          sQuery = sQuery + " AND CD_ANOEPI = ? AND IT_BAJA = ? ";

          //order by
          if (opmode == servletSELECCION_EQUIPO_X_CODIGO) {
            sQuery = sQuery + " order by CD_E_NOTIF";
          }
          else {
            sQuery = sQuery + " order by DS_E_NOTIF";

            //pregunta
          }
          st = con.prepareStatement(sQuery);

          // filtro normal
          st.setString(iValor, ent.getCD_E_NOTIF().trim() + "%");
          iValor++;

          //trama
          if (param.getFilter().length() > 0) {
            st.setString(iValor, param.getFilter().trim());
            iValor++;
          }

          //respecto a nivel 1 y 2 ---------------
          if (!ent.getCD_NIVEL_1().equals("")) {
            st.setString(iValor, ent.getCD_NIVEL_1().trim());
            iValor++;
          }
          if (!ent.getCD_NIVEL_2().equals("")) {
            st.setString(iValor, ent.getCD_NIVEL_2().trim());
            iValor++;
          }
          if (ent.getCD_NIVEL_1().equals("") && ent.getCD_NIVEL_2().equals("")) {
            switch (param.getPerfil()) {
              case CApp.perfilNIVEL1:
                for (i = 0; i < vN1.size(); i++) {
                  st.setString(iValor, (String) vN1.elementAt(i));
                  iValor++;
                }
                break;

              case CApp.perfilNIVEL2:
                for (i = 0; i < vN1.size(); i++) {
                  st.setString(iValor, (String) vN1.elementAt(i));
                  iValor++;
                  st.setString(iValor, (String) vN2.elementAt(i));
                  iValor++;
                }
                break;
            }
          }

          //---------------------------------------------

          //a�o e it_baja
          st.setString(iValor, ent.getCD_ANO().trim());
          iValor++;
          st.setString(iValor, "N");
          iValor++;

          break;

      }

      //lanzar query

      rs = st.executeQuery();

      // 05/04/2000 (JMT) No estaba bien inicializada la variable i
      i = 1;
      while (rs.next()) {

        //tramar ---
        if ( (opmode == servletSELECCION_EQUIPO_X_CODIGO) ||
            (opmode == servletSELECCION_EQUIPO_X_DESCRIPCION)) {

          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            listaSalida.setState(CLista.listaINCOMPLETA);

            if (opmode == servletSELECCION_EQUIPO_X_CODIGO) {
              listaSalida.setFilter( ( (DataBusqueda) listaSalida.lastElement()).
                                    getCD_E_NOTIF());
            }
            else {
              listaSalida.setFilter( ( (DataBusqueda) listaSalida.lastElement()).
                                    getDS_E_NOTIF());

            }
            break;
          }

          // control de estado
          if (listaSalida.getState() == CLista.listaVACIA) {
            listaSalida.setState(CLista.listaLLENA);
          }
        } //tramar ------

        dataSalida = new DataBusqueda("", "", "", "", "", "", "",
                                      rs.getString("CD_NIVEL_1"), "",
                                      rs.getString("CD_NIVEL_2"), "",
                                      rs.getString("CD_E_NOTIF"),
                                      rs.getString("DS_E_NOTIF"),
                                      rs.getString("CD_CENTRO"),
                                      "", "", "", "", "", -1, null, null);

        // a�ade un nodo
        listaSalida.addElement(dataSalida);
        i++;
      }

      rs.close();
      rs = null;
      st.close();
      st = null;

      //CENTRO***************************************************************
      for (int j = 0; j < listaSalida.size(); j++) {
        dataLinea = (DataBusqueda) listaSalida.elementAt(j);

        sQuery = "select DS_CENTRO "
            + " from SIVE_C_NOTIF where CD_CENTRO = ?";

        st = con.prepareStatement(sQuery);
        st.setString(1, dataLinea.getCD_CENTRO());
        rs = st.executeQuery();

        while (rs.next()) {
          // obtiene el resto de campos para cada elemento de data
          dataLinea.DS_CENTRO = rs.getString("DS_CENTRO");
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

      } //for
      //nivel1***************************************************************
      for (int j = 0; j < listaSalida.size(); j++) {
        dataLinea = (DataBusqueda) listaSalida.elementAt(j);

        sQuery =
            "select DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where CD_NIVEL_1 = ?";

        st = con.prepareStatement(sQuery);
        st.setString(1, dataLinea.getCD_NIVEL_1());
        rs = st.executeQuery();

        while (rs.next()) {

          des = rs.getString("DS_NIVEL_1");
          desL = rs.getString("DSL_NIVEL_1");
          // obtiene la descripcion auxiliar en funci�n del idioma
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (desL != null)) {
            des = desL;

          }
          dataLinea.DS_NIVEL_1 = des;
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

      } //for
      //nivel1***************************************************************
      for (int j = 0; j < listaSalida.size(); j++) {
        dataLinea = (DataBusqueda) listaSalida.elementAt(j);

        sQuery =
            "select DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_2 = ?";

        st = con.prepareStatement(sQuery);
        st.setString(1, dataLinea.getCD_NIVEL_2());
        rs = st.executeQuery();

        while (rs.next()) {

          des = rs.getString("DS_NIVEL_2");
          desL = rs.getString("DSL_NIVEL_2");
          // obtiene la descripcion auxiliar en funci�n del idioma
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (desL != null)) {
            des = desL;

          }
          dataLinea.DS_NIVEL_2 = des;
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

      } //for

      // valida la transacci�n

      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;

    }

    // cierra la conexi�n y acaba el procedimiento doWork
    closeConnection(con);
    listaSalida.trimToSize();

    return listaSalida;
  }

}
