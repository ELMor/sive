package brotes.servidor.bisrv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Vector;

import brotes.datos.bidata.DataBusqueda;
import capp.CLista;
import sapp.DBServlet;

public class SrvBAlertaIcm
    extends DBServlet {

  final int servletTABLAALERTAS = 0;

  public java.sql.Timestamp fecRecu = null;
  public String sfecRecu = "";
  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    //java.sql.Timestamp TSFec = new java.sql.Timestamp(dFecha.getTime());
    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);

    return TSFec;
  }

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 0;

    // objetos de datos
    CLista listaSalida = new CLista();
    DataBusqueda dataSalida = null;
    DataBusqueda dataEntrada = null;

    //querys
    String sQuery = "";
    int iValor = 1;
    int iTramar = 1;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    //modos
    final int modoMODIFICACION = 4;
    final int modoCONSULTA = 6;

    String ds = "";
    String dsl = "";
    String sDes = "";

    try {

      switch (opmode) {
        case servletTABLAALERTAS:

          dataEntrada = (DataBusqueda) param.firstElement();

          sQuery = "SELECT CD_ANO, NM_ALERBRO, DS_ALERTA, " +
              " IT_VALIDADA, CD_GRUPO, CD_SITALERBRO, " +
              " CD_NIVEL_1, CD_NIVEL_2, CD_E_NOTIF, " +
              " CD_NIVEL_1_EX, CD_NIVEL_2_EX ," +
              " FC_FECHAHORA, FC_ALERBRO " +
              " FROM SIVE_ALERTA_BROTES  ";

          sQuery = sQuery + Preparar_Query(dataEntrada, param, opmode);
          sQuery = sQuery +
              "ORDER BY CD_ANO DESC, NM_ALERBRO DESC, FC_FECHAHORA DESC";

          st = con.prepareStatement(sQuery);

          // para el prepare (?)
          iValor = 1;
          Preparar_rsget(iValor, dataEntrada, st, param, opmode);
          rs = st.executeQuery();

          //nulos
          String cd_nivel_2 = "";
          String cd_e_notif = "";
          String it_validada = "";

          // autorizaciones
          String cd_nivel_1 = "";
          String cd_nivel_1_ex = "";
          String cd_nivel_2_ex = "";

          //enteros
          int iN = 0;
          Integer IN = new Integer(iN);
          String sN = IN.toString();

          //modo
          int modo = 0;

          boolean bSalirdelrs = false;

          while (rs.next()) {

            //autoriz
            cd_nivel_1 = rs.getString("CD_NIVEL_1");
            cd_nivel_2 = rs.getString("CD_NIVEL_2");
            cd_nivel_1_ex = rs.getString("CD_NIVEL_1_EX");
            cd_nivel_2_ex = rs.getString("CD_NIVEL_2_EX");

            // # lista de Salida  ----------------------------------
            bSalirdelrs = false;
            int iAut = autorizaciones(param, cd_nivel_1, cd_nivel_2,
                                      cd_nivel_1_ex, cd_nivel_2_ex);

            if (iAut == -1 || iAut == 2) {
              bSalirdelrs = true; //no autorizado o error
            }
            else if (iAut == 0) {
              modo = modoMODIFICACION;
            }
            else if (iAut == 1) {
              modo = modoCONSULTA;
              //# autorizaciones    ----------------------------------

              //trama
            }
            if (iTramar > DBServlet.maxSIZE) {
              listaSalida.setState(CLista.listaINCOMPLETA);
              listaSalida.setFilter( ( (DataBusqueda) listaSalida.lastElement()).
                                    getNM_ALERBRO());
              break;
            }
            if (listaSalida.getState() == CLista.listaVACIA) {
              listaSalida.setState(CLista.listaLLENA);
            }
            //--------

            if (!bSalirdelrs) {

              //nulos
              cd_e_notif = rs.getString("CD_E_NOTIF");
              it_validada = rs.getString("IT_VALIDADA");
              if (cd_nivel_2 == null) {
                cd_nivel_2 = "";
              }
              if (cd_e_notif == null) {
                cd_e_notif = "";
              }
              if (it_validada == null) {
                it_validada = ""; //no procede
                //enteros
              }
              iN = rs.getInt("NM_ALERBRO");
              IN = new Integer(iN);
              sN = IN.toString();

              //fechahora
              fecRecu = rs.getTimestamp("FC_FECHAHORA");
              sfecRecu = timestamp_a_cadena(fecRecu);

              dataSalida = new DataBusqueda(rs.getString("CD_ANO"),
                                            sN,
                                            it_validada,
                                            rs.getString("DS_ALERTA"),
                                            rs.getString("CD_GRUPO"),
                                            "",
                                            rs.getString("CD_SITALERBRO"),
                                            cd_nivel_1, "",
                                            cd_nivel_2, "",
                                            cd_e_notif, "", "", "",
                                            sfecRecu, "", "",
                                            formater.format(rs.getDate(
                  "FC_ALERBRO")),
                                            modo, null, null);

              listaSalida.addElement(dataSalida);
              //tramar
              iTramar++;
              //----
            }

          } //salir de rs

          rs.close();
          rs = null;
          st.close();
          st = null;

          //la descripci�n grupo la cogemos de la combo de pantalla

          /*//descripcion de equipo
                  for(int j = 0; j < listaSalida.size(); j++){
           dataSalida = (DataBusqueda)listaSalida.elementAt(j);
           if (!dataSalida.getCD_E_NOTIF().equals("")){
             sQuery = "SELECT DS_E_NOTIF  "+
             " FROM SIVE_E_NOTIF "+
             " WHERE CD_E_NOTIF = ?";
             st = con.prepareStatement(sQuery);
             st.setString(1, dataSalida.getCD_E_NOTIF().trim());
             rs = st.executeQuery();
             while (rs.next()) {
               dataSalida.DS_E_NOTIF = rs.getString("DS_E_NOTIF");
             }
             rs.close();
             rs = null;
             st.close();
             st = null;
           }
                  }  */

          break;

      } //fin switch

      //valida la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return listaSalida;

  } //fin doWork

  protected void Preparar_rsget(int iValor,
                                DataBusqueda data,
                                PreparedStatement st,
                                CLista entrada,
                                int modo) throws Exception {

    st.setString(iValor, data.getCD_ANO().trim());
    iValor++;

    if (data.getNM_ALERBRO().trim().length() > 0) {
      st.setInt(iValor, (new Integer(data.getNM_ALERBRO().trim())).intValue());
      iValor++;
      return;
    }

    //trama
    if (entrada.getFilter().length() > 0) {

      st.setInt(iValor, (new Integer(entrada.getFilter().trim())).intValue());
      iValor++;
    } //----------------------

    if (data.getDS_ALERTA().trim().length() > 0) {
      st.setString(iValor, data.getDS_ALERTA().trim() + "%");
      iValor++;
    }
    if (data.getCD_GRUPO().trim().length() > 0) {
      st.setString(iValor, data.getCD_GRUPO().trim());
      iValor++;
    }
    if (data.getCD_SITALERBRO().trim().length() > 0) {
      st.setString(iValor, data.getCD_SITALERBRO().trim());
      iValor++;
    }
    if (data.getCD_NIVEL_1().trim().length() > 0) {
      st.setString(iValor, data.getCD_NIVEL_1().trim());
      iValor++;
      st.setString(iValor, data.getCD_NIVEL_1().trim());
      iValor++;
    }
    if (data.getCD_NIVEL_2().trim().length() > 0) {
      st.setString(iValor, data.getCD_NIVEL_2().trim());
      iValor++;
      st.setString(iValor, data.getCD_NIVEL_2().trim());
      iValor++;
    }
    if (data.getCD_E_NOTIF().trim().length() > 0) {
      st.setString(iValor, data.getCD_E_NOTIF().trim());
      iValor++;
    }
    if (data.getIT_VALIDADA().trim().length() > 0) {
      st.setString(iValor, data.getIT_VALIDADA().trim());
      iValor++;
    }
    java.sql.Timestamp fec = null;
    String sFec = "";
    //convertir a timestamp  $$
    if (data.getFC_FECHAHORA_DESDE().trim().length() > 0) {
      sFec = data.getFC_FECHAHORA_DESDE().trim() + " 00:00:00";
      fec = (java.sql.Timestamp) cadena_a_timestamp(sFec);
      st.setTimestamp(iValor, fec);
      iValor++;
    }
    if (data.getFC_FECHAHORA_HASTA().trim().length() > 0) {
      sFec = data.getFC_FECHAHORA_HASTA().trim() + " 00:00:00";
      fec = (java.sql.Timestamp) cadena_a_timestamp(sFec);
      st.setTimestamp(iValor, fec);
      iValor++;
    }

    //SOLO ALERTAS
    st.setString(iValor, "0");
    iValor++;
    st.setString(iValor, "1");
    iValor++;

  } //fin preparar

  protected String Preparar_Query(DataBusqueda data,
                                  CLista entrada, int modo) {

    String sQ = "WHERE CD_ANO = ? ";

    if (data.getNM_ALERBRO().trim().length() > 0) {
      sQ = sQ + " AND NM_ALERBRO = ? ";
      return (sQ);
    }

    //trama
    if (entrada.getFilter().length() > 0) {
      sQ = sQ + " AND NM_ALERBRO < ? ";
    } //----------------------

    if (data.getDS_ALERTA().trim().length() > 0) {
      sQ = sQ + " AND DS_ALERTA like ? ";
    }
    if (data.getCD_GRUPO().trim().length() > 0) {
      sQ = sQ + " AND CD_GRUPO = ? ";
    }
    if (data.getCD_SITALERBRO().trim().length() > 0) {
      sQ = sQ + " AND CD_SITALERBRO = ? ";
    }
    if (data.getCD_NIVEL_1().trim().length() > 0) {
      sQ = sQ + " AND (CD_NIVEL_1 = ? OR CD_NIVEL_1_EX = ?) ";
    }
    if (data.getCD_NIVEL_2().trim().length() > 0) {
      sQ = sQ + " AND (CD_NIVEL_2 = ? OR CD_NIVEL_2_EX = ?) ";
    }
    if (data.getCD_E_NOTIF().trim().length() > 0) {
      sQ = sQ + " AND CD_E_NOTIF = ? ";
    }
    if (data.getIT_VALIDADA().trim().length() > 0) {
      sQ = sQ + " AND IT_VALIDADA = ? ";
    }
    if (data.getFC_FECHAHORA_DESDE().trim().length() > 0) {
      sQ = sQ + " AND FC_FECHAHORA >= ? ";
    }
    if (data.getFC_FECHAHORA_HASTA().trim().length() > 0) {
      sQ = sQ + " AND FC_FECHAHORA <= ? ";
    }

    sQ = sQ + " AND  CD_SITALERBRO IN (?, ?) ";

    return (sQ);
  } //fin preparar

  //return: 0:modoMODIFICAR
  //        1:modoCONSULTA
  //        2:modoNADA (no esta autorizado)
  //        -1:ERROR
  /** Decidimos el 071099 solo: modif o no autorizado **/
  public int autorizaciones(CLista listaentrada,
                            String n1_normal, String n2_normal,
                            String n1_ex, String n2_ex) {
    try {

      DataBusqueda datosentrada = (DataBusqueda) listaentrada.firstElement();

      Vector vN1 = (Vector) datosentrada.getVN1();
      Vector vN2 = (Vector) datosentrada.getVN2();

      Integer IPerfil = new Integer(listaentrada.getPerfil());
      String sPerfil = IPerfil.toString();

      String N1 = n1_normal;
      String N2 = n2_normal;
      if (N2 == null) {
        N2 = "";
      }
      String N1_EX = n1_ex;
      String N2_EX = n2_ex;
      if (N2_EX == null) {
        N2_EX = "";

        //Epidemiologo N1: autorizado a vector
      }
      if (sPerfil.equals("3")) {

        //normal: pertenece al conjunto de autoriz, lo modif independiente de ex
        for (int i = 0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if (n1.trim().equals(N1)) {
            return (0);
          }
        } //for
        //-------- normal -----

        //Si llega hasta aqui, puede que pueda modificarla
        //expuestos: si pertenece al conjunto de autoriz: 1
        for (int i = 0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if (n1.trim().equals(N1_EX)) {
            return (0); //si consultarla 1, si modif 0
          }
        } //for
        //-------- expuestos -----

        //si llega hasta aqui, es que no se ha encontrado ni uno ni otro:
        return (2); //NO AUTORIZADO.
      } //fin Epidemiologo N1

      //Epidemiologo N2: autorizado a vectores
      if (sPerfil.equals("4")) {
        //normal -----------------------------------------------------
        //area, distrito  parejas (deben ser de igual tama�o)
        if (!N1.equals("") && !N2.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            String n2 = (String) vN2.elementAt(i);
            if (n1.trim().equals(N1)) {
              if (n2.trim().equals(N2)) {
                return (0);
              }
            }
          }
        }
        //caso area   parejas (1,x   pertenece a 1,3  1,0  2,1)
        if (!N1.equals("") && N2.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            if (n1.trim().equals(N1)) {
              return (0);
            }
          }
        }
        //-----------------------------------------------------
        //Si llega hasta aqui, puede que pueda modificarla
        //expuestos: se trata igual, si Si: 1
        //area, distrito  parejas (deben ser de igual tama�o)
        if (!N1_EX.equals("") && !N2_EX.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            String n2 = (String) vN2.elementAt(i);
            if (n1.trim().equals(N1_EX)) {
              if (n2.trim().equals(N2_EX)) {
                return (0); //si consultarla 1, si modif 0
              }
            }
          }
        }
        //caso area   parejas (1,x   pertenece a 1,3  1,0  2,1)
        if (!N1_EX.equals("") && N2_EX.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            if (n1.trim().equals(N1_EX)) {
              return (0); //si consultarla 1, si modif 0
            }
          }
        }
        //expuestos----------------------------------------

        //si llega hasta aqui, es que no se ha encontrado ni uno ni otro:
        return (2); //NO AUTORIZADO.
      } //fin Epidemiologo N2

      //perfil 2: CA: //permiso de modificar a todo
      if (sPerfil.equals("2")) {
        return (0);
      }

      //perfil 5: solo vera n1 n2 permitidos ptt podra modificarlos
      if (sPerfil.equals("5")) {
        return (0);
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      return ( -1);
    }

    return (1);
  } //autorizaciones

} //fin de la clase
