package brotes.servidor.bisrv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Vector;

import brotes.datos.bidata.DataBusqueda;
import capp.CLista;
import sapp.DBServlet;

/** servlet para PISTA:
 autorizados  (resultado de la select),
 aquellos que tienen permisos para
 la zonificacion de la alerta,
 o para la zonificacion de expuestos,
 o para la zonificacion del colectivo afectado
     NOTA: solo contemplamos modif, o no autorizado. Nunca modoCONSULTA...de momento
 */
public class SrvBrotes
    extends DBServlet {

  final int servletTABLABROTES = 0;

  public java.sql.Timestamp fecRecu = null;
  public String sfecRecu = "";
  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    //java.sql.Timestamp TSFec = new java.sql.Timestamp(dFecha.getTime());
    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);

    return TSFec;
  }

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 0;

    // objetos de datos
    CLista listaSalida = new CLista();
    DataBusqueda dataSalida = null;
    DataBusqueda dataEntrada = null;

    //querys
    String sQuery = "";
    int iValor = 1;
    int iTramar = 1;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    //modos
    final int modoMODIFICACION = 4;
    final int modoCONSULTA = 6;

    String ds = "";
    String dsl = "";
    String sDes = "";

    try {

      switch (opmode) {
        case servletTABLABROTES:

          dataEntrada = (DataBusqueda) param.firstElement();

          sQuery = "SELECT b.CD_ANO, b.NM_ALERBRO, b.DS_BROTE, " +
              " b.CD_GRUPO, b.FC_FECHAHORA, " +
              " b.CD_NIVEL_1_LCA,   b.CD_NIVEL_2_LCA," +
              " b.CD_NIVEL_1_LE, b.CD_NIVEL_2_LE, " +
              " a.CD_NIVEL_1, a.CD_NIVEL_2, " +
              " a.CD_SITALERBRO " +
              " FROM SIVE_BROTES b,  SIVE_ALERTA_BROTES a  " +
              " WHERE b.CD_ANO = a.CD_ANO AND b.NM_ALERBRO = a.NM_ALERBRO AND ";

          sQuery = sQuery + Preparar_Query(dataEntrada, param, opmode);
          sQuery = sQuery +
              "ORDER BY b.CD_ANO DESC, b.NM_ALERBRO DESC, b.FC_FECHAHORA DESC";

          st = con.prepareStatement(sQuery);

          System.out.println("sQuery " + sQuery);

          // para el prepare (?)
          iValor = 1;
          Preparar_rsget(iValor, dataEntrada, st, param, opmode);
          rs = st.executeQuery();

          //enteros
          int iN = 0;
          Integer IN = new Integer(iN);
          String sN = IN.toString();

          //modo
          int modo = 0;

          boolean bSalirdelrs = false;

          // autorizaciones
          String cd_nivel_2 = "";
          String cd_nivel_1 = "";
          String cd_nivel_1_lca = "";
          String cd_nivel_2_lca = "";
          String cd_nivel_1_le = "";
          String cd_nivel_2_le = "";

          while (rs.next()) {

            //autoriz
            cd_nivel_1 = rs.getString("CD_NIVEL_1");
            cd_nivel_2 = rs.getString("CD_NIVEL_2");
            cd_nivel_1_lca = rs.getString("CD_NIVEL_1_LCA");
            cd_nivel_2_lca = rs.getString("CD_NIVEL_2_LCA");
            cd_nivel_1_le = rs.getString("CD_NIVEL_1_LE");
            cd_nivel_2_le = rs.getString("CD_NIVEL_2_LE");

            // # lista de Salida  ----------------------------------
            bSalirdelrs = false;
            int iAut = autorizaciones(param,
                                      cd_nivel_1, cd_nivel_2,
                                      cd_nivel_1_lca, cd_nivel_2_lca,
                                      cd_nivel_1_le, cd_nivel_2_le);

            if (iAut == -1 || iAut == 2) {
              bSalirdelrs = true; //no autorizado o error
            }
            else if (iAut == 0) {
              modo = modoMODIFICACION;
            }
            else if (iAut == 1) {
              modo = modoCONSULTA;
              //# autorizaciones    ----------------------------------

              //trama
            }
            if (iTramar > DBServlet.maxSIZE) {
              listaSalida.setState(CLista.listaINCOMPLETA);
              listaSalida.setFilter( ( (DataBusqueda) listaSalida.lastElement()).
                                    getNM_ALERBRO());
              break;
            }
            if (listaSalida.getState() == CLista.listaVACIA) {
              listaSalida.setState(CLista.listaLLENA);
            }
            //--------

            if (!bSalirdelrs) {

              //nulos
              if (cd_nivel_2 == null) {
                cd_nivel_2 = "";

              }
              if (cd_nivel_1_lca == null) {
                cd_nivel_1_lca = "";
              }
              if (cd_nivel_2_lca == null) {
                cd_nivel_2_lca = "";

              }
              if (cd_nivel_1_le == null) {
                cd_nivel_1_le = "";
              }
              if (cd_nivel_2_le == null) {
                cd_nivel_2_le = "";

                //enteros
              }
              iN = rs.getInt("NM_ALERBRO");
              IN = new Integer(iN);
              sN = IN.toString();

              //fechahora
              fecRecu = rs.getTimestamp("FC_FECHAHORA");
              sfecRecu = timestamp_a_cadena(fecRecu);

              dataSalida = new DataBusqueda(rs.getString("CD_ANO"),
                                            sN,
                                            "",
                                            rs.getString("DS_BROTE"),
                                            rs.getString("CD_GRUPO"),
                                            "",
                                            rs.getString("CD_SITALERBRO"),
                                            "", "",
                                            "", "",
                                            "", "", "", "",
                                            sfecRecu, "", "",
                                            "",
                                            modo, null, null);

              listaSalida.addElement(dataSalida);
              //tramar
              iTramar++;
              //----
            }

          } //salir de rs

          rs.close();
          rs = null;
          st.close();
          st = null;

          break;

      } //fin switch

      //valida la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return listaSalida;

  } //fin doWork

  protected void Preparar_rsget(int iValor,
                                DataBusqueda data,
                                PreparedStatement st,
                                CLista entrada,
                                int modo) throws Exception {

    st.setString(iValor, data.getCD_ANO().trim());
    iValor++;

    if (data.getNM_ALERBRO().trim().length() > 0) {
      st.setInt(iValor, (new Integer(data.getNM_ALERBRO().trim())).intValue());
      iValor++;
      return;
    }

    //trama
    if (entrada.getFilter().length() > 0) {

      st.setInt(iValor, (new Integer(entrada.getFilter().trim())).intValue());
      iValor++;
    } //----------------------

    if (data.getDS_ALERTA().trim().length() > 0) {
      st.setString(iValor, data.getDS_ALERTA().trim() + "%");
      iValor++;
    }
    if (data.getCD_GRUPO().trim().length() > 0) {
      st.setString(iValor, data.getCD_GRUPO().trim());
      iValor++;
    }
    if (data.getCD_SITALERBRO().trim().length() > 0) {
      st.setString(iValor, data.getCD_SITALERBRO().trim());
      iValor++;
    }
    java.sql.Timestamp fec = null;
    String sFec = "";
    //convertir a timestamp  $$
    if (data.getFC_FECHAHORA_DESDE().trim().length() > 0) {
      sFec = data.getFC_FECHAHORA_DESDE().trim() + " 00:00:00";
      fec = (java.sql.Timestamp) cadena_a_timestamp(sFec);
      st.setTimestamp(iValor, fec);
      iValor++;
    }
    if (data.getFC_FECHAHORA_HASTA().trim().length() > 0) {
      sFec = data.getFC_FECHAHORA_HASTA().trim() + " 00:00:00";
      fec = (java.sql.Timestamp) cadena_a_timestamp(sFec);
      st.setTimestamp(iValor, fec);
      iValor++;
    }

    st.setString(iValor, "3");
    iValor++;
    st.setString(iValor, "4");
    iValor++;

  } //fin preparar

  protected String Preparar_Query(DataBusqueda data,
                                  CLista entrada, int modo) {

    String sQ = " b.CD_ANO = ? ";

    if (data.getNM_ALERBRO().trim().length() > 0) {
      sQ = sQ + " AND b.NM_ALERBRO = ? ";
      return (sQ);
    }

    //trama
    if (entrada.getFilter().length() > 0) {
      sQ = sQ + " AND b.NM_ALERBRO < ? ";
    } //----------------------

    if (data.getDS_ALERTA().trim().length() > 0) {
      sQ = sQ + " AND b.DS_ALERTA like ? ";
    }
    if (data.getCD_GRUPO().trim().length() > 0) {
      sQ = sQ + " AND b.CD_GRUPO = ? ";
    }
    if (data.getCD_SITALERBRO().trim().length() > 0) {
      sQ = sQ + " AND a.CD_SITALERBRO = ? ";
    }
    if (data.getFC_FECHAHORA_DESDE().trim().length() > 0) {
      sQ = sQ + " AND b.FC_FECHAHORA >= ? ";
    }
    if (data.getFC_FECHAHORA_HASTA().trim().length() > 0) {
      sQ = sQ + " AND b.FC_FECHAHORA <= ? ";
    }

    sQ = sQ + " AND a.CD_SITALERBRO IN (?, ?) ";

    return (sQ);
  } //fin preparar

  //return: 0:modoMODIFICAR
  //        1:modoCONSULTA
  //        2:modoNADA (no esta autorizado)
  //        -1:ERROR
  public int autorizaciones(CLista listaentrada,
                            String n1_alerta, String n2_alerta,
                            String n1_lca, String n2_lca,
                            String n1_le, String n2_le) {
    try {

      DataBusqueda datosentrada = (DataBusqueda) listaentrada.firstElement();

      Vector vN1 = (Vector) datosentrada.getVN1();
      Vector vN2 = (Vector) datosentrada.getVN2();

      Integer IPerfil = new Integer(listaentrada.getPerfil());
      String sPerfil = IPerfil.toString();

      String N1 = n1_alerta;
      String N2 = n2_alerta;
      if (N2 == null) {
        N2 = "";

      }
      String N1_LE = n1_le;
      String N2_LE = n2_le;
      if (N1_LE == null) {
        N1_LE = "";
      }
      if (N2_LE == null) {
        N2_LE = "";

      }
      String N1_LCA = n1_lca;
      String N2_LCA = n2_lca;
      if (N1_LCA == null) {
        N1_LCA = "";
      }
      if (N2_LCA == null) {
        N2_LCA = "";

        //Epidemiologo N1: autorizado a vector
      }
      if (sPerfil.equals("3")) {

        //normal: si pertenece al conjunto de autoriz,
        //lo modif independiente de le y lca
        for (int i = 0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if (n1.trim().equals(N1)) {
            return (0);
          }
        } //for
        //-------- normal -----

        //Si llega hasta aqui, puede que pueda modificarla por
        //l expuestos: si pertenece al conjunto de autoriz:
        for (int i = 0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if (n1.trim().equals(N1_LE)) {
            return (0);
          }
        } //for
        //-------- expuestos -----

        //Si llega hasta aqui, puede que pueda modificarla por
        //localizacion: si pertenece al conjunto de autoriz:
        for (int i = 0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if (n1.trim().equals(N1_LCA)) {
            return (0);
          }
        } //for
        //-------- localizacion ------

        //si llega hasta aqui, es que no se ha encontrado ni uno ni otro:
        return (2); //NO AUTORIZADO.
      } //fin Epidemiologo N1

      //Epidemiologo N2: autorizado a vectores
      if (sPerfil.equals("4")) {
        //normal -----------------------------------------------------
        //area, distrito  parejas (deben ser de igual tama�o)
        if (!N1.equals("") && !N2.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            String n2 = (String) vN2.elementAt(i);
            if (n1.trim().equals(N1)) {
              if (n2.trim().equals(N2)) {
                return (0);
              }
            }
          }
        }
        //caso area   parejas (1,x   pertenece a 1,3  1,0  2,1)
        if (!N1.equals("") && N2.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            if (n1.trim().equals(N1)) {
              return (0);
            }
          }
        }
        //-----------------------------------------------------

        //Si llega hasta aqui, puede que pueda modificar
        //l expuestos: se trata igual, si Si: 1
        //area, distrito  parejas (deben ser de igual tama�o)
        if (!N1_LE.equals("") && !N2_LE.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            String n2 = (String) vN2.elementAt(i);
            if (n1.trim().equals(N1_LE)) {
              if (n2.trim().equals(N2_LE)) {
                return (0);
              }
            }
          }
        }
        //caso area   parejas (1,x   pertenece a 1,3  1,0  2,1)
        if (!N1_LE.equals("") && N2_LE.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            if (n1.trim().equals(N1_LE)) {
              return (0);
            }
          }
        }
        //expuestos----------------------------------------

        //Si llega hasta aqui, puede que pueda modificar
        //l ocalizacion: se trata igual, si Si: 1
        //area, distrito  parejas (deben ser de igual tama�o)
        if (!N1_LCA.equals("") && !N2_LCA.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            String n2 = (String) vN2.elementAt(i);
            if (n1.trim().equals(N1_LCA)) {
              if (n2.trim().equals(N2_LCA)) {
                return (0);
              }
            }
          }
        }
        //caso area   parejas (1,x   pertenece a 1,3  1,0  2,1)
        if (!N1_LCA.equals("") && N2_LCA.equals("")) {
          for (int i = 0; i < vN1.size(); i++) {
            String n1 = (String) vN1.elementAt(i);
            if (n1.trim().equals(N1_LCA)) {
              return (0);
            }
          }
        }
        //LOCALIZACION----------------------------------------
        //si llega hasta aqui, es que no se ha encontrado ni uno ni otro:
        return (2); //NO AUTORIZADO.
      } //fin Epidemiologo N2

      //perfil 2: CA: //permiso de modificar a todo
      if (sPerfil.equals("2")) {
        return (0);
      }

      //perfil 5: solo vera n1 n2 permitidos ptt podra modificarlos
      //En Informes de Brotes, ni siquiera entrara.
      if (sPerfil.equals("5")) {
        return (0);
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      return ( -1);
    }

    return (2);
  } //autorizaciones

} //fin de la clase
