package brotes.servidor.factriesgo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;

//import gesvac.cliente.sipsec.sipincsec;

/**
 * Servlet con el que se realiza el mantenimiento de la
 * tasa de ataque por factor de riesgo
 * Se actualizan las siguientes tablas :
 *  SIVE_FACTOR_RIESGO, SIVE_VALOR_FACRI
 *
 * El par�metro de entrada del servlet es una Lista que contiene
 * una Lista con tantos Datas como l�neas de detalle que hayan sido altas, bajas o
 * modificaciones
     * Cada uno de estos Datas tienen una clave denominada "IT_OPERACION" que indica
 * la operaci�n qu se debe realizar: alta "A", modificaci�n "M y
 * baja "B".
 *
 * @autor JLT
 * @version 1.0
 */
public class SrvGestFactR
    extends DBServlet {

  final protected int servletACT_TASA_ACTUALIZACION = 10003;

  // control de error
  boolean bError = false;
  String sMsg = null;

  // objetos JDBC
  PreparedStatement st = null;
  ResultSet rs = null;

  // vector con el resultado
  Lista snapShot = new Lista();
  Lista listaDet = new Lista();

  //devolucion
  Lista lisDevolucion = null;
  Data dtCab = null;
  Data dtDet = null;
  Data dtDetalle = null;

  //incremento del secuenciador en el alta
//    sipincsec altaDev = new sipincsec();
//    Data dtSec = new Data();

  // buffers
//    int i = 1;
  int counter = 1;
  String sQuery = null;
  String sWhere = null;

//    String operacion = "";
  String operDetalle = "";
  int secuencial = -1;
  String parametro = "";

  String fecha = null;

  // querys fijas

  //altas
  final String sALTA_FACTOR_RIESGO =
      " insert into SIVE_FACTOR_RIESGO  "
      + "(NM_FACRI, DS_FACRI, CD_ANO, "
      + " NM_ALERBRO, CD_TSIVE, CD_PREGUNTA)"
      + " values (?,?,?,?,?,?)";

  final String sALTA_VALOR_FACRI =
      " insert into SIVE_VALOR_FACRI  "
      + "( NM_FACRI, DS_VFACRI, NM_EXPENF, "
      + " NM_EXPNOENF, NM_NOEXPENF,  NM_NMNOEXPNOENF) "
      + " values (?,?,?,?,?,?)";

  //modificaciones
  final String sMOD_FACTOR_RIESGO =
      " update SIVE_FACTOR_RIESGO  "
      + " set DS_FACRI = ? "
      + " where NM_FACRI = ? ";

  final String sMOD_VALOR_FACRI =
      " update SIVE_VALOR_FACRI  "
      + " set DS_VFACRI = ? , NM_EXPENF = ? , "
      + " NM_EXPNOENF = ? , NM_NOEXPENF=? , NM_NMNOEXPNOENF = ? "
      + " where NM_FACRI = ? ";

  //bajas
  final String sBAJA_FACTOR_RIESGO =
      " delete from SIVE_FACTOR_RIESGO  "
      + " where NM_FACRI = ? ";

  final String sBAJA_VALOR_FACRI =
      " delete from SIVE_VALOR_FACRI  "
      + " where NM_FACRI = ? ";

  // seleccion del secuencial nm_facri
  final String sSEL_SEC_GENERAL =
      " select NM_FACRI from SIVE_SEC_GENERAL ";

  //modificacion del secuencial
  final String sMOD_SEC_GENERAL =
      "update SIVE_SEC_GENERAL set NM_FACRI = ? ";

  // actualizaci�n de cd_ope y fc_ultact de sive_brotes
  final String sMOD_BROTES =
      "update SIVE_BROTES set CD_OPE = ? , FC_ULTACT = ? "
      + " where  CD_ANO = ? and NM_ALERBRO = ? ";

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {

    try {

      con.setAutoCommit(false);
      lisDevolucion = (Lista) vParametros.elementAt(0);
//      dtCab = (Data) vParametros.elementAt(0);
      parametro = vParametros.getParameter("CD_OPE");
//      trazaLog("modooperacion" + opmode);
//      trazaLog("dtCab" + dtCab);
      trazaLog("lisDevolucion" + lisDevolucion);

      switch (opmode) {
        case servletACT_TASA_ACTUALIZACION:

          // alta de factor_riesgo y valor_facri
          for (int i = 0; i < lisDevolucion.size(); i++) {
            //actualizaciones
            dtDet = new Data();
            dtDet = (Data) lisDevolucion.elementAt(i);
            operDetalle = (String) dtDet.get("IT_OPERACION");

            if (operDetalle.equals("A")) {
              altaFactor();
              trazaLog("altafactor");

              altaValor();
              trazaLog("altavalor");

//                updateAct();

            }
            else if (operDetalle.equals("M")) {

//                actualizarFactor();
//                trazaLog("actualizarfactor");

              actualizarValor();
              trazaLog("actualizarvalor");

//                updateAct();

            }
            else if (operDetalle.equals("B")) {

              bajaValor();
              trazaLog("bajavalor");

              bajaFactor();
              trazaLog("bajafactor");

//                updateAct();

            }

          } //for para actualizar

          // actualizo cd_ope y fc_ultact de sive_brotes
          trazaLog("llego a updateact");
          updateAct();
          trazaLog("paso a updateact");

          break;

      } // cierro el switch

    }
    catch (SQLException e1) {

      // traza el error
      trazaLog(e1);
      bError = true;
      sMsg = "Error al leer los registros";
      con.rollback();
      trazaLog("hago rollback");

    }
    catch (Exception e2) {
      // traza el error
      trazaLog(e2);
      bError = true;

      // selecciona el mensaje de error
      sMsg = "Error inesperado";
      con.rollback();
    }

    // devuelve el error recogido
    if (bError) {
      throw new Exception(sMsg);
    }

    // valida la transacci�n
    trazaLog("llego al commit");
    con.commit();

    //para devolver el numero de la peticion en caso de alta
    snapShot = null;
    snapShot = new Lista();
    snapShot.addElement(dtCab);
    trazaLog("snapShot" + snapShot);
    return snapShot;

  } // cierro el doWork

  // metodos que implementan las actualizaciones
  // sobre la base de datos

  public void altaFactor() throws Exception {

    secuencial = -1;
    // antes de hacer el alta calculamos nm_secuen
    st = con.prepareStatement(sSEL_SEC_GENERAL);
    // completa los valores de la cl�usula WHERE
    rs = st.executeQuery();
    if (rs.next()) {
      secuencial = rs.getInt("NM_FACRI") + 1;
    }
    else {
      secuencial = 1;
    }

    trazaLog("secuencial" + secuencial);
    // libero recursos
    rs.close();
    rs = null;
    st.close();
    st = null;

    // prepara la query  para actualizar sec_general
    st = con.prepareStatement(sMOD_SEC_GENERAL);
    // completa los valores

    st.setInt(1, secuencial);

    // ejecuta la query
    st.executeUpdate();
    //cierra los recursos
    st.close();
    st = null;

    // prepara la query
    st = con.prepareStatement(sALTA_FACTOR_RIESGO);
    // completa los valores de la cl�usula WHERE
    st.setInt(1, secuencial);
    st.setString(2, (String) dtDet.getString("DS_FACRI"));
    st.setString(3, (String) dtDet.getString("CD_ANO"));
    st.setInt(4,
              (new Integer( (String) dtDet.getString("NM_ALERBRO"))).intValue());
    st.setNull(5, Types.VARCHAR);
    st.setNull(6, Types.VARCHAR);
    trazaLog("execute update");

    // ejecuta la query
    st.executeUpdate();
    // cierra los recursos
    st.close();
    st = null;

  }

  public void altaValor() throws Exception {

    // prepara la query
    st = con.prepareStatement(sALTA_VALOR_FACRI);
    // completa los valores de la cl�usula WHERE
    st.setInt(1, secuencial);
    st.setString(2, (String) dtDet.getString("DS_VFACRI"));
    st.setInt(3, (new Integer( (String) dtDet.getString("NM_EXPENF"))).intValue());
    st.setInt(4,
              (new Integer( (String) dtDet.getString("NM_EXPNOENF"))).intValue());
    st.setInt(5,
              (new Integer( (String) dtDet.getString("NM_NOEXPENF"))).intValue());
    st.setInt(6,
              (new Integer( (String) dtDet.getString("NM_NMNOEXPNOENF"))).intValue());

    // ejecuta la query
    st.executeUpdate();
    // cierra los recursos
    st.close();
    st = null;

  }

  public void bajaFactor() throws Exception {
    // prepara la query
    st = con.prepareStatement(sBAJA_FACTOR_RIESGO);
    // completa los valores de la cl�usula WHERE
    st.setInt(1, (new Integer( (String) dtDet.getString("NM_FACRI"))).intValue());

    //ejecuta la query
    st.executeUpdate();
    // cierra los recursos
    st.close();
    st = null;

  }

  public void bajaValor() throws Exception {
    // prepara la query
    st = con.prepareStatement(sBAJA_VALOR_FACRI);
    // completa los valores de la cl�usula WHERE
    st.setInt(1, (new Integer( (String) dtDet.getString("NM_FACRI"))).intValue());

    //ejecuta la query
    st.executeUpdate();
    // cierra los recursos
    st.close();
    st = null;

  }

  public void actualizarFactor() throws Exception {

    // prepara la query
    st = con.prepareStatement(sMOD_FACTOR_RIESGO);
    // completa los valores

    st.setString(1, (String) dtDet.getString("DS_FACRI"));
    st.setInt(2, (new Integer( (String) dtDet.getString("NM_FACRI"))).intValue());

    // ejecuta la query
    st.executeUpdate();
    //cierra los recursos
    st.close();
    st = null;

  }

  public void actualizarValor() throws Exception {

    // prepara la query
    st = con.prepareStatement(sMOD_VALOR_FACRI);

    st.setString(1, (String) dtDet.getString("DS_VFACRI"));
    st.setInt(2, (new Integer( (String) dtDet.getString("NM_EXPENF"))).intValue());
    st.setInt(3,
              (new Integer( (String) dtDet.getString("NM_EXPNOENF"))).intValue());
    st.setInt(4,
              (new Integer( (String) dtDet.getString("NM_NOEXPENF"))).intValue());
    st.setInt(5,
              (new Integer( (String) dtDet.getString("NM_NMNOEXPNOENF"))).intValue());
    st.setInt(6, (new Integer( (String) dtDet.getString("NM_FACRI"))).intValue());

    // ejecuta la query
    st.executeUpdate();
    //cierra los recursos
    st.close();
    st = null;

  }

  public void updateAct() throws Exception {
    // prepara la query
    st = con.prepareStatement(sMOD_BROTES);

    fecha = Format.fechaHoraActual();
    trazaLog("fecha" + fecha);
    trazaLog("parametro" + parametro);
    // completa los valores
    st.setString(1, parametro);
    st.setTimestamp(2, Format.string2Timestamp(fecha));
    st.setString(3, (String) dtDet.getString("CD_ANO"));
    st.setInt(4,
              (new Integer( (String) dtDet.getString("NM_ALERBRO"))).intValue());

    //devuelvo estos datos
    dtCab = new Data();
    dtCab.put("CD_OPE", parametro);
    dtCab.put("FC_ULTACT", fecha);

    //ejecuta la query
    st.executeUpdate();
    // cierra los recursos
    st.close();
    st = null;

  }

// }

  public static void main(String args[]) {
    SrvGestFactR srv = new SrvGestFactR();
    Lista vParam = new Lista();
    Lista lisPet = new Lista();
    Lista lisDet = new Lista();
    Data dtCab = new Data();
    Data dtDet = new Data();

    // par�metros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");

    vParam.setParameter("CD_OPE", "02");

    // detalle

    dtDet.put("CD_ANO", "2000");
    dtDet.put("NM_ALERBRO", "3");
    dtDet.put("DS_FACRI", "FACTOR1");
    dtDet.put("DS_VFACRI", "VALOR2");
    dtDet.put("NM_EXPENF", "25");
    dtDet.put("NM_EXPNOENF", "4"); //45+2-2+5
    dtDet.put("NM_NOEXPENF", "3");
    dtDet.put("NM_NMNOEXPNOENF", "32");
    dtDet.put("IT_OPERACION", "M");
//    dtDet.put("NM_FACRI", "4");

    lisDet.addElement(dtDet);

    vParam.addElement(lisDet);
    srv.doDebug(10003, vParam);

    srv = null;
  }

} // fin clase
