/**
 * Clase: SrvLMed
 * Paquete: brotes.servidor.diamed
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 20/12/1999
 * Analisis Funcional: Punto 2.2 Mantenimiento Informe Final del Brote.
 * Descripcion: Obtiene el subconjunto de medidas que
 *   verifican una cierta condicion. 4 modos de
 *   funcionamiento: busqueda exacta (codigo), busqueda
 *   exacta (descripcion), busqueda aproximada (codigo),
 *   busqueda aproximada (descripcion).
 */

package brotes.servidor.diamed;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import brotes.datos.diamed.DatCDDSMed;
import capp.CLista;
import comun.constantes;
import sapp.DBServlet;

public class SrvLMed
    extends DBServlet {

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Configuraci�n
    String sQuery = "";

    // Objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int iValor = 1;

    // Objetos de datos
    CLista listaSalida = new CLista();
    DatCDDSMed ent = null;
    DatCDDSMed dataSalida = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {
      switch (opmode) {
        case constantes.sOBTENER_X_CODIGO:
        case constantes.sOBTENER_X_DESCRIPCION:

          // Parametro de busqueda
          ent = (DatCDDSMed) param.firstElement();

          // Query
          if (opmode == constantes.sOBTENER_X_CODIGO) {
            sQuery = "select CD_GRUPO, CD_MEDIDA, DS_MEDIDA, " +
                "IT_INFADIC, IT_REPE " +
                "from SIVE_MEDIDAS " +
                "where CD_GRUPO = ? AND CD_MEDIDA = ?";
          }
          else {
            sQuery = "select CD_GRUPO, CD_MEDIDA, DS_MEDIDA, " +
                "IT_INFADIC, IT_REPE " +
                "from SIVE_MEDIDAS " +
                "where CD_GRUPO = ? AND DS_MEDIDA = ?";

            // Preparacion del statement
          }
          st = con.prepareStatement(sQuery);

          // Instanciacion del CD_GRUPO y CD_MEDIDA (Cod o Desc)
          st.setString(iValor++, ent.getCD_GRUPO().trim());
          st.setString(iValor++, ent.getCD_MEDIDA().trim());

          break;

          //TRAMAR ****
        case constantes.sSELECCION_X_CODIGO:
        case constantes.sSELECCION_X_DESCRIPCION:

          ent = (DatCDDSMed) param.firstElement();

          // Query
          if (opmode == constantes.sSELECCION_X_CODIGO) {
            sQuery = "select CD_GRUPO, CD_MEDIDA, DS_MEDIDA, " +
                "IT_INFADIC, IT_REPE " +
                "from SIVE_MEDIDAS " +
                "where CD_GRUPO = ? AND CD_MEDIDA like ?";

            // Tramado
            if (param.getFilter().length() > 0) {
              sQuery = sQuery + " and CD_MEDIDA > ?";
            }
          }
          else {
            sQuery = "select CD_GRUPO, CD_MEDIDA, DS_MEDIDA, " +
                "IT_INFADIC, IT_REPE " +
                "from SIVE_MEDIDAS " +
                "where CD_GRUPO = ? AND DS_MEDIDA like ?";

            // Tramado
            if (param.getFilter().length() > 0) {
              sQuery = sQuery + " and DS_MEDIDA > ?";
            }
          }

          // Ordenacion
          if (opmode == constantes.sSELECCION_X_CODIGO) {
            sQuery = sQuery + " order by CD_MEDIDA";
          }
          else {
            sQuery = sQuery + " order by DS_MEDIDA";

            // Preparacion del statement
          }
          st = con.prepareStatement(sQuery);

          // Instanciacion del CD_GRUPO y CD_MEDIDA (Cod o Desc)
          st.setString(iValor++, ent.getCD_GRUPO().trim());
          st.setString(iValor++, ent.getCD_MEDIDA().trim() + "%");

          // trama
          if (param.getFilter().length() > 0) {
            st.setString(iValor++, param.getFilter().trim());

          }
          break;
      } // Fin switch

      // Ejecucion de la query
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido
      while (rs.next()) {

        // Tramado
        if ( (opmode == constantes.sSELECCION_X_CODIGO) ||
            (opmode == constantes.sSELECCION_X_DESCRIPCION)) {

          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            listaSalida.setState(CLista.listaINCOMPLETA);

            if (opmode == constantes.sSELECCION_X_CODIGO) {
              listaSalida.setFilter( ( (DatCDDSMed) listaSalida.lastElement()).
                                    getCD_MEDIDA());
            }
            else {
              listaSalida.setFilter( ( (DatCDDSMed) listaSalida.lastElement()).
                                    getDS_MEDIDA());

            }
            break;
          }

          // Control de estado
          if (listaSalida.getState() == CLista.listaVACIA) {
            listaSalida.setState(CLista.listaLLENA);
          }
        } // Fin Tramado

        dataSalida = new DatCDDSMed(rs.getString("CD_GRUPO"),
                                    rs.getString("CD_MEDIDA"),
                                    rs.getString("DS_MEDIDA"),
                                    rs.getString("IT_INFADIC"),
                                    rs.getString("IT_REPE"));

        // A�ade un nodo
        listaSalida.addElement(dataSalida);

        // Siguiente registro
        i++;
      } // Fin while

      rs.close();
      rs = null;
      st.close();
      st = null;

      // Valida la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // cierra la conexi�n y acaba el procedimiento doWork
    closeConnection(con);
    listaSalida.trimToSize();

    return listaSalida;
  }
} // Fin clase SrvLMed
