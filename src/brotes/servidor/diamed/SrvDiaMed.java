/**
 * Clase: SrvDiaMed
 * Paquete: brotes.servidor.diamed
 * Hereda: DBServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 20/12/1999
 * Analisis Funcional: Punto 2.2 Mantenimiento Informe Final del Brote.
 * Descripcion: Implementacion del servlet que permite dar de alta,
 *   baja, modificar una medida adoptada para un brote.
 */

package brotes.servidor.diamed;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import brotes.datos.panmed.DatMedCS;
import brotes.datos.panmed.DatMedSC;
import capp.CLista;
// SOLO DESARROLLO
//import jdbcpool.*;
import comun.Fechas;
import comun.constantes;
import sapp.DBServlet;

public class SrvDiaMed
    extends DBServlet {

  // SOLODESARROLLO
  /*public CLista doDebug(int opmode, CLista param) throws Exception {
      jdcdriver = new JDCConnectionDriver ("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
      return doWork(opmode, param);
     }*/

  // Funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    CLista listaSalida = new CLista();
    DatMedCS brote = null;
    DatMedSC med = null;
    DatMedSC result = null;
    java.util.Date dFechaUltact = null;
    SimpleDateFormat formUltact = null;
    String sOpe;
    String sFechaUltact = null;

    // Indicador de bloqueo
    boolean bBloqueo = true;

    // Querys
    String sQuery = "";

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con = openConnection();
    con.setAutoCommit(false);

    // Datos del sintoma actual
    brote = (DatMedCS) param.elementAt(0);
    med = (DatMedSC) param.elementAt(1);

    try {

      switch (opmode) {
        case constantes.modoALTA_SB:
          bBloqueo = false;
        case constantes.modoALTA:

          // En caso de que haya que comprobar el bloqueo, si se ha
          //  modificado el brote no se da de alta la medida
          if (bBloqueo) {
            if (broteModificado(con, brote)) {
              throw (new Exception(constantes.PRE_BLOQUEO +
                                   constantes.SIVE_BROTES));
            }
          }

          // Establecimiento de la cadena de la query
          sQuery = "INSERT INTO SIVE_BROTES_MED (CD_ANO, NM_ALERBRO, " +
              "CD_GRUPO, CD_MEDIDA, NM_SECMB, DS_INFADI) VALUES (?,?,?,?,?,?)";

          // Buscamos en la tabla de secuenciadores el siguiente numero
          //  de medida. Si ya existe el Grupo+Medida y no se puede repetir
          //  no se da de alta
          int iNextMed = siguienteMedida(con, med);
          if (iNextMed == -1) {
            throw (new Exception(constantes.NO_REPE +
                                 constantes.SIVE_BROTES_MED));
          }

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciacion de la query
          instanciarInsertQuery(med, st, iNextMed);

          // Ejecucion del insert
          st.executeUpdate();

          // Cierre del PreparedStatement (el ResultSet no se usa)
          st.close();
          st = null;

          // Se modifica el CD_OPE y FC_ULTACT del brote
          dFechaUltact = new java.util.Date();
          formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                            new Locale("es", "ES"));
          sFechaUltact = formUltact.format(dFechaUltact);
          sOpe = med.getCD_OPE();
          if (bBloqueo) { //si es true es con bloqueo
            modificarBrote(con, brote, sOpe, sFechaUltact);
          }
          else { //si es false es sin bloqueo
            modificarBrote_SB(con, brote, sOpe, sFechaUltact);

            // Validacion de la transacci�n
          }
          con.commit();

          // Devolvemos la medida modificada
          result = new DatMedSC(med.getCD_ANO(), med.getNM_ALERBRO(),
                                med.getCD_GRUPO(), med.getCD_MEDIDA(),
                                med.getDS_MEDIDA(), med.getIT_INFADIC(),
                                med.getIT_REPE(), "" + iNextMed,
                                med.getDS_INFADI(),
                                sOpe, sFechaUltact);
          listaSalida.addElement(result);
          break;

        case constantes.modoMODIFICACION_SB:
          bBloqueo = false;
        case constantes.modoMODIFICACION:

          // En caso de que haya que comprobar el bloqueo, si se ha
          //  modificado el brote no se modifica la medida
          if (bBloqueo) {
            if (broteModificado(con, brote)) {
              throw (new Exception(constantes.PRE_BLOQUEO +
                                   constantes.SIVE_BROTES));
            }
          }

          // Establecimiento de la cadena de la query
          sQuery = "UPDATE SIVE_BROTES_MED " +
              "SET DS_INFADI = ? " +
              "WHERE CD_ANO = ? AND NM_ALERBRO = ? " +
              "AND CD_GRUPO = ? AND CD_MEDIDA = ? AND NM_SECMB = ?";

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciacion de la query
          instanciarUpdateQuery(med, st);

          // Ejecucion del update
          st.executeUpdate();

          // Cierre del PreparedStatement (el ResultSet no se usa)
          st.close();
          st = null;

          // Se modifica el CD_OPE y FC_ULTACT de la notificacion
          dFechaUltact = new java.util.Date();
          formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                            new Locale("es", "ES"));
          sFechaUltact = formUltact.format(dFechaUltact);
          sOpe = med.getCD_OPE();
          if (bBloqueo) { //si es true es con bloqueo
            modificarBrote(con, brote, sOpe, sFechaUltact);
          }
          else { //si es false es sin bloqueo
            modificarBrote_SB(con, brote, sOpe, sFechaUltact);

            // Validacion de la transacci�n
          }
          con.commit();

          // Devolvemos el sintoma modificado
          result = new DatMedSC(med.getCD_ANO(), med.getNM_ALERBRO(),
                                med.getCD_GRUPO(), med.getCD_MEDIDA(),
                                med.getDS_MEDIDA(), med.getIT_INFADIC(),
                                med.getIT_REPE(), med.getNM_SECMB(),
                                med.getDS_INFADI(),
                                sOpe, sFechaUltact);

          listaSalida.addElement(result);

          break;

        case constantes.modoBAJA_SB:
          bBloqueo = false;
        case constantes.modoBAJA:

          // En caso de que haya que comprobar el bloqueo, si se ha
          //  modificado el brote no se da de baja la medida
          if (bBloqueo) {
            if (broteModificado(con, brote)) {
              throw (new Exception(constantes.PRE_BLOQUEO +
                                   constantes.SIVE_BROTES));
            }
          }

          // Establecimiento de la cadena de la query
          sQuery = "DELETE FROM SIVE_BROTES_MED " +
              "WHERE CD_ANO = ? AND NM_ALERBRO = ? " +
              "AND CD_GRUPO = ? AND CD_MEDIDA = ? AND NM_SECMB = ?";

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(sQuery);

          // Instanciacion de la query
          instanciarDeleteQuery(med, st);

          // Ejecucion del delete
          st.executeUpdate();

          // Cierre del PreparedStatement (el ResultSet no se usa)
          st.close();
          st = null;

          // Se modifica el CD_OPE y FC_ULTACT de la notificacion
          dFechaUltact = new java.util.Date();
          formUltact = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                            new Locale("es", "ES"));
          sFechaUltact = formUltact.format(dFechaUltact);
          sOpe = med.getCD_OPE();
          if (bBloqueo) { //si es true es con bloqueo
            modificarBrote(con, brote, sOpe, sFechaUltact);
          }
          else { //si es false es sin bloqueo
            modificarBrote_SB(con, brote, sOpe, sFechaUltact);

            // Validacion de la transacci�n
          }
          con.commit();

          // Devolvemos el sintoma modificado
          result = new DatMedSC(med.getCD_ANO(), med.getNM_ALERBRO(),
                                med.getCD_GRUPO(), med.getCD_MEDIDA(),
                                med.getDS_MEDIDA(), med.getIT_INFADIC(),
                                med.getIT_REPE(), med.getNM_SECMB(),
                                med.getDS_INFADI(),
                                sOpe, sFechaUltact);

          listaSalida.addElement(result);
          break;
        case constantes.modoCONSULTA:
          break;
      }

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);

    return listaSalida;
  } // Fin doWork()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void instanciarInsertQuery(DatMedSC med,
                                       PreparedStatement st,
                                       int nextMed) throws Exception {

    // Lleva la cuenta de ?
    int iParN = 1;

    // A�o
    st.setString(iParN++, med.getCD_ANO());

    // Numero de Brote
    String nmBrote = med.getNM_ALERBRO().trim();
    st.setInt(iParN++, (new Integer(nmBrote)).intValue());

    // Grupo
    st.setString(iParN++, med.getCD_GRUPO());

    // Medida
    st.setString(iParN++, med.getCD_MEDIDA());

    // Numero Secuencial de Medida
    st.setInt(iParN++, nextMed);

    // Informacion Adicional
    String sInfAdic = med.getDS_INFADI();
    if (med.getIT_INFADIC().equals("S")) {
      if (!sInfAdic.equals("")) {
        st.setString(iParN++, sInfAdic);
      }
      else {
        st.setNull(iParN++, java.sql.Types.VARCHAR);
      }
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

    }
  } // Fin instanciarInsertQuery()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void instanciarUpdateQuery(DatMedSC med,
                                       PreparedStatement st) throws Exception {

    // Lleva la cuenta de ?
    int iParN = 1;

    /************** Par�metros del SET ***************/

    // Mas Informacion
    String sInfAdi = med.getDS_INFADI();
    if (med.getIT_INFADIC().equals("S")) {
      if (!sInfAdi.equals("")) {
        st.setString(iParN++, sInfAdi);
      }
      else {
        st.setNull(iParN++, java.sql.Types.VARCHAR);
      }
    }
    else {
      st.setNull(iParN++, java.sql.Types.VARCHAR);

      /************** Par�metros del WHERE ****************/

      // A�o
    }
    st.setString(iParN++, med.getCD_ANO());

    // Numero Brote
    String sNumBrote = med.getNM_ALERBRO().trim();
    st.setInt(iParN++, (new Integer(sNumBrote)).intValue());

    // Codigo Grupo
    st.setString(iParN++, med.getCD_GRUPO());

    // Codigo Medida
    st.setString(iParN++, med.getCD_MEDIDA());

    // Numero Secuencial de Medida
    String sNumSecMed = med.getNM_SECMB();
    st.setInt(iParN++, (new Integer(sNumSecMed)).intValue());
  } // Fin instanciarUpdateQuery()

  // Instanciamos la query (modficando los ? en el PreparedStatement)
  //  con los valores de los datos de entrada
  protected void instanciarDeleteQuery(DatMedSC med,
                                       PreparedStatement st) throws Exception {

    // Lleva la cuenta de ?
    int iParN = 1;

    // A�o
    st.setString(iParN++, med.getCD_ANO());

    // Numero Brote
    String iNumBrote = med.getNM_ALERBRO().trim();
    st.setInt(iParN++, (new Integer(iNumBrote)).intValue());

    // Codigo Grupo
    st.setString(iParN++, med.getCD_GRUPO());

    // Codigo Medida
    st.setString(iParN++, med.getCD_MEDIDA());

    // Numero Secuencial de Medida
    String sSecMed = med.getNM_SECMB().trim();
    st.setInt(iParN++, (new Integer(sSecMed)).intValue());
  } // Fin instanciarDeleteQuery()

  // Comprueba el CD_OPE y FC_ULTACT del brote
  //  de la BD con el brote del cliente
  boolean broteModificado(Connection c, DatMedCS brote) throws Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery;
    String ope, fultact;

    try {
      sQuery = "SELECT CD_OPE,FC_ULTACT FROM SIVE_BROTES " +
          "WHERE CD_ANO = ? AND NM_ALERBRO = ?";

      // Preparacion de la sentencia SQL
      st = c.prepareStatement(sQuery);

      // INSTANCIACION DE LA QUERY

      // Lleva la cuenta de ?
      int iParN = 1;

      // A�o
      st.setString(iParN++, brote.getCD_ANO());

      // Numero Brote
      String nmBrote = brote.getNM_ALERBRO().trim();
      st.setInt(iParN++, (new Integer(nmBrote)).intValue());

      // FIN INSTANCIACION DE LA QUERY

      // Ejecucion de la query
      rs = st.executeQuery();

      // Obtenemos CD_OPE y FC_ULTACT
      if (rs.next()) {
        ope = rs.getString("CD_OPE");
        fultact = Fechas.timestamp2String(rs.getTimestamp("FC_ULTACT"));
      }
      else {
        return true;
      }

      // Cierre del PreparedStatement y del ResultSet
      rs.close();
      rs = null;
      st.close();
      st = null;

    }
    catch (Exception ex) {
      return true;
    }

    if (!ope.equals(brote.getCD_OPE()) || !fultact.equals(brote.getFC_ULTACT())) {
      return true;
    }
    else {
      return false;
    }

  } // broteModificado()

  // Modifica el CD_OPE y FC_ULTACT del brote (con comprobacion de bloqueo)
  void modificarBrote(Connection c, DatMedCS brote, String sOpe,
                      String sFechaUltact) throws Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    String sQuery;

    try {
      sQuery = "UPDATE SIVE_BROTES SET CD_OPE = ?,FC_ULTACT = ? " +
          "WHERE CD_ANO = ? AND NM_ALERBRO = ? AND CD_OPE = ? AND FC_ULTACT = ?";

      // Preparacion de la sentencia SQL
      st = c.prepareStatement(sQuery);

      // INSTANCIACION DE LA QUERY

      // Lleva la cuenta de ?
      int iParN = 1;

      // Parte del SET
      // CD_OPE NUEVO
      st.setString(iParN++, sOpe);
      // FC_ULTACT NUEVO
      java.sql.Timestamp fUltact = Fechas.string2Timestamp(sFechaUltact);
      st.setTimestamp(iParN++, fUltact);

      // Parte del WHERE (ANO, ALERBRO, CD_OPE, FC_ULTACT)
      // A�o
      st.setString(iParN++, brote.getCD_ANO());
      // Numero de Brote
      String nmBrote = brote.getNM_ALERBRO().trim();
      st.setInt(iParN++, (new Integer(nmBrote)).intValue());
      // CD_OPE ANTIGUO
      st.setString(iParN++, brote.getCD_OPE());
      // FC_ULTACT ANTIGUO
      st.setTimestamp(iParN++, Fechas.string2Timestamp(brote.getFC_ULTACT()));

      // FIN INSTANCIACION DE LA QUERY

      // Ejecucion del update
      int iActualizados = st.executeUpdate();

      //PREGUNTAR si ha modificado algo (ver de nuevo el bloqueo)
      if (iActualizados == 0) {
        throw (new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_BROTES));
      }

      // Cierre del PreparedStatement
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }
  } // Fin modificarBrote()

  // Modifica el CD_OPE y FC_ULTACT del brote (sin consideracion de bloqueo)
  void modificarBrote_SB(Connection c, DatMedCS brote, String sOpe,
                         String sFechaUltact) throws Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    String sQuery;

    try {
      sQuery = "UPDATE SIVE_BROTES SET CD_OPE = ?,FC_ULTACT = ? " +
          "WHERE CD_ANO = ? AND NM_ALERBRO = ?";

      // Preparacion de la sentencia SQL
      st = c.prepareStatement(sQuery);

      // INSTANCIACION DE LA QUERY

      // Lleva la cuenta de ?
      int iParN = 1;

      // Parte del
      // CD_OPE NUEVO
      st.setString(iParN++, sOpe);
      // FC_ULTACT NUEVO
      java.sql.Timestamp fUltact = Fechas.string2Timestamp(sFechaUltact);
      st.setTimestamp(iParN++, fUltact);
      // A�o
      st.setString(iParN++, brote.getCD_ANO());
      // Numero de Brote
      String nmBrote = brote.getNM_ALERBRO().trim();
      st.setInt(iParN++, (new Integer(nmBrote)).intValue());

      // FIN INSTANCIACION DE LA QUERY

      // Ejecucion del update
      st.executeUpdate();

      // Cierre del PreparedStatement
      st.close();
      st = null;

    }
    catch (Exception ex) {
      throw ex;
    }
  } // Fin modificarBrote_SB()

  // Buscamos en la tabla de secuenciadores el siguiente numero
  //  de medida. Si ya existe el Grupo+Medida y no se puede repetir
  //  segun el tipo de medida (IT_REPE), se devuelve -1
  int siguienteMedida(Connection c, DatMedSC med) {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery;
    int iNextMed;

    try {
      iNextMed = sapp.Funciones.SecGeneral(c, st, rs, "NM_SECMB");

      // Si el campo IT_REPE de med es falso, compruebo que
      //  no exista ya en la tabla BROTES_MED alguna medida
      //  con el mismo CD_GRUPO+CD_MEDIDA que la de med
      if (med.getIT_REPE().equals("N")) {
        sQuery = "SELECT CD_MEDIDA FROM SIVE_BROTES_MED " +
            "WHERE CD_ANO = ? AND NM_ALERBRO = ? " +
            "AND CD_GRUPO = ? AND CD_MEDIDA = ?";

        // Preparacion de la sentencia SQL
        st = c.prepareStatement(sQuery);

        // INSTANCIACION DE LA QUERY

        // Lleva la cuenta de ?
        int iParN = 1;

        // CD_ANO
        st.setString(iParN++, med.getCD_ANO());

        // NM_ALERBRO
        String sNumAlerBro = med.getNM_ALERBRO();
        st.setInt(iParN++, (new Integer(sNumAlerBro)).intValue());

        // CD_GRUPO
        st.setString(iParN++, med.getCD_GRUPO());

        // CD_MEDIDA
        st.setString(iParN++, med.getCD_MEDIDA());

        // FIN INSTANCIACION DE LA QUERY

        // Ejecucion del update
        rs = st.executeQuery();

        // Si hay algun elemento se hace saltar la excepcion
        if (rs.next()) {
          throw (new Exception());
        }

        // Cierre del ResultSet y del PreparedStatement
        rs.close();
        rs = null;
        st.close();
        st = null;

      } // Fin if

    }
    catch (Exception ex) {
      return -1;
    } // Fin try

    return iNextMed;
  } // Fin siguienteMedida()

} // Fin SrvDiaMed
