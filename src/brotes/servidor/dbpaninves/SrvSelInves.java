package brotes.servidor.dbpaninves;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

/**
 * @autor PDP
 * @version 1.0
 *
 * Recupera las l�neas de detalle de una petici�n
 * Tabla:    SIVE_INVESTIGADORES
 * Entrada:  un Data con CD_ANO, NM_ALERBRO, CD_CA
 * Salida:   una Lista con un Data para cada una de las filas de
 *           que correspondan a esa petici�n.
 *       COD_USUARIO
 *       COD_GRUPO
 *       FC_ALTA
 *
 *
 */
public class SrvSelInves
    extends DBServlet {

  final protected int servletINVESTIGADORES = 1;

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = null;

    // objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // vector con el resultado
    Lista snapShot = new Lista();

    // filtro
    Data dtFiltro = null;

    Data dtInv = null;
    Data dtReInv = null;
    // buffers
    int i = 1;
    int counter = 1;
    String sQuery = null;
    String sWhere = null;

    try {

      con.setAutoCommit(true);

      dtInv = (Data) vParametros.elementAt(0);
      switch (opmode) {
        case servletINVESTIGADORES:

          // prepara la query
          sQuery = " select a.CD_USUARIO, b.COD_USUARIO, a.FC_ALTA "
              + " from SIVE_INVESTIGADOR a, USUARIO_GRUPO b "
              + " where a.CD_USUARIO = b.COD_USUARIO and a.CD_ANO = ? and a.NM_ALERBRO = ? and a.CD_CA = ? and b.COD_APLICACION = 'BRO' ";

          st = con.prepareStatement(sQuery);

          // prepara la queryStatement(sQuery);
          st.setString(1, dtInv.getString("CD_ANO"));
          st.setInt(2, (new Integer(dtInv.getString("NM_ALERBRO"))).intValue());
          st.setString(3, dtInv.getString("CD_CA"));
          rs = st.executeQuery();

          // rellena la matriz de salida
          while (rs.next()) {
            dtReInv = new Data();
            i = 1;
            dtReInv.put("CD_USUARIO", rs, i++, QueryTool.STRING);
            dtReInv.put("COD_USUARIO", rs, i++, QueryTool.STRING);
            dtReInv.put("FC_ALTA", rs, i++, QueryTool.DATE);
            snapShot.addElement(dtReInv);
          }

          // cierra los recursos
          rs.close();
          rs = null;
          st.close();
          st = null;

          // completa la informaci�n de descripcion del usuario
          for (i = 0; i < snapShot.size(); i++) {
            dtReInv = (Data) snapShot.elementAt(i);

            String queryProv = " select NOMBRE "
                + " from  USUARIO "
                + " where COD_USUARIO = ? ";

            // prepara la query
            st = con.prepareStatement(queryProv);
            st.setString(1, dtReInv.getString("COD_USUARIO"));
            rs = st.executeQuery();
            // guarda el Provedor
            if (rs.next()) {
              dtReInv.put("NOMBRE", rs, 1, QueryTool.STRING);
            }
            // libera recursos
            rs.close();
            rs = null;
            st.close();
            st = null;
          } //fin de for

          break;
      } // cierro el switch

    }
    catch (SQLException e1) {

      // traza el error
      trazaLog(e1);
      bError = true;
      sMsg = "Error al leer investigadores";

    }
    catch (Exception e2) {
      // traza el error
      trazaLog(e2);
      bError = true;

      // selecciona el mensaje de error
      sMsg = "Error inesperado";
    }

    // devuelve el error recogido
    if (bError) {
      con.rollback();
      throw new Exception(sMsg);

    }

    // valida la transacci�n
    con.commit();
    //trazaLog(snapShot);
    return snapShot;
  } // cierra el catch

  public static void main(String args[]) {
    SrvSelInves srv = new SrvSelInves();
    Lista vParam = new Lista();
    Data filtro = new Data();
    Data peticion = new Data();

    // par�metros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");

    peticion.put("CD_ANO", "2000");
    peticion.put("NM_ALERBRO", "25");
    peticion.put("CD_CA", "13");

    vParam.addElement(peticion);
    srv.doDebug(1, vParam);
    srv = null;

  }

}