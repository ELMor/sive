/**
 * Clase: SrvBrApp
 * Paquete: brotes.servidor.menu
 * Hereda: HttpServlet
 * Fecha Inicio: 29/02/2000
 */

package brotes.servidor.menu;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import brotes.datos.menu.ParametrosBrUsu;

public class SrvBrApp
    extends HttpServlet {

  // applets
  // Alertas
  final int applet_ALERTA = 80;

  // Informe final
  final int applet_BROTES = 81;

  // Parametrizaci�n
  final int appletMODELOS_GRUPO_BROTE = 1087;
  final int appletMODELOS_BROTE = 88;
  final int appletPREG_BROTE = 89;
  final int appletLISTAS = 6;

  //Exportaciones
  final int appletINF_UN_BROTE = 41;
  final int appletPERS_BROTE = 42;
  final int appletINF_BROTES = 43;

  //Env�os
  final int appletENVIOS = 51;

  // Administraci�n
  // Mantenimiento
  final int appletGRUPO_EDAD = 38; // Grupos de edades
  final int appletEPI = 39; // Servicio de epidemiolog�a
  final int appletUSR = 35; // Usuarios
  final int appletGRUPOS = 700; //Grupos
  // Localizaci�n
  final int appletCCAA = 14;
  final int appletPROV = 15;
  final int appletNIVEL1 = 11;
  final int appletNIVEL2 = 12;
  final int appletZBS = 13;
  // Procesos epidemiol�gicos
  final int appletCIE = 9;
  final int appletPROCESOS = 10;
  final int appletEDO_CNE = 8;
  final int appletEDO = 1;
  // Mtos. generales de EDO
  final int appletTLINEA = 17;
  final int appletTPREG = 18;
  final int appletTSIVE = 19;
  final int appletTVIGI = 20;
  final int appletMOTBAJA = 21;
  final int appletNIVASIS = 22;
  final int appletSEXO = 23;
  final int appletDIAGNOSTICO = 61;
  // Mtos. brotes
  final int applet_MNTO_GRUPO_BROTE = 82;
  final int applet_MNTO_TIPO_NOTIFICADOR = 83;
  final int applet_MNTO_MECANISMOS = 84;
  final int applet_MNTO_COLECTIVOS = 85;
  final int applet_MNTO_SITUACION = 86;
  final int applet_MNTO_TIPO_BROTE = 87;
  // Mtos. tablas perifericas a SIVE_BROTES
  final int applet_MNTO_ORIGEN_MUESTRAS = 200;
  final int applet_MNTO_SINTOMAS = 201;
  final int applet_MNTO_AGCAUSALES = 203;
  final int applet_MNTO_FACCONT = 204;
  final int applet_MNTO_MEDIDAS = 205;
  final int applet_MNTO_GRUPOS_AGTOXICOS = 206;
  // Mtos. tablas perifericas alimentos
  final int applet_MNTO_TALIMENTOS = 202;
  final int applet_MNTO_TIMPL = 207;
  final int applet_MNTO_MCOMERC = 208;
  final int applet_MNTO_TRATPREV = 209;
  final int applet_MNTO_FINGER = 210;
  final int applet_MNTO_LCONTAMI = 211;
  final int applet_MNTO_LPREPAR = 212;
  final int applet_MNTO_LCONSUMO = 213;
  final int applet_MNTO_MTVIAJE = 214;

  //General
  final int applet_SEL_ZONIFICACION = 2000;

  //Ayuda
  final int ayuda_ACERCA_D = 1000;
  final int ayuda_INF_BROTES = 1001;

  // servicio
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {

    // Applet a enviar seg�n la opci�n de men�
    int AppletAEnviar;

    // streams
    //ObjectInputStream in = null;
    ServletOutputStream out;

    // par�metros del applet
    int iCat = -1;
    int iCatAux = -1;
    int iCatPral = -1;
    int iCatMnt = -1;
    String sWidth = "";
    String sHeight = "";
    String sCatalogoW = ""; //QQ Se establcen m�s abajo.
    String sCatalogoH = "";
    String sConsW = "";
    String sConsH = "";
    String sVolcW = "";
    String sVolcH = "";
    String sClass = "";
    String sLogin = "", sRef = "";
    String sAyuda = "ayuda_pp.html";

    final String sArchiveDefault = "capp.zip, sapp.zip";

    String sArchive = "";
    String sTSIVE = "B";

    // comunicaciones servlet-applet
    try {
      // par�metros recibidos
      ParametrosBrUsu hash = new ParametrosBrUsu();
      loadTable(hash, request);

      // abre el stream de salida
      response.setContentType("text/html");
      out = response.getOutputStream();

      // cabecera
      out.println("<HTML><HEAD>");
      out.println("<TITLE>ICM_99</TITLE>");
      out.println("<BASE HREF=\"" + hash.getString("URL_HTML", true) + "\">");
      out.println("</HEAD><BODY BGCOLOR=\"#FFFFFF\">");

      // determina el tama�o y el nombre de la clase
      sCatalogoW = "680"; //Tama�o para applets de mantenimiento.
      sCatalogoH = "325";
      sConsW = "640"; // Tama�o para applets de Consultas
      sConsH = "600";
      sVolcW = "502"; //Tama�o para applets de volcados (Exportaciones)
      sVolcH = "230";

      AppletAEnviar = new Integer(hash.getString("APPLET", true)).intValue();
      switch (AppletAEnviar) {

        // Alertas
        // Alertas de brotes
        case applet_ALERTA:
          sClass = "brotes.cliente.mantalerta.AppMantAlerta.class";
          sArchive = "brotes_util.zip,alerta_comun.zip";
          sWidth = "680";
          sHeight = "440";
          sAyuda = "ayuda_alert.html";
          break;

          // Informe final
          // Informe final de brote
        case applet_BROTES:
          sClass = "brotes.cliente.mantbif.AppMantBIF.class";
          sArchive = "infinal_comun.zip, brotes_util.zip";
          sWidth = "680";
          sHeight = "440";
          sAyuda = "ayuda_infor.html";
          break;

          // Parametrizaci�n
          // Mantenimiento de modelos -> Modelos Grupo Brote
        case appletMODELOS_GRUPO_BROTE:
          sArchive =
              sClass = "confprot.ApConfProt.class";
          sArchive = "brotes_util.zip, alarmas.zip, cargamodelo.zip, confprot.zip, listas.zip, preguntas.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_param.html";
          break;
          // Mantenimiento de modelos -> Modelos Brote
        case appletMODELOS_BROTE:
          sClass = "brotes.cliente.confprot2.AppConfprot2.class";
          sArchive = "brotes_util.zip, cargamodelo.zip, listas.zip, preguntas.zip, brotes.cliente.confprot2.zip, brotes.datos.bidata.zip, brotes.datos.confprot2.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_param.html";
          break;
          // Mantenimiento de preguntas
        case appletPREG_BROTE:
          sClass = "preguntas.MantDePreguntas.class";
          sArchive = "brotes_util.zip, listas.zip, preguntas.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_param.html";
          break;
          // Mantenimiento de listas
        case appletLISTAS:
          sClass = "listas.mantlis.class";
          sArchive = "brotes_util.zip, listas.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_param.html";
          break;

          //Exportacion de un brote
        case appletINF_UN_BROTE:
          sClass = "brotes.cliente.exportbrote.AppExportBrote.class";
          sArchive =
              "brotes_util.zip, brotes.cliente.exportbrote.zip, util_ficheros.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_exporta.html";
          break;

          //Exportacion de brotes
        case appletINF_BROTES:
          sClass =
              "brotes.cliente.exportbrotesperiodo.AppExportBrotesPeriodo.class";
          sArchive =
              "brotes_util.zip, brotes.cliente.exportbrotesperiodo.zip, util_ficheros.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_exporta.html";
          break;

          // Administraci�n
          // Grupos de Edades
        case appletGRUPO_EDAD:
          sClass = "brotes.cliente.mantgrupedad.AppMantGrupEdad.class";
          sArchive = "brotes_util.zip, brotes.cliente.mantgrupedad.zip";
          sWidth = "680";
          sHeight = "385";
          sAyuda = "ayuda_admin.html";
          break;
          // Servicio de epidemiolog�a
        case appletEPI:
          sClass = "mantdatos.mantdatos.class";
          sArchive = "brotes_util.zip, mantdatos.zip, param.zip";
          sWidth = sCatalogoW;
          sHeight = "415";
          sAyuda = "ayuda_admin.html";
          break;
          // Grupos de trabajo
        case appletGRUPOS:
          sClass = "usu2.AppUSU.class";
          sArchive = "brotes_util.zip, usu2.zip";
          sWidth = "640";
          sHeight = "450";
          sAyuda = "ayuda_admin.html";
          break;
          // Usuarios
        case appletUSR:
          sClass = "brotes.cliente.mantus.AppMantUsuBro.class";
          sArchive =
              "brotes.cliente.mantus.zip, brotes.datos.mantus.zip, brotes_util.zip";
          sWidth = "675";
          sHeight = "350";
          sAyuda = "ayuda_admin.html";
          break;
          // Localizaci�n
        case appletCCAA:
          iCat = 10;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case appletPROV:
          iCatPral = 10;
          iCatAux = 10;
          sArchive = "brotes_util.zip";
          sClass = "catalogo2.Catalogo2.class";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case appletNIVEL1:
          sClass = "nivel1.Nivel1.class";
          sArchive = "brotes_util.zip, nivel1.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case appletNIVEL2:
          iCatPral = 20;
          iCatAux = 130;
          sClass = "catalogo2.Catalogo2.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case appletZBS:
          sClass = "zbs.ZBS.class";
          sArchive = "brotes_util.zip, zbs.zip";
          sWidth = sCatalogoW;
          sHeight = "430";
          sAyuda = "ayuda_admin.html";
          break;

          // Procesos epidemiol�gicos
        case appletCIE:
          sClass = "enfcie.EnfCie.class";
          sArchive = "brotes_util.zip, enfcie.zip";
          sWidth = sCatalogoW; // "660";
          sHeight = sCatalogoH; // "380";
          sAyuda = "ayuda_admin.html";
          break;
        case appletPROCESOS:
          sClass = "Procesos.Procesos.class";
          sArchive = "brotes_util.zip, procesos.zip, enfcie.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case appletEDO_CNE:
          iCat = 20;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case appletEDO:
          sClass = "enfedo.MantEnferEDO.class";
          sArchive = "brotes_util.zip, enfedo.zip, enfcie.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;

          // Mtos. generales EDO
        case appletTSIVE:
          iCat = 50;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;

        case appletTLINEA:
          iCat = 30;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case appletTPREG:
          iCat = 40;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case appletTVIGI:
          iCat = 60;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case appletMOTBAJA:
          iCat = 70;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case appletNIVASIS:
          iCat = 80;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case appletSEXO:
          iCat = 100;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case appletDIAGNOSTICO:
          iCat = 180;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;

          // Mtos. Brotes
        case applet_MNTO_GRUPO_BROTE:
          iCatMnt = 120;
          sClass = "brotes.cliente.mantcatalog.AppMant.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_TIPO_NOTIFICADOR:
          iCat = 200;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case applet_MNTO_MECANISMOS:
          iCat = 210;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case applet_MNTO_COLECTIVOS:
          iCat = 220;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case applet_MNTO_SITUACION:
          iCat = 230;
          sClass = "catalogo.Catalogo.class";
          sArchive = "brotes_util.zip";
          sWidth = sCatalogoW;
          sHeight = sCatalogoH;
          sAyuda = "ayuda_admin.html";
          break;
        case applet_MNTO_TIPO_BROTE:
          iCatMnt = 50;
          iCatPral = 30;
          iCatAux = 190;
//        sClass = "catalogo2.Catalogo2.class";
//		    sArchive = sArchiveDefault + ", catalogo.zip, catalogo2.zip";
          sClass = "brotes.cliente.mantcatalog.AppMant2.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_ORIGEN_MUESTRAS:
          iCatMnt = 10;
          sClass = "brotes.cliente.mantcatalog.AppMant.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_SINTOMAS:
          iCatMnt = 20;
          sClass = "brotes.cliente.mantcatalog.AppMant.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_TALIMENTOS:
          iCatMnt = 30;
          sClass = "brotes.cliente.mantcatalog.AppMant.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_AGCAUSALES:
          iCatMnt = 10;
          sClass = "brotes.cliente.mantcatalog.AppMant2.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_FACCONT:
          iCatMnt = 20;
          sClass = "brotes.cliente.mantcatalog.AppMant2.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_MEDIDAS:
          iCatMnt = 30;
          sClass = "brotes.cliente.mantcatalog.AppMant2.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_GRUPOS_AGTOXICOS:
          iCatMnt = 40;
          sClass = "brotes.cliente.mantcatalog.AppMant2.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_TIMPL:
          iCatMnt = 40;
          sClass = "brotes.cliente.mantcatalog.AppMant.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_MCOMERC:
          iCatMnt = 50;
          sClass = "brotes.cliente.mantcatalog.AppMant.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_TRATPREV:
          iCatMnt = 60;
          sClass = "brotes.cliente.mantcatalog.AppMant.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_FINGER:
          iCatMnt = 70;
          sClass = "brotes.cliente.mantcatalog.AppMant.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_LCONTAMI:
          iCatMnt = 80;
          sClass = "brotes.cliente.mantcatalog.AppMant.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_LPREPAR:
          iCatMnt = 90;
          sClass = "brotes.cliente.mantcatalog.AppMant.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_LCONSUMO:
          iCatMnt = 100;
          sClass = "brotes.cliente.mantcatalog.AppMant.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

        case applet_MNTO_MTVIAJE:
          iCatMnt = 110;
          sClass = "brotes.cliente.mantcatalog.AppMant.class";
          sArchive = "brotes_util.zip";
          sWidth = "680";
          sHeight = "380";
          sAyuda = "ayuda_admin.html";
          break;

          // Opci�n General
          // Seleccionar Zonificaci�n
        case applet_SEL_ZONIFICACION:
          sClass = "brotes.cliente.zonificacion.AppZonBro.class";
          sArchive = "fechas.zip, panniveles.zip, " +
              "brotes_util.zip, brotes.cliente.zonificacion.zip";
          sWidth = "600";
          sHeight = "175";
          sAyuda = "ayuda_gen.html";
          break;

      }

      out.println("<TABLE WIDTH=\"100%\" HEIGHT=\"100%\" BORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"\"0>");
      out.println("<TR><TD ALIGN=\"CENTER\" VALIGN=\"MIDDLE\">");

      // applet
      out.println("<APPLET");
      out.println(" MAYSCRIPT");

      // ZIP
      if (sArchive.equals("")) {
        //out.println(" CODEBASE = \"applet/\"");
        out.println(" CODEBASE = \".\"");
      }
      else {
        out.println(" CODEBASE = \"zip/\"");
        out.println(" ARCHIVE = \"" + sArchive + "\"");
      }
      out.println(" CODE     = \"" + sClass + "\"");
      out.println(" NAME     = \"ICM_99" + hash.getString("APPLET", true) +
                  "\"");
      out.println(" WIDTH    = \"" + sWidth + "\"");
      out.println(" HEIGHT   = \"" + sHeight + "\"");
      out.println(" HSPACE   = \"0\"");
      out.println(" VSPACE   = \"0\"");
      out.println(" ALIGN   = \"CENTER\"");
      out.println(">");

      out.println("<PARAM NAME=\"CA\" VALUE=\"" + hash.getString("CA", true) +
                  "\">");
      out.println("<PARAM NAME=\"URL_SERVLET\" VALUE=\"" +
                  hash.getString("URL_SERVLET", true) + "\">");
      out.println("<PARAM NAME=\"URL_HTML\" VALUE=\"" +
                  hash.getString("URL_HTML", true) + "\">");
      out.println("<PARAM NAME=\"FTP_SERVER\" VALUE=\"" +
                  hash.getString("FTP_SERVER", false) + "\">");
      out.println("<PARAM NAME=\"DS_EMAIL\" VALUE=\"" +
                  hash.getString("DS_EMAIL", false) + "\">");
      out.println("<PARAM NAME=\"NIVEL1\" VALUE=\"" + hash.getString("NIVEL1", true) +
                  "\">");
      out.println("<PARAM NAME=\"NIVEL2\" VALUE=\"" + hash.getString("NIVEL2", true) +
                  "\">");
      out.println("<PARAM NAME=\"NIVEL3\" VALUE=\"" + hash.getString("NIVEL3", true) +
                  "\">");
      System.out.println("SrvBrApp: Par�metros correctos");
      out.println("<PARAM NAME=\"IDIOMA\" VALUE=\"" +
                  hash.getInteger("IDIOMA", true).toString() + "\">");
      out.println("<PARAM NAME=\"LOGIN\" VALUE=\"" + hash.getString("LOGIN", true) +
                  "\">");
      System.out.println("SrvBrApp: Par�metros correctos");
      out.println("<PARAM NAME=\"TSIVE\" VALUE=\"" + "B" + "\">");
      //out.println("<PARAM NAME=\"IDIOMA_LOCAL\" VALUE=\"" + hash.getString("IDIOMA_LOCAL", false) + "\">");
      out.println("<PARAM NAME=\"CATALOGO\" VALUE=\"" + Integer.toString(iCat) +
                  "\">");
      out.println("<PARAM NAME=\"TBL_MANT\" VALUE=\"" +
                  Integer.toString(iCatMnt) + "\">");
      out.println("<PARAM NAME=\"CATAUX\" VALUE=\"" + Integer.toString(iCatAux) +
                  "\">");
      out.println("<PARAM NAME=\"CATPRAL\" VALUE=\"" +
                  Integer.toString(iCatPral) + "\">");
      out.println("<PARAM NAME=\"PERFIL\" VALUE=\"" + hash.getString("PERFIL", true) +
                  "\">");
      out.println("<PARAM NAME=\"AYUDA\" VALUE=\"" + "ayuda/brotes/" + sAyuda +
                  "\">");
      out.println("<PARAM NAME=\"IT_AUTALTA\" VALUE=\"" +
                  hash.getString("IT_AUTALTA", false) + "\">");
      out.println("<PARAM NAME=\"IT_AUTBAJA\" VALUE=\"" +
                  hash.getString("IT_AUTBAJA", false) + "\">");
      out.println("<PARAM NAME=\"IT_AUTMOD\" VALUE=\"" +
                  hash.getString("IT_AUTMOD", false) + "\">");
      out.println("<PARAM NAME=\"IT_FG_ENFERMO\" VALUE=\"" +
                  hash.getString("IT_FG_ENFERMO", false) + "\">");
      out.println("<PARAM NAME=\"IT_MODULO_3\" VALUE=\"" +
                  hash.getString("IT_MODULO_3", true) + "\">");
      out.println("<PARAM NAME=\"IT_TRAMERO\" VALUE=\"" +
                  hash.getString("IT_TRAMERO", true) + "\">");
      out.println("<PARAM NAME=\"CD_E_NOTIF\" VALUE=\"" +
                  hash.getString("CD_E_NOTIF", false) + "\">");
      out.println("<PARAM NAME=\"DS_E_NOTIF\" VALUE=\"" +
                  hash.getString("DS_E_NOTIF", false) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL_1_EQ\" VALUE=\"" +
                  hash.getString("CD_NIVEL_1_EQ", false) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL_2_EQ\" VALUE=\"" +
                  hash.getString("CD_NIVEL_2_EQ", false) + "\">");
      out.println("<PARAM NAME=\"DS_NIVEL_1_EQ\" VALUE=\"" +
                  hash.getString("DS_NIVEL_1_EQ", false) + "\">");
      out.println("<PARAM NAME=\"DS_NIVEL_2_EQ\" VALUE=\"" +
                  hash.getString("DS_NIVEL_2_EQ", false) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL_1_AUTORIZACIONES\" VALUE=\"" +
                  hash.getString("CD_NIVEL_1_AUTORIZACIONES", false) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL_2_AUTORIZACIONES\" VALUE=\"" +
                  hash.getString("CD_NIVEL_2_AUTORIZACIONES", false) + "\">");
      out.println("<PARAM NAME=\"FC_ACTUAL\" VALUE=\"" +
                  hash.getString("FC_ACTUAL", true) + "\">");
      out.println("<PARAM NAME=\"IT_FG_VALIDAR\" VALUE=\"" +
                  hash.getString("IT_FG_VALIDAR", true) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL1_DEFECTO\" VALUE=\"" +
                  hash.getString("CD_NIVEL_1_DEFECTO", false) + "\">");
      out.println("<PARAM NAME=\"CD_NIVEL2_DEFECTO\" VALUE=\"" +
                  hash.getString("CD_NIVEL_2_DEFECTO", false) + "\">");
      out.println("<PARAM NAME=\"DS_NIVEL1_DEFECTO\" VALUE=\"" +
                  hash.getString("DS_NIVEL_1_DEFECTO", false) + "\">");
      out.println("<PARAM NAME=\"DS_NIVEL2_DEFECTO\" VALUE=\"" +
                  hash.getString("DS_NIVEL_2_DEFECTO", false) + "\">");
      out.println("<PARAM NAME=\"ANYO_DEFECTO\" VALUE=\"" +
                  hash.getString("ANYO_DEFECTO", false) + "\">");

      out.println("<PARAM NAME=\"IT_FG_RESOLVER_CONFLICTOS\" VALUE=\"" +
                  hash.getString("IT_FG_RESOLVER_CONFLICTOS", false) + "\">");
      //out.println("<PARAM NAME=\"IT_FG_EXPORT\" VALUE=\"" + hash.getString("IT_FG_EXPORT", false) + "\">");

      out.println("<PARAM NAME=\"IT_USU\" VALUE=\"" + hash.getString("IT_USU", false) +
                  "\">");
      out.println("<PARAM NAME=\"COD_APLICACION\" VALUE=\"" +
                  hash.getString("COD_APLICACION", false) + "\">");
      out.println("<PARAM NAME=\"CD_BROTES\" VALUE=\"" +
                  hash.getString("CD_BROTES", false) + "\">");

      out.println("</APPLET>");

      out.println("</TD></TR>");
      out.println("</TABLE></BODY></HTML>");
      out.flush();
      out.close();
      System.out.println("SrvBrApp: Par�metros correctos");

      // envia la excepci�n
    }
    catch (Exception e) {
      e.printStackTrace();
      System.out.println("SrvBrApp: Excepci�n");
      // abre el stream de salida
      response.setContentType("text/html");
      out = response.getOutputStream();
      out.println("<HTML><HEAD>");
      out.println("<TITLE>ICM_99</TITLE>");
      out.println("</HEAD><BODY BGCOLOR=\"#FFFFFF\">");
      out.println(
          "<STRONG><FONT SIZE=2>Petici�n no autorizada.</FONT></STRONG></BODY></HTML>");
      out.flush();
      out.close();
    }
  }

  // CARGA LA TABLA CON LOS DATOS DEL OBJETO REQUEST
  public void loadTable(ParametrosBrUsu param, HttpServletRequest request) {
    param.put("APPLET", request.getParameter("APPLET"));
    param.put("IDIOMA", new Integer(request.getParameter("IDIOMA")));
    param.put("LOGIN", request.getParameter("LOGIN"));
    param.put("URL_SERVLET", request.getParameter("URL_SERVLET"));
    param.put("FTP_SERVER", request.getParameter("FTP_SERVER"));
    param.put("DS_EMAIL", request.getParameter("DS_EMAIL"));
    param.put("URL_HTML", request.getParameter("URL_HTML"));
    param.put("PERFIL", request.getParameter("PERFIL"));
    param.put("CA", request.getParameter("CA"));
    param.put("NIVEL1", request.getParameter("NIVEL1"));
    param.put("NIVEL2", request.getParameter("NIVEL2"));
    param.put("NIVEL3", request.getParameter("NIVEL3"));
    param.put("TSIVE", request.getParameter("TSIVE"));
    //param.put("IDIOMA_LOCAL", request.getParameter("IDIOMA_LOCAL"));
    param.put("IT_AUTALTA", request.getParameter("IT_AUTALTA"));
    param.put("IT_AUTALTA", request.getParameter("IT_AUTALTA"));
    param.put("IT_AUTBAJA", request.getParameter("IT_AUTBAJA"));
    param.put("IT_AUTMOD", request.getParameter("IT_AUTMOD"));
    param.put("IT_FG_ENFERMO", request.getParameter("IT_FG_ENFERMO"));
    param.put("IT_FG_VALIDAR", request.getParameter("IT_FG_VALIDAR"));
    param.put("IT_MODULO_3", request.getParameter("IT_MODULO_3"));
    param.put("IT_TRAMERO", request.getParameter("IT_TRAMERO"));
    param.put("IT_FG_MNTO", request.getParameter("IT_FG_MNTO"));
    param.put("IT_FG_MNTO_USU", request.getParameter("IT_FG_MNTO_USU"));
    param.put("IT_FG_TCNE", request.getParameter("IT_FG_TCNE"));
    param.put("IT_FG_PROTOS", request.getParameter("IT_FG_PROTOS"));
    //param.put("IT_FG_ALARMAS", request.getParameter("IT_FG_ALARMAS"));
    //param.put("IT_FG_EXPORT", request.getParameter("IT_FG_EXPORT"));
    //param.put("IT_FG_GENALAUTO", request.getParameter("IT_FG_GENALAUTO"));
    //param.put("IT_FG_MNOTIFS", request.getParameter("IT_FG_MNOTIFS"));
    //param.put("IT_FG_CONSRES", request.getParameter("IT_FG_CONSRES"));
    param.put("CD_E_NOTIF", request.getParameter("CD_E_NOTIF"));
    param.put("DS_E_NOTIF", request.getParameter("DS_E_NOTIF"));
    param.put("CD_NIVEL_1_EQ", request.getParameter("CD_NIVEL_1_EQ"));
    param.put("CD_NIVEL_2_EQ", request.getParameter("CD_NIVEL_2_EQ"));
    param.put("DS_NIVEL_1_EQ", request.getParameter("DS_NIVEL_1_EQ"));
    param.put("DS_NIVEL_2_EQ", request.getParameter("DS_NIVEL_2_EQ"));
    param.put("CD_NIVEL_1_AUTORIZACIONES",
              request.getParameter("CD_NIVEL_1_AUTORIZACIONES"));
    param.put("CD_NIVEL_2_AUTORIZACIONES",
              request.getParameter("CD_NIVEL_2_AUTORIZACIONES"));
    param.put("FC_ACTUAL", request.getParameter("FC_ACTUAL"));
    param.put("CD_NIVEL_1_DEFECTO", request.getParameter("CD_NIVEL_1_DEFECTO"));
    param.put("CD_NIVEL_2_DEFECTO", request.getParameter("CD_NIVEL_2_DEFECTO"));
    param.put("DS_NIVEL_1_DEFECTO", request.getParameter("DS_NIVEL_1_DEFECTO"));
    param.put("DS_NIVEL_2_DEFECTO", request.getParameter("DS_NIVEL_2_DEFECTO"));
    param.put("ANYO_DEFECTO", request.getParameter("ANYO_DEFECTO"));
    param.put("IT_FG_RESOLVER_CONFLICTOS",
              request.getParameter("IT_FG_RESOLVER_CONFLICTOS"));
    param.put("IT_USU", request.getParameter("IT_USU"));
    param.put("COD_APLICACION", request.getParameter("COD_APLICACION"));
    param.put("CD_BROTES", request.getParameter("CD_BROTES"));
  }
}
