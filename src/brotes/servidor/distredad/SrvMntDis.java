package brotes.servidor.distredad;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;

/**
 * @autor PDP
 * @version 1.0
 *
 * Actualiza NM_DISTGEDAD en SIVE_SEC_GENERAL
 * Tabla:    SIVE_SEC_GENERAL
 * Entrada:  Le suma una al NM_DISTGEDAD
 *
 * Selecciona NM_DISTGEDAD en SIVE_SEC_GENERAL
 * Tabla:    SIVE_SEC_GENERAL
 * Salida:   NM_DISTGEDAD actualizado
 *
 * Inserta un registrop en la SIVE_BROTES_DISTGEDAD
 * Entrada:  NM_DISTGEDAD
 *           CD_ANO
 *           NM_ALERBRO
 *           IT_EDAD
 *           CD_GEDAD
 *           DIST_EDAD_I
 *           DIST_EDAD_F
 *           NM_V_EXP
 *           NM_M_EXP
 *           NM_NC_EXP
 *           NM_V_ENF
 *           NM_M_ENF
 *           NM_NC_ENF
 *           NM_V_HOS
 *           NM_M_HOS
 *           NM_NC_HOS
 *           NM_V_DEF
 *           NM_H_DEF
 *           NM_NC_DEF
 */
public class SrvMntDis
    extends DBServlet {

  final protected int servletALTADIST = 1;

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = null;

    // objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Lista de entrada;
    Lista lsEntrada = new Lista();
    lsEntrada = vParametros;

    // regularización
    Data dtReg = null;

    String sQuery = null;

    // vector con el resultado
    Lista snapShot = new Lista();

    // filtro
    Data dtFiltro = null;

    switch (opmode) {
      case servletALTADIST:
        try {
          // inserta el registro en SIVE_BROTES_GEDAD
          con.setAutoCommit(false);
          for (int i = 0; i < lsEntrada.size(); i++) {
            dtReg = (Data) lsEntrada.elementAt(i);
            //ResultSet rs = null;

            // Incrementamos el secuenciador

            sQuery = "select NM_DISTGEDAD from SIVE_SEC_GENERAL";

            st = con.prepareStatement(sQuery);
            rs = st.executeQuery();
            String secuencial = null;
            if (rs.next()) {
              secuencial = rs.getString("NM_DISTGEDAD");
            }
            int secuenciador = Integer.parseInt(secuencial);
            secuenciador++;

            // cierres

            rs.close();
            rs = null;
            st.close();
            st = null;

            sQuery = "update SIVE_SEC_GENERAL set NM_DISTGEDAD=?";

            st = con.prepareStatement(sQuery);
            st.setInt(1, secuenciador);
            st.executeUpdate();

            // cierres

            st.close();
            st = null;

            // Ahora insertamos nuestro registro....

            sQuery = "insert into SIVE_BROTES_DISTGEDAD (NM_DISTGEDAD," +
                "CD_ANO," +
                "NM_ALERBRO," +
                "IT_EDAD," +
                "CD_GEDAD," +
                "DIST_EDAD_I," +
                "DIST_EDAD_F," +
                "NM_V_EXP," +
                "NM_M_EXP," +
                "NM_NC_EXP," +
                "NM_V_ENF," +
                "NM_M_ENF," +
                "NM_NC_ENF," +
                "NM_V_HOS," +
                "NM_M_HOS," +
                "NM_NC_HOS," +
                "NM_V_DEF," +
                "NM_M_DEF," +
                "NM_NC_DEF)" +
                "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            st = con.prepareStatement(sQuery);
            st.setInt(1, secuenciador);
            st.setString(2, dtReg.getString("CD_ANO"));
            st.setInt(3, Integer.parseInt(dtReg.getString("NM_ALERBRO")));
            st.setString(4, dtReg.getString("IT_EDAD"));
            st.setString(5, dtReg.getString("CD_GEDAD"));
            st.setInt(6, Integer.parseInt(dtReg.getString("DIST_EDAD_I")));
            st.setInt(7, Integer.parseInt(dtReg.getString("DIST_EDAD_F")));
            st.setInt(8, Integer.parseInt(dtReg.getString("NM_V_EXP")));
            st.setInt(9, Integer.parseInt(dtReg.getString("NM_M_EXP")));
            st.setInt(10, Integer.parseInt(dtReg.getString("NM_NC_EXP")));
            st.setInt(11, Integer.parseInt(dtReg.getString("NM_V_ENF")));
            st.setInt(12, Integer.parseInt(dtReg.getString("NM_M_ENF")));
            st.setInt(13, Integer.parseInt(dtReg.getString("NM_NC_EXP")));
            st.setInt(14, Integer.parseInt(dtReg.getString("NM_V_HOS")));
            st.setInt(15, Integer.parseInt(dtReg.getString("NM_M_HOS")));
            st.setInt(16, Integer.parseInt(dtReg.getString("NM_NC_HOS")));
            st.setInt(17, Integer.parseInt(dtReg.getString("NM_V_DEF")));
            st.setInt(18, Integer.parseInt(dtReg.getString("NM_M_DEF")));
            st.setInt(19, Integer.parseInt(dtReg.getString("NM_NC_DEF")));

            st.executeUpdate();

            // cierre

            st.close();
            st = null;
          } // del for
          con.commit();

        }
        catch (SQLException e1) {

          // traza el error
          trazaLog(e1);
          bError = true;
          sMsg = "Error al insertar";

        }
        catch (Exception e2) {
          // traza el error
          trazaLog(e2);
          bError = true;

          // selecciona el mensaje de error
          sMsg = "Error inesperado";
        }

        break;
    } // cierro el switch

    // devuelve el error recogido
    if (bError) {
      con.rollback();
      throw new Exception(sMsg);

    }

    // valida la transacción
    con.commit();
    trazaLog(snapShot);
    return snapShot;
  } // cierra el catch

  public static void main(String args[]) {
    SrvMntDis srv = new SrvMntDis();
    Lista vParam = new Lista();
    Data filtro = new Data();
    Data dtReg = new Data();

    // parámetros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");

    dtReg.put("CD_ANO", "1997");
    dtReg.put("NM_ALERBRO", "1");
    dtReg.put("IT_EDAD", "S");
    dtReg.put("CD_GEDAD", "002");
    dtReg.put("DIST_EDAD_I", "9");
    dtReg.put("DIST_EDAD_F", "18");
    dtReg.put("NM_V_EXP", "60");
    dtReg.put("NM_M_EXP", "40");
    dtReg.put("NM_NC_EXP", "50");
    dtReg.put("NM_V_ENF", "53");
    dtReg.put("NM_M_ENF", "12");
    dtReg.put("NM_NC_ENF", "36");
    dtReg.put("NM_V_HOS", "56");
    dtReg.put("NM_M_HOS", "40");
    dtReg.put("NM_NC_HOS", "20");
    dtReg.put("NM_V_DEF", "30");
    dtReg.put("NM_M_DEF", "40");
    dtReg.put("NM_NC_DEF", "10");

    vParam.addElement(dtReg);

    srv.doDebug(1, vParam);
    srv = null;

  }

}