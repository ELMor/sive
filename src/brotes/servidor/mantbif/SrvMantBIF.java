package brotes.servidor.mantbif;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;

public class SrvMantBIF
    extends DBServlet {

  //Modos de operaci�n del Servlet
  final int servletCONSULTABROTES = 0;

  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    //java.sql.Timestamp TSFec = new java.sql.Timestamp(dFecha.getTime());
    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);
    //# System.Out.println("ts : " + TSFec.toString());

    return TSFec;
  }

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  // funcionalidad del servlet
  protected Lista doWork(int opmode, Lista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    // Objetos de datos
    Lista listaSalida = new Lista();
    Data dataEntrada = null;
    Data dataSalida = null;
//    boolean bFiltro = false;
//    int iValorFiltro = 0;
    Lista lisPeticion = null;

    //fechas
    /*    java.util.Date dFecha;
        java.sql.Date sqlFec;
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
        public SimpleDateFormat Format_Horas = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",new Locale("es", "ES"));
        public java.sql.Timestamp fecRecu = null;
        public String sfecRecu = "";*/

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    query = "select b.CD_ANO, b.NM_ALERBRO, b.DS_BROTE, b.CD_GRUPO, " +
        "b.FC_FECHAHORA, ab.CD_SITALERBRO, ab.CD_NIVEL_1 " +
        "from SIVE_BROTES b, SIVE_ALERTA_BROTES ab where " +
        "exists (select CD_ANO,NM_ALERBRO from SIVE_INVESTIGADOR " +
        "where CD_USUARIO= ? and CD_ANO=b.CD_ANO and " +
        "NM_ALERBRO =b.NM_ALERBRO) " +
        "AND b.CD_ANO=ab.CD_ANO AND b.NM_ALERBRO=ab.NM_ALERBRO ";

    try {
      con.setAutoCommit(false);
      if (param.size() != 0) {
        dataEntrada = (Data) param.elementAt(0);
      }

      switch (opmode) {
        //Data q recibe: anoepini,semepiini,anoepifin,semepifin,cd_enfcie,cd_indalar
        case servletCONSULTABROTES:

          //comprobar q filtros lleva
          String sCdArea = dataEntrada.getString("CD_NIVEL_1");
          String sCdAno = dataEntrada.getString("CD_ANO");
          String sNmAlerbro = dataEntrada.getString("NM_ALERBRO");
          String sDsDescripcion = dataEntrada.getString("DS_BROTE");
          String sFechaDesde = dataEntrada.getString("FC_FECHAHORADESDE");
          String sFechaHasta = dataEntrada.getString("FC_FECHAHORAHASTA");
          String sCdGrupo = dataEntrada.getString("CD_GRUPO");
          String sCdSituacion = dataEntrada.getString("CD_SITALERBRO");

          if (! (sCdAno.equals(""))) {
            query = query + "and b.CD_ANO=? ";
          }
          if (! (sNmAlerbro.equals(""))) {
            query = query + "and b.NM_ALERBRO=? ";
          }
          if (! (sDsDescripcion.equals(""))) {
            query = query + "and b.DS_BROTE=? ";
          }
          if (! (sFechaDesde.equals(""))) {
            query = query + "and b.FC_FECHAHORA>? ";
          }
          if (! (sFechaHasta.equals(""))) {
            query = query + "and b.FC_FECHAHORA<? ";
          }
          if (! (sCdGrupo.equals(""))) {
            query = query + "and b.CD_GRUPO=? ";
          }
          if (! (sCdArea.equals(""))) {
            query = query + "and exists (select CD_ANO,NM_ALERBRO " +
                "from SIVE_ALERTA_BROTES " +
                "where CD_NIVEL_1= ? and CD_ANO=ab.CD_ANO and " +
                "NM_ALERBRO =ab.NM_ALERBRO) ";
          }
          if (! (sCdSituacion.equals(""))) {
            query = query + "and exists (select CD_ANO,NM_ALERBRO " +
                "from SIVE_ALERTA_BROTES " +
                "where CD_SITALERBRO= ? and CD_ANO=ab.CD_ANO and " +
                "NM_ALERBRO =ab.NM_ALERBRO) ";
          }

          // Preparacion de la sentencia SQL
          st = con.prepareStatement(query);

          st.setString(1, dataEntrada.getString("CD_USUARIO"));
          int i = 2;

          if (! (sCdAno.equals(""))) {
            st.setString(i, dataEntrada.getString("CD_ANO"));
            i++;
          }
          if (! (sNmAlerbro.equals(""))) {
            st.setInt(i,
                      (new Integer( (String) dataEntrada.getString("NM_ALERBRO"))).
                      intValue());
            i++;
          }
          if (! (sDsDescripcion.equals(""))) {
            st.setString(i, dataEntrada.getString("DS_BROTE"));
            i++;
          }
          if (! (sFechaDesde.equals(""))) {
            //dFecha = new java.util.Date();
            //String sFecha = Format_Horas.format(dFecha);//string
            String sFecha = dataEntrada.getString("FC_FECHAHORADESDE");
            //String sfecRecuAlerta = sFecha;
            st.setTimestamp(i, cadena_a_timestamp(sFecha));
            i++;
          }
          if (! (sFechaHasta.equals(""))) {
            String sFecha = dataEntrada.getString("FC_FECHAHORAHASTA");
            st.setTimestamp(i, cadena_a_timestamp(sFecha));
            i++;
          }
          if (! (sCdGrupo.equals(""))) {
            st.setString(i, dataEntrada.getString("CD_GRUPO"));
            i++;
          }
          if (! (sCdArea.equals(""))) {
            st.setString(i, dataEntrada.getString("CD_NIVEL_1"));
            i++;
          }
          if (! (sCdSituacion.equals(""))) {
            st.setString(i, dataEntrada.getString("CD_SITALERBRO"));
            i++;
          }

          // Ejecucion de la query, obteniendo un ResultSet
          rs = st.executeQuery();

          // Procesamiento de cada registro obtenido de la BD
          while (rs.next()) {
            String sCdArea1 = rs.getString("CD_NIVEL_1");
            String sCdAno1 = rs.getString("CD_ANO");
            String sNmAlerbro1 = (new Integer(rs.getInt("NM_ALERBRO"))).
                toString();
            String sDsDescripcion1 = rs.getString("DS_BROTE");
            String sFecha1 = timestamp_a_cadena(rs.getTimestamp("FC_FECHAHORA"));
            String sCdGrupo1 = rs.getString("CD_GRUPO");
            String sCdSituacion1 = rs.getString("CD_SITALERBRO");

            dataSalida = new Data();
            dataSalida.put("CD_NIVEL_1", sCdArea1);
            dataSalida.put("CD_ANO", sCdAno1);
            dataSalida.put("DS_BROTE", sDsDescripcion1);
            dataSalida.put("NM_ALERBRO", sNmAlerbro1);
            dataSalida.put("FC_FECHAHORA", sFecha1);
            dataSalida.put("CD_GRUPO", sCdGrupo1);
            dataSalida.put("CD_SITALERBRO", sCdSituacion1);

            // Adicion del registro obtenido a la lista resultado de salida hacia cliente
            listaSalida.addElement(dataSalida);

          } //end while
          break;
      } //end switch
      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      ex.printStackTrace();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);
    return listaSalida;
  } //end do work
}
