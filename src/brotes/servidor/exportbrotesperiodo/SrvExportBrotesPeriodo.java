package brotes.servidor.exportbrotesperiodo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;

public class SrvExportBrotesPeriodo
    extends DBServlet {
  final int servletCONSULTAR_BROTES = 1;

  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    //java.sql.Timestamp TSFec = new java.sql.Timestamp(dFecha.getTime());
    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);
    //# System.Out.println("ts : " + TSFec.toString());

    return TSFec;
  }

  // funcionalidad del servlet
  protected Lista doWork(int opmode, Lista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    Data dtParam = new Data();
    Lista lDev = new Lista();

    // establece la conexión con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    dtParam = (Data) param.firstElement();

    // modos de operación
    switch (opmode) {

      // indicador de modelo con respuestas
      case servletCONSULTAR_BROTES:
        query = "select CD_ANO,NM_ALERBRO, CD_GRUPO " +
            "from SIVE_BROTES " +
            "where FC_FECHAHORA>? and FC_FECHAHORA<? " +
            "and CD_GRUPO=?";
        st = con.prepareStatement(query);

        String sFechaDesde = dtParam.getString("FC_FECHAHORADESDE");
        String sFechaHasta = dtParam.getString("FC_FECHAHORAHASTA");
        st.setTimestamp(1, cadena_a_timestamp(sFechaDesde));
        st.setTimestamp(2, cadena_a_timestamp(sFechaHasta));
        st.setString(3, dtParam.getString("CD_GRUPO"));

        rs = st.executeQuery();

        // graba el indicador
        while (rs.next()) {
          Data dtLinea = new Data();
          dtLinea.put("CD_ANO", rs.getString("CD_ANO"));
          dtLinea.put("NM_ALERBRO", rs.getString("NM_ALERBRO"));
          dtLinea.put("CD_GRUPO", rs.getString("CD_GRUPO"));

          lDev.addElement(dtLinea);
        }

        // cierra los resulsets
        rs.close();
        st.close();

        break;
    } //switch

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return lDev;
  } //END DO_WORK
} //end