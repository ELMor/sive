/**
 * Clase: DBPanSup
 * Hereda: CPanel
 * Autor: Pedro Antonio D�az (PDP)
 * Fecha Inicio: 18/05/2000
 * Analisis Funcional: Informe Final
 * Descripcion: Panel superior del di�logo de alta/mod/baja del informe final
 */

package brotes.cliente.dbpansup;

import java.util.Calendar;
import java.util.Vector;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.CHora;
import brotes.cliente.c_componentes.ChoiceCDDS;
import brotes.cliente.c_componentes.DatoRecogible;
import brotes.cliente.c_comuncliente.BDatos;
import brotes.cliente.c_comuncliente.SincrEventos;
import brotes.cliente.diabif.DiaBIF;
import capp2.CApp;
import capp2.CContextHelp;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CListaValores;
import capp2.CPanel;
import capp2.CTabPanel;
import capp2.CTexto;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class DBPanSup
    extends CPanel
    implements CInicializar, DatoRecogible {

  /*************** Datos propios del panel ****************/

  // Parametros de entrada
  CApp applet = null;
  DiaBIF dlg = null;
  Data brote = null;
  Lista lGrupo = null; // Lista Grupos
  Lista lTbro = null; // Lista Tipos de brotes

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  // Constantes del modoEntrada
  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;

  // Nombres de servlets
  public final static String srvQT = "servlet/SrvQueryTool";

  /****************** Componentes del panel ********************/

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Organizaci�n del panel
  private XYLayout xYLayout1 = new XYLayout();

  // Ano/Cod Brote
  Label lblAno = null;
  CEntero txtAno = null;
  boolean anoValid = false;
  CEntero txtCod = null;
  ButtonControl btnBrote = null;
  CTexto txtDesc = null;

  // Fecha/Hora de la notificaci�n
  Label lblFNotif = new Label();
  fechas.CFecha txtFNotif = new fechas.CFecha("S");
  CHora txtHNotif = new CHora("S");

  // Grupo de brote
  Label lblGrupo = null;
  ChoiceCDDS choGrupo = null;

  // Tipo de brote
  Label lblTbro = null;
  ChoiceCDDS chotBro = null;

  // Escuchadores (iniciados a null)

  // Botones
  DBPanSupActionAdapter actionAdapter = null;

  // Key Pressed
  DBPanSupKeyListener keyListener = null;

  // Choices
  DBPanSupItemListener itemListener = null;

  // Perdidas de foco
  DBPanSupFocusAdapter focusAdapter = null;

  // Escuchadores (iniciados a null)
  // Constructor
  // @param a: CApp
  // @param modo: modo de entrada al di�logo ()
  // @param listaGrupo: Lista Datas con CD y DS de los grupos
  // @param listaSit: Lista Datas con CD y DS de las situaciones
  public DBPanSup(CApp a, DiaBIF d, Data bro, int modo, Lista listaGrupo,
                  Lista listaTBrotes) {
    super(a);
    applet = a;
    dlg = d;
    brote = bro;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    lGrupo = listaGrupo;
    lTbro = listaTBrotes;
    try {
      // Iniciaci�n
      jbInit();
      Inicializar(modoOperacion);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicia el aspecto del panel superior del informe final
  public void jbInit() throws Exception {
    // Variables locales
    boolean nulo, bCD, bDS;
    String sCD, sDS;
    Color cObligatorio = new Color(255, 255, 150);

    // Un poco elevado
    setBorde(true);

    // Carga de imagenes
    getApp().getLibImagenes().put(constantes.imgLUPA);
    getApp().getLibImagenes().CargaImagenes();

    // Escuchadores: botones y presionado de teclado
    actionAdapter = new DBPanSupActionAdapter(this);
    keyListener = new DBPanSupKeyListener(this);
    itemListener = new DBPanSupItemListener(this);
    focusAdapter = new DBPanSupFocusAdapter(this);

    // Organizacion del panel
    this.setSize(new Dimension(725, 80));
    this.setLayout(xYLayout1);
    xYLayout1.setWidth(725);
    xYLayout1.setHeight(80);

    // Ano/Cod Brote
    lblAno = new Label();
    lblAno.setText("A�o-C�digo Brote:");
    txtAno = new CEntero(4);
    txtAno.setBackground(cObligatorio);
    if (modoEntrada == ALTA) {
      int iYearAct = 0;
      Calendar cal = Calendar.getInstance();
      iYearAct = cal.get(cal.YEAR);
      if (! (this.getApp().getParametro("ANYO_DEFECTO").equals(""))) {
        txtAno.setText(this.getApp().getParametro("ANYO_DEFECTO"));
      }
      else {
        txtAno.setText(new Integer(iYearAct).toString());
      }
    }

    txtAno.addKeyListener(keyListener);
    txtAno.setName("ano");
    if (modoEntrada == ALTA) {
      anoValid = false;
    }
    else {
      anoValid = true;
    }
    txtCod = new CEntero(7);
    txtCod.setBackground(cObligatorio);
    txtCod.setName("CodBrote");
    txtCod.addFocusListener(focusAdapter);
    btnBrote = new ButtonControl();
    btnBrote.setImage(this.getApp().getLibImagenes().get(constantes.imgLUPA));
    btnBrote.setActionCommand("Brote");
    btnBrote.addActionListener(actionAdapter);
    txtDesc = new CTexto(50);
    txtDesc.setBackground(cObligatorio);

    // Fecha/Hora de la notificaci�n
    lblFNotif = new Label("F-H. Notificaci�n:");
    txtFNotif = new fechas.CFecha("S");
    txtHNotif = new CHora("S");

    // Grupo de brote y Tipo del brote
    nulo = true;
    bCD = false;
    bDS = true;
    sCD = "CD_GRUPO";
    sDS = "DS_GRUPO";
    lblGrupo = new Label("Grupo:");
    choGrupo = new ChoiceCDDS(nulo, bCD, bDS, sCD, sDS, lGrupo);
    choGrupo.setBackground(cObligatorio);
    choGrupo.writeData();
    choGrupo.setName("grupo");
    choGrupo.addItemListener(itemListener);
    nulo = true;
    bCD = false;
    bDS = true;
    sCD = "CD_TBROTE";
    sDS = "DS_TBROTE";
    lblTbro = new Label("Tipo Brote:");
    chotBro = new ChoiceCDDS(nulo, bCD, bDS, sCD, sDS, lTbro);
    chotBro.setBackground(cObligatorio);
    chotBro.writeData();

    // Adici�n de componentes al di�logo
    int dVert = 20;

    this.add(lblAno, new XYConstraints(15, 15, 105, 20));
    this.add(txtAno, new XYConstraints(120, 15, 45, 20));
    this.add(txtCod, new XYConstraints(175, 15, 75, 20));
    this.add(btnBrote, new XYConstraints(260, 15, 25, 25));
    this.add(txtDesc, new XYConstraints(295, 15, 175, 20));

    this.add(lblFNotif, new XYConstraints(480, 14, 94, 20));
    this.add(txtFNotif, new XYConstraints(575, 14, 80, 20));
    this.add(txtHNotif, new XYConstraints(660, 14, 50, 20));

    this.add(lblGrupo, new XYConstraints(15, 47, 40, 20));
    this.add(choGrupo, new XYConstraints(60, 47, 280, 20));
    this.add(lblTbro, new XYConstraints(380, 48, 60, 20));
    this.add(chotBro, new XYConstraints(450, 48, 262, 20));

    repaint();
    // ARS 13-07-01
    // si estamos en alta, dehabilita la lista de tipo de brotes.
    if (modoEntrada == ALTA) {
      chotBro.setEnabled(false);
      // Tool Tips
    }
    new CContextHelp("Brotes", btnBrote);
  } // Fin jbInit()

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    if (dlg != null) {
      dlg.Inicializar(modoOperacion);
    }
  } // Fin Iniciar()

  public void InicializarDesdeFuera(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicia()

  public void Inicializar() {
    switch (modoOperacion) {
      // Modo espera
      case CInicializar.ESPERA:
        enableControls(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // Modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case ALTA:
            enableControls(true);
            // Cambio ARS 13-07-01; depende de si hay o no datos.
            if (choGrupo.getChoiceCD().trim().length() > 0) {
              chotBro.setEnabled(true);
            }
            else {
              chotBro.setEnabled(false);
            }
            break;
          case MODIFICACION:
            enableControls(true);
            // Grupo de brote no es modificable
            choGrupo.setEnabled(false);

            /*int bro = new Integer(brote.getString("CD_TBROTE")).intValue();
                         switch(bro){
                         case 0:
              enableControls(true);
              break;
                         case 1:case 2:case 3:case 4:
              enableControls(false);
              // Situaci�n del brote en estado 1 habilitado
              if(bro == 1)
                chotBro.setEnabled(true);
              break;
                         } */

            break;
          case BAJA:
          case CONSULTA:
            enableControls(false);
            // Grupo de brote no es modificable
            choGrupo.setEnabled(false);
            break;
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    } // Fin switch
  } // Fin Iniciar()

  public void enableControls(boolean state) {

    // Ano/Cod Brote
    if (modoEntrada == ALTA) {
      txtAno.setEnabled(state && true);
      txtCod.setEnabled(state && true);
      btnBrote.setEnabled(state && true);
    }
    else {
      txtAno.setEnabled(state && false);
      txtCod.setEnabled(state && false);
      btnBrote.setEnabled(state && false);
    }
    txtDesc.setEnabled(state);

    // Fecha/Hora de la notificaci�n
    txtFNotif.setEnabled(state);
    txtHNotif.setEnabled(state);

    // Grupo de brote
    choGrupo.setEnabled(state);

    // Tipo de brote
    chotBro.setEnabled(state);

  } // Fin enableControls()

  /********** Sincronizador de Eventos ******************/

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  /********** Funciones Interfaz DatoRecogible *****************/

  public void rellenarDatos(Data d) {

    // Ano/Cod Brote
    txtAno.setText( (String) d.getString("CD_ANO"));
    txtCod.setText( (String) d.getString("NM_ALERBRO"));
    txtDesc.setText( (String) d.getString("DS_BROTE"));

    // Fecha/Hora de la notificaci�n
    String f = ( (String) d.getString("FC_FECHAHORA")).trim();
    if (!f.equals("")) {
      txtFNotif.setText(f.substring(0, f.indexOf(' ', 0)));
      txtHNotif.setText( (f.substring(f.indexOf(' ', 0) + 1)).substring(0, 5));
    }

    // Grupo de brote
    choGrupo.selectChoiceElement( (String) d.getString("CD_GRUPO"));

    // Cambio ARS 13-07-01
    Lista l = filtraGrupoBrote( (String) d.getString("CD_GRUPO"), lTbro);
    chotBro.removeAll();
    chotBro.setLista(l);
    chotBro.writeData();
    // Fin cambio ARS.

    // Tipo de brote
    chotBro.selectChoiceElement( (String) d.getString("CD_TBROTE"));
  } // Fin rellenarDatos()

  public void recogerDatos(Data d) {
    d.put("CD_ANO", txtAno.getText().trim());
    d.put("NM_ALERBRO", txtCod.getText().trim());
    d.put("DS_BROTE", txtDesc.getText().trim());
    String fExp = "";
    if (!txtFNotif.getText().equals("")) {
      fExp = txtFNotif.getText().trim();
      if (!txtHNotif.getText().equals("")) {
        fExp += " " + txtHNotif.getText().trim() + ":00";
      }
      else {
        fExp += " 00:00:00";
      }
    }
    d.put("FC_FECHAHORA", fExp);
    d.put("CD_GRUPO", choGrupo.getChoiceCD());
    d.put("CD_TBROTE", chotBro.getChoiceCD());
  } // Fin recogerDatos()

  public boolean validarDatos() {
    /******* Campos obligatorios ********/

    // Ano/Cod Brote
    if (txtAno.getText().equals("") || (!anoValid)) {
      this.getApp().showError("El a�o/c�digo es un campo obligatorio");
      txtAno.requestFocus();
      return (false);
    }

    if (txtDesc.getText().equals("")) {
      this.getApp().showError("La descripci�n es un campo obligatorio");
      txtDesc.requestFocus();
      return (false);
    }

    // Fecha/Hora de la notificaci�n
    if (txtFNotif.getText().equals("")) {
      this.getApp().showError("La fecha es un campo obligatorio");
      txtFNotif.requestFocus();
      return (false);
    }
    if (txtHNotif.getText().equals("")) {
      this.getApp().showError("La hora es un campo obligatorio");
      txtHNotif.requestFocus();
      return (false);
    }

    // Grupo de brote
    if (choGrupo.getChoiceCD().equals("")) {
      this.getApp().showError("El grupo es un campo obligatorio");
      choGrupo.requestFocus();
      return (false);
    }

    // Tipo de brote
    if (chotBro.getChoiceCD().equals("")) {
      this.getApp().showError("El tipo de brote es un campo obligatorio");
      chotBro.requestFocus();
      return (false);
    }

    /******* Fin Campos obligatorios ********/

    /*********** Tipos de datos ************/

    // Fecha Notificaci�n debe ser una fecha v�lida
    txtFNotif.ValidarFecha();
    if (txtFNotif.getValid().equals("N")) {
      this.getApp().showError("Fecha incorrecta");
      txtFNotif.requestFocus();
      return (false);
    }

    // La Hora Notificaci�n debe ser una fecha v�lida
    txtHNotif.ValidarFecha();
    if (txtHNotif.getValid().equals("N")) {
      this.getApp().showError("Hora incorrecta");
      txtHNotif.requestFocus();
      return (false);
    }

    /*********** Fin Tipos de datos ************/

    return true;
  } // Fin validarDatos()

  /****************** Funciones Auxiliares *****************/

  public void cambiarBIF(Data newBIF) {
    brote = newBIF;
  } // Fin cambiarAlerta()

  public void setModoEntrada(int m) {
    modoEntrada = m;
  } // Fin setModoEntrada()

  /****************** Manejadores de Eventos *****************/
  void btnBroteActionPerformed() {
    seleccionarAlertas();
    anoValid = true;
  } // Fin btnAreaActionPerformed()

  private void seleccionarAlertas() {
    CListaValores clv = null;
    Vector vCod = null;
    QueryTool2 qt = new QueryTool2(); ;
    String ano = "";

    ano = txtAno.getText().trim();
    if (ano.equals("")) {
      applet.showAdvise("Debe introducir un a�o");
      txtAno.requestFocus();
      return;
    }

    qt.putName("SIVE_ALERTA_BROTES");
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("DS_ALERTA", QueryTool.STRING);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", ano);
    qt.putOperator("CD_ANO", "=");
    qt.putWhereType("CD_SITALERBRO", QueryTool.STRING);
    qt.putWhereValue("CD_SITALERBRO", "0");
    qt.putOperator("CD_SITALERBRO", "=");

    // Campos que se muestran en la lista de valores
    vCod = new Vector();
    vCod.addElement("NM_ALERBRO");
    vCod.addElement("DS_ALERTA");

    // Lista de valores
    clv = new CListaValores(this.getApp(),
                            "Alertas disponibles",
                            qt,
                            vCod);
    clv.show();

    // Recupera el �rea seleccionada
    if (clv.getSelected() != null) {
      txtDesc.setText( ( (String) clv.getSelected().getString("DS_ALERTA")));
      txtCod.setText(clv.getSelected().getString("NM_ALERBRO"));
    }

    clv = null;
  } // Fin seleccionarAlertas

  void txtValueChangeAno() {
    anoValid = false;
    txtCod.setText("");
    txtDesc.setText("");
  } // Fin txtValueChangeAno()

  void itemStateChangedGrupo() {
    String choice = choGrupo.getChoiceCD();
    // Cambio ARS 13-07-01.
    if (modoEntrada == ALTA) {
      if (choice.length() > 0) {
        chotBro.setEnabled(true);
        Lista l = filtraGrupoBrote(choice, lTbro);
        chotBro.removeAll();
        chotBro.setLista(l);
        chotBro.writeData();
      }
      else {
        chotBro.removeAll();
        chotBro.setEnabled(false);
      }
    } // del if de alta.

    if (choGrupo.getChoiceCD().equals("0")) {
      dlg.setVisibleVacunable(false);
      dlg.setVisibleToxAlim(true);
    }
    else if (choGrupo.getChoiceCD().equals("1")) {
      dlg.setVisibleToxAlim(false);
      dlg.setVisibleVacunable(true);
    }
    else {
      dlg.setVisibleToxAlim(false);
      dlg.setVisibleVacunable(false);
    }

    CTabPanel pActual = dlg.getTabPanel();
    if (pActual.getSolapaActual().equals("Datos B�sicos")) {
      pActual.VerPanel("Descripci�n Temporal");
      pActual.VerPanel("Datos B�sicos");
    }
  } // Fin itemStateChangedGrupo()

  void txtCodBroteFocusLost() {
    // Si es modo alta se comprueba la validez del codigo de alerta
    if (modoEntrada == ALTA) {
      comprobarAlerta();
      anoValid = true;
    }
  } // Fin txtCodBroteFocusLost()

  private void comprobarAlerta() {
    QueryTool2 qt = new QueryTool2(); ;
    String ano = "";
    String nm_alerbro = "";
    Lista lParam = new Lista();
    Lista lResul = null;

    try {

      ano = txtAno.getText().trim();
      if (ano.equals("")) {
        applet.showAdvise("Debe introducir un a�o");
        txtAno.requestFocus();
        return;
      }

      nm_alerbro = txtCod.getText().trim();
      if (nm_alerbro.equals("")) {
        return;
      }

      qt.putName("SIVE_ALERTA_BROTES");
      qt.putType("CD_ANO", QueryTool.STRING);
      qt.putType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putType("DS_ALERTA", QueryTool.STRING);

      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", ano);
      qt.putOperator("CD_ANO", "=");
      qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putWhereValue("NM_ALERBRO", nm_alerbro);
      qt.putOperator("NM_ALERBRO", "=");
      qt.putWhereType("CD_SITALERBRO", QueryTool.STRING);
      qt.putWhereValue("CD_SITALERBRO", "0");
      qt.putOperator("CD_SITALERBRO", "=");

      lParam.addElement(qt);
      lResul = BDatos.execSQL(applet, srvQT, 1, lParam); // 1 -> DO_SELECT

      if (lResul.size() != 1) {
        applet.showError("La alerta introducida no es valida");
        return;
      }

    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }

    // Recupera el �rea seleccionada
    txtDesc.setText( ( (Data) lResul.firstElement()).getString("DS_ALERTA"));

  } // Fin comprobarAlerta()

  // Para recuperar el grupo de brote ARS 12-07-01.
  private Lista filtraGrupoBrote(String codBrote, Lista lEntrada) {
    Data dtTemp = new Data();
    Lista lSalida = new Lista();
    for (int i = 0; i < lEntrada.size(); i++) {
      dtTemp = (Data) lEntrada.elementAt(i);
      if (dtTemp.getString("CD_GRUPO").equals(codBrote)) {
        lSalida.addElement(dtTemp);
      }
    } // Fin del for
    return lSalida;
  }

  // Fin del filtrado.

} // Fin DBPanSup

// Botones
class DBPanSupActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DBPanSup adaptee;
  ActionEvent evt;

  DBPanSupActionAdapter(DBPanSup adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if (evt.getActionCommand().equals("Brote")) {
        adaptee.btnBroteActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  DAPanSupActionAdapter

// Tecla presionada
class DBPanSupKeyListener
    implements java.awt.event.KeyListener, Runnable {
  DBPanSup adaptee = null;
  KeyEvent evt = null;

  DBPanSupKeyListener(DBPanSup adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(java.awt.event.KeyEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void keyTyped(java.awt.event.KeyEvent e) {}

  public void keyReleased(java.awt.event.KeyEvent e) {}

  public void run() {
    String name2 = ( (Component) evt.getSource()).getName();
    if (name2.equals("ano")) {
      adaptee.txtValueChangeAno();
    }
  }
} // Fin clase DBPanSupKeyListener

// Cambio en choices
class DBPanSupItemListener
    implements java.awt.event.ItemListener, Runnable {
  DBPanSup adaptee;
  ItemEvent evt;

  DBPanSupItemListener(DBPanSup adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      String name = ( (Component) evt.getSource()).getName();
      if (name.equals("grupo")) {
        adaptee.itemStateChangedGrupo();
      }
      s.desbloquea(adaptee);
    }
  }

} // endclass  DBPanSupItemListener

// P�rdidas de foco
class DBPanSupFocusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  DBPanSup adaptee;
  FocusEvent evt;

  DBPanSupFocusAdapter(DBPanSup adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if ( ( (TextField) evt.getSource()).getName().equals("CodBrote")) {
        adaptee.txtCodBroteFocusLost();
      }
      s.desbloquea(adaptee);
    }
  }
} // Fin clase DBPanSupFocusAdapter
