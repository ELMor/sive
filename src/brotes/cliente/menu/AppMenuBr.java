/**
 * Clase: AppMenuBr
 * Paquete: brotes.cliente.menu
 * Fecha Inicio: 29/02/2000
 * 03/05/2000 (JMT): Reestructuraci�n/Limpieza
 */

package brotes.cliente.menu;

import java.net.URL;
import java.util.StringTokenizer;

import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//import passwordusu.DiaPassword2;
// Para zonas por defecto, a�o y perfil
import brotes.datos.menu.ParametrosBrUsu;
import capp2.CApp;
import capp2.CLogin;
import capp2.CMenuPanel;
import capp2.UMenu;
import capp2.UMenuItem;
import sapp2.Data;

public class AppMenuBr
    extends CApp
    implements ActionListener {
  // Para que siempre exista una instancia de capp.CApp
  // y se mantenga el valor de sus variables est�ticas
  public static capp.CApp AppletEterno = null;

  // applets
  // Alertas
  final int applet_ALERTA = 80;

  // Informe final
  final int applet_BROTES = 81;

  /*// Otros datos del informe final del brote
    final int applet_OTROSINFBROTE = 1069;*/

 // Parametrizaci�n
 final int appletMODELOS_GRUPO_BROTE = 1087;
  final int appletMODELOS_BROTE = 88;
  final int appletPREG_BROTE = 89;
  final int appletLISTAS = 6;

  // Exportaciones
  final int appletINF_UN_BROTE = 41;
//  final int appletPERS_BROTE =  42;
  final int appletINF_BROTES = 43;

  //Env�os
  final int appletENVIOS = 51;

  // Administraci�n
  // Mantenimiento
  final int appletGRUPO_EDAD = 38; // Grupos de Edades
  final int appletEPI = 39; // Servicio de epidemiolog�a
  final int appletUSR = 35; // Usuarios
  final int appletGRUPOS = 700; //Grupos
  // Localizaci�n
  final int appletCCAA = 14;
  final int appletPROV = 15;
  final int appletNIVEL1 = 11;
  final int appletNIVEL2 = 12;
  final int appletZBS = 13;
  // Procesos epidemiol�gicos
  final int appletCIE = 9;
  final int appletPROCESOS = 10;
  final int appletEDO_CNE = 8;
  final int appletEDO = 1;
  // Mtos. generales de EDO
  final int appletTLINEA = 17;
  final int appletTPREG = 18;
  final int appletTSIVE = 19;
  final int appletTVIGI = 20;
  final int appletMOTBAJA = 21;
  final int appletNIVASIS = 22;
  final int appletSEXO = 23;
  final int appletDIAGNOSTICO = 61;
  // Mtos. brotes
  final int applet_MNTO_GRUPO_BROTE = 82;
  final int applet_MNTO_TIPO_NOTIFICADOR = 83;
  final int applet_MNTO_MECANISMOS = 84;
  final int applet_MNTO_COLECTIVOS = 85;
  final int applet_MNTO_SITUACION = 86;
  final int applet_MNTO_TIPO_BROTE = 87;
  // Mtos. tablas perifericas a SIVE_BROTES
  final int applet_MNTO_ORIGEN_MUESTRAS = 200;
  final int applet_MNTO_SINTOMAS = 201;
  final int applet_MNTO_AGCAUSALES = 203;
  final int applet_MNTO_FACCONT = 204;
  final int applet_MNTO_MEDIDAS = 205;
  final int applet_MNTO_GRUPOS_AGTOXICOS = 206;
  // Mtos. tablas perifericas alimentos
  final int applet_MNTO_TALIMENTOS = 202;
  final int applet_MNTO_TIMPL = 207;
  final int applet_MNTO_MCOMERC = 208;
  final int applet_MNTO_TRATPREV = 209;
  final int applet_MNTO_FINGER = 210;
  final int applet_MNTO_LCONTAMI = 211;
  final int applet_MNTO_LPREPAR = 212;
  final int applet_MNTO_LCONSUMO = 213;
  final int applet_MNTO_MTVIAJE = 214;

  //General
  final int applet_SEL_ZONIFICACION = 2000;

  //Ayuda
  final int ayuda_ACERCA_D = 1000;
  final int ayuda_INF_BROTES = 1001;

  // Di�logo para solicitar usuario/password
  private static CLogin dlg = null;

  // Par�metros 'antiguos'
  private ParametrosBrUsu ptu = null;

  // Objetos del men�
  CMenuPanel mPanel = new CMenuPanel();
  MenuBar mBar = new MenuBar();

  public void destroy() {
    if (dlg != null) {
      dlg.dispose();
      dlg = null;
    }

  }

  public void stop() {
    if (dlg != null) {
      dlg.dispose();
      dlg = null;
    }

  }

  public void init() {
    super.init();

    try {
      AppletEterno = new capp.CApp();

      if (dlg != null) {
        dlg.dispose();
        dlg = null;
      }

      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    Data dtAut = null;

    // Solicita las acreditaciones del usuario

    dlg = new CLogin(this, "servlet/SrvBrAut",
                     "Control de Acceso [" + getParameter("COD_APLICACION") +
                     "]");
    dlg.show();
    dtAut = dlg.getAutorizaciones();

    // modificacion jlt para recuperar el url_servlet y
    // el url_html
    String ur = new String();
    String ur2 = new String();
    String ur3 = new String();
    String ur4 = new String();
    int ind = 0;
    int indb = 0;
    ur = (String)this.getDocumentBase().toString();
    ur = "http://nticmdes02/sip/sive_sup_edo.html";
    ind = ur.lastIndexOf("/");
    ur2 = (String) ur.substring(0, ind + 1);
    ur3 = (String) ur2.substring(0, ind);
    indb = ur3.lastIndexOf("/");
    ur4 = (String) ur3.substring(0, indb + 1);
    System.out.println("urmenu2" + ur2);
    System.out.println("urmenu4" + ur4);
    /////////////////

    setAutorizaciones(dtAut);

    // Si el usuario est� acreditado tiene acceso
    if (dtAut != null) {
      // Recupera los par�metros 'antiguos'
      ptu = (ParametrosBrUsu) dtAut.get("PARAMETROS_BRO_USU");

      // Prepara la estructura de par�metros para solicitar los applets
      /*      dtParametros = new Data();
            dtParametros.put("IT_USU", parseAutorizaciones());
            dtParametros.put("COD_APLICACION", getParametro("COD_APLICACION"));
            dtParametros.put("URL_SERVLET", getParametro("URL_SERVLET"));
            dtParametros.put("URL_HTML", getParametro("URL_HTML"));
            dtParametros.put("COD_USUARIO", dtAut.getString("COD_USUARIO"));
            dtParametros.put("FC_ACTUAL", dtAut.getString("FC_ACTUAL"));
       */

      ///////////////////   modificacion jlt
      if (getParametro("URL_SERVLET").equals("") ||
          getParametro("URL_SERVLET").equals("/")
          || getParametro("URL_SERVLET").equals(null)) {

        ptu.put("URL_SERVLET", ur4);
      }
      else {
        ptu.put("URL_SERVLET", getParametro("URL_SERVLET"));
      }

      //ptu.put("URL_SERVLET", getParametro("URL_SERVLET"));

      if (getParametro("URL_HTML").equals("") ||
          getParametro("URL_HTML").equals("/")
          || getParametro("URL_HTML").equals(null)) {

        ptu.put("URL_HTML", ur2);
      }
      else {
        ptu.put("URL_HTML", getParametro("URL_HTML"));
      }

      //ptu.put("URL_HTML", getParametro("URL_HTML"));

      ptu.put("IT_USU", parseAutorizaciones());
      ptu.put("COD_APLICACION", getParametro("COD_APLICACION"));
      ptu.put("IT_MODULO_3", getParametro("IT_MODULO_3"));
      //ptu.put("CD_TUBERCULOSIS", getParametro("CD_TUBERCULOSIS"));

      mPanel.removeAll();
      mBar = new MenuBar();

      // Alertas
      if (isAccesible("1")) {
        UMenu m1 = new UMenu("Alertas", "1", this);
        mBar.add(m1);

        // Comprueba si esta cerrada
        if (isAllowed("1")) {
          UMenuItem mi11 = new UMenuItem("Alertas de brote", "11", 11, this, this);
          m1.add(mi11);

        } // Fin if(allowed "1")
      } // Fin if(accesible "1")

      // Informe Final
      if (isAccesible("2")) {
        UMenu m2 = new UMenu("Informe final", "2", this);
        mBar.add(m2);

        // Comprueba si esta cerrada
        if (isAllowed("2")) {
          UMenuItem mi21 = new UMenuItem("Informe final de brote", "21", 21, this, this);
          m2.add(mi21);
        } // Fin if(allowed "2")
      } // Fin if(accesible "2")

      // Parametrizaci�n
      if (isAccesible("3")) {
        UMenu m3 = new UMenu("Parametrizaci�n", "3", this);
        mBar.add(m3);

        // Comprueba si esta cerrada
        if (isAllowed("3")) {
          /*UMenuItem mi31 = new UMenuItem("Mantenimiento de modelos", "31", 31, this, this);
                     m3.add(mi31);*/
          UMenu m31 = new UMenu("Mantenimiento de modelos", "31", this);
          if (isAccesible("31")) {
            m3.add(m31);

            // Modelos Grupo Brote
            UMenuItem mi311 = new UMenuItem("Modelos Grupo Brote", "311", 311, this, this);
            m31.add(mi311);

            // Modelos Brote
            UMenuItem mi312 = new UMenuItem("Modelos Brote", "312", 312, this, this);
            m31.add(mi312);
          } // Fin if(accesible "31")

          UMenuItem mi32 = new UMenuItem("Mantenimiento de preguntas", "32", 32, this, this);
          m3.add(mi32);

          UMenuItem mi33 = new UMenuItem("Mantenimiento de listas", "33", 33, this, this);
          m3.add(mi33);
        } // Fin if(allowed "3")
      } // Fin if(accesible "3")

      // Exportaciones
      if (isAccesible("4")) {
        UMenu m4 = new UMenu("Exportaciones", "4", this);
        mBar.add(m4);

        // Comprueba si esta cerrada
        if (isAllowed("4")) {
          UMenuItem mi41 = new UMenuItem("Informaci�n general de un brote",
                                         "41", 41, this, this);
          m4.add(mi41);

          /*          UMenuItem mi42 = new UMenuItem("Personas estudiadas de un brote", "42", 42, this, this);
                    m4.add(mi42);*/

          UMenuItem mi43 = new UMenuItem("Informaci�n general de brotes", "43",
                                         43, this, this);
          m4.add(mi43);

        } //fin if(isAllowed("4"))
      } // fin if(isAccesible("4"))

      if (isAccesible("5")) {
        UMenu m5 = new UMenu("Env�os", "5", this);
        mBar.add(m5);

        // Comprueba si esta cerrada
        if (isAllowed("5")) {
          UMenuItem mi51 = new UMenuItem("Env�os al CNE", "51", 51, this, this);
          m5.add(mi51);
          mi51.setEnabled(false);
        } // Fin if(allowed "5")
      } // Fin if(accesible "5")

      // Mantenimientos
      if (isAccesible("6")) {
        UMenu m6 = new UMenu("Administraci�n", "6", this);
        mBar.add(m6);

        // Comprueba si esta cerrada
        if (isAllowed("6")) {

          // Grupos de Edades
          UMenuItem mi60 = new UMenuItem("Grupos de Edades", "60", 60, this, this);
          m6.add(mi60);

          // Servicio de epidemiolog�a
          UMenuItem mi61 = new UMenuItem("Servicio de epidemiolog�a", "61", 61, this, this);
          m6.add(mi61);

          // Separador
          MenuItem miSep = new MenuItem("-");
          m6.add(miSep);

          // Grupos de trabajo
          UMenuItem mi62 = new UMenuItem("Grupos de trabajo", "62", 62, this, this);
          m6.add(mi62);

          // Tablas de c�digos
          UMenuItem mi63 = new UMenuItem("Usuarios", "63", 63, this, this);
          m6.add(mi63);

          //Separador
          miSep = new MenuItem("-");
          m6.add(miSep);

          if (isAccesible("64")) {
            UMenu m64 = new UMenu("Tablas de c�digos", "64", this);
            m6.add(m64);

            // Localizacion
            UMenu m641 = new UMenu("Localizaci�n", "641", this);
            m64.add(m641);
            UMenuItem mi6411 = new UMenuItem("Comunidades Aut�nomas", "6411",
                                             6411, this, this);
            m641.add(mi6411);
            UMenuItem mi6412 = new UMenuItem("Provincias", "6412", 6412, this, this);
            m641.add(mi6412);
            UMenuItem mi6413 = new UMenuItem("�reas", "6413", 6413, this, this);
            m641.add(mi6413);
            UMenuItem mi6414 = new UMenuItem("Distritos", "6414", 6414, this, this);
            m641.add(mi6414);
            UMenuItem mi6415 = new UMenuItem("Zonas", "6415", 6415, this, this);
            m641.add(mi6415);

            // Separador
            miSep = new MenuItem("-");
            m64.add(miSep);

            // Procesos epidemiol�gicos
            UMenu m642 = new UMenu("Procesos epidemiol�gicos", "642", this);
            m64.add(m642);
            UMenuItem mi6421 = new UMenuItem("Enfermedades CIE", "6421", 6421, this, this);
            m642.add(mi6421);
            UMenuItem mi6422 = new UMenuItem("Procesos", "6422", 6422, this, this);
            m642.add(mi6422);
            UMenuItem mi6423 = new UMenuItem("Enfermedaes EDO CNE", "6423",
                                             6423, this, this);
            m642.add(mi6423);
            UMenuItem mi6424 = new UMenuItem("Enfermedades EDO", "6424", 6424, this, this);
            m642.add(mi6424);

            // Separador
            miSep = new MenuItem("-");
            m64.add(miSep);

            UMenuItem mi643 = new UMenuItem("Tipos SIVE", "643", 643, this, this);
            m64.add(mi643);
            UMenuItem mi644 = new UMenuItem("Tipos de l�nea", "644", 644, this, this);
            m64.add(mi644);
            UMenuItem mi645 = new UMenuItem("Tipos de pregunta", "645", 645, this, this);
            m64.add(mi645);
            UMenuItem mi646 = new UMenuItem("Tipos de vigilancia", "646", 646, this, this);
            m64.add(mi646);
            UMenuItem mi647 = new UMenuItem("Motivos de baja", "647", 647, this, this);
            m64.add(mi647);
            UMenuItem mi648 = new UMenuItem("Niveles asistenciales", "648", 648, this, this);
            m64.add(mi648);
            UMenuItem mi649 = new UMenuItem("Sexo", "649", 649, this, this);
            m64.add(mi649);
            UMenuItem mi64010 = new UMenuItem("Clasificaci�n diagn�stica",
                                              "64010", 64010, this, this);
            m64.add(mi64010);

            // Tablas de c�digos contin�a en otra lista desplegable.
            // con c�digo m643
            UMenu m643 = new UMenu("Otros ...", "643", this);
            m64.add(m643);

            UMenuItem mi64011 = new UMenuItem("Grupos Brotes", "64011", 64011, this, this);
            m643.add(mi64011);
            UMenuItem mi64012 = new UMenuItem("Tipos Notificadores", "64012",
                                              64012, this, this);
            m643.add(mi64012);
            UMenuItem mi64013 = new UMenuItem("Mecanismos de Transmisi�n",
                                              "64013", 64013, this, this);
            m643.add(mi64013);
            UMenuItem mi64014 = new UMenuItem("Colectivos", "64014", 64014, this, this);
            m643.add(mi64014);
            UMenuItem mi64015 = new UMenuItem("Situaci�n Alerta", "64015",
                                              64015, this, this);
            m643.add(mi64015);
            UMenuItem mi64016 = new UMenuItem("Tipos de Brotes", "64016", 64016, this, this);
            m643.add(mi64016);
            // Separador
            miSep = new MenuItem("-");
            m643.add(miSep);
            UMenuItem mi64017 = new UMenuItem("Or�genes de Muestras", "64017",
                                              64017, this, this);
            m643.add(mi64017);
            UMenuItem mi64018 = new UMenuItem("S�ntomas", "64018", 64018, this, this);
            m643.add(mi64018);

            // Alimentos
            UMenu m64019 = new UMenu("Alimentos", "64019", this);
            m643.add(m64019);
            UMenuItem mi640191 = new UMenuItem("Tipos de Alimentos", "640191",
                                               640191, this, this);
            m64019.add(mi640191);
            UMenuItem mi640192 = new UMenuItem("Tipos de Implicaciones",
                                               "640192", 640192, this, this);
            m64019.add(mi640192);
            UMenuItem mi640193 = new UMenuItem("M�todos de Comercializaci�n",
                                               "640193", 640193, this, this);
            m64019.add(mi640193);
            UMenuItem mi640194 = new UMenuItem("Tratamientos Previos", "640194",
                                               640194, this, this);
            m64019.add(mi640194);
            UMenuItem mi640195 = new UMenuItem("Formas de Ingerir", "640195",
                                               640195, this, this);
            m64019.add(mi640195);
            UMenuItem mi640196 = new UMenuItem("Lugares de Contaminaci�n",
                                               "640196", 640196, this, this);
            m64019.add(mi640196);
            UMenuItem mi640197 = new UMenuItem("Lugares Preparaci�n", "640197",
                                               640197, this, this);
            m64019.add(mi640197);
            UMenuItem mi640198 = new UMenuItem("Lugares de Consumo", "640198",
                                               640198, this, this);
            m64019.add(mi640198);
            UMenuItem mi640199 = new UMenuItem("M�todos de Transporte",
                                               "640199", 640199, this, this);
            m64019.add(mi640199);

            UMenuItem mi64020 = new UMenuItem("Agentes Causales", "64020",
                                              64020, this, this);
            m643.add(mi64020);
            UMenuItem mi64021 = new UMenuItem("Factores Contribuyentes",
                                              "64021", 64021, this, this);
            m643.add(mi64021);
            UMenuItem mi64022 = new UMenuItem("Medidas", "64022", 64022, this, this);
            m643.add(mi64022);
            UMenuItem mi64023 = new UMenuItem("Grupos de Agentes T�xicos",
                                              "64023", 64023, this, this);
            m643.add(mi64023);
          } // if(isAccesible("64"))

        } // Fin if(allowed "6")
      } // Fin if(accesible "6")

      //Opciones
      if (isAccesible("7")) {
        UMenu m7 = new UMenu("General", "7", this);
        mBar.add(m7);

        if (isAllowed("7")) {
          UMenuItem mi71 = new UMenuItem("Seleccionar Zonificaci�n", "71", 71, this, this);
          m7.add(mi71);
        }
      }

      // Ayuda
      if (isAccesible("8")) {
        UMenu m8 = new UMenu("Ayuda", "8", this);
        mBar.add(m8);

        // Comprueba si esta cerrada
        if (isAllowed("8")) {

          // Sistema de Informaci�n RTBC
          UMenuItem mi81 = new UMenuItem("Sistema de Informaci�n Brotes", "81",
                                         81, this, this);
          m8.add(mi81);

          // Separador
          MenuItem msep3 = new MenuItem("-");
          m8.add(msep3);

          // Acerca de ...
          UMenuItem mi82 = new UMenuItem("Acerca de ...", "82", 82, this, this);
          m8.add(mi82);

        } // Fin if(allowed "8")
      } // Fin if(accesible "8")

      /*      // Crea el di�logo de valores por defecto (a�o) y lo muestra
            dlgMenu = new DlgMenuTub(this, constantes.modoMODIFICACION,dtAut.getString("FC_ACTUAL").substring(6));
            dlgMenu.show();
            // Completa los valores por defecto: A�o
            if(dlgMenu.getAceptar()) {
           dtParametros.put("CD_ANO", dlgMenu.getParametros().getString("CD_ANO"));
            }*/

    } // Fin if(dtAut!=null)

    // pinta el men�
    mPanel.setMenuBar(mBar);
    add("North", mPanel);
    validate();
  }

  public void actionPerformed(ActionEvent e) {
    URL url;
    String get;
    // modificacion jlt para recuperar el url_servlet
    String ur = new String();
    String ur2 = new String();
    String ur3 = new String();
    String ur4 = new String();
    int ind = 0;
    int indb = 0;
    ur = (String)this.getDocumentBase().toString();
    ind = ur.lastIndexOf("/");
    ur2 = (String) ur.substring(0, ind + 1);
    ur3 = (String) ur2.substring(0, ind);
    indb = ur3.lastIndexOf("/");
    ur4 = (String) ur3.substring(0, indb + 1);
    System.out.println("urmenu2" + ur2);
    System.out.println("urmenu3" + ur4);

    if (e.getSource()instanceof UMenuItem) {
      UMenuItem mi = (UMenuItem) e.getSource();
      try {

        // Agrega el c�digo como par�metro
        //dtParametros.put("NM_APPLET",(new Integer(mi.getCode())).toString());

        // Recarga el applet
        switch (mi.getCode()) {
          case 51:
            showAdvise(
                "Conexi�n no disponible con el Centro Nacional de Epidemiolog�a");
            break;

          case 81:

            //Se obtiene la URL de la pag. html de ayuda
            if (getParametro("URL_HTML").equals("") ||
                getParametro("URL_HTML").equals("/")
                || getParametro("URL_HTML").equals(null)) {

              url = new URL(ur2 + "zip/ayuda/brotes/" + "ayuda_pp.html");
            }
            else {
              url = new URL(getParametro("URL_HTML") + "zip/ayuda/brotes/" +
                            "ayuda_pp.html");
            }

            //url=new URL(getParametro("URL_HTML") + "zip/ayuda/brotes/"+"ayuda_pp.html");
            //Se carga la p�gina en el frame "_blank" (nueva ventana)
            getAppletContext().showDocument(url, "_blank");
            break;

          case 82:

            //Se obtiene la URL de la pag. html de ayuda
            //Se obtiene la URL de la pag. html de ayuda
            if (getParametro("URL_HTML").equals("") ||
                getParametro("URL_HTML").equals("/")
                || getParametro("URL_HTML").equals(null)) {

              url = new URL(ur2 + "zip/ayuda/brotes/" + "ayuda_ad.html");
            }
            else {
              url = new URL(getParametro("URL_HTML") + "zip/ayuda/brotes/" +
                            "ayuda_ad.html");
            }

            //url=new URL(getParametro("URL_HTML") + "zip/ayuda/brotes/"+"ayuda_ad.html");
            //Se carga la p�gina en el frame "_blank" (nueva ventana)
            getAppletContext().showDocument(url, "_blank");
            break;

            // Applet de trabajo
          default:
            ptu.put("ANYO_DEFECTO", AppletEterno.getANYO_DEFECTO());

            ptu.put("CD_NIVEL_1_DEFECTO", AppletEterno.getCD_NIVEL1_DEFECTO());
            ptu.put("CD_NIVEL_2_DEFECTO", AppletEterno.getCD_NIVEL2_DEFECTO());
            ptu.put("DS_NIVEL_1_DEFECTO", AppletEterno.getDS_NIVEL1_DEFECTO());
            ptu.put("DS_NIVEL_2_DEFECTO", AppletEterno.getDS_NIVEL2_DEFECTO());

            // modificacion jlt para recuperar url_servlet
            if (getParameter("URL_SERVLET").equals("") ||
                getParameter("URL_SERVLET").equals("/")
                || getParameter("URL_SERVLET").equals(null)) {

              url = new URL(ur4 + "servlet/SrvBrApp" +
                            ptu.toURL(String.valueOf(idApplet(mi.getCode()))));
            }
            else {
              url = new URL(getParameter("URL_SERVLET") + "servlet/SrvBrApp" +
                            ptu.toURL(String.valueOf(idApplet(mi.getCode()))));
            }

            //url = new URL(getParameter("URL_SERVLET") + "servlet/SrvBrApp" + ptu.toURL(String.valueOf(idApplet(mi.getCode()))));
            //System.out.println(url);
            getAppletContext().showDocument(url, "main");
            break;
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
        showError(ex.getMessage());
      } // Fin try .. catch

    } // if(instancia de UmenuItem)
  } // Fin actionPerformed()

  private int idApplet(int numeroMenu) {
    int iValor = -1;

    switch (numeroMenu) {
      case 11:
        iValor = applet_ALERTA;
        break;
      case 21:
        iValor = applet_BROTES;
        break;
      case 311:
        iValor = appletMODELOS_GRUPO_BROTE;
        break;
      case 312:
        iValor = appletMODELOS_BROTE;
        break;
      case 32:
        iValor = appletPREG_BROTE;
        break;
      case 33:
        iValor = appletLISTAS;
        break;
      case 41:
        iValor = appletINF_UN_BROTE;
        break;
        /*      case 42:
                iValor = appletPERS_BROTE;
      break;*/
      case 43:
        iValor = appletINF_BROTES;
        break;
      case 51:
        iValor = appletENVIOS;
        break;
      case 60:
        iValor = appletGRUPO_EDAD;
        break;
      case 61:
        iValor = appletEPI;
        break;
      case 62:
        iValor = appletGRUPOS;
        break;
      case 63:
        iValor = appletUSR;
        break;
      case 6411:
        iValor = appletCCAA;
        break;
      case 6412:
        iValor = appletPROV;
        break;
      case 6413:
        iValor = appletNIVEL1;
        break;
      case 6414:
        iValor = appletNIVEL2;
        break;
      case 6415:
        iValor = appletZBS;
        break;
      case 6421:
        iValor = appletCIE;
        break;
      case 6422:
        iValor = appletPROCESOS;
        break;
      case 6423:
        iValor = appletEDO_CNE;
        break;
      case 6424:
        iValor = appletEDO;
        break;
      case 643:
        iValor = appletTSIVE;
        break;
      case 644:
        iValor = appletTLINEA;
        break;
      case 645:
        iValor = appletTPREG;
        break;
      case 646:
        iValor = appletTVIGI;
        break;
      case 647:
        iValor = appletMOTBAJA;
        break;
      case 648:
        iValor = appletNIVASIS;
        break;
      case 649:
        iValor = appletSEXO;
        break;
      case 64010:
        iValor = appletDIAGNOSTICO;
        break;
      case 64011:
        iValor = applet_MNTO_GRUPO_BROTE;
        break;
      case 64012:
        iValor = applet_MNTO_TIPO_NOTIFICADOR;
        break;
      case 64013:
        iValor = applet_MNTO_MECANISMOS;
        break;
      case 64014:
        iValor = applet_MNTO_COLECTIVOS;
        break;
      case 64015:
        iValor = applet_MNTO_SITUACION;
        break;
      case 64016:
        iValor = applet_MNTO_TIPO_BROTE;
        break;
      case 64017:
        iValor = applet_MNTO_ORIGEN_MUESTRAS;
        break;
      case 64018:
        iValor = applet_MNTO_SINTOMAS;
        break;
      case 640191:
        iValor = applet_MNTO_TALIMENTOS;
        break;
      case 640192:
        iValor = applet_MNTO_TIMPL;
        break;
      case 640193:
        iValor = applet_MNTO_MCOMERC;
        break;
      case 640194:
        iValor = applet_MNTO_TRATPREV;
        break;
      case 640195:
        iValor = applet_MNTO_FINGER;
        break;
      case 640196:
        iValor = applet_MNTO_LCONTAMI;
        break;
      case 640197:
        iValor = applet_MNTO_LPREPAR;
        break;
      case 640198:
        iValor = applet_MNTO_LCONSUMO;
        break;
      case 640199:
        iValor = applet_MNTO_MTVIAJE;
        break;
      case 64020:
        iValor = applet_MNTO_AGCAUSALES;
        break;
      case 64021:
        iValor = applet_MNTO_FACCONT;
        break;
      case 64022:
        iValor = applet_MNTO_MEDIDAS;
        break;
      case 64023:
        iValor = applet_MNTO_GRUPOS_AGTOXICOS;
        break;
      case 71:
        iValor = applet_SEL_ZONIFICACION;
        break;
      case 81:
        iValor = ayuda_INF_BROTES;
        break;
      case 82:
        iValor = ayuda_ACERCA_D;
        break;
    }
    return iValor;
  }

  // Chapuza
  public void showStatus(String s) {
    //System.out.println(s);

    String sBuffer = null;
    StringTokenizer st = new StringTokenizer(s, "$");

    sBuffer = st.nextToken();
    AppletEterno.setCD_NIVEL1_DEFECTO(sBuffer.equals("#") ? "" : sBuffer);

    sBuffer = st.nextToken();
    AppletEterno.setDS_NIVEL1_DEFECTO(sBuffer.equals("#") ? "" : sBuffer);

    /*sBuffer = st.nextToken();
         AppletEterno.setCD_NIVEL2_DEFECTO(sBuffer.equals("#") ? "" : sBuffer);
         sBuffer = st.nextToken();
         AppletEterno.setDS_NIVEL2_DEFECTO(sBuffer.equals("#") ? "" : sBuffer);*/

    sBuffer = st.nextToken();
    AppletEterno.setANYO_DEFECTO(sBuffer.equals("#") ? "" : sBuffer);

  }
  // Fin Chapuza

} // endclass AppMenu
