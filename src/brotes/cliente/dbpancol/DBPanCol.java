/**
 * Clase: DAPanCol
 * Hereda: CPanel
 * Autor: Pedro Antonio D�az (PDP)
 * Fecha Inicio: 19/05/2000
 * Analisis Funcional: Informe Final
 * Descripcion: Panel correspondiente al Colectivo
 */
package brotes.cliente.dbpancol;

import java.util.Hashtable;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.ChoiceCDDS;
import brotes.cliente.c_componentes.DatoRecogible;
import brotes.cliente.c_comuncliente.SincrEventos;
import brotes.cliente.diabif.DiaBIF;
import brotes.cliente.pannivelesbro.PanNivelesBro;
import brotes.cliente.suca.Suca;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPanel;
import capp2.CTexto;
import sapp2.Data;
import sapp2.Lista;

public class DBPanCol
    extends CPanel
    implements CInicializar, DatoRecogible {

  /*************** Datos propios del panel ****************/

  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;
  final int ESPERA = 4;
  final int NORMAL = 5;

  CApp applet = null;
  DiaBIF dlg = null;
  Data brote = null;

  //suca
  Suca pansuca = null;
  Hashtable listas = null;

  //Listas de Datas de entrada a Suca para convertirlas en CLista
  Lista listaPaises = null;
  Lista listaCA = null;

  //tipo notificador
  Lista listaColectivo = null;

  //niveles
  PanNivelesBro panNivCol = null;
  PanNivelesBro panNivExp = null;

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  /****************** Componentes del panel ********************/
  XYLayout xYLayout = new XYLayout();

  XYLayout xYLayout1 = new XYLayout();

  Label lColectivo = new Label();
  CTexto txtColectivo = new CTexto(50);

  Label lTelCol = new Label();
  CTexto txtTelCol = new CTexto(14);
  Label lTColec = new Label();

  ChoiceCDDS choColectivo = null;

  CApp capp = null;
  Label lLocCol = new Label();
  Label lLocExp = new Label();

  // constructor
  SincrEventos scr = new SincrEventos();

  // constructor

  public DBPanCol(CApp a,
                  DiaBIF d,
                  Data b,
                  int modo,
                  Hashtable hsCompleta) {

    super(a);
    applet = a;
    dlg = d;
    brote = b;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    listas = hsCompleta;

    try {
      // Recuperacion de listas
      listaPaises = (Lista) hsCompleta.get("PAISES");
      listaCA = (Lista) hsCompleta.get("CA");
      listaColectivo = (Lista) hsCompleta.get("TCOLECTIVO");

      jbInit();
      Inicializar(modoOperacion);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    boolean nulo, bCD, bDS;
    String sCD = "";
    String sDS = "";
    Color cObligatorio = new Color(255, 255, 150);

    this.setSize(new Dimension(755, 280));

    xYLayout.setHeight(270); //270
    xYLayout.setWidth(735);

    // Paneles suca
    pansuca = new Suca(applet, this, scr, modoEntrada, listas);

    // Paneles niveles
    panNivCol = new PanNivelesBro(applet, this, modoEntrada, scr);
    panNivExp = new PanNivelesBro(applet, this, modoEntrada, scr);

    lColectivo.setText("Colectivo afectado");
    lTelCol.setText("Telefono");
    lLocCol.setForeground(Color.black);
    lLocCol.setFont(new Font("Dialog", 0, 12));
    lLocCol.setText("Localizacion Colectivo Afectado");
    lLocExp.setForeground(Color.black);
    lLocExp.setFont(new Font("Dialog", 0, 12));
    lLocExp.setText("Localizacion Exposicion");
    lTColec.setForeground(Color.black);
    lTColec.setFont(new Font("Dialog", 0, 12));
    lTColec.setText("Tipo Colectivo");
    nulo = true;
    bCD = false;
    bDS = true;
    sCD = "CD_TIPOCOL";
    sDS = "DS_TIPOCOL";
    choColectivo = new ChoiceCDDS(nulo, bCD, bDS, sCD, sDS, listaColectivo);
    choColectivo.writeData();
    choColectivo.setBackground(cObligatorio);
    this.setLayout(xYLayout);

    this.add(lColectivo, new XYConstraints(9, 5, -1, -1));
    this.add(txtColectivo, new XYConstraints(122, 7, 275, -1));
    this.add(lTelCol, new XYConstraints(480, 7, 63, -1));
    this.add(txtTelCol, new XYConstraints(549, 5, 130, -1));
    this.add(lTColec, new XYConstraints(9, 35, 97, -1));
    this.add(choColectivo, new XYConstraints(122, 35, 205, -1));
    this.add(pansuca, new XYConstraints(9, 75, 596, 116));
    this.add(lLocCol, new XYConstraints(9, 195, 220, -1));
    this.add(panNivCol, new XYConstraints(9, 220, 655, 40));
    this.add(lLocExp, new XYConstraints(9, 275, 217, -1));
    this.add(panNivExp, new XYConstraints(9, 300, 655, 40));
  }

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    if (dlg != null) {
      dlg.Inicializar(modoOperacion);
    }
  } // Fin Inicializar()

  public void InicializarDesdeFuera(int mod) {
    modoOperacion = mod;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      // modo espera
      case CInicializar.ESPERA:
        pansuca.InicializarDesdeFuera(CInicializar.ESPERA);
        panNivCol.InicializarDesdeFuera(CInicializar.ESPERA);
        panNivExp.InicializarDesdeFuera(CInicializar.ESPERA);
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case ALTA:
          case MODIFICACION:
            txtColectivo.setEnabled(true);
            choColectivo.setEnabled(true);
            txtTelCol.setEnabled(true);
            pansuca.InicializarDesdeFuera(modoOperacion);
            panNivCol.InicializarDesdeFuera(modoOperacion);
            panNivExp.InicializarDesdeFuera(modoOperacion);
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            break;

          case BAJA:
          case CONSULTA:
            txtColectivo.setEnabled(false);
            choColectivo.setEnabled(false);
            txtTelCol.setEnabled(false);
            pansuca.InicializarDesdeFuera(modoOperacion);
            panNivCol.InicializarDesdeFuera(modoOperacion);
            panNivExp.InicializarDesdeFuera(modoOperacion);
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            break;
        }
    }
  }

  public void rellenarDatos(Data dtReg) {

    //Colectivo
    choColectivo.selectChoiceElement(dtReg.getString("CD_TIPOCOL"));

    // Nombre
    txtColectivo.setText(dtReg.getString("DS_NOMCOL"));

    //Tel�fono
    txtTelCol.setText(dtReg.getString("DS_TELCOL"));

    // Relleno de pansuca
    Data dtsuca = new Data();

    dtsuca.put("CA", dtReg.getString("CD_CA"));
    dtsuca.put("PROV", dtReg.getString("CD_PROVCOL"));
    dtsuca.put("MUN", dtReg.getString("CD_MUNCOL"));
    dtsuca.put("VIA", dtReg.getString("DS_DIRCOL"));
    dtsuca.put("NUM", dtReg.getString("DS_NMCALLE"));
    dtsuca.put("PISO", dtReg.getString("DS_PISOCOL"));
    dtsuca.put("CODPOSTAL", dtReg.getString("CD_POSTALCOL"));
    pansuca.rellenarDatos(dtsuca);

    // Relleno de panNivCol
    String cdNivel1Col = "";
    String dsNivel1Col = "";
    String cdNivel2Col = "";
    String dsNivel2Col = "";
    String cdZbsCol = "";
    String dsZbsCol = "";

    cdNivel1Col = dtReg.getString("CD_NIVEL_1_LCA");
    panNivCol.setCDNivel1(cdNivel1Col);

    cdNivel2Col = dtReg.getString("CD_NIVEL_2_LCA");
    panNivCol.setCDNivel2(cdNivel2Col);

    cdZbsCol = dtReg.getString("CD_ZBS_LCA");
    panNivCol.setCDNivel4(cdZbsCol);

    dsZbsCol = dtReg.getString("DS_ZBS_LCA");
    panNivCol.setDSNivel4(dsZbsCol);

    // Relleno de panNivExp
    String cdNivel1Exp = "";
    String dsNivel1Exp = "";
    String cdNivel2Exp = "";
    String dsNivel2Exp = "";
    String cdZbsExp = "";
    String dsZbsExp = "";

    cdNivel1Exp = dtReg.getString("CD_NIVEL_1_LE");
    panNivExp.setCDNivel1(cdNivel1Exp);

    cdNivel2Exp = dtReg.getString("CD_NIVEL_2_LE");
    panNivExp.setCDNivel2(cdNivel2Exp);

    cdZbsExp = dtReg.getString("CD_ZBS_LE");
    panNivExp.setCDNivel4(cdZbsExp);

    dsZbsExp = dtReg.getString("DS_ZBS_LE");
    panNivExp.setDSNivel4(dsZbsExp);

  }

  public void recogerDatos(Data dtDev) {

    dtDev.put("DS_NOMCOL", txtColectivo.getText().trim());
    dtDev.put("CD_TIPOCOL", choColectivo.getChoiceCD());
    dtDev.put("DS_TELCOL", txtTelCol.getText().trim());

    // Recogida de datos de pansuca
    Data dtSucaR = new Data();
    pansuca.recogerDatos(dtSucaR);
    dtDev.put("CD_PROVCOL", (String) dtSucaR.get("PROV"));
    dtDev.put("CD_MUNCOL", (String) dtSucaR.get("MUN"));
    dtDev.put("DS_DIRCOL", (String) dtSucaR.get("VIA"));
    dtDev.put("DS_NMCALLE", (String) dtSucaR.get("NUM"));
    dtDev.put("DS_PISOCOL", (String) dtSucaR.get("PISO"));
    dtDev.put("CD_POSTALCOL", (String) dtSucaR.get("CODPOSTAL"));

    dtDev.put("CD_NIVEL_1_LCA", panNivCol.getCDNivel1());
    dtDev.put("CD_NIVEL_2_LCA", panNivCol.getCDNivel2());
    dtDev.put("CD_ZBS_LCA", panNivCol.getCDNivel4());

    dtDev.put("CD_NIVEL_1_LE", panNivExp.getCDNivel1());
    dtDev.put("CD_NIVEL_2_LE", panNivExp.getCDNivel2());
    dtDev.put("CD_ZBS_LE", panNivExp.getCDNivel4());
  }

  public boolean validarDatos() {

    if (!pansuca.validarDatos()) {
      return false;
    }

    // Colectivo
    if (choColectivo.getChoiceCD().equals("")) {
      this.getApp().showError("El colectivo es un campo obligatorio");
      choColectivo.requestFocus();
      return (false);
    }

    return true;
  }

  /****************** Funciones Auxiliares *****************/

  public void cambiarBIF(Data newBIF) {
    brote = newBIF;
  } // Fin cambiarAlerta()

  public void setModoEntrada(int m) {
    pansuca.setModoEntrada(m);
    panNivCol.setModoEntrada(m);
    panNivExp.setModoEntrada(m);
    modoEntrada = m;
  } // Fin setModoEntrada()

} //fin
