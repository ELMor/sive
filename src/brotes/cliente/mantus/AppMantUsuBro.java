/*
 * Applet de mantenimiento de usuarios.
 * Es un Copy&Paste del de tuberculósis.
 *      Fecha         Autor           Accion
 *     11/04/2000     JRM             La escribe (C&P)
 */
package brotes.cliente.mantus;

import capp2.CApp;

/**
 * Applet principal de la aplicación.
 */
public class AppMantUsuBro
    extends CApp {

  public AppMantUsuBro() {
    super();
  }

  public void init() {
    super.init();
    setTitulo("Mantenimiento de Usuarios");
    PanMantUsuBro pnlUsuariosBro = new PanMantUsuBro( (CApp)this);
    pnlUsuariosBro.setBorde(false);
    VerPanel("", pnlUsuariosBro);
  }
}
