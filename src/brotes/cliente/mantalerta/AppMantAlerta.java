package brotes.cliente.mantalerta;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppMantAlerta
    extends CApp
    implements CInicializar {
  MantAlerta pan = null;

  public AppMantAlerta() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Mantenimiento de Alertas");
    pan = new MantAlerta(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }
}