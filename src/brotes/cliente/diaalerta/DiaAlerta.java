/**
 * Clase: DiaAlerta
 * Hereda: CDialog
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 10/05/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Alertas
 * Descripcion: Di�logo de alta/mod/baja de una alerta
 */

package brotes.cliente.diaalerta;

import java.util.Hashtable;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.DatoRecogible;
import brotes.cliente.c_comuncliente.BDatos;
import brotes.cliente.c_comuncliente.SincrEventos;
import brotes.cliente.c_comuncliente.nombreservlets;
import brotes.cliente.dapanexp.DAPanExp;
import brotes.cliente.dapanhosp.DAPanHosp;
import brotes.cliente.dapanidnotif.DAPanIdNotif;
import brotes.cliente.dapanotros.DAPanOtros;
import brotes.cliente.dapansup.DAPanSup;
import capp2.CApp;
import capp2.CContextHelp;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CTabPanel;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

//import brotes.servidor.alerta.*;

public class DiaAlerta
    extends CDialog
    implements CInicializar, DatoRecogible {

  /*************** Datos propios del panel ****************/

  // Parametros de entrada
  CApp applet = null;
  Hashtable listas = null; // Listas de cat�logos

  // Variables intermedias
  Data alertaIni = null;

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  // Constantes del modoEntrada
  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;

  // Nombre de los servlets
  final String srvAlerta = nombreservlets.strSERVLET_ALERTA;

  /****************** Componentes del panel ********************/

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Organizaci�n del panel
  private XYLayout xYLayout1 = new XYLayout();

  // Panel superior
  private DAPanSup psup = null;

  private CTabPanel tabPanel = null;
  private DAPanIdNotif pIdNotif = null;
  private DAPanExp pExp = null;
  private DAPanOtros pOtros = null;

  // Bot�n de Hospitales
  private ButtonControl btnHosp = new ButtonControl();

  // Botones de salida
  private ButtonControl btnAceptar = new ButtonControl();
  private ButtonControl btnCancelar = new ButtonControl();
  private ButtonControl btnSalir = new ButtonControl();

  // Escuchadores
  DiaAlertaActionAdapter actionAdapter = null;

  // Constructor
  // @param a: CApp
  // @param listas: Hash con Listas de cat�logos
  public DiaAlerta(CApp a, int modo, Data alerta, Hashtable hListas) {

    super(a);
    applet = a;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    alertaIni = alerta;
    listas = hListas;

    try {
      // Inicializaci�n
      jbInit();
      Inicializar(modoOperacion);

      switch (modoEntrada) {
        case ALTA:
          setTitle("Alta de Alerta");
          break;
        case MODIFICACION:
          setTitle("Modificaci�n de Alerta");
          rellenarDatos(alertaIni);
          break;
        case BAJA:
          setTitle("Baja de Alerta");
          rellenarDatos(alertaIni);
          break;
        case CONSULTA:
          setTitle("Consulta de Alerta");
          rellenarDatos(alertaIni);
          break;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicia el aspecto del dialogo DiaAlerta
  public void jbInit() throws Exception {
    // Carga las imagenes
    getApp().getLibImagenes().put(constantes.imgACEPTAR);
    getApp().getLibImagenes().put(constantes.imgCANCELAR);
    getApp().getLibImagenes().put(constantes.imgSALIR);
    getApp().getLibImagenes().CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaAlertaActionAdapter(this);

    // Organizacion del panel
    this.setSize(new Dimension(755, 575));
    this.setLayout(xYLayout1);
    xYLayout1.setWidth(755);
    xYLayout1.setHeight(575);

    // Panel Superior
    psup = new DAPanSup(applet, this, alertaIni, modoEntrada,
                        (Lista) listas.get("GRUPOS"),
                        (Lista) listas.get("SITUACIONES"));

    // Pesta�as: Notificador
    tabPanel = new CTabPanel();
    pIdNotif = new DAPanIdNotif(applet, this, alertaIni, modoEntrada, listas);
    pExp = new DAPanExp(applet, this, alertaIni, modoEntrada, listas);
    pOtros = new DAPanOtros(applet, this, alertaIni, modoEntrada, listas);

    // Bot�n de Hospitales
    btnHosp.setLabel("Hospitales");
    //btnHosp.setImage(this.getApp().getLibImagenes().get(constantes.imgACEPTAR));
    btnHosp.setActionCommand("Hospitales");
    btnHosp.addActionListener(actionAdapter);

    // Seg�n modoEntrada habr� botones diferentes
    switch (modoEntrada) {
      case ALTA:
      case MODIFICACION:

        // Bot�n Aceptar
        btnAceptar.setLabel("Grabar");
        btnAceptar.setImage(this.getApp().getLibImagenes().get(constantes.
            imgACEPTAR));
        btnAceptar.setActionCommand("Grabar");
        btnAceptar.addActionListener(actionAdapter);

        // Bot�n Cancelar
        btnCancelar.setLabel("Cancelar");
        btnCancelar.setImage(this.getApp().getLibImagenes().get(constantes.
            imgCANCELAR));
        btnCancelar.setActionCommand("Cancelar");
        btnCancelar.addActionListener(actionAdapter);
        break;

      case BAJA:

        // Bot�n Aceptar
        btnAceptar.setLabel("Aceptar");
        btnAceptar.setImage(this.getApp().getLibImagenes().get(constantes.
            imgACEPTAR));
        btnAceptar.setActionCommand("Aceptar");
        btnAceptar.addActionListener(actionAdapter);

        // Bot�n Cancelar
        btnCancelar.setLabel("Cancelar");
        btnCancelar.setImage(this.getApp().getLibImagenes().get(constantes.
            imgCANCELAR));
        btnCancelar.setActionCommand("Cancelar");
        btnCancelar.addActionListener(actionAdapter);
        break;

      case CONSULTA:

        // Bot�n Salir
        btnSalir.setLabel("Salir");
        btnSalir.setImage(this.getApp().getLibImagenes().get(constantes.
            imgSALIR));
        btnSalir.setActionCommand("Salir");
        btnSalir.addActionListener(actionAdapter);
        break;
    }

    // Adici�n de componentes al di�logo
    this.add(psup, new XYConstraints(10, 10, -1, -1));
    this.add(tabPanel, new XYConstraints(10, 100, 725, 400));
    this.add(btnHosp, new XYConstraints(10, 515, 100, -1));
    switch (modoEntrada) {
      case ALTA:
      case MODIFICACION:
        this.add(btnAceptar, new XYConstraints(537, 515, 80, -1));
        this.add(btnCancelar, new XYConstraints(627, 515, 80, -1));
        new CContextHelp("Grabar", btnAceptar);
        new CContextHelp("Cancelar", btnCancelar);
        break;

      case BAJA:
        this.add(btnAceptar, new XYConstraints(537, 515, 80, -1));
        this.add(btnCancelar, new XYConstraints(627, 515, 80, -1));
        new CContextHelp("Aceptar", btnAceptar);
        new CContextHelp("Cancelar", btnCancelar);
        break;

      case CONSULTA:
        this.add(btnSalir, new XYConstraints(627, 515, 80, -1));
        new CContextHelp("Salir", btnSalir);
        break;
    }

    // Paneles de tabPanel (con pesta�as)
    tabPanel.InsertarPanel("Identificaci�n del Notificador", pIdNotif);
    tabPanel.InsertarPanel("Datos de Exposici�n", pExp);
    tabPanel.InsertarPanel("Otros Datos", pOtros);
    tabPanel.VerPanel("Identificaci�n del Notificador");

  } // Fin jbInit()

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar()

  public void Inicializar() {
    // Inicializaci�n de los subcontenedores
    if (psup != null) {
      psup.InicializarDesdeFuera(modoOperacion);
    }
    if (pIdNotif != null) {
      pIdNotif.InicializarDesdeFuera(modoOperacion);
    }
    if (pExp != null) {
      pExp.InicializarDesdeFuera(modoOperacion);
    }
    if (pOtros != null) {
      pOtros.InicializarDesdeFuera(modoOperacion);

      // Componentes propios del di�logo
    }
    switch (modoOperacion) {
      // Modo espera
      case CInicializar.ESPERA:
        btnHosp.setEnabled(false);
        if (modoEntrada == CONSULTA) {
          btnSalir.setEnabled(false);
        }
        else {
          btnAceptar.setEnabled(false);
          btnCancelar.setEnabled(false);
        }
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // Modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case ALTA:
            btnHosp.setEnabled(false);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
          case MODIFICACION:
            btnHosp.setEnabled(true);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
          case BAJA:
            btnHosp.setEnabled(true);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
          case CONSULTA:
            btnHosp.setEnabled(true);
            btnSalir.setEnabled(true);
            break;
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    } // Fin switch
  }

  /********** Sincronizador de Eventos ******************/

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  /****************** Interfaz DatoRecogible ****************/

  public void recogerDatos(Data d) {
    // Recogida de datos del panel superior
    psup.recogerDatos(d);

    // Recogida de datos del panel IdNotif
    pIdNotif.recogerDatos(d);

    // Recogida de datos del panel Exposicion
    pExp.recogerDatos(d);

    // Recogida de datos del panel Otros
    pOtros.recogerDatos(d);

    // FC_ALERBRO
    if (!d.getString("CD_SITALERBRO").equals(alertaIni.getString(
        "CD_SITALERBRO"))) {
      d.put("FC_ALERBRO", "CHANGE");
    }
    else {
      d.put("FC_ALERBRO", alertaIni.getString("FC_ALERBRO"));
    }
  } // Fin recogerDatos()

  public void rellenarDatos(Data d) {
    // Relleno de datos del panel superior
    psup.rellenarDatos(d);

    // Relleno de datos del panel IdNotif
    pIdNotif.rellenarDatos(d);

    // Relleno de datos del panel Exposicion
    pExp.rellenarDatos(d);

    // Relleno de datos del panel Otros
    pOtros.rellenarDatos(d);
  } // Fin rellenarDatos()

  public boolean validarDatos() {

    // 1. PERDIDAS DE FOCO (Campos CD-Button-DS): No hay

    // 2. LONGITUDES: No hay

    // 3. DATOS NUMERICOS: No hay

    // 4. CAMPOS OBLIGATORIOS: No hay

    // 5. FECHAS

    // Validaci�n de datos del panel superior
    if (!psup.validarDatos()) {
      return false;
    }

    if (!pIdNotif.validarDatos()) {
      tabPanel.VerPanel("IdNotif");
      return false;
    }

    if (!pExp.validarDatos()) {
      tabPanel.VerPanel("Exposicion");
      return false;
    }

    if (!pOtros.validarDatos()) {
      tabPanel.VerPanel("Otros");
      return false;
    }

    // Si llega aqui todo ha ido bien
    return true;
  } // Fin validarDatos()

  // Dev. true si son iguales ambas estructuras
  boolean compararCampos(Data d1, Data d2) {
    if (d1 == null || d2 == null) {
      return true;
    }

    if (!d1.getString("CD_ANO").equals(d2.getString("CD_ANO"))) {
      return false;
    }

    if (!d1.getString("NM_ALERBRO").equals(d2.getString("NM_ALERBRO"))) {
      return false;
    }

    if (!d1.getString("FC_FECHAHORA").equals(d2.getString("FC_FECHAHORA"))) {
      return false;
    }

    if (!d1.getString("DS_ALERTA").equals(d2.getString("DS_ALERTA"))) {
      return false;
    }

    if (!d1.getString("CD_GRUPO").equals(d2.getString("CD_GRUPO"))) {
      return false;
    }

    if (!d1.getString("CD_NIVEL_1").equals(d2.getString("CD_NIVEL_1"))) {
      return false;
    }

    if (!d1.getString("CD_SITALERBRO").equals(d2.getString("CD_SITALERBRO"))) {
      return false;
    }

    if (!d1.getString("FC_ALERBRO").equals(d2.getString("FC_ALERBRO"))) {
      return false;
    }

    // Campos del panel IdNotif

    if (!d1.getString("CD_TNOTIF").equals(d2.getString("CD_TNOTIF"))) {
      return false;
    }

    if (!d1.getString("DS_NOTIFINST").equals(d2.getString("DS_NOTIFINST"))) {
      return false;
    }

    if (!d1.getString("DS_NOTIFICADOR").equals(d2.getString("DS_NOTIFICADOR"))) {
      return false;
    }

    if (!d1.getString("CD_NOTIFPROV").equals(d2.getString("CD_NOTIFPROV"))) {
      return false;
    }

    if (!d1.getString("CD_NOTIFMUN").equals(d2.getString("CD_NOTIFMUN"))) {
      return false;
    }

    if (!d1.getString("DS_NOTIFDIREC").equals(d2.getString("DS_NOTIFDIREC"))) {
      return false;
    }

    if (!d1.getString("DS_NNMCALLE").equals(d2.getString("DS_NNMCALLE"))) {
      return false;
    }

    if (!d1.getString("DS_NOTIFPISO").equals(d2.getString("DS_NOTIFPISO"))) {
      return false;
    }

    if (!d1.getString("CD_NOTIFPOSTAL").equals(d2.getString("CD_NOTIFPOSTAL"))) {
      return false;
    }

    if (!d1.getString("DS_NOTIFTELEF").equals(d2.getString("DS_NOTIFTELEF"))) {
      return false;
    }

    if (!d1.getString("DS_MINFPER").equals(d2.getString("DS_MINFPER"))) {
      return false;
    }

    if (!d1.getString("CD_MINPROV").equals(d2.getString("CD_MINPROV"))) {
      return false;
    }

    if (!d1.getString("CD_MINMUN").equals(d2.getString("CD_MINMUN"))) {
      return false;
    }

    if (!d1.getString("DS_MINFDIREC").equals(d2.getString("DS_MINFDIREC"))) {
      return false;
    }

    if (!d1.getString("DS_MNMCALLE").equals(d2.getString("DS_MNMCALLE"))) {
      return false;
    }

    if (!d1.getString("DS_MINFPISO").equals(d2.getString("DS_MINFPISO"))) {
      return false;
    }

    if (!d1.getString("CD_MINFPOSTAL").equals(d2.getString("CD_MINFPOSTAL"))) {
      return false;
    }

    if (!d1.getString("DS_MINFTELEF").equals(d2.getString("DS_MINFTELEF"))) {
      return false;
    }

    // Datos del panel PanExp (exposicion)

    if (!d1.getString("CD_LEXPROV").equals(d2.getString("CD_LEXPROV"))) {
      return false;
    }

    if (!d1.getString("CD_NIVEL_1_EX").equals(d2.getString("CD_NIVEL_1_EX"))) {
      return false;
    }

    if (!d1.getString("CD_NIVEL_2_EX").equals(d2.getString("CD_NIVEL_2_EX"))) {
      return false;
    }

    if (!d1.getString("CD_ZBS_EX").equals(d2.getString("CD_ZBS_EX"))) {
      return false;
    }

    if (!d1.getString("NM_EXPUESTOS").equals(d2.getString("NM_EXPUESTOS"))) {
      return false;
    }

    if (!d1.getString("NM_ENFERMOS").equals(d2.getString("NM_ENFERMOS"))) {
      return false;
    }

    if (!d1.getString("NM_INGHOSP").equals(d2.getString("NM_INGHOSP"))) {
      return false;
    }

    if (!d1.getString("FC_EXPOSICION").equals(d2.getString("FC_EXPOSICION"))) {
      return false;
    }

    if (!d1.getString("FC_INISINTOMAS").equals(d2.getString("FC_INISINTOMAS"))) {
      return false;
    }

    if (!d1.getString("DS_LEXP").equals(d2.getString("DS_LEXP"))) {
      return false;
    }

    if (!d1.getString("CD_LEXMUN").equals(d2.getString("CD_LEXMUN"))) {
      return false;
    }

    if (!d1.getString("DS_LEXPDIREC").equals(d2.getString("DS_LEXPDIREC"))) {
      return false;
    }

    if (!d1.getString("CD_LEXPPOST").equals(d2.getString("CD_LEXPPOST"))) {
      return false;
    }

    if (!d1.getString("DS_LNMCALLE").equals(d2.getString("DS_LNMCALLE"))) {
      return false;
    }

    if (!d1.getString("DS_LEXPPISO").equals(d2.getString("DS_LEXPPISO"))) {
      return false;
    }

    if (!d1.getString("DS_LEXPTELEF").equals(d2.getString("DS_LEXPTELEF"))) {
      return false;
    }

    // Datos del panel PanOtros

    if (!d1.getString("DS_NOMCOL").equals(d2.getString("DS_NOMCOL"))) {
      return false;
    }

    if (!d1.getString("DS_TELCOL").equals(d2.getString("DS_TELCOL"))) {
      return false;
    }

    if (!d1.getString("CD_PROV").equals(d2.getString("CD_PROV"))) {
      return false;
    }

    if (!d1.getString("CD_MUN").equals(d2.getString("CD_MUN"))) {
      return false;
    }

    if (!d1.getString("DS_DIRCOL").equals(d2.getString("DS_DIRCOL"))) {
      return false;
    }

    if (!d1.getString("DS_NMCALLE").equals(d2.getString("DS_NMCALLE"))) {
      return false;
    }

    if (!d1.getString("DS_PISOCOL").equals(d2.getString("DS_PISOCOL"))) {
      return false;
    }

    if (!d1.getString("CD_POSTALCOL").equals(d2.getString("CD_POSTALCOL"))) {
      return false;
    }

    if (!d1.getString("DS_OBSERV").equals(d2.getString("DS_OBSERV"))) {
      return false;
    }

    if (!d1.getString("DS_ALISOSP").equals(d2.getString("DS_ALISOSP"))) {
      return false;
    }

    if (!d1.getString("DS_SINTOMAS").equals(d2.getString("DS_SINTOMAS"))) {
      return false;
    }

    return true;
  } // Fin compararCampos()

  /******************** Manejadores *************************/

  // Altas y Modificaciones
  void btnGrabarActionPerformed() {
    Lista lQuery = new Lista(); // Lista con queries
    Lista lResul = null;
    Lista lBloqueo = null; // Lista con datos del bloqueo
    Data alertaTmp = new Data(); // Datos de la alerta (alta/mod)

    switch (modoEntrada) {
      case ALTA:
        if (validarDatos()) {
          // Recogida de datos de la alerta
          recogerDatos(alertaTmp);

          try {
            // Montaje de la query de ALTA
            lQuery = prepararQTAlta(alertaTmp);

            // Transacci�n con el servidor
            /*SrvAlerta srv = new SrvAlerta();
                       srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver","jdbc:oracle:thin:@194.140.66.208:1521:ORCL","sive_desa","sive_desa");
                       lResul =srv.doDebug(0, lQuery);*/

            lResul =
                BDatos.execSQL(applet, srvAlerta, 0, lQuery); // 0 porque SrvAlerta ignora dicho modo

            // Se establece la alertaIni
            alertaIni = alertaTmp;

            // Se establece el nuevo CD_OPE/FC_ULTACT
            alertaIni.put("CD_OPE", applet.getParametro("LOGIN"));
            alertaIni.put("FC_ULTACT",
                          ( (Lista) lResul.firstElement()).getFC_ULTACT());

            // Se establece el NM_ALERBRO asignado a la nueva alerta
            Integer nm_alerbro = (Integer) lResul.elementAt(1);
            alertaIni.put("NM_ALERBRO", "" + nm_alerbro.intValue());

            // Se establece la nueva alerta en cada subpanel
            psup.cambiarAlerta(alertaIni);
            pIdNotif.cambiarAlerta(alertaIni);
            pExp.cambiarAlerta(alertaIni);
            pOtros.cambiarAlerta(alertaIni);

            // Alta correcta => modoEntrada = MODIFICACION
            setTitle("Modificaci�n de Alerta");
            modoEntrada = MODIFICACION;
            psup.setModoEntrada(MODIFICACION);
            pIdNotif.setModoEntrada(MODIFICACION);
            pExp.setModoEntrada(MODIFICACION);
            pOtros.setModoEntrada(MODIFICACION);

          }
          catch (Exception ex) {
            this.getApp().trazaLog(ex);
            this.getApp().showError(ex.getMessage());
          }
        } // validarDatos() ya muestra mensajes de error
        break;

      case MODIFICACION:
        if (validarDatos()) {
          // Recogida de datos de la alerta
          recogerDatos(alertaTmp);

          // Comparar la alerta inicial con la nueva
          if (compararCampos(alertaIni, alertaTmp)) {
            break;
          }

          // Montaje de la query de MODIFICACION
          lQuery = prepararQTModificacion(alertaTmp);

          // Preparaci�n de los datos del bloqueo
          lBloqueo = prepararBloqueo();

          try {
            // Transacci�n con el servidor
            /*SrvAlerta srv = new SrvAlerta();
                       srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver","jdbc:oracle:thin:@194.140.66.208:1521:ORCL","sive_desa","sive_desa");
                       lResul =srv.doDebug(10000, lQuery);*/

            lResul =
                BDatos.execSQLBloqueo(applet, srvAlerta, 10000, lQuery, // 10000 para que ejecute el bloqueo
                                      (QueryTool) lBloqueo.elementAt(0),
                                      (Data) lBloqueo.elementAt(1));

            // Se establece la alertaIni
            alertaIni = alertaTmp;

            // Se establece el nuevo CD_OPE/FC_ULTACT
            alertaIni.put("CD_OPE", applet.getParametro("LOGIN"));
            alertaIni.put("FC_ULTACT",
                          ( (Lista) lResul.firstElement()).getFC_ULTACT());

            // Se establece la nueva alerta en cada subpanel
            psup.cambiarAlerta(alertaIni);
            pIdNotif.cambiarAlerta(alertaIni);
            pExp.cambiarAlerta(alertaIni);
            pOtros.cambiarAlerta(alertaIni);

          }
          catch (Exception ex) {
            this.getApp().trazaLog(ex);
            this.getApp().showError(ex.getMessage());
          }
        } // validarDatos() ya muestra mensajes de error
        break;
    } // Fin switch()

    // Si es ALTA Desaparece el dialogo
    /*if(modoEntrada == ALTA)
      dispose();*/
  } // Fin btnGrabarActionPerformed()

  // Bajas
  void btnAceptarActionPerformed() {
    Lista lQuery = new Lista(); // Lista con queries
    Lista lResul = null;
    Lista lBloqueo = null; // Lista con datos del bloqueo

    switch (modoEntrada) {
      case BAJA:

        // Montaje de la query de BAJA
        lQuery = prepararQTBaja(alertaIni);

        // Preparaci�n de los datos del bloqueo
        lBloqueo = prepararBloqueo();

        try {
          // Transacci�n con el servidor
          /*SrvAlerta srv = new SrvAlerta();
                   srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver","jdbc:oracle:thin:@194.140.66.208:1521:ORCL","sive_desa","sive_desa");
                   lResul =srv.doDebug(10000, lQuery);*/

          lResul =
              BDatos.execSQLBloqueo(applet, srvAlerta, 10000, lQuery, // 10000 para que ejecute el bloqueo
                                    (QueryTool) lBloqueo.elementAt(0),
                                    (Data) lBloqueo.elementAt(1));
        }
        catch (Exception ex) {
          this.getApp().trazaLog(ex);
          this.getApp().showError(ex.getMessage());
        }

        break;
    } // Fin switch()

    // Desaparece el dialogo
    dispose();
  } // Fin btnAceptarActionPerformed()

  // Altas/Mod/Bajas
  void btnCancelarActionPerformed() {
    // Desaparece el dialogo
    dispose();
  } // Fin btnCancelarActionPerformed()

  // Consulta
  void btnSalirActionPerformed() {
    // Desaparece el dialogo
    dispose();
  } // Fin btnSalirActionPerformed()

  void btnHospActionPerformed() {
    DAPanHosp h = null;
    if (modoEntrada == MODIFICACION) {
      int sit = new Integer(alertaIni.getString("CD_SITALERBRO")).intValue();
      switch (sit) {
        case 0:
          h = new DAPanHosp(applet, MODIFICACION, alertaIni);
          break;
        case 1:
        case 2:
        case 3:
        case 4:
          h = new DAPanHosp(applet, CONSULTA, alertaIni);
          break;
      }
    }
    else {
      h = new DAPanHosp(applet, modoEntrada, alertaIni);
    }

    h.show();
  } // Fin btnSinCasosActionPerformed()

  /******* M�todos auxiliares de preparaci�n de QueryTool *********/

  // Devuelve una Lista cuyo primer elemento es la QueryTool del bloqueo
  //  y su segundo elemento es el Data del bloqueo
  private Lista prepararBloqueo() {
    Lista vResult = new Lista();
    QueryTool qtBloqueo = new QueryTool();
    Data dtBloqueo = new Data();

    // Tabla  a bloquear
    qtBloqueo.putName("SIVE_ALERTA_BROTES");

    // Campos CD_OPE y FC_ULTACT
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Filtro del registro a bloquear
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", alertaIni.getString("CD_ANO"));
    qtBloqueo.putOperator("CD_ANO", "=");
    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", alertaIni.getString("NM_ALERBRO"));
    qtBloqueo.putOperator("NM_ALERBRO", "=");

    // Valores del bloqueo
    dtBloqueo.put("CD_OPE", alertaIni.getString("CD_OPE"));
    dtBloqueo.put("FC_ULTACT", alertaIni.getString("FC_ULTACT"));

    // Devolucion
    vResult.addElement(qtBloqueo);
    vResult.addElement(dtBloqueo);
    return vResult;
  } // Fin prepararBloqueo()

  // Devuelve una Lista con las queries que debe procesar el
  //  servlet SrvAlerta para dar de alta una Alerta (3 tablas)
  private Lista prepararQTAlta(Data alerta) throws Exception {
    Lista lResul = new Lista();

    // INSERT INTO SIVE_ALERTA_BROTES (CD_ANO,NM_ALERBRO,FC_FECHAHORA,
    //  DS_ALERTA,CD_GRUPO,CD_NIVEL_1,CD_SITALERBRO,FC_ALERBRO,CD_OPE,FC_ULTACT)
    //  CD_LEXPROV,CD_E_NOTIF,CD_NIVEL_1_EX,CD_NIVEL_2_EX,CD_ZBS_EX,
    //  CD_NIVEL_2,NM_EXPUESTOS,NM_ENFERMOS,NM_INGHOSP,FC_EXPOSICION,
    //  FC_INISINTOMAS,DS_LEXP,CD_LEXMUN,DS_LEXPDIREC,CD_LEXPPOST,
    //  DS_LNMCALLE,DS_LEXPPISO,DS_LEXPTELEF,IT_VALIDADA,FC_FECVALID,
    //  VALUES(?,?,?,?,?,?,?,?,?,?)
    QueryTool qtS_A_B = new QueryTool();
    Data dtS_A_B = new Data();

    qtS_A_B.putName("SIVE_ALERTA_BROTES");

    qtS_A_B.putType("CD_ANO", QueryTool.STRING);
    qtS_A_B.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_A_B.putType("FC_FECHAHORA", QueryTool.TIMESTAMP);
    qtS_A_B.putType("DS_ALERTA", QueryTool.STRING);
    qtS_A_B.putType("CD_GRUPO", QueryTool.STRING);
    qtS_A_B.putType("CD_NIVEL_1", QueryTool.STRING);
    qtS_A_B.putType("CD_SITALERBRO", QueryTool.STRING);
    qtS_A_B.putType("FC_ALERBRO", QueryTool.DATE);
    qtS_A_B.putType("CD_OPE", QueryTool.STRING);
    qtS_A_B.putType("FC_ULTACT", QueryTool.TIMESTAMP);
    qtS_A_B.putType("CD_LEXPROV", QueryTool.STRING);
    //qtS_A_B.putType("CD_E_NOTIF",QueryTool.STRING);
    qtS_A_B.putType("CD_NIVEL_1_EX", QueryTool.STRING);
    qtS_A_B.putType("CD_NIVEL_2_EX", QueryTool.STRING);
    qtS_A_B.putType("CD_ZBS_EX", QueryTool.STRING);
    //qtS_A_B.putType("CD_NIVEL_2",QueryTool.STRING);
    qtS_A_B.putType("NM_EXPUESTOS", QueryTool.INTEGER);
    qtS_A_B.putType("NM_ENFERMOS", QueryTool.INTEGER);
    qtS_A_B.putType("NM_INGHOSP", QueryTool.INTEGER);
    qtS_A_B.putType("FC_EXPOSICION", QueryTool.TIMESTAMP);
    qtS_A_B.putType("FC_INISINTOMAS", QueryTool.TIMESTAMP);
    qtS_A_B.putType("DS_LEXP", QueryTool.STRING);
    qtS_A_B.putType("CD_LEXMUN", QueryTool.STRING);
    qtS_A_B.putType("DS_LEXPDIREC", QueryTool.STRING);
    qtS_A_B.putType("CD_LEXPPOST", QueryTool.STRING);
    qtS_A_B.putType("DS_LNMCALLE", QueryTool.STRING);
    qtS_A_B.putType("DS_LEXPPISO", QueryTool.STRING);
    qtS_A_B.putType("DS_LEXPTELEF", QueryTool.STRING);
    /*qtS_A_B.putType("IT_VALIDADA",QueryTool.STRING);
         qtS_A_B.putType("FC_FECVALID",QueryTool.DATE);*/

    qtS_A_B.putValue("CD_ANO", alerta.getString("CD_ANO"));
    qtS_A_B.putValue("NM_ALERBRO", "");
    qtS_A_B.putValue("FC_FECHAHORA", alerta.getString("FC_FECHAHORA"));
    qtS_A_B.putValue("DS_ALERTA", alerta.getString("DS_ALERTA"));
    qtS_A_B.putValue("CD_GRUPO", alerta.getString("CD_GRUPO"));
    qtS_A_B.putValue("CD_NIVEL_1", alerta.getString("CD_NIVEL_1"));
    qtS_A_B.putValue("CD_SITALERBRO", alerta.getString("CD_SITALERBRO"));
    qtS_A_B.putValue("FC_ALERBRO", "");
    qtS_A_B.putValue("CD_OPE", applet.getParametro("LOGIN"));
    qtS_A_B.putValue("FC_ULTACT", "");
    qtS_A_B.putValue("CD_LEXPROV", alerta.getString("CD_LEXPROV"));
    //qtS_A_B.putValue("CD_E_NOTIF",alerta.getString("CD_E_NOTIF"));
    qtS_A_B.putValue("CD_NIVEL_1_EX", alerta.getString("CD_NIVEL_1_EX"));
    qtS_A_B.putValue("CD_NIVEL_2_EX", alerta.getString("CD_NIVEL_2_EX"));
    qtS_A_B.putValue("CD_ZBS_EX", alerta.getString("CD_ZBS_EX"));
    //qtS_A_B.putValue("CD_NIVEL_2",alerta.getString("CD_NIVEL_2"));
    qtS_A_B.putValue("NM_EXPUESTOS", alerta.getString("NM_EXPUESTOS"));
    qtS_A_B.putValue("NM_ENFERMOS", alerta.getString("NM_ENFERMOS"));
    qtS_A_B.putValue("NM_INGHOSP", alerta.getString("NM_INGHOSP"));
    qtS_A_B.putValue("FC_EXPOSICION", alerta.getString("FC_EXPOSICION"));
    qtS_A_B.putValue("FC_INISINTOMAS", alerta.getString("FC_INISINTOMAS"));
    qtS_A_B.putValue("DS_LEXP", alerta.getString("DS_LEXP"));
    qtS_A_B.putValue("CD_LEXMUN", alerta.getString("CD_LEXMUN"));
    qtS_A_B.putValue("DS_LEXPDIREC", alerta.getString("DS_LEXPDIREC"));
    qtS_A_B.putValue("CD_LEXPPOST", alerta.getString("CD_LEXPPOST"));
    qtS_A_B.putValue("DS_LNMCALLE", alerta.getString("DS_LNMCALLE"));
    qtS_A_B.putValue("DS_LEXPPISO", alerta.getString("DS_LEXPPISO"));
    qtS_A_B.putValue("DS_LEXPTELEF", alerta.getString("DS_LEXPTELEF"));
    /*qtS_A_B.putValue("IT_VALIDADA",alerta.getString("IT_VALIDADA"));
         qtS_A_B.putValue("FC_FECVALID",alerta.getString("FC_FECVALID"));*/

    dtS_A_B.put("3", qtS_A_B); // 3 -> Inserci�n
    lResul.addElement(dtS_A_B);

    // INSERT INTO SIVE_ALERTA_ADIC (CD_ANO,NM_ALERBRO,CD_TNOTIF)
    //  VALUES(?,?,?)
    QueryTool qtS_A_A = new QueryTool();
    Data dtS_A_A = new Data();

    qtS_A_A.putName("SIVE_ALERTA_ADIC");

    qtS_A_A.putType("CD_ANO", QueryTool.STRING);
    qtS_A_A.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_A_A.putType("CD_TNOTIF", QueryTool.STRING);
    qtS_A_A.putType("DS_NOTIFINST", QueryTool.STRING);
    qtS_A_A.putType("DS_NOTIFICADOR", QueryTool.STRING);
    qtS_A_A.putType("CD_NOTIFPROV", QueryTool.STRING);
    qtS_A_A.putType("CD_NOTIFMUN", QueryTool.STRING);
    qtS_A_A.putType("DS_NOTIFDIREC", QueryTool.STRING);
    qtS_A_A.putType("DS_NNMCALLE", QueryTool.STRING);
    qtS_A_A.putType("DS_NOTIFPISO", QueryTool.STRING);
    qtS_A_A.putType("CD_NOTIFPOSTAL", QueryTool.STRING);
    qtS_A_A.putType("DS_NOTIFTELEF", QueryTool.STRING);
    qtS_A_A.putType("DS_MINFPER", QueryTool.STRING);
    qtS_A_A.putType("CD_MINPROV", QueryTool.STRING);
    qtS_A_A.putType("CD_MINMUN", QueryTool.STRING);
    qtS_A_A.putType("DS_MINFDIREC", QueryTool.STRING);
    qtS_A_A.putType("DS_MNMCALLE", QueryTool.STRING);
    qtS_A_A.putType("DS_MINFPISO", QueryTool.STRING);
    qtS_A_A.putType("CD_MINFPOSTAL", QueryTool.STRING);
    qtS_A_A.putType("DS_MINFTELEF", QueryTool.STRING);
    qtS_A_A.putType("DS_OBSERV", QueryTool.STRING);
    qtS_A_A.putType("DS_ALISOSP", QueryTool.STRING);
    qtS_A_A.putType("DS_SINTOMAS", QueryTool.STRING);

    qtS_A_A.putValue("CD_ANO", alerta.getString("CD_ANO"));
    qtS_A_A.putValue("NM_ALERBRO", "");
    qtS_A_A.putValue("CD_TNOTIF", alerta.getString("CD_TNOTIF"));
    qtS_A_A.putValue("DS_NOTIFINST", alerta.getString("DS_NOTIFINST"));
    qtS_A_A.putValue("DS_NOTIFICADOR", alerta.getString("DS_NOTIFICADOR"));
    qtS_A_A.putValue("CD_NOTIFPROV", alerta.getString("CD_NOTIFPROV"));
    qtS_A_A.putValue("CD_NOTIFMUN", alerta.getString("CD_NOTIFMUN"));
    qtS_A_A.putValue("DS_NOTIFDIREC", alerta.getString("DS_NOTIFDIREC"));
    qtS_A_A.putValue("DS_NNMCALLE", alerta.getString("DS_NNMCALLE"));
    qtS_A_A.putValue("DS_NOTIFPISO", alerta.getString("DS_NOTIFPISO"));
    qtS_A_A.putValue("CD_NOTIFPOSTAL", alerta.getString("CD_NOTIFPOSTAL"));
    qtS_A_A.putValue("DS_NOTIFTELEF", alerta.getString("DS_NOTIFTELEF"));
    qtS_A_A.putValue("DS_MINFPER", alerta.getString("DS_MINFPER"));
    qtS_A_A.putValue("CD_MINPROV", alerta.getString("CD_MINPROV"));
    qtS_A_A.putValue("CD_MINMUN", alerta.getString("CD_MINMUN"));
    qtS_A_A.putValue("DS_MINFDIREC", alerta.getString("DS_MINFDIREC"));
    qtS_A_A.putValue("DS_MNMCALLE", alerta.getString("DS_MNMCALLE"));
    qtS_A_A.putValue("DS_MINFPISO", alerta.getString("DS_MINFPISO"));
    qtS_A_A.putValue("CD_MINFPOSTAL", alerta.getString("CD_MINFPOSTAL"));
    qtS_A_A.putValue("DS_MINFTELEF", alerta.getString("DS_MINFTELEF"));
    qtS_A_A.putValue("DS_OBSERV", alerta.getString("DS_OBSERV"));
    qtS_A_A.putValue("DS_ALISOSP", alerta.getString("DS_ALISOSP"));
    qtS_A_A.putValue("DS_SINTOMAS", alerta.getString("DS_SINTOMAS"));

    dtS_A_A.put("3", qtS_A_A); // 3 -> Inserci�n
    lResul.addElement(dtS_A_A);

    // INSERT INTO SIVE_ALERTA_COLEC (CD_ANO,NM_ALERBRO,CD_PROV,
    //  CD_MUN,DS_NOMCOL,DS_DIRCOL,DS_NMCALLE,DS_PISOCOL,CD_POSTALCOL,
    //  DS_TELCOL) VALUES(?,?,?,?,?,?,?,?,?,?)
    QueryTool qtS_A_C = new QueryTool();
    Data dtS_A_C = new Data();

    qtS_A_C.putName("SIVE_ALERTA_COLEC");

    qtS_A_C.putType("CD_ANO", QueryTool.STRING);
    qtS_A_C.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_A_C.putType("CD_PROV", QueryTool.STRING);
    qtS_A_C.putType("CD_MUN", QueryTool.STRING);
    qtS_A_C.putType("DS_NOMCOL", QueryTool.STRING);
    qtS_A_C.putType("DS_DIRCOL", QueryTool.STRING);
    qtS_A_C.putType("DS_NMCALLE", QueryTool.STRING);
    qtS_A_C.putType("DS_PISOCOL", QueryTool.STRING);
    qtS_A_C.putType("CD_POSTALCOL", QueryTool.STRING);
    qtS_A_C.putType("DS_TELCOL", QueryTool.STRING);

    qtS_A_C.putValue("CD_ANO", alerta.getString("CD_ANO"));
    qtS_A_C.putValue("NM_ALERBRO", "");
    qtS_A_C.putValue("CD_PROV", alerta.getString("CD_PROV"));
    qtS_A_C.putValue("CD_MUN", alerta.getString("CD_MUN"));
    qtS_A_C.putValue("DS_NOMCOL", alerta.getString("DS_NOMCOL"));
    qtS_A_C.putValue("DS_DIRCOL", alerta.getString("DS_DIRCOL"));
    qtS_A_C.putValue("DS_NMCALLE", alerta.getString("DS_NMCALLE"));
    qtS_A_C.putValue("DS_PISOCOL", alerta.getString("DS_PISOCOL"));
    qtS_A_C.putValue("CD_POSTALCOL", alerta.getString("CD_POSTALCOL"));
    qtS_A_C.putValue("DS_TELCOL", alerta.getString("DS_TELCOL"));

    dtS_A_C.put("3", qtS_A_C); // 3 -> Inserci�n
    lResul.addElement(dtS_A_C);

    return lResul;
  } // Fin prepararQTAlta()

  // Devuelve una Lista con las queries que debe procesar el
  //  servlet SrvAlerta para modificar una Alerta (3 tablas)
  private Lista prepararQTModificacion(Data alerta) {
    Lista lResul = new Lista();

    // UPDATE SIVE_ALERTA_BROTES SET FC_FECHAHORA=?, DS_ALERTA=?,
    //  CD_GRUPO=?, CD_NIVEL_1=?, CD_SITALERBRO=?, FC_ALERBRO=?,
    //  CD_OPE=?, FC_ULTACT=?
    //  WHERE CD_ANO=? AND NM_ALERBRO=?
    QueryTool qtS_A_B = new QueryTool();
    Data dtS_A_B = new Data();

    qtS_A_B.putName("SIVE_ALERTA_BROTES");

    /*qtS_A_B.putType("CD_ANO",QueryTool.STRING);
         qtS_A_B.putType("NM_ALERBRO",QueryTool.INTEGER);*/
    qtS_A_B.putType("FC_FECHAHORA", QueryTool.TIMESTAMP);
    qtS_A_B.putType("DS_ALERTA", QueryTool.STRING);
    qtS_A_B.putType("CD_GRUPO", QueryTool.STRING);
    qtS_A_B.putType("CD_NIVEL_1", QueryTool.STRING);
    qtS_A_B.putType("CD_SITALERBRO", QueryTool.STRING);
    qtS_A_B.putType("FC_ALERBRO", QueryTool.DATE);
    qtS_A_B.putType("CD_OPE", QueryTool.STRING);
    qtS_A_B.putType("FC_ULTACT", QueryTool.TIMESTAMP);
    qtS_A_B.putType("CD_LEXPROV", QueryTool.STRING);
    //qtS_A_B.putType("CD_E_NOTIF",QueryTool.STRING);
    qtS_A_B.putType("CD_NIVEL_1_EX", QueryTool.STRING);
    qtS_A_B.putType("CD_NIVEL_2_EX", QueryTool.STRING);
    qtS_A_B.putType("CD_ZBS_EX", QueryTool.STRING);
    //qtS_A_B.putType("CD_NIVEL_2",QueryTool.STRING);
    qtS_A_B.putType("NM_EXPUESTOS", QueryTool.INTEGER);
    qtS_A_B.putType("NM_ENFERMOS", QueryTool.INTEGER);
    qtS_A_B.putType("NM_INGHOSP", QueryTool.INTEGER);
    qtS_A_B.putType("FC_EXPOSICION", QueryTool.TIMESTAMP);
    qtS_A_B.putType("FC_INISINTOMAS", QueryTool.TIMESTAMP);
    qtS_A_B.putType("DS_LEXP", QueryTool.STRING);
    qtS_A_B.putType("CD_LEXMUN", QueryTool.STRING);
    qtS_A_B.putType("DS_LEXPDIREC", QueryTool.STRING);
    qtS_A_B.putType("CD_LEXPPOST", QueryTool.STRING);
    qtS_A_B.putType("DS_LNMCALLE", QueryTool.STRING);
    qtS_A_B.putType("DS_LEXPPISO", QueryTool.STRING);
    qtS_A_B.putType("DS_LEXPTELEF", QueryTool.STRING);
    /*qtS_A_B.putType("IT_VALIDADA",QueryTool.STRING);
         qtS_A_B.putType("FC_FECVALID",QueryTool.DATE);*/

    /*qtS_A_B.putValue("CD_ANO",alerta.getString("CD_ANO"));
         qtS_A_B.putValue("NM_ALERBRO","");*/
    qtS_A_B.putValue("FC_FECHAHORA", alerta.getString("FC_FECHAHORA"));
    qtS_A_B.putValue("DS_ALERTA", alerta.getString("DS_ALERTA"));
    qtS_A_B.putValue("CD_GRUPO", alerta.getString("CD_GRUPO"));
    qtS_A_B.putValue("CD_NIVEL_1", alerta.getString("CD_NIVEL_1"));
    qtS_A_B.putValue("CD_SITALERBRO", alerta.getString("CD_SITALERBRO"));
    qtS_A_B.putValue("FC_ALERBRO", alerta.getString("FC_ALERBRO"));
    qtS_A_B.putValue("CD_OPE", applet.getParametro("LOGIN"));
    qtS_A_B.putValue("FC_ULTACT", "");
    qtS_A_B.putValue("CD_LEXPROV", alerta.getString("CD_LEXPROV"));
    //qtS_A_B.putValue("CD_E_NOTIF",alerta.getString("CD_E_NOTIF"));
    qtS_A_B.putValue("CD_NIVEL_1_EX", alerta.getString("CD_NIVEL_1_EX"));
    qtS_A_B.putValue("CD_NIVEL_2_EX", alerta.getString("CD_NIVEL_2_EX"));
    qtS_A_B.putValue("CD_ZBS_EX", alerta.getString("CD_ZBS_EX"));
    //qtS_A_B.putValue("CD_NIVEL_2",alerta.getString("CD_NIVEL_2"));
    qtS_A_B.putValue("NM_EXPUESTOS", alerta.getString("NM_EXPUESTOS"));
    qtS_A_B.putValue("NM_ENFERMOS", alerta.getString("NM_ENFERMOS"));
    qtS_A_B.putValue("NM_INGHOSP", alerta.getString("NM_INGHOSP"));
    qtS_A_B.putValue("FC_EXPOSICION", alerta.getString("FC_EXPOSICION"));
    qtS_A_B.putValue("FC_INISINTOMAS", alerta.getString("FC_INISINTOMAS"));
    qtS_A_B.putValue("DS_LEXP", alerta.getString("DS_LEXP"));
    qtS_A_B.putValue("CD_LEXMUN", alerta.getString("CD_LEXMUN"));
    qtS_A_B.putValue("DS_LEXPDIREC", alerta.getString("DS_LEXPDIREC"));
    qtS_A_B.putValue("CD_LEXPPOST", alerta.getString("CD_LEXPPOST"));
    qtS_A_B.putValue("DS_LNMCALLE", alerta.getString("DS_LNMCALLE"));
    qtS_A_B.putValue("DS_LEXPPISO", alerta.getString("DS_LEXPPISO"));
    qtS_A_B.putValue("DS_LEXPTELEF", alerta.getString("DS_LEXPTELEF"));
    /*qtS_A_B.putValue("IT_VALIDADA",alerta.getString("IT_VALIDADA"));
         qtS_A_B.putValue("FC_FECVALID",alerta.getString("FC_FECVALID"));*/

    // Parte del where
    qtS_A_B.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_A_B.putWhereValue("CD_ANO", alerta.getString("CD_ANO"));
    qtS_A_B.putOperator("CD_ANO", "=");
    qtS_A_B.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_A_B.putWhereValue("NM_ALERBRO", alerta.getString("NM_ALERBRO"));
    qtS_A_B.putOperator("NM_ALERBRO", "=");

    dtS_A_B.put("4", qtS_A_B); // 4 -> Modificaci�n
    lResul.addElement(dtS_A_B);

    // UPDATE SIVE_ALERTA_ADIC SET CD_TNOTIF=?
    //  WHERE CD_ANO=? AND NM_ALERBRO=?
    QueryTool qtS_A_A = new QueryTool();
    Data dtS_A_A = new Data();

    qtS_A_A.putName("SIVE_ALERTA_ADIC");

    /*qtS_A_A.putType("CD_ANO",QueryTool.STRING);
         qtS_A_A.putType("NM_ALERBRO",QueryTool.INTEGER);*/
    qtS_A_A.putType("CD_TNOTIF", QueryTool.STRING);
    qtS_A_A.putType("DS_NOTIFINST", QueryTool.STRING);
    qtS_A_A.putType("DS_NOTIFICADOR", QueryTool.STRING);
    qtS_A_A.putType("CD_NOTIFPROV", QueryTool.STRING);
    qtS_A_A.putType("CD_NOTIFMUN", QueryTool.STRING);
    qtS_A_A.putType("DS_NOTIFDIREC", QueryTool.STRING);
    qtS_A_A.putType("DS_NNMCALLE", QueryTool.STRING);
    qtS_A_A.putType("DS_NOTIFPISO", QueryTool.STRING);
    qtS_A_A.putType("CD_NOTIFPOSTAL", QueryTool.STRING);
    qtS_A_A.putType("DS_NOTIFTELEF", QueryTool.STRING);
    qtS_A_A.putType("DS_MINFPER", QueryTool.STRING);
    qtS_A_A.putType("CD_MINPROV", QueryTool.STRING);
    qtS_A_A.putType("CD_MINMUN", QueryTool.STRING);
    qtS_A_A.putType("DS_MINFDIREC", QueryTool.STRING);
    qtS_A_A.putType("DS_MNMCALLE", QueryTool.STRING);
    qtS_A_A.putType("DS_MINFPISO", QueryTool.STRING);
    qtS_A_A.putType("CD_MINFPOSTAL", QueryTool.STRING);
    qtS_A_A.putType("DS_MINFTELEF", QueryTool.STRING);
    qtS_A_A.putType("DS_OBSERV", QueryTool.STRING);
    qtS_A_A.putType("DS_ALISOSP", QueryTool.STRING);
    qtS_A_A.putType("DS_SINTOMAS", QueryTool.STRING);

    /*qtS_A_A.putValue("CD_ANO",alerta.getString("CD_ANO"));
         qtS_A_A.putValue("NM_ALERBRO","");*/
    qtS_A_A.putValue("CD_TNOTIF", alerta.getString("CD_TNOTIF"));
    qtS_A_A.putValue("DS_NOTIFINST", alerta.getString("DS_NOTIFINST"));
    qtS_A_A.putValue("DS_NOTIFICADOR", alerta.getString("DS_NOTIFICADOR"));
    qtS_A_A.putValue("CD_NOTIFPROV", alerta.getString("CD_NOTIFPROV"));
    qtS_A_A.putValue("CD_NOTIFMUN", alerta.getString("CD_NOTIFMUN"));
    qtS_A_A.putValue("DS_NOTIFDIREC", alerta.getString("DS_NOTIFDIREC"));
    qtS_A_A.putValue("DS_NNMCALLE", alerta.getString("DS_NNMCALLE"));
    qtS_A_A.putValue("DS_NOTIFPISO", alerta.getString("DS_NOTIFPISO"));
    qtS_A_A.putValue("CD_NOTIFPOSTAL", alerta.getString("CD_NOTIFPOSTAL"));
    qtS_A_A.putValue("DS_NOTIFTELEF", alerta.getString("DS_NOTIFTELEF"));
    qtS_A_A.putValue("DS_MINFPER", alerta.getString("DS_MINFPER"));
    qtS_A_A.putValue("CD_MINPROV", alerta.getString("CD_MINPROV"));
    qtS_A_A.putValue("CD_MINMUN", alerta.getString("CD_MINMUN"));
    qtS_A_A.putValue("DS_MINFDIREC", alerta.getString("DS_MINFDIREC"));
    qtS_A_A.putValue("DS_MNMCALLE", alerta.getString("DS_MNMCALLE"));
    qtS_A_A.putValue("DS_MINFPISO", alerta.getString("DS_MINFPISO"));
    qtS_A_A.putValue("CD_MINFPOSTAL", alerta.getString("CD_MINFPOSTAL"));
    qtS_A_A.putValue("DS_MINFTELEF", alerta.getString("DS_MINFTELEF"));
    qtS_A_A.putValue("DS_OBSERV", alerta.getString("DS_OBSERV"));
    qtS_A_A.putValue("DS_ALISOSP", alerta.getString("DS_ALISOSP"));
    qtS_A_A.putValue("DS_SINTOMAS", alerta.getString("DS_SINTOMAS"));

    // Parte del where
    qtS_A_A.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_A_A.putWhereValue("CD_ANO", alerta.getString("CD_ANO"));
    qtS_A_A.putOperator("CD_ANO", "=");
    qtS_A_A.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_A_A.putWhereValue("NM_ALERBRO", alerta.getString("NM_ALERBRO"));
    qtS_A_A.putOperator("NM_ALERBRO", "=");

    dtS_A_A.put("4", qtS_A_A); // 4 -> Modificaci�n
    lResul.addElement(dtS_A_A);

    // UPDATE SIVE_ALERTA_COLEC SET CD_PROV=?,CD_MUN=?,
    //  DS_NOMCOL=?,DS_DIRCOL=?,DS_NMCALLE=?,DS_PISOCOL=?,
    //  CD_POSTALCOL=?,DS_TELCOL=?
    //  WHERE CD_ANO=? AND NM_ALERBRO=?
    QueryTool qtS_A_C = new QueryTool();
    Data dtS_A_C = new Data();

    qtS_A_C.putName("SIVE_ALERTA_COLEC");

    /*qtS_A_C.putType("CD_ANO",QueryTool.STRING);
         qtS_A_C.putType("NM_ALERBRO",QueryTool.INTEGER);*/
    qtS_A_C.putType("CD_PROV", QueryTool.STRING);
    qtS_A_C.putType("CD_MUN", QueryTool.STRING);
    qtS_A_C.putType("DS_NOMCOL", QueryTool.STRING);
    qtS_A_C.putType("DS_DIRCOL", QueryTool.STRING);
    qtS_A_C.putType("DS_NMCALLE", QueryTool.STRING);
    qtS_A_C.putType("DS_PISOCOL", QueryTool.STRING);
    qtS_A_C.putType("CD_POSTALCOL", QueryTool.STRING);
    qtS_A_C.putType("DS_TELCOL", QueryTool.STRING);

    /*qtS_A_C.putValue("CD_ANO",alerta.getString("CD_ANO"));
         qtS_A_C.putValue("NM_ALERBRO","");*/
    qtS_A_C.putValue("CD_PROV", alerta.getString("CD_PROV"));
    qtS_A_C.putValue("CD_MUN", alerta.getString("CD_MUN"));
    qtS_A_C.putValue("DS_NOMCOL", alerta.getString("DS_NOMCOL"));
    qtS_A_C.putValue("DS_DIRCOL", alerta.getString("DS_DIRCOL"));
    qtS_A_C.putValue("DS_NMCALLE", alerta.getString("DS_NMCALLE"));
    qtS_A_C.putValue("DS_PISOCOL", alerta.getString("DS_PISOCOL"));
    qtS_A_C.putValue("CD_POSTALCOL", alerta.getString("CD_POSTALCOL"));
    qtS_A_C.putValue("DS_TELCOL", alerta.getString("DS_TELCOL"));

    // Parte del where
    qtS_A_C.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_A_C.putWhereValue("CD_ANO", alerta.getString("CD_ANO"));
    qtS_A_C.putOperator("CD_ANO", "=");
    qtS_A_C.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_A_C.putWhereValue("NM_ALERBRO", alerta.getString("NM_ALERBRO"));
    qtS_A_C.putOperator("NM_ALERBRO", "=");

    dtS_A_C.put("4", qtS_A_C); // 4 -> Modificaci�n
    lResul.addElement(dtS_A_C);

    return lResul;
  } // Fin prepararQTModificacion()

  // Devuelve una Lista con las queries que debe procesar el
  //  servlet SrvAlerta para dar de baja una Alerta (3 tablas)
  private Lista prepararQTBaja(Data alerta) {
    Lista lResul = new Lista();

    // DELETE FROM SIVE_ALERTA_ADIC
    //  WHERE CD_ANO=? AND NM_ALERBRO=?
    QueryTool qtS_A_A = new QueryTool();
    Data dtS_A_A = new Data();

    qtS_A_A.putName("SIVE_ALERTA_ADIC");

    qtS_A_A.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_A_A.putWhereValue("CD_ANO", alerta.getString("CD_ANO"));
    qtS_A_A.putOperator("CD_ANO", "=");
    qtS_A_A.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_A_A.putWhereValue("NM_ALERBRO", alerta.getString("NM_ALERBRO"));
    qtS_A_A.putOperator("NM_ALERBRO", "=");

    dtS_A_A.put("5", qtS_A_A); // 5 -> Baja
    lResul.addElement(dtS_A_A);

    // DELETE FROM SIVE_ALERTA_COLEC
    //  WHERE CD_ANO=? AND NM_ALERBRO=?
    QueryTool qtS_A_C = new QueryTool();
    Data dtS_A_C = new Data();

    qtS_A_C.putName("SIVE_ALERTA_COLEC");

    qtS_A_C.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_A_C.putWhereValue("CD_ANO", alerta.getString("CD_ANO"));
    qtS_A_C.putOperator("CD_ANO", "=");
    qtS_A_C.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_A_C.putWhereValue("NM_ALERBRO", alerta.getString("NM_ALERBRO"));
    qtS_A_C.putOperator("NM_ALERBRO", "=");

    dtS_A_C.put("5", qtS_A_C); // 5 -> Baja
    lResul.addElement(dtS_A_C);

    // BORRADO DE HOSPITALES
    // DELETE FROM SIVE_HOSPITAL_ALERT
    //  WHERE CD_ANO=? AND NM_ALERBRO=?
    QueryTool qtS_H_A = new QueryTool();
    Data dtS_H_A = new Data();

    qtS_H_A.putName("SIVE_HOSPITAL_ALERT");

    qtS_H_A.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_H_A.putWhereValue("CD_ANO", alerta.getString("CD_ANO"));
    qtS_H_A.putOperator("CD_ANO", "=");
    qtS_H_A.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_H_A.putWhereValue("NM_ALERBRO", alerta.getString("NM_ALERBRO"));
    qtS_H_A.putOperator("NM_ALERBRO", "=");

    dtS_H_A.put("5", qtS_H_A); // 5 -> Baja
    lResul.addElement(dtS_H_A);

    // LA ULTIMA PORQUE LAS DEMAS DEPENDEN DE ESTE REGISTRO
    // DELETE FROM SIVE_ALERTA_BROTES
    //  WHERE CD_ANO=? AND NM_ALERBRO=?
    QueryTool qtS_A_B = new QueryTool();
    Data dtS_A_B = new Data();

    qtS_A_B.putName("SIVE_ALERTA_BROTES");

    qtS_A_B.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_A_B.putWhereValue("CD_ANO", alerta.getString("CD_ANO"));
    qtS_A_B.putOperator("CD_ANO", "=");
    qtS_A_B.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_A_B.putWhereValue("NM_ALERBRO", alerta.getString("NM_ALERBRO"));
    qtS_A_B.putOperator("NM_ALERBRO", "=");

    dtS_A_B.put("5", qtS_A_B); // 5 -> Baja
    lResul.addElement(dtS_A_B);

    return lResul;
  } // Fin prepararQTBaja()

} // endclass DiaAlerta

/******************* ESCUCHADORES **********************/

// Botones
class DiaAlertaActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaAlerta adaptee;
  ActionEvent evt;

  DiaAlertaActionAdapter(DiaAlerta adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if (evt.getActionCommand().equals("Grabar")) {
        adaptee.btnGrabarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Salir")) {
        adaptee.btnSalirActionPerformed();
      }
      else if (evt.getActionCommand().equals("Hospitales")) {
        adaptee.btnHospActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  DiaAlertaActionAdapter
