package brotes.cliente.distredad;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import comun.Common;
import jclass.bwt.BWTEnum;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;
import sapp2.StubSrvBD;

//import brotes.servidor.distredad.*;

/**
 * Panel a trav�s del que se realizan los mantenimientos
 * de la distribuci�n de grupos de edad y sexo.
 * @autor PDP
 * @version 1.0
 */

public class PanDisEdad
    extends CDialog
    implements CInicializar, CFiltro {
//  BorderLayout borderLayout1 = new BorderLayout();

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;
  public int modoActualizacion = ALTA;

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  //Retorno de alta
  public boolean bFirst = false;

  // Estructuras para comprobar el bloqueo
  private QueryTool qtBloqueo = null;
  private Data dtBloqueo = null;

  final String servletDistredad = "servlet/SrvPanDisEdad";

  // N�mero total de registros en la tabla en cualquier momento
  public int indTotal = 0;

  // N�mero inicial de registros en la tabla
  public int indTotalIni = 0;

  // Lista de registros para dar de baja
  private Lista lismanBaj = new Lista();

  Label lblBroteDesc = new Label();
  Label lblDatosBrote = new Label();

  CListaMantenimiento clmMantDis = null;

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  DialogActionAdapter1 ActionAdapter = new DialogActionAdapter1(this);

  private boolean OK = false;
  Lista lBd = new Lista();

  Data dtBrote = null;
  XYLayout xYLayout1 = new XYLayout();

  public PanDisEdad(CApp a, int mod, Data dt) {
    super(a);

    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    QueryTool2 qt = new QueryTool2();
    QueryTool qtP = new QueryTool();

    try {

      dtBrote = dt;

      this.app = a;
      modoActualizacion = mod;

      //botones
      vBotones.addElement(new CBoton("",
                                     "images/alta2.gif",
                                     "Nueva Distribuci�n",
                                     false,
                                     false));

      vBotones.addElement(new CBoton("",
                                     "images/modificacion2.gif",
                                     "Modificar Distribuci�n",
                                     true,
                                     true));

      vBotones.addElement(new CBoton("",
                                     "images/baja2.gif",
                                     "Borrar Distribuci�n",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("Edad",
                                      "79",
                                      "DS_DIST_EDAD"));

      vLabels.addElement(new CColumna("ExpV",
                                      "40",
                                      "NM_V_EXP"));

      vLabels.addElement(new CColumna("ExpM",
                                      "40",
                                      "NM_M_EXP"));

      vLabels.addElement(new CColumna("ExpNc",
                                      "40",
                                      "NM_NC_EXP"));

      vLabels.addElement(new CColumna("T",
                                      "30",
                                      "NM_V_EXP + NM_M_EXP + NM_NC_EXP"));

      vLabels.addElement(new CColumna("EnfV",
                                      "40",
                                      "NM_V_ENF"));

      vLabels.addElement(new CColumna("EnfM",
                                      "40",
                                      "NM_M_ENF"));

      vLabels.addElement(new CColumna("EnfNc",
                                      "40",
                                      "NM_NC_ENF"));

      vLabels.addElement(new CColumna("T",
                                      "30",
                                      "NM_V_ENF + NM_M_ENF + NM_NC_ENF"));

      vLabels.addElement(new CColumna("HosV",
                                      "40",
                                      "NM_V_HOS"));

      vLabels.addElement(new CColumna("HosM",
                                      "40",
                                      "NM_M_HOS"));

      vLabels.addElement(new CColumna("HosNc",
                                      "40",
                                      "NM_NC_HOS"));

      vLabels.addElement(new CColumna("T",
                                      "30",
                                      "NM_V_HOS + NM_M_HOS + NM_NC_HOS"));

      vLabels.addElement(new CColumna("DefV",
                                      "40",
                                      "NM_V_DEF"));

      vLabels.addElement(new CColumna("DefM",
                                      "40",
                                      "NM_M_DEF"));

      vLabels.addElement(new CColumna("DefNc",
                                      "40",
                                      "NM_NC_DEF"));

      vLabels.addElement(new CColumna("T",
                                      "28",
                                      "NM_V_DEF + NM_M_DEF + NM_NC_DEF"));

      int[] ajustar = new int[17];
      ajustar[0] = BWTEnum.TOPLEFT;
      ajustar[1] = BWTEnum.TOPRIGHT;
      ajustar[2] = BWTEnum.TOPRIGHT;
      ajustar[3] = BWTEnum.TOPRIGHT;
      ajustar[4] = BWTEnum.TOPRIGHT;
      ajustar[5] = BWTEnum.TOPRIGHT;
      ajustar[6] = BWTEnum.TOPRIGHT;
      ajustar[7] = BWTEnum.TOPRIGHT;
      ajustar[8] = BWTEnum.TOPRIGHT;
      ajustar[9] = BWTEnum.TOPRIGHT;
      ajustar[10] = BWTEnum.TOPRIGHT;
      ajustar[11] = BWTEnum.TOPRIGHT;
      ajustar[12] = BWTEnum.TOPRIGHT;
      ajustar[13] = BWTEnum.TOPRIGHT;
      ajustar[14] = BWTEnum.TOPRIGHT;
      ajustar[15] = BWTEnum.TOPRIGHT;
      ajustar[16] = BWTEnum.TOPRIGHT;

      clmMantDis = new CListaMantenimiento(a,
                                           vLabels,
                                           vBotones,
                                           ajustar,
                                           this,
                                           this);
      jbInit();
      Lista lismanIni = this.clmMantDis.getLista();
      indTotalIni = lismanIni.size();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setSize(750, 430);
    this.setTitle("Distribuci�n por grupos de edad y sexo");

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    this.setLayout(xYLayout1);
    lblBroteDesc.setText("Brote:");
    lblDatosBrote.setText(dtBrote.getString("CD_ANO") + "/" +
                          dtBrote.getString("NM_ALERBRO") + " - " +
                          dtBrote.getString("DS_BROTE"));
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(ActionAdapter);
    btnCancelar.addActionListener(ActionAdapter);

    btnAceptar.setEnabled(true);
    btnCancelar.setEnabled(true);

    this.add(lblBroteDesc, new XYConstraints(29, 23, 42, -1));
    this.add(lblDatosBrote, new XYConstraints(82, 23, 600, -1));

    this.add(clmMantDis, new XYConstraints(20, 50, 800, 300));
    this.add(btnAceptar, new XYConstraints(511, 350, 88, 29));
    this.add(btnCancelar, new XYConstraints(622, 350, 88, 29));

    Inicializar(CInicializar.ESPERA);
    clmMantDis.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
  }

  public void GenerarNc(Lista lis) {

    /*
         //Generamos el registro de edad no conocida
         int vExp = 0;
         int mExp = 0;
         int ncExp = 0;
         int tExp = 0;
         int vEnf = 0;
         int mEnf = 0;
         int ncEnf = 0;
         int tEnf = 0;
         int vHos = 0;
         int mHos = 0;
         int ncHos = 0;
         int tHos = 0;
         int vDef = 0;
         int mDef = 0;
         int ncDef = 0;
         int tDef = 0;
         int iTotal = 0;
         String sDS_DIST_EDAD = "";
         int indNC = 0;
         Data fila = null;
         fila = new Data();
         for (int i=0; i<lis.size(); i++){
      fila = (Data)lis.elementAt(i);
      iTotal = lis.size();
      sDS_DIST_EDAD = (String) fila.getString("DS_DIST_EDAD");
     if (sDS_DIST_EDAD.equals("NC")){
        indNC =i;
         //sDS_DIST_EDAD = "NC";
      vExp = Integer.parseInt( fila.getString("NM_V_EXP"));
      mExp = Integer.parseInt( fila.getString("NM_M_EXP"));
      ncExp = Integer.parseInt( fila.getString("NM_NC_EXP"));
         tExp = Integer.parseInt(fila.getString("NM_V_EXP + NM_M_EXP + NM_NC_EXP"));
      vEnf = Integer.parseInt( fila.getString("NM_V_ENF"));
      mEnf = Integer.parseInt( fila.getString("NM_M_ENF"));
      ncEnf = Integer.parseInt( fila.getString("NM_NC_ENF"));
         tEnf = Integer.parseInt(fila.getString("NM_V_ENF + NM_M_ENF + NM_NC_ENF"));
      vHos = Integer.parseInt( fila.getString("NM_V_HOS"));
      mHos = Integer.parseInt( fila.getString("NM_M_HOS"));
      ncHos = Integer.parseInt( fila.getString("NM_NC_HOS"));
         tHos = Integer.parseInt(fila.getString("NM_V_HOS + NM_M_HOS + NM_NC_HOS"));
      vDef = Integer.parseInt( fila.getString("NM_V_DEF"));
      mDef = Integer.parseInt( fila.getString("NM_M_DEF"));
      ncDef = Integer.parseInt( fila.getString("NM_NC_DEF"));
         tDef = Integer.parseInt(fila.getString("NM_V_DEF + NM_M_DEF + NM_NC_DEF"));
      fila = new Data();
      fila.put("NM_V_EXP", (new Integer(vExp)).toString());
      fila.put("NM_M_EXP", (new Integer(mExp)).toString());
      fila.put("NM_NC_EXP", (new Integer(ncExp)).toString());
         fila.put("NM_V_EXP + NM_M_EXP + NM_NC_EXP", (new Integer(tExp)).toString());
      fila.put("NM_V_ENF", (new Integer(vEnf)).toString());
      fila.put("NM_M_ENF", (new Integer(mEnf)).toString());
      fila.put("NM_NC_ENF", (new Integer(ncEnf)).toString());
         fila.put("NM_V_ENF + NM_M_ENF + NM_NC_ENF", (new Integer(tEnf)).toString());
      fila.put("NM_V_HOS", (new Integer(vHos)).toString());
      fila.put("NM_M_HOS", (new Integer(mHos)).toString());
      fila.put("NM_NC_HOS", (new Integer(ncHos)).toString());
         fila.put("NM_V_HOS + NM_M_HOS + NM_NC_HOS", (new Integer(tHos)).toString());
      fila.put("NM_V_DEF", (new Integer(vDef)).toString());
      fila.put("NM_M_DEF", (new Integer(mDef)).toString());
      fila.put("NM_NC_DEF", (new Integer(ncDef)).toString());
         fila.put("NM_V_DEF + NM_M_DEF + NM_NC_DEF", (new Integer(tDef)).toString());
      fila.put("DS_DIST_EDAD", "NC");
      lis.addElement(fila);
      //Borramos el registro que no tinene descripci�n
      lis.removeElementAt(indNC);
     }//fin if
         }//fin for
     */
  }

  public void ObtenerTotales(Lista lis) {
    int vExp = 0;
    int mExp = 0;
    int ncExp = 0;
    int tExp = 0;
    int vEnf = 0;
    int mEnf = 0;
    int ncEnf = 0;
    int tEnf = 0;
    int vHos = 0;
    int mHos = 0;
    int ncHos = 0;
    int tHos = 0;
    int vDef = 0;
    int mDef = 0;
    int ncDef = 0;
    int tDef = 0;
    String total = "Total";
    String sNM_V_EXP = "";
    String sDS_DIST_EDAD = "";
    int iTotal = 0;

    Data fila = null;

    for (int i = 0; i < lis.size(); i++) {
      fila = (Data) lis.elementAt(i);
      iTotal = lis.size();

      sDS_DIST_EDAD = (String) fila.getString("DS_DIST_EDAD");
      vExp += Integer.parseInt(fila.getString("NM_V_EXP"));
      mExp += Integer.parseInt(fila.getString("NM_M_EXP"));
      ncExp += Integer.parseInt(fila.getString("NM_NC_EXP"));
      tExp += Integer.parseInt(fila.getString("NM_V_EXP + NM_M_EXP + NM_NC_EXP"));
      vEnf += Integer.parseInt(fila.getString("NM_V_ENF"));
      mEnf += Integer.parseInt(fila.getString("NM_M_ENF"));
      ncEnf += Integer.parseInt(fila.getString("NM_NC_ENF"));
      tEnf += Integer.parseInt(fila.getString("NM_V_ENF + NM_M_ENF + NM_NC_ENF"));
      vHos += Integer.parseInt(fila.getString("NM_V_HOS"));
      mHos += Integer.parseInt(fila.getString("NM_M_HOS"));
      ncHos += Integer.parseInt(fila.getString("NM_NC_HOS"));
      tHos += Integer.parseInt(fila.getString("NM_V_HOS + NM_M_HOS + NM_NC_HOS"));
      vDef += Integer.parseInt(fila.getString("NM_V_DEF"));
      mDef += Integer.parseInt(fila.getString("NM_M_DEF"));
      ncDef += Integer.parseInt(fila.getString("NM_NC_DEF"));
      tDef += Integer.parseInt(fila.getString("NM_V_DEF + NM_M_DEF + NM_NC_DEF"));
    } //fin for

    fila = new Data();

    fila.put("NM_V_EXP", (new Integer(vExp)).toString());
    fila.put("NM_M_EXP", (new Integer(mExp)).toString());
    fila.put("NM_NC_EXP", (new Integer(ncExp)).toString());
    fila.put("NM_V_EXP + NM_M_EXP + NM_NC_EXP", (new Integer(tExp)).toString());
    fila.put("NM_V_ENF", (new Integer(vEnf)).toString());
    fila.put("NM_M_ENF", (new Integer(mEnf)).toString());
    fila.put("NM_NC_ENF", (new Integer(ncEnf)).toString());
    fila.put("NM_V_ENF + NM_M_ENF + NM_NC_ENF", (new Integer(tEnf)).toString());
    fila.put("NM_V_HOS", (new Integer(vHos)).toString());
    fila.put("NM_M_HOS", (new Integer(mHos)).toString());
    fila.put("NM_NC_HOS", (new Integer(ncHos)).toString());
    fila.put("NM_V_HOS + NM_M_HOS + NM_NC_HOS", (new Integer(tHos)).toString());
    fila.put("NM_V_DEF", (new Integer(vDef)).toString());
    fila.put("NM_M_DEF", (new Integer(mDef)).toString());
    fila.put("NM_NC_DEF", (new Integer(ncDef)).toString());
    fila.put("NM_V_DEF + NM_M_DEF + NM_NC_DEF", (new Integer(tDef)).toString());
    fila.put("DS_DIST_EDAD", (new String(total)));
    lis.addElement(fila);
  }

  public void Inicializar() {}

  public void Inicializar(int i) {
    // Data d = null;
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if ( (modoActualizacion == BAJA) || (modoActualizacion == CONSULTA)) {
          this.setEnabled(false);
          btnAceptar.setVisible(false);
          btnCancelar.setLabel("Salir");
          btnCancelar.setEnabled(true);
          clmMantDis.setEnabled(false);
        }
        else {
          this.setEnabled(true);
          btnAceptar.setEnabled(true);
          btnCancelar.setEnabled(true);
          clmMantDis.setEnabled(true);
        }
    }
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvQueryTool";

    try {
      Data fila = null;
      fila = new Data();
      QueryTool2 qt = new QueryTool2();
      qt.putName("SIVE_BROTES_DISTGEDAD");
      qt.putType("NM_DISTGEDAD", QueryTool.INTEGER);
      qt.putType("CD_ANO", QueryTool.STRING);
      qt.putType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putType("IT_EDAD", QueryTool.STRING);
      qt.putType("CD_GEDAD", QueryTool.STRING);
      qt.putType("DIST_EDAD_I", QueryTool.INTEGER);
      qt.putType("DIST_EDAD_F", QueryTool.INTEGER);
      qt.putType("NM_V_EXP", QueryTool.INTEGER);
      qt.putType("NM_M_EXP", QueryTool.INTEGER);
      qt.putType("NM_NC_EXP", QueryTool.INTEGER);
      qt.putType("NM_V_EXP + NM_M_EXP + NM_NC_EXP", QueryTool.INTEGER);
      qt.putType("NM_V_ENF", QueryTool.INTEGER);
      qt.putType("NM_M_ENF", QueryTool.INTEGER);
      qt.putType("NM_NC_ENF", QueryTool.INTEGER);
      qt.putType("NM_V_ENF + NM_M_ENF + NM_NC_ENF", QueryTool.INTEGER);
      qt.putType("NM_V_HOS", QueryTool.INTEGER);
      qt.putType("NM_M_HOS", QueryTool.INTEGER);
      qt.putType("NM_NC_HOS", QueryTool.INTEGER);
      qt.putType("NM_V_HOS + NM_M_HOS + NM_NC_HOS", QueryTool.INTEGER);
      qt.putType("NM_V_DEF", QueryTool.INTEGER);
      qt.putType("NM_M_DEF", QueryTool.INTEGER);
      qt.putType("NM_NC_DEF", QueryTool.INTEGER);
      qt.putType("NM_V_DEF + NM_M_DEF + NM_NC_DEF", QueryTool.INTEGER);
      qt.putType("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F",
                 QueryTool.STRING);

      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");

      qt.putWhereType("NM_ALERBRO", QueryTool.STRING);
      qt.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");

      qt.addOrderField("DIST_EDAD_F");
// Comentado el 20-04-01 (ARS)
      /*          // Se comprueba si es TIA
                if (dtBrote.getString("CD_GRUPO").equals("0")){
                  //QueryTool qtA1 = new QueryTool2();
                  qt.putWhereType("CD_GEDAD", QueryTool.STRING);
                  qt.putWhereValue("CD_GEDAD","001"); //  C�digo de TIA
                  qt.putOperator("CD_GEDAD","=");
                }*/
      QueryTool qtA1 = new QueryTool();
      qtA1.putName("SIVE_DISTR_GEDAD");
      qtA1.putType("DS_DIST_EDAD", QueryTool.STRING);
      Data dtA1 = new Data();
      dtA1.put("CD_GEDAD", QueryTool.STRING);
      dtA1.put("DIST_EDAD_I", QueryTool.INTEGER);
      dtA1.put("DIST_EDAD_F", QueryTool.INTEGER);
      qt.addQueryTool(qtA1);
      qt.addColumnsQueryTool(dtA1);

      //otros datos
      QueryTool qtD = new QueryTool();
      qtD.putName("SIVE_GEDAD");
      qtD.putType("DS_GEDAD", QueryTool.STRING);
      Data dtA2 = new Data();
      dtA2.put("CD_GEDAD", QueryTool.STRING);
      qt.addQueryTool(qtD);
      qt.addColumnsQueryTool(dtA2);

      p.addElement(qt);

      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      p1 = (Lista)this.getApp().getStub().doPost(1, p);
      /*
                 if (p1.size()==0) {
       this.getApp().showAdvise("No se poseen elementos");
                 }
       */
    }
    catch (Exception ex) {
      ex.printStackTrace();
      this.getApp().trazaLog(ex);
    }
// Comentado el 20-04-01: ARS
    /*   if (!dtBrote.getString("CD_GRUPO").equals("0")){
          GenerarNc(p1);
       }*/

    ObtenerTotales(p1);
    return p1;
  }

  public void realizaOperacion(int j) {

    Lista lisman = this.clmMantDis.getLista();
    int ind = clmMantDis.getSelectedIndex();
    indTotal = lisman.size();

    DiaDis dlg = null;
    Data dMantDis = new Data();
    Data dtResultado = new Data();

    switch (j) {
      // bot�n de alta:data vacio
      case ALTA:
        dlg = new DiaDis(this.getApp(), 0, dtBrote, null, lisman);
        dlg.show();
        // a�adir el nuevo elem a la lista
        if (dlg.bAceptar()) {
          lisman.removeElementAt(indTotal - 1);
          dtResultado = dlg.devuelveData();
          lisman.addElement(dtResultado);
          ObtenerTotales(lisman);
          clmMantDis.setPrimeraPagina(lisman);
        }
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        dMantDis = clmMantDis.getSelected();
        String sTotal = dMantDis.getString("DS_DIST_EDAD");

        //si existe alguna fila seleccionada
        if ( (dMantDis != null) && (!sTotal.equals("Total"))) {
          dlg = new DiaDis(this.getApp(), 1, dtBrote, dMantDis, lisman);
          dlg.show();
          //Tratamiento de bAceptar para Salir
          if (dlg.bAceptar()) {
            lisman.removeElementAt(ind);
            lisman.removeElementAt(indTotal - 2);
            dtResultado = dlg.devuelveData();
            lisman.addElement(dtResultado);
            ObtenerTotales(lisman);
            clmMantDis.setPrimeraPagina(lisman);
          }
        }
        else {
          if (dMantDis == null) {
            this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
          }
          else {
            this.getApp().showAdvise(
                "Debe seleccionar un registro en la tabla diferente a la linea de totales");
          }
        }
        break;

      case BAJA:
        dMantDis = clmMantDis.getSelected();
        String sTotal2 = dMantDis.getString("DS_DIST_EDAD");
        if ( (dMantDis != null) && (!sTotal2.equals("Total"))) {
          dlg = new DiaDis(this.getApp(), 2, dtBrote, dMantDis, lisman);
          dlg.show();
          if (dlg.bAceptar()) {
            lismanBaj.addElement(clmMantDis.getSelected());
            lisman.removeElementAt(ind);
            lisman.removeElementAt(indTotal - 2);
            ObtenerTotales(lisman);
            clmMantDis.setPrimeraPagina(lisman);
          }
        }
        else {
          if (dMantDis == null) {
            this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
          }
          else {
            this.getApp().showAdvise(
                "Debe seleccionar un registro en la tabla diferente a la linea de totales");
          }
        }
        break;
    }

  }

  public Lista siguientePagina() {
    Lista v = null;
    return v;
  }

  void btn_actionPerformed(ActionEvent e) {
    Inicializar(CInicializar.ESPERA);
    Lista lstMant = new Lista();
    Lista lstOperar = new Lista();
    Lista vResultado = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {
      lstMant = this.clmMantDis.getLista();

      //recorro la lista q tengo para altas y modificaciones
      for (int i = 0; i < lstMant.size(); i++) {
        Data dtMant = (Data) lstMant.elementAt(i);

        //Se realiza un alta
        if (dtMant.getString("TIPO_OPERACION").equals("A")) {
          QueryTool qtIns = new QueryTool();
          qtIns = realizarInsert(dtMant);
          Data dtInsert = new Data();
          dtInsert.put("10003", qtIns);
          lBd.addElement(dtInsert);
        } //end if ALTA

        if (dtMant.getString("TIPO_OPERACION").equals("M")) {
          QueryTool qtUpd = new QueryTool();
          qtUpd = realizarUpdate(dtMant);
          Data dtUpdate = new Data();
          dtUpdate.put("10006", qtUpd);
          lBd.addElement(dtUpdate);
        }
      } //end for
      //lista para bajas
      for (int i = 0; i < lismanBaj.size(); i++) {
        Data dtMantBaja = (Data) lismanBaj.elementAt(i);
        String sMod = dtMantBaja.getString("TIPO_OPERACION");
        if (!sMod.equals("A")) {
          QueryTool qtDel = new QueryTool();
          qtDel = realizarBaja(dtMantBaja);
          Data dtDel = new Data();
          dtDel.put("10007", qtDel);
          lBd.addElement(dtDel);
        }
      } //end for lBajas

      //si se ha modificado algo:se inserta para ese brote el nuevo
      //cd_ope y fc_ultact
      if (lBd.size() != 0) {
        Data dtDatBrot = new Data();
        dtDatBrot.put("CD_OPE", dtBrote.getString("CD_OPE"));
        dtDatBrot.put("FC_ULTACT", "");

        QueryTool qtUpdBro = new QueryTool();
        qtUpdBro = realizarUpdateBrote(dtDatBrot);
        Data dtUpdBro = new Data();
        dtUpdBro.put("10006", qtUpdBro);
        lBd.addElement(dtUpdBro);
//    }

        preparaBloqueo();
        try {
          /*            SrvPanDisEdad servlet=new SrvPanDisEdad();
               servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
               "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                     "sive_desa",
                                     "sive_desa");
                      vResultado = (Lista) servlet.doDebug(0,lBd);*/
//            vResultado = (Lista)this.getApp().getStub().doPost(0,lBd);

          this.getApp().getStub().setUrl(servletDistredad);
          vResultado = (Lista)this.getApp().getStub().doPost(10000,
              lBd,
              qtBloqueo,
              dtBloqueo,
              getApp());

          //obtener el nuevo cd_ope y fc_ultact de la base de datos.
          int tamano = vResultado.size();
          String fecha = null;
          for (int i = 0; i < vResultado.size(); i++) {
            fecha = null;
            fecha = ( (Lista) vResultado.elementAt(i)).getFC_ULTACT();
          }
          dtBrote.put("CD_OPE", dtBrote.getString("CD_OPE"));
          dtBrote.put("FC_ULTACT", fecha);
          dispose();
        }
        catch (Exception ex) {
          if (Common.ShowPregunta(this.getApp(), "Los datos han sido modificados por otro usuario. �Sigue queriendo realizar esta actualizaci�n?")) {
            try {
              ModosOperacNormal();
              this.getApp().getStub().setUrl(servletDistredad);
              vResultado = (Lista)this.getApp().getStub().doPost(0, lBd);
              dispose();
            }
            catch (Exception exc) {
              this.getApp().trazaLog(exc);
              this.getApp().showError(ex.getMessage());
              dispose();
            } //end 2.catch
          }
          else {
            dispose();
          } //end if comun.showpregunta
        } //end 1. catch
      }
      else {
        dispose();
      } //end if se han realizado cambios
      /*      try {
            this.getApp().getStub().setUrl("servlet/SrvMntDis");
            /************** ALTAS ***************/
       /*      lstMant = this.clmMantDis.getLista();
             Data dtTemp = null;
             for (int i=0;i<lstMant.size();i++) {
                dtTemp = (Data)lstMant.elementAt(i);
                if (dtTemp.getString("TIPO_OPERACION").equals("A"))  {
                  lstOperar.addElement(dtTemp);
                  dtTemp.remove("TIPO_OPERACION");
                }
             }
             lstOperar.setParameter("CD_ANO",dtBrote.getString("CD_ANO"));
            lstOperar.setParameter("NM_ALERBRO",dtBrote.getString("NM_ALERBRO"));
             // Altas
             if (lstOperar.size()>0)
            vResultado = (Lista) this.getApp().getStub().doPost(1, lstOperar);
             /************** MODIFICAR ***************/
        /*      this.getApp().getStub().setUrl("servlet/SrvTransaccion");
              preparaBloqueo();
              lstOperar = new Lista ();
              for (int i=0;i<lstMant.size();i++) {
                 dtTemp = (Data)lstMant.elementAt(i);
                 if (dtTemp.getString("TIPO_OPERACION").equals("M")) {
                    QueryTool qtUpd=new QueryTool();
                    qtUpd=realizarUpdate(dtTemp);
                    Data dtUpdate = new Data();
                    dtUpdate.put("10006",qtUpd);
                    lstOperar.addElement(dtUpdate);
                    dtTemp.remove("TIPO_OPERACION");
                 }
              }
              if (lstOperar.size()>0)
              this.getApp().getStub().doPost(10006,
                                             lstOperar,
                                             qtBloqueo,
                                             dtBloqueo,
                                             getApp());
              //this.getApp().getStub().doPost(4,lstOperar);
              // Metemos los datos que obtiene el servlet en el devuelto
              dtTemp = new Data ();
              if ((vResultado!=null)&&(vResultado.size()>0)) {
                dtTemp = (Data)vResultado.elementAt(0);
                dtBrote.put("CD_OPE",dtTemp.getString("COD_USUARIO"));
                dtBrote.put("FC_ULTACT",dtTemp.getString("FC_ULTACT"));
              }
              /************** BAJAS ***************/
         /*      preparaBloqueo();
               lstOperar = new Lista ();
               if (lismanBaj.size()>0){
                 for (int i=0;i<(lismanBaj.size());i++) {
                     dtTemp = (Data)lismanBaj.elementAt(i);
                     QueryTool qtBaj=new QueryTool();
                     qtBaj=realizarBaja(dtTemp);
                     Data dtUpBaj = new Data();
                     dtUpBaj.put("10007",qtBaj);
                     lstOperar.addElement(dtUpBaj);
                 }
               this.getApp().getStub().doPost(10007,
                                              lstOperar,
                                              qtBloqueo,
                                              dtBloqueo,
                                              getApp());
               //this.getApp().getStub().doPost(5,lstOperar);
               }
               if (modoActualizacion == BAJA){
                 for (int i=0;i<lstMant.size()-1;i++) {
                   dtTemp = (Data)lstMant.elementAt(i);
                   QueryTool qtBaj=new QueryTool();
                   qtBaj=realizarBaja(dtTemp);
                   Data dtUpBaj = new Data();
                   dtUpBaj.put("10007",qtBaj);
                   lstOperar.addElement(dtUpBaj);
                 }
                   this.getApp().getStub().doPost(10007,
                                              lstOperar,
                                              qtBloqueo,
                                              dtBloqueo,
                                              getApp());
                   //this.getApp().getStub().doPost(5,lstOperar);
               }
               // Metemos los datos que obtiene el servlet en el devuelto
               dtTemp = new Data ();
               if ((vResultado!=null)&&(vResultado.size()>0)) {
                 dtTemp = (Data)vResultado.elementAt(0);
                 dtBrote.put("CD_OPE",dtTemp.getString("COD_USUARIO"));
                 dtBrote.put("FC_ULTACT",dtTemp.getString("FC_ULTACT"));
               }
               } catch (Exception exc) {
                 this.getApp().trazaLog(exc);
                 this.getApp().showError(exc.getMessage());
               } // del "trai cach"
               dispose ();*/
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
    Inicializar(CInicializar.NORMAL);
  }

  // prepara la informaci�n para el control de bloqueo
  private void preparaBloqueo() {
    // query tool
    qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
    qtBloqueo.putOperator("NM_ALERBRO", "=");

    // informaci�n de bloqueo
    dtBloqueo = new Data();
    dtBloqueo.put("CD_OPE", dtBrote.getString("CD_OPE"));
    dtBloqueo.put("FC_ULTACT", dtBrote.getString("FC_ULTACT"));
  }

  QueryTool realizarUpdate(Data dtUpd) {

    QueryTool qtUpd = new QueryTool();

    // tabla de familias de tasas vacunables
    qtUpd.putName("SIVE_BROTES_DISTGEDAD");

    // campos que se escriben
    qtUpd.putType("NM_V_EXP", QueryTool.INTEGER);
    qtUpd.putType("NM_M_EXP", QueryTool.INTEGER);
    qtUpd.putType("NM_NC_EXP", QueryTool.INTEGER);
    qtUpd.putType("NM_V_ENF", QueryTool.INTEGER);
    qtUpd.putType("NM_M_ENF", QueryTool.INTEGER);
    qtUpd.putType("NM_NC_ENF", QueryTool.INTEGER);
    qtUpd.putType("NM_V_HOS", QueryTool.INTEGER);
    qtUpd.putType("NM_M_HOS", QueryTool.INTEGER);
    qtUpd.putType("NM_NC_HOS", QueryTool.INTEGER);
    qtUpd.putType("NM_V_DEF", QueryTool.INTEGER);
    qtUpd.putType("NM_M_DEF", QueryTool.INTEGER);
    qtUpd.putType("NM_NC_DEF", QueryTool.INTEGER);

    // Valores de los campos
    qtUpd.putValue("NM_V_EXP", dtUpd.getString("NM_V_EXP"));
    qtUpd.putValue("NM_M_EXP", dtUpd.getString("NM_M_EXP"));
    qtUpd.putValue("NM_NC_EXP", dtUpd.getString("NM_NC_EXP"));
    qtUpd.putValue("NM_V_ENF", dtUpd.getString("NM_V_ENF"));
    qtUpd.putValue("NM_M_ENF", dtUpd.getString("NM_M_ENF"));
    qtUpd.putValue("NM_NC_ENF", dtUpd.getString("NM_NC_ENF"));
    qtUpd.putValue("NM_V_HOS", dtUpd.getString("NM_V_HOS"));
    qtUpd.putValue("NM_M_HOS", dtUpd.getString("NM_M_HOS"));
    qtUpd.putValue("NM_NC_HOS", dtUpd.getString("NM_NC_HOS"));
    qtUpd.putValue("NM_V_DEF", dtUpd.getString("NM_V_DEF"));
    qtUpd.putValue("NM_M_DEF", dtUpd.getString("NM_M_DEF"));
    qtUpd.putValue("NM_NC_DEF", dtUpd.getString("NM_NC_DEF"));

    //
    qtUpd.putWhereType("NM_DISTGEDAD", QueryTool.INTEGER);
    qtUpd.putWhereValue("NM_DISTGEDAD", dtUpd.getString("NM_DISTGEDAD"));
    qtUpd.putOperator("NM_DISTGEDAD", "=");
    qtUpd.putWhereType("CD_ANO", QueryTool.STRING);
    qtUpd.putWhereValue("CD_ANO", dtUpd.getString("CD_ANO"));
    qtUpd.putOperator("CD_ANO", "=");
    qtUpd.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtUpd.putWhereValue("NM_ALERBRO", dtUpd.getString("NM_ALERBRO"));
    qtUpd.putOperator("NM_ALERBRO", "=");

    return qtUpd;
  }

  QueryTool realizarInsert(Data dtUpd) {
    QueryTool qtIns = new QueryTool();
    qtIns.putName("SIVE_BROTES_DISTGEDAD");

    qtIns.putType("CD_ANO", QueryTool.INTEGER);
    qtIns.putValue("CD_ANO", dtUpd.getString("CD_ANO"));

    qtIns.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtIns.putValue("NM_ALERBRO", dtUpd.getString("NM_ALERBRO"));

    qtIns.putType("IT_EDAD", QueryTool.STRING);
    qtIns.putValue("IT_EDAD", dtUpd.getString("IT_EDAD"));

    qtIns.putType("CD_GEDAD", QueryTool.STRING);
    qtIns.putValue("CD_GEDAD", dtUpd.getString("CD_GEDAD"));

    qtIns.putType("DIST_EDAD_I", QueryTool.INTEGER);
    qtIns.putValue("DIST_EDAD_I", dtUpd.getString("DIST_EDAD_I"));

    qtIns.putType("DIST_EDAD_F", QueryTool.INTEGER);
    qtIns.putValue("DIST_EDAD_F", dtUpd.getString("DIST_EDAD_F"));

    qtIns.putType("NM_V_EXP", QueryTool.INTEGER);
    qtIns.putValue("NM_V_EXP", dtUpd.getString("NM_V_EXP"));

    qtIns.putType("NM_M_EXP", QueryTool.INTEGER);
    qtIns.putValue("NM_M_EXP", dtUpd.getString("NM_M_EXP"));

    qtIns.putType("NM_NC_EXP", QueryTool.INTEGER);
    qtIns.putValue("NM_NC_EXP", dtUpd.getString("NM_NC_EXP"));

    qtIns.putType("NM_V_ENF", QueryTool.INTEGER);
    qtIns.putValue("NM_V_ENF", dtUpd.getString("NM_V_ENF"));

    qtIns.putType("NM_M_ENF", QueryTool.INTEGER);
    qtIns.putValue("NM_M_ENF", dtUpd.getString("NM_M_ENF"));

    qtIns.putType("NM_NC_ENF", QueryTool.INTEGER);
    qtIns.putValue("NM_NC_ENF", dtUpd.getString("NM_NC_ENF"));

    qtIns.putType("NM_V_HOS", QueryTool.INTEGER);
    qtIns.putValue("NM_V_HOS", dtUpd.getString("NM_V_HOS"));

    qtIns.putType("NM_M_HOS", QueryTool.INTEGER);
    qtIns.putValue("NM_M_HOS", dtUpd.getString("NM_M_HOS"));

    qtIns.putType("NM_NC_HOS", QueryTool.INTEGER);
    qtIns.putValue("NM_NC_HOS", dtUpd.getString("NM_NC_HOS"));

    qtIns.putType("NM_V_DEF", QueryTool.INTEGER);
    qtIns.putValue("NM_V_DEF", dtUpd.getString("NM_V_DEF"));

    qtIns.putType("NM_M_DEF", QueryTool.INTEGER);
    qtIns.putValue("NM_M_DEF", dtUpd.getString("NM_M_DEF"));

    qtIns.putType("NM_NC_DEF", QueryTool.INTEGER);
    qtIns.putValue("NM_NC_DEF", dtUpd.getString("NM_NC_DEF"));

    qtIns.putType("NM_DISTGEDAD", QueryTool.INTEGER);
    qtIns.putValue("NM_DISTGEDAD", "");

    return qtIns;
  } //END REALIZAR INSERT

  QueryTool realizarBaja(Data dtBaj) {

    QueryTool qtBaj = new QueryTool();

    // tabla de familias de productos
    qtBaj.putName("SIVE_BROTES_DISTGEDAD");
    //
    qtBaj.putWhereType("NM_DISTGEDAD", QueryTool.STRING);
    qtBaj.putWhereValue("NM_DISTGEDAD", dtBaj.getString("NM_DISTGEDAD"));
    qtBaj.putOperator("NM_DISTGEDAD", "=");
    qtBaj.putWhereType("CD_ANO", QueryTool.STRING);
    qtBaj.putWhereValue("CD_ANO", dtBaj.getString("CD_ANO"));
    qtBaj.putOperator("CD_ANO", "=");
    qtBaj.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBaj.putWhereValue("NM_ALERBRO", dtBaj.getString("NM_ALERBRO"));
    qtBaj.putOperator("NM_ALERBRO", "=");

    return qtBaj;
  }

  QueryTool realizarUpdateBrote(Data dtUpd) {
    QueryTool qtUpd = new QueryTool();
    qtUpd.putName("SIVE_BROTES");
    qtUpd.putType("CD_OPE", QueryTool.STRING);
    qtUpd.putValue("CD_OPE", dtUpd.getString("CD_OPE"));
    qtUpd.putType("FC_ULTACT", QueryTool.TIMESTAMP);
    qtUpd.putValue("FC_ULTACT", dtUpd.getString("FC_ULTACT"));

    // filtro
    qtUpd.putWhereType("CD_ANO", QueryTool.STRING);
    qtUpd.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
    qtUpd.putOperator("CD_ANO", "=");

    qtUpd.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtUpd.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
    qtUpd.putOperator("NM_ALERBRO", "=");

    return qtUpd;
  }

  //sin el bloqueo
  public void ModosOperacNormal() {
    Lista lstMant = this.clmMantDis.getLista();

    //recorro la lista q tengo para altas y modificaciones
    for (int i = 0; i < lstMant.size(); i++) {
      Data dtMant = (Data) lstMant.elementAt(i);

      //Se realiza un alta
      if (dtMant.getString("TIPO_OPERACION").equals("A")) {
        QueryTool qtIns = new QueryTool();
        qtIns = realizarInsert(dtMant);
        Data dtInsert = new Data();
        dtInsert.put("10003", qtIns);
        lBd.addElement(dtInsert);
      } //end if ALTA

      if (dtMant.getString("TIPO_OPERACION").equals("M")) {
        QueryTool qtUpd = new QueryTool();
        qtUpd = realizarUpdate(dtMant);
        Data dtUpdate = new Data();
        dtUpdate.put("10006", qtUpd);
        lBd.addElement(dtUpdate);
      }
    } //end for
    //lista para bajas
    for (int i = 0; i < lismanBaj.size(); i++) {
      Data dtMantBaja = (Data) lismanBaj.elementAt(i);
      QueryTool qtDel = new QueryTool();
      qtDel = realizarBaja(dtMantBaja);
      Data dtDel = new Data();
      dtDel.put("10007", qtDel);
      lBd.addElement(dtDel);
    } //end for lBajas
  } //end   ModosOperacNormal

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

}

class DialogActionAdapter1
    implements java.awt.event.ActionListener, Runnable {
  PanDisEdad adaptee;
  ActionEvent e;

  DialogActionAdapter1(PanDisEdad adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
