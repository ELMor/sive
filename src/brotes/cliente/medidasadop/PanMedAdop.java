/**
 * Clase: PanMed
 * Paquete: brotes.cliente.panmed
 * Hereda: CPanel
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 20/12/1999
 * Analisis Funcional: Punto 2.2 Mantenimiento Informe Final del Brote.
 * Descripcion: Implementacion del panel que ense�a una tabla con las
 *   adoptadas par un brote. Desde este panel
 *   se puede acceder al alta/modificacion/baja de medidas.
 */
/**
 * Panel a trav�s del que se realizan los mantenimientos
 * de medidas adoptadas de forma masiva.
 * @autor PDP 09/05/2000
 * @version 1.1
 */

package brotes.cliente.medidasadop;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;
import sapp2.StubSrvBD;

//import comun.CFechaSimple; import comun.CHora; import comun.CListaNiveles1; import comun.Comunicador; import comun.constantes; import comun.Data; import comun.DataAutoriza; import comun.DataCasIn; import comun.DataEnferedo; import comun.DataEnfermo; import comun.DataEntradaEDO; import comun.DataEqNot; import comun.DataGeneralCDDS; import comun.DataIndivEnfermedad; import comun.DataLongValid; import comun.DataMun; import comun.DataMunicipioEDO; import comun.DataNivel1; import comun.DataNotifEDO; import comun.DataNotifSem; import comun.DataRegistroEDO; import comun.DataValoresEDO; import comun.DataZBS; import comun.DataZBS2; import comun.DatoBase; import comun.datosParte; import comun.datosPreg; import comun.DialogoGeneral; import comun.DialogRegistroEDO; import comun.DialogSelDomi; import comun.DialogSelEnfermo; import comun.Fechas; import comun.IntContenedor; import comun.PanAnoSemFech; import comun.SrvDebugGeneral; import comun.SrvDlgBuscarRegistro; import comun.SrvEnfermedad; import comun.SrvEntradaEDO; import comun.SrvMaestroEDO; import comun.SrvMun; import comun.SrvMunicipioCont; import comun.SrvNivel1; import comun.SrvZBS2; import comun.UtilEDO; import comun.comun;

public class PanMedAdop
    extends CDialog
    implements CInicializar, CFiltro {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;
  public int modoActualizacion = ALTA;

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  // gesti�n de la linea de totales
  public boolean bTotales = false;

  //Retorno de alta
  public boolean bFirst = false;

  // estructuras para comprobar el bloqueo
  private QueryTool qtBloqueo = null;
  private Data dtBloqueo = null;

  public int indTotal = 0;

  public int indTotalIni = 0;

  private Lista lismanBaj = new Lista();

  CListaMantenimiento clmMantMed = null;

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  DialogActionAdapter1 ActionAdapter = new DialogActionAdapter1(this);

  Label lblBroteDesc = new Label();
  Label lblDatosBrote = new Label();

  final String srvTrans = "servlet/SrvTransaccion";
  private boolean OK = false;

  Data dtBrote = null;
  XYLayout xYLayout1 = new XYLayout();

  public PanMedAdop(CApp a, int modo, Data dt) {
    super(a);

    bFirst = true;

    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    QueryTool2 qt = new QueryTool2();
    QueryTool qtP = new QueryTool();

    try {

      dtBrote = dt;

      modoActualizacion = modo;
      this.app = a;

      //botones
      vBotones.addElement(new CBoton("",
                                     "images/alta2.gif",
                                     "Nueva Medida",
                                     false,
                                     false));

      vBotones.addElement(new CBoton("",
                                     "images/modificacion2.gif",
                                     "Modificar Medida",
                                     true,
                                     true));

      vBotones.addElement(new CBoton("",
                                     "images/baja2.gif",
                                     "Borrar Medida",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("Descripci�n Medida",
                                      "365",
                                      "DS_MEDIDA"));

      vLabels.addElement(new CColumna("Informaci�n Adicional",
                                      "310",
                                      "DS_INFADI"));

      clmMantMed = new CListaMantenimiento(a,
                                           vLabels,
                                           vBotones,
                                           this,
                                           this);
      jbInit();
      Lista lismanIni = this.clmMantMed.getLista();
      indTotalIni = lismanIni.size();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setSize(760, 420);
    this.setTitle("Brotes: Medidas Adoptadas");

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    lblBroteDesc.setText("Brote:");
    lblDatosBrote.setText(dtBrote.getString("CD_ANO") + "/" +
                          dtBrote.getString("NM_ALERBRO") + " - " +
                          dtBrote.getString("DS_BROTE"));
    xYLayout1.setHeight(420);
    xYLayout1.setWidth(760);

    this.setLayout(xYLayout1);
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(ActionAdapter);
    btnCancelar.addActionListener(ActionAdapter);
    //chResultado.addItemListener(chkItemAdapter);

    btnAceptar.setEnabled(true);
    btnCancelar.setEnabled(true);

    this.add(lblBroteDesc, new XYConstraints(39, 9, 42, -1));
    this.add(lblDatosBrote, new XYConstraints(92, 9, 600, -1));
    this.add(clmMantMed, new XYConstraints(20, 37, 780, 300));
    this.add(btnAceptar, new XYConstraints(523, 341, 88, 29));
    this.add(btnCancelar, new XYConstraints(634, 341, 88, 29));

    Inicializar(CInicializar.ESPERA);
    clmMantMed.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
  }

  public void Inicializar() {}

  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if ( (modoActualizacion == BAJA) || (modoActualizacion == CONSULTA)) {
          this.setEnabled(false);
          btnAceptar.setVisible(false);
          btnCancelar.setLabel("Salir");
          btnCancelar.setEnabled(true);
          clmMantMed.setEnabled(false);
        }
        else {
          clmMantMed.setEnabled(true);
          btnAceptar.setEnabled(true);
          btnCancelar.setEnabled(true);
        }
    }
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvQueryTool";

    try {
      Data fila = null;
      fila = new Data();
      QueryTool2 qt = new QueryTool2();
      qt.putName("SIVE_BROTES_MED");
      qt.putType("CD_ANO", QueryTool.STRING);
      qt.putType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putType("CD_GRUPO", QueryTool.STRING);
      qt.putType("CD_MEDIDA", QueryTool.STRING);
      qt.putType("NM_SECMB", QueryTool.STRING);
      qt.putType("DS_INFADI", QueryTool.STRING);

      /*
                  qt.putWhereType("CD_MEDIDA", QueryTool.STRING);
                  qt.putWhereValue("CD_MEDIDA",dtBrote.getString("CD_MEDIDA"));
                  qt.putOperator("CD_MEDIDA","=");
       */
      /*
                  qt.putWhereType("CD_GRUPO", QueryTool.STRING);
                  qt.putWhereValue("CD_GRUPO",dtBrote.getString("CD_GRUPO"));
                  qt.putOperator("CD_GRUPO","=");
       */

      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");

      qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");

      //otros datos
      QueryTool qtP = new QueryTool();
      qtP.putName("SIVE_MEDIDAS");
      qtP.putType("DS_MEDIDA", QueryTool.STRING);
      qtP.putType("IT_INFADIC", QueryTool.STRING);
      qtP.putType("IT_REPE", QueryTool.STRING);

      Data dtA1 = new Data();
      dtA1.put("CD_GRUPO", QueryTool.STRING);
      dtA1.put("CD_MEDIDA", QueryTool.STRING);
      qt.addQueryTool(qtP);
      qt.addColumnsQueryTool(dtA1);

      qt.addOrderField("NM_SECMB");

      p.addElement(qt);

      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      //p.setTrama("CD_SINTOMA||'-'||DS_SINTOMA", "");
      p1 = (Lista)this.getApp().getStub().doPost(1, p);
      if (p1.size() == 0) {
//           this.getApp().showAdvise("No existen datos");
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
      this.getApp().trazaLog(ex);
    }

    return p1;
  }

  public void realizaOperacion(int j) {

    Lista lisman = this.clmMantMed.getLista();
    int ind = clmMantMed.getSelectedIndex();
    indTotal = lisman.size();
    DiaMed dlg = null;
    Data dMantMed = new Data();

    Data dtResultado = new Data();
    Lista ltmpsal = new Lista();

    switch (j) {
      // bot�n de alta:data vacio
      case ALTA:

        dlg = new DiaMed(this.getApp(), 0, dtBrote, null, lisman);
        dlg.show();
        // a�adir el nuevo elem a la lista
        if (dlg.bAceptar()) {
          dtResultado = dlg.devuelveData();
          lisman.addElement(dtResultado);
          clmMantMed.setPrimeraPagina(lisman);
        }

        break;

        // bot�n de modificaci�n
      case MODIFICACION:

        dMantMed = clmMantMed.getSelected();

        //si existe alguna fila seleccionada
        if ( (dMantMed != null)) {
          dlg = new DiaMed(this.getApp(), 1, dtBrote, dMantMed, lisman);
          dlg.show();
          //Tratamiento de bAceptar para Salir
          if (dlg.bAceptar()) {
            lisman.removeElementAt(ind);
            dtResultado = dlg.devuelveData();
            lisman.addElement(dtResultado);
            clmMantMed.setPrimeraPagina(lisman);
          }
        }
        else {
          if (dMantMed == null) {
            this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
          }
          else {
            this.getApp().showAdvise(
                "Debe seleccionar un registro en la tabla diferente a la linea de totales");
          }
        }

        break;

      case BAJA:

        dMantMed = clmMantMed.getSelected();
        if ( (dMantMed != null)) {
          dlg = new DiaMed(this.getApp(), 2, dtBrote, dMantMed, lisman);
          dlg.show();
          if (dlg.bAceptar()) {
            lismanBaj.addElement(clmMantMed.getSelected());
            lisman.removeElementAt(ind);
            clmMantMed.setPrimeraPagina(lisman);

          }
        }
        else {
          if (dMantMed == null) {
            this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
          }
          else {
            this.getApp().showAdvise(
                "Debe seleccionar un registro en la tabla diferente a la linea de totales");
          }
        }

        break;
    }
  }

  public Lista siguientePagina() {
    Lista v = null;
    return v;
  }

  void btn_actionPerformed(ActionEvent e) {
    Inicializar(CInicializar.ESPERA);
    Lista lstMant = new Lista();
    Lista lstOperar = new Lista();

    Lista vResultado = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {
      try {
        this.getApp().getStub().setUrl("servlet/SrvMedidas");

        // Query para actualizar SIVE_BROTES (CD_OPE/FC_ULTACT)
        QueryTool qtBro = new QueryTool();
        Data dtBro = new Data();
        qtBro.putName("SIVE_BROTES");
        qtBro.putType("CD_OPE", QueryTool.STRING);
        qtBro.putType("FC_ULTACT", QueryTool.TIMESTAMP);
        qtBro.putValue("CD_OPE", dtBrote.getString("CD_OPE"));
        qtBro.putValue("FC_ULTACT", "");

        qtBro.putWhereType("CD_ANO", QueryTool.STRING);
        qtBro.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
        qtBro.putOperator("CD_ANO", "=");
        qtBro.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
        qtBro.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
        qtBro.putOperator("NM_ALERBRO", "=");

        dtBro.put("4", qtBro);
        lstOperar.addElement(dtBro);

        /************** ALTAS ***************/
        lstMant = this.clmMantMed.getLista();

        Data dtTemp = null;

        for (int i = 0; i < lstMant.size(); i++) {
          dtTemp = (Data) lstMant.elementAt(i);
          if (dtTemp.getString("TIPO_OPERACION").equals("A")) {
            QueryTool qtAlt = new QueryTool();
            qtAlt = realizarAlta(dtTemp);

            Data dtAlt = new Data();
            dtAlt.put("3", qtAlt);

            lstOperar.addElement(dtAlt);

            dtTemp.remove("TIPO_OPERACION");
          }
        }

        /************** MODIFICAR ***************/

        for (int i = 0; i < lstMant.size(); i++) {
          dtTemp = (Data) lstMant.elementAt(i);
          if (dtTemp.getString("TIPO_OPERACION").equals("M")) {
            QueryTool qtUpd = new QueryTool();
            qtUpd = realizarUpdate(dtTemp);

            Data dtUpdate = new Data();
            dtUpdate.put("4", qtUpd);

            lstOperar.addElement(dtUpdate);

            dtTemp.remove("TIPO_OPERACION");
          }
        }

        /************** BAJAS ***************/
        if (lismanBaj.size() > 0) {
          for (int i = 0; i < lismanBaj.size(); i++) {
            dtTemp = (Data) lismanBaj.elementAt(i);
            QueryTool qtBaja = new QueryTool();
            qtBaja = realizarBaja(dtTemp);

            Data dtBaja = new Data();
            dtBaja.put("5", qtBaja);

            lstOperar.addElement(dtBaja);
          }

        }

        if (modoActualizacion == BAJA) {
          for (int i = 0; i < lstMant.size() - 1; i++) {
            dtTemp = (Data) lstMant.elementAt(i);

            QueryTool qtBaj = new QueryTool();
            qtBaj = realizarBaja(dtTemp);

            Data dtUpBaj = new Data();
            dtUpBaj.put("5", qtBaj);

            lstOperar.addElement(dtUpBaj);
          }
        }

        //realizamos la actualizaci�n
        preparaBloqueo();
        if (lstOperar.size() > 0) {
          vResultado = (Lista)this.getApp().getStub().doPost(10000,
              lstOperar,
              qtBloqueo,
              dtBloqueo,
              getApp());

          //obtener el nuevo cd_ope y fc_ultact de la base de datos.
        }
        int tamano = vResultado.size();
        String fecha = "";
        //for (int i=0; i<vResultado.size();i++){
        //fecha=null;
        //fecha=((Lista)vResultado.elementAt(i)).getFC_ULTACT();
        //}
        fecha = ( (Lista) vResultado.firstElement()).getFC_ULTACT();

        dtBrote.put("CD_OPE", dtBrote.getString("CD_OPE"));
        dtBrote.put("FC_ULTACT", fecha);

        dispose();

      }
      catch (Exception exc) {
        /*
                 if(ex.getMessage().indexOf(constantes.PRE_BLOQUEO) != -1){
          if(comun.ShowPregunta(this.app,constantes.DATOS_BLOQUEADOS)) {
            try {
              this.getApp().getStub().doPost(10007,
                                          lstOperar,
                                          qtBloqueo,
                                          dtBloqueo,
                                          getApp());
            }catch(Exception exc) {
              this.getApp().trazaLog(exc);
              this.getApp().showError(exc.getMessage());
              //comun.ShowWarning(this.app, "Error al realizar la modificaci�n del caso");
            }
          }
                 }
         else {//error normal
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
              //comun.ShowWarning(this.app, "Error al realizar la modificaci�n del caso");
                 }
         */

        //this.getApp().trazaLog(exc);
        //this.getApp().showError(exc.getMessage());
        dispose();

      } // del "trai cach"
      dispose();
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
    Inicializar(CInicializar.NORMAL);
  }

  // prepara la informaci�n para el control de bloqueo
  private void preparaBloqueo() {
    // query tool
    qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
    qtBloqueo.putOperator("NM_ALERBRO", "=");

    // informaci�n de bloqueo
    dtBloqueo = new Data();
    dtBloqueo.put("CD_OPE", dtBrote.getString("CD_OPE"));
    dtBloqueo.put("FC_ULTACT", dtBrote.getString("FC_ULTACT"));
  }

  QueryTool realizarAlta(Data dtAlta) {

    QueryTool qtAlt = new QueryTool();

    // tabla de familias de tasas vacunables
    qtAlt.putName("SIVE_BROTES_MED");

    // campos que se escriben
    qtAlt.putType("NM_SECMB", QueryTool.INTEGER);
    qtAlt.putType("CD_GRUPO", QueryTool.INTEGER);
    qtAlt.putType("CD_MEDIDA", QueryTool.STRING);
    qtAlt.putType("CD_ANO", QueryTool.STRING);
    qtAlt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtAlt.putType("DS_INFADI", QueryTool.STRING);

    // Valores de los campos
    qtAlt.putValue("NM_SECMB", "");
    qtAlt.putValue("CD_GRUPO", dtAlta.getString("CD_GRUPO"));
    qtAlt.putValue("CD_MEDIDA", dtAlta.getString("CD_MEDIDA"));
    qtAlt.putValue("CD_ANO", dtAlta.getString("CD_ANO"));
    qtAlt.putValue("NM_ALERBRO", dtAlta.getString("NM_ALERBRO"));
    qtAlt.putValue("DS_INFADI", dtAlta.getString("DS_INFADI"));

    return qtAlt;
  }

  QueryTool realizarUpdate(Data dtUpd) {

    QueryTool qtUpd = new QueryTool();

    // tabla de familias de tasas vacunables
    qtUpd.putName("SIVE_BROTES_MED");

    // campos que se escriben
    qtUpd.putType("DS_INFADI", QueryTool.STRING);

    // Valores de los campos
    qtUpd.putValue("DS_INFADI", dtUpd.getString("DS_INFADI"));

    //
    qtUpd.putWhereType("NM_SECMB", QueryTool.STRING);
    qtUpd.putWhereValue("NM_SECMB", dtUpd.getString("NM_SECMB"));
    qtUpd.putOperator("NM_SECMB", "=");

    /*
            qtUpd.putWhereType("CD_ANO", QueryTool.STRING);
            qtUpd.putWhereValue("CD_ANO",dtUpd.getString("CD_ANO"));
            qtUpd.putOperator("CD_ANO","=");
            qtUpd.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
            qtUpd.putWhereValue("NM_ALERBRO",dtUpd.getString("NM_ALERBRO"));
            qtUpd.putOperator("NM_ALERBRO","=");
     */

    return qtUpd;
  }

  QueryTool realizarBaja(Data dtBaj) {

    QueryTool qtBaj = new QueryTool();

    // tabla de familias de productos
    qtBaj.putName("SIVE_BROTES_MED");
    //
    qtBaj.putWhereType("NM_SECMB", QueryTool.STRING);
    qtBaj.putWhereValue("NM_SECMB", dtBaj.getString("NM_SECMB"));
    qtBaj.putOperator("NM_SECMB", "=");

    /*
             qtBaj.putWhereType("CD_ANO", QueryTool.STRING);
             qtBaj.putWhereValue("CD_ANO",dtBaj.getString("CD_ANO"));
             qtBaj.putOperator("CD_ANO","=");
             qtBaj.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
             qtBaj.putWhereValue("NM_ALERBRO",dtBaj.getString("NM_ALERBRO"));
             qtBaj.putOperator("NM_ALERBRO","=");
     */

    return qtBaj;
  }

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }
}

class DialogActionAdapter1
    implements java.awt.event.ActionListener, Runnable {
  PanMedAdop adaptee;
  ActionEvent e;

  DialogActionAdapter1(PanMedAdop adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);

  }
}
