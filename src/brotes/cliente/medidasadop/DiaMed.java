/**
 * Clase: DiaMed
 * Paquete: brotes.cliente.medidasadop
 * Hereda: CDialog
 * Autor:  (PDP)
 * Fecha Inicio: 20/12/1999
 * Analisis Funcional: Punto 2.2 Mantenimiento Informe Final del Brote.
 * Descripcion: Implementacion del dialogo que permite dar de alta
 *   baja, modificar y consultar medidas adoptadas para un brote.
 */

package brotes.cliente.medidasadop;

import java.util.Vector;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
// <------------- cambiar ruta
import brotes.datos.diamed.DatCDDSMed;
import capp2.CApp;
import capp2.CCargadorImagen;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CListaValores;
import comun.constantes;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.StubSrvBD;

public class DiaMed
    extends CDialog {

  // Indicador de validez del codigo de la medida
  protected boolean bMedidaValid = false;

  /*
     // Mensaje en caso de bloqueo
     private final String DATOS_BLOQUEADOS = "Los datos han sido modificados. �Desea sobrescribirlos?";
   */

  // En caso de alta, indican los flags IT_INFADIC e IT_REPE de la medida obtenida
  private boolean bMasInfo = false;
  private boolean bRepe = false;

  // Vars. de gestion del boton de medidas
  sapp.StubSrvBD stubCliente = null;
  final String strSERVLET_LMED = "servlet/SrvLMed";

  // Estado: indica si se ha llevado a cabo correctamente la operacion deseada
  public boolean accionOK = false;
  //public DatMedSC medNuevo = null;

  /*
     // Localizacion del servlet
     final String strSERVLET_DIAMED = "servlet/SrvDiaMed";
   */

  // Variable estado del panel
  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;
  final public int modoCONSULTA = 3;
  final public int modoESPERA = 4;
  final public int modoNORMAL = 5;

  protected int modoOperacion = 0;
  protected int modoEntrada = 0;

  // Sincronizaci�n de eventos
  protected boolean sinBloquear = true;
  protected int modoAnterior;
  ;

  //Datos de la medida obtenidos de ls CListaValores
  private sapp2.Data dtMedidas = null;

  // parametros pasados al di�logo
  public sapp2.Data dtRegSubT = new sapp2.Data();

  public sapp2.Data dtReg = new sapp2.Data();

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  // Datos del Brote
  sapp2.Data diaBrote = null;

  // Datos de la medida a modificar
  sapp2.Data dtMedIni = null;

  // Lista de medidas adoptadas
  Lista MedAdop = null;

  // IT_REPE
  String sIT_REPE = "";

  // Medida establecida con boton o perdida de foco de Medida
  DatCDDSMed medida = null;

  // Imagenes
  protected CCargadorImagen imgs = null;
  final String imgNAME[] = {
      constantes.imgACEPTAR,
      constantes.imgCANCELAR,
      constantes.imgLUPA};

  /****************** Componentes del panel ********************/

  // Organizacion del panel
  private XYLayout xYLayout = new XYLayout();

  // Medida
  private Label lblMedida = new Label("Medida:");
  private capp.CCampoCodigo txtMedida = new capp.CCampoCodigo();
  private ButtonControl btnMedida = new ButtonControl();
  private TextField txtDescMedida = new TextField();

  // Informacion Adicional
  private Label lblMasInfo = new Label("Informaci�n Adicional: ");
  private Checkbox chkMasInfo = new Checkbox();
  private TextArea txtaMasInfo = new TextArea("", 2, 30,
                                              TextArea.SCROLLBARS_VERTICAL_ONLY);

  // Botones de aceptar y cancelar
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores
  DiaMedActionAdapter actionAdapter = null;
  DiaTextAdapter textAdapter = null;
  DiaFocusAdapter focusAdapter = null;

  // Constructor
  // CApp sera luego el panel del que procede: PanMed
  // @param brote: datos de un brote
  // @param med: lleva los datos de una medida en caso de alta/modificaci�n/baja/consulta
  public DiaMed(CApp a, int modo, sapp2.Data brote, sapp2.Data med,
                Lista Medidas) {
    super(a);
    this.setTitle("Brotes: Medidas Adoptadas");
    try {
      // Variables globales
      diaBrote = brote;
      dtMedIni = med;
      MedAdop = Medidas;

      // Inicializacion
      modoOperacion = modo;
      modoEntrada = modo;
      jbInit();

      if (modoEntrada == modoMODIFICACION ||
          modoEntrada == modoBAJA ||
          modoEntrada == modoCONSULTA) {
        rellenarDatos(dtMedIni);
        txtMedidaFocusLost();
      }

      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    // Organizacion del panel
    this.setSize(new Dimension(535, 205));
    xYLayout.setHeight(205);
    xYLayout.setWidth(535);
    this.setLayout(xYLayout);

    // Carga de las im�genes
    imgs = new CCargadorImagen(getApp(), imgNAME);
    imgs.CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaMedActionAdapter(this);
    textAdapter = new DiaTextAdapter(this);
    focusAdapter = new DiaFocusAdapter(this);

    // Medidas
    this.add(lblMedida, new XYConstraints(15, 15, 48, 25));
    this.add(txtMedida, new XYConstraints(15 + 48 + 10, 15, 45, 25));
    txtMedida.addKeyListener(textAdapter);
    txtMedida.addFocusListener(focusAdapter);
    txtMedida.setName("Medida");
    txtMedida.setBackground(new Color(255, 255, 150));
    this.add(btnMedida, new XYConstraints(15 + 48 + 10 + 45 + 10, 15, 25, 25));
    btnMedida.setImage(imgs.getImage(2));
    btnMedida.addActionListener(actionAdapter);
    btnMedida.setActionCommand("Medidas");
    this.add(txtDescMedida,
             new XYConstraints(15 + 48 + 10 + 45 + 10 + 25 + 10, 15, 350, 25));
    txtDescMedida.setEnabled(false);
    txtDescMedida.setEditable(false);

    // Informacion Adicional
    this.add(lblMasInfo, new XYConstraints(15, 50, 120, 25));
    this.add(chkMasInfo, new XYConstraints(15 + 120 + 10, 50, 25, 25));
    this.add(txtaMasInfo,
             new XYConstraints(15 + 120 + 10 + 25 + 10, 50, 333, 60));
    chkMasInfo.addItemListener(new DiaMedChkMasInfoItemAdapter(this));

    // Boton de Aceptar
    btnAceptar.setImage(imgs.getImage(0));
    btnAceptar.setLabel("Aceptar");
    this.add(btnAceptar, new XYConstraints(513 - 80 - 10 - 80, 100 + 35, 80, -1));
    if (modoOperacion == constantes.modoCONSULTA) {
      btnAceptar.setEnabled(false);
    }
    else {
      btnAceptar.setActionCommand("Aceptar");
      btnAceptar.addActionListener(actionAdapter);
    }

    // Boton de Cancelar
    btnCancelar.setImage(imgs.getImage(1));
    btnCancelar.setLabel("Cancelar");
    this.add(btnCancelar, new XYConstraints(513 - 80, 100 + 35, 80, -1));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);
  } // Fin jbInit()

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {
    switch (modoOperacion) {
      case modoESPERA:
        chkMasInfo.setEnabled(false);
        txtaMasInfo.setEnabled(false);
        enableControls(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA:
      case modoMODIFICACION:
        if (bMasInfo) {
          chkMasInfo.setEnabled(true);
          txtaMasInfo.setEnabled(true);
        }
        else {
          chkMasInfo.setEnabled(false);
          chkMasInfo.setState(false);
          txtaMasInfo.setEnabled(false);
        }

        if (chkMasInfo.getState()) {
          txtaMasInfo.setVisible(true);
        }
        else {
          txtaMasInfo.setVisible(false);

        }
        enableControls(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
      case modoCONSULTA:
        chkMasInfo.setEnabled(false);
        txtaMasInfo.setEnabled(false);
        if (chkMasInfo.getState()) {
          txtaMasInfo.setVisible(true);
        }
        else {
          txtaMasInfo.setVisible(false);

        }
        enableControls(false);

        // Botones Aceptar/Cancelar
        btnAceptar.setEnabled(true);
        if (modoEntrada == modoCONSULTA) {
          btnAceptar.setEnabled(false);
        }
        btnCancelar.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
    this.doLayout();
  } // Fin Inicializar()

  private void enableControls(boolean en) {
    // Medida
    if (modoOperacion == modoMODIFICACION) {
      txtMedida.setEnabled(false);
      btnMedida.setEnabled(false);
    }
    else {
      txtMedida.setEnabled(en);
      btnMedida.setEnabled(en);
    }

    txtDescMedida.setEnabled(false);

    // Informacion Adicional: tratado en Inicializar()
  } // Fin enableControls()

  // Devuelve los datos seleccionados en el di�logo.
  public sapp2.Data devuelveData() {
    return dtReg;
  }

  public boolean InsertDuplicado() {
    //controlar la inserci�n si it_repe es igual a "S"
    //boolean salir = false;
    boolean ExisteMedida = false;
    String sCdMed = txtMedida.getText();
    String sRepe = sIT_REPE;
    ExisteMedida = ComprobarRepeticion();

    if (sRepe.equals("N") && (ExisteMedida)) {
      this.getApp().showAdvise(
          "Esta medida no se puede repetir, seleccione otra.");
      return false;
    }
    else if (sRepe.equals("S") && (ExisteMedida)) {
      return true;
    }
    else {
      for (int i = 0; i < MedAdop.size(); i++) {
        String sEnt = ( (sapp2.Data) MedAdop.elementAt(i)).getString(
            "CD_MEDIDA");
        if (sCdMed.equals(sEnt)) {
          this.getApp().showAdvise(
              "Esta medida no se puede repetir, seleccione otra.");
          return false;
        }
      }
      return true;
    }
  }

  // Comprobaci�n de la existencia de un registro con ya repetido
  private boolean ComprobarRepeticion() {
    Lista p = new Lista();
    Lista p1 = new Lista();

    try {

      QueryTool qt = new QueryTool();

      qt.putName("SIVE_BROTES_MED");
      qt.putType("NM_SECMB", QueryTool.STRING);

      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", diaBrote.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");

      qt.putWhereType("CD_MEDIDA", QueryTool.STRING);
      qt.putWhereValue("CD_MEDIDA", txtMedida.getText());
      qt.putOperator("CD_MEDIDA", "=");

      qt.putWhereType("CD_GRUPO", QueryTool.STRING);
      qt.putWhereValue("CD_GRUPO", diaBrote.getString("CD_GRUPO"));
      qt.putOperator("CD_GRUPO", "=");

      qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putWhereValue("NM_ALERBRO", diaBrote.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");

      p.addElement(qt);

      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      p1 = (Lista)this.getApp().getStub().doPost(1, p);

    }
    catch (Exception ex) {
      ex.printStackTrace();
      this.getApp().trazaLog(ex);
    }

    if (p1.size() == 0) {
      return false;
    }
    else {
      //this.getApp().showAdvise("Ya existe esta medida no se puede repetir para el brote, seleccione otra.");
      return true;
    }

  }

  public boolean validarDatos() {
    // 1. PERDIDAS DE FOCO: Campos CD-Button-DS
    if (!bMedidaValid) {
      this.getApp().showAdvise("Medida no v�lida");
      txtMedida.setText("");
      txtMedida.requestFocus();
      return false;
    }

    // 2. LONGITUDES

    // txtMedida
    if (txtMedida.getText().length() > 2) {
      this.getApp().showAdvise(
          "La longitud del c�digo de la medida excede de 2 caracteres");
      txtMedida.setText("");
      txtMedida.requestFocus();
      return false;
    }

    // txtaMasInfo
    if (txtaMasInfo.getText().length() > 40) {
      this.getApp().showAdvise(
          "La longitud del texto de Informaci�n Adicional excede de 40 caracteres");
      txtaMasInfo.setText("");
      txtaMasInfo.requestFocus();
      return false;
    }

    // 3. DATOS NUMERICOS: No hay

    // 4. CAMPOS OBLIGATORIOS:

    // txtMedida
    if (txtMedida.getText().length() <= 0) {
      this.getApp().showAdvise("El c�digo de la medida es un campo obligatorio");
      txtMedida.requestFocus();
      return false;
    }

    // Si llega aqui todo ha ido bien
    return true;
  } // Fin validarDatos()

  public void rellenarDatos(sapp2.Data datMed) {
    // Medida
    txtMedida.setText(datMed.getString("CD_MEDIDA"));
    txtDescMedida.setText(datMed.getString("DS_MEDIDA"));

    // Informacion Adicional
    if (datMed.getString("IT_INFADIC").equals("S")) {
      chkMasInfo.setState(true);
      txtaMasInfo.setText(datMed.getString("DS_INFADI"));
    }
    else {
      chkMasInfo.setEnabled(false);

    }
  } // Fin rellenarDatos()

  /*
     // Compara si los valores de 2 DatCasTratSC son iguales (true)
     private boolean compararCampos(DatMedSC a,DatMedSC b){
    if(a == null || b == null)
      return false;
    if(!a.getCD_GRUPO().equals(b.getCD_GRUPO()))
      return false;
    if(!a.getCD_MEDIDA().equals(b.getCD_MEDIDA()))
      return false;
    if(!a.getDS_MEDIDA().equals(b.getDS_MEDIDA()))
      return false;
    if(!a.getIT_INFADIC().equals(b.getIT_INFADIC()))
      return false;
    if(!a.getIT_REPE().equals(b.getIT_REPE()))
      return false;
    if(!a.getDS_INFADI().equals(b.getDS_INFADI()))
      return false;
    return true;
     } // Fin  compararCampos()
   */

  /*
     // Realiza la comunicaci�n con el servlet strSERVLET_DIAMED
     CLista hazAccion(StubSrvBD stubCliente, int modo, CLista parametros) throws Exception {
    CLista result = null;
    // Invocacion del servlet para que haga el insert
    result = Comunicador.Communicate(this.getCApp(),
                                  stubCliente,
                                  modo,
                                  strSERVLET_DIAMED,
                                  parametros);
    // SOLO DESARROLLO
    //SrvDiaMed srv = new SrvDiaMed();
    // SOLO DESARROLLO
    //result = srv.doDebug(modo,parametros);
    return result;
     } // Fin hazAccion()
   */

  // Bloquear eventos
  /** Este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized boolean bloquea() {
    if (sinBloquear) {
      // No hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modoOperacion;
      modoOperacion = constantes.modoESPERA;
      Inicializar();

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // Ya est� bloqueado
      return false;
    }
  } // Fin bloquea()

  /** Este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    modoOperacion = modoAnterior;
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  } // Fin desbloquea()

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

  /******************** Manejadores *************************/
  void btnAceptarActionPerformed() {

    if (validarDatos()) {
      switch (modoEntrada) {
        case modoALTA:

          Inicializar(modoESPERA);
          if (InsertDuplicado()) {
            dtReg = new sapp2.Data();
            dtReg.put("CD_ANO", diaBrote.getString("CD_ANO"));
            dtReg.put("NM_ALERBRO", diaBrote.getString("NM_ALERBRO"));
            dtReg.put("CD_GRUPO", diaBrote.getString("CD_GRUPO"));
            dtReg.put("CD_MEDIDA", txtMedida.getText());
            dtReg.put("DS_MEDIDA", txtDescMedida.getText());

            if (chkMasInfo.getState() == true) {
              dtReg.put("IT_INFADIC", "S");
            }
            else {
              dtReg.put("IT_INFADIC", "N");
            }

            dtReg.put("DS_INFADI", txtaMasInfo.getText());

            dtReg.put("TIPO_OPERACION", "A");

            bAceptar = true;
            dispose();
          }

          Inicializar(modoNORMAL);

          break;

        case modoMODIFICACION:

          Inicializar(modoESPERA);
          dtReg = new sapp2.Data();
          dtReg.put("CD_ANO", diaBrote.getString("CD_ANO"));
          dtReg.put("NM_ALERBRO", diaBrote.getString("NM_ALERBRO"));
          dtReg.put("CD_GRUPO", diaBrote.getString("CD_GRUPO"));
          dtReg.put("CD_MEDIDA", txtMedida.getText());
          dtReg.put("DS_MEDIDA", txtDescMedida.getText());

          if (chkMasInfo.getState() == true) {
            dtReg.put("IT_INFADIC", "S");
          }
          else {
            dtReg.put("IT_INFADIC", "N");
          }

          dtReg.put("DS_INFADI", txtaMasInfo.getText());

          String OpeAnt = dtMedIni.getString("TIPO_OPERACION");
          if (OpeAnt.equals("A")) {
            dtReg.put("TIPO_OPERACION", "A");
          }
          else {
            dtReg.put("TIPO_OPERACION", "M");
            dtReg.put("NM_SECMB", dtMedIni.getString("NM_SECMB"));
          }

          bAceptar = true;
          dispose();
          Inicializar(modoNORMAL);

          break;

        case modoBAJA:

          Inicializar(modoESPERA);
          dtReg = new sapp2.Data();
          dtReg.put("CD_ANO", diaBrote.getString("CD_ANO"));
          dtReg.put("NM_ALERBRO", diaBrote.getString("NM_ALERBRO"));
          dtReg.put("CD_GRUPO", diaBrote.getString("CD_GRUPO"));
          dtReg.put("CD_MEDIDA", txtMedida.getText());
          dtReg.put("DS_MEDIDA", txtDescMedida.getText());

          if (chkMasInfo.getState() == true) {
            dtReg.put("IT_INFADIC", "S");
          }
          else {
            dtReg.put("IT_INFADIC", "N");
          }

          dtReg.put("DS_INFADI", txtaMasInfo.getText());
          dtReg.put("NM_SECMB", dtMedIni.getString("NM_SECMB"));

          String OpeAntBaj = dtMedIni.getString("TIPO_OPERACION");
          if (OpeAntBaj.equals("M")) {
            dtReg.put("TIPO_OPERACION", "M");
          }
          else {
            dtReg.put("TIPO_OPERACION", "B");
          }

          //dtReg.put("TIPO_OPERACION","B");

          bAceptar = true;
          dispose();
          Inicializar(modoNORMAL);

          break;
      } // Fin switch
    } // Fin if validar datos
  } // Fin btnAceptarActionPerformed()

  void btnCancelarActionPerformed() {
    dispose();
  } // Fin btnCancelarActionPerformed()

  void btnMedidaActionPerformed() {
    CListaValores clv = null;
    QueryTool qt = null;
    Vector v = null;

    Inicializar(CInicializar.ESPERA);

    // configura el lector de centros
    qt = new QueryTool();

    // centros de vacunacion
    qt.putName("SIVE_MEDIDAS");

    // campos que se leen
    qt.putType("CD_MEDIDA", QueryTool.STRING);
    qt.putType("DS_MEDIDA", QueryTool.STRING);
    qt.putType("IT_INFADIC", QueryTool.STRING);
    qt.putType("IT_REPE", QueryTool.STRING);

    qt.putWhereType("CD_GRUPO", QueryTool.STRING);
    qt.putWhereValue("CD_GRUPO", diaBrote.getString("CD_GRUPO"));
    qt.putOperator("CD_GRUPO", "=");

    // campos que se muestran en la lista de valores
    v = new Vector();
    v.addElement("CD_MEDIDA");
    v.addElement("DS_MEDIDA");

    // lista de valores
    clv = new CListaValores(this.getApp(),
                            "Lista de Medidas",
                            qt,
                            v);
    clv.show();

    // recupera el s�ntoma seleccionado
    if (clv.getSelected() != null) {
      setMedida(clv.getSelected());
      bMedidaValid = true;
    }
    sIT_REPE = clv.getSelected().getString("IT_REPE");
    clv = null;
    // por si es necesario, borramos la caja de texto y ponemos
    // el 'tic' a false.
    txtaMasInfo.setText("");
    chkMasInfo.setState(false);

  } // Fin btnMedidaActionPerformed

  // guarda un Sintoma
  public void setMedida(sapp2.Data dt) {
    this.dtMedidas = dt;
    txtMedida.setText( (String) dt.get("CD_MEDIDA"));
    txtDescMedida.setText( (String) dt.get("DS_MEDIDA"));

    if (dt.get("IT_INFADIC").equals("S")) {
      bMasInfo = true;
    }
    else {
      bMasInfo = false;

    }
    if (dt.getString("IT_REPE").equals("S")) {
      bRepe = true;
    }
    else {
      bRepe = false;

    }
    bMedidaValid = true;

  }

  void chkMasInfoItemStateChanged(ItemEvent e) {
    // Para que sea inicializado el dialogo
    txtaMasInfo.setText("");
  }

  void txtMedidaKeyPressed() {
    txtDescMedida.setText("");
    bMedidaValid = false;
  } // Fin txtMedidaKeyPressed()

  void txtMedidaFocusLost() {

    DatCDDSMed data;

    // Si ya es valido el codigo se deja como esta
    if (bMedidaValid) {
      return;
    }

    // Si es vacio el codigo, se establece a invalido el bool
    if (txtMedida.getText().trim().equals("")) {
      bMedidaValid = false;
      return;
    }

    // Comprueba que los datos introducidos son v�lidos si no se han
    // seleccionado de los popup
    capp.CLista listaSalida = null;
    data = new DatCDDSMed(diaBrote.getString("CD_GRUPO"),
                          txtMedida.getText().trim(), "", "", "");

    capp.CLista parametros = new capp.CLista();
    parametros.addElement(data);

    try {
      stubCliente = new sapp.StubSrvBD();
      stubCliente.setUrl(new java.net.URL(app.getParametro("URL_SERVLET") +
                                          strSERVLET_LMED));
      listaSalida = (capp.CLista) stubCliente.doPost(constantes.
          sOBTENER_X_CODIGO, parametros);

      /*
             listaSalida = Comunicador.Communicate(this.app,
                          stubCliente,
                          constantes.sOBTENER_X_CODIGO,
                          strSERVLET_LMED,
                          parametros);*/

      if (listaSalida.size() == 0) {
        this.getApp().showAdvise("No hay datos con el criterio informado.");
        txtMedida.setText("");
        bMedidaValid = false;
        return;
      }
    }
    catch (Exception e) { // Acceso a base de datos erroneo
      e.printStackTrace();
      //comun.ShowWarning(this.getCApp(),e.getMessage());
      bMedidaValid = false;
      return;
    } // Fin try

    // Si todo ha ido bien
    data = (DatCDDSMed) listaSalida.elementAt(0);
    medida = data;
    txtMedida.setText(data.getCD_MEDIDA().trim());
    txtDescMedida.setText(data.getDS_MEDIDA().trim());

    if (data.getIT_INFADIC().equals("S")) {
      bMasInfo = true;
    }
    else {
      bMasInfo = false;

    }
    if (data.getIT_REPE().equals("S")) {
      bRepe = true;
    }
    else {
      bRepe = false;

      // Si no ha salido por ninguna de las condiciones anteriores,
      // la medida es v�lida, y se pone su boolean a true
    }
    bMedidaValid = true;

  } // Fin txtMedidaFocusLost()

} // endclass DiaMed

/******************* ESCUCHADORES **********************/

// Botones
class DiaMedActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaMed adaptee;
  ActionEvent evt;

  DiaMedActionAdapter(DiaMed adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    if (adaptee.bloquea()) {
      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Medidas")) {
        adaptee.btnMedidaActionPerformed();
      }
      adaptee.desbloquea();
    }
  }
} // Fin clase DiaMedActionAdapter

class DiaMedChkMasInfoItemAdapter
    implements java.awt.event.ItemListener {
  DiaMed adaptee;

  DiaMedChkMasInfoItemAdapter(DiaMed adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    if (adaptee.bloquea()) {
      adaptee.chkMasInfoItemStateChanged(e);
      adaptee.desbloquea();
    }
  }
} // Fin clase DiaMedChkMasInfoItemAdapter

/*
// CLista de Valores para el dialogo de Medidas
 class CListaMedidas extends CListaValores {
  protected DiaMed dia;
  public CListaMedidas(DiaMed d,
                       String title,
                       StubSrvBD stub,
                       String servlet,
                       int obtener_x_codigo,
                       int obtener_x_descripcion,
                       int seleccion_x_codigo,
                       int seleccion_x_descripcion) {
    super(d.getCApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
     dia = d;
     btnSearch_actionPerformed();
  }
  public Object setComponente(String s) {
    return new DatCDDSMed(dia.diaBrote.getCD_GRUPO(),s,"","","");
  }
  public String getGrupo(Object o) {
    return ( ((DatCDDSMed)o).getCD_GRUPO() );
  }
  public String getCodigo(Object o) {
    return ( ((DatCDDSMed)o).getCD_MEDIDA() );
  }
  public String getDescripcion(Object o) {
    return ( ((DatCDDSMed)o).getDS_MEDIDA() );
  }
  public String getFlMasInf(Object o) {
    return ( ((DatCDDSMed)o).getIT_INFADIC() );
  }
  public String getFlRepe(Object o) {
    return ( ((DatCDDSMed)o).getIT_REPE() );
  }
 } // Lista de Medidas
 */

// Escuchador para las cajas de texto
class DiaTextAdapter
    extends java.awt.event.KeyAdapter {
  DiaMed adaptee;

  DiaTextAdapter(DiaMed adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    if (adaptee.bloquea()) {
      if ( ( (TextField) e.getSource()).getName().equals("Medida")) {
        adaptee.txtMedidaKeyPressed();
      }
      adaptee.desbloquea();
    }
  }
} // Fin clase DiaTextAdapter

// Perdida de foco de las cajas
class DiaFocusAdapter
    implements java.awt.event.FocusListener, Runnable {
  DiaMed adaptee;
  FocusEvent evt;

  DiaFocusAdapter(DiaMed adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void focusGained(FocusEvent e) {

  }

  public void run() {
    if (adaptee.bloquea()) {
      if ( ( (TextField) evt.getSource()).getName().equals("Medida")) {
        adaptee.txtMedidaFocusLost();
      }
      adaptee.desbloquea();
    }
  }
} // Fin clase DiaFocusAdapter
