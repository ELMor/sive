/* Applet de prueba Suca */

package brotes.cliente.suca;

import java.util.Hashtable;

import brotes.cliente.c_comuncliente.SincrEventos;
import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;
import sapp2.Lista;

public class AppSuca
    extends CApp
    implements CInicializar {

  Suca pan = null;
  Lista lPaises = null;
  Lista lCAs = null;

  public void init() {
    super.init();
    setTitulo("Suca para Brotes");
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    PrepararListas();
    Hashtable hash = new Hashtable();
    hash.put("PAISES", lPaises);
    hash.put("CA", lCAs);

    pan = new Suca(this, this, new SincrEventos(), 0, hash);
    VerPanel("", pan);
  }

  private void PrepararListas() {
    // Paises
    lPaises = new Lista();
    Data d1 = new Data();
    d1.put("CD", "ALE");
    d1.put("DS", "Alemania");
    Data d2 = new Data();
    d2.put("CD", "ESP");
    d2.put("DS", "Espa�a");
    Data d3 = new Data();
    d3.put("CD", "ECU");
    d3.put("DS", "Ecuador");
    lPaises.addElement(d1);
    lPaises.addElement(d2);
    lPaises.addElement(d3);

    // CAs
    lCAs = new Lista();
    Data d4 = new Data();
    d4.put("CD", "02");
    d4.put("DS", "Arag�n");
    Data d5 = new Data();
    d5.put("CD", "13");
    d5.put("DS", "Madrid");
    Data d6 = new Data();
    d6.put("CD", "01");
    d6.put("DS", "Andalucia");
    lCAs.addElement(d4);
    lCAs.addElement(d5);
    lCAs.addElement(d6);

  }

  public void Inicializar(int modo) {

  }

}
