/**
 * Clase: Suca
 * Hereda: CPanel
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 17/05/2000
 * Analisis Funcional: Suca para brotes
 * Descripcion: Panel con la est�tica de suca
 *   con alguna funcionalida suca.
 * Notas: la interfaz es mediante rellenaDatos() y recogeDatos()
 *   a la que se les pasa un Data. keys que reconoce:
 *   CA,PROV,MUN,DSMUN,VIA,NUM,PISO,CODPOSTAL
 *
 */

package brotes.cliente.suca;

import java.util.Hashtable;
import java.util.Vector;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.ChoiceCDDS;
import brotes.cliente.c_componentes.DatoRecogible;
import brotes.cliente.c_comuncliente.BDatos;
import brotes.cliente.c_comuncliente.SincrEventos;
import capp2.CApp;
import capp2.CContextHelp;
import capp2.CInicializar;
import capp2.CListaValores;
import capp2.CPanel;
import capp2.CTexto;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class Suca
    extends CPanel
    implements CInicializar, DatoRecogible {

  /*************** Datos propios del panel ****************/

  // Parametros de entrada
  CApp applet = null;
  CInicializar pan = null;
  SincrEventos scr = null;
  Hashtable listas = null;

  // Vars. intermedias
  Lista lPaises = null;
  Lista lCAs = null;
  Lista lProvs = null;

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  // Constantes del modoEntrada
  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;

  // Nombres de servlets
  public final static String srvQT = "servlet/SrvQueryTool";

  /****************** Componentes del panel ********************/

  // Sincronizador de Eventos
  // Pasado como par�metro en constructor

  // Organizaci�n del panel
  private XYLayout xYLayout1 = new XYLayout();

  // Pais
  private Label lblPais = null;
  private ChoiceCDDS choPaises = null;

  // CA
  private Label lblCA = null;
  private ChoiceCDDS choCA = null;
  boolean caValid = false;

  // Provincia
  private Label lblProvincia = null;
  private ChoiceCDDS choProvincias = null;
  boolean provValid = false;

  // Municipio
  private Label lblMunicipio = null;
  private CTexto txtCodMun = null;
  private ButtonControl btnMunicipio = null;
  private TextField txtDescMunicipio = null;
  boolean munValid = false;

  // Nombre Via
  private Label lblNombreVia = new Label();
  private CTexto txtVia = null;
  // N�mero
  private Label lblNo = new Label();
  private CTexto txtNo = null;
  // Piso
  private Label lblPiso = new Label();
  private CTexto txtPiso = null;
  // Cod. Postal
  private Label lblCodPostal = new Label();
  private CTexto txtCodPostal = null;

  // Escuchadores (inicializados a null)

  // Botones
  SucaActionAdapter actionAdapter = null;

  // P�rdidas de foco
  SucaFocusAdapter focusAdapter = null;

  // Choices
  SucaChoiceItemListener choiceListener = null;

  // Constructor
  // @param a: CApp
  // @param s: sincronizador de eventos
  // @param modo: modo de entrada al di�logo ()
  // @param l: listas de valores
  public Suca(CApp a, CInicializar p, SincrEventos s, int modo, Hashtable l) {

    super(a);
    applet = a;
    pan = p;
    scr = s;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    lPaises = (Lista) l.get("PAISES");
    lCAs = (Lista) l.get("CA");

    try {
      // Inicializaci�n
      jbInit();
      Inicializar();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicia el aspecto del panel superior de alertas
  public void jbInit() throws Exception {
    // Variables locales
    boolean nulo, bCD, bDS;
    String sCD, sDS;

    // Carga de imagenes
    getApp().getLibImagenes().put(constantes.imgLUPA);
    getApp().getLibImagenes().CargaImagenes();

    // Escuchadores: botones y p�rdidas de foco
    actionAdapter = new SucaActionAdapter(this);
    focusAdapter = new SucaFocusAdapter(this);
    choiceListener = new SucaChoiceItemListener(this);

    // Organizacion del panel
    this.setSize(new Dimension(596, 116));
    this.setLayout(xYLayout1);
    xYLayout1.setWidth(596);
    xYLayout1.setHeight(116);

    // Pa�s
    lblPais = new Label("Pais:");
    nulo = false;
    bCD = false;
    bDS = true;
    sCD = "CD_PAIS";
    sDS = "DS_PAIS";
    choPaises = new ChoiceCDDS(nulo, bCD, bDS, sCD, sDS, lPaises);
    choPaises.writeData();
    choPaises.selectChoiceElement("ESP");
    choPaises.setEnabled(false); // Spain forever

    // CAs
    lblCA = new Label("C.A.");
    nulo = false;
    bCD = false;
    bDS = true;
    sCD = "CD_CA";
    sDS = "DS_CA";
    choCA = new ChoiceCDDS(nulo, bCD, bDS, sCD, sDS, lCAs);
    choCA.setName("CA");
    choCA.addItemListener(choiceListener);
    choCA.writeData();
    choCA.selectChoiceElement(applet.getParametro("CA")); // Inicialmente Madrid
    choCA.setEnabled(false);
    caValid = true;

    // Busqueda de las provincias de la CA inicial
    lProvs = buscarProvincias(choCA.getChoiceCD());

    // Provincia
    lblProvincia = new Label("Provincia");
    nulo = false;
    bCD = false;
    bDS = true;
    sCD = "CD_PROV";
    sDS = "DS_PROV";
    choProvincias = new ChoiceCDDS(nulo, bCD, bDS, sCD, sDS, lProvs);
    choProvincias.setName("Provincia");
    choProvincias.addItemListener(choiceListener);
    choProvincias.writeData();
    provValid = true;

    // Municipio
    lblMunicipio = new Label("Municipio");
    txtCodMun = new CTexto(3);
    txtCodMun.setName("Municipio");
    txtCodMun.addFocusListener(focusAdapter);
    btnMunicipio = new ButtonControl();
    btnMunicipio.setImage(this.getApp().getLibImagenes().get(constantes.imgLUPA));
    btnMunicipio.setActionCommand("Municipio");
    btnMunicipio.addActionListener(actionAdapter);
    txtDescMunicipio = new TextField();
    txtDescMunicipio.setEnabled(false);
    txtDescMunicipio.setEditable(false);
    munValid = false;

    // Nombre Via
    lblNombreVia = new Label("Via");
    txtVia = new CTexto(50);

    // N�mero
    lblNo = new Label("N�.");
    txtNo = new CTexto(6);

    // Piso
    lblPiso = new Label("Piso");
    txtPiso = new CTexto(4);

    // Cod. Postal
    lblCodPostal = new Label("C. Postal");
    txtCodPostal = new CTexto(5);

    // Adici�n de componentes al di�logo
    this.add(lblPais, new XYConstraints(3, 3, 32, 19));
    this.add(choPaises, new XYConstraints(59, 3, 181, 24));
    this.add(lblCA, new XYConstraints(249, 3, 28, 19));
    this.add(choCA, new XYConstraints(308, 3, 278, 24));
    this.add(lblProvincia, new XYConstraints(3, 32, 54, 19));
    this.add(choProvincias, new XYConstraints(59, 32, 181, 24));
    this.add(lblMunicipio, new XYConstraints(249, 34, 55, 19));
    this.add(txtCodMun, new XYConstraints(308, 32, 49, 24));
    this.add(btnMunicipio, new XYConstraints(362, 30, 25, 25));
    this.add(txtDescMunicipio, new XYConstraints(400, 32, 186, 24));
    this.add(lblNombreVia, new XYConstraints(3, 61, 43, 19));
    this.add(txtVia, new XYConstraints(3, 83, 370, 24));
    this.add(lblNo, new XYConstraints(384, 61, 25, 19));
    this.add(txtNo, new XYConstraints(381, 83, 42, 24));
    this.add(lblPiso, new XYConstraints(433, 61, 38, 19));
    this.add(txtPiso, new XYConstraints(431, 83, 75, 24));
    this.add(lblCodPostal, new XYConstraints(518, 61, 54, 19));
    this.add(txtCodPostal, new XYConstraints(518, 83, 68, 24));

    // Tool Tips
    new CContextHelp("Municipios", btnMunicipio);
  } // Fin jbInit()

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    if (pan != null) {
      pan.Inicializar(modoOperacion);
    }
  } // Fin Inicializar()

  public void InicializarDesdeFuera(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicia()

  public void Inicializar() {
    switch (modoOperacion) {
      // Modo espera
      case CInicializar.ESPERA:
        enableControls(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // Modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case ALTA:
          case MODIFICACION:
            enableControls(true);
            break;
          case BAJA:
          case CONSULTA:
            enableControls(false);
            break;
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    } // Fin switch
  } // Fin Inicializar()

  public void enableControls(boolean state) {
    // Pais: siempre a false
    // choPaises.setEnabled(state);

    // CA
    //choCA.setEnabled(state);

    // Provincia
    choProvincias.setEnabled(state && caValid);

    // Municipio
    txtCodMun.setEnabled(state && caValid && provValid);
    btnMunicipio.setEnabled(state);

    // Nombre Via
    txtVia.setEnabled(state);

    // N�mero
    txtNo.setEnabled(state);

    // Piso
    txtPiso.setEnabled(state);

    // Cod. Postal
    txtCodPostal.setEnabled(state);
  } // Fin enableControls()

  /********** Sincronizador de Eventos ******************/

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  /********** Funciones Interfaz DatoRecogible *****************/

  public void rellenarDatos(Data d) {

    if (scr.bloquea(modoOperacion, this)) {

      if (! ( (String) d.get("CA")).equals("")) {
        choCA.selectChoiceElement( (String) d.get("CA"));
        try {
          lProvs = buscarProvincias( (String) d.get("CA"));
        }
        catch (Exception e) {}
        ;
        choProvincias.removeAll();
        choProvincias.writeData();

        if (! ( (String) d.get("PROV")).equals("")) {
          choProvincias.selectChoiceElement( (String) d.get("PROV"));
          if (! ( (String) d.get("MUN")).equals("")) {
            txtCodMun.setText( (String) d.get("MUN"));
            txtMunicipioFocusLost();
          }
        }
      }

      if (! ( (String) d.get("VIA")).equals("")) {
        txtVia.setText( (String) d.get("VIA"));

      }
      if (! ( (String) d.get("NUM")).equals("")) {
        txtNo.setText( (String) d.get("NUM"));

      }
      if (! ( (String) d.get("PISO")).equals("")) {
        txtPiso.setText( (String) d.get("PISO"));

      }
      if (! ( (String) d.get("CODPOSTAL")).equals("")) {
        txtCodPostal.setText( (String) d.get("CODPOSTAL"));

      }
      scr.desbloquea(this);
    }
  } // Fin rellenarDatos()

  public void recogerDatos(Data d) {
    if (caValid) {
      d.put("CA", choCA.getChoiceCD());
    }
    else {
      d.put("CA", "");

    }
    if (provValid) {
      d.put("PROV", choProvincias.getChoiceCD());
    }
    else {
      d.put("PROV", "");

    }
    if (munValid) {
      d.put("MUN", txtCodMun.getText().trim());
      d.put("DSMUN", txtDescMunicipio.getText().trim());
    }
    else {
      d.put("MUN", "");
      d.put("DSMUN", "");
    }

    if (!txtVia.getText().equals("")) {
      d.put("VIA", txtVia.getText().trim());
    }
    else {
      d.put("VIA", "");

    }
    if (!txtNo.getText().equals("")) {
      d.put("NUM", txtNo.getText().trim());
    }
    else {
      d.put("NUM", "");

    }
    if (!txtPiso.getText().equals("")) {
      d.put("PISO", txtPiso.getText().trim());
    }
    else {
      d.put("PISO", "");

    }
    if (!txtCodPostal.getText().equals("")) {
      d.put("CODPOSTAL", txtCodPostal.getText().trim());
    }
    else {
      d.put("CODPOSTAL", "");

    }
  } // Fin recogerDatos()

  public boolean validarDatos() {
    // No hay campos obligatorios, ni comprobaciones de tipos de datos
    //  pq ya se hacen en las p�rdidas de foco

    return true;
  } // Fin validarDatos()

  /***************** Funciones auxiliares ***************/
  private Lista buscarProvincias(String CA) throws Exception {
    // Montaje de la QueryTool
    QueryTool qt = new QueryTool();
    Lista lQuery = new Lista();
    Lista lResul = null;

    qt.putName("SIVE_PROVINCIA");
    qt.putType("CD_PROV", QueryTool.STRING);
    qt.putType("DS_PROV", QueryTool.STRING);
    qt.putWhereType("CD_CA", QueryTool.STRING);
    qt.putWhereValue("CD_CA", CA);
    qt.putOperator("CD_CA", "=");
    lQuery.addElement(qt);

    lResul = BDatos.execSQL(applet, "servlet/SrvQueryTool", 1, lQuery); // 1 -> DO_SELECT

    return lResul;
  }

  public void setModoEntrada(int m) {
    modoEntrada = m;
  } // Fin setModoEntrada()

  /****************** Manejadores de Eventos *****************/

  void btnMunicipioActionPerformed() {
    CListaValores clvMun = null;
    Vector vCod = null;
    QueryTool2 qt = new QueryTool2(); ;

    // SELECT CD_MUN,DS_MUN FROM SIVE_MUNICIPIO
    //  WHERE CD_PROV = ?
    qt.putName("SIVE_MUNICIPIO");
    qt.putType("CD_MUN", QueryTool.STRING);
    qt.putType("DS_MUN", QueryTool.STRING);
    qt.putWhereType("CD_PROV", QueryTool.STRING);
    qt.putWhereValue("CD_PROV", choProvincias.getChoiceCD());
    qt.putOperator("CD_PROV", "=");

    // Campos que se muestran en la lista de valores
    vCod = new Vector();
    vCod.addElement("CD_MUN");
    vCod.addElement("DS_MUN");

    // Lista de valores
    clvMun = new CListaValores(applet,
                               "Municipios",
                               qt,
                               vCod);
    clvMun.show();

    // Recupera el centro seleccionado
    if (clvMun.getSelected() != null) {
      txtCodMun.setText( ( (String) clvMun.getSelected().get("CD_MUN")));
      txtDescMunicipio.setText( ( (String) clvMun.getSelected().get("DS_MUN")));
      munValid = true;
    }

    clvMun = null;
  } // Fin btnMunicipioActionPerformed()

  void txtMunicipioFocusLost() {
    if (txtCodMun.getText().equals("")) {
      return;
    }

    QueryTool2 qt = new QueryTool2();
    Lista lQuery = new Lista();
    Lista lResul = null;
    Data dtResul = null;

    // SELECT CD_MUN,DS_MUN FROM SIVE_MUNICIPIO
    //  WHERE CD_MUN = ?
    qt.putName("SIVE_MUNICIPIO");
    qt.putType("CD_MUN", QueryTool.STRING);
    qt.putType("DS_MUN", QueryTool.STRING);
    qt.putWhereType("CD_PROV", QueryTool.STRING);
    qt.putWhereValue("CD_PROV", choProvincias.getChoiceCD());
    qt.putOperator("CD_PROV", "=");
    qt.putWhereType("CD_MUN", QueryTool.STRING);
    qt.putWhereValue("CD_MUN", txtCodMun.getText());
    qt.putOperator("CD_MUN", "=");
    lQuery.addElement(qt);

    try {
      lResul = BDatos.execSQL(applet, "servlet/SrvQueryTool", 1, lQuery); // 1- DO_SELECT

      if (lResul.size() == 0) {
        this.getApp().showAdvise("Municipio no v�lido");
        txtCodMun.setText("");
        txtDescMunicipio.setText("");
        munValid = false;
      }
      else {
        dtResul = (Data) lResul.firstElement();
        txtDescMunicipio.setText( (String) dtResul.get("DS_MUN"));
        munValid = true;
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }

  } // Fin txtMunicipioFocusLost()

  void choCAItemStateChanged() {
    // Busqueda de las provincias de la CA inicial
    try {
      lProvs = buscarProvincias(choCA.getChoiceCD());
      choProvincias.setLista(lProvs);
      choProvincias.removeAll();
      choProvincias.writeData();
      munValid = false;
      txtCodMun.setText("");
      txtDescMunicipio.setText("");
    }
    catch (Exception e) {}
  } // Fin choCAItemStateChanged()

  void choProvItemStateChanged() {
    munValid = false;
    txtCodMun.setText("");
    txtDescMunicipio.setText("");
  } // Fin choProvItemStateChanged()

} // Fin class Suca

/******************* ESCUCHADORES **********************/

// Botones
class SucaActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Suca adaptee;
  ActionEvent evt;

  SucaActionAdapter(Suca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if (evt.getActionCommand().equals("Municipio")) {
        adaptee.btnMunicipioActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  SucaActionAdapter

// P�rdidas de foco
class SucaFocusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Suca adaptee;
  FocusEvent evt;

  SucaFocusAdapter(Suca adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if ( ( (TextField) evt.getSource()).getName().equals("Municipio")) {
        adaptee.txtMunicipioFocusLost();
      }
      s.desbloquea(adaptee);
    }
  }
} // Fin clase SucaFocusAdapter

// Choices: cambio de seleccion
class SucaChoiceItemListener
    implements java.awt.event.ItemListener, Runnable {
  Suca adaptee;
  ItemEvent evt = null;

  SucaChoiceItemListener(Suca adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      String cho = ( (Component) evt.getSource()).getName();
      if (cho.equals("CA")) {
        adaptee.choCAItemStateChanged();
      }
      else if (cho.equals("Provincia")) {
        adaptee.choProvItemStateChanged();
      }
      s.desbloquea(adaptee);
    }
  }
} // Fin clase SucaChoiceItemListener
