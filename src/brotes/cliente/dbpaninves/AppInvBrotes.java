/*
 package brotes.cliente.invesbrotes;
 import capp2.*;
 public class AppInvBrotes extends CApp {
  public AppInvBrotes() {
    super();
  }
  public void init() {
    super.init();
    setTitulo("Mantenimiento de Investigadores de Brotes");
    VerPanel("", new PanInvBrotes(this));
  }
 }
 */
package brotes.cliente.dbpaninves;

import capp2.CApp;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class AppInvBrotes
    extends CApp {

  DBPanInves pan = null;

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {

    // query tool
    QueryTool qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", "1999");
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", "3");
    qtBloqueo.putOperator("NM_ALERBRO", "=");
    Lista l = new Lista();
    l.addElement(qtBloqueo);

    Lista vResultado = null;
    try {
      this.getStub().setUrl("servlet/SrvQueryTool");
      vResultado = (Lista)this.getStub().doPost(1, l);
    }
    catch (Exception ex) {}
    ;

    Data miData = (Data) vResultado.elementAt(0);

    String Ano = "2000";
    String NumBrote = "25";
    String Grupo = "3";
    String DescBrote = "Informe Brote 1";
    String CDCA = "13";
    String Ope = "D_1";
    String FUlt = "03/03/2000 12:33:36";
    Data datos = new Data();
    datos.put("CD_ANO", Ano);
    datos.put("NM_ALERBRO", NumBrote);
    datos.put("DS_BROTE", DescBrote);
    datos.put("CD_CA", CDCA);
    datos.put("CD_GRUPO", Grupo);
    datos.put("CD_OPE", Ope); //miData.getString("CD_OPE"));
    datos.put("FC_ULTACT", FUlt); //miData.getString("FC_ULTACT"));

    setTitulo("Applet prueba investigadores");
    //pan = new PanTasVac(this,pan.MODIFICACION,datos);
    pan = new DBPanInves(this, pan.CONSULTA, datos);
    pan.show();
  }
}
