package brotes.cliente.dbpaninves;

import java.util.Vector;

import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.CheckboxControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CContextHelp;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

/**
 * Panel a trav�s del que se realizan los mantenimientos
 * de investigadores de brotes.
 * @autor PDP
 * @version 1.0
 */
public class DBPanInves
    extends CDialog
    implements CInicializar, CFiltro { //CPanel

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  CApp applet = null;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;
  public int modoActualizacion = ALTA;

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  // estructuras para comprobar el bloqueo
  private QueryTool qtBloqueo = null;
  private Data dtBloqueo = null;

  private Lista lismanBaj = new Lista();

  XYLayout xYLayout1 = new XYLayout();
  Label lblInv = new Label();
  TextField txtCod = new TextField();
  TextField txtDes = new TextField();
  Label lblBroteDesc = new Label();
  Label lblDatosBrote = new Label();
  CListaMantenimiento clmMantenimiento = null;
  ButtonControl btnBuscar = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  CheckboxControl chckCod = new CheckboxControl();
  CheckboxControl chckDes = new CheckboxControl();
  CheckboxGroup chkboxGrupo = new CheckboxGroup();

  // datos brote
  Data dtBrote = null;

  // filtro
  private Data dtFiltro = null;

  // variables necesarias para adaptar dialogos y paneles a
  // nuestra tabla...

  Data dtConstPaneles = new Data();

  boolean campoExtra = true; // Variables para este panel..
  boolean campoNumExtra = true;

  // constructor del panel PanInvBrotes
  public DBPanInves(CApp a, int modo, Data dt) {

    super(a);
    applet = a;

    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    dtBrote = dt;
    modoActualizacion = modo;

    dtConstPaneles.put("sTituloDialogoAlta", "Brotes: Investigadores");
    dtConstPaneles.put("sTituloDialogoModif", "Brotes: Investigadores");
    dtConstPaneles.put("sTituloDialogoBaja", "Brotes: Investigadores");
    dtConstPaneles.put("sNomTabla1", "USUARIO");
    dtConstPaneles.put("sNomTabla2", "SIVE_INVESTIGADOR");
    dtConstPaneles.put("sNomCampoCod", "COD_USUARIO");
    dtConstPaneles.put("sTipoCampoCod", QueryTool.STRING + "");
    dtConstPaneles.put("sNomCampoDes", "NOMBRE");
    dtConstPaneles.put("sTipoCampoDes", QueryTool.STRING + "");
    dtConstPaneles.put("bCampoExtra", "false");
    dtConstPaneles.put("bNumExtra", "false");
    dtConstPaneles.put("nLongCod", "3");
    dtConstPaneles.put("nLongDesc", "30");
    dtConstPaneles.put("CD_ANO", dtBrote.getString("CD_ANO"));
    dtConstPaneles.put("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
    dtConstPaneles.put("DS_BROTE", dtBrote.getString("DS_BROTE"));
    dtConstPaneles.put("CD_CA", dtBrote.getString("CD_CA"));

    campoExtra = false; // Variable para este panel
    campoNumExtra = false;

    try {

      // botones
      vBotones.addElement(new CBoton("",
                                     "images/alta.gif",
                                     "Nueva petici�n",
                                     false,
                                     false));

      vBotones.addElement(new CBoton("",
                                     "images/modificacion.gif",
                                     "Modificar petici�n",
                                     true,
                                     true));

      vBotones.addElement(new CBoton("",
                                     "images/baja.gif",
                                     "Borrar petici�n",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("Usuario",
                                      "225",
                                      (String) dtConstPaneles.get(
          "sNomCampoCod")));

      vLabels.addElement(new CColumna("Descripci�n",
                                      "250",
                                      "NOMBRE"));

      vLabels.addElement(new CColumna("Fecha de alta",
                                      "200",
                                      "FC_ALTA"));

      clmMantenimiento = new CListaMantenimiento(a,
                                                 vLabels,
                                                 vBotones,
                                                 this,
                                                 this);
      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del panel PanInvBrotes
  public void jbInit() throws Exception {

    final String imgBuscar = "images/refrescar.gif";
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgBuscar);
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    // Escuchadores de eventos

    PanInvBrotes_actionAdapter actionAdapter = new PanInvBrotes_actionAdapter(this);

    this.setSize(new Dimension(755, 420));
    this.setLayout(xYLayout1);
    this.setTitle("Investigadores de un brote");
    xYLayout1.setWidth(725);
    xYLayout1.setHeight(420);

    lblBroteDesc.setText("Brote:");
    lblDatosBrote.setText(dtBrote.getString("CD_ANO") + "/" +
                          dtBrote.getString("NM_ALERBRO") + " - " +
                          dtBrote.getString("DS_BROTE"));

    txtCod.setVisible(true);
    txtDes.setVisible(false);

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnAceptar.addActionListener(actionAdapter);

    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));
    btnCancelar.addActionListener(actionAdapter);

    this.setLayout(xYLayout1);
    this.add(lblBroteDesc, new XYConstraints(16, 15, 42, -1));
    this.add(lblDatosBrote, new XYConstraints(98, 15, 600, -1));
    this.add(clmMantenimiento, new XYConstraints(7, 45, 710, 292));
    this.add(btnAceptar, new XYConstraints(523, 340, 88, 29));
    this.add(btnCancelar, new XYConstraints(634, 340, 88, 29));

    // tool tips

    new CContextHelp("Obtener mantenimientos", btnBuscar);

    Inicializar(CInicializar.ESPERA);
    clmMantenimiento.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);

  }

  public void Inicializar() {}

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if (modoActualizacion == MODIFICACION) {
          this.setEnabled(true);
          btnAceptar.setEnabled(true);
          btnCancelar.setEnabled(true);
          clmMantenimiento.setEnabled(true);
        }
        else if (modoActualizacion == CONSULTA) {
          this.setEnabled(false);
          clmMantenimiento.setEnabled(false);
          btnAceptar.setVisible(false);
          btnCancelar.setLabel("Salir");
          btnCancelar.setEnabled(true);
        }
        else if (modoActualizacion == BAJA) {
          this.setEnabled(false);
          clmMantenimiento.setEnabled(false);
          btnAceptar.setVisible(false);
          btnCancelar.setLabel("Salir");
          btnCancelar.setEnabled(true);
        }
        break;
    }
  }

  //Buscar
  void btn_actionPerformed(ActionEvent e) {
    Inicializar(CInicializar.ESPERA);
    Lista lstMant = new Lista();
    Lista lstOperar = new Lista();
    Lista vResultado = new Lista();
    if (e.getActionCommand().equals("Aceptar")) {
      try {
        this.getApp().getStub().setUrl("servlet/SrvTransaccion");

        // Query para actualizar SIVE_BROTES (CD_OPE/FC_ULTACT)
        QueryTool qtBro = new QueryTool();
        Data dtBro = new Data();
        qtBro.putName("SIVE_BROTES");
        qtBro.putType("CD_OPE", QueryTool.STRING);
        qtBro.putType("FC_ULTACT", QueryTool.TIMESTAMP);
        qtBro.putValue("CD_OPE", dtBrote.getString("CD_OPE"));
        qtBro.putValue("FC_ULTACT", "");

        qtBro.putWhereType("CD_ANO", QueryTool.STRING);
        qtBro.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
        qtBro.putOperator("CD_ANO", "=");
        qtBro.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
        qtBro.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
        qtBro.putOperator("NM_ALERBRO", "=");

        dtBro.put("4", qtBro);
        lstOperar.addElement(dtBro);

        /************** ALTAS ***************/
        lstMant = this.clmMantenimiento.getLista();

        Data dtTemp = null;

        for (int i = 0; i < lstMant.size(); i++) {
          dtTemp = (Data) lstMant.elementAt(i);
          if (dtTemp.getString("TIPO_OPERACION").equals("A")) {
            QueryTool qtAlt = new QueryTool();
            qtAlt = realizarAlta(dtTemp);

            Data dtAlt = new Data();
            dtAlt.put("3", qtAlt);

            lstOperar.addElement(dtAlt);

            dtTemp.remove("TIPO_OPERACION");
          }
        }

        /************** MODIFICAR ***************/
        for (int i = 0; i < lstMant.size(); i++) {
          dtTemp = (Data) lstMant.elementAt(i);
          if (dtTemp.getString("TIPO_OPERACION").equals("M")) {
            QueryTool qtUpd = new QueryTool();
            qtUpd = realizarUpdate(dtTemp);

            Data dtUpdate = new Data();
            dtUpdate.put("4", qtUpd);

            lstOperar.addElement(dtUpdate);

            dtTemp.remove("TIPO_OPERACION");
          }
        }

        /************** BAJAS ***************/
        if (lismanBaj.size() > 0) {
          for (int i = 0; i < lismanBaj.size(); i++) {
            dtTemp = (Data) lismanBaj.elementAt(i);
            QueryTool qtBaja = new QueryTool();
            qtBaja = realizarBaja(dtTemp);

            Data dtBaja = new Data();
            dtBaja.put("5", qtBaja);

            lstOperar.addElement(dtBaja);
          }
        }

        //realizamos la actualizaci�n
        preparaBloqueo();
        if (lstOperar.size() > 0) {
          vResultado = (Lista)this.getApp().getStub().doPost(10000,
              lstOperar,
              qtBloqueo,
              dtBloqueo,
              getApp());

          //obtener el nuevo cd_ope y fc_ultact de la base de datos.
        }
        int tamano = vResultado.size();
        String fecha = "";

        fecha = ( (Lista) vResultado.firstElement()).getFC_ULTACT();

        dtBrote.put("CD_OPE", dtBrote.getString("CD_OPE"));
        dtBrote.put("FC_ULTACT", fecha);

        dispose();

      }
      catch (Exception exc) {

        dispose();

      } // del "trai cach"
      dispose();
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
    Inicializar(CInicializar.NORMAL);

  }

  // prepara la informaci�n para el control de bloqueo
  private void preparaBloqueo() {
    // query tool
    qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
    qtBloqueo.putOperator("NM_ALERBRO", "=");

    // informaci�n de bloqueo
    dtBloqueo = new Data();
    dtBloqueo.put("CD_OPE", dtBrote.getString("CD_OPE"));
    dtBloqueo.put("FC_ULTACT", dtBrote.getString("FC_ULTACT"));
  }

  QueryTool realizarAlta(Data dtAlta) {

    QueryTool qtAlt = new QueryTool();

    // tabla de familias de tasas vacunables
    qtAlt.putName("SIVE_INVESTIGADOR");

    // campos que se escriben
    qtAlt.putType("CD_USUARIO", QueryTool.STRING);
    qtAlt.putType("CD_ANO", QueryTool.STRING);
    qtAlt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtAlt.putType("FC_ALTA", QueryTool.DATE);
    qtAlt.putType("CD_CA", QueryTool.STRING);

    // Valores de los campos
    qtAlt.putValue("CD_USUARIO", dtAlta.getString("COD_USUARIO"));
    qtAlt.putValue("CD_ANO", dtBrote.getString("CD_ANO"));
    qtAlt.putValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
    qtAlt.putValue("FC_ALTA", dtAlta.getString("FC_ALTA"));
    qtAlt.putValue("CD_CA", applet.getParametro("CA"));

    return qtAlt;
  }

  QueryTool realizarUpdate(Data dtUpd) {

    QueryTool qtUpd = new QueryTool();

    // tabla de familias de tasas vacunables
    qtUpd.putName("SIVE_INVESTIGADOR");

    // campos que se escriben
    qtUpd.putType("FC_ALTA", QueryTool.DATE);

    // Valores de los campos
    qtUpd.putValue("FC_ALTA", dtUpd.getString("FC_ALTA"));

    //
    qtUpd.putWhereType("CD_USUARIO", QueryTool.STRING);
    qtUpd.putWhereValue("CD_USUARIO", dtUpd.getString("COD_USUARIO"));
    qtUpd.putOperator("CD_USUARIO", "=");
    qtUpd.putWhereType("CD_ANO", QueryTool.STRING);
    qtUpd.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
    qtUpd.putOperator("CD_ANO", "=");
    qtUpd.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtUpd.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
    qtUpd.putOperator("NM_ALERBRO", "=");
    qtUpd.putWhereType("CD_CA", QueryTool.STRING);
    qtUpd.putWhereValue("CD_CA", applet.getParametro("CA"));
    qtUpd.putOperator("CD_CA", "=");

    return qtUpd;
  }

  QueryTool realizarBaja(Data dtBaj) {

    QueryTool qtBaj = new QueryTool();

    // tabla de familias de productos
    qtBaj.putName("SIVE_INVESTIGADOR");
    //
    qtBaj.putWhereType("CD_USUARIO", QueryTool.STRING);
    qtBaj.putWhereValue("CD_USUARIO", dtBaj.getString("COD_USUARIO"));
    qtBaj.putOperator("CD_USUARIO", "=");
    qtBaj.putWhereType("CD_ANO", QueryTool.STRING);
    qtBaj.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
    qtBaj.putOperator("CD_ANO", "=");
    qtBaj.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBaj.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
    qtBaj.putOperator("NM_ALERBRO", "=");
    qtBaj.putWhereType("CD_CA", QueryTool.STRING);
    qtBaj.putWhereValue("CD_CA", applet.getParametro("CA"));
    qtBaj.putOperator("CD_CA", "=");

    return qtBaj;
  }

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {

    // nombre del servlet
    final String servlet = "servlet/SrvSelInves";

    // lista con los investigadores
    Lista vInv = null;

    // lista para el filtro
    Lista vFiltro = new Lista();

    // rellena los par�metros
    dtFiltro = new Data();

    // tipo presupuesto
    dtFiltro.put("CD_ANO", dtBrote.getString("CD_ANO"));
    dtFiltro.put("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
    dtFiltro.put("CD_CA", applet.getParametro("CA"));

    // realiza la consulta al servlet
    try {

      this.getApp().getStub().setUrl(servlet);
      vFiltro.addElement(dtFiltro);
      vInv = (Lista)this.getApp().getStub().doPost(1, vFiltro);

      // muestra incidencia
      if (vInv.size() == 0) {
        this.getApp().showAdvise("No hay investigadores con estos criterios.");
      }

      // error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    // la lista debe estar creada para pasarsela al componente
    if (vInv == null) {
      vInv = new Lista();

      // devuelve la lista para que la procese el componente clm
    }
    return vInv;

  }

  // solicita la siguiente trama de datos
  public Lista siguientePagina() {
    Lista vPetic = new Lista();
    return vPetic;

  }

  // operaciones de la botonera
  public void realizaOperacion(int j) {

    Lista lisPet = this.clmMantenimiento.getLista();
    DBDiaInves dm = null;
    Data dPetMod = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:
        dm = new DBDiaInves(this.getApp(), 0, null, dtConstPaneles);
        dm.show();
        if (dm.bAceptar) {
          dPetMod = dm.devuelveData();
          lisPet.addElement(dPetMod);
          clmMantenimiento.setPrimeraPagina(lisPet);
        }
        break;
      case MODIFICACION:
        dPetMod = clmMantenimiento.getSelected();
        if (dPetMod != null) {
          int ind = clmMantenimiento.getSelectedIndex();
          dm = new DBDiaInves(this.getApp(), 1, dPetMod, dtConstPaneles);
          dm.show();
          if (dm.bAceptar) {
            lisPet.removeElementAt(ind);
            dPetMod = dm.devuelveData();
            lisPet.addElement(dPetMod);
            clmMantenimiento.setPrimeraPagina(lisPet);
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dPetMod = clmMantenimiento.getSelected();
        if (dPetMod != null) {
          int ind = clmMantenimiento.getSelectedIndex();
          dm = new DBDiaInves(this.getApp(), 2, dPetMod, dtConstPaneles);
          dm.show();
          if (dm.bAceptar()) {
            lismanBaj.addElement(clmMantenimiento.getSelected());
            lisPet.removeElementAt(ind);
            clmMantenimiento.setPrimeraPagina(lisPet);
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;
    }
  }

}

// boton buscar
class PanInvBrotes_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DBPanInves adaptee;
  ActionEvent e;

  PanInvBrotes_actionAdapter(DBPanInves adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
