package brotes.cliente.dbpaninves;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CCodigo;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CPnlCodigo;
import capp2.CTexto;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;
import sapp2.StubSrvBD;

/**
 * Di�logo a trav�s del cu�l se insertan/modifican/borran datos
 * en tablas de mantenimiento de investigadores de brptes.
 * @autor PDP
 * @version 1.0
 */

public class DBDiaInves
    extends CDialog
    implements CInicializar {

  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  // parametros pasados al di�logo
  public Data dtDev = null;
  // par�metros configuraci�n de ventana.
  private Data dtParam = null;
  private boolean bExtra = false;
  private boolean bNumerico = false;
  private String nomCampoCod = "";
  private String nomCampoDesc = "";
  private String nomTabla = "";
  private int tipoCampoCod = 0;
  private int tipoCampoDesc = 0;
  //
  public Data dtDevuelto = new Data();
  public Data dtBro = new Data();

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  CApp applet = null;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xYLayout1 = new XYLayout();
  CPnlCodigo pnlInv = null;
  Label lblCod = new Label();
  Label lblDatosBrote = new Label();
  Label lblDes = new Label();
  Label label1 = new Label();
  CCodigo txtCodigo = null;
  CTexto txtDescripcion = null;
  Label lblFecha = new Label();
//  CFechaSimple txtFecha = new CFechaSimple("N");  21-05-01 (ARS)
// Cambio para automatizar el poner barras de fecha.
  fechas.CFecha txtFecha = new fechas.CFecha("N");

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores de eventos...

  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  // constructor del di�logo DBDiaInves
  public DBDiaInves(CApp a, int modoop, Data dt, Data parametros) {

    super(a);
    applet = a;
    QueryTool qt1 = null;
    QueryTool2 qt = null;

    try {
      modoOperacion = modoop;
      dtDev = dt;
      dtParam = parametros;
      // longitud de los campos de texto...
      txtCodigo = new CCodigo(Integer.parseInt( (String) dtParam.get("nLongCod")));
      txtDescripcion = new CTexto(Integer.parseInt( (String) dtParam.get(
          "nLongDesc")));

      // configura el componente de investigador
      //QueryTool qt1 = QueryTool();
      qt1 = new QueryTool();
      qt1.putName("USUARIO");
      qt1.putType("NOMBRE", QueryTool.STRING);
      qt1.putType("COD_USUARIO", QueryTool.STRING);

      String subQuery = " select COD_USUARIO from USUARIO_GRUPO "
          + " where COD_APLICACION = ? ";
      qt1.putSubquery("COD_USUARIO", subQuery);

      java.util.Vector vTV = new java.util.Vector();
      Data dtTV = new Data();
      dtTV.put(new Integer(QueryTool.STRING), "BRO");
      vTV.addElement(dtTV);
      qt1.putVectorSubquery("COD_USUARIO", vTV);

      // panel investigador
      pnlInv = new CPnlCodigo(a,
                              this,
                              qt1,
                              "COD_USUARIO",
                              "NOMBRE",
                              true,
                              "Tipos de Investigadores",
                              "Tipos de Investigadores");

      // Cogemos los par�metros de la tabla
      nomCampoCod = (String) dtParam.get("sNomCampoCod");
      nomCampoDesc = (String) dtParam.get("sNomCampoDes");
      tipoCampoCod = Integer.parseInt( (String) dtParam.get("sTipoCampoCod"));
      tipoCampoDesc = Integer.parseInt( (String) dtParam.get("sTipoCampoDes"));

      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del dialogo
  public void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xYLayout1);
    this.setSize(400, 239);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    txtCodigo.setBackground(new Color(255, 255, 150));
    lblDes.setText("Investigador :");
    lblCod.setText("Brote N� :");
    lblDatosBrote.setText(dtParam.getString("CD_ANO") + "/" +
                          dtParam.getString("NM_ALERBRO") + " - " +
                          dtParam.getString("DS_BROTE"));
    lblFecha.setText("Fecha de Alta:");
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xYLayout1.setHeight(239);
    xYLayout1.setWidth(397);

    this.add(lblCod, new XYConstraints(21, 29, 84, -1));
    this.add(lblDatosBrote, new XYConstraints(131, 29, 252, -1));

    this.add(lblDes, new XYConstraints(21, 71, 89, -1));
    this.add(pnlInv, new XYConstraints(129, 71, 246, -1));
    this.add(lblFecha, new XYConstraints(21, 119, 89, -1));
    this.add(txtFecha, new XYConstraints(134, 119, 89, -1));
    this.add(btnAceptar, new XYConstraints(187, 167, 89, -1));
    this.add(btnCancelar, new XYConstraints(281, 167, 89, -1));
    //m�todo temporal para poder ver los datos en el panel
    //pasados a trav�s de la applet
    verDatos();
    // ponemos el t�tulo

    switch (modoOperacion) {
      case modoALTA:
        setTitle( (String) dtParam.get("sTituloDialogoAlta"));
        break;
      case modoMODIFICACION:
        this.setTitle( (String) dtParam.get("sTituloDialogoModif"));
        break;
      case modoBAJA:
        this.setTitle( (String) dtParam.get("sTituloDialogoBaja"));
        break;
    }
  }

  // Pone los campos de texto en el estado que les corresponda
  // segun el modo.

  public void verDatos() {
    switch (modoOperacion) {
      case modoALTA:
        txtCodigo.setEnabled(true);
        txtDescripcion.setEnabled(true);
        break;

      case modoMODIFICACION:
        pnlInv.setEnabled(false);
        // datos panel principal
        pnlInv.setCodigo(dtDev);
        txtFecha.setText(dtDev.getString("FC_ALTA"));
        break;

        // s�lo habilitado aceptar y cancelar
      case modoBAJA:
        pnlInv.setEnabled(false);
        // datos panel principal
        pnlInv.setCodigo(dtDev);
        txtFecha.setEnabled(false);
        txtFecha.setText(dtDev.getString("FC_ALTA"));
        break;
    }
  }

  // Pone el di�logo en modo de espera

  public void Inicializar(int modo) {
    switch (modo) {

      // componente producto accediendo a base de datos
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // componente en el modo requerido
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoALTA:
            this.setEnabled(true);

            break;

          case modoMODIFICACION:
            pnlInv.setEnabled(false);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;

            // s�lo habilitado salir y grabar
          case modoBAJA:
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  }

  private boolean isDataValid() {
    if ( (pnlInv.getDatos() == null)) {
      getApp().showAdvise("Debe completar todos los campos obligatorios");
      return false;
    }
    else {
      return true;
    }
  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {

    final String servlet = "servlet/SrvQueryTool";
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        switch (modoOperacion) {

          // ----- NUEVO -----------

          case 0:

            Inicializar(CInicializar.ESPERA);

            if (ComprobarRepeticion()) {

              // Valores de los campos
              dtDevuelto.put("COD_USUARIO", pnlInv.getDatos().get("COD_USUARIO"));
              dtDevuelto.put("NOMBRE", pnlInv.getDatos().get("NOMBRE"));
              dtDevuelto.put("CD_ANO", dtParam.getString("CD_ANO"));
              dtDevuelto.put("NM_ALERBRO", dtParam.getString("NM_ALERBRO"));
              dtDevuelto.put("CD_ANO||'/'||NM_ALERBRO",
                             dtParam.getString("CD_ANO") + "/" +
                             dtParam.getString("NM_ALERBRO"));
              dtDevuelto.put("FC_ALTA", txtFecha.getText());

              dtDevuelto.put("TIPO_OPERACION", "A");
              bAceptar = true;
              dispose();
            }

            Inicializar(CInicializar.NORMAL);
            break;

          case 1:
            Inicializar(CInicializar.ESPERA);

            // Valores de los campos
            dtDevuelto.put("COD_USUARIO", pnlInv.getDatos().get("COD_USUARIO"));
            dtDevuelto.put("NOMBRE", pnlInv.getDatos().get("NOMBRE"));
            dtDevuelto.put("CD_ANO", dtParam.getString("CD_ANO"));
            dtDevuelto.put("NM_ALERBRO", dtParam.getString("NM_ALERBRO"));
            dtDevuelto.put("CD_ANO||'/'||NM_ALERBRO",
                           dtParam.getString("CD_ANO") + "/" +
                           dtParam.getString("NM_ALERBRO"));

            dtDevuelto.put("FC_ALTA", txtFecha.getText());

            String OpeAnt = dtDev.getString("TIPO_OPERACION");
            if (OpeAnt.equals("A")) {
              dtDevuelto.put("TIPO_OPERACION", "A");
            }
            else {
              dtDevuelto.put("TIPO_OPERACION", "M");
            }
            bAceptar = true;
            dispose();

            Inicializar(CInicializar.NORMAL);
            break;

          case 2:
            Inicializar(CInicializar.ESPERA);

            // Valores de los campos
            dtDevuelto.put("COD_USUARIO", pnlInv.getDatos().get("COD_USUARIO"));
            dtDevuelto.put("NOMBRE", pnlInv.getDatos().get("NOMBRE"));

            String OpeAntBaj = dtDev.getString("TIPO_OPERACION");
            if (OpeAntBaj.equals("M")) {
              dtDevuelto.put("TIPO_OPERACION", "M");
            }
            else {
              dtDevuelto.put("TIPO_OPERACION", "B");
            }

            bAceptar = true;
            dispose();

            Inicializar(CInicializar.NORMAL);
            break;
        }
      }
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  }

  // Comprobaci�n de la existencia de un registro con ya repetido
  private boolean ComprobarRepeticion() {
    Lista p = new Lista();
    Lista p1 = new Lista();

    try {

      QueryTool qt = new QueryTool();

      // tabla de familias de productos
      qt.putName("SIVE_INVESTIGADOR");
      // campos que se escriben
      qt.putType("CD_USUARIO", QueryTool.STRING);
      qt.putType("CD_ANO", QueryTool.STRING);
      qt.putType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putType("FC_ALTA", QueryTool.DATE);

      qt.putWhereType("CD_USUARIO", QueryTool.STRING);
      qt.putWhereValue("CD_USUARIO", pnlInv.getDatos().getString("COD_USUARIO"));
      qt.putOperator("CD_USUARIO", "=");
      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", dtParam.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");
      qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putWhereValue("NM_ALERBRO", dtParam.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");
      qt.putWhereType("CD_CA", QueryTool.STRING);
      qt.putWhereValue("CD_CA", applet.getParametro("CA"));
      qt.putOperator("CD_CA", "=");

      p.addElement(qt);

      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      p1 = (Lista)this.getApp().getStub().doPost(1, p);

    }
    catch (Exception ex) {
      ex.printStackTrace();
      this.getApp().trazaLog(ex);
    }

    if (p1.size() == 0) {
      return true;
    }
    else {
      this.getApp().showAdvise(
          "Ya existe este investigador para el brote, seleccione otro.");
      return false;
    }

  }

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

  // Devuelve los datos seleccionados en el di�logo.
  public Data devuelveData() {
    return dtDevuelto;
  }

// botones de aceptar y cancelar.

  class DialogActionAdapter
      implements java.awt.event.ActionListener, Runnable {
    DBDiaInves adaptee;
    ActionEvent e;

    DialogActionAdapter(DBDiaInves adaptee) {
      this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
      this.e = e;
      Thread th = new Thread(this);
      th.start();
    }

    public void run() {
      adaptee.btn_actionPerformed(e);
    }
  }
}
