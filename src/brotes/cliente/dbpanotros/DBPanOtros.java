/**
 * Clase: DBPanOtros
 * Hereda: CPanel
 * Autor: Pedro Antonio D�az (JMT)
 * Fecha Inicio: 23/05/2000
 * Analisis Funcional: Informe Final
 * Descripcion: Panel con los 10 mantenimientos perifericos a un brote
 */

package brotes.cliente.dbpanotros;

import java.util.Hashtable;

import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.CheckboxControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.agcausales.PanAgCausal;
import brotes.cliente.alibrote.PanAlibrote;
import brotes.cliente.ataquealim.DiaAtaqAlim;
import brotes.cliente.c_comuncliente.SincrEventos;
import brotes.cliente.diabif.DiaBIF;
import brotes.cliente.distredad.PanDisEdad;
import brotes.cliente.faccont.PanFaccont;
import brotes.cliente.factriesgo.DiaGestFactR;
import brotes.cliente.mantmuestras.DiaResumMuestras;
import brotes.cliente.medidasadop.PanMedAdop;
import brotes.cliente.sintomatologia.PanSint;
import brotes.cliente.tasasvacunables.PanTasVac;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPanel;
import comun.constantes;
import sapp2.Data;

public class DBPanOtros
    extends CPanel
    implements CInicializar {

  /*************** Datos propios del panel ****************/

  // Parametros de entrada
  CApp applet = null;
  DiaBIF dlg = null;
  Data brote = null;
  Hashtable listas = null;

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  // Constantes del modoEntrada
  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;

  // Nombres de servlets

  /****************** Componentes del panel ********************/

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Organizaci�n del panel
  private XYLayout xYLayout1 = new XYLayout();

  CheckboxGroup checkGroup = new CheckboxGroup();

  CheckboxControl checkAI = new CheckboxControl(); // Alimentos Implicados
  CheckboxControl checkSinto = new CheckboxControl(); // Sintomatolog�a
  CheckboxControl checkFC = new CheckboxControl(); // Factores Contribuyentes
  CheckboxControl checkAC = new CheckboxControl(); // Agentes Causales
  CheckboxControl checkMA = new CheckboxControl(); // Medidas
  CheckboxControl checkDistES = new CheckboxControl(); // Distr. Edad - Sexo
  CheckboxControl checkTAA = new CheckboxControl(); // Tasas de Ataque Alimentario
  CheckboxControl checkTAFR = new CheckboxControl(); // Factores Riesgo
  CheckboxControl checkTAEV = new CheckboxControl(); // Tasas Vacunables
  CheckboxControl checkRMB = new CheckboxControl(); // Resumen Muestras
  ButtonControl btnIrA = new ButtonControl();

  // Escuchadores (iniciados a null)

  // Botones
  DBPanOtrosActionAdapter actionAdapter = null;

  // Escuchadores (iniciados a null)
  // Constructor
  // @param a: CApp
  // @param dlg: Dialogo
  // @param modo: modo de entrada al di�logo ()
  // @param hash: lista de listas
  public DBPanOtros(CApp a, DiaBIF d, Data bro, int modo, Hashtable hash) {
    super(a);
    applet = a;
    dlg = d;
    brote = bro;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    listas = hash;

    try {
      // Iniciaci�n
      jbInit();
      Inicializar(modoOperacion);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicia el aspecto del panel superior del informe final
  public void jbInit() throws Exception {

    // Carga de imagenes
    getApp().getLibImagenes().put(constantes.imgACEPTAR);
    getApp().getLibImagenes().CargaImagenes();

    // Escuchadores: botones y presionado de teclado
    actionAdapter = new DBPanOtrosActionAdapter(this);

    // Organizacion del panel
    this.setSize(new Dimension(735, 270));
    this.setLayout(xYLayout1);
    xYLayout1.setWidth(735);
    xYLayout1.setHeight(270);

    btnIrA.setImage(getApp().getLibImagenes().get(constantes.imgACEPTAR));
    btnIrA.setLabel("Ir a ...");
    btnIrA.setActionCommand("ir a ...");
    btnIrA.addActionListener(actionAdapter);

    checkDistES.setCheckboxGroup(checkGroup);
    checkDistES.setLabel("Distribucion por grupos de edad y sexo");
    checkSinto.setCheckboxGroup(checkGroup);
    checkSinto.setLabel("Sintomatologia");
    checkTAA.setCheckboxGroup(checkGroup);
    checkTAA.setLabel("Tasa de ataque por alimentos");
    checkTAFR.setCheckboxGroup(checkGroup);
    checkTAFR.setLabel("Tasa de ataque por factor de riesgo");
    checkAI.setCheckboxGroup(checkGroup);
    checkAI.setLabel("Alimento Implicado");
    checkTAEV.setCheckboxGroup(checkGroup);
    checkTAEV.setLabel("Tasa de ataque enf. vacunables");
    checkRMB.setCheckboxGroup(checkGroup);
    checkRMB.setLabel("Resumen Muestras del Brote");
    checkAC.setCheckboxGroup(checkGroup);
    checkAC.setLabel("Agentes Causales");
    checkMA.setCheckboxGroup(checkGroup);
    checkMA.setLabel("Medidas Adoptadas");
    checkFC.setCheckboxGroup(checkGroup);
    checkFC.setLabel("Factores Contribuyentes");

    this.add(checkDistES, new XYConstraints(20, 30, 249, -1));
    this.add(checkTAA, new XYConstraints(20, 65, 225, -1));
    this.add(checkTAEV, new XYConstraints(20, 100, 248, -1));
    this.add(checkRMB, new XYConstraints(20, 135, 225, -1));
    this.add(checkMA, new XYConstraints(20, 170, 232, -1));
    this.add(checkSinto, new XYConstraints(320, 30, 177, -1));
    this.add(checkTAFR, new XYConstraints(320, 65, -1, -1));
    this.add(checkAI, new XYConstraints(320, 100, 137, -1));
    this.add(checkFC, new XYConstraints(320, 135, 170, -1));
    this.add(checkAC, new XYConstraints(320, 170, 180, -1));

    this.add(btnIrA, new XYConstraints(320, 220, 80, -1));

  } // Fin jbInit()

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    if (dlg != null) {
      dlg.Inicializar(modoOperacion);
    }
  } // Fin Iniciar()

  public void InicializarDesdeFuera(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicia()

  public void Inicializar() {
    switch (modoOperacion) {
      // Modo espera
      case CInicializar.ESPERA:
        enableControls(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // Modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case ALTA:
          case MODIFICACION:
            enableControls(true);
            break;
          case BAJA:
          case CONSULTA:
            enableControls(true);
            break;
        }
        if (brote.getString("CD_GRUPO").equals("0")) {
          checkTAEV.setEnabled(false);
        }
        else {
          checkTAEV.setEnabled(true);
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    } // Fin switch
  } // Fin Iniciar()

  public void enableControls(boolean state) {
    checkAI.setEnabled(state);
    checkSinto.setEnabled(state);
    checkFC.setEnabled(state);
    checkAC.setEnabled(state);
    checkMA.setEnabled(state);
    checkDistES.setEnabled(state);
    checkTAA.setEnabled(state);
    checkTAFR.setEnabled(state);
    checkTAEV.setEnabled(state);
    checkRMB.setEnabled(state);
    btnIrA.setEnabled(state);
  } // Fin enableControls()

  /********** Sincronizador de Eventos ******************/

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  /****************** Funciones Auxiliares *****************/

  public void cambiarBIF(Data newBIF) {
    brote = newBIF;
  } // Fin cambiarAlerta()

  public void setModoEntrada(int m) {
    modoEntrada = m;
  } // Fin setModoEntrada()

  /****************** Manejadores de Eventos *****************/
  void btnIrAActionPerformed() {
    if (checkDistES.isChecked()) {
      DistribucionEdadSexo();
    }
    else if (checkSinto.isChecked()) {
      Sintomatologia();
    }
    else if (checkTAA.isChecked()) {
      TasaAtaqueAlimentos();
    }
    else if (checkTAFR.isChecked()) {
      TasaAtaqueFactorRiesgo();
    }
    else if (checkAI.isChecked()) {
      AlimentoImplicado();
    }
    else if (checkTAEV.isChecked()) {
      TasaAtaqueEnfermedadesVacunables();
    }
    else if (checkRMB.isChecked()) {
      ResumenMuestrasBrote();
    }
    else if (checkAC.isChecked()) {
      AgentesCausales();
    }
    else if (checkMA.isChecked()) {
      MedidasAdoptadas();
    }
    else if (checkFC.isChecked()) {
      FactoresContribuyentes();
    }
  } // Fin btnAreaActionPerformed()

  private void DistribucionEdadSexo() {
    // Llamada al dialogo
    PanDisEdad dlg = null;
    dlg = new PanDisEdad(this.getApp(),
                         modoEntrada,
                         brote);
    dlg.show();
    dlg = null;

    // Ya se establece el nuevo CD_OPE/FC_ULTACT en el mto.
  }

  private void TasaAtaqueAlimentos() {
    // Llamada al dialogo
    DiaAtaqAlim dlg = null;
    dlg = new DiaAtaqAlim(this.getApp(),
                          modoEntrada,
                          brote);
    dlg.show();
    dlg = null;

    // Ya se establece el nuevo CD_OPE/FC_ULTACT en el mto.
  }

  private void TasaAtaqueEnfermedadesVacunables() {
    // Llamada al dialogo
    PanTasVac dlg = null;
    dlg = new PanTasVac(this.getApp(),
                        modoEntrada,
                        brote);
    dlg.show();
    dlg = null;

    // Ya se establece el nuevo CD_OPE/FC_ULTACT en el mto.
  }

  private void Sintomatologia() {

    // Llamada al dialogo
    PanSint dlg = null;
    dlg = new PanSint(this.getApp(),
                      modoEntrada,
                      brote);
    dlg.show();
    dlg = null;

    // Ya se establece el nuevo CD_OPE/FC_ULTACT en el mto.
  }

  private void TasaAtaqueFactorRiesgo() {

    // Llamada al dialogo
    DiaGestFactR dlg = null;
    dlg = new DiaGestFactR(this.getApp(),
                           modoEntrada,
                           brote);
    dlg.show();
    dlg = null;

    // Ya se establece el nuevo CD_OPE/FC_ULTACT en el mto.
  } // Fin TasaAtaqueFactorRiesgo()

  private void ResumenMuestrasBrote() {

    // Llamada al dialogo
    DiaResumMuestras dlg = null;
    dlg = new DiaResumMuestras(this.getApp(),
                               modoEntrada,
                               brote);
    dlg.show();
    dlg = null;

    // Ya se establece el nuevo CD_OPE/FC_ULTACT en el mto.
  } // Fin ResumenMuestrasBrote()

  private void AlimentoImplicado() {

    // Llamada al dialogo
    PanAlibrote dlg = null;
    dlg = new PanAlibrote(this.getApp(),
                          modoEntrada,
                          brote);
    dlg.show();
    dlg = null;

    // Ya se establece el nuevo CD_OPE/FC_ULTACT en el mto.
  }

  private void FactoresContribuyentes() {

    // Llamada al dialogo
    PanFaccont dlg = null;
    dlg = new PanFaccont(this.getApp(),
                         modoEntrada,
                         brote);
    dlg.show();
    dlg = null;

    // Ya se establece el nuevo CD_OPE/FC_ULTACT en el mto.
  }

  private void AgentesCausales() {

    // Llamada al dialogo
    PanAgCausal dlg = null;
    dlg = new PanAgCausal(this.getApp(),
                          modoEntrada,
                          brote);
    dlg.show();
    dlg = null;

    // Ya se establece el nuevo CD_OPE/FC_ULTACT en el mto.
  }

  private void MedidasAdoptadas() {

    // Llamada al dialogo
    PanMedAdop dlg = null;
    dlg = new PanMedAdop(this.getApp(),
                         modoEntrada,
                         brote);
    dlg.show();
    dlg = null;

    // Ya se establece el nuevo CD_OPE/FC_ULTACT en el mto.
  }

} // Fin DBPanOtros

// Botones
class DBPanOtrosActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DBPanOtros adaptee;
  ActionEvent evt;

  DBPanOtrosActionAdapter(DBPanOtros adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if (evt.getActionCommand().equals("ir a ...")) {
        adaptee.btnIrAActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  DAPanSupActionAdapter
