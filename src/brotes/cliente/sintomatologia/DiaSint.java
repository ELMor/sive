/**
 * Clase: DiaSint
 * Paquete: brotes.cliente.diasint
 * Hereda: CDialog
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 09/12/1999
 * Analisis Funcional: Punto 2. Mantenimiento Brotes.
 * Descripcion: Implementacion del dialogo que permite dar de alta
 *   baja, modificar y consultar sintomas de un brote.
 */
/**
 * Panel a trav�s del que se realizan los mantenimientos
 * de sintomatolog�a de ataque de forma masiva.
 * @autor PDP 05/05/2000
 * @version 1.1
 */

package brotes.cliente.sintomatologia;

import java.util.Vector;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.datos.pansint.DatSintSC;
import capp2.CApp;
import capp2.CCargadorImagen;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CListaValores;
import comun.DataGeneralCDDS;
import comun.UtilEDO;
import comun.constantes;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.StubSrvBD;

public class DiaSint
    extends CDialog {

  // Indicador de validez del codigo del sintoma
  protected boolean bSintomaValid = false;

  // Mensaje en caso de bloqueo
  private final String DATOS_BLOQUEADOS =
      "Los datos han sido modificados. �Desea sobrescribirlos?";

  // Vars. de gestion del boton de sintomas
  sapp.StubSrvBD stubCliente = null;
  final String strSERVLET_LSINT = "servlet/SrvLSint";

  // Estado: indica si se ha llevado a cabo correctamente la operacion deseada
  public boolean accionOK = false;
  public DatSintSC sintNuevo = null;

  // Localizacion del servlet
  final String strSERVLET_DIASINT = "servlet/SrvDiaSint";

  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;
  final public int modoCONSULTA = 3;
  final public int modoESPERA = 4;
  final public int modoNORMAL = 5;

  // Variable estado del panel
  protected int modoOperacion = 0;
  protected int modoEntrada = 0;

  //PsAppSint PsAppDlg = null;

  // Sincronizaci�n de eventos
  protected boolean sinBloquear = true;
  protected int modoAnterior;
  ;

  //Datos del s�ntoma obtenidos de ls CListaValores
  private sapp2.Data dtSintoma = null;

  // parametros pasados al di�logo
  public sapp2.Data dtRegSubT = new sapp2.Data();

  public sapp2.Data dtReg = new sapp2.Data();

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  // Datos del Brote
  //DatSintCS diaBrote = null;
  sapp2.Data diaBrote = null;

  // Datos del sintoma a modificar
  //DatSintSC SintInicial = null;
  sapp2.Data SintInicial = null;

  // Sintoma establecido con boton o perdida de foco de Sintoma
  DataGeneralCDDS sintoma = null;

  // Imagenes
  protected CCargadorImagen imgs = null;
  final String imgNAME[] = {
      constantes.imgACEPTAR,
      constantes.imgCANCELAR,
      constantes.imgLUPA};

  /****************** Componentes del panel ********************/

  // Organizacion del panel
  private XYLayout xYLayout = new XYLayout();

  // Sintoma
  private Label lblSintoma = new Label("Sintoma:");
  private capp.CCampoCodigo txtSintoma = new capp.CCampoCodigo();
  private ButtonControl btnSintoma = new ButtonControl();
  private TextField txtDescSintoma = new TextField();

  // Informacion Adicional
  private Label lblMasInfo = new Label("Informaci�n Adicional: ");
  private Checkbox chkMasInfo = new Checkbox();
  private boolean bMasInfo = false;
  private TextArea txtaMasInfo = new TextArea("", 2, 30,
                                              TextArea.SCROLLBARS_VERTICAL_ONLY);

  // Numero de Casos
  private Label lblCasos = new Label("Num. Casos: ");
  private TextField txtCasos = new TextField();

  // Botones de aceptar y cancelar
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores
  DiaSintActionAdapter actionAdapter = null;
  DiaTextAdapter textAdapter = null;
  DiaFocusAdapter focusAdapter = null;

  // Constructor
  // CApp sera luego el panel del que procede: PanSint
  // @param brote: datos de un brote  (cdope, fc_ultact del brote)
  // @param sint: lleva los datos de un sintoma en caso de alta/modificaci�n/baja/consulta
  public DiaSint(CApp a, int modo, sapp2.Data brote, sapp2.Data sint) {
    super(a);
    setTitle("Brotes: S�ntomas");
    try {
      // Variables globales
      diaBrote = brote;
      SintInicial = sint;

      //PsAppDlg = PsApp;

      // Inicializacion
      modoOperacion = modo;
      modoEntrada = modo;
      jbInit();

      if (modoEntrada == modoMODIFICACION ||
          modoEntrada == modoBAJA ||
          modoEntrada == constantes.modoCONSULTA) {
        rellenarDatos(SintInicial);
        txtSintomaFocusLost();
      }

      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    // Organizacion del panel
    this.setSize(new Dimension(535, 205));
    xYLayout.setHeight(205);
    xYLayout.setWidth(535);
    this.setLayout(xYLayout);

    // Carga de las im�genes
    imgs = new CCargadorImagen(getApp(), imgNAME);
    imgs.CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaSintActionAdapter(this);
    textAdapter = new DiaTextAdapter(this);
    focusAdapter = new DiaFocusAdapter(this);

    // Sintomas
    this.add(lblSintoma, new XYConstraints(15, 15, 48, 25));
    this.add(txtSintoma, new XYConstraints(15 + 48 + 10, 15, 45, 25));
    txtSintoma.addKeyListener(textAdapter);
    txtSintoma.addFocusListener(focusAdapter);
    txtSintoma.setName("Sintoma");
    txtSintoma.setBackground(new Color(255, 255, 150));
    this.add(btnSintoma, new XYConstraints(15 + 48 + 10 + 45 + 10, 15, 25, 25));
    btnSintoma.setImage(imgs.getImage(2));
    btnSintoma.addActionListener(actionAdapter);
    btnSintoma.setActionCommand("Sintomas");
    this.add(txtDescSintoma,
             new XYConstraints(15 + 48 + 10 + 45 + 10 + 25 + 10, 15, 350, 25));
    txtDescSintoma.setEnabled(false);
    txtDescSintoma.setEditable(false);

    // Informacion Adicional
    this.add(lblMasInfo, new XYConstraints(15, 50, 120, 25));
    this.add(chkMasInfo, new XYConstraints(15 + 120 + 10, 50, 25, 25));
    this.add(txtaMasInfo,
             new XYConstraints(15 + 120 + 10 + 25 + 10, 50, 333, 60));
    chkMasInfo.addItemListener(new DiaSintChkMasInfoItemAdapter(this));

    // Numero de Casos
    this.add(lblCasos, new XYConstraints(15, 85, 75, 25));
    this.add(txtCasos, new XYConstraints(15 + 75 + 10, 85, 60, 25));

    // Boton de Aceptar
    btnAceptar.setImage(imgs.getImage(0));
    btnAceptar.setLabel("Aceptar");
    this.add(btnAceptar, new XYConstraints(513 - 80 - 10 - 80, 100 + 35, 80, -1));
    if (modoOperacion == constantes.modoCONSULTA) {
      btnAceptar.setEnabled(false);
    }
    else {
      btnAceptar.setActionCommand("Aceptar");
      btnAceptar.addActionListener(actionAdapter);
    }

    // Boton de Cancelar
    btnCancelar.setImage(imgs.getImage(1));
    btnCancelar.setLabel("Cancelar");
    this.add(btnCancelar, new XYConstraints(513 - 80, 100 + 35, 80, -1));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);
  } // Fin jbInit()

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {
    switch (modoOperacion) {
      case modoESPERA:
        chkMasInfo.setEnabled(false);
        txtaMasInfo.setEnabled(false);
        enableControls(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

      case modoALTA:

      case modoMODIFICACION:
        if (bMasInfo) {
          chkMasInfo.setEnabled(true);
          txtaMasInfo.setEnabled(true);
        }
        else {
          chkMasInfo.setEnabled(false);
          chkMasInfo.setState(false);
          txtaMasInfo.setEnabled(false);
        }

        if (chkMasInfo.getState()) {
          txtaMasInfo.setVisible(true);
        }
        else {
          txtaMasInfo.setVisible(false);

        }
        enableControls(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
        chkMasInfo.setEnabled(false);
        txtaMasInfo.setEnabled(false);

        if (chkMasInfo.getState()) {
          txtaMasInfo.setVisible(true);
        }
        else {
          txtaMasInfo.setVisible(false);

        }
        enableControls(false);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
      case modoCONSULTA:
        chkMasInfo.setEnabled(false);
        txtaMasInfo.setEnabled(false);
        if (chkMasInfo.getState()) {
          txtaMasInfo.setVisible(true);
        }
        else {
          txtaMasInfo.setVisible(false);

        }
        enableControls(false);

        // Botones Aceptar/Cancelar
        btnAceptar.setEnabled(true);
        if (modoEntrada == constantes.modoCONSULTA) {
          btnAceptar.setEnabled(false);
        }
        btnCancelar.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
    this.doLayout();
  } // Fin Inicializar()

  private void enableControls(boolean en) {
    // Sintoma
    if (modoOperacion == modoMODIFICACION) {
      txtSintoma.setEnabled(false);
      btnSintoma.setEnabled(false);
    }
    else {
      txtSintoma.setEnabled(en);
      btnSintoma.setEnabled(en);
    }

    txtDescSintoma.setEnabled(false);

    // Informacion Adicional: tratado en Inicializar()

    // Casos
    txtCasos.setEnabled(en);
  } // Fin enableControls()

  public boolean validarDatos() {
    // 1. PERDIDAS DE FOCO: No hay campos CD-Button-DS
    if (!bSintomaValid) {
      this.getApp().showAdvise("Sintoma no v�lido");
      txtSintoma.setText("");
      txtSintoma.requestFocus();
      return false;
    }

    // 2. LONGITUDES

    // txtSintoma
    if (txtSintoma.getText().length() > 4) {
      this.getApp().showAdvise(
          "La longitud del c�digo del s�ntoma excede de 4 caracteres");
      txtSintoma.setText("");
      txtSintoma.requestFocus();
      return false;
    }

    // txtaMasInfo
    if (txtaMasInfo.getText().length() > 40) {
      this.getApp().showAdvise(
          "La longitud del texto de Informaci�n Adicional excede de 40 caracteres");
      txtaMasInfo.setText("");
      txtaMasInfo.requestFocus();
      return false;
    }

    // txtCasos
    if (txtCasos.getText().length() > 4) {
      this.getApp().showAdvise(
          "La longitud del numero de casos excede de 5 digitos");
      txtCasos.setText("");
      txtCasos.requestFocus();
      return false;
    }

    // 3. DATOS NUMERICOS:

    // NumCasos
    if (!txtCasos.getText().equals("")) {
      boolean b = UtilEDO.isNum(txtCasos.getText());
      if (!b) {
        this.getApp().showAdvise(
            "El Numero de Casos debe ser un valor Entero Positivo");
        txtCasos.setText("");
        txtCasos.requestFocus();
        return false;
      }
    }

    // 4. CAMPOS OBLIGATORIOS:

    // txtSintoma
    if (txtSintoma.getText().length() <= 0) {
      this.getApp().showAdvise("El c�digo del s�ntoma es un campo obligatorio");
      txtSintoma.requestFocus();
      return false;
    }

    // Si llega aqui todo ha ido bien
    return true;
  } // Fin validarDatos()

  public void rellenarDatos(sapp2.Data datSint) {
    // Sintoma
    txtSintoma.setText(datSint.getString("CD_SINTOMA"));
    txtDescSintoma.setText(datSint.getString("DS_SINTOMA"));

    // Informacion Adicional
    if (datSint.getString("IT_MASINF").equals("S")) {
      chkMasInfo.setState(true);
      txtaMasInfo.setText(datSint.getString("DS_MASINF"));
    }
    else {
      chkMasInfo.setEnabled(false);

      // Numero de Casos
    }
    txtCasos.setText(datSint.getString("NM_CASOS"));

  } // Fin rellenarDatos()

  // guarda un Sintoma
  public void setSintoma(sapp2.Data dt) {
    this.dtSintoma = dt;
    txtSintoma.setText( (String) dt.get("CD_SINTOMA"));
    txtDescSintoma.setText( (String) dt.get("DS_SINTOMA"));
    if (dt.get("IT_MASINF").equals("S")) {
      bMasInfo = true;
    }
    else {
      bMasInfo = false;
    }
  }

  // Compara si los valores de 2 DatCasTratSC son iguales (true)
  private boolean compararCampos(DatSintSC a, DatSintSC b) {
    if (a == null || b == null) {
      return false;
    }

    if (!a.getCD_SINTOMA().equals(b.getCD_SINTOMA())) {
      return false;
    }

    if (!a.getDS_SINTOMA().equals(b.getDS_SINTOMA())) {
      return false;
    }

    if (!a.getIT_MASINF().equals(b.getIT_MASINF())) {
      return false;
    }

    if (!a.getDS_MASINF().equals(b.getDS_MASINF())) {
      return false;
    }

    if (!a.getNM_CASOS().equals(b.getNM_CASOS())) {
      return false;
    }

    return true;
  } // Fin  compararCampos()

  // Bloquear eventos
  /** Este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized boolean bloquea() {
    if (sinBloquear) {
      // No hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modoOperacion;
      modoOperacion = modoESPERA;
      Inicializar();

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // Ya est� bloqueado
      return false;
    }
  } // Fin bloquea()

  /** Este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    modoOperacion = modoAnterior;
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  } // Fin desbloquea()

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

  // Devuelve los datos seleccionados en el di�logo.
  public sapp2.Data devuelveData() {
    return dtReg;
  }

  // Comprobaci�n de la existencia de un registro con ya repetido
  private boolean ComprobarRepeticion() {
    Lista p = new Lista();
    Lista p1 = new Lista();

    try {

      QueryTool qt = new QueryTool();

      // tabla de familias de productos
      qt.putName("SIVE_SINTOMAS_BROTE");
      // campos que se escriben
      qt.putType("NM_CASOS", QueryTool.INTEGER);

      qt.putWhereType("CD_SINTOMA", QueryTool.STRING);
      qt.putWhereValue("CD_SINTOMA", txtSintoma.getText());
      qt.putOperator("CD_SINTOMA", "=");
      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", diaBrote.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");
      qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putWhereValue("NM_ALERBRO", diaBrote.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");

      p.addElement(qt);

      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      p1 = (Lista)this.getApp().getStub().doPost(1, p);

    }
    catch (Exception ex) {
      ex.printStackTrace();
      this.getApp().trazaLog(ex);
    }

    if (p1.size() == 0) {
      return true;
    }
    else {
      this.getApp().showAdvise("Este s�ntoma ya existe, seleccione otro.");
      return false;
    }

  }

  /******************** Manejadores *************************/

  void btnAceptarActionPerformed() {

    if (validarDatos()) {
      switch (modoEntrada) {
        case modoALTA:

          Inicializar(modoESPERA);
          if (ComprobarRepeticion()) {
            dtReg = new sapp2.Data();
            dtReg.put("CD_ANO", diaBrote.get("CD_ANO"));
            dtReg.put("NM_ALERBRO", diaBrote.get("NM_ALERBRO"));
            dtReg.put("CD_SINTOMA", txtSintoma.getText());
            dtReg.put("DS_SINTOMA", txtDescSintoma.getText());
            dtReg.put("CD_SINTOMA||'-'||DS_SINTOMA",
                      txtSintoma.getText() + "-" + txtDescSintoma.getText());
            if (chkMasInfo.getState() == true) {
              dtReg.put("IT_MASINF", "S");
            }
            else {
              dtReg.put("IT_MASINF", "N");
            }
            dtReg.put("DS_MASINF", txtaMasInfo.getText());
            dtReg.put("NM_CASOS", txtCasos.getText());
            dtReg.put("TIPO_OPERACION", "A");

            bAceptar = true;
            dispose();
          }
          Inicializar(modoNORMAL);

          break;

        case modoMODIFICACION:

          Inicializar(modoESPERA);
          dtReg = new sapp2.Data();
          dtReg.put("CD_ANO", diaBrote.get("CD_ANO"));
          dtReg.put("NM_ALERBRO", diaBrote.get("NM_ALERBRO"));
          dtReg.put("CD_SINTOMA", txtSintoma.getText());
          dtReg.put("DS_SINTOMA", txtDescSintoma.getText());
          dtReg.put("CD_SINTOMA||'-'||DS_SINTOMA",
                    txtSintoma.getText() + "-" + txtDescSintoma.getText());
          if (chkMasInfo.getState() == true) {
            dtReg.put("IT_MASINF", "S");
          }
          else {
            dtReg.put("IT_MASINF", "N");
          }
          dtReg.put("DS_MASINF", txtaMasInfo.getText());
          dtReg.put("NM_CASOS", txtCasos.getText());

          String OpeAnt = SintInicial.getString("TIPO_OPERACION");
          if (OpeAnt.equals("A")) {
            dtReg.put("TIPO_OPERACION", "A");
          }
          else {
            dtReg.put("TIPO_OPERACION", "M");
          }

          bAceptar = true;
          dispose();
          Inicializar(modoNORMAL);
          break;

        case modoBAJA:

          Inicializar(modoESPERA);
          dtReg = new sapp2.Data();
          dtReg.put("CD_ANO", diaBrote.get("CD_ANO"));
          dtReg.put("NM_ALERBRO", diaBrote.get("NM_ALERBRO"));
          dtReg.put("CD_SINTOMA", txtSintoma.getText());
          dtReg.put("DS_SINTOMA", txtDescSintoma.getText());
          dtReg.put("CD_SINTOMA||'-'||DS_SINTOMA",
                    txtSintoma.getText() + "-" + txtDescSintoma.getText());
          if (chkMasInfo.getState() == true) {
            dtReg.put("IT_MASINF", "S");
          }
          else {
            dtReg.put("IT_MASINF", "N");
          }
          dtReg.put("DS_MASINF", txtaMasInfo.getText());
          dtReg.put("NM_CASOS", txtCasos.getText());

          String OpeAntBaj = SintInicial.getString("TIPO_OPERACION");
          if (OpeAntBaj.equals("M")) {
            dtReg.put("TIPO_OPERACION", "M");
          }
          else {
            dtReg.put("TIPO_OPERACION", "B");
          }

          //dtReg.put("TIPO_OPERACION","B");
          bAceptar = true;
          dispose();
          Inicializar(modoNORMAL);

          break;
      } // Fin switch
    } // Fin if validar datos
  } // Fin btnAceptarActionPerformed()

  void btnCancelarActionPerformed() {
    dispose();
  } // Fin btnCancelarActionPerformed()

  void btnSintomaActionPerformed() {
    CListaValores clv = null;
    QueryTool qt = null;
    Vector v = null;

    Inicializar(CInicializar.ESPERA);

    // configura el lector de centros
    qt = new QueryTool();

    // centros de vacunacion
    qt.putName("SIVE_SINTOMAS");

    // campos que se leen
    qt.putType("CD_SINTOMA", QueryTool.STRING);
    qt.putType("DS_SINTOMA", QueryTool.STRING);
    qt.putType("IT_MASINF", QueryTool.STRING);

    // campos que se muestran en la lista de valores
    v = new Vector();
    v.addElement("CD_SINTOMA");
    v.addElement("DS_SINTOMA");

    // lista de valores
    clv = new CListaValores(this.getApp(),
                            "Lista de S�ntomas",
                            qt,
                            v);
    clv.show();

    // recupera el s�ntoma seleccionado
    if (clv.getSelected() != null) {
      setSintoma(clv.getSelected());
      bSintomaValid = true;
    }

    clv = null;

  } // Fin btnSintomaActionPerformed

  void chkMasInfoItemStateChanged(ItemEvent e) {
    // Para que sea inicializado el dialogo
    txtaMasInfo.setText("");
  }

  void txtSintomaKeyPressed() {
    txtDescSintoma.setText("");
    bSintomaValid = false;
  } // Fin txtSintomaKeyPressed()

  void txtSintomaFocusLost() {

    DataGeneralCDDS data;

    // Si ya es valido el codigo se deja como esta
    if (bSintomaValid) {
      return;
    }

    // Si es vacio el codigo, se establece a invalido el bool
    if (txtSintoma.getText().trim().equals("")) {
      bSintomaValid = false;
      return;
    }

    // Comprueba que los datos introducidos son v�lidos si no se han
    // seleccionado de los popup
    capp.CLista listaSalida = null;
    data = new DataGeneralCDDS("", txtSintoma.getText().trim(), "");

    capp.CLista parametros = new capp.CLista();
    parametros.addElement(data);

    try {

      stubCliente = new sapp.StubSrvBD();
      stubCliente.setUrl(new java.net.URL(app.getParametro("URL_SERVLET") +
                                          strSERVLET_LSINT));
      listaSalida = (capp.CLista) stubCliente.doPost(constantes.
          sOBTENER_X_CODIGO, parametros);

      if (listaSalida.size() == 0) {
        this.getApp().showAdvise("No hay datos con el criterio informado.");
        txtSintoma.setText("");
        bSintomaValid = false;
        return;
      }
    }
    catch (Exception e) { // Acceso a base de datos erroneo
      e.printStackTrace();
      //comun.ShowWarning(this.getCApp(),e.getMessage());
      bSintomaValid = false;
      return;
    } // Fin try

    // Si todo ha ido bien
    data = (DataGeneralCDDS) listaSalida.elementAt(0);
    sintoma = data;
    txtSintoma.setText(data.getCD().trim());
    txtDescSintoma.setText(data.getDS().trim());
    if (data.getCDpadre().equals("S")) {
      bMasInfo = true;
    }
    else {
      bMasInfo = false;

      // Si no ha salido por ninguna de las condiciones anteriores,
      // el sintoma es v�lido, y se pone su boolean a true
    }
    bSintomaValid = true;

  } // Fin txtSintomaFocusLost()

} // endclass DiaSint

/******************* ESCUCHADORES **********************/

// Botones
class DiaSintActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaSint adaptee;
  ActionEvent evt;

  DiaSintActionAdapter(DiaSint adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    if (adaptee.bloquea()) {
      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Sintomas")) {
        adaptee.btnSintomaActionPerformed();
      }
      adaptee.desbloquea();
    }
  }
} // Fin clase DiaSintActionAdapter

class DiaSintChkMasInfoItemAdapter
    implements java.awt.event.ItemListener {
  DiaSint adaptee;

  DiaSintChkMasInfoItemAdapter(DiaSint adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    if (adaptee.bloquea()) {
      adaptee.chkMasInfoItemStateChanged(e);
      adaptee.desbloquea();
    }
  }
} // Fin clase DiaSintChkMasInfoItemAdapter

// Escuchador para las cajas de texto
class DiaTextAdapter
    extends java.awt.event.KeyAdapter {
  DiaSint adaptee;

  DiaTextAdapter(DiaSint adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    if (adaptee.bloquea()) {
      if ( ( (TextField) e.getSource()).getName().equals("Sintoma")) {
        adaptee.txtSintomaKeyPressed();
      }
      adaptee.desbloquea();
    }
  }
} // Fin clase DiaTextAdapter

// Perdida de foco de las cajas
class DiaFocusAdapter
    implements java.awt.event.FocusListener, Runnable {
  DiaSint adaptee;
  FocusEvent evt;

  DiaFocusAdapter(DiaSint adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void focusGained(FocusEvent e) {

  }

  public void run() {
    if (adaptee.bloquea()) {
      if ( ( (TextField) evt.getSource()).getName().equals("Sintoma")) {
        adaptee.txtSintomaFocusLost();
      }
      adaptee.desbloquea();
    }
  }
} // Fin clase DiaFocusAdapter
