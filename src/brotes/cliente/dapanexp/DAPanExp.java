package brotes.cliente.dapanexp;

import java.util.Hashtable;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.TextField;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.CHora;
import brotes.cliente.c_componentes.DatoPeriferico;
import brotes.cliente.c_comuncliente.SincrEventos;
import brotes.cliente.diaalerta.DiaAlerta;
import brotes.cliente.pannivelesbro.PanNivelesBro;
import brotes.cliente.suca.Suca;
import capp2.CApp;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CPanel;
import capp2.CTexto;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;

public class DAPanExp
    extends CPanel
    implements CInicializar, DatoPeriferico {

  /*************** Datos propios del panel ****************/

  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;
  final int ESPERA = 4;
  final int NORMAL = 5;

  //suca
  Suca pansuca = null;
  Hashtable listas = null;

  // Alerta inicial
  Data alerta = null;

  //Listas de Datas de entrada a Suca para convertirlas en CLista
  Lista listaPaises = null;
  Lista listaCA = null;

  //tipo notificador
  Lista listaTNotif = null;

  //niveles
  PanNivelesBro panNiv = null;

  //fechas y horas
  fechas.CFecha fechaExp = new fechas.CFecha("N");
  fechas.CFecha fechaSin = new fechas.CFecha("N");
  CHora horaExp = new CHora("N");
  CHora horaSin = new CHora("N");

  //para la relacion situacion-fecha
  String sitIni = "";
  String fechaIni = "";

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  /****************** Componentes del panel ********************/
  XYLayout xYLayout = new XYLayout();

  XYLayout xYLayout1 = new XYLayout();
  CApp applet = null;
  DiaAlerta dlg = null;

  XYLayout xYLayout4 = new XYLayout();
  Label lExp = new Label();
  CEntero txtExp = new CEntero(5);

  Label lEnf = new Label();
  CEntero txtEnf = new CEntero(5);
  Label lIngre = new Label();
  CTexto txtIngre = new CTexto(50);
  Label lFecExp = new Label();
  Label lFecSin = new Label();
  Label lLugarExp = new Label();
  TextField txtLugarExp = new TextField();
  Label lTel = new Label();
  CTexto txtTel = new CTexto(14);

  SincrEventos scr = new SincrEventos();

  // constructor
  public DAPanExp(CApp a,
                  DiaAlerta d,
                  Data al,
                  int modo,
                  Hashtable hsCompleta) {
    super(a);
    applet = a;
    dlg = d;
    alerta = al;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    listas = hsCompleta;

    try {
      // Recuperacion de listas
      listaPaises = (Lista) hsCompleta.get("PAISES");
      listaCA = (Lista) hsCompleta.get("CA");
      listaTNotif = (Lista) hsCompleta.get("TNOTIFICADOR");

      jbInit();
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(755, 280));

    xYLayout.setHeight(270); //(280);
    xYLayout.setWidth(725); //(745);

    //Panel Suca
    pansuca = new Suca(applet, this, scr, modoEntrada, listas);

    //Panel Niveles
    panNiv = new PanNivelesBro(applet, this, modoEntrada, scr);

    //nombres
    /*fechaExp.setName("fechaExp");
         fechaSin.setName("fechaSin");
         horaExp.setName("horaExp");
         horaSin.setName("horaSin");
     */
    /*//focus
        focusAdap = new focusAdapter(this);
        focusAdapHora = new focusAdapterHora(this);*/

    lExp.setForeground(Color.black);
    lExp.setFont(new Font("Dialog", 0, 12));
    lExp.setText("Expuestos");
    lEnf.setForeground(Color.black);
    lEnf.setFont(new Font("Dialog", 0, 12));
    lEnf.setText("Enfermos");
    lIngre.setForeground(Color.black);
    lIngre.setFont(new Font("Dialog", 0, 12));
    lIngre.setText("Ingresos Hospitalarios");
    lFecExp.setForeground(Color.black);
    lFecExp.setFont(new Font("Dialog", 0, 12));
    lFecExp.setText("Fecha y hora de exposicion");
    lFecSin.setForeground(Color.black);
    lFecSin.setFont(new Font("Dialog", 0, 12));
    lFecSin.setText("Fecha y hora de inicio sintomas");
    lLugarExp.setForeground(Color.black);
    lLugarExp.setFont(new Font("Dialog", 0, 12));
    lLugarExp.setText("Lugar de exposicion");
    lTel.setForeground(Color.black);
    lTel.setFont(new Font("Dialog", 0, 12));
    lTel.setText("Telefono");
    this.setLayout(xYLayout);

    this.add(lExp, new XYConstraints(13, 12, 75, -1));
    this.add(txtExp, new XYConstraints(90, 12, 51, -1));
    this.add(lEnf, new XYConstraints(164, 12, 62, -1));
    this.add(txtEnf, new XYConstraints(231, 12, 51, -1));
    this.add(lIngre, new XYConstraints(292, 12, 134, -1));
    this.add(txtIngre, new XYConstraints(432, 12, 51, -1));

    this.add(lFecExp, new XYConstraints(13, 47, 154, -1));
    this.add(fechaExp, new XYConstraints(167, 47, 77, -1));
    this.add(horaExp, new XYConstraints(248, 47, 43, -1));
    this.add(lFecSin, new XYConstraints(348, 47, 184, -1));
    this.add(fechaSin, new XYConstraints(533, 47, 77, -1));
    this.add(horaSin, new XYConstraints(614, 47, 43, -1));

    this.add(lLugarExp, new XYConstraints(13, 82, 127, -1));
    this.add(txtLugarExp, new XYConstraints(165, 82, 275, -1));
    this.add(lTel, new XYConstraints(472, 82, -1, -1));
    this.add(txtTel, new XYConstraints(533, 82, 100, -1));

    this.add(pansuca, new XYConstraints(10, 124, 596, 116));
    this.add(panNiv, new XYConstraints(10, 245, 655, 40));
    setBorde(false);

  }

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    if (dlg != null) {
      dlg.Inicializar(modoOperacion);
    }
  } // Fin Inicializar()

  public void InicializarDesdeFuera(int mod) {
    modoOperacion = mod;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      // modo espera
      case CInicializar.ESPERA:
        this.setEnabled(false);
        pansuca.InicializarDesdeFuera(CInicializar.ESPERA);
        panNiv.InicializarDesdeFuera(CInicializar.ESPERA);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        switch (modoEntrada) {
          case ALTA:
            enableControls(true);
            pansuca.InicializarDesdeFuera(CInicializar.NORMAL);
            panNiv.InicializarDesdeFuera(CInicializar.NORMAL);
            break;
          case MODIFICACION:
            int sit = new Integer(alerta.getString("CD_SITALERBRO")).intValue();
            switch (sit) {
              case 0:
                enableControls(true);
                pansuca.setModoEntrada(modoEntrada);
                pansuca.InicializarDesdeFuera(modoOperacion);
                panNiv.setModoEntrada(modoEntrada);
                panNiv.InicializarDesdeFuera(modoOperacion);
                break;
              case 1:
              case 2:
              case 3:
              case 4:
                enableControls(false);
                pansuca.setModoEntrada(CONSULTA);
                pansuca.InicializarDesdeFuera(modoOperacion);
                panNiv.setModoEntrada(CONSULTA);
                panNiv.InicializarDesdeFuera(modoOperacion);
                break;
            }
            break;

          case BAJA:
          case CONSULTA:
            enableControls(false);
            pansuca.InicializarDesdeFuera(modoOperacion);
            panNiv.InicializarDesdeFuera(modoOperacion);
            break;
        } // Fin switch modoEntrada
        break;
    } // Fin switch modoOperacion
  }

  public void enableControls(boolean state) {
    txtExp.setEnabled(state);
    txtEnf.setEnabled(state);
    txtIngre.setEnabled(state);
    fechaExp.setEnabled(state);
    horaExp.setEnabled(state);
    fechaSin.setEnabled(state);
    horaSin.setEnabled(state);
    txtLugarExp.setEnabled(state);
    txtTel.setEnabled(state);
  }

  public void recogerDatos(sapp2.Data dtDev) {

    dtDev.put("NM_EXPUESTOS", txtExp.getText().trim());
    dtDev.put("NM_ENFERMOS", txtEnf.getText().trim());
    dtDev.put("NM_INGHOSP", txtIngre.getText().trim());
    String fExp = "";
    if (!fechaExp.getText().equals("")) {
      fExp = fechaExp.getText().trim();
      if (!horaExp.getText().equals("")) {
        fExp += " " + horaExp.getText().trim() + ":00";
      }
      else {
        fExp += " 00:00:00";
      }
    }
    dtDev.put("FC_EXPOSICION", fExp);
    String fSin = "";
    if (!fechaSin.getText().equals("")) {
      fSin = fechaSin.getText().trim();
      if (!horaSin.getText().equals("")) {
        fSin += " " + horaSin.getText().trim() + ":00";
      }
      else {
        fSin += " 00:00:00";
      }
    }
    dtDev.put("FC_INISINTOMAS", fSin);
    dtDev.put("DS_LEXP", txtLugarExp.getText().trim());
    dtDev.put("DS_LEXPTELEF", txtTel.getText().trim());

    // Recogida de datos de sucaNot
    sapp2.Data dtSucaR = new sapp2.Data();
    pansuca.recogerDatos(dtSucaR);
    dtDev.put("CD_LEXPCA", (String) dtSucaR.getString("CA"));
    dtDev.put("CD_LEXPROV", (String) dtSucaR.getString("PROV"));
    dtDev.put("CD_LEXMUN", (String) dtSucaR.getString("MUN"));
    dtDev.put("DS_LEXPDIREC", (String) dtSucaR.getString("VIA"));
    dtDev.put("DS_LNMCALLE", (String) dtSucaR.getString("NUM"));
    dtDev.put("DS_LEXPPISO", (String) dtSucaR.getString("PISO"));
    dtDev.put("CD_LEXPPOST", (String) dtSucaR.getString("CODPOSTAL"));

    // Recogida de datos de panniveles
    dtDev.put("CD_NIVEL_1_EX", panNiv.getCDNivel1().trim());
    dtDev.put("CD_NIVEL_2_EX", panNiv.getCDNivel2().trim());
    dtDev.put("CD_ZBS_EX", panNiv.getCDNivel4().trim());
    /*dtDev.put("DS_NIVEL_1_EX",panNiv.getDSNivel1().trim());
         dtDev.put("DS_NIVEL_2_EX",panNiv.getDSNivel2().trim());
         dtDev.put("DS_ZBS_EX",panNiv.getDSNivel4().trim());*/
  }

  public void rellenarDatos(sapp2.Data dtReg) {

    txtExp.setText(dtReg.getString("NM_EXPUESTOS"));
    txtEnf.setText(dtReg.getString("NM_ENFERMOS"));
    txtIngre.setText(dtReg.getString("NM_INGHOSP"));

    // Fecha/Hora de la exposicion
    String fe = ( (String) dtReg.getString("FC_EXPOSICION")).trim();
    if (!fe.equals("")) {
      fechaExp.setText(fe.substring(0, fe.indexOf(' ', 0)));
      horaExp.setText( (fe.substring(fe.indexOf(' ', 0) + 1)).substring(0, 5));
    }

    // Fecha/Hora de la sintomas
    String fs = ( (String) dtReg.getString("FC_INISINTOMAS")).trim();
    if (!fs.equals("")) {
      fechaSin.setText(fs.substring(0, fe.indexOf(' ', 0)));
      horaSin.setText( (fs.substring(fe.indexOf(' ', 0) + 1)).substring(0, 5));
    }

    txtLugarExp.setText(dtReg.getString("DS_LEXP"));
    txtTel.setText(dtReg.getString("DS_LEXPTELEF"));

    // Relleno de sucaNot
    Data dtSuca = new Data();
    dtSuca.put("CA", dtReg.getString("CD_CA"));
    dtSuca.put("PROV", dtReg.getString("CD_LEXPROV"));
    dtSuca.put("MUN", dtReg.getString("CD_LEXMUN"));
    dtSuca.put("VIA", dtReg.getString("DS_LEXPDIREC"));
    dtSuca.put("NUM", dtReg.getString("DS_LNMCALLE"));
    dtSuca.put("PISO", dtReg.getString("DS_LEXPPISO"));
    dtSuca.put("CODPOSTAL", dtReg.getString("CD_LEXPPOST"));
    pansuca.rellenarDatos(dtSuca);

    // Relleno de panniveles
    String cdNivel1Ex = "";
    if (dtReg.getString("CD_NIVEL_1_EX") != null) {
      cdNivel1Ex = dtReg.getString("CD_NIVEL_1_EX");
      panNiv.setCDNivel1(cdNivel1Ex);
    }

    String dsNivel1Ex = "";
    if (dtReg.getString("DS_NIVEL_1_EX") != null) {
      dsNivel1Ex = dtReg.getString("DS_NIVEL_1_EX");
    }
    panNiv.setDSNivel1(dsNivel1Ex);

    String cdNivel2Ex = "";
    if (dtReg.getString("CD_NIVEL_2_EX") != null) {
      cdNivel2Ex = dtReg.getString("CD_NIVEL_2_EX");
      panNiv.setCDNivel2(cdNivel2Ex);
    }

    String dsNivel2Ex = "";
    if (dtReg.getString("DS_NIVEL_2_EX") != null) {
      dsNivel2Ex = dtReg.getString("DS_NIVEL_2_EX");
    }
    panNiv.setDSNivel2(dsNivel2Ex);

    String cdZbsEx = "";
    if (dtReg.getString("CD_ZBS_EX") != null) {
      cdZbsEx = dtReg.getString("CD_ZBS_EX");
      panNiv.setCDNivel4(cdZbsEx);
    }

    String dsZbsEx = "";
    if (dtReg.getString("DS_ZBS_EX") != null) {
      dsZbsEx = dtReg.getString("DS_ZBS_EX");
    }
    panNiv.setDSNivel4(dsZbsEx);

  } //fin

  /*  void fechaExp_focusLost(){
      fechaExp.ValidarFecha(); //validamos el dato
      if (fechaExp.getValid().equals("N")){
          //opcional vaciarlo
          fechaExp.setText(fechaExp.getFecha());
          horaExp.setText("");
      }
      else if (fechaExp.getValid().equals("S")){
        fechaExp.setText(fechaExp.getFecha());
        if (horaExp.getText().equals(""))
         horaExp.setText("00:00");
      }
    }
    void fechaSin_focusLost(){
      fechaSin.ValidarFecha(); //validamos el dato
      if (fechaSin.getValid().equals("N")){
          //opcional vaciarlo
          fechaSin.setText(fechaSin.getFecha());
          horaSin.setText("");
      }
      else if (fechaSin.getValid().equals("S")){
        fechaSin.setText(fechaSin.getFecha());
        if (horaSin.getText().equals(""))
         horaSin.setText("00:00");
      }
    }
    void horaExp_focusLost(){
      horaExp.ValidarFecha(); //validamos el dato
      if (horaExp.getValid().equals("N")){
          //opcional vaciarlo
          horaExp.setText(horaExp.getFecha());
      }
      else if (horaExp.getValid().equals("S"))
        horaExp.setText(horaExp.getFecha());
    }
    void horaSin_focusLost(){
      horaSin.ValidarFecha(); //validamos el dato
      if (horaSin.getValid().equals("N")){
          //opcional vaciarlo
          horaSin.setText(horaSin.getFecha());
      }
      else if (horaSin.getValid().equals("S"))
        horaSin.setText(horaSin.getFecha());
    }*/

  public boolean validarDatos() {

    // Fecha Exposicion debe ser una fecha v�lida
    if (!fechaExp.getText().equals("")) {
      fechaExp.ValidarFecha();
      if (fechaExp.getValid().equals("N")) {
        this.getApp().showError("Fecha incorrecta");
        fechaExp.requestFocus();
        return (false);
      }
    }

    // La Hora Exposicion debe ser una fecha v�lida
    if (!horaExp.getText().equals("")) {
      horaExp.ValidarFecha();
      if (horaExp.getValid().equals("N")) {
        this.getApp().showError("Hora incorrecta");
        horaExp.requestFocus();
        return (false);
      }
    }

    // Fecha Sintomas debe ser una fecha v�lida
    if (!fechaSin.getText().equals("")) {
      fechaSin.ValidarFecha();
      if (fechaSin.getValid().equals("N")) {
        this.getApp().showError("Fecha incorrecta");
        fechaSin.requestFocus();
        return (false);
      }
    }

    // La Hora Sintomas debe ser una fecha v�lida
    if (!horaSin.getText().equals("")) {
      horaSin.ValidarFecha();
      if (horaSin.getValid().equals("N")) {
        this.getApp().showError("Hora incorrecta");
        horaSin.requestFocus();
        return (false);
      }
    }

    // La fecha/hora s�ntomas debe ser posterior a la
    // fecha hora exposici�n.
    if (fechaCorrecta(fechaExp.getText(),
                      fechaSin.getText(),
                      horaExp.getText(),
                      horaSin.getText()) == 1) {
      this.getApp().showError(
          "La fecha de exposici�n es posterior a la fecha de aparici�n de s�ntomas");
      return (false);
    }

    if (!pansuca.validarDatos()) {
      return false;
    }

    return true;

  }

  private int fechaCorrecta(String f1, String f2, String h1, String h2) {
    int compara = 0;
    int compara_caso = 0;
    int compara_hora = 0;
    int compara_hora_caso = 0;
    boolean estado = true;

    try {
      if (f1.length() > 10) {
        h1 = f1.substring(10).trim();
      }
      if (f2.length() > 10) {
        h2 = f2.substring(10).trim();
      }
      if (!f1.equals("")) {
        f1 = f1.substring(0, 10);
      }
      if (!f2.equals("")) {
        f2 = f2.substring(0, 10);

      }
      if ( (f1.length() > 0) && (f2.length() > 0)) {
        compara = Format.comparaFechas(f1, f2);
      }
      if (compara == 0) { // Da el caso de que son iguales.
        if ( (h1.length() > 0) && (h2.length() > 0)) {
          compara_hora = comparaHora(h1, h2);
        }
      }
    }
    catch (Exception e) {
      this.getApp().showError("Formato de fecha/hora incorrecto.");
      estado = false;
    }
    int salida = compara;
    if (salida == 0) {
      if (compara_hora == 1) {
        salida = 1;
      }
      if (compara_hora == -1) {
        salida = 2;
      }
    }
    return salida;
  }

  private int comparaHora(String h1, String h2) throws Exception {
    //Devuelve 0 si son iguales, 1 si h1<h2 y -1 si h2<h1.
    int estado = 0;

    h1 = h1.trim();
    h2 = h2.trim();

    String hora1 = h1.substring(0, 2);
    String minuto1 = h1.substring(3, 5);
//    String segundo1 = h1.substring (6,8);
    String hora2 = h2.substring(0, 2);
    String minuto2 = h2.substring(3, 5);
//    String segundo2 = h2.substring (6,8);

    int ihora1 = Integer.parseInt(hora1);
    int iminuto1 = Integer.parseInt(minuto1);
//    int isegundo1 = Integer.parseInt (segundo1);
    int ihora2 = Integer.parseInt(hora2);
    int iminuto2 = Integer.parseInt(minuto2);
//    int isegundo2 = Integer.parseInt (segundo2);

    // horas
    if (ihora1 > ihora2) {
      estado = 1;
    }
    if (ihora1 < ihora2) {
      estado = -1;
    }
    if (ihora1 == ihora2) {
      // minutos
      if (iminuto1 > iminuto2) {
        estado = 1;
      }
      if (iminuto1 < iminuto2) {
        estado = -1;
      }
      if (iminuto1 == iminuto2) {
        estado = 0;
        // segundos
//      if (isegundo1 > isegundo2) estado = -1;
//      if (isegundo1 < isegundo2) estado = 1;
//      if (isegundo1 == isegundo2) estado = 0;
//     }
      }
    }
    return estado;
  }

  /**************** Funciones de acceso a vars. del panel *******/
  public void cambiarAlerta(Data newAlerta) {
    alerta = newAlerta;
  } // Fin cambiarAlerta()

  public void setModoEntrada(int m) {
    modoEntrada = m;
  }

  /********** Sincronizador de Eventos ******************/

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

} //fin clase
/*class focusAdapter implements java.awt.event.FocusListener, Runnable {
  DAPanExp adaptee;
  FocusEvent evt;
  focusAdapter(DAPanExp adaptee) {
    this.adaptee = adaptee;
  }
  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }
  public void focusGained(FocusEvent e) {
  }
  public void run() {
   SincrEventos s = adaptee.getSincrEventos();
   if(s.bloquea(adaptee.modoOperacion,adaptee)){
    if(((comun.CFechaSimple)evt.getSource()).getName().equals("fechaExp")){
        adaptee.fechaExp_focusLost();
     }
     else if(((comun.CFechaSimple)evt.getSource()).getName().equals("fechaSin")){
        adaptee.fechaSin_focusLost();
     }
    s.desbloquea(adaptee);
   }
  }
 }*/
/*class focusAdapterHora implements java.awt.event.FocusListener, Runnable {
  DAPanExp adaptee;
  FocusEvent evt;
  focusAdapterHora(DAPanExp adaptee) {
    this.adaptee = adaptee;
  }
  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }
  public void focusGained(FocusEvent e) {
  }
  public void run() {
   SincrEventos s = adaptee.getSincrEventos();
   if(s.bloquea(adaptee.modoOperacion,adaptee)){
     if(((comun.CHora)evt.getSource()).getName().equals("horaExp")){
        adaptee.horaExp_focusLost();
     }
     else if(((comun.CHora)evt.getSource()).getName().equals("horaSin")){
        adaptee.horaSin_focusLost();
     }
   s.desbloquea(adaptee);
   }
  }
 } */
