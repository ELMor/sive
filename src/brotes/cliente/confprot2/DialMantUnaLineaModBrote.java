package brotes.cliente.confprot2;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.datos.confprot2.DataMantLineaBrote;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import catalogo.DataCat;
import comun.CFechaSimple;
import listas.DataMantlis;
import preguntas.DataMantPreguntas;
import preguntas.Pan_MantPreguntas;
import sapp.StubSrvBD;

public class DialMantUnaLineaModBrote
    extends CDialog {

  // stub
  protected StubSrvBD stubCliente;
  ResourceBundle res = ResourceBundle.getBundle("brotes.cliente.confprot2.Res" +
                                                this.app.getIdioma());

  // modos de operaci�n
  final int modoNORMAL = 0;
  final int modoCONSULTA = 2;
  final int modoESPERA = 1;

  // modos de la ventana
  public static final int modoTEXTO = 0;
  public static final int modoPREGUNTA = 1;
  public static final int modoPREGUNTA_CONDICIONADA = 5;
  public static final int modoPREGUNTA_PROTEGIDO = 7;
  public static final int modoPREGUNTA_CONDICIONADA_PROTEGIDO = 8;

  // modos de operaci�n del servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletOBTENER_VALORES = 10;

  final String strSERVLET_AUX = "servlet/SrvMantPreguntas";
  final String strSERVLET_LISTAS = "servlet/SrvMantlis";

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;
  protected String miTipo = null;

  // im�genes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/inspreg.gif"
  };

  // modos de operacion
  protected int modoVentana;
  protected int modoOperacion; //modo op. guardado si es necesario
  protected int modoOperacionIni; //modo op. guardado si es necesario

  // bot�n pulsado
  protected boolean bEstado = false;

  protected PanelModeloBrote pnlModelo;
  protected CLista miLista = null;
  protected DataCat miElemento = null;

  // datos
  protected DataMantLineaBrote datLineaModificada = null;
  protected int iNumLinea = -1;
  protected int iCondicionada = 0;

  // gestores de eventos
  DialMantUnaLineaModBroteActionListener btnActionListener = new
      DialMantUnaLineaModBroteActionListener(this);
  DialMantUnaLineaModBroteTxtCodLineaKeyAdapter txtCodPreguntaKeyListener = new
      DialMantUnaLineaModBroteTxtCodLineaKeyAdapter(this);
  ModfocusAdapter txtFocusAdapter = new ModfocusAdapter(this);

  // controles
  XYLayout xYLayout = new XYLayout();
  Label lblDescGeneral = new Label();
  TextField txtDescGeneral = new TextField();
  CCampoCodigo txtCodPregunta = new CCampoCodigo();
  ButtonControl btnCtrlCodPregunta = new ButtonControl();
  TextField txtDescPregunta = new TextField();
  Label lblPregunta = new Label();
  Label lblDescLocal = new Label();
  TextField txtDescLocal = new TextField();
  Checkbox chkbxObligatoria = new Checkbox();
  Label lblValor = new Label();

  TextField txtCondAValor = new TextField();
  Choice chkCondAValor = new Choice();
  Choice chkCondAValorBool = new Choice();
  CFechaSimple cfCondAValor = new CFechaSimple("S");

  ButtonControl btnPreg = new ButtonControl();

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  Panel pnl = new Panel();
  Panel pnlStatus = new Panel();
  BorderLayout borderLayout1 = new BorderLayout();
  BorderLayout borderLayout2 = new BorderLayout();
  StatusBar tipoPreg = new StatusBar();
  StatusBar tipoPregC = new StatusBar();

  // indicador de cambio en pregunta
  protected boolean bCambioEnPregunta = true;

  // constructor
  public DialMantUnaLineaModBrote(PanelModeloBrote p,
                                  int modo,
                                  DataMantLineaBrote linea,
                                  String s,
                                  int iCond,
                                  int modoOp) {
    super(p.getCApp());

    try {

      pnlModelo = p;
      datLineaModificada = linea;
      iCondicionada = iCond;
      modoVentana = modo;
      modoOperacionIni = modoOp;
      jbInit();
      setTitle(s);
      pack();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicializa controles
  private void jbInit() throws Exception {
    CLista param;
    DataMantPreguntas data;

    tipoPreg.setBevelOuter(BevelPanel.LOWERED);
    tipoPreg.setBevelInner(BevelPanel.LOWERED);
    tipoPregC.setBevelOuter(BevelPanel.LOWERED);
    tipoPregC.setBevelInner(BevelPanel.LOWERED);

    switch (modoVentana) {
      case modoTEXTO:
        xYLayout.setHeight(120);
        xYLayout.setWidth(460);
        pnl.setLayout(xYLayout);
        pnl.add(lblDescGeneral, new XYConstraints(10, 10, 77, -1));
        pnl.add(txtDescGeneral, new XYConstraints(111, 10, 332, -1));
        pnl.add(lblDescLocal, new XYConstraints(10, 44, 105, -1));
        pnl.add(txtDescLocal, new XYConstraints(112, 44, 333, -1));
        pnl.add(btnAceptar, new XYConstraints(279, 82, 80, 26));
        pnl.add(btnCancelar, new XYConstraints(366, 82, 80, 26));

        setLayout(borderLayout2);
        setSize(460, 120);
        add(pnl, BorderLayout.CENTER);
        break;

      case modoPREGUNTA:
      case modoPREGUNTA_PROTEGIDO:
        xYLayout.setHeight(190);
        xYLayout.setWidth(460);
        pnl.setLayout(xYLayout);
        pnl.add(lblPregunta, new XYConstraints(10, 10, -1, -1));
        pnl.add(txtCodPregunta, new XYConstraints(112, 10, 80, -1));
        pnl.add(btnCtrlCodPregunta, new XYConstraints(202, 10, -1, -1));
        pnl.add(txtDescPregunta, new XYConstraints(240, 10, 206, -1));
        pnl.add(lblDescGeneral, new XYConstraints(10, 44, 77, -1));
        pnl.add(txtDescGeneral, new XYConstraints(111, 44, 332, -1));
        pnl.add(lblDescLocal, new XYConstraints(10, 82, 105, -1));
        pnl.add(txtDescLocal, new XYConstraints(112, 82, 333, -1));
        pnl.add(chkbxObligatoria, new XYConstraints(10, 121, 108, -1));
        pnl.add(btnPreg, new XYConstraints(150, 158, -1, 26));
        pnl.add(btnAceptar, new XYConstraints(279, 158, 80, 26));
        pnl.add(btnCancelar, new XYConstraints(366, 158, 80, 26));
        pnlStatus.setLayout(borderLayout1);
        pnlStatus.add(tipoPreg, BorderLayout.CENTER);
        setLayout(borderLayout2);
        setSize(460, 230);
        add(pnlStatus, BorderLayout.SOUTH);
        add(pnl, BorderLayout.CENTER);
        break;

      case modoPREGUNTA_CONDICIONADA:
      case modoPREGUNTA_CONDICIONADA_PROTEGIDO:
        xYLayout.setHeight(190);
        xYLayout.setWidth(460);
        pnl.setLayout(xYLayout);

        pnl.add(lblPregunta, new XYConstraints(10, 10, -1, -1));
        pnl.add(txtCodPregunta, new XYConstraints(112, 10, 80, -1));
        pnl.add(btnCtrlCodPregunta, new XYConstraints(202, 10, -1, -1));
        pnl.add(txtDescPregunta, new XYConstraints(240, 10, 206, -1));
        pnl.add(lblDescGeneral, new XYConstraints(10, 44, 77, -1));
        pnl.add(txtDescGeneral, new XYConstraints(111, 44, 332, -1));
        pnl.add(lblDescLocal, new XYConstraints(10, 82, 105, -1));
        pnl.add(txtDescLocal, new XYConstraints(112, 82, 333, -1));
        pnl.add(chkbxObligatoria, new XYConstraints(10, 121, 108, -1));
        pnl.add(lblValor, new XYConstraints(202, 121, -1, -1));

        // variante seg�n el tipo de pregunta condicionada
        txtCondAValor.setBackground(new Color(255, 255, 150));
        txtCondAValor.setEnabled(false);
        txtCondAValor.setVisible(false);
        pnl.add(txtCondAValor, new XYConstraints(331, 121, 115, -1));

        chkCondAValor.setBackground(new Color(255, 255, 150));
        chkCondAValor.setEnabled(false);
        chkCondAValor.setVisible(false);

        pnl.add(chkCondAValor, new XYConstraints(331, 121, 115, -1));

        chkCondAValorBool.setBackground(new Color(255, 255, 150));
        chkCondAValorBool.setEnabled(false);
        chkCondAValorBool.setVisible(false);
        chkCondAValorBool.addItem(res.getString("msg22.Text"));
        chkCondAValorBool.addItem(res.getString("msg23.Text"));
        pnl.add(chkCondAValorBool, new XYConstraints(331, 121, 115, -1));

        cfCondAValor.setEnabled(false);
        cfCondAValor.setVisible(false);
        cfCondAValor.addFocusListener(new java.awt.event.FocusAdapter() {
          public void focusLost(FocusEvent e) {
            cfCondAValor_focusLost(e);
          }
        });

        pnl.add(cfCondAValor, new XYConstraints(331, 121, 115, -1));

        pnl.add(btnPreg, new XYConstraints(150, 158, -1, 26));
//        pnl.add(btnLista, new XYConstraints(200, 158, -1, 26));

        pnl.add(btnAceptar, new XYConstraints(279, 158, 80, 26));
        pnl.add(btnCancelar, new XYConstraints(366, 158, 80, 26));

        pnlStatus.setLayout(borderLayout1);
        pnlStatus.add(tipoPreg, BorderLayout.WEST);
        pnlStatus.add(tipoPregC, BorderLayout.CENTER);
        setLayout(borderLayout2);
        setSize(460, 230);
        add(pnlStatus, BorderLayout.SOUTH);
        add(pnl, BorderLayout.CENTER);

        // tipo de respuesta condicionada
        DataMantLineaBrote dataC = (DataMantLineaBrote) pnlModelo.lineasModelo.
            elementAt(iCondicionada - 1);
        param = new CLista();
        //# System.Out.println(dataC.sCodPregunta);
        param.addElement(new DataMantPreguntas(dataC.sCodPregunta));
        param.setTSive(this.getCApp().getTSive());
        pnlModelo.stubCliente.setUrl(new URL(app.getURL() + strSERVLET_AUX));
        param = (CLista) pnlModelo.stubCliente.doPost(servletOBTENER_X_CODIGO,
            param);

        // rellena los datos
        if (param.size() > 0) {
          data = (DataMantPreguntas) param.elementAt(0);
          miTipo = data.getTipo();

          if ( (data.getTipo().equals("C")) || (data.getTipo().equals("N"))) {
            txtCondAValor.setVisible(true);
            txtCondAValor.setEnabled(true);
          }
          else if (data.getTipo().equals("L")) {
            param = new CLista();
            miLista = new CLista();
            param.addElement(new DataMantlis(data.getListaValores()));
            param.setTSive(this.getCApp().getTSive());
            stubCliente = new StubSrvBD();
            stubCliente.setUrl(new URL(app.getURL() + strSERVLET_LISTAS));
            miLista = (CLista) stubCliente.doPost(servletOBTENER_VALORES, param);

            if (miLista.size() > 0) {
              for (int g = 0; g < miLista.size(); g++) {
                miElemento = new DataCat();
                miElemento = (DataCat) miLista.elementAt(g);
                chkCondAValor.addItem(miElemento.getDes());
                miElemento = null;
              }
            }
            chkCondAValor.setVisible(true);
            chkCondAValor.setEnabled(true);
          }
          else if (data.getTipo().equals("B")) {
            chkCondAValorBool.setVisible(true);
            chkCondAValorBool.setEnabled(true);
          }
          else if (data.getTipo().equals("F")) {
            cfCondAValor.setVisible(true);
            cfCondAValor.setEnabled(true);
          }

          tipoPregC.setText(res.getString("tipoPregC.Text") +
                            sTipoDato(data.getTipo(), data.getLongitud(),
                                      data.getEnteros(), data.getDecimales()));
        }

        break;
    }

    // colores obligatorios
    txtCodPregunta.setBackground(new Color(255, 255, 150));
    txtCondAValor.setBackground(new Color(255, 255, 150));
    txtDescGeneral.setBackground(new Color(255, 255, 150));
    txtDescPregunta.setEditable(false);

    // etiquetas
    lblDescGeneral.setText(res.getString("lblDescGeneral.Text"));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    lblPregunta.setText(res.getString("lblPregunta.Text"));
    lblDescLocal.setText(res.getString("lblDescLocal2.Text"));
    chkbxObligatoria.setLabel(res.getString("chkbxObligatoria.Label"));
    lblValor.setText(res.getString("lblValor.Text"));

    // im�genes

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnCtrlCodPregunta.setImage(imgs.getImage(0));
    btnAceptar.setImage(imgs.getImage(1));
    btnCancelar.setImage(imgs.getImage(2));
    btnPreg.setImage(imgs.getImage(3));
//    btnLista.setImage(imgs.getImage(4));

    // aceptar, cancelar y buscar pregunta
    btnAceptar.setActionCommand("Aceptar");
    btnCancelar.setActionCommand("Cancelar");
    btnCtrlCodPregunta.setActionCommand("Buscar");
    btnPreg.setActionCommand("pregunta");
//    btnLista.setActionCommand("lista");

    btnAceptar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);
    btnCtrlCodPregunta.addActionListener(btnActionListener);
    btnPreg.addActionListener(btnActionListener);
//    btnLista.addActionListener(btnActionListener);

    // pregunta
    txtCodPregunta.addKeyListener(txtCodPreguntaKeyListener);
    txtCodPregunta.addFocusListener(txtFocusAdapter);

    modoOperacion = modoOperacionIni;

    // si estamos en modo modificar, rellena los datos
    if (datLineaModificada != null) {

      iNumLinea = datLineaModificada.iLinea;
      //# System.Out.println(iNumLinea);

      // des
      txtDescGeneral.setText(datLineaModificada.sDescGeneral);

      // desl
      txtDescLocal.setText(datLineaModificada.sDescLocal);

      // preg
      if (modoVentana != modoTEXTO) {

        // oblig
        chkbxObligatoria.setState(datLineaModificada.bObligatoria);

        // cod
        txtCodPregunta.setText(datLineaModificada.sCodPregunta);
        param = new CLista();
        bCambioEnPregunta = false;

        param.addElement(new DataMantPreguntas(datLineaModificada.sCodPregunta));
        param.setTSive(this.getCApp().getTSive());
        pnlModelo.stubCliente.setUrl(new URL(app.getURL() + strSERVLET_AUX));
        param = (CLista) pnlModelo.stubCliente.doPost(servletOBTENER_X_CODIGO,
            param);

        // rellena los datos
        if (param.size() > 0) {
          data = (DataMantPreguntas) param.elementAt(0);

          String sDes = data.getDescPregunta();
          if ( (this.app.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (data.getDescLPregunta() != null)) {
            sDes = data.getDescLPregunta();
          }
          txtDescPregunta.setText(sDes);

          // tipo de dato
          //# System.Out.println(data.getTipo());
          tipoPreg.setText(res.getString("tipoPreg.Text") +
                           sTipoDato(data.getTipo(), data.getLongitud(),
                                     data.getEnteros(), data.getDecimales()));
        }

        if (datLineaModificada.bCondicionada) {
          // valor cond
          if ( (miTipo.equals("C")) || (miTipo.equals("N"))) {
            txtCondAValor.setText(datLineaModificada.sCondAValor);
          }
          else if (miTipo.equals("L")) {
            for (int miPo = 0; miPo < miLista.size(); miPo++) {
              miElemento = new DataCat("");
              miElemento = (DataCat) miLista.elementAt(miPo);
              if (miElemento.getCod().equals(datLineaModificada.sCondAValor)) {
                chkCondAValor.select(miPo);
              }
              miElemento = null;
            }
          }
          else if (miTipo.equals("B")) {
            if (datLineaModificada.sCondAValor.equals("S")) {
              chkCondAValorBool.select(0);
            }
            else {
              chkCondAValorBool.select(1);
            }
          }
          else if (miTipo.equals("F")) {
            cfCondAValor.setText(datLineaModificada.sCondAValor);
            cfCondAValor.ValidarFecha(); //validamos el dato
            if (cfCondAValor.getValid().equals("N")) {
              //opcional vaciarlo
              cfCondAValor.setText(cfCondAValor.getFecha());
            }
            else if (cfCondAValor.getValid().equals("S")) {
              //opcional vaciarlo
              cfCondAValor.setText(cfCondAValor.getFecha());
            }
          }

        }
      }
    }

    // idioma local no es visible si no tengo
    if (app.getIdiomaLocal().length() == 0) {
      txtDescLocal.setVisible(false);
      lblDescLocal.setVisible(false);
    }
    else {
      lblDescLocal.setText("Descripci�n (" + app.getIdiomaLocal() + "):");
    }
    Inicializar();
  }

  void cfCondAValor_focusLost(FocusEvent e) {
    cfCondAValor.ValidarFecha(); //validamos el dato

    if (cfCondAValor.getValid().equals("N")) {
      cfCondAValor.setText(cfCondAValor.getFecha());
    }
    else if (cfCondAValor.getValid().equals("S")) {
      cfCondAValor.setText(cfCondAValor.getFecha());
    }
  }

  // determina el tipo de dato de la pregunta
  protected String sTipoDato(String sTipo,
                             int iLon,
                             int iEnt,
                             int iDec) {
    String sParseTipo = "";

    if (sTipo.equals(DataMantPreguntas.tipoBOOLEANO)) {
      sParseTipo = res.getString("msg24.Text");
    }
    else if (sTipo.equals(DataMantPreguntas.tipoCADENA)) {
      sParseTipo = res.getString("msg25.Text") + Integer.toString(iLon) + ")";
    }
    else if (sTipo.equals(DataMantPreguntas.tipoFECHA)) {
      sParseTipo = res.getString("msg26.Text");
    }
    else if (sTipo.equals(DataMantPreguntas.tipoLISTA)) {
      sParseTipo = res.getString("msg27.Text");
    }
    else if (sTipo.equals(DataMantPreguntas.tipoNUMERICO)) {
      sParseTipo = res.getString("msg28.Text") + Integer.toString(iEnt) + "," +
          Integer.toString(iDec) + ")";
    }

    return sParseTipo;
  }

  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        txtDescGeneral.setEnabled(true);
        txtDescLocal.setEnabled(true);
        txtCodPregunta.setEnabled(true);
        btnCtrlCodPregunta.setEnabled(true);
        chkbxObligatoria.setEnabled(true);
        txtCondAValor.setEnabled(true);
        btnPreg.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoCONSULTA:
        txtDescGeneral.setEnabled(false);
        txtDescLocal.setEnabled(false);
        txtCodPregunta.setEnabled(false);
        btnCtrlCodPregunta.setEnabled(false);
        chkbxObligatoria.setEnabled(false);
        txtCondAValor.setEnabled(false);
        btnPreg.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtDescGeneral.setEnabled(false);
        txtDescLocal.setEnabled(false);
        txtCodPregunta.setEnabled(false);
        btnCtrlCodPregunta.setEnabled(false);
        chkbxObligatoria.setEnabled(false);
        txtCondAValor.setEnabled(false);
        btnPreg.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
    doLayout();
  }

  // busqueda de c�digo de pregunta
  void btnCtrlCodPregunta_actionPerformed(ActionEvent e) {
    String sDes;

    modoOperacion = modoESPERA;
    Inicializar();

    CListaPreg lista = new CListaPreg(app,
                                      res.getString("msg29.Text"),
                                      pnlModelo.stubCliente,
                                      strSERVLET_AUX,
                                      servletOBTENER_X_CODIGO,
                                      servletOBTENER_X_DESCRIPCION,
                                      servletSELECCION_X_CODIGO,
                                      servletSELECCION_X_DESCRIPCION);
    lista.show();
    DataMantPreguntas data = (DataMantPreguntas) lista.getComponente();
    lista = null;

    if (data != null) {
      txtCodPregunta.setText(data.getCodPregunta());

      sDes = data.getDescPregunta();
      /*
         if ((this.app.getIdioma() != CApp.idiomaPORDEFECTO) && (data.getDescLPregunta() != null))
           sDes = data.getDescLPregunta();
       */
      txtDescPregunta.setText(sDes);

      // rellena las descripciones
      if (txtDescGeneral.getText().length() == 0) {
        txtDescGeneral.setText(data.getDescPregunta());
      }
      if (txtDescLocal.getText().length() == 0) {
        txtDescLocal.setText(data.getDescLPregunta());

      }
      bCambioEnPregunta = false;

      // tipo de dato
      tipoPreg.setText(res.getString("tipoPreg.Text") +
                       sTipoDato(data.getTipo(), data.getLongitud(),
                                 data.getEnteros(), data.getDecimales()));
    }

    modoOperacion = modoOperacionIni;
    Inicializar();
  }

  void btnAceptar_actionPerformed(ActionEvent e) {
    CMessage msgBox;
    String sWarning = "";
    boolean isDataValid = true;

    switch (modoVentana) {
      case modoTEXTO:

        // datos m�nimos
        if (txtDescGeneral.getText().length() == 0) {
          sWarning = res.getString("msg30.Text");
          isDataValid = false;
        }

        break;
      case modoPREGUNTA:
      case modoPREGUNTA_PROTEGIDO:

        // datos m�nimos
        if ( (txtDescGeneral.getText().length() == 0) ||
            (txtDescPregunta.getText().length() == 0)) {
          sWarning = res.getString("msg30.Text");
          isDataValid = false;
        }

        // codigo pregunta no repetido
        if (isDataValid) {
          if (bPreguntaRepetida()) {
            sWarning = res.getString("msg31.Text");
            isDataValid = false;
          }
        }

        break;
      case modoPREGUNTA_CONDICIONADA:
      case modoPREGUNTA_CONDICIONADA_PROTEGIDO:

        // datos m�nimos
        if ( (txtDescGeneral.getText().length() == 0) ||
            (txtDescPregunta.getText().length() == 0) ||
            ( (txtCondAValor.getText().length() == 0) && ( (miTipo.equals("C")) ||
            (miTipo.equals("N"))) ||
             ( (cfCondAValor.getFecha().length() == 0) && (miTipo.equals("F"))) ||
             ( (chkCondAValor.getSelectedIndex() == -1) && (miTipo.equals("L")))
             )) {
          sWarning = res.getString("msg32.Text");
          isDataValid = false;
        }

        // codigo pregunta no repetido
        if (isDataValid) {
          if (bPreguntaRepetida()) {
            sWarning = res.getString("msg33.Text");
            isDataValid = false;
          }
        }

        break;
    }

    if (isDataValid) {

      if (txtDescGeneral.getText().length() > 40) {
        isDataValid = false;
        sWarning = res.getString("msg34.Text");
        txtDescGeneral.selectAll();
      }

      if (txtDescLocal.getText().length() > 40) {
        isDataValid = false;
        sWarning = res.getString("msg34.Text");
        txtDescLocal.selectAll();
      }

      if (txtCondAValor.getText().length() > 60) {
        isDataValid = false;
        sWarning = res.getString("msg34.Text");
        txtCondAValor.selectAll();
      }

    }

    // si los datos son correctos son validados
    if (isDataValid) {
      bEstado = true;

      // insertamos la informaci�n en la caja de texto
      if ( (modoVentana == modoPREGUNTA_CONDICIONADA_PROTEGIDO) ||
          (modoVentana == modoPREGUNTA_CONDICIONADA)) {
        if (miTipo.equals("B")) {
          if (chkCondAValorBool.getSelectedIndex() == 0) {
            txtCondAValor.setText(res.getString("txtCondAValor.Text"));
          }
          else {
            txtCondAValor.setText(res.getString("txtCondAValor.Text1"));
          }
        }
        else if (miTipo.equals("F")) {
          cfCondAValor.ValidarFecha(); //validamos el dato
          if (cfCondAValor.getValid().equals("N")) {
            cfCondAValor.setText(cfCondAValor.getFecha());
          }
          else if (cfCondAValor.getValid().equals("S")) {
            cfCondAValor.setText(cfCondAValor.getFecha());
          }
          txtCondAValor.setText(cfCondAValor.getFecha());
        }
        else if (miTipo.equals("L")) {
          miElemento = new DataCat("");
          miElemento = (DataCat) miLista.elementAt(chkCondAValor.
              getSelectedIndex());
          txtCondAValor.setText(miElemento.getCod());
          miElemento = null;
        }
      }
      dispose();
    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgAVISO, sWarning);
      msgBox.show();
      msgBox = null;
    }
  }

  protected boolean bPreguntaRepetida() {
    DataMantLineaBrote datLinea;
    boolean b = false;

    for (int j = 0; j < pnlModelo.lineasModelo.size(); j++) {
      datLinea = (DataMantLineaBrote) pnlModelo.lineasModelo.elementAt(j);
      if (datLinea.sTipo.equals(DataMantLineaBrote.tipoPREGUNTA)) {
        if ( (datLinea.sCodPregunta.equals(txtCodPregunta.getText())) &&
            (j != iNumLinea - 1)) {
          b = true;
        }
      }
    }
    return b;
  }

  void btnCancelar_actionPerformed(ActionEvent e) {
    dispose();
  }

  // devuelve el estado de la ventana de detalle
  public boolean getEstado() {
    return bEstado;
  }

  public String getDes() {
    return txtDescGeneral.getText();
  }

  public String getDesL() {
    return txtDescLocal.getText();
  }

  public String getPregunta() {
    return txtCodPregunta.getText();
  }

  public boolean getObligatoria() {
    return chkbxObligatoria.getState();
  }

  public String getValorCond() {
    return txtCondAValor.getText();
  }

  // cambios en el c�digo de la pregunta
  void txtCodPregunta_keyPressed(KeyEvent e) {
    txtDescPregunta.setText("");
    txtDescGeneral.setText("");
    txtDescLocal.setText("");
    tipoPreg.setText("");
    bCambioEnPregunta = true;
  }

  // perdida de foco de cajas de c�digos
  void focusLost() {

    // datos de envio
    DataMantPreguntas data;
    CLista param = null;
    CMessage msg;
    String sDes;

    if ( (txtCodPregunta.getText().length() > 0) && bCambioEnPregunta) {
      try {
        param = new CLista();
        param.addElement(new DataMantPreguntas(txtCodPregunta.getText()));

        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        pnlModelo.stubCliente.setUrl(new URL(app.getURL() + strSERVLET_AUX));
        param.setTSive(this.getCApp().getTSive());
        param = (CLista) pnlModelo.stubCliente.doPost(servletOBTENER_X_CODIGO,
            param);

        // rellena los datos
        if (param.size() > 0) {
          data = (DataMantPreguntas) param.elementAt(0);

          txtCodPregunta.setText(data.getCodPregunta());
          sDes = data.getDescPregunta();
          if ( (this.app.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (data.getDescLPregunta() != null)) {
            sDes = data.getDescLPregunta();
          }
          txtDescPregunta.setText(sDes);

          // rellena las descripciones
          if (txtDescGeneral.getText().length() == 0) {
            txtDescGeneral.setText(data.getDescPregunta());
          }
          if (txtDescLocal.getText().length() == 0) {
            txtDescLocal.setText(data.getDescLPregunta());

          }
          bCambioEnPregunta = false;

          // tipo de dato
          tipoPreg.setText(res.getString("tipoPreg.Text") +
                           sTipoDato(data.getTipo(), data.getLongitud(),
                                     data.getEnteros(), data.getDecimales()));
        }
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg35.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }

    // modo normal
    modoOperacion = modoOperacionIni;
    Inicializar();
  }

  // insertar pregunta
  public void InsertarPregunta() {

    // datos de envio
    DataMantPreguntas data;
    CLista param = null;
    CMessage msg;
    String sDes;

    modoOperacion = modoESPERA;
    Inicializar();
    Pan_MantPreguntas panel = new Pan_MantPreguntas(this.app,
        Pan_MantPreguntas.modoALTA,
        null);
    panel.show();

    if ( (panel.bAceptar) && (panel.valido)) {
      data = panel.pregunta;
      txtCodPregunta.setText(data.getCodPregunta());

      sDes = data.getDescPregunta();
      if ( (this.app.getIdioma() != CApp.idiomaPORDEFECTO) &&
          (data.getDescLPregunta() != null)) {
        sDes = data.getDescLPregunta();
      }
      txtDescPregunta.setText(sDes);

      // rellena las descripciones
      if (txtDescGeneral.getText().length() == 0) {
        txtDescGeneral.setText(data.getDescPregunta());
      }
      if (txtDescLocal.getText().length() == 0) {
        txtDescLocal.setText(data.getDescLPregunta());

      }
      bCambioEnPregunta = false;

      // tipo de dato
      tipoPreg.setText(res.getString("tipoPreg.Text") +
                       sTipoDato(data.getTipo(), data.getLongitud(),
                                 data.getEnteros(), data.getDecimales()));

    }

    panel = null;

    modoOperacion = modoOperacionIni;
    Inicializar();

  }
} //fin clase

// action listener de evento en bot�nes
class DialMantUnaLineaModBroteActionListener
    implements ActionListener, Runnable {
  DialMantUnaLineaModBrote adaptee = null;
  ActionEvent e;

  public DialMantUnaLineaModBroteActionListener(DialMantUnaLineaModBrote
                                                adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent ev) {
    this.e = ev;
    Thread th = new Thread(this);
    th.start();
  }

  // evento
  public void run() {
    if (e.getActionCommand().equals("Aceptar")) {
      adaptee.btnAceptar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      adaptee.btnCancelar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("Buscar")) {
      adaptee.btnCtrlCodPregunta_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("pregunta")) { // insertar pregunta
      adaptee.InsertarPregunta();
//    else if (e.getActionCommand().equals("lista")) // insertar lista de valores
//      adaptee.InsertarLista();

    }
  }
}

// control de cambios en el c�digo de pregunta
class DialMantUnaLineaModBroteTxtCodLineaKeyAdapter
    extends java.awt.event.KeyAdapter {
  DialMantUnaLineaModBrote adaptee;

  DialMantUnaLineaModBroteTxtCodLineaKeyAdapter(DialMantUnaLineaModBrote
                                                adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCodPregunta_keyPressed(e);
  }
}

// perdida del foco de una caja de codigo
class ModfocusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  DialMantUnaLineaModBrote adaptee;

  ModfocusAdapter(DialMantUnaLineaModBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost();
  }
}

// lista de valores
class CListaPreg
    extends CListaValores {

  public CListaPreg(CApp a,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataMantPreguntas(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataMantPreguntas) o).getCodPregunta());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataMantPreguntas) o).getDescPregunta());
  }
}
