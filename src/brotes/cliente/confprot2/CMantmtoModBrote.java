package brotes.cliente.confprot2;

import brotes.datos.confprot2.DataModeloBrote;
import capp.CApp;
import capp.CListaMantenimiento;
import comun.constantes;
import sapp.StubSrvBD;

public class CMantmtoModBrote
    extends CListaMantenimiento {

  public CMantmtoModBrote(AppConfprot2 a) { //^^^^^^^^^^^^^^

    super( (CApp) a, //^^^^^^^^^^^^^^^^^^^^^^^^
          3,
          "100\n400\n62",
          a.sEtCodigo + "\n" + a.sEtModelo + "\n" + a.sEtOperativo, //^^^^^^^^
          new StubSrvBD(),
          "servlet/SrvGesModBrote",
          5,
          6);

    //Activa el bot�n auxiliar de la CListaMantenimiento
    ActivarAuxiliar();
  }

  //Constructor que activa/desativa botones en funci�n de autorizaciones
  // del usuario
  public CMantmtoModBrote(AppConfprot2 a, boolean USU) { //^^^^^^^^^^^^^^

    super( (CApp) a, //^^^^^^^^^^^^^^^^^^^^^^^^
          3,
          "100\n400\n62",
          a.sEtCodigo + "\n" + a.sEtModelo + "\n" + a.sEtOperativo, //^^^^^^^^
          new StubSrvBD(),
          "servlet/SrvGesModBrote",
          5,
          6,
          constantes.keyBtnAnadir,
          constantes.keyBtnModificar,
          constantes.keyBtnBorrar,
          constantes.keyBtnAux);

    //Activa el bot�n auxiliar de la CListaMantenimiento
    ActivarAuxiliar();
  }

  // llama a pantalla para confeccionar modelo
  public void btnAuxiliar_actionPerformed(int i) {
    modoOperacion = modoESPERA;
    Inicializar();
    DataModeloBrote dat = (DataModeloBrote) lista.elementAt(i);
    PanelModeloBrote panel = new PanelModeloBrote(this.app,
                                                  new StubSrvBD(),
                                                  dat.getCD_Modelo(),
                                                  dat.getDS_Modelo(),
                                                  dat.getIT_OK());
    panel.show();
    panel = null;

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {
    modoOperacion = modoESPERA;
    Inicializar();
    DialMantModBrote panel = new DialMantModBrote(this.app,
                                                  DialMantModBrote.modoALTA,
                                                  null);
    panel.show();

    if (panel.bAceptar) {
      lista.addElement(panel.datModelo);
      RellenaTabla();
    }

    panel = null;

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // modifica l�nea de la lista
  public void btnModificar_actionPerformed(int i) {

    modoOperacion = modoESPERA;
    Inicializar();
    DialMantModBrote panel = new DialMantModBrote(this.app,
                                                  DialMantModBrote.
                                                  modoMODIFICAR,
                                                  (DataModeloBrote) lista.
                                                  elementAt(i));

    panel.show();

    if (panel.bAceptar) {
      //Si modelo est� operativo se recarga otra vez toda la lista
      //Por si es necesario actualizar a no operat. otros modelos de lista
      if (panel.datModelo.getIT_OK().compareTo("S") == 0) {
        btnCtrlCodModelo_actionPerformed();
      }
      //Si modelo no est� operativo simplemente se modifica ese elemento en lista
      else {
        lista.removeElementAt(i);
        lista.insertElementAt(panel.datModelo, i);
        RellenaTabla();

      } //else

    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed(int i) {

    modoOperacion = modoESPERA;
    Inicializar();
    DialMantModBrote panel = new DialMantModBrote(this.app,
                                                  DialMantModBrote.modoBAJA,
                                                  (DataModeloBrote) lista.
                                                  elementAt(i));
    panel.show();

    if (panel.bAceptar) {
      lista.removeElementAt(i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // rellena los par�metros para enviar al servlet
  public Object setComponente(String s) {
    return new DataModeloBrote(app.getTSive(),
                               s,
                               "",
                               "",
                               "",
                               0, //nmAlerbro
                               "",
                               app.getParametro("CD_TUBERCULOSIS"), //"",
                               "",
                               "",
                               this.app.getParametro("ca"));
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(Object o) {
    DataModeloBrote dat = (DataModeloBrote) o;
    String sLinea = dat.getCD_Modelo() + '&' +
        dat.getDS_Modelo() + '&' +
        dat.getIT_OK();
    return sLinea;
  }

}