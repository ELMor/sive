package brotes.cliente.c_componentes;

import sapp2.Data;

public interface ContCPnlCodigoOtraDesc {

  public String getDescCompleta(Data dat);

}
