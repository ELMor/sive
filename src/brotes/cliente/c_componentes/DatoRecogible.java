/**
 * Interfaz: DatoRecogible
 * Paquete: brotes.cliente.c_componentes
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 10/05/2000
 * Descripcion: Conjunto de funciones que debe implementar un
 *   subcontenedor para comunicar datos entre el mismo y el
 *   contenedor del que depende
 */

package brotes.cliente.c_componentes;

import sapp2.Data;

public interface DatoRecogible {

  // A partir de los datos que hay en un Data rellena los
  //  componentes de un contenedor
  public void rellenarDatos(Data d);

  // Devuelve en un Data los valores introducidos en los componentes
  //  de un contenedor
  public void recogerDatos(Data d);

  // Dev. true si los valores de todos los componentes son correctos
  //  realizando chequeos de campos obligatorios, valores num�ricos,
  //  fechas, etc.
  // Dev. false en caso contrario
  public boolean validarDatos();

} // Fin interfaz DatoRecogible
