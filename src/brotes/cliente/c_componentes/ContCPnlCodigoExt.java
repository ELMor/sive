/*Clase
 * Autor: Luis Rivera
 * Fecha Inicio: 13/01/2000
 * Descripcion: Interfaz que debe implementar cualquier contenedor del PnlCodigoDos
 * NOTA: Adem�s de este tambi�n se pasa el CInicializar
 * Modificaciones:
 *
 *
 **/

package brotes.cliente.c_componentes;

import sapp2.Data;

public interface ContCPnlCodigoExt {

  public void trasEventoEnCPnlCodigoExt();

  public String getDescCompleta(Data dat);

}