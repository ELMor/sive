package brotes.cliente.alibrote;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_comuncliente.BDatos;
import brotes.cliente.c_comuncliente.SincrEventos;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import comun.constantes;
import comun.Common;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

//import brotes.servidor.alibrote.*;

public class PanAlibrote
    extends CDialog
    implements CInicializar, CFiltro {
  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 20;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Botones
  PanAlibroteActionAdapter actionAdapter = new PanAlibroteActionAdapter(this);

  // modo de operaci�n
  public int modoOperacion = 0;
  public int modoBloqueo = modoNORMAL;

  public Data dtDev = null;
  public Lista lClm = new Lista();
  public Lista lClmBaja = new Lista();
  public Lista lBd = new Lista();

  // estructuras para comprobar el bloqueo
  private QueryTool qtBloqueo = null;
  private Data dtBloqueo = null;

  //imagenes a insertar en la tabla
  final String imgCANCELAR = "images/cancelar.gif";
  final String imgACEPTAR = "images/aceptar.gif";

  final String servletAlibrote = "servlet/SrvPanAliBrote";

  XYLayout xyLayout = new XYLayout();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  CListaMantenimiento clmAlibrote = null;

  Label lblBrote = new Label();
  Label lblDatosBrote = new Label();

  public PanAlibrote(CApp a, int modo, Data dataBusq) {
    super(a);
    modoOperacion = modo;
    dtDev = dataBusq;
    setTitle("Brotes: Alimentos");
    try {
      //Configuro la CListaMantenimento
      Vector vBotones = new Vector();
      Vector vLabels = new Vector();
      // botones
      vBotones.addElement(new CBoton(constantes.keyBtnAnadir,
                                     "images/alta2.gif",
                                     "Nuevo alimento",
                                     false,
                                     false));

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion2.gif",
                                     "Modificar alimento",
                                     true,
                                     true));

      vBotones.addElement(new CBoton(constantes.keyBtnBorrar,
                                     "images/baja2.gif",
                                     "Borrar alimento",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("Descripci�n Alimento",
                                      "200",
                                      "DS_ALI"));

      vLabels.addElement(new CColumna("Tipo Alimento",
                                      "200",
                                      "TIPO_ALIMENTO"));

      vLabels.addElement(new CColumna("Estado",
                                      "200",
                                      "ESTADO"));

      clmAlibrote = new CListaMantenimiento(a,
                                            vLabels,
                                            vBotones,
                                            this,
                                            this,
                                            250,
                                            640);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor

  void jbInit() throws Exception {

    // carga la imagen
    this.getApp().getLibImagenes().put(imgACEPTAR);
    this.getApp().getLibImagenes().put(imgCANCELAR);
    this.getApp().getLibImagenes().CargaImagenes();

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));
    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));
    btnCancelar.addActionListener(actionAdapter);

    this.setSize(680, 385);
    xyLayout.setHeight(385);
    xyLayout.setWidth(680);
    this.setLayout(xyLayout);

    lblBrote.setText("Brote:");
    lblDatosBrote.setText(dtDev.getString("CD_ANO") + "/" +
                          dtDev.getString("NM_ALERBRO") + " - " +
                          dtDev.getString("DS_BROTE"));

    // a�ade los componentes
    this.add(lblBrote, new XYConstraints(MARGENIZQ, MARGENSUP, 80, ALTO));
    this.add(lblDatosBrote,
             new XYConstraints(MARGENIZQ + 80 + INTERHOR, MARGENSUP, 300, ALTO));
    this.add(clmAlibrote,
             new XYConstraints(MARGENIZQ - 10, MARGENSUP + ALTO + 2 * INTERVERT,
                               650, 230));
    this.add(btnAceptar,
             new XYConstraints(535 - 90 - INTERVERT,
                               MARGENSUP + ALTO + 4 * INTERVERT + 230, 88, 29));
    this.add(btnCancelar,
             new XYConstraints(548, MARGENSUP + ALTO + 4 * INTERVERT + 230, 88,
                               29));

    //Se a�aden a la lista los factores ya existentes.
    Inicializar(CInicializar.ESPERA);
    clmAlibrote.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);

  } //end jbinit

  public void Inicializar() {}

  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if ( (modoOperacion == BAJA) || (modoOperacion == CONSULTA)) {
          this.setEnabled(false);
          btnAceptar.setVisible(false);
          btnCancelar.setLabel("Salir");
          btnCancelar.setEnabled(true);
          clmAlibrote.setEnabled(false);
        }
        else {
          clmAlibrote.setEnabled(true);
          btnAceptar.setEnabled(true);
          btnCancelar.setEnabled(true);
        }
    }
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvQueryTool";

    try {
      Data fila = null;
      fila = new Data();
      QueryTool2 qt = new QueryTool2();
      qt.putName("SIVE_ALI_BROTE");

      qt.putType("NM_ALIBROTE", QueryTool.INTEGER);
      qt.putType("CD_ANO", QueryTool.STRING);
      qt.putType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putType("CD_TALIMENTO", QueryTool.STRING);
      qt.putType("CD_TIMPLICACION", QueryTool.STRING);
      qt.putType("DS_NOMCOMER", QueryTool.STRING);
      qt.putType("DS_FABRICAN", QueryTool.STRING);
      qt.putType("DS_LOTE", QueryTool.STRING);
      qt.putType("CD_MCOMERCALI", QueryTool.STRING);
      qt.putType("CD_TRATPREVALI", QueryTool.STRING);
      qt.putType("CD_FINGERIRALI", QueryTool.STRING);
      qt.putType("CD_MTVIAJEALI", QueryTool.STRING);
      qt.putType("CD_LCONTAMIALI", QueryTool.STRING);
      qt.putType("CD_PAIS_LCONA", QueryTool.STRING);
      qt.putType("CD_LPREPALI", QueryTool.STRING);
      qt.putType("CD_PAIS_LPA", QueryTool.STRING);
      qt.putType("DS_NOMESTAB", QueryTool.STRING);
      qt.putType("DS_DIRESTAB", QueryTool.STRING);
      qt.putType("CD_POSESTAB", QueryTool.STRING);
      qt.putType("CD_PROV_LPREP", QueryTool.STRING);
      qt.putType("CD_MUN_LPREP", QueryTool.STRING);
      qt.putType("DS_TELEF_LPREP", QueryTool.STRING);
      qt.putType("FC_PREPARACION", QueryTool.TIMESTAMP);
      qt.putType("CD_CONSUMOALI", QueryTool.STRING);
      qt.putType("CD_PAIS_LCON", QueryTool.STRING);
      qt.putType("DS_ESTAB_LCON", QueryTool.STRING);
      qt.putType("DS_DIRESTAB_LCON", QueryTool.STRING);
      qt.putType("CD_POSESTAB_LCON", QueryTool.STRING);
      qt.putType("CD_PROV_LCON", QueryTool.STRING);
      qt.putType("CD_MUN_LCON", QueryTool.STRING);
      qt.putType("DS_TELEF_LCON", QueryTool.STRING);
      qt.putType("CD_PAIS_DE", QueryTool.STRING);
      qt.putType("CD_PAIS_A", QueryTool.STRING);
      qt.putType("IT_CALCTASA", QueryTool.STRING);
      qt.putType("NM_EXPENF", QueryTool.INTEGER);
      qt.putType("NM_EXPNOENF", QueryTool.INTEGER);
      qt.putType("NM_NOEXPENF", QueryTool.INTEGER);
      qt.putType("NM_NOEXPNOENF", QueryTool.INTEGER);
      qt.putType("DS_ALI", QueryTool.STRING);

      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", dtDev.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");

      qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putWhereValue("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");

      //Datos adicionales de alerta de la tabla SIVE_FACT_CONTRIB
      QueryTool qtAdic1 = new QueryTool();
      qtAdic1.putName("SIVE_TALIMENTO");
      qtAdic1.putType("DS_TALIMENTO", QueryTool.STRING);

      Data dtAdic1 = new Data();
      dtAdic1.put("CD_TALIMENTO", QueryTool.STRING);
      qt.addQueryTool(qtAdic1);
      qt.addColumnsQueryTool(dtAdic1);

      //Datos adicionales de alerta de la tabla SIVE_FACT_CONTRIB
      QueryTool qtAdic2 = new QueryTool();
      qtAdic2.putName("SIVE_T_IMPLICACION");
      qtAdic2.putType("DS_TIMPLICACION", QueryTool.STRING);

      Data dtAdic2 = new Data();
      dtAdic2.put("CD_TIMPLICACION", QueryTool.STRING);
      qt.addQueryTool(qtAdic2);
      qt.addColumnsQueryTool(dtAdic2);

      //Datos adicionales de alerta de la tabla SIVE_FACT_CONTRIB
      QueryTool qtAdic3 = new QueryTool();
      qtAdic3.putName("SIVE_MCOMERC_ALI");
      qtAdic3.putType("DS_MCOMERCALI", QueryTool.STRING);

      Data dtAdic3 = new Data();
      dtAdic3.put("CD_MCOMERCALI", QueryTool.STRING);
      qt.addQueryTool(qtAdic3);
      qt.addColumnsQueryTool(dtAdic3);

      //Datos adicionales de alerta de la tabla SIVE_FACT_CONTRIB
      QueryTool qtAdic4 = new QueryTool();
      qtAdic4.putName("SIVE_TRAT_PREVIO_ALI");
      qtAdic4.putType("DS_TRATPREVALI", QueryTool.STRING);

      Data dtAdic4 = new Data();
      dtAdic4.put("CD_TRATPREVALI", QueryTool.STRING);
      qt.addQueryTool(qtAdic4);
      qt.addColumnsQueryTool(dtAdic4);

      //Datos adicionales de alerta de la tabla SIVE_FACT_CONTRIB
      QueryTool qtAdic5 = new QueryTool();
      qtAdic5.putName("SIVE_F_INGERIR_ALI");
      qtAdic5.putType("DS_FINGERIRALI", QueryTool.STRING);

      Data dtAdic5 = new Data();
      dtAdic5.put("CD_FINGERIRALI", QueryTool.STRING);
      qt.addQueryTool(qtAdic5);
      qt.addColumnsQueryTool(dtAdic5);

      //Datos adicionales de alerta de la tabla SIVE_FACT_CONTRIB
      QueryTool qtAdic6 = new QueryTool();
      qtAdic6.putName("SIVE_LCONTAMI_ALI");
      qtAdic6.putType("DS_LCONTAMIALI", QueryTool.STRING);

      Data dtAdic6 = new Data();
      dtAdic6.put("CD_LCONTAMIALI", QueryTool.STRING);
      qt.addQueryTool(qtAdic6);
      qt.addColumnsQueryTool(dtAdic6);

      /*            //Datos adicionales de alerta de la tabla SIVE_FACT_CONTRIB
                  QueryTool qtAdic7 = new QueryTool();
                  qtAdic7.putName("SIVE_PAISES");
                  qtAdic7.putType("DS_PAIS",QueryTool.STRING);
                  Data dtAdic7 = new Data();
                  dtAdic7.put("CD_PAIS", QueryTool.STRING);
                  qt.addQueryTool(qtAdic7);
                  qt.addColumnsQueryTool(dtAdic7);*/

      //Datos adicionales de alerta de la tabla SIVE_FACT_CONTRIB
      QueryTool qtAdic8 = new QueryTool();
      qtAdic8.putName("SIVE_LPREP_ALI");
      qtAdic8.putType("DS_LPREPALI", QueryTool.STRING);

      Data dtAdic8 = new Data();
      dtAdic8.put("CD_LPREPALI", QueryTool.STRING);
      qt.addQueryTool(qtAdic8);
      qt.addColumnsQueryTool(dtAdic8);

      /*            //Datos adicionales de alerta de la tabla SIVE_FACT_CONTRIB
                  QueryTool qtAdic9 = new QueryTool();
                  qtAdic9.putName("SIVE_MUNICIPIO");
                  qtAdic9.putType("DS_MUN",QueryTool.STRING);
                  Data dtAdic9 = new Data();
                  dtAdic9.put("CD_MUN", QueryTool.STRING);
                  qt.addQueryTool(qtAdic9);
                  qt.addColumnsQueryTool(dtAdic9);*/

      //Datos adicionales de alerta de la tabla SIVE_FACT_CONTRIB
      QueryTool qtAdic10 = new QueryTool();
      qtAdic10.putName("SIVE_LCONSUMO_ALI");
      qtAdic10.putType("DS_CONSUMOALI", QueryTool.STRING);

      Data dtAdic10 = new Data();
      dtAdic10.put("CD_CONSUMOALI", QueryTool.STRING);
      qt.addQueryTool(qtAdic10);
      qt.addColumnsQueryTool(dtAdic10);

      //Datos adicionales de alerta de la tabla SIVE_FACT_CONTRIB
      QueryTool qtAdic11 = new QueryTool();
      qtAdic11.putName("SIVE_MTVIAJE_ALI");
      qtAdic11.putType("DS_MTVIAJEALI", QueryTool.STRING);

      Data dtAdic11 = new Data();
      dtAdic11.put("CD_MTVIAJEALI", QueryTool.STRING);
      qt.addQueryTool(qtAdic11);
      qt.addColumnsQueryTool(dtAdic11);

      qt.addOrderField("DS_ALI");

      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() == 0) {
//            this.getApp().showAdvise("No hay datos con esta entrada");
      }
      else {
        p1 = anadirTipoOperacion(p1);
        p1 = anadirTipoAlimento(p1);
        p1 = anadirEstado(p1);
        lClm = p1;
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      ex.printStackTrace();

    }
    return p1;
  } //end primerapagina

  //Tipo Operacion=BD
  Lista anadirTipoOperacion(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      ( (Data) lCampo.elementAt(i)).put("TIPO_OPERACION", "BD");
    } //end for
    return lCampo;
  } //end anadirTipoOperacion

  //concatena c�digo y descripci�n de alimento
  Lista anadirTipoAlimento(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = (Data) lCampo.elementAt(i);
      ( (Data) lCampo.elementAt(i)).put("TIPO_ALIMENTO",
                                        dCampo.getString("CD_TALIMENTO") +
                                        " - " + dCampo.getString("DS_TALIMENTO"));
    } //end for

    return lCampo;
  } //end anadirTipoAlimento

  //concatena c�digo y descripci�n de t_implicacion
  Lista anadirEstado(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = (Data) lCampo.elementAt(i);
      if (dCampo.getString("CD_TIMPLICACION").length() != 0) {
        ( (Data) lCampo.elementAt(i)).put("ESTADO",
                                          dCampo.getString("CD_TIMPLICACION") +
                                          " - " +
                                          dCampo.getString("DS_TIMPLICACION"));
      }
    } //end for
    return lCampo;
  } //end anadirTipoAlimento

  public Lista siguientePagina() {
    Lista l = new Lista();
    return l;
  }

  public void realizaOperacion(int j) {
    DiaAlibrote di = null;
    Data dtAlibrote = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:

//        dtAlibrote.put("CD_GRUPO",dtDev.getString("CD_GRUPO"));
        dtAlibrote.put("CD_ANO", dtDev.getString("CD_ANO"));
        dtAlibrote.put("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
//        di = new DiaAlibrote(this.getApp(), ALTA, dtAlibrote);
        di = new DiaAlibrote(this.getApp(), ALTA, dtAlibrote, lClm);
        di.show();
        // a�adir el nuevo elem a la lista
        if (di.bAceptar) {
          Data dtDevuelto = di.dtDevolver();
          lClm.addElement(dtDevuelto);
          lClm = anadirTipoAlimento(lClm);
          lClm = anadirEstado(lClm);
          clmAlibrote.setPrimeraPagina(lClm);
          //clmAlibrote.setPrimeraPagina(primeraPagina());
        }
        break;
        /*      // bot�n de consulta
              case CONSULTA:
                dtAlibrote = clmAlibrote.getSelected();
                if (dtAlibrote!=null){
                  int ind= clmAlibrote.getSelectedIndex();
                  di = new DiaAlibrote(this.getApp(), CONSULTA, dtAlibrote);
                  di.show();
                 }
                break;*/

        // bot�n de modificaci�n
      case MODIFICACION:
        dtAlibrote = clmAlibrote.getSelected();
        //si existe alguna fila seleccionada
        if (dtAlibrote != null) {
          int ind = clmAlibrote.getSelectedIndex();
          di = new DiaAlibrote(this.getApp(), MODIFICACION, dtAlibrote, lClm);
          di.show();
          //Tratamiento de bAceptar para Salir
          if (di.bAceptar) {
            Data dtDevuelto = di.dtDevolver();
            lClm.removeElementAt(ind);
            lClm.addElement(dtDevuelto);
            lClm = anadirTipoAlimento(lClm);
            lClm = anadirEstado(lClm);
            clmAlibrote.setPrimeraPagina(lClm);
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dtAlibrote = clmAlibrote.getSelected();
        if (dtAlibrote != null) {
          int ind = clmAlibrote.getSelectedIndex();
          di = new DiaAlibrote(this.getApp(), BAJA, dtAlibrote);
          di.show();
          if (di.bAceptar) {
            Data dtDevuelto = di.dtDevolver();
            //si no ha sido borrado sino marcado
            if (dtDevuelto.getString("TIPO_OPERACION").equals("M")) {
              Data dtDevuelto1 = di.dtDevolver();
              lClm.removeElementAt(ind);
              lClm.addElement(dtDevuelto1);
              lClm = anadirTipoAlimento(lClm);
              lClm = anadirEstado(lClm);
              clmAlibrote.setPrimeraPagina(lClm);
            }
            else { //si ha sido borrado
              if (dtDevuelto.getString("TIPO_OPERACION").equals("A")) {
                Data dtDevuelto2 = di.dtDevolver();
                lClm.removeElementAt(ind);
                lClm.addElement(dtDevuelto2);
                lClm = anadirTipoAlimento(lClm);
                lClm = anadirEstado(lClm);
                clmAlibrote.setPrimeraPagina(lClm);
              }
              else {
                lClm.removeElementAt(ind);
                if (dtDevuelto.getString("TIPO_OPERACION").equals("B")) {
                  lClmBaja.addElement(dtAlibrote);
                }
                clmAlibrote.setPrimeraPagina(lClm);
              }
            }
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
        }
        break;

    }
  }

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  //funci�n q genera la query y el data necesarios para el bloqueo
  private void preparaBloqueo() {
    // query tool
    qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", dtDev.getString("CD_ANO"));
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
    qtBloqueo.putOperator("NM_ALERBRO", "=");

    // informaci�n de bloqueo
    dtBloqueo = new Data();
    dtBloqueo.put("CD_OPE", dtDev.getString("CD_OPE"));
    dtBloqueo.put("FC_ULTACT", dtDev.getString("FC_ULTACT"));
  } //end prepara bloqueo

  void btnAceptarActionPerformed() {
    Inicializar(CInicializar.ESPERA);
    Lista vResultado = new Lista();
    //recorro la lista q tengo para altas y modificaciones
    for (int i = 0; i < lClm.size(); i++) {
      Data dtClm = (Data) lClm.elementAt(i);

      //Se realiza un alta
      if (dtClm.getString("TIPO_OPERACION").equals("A")) {
        QueryTool qtIns = new QueryTool();
        qtIns = realizarInsert(dtClm);
        Data dtInsert = new Data();
        dtInsert.put("10003", qtIns);
        lBd.addElement(dtInsert);
      } //end if ALTA

      if (dtClm.getString("TIPO_OPERACION").equals("M")) {
        QueryTool qtUpd = new QueryTool();
        qtUpd = realizarUpdate(dtClm);
        Data dtUpdate = new Data();
        dtUpdate.put("10006", qtUpd);
        lBd.addElement(dtUpdate);
      }
    } //end for
    //lista para bajas
    for (int i = 0; i < lClmBaja.size(); i++) {
      Data dtClmBaja = (Data) lClmBaja.elementAt(i);
      QueryTool qtDel = new QueryTool();
      qtDel = realizarDelete(dtClmBaja);
      Data dtDel = new Data();
      dtDel.put("10007", qtDel);
      lBd.addElement(dtDel);
    } //end for lBajas

    //si se ha modificado algo:se inserta para ese brote el nuevo
    //cd_ope y fc_ultact
    if (lBd.size() != 0) {
      Data dtDatBrot = new Data();
      dtDatBrot.put("CD_OPE", dtDev.getString("CD_OPE"));
      dtDatBrot.put("FC_ULTACT", "");

      QueryTool qtUpdBro = new QueryTool();
      qtUpdBro = realizarUpdateBrote(dtDatBrot);
      Data dtUpdBro = new Data();
      dtUpdBro.put("10006", qtUpdBro);
      lBd.addElement(dtUpdBro);
//    }

      preparaBloqueo();
      try {
        //BDatos.ejecutaSQL(false,this.getApp(),servletAlibrote,0,lBd);
        /*            SrvPanAliBrote servlet=new SrvPanAliBrote();
             servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                    vResultado = (Lista) servlet.doDebug(0,lBd);*/
//            vResultado = (Lista)this.getApp().getStub().doPost(0,lBd);

        this.getApp().getStub().setUrl(servletAlibrote);
        vResultado = (Lista)this.getApp().getStub().doPost(10000,
            lBd,
            qtBloqueo,
            dtBloqueo,
            getApp());

        //obtener el nuevo cd_ope y fc_ultact de la base de datos.
        int tamano = vResultado.size();
        String fecha = null;
        for (int i = 0; i < vResultado.size(); i++) {
          fecha = null;
          fecha = ( (Lista) vResultado.elementAt(i)).getFC_ULTACT();
        }
        dtDev.put("CD_OPE", dtDev.getString("CD_OPE"));
        dtDev.put("FC_ULTACT", fecha);
        dispose();
      }
      catch (Exception ex) {
        if (Common.ShowPregunta(this.getApp(), "Los datos han sido modificados por otro usuario. �Sigue queriendo realizar esta actualizaci�n?")) {
          try {
            ModosOperacNormal();
            this.getApp().getStub().setUrl(servletAlibrote);
            vResultado = (Lista)this.getApp().getStub().doPost(0, lBd);
            dispose();
          }
          catch (Exception exc) {
            this.getApp().trazaLog(exc);
            this.getApp().showError(ex.getMessage());
            dispose();
          } //end 2.catch
        }
        else {
          dispose();
        } //end if comun.showpregunta
      } //end 1. catch
    }
    else {
      dispose();
    } //end if se han realizado cambios
    Inicializar(CInicializar.NORMAL);
  } //end btnAceptar

  //sin el bloqueo
  public void ModosOperacNormal() {
    for (int i = 0; i < lClm.size(); i++) {
      Data dtClm = (Data) lClm.elementAt(i);

      //Se realiza un alta
      if (dtClm.getString("TIPO_OPERACION").equals("A")) {
        QueryTool qtIns = new QueryTool();
        qtIns = realizarInsert(dtClm);
        Data dtInsert = new Data();
        dtInsert.put("3", qtIns);
        lBd.addElement(dtInsert);
      } //end if ALTA
      if (dtClm.getString("TIPO_OPERACION").equals("M")) {
        QueryTool qtUpd = new QueryTool();
        qtUpd = realizarUpdate(dtClm);
        Data dtUpdate = new Data();
        dtUpdate.put("4", qtUpd);
        lBd.addElement(dtUpdate);
      }
    } //end for
    //lista para bajas
    for (int i = 0; i < lClmBaja.size(); i++) {
      Data dtClmBaja = (Data) lClmBaja.elementAt(i);
      QueryTool qtDel = new QueryTool();
      qtDel = realizarDelete(dtClmBaja);
      Data dtDel = new Data();
      dtDel.put("5", qtDel);
      lBd.addElement(dtDel);
    } //end for lBajas
    /*    if (lBd.size()!=0){
            Data dtDatBrot=new Data();
            dtDatBrot.put("CD_OPE",dtDev.getString("CD_OPE"));
            dtDatBrot.put("FC_ULTACT","");
            QueryTool qtUpdBro=new QueryTool();
            qtUpdBro=realizarUpdateBrote(dtDatBrot);
            Data dtUpdBro = new Data();
            dtUpdBro.put("4",qtUpdBro);
            lBd.addElement(dtUpdBro);
        }*/
  } //end modosOperacNormal

  QueryTool realizarDelete(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_ALI_BROTE");

    qtModif.putWhereType("NM_ALIBROTE", QueryTool.INTEGER);
    qtModif.putWhereValue("NM_ALIBROTE", dtUpd.getString("NM_ALIBROTE"));
    qtModif.putOperator("NM_ALIBROTE", "=");
    return qtModif;
  }

  QueryTool realizarInsert(Data dtUpd) {
    QueryTool qtIns = new QueryTool();
    qtIns.putName("SIVE_ALI_BROTE");
    qtIns.putType("NM_ALIBROTE", QueryTool.INTEGER);
    qtIns.putValue("NM_ALIBROTE", dtUpd.getString("NM_ALIBROTE"));
    qtIns.putType("CD_ANO", QueryTool.STRING);
    qtIns.putValue("CD_ANO", dtUpd.getString("CD_ANO"));
    qtIns.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtIns.putValue("NM_ALERBRO", dtUpd.getString("NM_ALERBRO"));
    qtIns.putType("CD_TALIMENTO", QueryTool.STRING);
    qtIns.putValue("CD_TALIMENTO", dtUpd.getString("CD_TALIMENTO"));
    qtIns.putType("CD_TIMPLICACION", QueryTool.STRING);
    qtIns.putValue("CD_TIMPLICACION", dtUpd.getString("CD_TIMPLICACION"));
    qtIns.putType("DS_NOMCOMER", QueryTool.STRING);
    qtIns.putValue("DS_NOMCOMER", dtUpd.getString("DS_NOMCOMER"));
    qtIns.putType("DS_FABRICAN", QueryTool.STRING);
    qtIns.putValue("DS_FABRICAN", dtUpd.getString("DS_FABRICAN"));
    qtIns.putType("DS_LOTE", QueryTool.STRING);
    qtIns.putValue("DS_LOTE", dtUpd.getString("DS_LOTE"));
    qtIns.putType("CD_MCOMERCALI", QueryTool.STRING);
    qtIns.putValue("CD_MCOMERCALI", dtUpd.getString("CD_MCOMERCALI"));
    qtIns.putType("CD_TRATPREVALI", QueryTool.STRING);
    qtIns.putValue("CD_TRATPREVALI", dtUpd.getString("CD_TRATPREVALI"));
    qtIns.putType("CD_FINGERIRALI", QueryTool.STRING);
    qtIns.putValue("CD_FINGERIRALI", dtUpd.getString("CD_FINGERIRALI"));
    qtIns.putType("CD_MTVIAJEALI", QueryTool.STRING);
    qtIns.putValue("CD_MTVIAJEALI", dtUpd.getString("CD_MTVIAJEALI"));
    qtIns.putType("CD_LCONTAMIALI", QueryTool.STRING);
    qtIns.putValue("CD_LCONTAMIALI", dtUpd.getString("CD_LCONTAMIALI"));
    qtIns.putType("CD_PAIS_LCONA", QueryTool.STRING);
    qtIns.putValue("CD_PAIS_LCONA", dtUpd.getString("CD_PAIS_LCONA"));
    qtIns.putType("CD_LPREPALI", QueryTool.STRING);
    qtIns.putValue("CD_LPREPALI", dtUpd.getString("CD_LPREPALI"));
    qtIns.putType("CD_PAIS_LPA", QueryTool.STRING);
    qtIns.putValue("CD_PAIS_LPA", dtUpd.getString("CD_PAIS_LPA"));
    qtIns.putType("DS_NOMESTAB", QueryTool.STRING);
    qtIns.putValue("DS_NOMESTAB", dtUpd.getString("DS_NOMESTAB"));
    qtIns.putType("DS_DIRESTAB", QueryTool.STRING);
    qtIns.putValue("DS_DIRESTAB", dtUpd.getString("DS_DIRESTAB"));
    qtIns.putType("CD_POSESTAB", QueryTool.STRING);
    qtIns.putValue("CD_POSESTAB", dtUpd.getString("CD_POSESTAB"));
    qtIns.putType("CD_PROV_LPREP", QueryTool.STRING);
    qtIns.putValue("CD_PROV_LPREP", dtUpd.getString("CD_PROV_LPREP"));
    qtIns.putType("CD_MUN_LPREP", QueryTool.STRING);
    qtIns.putValue("CD_MUN_LPREP", dtUpd.getString("CD_MUN_LPREP"));
    qtIns.putType("DS_TELEF_LPREP", QueryTool.STRING);
    qtIns.putValue("DS_TELEF_LPREP", dtUpd.getString("DS_TELEF_LPREP"));
    qtIns.putType("FC_PREPARACION", QueryTool.TIMESTAMP);
    qtIns.putValue("FC_PREPARACION", dtUpd.getString("FC_PREPARACION"));
    qtIns.putType("CD_CONSUMOALI", QueryTool.STRING);
    qtIns.putValue("CD_CONSUMOALI", dtUpd.getString("CD_CONSUMOALI"));
    qtIns.putType("CD_PAIS_LCON", QueryTool.STRING);
    qtIns.putValue("CD_PAIS_LCON", dtUpd.getString("CD_PAIS_LCON"));
    qtIns.putType("DS_ESTAB_LCON", QueryTool.STRING);
    qtIns.putValue("DS_ESTAB_LCON", dtUpd.getString("DS_ESTAB_LCON"));
    qtIns.putType("DS_DIRESTAB_LCON", QueryTool.STRING);
    qtIns.putValue("DS_DIRESTAB_LCON", dtUpd.getString("DS_DIRESTAB_LCON"));
    qtIns.putType("CD_POSESTAB_LCON", QueryTool.STRING);
    qtIns.putValue("CD_POSESTAB_LCON", dtUpd.getString("CD_POSESTAB_LCON"));
    qtIns.putType("CD_PROV_LCON", QueryTool.STRING);
    qtIns.putValue("CD_PROV_LCON", dtUpd.getString("CD_PROV_LCON"));
    qtIns.putType("CD_MUN_LCON", QueryTool.STRING);
    qtIns.putValue("CD_MUN_LCON", dtUpd.getString("CD_MUN_LCON"));
    qtIns.putType("DS_TELEF_LCON", QueryTool.STRING);
    qtIns.putValue("DS_TELEF_LCON", dtUpd.getString("DS_TELEF_LCON"));
    qtIns.putType("CD_PAIS_DE", QueryTool.STRING);
    qtIns.putValue("CD_PAIS_DE", dtUpd.getString("CD_PAIS_DE"));
    qtIns.putType("CD_PAIS_A", QueryTool.STRING);
    qtIns.putValue("CD_PAIS_A", dtUpd.getString("CD_PAIS_A"));
    qtIns.putType("IT_CALCTASA", QueryTool.STRING);
    qtIns.putValue("IT_CALCTASA", dtUpd.getString("IT_CALCTASA"));
    qtIns.putType("NM_EXPENF", QueryTool.INTEGER);
    qtIns.putValue("NM_EXPENF", "");
    qtIns.putType("NM_EXPNOENF", QueryTool.INTEGER);
    qtIns.putValue("NM_EXPNOENF", "");
    qtIns.putType("NM_NOEXPENF", QueryTool.INTEGER);
    qtIns.putValue("NM_NOEXPENF", "");
    qtIns.putType("NM_NOEXPNOENF", QueryTool.INTEGER);
    qtIns.putValue("NM_NOEXPNOENF", "");
    qtIns.putType("DS_ALI", QueryTool.STRING);
    qtIns.putValue("DS_ALI", dtUpd.getString("DS_ALI"));

    return qtIns;
  }

  QueryTool realizarUpdate(Data dtUpd) {
    QueryTool qtModif = new QueryTool();

    qtModif.putName("SIVE_ALI_BROTE");
    qtModif.putType("CD_TALIMENTO", QueryTool.STRING);
    qtModif.putValue("CD_TALIMENTO", dtUpd.getString("CD_TALIMENTO"));
    qtModif.putType("CD_TIMPLICACION", QueryTool.STRING);
    qtModif.putValue("CD_TIMPLICACION", dtUpd.getString("CD_TIMPLICACION"));
    qtModif.putType("DS_NOMCOMER", QueryTool.STRING);
    qtModif.putValue("DS_NOMCOMER", dtUpd.getString("DS_NOMCOMER"));
    qtModif.putType("DS_FABRICAN", QueryTool.STRING);
    qtModif.putValue("DS_FABRICAN", dtUpd.getString("DS_FABRICAN"));
    qtModif.putType("DS_LOTE", QueryTool.STRING);
    qtModif.putValue("DS_LOTE", dtUpd.getString("DS_LOTE"));
    qtModif.putType("CD_MCOMERCALI", QueryTool.STRING);
    qtModif.putValue("CD_MCOMERCALI", dtUpd.getString("CD_MCOMERCALI"));
    qtModif.putType("CD_TRATPREVALI", QueryTool.STRING);
    qtModif.putValue("CD_TRATPREVALI", dtUpd.getString("CD_TRATPREVALI"));
    qtModif.putType("CD_FINGERIRALI", QueryTool.STRING);
    qtModif.putValue("CD_FINGERIRALI", dtUpd.getString("CD_FINGERIRALI"));
    qtModif.putType("CD_MTVIAJEALI", QueryTool.STRING);
    qtModif.putValue("CD_MTVIAJEALI", dtUpd.getString("CD_MTVIAJEALI"));
    qtModif.putType("CD_LCONTAMIALI", QueryTool.STRING);
    qtModif.putValue("CD_LCONTAMIALI", dtUpd.getString("CD_LCONTAMIALI"));
    qtModif.putType("CD_PAIS_LCONA", QueryTool.STRING);
    qtModif.putValue("CD_PAIS_LCONA", dtUpd.getString("CD_PAIS_LCONA"));
    qtModif.putType("CD_LPREPALI", QueryTool.STRING);
    qtModif.putValue("CD_LPREPALI", dtUpd.getString("CD_LPREPALI"));
    qtModif.putType("CD_PAIS_LPA", QueryTool.STRING);
    qtModif.putValue("CD_PAIS_LPA", dtUpd.getString("CD_PAIS_LPA"));
    qtModif.putType("DS_NOMESTAB", QueryTool.STRING);
    qtModif.putValue("DS_NOMESTAB", dtUpd.getString("DS_NOMESTAB"));
    qtModif.putType("DS_DIRESTAB", QueryTool.STRING);
    qtModif.putValue("DS_DIRESTAB", dtUpd.getString("DS_DIRESTAB"));
    qtModif.putType("CD_POSESTAB", QueryTool.STRING);
    qtModif.putValue("CD_POSESTAB", dtUpd.getString("CD_POSESTAB"));
    qtModif.putType("CD_PROV_LPREP", QueryTool.STRING);
    qtModif.putValue("CD_PROV_LPREP", dtUpd.getString("CD_PROV_LPREP"));
    qtModif.putType("CD_MUN_LPREP", QueryTool.STRING);
    qtModif.putValue("CD_MUN_LPREP", dtUpd.getString("CD_MUN_LPREP"));
    qtModif.putType("DS_TELEF_LPREP", QueryTool.STRING);
    qtModif.putValue("DS_TELEF_LPREP", dtUpd.getString("DS_TELEF_LPREP"));
    qtModif.putType("FC_PREPARACION", QueryTool.TIMESTAMP);
    qtModif.putValue("FC_PREPARACION", dtUpd.getString("FC_PREPARACION"));
    qtModif.putType("CD_CONSUMOALI", QueryTool.STRING);
    qtModif.putValue("CD_CONSUMOALI", dtUpd.getString("CD_CONSUMOALI"));
    qtModif.putType("CD_PAIS_LCON", QueryTool.STRING);
    qtModif.putValue("CD_PAIS_LCON", dtUpd.getString("CD_PAIS_LCON"));
    qtModif.putType("DS_ESTAB_LCON", QueryTool.STRING);
    qtModif.putValue("DS_ESTAB_LCON", dtUpd.getString("DS_ESTAB_LCON"));
    qtModif.putType("DS_DIRESTAB_LCON", QueryTool.STRING);
    qtModif.putValue("DS_DIRESTAB_LCON", dtUpd.getString("DS_DIRESTAB_LCON"));
    qtModif.putType("CD_POSESTAB_LCON", QueryTool.STRING);
    qtModif.putValue("CD_POSESTAB_LCON", dtUpd.getString("CD_POSESTAB_LCON"));
    qtModif.putType("CD_PROV_LCON", QueryTool.STRING);
    qtModif.putValue("CD_PROV_LCON", dtUpd.getString("CD_PROV_LCON"));
    qtModif.putType("CD_MUN_LCON", QueryTool.STRING);
    qtModif.putValue("CD_MUN_LCON", dtUpd.getString("CD_MUN_LCON"));
    qtModif.putType("DS_TELEF_LCON", QueryTool.STRING);
    qtModif.putValue("DS_TELEF_LCON", dtUpd.getString("DS_TELEF_LCON"));
    qtModif.putType("CD_PAIS_DE", QueryTool.STRING);
    qtModif.putValue("CD_PAIS_DE", dtUpd.getString("CD_PAIS_DE"));
    qtModif.putType("CD_PAIS_A", QueryTool.STRING);
    qtModif.putValue("CD_PAIS_A", dtUpd.getString("CD_PAIS_A"));
    qtModif.putType("DS_ALI", QueryTool.STRING);
    qtModif.putValue("DS_ALI", dtUpd.getString("DS_ALI"));

    qtModif.putWhereType("NM_ALIBROTE", QueryTool.INTEGER);
    qtModif.putWhereValue("NM_ALIBROTE", dtUpd.getString("NM_ALIBROTE"));
    qtModif.putOperator("NM_ALIBROTE", "=");

    return qtModif;
  }

  QueryTool realizarUpdateBrote(Data dtUpd) {
    QueryTool qtUpd = new QueryTool();
    qtUpd.putName("SIVE_BROTES");
    qtUpd.putType("CD_OPE", QueryTool.STRING);
    qtUpd.putValue("CD_OPE", dtUpd.getString("CD_OPE"));
    qtUpd.putType("FC_ULTACT", QueryTool.TIMESTAMP);
    qtUpd.putValue("FC_ULTACT", dtUpd.getString("FC_ULTACT"));

    // filtro
    qtUpd.putWhereType("CD_ANO", QueryTool.STRING);
    qtUpd.putWhereValue("CD_ANO", dtDev.getString("CD_ANO"));
    qtUpd.putOperator("CD_ANO", "=");

    qtUpd.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtUpd.putWhereValue("NM_ALERBRO", dtDev.getString("NM_ALERBRO"));
    qtUpd.putOperator("NM_ALERBRO", "=");

    return qtUpd;
  }

  void btnCancelarActionPerformed() {
    dispose();
  }

} //end class

class PanAlibroteActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanAlibrote adaptee;
  ActionEvent evt;

  PanAlibroteActionAdapter(PanAlibrote adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoBloqueo, adaptee)) {
      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
        //adaptee.btnAreaActionPerformed();
      }
      if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  PanAlibroteActionAdapter
