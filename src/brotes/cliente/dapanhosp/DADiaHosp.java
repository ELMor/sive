package brotes.cliente.dapanhosp;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CTextArea;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;
import sapp2.StubSrvBD;

/**
 * Di�logo a trav�s del cu�l se insertan/modifican/borran datos
 * en tablas de mantenimiento de investigadores de brptes.
 * @autor PDP
 * @version 1.0
 */

public class DADiaHosp
    extends CDialog
    implements CInicializar {

  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  // parametros pasados al di�logo
  public Data dtDev = null;
  //
  public Data dtDevuelto = new Data();
  //
  public Data dtReg = new Data();
  //
  Data diaBrote = null;

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xYLayout1 = new XYLayout();
  Label lblHosp = new Label();
  TextField txtHosp = new TextField();
  Label lblFecIn = new Label();
  fechas.CFecha txtFecIn = new fechas.CFecha("S");
  Label lblObs = new Label();
  CTextArea textAreaObs = new CTextArea(500);
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores de eventos...

  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  // constructor del di�logo DiaInvBrotes
  public DADiaHosp(CApp a, int modoop, Data dt, Data brote) {

    super(a);
    QueryTool qt1 = null;
    QueryTool2 qt = null;

    try {
      modoOperacion = modoop;
      dtDev = dt;
      diaBrote = brote;

      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del dialogo
  public void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xYLayout1);
    this.setSize(483, 262);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    txtHosp.setBackground(new Color(255, 255, 150));
    lblHosp.setText("Hospital:");
    txtHosp.setName("hospital");
    lblFecIn.setText("Fecha de Ingreso:");
    txtFecIn.setName("fechain");
    lblObs.setText("Observaciones:");
    textAreaObs.setName("observaciones");
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));
    txtFecIn.setBackground(new Color(255, 255, 150));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xYLayout1.setHeight(262);
    xYLayout1.setWidth(483);

    this.add(lblHosp, new XYConstraints(21, 29, 84, -1));
    this.add(txtHosp, new XYConstraints(129, 29, 329, -1));
    this.add(lblFecIn, new XYConstraints(21, 68, 102, -1));
    this.add(txtFecIn, new XYConstraints(129, 68, 149, -1));
    this.add(lblObs, new XYConstraints(21, 105, 89, -1));
    this.add(textAreaObs, new XYConstraints(129, 107, 331, 63));
    this.add(btnAceptar, new XYConstraints(276, 195, 89, -1));
    this.add(btnCancelar, new XYConstraints(370, 195, 89, -1));
    //m�todo temporal para poder ver los datos en el panel
    //pasados a trav�s de la applet
    verDatos();
    // ponemos el t�tulo

    switch (modoOperacion) {
      case modoALTA:
        setTitle("Alta de un nuevo hospital de ingreso");
        break;
      case modoMODIFICACION:
        this.setTitle("Modificaci�n del hospital de ingreso");
        break;
      case modoBAJA:
        this.setTitle("Baja del hospital de ingreso");
        break;

    }
  }

  // Pone los campos de texto en el estado que les corresponda
  // segun el modo.

  public void verDatos() {
    switch (modoOperacion) {
      case modoALTA:
        txtHosp.setEnabled(true);
        txtFecIn.setEnabled(true);
        textAreaObs.setEnabled(true);
        break;

      case modoMODIFICACION:
        txtHosp.setEnabled(true);
        txtFecIn.setEnabled(true);
        textAreaObs.setEnabled(true);

        // datos panel principal
        txtHosp.setText(dtDev.getString("DS_HOSPITAL"));
        txtFecIn.setText(dtDev.getString("FC_INGHOSP"));
        textAreaObs.setText(dtDev.getString("DS_OBSERV"));

        break;

        // s�lo habilitado aceptar y cancelar
      case modoBAJA:
        txtHosp.setEnabled(false);
        txtFecIn.setEnabled(false);
        textAreaObs.setEnabled(false);

        // datos panel principal
        txtHosp.setText(dtDev.getString("DS_HOSPITAL"));
        txtFecIn.setText(dtDev.getString("FC_INGHOSP"));
        textAreaObs.setText(dtDev.getString("DS_OBSERV"));

        break;
    }
  }

  // Pone el di�logo en modo de espera

  public void Inicializar(int modo) {
    switch (modo) {

      // componente producto accediendo a base de datos
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // componente en el modo requerido
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoALTA:
            this.setEnabled(true);

            break;

          case modoMODIFICACION:
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;

            // s�lo habilitado salir y grabar
          case modoBAJA:
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  }

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

  // Devuelve los datos seleccionados en el di�logo.
  public Data devuelveData() {
    return dtReg;
  }

  private boolean isDataValid() {
    String sMsg = null;
    boolean bCtrl1 = true;
    boolean bCtrl11 = true;
    boolean bCtrl2 = true;
    boolean bCtrl3 = true;

    if (txtHosp.getText().trim().length() == 0) {
      bCtrl1 = false;
      sMsg = "Debe cumplimentar el nombre del hospital";
    }

    if (txtHosp.getText().trim().length() > 40) {
      bCtrl11 = false;
      sMsg = "Debe introducir un nombre de hospital m�s corto";
    }

    if (txtFecIn.getText().length() == 0) {
      bCtrl2 = false;
      sMsg = "Debe cumplimentar la fecha";
    }
    else {
      txtFecIn.ValidarFecha();
      if (txtFecIn.getValid().equals("S")) {
        bCtrl3 = true;
      }
      else {
        sMsg = "Debe introducir una fecha correcta";
        bCtrl3 = false;
      }
    }
    if ( (bCtrl1 && bCtrl2) == false) {
      sMsg = "Debe cumplimentar los datos obligatorios";
    }
    if (sMsg == null) {
      return true;
    }
    else {
      this.getApp().showAdvise(sMsg);
      return false;
    }

  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        switch (modoOperacion) {

          // ----- NUEVO -----------

          case 0:

            Inicializar(CInicializar.ESPERA);

            if (ComprobarRepeticion()) {
              dtReg = new Data();
              dtReg.put("CD_ANO", diaBrote.getString("CD_ANO"));
              dtReg.put("NM_ALERBRO", diaBrote.getString("NM_ALERBRO"));
              dtReg.put("CD_ANO||'/'||NM_ALERBRO",
                        diaBrote.getString("CD_ANO") + "/" +
                        diaBrote.getString("NM_ALERBRO"));
              dtReg.put("DS_HOSPITAL", txtHosp.getText().trim());
              dtReg.put("FC_INGHOSP", txtFecIn.getText().trim());
              dtReg.put("DS_OBSERV", textAreaObs.getText().trim());
              dtReg.put("TIPO_OPERACION", "A");

              bAceptar = true;
              dispose();
            }

            Inicializar(CInicializar.NORMAL);
            break;
          case 1:
            Inicializar(CInicializar.ESPERA);

            dtReg = new Data();
            dtReg.put("CD_ANO", diaBrote.getString("CD_ANO"));
            dtReg.put("NM_ALERBRO", diaBrote.getString("NM_ALERBRO"));
            dtReg.put("NM_ALERHOSP", dtDev.getString("NM_ALERHOSP"));
            dtReg.put("CD_ANO||'/'||NM_ALERBRO",
                      diaBrote.getString("CD_ANO") + "/" +
                      diaBrote.getString("NM_ALERBRO"));
            dtReg.put("DS_HOSPITAL", txtHosp.getText().trim());
            dtReg.put("FC_INGHOSP", txtFecIn.getText().trim());
            dtReg.put("DS_OBSERV", textAreaObs.getText().trim());
            String OpeAnt = dtDev.getString("TIPO_OPERACION");
            if (OpeAnt.equals("A")) {
              dtReg.put("TIPO_OPERACION", "A");
            }
            else {
              dtReg.put("TIPO_OPERACION", "M");
            }
            bAceptar = true;
            dispose();

            Inicializar(CInicializar.ESPERA);
            break;
          case 2:
            Inicializar(CInicializar.ESPERA);

            dtReg = new Data();
            dtReg.put("CD_ANO", diaBrote.getString("CD_ANO"));
            dtReg.put("NM_ALERBRO", diaBrote.getString("NM_ALERBRO"));
            dtReg.put("NM_ALERHOSP", dtDev.getString("NM_ALERHOSP"));
            dtReg.put("CD_ANO||'/'||NM_ALERBRO",
                      diaBrote.getString("CD_ANO") + "/" +
                      diaBrote.getString("NM_ALERBRO"));
            dtReg.put("DS_HOSPITAL", txtHosp.getText().trim());
            dtReg.put("FC_INGHOSP", txtFecIn.getText().trim());
            dtReg.put("DS_OBSERV", textAreaObs.getText().trim());
            String OpeAntBaj = dtDev.getString("TIPO_OPERACION");
            if (OpeAntBaj.equals("M")) {
              dtReg.put("TIPO_OPERACION", "M");
            }
            else {
              dtReg.put("TIPO_OPERACION", "B");
            }

            bAceptar = true;
            dispose();

            Inicializar(CInicializar.ESPERA);
            break;
        }
      }
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  }

  // Comprobaci�n de la existencia de un registro con ya repetido
  private boolean ComprobarRepeticion() {

    Lista p = new Lista();
    Lista p1 = new Lista();

    try {

      QueryTool qt = new QueryTool();

      // tabla de familias de productos
      qt.putName("SIVE_HOSPITAL_ALERT");
      // campos que se escriben
      qt.putType("NM_ALERHOSP", QueryTool.INTEGER);

      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", diaBrote.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");
      qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putWhereValue("NM_ALERBRO", diaBrote.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");
      qt.putWhereType("DS_HOSPITAL", QueryTool.STRING);
      qt.putWhereValue("DS_HOSPITAL", txtHosp.getText().trim());
      qt.putOperator("DS_HOSPITAL", "=");

      p.addElement(qt);

      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      p1 = (Lista)this.getApp().getStub().doPost(1, p);

    }
    catch (Exception ex) {
      ex.printStackTrace();
      this.getApp().trazaLog(ex);
    }

    if (p1.size() == 0) {
      return true;
    }
    else {
      this.getApp().showAdvise(
          "Este hospital ya existe para este brotes, seleccione otro.");
      txtHosp.setText(" ");
      return false;
    }
  }

// botones de aceptar y cancelar.

  class DialogActionAdapter
      implements java.awt.event.ActionListener, Runnable {
    DADiaHosp adaptee;
    ActionEvent e;

    DialogActionAdapter(DADiaHosp adaptee) {
      this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
      this.e = e;
      Thread th = new Thread(this);
      th.start();
    }

    public void run() {
      adaptee.btn_actionPerformed(e);
    }
  }
}
