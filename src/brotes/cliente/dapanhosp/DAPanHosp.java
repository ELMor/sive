package brotes.cliente.dapanhosp;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.CListaMantenimientoExt;
import brotes.cliente.c_componentes.ContCListaMantenimientoExt;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.UButtonControl;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;
import sapp2.StubSrvBD;

/**
 * Panel a trav�s del que se realizan los mantenimientos
 * de investigadores de brotes.
 * @autor PDP
 * @version 1.0
 */
public class DAPanHosp
    extends CDialog
    implements CInicializar, CFiltro, ContCListaMantenimientoExt {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  // estructuras para comprobar el bloqueo
  private QueryTool qtBloqueo = null;
  private Data dtBloqueo = null;

  //A�o y n�mero de brote que se le pas al di�logo
  Data dtBrote = null;

  //Secuenciador de Alerta Hospitales
  int secAlertHosp = 0;

  //Registro seleccionado
  public Data dtRegSel = null;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;
  public int modoActualizacion = ALTA;

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  private Lista lismanBaj = new Lista();

  XYLayout xYLayout1 = new XYLayout();
  //CListaMantenimiento clmMantenimiento = null;
  CListaMantenimientoExt clmMantenimiento = null;
  ButtonControl btnSalir = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();

  Label lblBroteDesc = new Label();
  Label lblDatosBrote = new Label();

  final String srvTrans = "servlet/SrvTransaccion";

  DialogActionAdapter1 ActionAdapter = new DialogActionAdapter1(this);

  // filtro
  private Data dtFiltro = null;

  // constructor del panel DAPanHosp
  public DAPanHosp(CApp a, int modo, Data dt) {
    super(a);
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    try {
      this.app = a;
      modoActualizacion = modo;
      dtBrote = dt;

      // botones
      vBotones.addElement(new CBoton("",
                                     "images/alta.gif",
                                     "Nueva petici�n",
                                     false,
                                     false));

      vBotones.addElement(new CBoton("",
                                     "images/modificacion.gif",
                                     "Modificar petici�n",
                                     true,
                                     true));

      vBotones.addElement(new CBoton("",
                                     "images/baja.gif",
                                     "Borrar petici�n",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("Hospital",
                                      "400",
                                      "DS_HOSPITAL"));

      vLabels.addElement(new CColumna("A�o/N�m. Alerta",
                                      "100",
                                      "CD_ANO||'/'||NM_ALERBRO"));

      vLabels.addElement(new CColumna("Fecha de Ingreso",
                                      "175",
                                      "FC_INGHOSP"));

      clmMantenimiento = new CListaMantenimientoExt(a,
          vLabels,
          vBotones,
          this,
          this,
          this);
      jbInit();
      clmMantenimiento.setPrimeraPagina(this.primeraPagina());
      // 27/04/01 (ARS) -- clmMantenimiento.getLista().size()>0
      if ( (clmMantenimiento.getLista() != null) &&
          (clmMantenimiento.getLista().size() > 0) &&
          (modoActualizacion == ALTA || modoActualizacion == MODIFICACION)) {
        clmMantenimiento.tabla.select(0);
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del panel DAPanHosp
  public void jbInit() throws Exception {

    final String imgSalir = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";
    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgSalir);
    this.getApp().getLibImagenes().CargaImagenes();
    lblBroteDesc.setText("Brote:");
    lblDatosBrote.setText(dtBrote.getString("CD_ANO") + "/" +
                          dtBrote.getString("NM_ALERBRO") + " - " +
                          dtBrote.getString("DS_ALERTA"));

    btnSalir.setActionCommand("Salir");
    btnSalir.setLabel("Cancelar");
    btnSalir.setImage(this.getApp().getLibImagenes().get(imgSalir));
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnAceptar.setLabel("Aceptar");

    //Escuchadores
    btnSalir.addActionListener(ActionAdapter);
    btnSalir.setEnabled(true);

    btnAceptar.addActionListener(ActionAdapter);
    btnAceptar.setEnabled(true);

    this.setSize(725, 440);
    this.setTitle("Mantenimiento de Hospitales");

    xYLayout1.setWidth(725);
    xYLayout1.setHeight(440);

    this.setLayout(xYLayout1);
    this.add(lblBroteDesc, new XYConstraints(12, 11, 42, -1));
    this.add(lblDatosBrote, new XYConstraints(65, 11, 600, -1));
    this.add(clmMantenimiento, new XYConstraints(7, 50, 710, 292));
    this.add(btnSalir, new XYConstraints(621, 352, 88, 29));
    this.add(btnAceptar, new XYConstraints(516, 352, 88, 29));

  }

  public void Inicializar() {}

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        if (modoActualizacion == ALTA) {
          this.setEnabled(true);
        }
        else if (modoActualizacion == MODIFICACION) {
          this.setEnabled(true);
        }
        else if (modoActualizacion == CONSULTA) {
          clmMantenimiento.setEnabled(false);
          btnAceptar.setVisible(false);
          btnSalir.setLabel("Salir");
          btnSalir.setEnabled(true);
        }
        else if (modoActualizacion == BAJA) {
          clmMantenimiento.setEnabled(false);
          btnAceptar.setVisible(false);
          btnSalir.setLabel("Salir");
          btnSalir.setEnabled(true);
        }

        break;
    }
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {

    Lista v = new Lista();
    Lista vFiltro = new Lista();

    Inicializar(CInicializar.ESPERA);

    try {
      QueryTool2 qt = new QueryTool2();

      // Investigadores de brotes
      qt.putName("SIVE_HOSPITAL_ALERT");

      // campos que se leen

      qt.putType("NM_ALERHOSP", QueryTool.INTEGER);
      qt.putType("CD_ANO||'/'||NM_ALERBRO", QueryTool.STRING);
      qt.putType("CD_ANO", QueryTool.STRING);
      qt.putType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putType("DS_HOSPITAL", QueryTool.STRING);
      qt.putType("FC_INGHOSP", QueryTool.DATE);
      qt.putType("DS_OBSERV", QueryTool.STRING);
      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");
      qt.putWhereType("NM_ALERBRO", QueryTool.STRING);
      qt.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");
      vFiltro.setTrama("NM_ALERHOSP", "");
      vFiltro.addElement(qt);

      // consulta el servidor
      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      v = (Lista)this.getApp().getStub().doPost(8, vFiltro);
      if (v.size() == 0) {
        this.getApp().showAdvise("No se encontraron registros");
      }

    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    Inicializar(CInicializar.NORMAL);

    return v;
  }

  // solicita la siguiente trama de datos
  public Lista siguientePagina() {

    // lista para el filtro
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();

    Inicializar(CInicializar.ESPERA);

    // realiza la consulta al servlet
    try {
      QueryTool2 qt = new QueryTool2();

      // Hospitales
      qt.putName("SIVE_HOSPITAL_ALERT");

      // campos que se leen

      qt.putType("NM_ALERHOSP", QueryTool.INTEGER);
      qt.putType("CD_ANO", QueryTool.STRING);
      qt.putType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putType("CD_ANO||'/'||NM_ALERBRO", QueryTool.STRING);
      qt.putType("DS_HOSPITAL", QueryTool.STRING);
      qt.putType("FC_INGHOSP", QueryTool.DATE);
      qt.putType("DS_OBSERV", QueryTool.STRING);

      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");
      qt.putWhereType("NM_ALERBRO", QueryTool.STRING);
      qt.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");

      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);

      // fija el filtro
      vFiltro.addElement(qt);

      // fija la trama
      vFiltro.setTrama(this.clmMantenimiento.getLista().getTrama());

      vPetic = (Lista)this.getApp().getStub().doPost(8, vFiltro);

      // error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    // la lista debe estar creada
    if (vPetic == null) {
      vPetic = new Lista();

    }
    Inicializar(CInicializar.NORMAL);

    return vPetic;
  }

  public Data RegSel() {
    return dtRegSel = clmMantenimiento.getSelected();
  }

  void btn_actionPerformed(ActionEvent e) {
    Inicializar(CInicializar.ESPERA);
    Lista lstMant = new Lista();
    Lista lstOperar = new Lista();
    Lista vResultado = new Lista();
    String secAlerta = new String();

    if (e.getActionCommand().equals("Aceptar")) {
      try {
        this.getApp().getStub().setUrl("servlet/SrvPanHos");

        // Query para actualizar SIVE_BROTES (CD_OPE/FC_ULTACT)
        QueryTool qtBro = new QueryTool();
        Data dtBro = new Data();
        qtBro.putName("SIVE_ALERTA_BROTES");
        qtBro.putType("CD_OPE", QueryTool.STRING);
        qtBro.putType("FC_ULTACT", QueryTool.TIMESTAMP);
        qtBro.putValue("CD_OPE", dtBrote.getString("CD_OPE"));
        qtBro.putValue("FC_ULTACT", "");

        qtBro.putWhereType("CD_ANO", QueryTool.STRING);
        qtBro.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
        qtBro.putOperator("CD_ANO", "=");
        qtBro.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
        qtBro.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
        qtBro.putOperator("NM_ALERBRO", "=");

        dtBro.put("4", qtBro);
        lstOperar.addElement(dtBro);

        /************** ALTAS ***************/
        lstMant = this.clmMantenimiento.getLista();

        Data dtTemp = null;

        for (int i = 0; i < lstMant.size(); i++) {
          dtTemp = (Data) lstMant.elementAt(i);
          if (dtTemp.getString("TIPO_OPERACION").equals("A")) {
            QueryTool qtAlt = new QueryTool();
            qtAlt = realizarAlta(dtTemp);

            Data dtAlt = new Data();
            dtAlt.put("3", qtAlt);

            lstOperar.addElement(dtAlt);

            dtTemp.remove("TIPO_OPERACION");
          }
        }

        /************** MODIFICAR ***************/
        for (int i = 0; i < lstMant.size(); i++) {
          dtTemp = (Data) lstMant.elementAt(i);
          if (dtTemp.getString("TIPO_OPERACION").equals("M")) {
            QueryTool qtUpd = new QueryTool();
            qtUpd = realizarUpdate(dtTemp);

            Data dtUpdate = new Data();
            dtUpdate.put("4", qtUpd);

            lstOperar.addElement(dtUpdate);

            dtTemp.remove("TIPO_OPERACION");
          }
        }

        /************** BAJAS ***************/
        if (lismanBaj.size() > 0) {

          for (int i = 0; i < (lismanBaj.size()); i++) {

            dtTemp = (Data) lismanBaj.elementAt(i);

            QueryTool qtBaj = new QueryTool();
            qtBaj = realizarBaja(dtTemp);

            Data dtUpBaj = new Data();
            dtUpBaj.put("5", qtBaj);

            lstOperar.addElement(dtUpBaj);
          }
        }

        if (modoActualizacion == BAJA) {
          for (int i = 0; i < lstMant.size() - 1; i++) {
            dtTemp = (Data) lstMant.elementAt(i);

            QueryTool qtBaj = new QueryTool();
            qtBaj = realizarBaja(dtTemp);

            Data dtUpBaj = new Data();
            dtUpBaj.put("5", qtBaj);

            lstOperar.addElement(dtUpBaj);
          }
        }

        //realizamos la actualizaci�n
        preparaBloqueo();
        if (lstOperar.size() > 0) {

          /*SrvTasVac servlet=new SrvTasVac();
               servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                        "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                        "sive_desa",
                        "sive_desa");
                       vResultado = (Lista) servlet.doDebug(0,lstOperar);
                  }*/
          vResultado = (Lista)this.getApp().getStub().doPost(10000,
              lstOperar,
              qtBloqueo,
              dtBloqueo,
              getApp());

          //obtener el nuevo cd_ope y fc_ultact de la base de datos.
        }
        int tamano = vResultado.size();
        String fecha = "";
        fecha = ( (Lista) vResultado.firstElement()).getFC_ULTACT();

        dtBrote.put("CD_OPE", dtBrote.getString("CD_OPE"));
        dtBrote.put("FC_ULTACT", fecha);

        dispose();

      }
      catch (Exception exc) {
        dispose();
        //this.getApp().trazaLog(exc);
        //this.getApp().showError(exc.getMessage());
      } // del "trai cach"
      dispose();
    }
    else if (e.getActionCommand().equals("Salir")) {
      bAceptar = false;
      dispose();
    }
    Inicializar(CInicializar.NORMAL);
  }

  // prepara la informaci�n para el control de bloqueo
  private void preparaBloqueo() {
    // query tool
    qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_ALERTA_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
    qtBloqueo.putOperator("NM_ALERBRO", "=");

    // informaci�n de bloqueo
    dtBloqueo = new Data();
    dtBloqueo.put("CD_OPE", dtBrote.getString("CD_OPE"));
    dtBloqueo.put("FC_ULTACT", dtBrote.getString("FC_ULTACT"));
  }

  QueryTool realizarAlta(Data dtAlt) {

    QueryTool qtAlt = new QueryTool();

    // tabla de familias de tasas vacunables
    qtAlt.putName("SIVE_HOSPITAL_ALERT");

    // campos que se escriben
    qtAlt.putType("NM_ALERHOSP", QueryTool.INTEGER);
    qtAlt.putType("CD_ANO", QueryTool.STRING);
    qtAlt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtAlt.putType("DS_HOSPITAL", QueryTool.STRING);
    qtAlt.putType("FC_INGHOSP", QueryTool.DATE);
    qtAlt.putType("DS_OBSERV", QueryTool.STRING);

    // Valores de los campos
    qtAlt.putValue("NM_ALERHOSP", "");
    qtAlt.putValue("CD_ANO", dtAlt.getString("CD_ANO"));
    qtAlt.putValue("NM_ALERBRO", dtAlt.getString("NM_ALERBRO"));
    qtAlt.putValue("DS_HOSPITAL", dtAlt.getString("DS_HOSPITAL"));
    qtAlt.putValue("FC_INGHOSP", dtAlt.getString("FC_INGHOSP"));
    qtAlt.putValue("DS_OBSERV", dtAlt.getString("DS_OBSERV"));

    return qtAlt;
  }

  QueryTool realizarUpdate(Data dtUpd) {

    QueryTool qtUpd = new QueryTool();

    // tabla de familias de tasas vacunables
    qtUpd.putName("SIVE_HOSPITAL_ALERT");

    // campos que se escriben
    qtUpd.putType("DS_HOSPITAL", QueryTool.STRING);
    qtUpd.putType("FC_INGHOSP", QueryTool.DATE);
    qtUpd.putType("DS_OBSERV", QueryTool.STRING);

    // Valores de los campos
    qtUpd.putValue("DS_HOSPITAL", dtUpd.getString("DS_HOSPITAL"));
    qtUpd.putValue("FC_INGHOSP", dtUpd.getString("FC_INGHOSP"));
    qtUpd.putValue("DS_OBSERV", dtUpd.getString("DS_OBSERV"));

    //
    qtUpd.putWhereType("NM_ALERHOSP", QueryTool.STRING);
    qtUpd.putWhereValue("NM_ALERHOSP", dtUpd.getString("NM_ALERHOSP"));
    qtUpd.putOperator("NM_ALERHOSP", "=");
    qtUpd.putWhereType("CD_ANO", QueryTool.INTEGER);
    qtUpd.putWhereValue("CD_ANO", dtUpd.getString("CD_ANO"));
    qtUpd.putOperator("CD_ANO", "=");
    qtUpd.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtUpd.putWhereValue("NM_ALERBRO", dtUpd.getString("NM_ALERBRO"));
    qtUpd.putOperator("NM_ALERBRO", "=");

    return qtUpd;
  }

  QueryTool realizarBaja(Data dtBaj) {

    QueryTool qtBaj = new QueryTool();

    // tabla de familias de productos
    qtBaj.putName("SIVE_HOSPITAL_ALERT");
    //
    qtBaj.putWhereType("NM_ALERHOSP", QueryTool.STRING);
    qtBaj.putWhereValue("NM_ALERHOSP", dtBaj.getString("NM_ALERHOSP"));
    qtBaj.putOperator("NM_ALERHOSP", "=");
    qtBaj.putWhereType("CD_ANO", QueryTool.INTEGER);
    qtBaj.putWhereValue("CD_ANO", dtBaj.getString("CD_ANO"));
    qtBaj.putOperator("CD_ANO", "=");
    qtBaj.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBaj.putWhereValue("NM_ALERBRO", dtBaj.getString("NM_ALERBRO"));
    qtBaj.putOperator("NM_ALERBRO", "=");

    return qtBaj;
  }

  String obtenerSecAlerHosp() {
    final String servlet = "servlet/SrvQueryTool";
    String sec = new String();
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_SEC_GENERAL");
    qt.putType("NM_ALERHOSP", QueryTool.INTEGER);
    try {
      this.getApp().getStub().setUrl(servlet);
      p.addElement(qt);
      p.addElement(qt);

      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      p1 = (Lista)this.getApp().getStub().doPost(1, p);
      //p1=BDatos.ejecutaSQL(false,this.getApp(),servlet,1,p);
      String secuencial = ( (Data) p1.elementAt(0)).getString("NM_ALERHOSP");
      secAlertHosp = Integer.parseInt(secuencial);
      sec = Integer.toString(secAlertHosp);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    return sec;
  }

  //HAbilita o deshabilita botones en funci�n del modo de entrada
  public void cambiarBotones(Vector vectBotones) {
    UButtonControl btnControlAlta = (UButtonControl) vectBotones.elementAt(0);
    UButtonControl btnControlMod = (UButtonControl) vectBotones.elementAt(1);
    UButtonControl btnControlBaja = (UButtonControl) vectBotones.elementAt(2);
    if (modoActualizacion == BAJA || modoActualizacion == CONSULTA) {
      btnControlAlta.setEnabled(false);
      btnControlBaja.setEnabled(false);
      btnControlMod.setEnabled(false);
    }

  }

  public void borradoMasivo() {

    Lista v1 = new Lista();
    Lista vFiltro1 = new Lista();

    Inicializar(CInicializar.ESPERA);

    try {
      QueryTool2 qt = new QueryTool2();

      // Investigadores de brotes
      qt.putName("SIVE_HOSPITAL_ALERT");

      // campos que se leen

      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");
      qt.putWhereType("NM_ALERBRO", QueryTool.STRING);
      qt.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");
      vFiltro1.addElement(qt);

      // consulta el servidor
      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      v1 = (Lista)this.getApp().getStub().doPost(5, vFiltro1);

    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    Inicializar(CInicializar.NORMAL);

  }

  // operaciones de la botonera
  public void realizaOperacion(int j) {

    Lista lisPet = this.clmMantenimiento.getLista();
    DADiaHosp dm = null;
    Data dPetMod = new Data();
    Data dtResultado = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:
        dm = new DADiaHosp(this.getApp(), 0, null, dtBrote);
        dm.show();
        if (dm.bAceptar) {
          dtResultado = dm.devuelveData();
          lisPet.addElement(dtResultado);
          //clmMantenimiento.setPrimeraPagina(primeraPagina());
          clmMantenimiento.setPrimeraPagina(lisPet);
        }
        break;
      case MODIFICACION:
        dPetMod = clmMantenimiento.getSelected();
        if (dPetMod != null) {
          int ind = clmMantenimiento.getSelectedIndex();
          dm = new DADiaHosp(this.getApp(), 1, dPetMod, dtBrote);
          dm.show();
          if (dm.bAceptar) {
            lisPet.removeElementAt(ind);
            dtResultado = dm.devuelveData();
            lisPet.addElement(dtResultado);
            clmMantenimiento.setPrimeraPagina(lisPet);
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dPetMod = clmMantenimiento.getSelected();
        if (dPetMod != null) {
          int ind = clmMantenimiento.getSelectedIndex();
          dm = new DADiaHosp(this.getApp(), 2, dPetMod, dtBrote);
          dm.show();
          if (dm.bAceptar()) {
            lismanBaj.addElement(clmMantenimiento.getSelected());
            lisPet.removeElementAt(ind);
            clmMantenimiento.setPrimeraPagina(lisPet);
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;
    }
  }
}

class DialogActionAdapter1
    implements java.awt.event.ActionListener {
  DAPanHosp adaptee;
  ActionEvent e;

  DialogActionAdapter1(DAPanHosp adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    adaptee.btn_actionPerformed(e);
  }
}
