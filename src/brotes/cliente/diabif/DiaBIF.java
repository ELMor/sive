/**
 * Clase: DiaBIF
 * Hereda: CDialog
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 23/05/2000
 * Analisis Funcional: Punto 2. Mantenimiento de Informe Final del Brote (BIF)
 * Descripcion: Di�logo de alta/mod/baja de un brote
 */

package brotes.cliente.diabif;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import java.awt.Checkbox;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.DatoRecogible;
import brotes.cliente.c_comuncliente.BDatos;
import brotes.cliente.c_comuncliente.SincrEventos;
import brotes.cliente.c_comuncliente.nombreservlets;
import brotes.cliente.dbpancol.DBPanCol;
import brotes.cliente.dbpandatosbas.DBPanDatosBas;
import brotes.cliente.dbpandescrtemp.DBPanDescrTemp;
import brotes.cliente.dbpaninves.DBPanInves;
import brotes.cliente.dbpanotros.DBPanOtros;
import brotes.cliente.dbpanprotocolo.PanelBrote;
import brotes.cliente.dbpansup.DBPanSup;
import brotes.datos.dbpanprotocolo.DataProtocolo;
import capp2.CApp;
import capp2.CContextHelp;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CTabPanel;
import comun.constantes;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;
import sapp2.QueryTool;

//import brotes.servidor.bif.*;

public class DiaBIF
    extends CDialog
    implements CInicializar, DatoRecogible {

  /*************** Datos propios del panel ****************/

  // Parametros de entrada
  CApp applet = null;
  Hashtable listas = null; // Listas de cat�logos
  Lista lGrupos = null;

  // Variables intermedias
  Data bifIni = null;

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  // Constantes del modoEntrada
  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;

  // Nombre de los servlets
  final String srvTrans = nombreservlets.strSERVLET_BIF;
  /*final String srvTrans = nombreservlets.strSERVLET_TRANSACCION;*/

  /****************** Componentes del panel ********************/

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Organizaci�n del panel
  private XYLayout xYLayout1 = new XYLayout();

  // Panel superior
  private DBPanSup psup = null;

  private CTabPanel tabPanel = null;
  private DBPanDatosBas pBas = null;
  private DBPanCol pCol = null;
  private DBPanDescrTemp pTemp = null;
  private DBPanOtros pOtros = null;
  private PanelBrote pProtocolo = null;
  private boolean MensajeNoProtocolo = false;

  // Bot�n de Investigadores
  private ButtonControl btnInv = new ButtonControl();

  // Check de Investigacion Finalizada
  private Checkbox chkInvFin = new Checkbox("Investigaci�n Finalizada", false);
  private Label lblInvFin = new Label("Fecha:");
  private fechas.CFecha txtInvFin = new fechas.CFecha("N");

  // Botones de salida
  private ButtonControl btnAceptar = new ButtonControl();
  private ButtonControl btnCancelar = new ButtonControl();
  private ButtonControl btnSalir = new ButtonControl();

  // Escuchadores
  DiaBIFActionAdapter actionAdapter = null;

  // Constructor
  // @param a: CApp
  // @param listas: Hash con Listas de cat�logos
  public DiaBIF(CApp a, int modo, Data bif, Hashtable hListas) {

    super(a);
    applet = a;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    bifIni = bif;
    listas = hListas;
    lGrupos = (Lista) listas.get("GRUPOS");

    try {
      // Iniciaci�n
      jbInit();
      Inicializar(modoOperacion);

      switch (modoEntrada) {
        case ALTA:
          setTitle("Alta de Informe Final de Brote");
          break;
        case MODIFICACION:
          setTitle("Modificaci�n de Informe Final de Brote");
          rellenarDatos(bifIni);
          break;
        case BAJA:
          setTitle("Baja de Informe Final de Brote");
          rellenarDatos(bifIni);
          break;
        case CONSULTA:
          setTitle("Consulta de Informe Final de Brote");
          rellenarDatos(bifIni);
          break;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicia el aspecto del dialogo DiaBIF
  public void jbInit() throws Exception {
    // Carga las imagenes
    getApp().getLibImagenes().put(constantes.imgACEPTAR);
    getApp().getLibImagenes().put(constantes.imgCANCELAR);
    getApp().getLibImagenes().put(constantes.imgSALIR);
    getApp().getLibImagenes().CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaBIFActionAdapter(this);

    // Organizacion del panel
    this.setSize(new Dimension(755, 575));
    this.setLayout(xYLayout1);
    xYLayout1.setWidth(755);
    xYLayout1.setHeight(575);

    // Panel Superior
    psup = new DBPanSup(applet, this, bifIni, modoEntrada,
                        (Lista) listas.get("GRUPOS"),
                        (Lista) listas.get("TBROTES"));

    // Pesta�as: Notificador
    tabPanel = new CTabPanel();
    pBas = new DBPanDatosBas(applet, this, bifIni, modoEntrada, listas);
    pCol = new DBPanCol(applet, this, bifIni, modoEntrada, listas);
    pTemp = new DBPanDescrTemp(applet, this, bifIni, modoEntrada);

    switch (modoEntrada) {
      case ALTA:
        break;
      case MODIFICACION:
      case BAJA:
      case CONSULTA:
        pOtros = new DBPanOtros(applet, this, bifIni, modoEntrada, listas);
        crearProtocolo();
        break;
    }

    // Bot�n de Investigadores
    btnInv.setLabel("Investigadores");
    //btnInv.setImage(this.getApp().getLibImagenes().get(constantes.imgACEPTAR));
    btnInv.setActionCommand("Investigadores");
    btnInv.addActionListener(actionAdapter);

    // Inhabilitamos la caja de la fecha.
    txtInvFin.setEditable(false);

    // Seg�n modoEntrada habr� botones diferentes
    switch (modoEntrada) {
      case ALTA:
      case MODIFICACION:

        // Bot�n Aceptar
        btnAceptar.setLabel("Grabar");
        btnAceptar.setImage(this.getApp().getLibImagenes().get(constantes.
            imgACEPTAR));
        btnAceptar.setActionCommand("Grabar");
        btnAceptar.addActionListener(actionAdapter);

        // Bot�n Cancelar
        btnCancelar.setLabel("Cancelar");
        btnCancelar.setImage(this.getApp().getLibImagenes().get(constantes.
            imgCANCELAR));
        btnCancelar.setActionCommand("Cancelar");
        btnCancelar.addActionListener(actionAdapter);
        break;

      case BAJA:

        // Bot�n Aceptar
        btnAceptar.setLabel("Aceptar");
        btnAceptar.setImage(this.getApp().getLibImagenes().get(constantes.
            imgACEPTAR));
        btnAceptar.setActionCommand("Aceptar");
        btnAceptar.addActionListener(actionAdapter);

        // Bot�n Cancelar
        btnCancelar.setLabel("Cancelar");
        btnCancelar.setImage(this.getApp().getLibImagenes().get(constantes.
            imgCANCELAR));
        btnCancelar.setActionCommand("Cancelar");
        btnCancelar.addActionListener(actionAdapter);
        break;

      case CONSULTA:

        // Bot�n Salir
        btnSalir.setLabel("Salir");
        btnSalir.setImage(this.getApp().getLibImagenes().get(constantes.
            imgSALIR));
        btnSalir.setActionCommand("Salir");
        btnSalir.addActionListener(actionAdapter);
        break;
    }

    // Adici�n de componentes al di�logo
    this.add(psup, new XYConstraints(10, 10, -1, -1));
    this.add(tabPanel, new XYConstraints(10, 100, 725, 400));
    this.add(chkInvFin, new XYConstraints(10, 515, 160, -1));
    this.add(lblInvFin, new XYConstraints(180, 515, 50, -1));
    this.add(txtInvFin, new XYConstraints(240, 515, 80, -1));
    this.add(btnInv, new XYConstraints(350, 515, 100, -1));
    switch (modoEntrada) {
      case ALTA:
      case MODIFICACION:
        this.add(btnAceptar, new XYConstraints(537, 515, 80, -1));
        this.add(btnCancelar, new XYConstraints(627, 515, 80, -1));
        new CContextHelp("Grabar", btnAceptar);
        new CContextHelp("Cancelar", btnCancelar);
        break;

      case BAJA:
        this.add(btnAceptar, new XYConstraints(537, 515, 80, -1));
        this.add(btnCancelar, new XYConstraints(627, 515, 80, -1));
        new CContextHelp("Aceptar", btnAceptar);
        new CContextHelp("Cancelar", btnCancelar);
        break;

      case CONSULTA:
        this.add(btnSalir, new XYConstraints(627, 515, 80, -1));
        new CContextHelp("Salir", btnSalir);
        break;
    }

    // Paneles de tabPanel (con pesta�as)
    tabPanel.InsertarPanel("Datos B�sicos", pBas);
    tabPanel.InsertarPanel("Colectivo Afectado", pCol);
    tabPanel.InsertarPanel("Descripci�n Temporal", pTemp);
    switch (modoEntrada) {
      case ALTA:
        break;
      case MODIFICACION:
      case BAJA:
      case CONSULTA:
        tabPanel.InsertarPanel("Otros", pOtros);
        addProtocolo();
        break;

    }

    // Panel a visualizar
    tabPanel.VerPanel("Datos B�sicos");
  } // Fin jbInit()

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Iniciar()

  public void Inicializar() {
    // Iniciaci�n de los subcontenedores
    if (psup != null) {
      psup.InicializarDesdeFuera(modoOperacion);
    }
    if (pBas != null) {
      pBas.InicializarDesdeFuera(modoOperacion);
    }
    if (pCol != null) {
      pCol.InicializarDesdeFuera(modoOperacion);
    }
    if (pTemp != null) {
      pTemp.InicializarDesdeFuera(modoOperacion);
    }
    if (pOtros != null) {
      pOtros.InicializarDesdeFuera(modoOperacion);
    }
    if (pProtocolo != null) {
      pProtocolo.InicializarDesdeFuera(modoOperacion);

      // Componentes propios del di�logo
    }
    switch (modoOperacion) {
      // Modo espera
      case CInicializar.ESPERA:
        btnInv.setEnabled(false);
        txtInvFin.setEnabled(false);
        chkInvFin.setEnabled(false);
        if (modoEntrada == CONSULTA) {
          btnSalir.setEnabled(false);
        }
        else {
          btnAceptar.setEnabled(false);
          btnCancelar.setEnabled(false);
        }
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // Modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case ALTA:
            btnInv.setEnabled(false);
            txtInvFin.setEnabled(false);
            chkInvFin.setEnabled(false);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
          case MODIFICACION:
            int sit = new Integer(bifIni.getString("CD_SITALERBRO")).intValue();
            switch (sit) {
              case 3:
                if (psup != null) {
                  psup.setModoEntrada(MODIFICACION);
                }
                if (psup != null) {
                  psup.InicializarDesdeFuera(modoOperacion);
                }
                if (pBas != null) {
                  pBas.setModoEntrada(MODIFICACION);
                }
                if (pBas != null) {
                  pBas.InicializarDesdeFuera(modoOperacion);
                }
                if (pTemp != null) {
                  pTemp.setModoEntrada(MODIFICACION);
                }
                if (pTemp != null) {
                  pTemp.InicializarDesdeFuera(modoOperacion);
                }
                if (pCol != null) {
                  pCol.setModoEntrada(MODIFICACION);
                }
                if (pCol != null) {
                  pCol.InicializarDesdeFuera(modoOperacion);
                }
                if (pOtros != null) {
                  pOtros.setModoEntrada(MODIFICACION);
                }
                if (pOtros != null) {
                  pOtros.InicializarDesdeFuera(modoOperacion);
                }
                if (pProtocolo != null) {
                  pProtocolo.setModoEntrada(MODIFICACION);
                }
                if (pProtocolo != null) {
                  pProtocolo.InicializarDesdeFuera(modoOperacion);
                }
                break;
              case 4:
                if (psup != null) {
                  psup.setModoEntrada(CONSULTA);
                }
                if (psup != null) {
                  psup.InicializarDesdeFuera(modoOperacion);
                }
                if (pBas != null) {
                  pBas.setModoEntrada(CONSULTA);
                }
                if (pBas != null) {
                  pBas.InicializarDesdeFuera(modoOperacion);
                }
                if (pTemp != null) {
                  pTemp.setModoEntrada(CONSULTA);
                }
                if (pTemp != null) {
                  pTemp.InicializarDesdeFuera(modoOperacion);
                }
                if (pCol != null) {
                  pCol.setModoEntrada(CONSULTA);
                }
                if (pCol != null) {
                  pCol.InicializarDesdeFuera(modoOperacion);
                }
                if (pOtros != null) {
                  pOtros.setModoEntrada(CONSULTA);
                }
                if (pOtros != null) {
                  pOtros.InicializarDesdeFuera(modoOperacion);
                }
                if (pProtocolo != null) {
                  pProtocolo.setModoEntrada(CONSULTA);
                }
                if (pProtocolo != null) {
                  pProtocolo.InicializarDesdeFuera(modoOperacion);
                }
                break;
            }

            btnInv.setEnabled(true);
            txtInvFin.setEnabled(true);
            chkInvFin.setEnabled(true);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
          case BAJA:
            btnInv.setEnabled(true);
            chkInvFin.setEnabled(false);
            txtInvFin.setEnabled(false);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
          case CONSULTA:
            btnInv.setEnabled(true);
            chkInvFin.setEnabled(false);
            txtInvFin.setEnabled(false);
            btnSalir.setEnabled(true);
            break;
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    } // Fin switch
  }

  /********** Sincronizador de Eventos ******************/

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  /****************** Interfaz DatoRecogible ****************/

  public void recogerDatos(Data d) {
    // Recogida de datos del panel superior
    psup.recogerDatos(d);

    // Recogida de datos del panel Datos B�sicos
    pBas.recogerDatos(d);

    // Recogida de datos del panel Colectivo Afectado
    pCol.recogerDatos(d);

    // Recogida de datos del panel Descripci�n Temporal
    pTemp.recogerDatos(d);

    // Datos del propio dialogo

    // Estado de la alerta
    if (chkInvFin.getState()) {
      d.put("CD_SITALERBRO", "4");
    }
    else {
      d.put("CD_SITALERBRO", "3");

      // Fecha de Fin de Investigacion
    }
    d.put("FC_ALERBRO", txtInvFin.getText());

  } // Fin recogerDatos()

  public void rellenarDatos(Data d) {
    // Relleno de datos del panel superior
    psup.rellenarDatos(d);

    // Relleno de datos del panel Datos b�sicos
    pBas.rellenarDatos(d);

    // Relleno de datos del panel Colectivo
    pCol.rellenarDatos(d);

    // Relleno de datos del panel Descripcion Temporal
    pTemp.rellenarDatos(d);

    // Relleno de InvFinalizada (Check + Fecha)
    txtInvFin.setText( ( (String) d.get("FC_ALERBRO")));
    if ( ( (String) d.get("CD_SITALERBRO")).equals("3")) {
      chkInvFin.setState(false);
    }
    else if ( ( (String) d.get("CD_SITALERBRO")).equals("4")) {
      chkInvFin.setState(true);

    }
  } // Fin rellenarDatos()

  public boolean validarDatos() {

    // 1. PERDIDAS DE FOCO (Campos CD-Button-DS): No hay

    // 2. LONGITUDES: No hay

    // 3. DATOS NUMERICOS: No hay

    // 4. CAMPOS OBLIGATORIOS: No hay

    // 5. FECHAS

    // Validaci�n de datos del panel superior
    if (!psup.validarDatos()) {
      return false;
    }

    if (!pBas.validarDatos()) {
      tabPanel.VerPanel("Datos B�sicos");
      return false;
    }

    if (!pCol.validarDatos()) {
      tabPanel.VerPanel("Colectivo Afectado");
      return false;
    }

    if (!pTemp.validarDatos()) {
      tabPanel.VerPanel("Descripci�n Temporal");
      return false;
    }

    if (pProtocolo != null) {
      if (!pProtocolo.pnl.ValidarObligatorios()) {
        return false;
      }
    }

    // Validar la fechaInvFin
    if (!txtInvFin.getText().equals("")) {
      txtInvFin.ValidarFecha();
      if (txtInvFin.getValid().equals("N")) {
        this.getApp().showError("Fecha incorrecta");
        txtInvFin.requestFocus();
        return (false);
      }
    }

    // Si llega aqui todo ha ido bien
    return true;
  } // Fin validarDatos()

  // Dev. true si son iguales ambas estructuras
  boolean compararCampos(Data d1, Data d2) {
    if (d1 == null || d2 == null) {
      return true;
    }

    if (!d1.getString("CD_ANO").equals(d2.getString("CD_ANO"))) {
      return false;
    }

    if (!d1.getString("NM_ALERBRO").equals(d2.getString("NM_ALERBRO"))) {
      return false;
    }

    if (!d1.getString("CD_TIPOCOL").equals(d2.getString("CD_TIPOCOL"))) {
      return false;
    }

    if (!d1.getString("CD_TNOTIF").equals(d2.getString("CD_TNOTIF"))) {
      return false;
    }

    if (!d1.getString("CD_TRANSMIS").equals(d2.getString("CD_TRANSMIS"))) {
      return false;
    }

    if (!d1.getString("CD_GRUPO").equals(d2.getString("CD_GRUPO"))) {
      return false;
    }

    if (!d1.getString("CD_TBROTE").equals(d2.getString("CD_TBROTE"))) {
      return false;
    }

    if (!d1.getString("FC_FECHAHORA").equals(d2.getString("FC_FECHAHORA"))) {
      return false;
    }

    if (!d1.getString("DS_BROTE").equals(d2.getString("DS_BROTE"))) {
      return false;
    }

    if (!d1.getString("DS_NOMCOL").equals(d2.getString("DS_NOMCOL"))) {
      return false;
    }

    if (!d1.getString("DS_DIRCOL").equals(d2.getString("DS_DIRCOL"))) {
      return false;
    }

    if (!d1.getString("CD_POSTALCOL").equals(d2.getString("CD_POSTALCOL"))) {
      return false;
    }

    if (!d1.getString("CD_PROVCOL").equals(d2.getString("CD_PROVCOL"))) {
      return false;
    }

    if (!d1.getString("CD_MUNCOL").equals(d2.getString("CD_MUNCOL"))) {
      return false;
    }

    if (!d1.getString("DS_NMCALLE").equals(d2.getString("DS_NMCALLE"))) {
      return false;
    }

    if (!d1.getString("DS_PISOCOL").equals(d2.getString("DS_PISOCOL"))) {
      return false;
    }

    if (!d1.getString("DS_TELCOL").equals(d2.getString("DS_TELCOL"))) {
      return false;
    }

    if (!d1.getString("CD_NIVEL_1_LCA").equals(d2.getString("CD_NIVEL_1_LCA"))) {
      return false;
    }

    if (!d1.getString("CD_NIVEL_2_LCA").equals(d2.getString("CD_NIVEL_2_LCA"))) {
      return false;
    }

    if (!d1.getString("CD_ZBS_LCA").equals(d2.getString("CD_ZBS_LCA"))) {
      return false;
    }

    if (!d1.getString("CD_NIVEL_1_LE").equals(d2.getString("CD_NIVEL_1_LE"))) {
      return false;
    }

    if (!d1.getString("CD_NIVEL_2_LE").equals(d2.getString("CD_NIVEL_2_LE"))) {
      return false;
    }

    if (!d1.getString("CD_ZBS_LE").equals(d2.getString("CD_ZBS_LE"))) {
      return false;
    }

    if (!d1.getString("NM_MANIPUL").equals(d2.getString("NM_MANIPUL"))) {
      return false;
    }

    if (!d1.getString("IT_RESCALC").equals(d2.getString("IT_RESCALC"))) {
      return false;
    }

    if (!d1.getString("NM_EXPUESTOS").equals(d2.getString("NM_EXPUESTOS"))) {
      return false;
    }

    if (!d1.getString("NM_ENFERMOS").equals(d2.getString("NM_ENFERMOS"))) {
      return false;
    }

    if (!d1.getString("NM_INGHOSP").equals(d2.getString("NM_INGHOSP"))) {
      return false;
    }

    if (!d1.getString("NM_DEFUNCION").equals(d2.getString("NM_DEFUNCION"))) {
      return false;
    }

    if (!d1.getString("FC_EXPOSICION").equals(d2.getString("FC_EXPOSICION"))) {
      return false;
    }

    if (!d1.getString("FC_ISINPRIMC").equals(d2.getString("FC_ISINPRIMC"))) {
      return false;
    }

    if (!d1.getString("FC_FSINPRIMC").equals(d2.getString("FC_FSINPRIMC"))) {
      return false;
    }

    if (!d1.getString("NM_PERINMIN").equals(d2.getString("NM_PERINMIN"))) {
      return false;
    }

    if (!d1.getString("NM_PERINMAX").equals(d2.getString("NM_PERINMAX"))) {
      return false;
    }

    if (!d1.getString("NM_PERINMED").equals(d2.getString("NM_PERINMED"))) {
      return false;
    }

    if (!d1.getString("NM_DCUACMIN").equals(d2.getString("NM_DCUACMIN"))) {
      return false;
    }

    if (!d1.getString("NM_DCUACMAX").equals(d2.getString("NM_DCUACMAX"))) {
      return false;
    }

    if (!d1.getString("NM_DCUACMED").equals(d2.getString("NM_DCUACMED"))) {
      return false;
    }

    if (!d1.getString("NM_VACNENF").equals(d2.getString("NM_VACNENF"))) {
      return false;
    }

    if (!d1.getString("NM_VACENF").equals(d2.getString("NM_VACENF"))) {
      return false;
    }

    if (!d1.getString("NM_NVACNENF").equals(d2.getString("NM_NVACNENF"))) {
      return false;
    }

    if (!d1.getString("NM_NVACENF").equals(d2.getString("NM_NVACENF"))) {
      return false;
    }

    if (!d1.getString("DS_OBSERV").equals(d2.getString("DS_OBSERV"))) {
      return false;
    }

    if (!d1.getString("IT_PERIN").equals(d2.getString("IT_PERIN"))) {
      return false;
    }

    if (!d1.getString("IT_DCUAC").equals(d2.getString("IT_DCUAC"))) {
      return false;
    }

    if (!d1.getString("CD_SITALERBRO").equals(d2.getString("CD_SITALERBRO"))) {
      return false;
    }

    return true;
  } // Fin compararCampos()

  /******************** Manejadores *************************/

  // Altas y Modificaciones
  void btnGrabarActionPerformed() {
    Lista lQuery = new Lista(); // Lista con queries
    Lista lResul = null;
    Lista lBloqueo = null; // Lista con datos del bloqueo
    Data bifTmp = new Data(); // Datos de la BIF (alta/mod)

    switch (modoEntrada) {
      case ALTA:
        if (validarDatos()) {
          // Recogida de datos de la BIF
          recogerDatos(bifTmp);

          // Comprobar la fecha de notificaci�n/exposici�n as� como enfermos/expuestos
          if (fechaCorrecta(bifTmp)) {

            try {
              // Montaje de la query de ALTA
              lQuery = prepararQTAlta(bifTmp);

              /*// Transacci�n con el servidor
                        SrvBIF srv = new SrvBIF();
                        srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver","jdbc:oracle:thin:@194.140.66.208:1521:ORCL","sive_desa","sive_desa");
                        lResul =srv.doDebug(0, lQuery);*/

              lResul =
                  BDatos.execSQL(applet, srvTrans, 0, lQuery); // 0 porque SrvBIF ignora dicho modo

              // Se establece la bifIni
              bifIni = bifTmp;

              // Se establece el nuevo CD_OPE/FC_ULTACT
              bifIni.put("CD_OPE", applet.getParametro("LOGIN"));
              bifIni.put("FC_ULTACT",
                         ( (Lista) lResul.firstElement()).getFC_ULTACT());

              // Se establece la nueva BIF en cada subpanel
              psup.cambiarBIF(bifIni);
              pBas.cambiarBIF(bifIni);
              pCol.cambiarBIF(bifIni);
              pTemp.cambiarBIF(bifIni);

              // Alta correcta => modoEntrada = MODIFICACION
              setTitle("Modificaci�n de Informe Final de Brote");
              modoEntrada = MODIFICACION;

              // Creacion de DBPanOtros y de PanelBrote
              pOtros = new DBPanOtros(applet, this, bifIni, modoEntrada, listas);
              tabPanel.InsertarPanel("Otros", pOtros);

              // Se crea el protocolo, se a�ade y se insertan las respuestas
              //  de forma vacia, sin validacion de campos obligatorios
              crearProtocolo();
              addProtocolo();
              if (pProtocolo != null) {
                pProtocolo.pnl.setIEstado(1); // MODIFICACION
              }

              psup.setModoEntrada(MODIFICACION);
              pBas.setModoEntrada(MODIFICACION);
              pCol.setModoEntrada(MODIFICACION);
              pTemp.setModoEntrada(MODIFICACION);

            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
            }
          } // Fin del fechaYEnfermoCorrecto
        } // validarDatos() ya muestra mensajes de error
        break;

      case MODIFICACION:
        if (validarDatos()) {
          // Recogida de datos de la BIF
          recogerDatos(bifTmp);

          if (fechaCorrecta(bifTmp)) {

            // Comparar la BIF inicial con la nueva
            if (compararCampos(bifIni, bifTmp)) {
              break;
            }

            // Montaje de la query de MODIFICACION
            lQuery = prepararQTModificacion(bifTmp);

            // Preparaci�n de los datos del bloqueo
            lBloqueo = prepararBloqueo();

            try {
              /*// Transacci�n con el servidor
                        SrvTransaccion srv = new SrvTransaccion();
                        srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver","jdbc:oracle:thin:@194.140.66.208:1521:ORCL","sive_desa","sive_desa");
                        lResul =srv.doDebug(10000, lQuery);*/

             lResul =
                 BDatos.execSQLBloqueo(applet, srvTrans, 10000, lQuery, // 10000 para que ejecute el bloqueo
                                       (QueryTool) lBloqueo.elementAt(0),
                                       (Data) lBloqueo.elementAt(1));

             // Se establece la bifIni
              bifIni = bifTmp;

              // Se establece el nuevo CD_OPE/FC_ULTACT
              bifIni.put("CD_OPE", applet.getParametro("LOGIN"));
              bifIni.put("FC_ULTACT",
                         ( (Lista) lResul.firstElement()).getFC_ULTACT());

              // Se establece la nueva BIF en cada subpanel
              psup.cambiarBIF(bifIni);
              pBas.cambiarBIF(bifIni);
              pCol.cambiarBIF(bifIni);
              pTemp.cambiarBIF(bifIni);
              pOtros.cambiarBIF(bifIni);

              // Insercion de respuestas del protocolo
              insertarProtocolo();

            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
            }
          } // Del comprobar fechas y num_enfermos.
        } // validarDatos() ya muestra mensajes de error
        break;
    } // Fin switch()

    // Si es ALTA Desaparece el dialogo
    /*if(modoEntrada == ALTA)
      dispose();*/
  } // Fin btnGrabarActionPerformed()

  // Bajas
  void btnAceptarActionPerformed() {
    Lista lQuery = new Lista(); // Lista con queries
    Lista lResul = null;
    Lista lBloqueo = null; // Lista con datos del bloqueo

    switch (modoEntrada) {
      case BAJA:

        // Montaje de la query de BAJA
        lQuery = prepararQTBaja(bifIni);

        // Preparaci�n de los datos del bloqueo
        lBloqueo = prepararBloqueo();

        try {
          /*        // Transacci�n con el servidor
                  SrvBIF srv = new SrvBIF();
                  srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver","jdbc:oracle:thin:@194.140.66.208:1521:ORCL","sive_desa","sive_desa");
                  lResul =srv.doDebug(10000, lQuery);*/

          lResul =
              BDatos.execSQLBloqueo(applet, srvTrans, 10000, lQuery, // 10000 para que ejecute el bloqueo
                                    (QueryTool) lBloqueo.elementAt(0),
                                    (Data) lBloqueo.elementAt(1));
        }
        catch (Exception ex) {
          this.getApp().trazaLog(ex);
          this.getApp().showError(ex.getMessage());
        }

        break;
    } // Fin switch()

    // Desaparece el dialogo
    dispose();
  } // Fin btnAceptarActionPerformed()

  // Altas/Mod/Bajas
  void btnCancelarActionPerformed() {
    // Desaparece el dialogo
    dispose();
  } // Fin btnCancelarActionPerformed()

  // Consulta
  void btnSalirActionPerformed() {
    // Desaparece el dialogo
    dispose();
  } // Fin btnSalirActionPerformed()

  void btnInvActionPerformed() {
    DBPanInves dlg = null;

    if (modoEntrada == MODIFICACION) {
      int sit = new Integer(bifIni.getString("CD_SITALERBRO")).intValue();
      switch (sit) {
        case 3:
          dlg = new DBPanInves(applet, MODIFICACION, bifIni);
          break;
        case 4:
          dlg = new DBPanInves(applet, CONSULTA, bifIni);
          break;
      }
    }
    else {
      dlg = new DBPanInves(applet, modoEntrada, bifIni);
    }

    dlg.show();
    dlg = null;
  } // Fin btnInvActionPerformed()

  /******* M�todos auxiliares de preparaci�n de QueryTool *********/

  // Devuelve una Lista cuyo primer elemento es la QueryTool del bloqueo
  //  y su segundo elemento es el Data del bloqueo
  private Lista prepararBloqueo() {
    Lista vResult = new Lista();
    QueryTool qtBloqueo = new QueryTool();
    Data dtBloqueo = new Data();

    // Tabla  a bloquear
    qtBloqueo.putName("SIVE_BROTES");

    // Campos CD_OPE y FC_ULTACT
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Filtro del registro a bloquear
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", bifIni.getString("CD_ANO"));
    qtBloqueo.putOperator("CD_ANO", "=");
    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", bifIni.getString("NM_ALERBRO"));
    qtBloqueo.putOperator("NM_ALERBRO", "=");

    // Valores del bloqueo
    dtBloqueo.put("CD_OPE", bifIni.getString("CD_OPE"));
    dtBloqueo.put("FC_ULTACT", bifIni.getString("FC_ULTACT"));

    // Devolucion
    vResult.addElement(qtBloqueo);
    vResult.addElement(dtBloqueo);
    return vResult;
  } // Fin prepararBloqueo()

  // Devuelve una Lista con las queries que debe procesar el
  //  servlet SrvBIF para dar de alta un BIF
  private Lista prepararQTAlta(Data bif) throws Exception {
    Lista lResul = new Lista();

    //(PRIMERO POR ACTUALIZACION DE CD_OPE/FC_ULTACT)
    // Insercion de los datos del  brote
    QueryTool qtS_B = new QueryTool();
    Data dtS_B = new Data();

    qtS_B.putName("SIVE_BROTES");

    qtS_B.putType("CD_ANO", QueryTool.STRING);
    qtS_B.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_B.putType("CD_TIPOCOL", QueryTool.STRING);
    qtS_B.putType("CD_TNOTIF", QueryTool.STRING);
    qtS_B.putType("CD_TRANSMIS", QueryTool.STRING);
    qtS_B.putType("CD_GRUPO", QueryTool.STRING);
    qtS_B.putType("CD_TBROTE", QueryTool.STRING);
    qtS_B.putType("FC_FECHAHORA", QueryTool.TIMESTAMP);
    qtS_B.putType("DS_BROTE", QueryTool.STRING);
    qtS_B.putType("DS_NOMCOL", QueryTool.STRING);
    qtS_B.putType("DS_DIRCOL", QueryTool.STRING);
    qtS_B.putType("CD_POSTALCOL", QueryTool.STRING);
    qtS_B.putType("CD_PROVCOL", QueryTool.STRING);
    qtS_B.putType("CD_MUNCOL", QueryTool.STRING);
    qtS_B.putType("DS_NMCALLE", QueryTool.STRING);
    qtS_B.putType("DS_PISOCOL", QueryTool.STRING);
    qtS_B.putType("DS_TELCOL", QueryTool.STRING);
    qtS_B.putType("CD_NIVEL_1_LCA", QueryTool.STRING);
    qtS_B.putType("CD_NIVEL_2_LCA", QueryTool.STRING);
    qtS_B.putType("CD_ZBS_LCA", QueryTool.STRING);
    qtS_B.putType("CD_NIVEL_1_LE", QueryTool.STRING);
    qtS_B.putType("CD_NIVEL_2_LE", QueryTool.STRING);
    qtS_B.putType("CD_ZBS_LE", QueryTool.STRING);
    qtS_B.putType("NM_MANIPUL", QueryTool.INTEGER);
    qtS_B.putType("IT_RESCALC", QueryTool.STRING);
    qtS_B.putType("NM_EXPUESTOS", QueryTool.INTEGER);
    qtS_B.putType("NM_ENFERMOS", QueryTool.INTEGER);
    qtS_B.putType("NM_INGHOSP", QueryTool.INTEGER);
    qtS_B.putType("NM_DEFUNCION", QueryTool.INTEGER);
    qtS_B.putType("FC_EXPOSICION", QueryTool.TIMESTAMP);
    qtS_B.putType("FC_ISINPRIMC", QueryTool.TIMESTAMP);
    qtS_B.putType("FC_FSINPRIMC", QueryTool.TIMESTAMP);
    qtS_B.putType("NM_PERINMIN", QueryTool.INTEGER);
    qtS_B.putType("NM_PERINMAX", QueryTool.INTEGER);
    qtS_B.putType("NM_PERINMED", QueryTool.REAL);
    qtS_B.putType("NM_DCUACMIN", QueryTool.INTEGER);
    qtS_B.putType("NM_DCUACMAX", QueryTool.INTEGER);
    qtS_B.putType("NM_DCUACMED", QueryTool.REAL);
    qtS_B.putType("NM_VACNENF", QueryTool.INTEGER);
    qtS_B.putType("NM_VACENF", QueryTool.INTEGER);
    qtS_B.putType("NM_NVACNENF", QueryTool.INTEGER);
    qtS_B.putType("NM_NVACENF", QueryTool.INTEGER);
    qtS_B.putType("DS_OBSERV", QueryTool.STRING);
    qtS_B.putType("IT_PERIN", QueryTool.STRING);
    qtS_B.putType("IT_DCUAC", QueryTool.STRING);
    qtS_B.putType("CD_OPE", QueryTool.STRING);
    qtS_B.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    qtS_B.putValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_B.putValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_B.putValue("CD_TIPOCOL", bif.getString("CD_TIPOCOL"));
    qtS_B.putValue("CD_TNOTIF", bif.getString("CD_TNOTIF"));
    qtS_B.putValue("CD_TRANSMIS", bif.getString("CD_TRANSMIS"));
    qtS_B.putValue("CD_GRUPO", bif.getString("CD_GRUPO"));
    qtS_B.putValue("CD_TBROTE", bif.getString("CD_TBROTE"));
    qtS_B.putValue("FC_FECHAHORA", bif.getString("FC_FECHAHORA"));
    qtS_B.putValue("DS_BROTE", bif.getString("DS_BROTE"));
    qtS_B.putValue("DS_NOMCOL", bif.getString("DS_NOMCOL"));
    qtS_B.putValue("DS_DIRCOL", bif.getString("DS_DIRCOL"));
    qtS_B.putValue("CD_POSTALCOL", bif.getString("CD_POSTALCOL"));
    qtS_B.putValue("CD_PROVCOL", bif.getString("CD_PROVCOL"));
    qtS_B.putValue("CD_MUNCOL", bif.getString("CD_MUNCOL"));
    qtS_B.putValue("DS_NMCALLE", bif.getString("DS_NMCALLE"));
    qtS_B.putValue("DS_PISOCOL", bif.getString("DS_PISOCOL"));
    qtS_B.putValue("DS_TELCOL", bif.getString("DS_TELCOL"));
    qtS_B.putValue("CD_NIVEL_1_LCA", bif.getString("CD_NIVEL_1_LCA"));
    qtS_B.putValue("CD_NIVEL_2_LCA", bif.getString("CD_NIVEL_2_LCA"));
    qtS_B.putValue("CD_ZBS_LCA", bif.getString("CD_ZBS_LCA"));
    qtS_B.putValue("CD_NIVEL_1_LE", bif.getString("CD_NIVEL_1_LE"));
    qtS_B.putValue("CD_NIVEL_2_LE", bif.getString("CD_NIVEL_2_LE"));
    qtS_B.putValue("CD_ZBS_LE", bif.getString("CD_ZBS_LE"));
    qtS_B.putValue("NM_MANIPUL", bif.getString("NM_MANIPUL"));
    qtS_B.putValue("IT_RESCALC", "N");
    qtS_B.putValue("NM_EXPUESTOS", bif.getString("NM_EXPUESTOS"));
    qtS_B.putValue("NM_ENFERMOS", bif.getString("NM_ENFERMOS"));
    qtS_B.putValue("NM_INGHOSP", bif.getString("NM_INGHOSP"));
    qtS_B.putValue("NM_DEFUNCION", bif.getString("NM_DEFUNCION"));
    qtS_B.putValue("FC_EXPOSICION", bif.getString("FC_EXPOSICION"));
    qtS_B.putValue("FC_ISINPRIMC", bif.getString("FC_ISINPRIMC"));
    qtS_B.putValue("FC_FSINPRIMC", bif.getString("FC_FSINPRIMC"));
    qtS_B.putValue("NM_PERINMIN", bif.getString("NM_PERINMIN"));
    qtS_B.putValue("NM_PERINMAX", bif.getString("NM_PERINMAX"));
    qtS_B.putValue("NM_PERINMED", bif.getString("NM_PERINMED"));
    qtS_B.putValue("NM_DCUACMIN", bif.getString("NM_DCUACMIN"));
    qtS_B.putValue("NM_DCUACMAX", bif.getString("NM_DCUACMAX"));
    qtS_B.putValue("NM_DCUACMED", bif.getString("NM_DCUACMED"));
    qtS_B.putValue("NM_VACNENF", bif.getString("NM_VACNENF"));
    qtS_B.putValue("NM_VACENF", bif.getString("NM_VACENF"));
    qtS_B.putValue("NM_NVACNENF", bif.getString("NM_NVACNENF"));
    qtS_B.putValue("NM_NVACENF", bif.getString("NM_NVACENF"));
    qtS_B.putValue("DS_OBSERV", bif.getString("DS_OBSERV"));
    qtS_B.putValue("IT_PERIN", bif.getString("IT_PERIN"));
    qtS_B.putValue("IT_DCUAC", bif.getString("IT_DCUAC"));
    qtS_B.putValue("CD_OPE", applet.getParametro("LOGIN"));
    qtS_B.putValue("FC_ULTACT", "");

    dtS_B.put("3", qtS_B); // 3 -> Inserci�n
    lResul.addElement(dtS_B);

    // Establecer el estado de la alerta asociada a 3
    prepararCambioSituacionAlerta(bif, lResul, "3", "CHANGE");

    // Dar de alta al usuario en la tabla SIVE_INVESTIGADORES
    QueryTool qtS_I = new QueryTool();
    Data dtS_I = new Data();

    qtS_I.putName("SIVE_INVESTIGADOR");

    qtS_I.putType("CD_ANO", QueryTool.STRING);
    qtS_I.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_I.putType("CD_USUARIO", QueryTool.STRING);
    qtS_I.putType("CD_CA", QueryTool.STRING);
    qtS_I.putType("FC_ALTA", QueryTool.DATE);

    qtS_I.putValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_I.putValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_I.putValue("CD_USUARIO", applet.getParametro("LOGIN"));
    qtS_I.putValue("CD_CA", applet.getParametro("CA"));
    qtS_I.putValue("FC_ALTA", Format.fechaActual());

    dtS_I.put("3", qtS_I); // 3 -> Inserci�n
    lResul.addElement(dtS_I);

    return lResul;
  } // Fin prepararQTAlta()

  // Devuelve una Lista con las queries que debe procesar el
  //  servlet SrvBIF para modificar un BIF
  private Lista prepararQTModificacion(Data bif) {
    Lista lResul = new Lista();

    // (PRIMERO POR ACTUALIZACION DE CD_OPE/FC_ULTACT)

    // Modificacion del brote
    QueryTool qtS_B = new QueryTool();
    Data dtS_B = new Data();

    qtS_B.putName("SIVE_BROTES");

    qtS_B.putType("CD_TIPOCOL", QueryTool.STRING);
    qtS_B.putType("CD_TNOTIF", QueryTool.STRING);
    qtS_B.putType("CD_TRANSMIS", QueryTool.STRING);
    qtS_B.putType("CD_GRUPO", QueryTool.STRING);
    qtS_B.putType("CD_TBROTE", QueryTool.STRING);
    qtS_B.putType("FC_FECHAHORA", QueryTool.TIMESTAMP);
    qtS_B.putType("DS_BROTE", QueryTool.STRING);
    qtS_B.putType("DS_NOMCOL", QueryTool.STRING);
    qtS_B.putType("DS_DIRCOL", QueryTool.STRING);
    qtS_B.putType("CD_POSTALCOL", QueryTool.STRING);
    qtS_B.putType("CD_PROVCOL", QueryTool.STRING);
    qtS_B.putType("CD_MUNCOL", QueryTool.STRING);
    qtS_B.putType("DS_NMCALLE", QueryTool.STRING);
    qtS_B.putType("DS_PISOCOL", QueryTool.STRING);
    qtS_B.putType("DS_TELCOL", QueryTool.STRING);
    qtS_B.putType("CD_NIVEL_1_LCA", QueryTool.STRING);
    qtS_B.putType("CD_NIVEL_2_LCA", QueryTool.STRING);
    qtS_B.putType("CD_ZBS_LCA", QueryTool.STRING);
    qtS_B.putType("CD_NIVEL_1_LE", QueryTool.STRING);
    qtS_B.putType("CD_NIVEL_2_LE", QueryTool.STRING);
    qtS_B.putType("CD_ZBS_LE", QueryTool.STRING);
    qtS_B.putType("NM_MANIPUL", QueryTool.INTEGER);
    qtS_B.putType("IT_RESCALC", QueryTool.STRING);
    qtS_B.putType("NM_EXPUESTOS", QueryTool.INTEGER);
    qtS_B.putType("NM_ENFERMOS", QueryTool.INTEGER);
    qtS_B.putType("NM_INGHOSP", QueryTool.INTEGER);
    qtS_B.putType("NM_DEFUNCION", QueryTool.INTEGER);
    qtS_B.putType("FC_EXPOSICION", QueryTool.TIMESTAMP);
    qtS_B.putType("FC_ISINPRIMC", QueryTool.TIMESTAMP);
    qtS_B.putType("FC_FSINPRIMC", QueryTool.TIMESTAMP);
    qtS_B.putType("NM_PERINMIN", QueryTool.INTEGER);
    qtS_B.putType("NM_PERINMAX", QueryTool.INTEGER);
    qtS_B.putType("NM_PERINMED", QueryTool.REAL);
    qtS_B.putType("NM_DCUACMIN", QueryTool.INTEGER);
    qtS_B.putType("NM_DCUACMAX", QueryTool.INTEGER);
    qtS_B.putType("NM_DCUACMED", QueryTool.REAL);
    qtS_B.putType("NM_VACNENF", QueryTool.INTEGER);
    qtS_B.putType("NM_VACENF", QueryTool.INTEGER);
    qtS_B.putType("NM_NVACNENF", QueryTool.INTEGER);
    qtS_B.putType("NM_NVACENF", QueryTool.INTEGER);
    qtS_B.putType("DS_OBSERV", QueryTool.STRING);
    qtS_B.putType("IT_PERIN", QueryTool.STRING);
    qtS_B.putType("IT_DCUAC", QueryTool.STRING);
    qtS_B.putType("CD_OPE", QueryTool.STRING);
    qtS_B.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    qtS_B.putValue("CD_TIPOCOL", bif.getString("CD_TIPOCOL"));
    qtS_B.putValue("CD_TNOTIF", bif.getString("CD_TNOTIF"));
    qtS_B.putValue("CD_TRANSMIS", bif.getString("CD_TRANSMIS"));
    qtS_B.putValue("CD_GRUPO", bif.getString("CD_GRUPO"));
    qtS_B.putValue("CD_TBROTE", bif.getString("CD_TBROTE"));
    qtS_B.putValue("FC_FECHAHORA", bif.getString("FC_FECHAHORA"));
    qtS_B.putValue("DS_BROTE", bif.getString("DS_BROTE"));
    qtS_B.putValue("DS_NOMCOL", bif.getString("DS_NOMCOL"));
    qtS_B.putValue("DS_DIRCOL", bif.getString("DS_DIRCOL"));
    qtS_B.putValue("CD_POSTALCOL", bif.getString("CD_POSTALCOL"));
    qtS_B.putValue("CD_PROVCOL", bif.getString("CD_PROVCOL"));
    qtS_B.putValue("CD_MUNCOL", bif.getString("CD_MUNCOL"));
    qtS_B.putValue("DS_NMCALLE", bif.getString("DS_NMCALLE"));
    qtS_B.putValue("DS_PISOCOL", bif.getString("DS_PISOCOL"));
    qtS_B.putValue("DS_TELCOL", bif.getString("DS_TELCOL"));
    qtS_B.putValue("CD_NIVEL_1_LCA", bif.getString("CD_NIVEL_1_LCA"));
    qtS_B.putValue("CD_NIVEL_2_LCA", bif.getString("CD_NIVEL_2_LCA"));
    qtS_B.putValue("CD_ZBS_LCA", bif.getString("CD_ZBS_LCA"));
    qtS_B.putValue("CD_NIVEL_1_LE", bif.getString("CD_NIVEL_1_LE"));
    qtS_B.putValue("CD_NIVEL_2_LE", bif.getString("CD_NIVEL_2_LE"));
    qtS_B.putValue("CD_ZBS_LE", bif.getString("CD_ZBS_LE"));
    qtS_B.putValue("NM_MANIPUL", bif.getString("NM_MANIPUL"));
    qtS_B.putValue("IT_RESCALC", "N");
    qtS_B.putValue("NM_EXPUESTOS", bif.getString("NM_EXPUESTOS"));
    qtS_B.putValue("NM_ENFERMOS", bif.getString("NM_ENFERMOS"));
    qtS_B.putValue("NM_INGHOSP", bif.getString("NM_INGHOSP"));
    qtS_B.putValue("NM_DEFUNCION", bif.getString("NM_DEFUNCION"));
    qtS_B.putValue("FC_EXPOSICION", bif.getString("FC_EXPOSICION"));
    qtS_B.putValue("FC_ISINPRIMC", bif.getString("FC_ISINPRIMC"));
    qtS_B.putValue("FC_FSINPRIMC", bif.getString("FC_FSINPRIMC"));
    qtS_B.putValue("NM_PERINMIN", bif.getString("NM_PERINMIN"));
    qtS_B.putValue("NM_PERINMAX", bif.getString("NM_PERINMAX"));
    qtS_B.putValue("NM_PERINMED", bif.getString("NM_PERINMED"));
    qtS_B.putValue("NM_DCUACMIN", bif.getString("NM_DCUACMIN"));
    qtS_B.putValue("NM_DCUACMAX", bif.getString("NM_DCUACMAX"));
    qtS_B.putValue("NM_DCUACMED", bif.getString("NM_DCUACMED"));
    qtS_B.putValue("NM_VACNENF", bif.getString("NM_VACNENF"));
    qtS_B.putValue("NM_VACENF", bif.getString("NM_VACENF"));
    qtS_B.putValue("NM_NVACNENF", bif.getString("NM_NVACNENF"));
    qtS_B.putValue("NM_NVACENF", bif.getString("NM_NVACENF"));
    qtS_B.putValue("DS_OBSERV", bif.getString("DS_OBSERV"));
    qtS_B.putValue("IT_PERIN", bif.getString("IT_PERIN"));
    qtS_B.putValue("IT_DCUAC", bif.getString("IT_DCUAC"));
    qtS_B.putValue("CD_OPE", applet.getParametro("LOGIN"));
    qtS_B.putValue("FC_ULTACT", "");

    // Parte del where
    qtS_B.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_B.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_B.putOperator("CD_ANO", "=");
    qtS_B.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_B.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_B.putOperator("NM_ALERBRO", "=");

    dtS_B.put("4", qtS_B); // 4 -> Modificaci�n
    lResul.addElement(dtS_B);

    // Modificacion de los campos de alerta si es necesario (ha cambiado la situacion de la alerta)
    boolean cambioSitAlerta = !bif.getString("CD_SITALERBRO").equals(bifIni.
        getString("CD_SITALERBRO"));
    boolean cambioFechaInv = !bif.getString("FC_ALERBRO").equals(bifIni.
        getString("FC_ALERBRO"));

    if (cambioSitAlerta || cambioFechaInv) {
      if (cambioFechaInv) {
        prepararCambioSituacionAlerta(bif, lResul,
                                      bif.getString("CD_SITALERBRO"),
                                      bif.getString("FC_ALERBRO"));
      }
      else {
        if (cambioSitAlerta) {
          prepararCambioSituacionAlerta(bif, lResul,
                                        bif.getString("CD_SITALERBRO"),
                                        "CHANGE");
        }
        else {
          prepararCambioSituacionAlerta(bif, lResul,
                                        bif.getString("CD_SITALERBRO"),
                                        bif.getString("FC_ALERBRO"));
        }
      }
    }

    return lResul;
  } // Fin prepararQTModificacion()

  // Devuelve una Lista con las queries que debe procesar el
  //  servlet SrvBIF para dar de baja un BIF
  private Lista prepararQTBaja(Data bif) {
    Lista lResul = new Lista();

    // Insertar Registro en el historico
    prepararInsertRegistroHistorico(bif, lResul);

    // Borrado de investigadores
    prepararBorradoInvestigadores(bif, lResul);

    // Borrado del protocolo + los modelos propios del brote
    prepararBorradoProtocolo(bif, lResul);

    // Borrado de tablas perifericas
    prepararBorradoTablasPerifericas(bif, lResul);

    // Paso de la alerta asociada a estado e1 (Alerta No Confirmada como brote)
    prepararCambioSituacionAlerta(bif, lResul, "1", "CHANGE");

    // LA ULTIMA PORQUE LAS DEMAS DEPENDEN DE ESTE REGISTRO
    // Borrado del brote
    QueryTool qtS_B = new QueryTool();
    Data dtS_B = new Data();

    qtS_B.putName("SIVE_BROTES");

    qtS_B.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_B.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_B.putOperator("CD_ANO", "=");
    qtS_B.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_B.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_B.putOperator("NM_ALERBRO", "=");

    dtS_B.put("5", qtS_B); // 5 -> Baja
    lResul.addElement(dtS_B);

    return lResul;
  } // Fin prepararQTBaja()

  public void prepararInsertRegistroHistorico(Data bif, Lista lResul) {
    // Insercion de los datos del  brote en el registro historico
    QueryTool qtS_B = new QueryTool();
    Data dtS_B = new Data();

    qtS_B.putName("SIVE_HIST_BROTES");

    qtS_B.putType("CD_ANO", QueryTool.STRING);
    qtS_B.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_B.putType("CD_TIPOCOL", QueryTool.STRING);
    qtS_B.putType("CD_TNOTIF", QueryTool.STRING);
    qtS_B.putType("CD_TRANSMIS", QueryTool.STRING);
    qtS_B.putType("CD_GRUPO", QueryTool.STRING);
    qtS_B.putType("CD_TBROTE", QueryTool.STRING);
    qtS_B.putType("FC_FECHAHORA", QueryTool.TIMESTAMP);
    qtS_B.putType("DS_BROTE", QueryTool.STRING);
    qtS_B.putType("DS_NOMCOL", QueryTool.STRING);
    qtS_B.putType("DS_DIRCOL", QueryTool.STRING);
    qtS_B.putType("CD_POSTALCOL", QueryTool.STRING);
    qtS_B.putType("CD_PROVCOL", QueryTool.STRING);
    qtS_B.putType("CD_MUNCOL", QueryTool.STRING);
    qtS_B.putType("DS_NMCALLE", QueryTool.STRING);
    qtS_B.putType("DS_PISOCOL", QueryTool.STRING);
    qtS_B.putType("DS_TELCOL", QueryTool.STRING);
    qtS_B.putType("CD_NIVEL_1_LCA", QueryTool.STRING);
    qtS_B.putType("CD_NIVEL_2_LCA", QueryTool.STRING);
    qtS_B.putType("CD_ZBS_LCA", QueryTool.STRING);
    qtS_B.putType("CD_NIVEL_1_LE", QueryTool.STRING);
    qtS_B.putType("CD_NIVEL_2_LE", QueryTool.STRING);
    qtS_B.putType("CD_ZBS_LE", QueryTool.STRING);
    qtS_B.putType("NM_MANIPUL", QueryTool.INTEGER);
    qtS_B.putType("IT_RESCALC", QueryTool.STRING);
    qtS_B.putType("NM_EXPUESTOS", QueryTool.INTEGER);
    qtS_B.putType("NM_ENFERMOS", QueryTool.INTEGER);
    qtS_B.putType("NM_INGHOSP", QueryTool.INTEGER);
    qtS_B.putType("NM_DEFUNCION", QueryTool.INTEGER);
    qtS_B.putType("FC_EXPOSICION", QueryTool.TIMESTAMP);
    qtS_B.putType("FC_ISINPRIMC", QueryTool.TIMESTAMP);
    qtS_B.putType("FC_FSINPRIMC", QueryTool.TIMESTAMP);
    qtS_B.putType("NM_PERINMIN", QueryTool.INTEGER);
    qtS_B.putType("NM_PERINMAX", QueryTool.INTEGER);
    qtS_B.putType("NM_PERINMED", QueryTool.REAL);
    qtS_B.putType("NM_DCUACMIN", QueryTool.INTEGER);
    qtS_B.putType("NM_DCUACMAX", QueryTool.INTEGER);
    qtS_B.putType("NM_DCUACMED", QueryTool.REAL);
    qtS_B.putType("NM_VACNENF", QueryTool.INTEGER);
    qtS_B.putType("NM_VACENF", QueryTool.INTEGER);
    qtS_B.putType("NM_NVACNENF", QueryTool.INTEGER);
    qtS_B.putType("NM_NVACENF", QueryTool.INTEGER);
    qtS_B.putType("DS_OBSERV", QueryTool.STRING);
    qtS_B.putType("IT_PERIN", QueryTool.STRING);
    qtS_B.putType("IT_DCUAC", QueryTool.STRING);
    qtS_B.putType("CD_MOTBAJA", QueryTool.STRING);
    qtS_B.putType("CD_OPE", QueryTool.STRING);
    qtS_B.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    qtS_B.putValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_B.putValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_B.putValue("CD_TIPOCOL", bif.getString("CD_TIPOCOL"));
    qtS_B.putValue("CD_TNOTIF", bif.getString("CD_TNOTIF"));
    qtS_B.putValue("CD_TRANSMIS", bif.getString("CD_TRANSMIS"));
    qtS_B.putValue("CD_GRUPO", bif.getString("CD_GRUPO"));
    qtS_B.putValue("CD_TBROTE", bif.getString("CD_TBROTE"));
    qtS_B.putValue("FC_FECHAHORA", bif.getString("FC_FECHAHORA"));
    qtS_B.putValue("DS_BROTE", bif.getString("DS_BROTE"));
    qtS_B.putValue("DS_NOMCOL", bif.getString("DS_NOMCOL"));
    qtS_B.putValue("DS_DIRCOL", bif.getString("DS_DIRCOL"));
    qtS_B.putValue("CD_POSTALCOL", bif.getString("CD_POSTALCOL"));
    qtS_B.putValue("CD_PROVCOL", bif.getString("CD_PROVCOL"));
    qtS_B.putValue("CD_MUNCOL", bif.getString("CD_MUNCOL"));
    qtS_B.putValue("DS_NMCALLE", bif.getString("DS_NMCALLE"));
    qtS_B.putValue("DS_PISOCOL", bif.getString("DS_PISOCOL"));
    qtS_B.putValue("DS_TELCOL", bif.getString("DS_TELCOL"));
    qtS_B.putValue("CD_NIVEL_1_LCA", bif.getString("CD_NIVEL_1_LCA"));
    qtS_B.putValue("CD_NIVEL_2_LCA", bif.getString("CD_NIVEL_2_LCA"));
    qtS_B.putValue("CD_ZBS_LCA", bif.getString("CD_ZBS_LCA"));
    qtS_B.putValue("CD_NIVEL_1_LE", bif.getString("CD_NIVEL_1_LE"));
    qtS_B.putValue("CD_NIVEL_2_LE", bif.getString("CD_NIVEL_2_LE"));
    qtS_B.putValue("CD_ZBS_LE", bif.getString("CD_ZBS_LE"));
    qtS_B.putValue("NM_MANIPUL", bif.getString("NM_MANIPUL"));
    qtS_B.putValue("IT_RESCALC", "N");
    qtS_B.putValue("NM_EXPUESTOS", bif.getString("NM_EXPUESTOS"));
    qtS_B.putValue("NM_ENFERMOS", bif.getString("NM_ENFERMOS"));
    qtS_B.putValue("NM_INGHOSP", bif.getString("NM_INGHOSP"));
    qtS_B.putValue("NM_DEFUNCION", bif.getString("NM_DEFUNCION"));
    qtS_B.putValue("FC_EXPOSICION", bif.getString("FC_EXPOSICION"));
    qtS_B.putValue("FC_ISINPRIMC", bif.getString("FC_ISINPRIMC"));
    qtS_B.putValue("FC_FSINPRIMC", bif.getString("FC_FSINPRIMC"));
    qtS_B.putValue("NM_PERINMIN", bif.getString("NM_PERINMIN"));
    qtS_B.putValue("NM_PERINMAX", bif.getString("NM_PERINMAX"));
    qtS_B.putValue("NM_PERINMED", bif.getString("NM_PERINMED"));
    qtS_B.putValue("NM_DCUACMIN", bif.getString("NM_DCUACMIN"));
    qtS_B.putValue("NM_DCUACMAX", bif.getString("NM_DCUACMAX"));
    qtS_B.putValue("NM_DCUACMED", bif.getString("NM_DCUACMED"));
    qtS_B.putValue("NM_VACNENF", bif.getString("NM_VACNENF"));
    qtS_B.putValue("NM_VACENF", bif.getString("NM_VACENF"));
    qtS_B.putValue("NM_NVACNENF", bif.getString("NM_NVACNENF"));
    qtS_B.putValue("NM_NVACENF", bif.getString("NM_NVACENF"));
    qtS_B.putValue("DS_OBSERV", bif.getString("DS_OBSERV"));
    qtS_B.putValue("IT_PERIN", bif.getString("IT_PERIN"));
    qtS_B.putValue("IT_DCUAC", bif.getString("IT_DCUAC"));
    qtS_B.putValue("CD_MOTBAJA", "");
    qtS_B.putValue("CD_OPE", applet.getParametro("LOGIN"));
    qtS_B.putValue("FC_ULTACT", "");

    dtS_B.put("3", qtS_B); // 3 -> Inserci�n
    lResul.addElement(dtS_B);

  } // Fin prepararInsertRegistroHistorico()

  public void prepararBorradoInvestigadores(Data bif, Lista lResul) {
    QueryTool qtS_INV = new QueryTool();
    Data dtS_INV = new Data();

    qtS_INV.putName("SIVE_INVESTIGADOR");

    qtS_INV.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_INV.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_INV.putOperator("CD_ANO", "=");
    qtS_INV.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_INV.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_INV.putOperator("NM_ALERBRO", "=");

    dtS_INV.put("5", qtS_INV); // 5 -> Baja
    lResul.addElement(dtS_INV);
  } // Fin prepararInsertRegistroHistorico()

  public void prepararBorradoProtocolo(Data bif, Lista lResul) {

    // Borrado de TODAS las respuestas del brote (SIVE_RESP_BROTES por CD_ANO/NM_ALERBRO)

    QueryTool qtS_RESP_B = new QueryTool();
    Data dtS_RESP_B = new Data();

    qtS_RESP_B.putName("SIVE_RESP_BROTES");

    qtS_RESP_B.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_RESP_B.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_RESP_B.putOperator("CD_ANO", "=");
    qtS_RESP_B.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_RESP_B.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_RESP_B.putOperator("NM_ALERBRO", "=");

    dtS_RESP_B.put("5", qtS_RESP_B); // 5 -> Baja
    lResul.addElement(dtS_RESP_B);

    /* COMENTADO ARS 13-07-01: NO SE BORRAN LAS PREGUNTAS
     Se comentan estos tres borrados, porque realizan el borrado
     completo del protocolo, no s�lo las respuestas, sino tambi�n
     las preguntas. Esto daba problemas al borrar, pues no se borraban
     todas las respuestas para una misma pregunta, al depender del brote.
     Adem�s tampoco tiene sentido borrar las preguntas del protocolo.
        // Borrado de SIVE_LINEA_ITEM
        QueryTool qtS_LINEA_I = new QueryTool();
        Vector vSubS_LINEA_I = new Vector();
        Data dtSubS_LINEA_I = null;
        Data dtS_LINEA_I = new Data();
        qtS_LINEA_I.putName("SIVE_LINEA_ITEM");
        qtS_LINEA_I.putSubquery("(CD_TSIVE||CD_MODELO)","select (CD_TSIVE||CD_MODELO) from SIVE_MODELO where CD_ANO=? and NM_ALERBRO=?");
        dtSubS_LINEA_I = new Data();
         dtSubS_LINEA_I.put(new Integer(QueryTool.STRING),bif.getString("CD_ANO"));
        vSubS_LINEA_I.addElement(dtSubS_LINEA_I);
        dtSubS_LINEA_I = new Data();
         dtSubS_LINEA_I.put(new Integer(QueryTool.INTEGER),bif.getString("NM_ALERBRO"));
        vSubS_LINEA_I.addElement(dtSubS_LINEA_I);
        qtS_LINEA_I.putVectorSubquery("(CD_TSIVE||CD_MODELO)",vSubS_LINEA_I);
        dtS_LINEA_I.put("5",qtS_LINEA_I); // 5 -> Baja
        lResul.addElement(dtS_LINEA_I);
        // Borrado de SIVE_LINEASM
        QueryTool qtS_LINEASM = new QueryTool();
        Vector vSubS_LINEASM = new Vector();
        Data dtSubS_LINEASM = null;
        Data dtS_LINEASM = new Data();
        qtS_LINEASM.putName("SIVE_LINEASM");
        qtS_LINEASM.putSubquery("(CD_TSIVE||CD_MODELO)","select (CD_TSIVE||CD_MODELO) from SIVE_MODELO where CD_ANO=? and NM_ALERBRO=?");
        dtSubS_LINEASM = new Data();
         dtSubS_LINEASM.put(new Integer(QueryTool.STRING),bif.getString("CD_ANO"));
        vSubS_LINEASM.addElement(dtSubS_LINEASM);
        dtSubS_LINEASM = new Data();
         dtSubS_LINEASM.put(new Integer(QueryTool.INTEGER),bif.getString("NM_ALERBRO"));
        vSubS_LINEASM.addElement(dtSubS_LINEASM);
        qtS_LINEASM.putVectorSubquery("(CD_TSIVE||CD_MODELO)",vSubS_LINEASM);
        dtS_LINEASM.put("5",qtS_LINEASM); // 5 -> Baja
        lResul.addElement(dtS_LINEASM);
        // Borrado de los modelos asoicados al brote
        QueryTool qtS_MODELO = new QueryTool();
        Data dtS_MODELO = new Data();
        qtS_MODELO.putName("SIVE_MODELO");
        qtS_MODELO.putWhereType("CD_ANO",QueryTool.STRING);
        qtS_MODELO.putWhereValue("CD_ANO",bif.getString("CD_ANO"));
        qtS_MODELO.putOperator("CD_ANO","=");
        qtS_MODELO.putWhereType("NM_ALERBRO",QueryTool.INTEGER);
        qtS_MODELO.putWhereValue("NM_ALERBRO",bif.getString("NM_ALERBRO"));
        qtS_MODELO.putOperator("NM_ALERBRO","=");
        dtS_MODELO.put("5",qtS_MODELO); // 5 -> Baja
        lResul.addElement(dtS_MODELO);
     */
  } // Fin prepararBorradoProtocolo()

  public void prepararBorradoTablasPerifericas(Data bif, Lista lResul) {

    // Borrado de agentes causales

    QueryTool qtS_AC = new QueryTool();
    Data dtS_AC = new Data();

    qtS_AC.putName("SIVE_AGCAUSAL_BROTE");

    qtS_AC.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_AC.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_AC.putOperator("CD_ANO", "=");
    qtS_AC.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_AC.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_AC.putOperator("NM_ALERBRO", "=");

    dtS_AC.put("5", qtS_AC); // 5 -> Baja
    lResul.addElement(dtS_AC);

    // Borrado de distribuciones de edad

    QueryTool qtS_DE = new QueryTool();
    Data dtS_DE = new Data();

    qtS_DE.putName("SIVE_BROTES_DISTGEDAD");

    qtS_DE.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_DE.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_DE.putOperator("CD_ANO", "=");
    qtS_DE.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_DE.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_DE.putOperator("NM_ALERBRO", "=");

    dtS_DE.put("5", qtS_DE); // 5 -> Baja
    lResul.addElement(dtS_DE);

    // Borrado de factores contribuyentes

    QueryTool qtS_FC = new QueryTool();
    Data dtS_FC = new Data();

    qtS_FC.putName("SIVE_BROTES_FACCONT");

    qtS_FC.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_FC.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_FC.putOperator("CD_ANO", "=");
    qtS_FC.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_FC.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_FC.putOperator("NM_ALERBRO", "=");

    dtS_FC.put("5", qtS_FC); // 5 -> Baja
    lResul.addElement(dtS_FC);

    // Borrado de medidas

    QueryTool qtS_MED = new QueryTool();
    Data dtS_MED = new Data();

    qtS_MED.putName("SIVE_BROTES_MED");

    qtS_MED.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_MED.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_MED.putOperator("CD_ANO", "=");
    qtS_MED.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_MED.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_MED.putOperator("NM_ALERBRO", "=");

    dtS_MED.put("5", qtS_MED); // 5 -> Baja
    lResul.addElement(dtS_MED);

    // Borrado de valores de factor de riesgo y de factores de riesgo (en este orden)

    QueryTool qtS_VF = new QueryTool();
    Vector vSubS_VF = new Vector();
    Data dtSubS_VF = null;
    Data dtS_VF = new Data();

    qtS_VF.putName("SIVE_VALOR_FACRI");

    qtS_VF.putSubquery("NM_FACRI",
        "select NM_FACRI from SIVE_FACTOR_RIESGO where CD_ANO=? and NM_ALERBRO=?");
    dtSubS_VF = new Data();
    dtSubS_VF.put(new Integer(QueryTool.STRING), bif.getString("CD_ANO"));
    vSubS_VF.addElement(dtSubS_VF);
    dtSubS_VF = new Data();
    dtSubS_VF.put(new Integer(QueryTool.INTEGER), bif.getString("NM_ALERBRO"));
    vSubS_VF.addElement(dtSubS_VF);
    qtS_VF.putVectorSubquery("NM_MUESTRA", vSubS_VF);

    dtS_VF.put("5", qtS_VF); // 5 -> Baja
    lResul.addElement(dtS_VF);

    QueryTool qtS_FR = new QueryTool();
    Data dtS_FR = new Data();

    qtS_FR.putName("SIVE_FACTOR_RIESGO");

    qtS_FR.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_FR.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_FR.putOperator("CD_ANO", "=");
    qtS_FR.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_FR.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_FR.putOperator("NM_ALERBRO", "=");

    dtS_FR.put("5", qtS_FR); // 5 -> Baja
    lResul.addElement(dtS_FR);

    // Borrado de agentes toxicos y de muestras del brote (en este orden)

    QueryTool qtS_MTM = new QueryTool();
    Vector vSubS_MTM = new Vector();
    Data dtSubS_MTM = null;
    Data dtS_MTM = new Data();

    qtS_MTM.putName("SIVE_MICROTOX_MUESTRAS");

    qtS_MTM.putSubquery("NM_MUESTRA",
        "select NM_MUESTRA from SIVE_MUESTRAS_BROTE where CD_ANO=? and NM_ALERBRO=?");
    dtSubS_MTM = new Data();
    dtSubS_MTM.put(new Integer(QueryTool.STRING), bif.getString("CD_ANO"));
    vSubS_MTM.addElement(dtSubS_MTM);
    dtSubS_MTM = new Data();
    dtSubS_MTM.put(new Integer(QueryTool.INTEGER), bif.getString("NM_ALERBRO"));
    vSubS_MTM.addElement(dtSubS_MTM);
    qtS_MTM.putVectorSubquery("NM_MUESTRA", vSubS_MTM);

    dtS_MTM.put("5", qtS_MTM); // 5 -> Baja
    lResul.addElement(dtS_MTM);

    QueryTool qtS_MB = new QueryTool();
    Data dtS_MB = new Data();

    qtS_MB.putName("SIVE_MUESTRAS_BROTE");

    qtS_MB.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_MB.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_MB.putOperator("CD_ANO", "=");
    qtS_MB.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_MB.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_MB.putOperator("NM_ALERBRO", "=");

    dtS_MB.put("5", qtS_MB); // 5 -> Baja
    lResul.addElement(dtS_MB);

    // Borrado de sintomas

    QueryTool qtS_SB = new QueryTool();
    Data dtS_SB = new Data();

    qtS_SB.putName("SIVE_SINTOMAS_BROTE");

    qtS_SB.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_SB.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_SB.putOperator("CD_ANO", "=");
    qtS_SB.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_SB.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_SB.putOperator("NM_ALERBRO", "=");

    dtS_SB.put("5", qtS_SB); // 5 -> Baja
    lResul.addElement(dtS_SB);

    // Borrado de tasas de ataque de enfermedades vacunables

    QueryTool qtS_TE = new QueryTool();
    Data dtS_TE = new Data();

    qtS_TE.putName("SIVE_TATAQ_ENFVAC");

    qtS_TE.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_TE.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_TE.putOperator("CD_ANO", "=");
    qtS_TE.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_TE.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_TE.putOperator("NM_ALERBRO", "=");

    dtS_TE.put("5", qtS_TE); // 5 -> Baja
    lResul.addElement(dtS_TE);

    // Borrado de alimentos

    QueryTool qtS_AB = new QueryTool();
    Data dtS_AB = new Data();

    qtS_AB.putName("SIVE_ALI_BROTE");

    qtS_AB.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_AB.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_AB.putOperator("CD_ANO", "=");
    qtS_AB.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_AB.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_AB.putOperator("NM_ALERBRO", "=");

    dtS_AB.put("5", qtS_AB); // 5 -> Baja
    lResul.addElement(dtS_AB);

  } // Fin prepararBorradoTablasPerifericas()

  public void prepararCambioSituacionAlerta(Data bif, Lista lResul,
                                            String situacion, String sFecha) {
    QueryTool qtS_A = new QueryTool();
    Data dtS_A = new Data();

    qtS_A.putName("SIVE_ALERTA_BROTES");
    qtS_A.putType("CD_SITALERBRO", QueryTool.STRING);
    qtS_A.putValue("CD_SITALERBRO", situacion);
    qtS_A.putType("FC_ALERBRO", QueryTool.DATE);
    qtS_A.putValue("FC_ALERBRO", sFecha); // Modificacion de FC_ALERBRO
    qtS_A.putType("CD_OPE", QueryTool.STRING);
    qtS_A.putValue("CD_OPE", applet.getParametro("LOGIN"));
    qtS_A.putType("FC_ULTACT", QueryTool.TIMESTAMP);
    qtS_A.putValue("FC_ULTACT", "");

    qtS_A.putWhereType("CD_ANO", QueryTool.STRING);
    qtS_A.putWhereValue("CD_ANO", bif.getString("CD_ANO"));
    qtS_A.putOperator("CD_ANO", "=");
    qtS_A.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtS_A.putWhereValue("NM_ALERBRO", bif.getString("NM_ALERBRO"));
    qtS_A.putOperator("NM_ALERBRO", "=");

    dtS_A.put("4", qtS_A); // 4 -> Modificacion
    lResul.addElement(dtS_A);
  } // Fin prepararCambioSituacionAlerta()

  /***************** Funciones Auxiliares *****************/

  // Llama a setVisibleToxAlim de PanDatosBas
  public void setVisibleToxAlim(boolean state) {
    if (pBas != null) {
      pBas.setVisibleToxAlim(state);
    }
  }

  // Llama a setVisibleVacunable de PanDatosBas
  public void setVisibleVacunable(boolean state) {
    if (pBas != null) {
      pBas.setVisibleVacunable(state);
    }
  }

  // Dev. el CTabPanel
  public CTabPanel getTabPanel() {
    return tabPanel;
  }

  // Crea el panel del protocolo de Brote
  public void crearProtocolo() {

    DataProtocolo data = new DataProtocolo();
    data.sCodigo = "B" + bifIni.getString("CD_GRUPO");

    Enumeration e = lGrupos.elements();
    Data nGrupo = null;
    data.sDescripcion = "";
    for (; e.hasMoreElements(); ) {
      nGrupo = (Data) e.nextElement();
      if ( ( (String) nGrupo.get("CD_GRUPO")).equals(bifIni.getString(
          "CD_GRUPO"))) {
        data.sDescripcion = (String) nGrupo.get("DS_GRUPO");
      }
    }
    data.sNivel1 = bifIni.getString("CD_NIVEL_1");
    data.sNivel2 = "";
    data.NumCaso = bifIni.getString("CD_ANO") + bifIni.getString("NM_ALERBRO");
    data.sCa = applet.getParametro("CA");

    pProtocolo = new PanelBrote(applet, modoEntrada, data, this);
  } // crearProtocolo()

  public void addProtocolo() {
    if (!pProtocolo.pnl.getNoSeEncontraronDatos()) {
      MensajeNoProtocolo = false;
      tabPanel.InsertarPanel("Protocolo", pProtocolo);
      pProtocolo.doLayout();
    }
    else {
      if (!MensajeNoProtocolo) {
        applet.showAdvise("El Grupo de Brote no tiene un protocolo definido.");

      }
      MensajeNoProtocolo = true;
      pProtocolo = null;
    }
  } // addProtocolo()

  public void insertarProtocolo() {

    if (pProtocolo != null) {
      if (pProtocolo.pnl.ValidarObligatorios()) {
        pProtocolo.pnl.Insertar();
      }
    }
  } // Fin insertarProtocolo()

  private boolean fechaCorrecta(Data dt) {
    int compara = 0;
    int compara_caso = 0;
    int compara_hora = 0;
    int compara_hora_caso = 0;
    boolean estado = true;
    String h1 = "";
    String h2 = "";
    String hpc = "";
    String huc = "";

    try {
      String f1 = dt.getString("FC_FECHAHORA").trim();
      String f2 = dt.getString("FC_EXPOSICION").trim();
      if (f1.length() > 10) {
        h1 = f1.substring(10).trim();
      }
      if (f2.length() > 10) {
        h2 = f2.substring(10).trim();
      }
      if (!f1.equals("")) {
        f1 = f1.substring(0, 10);
      }
      if (!f2.equals("")) {
        f2 = f2.substring(0, 10);

      }
      String fpc = dt.getString("FC_ISINPRIMC").trim();
      String fuc = dt.getString("FC_FSINPRIMC").trim();

      if (fpc.length() > 10) {
        hpc = fpc.substring(10).trim();
      }
      if (fuc.length() > 10) {
        huc = fuc.substring(10).trim();
      }
      if (!fpc.equals("")) {
        fpc = fpc.substring(0, 10);
      }
      if (!fuc.equals("")) {
        fuc = fuc.substring(0, 10);

      }
      if ( (f1.length() > 0) && (f2.length() > 0)) {
        compara = Format.comparaFechas(f1, f2);
      }

      if ( (h1.length() > 0) && (h2.length() > 0)) {
        compara_hora = comparaHora(h1, h2);
      }

      if ( (fpc.length() > 0) && (fuc.length() > 0)) {
        compara_caso = Format.comparaFechas(fpc, fuc);
      }

      if ( (hpc.length() > 0) && (huc.length() > 0)) {
        compara_hora_caso = comparaHora(hpc, huc);
      }

      if (compara == 2) {
        this.getApp().showError(
            "La fecha de exposici�n ha de ser anterior a la fecha de notificaci�n.");
        estado = false;
      }
      if (compara_caso == 1) {
        this.getApp().showError("La fecha de inicio de los s�ntomas del �ltimo caso ha de ser posterior a la del primero.");
        estado = false;
      }
      if ( (compara == 0) && (compara_hora == -1)) {
        this.getApp().showError(
            "La hora de exposici�n ha de ser anterior a la hora de notificaci�n.");
        estado = false;
      }
      if ( (compara_caso == 0) && (compara_hora_caso == 1)) {
        this.getApp().showError("La hora de inicio de los s�ntomas del �ltimo caso ha de ser posterior a la del primero.");
        estado = false;
      }
    }
    catch (Exception e) {
      this.getApp().showError("Formato de fecha/hora incorrecto.");
      estado = false;
    }

    return estado;
  }

  private int comparaHora(String h1, String h2) throws Exception {
    //Devuelve 0 si son iguales, 1 si h1<h2 y -1 si h2<h1.
    int estado = 0;

    h1 = h1.trim();
    h2 = h2.trim();

    String hora1 = h1.substring(0, 2);
    String minuto1 = h1.substring(3, 5);
//    String segundo1 = h1.substring (6,8);
    String hora2 = h2.substring(0, 2);
    String minuto2 = h2.substring(3, 5);
//    String segundo2 = h2.substring (6,8);

    int ihora1 = Integer.parseInt(hora1);
    int iminuto1 = Integer.parseInt(minuto1);
//    int isegundo1 = Integer.parseInt (segundo1);
    int ihora2 = Integer.parseInt(hora2);
    int iminuto2 = Integer.parseInt(minuto2);
//    int isegundo2 = Integer.parseInt (segundo2);

    // horas
    if (ihora1 > ihora2) {
      estado = 1;
    }
    if (ihora1 < ihora2) {
      estado = -1;
    }
    if (ihora1 == ihora2) {
      // minutos
      if (iminuto1 > iminuto2) {
        estado = 1;
      }
      if (iminuto1 < iminuto2) {
        estado = -1;
      }
      if (iminuto1 == iminuto2) {
        estado = 0;
        // segundos
//      if (isegundo1 > isegundo2) estado = -1;
//      if (isegundo1 < isegundo2) estado = 1;
//      if (isegundo1 == isegundo2) estado = 0;
//     }
      }
    }
    return estado;
  }

} // endclass DiaBIF

/******************* ESCUCHADORES **********************/

// Botones
class DiaBIFActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaBIF adaptee;
  ActionEvent evt;

  DiaBIFActionAdapter(DiaBIF adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if (evt.getActionCommand().equals("Grabar")) {
        adaptee.btnGrabarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Salir")) {
        adaptee.btnSalirActionPerformed();
      }
      else if (evt.getActionCommand().equals("Investigadores")) {
        adaptee.btnInvActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  DiaBIFActionAdapter
