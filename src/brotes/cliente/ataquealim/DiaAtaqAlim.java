/**
 * Clase: DiaMed
 * Paquete: brotes.cliente.ataquealim
 * Hereda: CDialog
 * Autor: �ngel Rodr�guez S�nchez (ARS)
 * Fecha Inicio: 14/02/2000
 * Analisis Funcional: Punto 1.0 Tasa de ataque espec�fica por alimentos.
 * Descripcion: Implementacion de los dialogos que permite dar de alta
 *   baja, modificar y consultar alimentos que han provocado brotes de alergia
 */

package brotes.cliente.ataquealim;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import jclass.bwt.BWTEnum;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class DiaAtaqAlim
    extends CDialog
    implements CInicializar, CFiltro {

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  // Mensaje en caso de bloqueo
  private final String DATOS_BLOQUEADOS =
      "Los datos han sido modificados. �Desea sobrescribirlos?";

  private int modoOperacion;

  // Variables para calcular la tasa de ataque global
  private double no_enfermos = 0.0;
  private double no_expuestos = 0.0;

  private Data dtEnt = new Data();

  private Lista lstBorrar = new Lista();

  /****************** Componentes del panel ********************/

  // Organizacion del panel
  private XYLayout xYLayout = new XYLayout();

  // CListaMantenimiento (mi favorita)
  CListaMantenimiento clmMantenimiento = null;

  // Botones de aceptar y cancelar
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  DiaAtaqAlimActionAdapter actionAdapter = null;
  TextField txtTasaAtTotal = new TextField();
  Label label1 = new Label();
  Label label2 = new Label();
  Label lblBroteDesc = new Label();
  Label lblDatosBrote = new Label();

  // estructuras para comprobar el bloqueo
  private QueryTool qtBloqueo = null;
  private Data dtBloqueo = null;

  // Constructor
  // CApp sera luego el panel del que procede.

  public DiaAtaqAlim(CApp a, int modo, Data entrada) {

    super(a);

    dtEnt = entrada;

    this.setTitle("Brotes: Ataques por alimentos");
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    try {

      // Inicializacion
      modoOperacion = modo;

      // CLista mantenimiento que necesitamos poner aqu�.
      // botones
      vBotones.addElement(new CBoton("",
                                     "images/alta.gif",
                                     "Nueva petici�n",
                                     false,
                                     false));

      vBotones.addElement(new CBoton("",
                                     "images/modificacion.gif",
                                     "Modificar petici�n",
                                     true,
                                     true));

      vBotones.addElement(new CBoton("",
                                     "images/baja.gif",
                                     "Borrar petici�n",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("Alimentos",
                                      "90",
                                      "DS_ALI"));

      vLabels.addElement(new CColumna("E.Enf.",
                                      "55",
                                      "NM_EXPENF"));

      vLabels.addElement(new CColumna("E.N.Enf.",
                                      "55",
                                      "NM_EXPNOENF"));

      vLabels.addElement(new CColumna("T.Exp.",
                                      "55",
                                      "TOTAL_EXP"));

      vLabels.addElement(new CColumna("T.Atq.(%)",
                                      "58",
                                      "TOTAL_AT_EXP"));

      vLabels.addElement(new CColumna("N.Exp.E",
                                      "55",
                                      "NM_NOEXPENF"));

      vLabels.addElement(new CColumna("N.Exp.N",
                                      "55",
                                      "NM_NOEXPNOENF"));

      vLabels.addElement(new CColumna("T.N.Exp.",
                                      "55",
                                      "TOTAL_NO_EXP"));

      vLabels.addElement(new CColumna("T.Atq.(%)",
                                      "58",
                                      "TOTAL_AT_NO_EXP"));

      vLabels.addElement(new CColumna("D.T.(%)",
                                      "48",
                                      "DIF_TASAS"));

      vLabels.addElement(new CColumna("OR (IC)",
                                      "100",
                                      "OR"));

      int[] ajustar = new int[11];
      ajustar[0] = BWTEnum.TOPLEFT;
      ajustar[1] = BWTEnum.TOPRIGHT;
      ajustar[2] = BWTEnum.TOPRIGHT;
      ajustar[3] = BWTEnum.TOPRIGHT;
      ajustar[4] = BWTEnum.TOPRIGHT;
      ajustar[5] = BWTEnum.TOPRIGHT;
      ajustar[6] = BWTEnum.TOPRIGHT;
      ajustar[7] = BWTEnum.TOPRIGHT;
      ajustar[8] = BWTEnum.TOPRIGHT;
      ajustar[9] = BWTEnum.TOPRIGHT;
      ajustar[10] = BWTEnum.TOPRIGHT;

      clmMantenimiento = new CListaMantenimiento(a,
                                                 vLabels,
                                                 vBotones,
                                                 ajustar,
                                                 this,
                                                 this,
                                                 290,
                                                 726);

      jbInit();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    // Organizacion del panel
    this.setSize(new Dimension(730, 450));
    xYLayout.setHeight(427);
    xYLayout.setWidth(730);
    this.setLayout(xYLayout);

    // Im�genes

    final String imgACEPTAR = "images/aceptar.gif";
    final String imgCANCELAR = "images/cancelar.gif";
    this.getApp().getLibImagenes().put(imgACEPTAR);
    this.getApp().getLibImagenes().put(imgCANCELAR);
    this.getApp().getLibImagenes().CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaAtaqAlimActionAdapter(this);

    // Boton de Aceptar
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.addActionListener(actionAdapter);
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));

    // Boton de Cancelar
    btnCancelar.setLabel("Cancelar");
    txtTasaAtTotal.setEditable(false);
    label1.setText("Tasa de ataque total: ");
    label2.setText("%");
    lblBroteDesc.setText("Brote:");
    lblDatosBrote.setText(dtEnt.getString("CD_ANO") + "/" +
                          dtEnt.getString("NM_ALERBRO") + " - " +
                          dtEnt.getString("DS_BROTE"));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));

    this.add(lblBroteDesc, new XYConstraints(14, 23, 42, -1));
    this.add(lblDatosBrote, new XYConstraints(56, 23, 263, -1));
    this.add(clmMantenimiento, new XYConstraints(2, 56, 726, 290));
    this.add(txtTasaAtTotal, new XYConstraints(147, 354, 52, 19));
    this.add(btnAceptar, new XYConstraints(518, 378, 88, 29));
    this.add(btnCancelar, new XYConstraints(629, 378, 88, 29));
    this.add(label1, new XYConstraints(18, 356, 126, 15));
    this.add(label2, new XYConstraints(209, 356, 20, 15));

    verDatos();
    Inicializar(CInicializar.NORMAL);

  } // Fin jbInit()

  private void verDatos() {
    clmMantenimiento.setPrimeraPagina(this.primeraPagina());
    txtTasaAtTotal.setText(cogeDecimal(100 * no_enfermos / no_expuestos, 1, '.'));
  }

  public void Inicializar() {}

  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if ( (modoOperacion == BAJA) || (modoOperacion == CONSULTA)) {
          btnAceptar.setVisible(false);
          btnCancelar.setLabel("Salir");
          btnCancelar.setEnabled(true);
          clmMantenimiento.setEnabled(false);
        }
        else {
          btnAceptar.setEnabled(true);
          btnCancelar.setEnabled(true);
          clmMantenimiento.setEnabled(true);

        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

        /*
               case ALTA:
               case MODIFICACION:
               break;
               case CONSULTA:
               case BAJA:
          btnAceptar.setVisible(false);
          btnCancelar.setLabel("Salir");
          btnCancelar.setEnabled(true);
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
               break;
      */
    }
    this.doLayout();
  } // Fin Inicializar()

  // operaciones de la botonera
  public void realizaOperacion(int j) {

    Lista lisPet = this.clmMantenimiento.getLista();
    DiaAtaqDatos dm = null;
    Data dPetMod = new Data();
    Data dtResultado = new Data();
    Lista ltmpsal = new Lista();

    switch (j) {
      // bot�n de alta
      case ALTA:
        dm = new DiaAtaqDatos(this.getApp(), 0, null, lisPet);
        dm.show();
        if (dm.bAceptar()) {
          dtResultado = dm.dtResultado();
          lisPet.addElement(dtResultado);
          no_enfermos = 0;
          no_expuestos = 0;
          ltmpsal = hazCalculos(lisPet);
          clmMantenimiento.setPrimeraPagina(ltmpsal);
          // Actualizamos la tasa de ataques total.
          txtTasaAtTotal.setText(cogeDecimal(100 * no_enfermos / no_expuestos,
                                             1, '.'));
        }
        break;
      case MODIFICACION:
        dPetMod = clmMantenimiento.getSelected();
        if (dPetMod != null) {
          int ind = clmMantenimiento.getSelectedIndex();
          dm = new DiaAtaqDatos(this.getApp(), 1, dPetMod, lisPet);
          dm.show();
          if (dm.bAceptar()) {
            lisPet.removeElementAt(ind);
            dtResultado = dm.dtResultado();
            // Creamos esta lista, para actualizar los c�lculos
//             Lista ltmp = new Lista ();
//             ltmp.addElement(dtResultado);
//             Lista ltmpsal = hazCalculos(ltmp);
            // ahora, esta lista temporal, de un data, la a�adimos a la
            // nuestra, CListaMantenimiento
            lisPet.insertElementAt( (Data) dtResultado, ind);
            no_enfermos = 0;
            no_expuestos = 0;
            ltmpsal = hazCalculos(lisPet);
            clmMantenimiento.setPrimeraPagina(ltmpsal);
            txtTasaAtTotal.setText(cogeDecimal(100 * no_enfermos / no_expuestos,
                                               1, '.'));
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un proveedor en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dPetMod = clmMantenimiento.getSelected();
        if (dPetMod != null) {
          int ind = clmMantenimiento.getSelectedIndex();
          dm = new DiaAtaqDatos(this.getApp(), 2, dPetMod);
          dm.show();
          if (dm.bAceptar()) {
            lstBorrar.addElement(clmMantenimiento.getSelected());
            lisPet.removeElementAt(ind);
            no_enfermos = 0;
            no_expuestos = 0;
            ltmpsal = hazCalculos(lisPet);
            clmMantenimiento.setPrimeraPagina(ltmpsal);
            txtTasaAtTotal.setText(cogeDecimal(100 * no_enfermos / no_expuestos,
                                               1, '.'));
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un almacen en la tabla.");
        }
        break;
    }
  }

  // prepara la informaci�n para el control de bloqueo
  private void preparaBloqueo() {
    // query tool
    qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", dtEnt.getString("CD_ANO"));
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", dtEnt.getString("NM_ALERBRO"));
    qtBloqueo.putOperator("NM_ALERBRO", "=");

    // informaci�n de bloqueo
    dtBloqueo = new Data();
    dtBloqueo.put("CD_OPE", dtEnt.getString("CD_OPE"));
    dtBloqueo.put("FC_ULTACT", dtEnt.getString("FC_ULTACT"));
  }

  QueryTool realizarAlta(Data dtAlta) {

    QueryTool qtAlt = new QueryTool();

    // tabla de familias de tasas vacunables
    qtAlt.putName("SIVE_ALI_BROTE");

    // campos que se escriben
    qtAlt.putType("NM_ALIBROTE", QueryTool.INTEGER);
    qtAlt.putType("DS_ALI", QueryTool.STRING);
    qtAlt.putType("IT_CALCTASA", QueryTool.STRING);
    qtAlt.putType("CD_ANO", QueryTool.STRING);
    qtAlt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtAlt.putType("CD_TALIMENTO", QueryTool.STRING);
    qtAlt.putType("NM_EXPENF", QueryTool.INTEGER);
    qtAlt.putType("NM_EXPNOENF", QueryTool.INTEGER);
    qtAlt.putType("NM_NOEXPENF", QueryTool.INTEGER);
    qtAlt.putType("NM_NOEXPNOENF", QueryTool.INTEGER);

    // Valores de los campos
    qtAlt.putValue("NM_ALIBROTE", "");
    qtAlt.putValue("DS_ALI", dtAlta.getString("DS_ALI"));
    qtAlt.putValue("IT_CALCTASA", "M");
    qtAlt.putValue("CD_ANO", dtEnt.getString("CD_ANO"));
    qtAlt.putValue("NM_ALERBRO", dtEnt.getString("NM_ALERBRO"));
    qtAlt.putValue("CD_TALIMENTO", dtAlta.getString("CD_TALIMENTO"));
    qtAlt.putValue("NM_EXPENF", dtAlta.getString("NM_EXPENF"));
    qtAlt.putValue("NM_EXPNOENF", dtAlta.getString("NM_EXPNOENF"));
    qtAlt.putValue("NM_NOEXPENF", dtAlta.getString("NM_NOEXPENF"));
    qtAlt.putValue("NM_NOEXPNOENF", dtAlta.getString("NM_NOEXPNOENF"));

    return qtAlt;
  }

  QueryTool realizarUpdate(Data dtUpd) {

    QueryTool qtUpd = new QueryTool();

    // tabla de familias de tasas vacunables
    qtUpd.putName("SIVE_ALI_BROTE");

    // campos que se escriben
    qtUpd.putType("CD_TALIMENTO", QueryTool.STRING);
    qtUpd.putType("NM_EXPENF", QueryTool.INTEGER);
    qtUpd.putType("NM_EXPNOENF", QueryTool.INTEGER);
    qtUpd.putType("NM_NOEXPENF", QueryTool.INTEGER);
    qtUpd.putType("NM_NOEXPNOENF", QueryTool.INTEGER);

    // Valores de los campos
    qtUpd.putValue("CD_TALIMENTO", dtUpd.getString("CD_TALIMENTO"));
    qtUpd.putValue("NM_EXPENF", dtUpd.getString("NM_EXPENF"));
    qtUpd.putValue("NM_EXPNOENF", dtUpd.getString("NM_EXPNOENF"));
    qtUpd.putValue("NM_NOEXPENF", dtUpd.getString("NM_NOEXPENF"));
    qtUpd.putValue("NM_NOEXPNOENF", dtUpd.getString("NM_NOEXPNOENF"));

    //
    qtUpd.putWhereType("NM_ALIBROTE", QueryTool.INTEGER);
    qtUpd.putWhereValue("NM_ALIBROTE", dtUpd.getString("NM_ALIBROTE"));
    qtUpd.putOperator("NM_ALIBROTE", "=");

    return qtUpd;
  }

  QueryTool realizarBaja(Data dtBaj) {

    QueryTool qtBaj = new QueryTool();

    // tabla de familias de productos
    qtBaj.putName("SIVE_ALI_BROTE");
    //
    qtBaj.putWhereType("NM_ALIBROTE", QueryTool.INTEGER);
    qtBaj.putWhereValue("NM_ALIBROTE", dtBaj.getString("NM_ALIBROTE"));
    qtBaj.putOperator("NM_ALIBROTE", "=");

    return qtBaj;
  }

  /******************** Botoncicos de la lista  *************************/

  void btn_ActionPerformed(ActionEvent e) {
    Inicializar(CInicializar.ESPERA);
    Lista lstCLista = new Lista();
    Lista lstOperar = new Lista();
    Lista vResultado = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {

      try {

        this.getApp().getStub().setUrl("servlet/SrvPanAtaq");

        // Query para actualizar SIVE_BROTES (CD_OPE/FC_ULTACT)
        QueryTool qtBro = new QueryTool();
        Data dtBro = new Data();
        qtBro.putName("SIVE_BROTES");
        qtBro.putType("CD_OPE", QueryTool.STRING);
        qtBro.putType("FC_ULTACT", QueryTool.TIMESTAMP);
        qtBro.putValue("CD_OPE", dtEnt.getString("CD_OPE"));
        qtBro.putValue("FC_ULTACT", "");

        qtBro.putWhereType("CD_ANO", QueryTool.STRING);
        qtBro.putWhereValue("CD_ANO", dtEnt.getString("CD_ANO"));
        qtBro.putOperator("CD_ANO", "=");
        qtBro.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
        qtBro.putWhereValue("NM_ALERBRO", dtEnt.getString("NM_ALERBRO"));
        qtBro.putOperator("NM_ALERBRO", "=");

        dtBro.put("4", qtBro);
        lstOperar.addElement(dtBro);

        /************** ALTAS ***************/
        lstCLista = this.clmMantenimiento.getLista();

        Data dtTemp = null;

        for (int i = 0; i < lstCLista.size(); i++) {
          dtTemp = (Data) lstCLista.elementAt(i);
          if (dtTemp.getString("TIPO_OPERACION").equals("A")) {
            QueryTool qtAlt = new QueryTool();
            qtAlt = realizarAlta(dtTemp);

            Data dtAlt = new Data();
            dtAlt.put("3", qtAlt);

            lstOperar.addElement(dtAlt);

            dtTemp.remove("TIPO_OPERACION");
          }
        }

        /************** MODIFICAR ***************/
        for (int i = 0; i < lstCLista.size(); i++) {
          dtTemp = (Data) lstCLista.elementAt(i);
          if (dtTemp.getString("TIPO_OPERACION").equals("M")) {
            QueryTool qtUpd = new QueryTool();
            qtUpd = realizarUpdate(dtTemp);

            Data dtUpdate = new Data();
            dtUpdate.put("4", qtUpd);

            lstOperar.addElement(dtUpdate);

            dtTemp.remove("TIPO_OPERACION");
          }
        }

        /************** BAJAS ***************/
        if (lstBorrar.size() > 0) {
          for (int i = 0; i < lstBorrar.size(); i++) {
            dtTemp = (Data) lstBorrar.elementAt(i);
            QueryTool qtBaja = new QueryTool();
            qtBaja = realizarBaja(dtTemp);

            Data dtBaja = new Data();
            dtBaja.put("5", qtBaja);

            lstOperar.addElement(dtBaja);
          }
        }

        //realizamos la actualizaci�n
        preparaBloqueo();
        if (lstOperar.size() > 0) {
          vResultado = (Lista)this.getApp().getStub().doPost(10000,
              lstOperar,
              qtBloqueo,
              dtBloqueo,
              getApp());

          //obtener el nuevo cd_ope y fc_ultact de la base de datos.
        }
        int tamano = vResultado.size();
        String fecha = "";

        fecha = ( (Lista) vResultado.firstElement()).getFC_ULTACT();

        dtEnt.put("CD_OPE", dtEnt.getString("CD_OPE"));
        dtEnt.put("FC_ULTACT", fecha);

        dispose();

      }
      catch (Exception exc) {
        dispose();
      } // del "trai cach"
      dispose();
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      dispose();
    }
    Inicializar(CInicializar.NORMAL);
  }

  public Lista primeraPagina() {
    QueryTool qt = new QueryTool();
    Lista Filtro = new Lista();
    Lista Resultado = new Lista();
    qt.putName("SIVE_ALI_BROTE");
    qt.putType("NM_ALIBROTE", QueryTool.INTEGER);
    qt.putType("DS_ALI", QueryTool.STRING);
    qt.putType("CD_TALIMENTO", QueryTool.STRING);
    qt.putType("NM_EXPENF", QueryTool.INTEGER);
    qt.putType("NM_EXPNOENF", QueryTool.INTEGER);
    qt.putType("NM_NOEXPENF", QueryTool.INTEGER);
    qt.putType("NM_NOEXPNOENF", QueryTool.INTEGER);
    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", dtEnt.getString("CD_ANO"));
    qt.putOperator("CD_ANO", "=");
    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", dtEnt.getString("NM_ALERBRO"));
    qt.putOperator("NM_ALERBRO", "=");

    Filtro.addElement(qt);
    try {
      this.getApp().getStub().setUrl("servlet/SrvQueryTool");
      Resultado = (Lista)this.getApp().getStub().doPost(1, Filtro);
    }
    catch (Exception e) {
      this.getApp().trazaLog(e);
    }

    return hazCalculos(Resultado);
  }

  public Lista siguientePagina() {
    Lista l = new Lista();
    return l;
  }

  private String cogeDecimal(double dent, int num_dec, char decimal) {
    String salida = "";
    int pos_coma = 0;
    if (!Double.isNaN(dent)) {
      String cadena = String.valueOf(dent);
      for (int a = 0; a < cadena.length(); a++) {
        if (cadena.charAt(a) == decimal) {
          pos_coma = a;
        }
      }
      if ( (num_dec > 0) && (pos_coma + num_dec < cadena.length())) {
        salida = cadena.substring(0, pos_coma + num_dec + 1);
      }
      else if (num_dec == 0) {
        salida = cadena.substring(0, pos_coma);
      }
      else {
        salida = cadena;
      }
    }
    else {
      salida = "-";
    }
    return salida;
  }

  private Lista hazCalculos(Lista entrada) {

    Lista salida = entrada;

    Data temp = null;

    double nm_expenf = 0.0;
    double nm_expnoenf = 0.0;
    double nm_noexpenf = 0.0;
    double nm_noexpnoenf = 0.0;

    double tasas_exp = 0.0;
    double tasas_no_exp = 0.0;

    for (int i = 0; i < salida.size(); i++) {
      temp = (Data) salida.elementAt(i);

      if ( (!temp.getString("NM_EXPENF").equals("")) &&
          (!temp.getString("NM_NOEXPENF").equals("")) &&
          (!temp.getString("NM_EXPNOENF").equals("")) &&
          (!temp.getString("NM_NOEXPNOENF").equals(""))) {

        nm_expenf = Double.valueOf(temp.getString("NM_EXPENF")).doubleValue();
        nm_expnoenf = Double.valueOf(temp.getString("NM_EXPNOENF")).doubleValue();
        nm_noexpenf = Double.valueOf(temp.getString("NM_NOEXPENF")).doubleValue();
        nm_noexpnoenf = Double.valueOf(temp.getString("NM_NOEXPNOENF")).
            doubleValue();

        tasas_exp = (100 * nm_expenf) / (nm_expenf + nm_expnoenf);
        tasas_no_exp = (100 * nm_noexpenf) / (nm_noexpenf + nm_noexpnoenf);

        // incrementamos las variables para calcular la tasa de
        // ataque global.           ( �OJO! , aqu� "no" es "n�")

        no_enfermos = no_enfermos + nm_expenf + nm_noexpenf;
        no_expuestos = no_expuestos + nm_expenf + nm_expnoenf;

        temp.put("TOTAL_EXP", cogeDecimal(nm_expenf + nm_expnoenf, 0, '.'));
        temp.put("TOTAL_AT_EXP", cogeDecimal(tasas_exp, 1, '.'));
        temp.put("TOTAL_NO_EXP",
                 cogeDecimal(nm_noexpenf + nm_noexpnoenf, 0, '.'));
        temp.put("TOTAL_AT_NO_EXP", cogeDecimal(tasas_no_exp, 1, '.'));
        temp.put("DIF_TASAS", cogeDecimal(tasas_exp - tasas_no_exp, 1, '.'));
        temp.put("OR",
                 intervaloConfianza(nm_expenf, nm_noexpenf, nm_expnoenf, nm_noexpnoenf,
                                    (nm_expenf + nm_noexpenf),
                                    (nm_expnoenf + nm_noexpnoenf),
                                    (nm_expenf + nm_expnoenf),
                                    (nm_noexpenf + nm_noexpnoenf),
                                    (nm_expenf + nm_noexpenf + nm_expnoenf +
                                     nm_noexpnoenf),
                                    2));
      } // del if
    } // del bucle
    return salida;
  }

  private String intervaloConfianza(double a, double b, double c, double d,
                                    double M1, double M0, double N1, double N0,
                                    double T, int decimales) {
    String salida = "";
    double rv = 0.0;
    double Z = 1.96;
    double varianza = 0.0;
    double E = 0.0;
    double X = 0.0;
    double LCI = 0.0;
    double LCS = 0.0;
    int indice = 0;
    int indice_E = 0;

    rv = (a * d) / (b * c);
    varianza = (M1 * N1 * M0 * N0) / (T * T * (T - 1));
    E = M1 * N1 / T;
    // ARS (19-04-01)
    X = java.lang.Math.sqrt( ( (a - E) * (a - E)) / varianza);

    LCI = java.lang.Math.pow(rv, (1 - Z / X));
    LCS = java.lang.Math.pow(rv, (1 + Z / X));

    indice = Double.toString(LCI).indexOf(".");
    indice_E = Double.toString(LCI).indexOf("E");
    if (LCI != 0.0) {
      salida = Double.toString(LCI).substring(0, 1 + indice + decimales);
    }
    else {
      salida = Double.toString(LCI);
    }
    if (indice_E != -1) {
      salida += Double.toString(LCI).substring(indice_E);

    }
    indice = Double.toString(LCS).indexOf(".");
    indice_E = Double.toString(LCS).indexOf("E");
    if (LCS != 0.0) {
      salida += "-" + Double.toString(LCS).substring(0, 1 + indice + decimales);
    }
    else {
      salida += "-" + Double.toString(LCS);

    }
    if (indice_E != -1) {
      salida += Double.toString(LCS).substring(indice_E);
      // Cambio nuevo: 19-04-01. Para ponerlo en otro formato.
      // Otro cambio m�s:14-05-01. Para ponerle 2 decimales.
    }
    salida = cogeDecimal(rv, decimales, '.') + "(" + salida + ")";
    // Cambio nuevo: 19-04-01. Si cualquiera de los dos l�mites sale indeterminado
    // se pone indeterminado y santas pascuas.

    if ( (salida.indexOf("N") != -1) || (salida.indexOf("I") != -1)) {
      salida = "Indeterminado";
      /*    // Quitamos Na e Inf, �pero dejamos 0.0!.
          int indiceNa = salida.indexOf("N");
          int indice0_0 = salida.indexOf("0.0");
          if (indiceNa==-1)
            indiceNa = salida.indexOf("I");
          if (indiceNa!=-1) {
            if (indice0_0!=-1) {
              if (indice0_0>indiceNa)
                salida = "Indef." + salida.substring(indiceNa+1);
              else if (indice0_0<indiceNa)
                salida = salida.substring(0);
            } else {
                salida = "Indef." + salida.substring(indiceNa+1);
            }
          }
          //***************
           indiceNa = salida.indexOf("N");
           if (indiceNa==-1)
             indiceNa = salida.lastIndexOf("I");
           if (indiceNa!=-1) {
                 salida = salida.substring(0, indiceNa)+"Indef.";
           }*/

    }
    return salida;
  }

}

/******************* ESCUCHADORES **********************/

// Botones
class DiaAtaqAlimActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaAtaqAlim adaptee;
  ActionEvent evt;

  DiaAtaqAlimActionAdapter(DiaAtaqAlim adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btn_ActionPerformed(evt);
  }
}
