package brotes.cliente.ataquealim;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CPnlCodigo;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class DiaAtaqDatos
    extends CDialog
    implements CInicializar {

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  /****************** Componentes del panel ********************/

  // Organizacion del panel
  private XYLayout xYLayout = new XYLayout();

  // Botones de aceptar y cancelar

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  private int modoOperacion = 0;
  private boolean bAceptar = false;

  DiaAtaqDatosActionAdapter actionAdapter = null;
  TextField txtAlimento = new TextField();
  Label lblAlimento = new Label();
  ButtonControl btnLupa = new ButtonControl();
  Label lblExpEnf = new Label();
  Label lblExpNoEnf = new Label();
  Label lblNoExpEnf = new Label();
  Label lblNoExpNoEnf = new Label();
  CEntero txtExpEnf = new CEntero(6);
  CEntero txtNoExpEnf = new CEntero(6);
  CEntero txtExpNoEnf = new CEntero(6);
  CEntero txtNoExpNoEnf = new CEntero(6);

  // Parte encargada del CPanel C�digo
  CPnlCodigo pnlTipoAlim = null;
  QueryTool qtTemp = new QueryTool();
  // Final del CPanel C�digo...

  Data dtEnt = new Data();
  Lista lEntra = new Lista();
  Data dtSal = new Data();

  // String de cach�.
  String txtAlimCache = "";

  // Constructor
  // CApp sera luego el panel del que procede.

  public DiaAtaqDatos(CApp a, int modo, Data entrada) {

    super(a);
    try {

      // Inicializacion
      modoOperacion = modo;
      dtEnt = entrada;

      qtTemp.putName("SIVE_TALIMENTO");
      qtTemp.putType("CD_TALIMENTO", QueryTool.STRING);
      qtTemp.putType("DS_TALIMENTO", QueryTool.STRING);

      pnlTipoAlim = new CPnlCodigo(a, this, qtTemp, "CD_TALIMENTO",
                                   "DS_TALIMENTO", true, "Tipos de alimentos",
                                   "Tipos de alimentos");

      this.setTitle("Brotes: Ataques por alimentos");

      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor1

  public DiaAtaqDatos(CApp a, int modo, Data entrada, Lista lEntrada) {

    super(a);
    try {

      // Inicializacion
      modoOperacion = modo;
      dtEnt = entrada;
      lEntra = lEntrada;

      qtTemp.putName("SIVE_TALIMENTO");
      qtTemp.putType("CD_TALIMENTO", QueryTool.STRING);
      qtTemp.putType("DS_TALIMENTO", QueryTool.STRING);

      pnlTipoAlim = new CPnlCodigo(a, this, qtTemp, "CD_TALIMENTO",
                                   "DS_TALIMENTO", true, "Tipos de alimentos",
                                   "Tipos de alimentos");

      this.setTitle("Brotes: Ataques por alimentos");

      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor2

  void jbInit() throws Exception {

    // Organizacion del panel
    this.setSize(new Dimension(580, 200));
    xYLayout.setHeight(183);
    xYLayout.setWidth(580);
    this.setLayout(xYLayout);

    // Im�genes

    final String imgACEPTAR = "images/aceptar.gif";
    final String imgCANCELAR = "images/cancelar.gif";
    final String imgLUPA = "images/magnify.gif";
    this.getApp().getLibImagenes().put(imgACEPTAR);
    this.getApp().getLibImagenes().put(imgCANCELAR);
    this.getApp().getLibImagenes().put(imgLUPA);
    this.getApp().getLibImagenes().CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaAtaqDatosActionAdapter(this);

    // Boton de Aceptar
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.addActionListener(actionAdapter);
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));

    lblAlimento.setText("Alimento:");
    btnLupa.addActionListener(new DiaAtaqDatos_btnLupa_actionAdapter(this));
    btnLupa.setImage(this.getApp().getLibImagenes().get(imgLUPA));

    lblExpEnf.setText("Expuestos enfermos:");
    lblExpNoEnf.setText("Expuestos no enfermos:");
    lblNoExpEnf.setText("No expuestos enfermos:");
    lblNoExpNoEnf.setText("No expuestos no enfermos:");
    txtExpEnf.setBackground(new Color(250, 250, 150));
    txtNoExpEnf.setBackground(new Color(250, 250, 150));
    txtExpNoEnf.setBackground(new Color(250, 250, 150));
    txtNoExpNoEnf.setBackground(new Color(250, 250, 150));

    btnCancelar.setLabel("Cancelar");
    txtAlimento.setBackground(new Color(250, 250, 150));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));

    txtAlimento.addFocusListener(new DiaAtaqDatos_txtAlimento_focusAdapter(this));

    this.add(lblAlimento, new XYConstraints(29, 19, 55, 25));
    this.add(txtAlimento, new XYConstraints(87, 21, 118, 20));
    this.add(btnLupa, new XYConstraints(212, 19, 25, 25));
    this.add(pnlTipoAlim, new XYConstraints(250, 15, 317, 32));
    this.add(lblExpEnf, new XYConstraints(29, 57, 126, 19));
    this.add(lblExpNoEnf, new XYConstraints(305, 57, -1, 19));
    this.add(lblNoExpEnf, new XYConstraints(29, 86, 141, 19));
    this.add(lblNoExpNoEnf, new XYConstraints(305, 86, 161, 19));
    this.add(txtExpEnf, new XYConstraints(184, 56, 57, 21));
    this.add(txtExpNoEnf, new XYConstraints(476, 56, 57, 21));
    this.add(txtNoExpEnf, new XYConstraints(184, 85, 57, 21));
    this.add(txtNoExpNoEnf, new XYConstraints(476, 85, 57, 21));
    this.add(btnAceptar, new XYConstraints(381, 141, 80, -1));
    this.add(btnCancelar, new XYConstraints(467, 141, 80, -1));

    verDatos();
  } // Fin jbInit()

  public void Inicializar(int i) {

    switch (i) {

      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // componente en el modo requerido
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {

          case ALTA:
            this.setEnabled(true);
            break;

          case MODIFICACION:
            this.setEnabled(true);
            txtAlimento.setEnabled(false);
            btnLupa.setEnabled(false);
            break;

            // s�lo habilitado salir y grabar
          case BAJA:
            this.setEnabled(false);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  } // Fin Inicializar()

  public boolean validarDatos() {
    boolean estado = false;
    if ( (txtAlimento.getText().length() > 0) &&
        (txtExpEnf.getText().length() > 0) &&
        (txtExpNoEnf.getText().length() > 0) &&
        (txtNoExpNoEnf.getText().length() > 0) &&
        (txtNoExpEnf.getText().length() > 0) &&
        (pnlTipoAlim.getDatos() != null)) {
      /*          if ((Integer.parseInt(txtExpEnf.getText().trim())+
                     Integer.parseInt(txtExpNoEnf.getText().trim()) !=0) &&
                    (Integer.parseInt(txtNoExpNoEnf.getText().trim())+
                     Integer.parseInt(txtNoExpEnf.getText().trim())!=0) &&
                    (Integer.parseInt(txtExpEnf.getText().trim())+
                     Integer.parseInt(txtNoExpEnf.getText().trim())!=0))*/
      estado = true;
      /*          else
           this.getApp().showAdvise("Los datos metidos dar�n divisi�n por cero");*/
    }
    else {
      this.getApp().showAdvise("Debe completar los campos obligatorios");
    }

    //si se han hecho las validaciones de datos y todos son correctos->
    //se valida q la descripci�n introducida del alimento no exista ya
    //en la lista de ese brote
    if (estado) {
      //solo se comprueba si es modif o alta
      if (modoOperacion == ALTA) {
        boolean encontrado = buscarDescripcion(txtAlimento.getText().trim());
        if (encontrado) {
          this.getApp().showError(
              "La descripci�n del alimento que se quiere introducir ya existe");
          txtAlimento.setText("");
          estado = false;
        }
        else {
          estado = true;
        }
      } //end if modoOp=BAJA
    } //end if estado

    return estado;
  } // Fin validarDatos()

  boolean buscarDescripcion(String desc) {
    //recorro la lista de alimentosq actualmente hay para ese brote->
    //no lo hago en la BD, pq puede q existan alimentos cuyos valores
    //de bd han sido modif pero todavia no se ha llevado a cabo la modif
    boolean salir = false;
    for (int i = 0; i < lEntra.size() && salir == false; i++) {
      //si existe 1 descripc q sea =
      if ( ( (Data) lEntra.elementAt(i)).getString("DS_ALI").equals(desc)) {
        salir = true;
      } //end if
    } //end for

    return salir;
  } //end  buscarDescripcion

  public boolean bAceptar() {
    return bAceptar;
  }

  public Data dtResultado() {
    return dtSal;
  }

  public void verDatos() {
    switch (modoOperacion) {
      case ALTA:
        this.setEnabled(true);
        break;

      case MODIFICACION:
        this.setEnabled(true);
        txtAlimento.setEnabled(false);
        btnLupa.setEnabled(false);
        break;

      case BAJA:
        this.setEnabled(false);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        break;
    }
    if (dtEnt != null) {
      txtAlimento.setText(dtEnt.getString("DS_ALI"));
      txtExpEnf.setText(dtEnt.getString("NM_EXPENF"));
      txtExpNoEnf.setText(dtEnt.getString("NM_EXPNOENF"));
      txtNoExpEnf.setText(dtEnt.getString("NM_NOEXPENF"));
      txtNoExpNoEnf.setText(dtEnt.getString("NM_NOEXPNOENF"));
      llenaDescAlim();
    }
  } // Fin rellenarDatos()

  /******************** Manejadores *************************/

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {
    dtSal = new Data();

    if (e.getActionCommand().equals("Aceptar")) {

      if (validarDatos()) {

        switch (modoOperacion) {

          // ----- NUEVO -----------

          case 0:
            Inicializar(CInicializar.ESPERA);
            dtSal.put("DS_ALI", txtAlimento.getText());
            dtSal.put("NM_EXPENF", txtExpEnf.getText());
            dtSal.put("NM_NOEXPNOENF", txtNoExpNoEnf.getText());
            dtSal.put("NM_EXPNOENF", txtExpNoEnf.getText());
            dtSal.put("NM_NOEXPENF", txtNoExpEnf.getText());
            dtSal.put("CD_TALIMENTO",
                      pnlTipoAlim.getDatos().getString("CD_TALIMENTO"));
            dtSal.put("TIPO_OPERACION", "A");
            bAceptar = true;
            dispose();
            Inicializar(CInicializar.NORMAL);
            break;
          case 1:

            // ---- MODIFICAR ----
            Inicializar(CInicializar.ESPERA);
            dtSal.put("DS_ALI", txtAlimento.getText());
            dtSal.put("NM_EXPENF", txtExpEnf.getText());
            dtSal.put("NM_EXPNOENF", txtExpNoEnf.getText());
            dtSal.put("NM_NOEXPNOENF", txtNoExpNoEnf.getText());
            dtSal.put("NM_NOEXPENF", txtNoExpEnf.getText());
            dtSal.put("CD_TALIMENTO",
                      pnlTipoAlim.getDatos().getString("CD_TALIMENTO"));

            String OpeAnt = dtEnt.getString("TIPO_OPERACION");
            if (OpeAnt.equals("A")) {
              dtSal.put("TIPO_OPERACION", "A");
            }
            else {
              dtSal.put("TIPO_OPERACION", "M");
            }

            dtSal.put("NM_ALIBROTE", dtEnt.getString("NM_ALIBROTE"));
            bAceptar = true;
            dispose();
            Inicializar(CInicializar.NORMAL);
            break;
          case 2:

            // ---- BORRAR -------
            dtSal.put("NM_ALIBROTE", dtEnt.getString("NM_ALIBROTE"));
            String OpeAntBaj = dtEnt.getString("TIPO_OPERACION");

            if (OpeAntBaj.equals("M")) {
              dtSal.put("TIPO_OPERACION", "M");
            }
            else {
              dtSal.put("TIPO_OPERACION", "B");
            }

            bAceptar = true;
            dispose();
            break;
        }
      }
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      dispose();
    }
  }

  void txtAlimento_focusLost() {

    // Si el texto del alimento ha cambiado, lanza la b�squeda
    // del c�digo del alimento.
    // Si no, no.

    if (!txtAlimento.getText().equals(txtAlimCache)) {

      // Metemos el valor de txtAlimento en cach�.
      txtAlimCache = txtAlimento.getText();

      // Comenzamos la consulta...
      QueryTool qt = new QueryTool();

      Lista vFiltro = new Lista();
      Lista vResultado = new Lista();

      qt.putName("SIVE_ALI_BROTE");
      qt.putType("DS_ALI", QueryTool.STRING);
      qt.putType("CD_TALIMENTO", QueryTool.STRING);
      qt.putWhereType("DS_ALI", QueryTool.STRING);
      qt.putWhereValue("DS_ALI", txtAlimento.getText());
      qt.putOperator("DS_ALI", "=");

      vFiltro.addElement(qt);

      try {
        this.getApp().getStub().setUrl("servlet/SrvQueryTool");
        vResultado = (Lista)this.getApp().getStub().doPost(1, vFiltro);
        if ( (vResultado != null) && (vResultado.size() > 0)) {

          String txtalimento = ( (Data) vResultado.elementAt(0)).getString(
              "DS_ALI");
          String cd_talimento = ( (Data) vResultado.elementAt(0)).getString(
              "CD_TALIMENTO");

          qt = new QueryTool();

          vFiltro = new Lista();
          vResultado = new Lista();

          qt.putName("SIVE_TALIMENTO");
          qt.putType("DS_TALIMENTO", QueryTool.STRING);
          qt.putType("CD_TALIMENTO", QueryTool.STRING);
          qt.putWhereType("CD_TALIMENTO", QueryTool.STRING);
          qt.putWhereValue("CD_TALIMENTO", cd_talimento);
          qt.putOperator("CD_TALIMENTO", "=");

          vFiltro.addElement(qt);

          this.getApp().getStub().setUrl("servlet/SrvQueryTool");
          vResultado = (Lista)this.getApp().getStub().doPost(1, vFiltro);
          if ( (vResultado != null) && (vResultado.size() > 0)) {
            txtAlimento.setText(txtalimento);
            pnlTipoAlim.setCodigo( (Data) vResultado.elementAt(0));
          }
        }
        else {
          pnlTipoAlim.limpiarDatos();
        } // del else
      }
      catch (Exception e) {
        this.getApp().trazaLog(e);
      }
    } // De la b�squeda del c�digo de alimento.
  }

  void llenaDescAlim() {

    QueryTool qt = new QueryTool();

    Lista vFiltro = new Lista();
    Lista vResultado = new Lista();

    qt.putName("SIVE_TALIMENTO");
    qt.putType("DS_TALIMENTO", QueryTool.STRING);
    qt.putType("CD_TALIMENTO", QueryTool.STRING);
    qt.putWhereType("CD_TALIMENTO", QueryTool.STRING);
    qt.putWhereValue("CD_TALIMENTO", dtEnt.getString("CD_TALIMENTO"));
    qt.putOperator("CD_TALIMENTO", "=");

    vFiltro.addElement(qt);
    try {
      this.getApp().getStub().setUrl("servlet/SrvQueryTool");
      vResultado = (Lista)this.getApp().getStub().doPost(1, vFiltro);
      if ( (vResultado != null) && (vResultado.size() > 0)) {
        pnlTipoAlim.setCodigo( (Data) vResultado.elementAt(0));
      }
    }
    catch (Exception e) {
      this.getApp().trazaLog(e);
    }
  }

  void btnLupa_actionPerformed() {

    // Cambios 20-04-01 (ARS)
    //CListaValores clv = null;
    DiaListaAlimentos diaAlim = null;
    Lista vAlim = new Lista();
    Lista vFiltro = new Lista();
    //Vector vCod = null;
    QueryTool qt = new QueryTool();

    Inicializar(CInicializar.ESPERA);

    // Cambios 20-04-01 (ARS)
    // campos que se muestran en la lista de valores
    //vCod = new Vector();
    //vCod.addElement("NM_ALIBROTE");
    //vCod.addElement("DS_ALI");

    qt.putName("SIVE_ALI_BROTE");
    qt.putType("DS_ALI", QueryTool.STRING);
    qt.setDistinct(true);

    vFiltro.addElement(qt);
    try {
      this.getApp().getStub().setUrl("servlet/SrvQueryTool");
      vAlim = (Lista)this.getApp().getStub().doPost(1, vFiltro);
      if ( (vAlim != null) && (vAlim.size() > 0)) {
        diaAlim = new DiaListaAlimentos(this.getApp(), 0, vAlim);
        diaAlim.show();
      }
      else {
        this.getApp().showAdvise("No se han encontrado alimentos");
        diaAlim = null;
      }
    }
    catch (Exception e) {
      this.getApp().trazaLog(e);
    }
    // Si se ha seleccionado un alimento, pues ...
    if ( (diaAlim != null) && (diaAlim.bAceptar()) &&
        (diaAlim.devuelveSeleccion() != null) &&
        (diaAlim.devuelveSeleccion().length() > 0)) {
      txtAlimento.setText(diaAlim.devuelveSeleccion());
      txtAlimento_focusLost();
    }
    diaAlim = null;

    // Cambios 20-04-01 (ARS)
    // lista de valores
    //clv = new CListaValores(this.getApp(),
    //                        "Lista de alimentos",
    //                        qt,
    //                        vCod);
    //clv.show();

    // recupera el centro seleccionado
    //if (clv.getSelected() != null) {
    //  txtAlimento.setText(clv.getSelected().getString("DS_ALI"));
    //  txtAlimento_focusLost();
    //}

    //clv = null;

    Inicializar(CInicializar.NORMAL);
  }

}

/******************* ESCUCHADORES **********************/

// Botones
class DiaAtaqDatosActionAdapter
    implements java.awt.event.ActionListener, Runnable {

  DiaAtaqDatos adaptee;
  ActionEvent evt;

  DiaAtaqDatosActionAdapter(DiaAtaqDatos adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btn_actionPerformed(evt);
  }
}

class DiaAtaqDatos_txtAlimento_focusAdapter
    extends java.awt.event.FocusAdapter {
  DiaAtaqDatos adaptee;

  DiaAtaqDatos_txtAlimento_focusAdapter(DiaAtaqDatos adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.txtAlimento_focusLost();
  }
}

class DiaAtaqDatos_btnLupa_actionAdapter
    implements java.awt.event.ActionListener {
  DiaAtaqDatos adaptee;

  DiaAtaqDatos_btnLupa_actionAdapter(DiaAtaqDatos adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnLupa_actionPerformed();
  }
}
