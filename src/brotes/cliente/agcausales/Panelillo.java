package brotes.cliente.agcausales;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPanel;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

/**
 * Panelillo para probar el paquete ...
 */
public class Panelillo
    extends CPanel { //CPanel

  ButtonControl btnAlta = new ButtonControl();
  ButtonControl btnBaja = new ButtonControl();
  ButtonControl btnModificacion = new ButtonControl();
  ButtonControl btnConsulta = new ButtonControl();

  // Organizacion del panel
  private XYLayout xYLayout = new XYLayout();

  // constructor del panel PanMant
  public Panelillo(CApp a) {

    try {
      setApp(a);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del panel PanMant
  public void jbInit() throws Exception {

    // Organizacion del panel
    this.setSize(new Dimension(200, 200));
    xYLayout.setHeight(200);
    xYLayout.setWidth(200);
    this.setLayout(xYLayout);

    // Escuchadores de eventos

    PanMant_actionAdapter actionAdapter = new PanMant_actionAdapter(this);

    btnAlta.setActionCommand("alta");
    btnAlta.setLabel("Alta");
    btnAlta.addActionListener(actionAdapter);

    btnBaja.setActionCommand("baja");
    btnBaja.setLabel("Baja");
    btnBaja.addActionListener(actionAdapter);

    btnModificacion.setActionCommand("modificacion");
    btnModificacion.setLabel("Modificacion");
    btnModificacion.addActionListener(actionAdapter);

    btnConsulta.setActionCommand("consulta");
    btnConsulta.setLabel("Consulta");
    btnConsulta.addActionListener(actionAdapter);

    final String imgBUSCAR = "images/refrescar.gif";
    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().CargaImagenes();
    btnAlta.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));
    btnBaja.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));
    btnModificacion.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));
    btnConsulta.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));

    // tool tips

//    new CContextHelp("Obtener almacenes",btnBuscar);
    this.add(btnAlta, new XYConstraints(5, 5, -1, -1));
    this.add(btnBaja, new XYConstraints(105, 5, -1, -1));
    this.add(btnModificacion, new XYConstraints(5, 105, -1, -1));
    this.add(btnConsulta, new XYConstraints(105, 105, -1, -1));
  }

  public void Inicializar() {}

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;
    }
  }

  // gesti�n de los botones
  void btn_actionPerformed(ActionEvent e) {

    QueryTool qt = new QueryTool();
    Lista filtro = new Lista();
    Lista resultado = new Lista();
    Data pepito = new Data();

    pepito.put("CD_ANO", "1997");
    pepito.put("NM_ALERBRO", "2");

    qt.putName("SIVE_BROTES");
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);
    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("CD_ANO", pepito.getString("CD_ANO"));
    qt.putWhereValue("NM_ALERBRO", pepito.getString("NM_ALERBRO"));
    qt.putOperator("CD_ANO", "=");
    qt.putOperator("NM_ALERBRO", "=");

    filtro.addElement(qt);
    try {
      this.getApp().getStub().setUrl("servlet/SrvQueryTool");
      resultado = (Lista)this.getApp().getStub().doPost(1, filtro);
    }
    catch (Exception x) {
      this.getApp().trazaLog(x);
    }

    pepito.put("DS_BROTE", "Agente Causal");
    pepito.put("CD_OPE", ( (Data) resultado.elementAt(0)).getString("CD_OPE"));
    pepito.put("FC_ULTACT", "03/03/2000 12:33:36"); //((Data)resultado.elementAt(0)).getString("FC_ULTACT"));
    pepito.put("CD_GRUPO", "0");

    if (e.getActionCommand().equals("alta")) {
      PanAgCausal dm = null;
      dm = new PanAgCausal(this.getApp(), 0, pepito);
      dm.show();
    }
    if (e.getActionCommand().equals("modificacion")) {
      PanAgCausal dm = null;
      dm = new PanAgCausal(this.getApp(), 1, pepito);
      dm.show();
    }
    if (e.getActionCommand().equals("baja")) {
      PanAgCausal dm = null;
      dm = new PanAgCausal(this.getApp(), 2, pepito);
      dm.show();
    }
    if (e.getActionCommand().equals("consulta")) {
      PanAgCausal dm = null;
      dm = new PanAgCausal(this.getApp(), 3, pepito);
      dm.show();
    }

  }

}

// botones de centro, almac�n y buscar
class PanMant_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Panelillo adaptee;
  ActionEvent e;

  PanMant_actionAdapter(Panelillo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
