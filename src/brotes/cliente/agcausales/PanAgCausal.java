/**
 * Clase: DiaMed
 * Paquete: brotes.cliente.ataquealim
 * Hereda: CDialog
 * Autor: �ngel Rodr�guez S�nchez (ARS)
 * Fecha Inicio: 14/02/2000
 * Analisis Funcional: Punto 1.0 Tasa de ataque espec�fica por alimentos.
 * Descripcion: Implementacion de los dialogos que permite dar de alta
 *   baja, modificar y consultar alimentos que han provocado brotes de alergia
 */

package brotes.cliente.agcausales;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.UButtonControl;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class PanAgCausal
    extends CDialog
    implements CInicializar, CFiltro, ContCListaMantenimientoExt {

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  final String imgACEPTAR = "images/aceptar.gif";
  final String imgCANCELAR = "images/cancelar.gif";

  // Mensaje en caso de bloqueo
  private final String DATOS_BLOQUEADOS =
      "Los datos han sido modificados. �Desea sobrescribirlos?";

  private int modoOperacion;

  private Data dtEnt = new Data();

  private Lista lstBorrar = new Lista();

  /****************** Componentes del panel ********************/

  // Organizacion del panel
  private XYLayout xYLayout = new XYLayout();

  // CListaMantenimiento (mi favorita)
  // Pero no... ahora otra m�s chula, nueva, que permite
  // habilitar y dehabilitar botoncicos.... (ma�ico)
  CListaMantenimientoExt clmMantenimiento = null;

  // Botones de aceptar y cancelar
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  PanAgCausalActionAdapter actionAdapter = null;
  Label lblBroteDesc = new Label();
  Label lblDatosBrote = new Label();

  // estructuras para comprobar el bloqueo
  private QueryTool qtBloqueo = null;
  private Data dtBloqueo = null;

  // Constructor
  // CApp sera luego el panel del que procede.

  public PanAgCausal(CApp a, int modo, Data entrada) {

    super(a);

    dtEnt = entrada;

    this.setTitle("Brotes: Agentes Causales");
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    try {

      // Inicializacion
      modoOperacion = modo;

      // CLista mantenimiento que necesitamos poner aqu�.
      // botones

      CBoton btn1 = new CBoton("", "images/alta.gif", "Nueva petici�n", false, false);
//      btn1.setEnabled(false);
      vBotones.addElement(btn1);
      btn1 = null;

      vBotones.addElement(new CBoton("",
                                     "images/modificacion.gif",
                                     "Modificar petici�n",
                                     true,
                                     true));

      vBotones.addElement(new CBoton("",
                                     "images/baja.gif",
                                     "Borrar petici�n",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumnaImagen("Agente Causal",
                                            "300",
                                            "AG_CAUSAL",
                                            false));

      vLabels.addElement(new CColumnaImagen("Informaci�n Adicional",
                                            "300",
                                            "DS_MASAGCAUSAL",
                                            false));

      vLabels.addElement(new CColumnaImagen("Confirmado",
                                            "75",
                                            "IT_CONFIRMADO_IMG",
                                            true));

      clmMantenimiento = new CListaMantenimientoExt(a,
          vLabels,
          vBotones,
          this,
          this,
          this);

      /*
             clmMantenimiento = new CListaMantenimiento(a,
                                                vLabels,
                                                vBotones,
                                                this,
                                                this);
       */

      jbInit();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    // Organizacion del panel
    this.setSize(new Dimension(720, 450));
    xYLayout.setHeight(450);
    xYLayout.setWidth(720);
    this.setLayout(xYLayout);

    // Im�genes

    this.getApp().getLibImagenes().put(imgACEPTAR);
    this.getApp().getLibImagenes().put(imgCANCELAR);
    this.getApp().getLibImagenes().CargaImagenes();

    // Escuchadores
    actionAdapter = new PanAgCausalActionAdapter(this);

    // Boton de Aceptar
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.addActionListener(actionAdapter);
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));

    // Boton de Cancelar
    btnCancelar.setLabel("Cancelar");
    lblBroteDesc.setText("Brote:");
    lblDatosBrote.setText(dtEnt.getString("CD_ANO") + "/" +
                          dtEnt.getString("NM_ALERBRO") + " - " +
                          dtEnt.getString("DS_BROTE"));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));

    this.add(lblBroteDesc, new XYConstraints(4, 23, 42, -1));
    this.add(lblDatosBrote, new XYConstraints(56, 23, 263, -1));
    this.add(clmMantenimiento, new XYConstraints(2, 56, 716, 290));
    this.add(btnAceptar, new XYConstraints(507, 376, 88, 29));
    this.add(btnCancelar, new XYConstraints(618, 376, 88, 29));

    verDatos();
  } // Fin jbInit()

  private void verDatos() {
    if ( (modoOperacion == BAJA) || (modoOperacion == CONSULTA)) {
      this.setEnabled(false);
      btnAceptar.setVisible(false);
      btnCancelar.setLabel("Salir");
      btnCancelar.setEnabled(true);
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
    if (modoOperacion != ALTA) {
      clmMantenimiento.setPrimeraPagina(this.primeraPagina());

    }
  }

  public void Inicializar(int i) {
    switch (i) {
      /*
             // modo espera
             case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
             // modo normal
             case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if((modoOperacion==BAJA) || (modoOperacion==CONSULTA)){
          this.setEnabled(false);
          btnAceptar.setVisible(false);
          btnCancelar.setLabel("Salir");
          btnCancelar.setEnabled(true);
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }else{
          btnAceptar.setEnabled(true);
          btnCancelar.setEnabled(true);
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
        break;
       */

      case ALTA:
      case MODIFICACION:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case CONSULTA:
      case BAJA:
        this.setEnabled(false);
        btnAceptar.setVisible(false);
        btnCancelar.setLabel("Salir");
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
    this.doLayout();
  } // Fin Inicializar()

  // operaciones de la botonera
  public void realizaOperacion(int j) {

    Lista lisPet = this.clmMantenimiento.getLista();
    DiaAgCausal dm = null;
    Data dPetMod = new Data();
    Data dtResultado = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:
        dm = new DiaAgCausal(this.getApp(), 0, dtEnt);
        dm.show();
        if (dm.bAceptar()) {
          dtResultado = dm.dtResultado();
          if (!esConfirmado(lisPet, dtResultado)) {
            if (!sePuedeRepetir(lisPet, dtResultado)) {
              lisPet.addElement(dtResultado);
              clmMantenimiento.setPrimeraPagina(preparaDatos(lisPet));
            }
            else {
              this.getApp().showAdvise("No se pueden repetir ese agente.");
            }
          }
          else {
            this.getApp().showAdvise(
                "No se pueden repetir dos agentes confirmados.");
          }
//           clmMantenimiento.setPrimeraPagina(this.primeraPagina());
          // Actualizamos la tasa de ataques total.
        }
        break;
      case MODIFICACION:
        dPetMod = clmMantenimiento.getSelected();
        if (dPetMod != null) {
          if (modoOperacion != CONSULTA) {
            int ind = clmMantenimiento.getSelectedIndex();
            dm = new DiaAgCausal(this.getApp(), 1, dPetMod);
            dm.show();
            if (dm.bAceptar()) {
              dtResultado = dm.dtResultado();
              if (!esConfirmado(lisPet, dtResultado)) {
                lisPet.removeElementAt(ind);
                lisPet.insertElementAt( (Data) dtResultado, ind);
                clmMantenimiento.setPrimeraPagina(preparaDatos(lisPet));
              }
              else {
                this.getApp().showAdvise(
                    "No puede haber dos agentes confirmados.");
              }
//               clmMantenimiento.setPrimeraPagina(this.primeraPagina());
            }
          }
          else if (modoOperacion == CONSULTA) {
            int ind = clmMantenimiento.getSelectedIndex();
            dm = new DiaAgCausal(this.getApp(), 3, dPetMod);
            dm.show();
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un proveedor en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dPetMod = clmMantenimiento.getSelected();
        if (dPetMod != null) {
          int ind = clmMantenimiento.getSelectedIndex();
          dm = new DiaAgCausal(this.getApp(), 2, dPetMod);
          dm.show();
          if (dm.bAceptar()) {
            dtResultado = dm.dtResultado();
            if (dtResultado != null) {
              lstBorrar.addElement(dtResultado);
            }
            lisPet.removeElementAt(ind);
            clmMantenimiento.setPrimeraPagina(preparaDatos(lisPet));
//            clmMantenimiento.setPrimeraPagina(this.primeraPagina());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un almacen en la tabla.");
        }
        break;
    }
  }

  private boolean sePuedeRepetir(Lista l, Data d) {

    Data dtTmp = new Data();
    boolean igual = false;

    for (int i = 0; i < l.size(); i++) {
      dtTmp = (Data) l.elementAt(i);

      if ( (dtTmp.getString("CD_GRUPO").equals(d.getString("CD_GRUPO"))) &&
          (dtTmp.getString("IT_REPE").equals("N")) &&
          (dtTmp.getString("CD_AGENTC").equals(d.getString("CD_AGENTC"))) &&
          (dtTmp.getString("IT_REPE").equals(d.getString("IT_REPE")))) {
        igual = true;
      }
    } // del for
    return igual;
  }

  private boolean esConfirmado(Lista l, Data d) {

    Data dtTmp = new Data();
    boolean igual = false;

    for (int i = 0; i < l.size(); i++) {
      dtTmp = (Data) l.elementAt(i);

      if ( (dtTmp.getString("IT_CONFIRMADO").equals("S")) &&
          (d.getString("IT_CONFIRMADO").equals("S"))) {
        igual = true;
      }
    } // del for
    return igual;
  }

  //Habilita o deshabilita botones en funci�n de si son de cobertura o hay item elegido
  public void cambiarBotones(Vector vectBotones) {
    UButtonControl btnControlAlta = (UButtonControl) vectBotones.elementAt(0);
    UButtonControl btnControlBaja = (UButtonControl) vectBotones.elementAt(2);

    if (modoOperacion == CONSULTA) {
      btnControlBaja.setEnabled(false);
      btnControlAlta.setEnabled(false);
    }
  }

  // prepara la informaci�n para el control de bloqueo
  private void preparaBloqueo() {
    // query tool
    qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", dtEnt.getString("CD_ANO"));
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", dtEnt.getString("NM_ALERBRO"));
    qtBloqueo.putOperator("NM_ALERBRO", "=");

    // informaci�n de bloqueo
    dtBloqueo = new Data();
    dtBloqueo.put("CD_OPE", dtEnt.getString("CD_OPE"));
    dtBloqueo.put("FC_ULTACT", dtEnt.getString("FC_ULTACT"));
  }

  /******************** Botoncicos de la lista  *************************/

  void btn_ActionPerformed(ActionEvent e) {
    Lista lstCLista = new Lista();
    Lista lstOperar = new Lista();
    Lista vResultado = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {

      try {
        this.getApp().getStub().setUrl("servlet/SrvAgCausal");

        preparaBloqueo();

        QueryTool qtHazBloqueo = new QueryTool();
        qtHazBloqueo = actualizaFC_ULTACT(dtEnt);
        Data dtHazBloqueo = new Data();
        dtHazBloqueo.put("4", qtHazBloqueo);
        lstOperar.addElement(dtHazBloqueo);

        /************** ALTAS ***************/
        lstCLista = this.clmMantenimiento.getLista();

        Data dtTemp = null;

        for (int i = 0; i < lstCLista.size(); i++) {
          dtTemp = (Data) lstCLista.elementAt(i);
          if (dtTemp.getString("TIPO_OPERACION").equals("A")) {
            QueryTool qtAlt = new QueryTool();
            qtAlt = realizarAlta(dtTemp);
            Data dtAlt = new Data();
            dtAlt.put("3", qtAlt);
            lstOperar.addElement(dtAlt);
            dtTemp.remove("TIPO_OPERACION");
          }
        }

        /************** MODIFICAR ***************/

        for (int i = 0; i < lstCLista.size(); i++) {
          dtTemp = (Data) lstCLista.elementAt(i);
          if (dtTemp.getString("TIPO_OPERACION").equals("M")) {
            QueryTool qtUpd = new QueryTool();
            qtUpd = realizarModificacion(dtTemp);
            Data dtUpd = new Data();
            dtUpd.put("4", qtUpd);
            lstOperar.addElement(dtUpd);
            dtTemp.remove("TIPO_OPERACION");
          }
        }

        /************** BAJAS ***************/
        for (int i = 0; i < lstBorrar.size(); i++) {
          dtTemp = (Data) lstBorrar.elementAt(i);
          QueryTool qtBaja = new QueryTool();
          qtBaja = realizarBaja(dtTemp);
          Data dtBaja = new Data();
          dtBaja.put("5", qtBaja);
          lstOperar.addElement(dtBaja);
        }

        // Operamos sobre la base de datos.

        if (lstOperar.size() > 0) {
          vResultado = (Lista)this.getApp().getStub().doPost(10000,
              lstOperar,
              qtBloqueo,
              dtBloqueo,
              getApp());

          // Devolvemos a la lista CD_OPE y FC_ULTACT.
        }
        String fecha = "";
        fecha = ( (Lista) vResultado.firstElement()).getFC_ULTACT();

        dtEnt.put("CD_OPE", dtEnt.getString("CD_OPE"));
        dtEnt.put("FC_ULTACT", fecha);

      }
      catch (Exception exc) {
        dispose();
        //this.getApp().trazaLog(exc);
        //this.getApp().showError(exc.getMessage());
      } // del "trai cach"
      dispose();
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      dispose();
    }
  }

  private QueryTool realizarAlta(Data datos) {
    QueryTool qtResultado = new QueryTool();
    qtResultado.putName("SIVE_AGCAUSAL_BROTE");
    qtResultado.putType("NM_SECAGB", QueryTool.INTEGER);
    qtResultado.putType("CD_ANO", QueryTool.STRING);
    qtResultado.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtResultado.putType("CD_GRUPO", QueryTool.STRING);
    qtResultado.putType("CD_AGENTC", QueryTool.STRING);
    qtResultado.putType("IT_CONFIRMADO", QueryTool.STRING);
    qtResultado.putType("DS_OBSERV", QueryTool.STRING);
    qtResultado.putType("DS_MASAGCAUSAL", QueryTool.STRING);
    /****/
    qtResultado.putValue("NM_SECAGB", "");
    qtResultado.putValue("CD_ANO", dtEnt.getString("CD_ANO"));
    qtResultado.putValue("NM_ALERBRO", dtEnt.getString("NM_ALERBRO"));
    qtResultado.putValue("CD_GRUPO", datos.getString("CD_GRUPO"));
    qtResultado.putValue("CD_AGENTC", datos.getString("CD_AGENTC"));
    qtResultado.putValue("IT_CONFIRMADO", datos.getString("IT_CONFIRMADO"));
    qtResultado.putValue("DS_OBSERV", datos.getString("DS_OBSERV"));
    qtResultado.putValue("DS_MASAGCAUSAL", datos.getString("DS_MASAGCAUSAL"));
    return qtResultado;
  }

  private QueryTool realizarModificacion(Data datos) {
    QueryTool qtResultado = new QueryTool();

    qtResultado.putName("SIVE_AGCAUSAL_BROTE");

    qtResultado.putType("IT_CONFIRMADO", QueryTool.STRING);
    qtResultado.putType("DS_OBSERV", QueryTool.STRING);
    qtResultado.putType("DS_MASAGCAUSAL", QueryTool.STRING);
    /****/
    qtResultado.putValue("IT_CONFIRMADO", datos.getString("IT_CONFIRMADO"));
    qtResultado.putValue("DS_OBSERV", datos.getString("DS_OBSERV"));
    qtResultado.putValue("DS_MASAGCAUSAL", datos.getString("DS_MASAGCAUSAL"));

    qtResultado.putWhereType("CD_ANO", QueryTool.STRING);
    qtResultado.putWhereValue("CD_ANO", dtEnt.getString("CD_ANO"));
    qtResultado.putOperator("CD_ANO", "=");

    qtResultado.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtResultado.putWhereValue("NM_ALERBRO", dtEnt.getString("NM_ALERBRO"));
    qtResultado.putOperator("NM_ALERBRO", "=");

    qtResultado.putWhereType("NM_SECAGB", QueryTool.INTEGER);
    qtResultado.putWhereValue("NM_SECAGB", datos.getString("NM_SECAGB"));
    qtResultado.putOperator("NM_SECAGB", "=");

    return qtResultado;
  }

  private QueryTool realizarBaja(Data datos) {
    QueryTool qtResultado = new QueryTool();

    qtResultado.putName("SIVE_AGCAUSAL_BROTE");

    qtResultado.putWhereType("NM_SECAGB", QueryTool.INTEGER);
    qtResultado.putWhereValue("NM_SECAGB", datos.getString("NM_SECAGB"));
    qtResultado.putOperator("NM_SECAGB", "=");

    return qtResultado;
  }

  private QueryTool actualizaFC_ULTACT(Data datos) {
    QueryTool qtResultado = new QueryTool();
    qtResultado.putName("SIVE_BROTES");

    qtResultado.putType("CD_OPE", QueryTool.STRING);
    qtResultado.putValue("CD_OPE", dtEnt.getString("CD_OPE"));
    qtResultado.putType("FC_ULTACT", QueryTool.STRING);
    qtResultado.putValue("FC_ULTACT", "");

    qtResultado.putWhereType("CD_ANO", QueryTool.STRING);
    qtResultado.putWhereValue("CD_ANO", dtEnt.getString("CD_ANO"));
    qtResultado.putOperator("CD_ANO", "=");

    qtResultado.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtResultado.putWhereValue("NM_ALERBRO", dtEnt.getString("NM_ALERBRO"));
    qtResultado.putOperator("NM_ALERBRO", "=");

    return qtResultado;
  }

  public Lista primeraPagina() {
    QueryTool2 qt = new QueryTool2();
    QueryTool qtAdic = new QueryTool();
    Data dtAdic = new Data();

    Lista Filtro = new Lista();
    Lista Resultado = new Lista();

    qt.putName("SIVE_AGCAUSAL_BROTE");
    qt.putType("NM_SECAGB", QueryTool.INTEGER);
    qt.putType("CD_GRUPO", QueryTool.STRING);
    qt.putType("CD_AGENTC", QueryTool.STRING);
    qt.putType("DS_OBSERV", QueryTool.STRING);
    qt.putType("DS_MASAGCAUSAL", QueryTool.STRING);
    qt.putType("IT_CONFIRMADO", QueryTool.STRING);
    // buscamos las descripciones del agente
    qtAdic.putName("SIVE_AG_CAUSALES");
    qtAdic.putType("CD_GRUPO", QueryTool.STRING);
    qtAdic.putType("CD_AGENTC", QueryTool.STRING);
    qtAdic.putType("DS_AGENTC", QueryTool.STRING);
    qtAdic.putType("IT_INFADIC", QueryTool.STRING);
    qtAdic.putType("IT_REPE", QueryTool.STRING);
    /*  -----  */
    dtAdic.put("CD_AGENTC", QueryTool.STRING);
    qt.addQueryTool(qtAdic);
    qt.addColumnsQueryTool(dtAdic);
    // Fin de la QueryTool2
    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", dtEnt.getString("CD_ANO"));
    qt.putOperator("CD_ANO", "=");
    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", dtEnt.getString("NM_ALERBRO"));
    qt.putOperator("NM_ALERBRO", "=");

    Filtro.addElement(qt);
    try {
      this.getApp().getStub().setUrl("servlet/SrvQueryTool");
      Resultado = (Lista)this.getApp().getStub().doPost(1, Filtro);
    }
    catch (Exception e) {
      this.getApp().trazaLog(e);
    }

    return preparaDatos(Resultado);
  }

  public Lista siguientePagina() {
    Lista l = new Lista();
    return l;
  }

  private Lista preparaDatos(Lista entrada) {

    Lista salida = entrada;
    Data temp = new Data();

    for (int i = 0; i < salida.size(); i++) {
      temp = (Data) salida.elementAt(i);
      temp.put("AG_CAUSAL",
               temp.getString("CD_AGENTC") + " " + temp.getString("DS_AGENTC"));
      if (temp.getString("IT_CONFIRMADO").equals("S")) {
        temp.put("IT_CONFIRMADO_IMG",
                 this.getApp().getLibImagenes().get(imgACEPTAR));
      }
      else if (temp.getString("IT_CONFIRMADO").equals("N")) {
        temp.put("IT_CONFIRMADO_IMG",
                 this.getApp().getLibImagenes().get(imgCANCELAR));
      }
    } // del bucle
    return salida;
  }

}

/******************* ESCUCHADORES **********************/

// Botones
class PanAgCausalActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanAgCausal adaptee;
  ActionEvent evt;

  PanAgCausalActionAdapter(PanAgCausal adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btn_ActionPerformed(evt);
  }
}
