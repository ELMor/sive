/**
 * Clase: DAPanDescrTemp
 * Hereda: CPanel
 * Autor: Pedro Antonio D�az (PDP)
 * Fecha Inicio: 19/05/2000
 * Analisis Funcional: Informe Final
 * Descripcion: Panel correspondiente a la descripci�n temporal
 */
package brotes.cliente.dbpandescrtemp;

import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.CheckboxControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.CHora;
import brotes.cliente.c_componentes.DatoRecogible;
import brotes.cliente.c_comuncliente.SincrEventos;
import brotes.cliente.diabif.DiaBIF;
import capp2.CApp;
import capp2.CDecimal;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CPanel;
import sapp2.Data;

public class DBPanDescrTemp
    extends CPanel
    implements CInicializar, DatoRecogible {

  /*************** Datos propios del panel ****************/

  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;
  final int ESPERA = 4;
  final int NORMAL = 5;

  //para guardar el estado anterior
  private String PI = "";
  private String CC = "";

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  /****************** Componentes del panel ********************/
  XYLayout xYLayout = new XYLayout();

  XYLayout xYLayout1 = new XYLayout();
  CApp applet = null;
  DiaBIF dlg = null;
  Data brote = null;

  Label lFIS = new Label();
  Label lFISUC = new Label();
  Label lPI = new Label();
  Label lFISPC = new Label();
  Label lMinPI = new Label();
  Label lDCC = new Label();

  CheckboxGroup checkGroupPI = new CheckboxGroup();
  CheckboxGroup checkGroupDCC = new CheckboxGroup();
  // 21-05-01 (ARS) Cambio para CFecha con "/" autom�ticas.
  fechas.CFecha fechaPC = new fechas.CFecha("N");
  fechas.CFecha fechaUC = new fechas.CFecha("N");
  CHora horaPC = new CHora("N");
  CHora horaUC = new CHora("N");

  itemListener item = null;

  CEntero txtMinPI = new CEntero(6);
  CEntero txtMaxPI = new CEntero(6);
  Label lMaxPI = new Label();
  CDecimal txtMedPI = new CDecimal(5, 1);
  Label lMedPI = new Label();
  CheckboxControl checkDPI = new CheckboxControl();
  CheckboxControl checkHPI = new CheckboxControl();
  CEntero txtMinDCC = new CEntero(6);
  Label lMinDCC = new Label();
  CheckboxControl checkDDCC = new CheckboxControl();
  CDecimal txtMedDCC = new CDecimal(5, 1);
  Label lMedDCC = new Label();
  CheckboxControl checkHDCC = new CheckboxControl();
  CEntero txtMaxDCC = new CEntero(6);
  Label lMaxDCC = new Label();

  SincrEventos scr = new SincrEventos();

  // constructor

  public DBPanDescrTemp(CApp a,
                        DiaBIF d,
                        Data b,
                        int modo) {

    super(a);
    applet = a;
    dlg = d;
    brote = b;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;

    try {

      jbInit();

      //por defecto DIAS
      checkHPI.setChecked(false);
      checkDPI.setChecked(true);
      checkHDCC.setChecked(false);
      checkDDCC.setChecked(true);

      PI = "H";
      CC = "H";

      Inicializar(modoOperacion);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(755, 280));

    xYLayout.setHeight(270); //(280);
    xYLayout.setWidth(735); //(745);

    fechaPC.setName("fechaPC");
    fechaUC.setName("fechaUC");
    horaPC.setName("horaPC");
    horaUC.setName("horaUC");
    checkHPI.setName("horasPI");
    checkHDCC.setName("horasCC");
    checkDPI.setName("diasPI");
    checkDDCC.setName("diasCC");

    item = new itemListener(this);

    checkHPI.addItemListener(item);
    checkHDCC.addItemListener(item);
    checkDPI.addItemListener(item);
    checkDDCC.addItemListener(item);

    //fechaPC.addFocusListener(focusAdap);
    //horaPC.addFocusListener(focusAdapHora);
    //fechaUC.addFocusListener(focusAdap);
    //horaUC.addFocusListener(focusAdapHora);

    lFIS.setText("F-H. Inicio Sintomas");
    lFISUC.setForeground(Color.black);
    lFISUC.setFont(new Font("Dialog", 0, 12));
    lFISUC.setText("Ultimo caso:");
    lPI.setForeground(Color.black);
    lPI.setFont(new Font("Dialog", 0, 12));
    lPI.setText("Periodo de incubacion");
    lMinPI.setForeground(Color.black);
    lMinPI.setFont(new Font("Dialog", 0, 12));
    lMinPI.setText("Minimo");
    lMaxPI.setForeground(Color.black);
    lMaxPI.setFont(new Font("Dialog", 0, 12));
    lMaxPI.setText("Maximo");
    lMedPI.setForeground(Color.black);
    lMedPI.setFont(new Font("Dialog", 0, 12));
    lMedPI.setText("Mediana");

    checkDPI.setCheckboxGroup(checkGroupPI);
    checkDPI.setLabel("Dias");
    checkHPI.setCheckboxGroup(checkGroupPI);
    checkHPI.setLabel("Horas");
    lDCC.setForeground(Color.black);
    lDCC.setFont(new Font("Dialog", 0, 12));
    lDCC.setText("Duracion del cuadro clinico");
    lMinDCC.setForeground(Color.black);
    lMinDCC.setFont(new Font("Dialog", 0, 12));
    lMinDCC.setText("Minimo");
    checkDDCC.setCheckboxGroup(checkGroupDCC);
    checkDDCC.setLabel("Dias");

    lMedDCC.setForeground(Color.black);
    lMedDCC.setFont(new Font("Dialog", 0, 12));
    lMedDCC.setText("Mediana");
    checkHDCC.setCheckboxGroup(checkGroupDCC);
    checkHDCC.setLabel("Horas");
    lMaxDCC.setForeground(Color.black);
    lMaxDCC.setFont(new Font("Dialog", 0, 12));
    lMaxDCC.setText("M�ximo");
    lFISPC.setText("Primer caso:");

    this.setLayout(xYLayout);
    //lbl F-H
    this.add(lFIS, new XYConstraints(9, 20, 132, -1));

    this.add(lFISPC, new XYConstraints(9 + 30, 50, 80, -1));
    this.add(fechaPC, new XYConstraints(100 + 30, 50, 100, -1));
    this.add(horaPC, new XYConstraints(205 + 30, 50, 60, -1));

    this.add(lFISUC, new XYConstraints(300 + 30, 50, -1, -1));
    this.add(fechaUC, new XYConstraints(400 + 30, 50, 100, -1));
    this.add(horaUC, new XYConstraints(505 + 30, 50, 60, -1));

    //lbl periodo de incubaci�n
    this.add(lPI, new XYConstraints(9, 50 + 50, 147, -1));

    this.add(checkDPI, new XYConstraints(9 + 30, 50 + 50 + 35, -1, -1));
    this.add(checkHPI, new XYConstraints(9 + 30 + 85, 50 + 50 + 35, -1, -1));

    this.add(lMinPI, new XYConstraints(9 + 30, 50 + 50 + 2 * 35, 58, -1));
    this.add(txtMinPI, new XYConstraints(71 + 30, 50 + 50 + 2 * 35, 50, -1));
    this.add(lMaxPI, new XYConstraints(146 + 30, 50 + 50 + 2 * 35, 58, -1));
    this.add(txtMaxPI, new XYConstraints(208 + 30, 50 + 50 + 2 * 35, 50, -1));
    this.add(lMedPI, new XYConstraints(283 + 30, 50 + 50 + 2 * 35, 58, -1));
    this.add(txtMedPI, new XYConstraints(345 + 30, 50 + 50 + 2 * 35, 50, -1));

    //lbl cuadro clinico
    this.add(lDCC, new XYConstraints(9, 50 + 2 * 50 + 2 * 35, 221, -1));

    this.add(checkDDCC, new XYConstraints(9 + 30, 50 + 2 * 50 + 3 * 35, -1, -1));
    this.add(checkHDCC,
             new XYConstraints(9 + 30 + 85, 50 + 2 * 50 + 3 * 35, -1, -1));

    this.add(lMinDCC, new XYConstraints(9 + 30, 50 + 2 * 50 + 4 * 35, 58, -1));
    this.add(txtMinDCC, new XYConstraints(71 + 30, 50 + 2 * 50 + 4 * 35, 50, -1));
    this.add(lMaxDCC, new XYConstraints(146 + 30, 50 + 2 * 50 + 4 * 35, 58, -1));
    this.add(txtMaxDCC,
             new XYConstraints(208 + 30, 50 + 2 * 50 + 4 * 35, 50, -1));
    this.add(lMedDCC, new XYConstraints(283 + 30, 50 + 2 * 50 + 4 * 35, 58, -1));
    this.add(txtMedDCC,
             new XYConstraints(345 + 30, 50 + 2 * 50 + 4 * 35, 50, -1));
  }

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    if (dlg != null) {
      dlg.Inicializar(modoOperacion);
    }
  } // Fin Inicializar()

  public void InicializarDesdeFuera(int mod) {
    modoOperacion = mod;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      // modo espera
      case CInicializar.ESPERA:
        enableControls(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case ALTA:
          case MODIFICACION:
            enableControls(true);
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            break;

          case BAJA:
          case CONSULTA:
            enableControls(false);
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            break;
        }
    }
  }

  private void enableControls(boolean en) {

    fechaPC.setEnabled(en);
    fechaUC.setEnabled(en);
    horaPC.setEnabled(en);
    horaUC.setEnabled(en);
    txtMaxPI.setEnabled(en);
    txtMedPI.setEnabled(en);
    txtMinPI.setEnabled(en);
    txtMinDCC.setEnabled(en);
    txtMaxDCC.setEnabled(en);
    txtMedDCC.setEnabled(en);
    checkDDCC.setEnabled(en);
    checkDPI.setEnabled(en);
    checkHDCC.setEnabled(en);
    checkHPI.setEnabled(en);
  }

  public void rellenarDatos(Data dtReg) {

    //Fecha Inicio S�ntomas Primer Caso
    String fcFIPC = (String) dtReg.getString("FC_ISINPRIMC");
    if (!fcFIPC.equals("")) {
      fechaPC.setText(fcFIPC.substring(0, fcFIPC.indexOf(' ', 0)));
      horaPC.setText( (fcFIPC.substring(fcFIPC.indexOf(' ', 0) + 1)).substring(
          0, 5));
    }

    //Fecha Inicio S�ntomas �ltimo Caso
    String fcFIUC = (String) dtReg.getString("FC_FSINPRIMC");
    if (!fcFIUC.equals("")) {
      fechaUC.setText(fcFIUC.substring(0, fcFIUC.indexOf(' ', 0)));
      horaUC.setText( (fcFIUC.substring(fcFIUC.indexOf(' ', 0) + 1)).substring(
          0, 5));
    }

    //PERIODO DE INCUBACION
    String it = "";
    String nmPI = "";
    it = dtReg.getString("IT_PERIN");
    if (it.equals("H")) {
      checkHPI.setChecked(true);
      checkDPI.setChecked(false);

      txtMinPI.setText(dtReg.getString("NM_PERINMIN"));

      txtMaxPI.setText(dtReg.getString("NM_PERINMAX"));

      txtMedPI.setText(dtReg.getString("NM_PERINMED"));
    }
    else if (it.equals("D")) {
      checkHPI.setChecked(false);
      checkDPI.setChecked(true);

      nmPI = dtReg.getString("NM_PERINMIN");
      txtMinPI.setText(HorasADias(nmPI));

      nmPI = dtReg.getString("NM_PERINMAX");
      txtMaxPI.setText(HorasADias(nmPI));

      nmPI = dtReg.getString("NM_PERINMED");
      txtMedPI.setText(HorasADiasDbl(nmPI, 1));
    }

    //DURACION CUADRO CLINICO
    String nmDCC = "";
    it = dtReg.getString("IT_DCUAC");
    if (it.equals("H")) {
      checkHDCC.setChecked(true);
      checkDDCC.setChecked(false);

      txtMinDCC.setText(dtReg.getString("NM_DCUACMIN"));

      txtMaxDCC.setText(dtReg.getString("NM_DCUACMAX"));

      txtMedDCC.setText(dtReg.getString("NM_DCUACMED"));
    }
    else if (it.equals("D")) {
      checkHDCC.setChecked(false);
      checkDDCC.setChecked(true);

      nmDCC = dtReg.getString("NM_DCUACMIN");
      txtMinDCC.setText(HorasADias(nmDCC));
      nmDCC = dtReg.getString("NM_DCUACMAX");
      txtMaxDCC.setText(HorasADias(nmDCC));
      nmDCC = dtReg.getString("NM_DCUACMED");
      txtMedDCC.setText(HorasADiasDbl(nmDCC, 1));
    }

    //backup
    if (checkHDCC.isChecked()) {
      CC = "H";
    }
    else {
      CC = "D";
    }
    if (checkHPI.isChecked()) {
      PI = "H";
    }
    else {
      PI = "D";

    }
  }

//En el data el periodo de incubacion y la duracion del cuadro
//clinico se guarda en horas
  private String HorasADias(String h) {
    if (h.trim().equals("")) {
      return ("");
    }

    Integer aux = new Integer(h);
    aux = new Integer(aux.intValue() / 24);
    return aux.toString();
  }

  private String DiasAHoras(String d) {
    if (d.trim().equals("")) {
      return ("");
    }
    Integer aux = new Integer(d);
    aux = new Integer(aux.intValue() * 24);
    return aux.toString();
  }

  private String DiasAHorasDbl(String d, int ndec) {
    String salida = null;
    if (d.trim().equals("")) {
      return ("");
    }
    Double aux = new Double(d);
    double aux1 = aux.doubleValue() * 24;
    salida = Double.toString(aux1);
    int pos_pto = salida.indexOf(".");
    salida = salida.substring(0, pos_pto + ndec + 1);
    return salida;
  }

  private String HorasADiasDbl(String h, int ndec) {
    String salida = null;
    if (h.trim().equals("")) {
      return ("");
    }
    Double aux = new Double(h);
    double aux1 = aux.doubleValue() / 24;
    salida = Double.toString(aux1);
    int pos_pto = salida.indexOf(".");
    salida = salida.substring(0, pos_pto + ndec + 1);
    return salida;
  }

  public void recogerDatos(Data dtDev) {

    //Fecha inicio de s�ntomas primer caso
    String fPC = "";
    if (!fechaPC.getText().equals("")) {
      fPC = fechaPC.getText().trim();
      if (!horaPC.getText().equals("")) {
        fPC += " " + horaPC.getText().trim() + ":00";
      }
      else {
        fPC += " 00:00:00";
      }
    }
    dtDev.put("FC_ISINPRIMC", fPC);

    //Fecha inicio de s�ntomas �ltimo caso
    String fUC = "";
    if (!fechaUC.getText().equals("")) {
      fUC = fechaUC.getText().trim();
      if (!horaUC.getText().equals("")) {
        fUC += " " + horaUC.getText().trim() + ":00";
      }
      else {
        fUC += " 00:00:00";
      }
    }
    dtDev.put("FC_FSINPRIMC", fUC);

    //Periodo de Incubaci�n
    if (checkDPI.isChecked()) {
      dtDev.put("IT_PERIN", "D");
      dtDev.put("NM_PERINMIN", DiasAHoras(txtMinPI.getText().trim()));
      dtDev.put("NM_PERINMAX", DiasAHoras(txtMaxPI.getText().trim()));
      dtDev.put("NM_PERINMED", DiasAHorasDbl(txtMedPI.getText().trim(), 1));
    }
    else {
      dtDev.put("IT_PERIN", "H");
      dtDev.put("NM_PERINMIN", txtMinPI.getText().trim());
      dtDev.put("NM_PERINMAX", txtMaxPI.getText().trim());
      dtDev.put("NM_PERINMED", txtMedPI.getText().trim());
    }

    //Duraci�n del cuadro cl�nico
    if (checkDDCC.isChecked()) {
      dtDev.put("IT_DCUAC", "D");
      dtDev.put("NM_DCUACMIN", DiasAHoras(txtMinDCC.getText().trim()));
      dtDev.put("NM_DCUACMAX", DiasAHoras(txtMaxDCC.getText().trim()));
      dtDev.put("NM_DCUACMED", DiasAHorasDbl(txtMedDCC.getText().trim(), 1));
    }
    else {
      dtDev.put("IT_DCUAC", "H");
      dtDev.put("NM_DCUACMIN", txtMinDCC.getText().trim());
      dtDev.put("NM_DCUACMAX", txtMaxDCC.getText().trim());
      dtDev.put("NM_DCUACMED", txtMedDCC.getText().trim());
    }
  }

  public boolean validarDatos() {

    boolean resultado = true;
    // Ver si la fecha/hora es correcta
    if (!horaPC.getText().equals("")) {
      horaPC.ValidarFecha();
      if (horaPC.getValid().equals("N")) {
        this.getApp().showError("Hora incorrecta");
        resultado = false;
      }
    }

    if (!horaUC.getText().equals("")) {
      horaUC.ValidarFecha();
      if (horaUC.getValid().equals("N")) {
        this.getApp().showError("Hora incorrecta");
        resultado = false;
      }
    }

    if (!fechaPC.getText().equals("")) {
      fechaPC.ValidarFecha();
      if (fechaPC.getValid().equals("N")) {
        this.getApp().showError("Fecha incorrecta");
        resultado = false;
      }
    }

    if (!fechaUC.getText().equals("")) {
      fechaUC.ValidarFecha();
      if (fechaUC.getValid().equals("N")) {
        this.getApp().showError("Fecha incorrecta");
        resultado = false;
      }
    }

    if (!ComprobarMaximos(0, "PI")) {
      resultado = false;
    }
    if (!ComprobarMaximos(0, "CC")) {
      resultado = false;

      // Comprobamos que el per�odo de incubaci�n el m�nimo sea menor
      // que la mediana  y que el m�ximo. De momento s�lo para per�odo de
      // incubaci�n. Para cuadro cl�nico ser�a (lo pongo por si acaso):
      /*
           if (!comprobarMinimo(Double.valueOf(txtMinDCC.getText()).doubleValue(),
                           Double.valueOf(txtMaxDCC.getText()).doubleValue(),
                           Double.valueOf(txtMedDCC.getText()).doubleValue()))
        resultado = false;
       */
      // 30-03-01 (ARS) Se incluyen estas comprobaciones, para que no
      // salte 'excepci�n'.
    }
    if ( (txtMinPI.getText().trim().length() > 0) &&
        (txtMaxPI.getText().trim().length() > 0) &&
        (txtMedPI.getText().trim().length() > 0)) {
      if (!comprobarMinimo(Double.valueOf(txtMinPI.getText()).doubleValue(),
                           Double.valueOf(txtMaxPI.getText()).doubleValue(),
                           Double.valueOf(txtMedPI.getText()).doubleValue())) {
        resultado = false;
        this.getApp().showError("El m�nimo ha de ser inferior al m�ximo y a la mediana, y el m�ximo superior a la mediana.");
      }
    }
    return resultado;
  }

  private boolean comprobarMinimo(double min, double max, double med) {
    boolean estado = true;
    if ( (min > max) || (min > med) || (med > max)) {
      estado = false;
    }
    return estado;
  }

// Comprueba que todos Numericos positivos y su longitud
//-1: es con el anterior de backup
//0: con el actual
  boolean ComprobarMaximos(int ActualoAnterior, String PICC) {

    boolean consultardias = true;
//P Incubacion ******************
    if (PICC.equals("PI")) {

      //-------------------------------------------------------
      if (ActualoAnterior == -1) { //ie al reves de lo que este
        if (checkHPI.isChecked()) {
          consultardias = true;
        }
        else {
          consultardias = false;
        }
      }
      else { //normal
        if (checkHPI.isChecked()) {
          consultardias = false;
        }
        else {
          consultardias = true;
        }
      } //-----------------------------------------------------

      //si vacio, estan bien
      if (txtMaxPI.getText().trim().equals("") ||
          txtMinPI.getText().trim().equals("") ||
          txtMedPI.getText().trim().equals("")) {
        return true;
      }

      if (consultardias) { //dias
        if ( (new Integer(txtMaxPI.getText()).intValue()) > 41666) {
          this.getApp().showAdvise(
              "El campo Periodo de Incubaci�n M�ximo excede de 41666 d�as");
          txtMaxPI.requestFocus();
          return false;
        }
        if ( (new Integer(txtMinPI.getText()).intValue()) > 41666) {
          this.getApp().showAdvise(
              "El campo Periodo de Incubaci�n M�nimo excede de 41666 d�as");
          txtMinPI.requestFocus();
          return false;
        }
        if ( (new Double(txtMedPI.getText()).doubleValue()) > 4166) {
          this.getApp().showAdvise(
              "El campo Periodo de Incubaci�n Mediana excede de 4166 d�as");
          txtMedPI.requestFocus();
          return false;
        }
      }
      /*
         else{ //horas
        if ( (new Integer(txtMaxPI.getText()).intValue()) > 999999 ){
           ShowError("El campo Periodo de Incubaci�n M�ximo excede de 999999 horas");
            txtMaxPI.requestFocus();
            return false;
         }
          if ( (new Integer(txtMinPI.getText()).intValue()) > 999999 ){
           ShowError("El campo Periodo de Incubaci�n M�nimo excede de 999999 horas");
            txtMinPI.requestFocus();
            return false;
         }
          if ( (new Integer(txtMedPI.getText()).intValue()) > 999999 ){
           ShowError("El campo Periodo de Incubaci�n Mediana excede de 999999 horas");
            txtMedPI.requestFocus();
            return false;
         }
         }
       */
      //PI  ****************************
    }
//D Cuadro Clinico ******************
    if (PICC.equals("CC")) {

      //-------------------------------------------------------
      if (ActualoAnterior == -1) { //ie al reves de lo que este
        if (checkHDCC.isChecked()) {
          consultardias = true;
        }
        else {
          consultardias = false;
        }
      }
      else { //normal
        if (checkHDCC.isChecked()) {
          consultardias = false;
        }
        else {
          consultardias = true;
        }
      } //-----------------------------------------------------

      //si vacio, estan bien
      if (txtMaxDCC.getText().trim().equals("") ||
          txtMinDCC.getText().trim().equals("") ||
          txtMedDCC.getText().trim().equals("")) {
        return true;
      }

      if (consultardias) { //dias
        if ( (new Integer(txtMaxDCC.getText()).intValue()) > 41666) {
          this.getApp().showAdvise(
              "El campo Duraci�n Cuadro Cl�nico M�ximo excede de 41666 d�as");
          txtMaxDCC.requestFocus();
          return false;
        }
        if ( (new Integer(txtMinDCC.getText()).intValue()) > 41666) {
          this.getApp().showAdvise(
              "El campo Duraci�n Cuadro Cl�nico M�nimo excede de 41666 d�as");
          txtMinDCC.requestFocus();
          return false;
        }
        if ( (new Double(txtMedDCC.getText()).doubleValue()) > 4166) {
          this.getApp().showAdvise(
              "El campo Duraci�n Cuadro Cl�nico Mediana excede de 4166 d�as");
          txtMedDCC.requestFocus();
          return false;
        }
      }
      /*
         else{ //horas
         if ( (new Integer(txtMaxDCC.getText()).intValue()) > 999999 ){
           ShowError("El campo Duraci�n Cuadro Cl�nico M�ximo excede de 999999 horas");
            txtMaxDCC.requestFocus();
            return false;
         }
         if ( (new Integer(txtMinDCC.getText()).intValue()) > 999999 ){
           ShowError("El campo Duraci�n Cuadro Cl�nico M�nimo excede de 999999 horas");
            txtMinDCC.requestFocus();
            return false;
         }
         if ( (new Integer(txtMedDCC.getText()).intValue()) > 999999 ){
           ShowError("El campo Duraci�n Cuadro Cl�nico Mediana excede de 999999 horas");
            txtMedDCC.requestFocus();
            return false;
         }
         }
       */
      //CC  ****************************
    }
    return true;
  }

  /********** Sincronizador de Eventos ******************/

// Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  /****************** Funciones Auxiliares *****************/

  public void cambiarBIF(Data newBIF) {
    brote = newBIF;
  } // Fin cambiarAlerta()

  public void setModoEntrada(int m) {
    modoEntrada = m;
  } // Fin setModoEntrada()

//se invoca cuando se seleciona horas en PI
  void PIHoras_itemStateChanged(ItemEvent e) {

    if (!ComprobarMaximos( -1, "PI")) { //estaban MAL los dias
      checkDPI.setChecked(true); //volvemos a dias
      //backup: igual
    }
    else { //estaban BIEN los dias
      txtMaxPI.setText(DiasAHoras(txtMaxPI.getText().trim()));
      txtMedPI.setText(DiasAHorasDbl(txtMedPI.getText().trim(), 1));
      txtMinPI.setText(DiasAHoras(txtMinPI.getText().trim()));
      //backup: ahora estamos en horas
      PI = "H";
    }

  }

//se invoca cuando se seleciona dias en PI
  void PIDias_itemStateChanged(ItemEvent e) {
    if (!ComprobarMaximos( -1, "PI")) { //estaban MAL las horas
      checkHPI.setChecked(true); //volvemos a horas
      //backup: igual
    }
    else { //estaban BIEN las horas
      txtMaxPI.setText(HorasADias(txtMaxPI.getText().trim()));
      txtMedPI.setText(HorasADiasDbl(txtMedPI.getText().trim(), 1));
      txtMinPI.setText(HorasADias(txtMinPI.getText().trim()));
      //backup: ahora estamos en dias
      PI = "D";
    }

  };

//se invoca cuando se seleciona horas en CC
  void CCHoras_itemStateChanged(ItemEvent e) {
    if (!ComprobarMaximos( -1, "CC")) { //estaban MAL los dias
      checkDDCC.setChecked(true); //volvemos a dias
      //backup: igual
    }
    else { //estaban BIEN los dias
      txtMaxDCC.setText(DiasAHoras(txtMaxDCC.getText().trim()));
      txtMedDCC.setText(DiasAHorasDbl(txtMedDCC.getText().trim(), 1));
      txtMinDCC.setText(DiasAHoras(txtMinDCC.getText().trim()));
      //backup: ahora estamos en horas
      CC = "H";
    }
  }

//se invoca cuando se seleciona dias en CC
  void CCDias_itemStateChanged(ItemEvent e) {
    if (!ComprobarMaximos( -1, "CC")) { //estaban MAL las horas
      checkHDCC.setChecked(true); //volvemos a horas
      //backup: igual
    }
    else { //estaban BIEN las horas
      txtMaxDCC.setText(HorasADias(txtMaxDCC.getText().trim()));
      txtMedDCC.setText(HorasADiasDbl(txtMedDCC.getText().trim(), 1));
      txtMinDCC.setText(HorasADias(txtMinDCC.getText().trim()));
      //backup: ahora estamos en dias
      CC = "D";
    }

  };
} //clase

/*
 class focusAdapter implements java.awt.event.FocusListener, Runnable {
  DBPanDescrTemp adaptee;
  FocusEvent evt;
  focusAdapter(DBPanDescrTemp adaptee) {
    this.adaptee = adaptee;
  }
  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }
  public void focusGained(FocusEvent e) {
  }
  public void run() {
     if(((CFechaSimple)evt.getSource()).getName().equals("fechaPC")){
        adaptee.fechaPC_focusLost();
     }else if(((CFechaSimple)evt.getSource()).getName().equals("fechaUC")){
        adaptee.fechaUC_focusLost();
     }
  }
 }
 */

/*
 class focusAdapterHora implements java.awt.event.FocusListener, Runnable {
  DBPanDescrTemp adaptee;
  FocusEvent evt;
  focusAdapterHora(DBPanDescrTemp adaptee) {
    this.adaptee = adaptee;
  }
  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }
  public void focusGained(FocusEvent e) {
  }
  public void run() {
     if(((CHora)evt.getSource()).getName().equals("horaPC")){
        adaptee.horaPC_focusLost();
     }else if(((CHora)evt.getSource()).getName().equals("horaUC")){
        adaptee.horaUC_focusLost();
     }
  }
 }
 */

class itemListener
    implements java.awt.event.ItemListener, Runnable {
  DBPanDescrTemp adaptee;
  ItemEvent e = null;

  itemListener(DBPanDescrTemp adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent evt) {
    e = evt;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {

      String name = ( (Component) e.getSource()).getName();

      if (name.equals("horasPI")) {
        adaptee.PIHoras_itemStateChanged(e);
      }
      else if (name.equals("horasCC")) {
        adaptee.CCHoras_itemStateChanged(e);
      }
      else if (name.equals("diasPI")) {
        adaptee.PIDias_itemStateChanged(e);
      }
      else if (name.equals("diasCC")) {
        adaptee.CCDias_itemStateChanged(e);
      }
      s.desbloquea(adaptee);
    }
  }
} //class
