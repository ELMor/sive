package brotes.cliente.mantcatalog;

import capp2.CApp;
import capp2.CInicializar;

/* Implemento CInicializar, de manera que existan dos modos de espera y modo
 normal, de comportamiento*/
public class AppMant2
    extends CApp { //implements CInicializar {

  PanMant2 pan = null;
//creo una instancia del tipo de panel padre que se va a incluir en este applet

  public void start() {
    int tmp = Integer.parseInt(getParameter("TBL_MANT"));
    switch (tmp) {
      case 10:
        setTitulo("Mantenimiento de Agentes Causales");
        break;
      case 20:
        setTitulo("Mantenimiento de Factores Contribuyentes");
        break;
      case 30:
        setTitulo("Mantenimiento de Medidas");
        break;
      case 40:
        setTitulo("Mantenimiento de Grupos de Agentes T�xicos");
        break;
      case 50:
        setTitulo("Mantenimiento de Tipos de Brote");
        break;
    }
  }

  //public AppMant(){}???
  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {

    PanMant2 pan = new PanMant2(this, Integer.parseInt(getParameter("TBL_MANT")));
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }

}
