package brotes.cliente.mantcatalog;

import java.util.Vector;

import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.CheckboxControl;
import com.borland.jbcl.control.TextControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CCodigo;
import capp2.CColumna;
import capp2.CContextHelp;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CListaValores;
import capp2.CPanel;
import capp2.CPnlCodigo;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.StubSrvBD;

/**
 * Panel a trav�s del que se realizan los mantenimientos
 * de vacunaci�n.
 * @autor ARS
 * @version 1.0
 */
public class PanMant2
    extends CPanel
    implements CInicializar, CFiltro { //CPanel

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  XYLayout xYLayout1 = new XYLayout();
  CCodigo txtCod = new CCodigo();
  TextField txtDes = new TextField();
  CListaMantenimiento clmMantenimiento = null;
  CPnlCodigo pnlSeleccion = null;
  ButtonControl btnBuscar = new ButtonControl();
  CheckboxControl chckCod = new CheckboxControl();
  CheckboxControl chckDes = new CheckboxControl();
  CheckboxGroup chkboxGrupo = new CheckboxGroup();

  // datos
  private Data dtCentro = null;

  // filtro
  private Data dtFiltro = null;

  boolean campoExtra = true; // Variables para este panel..

  // tipos de tabla disponibles...
  public static final int mantAG_CAUSALES = 10;
  public static final int mantFACT_CONTRIB = 20;
  public static final int mantMEDIDAS = 30;
  public static final int mantAGENTE_TOXICO = 40;
  public static final int mantTIPO_BROTE = 50;

  // variables necesarias para adaptar dialogos y paneles a
  // nuestra tabla...

  Data dtConstPaneles = new Data();

  TextControl textControl1 = new TextControl();

  // constructor del panel PanMant2
  public PanMant2(CApp a, int tabla) {

    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    switch (tabla) {
      case PanMant2.mantAG_CAUSALES:
        dtConstPaneles.put("sTituloDialogoAlta", "Crear nuevo agente causal");
        dtConstPaneles.put("sTituloDialogoModif", "Modificar agente causal");
        dtConstPaneles.put("sTituloDialogoBaja", "Borrar agente causal");
        dtConstPaneles.put("sNomTabla", "SIVE_AG_CAUSALES");
        dtConstPaneles.put("sCodigo", "CD_AGENTC");
        dtConstPaneles.put("sTipoCodigo", QueryTool.STRING + "");
        dtConstPaneles.put("sDescripcion", "DS_AGENTC");
        dtConstPaneles.put("sTipoDescripcion", QueryTool.STRING + "");

        dtConstPaneles.put("sNomCheckExtra", "IT_INFADIC");
        dtConstPaneles.put("sTipoCheckExtra", QueryTool.STRING + "");

        dtConstPaneles.put("sNomCheckExtra1", "IT_REPE");
        dtConstPaneles.put("sTipoCheckExtra1", QueryTool.STRING + "");
        dtConstPaneles.put("bCheckExtra", "true");
        campoExtra = true; // Variable para este panel

        // Para la tabla padre .....
        dtConstPaneles.put("sNomTablaPadre", "SIVE_GRUPO_BROTE");
        dtConstPaneles.put("sCodigoPadre", "CD_GRUPO");
        dtConstPaneles.put("sTipoCodigoPadre", QueryTool.STRING + "");
        dtConstPaneles.put("sDescripcionPadre", "DS_GRUPO");
        dtConstPaneles.put("sTipoDescripcionPadre", QueryTool.STRING + "");

        dtConstPaneles.put("sLongitudCodigo", "6");
        dtConstPaneles.put("sLongitudDescripcion", "35");
        break;

      case PanMant2.mantFACT_CONTRIB:
        dtConstPaneles.put("sTituloDialogoAlta",
                           "Crear nuevo factor contribuyente");
        dtConstPaneles.put("sTituloDialogoModif",
                           "Modificar factor contribuyente");
        dtConstPaneles.put("sTituloDialogoBaja", "Borrar factor contribuyente");
        dtConstPaneles.put("sNomTabla", "SIVE_FACT_CONTRIB");
        dtConstPaneles.put("sCodigo", "CD_FACCONT");
        dtConstPaneles.put("sTipoCodigo", QueryTool.STRING + "");
        dtConstPaneles.put("sDescripcion", "DS_FACCON");
        dtConstPaneles.put("sTipoDescripcion", QueryTool.STRING + "");

        dtConstPaneles.put("sNomCheckExtra", "IT_MASFACCONT");
        dtConstPaneles.put("sTipoCheckExtra", QueryTool.STRING + "");

        dtConstPaneles.put("sNomCheckExtra1", "IT_REPE");
        dtConstPaneles.put("sTipoCheckExtra1", QueryTool.STRING + "");
        dtConstPaneles.put("bCheckExtra", "true");
        campoExtra = true; // Variable para este panel

        // Para la tabla padre .....
        dtConstPaneles.put("sNomTablaPadre", "SIVE_GRUPO_BROTE");
        dtConstPaneles.put("sCodigoPadre", "CD_GRUPO");
        dtConstPaneles.put("sTipoCodigoPadre", QueryTool.STRING + "");
        dtConstPaneles.put("sDescripcionPadre", "DS_GRUPO");
        dtConstPaneles.put("sTipoDescripcionPadre", QueryTool.STRING + "");

        dtConstPaneles.put("sLongitudCodigo", "2");
        dtConstPaneles.put("sLongitudDescripcion", "40");
        break;

      case PanMant2.mantMEDIDAS:
        dtConstPaneles.put("sTituloDialogoAlta", "Crear nueva medida");
        dtConstPaneles.put("sTituloDialogoModif", "Modificar medida");
        dtConstPaneles.put("sTituloDialogoBaja", "Borrar medida");
        dtConstPaneles.put("sNomTabla", "SIVE_MEDIDAS");
        dtConstPaneles.put("sCodigo", "CD_MEDIDA");
        dtConstPaneles.put("sTipoCodigo", QueryTool.STRING + "");
        dtConstPaneles.put("sDescripcion", "DS_MEDIDA");
        dtConstPaneles.put("sTipoDescripcion", QueryTool.STRING + "");

        dtConstPaneles.put("sNomCheckExtra", "IT_INFADIC");
        dtConstPaneles.put("sTipoCheckExtra", QueryTool.STRING + "");

        dtConstPaneles.put("sNomCheckExtra1", "IT_REPE");
        dtConstPaneles.put("sTipoCheckExtra1", QueryTool.STRING + "");
        dtConstPaneles.put("bCheckExtra", "true");
        campoExtra = true; // Variable para este panel

        // Para la tabla padre .....
        dtConstPaneles.put("sNomTablaPadre", "SIVE_GRUPO_BROTE");
        dtConstPaneles.put("sCodigoPadre", "CD_GRUPO");
        dtConstPaneles.put("sTipoCodigoPadre", QueryTool.STRING + "");
        dtConstPaneles.put("sDescripcionPadre", "DS_GRUPO");
        dtConstPaneles.put("sTipoDescripcionPadre", QueryTool.STRING + "");

        dtConstPaneles.put("sLongitudCodigo", "2");
        dtConstPaneles.put("sLongitudDescripcion", "40");
        break;

      case PanMant2.mantAGENTE_TOXICO:
        dtConstPaneles.put("sTituloDialogoAlta", "Crear nuevo agente t�xico");
        dtConstPaneles.put("sTituloDialogoModif", "Modificar agente t�xico");
        dtConstPaneles.put("sTituloDialogoBaja", "Borrar agente t�xico");
        dtConstPaneles.put("sNomTabla", "SIVE_AGENTE_TOXICO");
        dtConstPaneles.put("sCodigo", "CD_AGENTET");
        dtConstPaneles.put("sTipoCodigo", QueryTool.STRING + "");
        dtConstPaneles.put("sDescripcion", "DS_AGENTET");
        dtConstPaneles.put("sTipoDescripcion", QueryTool.STRING + "");

        dtConstPaneles.put("sNomCheckExtra", "IT_INFADI");
        dtConstPaneles.put("sTipoCheckExtra", QueryTool.STRING + "");

        //no tiene un segundo check
        dtConstPaneles.put("bCheckExtra", "false");
        campoExtra = false; // Variable para este panel

        // Para la tabla padre .....
        dtConstPaneles.put("sNomTablaPadre", "SIVE_GRUPO_BROTE");
        dtConstPaneles.put("sCodigoPadre", "CD_GRUPO");
        dtConstPaneles.put("sTipoCodigoPadre", QueryTool.STRING + "");
        dtConstPaneles.put("sDescripcionPadre", "DS_GRUPO");
        dtConstPaneles.put("sTipoDescripcionPadre", QueryTool.STRING + "");

        dtConstPaneles.put("sLongitudCodigo", "2");
        dtConstPaneles.put("sLongitudDescripcion", "30");
        break;

      case PanMant2.mantTIPO_BROTE:
        dtConstPaneles.put("sTituloDialogoAlta", "Crear nuevo tipo de brote");
        dtConstPaneles.put("sTituloDialogoModif", "Modificar tipo de brote");
        dtConstPaneles.put("sTituloDialogoBaja", "Borrar tipo de brote");
        dtConstPaneles.put("sNomTabla", "SIVE_TIPO_BROTE");
        dtConstPaneles.put("sCodigo", "CD_TBROTE");
        dtConstPaneles.put("sTipoCodigo", QueryTool.STRING + "");
        dtConstPaneles.put("sDescripcion", "DS_TBROTE");
        dtConstPaneles.put("sTipoDescripcion", QueryTool.STRING + "");

//             dtConstPaneles.put("sNomCheckExtra","IT_INFADI");
//             dtConstPaneles.put("sTipoCheckExtra",QueryTool.STRING+"");

        //no tiene un segundo check
        dtConstPaneles.put("bCheckExtra", "false");
        campoExtra = false; // Variable para este panel

        // Para la tabla padre .....
        dtConstPaneles.put("sNomTablaPadre", "SIVE_GRUPO_BROTE");
        dtConstPaneles.put("sCodigoPadre", "CD_GRUPO");
        dtConstPaneles.put("sTipoCodigoPadre", QueryTool.STRING + "");
        dtConstPaneles.put("sDescripcionPadre", "DS_GRUPO");
        dtConstPaneles.put("sTipoDescripcionPadre", QueryTool.STRING + "");

        dtConstPaneles.put("sLongitudCodigo", "2");
        dtConstPaneles.put("sLongitudDescripcion", "40");
        break;

    } //end switch

    try {
      setApp(a);

      // Petici�n para el CPnlC�digo:

      QueryTool qtSeleccion = new QueryTool();
      // Tabla
      qtSeleccion.putName(dtConstPaneles.getString("sNomTablaPadre"));
      // peticiones
      qtSeleccion.putType(dtConstPaneles.getString("sCodigoPadre"),
                          Integer.parseInt(dtConstPaneles.getString(
          "sTipoCodigoPadre")));
      qtSeleccion.putType(dtConstPaneles.getString("sDescripcionPadre"),
                          Integer.parseInt(dtConstPaneles.getString(
          "sTipoDescripcionPadre")));

      // panel
      pnlSeleccion = new CPnlCodigo(a,
                                    this,
                                    qtSeleccion,
                                    dtConstPaneles.getString("sCodigoPadre"),
                                    dtConstPaneles.getString(
          "sDescripcionPadre"),
                                    false,
                                    "Tabla de elementos",
                                    "Tabla de elementos");

      // botones
      vBotones.addElement(new CBoton(constantes.keyBtnAnadir,
                                     "images/alta.gif",
                                     "Nueva petici�n",
                                     false,
                                     false));

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion.gif",
                                     "Modificar petici�n",
                                     true,
                                     true));

      vBotones.addElement(new CBoton(constantes.keyBtnBorrar,
                                     "images/baja.gif",
                                     "Borrar petici�n",
                                     false,
                                     true));

      // etiquetas
      //si hay 2 check, sacar las 2 en la lista
      if (campoExtra) {
        vLabels.addElement(new CColumna("Grupo",
                                        "100",
                                        dtConstPaneles.getString("sCodigoPadre")));

        vLabels.addElement(new CColumna("C�digo",
                                        "100",
                                        dtConstPaneles.getString("sCodigo")));

        vLabels.addElement(new CColumna("Descripci�n",
                                        "250",
                                        dtConstPaneles.getString("sDescripcion")));

        if (dtConstPaneles.getString("sNomTabla").equals("SIVE_FACT_CONTRIB")) {
          vLabels.addElement(new CColumna("It_Masfaccont",
                                          "75",
                                          dtConstPaneles.getString(
              "sNomCheckExtra")));
        }
        else {
          vLabels.addElement(new CColumna("It_InfAdic",
                                          "75",
                                          dtConstPaneles.getString(
              "sNomCheckExtra")));
        }
        vLabels.addElement(new CColumna("It_Repet",
                                        "75",
                                        dtConstPaneles.getString(
            "sNomCheckExtra1")));
      }
      else {
        vLabels.addElement(new CColumna("Grupo",
                                        "100",
                                        dtConstPaneles.getString("sCodigoPadre")));

        vLabels.addElement(new CColumna("C�digo",
                                        "100",
                                        dtConstPaneles.getString("sCodigo")));

        if (dtConstPaneles.getString("sNomTabla").equals("SIVE_TIPO_BROTE")) {
          vLabels.addElement(new CColumna("Descripci�n",
                                          "400",
                                          dtConstPaneles.getString(
              "sDescripcion")));

        }
        else {
          vLabels.addElement(new CColumna("Descripci�n",
                                          "325",
                                          dtConstPaneles.getString(
              "sDescripcion")));

          if (dtConstPaneles.getString("sNomTabla").equals("SIVE_FACT_CONTRIB")) {
            vLabels.addElement(new CColumna("It_Masfaccont",
                                            "75",
                                            dtConstPaneles.getString(
                "sNomCheckExtra")));
          }
          else {
            vLabels.addElement(new CColumna("It_InfAdic",
                                            "75",
                                            dtConstPaneles.getString(
                "sNomCheckExtra")));
          }
        } //end de if es SIVE_TIPO_BROTES
      } //end if
      clmMantenimiento = new CListaMantenimiento(a,
                                                 vLabels,
                                                 vBotones,
                                                 this,
                                                 this,
                                                 250,
                                                 640);
      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del panel PanMant2
  public void jbInit() throws Exception {

    final String imgBuscar = "images/refrescar.gif";

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgBuscar);
    this.getApp().getLibImagenes().CargaImagenes();

    // Escuchadores de eventos

    PanMant2_actionAdapter actionAdapter = new PanMant2_actionAdapter(this);
    CListaMantItemAdapter2 itemAdapter = new CListaMantItemAdapter2(this);

    chckCod.setLabel("C�digo");
    chckCod.setCheckboxGroup(chkboxGrupo);
    chckCod.addItemListener(itemAdapter);
    chckCod.setState(true);
    chckDes.setLabel("Descripci�n");
    chckDes.setCheckboxGroup(chkboxGrupo);
    chckDes.setState(false);
    textControl1.setText("Tipo:");
    chckDes.addItemListener(itemAdapter);

    xYLayout1.setWidth(675);
    xYLayout1.setHeight(340);

    txtCod.setName("codigo");
    txtDes.setName("descripcion");

    txtCod.setVisible(true);
    txtDes.setVisible(false);

    btnBuscar.setActionCommand("buscar");
    btnBuscar.setLabel("Buscar");
    btnBuscar.addActionListener(actionAdapter);
    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBuscar));

    this.setLayout(xYLayout1);
    this.add(pnlSeleccion, new XYConstraints(67, 6, 629, 34));
    this.add(txtCod, new XYConstraints(28, 40, 312, -1));
    this.add(txtDes, new XYConstraints(28, 40, 312, -1));
//    this.add(txtDes, new XYConstraints(27, 67, 312, -1));
    this.add(chckCod, new XYConstraints(28, 70, 130, -1));
    this.add(chckDes, new XYConstraints(210, 70, 130, -1));
    this.add(btnBuscar, new XYConstraints(535, 58, 79, 24));
    this.add(clmMantenimiento, new XYConstraints(3, 95, 650, 230));
    this.add(textControl1, new XYConstraints(27, 6, 33, 33));

    // tool tips

    new CContextHelp("Obtener mantenimientos", btnBuscar);
  }

  public void Inicializar() {}

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;
    }
  }

  // gesti�n de los botones
  void btn_actionPerformed(ActionEvent e) {
    CListaValores clv = null;
    QueryTool qt = null;
    Vector v = null;

    if (e.getActionCommand().equals("buscar")) {
      clmMantenimiento.setPrimeraPagina(this.primeraPagina());
    }
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {

    // nombre del servlet
    final String servlet = "servlet/SrvQueryTool";

    // lista para el filtro

    Lista v = new Lista();
    Lista vFiltro = new Lista();

    Inicializar(CInicializar.ESPERA);

    try {
      QueryTool qt = new QueryTool();

      // centros de vacunacion
      qt.putName(dtConstPaneles.getString("sNomTabla"));

      // campos que se leen

      qt.putType(dtConstPaneles.getString("sCodigo"),
                 Integer.parseInt(dtConstPaneles.getString("sTipoCodigo")));
      qt.putType(dtConstPaneles.getString("sCodigoPadre"),
                 Integer.parseInt(dtConstPaneles.getString("sTipoCodigoPadre")));
      qt.putType(dtConstPaneles.getString("sDescripcion"),
                 Integer.parseInt(dtConstPaneles.getString("sTipoDescripcion")));
      qt.putType(dtConstPaneles.getString("sCodigo") + "||" +
                 dtConstPaneles.getString("sCodigoPadre"),
                 Integer.parseInt(dtConstPaneles.getString("sTipoDescripcion")));

      //si es SIVE_TIPO_BROTE q no obtenga el campo extra
      if (! (dtConstPaneles.getString("sNomTabla").equals("SIVE_TIPO_BROTE"))) {
        qt.putType(dtConstPaneles.getString("sNomCheckExtra"),
                   Integer.parseInt(dtConstPaneles.getString("sTipoCheckExtra")));
      }

      if (campoExtra) {
        qt.putType(dtConstPaneles.getString("sNomCheckExtra1"),
                   Integer.parseInt(dtConstPaneles.getString("sTipoCheckExtra1")));
      }

      // filtro de c�digo
      if (chckCod.getState()) {
        qt.putWhereType(dtConstPaneles.getString("sCodigo"),
                        Integer.parseInt(dtConstPaneles.getString("sTipoCodigo")));
        qt.putWhereValue(dtConstPaneles.getString("sCodigo"),
                         txtCod.getText().trim() + "%");
        qt.putOperator(dtConstPaneles.getString("sCodigo"), "like");

        if (pnlSeleccion.getDatos() != null) {
          qt.putWhereType(dtConstPaneles.getString("sCodigoPadre"),
                          Integer.parseInt(dtConstPaneles.getString(
              "sTipoCodigoPadre")));
          qt.putWhereValue(dtConstPaneles.getString("sCodigoPadre"),
                           pnlSeleccion.getDatos().getString(
              dtConstPaneles.getString("sCodigoPadre")
              ));
          qt.putOperator(dtConstPaneles.getString("sCodigoPadre"), "=");
        } // del if
      }
      else {
        // filtro de descripci�n
        qt.putWhereType(dtConstPaneles.getString("sDescripcion"),
                        Integer.parseInt(dtConstPaneles.getString(
            "sTipoDescripcion")));
        qt.putWhereValue(dtConstPaneles.getString("sDescripcion"),
                         txtDes.getText().trim() + "%");
        qt.putOperator(dtConstPaneles.getString("sDescripcion"), "like");
        if (pnlSeleccion.getDatos() != null) {
          qt.putWhereType(dtConstPaneles.getString("sCodigoPadre"),
                          Integer.parseInt(dtConstPaneles.getString(
              "sTipoCodigoPadre")));
          qt.putWhereValue(dtConstPaneles.getString("sCodigoPadre"),
                           pnlSeleccion.getDatos().getString(
              dtConstPaneles.getString("sCodigoPadre")
              ));
          qt.putOperator(dtConstPaneles.getString("sCodigoPadre"), "=");
        } // del if
      }

      qt.addOrderField(dtConstPaneles.getString("sCodigoPadre"));
      String s = dtConstPaneles.getString("sCodigo");
      qt.addOrderField(s);

      /*          vFiltro.setTrama(dtConstPaneles.getString("sCodigo")+"||"+
           dtConstPaneles.getString("sCodigoPadre"),"");*/

      vFiltro.addElement(qt);

      // consulta el servidor
      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      // debug
      /*                    SrvQueryTool srv = new SrvQueryTool();
                          // par�metros jdbc
           srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sip_desa",
                                 "sip_desa");
                          srv.doDebug(2, vFiltro);*/
      v = (Lista)this.getApp().getStub().doPost(1, vFiltro);
      if (v.size() == 0) {
        this.getApp().showAdvise("Registro no encontrado");
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    Inicializar(CInicializar.NORMAL);

    return v;
  }

  // solicita la siguiente trama de datos
  public Lista siguientePagina() {
    // nombre del servlet
    final String servlet = "servlet/SrvQueryTool";

    // lista para el filtro
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();

    Inicializar(CInicializar.ESPERA);

    // realiza la consulta al servlet
    try {
      QueryTool qt = new QueryTool();

      // tabla de familias de productos
      qt.putName(dtConstPaneles.getString("sNomTabla"));

      // campos que se leen
      qt.putType(dtConstPaneles.getString("sCodigo"),
                 Integer.parseInt(dtConstPaneles.getString("sTipoCodigo")));
      qt.putType(dtConstPaneles.getString("sCodigoPadre"),
                 Integer.parseInt(dtConstPaneles.getString("sTipoCodigoPadre")));
      qt.putType(dtConstPaneles.getString("sDescripcion"),
                 Integer.parseInt(dtConstPaneles.getString("sTipoDescripcion")));
      qt.putType(dtConstPaneles.getString("sCodigo") + "||" +
                 dtConstPaneles.getString("sCodigoPadre"),
                 Integer.parseInt(dtConstPaneles.getString("sTipoDescripcion")));

      //si es SIVE_TIPO_BROTE q no obtenga el campo extra
      if (! (dtConstPaneles.getString("sNomTabla").equals("SIVE_TIPO_BROTE"))) {
        qt.putType(dtConstPaneles.getString("sNomCheckExtra"),
                   Integer.parseInt(dtConstPaneles.getString("sTipoCheckExtra")));
      }

      if (campoExtra) {
        qt.putType(dtConstPaneles.getString("sNomCheckExtra1"),
                   Integer.parseInt(dtConstPaneles.getString("sTipoCheckExtra1")));
      }

      // filtro de c�digo
      if (chckCod.getState()) {
        qt.putWhereType(dtConstPaneles.getString("sCodigo"),
                        Integer.parseInt(dtConstPaneles.getString("sTipoCodigo")));
        qt.putWhereValue(dtConstPaneles.getString("sCodigo"),
                         txtCod.getText().trim() + "%");
        qt.putOperator(dtConstPaneles.getString("sCodigo"), "like");
        if (pnlSeleccion.getDatos() != null) {
          qt.putWhereType(dtConstPaneles.getString("sCodigoPadre"),
                          Integer.parseInt(dtConstPaneles.getString(
              "sTipoCodigoPadre")));
          qt.putWhereValue(dtConstPaneles.getString("sCodigoPadre"),
                           pnlSeleccion.getDatos().getString(
              dtConstPaneles.getString("sCodigoPadre")
              ));
          qt.putOperator(dtConstPaneles.getString("sCodigoPadre"), "=");
        } // de lif
      }
      else {
        // filtro de descripci�n
        qt.putWhereType(dtConstPaneles.getString("sDescripcion"),
                        Integer.parseInt(dtConstPaneles.getString(
            "sTipoDescripcion")));
        qt.putWhereValue(dtConstPaneles.getString("sDescripcion"),
                         txtDes.getText().trim() + "%");
        qt.putOperator(dtConstPaneles.getString("sDescripcion"), "like");
        if (pnlSeleccion.getDatos() != null) {
          qt.putWhereType(dtConstPaneles.getString("sCodigoPadre"),
                          Integer.parseInt(dtConstPaneles.getString(
              "sTipoCodigoPadre")));
          qt.putWhereValue(dtConstPaneles.getString("sCodigoPadre"),
                           pnlSeleccion.getDatos().getString(
              dtConstPaneles.getString("sCodigoPadre")
              ));
          qt.putOperator(dtConstPaneles.getString("sCodigoPadre"), "=");
        } // del if
      }
      qt.addOrderField(dtConstPaneles.getString("sCodigoPadre"));
      String s = dtConstPaneles.getString("sCodigo");
      qt.addOrderField(s);

      // fija el filtro
      vFiltro.addElement(qt);
      // fija la trama
//      vFiltro.setTrama(this.clmMantenimiento.getLista().getTrama());

      /*                    // debug
                          SrvQueryTool srv = new SrvQueryTool();
                          // par�metros jdbc
           srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sip_desa",
                                 "sip_desa");
                          srv.doDebug(2, vFiltro);    */

      this.getApp().getStub().setUrl(servlet);
      vPetic = (Lista)this.getApp().getStub().doPost(2, vFiltro);

      // error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    // la lista debe estar cread

    if (vPetic == null) {
      vPetic = new Lista();

    }
    Inicializar(CInicializar.NORMAL);

    return vPetic;
  }

  // operaciones de la botonera
  public void realizaOperacion(int j) {

    Lista lisPet = this.clmMantenimiento.getLista();
    DiaMant2 dm = null;
    Data dPetMod = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:
        dm = new DiaMant2(this.getApp(), 0, null, dtConstPaneles);
        dm.show();
        if (dm.bAceptar) {
          dPetMod = dm.devuelveData();
          clmMantenimiento.getLista().addElement(dPetMod);
          // No la vaciamos, porque este m�todo lo hace ya..
          //clmMantenimiento.setPrimeraPagina(clmMantenimiento.getLista());
          clmMantenimiento.setPrimeraPagina(primeraPagina());
        }
        break;
      case MODIFICACION:
        dPetMod = clmMantenimiento.getSelected();
        if (dPetMod != null) {
          int ind = clmMantenimiento.getSelectedIndex();

          // Ahora se hace una petici�n simple, para obtener la descripci�n
          // desde la tabla padre....

          QueryTool qtTemp = new QueryTool();
          Lista Filtro = new Lista();
          Lista Resultado = new Lista();
          try {
            qtTemp.putName(dtConstPaneles.getString("sNomTablaPadre"));
            qtTemp.putType(dtConstPaneles.getString("sDescripcionPadre"),
                           Integer.parseInt(dtConstPaneles.getString(
                "sTipoDescripcionPadre")));

            qtTemp.putWhereType(dtConstPaneles.getString("sCodigoPadre"),
                                Integer.parseInt(dtConstPaneles.getString(
                "sTipoCodigoPadre")));
            qtTemp.putWhereValue(dtConstPaneles.getString("sCodigoPadre"),
                                 dPetMod.getString(dtConstPaneles.getString(
                "sCodigoPadre")));
            qtTemp.putOperator(dtConstPaneles.getString("sCodigoPadre"), "=");
            Filtro.addElement(qtTemp);

            this.getApp().getStub().setUrl("servlet/SrvQueryTool");
            Resultado = (Lista)this.getApp().getStub().doPost(1, Filtro);
            if (Resultado != null) {
              dPetMod.put(dtConstPaneles.getString("sDescripcionPadre"),
                          ( (Data) Resultado.elementAt(0)).getString(
                  dtConstPaneles.getString("sDescripcionPadre")));
            }
          }
          catch (Exception e) {
            this.getApp().trazaLog(e);
            this.getApp().showError(e.getMessage());
          }
          // Seguimos....
          dm = new DiaMant2(this.getApp(), 1, dPetMod, dtConstPaneles);
          dm.show();
          if (dm.bAceptar) {
            dPetMod = dm.devuelveData();
            clmMantenimiento.getLista().setElementAt(dPetMod, ind);
            // No la vaciamos, porque este m�todo lo hace ya..
            clmMantenimiento.setPrimeraPagina(clmMantenimiento.getLista());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dPetMod = clmMantenimiento.getSelected();
        if (dPetMod != null) {
          int ind = clmMantenimiento.getSelectedIndex();
          // Famos�sima consulta, pa coger las descripciones...
          QueryTool qtTemp = new QueryTool();
          Lista Filtro = new Lista();
          Lista Resultado = new Lista();
          try {
            qtTemp.putName(dtConstPaneles.getString("sNomTablaPadre"));
            qtTemp.putType(dtConstPaneles.getString("sDescripcionPadre"),
                           Integer.parseInt(dtConstPaneles.getString(
                "sTipoDescripcionPadre")));

            qtTemp.putWhereType(dtConstPaneles.getString("sCodigoPadre"),
                                Integer.parseInt(dtConstPaneles.getString(
                "sTipoCodigoPadre")));
            qtTemp.putWhereValue(dtConstPaneles.getString("sCodigoPadre"),
                                 dPetMod.getString(dtConstPaneles.getString(
                "sCodigoPadre")));
            qtTemp.putOperator(dtConstPaneles.getString("sCodigoPadre"), "=");
            Filtro.addElement(qtTemp);

            this.getApp().getStub().setUrl("servlet/SrvQueryTool");
            Resultado = (Lista)this.getApp().getStub().doPost(1, Filtro);
            if (Resultado != null) {
              dPetMod.put(dtConstPaneles.getString("sDescripcionPadre"),
                          ( (Data) Resultado.elementAt(0)).getString(
                  dtConstPaneles.getString("sDescripcionPadre")));
            }
          }
          catch (Exception e) {
            this.getApp().trazaLog(e);
            this.getApp().showError(e.getMessage());
          }
          // Seguimos....
          dm = new DiaMant2(this.getApp(), 2, dPetMod, dtConstPaneles);
          dm.show();
          if (dm.bAceptar()) {
            clmMantenimiento.getLista().removeElementAt(ind);
            clmMantenimiento.setPrimeraPagina(clmMantenimiento.getLista());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;
    }
  }

  void chkItemStateChanged(ItemEvent e) {
    if (e.getItem().equals("C�digo")) {
      txtDes.setText("");
      txtCod.setVisible(true);
      txtCod.setText(txtDes.getText().trim().toUpperCase());
      txtDes.setVisible(false);
    }
    else {
      txtCod.setVisible(false);
      txtCod.setText("");
      txtDes.setVisible(true);
      txtDes.setText(txtCod.getText().trim());
    }
    doLayout();
  }
}

// botones de centro, almac�n y buscar
class PanMant2_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanMant2 adaptee;
  ActionEvent e;

  PanMant2_actionAdapter(PanMant2 adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}

// control de cambio de buscar por c�digo/descripci�n
class CListaMantItemAdapter2
    implements java.awt.event.ItemListener {
  PanMant2 adaptee;

  CListaMantItemAdapter2(PanMant2 adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkItemStateChanged(e);
  }
}
