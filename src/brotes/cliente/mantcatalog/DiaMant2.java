package brotes.cliente.mantcatalog;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CCodigo;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CPnlCodigo;
import capp2.CTexto;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

//import gesvac.cliente.sipproducto.PanProdu;

/**
 * Di�logo a trav�s del cu�l se insertan/modifican/borran datos
 * en tablas de mantenimiento. En este caso, son di�logos que
 * conectan con una tabla padre.
 * @autor ARS
 * @version 1.0
 */

public class DiaMant2
    extends CDialog
    implements CInicializar {

  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  // parametros pasados al di�logo
  public Data dtDev = null;
  // par�metros configuraci�n de ventana.
  private Data dtParam = null;

  private String nomTabla = "";
  private String nomCampoCod = "";
  private String nomCampoDesc = "";
  private int tipoCampoCod = 0;
  private int tipoCampoDesc = 0;
  private String nomCampoExt = "";
  private int tipoCampoExt = 0;
  private String nomCampoExt1 = "";
  private int tipoCampoExt1 = 0;
  // Para la tabla padre
  private String nomTablaPadre = "";
  private String nomCodPadre = "";
  private String nomDescPadre = "";
  private int tipoCodPadre = 0;
  private int tipoDescPadre = 0;
  private boolean bExtra = false;

  public Data dtDevuelto = new Data();

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xYLayout1 = new XYLayout();

  Label lblCod = new Label();
  Label lblDes = new Label();
  Label label1 = new Label();
  CCodigo txtCodigo = null;
  CTexto txtDescripcion = null;
  Checkbox chkPres = new Checkbox();
  Checkbox chkPresOpc = new Checkbox();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  CPnlCodigo pnlCentro = null;

  // Escuchadores de eventos...

  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  // constructor del di�logo DiaMant2
  public DiaMant2(CApp a, int modoop, Data dt, Data parametros) {

    super(a);

    try {
      modoOperacion = modoop;
      dtDev = dt;
      dtParam = parametros;
      // longitud de los campos de texto...
      txtCodigo = new CCodigo(Integer.parseInt(dtParam.getString(
          "sLongitudCodigo")));
      txtDescripcion = new CTexto(Integer.parseInt(dtParam.getString(
          "sLongitudDescripcion")));
      bExtra = Boolean.valueOf(dtParam.getString("bCheckExtra")).booleanValue();

      // Cogemos los par�metros de la tabla
      nomTabla = dtParam.getString("sNomTabla");
      nomCampoCod = dtParam.getString("sCodigo");
      nomCampoDesc = dtParam.getString("sDescripcion");
      tipoCampoCod = Integer.parseInt(dtParam.getString("sTipoCodigo"));
      tipoCampoDesc = Integer.parseInt(dtParam.getString("sTipoDescripcion"));
      if (! (nomTabla.equals("SIVE_TIPO_BROTE"))) {
        nomCampoExt = dtParam.getString("sNomCheckExtra");
        tipoCampoExt = Integer.parseInt(dtParam.getString("sTipoCheckExtra"));
      }
      if (bExtra) {
        nomCampoExt1 = dtParam.getString("sNomCheckExtra1");
        tipoCampoExt1 = Integer.parseInt(dtParam.getString("sTipoCheckExtra1"));
      }

      // Pues hacemos lo mismo, de la tabla padre
      nomTablaPadre = dtParam.getString("sNomTablaPadre");
      nomCodPadre = dtParam.getString("sCodigoPadre");
      nomDescPadre = dtParam.getString("sDescripcionPadre");
      tipoCodPadre = Integer.parseInt(dtParam.getString("sTipoCodigoPadre"));
      tipoDescPadre = Integer.parseInt(dtParam.getString(
          "sTipoDescripcionPadre"));

      QueryTool qtCentroVac = new QueryTool();
      // Tabla
      qtCentroVac.putName(nomTablaPadre);
      // peticiones
      qtCentroVac.putType(nomCodPadre, tipoCodPadre);
      qtCentroVac.putType(nomDescPadre, tipoDescPadre);
      // panel
      pnlCentro = new CPnlCodigo(a,
                                 this,
                                 qtCentroVac,
                                 nomCodPadre,
                                 nomDescPadre,
                                 true,
                                 "Tipo",
                                 "Tipo");
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del dialogo
  public void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xYLayout1);
    this.setSize(400, 260);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    txtCodigo.setBackground(new Color(250, 250, 150));
    txtCodigo.setText("");
    lblDes.setText("Descripci�n");
    lblCod.setText("C�digo");
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    label1.setText("Tipo:");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));
    txtDescripcion.setBackground(new Color(250, 250, 150));
    txtDescripcion.setText("");
    if (dtParam.getString("sNomTabla").equals("SIVE_FACT_CONTRIB")) {
      chkPres.setLabel("Mas factores contribuyentes");
    }
    else {
      chkPres.setLabel("Informacion adicional");
    }
    chkPresOpc.setLabel("Permitir repetici�n");

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xYLayout1.setHeight(250);
    xYLayout1.setWidth(401);

    this.add(pnlCentro, new XYConstraints(50, 12, 332, 40));
    this.add(lblCod, new XYConstraints(23, 60, 62, -1));
    this.add(txtCodigo, new XYConstraints(131, 60, 142, -1));
    this.add(lblDes, new XYConstraints(23, 90, 76, -1));
    this.add(txtDescripcion, new XYConstraints(131, 90, 246, -1));

    this.add(chkPres, new XYConstraints(23, 120, 201, 24));
    this.add(chkPresOpc, new XYConstraints(23, 150, 201, 24));

    this.add(btnAceptar, new XYConstraints(187, 190, 89, -1));
    this.add(btnCancelar, new XYConstraints(281, 190, 89, -1));
    this.add(label1, new XYConstraints(8, 12, 37, 40));
    //m�todo temporal para poder ver los datos en el panel
    //pasados a trav�s de la applet
    verDatos();
    if (nomTabla.equals("SIVE_TIPO_BROTE")) {
      chkPres.setVisible(false);
    }
    chkPresOpc.setVisible(bExtra);

    // ponemos el t�tulo
    switch (modoOperacion) {
      case modoALTA:
        setTitle(dtParam.getString("sTituloDialogoAlta"));
        break;
      case modoMODIFICACION:
        this.setTitle(dtParam.getString("sTituloDialogoModif"));
        break;
      case modoBAJA:
        this.setTitle(dtParam.getString("sTituloDialogoBaja"));
        break;
    }
  }

  // Pone los campos de texto en el estado que les corresponda
  // segun el modo.

  public void verDatos() {
    switch (modoOperacion) {
      case modoALTA:
        pnlCentro.setEnabled(true);
        txtCodigo.setEnabled(true);
        txtDescripcion.setEnabled(true);
        if (! (nomTabla.equals("SIVE_TIPO_BROTE"))) {
          chkPres.setEnabled(true);
        }
        if (bExtra) {
          chkPresOpc.setEnabled(true);
        }
        break;

      case modoMODIFICACION:
        pnlCentro.setEnabled(false);
        txtCodigo.setEnabled(false);
        txtDescripcion.setEnabled(true);
        // datos
        Data dtMeteDatos = new Data();
        dtMeteDatos.put(nomCodPadre, dtDev.getString(nomCodPadre));
        dtMeteDatos.put(nomDescPadre, dtDev.getString(nomDescPadre));
        pnlCentro.setCodigo(dtMeteDatos);
        txtCodigo.setText(dtDev.getString(nomCampoCod));
        txtDescripcion.setText(dtDev.getString(nomCampoDesc));
        chkPres.setEnabled(true);
        if (! (nomTabla.equals("SIVE_TIPO_BROTE"))) {
          if (dtDev.getString(nomCampoExt).equals("S")) {
            chkPres.setState(true);
          }
          else if (dtDev.getString(nomCampoExt).equals("N")) {
            chkPres.setState(false);
          }
        }

        if (bExtra) {
          chkPresOpc.setEnabled(true);
          if (dtDev.getString(nomCampoExt1).equals("S")) {
            chkPresOpc.setState(true);
          }
          else if (dtDev.getString(nomCampoExt1).equals("N")) {
            chkPresOpc.setState(false);
          }
        }
        break;

        // s�lo habilitado aceptar y cancelar
      case modoBAJA:
        pnlCentro.setEnabled(false);
        txtCodigo.setEnabled(false);
        txtDescripcion.setEnabled(false);
        // datos
        dtMeteDatos = new Data();
        dtMeteDatos.put(nomCodPadre, dtDev.getString(nomCodPadre));
        dtMeteDatos.put(nomDescPadre, dtDev.getString(nomDescPadre));
        pnlCentro.setCodigo(dtMeteDatos);
        txtCodigo.setText(dtDev.getString(nomCampoCod));
        txtDescripcion.setText(dtDev.getString(nomCampoDesc));
        chkPres.setEnabled(false);
        if (! (nomTabla.equals("SIVE_TIPO_BROTE"))) {
          if (dtDev.getString(nomCampoExt).equals("S")) {
            chkPres.setState(true);
          }
          else if (dtDev.getString(nomCampoExt).equals("N")) {
            chkPres.setState(false);
          }
        }

        if (bExtra) {
          chkPresOpc.setEnabled(false);
          if (dtDev.getString(nomCampoExt1).equals("S")) {
            chkPresOpc.setState(true);
          }
          else if (dtDev.getString(nomCampoExt1).equals("N")) {
            chkPresOpc.setState(false);
          }
        }
        break;
    }
  }

  // Pone el di�logo en modo de espera

  public void Inicializar(int modo) {
    switch (modo) {

      // componente producto accediendo a base de datos
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // componente en el modo requerido
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoALTA:
            this.setEnabled(true);
            break;

          case modoMODIFICACION:
            pnlCentro.setEnabled(false);
            txtCodigo.setEnabled(false);
            txtDescripcion.setEnabled(true);
            chkPres.setEnabled(true);
            if (bExtra) {
              chkPresOpc.setEnabled(true);
            }
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;

            // s�lo habilitado salir y grabar
          case modoBAJA:
            this.setEnabled(false);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  }

  private boolean isDataValid() {
    if ( (txtCodigo.getText().trim().length() == 0) ||
        (txtDescripcion.getText().trim().length() == 0) ||
        (pnlCentro.getDatos() == null)) {
      getApp().showAdvise("Debe completar todos los campos obligatorios");
      return false;
    }
    else {
      return true;
    }
  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {

    final String servlet = "servlet/SrvQueryTool";
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();
    boolean bTieneProducto = false;

    if (e.getActionCommand().equals("Aceptar")) {

      // A ver si todos los campos est�n rellenos....
      if (isDataValid()) {

        switch (modoOperacion) {

          // ----- NUEVO -----------

          case 0:
            Inicializar(CInicializar.ESPERA);

            // realiza la consulta al servlet
            try {
              QueryTool qt = new QueryTool();

              // tabla de familias de productos
              qt.putName(nomTabla);

              // campos que se escriben
              qt.putType(nomCodPadre, tipoCodPadre);
//                    qt.putType(nomDescPadre,tipoDescPadre);
              qt.putType(nomCampoCod, tipoCampoCod);
              qt.putType(nomCampoDesc, tipoCampoDesc);

              // Valores de los campos
              qt.putValue(nomCodPadre,
                          pnlCentro.getDatos().getString(nomCodPadre));
//                    qt.putValue(nomDescPadre,pnlCentro.getDatos().get(nomDescPadre));
              qt.putValue(nomCampoCod, txtCodigo.getText().trim());
              qt.putValue(nomCampoDesc, txtDescripcion.getText().trim());

              // Valores de Data a devolver
              dtDevuelto.put(nomCodPadre,
                             pnlCentro.getDatos().getString(nomCodPadre));
              dtDevuelto.put(nomDescPadre,
                             pnlCentro.getDatos().getString(nomDescPadre));
              dtDevuelto.put(nomCampoCod, txtCodigo.getText().trim());
              dtDevuelto.put(nomCampoDesc, txtDescripcion.getText().trim());

              if (! (nomTabla.equals("SIVE_TIPO_BROTE"))) {
                qt.putType(nomCampoExt, tipoCampoExt);
                if (chkPres.getState()) {
                  qt.putValue(nomCampoExt, "S");
                  dtDevuelto.put(nomCampoExt, "S");
                }
                else {
                  qt.putValue(nomCampoExt, "N");
                  dtDevuelto.put(nomCampoExt, "N");
                }
              }

              if (bExtra) {
                qt.putType(nomCampoExt1, tipoCampoExt1);
                if (chkPresOpc.getState()) {
                  qt.putValue(nomCampoExt1, "S");
                  dtDevuelto.put(nomCampoExt1, "S");
                }
                else {
                  qt.putValue(nomCampoExt1, "N");
                  dtDevuelto.put(nomCampoExt1, "N");
                }
              }

              vFiltro.addElement(qt);
              this.getApp().getStub().setUrl(servlet);
              vPetic = (Lista)this.getApp().getStub().doPost(3, vFiltro);
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
//                    this.getApp().trazaLog(ex);
              String sMsg = "";
              if (ex.toString().indexOf("ORA-00001") != -1) {
                sMsg = "Clave repetida; ";
              }
              if (ex.toString().indexOf("ORA-20002") != -1) {
                sMsg += "Registro padre no encontrado; ";
              }
              if (ex.toString().indexOf("ORA-01401") != -1) {
                sMsg += "Valor demasiado largo para columna; ";
              }
              if (ex.toString().indexOf("ORA-01400") != -1) {
                sMsg += "Insertando valor a nulo; ";
              }
              if (ex.toString().indexOf("ORA-02291") != -1) {
                sMsg = "Clave padre no encontrada.";
              }
              this.getApp().showError(sMsg);
              bAceptar = false;
            }
            Inicializar(CInicializar.NORMAL);
            break;
          case 1:

            // ---- MODIFICAR ----
            Inicializar(CInicializar.ESPERA);
            try {
              QueryTool qt = new QueryTool();

              // tabla de familias de productos
              qt.putName(nomTabla);

              // campos que se leen

              qt.putType(nomCampoDesc, tipoCampoDesc);
              qt.putValue(nomCampoDesc, txtDescripcion.getText().trim());
              dtDevuelto.put(nomCampoDesc, txtDescripcion.getText().trim());

              if (! (nomTabla.equals("SIVE_TIPO_BROTE"))) {
                qt.putType(nomCampoExt, tipoCampoExt);
                if (chkPres.getState()) {
                  qt.putValue(nomCampoExt, "S");
                  dtDevuelto.put(nomCampoExt, "S");
                }
                else {
                  qt.putValue(nomCampoExt, "N");
                  dtDevuelto.put(nomCampoExt, "N");
                }
              }

              if (bExtra) {
                qt.putType(nomCampoExt1, tipoCampoExt1);
                if (chkPresOpc.getState()) {
                  qt.putValue(nomCampoExt1, "S");
                  dtDevuelto.put(nomCampoExt1, "S");
                }
                else {
                  qt.putValue(nomCampoExt1, "N");
                  dtDevuelto.put(nomCampoExt1, "N");
                }
              }

              qt.putWhereType(nomCampoCod, tipoCampoCod);
              qt.putWhereValue(nomCampoCod, dtDev.getString(nomCampoCod));
              qt.putOperator(nomCampoCod, "=");

              qt.putWhereType(nomCodPadre, tipoCodPadre);
              qt.putWhereValue(nomCodPadre, dtDev.getString(nomCodPadre));
              qt.putOperator(nomCodPadre, "=");

              dtDevuelto.put(nomCampoCod, dtDev.getString(nomCampoCod));
              dtDevuelto.put(nomCodPadre, dtDev.getString(nomCodPadre));

              vFiltro.addElement(qt);

              /*                    // debug
                                  SrvQueryTool srv = new SrvQueryTool();
                                  // par�metros jdbc
                   srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                   "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                         "sip_desa",
                                         "sip_desa");
                                  srv.doDebug(4, vFiltro);  */

              this.getApp().getStub().setUrl(servlet);
              vPetic = (Lista)this.getApp().getStub().doPost(4, vFiltro);
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
//                    this.getApp().trazaLog(ex);
              String sMsg = "";
              if (ex.toString().indexOf("ORA-00001") != -1) {
                sMsg = "Clave repetida; ";
              }
              if (ex.toString().indexOf("ORA-20002") != -1) {
                sMsg += "Registro padre no encontrado; ";
              }
              if (ex.toString().indexOf("ORA-01401") != -1) {
                sMsg += "Valor demasiado largo para columna; ";
              }
              if (ex.toString().indexOf("ORA-01400") != -1) {
                sMsg += "Insertando valor a nulo; ";
              }
              if (ex.toString().indexOf("ORA-02291") != -1) {
                sMsg = "Clave padre no encontrada.";
              }
              this.getApp().showError(sMsg);
            }
            Inicializar(CInicializar.NORMAL);
            break;
          case 2:

            // ---- BORRAR -------
            Inicializar(CInicializar.ESPERA);
            try {
              QueryTool qt = new QueryTool();

              if (nomTabla.equals("SIP_GRUPO_PROD")) {
                qt.putName("SIP_PRODUCTO");
                qt.putType("CD_PRODUCTO", QueryTool.STRING);
                qt.putWhereType("CD_GPROD", QueryTool.STRING);
                qt.putWhereValue("CD_GPROD", dtDev.getString(nomCampoCod));
                qt.putOperator("CD_GPROD", "=");
                vFiltro.addElement(qt);

                this.getApp().getStub().setUrl(servlet);
                vPetic = (Lista)this.getApp().getStub().doPost(1, vFiltro);
                if (vPetic.size() > 0) {
                  bTieneProducto = true;

                }
                vPetic = null;
                vPetic = new Lista();
                vFiltro = null;
                vFiltro = new Lista();
                qt = null;
                qt = new QueryTool();
              }

              if (!bTieneProducto) {
                // tabla de familias de productos
                qt.putName(nomTabla);

                // campos que se seleccionan
                qt.putWhereType(nomCampoCod, tipoCampoCod);
                qt.putWhereValue(nomCampoCod, dtDev.getString(nomCampoCod));
                qt.putOperator(nomCampoCod, "=");

                qt.putWhereType(nomCodPadre, tipoCodPadre);
                qt.putWhereValue(nomCodPadre, dtDev.getString(nomCodPadre));
                qt.putOperator(nomCodPadre, "=");

                vFiltro.addElement(qt);

                this.getApp().getStub().setUrl(servlet);
                vPetic = (Lista)this.getApp().getStub().doPost(5, vFiltro);
                bAceptar = true;
                dispose();
                // error en el servlet
              }
              else {
                this.getApp().showAdvise(
                    "Este grupo de productos contiene productos.");
              }
            }
            catch (Exception ex) {
              //this.getApp().trazaLog(ex);
              String sMsg = "";
              if (ex.toString().indexOf("ORA-00001") != -1) {
                sMsg = "Clave repetida; ";
              }
              if (ex.toString().indexOf("ORA-20002") != -1) {
                sMsg += "Registro padre no encontrado; ";
              }
              if (ex.toString().indexOf("ORA-01401") != -1) {
                sMsg += "Valor demasiado largo para columna; ";
              }
              if (ex.toString().indexOf("ORA-01400") != -1) {
                sMsg += "Insertando valor a nulo; ";
              }
              if (ex.toString().indexOf("ORA-02291") != -1) {
                sMsg = "Clave padre no encontrada.";
              }
              if (ex.toString().indexOf("ORA-02292") != -1) {
                sMsg =
                    "Registro hijo encontrado. No se puede borrar este elemento";
              }
              if (ex.toString().indexOf("ORA-04088") != -1) {
                sMsg = "No se puede borrar este elemento";
              }
              this.getApp().showError(sMsg);
            }
            Inicializar(CInicializar.NORMAL);
            break;
        }
      } // del isDataValid()
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  }

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

  // Devuelve los datos seleccionados en el di�logo.
  public Data devuelveData() {
    return dtDevuelto;
  }

// botones de aceptar y cancelar.

  class DialogActionAdapter
      implements java.awt.event.ActionListener, Runnable {
    DiaMant2 adaptee;
    ActionEvent e;

    DialogActionAdapter(DiaMant2 adaptee) {
      this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
      this.e = e;
      Thread th = new Thread(this);
      th.start();
    }

    public void run() {
      adaptee.btn_actionPerformed(e);
    }
  }

}
