package brotes.cliente.mantcatalog;

import java.util.Vector;

import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.CheckboxControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CCodigo;
import capp2.CColumna;
import capp2.CContextHelp;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CListaValores;
import capp2.CPanel;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.StubSrvBD;

/**
 * Panel a trav�s del que se realizan los mantenimientos
 * de vacunaci�n.
 * @autor ARS
 * @version 1.0
 */
public class PanMant
    extends CPanel
    implements CInicializar, CFiltro { //CPanel

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  XYLayout xYLayout1 = new XYLayout();
  CCodigo txtCod = new CCodigo();
  TextField txtDes = new TextField();
  CListaMantenimiento clmMantenimiento = null;
  ButtonControl btnBuscar = new ButtonControl();
  CheckboxControl chckCod = new CheckboxControl();
  CheckboxControl chckDes = new CheckboxControl();
  CheckboxGroup chkboxGrupo = new CheckboxGroup();

  // datos
  private Data dtCentro = null;

  // filtro
  private Data dtFiltro = null;

  // tipos de tabla disponibles...

  public static final int mantORIGEN_MUESTRA = 10;
  public static final int mantSINTOMAS = 20;
  public static final int mantTALIMENTOS = 30;
  public static final int mantTIMPLICACION = 40;
  public static final int mantMCOMERCALI = 50;
  public static final int mantTRATPREVIOALI = 60;
  public static final int mantFINGERIRALI = 70;
  public static final int mantLCONTAMIALI = 80;
  public static final int mantLPREPALI = 90;
  public static final int mantLCONSUMOALI = 100;
  public static final int mantMTVIAJEALI = 110;
  public static final int mantGRUPBROTES = 120;

  // variables necesarias para adaptar dialogos y paneles a
  // nuestra tabla...

  Data dtConstPaneles = new Data();

  int tab = 0;

  boolean campoExtra = true; // Variables para este panel..
  boolean campoNumExtra = true;

  // constructor del panel PanMant
  public PanMant(CApp a, int tabla) {

    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    tab = tabla;

    switch (tabla) {
      case PanMant.mantORIGEN_MUESTRA:
        dtConstPaneles.put("sTituloDialogoAlta",
                           "Crear nuevo origen de muestra");
        dtConstPaneles.put("sTituloDialogoModif", "Modificar origen de muestra");
        dtConstPaneles.put("sTituloDialogoBaja", "Borrar origen de muestra");
        dtConstPaneles.put("sNomTabla", "SIVE_ORIGEN_MUESTRA");
        dtConstPaneles.put("sNomCampoCod", "CD_MUESTRA");
        dtConstPaneles.put("sTipoCampoCod", QueryTool.STRING + "");
        dtConstPaneles.put("sNomCampoDes", "DS_MUESTRA");
        dtConstPaneles.put("sTipoCampoDes", QueryTool.STRING + "");
        dtConstPaneles.put("bCampoExtra", "false");
        dtConstPaneles.put("bNumExtra", "false");
        dtConstPaneles.put("nLongCod", "2");
        dtConstPaneles.put("nLongDesc", "30");
        campoExtra = false; // Variable para este panel
        campoNumExtra = false;
        break;

      case PanMant.mantSINTOMAS:
        dtConstPaneles.put("sTituloDialogoAlta", "A�adir nuevo sintoma");
        dtConstPaneles.put("sTituloDialogoModif", "Modificar sintoma");
        dtConstPaneles.put("sTituloDialogoBaja", "Borrar sintoma");
        dtConstPaneles.put("sNomTabla", "SIVE_SINTOMAS");
        dtConstPaneles.put("sNomCampoCod", "CD_SINTOMA");
        dtConstPaneles.put("sTipoCampoCod", QueryTool.STRING + "");
        dtConstPaneles.put("sNomCampoDes", "DS_SINTOMA");
        dtConstPaneles.put("sTipoCampoDes", QueryTool.STRING + "");
        dtConstPaneles.put("sNomCheckExtra", "IT_MASINF");
        dtConstPaneles.put("sTipoCheckExtra", QueryTool.STRING + "");
        dtConstPaneles.put("bCheckExtra", "true");
        dtConstPaneles.put("bNumExtra", "false");
        dtConstPaneles.put("nLongCod", "4");
        dtConstPaneles.put("nLongDesc", "40");
        campoExtra = true; // Variable en este panel
        campoNumExtra = false;
        break;

      case PanMant.mantTALIMENTOS:
        dtConstPaneles.put("sTituloDialogoAlta", "Crear nuevo tipo de alimento");
        dtConstPaneles.put("sTituloDialogoModif", "Modificar tipo de alimento");
        dtConstPaneles.put("sTituloDialogoBaja", "Borrar tipo de alimento");
        dtConstPaneles.put("sNomTabla", "SIVE_TALIMENTO");
        dtConstPaneles.put("sNomCampoCod", "CD_TALIMENTO");
        dtConstPaneles.put("sTipoCampoCod", QueryTool.STRING + "");
        dtConstPaneles.put("sNomCampoDes", "DS_TALIMENTO");
        dtConstPaneles.put("sTipoCampoDes", QueryTool.STRING + "");
        dtConstPaneles.put("bCampoExtra", "false");
        dtConstPaneles.put("bNumExtra", "false");
        dtConstPaneles.put("nLongCod", "6");
        dtConstPaneles.put("nLongDesc", "40");
        campoExtra = false; // Variable para este panel
        campoNumExtra = false;
        break;

      case PanMant.mantTIMPLICACION:
        dtConstPaneles.put("sTituloDialogoAlta",
                           "Crear nuevo tipo de implicaci�n");
        dtConstPaneles.put("sTituloDialogoModif",
                           "Modificar tipo de implicaci�n");
        dtConstPaneles.put("sTituloDialogoBaja", "Borrar tipo de implicaci�n");
        dtConstPaneles.put("sNomTabla", "SIVE_T_IMPLICACION");
        dtConstPaneles.put("sNomCampoCod", "CD_TIMPLICACION");
        dtConstPaneles.put("sTipoCampoCod", QueryTool.STRING + "");
        dtConstPaneles.put("sNomCampoDes", "DS_TIMPLICACION");
        dtConstPaneles.put("sTipoCampoDes", QueryTool.STRING + "");
        dtConstPaneles.put("bCampoExtra", "false");
        dtConstPaneles.put("bNumExtra", "false");
        dtConstPaneles.put("nLongCod", "2");
        dtConstPaneles.put("nLongDesc", "30");
        campoExtra = false; // Variable para este panel
        campoNumExtra = false;
        break;

      case PanMant.mantMCOMERCALI:
        dtConstPaneles.put("sTituloDialogoAlta",
                           "Crear nuevo m�todo de comercializaci�n");
        dtConstPaneles.put("sTituloDialogoModif",
                           "Modificar m�todo de comercializaci�n");
        dtConstPaneles.put("sTituloDialogoBaja",
                           "Borrar m�todo de comercializaci�n");
        dtConstPaneles.put("sNomTabla", "SIVE_MCOMERC_ALI");
        dtConstPaneles.put("sNomCampoCod", "CD_MCOMERCALI");
        dtConstPaneles.put("sTipoCampoCod", QueryTool.STRING + "");
        dtConstPaneles.put("sNomCampoDes", "DS_MCOMERCALI");
        dtConstPaneles.put("sTipoCampoDes", QueryTool.STRING + "");
        dtConstPaneles.put("bCampoExtra", "false");
        dtConstPaneles.put("bNumExtra", "false");
        dtConstPaneles.put("nLongCod", "2");
        dtConstPaneles.put("nLongDesc", "30");
        campoExtra = false; // Variable para este panel
        campoNumExtra = false;
        break;

      case PanMant.mantTRATPREVIOALI:
        dtConstPaneles.put("sTituloDialogoAlta",
                           "Crear nuevo tratamiento previo");
        dtConstPaneles.put("sTituloDialogoModif",
                           "Modificar tratamiento previo");
        dtConstPaneles.put("sTituloDialogoBaja", "Borrar tratamiento previo");
        dtConstPaneles.put("sNomTabla", "SIVE_TRAT_PREVIO_ALI");
        dtConstPaneles.put("sNomCampoCod", "CD_TRATPREVALI");
        dtConstPaneles.put("sTipoCampoCod", QueryTool.STRING + "");
        dtConstPaneles.put("sNomCampoDes", "DS_TRATPREVALI");
        dtConstPaneles.put("sTipoCampoDes", QueryTool.STRING + "");
        dtConstPaneles.put("bCampoExtra", "false");
        dtConstPaneles.put("bNumExtra", "false");
        dtConstPaneles.put("nLongCod", "2");
        dtConstPaneles.put("nLongDesc", "30");
        campoExtra = false; // Variable para este panel
        campoNumExtra = false;
        break;

      case PanMant.mantFINGERIRALI:
        dtConstPaneles.put("sTituloDialogoAlta",
                           "Crear nueva forma de ingerir el alimento");
        dtConstPaneles.put("sTituloDialogoModif",
                           "Modificar forma de ingerir el alimento");
        dtConstPaneles.put("sTituloDialogoBaja",
                           "Borrar forma de ingerir el alimento");
        dtConstPaneles.put("sNomTabla", "SIVE_F_INGERIR_ALI");
        dtConstPaneles.put("sNomCampoCod", "CD_FINGERIRALI");
        dtConstPaneles.put("sTipoCampoCod", QueryTool.STRING + "");
        dtConstPaneles.put("sNomCampoDes", "DS_FINGERIRALI");
        dtConstPaneles.put("sTipoCampoDes", QueryTool.STRING + "");
        dtConstPaneles.put("bCampoExtra", "false");
        dtConstPaneles.put("bNumExtra", "false");
        dtConstPaneles.put("nLongCod", "2");
        dtConstPaneles.put("nLongDesc", "30");
        campoExtra = false; // Variable para este panel
        campoNumExtra = false;
        break;

      case PanMant.mantLCONTAMIALI:
        dtConstPaneles.put("sTituloDialogoAlta",
                           "Crear nuevo lugar de contaminaci�n");
        dtConstPaneles.put("sTituloDialogoModif",
                           "Modificar lugar de contaminaci�n");
        dtConstPaneles.put("sTituloDialogoBaja",
                           "Borrar lugar de contaminaci�n");
        dtConstPaneles.put("sNomTabla", "SIVE_LCONTAMI_ALI");
        dtConstPaneles.put("sNomCampoCod", "CD_LCONTAMIALI");
        dtConstPaneles.put("sTipoCampoCod", QueryTool.STRING + "");
        dtConstPaneles.put("sNomCampoDes", "DS_LCONTAMIALI");
        dtConstPaneles.put("sTipoCampoDes", QueryTool.STRING + "");
        dtConstPaneles.put("bCampoExtra", "false");
        dtConstPaneles.put("bNumExtra", "false");
        dtConstPaneles.put("nLongCod", "2");
        dtConstPaneles.put("nLongDesc", "30");
        campoExtra = false; // Variable para este panel
        campoNumExtra = false;
        break;

      case PanMant.mantLPREPALI:
        dtConstPaneles.put("sTituloDialogoAlta",
                           "Crear nuevo lugar de preparaci�n");
        dtConstPaneles.put("sTituloDialogoModif",
                           "Modificar lugar de preparaci�n");
        dtConstPaneles.put("sTituloDialogoBaja", "Borrar lugar de preparaci�n");
        dtConstPaneles.put("sNomTabla", "SIVE_LPREP_ALI");
        dtConstPaneles.put("sNomCampoCod", "CD_LPREPALI");
        dtConstPaneles.put("sTipoCampoCod", QueryTool.STRING + "");
        dtConstPaneles.put("sNomCampoDes", "DS_LPREPALI");
        dtConstPaneles.put("sTipoCampoDes", QueryTool.STRING + "");
        dtConstPaneles.put("bCampoExtra", "false");
        dtConstPaneles.put("bNumExtra", "false");
        dtConstPaneles.put("nLongCod", "2");
        dtConstPaneles.put("nLongDesc", "30");
        campoExtra = false; // Variable para este panel
        campoNumExtra = false;
        break;

      case PanMant.mantLCONSUMOALI:
        dtConstPaneles.put("sTituloDialogoAlta",
                           "Crear nuevo lugar de consumici�n");
        dtConstPaneles.put("sTituloDialogoModif",
                           "Modificar lugar de consumici�n");
        dtConstPaneles.put("sTituloDialogoBaja", "Borrar lugar de consumici�n");
        dtConstPaneles.put("sNomTabla", "SIVE_LCONSUMO_ALI");
        dtConstPaneles.put("sNomCampoCod", "CD_CONSUMOALI");
        dtConstPaneles.put("sTipoCampoCod", QueryTool.STRING + "");
        dtConstPaneles.put("sNomCampoDes", "DS_CONSUMOALI");
        dtConstPaneles.put("sTipoCampoDes", QueryTool.STRING + "");
        dtConstPaneles.put("bCampoExtra", "false");
        dtConstPaneles.put("bNumExtra", "false");
        dtConstPaneles.put("nLongCod", "2");
        dtConstPaneles.put("nLongDesc", "30");
        campoExtra = false; // Variable para este panel
        campoNumExtra = false;
        break;

      case PanMant.mantMTVIAJEALI:
        dtConstPaneles.put("sTituloDialogoAlta",
                           "Crear nuevo m�todo de transporte");
        dtConstPaneles.put("sTituloDialogoModif",
                           "Modificar m�todo de transporte");
        dtConstPaneles.put("sTituloDialogoBaja", "Borrar m�todo de transporte");
        dtConstPaneles.put("sNomTabla", "SIVE_MTVIAJE_ALI");
        dtConstPaneles.put("sNomCampoCod", "CD_MTVIAJEALI");
        dtConstPaneles.put("sTipoCampoCod", QueryTool.STRING + "");
        dtConstPaneles.put("sNomCampoDes", "DS_MTVIAJEALI");
        dtConstPaneles.put("sTipoCampoDes", QueryTool.STRING + "");
        dtConstPaneles.put("bCampoExtra", "false");
        dtConstPaneles.put("bNumExtra", "false");
        dtConstPaneles.put("nLongCod", "2");
        dtConstPaneles.put("nLongDesc", "30");
        campoExtra = false; // Variable para este panel
        campoNumExtra = false;
        break;

      case PanMant.mantGRUPBROTES:
        dtConstPaneles.put("sTituloDialogoAlta", "Grupo de brote");
        dtConstPaneles.put("sTituloDialogoModif", "Grupo de brote");
        dtConstPaneles.put("sTituloDialogoBaja", "Grupo de brote");
        dtConstPaneles.put("sNomTabla", "SIVE_GRUPO_BROTE");
        dtConstPaneles.put("sNomCampoCod", "CD_GRUPO");
        dtConstPaneles.put("sTipoCampoCod", QueryTool.STRING + "");
        dtConstPaneles.put("sNomCampoDes", "DS_GRUPO");
        dtConstPaneles.put("sTipoCampoDes", QueryTool.STRING + "");
        dtConstPaneles.put("bCampoExtra", "false");
        dtConstPaneles.put("bNumExtra", "false");
        dtConstPaneles.put("nLongCod", "1");
        dtConstPaneles.put("nLongDesc", "25");
        campoExtra = false; // Variable para este panel
        campoNumExtra = false;
        break;
    } //end switch

    try {
      setApp(a);

      // botones
      vBotones.addElement(new CBoton(constantes.keyBtnAnadir,
                                     "images/alta.gif",
                                     "Nueva petici�n",
                                     false,
                                     false));

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion.gif",
                                     "Modificar petici�n",
                                     true,
                                     true));

      vBotones.addElement(new CBoton(constantes.keyBtnBorrar,
                                     "images/baja.gif",
                                     "Borrar petici�n",
                                     false,
                                     true));

      // etiquetas
      //si tiene un campo extra de it a�adirlo a la lista
      if (campoExtra) {
        vLabels.addElement(new CColumna("C�digo",
                                        "125",
                                        dtConstPaneles.getString("sNomCampoCod")));

        vLabels.addElement(new CColumna("Descripci�n",
                                        "375",
                                        dtConstPaneles.getString("sNomCampoDes")));

        vLabels.addElement(new CColumna("It_MasInf",
                                        "100",
                                        dtConstPaneles.getString(
            "sNomCheckExtra")));

      }
      else {
        vLabels.addElement(new CColumna("C�digo",
                                        "150",
                                        dtConstPaneles.getString("sNomCampoCod")));

        vLabels.addElement(new CColumna("Descripci�n",
                                        "450",
                                        dtConstPaneles.getString("sNomCampoDes")));

      } //end else de CampoExtra
      clmMantenimiento = new CListaMantenimiento(a,
                                                 vLabels,
                                                 vBotones,
                                                 this,
                                                 this,
                                                 250,
                                                 640);
      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del panel PanMant
  public void jbInit() throws Exception {

    final String imgBuscar = "images/refrescar.gif";

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgBuscar);
    this.getApp().getLibImagenes().CargaImagenes();

    // Escuchadores de eventos

    PanMant_actionAdapter actionAdapter = new PanMant_actionAdapter(this);
    CListaMantItemAdapter itemAdapter = new CListaMantItemAdapter(this);

    chckCod.setLabel("C�digo");
    chckCod.setCheckboxGroup(chkboxGrupo);
    chckCod.addItemListener(itemAdapter);
    chckCod.setState(true);
    chckDes.setLabel("Descripci�n");
    chckDes.setCheckboxGroup(chkboxGrupo);
    chckDes.setState(false);
    chckDes.addItemListener(itemAdapter);

    xYLayout1.setWidth(675);
    xYLayout1.setHeight(340);

    txtCod.setName("codigo");
    txtDes.setName("descripcion");

    txtCod.setVisible(true);
    txtDes.setVisible(false);

    btnBuscar.setActionCommand("buscar");
    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBuscar));
    btnBuscar.setLabel("Buscar");
    btnBuscar.addActionListener(actionAdapter);

    this.setLayout(xYLayout1);
    this.add(txtCod, new XYConstraints(30, 6, 312, 24));
    this.add(txtDes, new XYConstraints(30, 6, 312, 24));
    this.add(chckCod, new XYConstraints(30, 36, 130, -1));
    this.add(chckDes, new XYConstraints(212, 36, 130, -1));
    this.add(btnBuscar, new XYConstraints(515, 36, 98, 24));

    this.add(clmMantenimiento, new XYConstraints(7, 80, 650, 230));

    // tool tips

    new CContextHelp("Obtener mantenimientos", btnBuscar);
  }

  public void Inicializar() {}

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;
    }
  }

  // gesti�n de los botones
  void btn_actionPerformed(ActionEvent e) {
    CListaValores clv = null;
    QueryTool qt = null;
    Vector v = null;

    if (e.getActionCommand().equals("buscar")) {
      clmMantenimiento.setPrimeraPagina(this.primeraPagina());
    }
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {

    // nombre del servlet
    final String servlet = "servlet/SrvQueryTool";

    // lista para el filtro

    Lista v = new Lista();
    Lista vFiltro = new Lista();

    Inicializar(CInicializar.ESPERA);
    System.out.println("1");
    try {
      QueryTool qt = new QueryTool();

      // centros de vacunacion
      qt.putName(dtConstPaneles.getString("sNomTabla"));
      System.out.println("2");

      // campos que se leen

      qt.putType(dtConstPaneles.getString("sNomCampoCod"),
                 Integer.parseInt(dtConstPaneles.getString("sTipoCampoCod")));
      qt.putType(dtConstPaneles.getString("sNomCampoDes"),
                 Integer.parseInt(dtConstPaneles.getString("sTipoCampoDes")));

      System.out.println("3");
      if (campoExtra) {
        qt.putType(dtConstPaneles.getString("sNomCheckExtra"),
                   Integer.parseInt(dtConstPaneles.getString("sTipoCheckExtra")));
      }

      System.out.println("4");
      if (campoNumExtra) {
        qt.putType(dtConstPaneles.getString("sNomNumExtra"),
                   Integer.parseInt(dtConstPaneles.getString("sTipoNumExtra")));
      }
      System.out.println("5");
      // filtro de c�digo
      if (chckCod.getState()) {
        qt.putWhereType(dtConstPaneles.getString("sNomCampoCod"),
                        Integer.parseInt(dtConstPaneles.getString(
            "sTipoCampoCod")));
        System.out.println("6");
        qt.putWhereValue(dtConstPaneles.getString("sNomCampoCod"),
                         txtCod.getText().trim() + "%");
        System.out.println("7");
        qt.putOperator(dtConstPaneles.getString("sNomCampoCod"),
                       "like");

      }
      else {
        // filtro de descripci�n
        System.out.println("8");
        qt.putWhereType(dtConstPaneles.getString("sNomCampoDes"),
                        Integer.parseInt(dtConstPaneles.getString(
            "sTipoCampoDes")));
        System.out.println("9");
        qt.putWhereValue(dtConstPaneles.getString("sNomCampoDes"),
                         txtDes.getText().trim() + "%");
        System.out.println("10");
        qt.putOperator(dtConstPaneles.getString("sNomCampoDes"),
                       "like");
      }
      qt.addOrderField(dtConstPaneles.getString("sNomCampoCod"));

      vFiltro.setTrama(dtConstPaneles.getString("sNomCampoCod"), "");
      vFiltro.addElement(qt);

      // consulta el servidor
      System.out.println("11");
      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      System.out.println("12");
      v = (Lista)this.getApp().getStub().doPost(2, vFiltro);
      if (v.size() == 0) {
        this.getApp().showAdvise("No se encontraron registros");
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    Inicializar(CInicializar.NORMAL);

    return v;
  }

  // solicita la siguiente trama de datos
  public Lista siguientePagina() {
    // nombre del servlet
    final String servlet = "servlet/SrvQueryTool";

    // lista para el filtro
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();

    Inicializar(CInicializar.ESPERA);

    // realiza la consulta al servlet
    try {
      QueryTool qt = new QueryTool();

      // tabla de familias de productos
      qt.putName(dtConstPaneles.getString("sNomTabla"));

      // campos que se leen
      qt.putType(dtConstPaneles.getString("sNomCampoCod"),
                 Integer.parseInt(dtConstPaneles.getString("sTipoCampoCod")));
      qt.putType(dtConstPaneles.getString("sNomCampoDes"),
                 Integer.parseInt(dtConstPaneles.getString("sTipoCampoDes")));
      if (campoExtra) {
        qt.putType(dtConstPaneles.getString("sNomCheckExtra"),
                   Integer.parseInt(dtConstPaneles.getString("sTipoCheckExtra")));
      }

      if (campoNumExtra) {
        qt.putType(dtConstPaneles.getString("sNomNumExtra"),
                   Integer.parseInt(dtConstPaneles.getString("sTipoNumExtra")));
      }

      // filtro de c�digo
      if (chckCod.getState()) {
        qt.putWhereType(dtConstPaneles.getString("sNomCampoCod"),
                        Integer.parseInt(dtConstPaneles.getString(
            "sTipoCampoCod")));
        qt.putWhereValue(dtConstPaneles.getString("sNomCampoCod"),
                         txtCod.getText().trim() + "%");
        qt.putOperator(dtConstPaneles.getString("sNomCampoCod"),
                       "like");
      }
      else {
        // filtro de descripci�n
        qt.putWhereType(dtConstPaneles.getString("sNomCampoDes"),
                        Integer.parseInt(dtConstPaneles.getString(
            "sTipoCampoDes")));
        qt.putWhereValue(dtConstPaneles.getString("sNomCampoDes"),
                         txtDes.getText().trim() + "%");
        qt.putOperator(dtConstPaneles.getString("sNomCampoDes"),
                       "like");
      }

      qt.addOrderField(dtConstPaneles.getString("sNomCampoCod"));

      // fija el filtro
      vFiltro.addElement(qt);
      // fija la trama
      vFiltro.setTrama(this.clmMantenimiento.getLista().getTrama());

      this.getApp().getStub().setUrl(servlet);
      vPetic = (Lista)this.getApp().getStub().doPost(2, vFiltro);

      // error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    // la lista debe estar creada
    if (vPetic == null) {
      vPetic = new Lista();

    }
    Inicializar(CInicializar.NORMAL);

    return vPetic;
  }

  // operaciones de la botonera
  public void realizaOperacion(int j) {

    Lista lisPet = this.clmMantenimiento.getLista();
    DiaMant dm = null;
    DiaMantGrupBrotes dmGrup = null;
    Data dPetMod = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:

        //si estamos en el caso de grupos de brotes hay q dar de alta
        //en dos tablas
        if (tab == PanMant.mantGRUPBROTES) {
          dmGrup = new DiaMantGrupBrotes(this.getApp(), 0, null, dtConstPaneles);
          dmGrup.show();
          if (dmGrup.bAceptar) {
            dPetMod = dmGrup.devuelveData();
            clmMantenimiento.getLista().addElement(dPetMod);
            // No la vaciamos, porque este m�todo lo hace ya..
            clmMantenimiento.setPrimeraPagina(clmMantenimiento.getLista());
            //clmMantenimiento.setPrimeraPagina(primeraPagina());
          }
        }
        else {
          dm = new DiaMant(this.getApp(), 0, null, dtConstPaneles);
          dm.show();
          if (dm.bAceptar) {
            dPetMod = dm.devuelveData();
            clmMantenimiento.getLista().addElement(dPetMod);
            // No la vaciamos, porque este m�todo lo hace ya..
            clmMantenimiento.setPrimeraPagina(clmMantenimiento.getLista());
            //clmMantenimiento.setPrimeraPagina(primeraPagina());
          }
        }
        break;
      case MODIFICACION:
        dPetMod = clmMantenimiento.getSelected();
        if (dPetMod != null) {
          int ind = clmMantenimiento.getSelectedIndex();
          if (tab == PanMant.mantGRUPBROTES) {
            dmGrup = new DiaMantGrupBrotes(this.getApp(), 1, dPetMod,
                                           dtConstPaneles);
            dmGrup.show();
            if (dmGrup.bAceptar) {
              dPetMod = dmGrup.devuelveData();
              clmMantenimiento.getLista().setElementAt(dPetMod, ind);
              // No la vaciamos, porque este m�todo lo hace ya..
              clmMantenimiento.setPrimeraPagina(clmMantenimiento.getLista());
            }
          }
          else {
            dm = new DiaMant(this.getApp(), 1, dPetMod, dtConstPaneles);
            dm.show();
            if (dm.bAceptar) {
              dPetMod = dm.devuelveData();
              clmMantenimiento.getLista().setElementAt(dPetMod, ind);
              // No la vaciamos, porque este m�todo lo hace ya..
              clmMantenimiento.setPrimeraPagina(clmMantenimiento.getLista());
            }
          }

        } //end dtPedMod != null
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dPetMod = clmMantenimiento.getSelected();
        if (dPetMod != null) {
          int ind = clmMantenimiento.getSelectedIndex();
          if (tab == PanMant.mantGRUPBROTES) {
            dmGrup = new DiaMantGrupBrotes(this.getApp(), 2, dPetMod,
                                           dtConstPaneles);
            dmGrup.show();
            if (dmGrup.bAceptar()) {
              clmMantenimiento.getLista().removeElementAt(ind);
              clmMantenimiento.setPrimeraPagina(clmMantenimiento.getLista());
            }
          }
          else {
            dm = new DiaMant(this.getApp(), 2, dPetMod, dtConstPaneles);
            dm.show();
            if (dm.bAceptar()) {
              clmMantenimiento.getLista().removeElementAt(ind);
              clmMantenimiento.setPrimeraPagina(clmMantenimiento.getLista());
            }
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;
    }
  }

  void chkItemStateChanged(ItemEvent e) {
    if (e.getItem().equals("C�digo")) {
      txtCod.setVisible(true);
      txtDes.setText("");
      txtCod.setText(txtDes.getText().trim().toUpperCase());
      txtDes.setVisible(false);
    }
    else {
      txtCod.setVisible(false);
      txtCod.setText("");
      txtDes.setVisible(true);
      txtDes.setText(txtCod.getText().trim());
    }
    doLayout();
  }
}

// botones de centro, almac�n y buscar
class PanMant_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanMant adaptee;
  ActionEvent e;

  PanMant_actionAdapter(PanMant adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}

// control de cambio de buscar por c�digo/descripci�n
class CListaMantItemAdapter
    implements java.awt.event.ItemListener {
  PanMant adaptee;

  CListaMantItemAdapter(PanMant adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkItemStateChanged(e);
  }
}
