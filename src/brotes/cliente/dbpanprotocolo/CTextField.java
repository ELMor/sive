package brotes.cliente.dbpanprotocolo;

import java.awt.Color;
import java.awt.TextField;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.TextEvent;

/*Si se pasa como obligatorio, se pintara amarilo*/
public class CTextField
    extends TextField {

  //datos de entrada
  public String sOblig;
  public String sTpreg;
  public int iLong;
  public int iEnt;
  public int iDec;
  public String sCondicionada;
  public String sCodM;
  public String sNum;
  public String sNumC;
  public String sDesC;
  public String sPregunta;

  protected boolean bActivarChange = true;

  protected String sValidaSN = "N";
  protected String sFechaAlmacen = ""; //si fechaOK, almacena la fecha
  protected String sStringAntes = "";
  protected String sStringDespues = "";
  protected String sNumSinFormato = "";
  //constructor
  public CTextField(String sOBLIGATORIO,
                    String sTPREG,
                    int iLONG,
                    int iENT,
                    int iDEC,
                    String sCONDICIONADA, String sCD_MODELO,
                    String sNM_LIN,
                    String sNM_LIN_COND, String sDS_PREG_COND,
                    String CD_PREGUNTA) {
    super();

    //parametros de entrada
    sOblig = sOBLIGATORIO;
    sTpreg = sTPREG;
    iLong = iLONG;
    iEnt = iENT;
    iDec = iDEC;
    sCondicionada = sCONDICIONADA;
    sCodM = sCD_MODELO;
    sNum = sNM_LIN;
    sNumC = sNM_LIN_COND;
    sDesC = sDS_PREG_COND;
    sPregunta = CD_PREGUNTA;

    //Color
    if (sOblig.equals("S")) {
      setBackground(new Color(255, 255, 150));

      //longitud del numero que puede escribir el usuario
      //No permitimos ','
      //Al formatearlo, lo aumentamos
    }
    if (sTpreg.equals("N")) {
      iLong = iEnt + 1 + iDec;

    }
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  } //end Constructor

//jbInit*****************************************
  private void jbInit() throws Exception {

    //ESCRIBIR
    this.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyPressed(KeyEvent e) {
        txt_keyPressed(e);
      }
    });

    //CHANGE
    this.addTextListener(new java.awt.event.TextListener() {
      public void textValueChanged(TextEvent e) {
        txt_textValueChanged(e);
      }
    });
    // ARS 10-07-01 dar formato a fecha al perder el foco.
    this.addFocusListener(new java.awt.event.FocusListener() {
      public void focusLost(FocusEvent e) {
        if (sTpreg.equals("F")) {
          txt_focusLost(e);
        }
      }

      public void focusGained(FocusEvent e) {}
    });

  } //fin jbnit

// ARG: Se a�ade para a�adir las '/' al escribir las fechas
//Fecha: lee y escribe '/' si toca
  void AutoEscribeFormatoFecha(KeyEvent e) throws Exception {
    String sFecha = getText();
// CORREGIDO ARS 10-07-01
    if ( (sFecha.length() == 2) &&
        (e.getKeyCode() != java.awt.event.KeyEvent.VK_BACK_SPACE) &&
        (e.getKeyCode() != java.awt.event.KeyEvent.VK_DELETE)) {
      setText(sFecha.substring(0, 2) + "/");
      setText(getText().substring(0, 3));
    }
    if ( (sFecha.length() == 5) &&
        (e.getKeyCode() != java.awt.event.KeyEvent.VK_BACK_SPACE) &&
        (e.getKeyCode() != java.awt.event.KeyEvent.VK_DELETE)) {
      setText(sFecha.substring(0, 5) + "/");
      setText(getText().substring(0, 6));
    }
  }

//-------------------------------------------------------------------------
//uOrden = 1: dia/mes/a�o � uOrden = 2: mes/dia/a�o  (01/02/97)(01/02/8)
  protected boolean ChequearFecha(String sCad, int uOrden) {

    boolean b = true;
    String sCadena = "";
    int i = 0;
    String j1, j2, aa, bb, cc, i1 = "";
    int x1, x2 = 0;
    String dd = "";
    String mm = "";
    String yy = "";

    String ch = "";
    String tt = "";

    int bisiesto = 0;
    int alerta = 0;

    int total = 0;
    int resto4 = 0;
    int resto100 = 0;
    int resto400 = 0;

    sCadena = sCad;

    try {

      if (sCad.equals("")) {
        b = false;
      }
      // ARS 10-07-01 Controlar longitud de la fecha
      if ( (!sCad.equals("")) && (sCad.length() >= 7)) {
        //SEPARAMOS LOS DATOS
        x1 = sCadena.indexOf("/", 0);
        x2 = sCadena.indexOf("/", x1 + 1);
        if (x1 == -1 || x2 == -1) {
          b = false; //no se encontraron las barras
        }
        else {
          aa = sCadena.substring(0, x1);
          bb = sCadena.substring(x1 + 1, x2);
          cc = sCadena.substring(x2 + 1, sCadena.length());

          if (aa.equals("") || bb.equals("") || cc.equals("")) {
            b = false; //algun bloque falta
          }
          else {

            //deletepot del a�o.
            for (i = 0; i < cc.length(); ++i) {
              if ( (ch = cc.substring(i, i + 1)) != ".") {
                tt = tt + ch;
              }
              if ( (ch = cc.substring(i, i + 1)) == ".") {
                tt = tt;
              }
            }
            //FORMATO dia/mes/a�o � mes/dia/a�o
            if (uOrden == 1) {
              dd = aa;
              mm = bb;
              yy = tt;
            }
            else if (uOrden == 2) {
              dd = bb;
              mm = aa;
              yy = tt;
            }
            else {
              //NO DEBE DARSE UN ORDEN DISTINTO DE 1 o 2
              b = false;
            }

            //COMPROBAR QUE SON NUMEROS y su rango.
            // ARS 10-07-01 Controlar longitud de la fecha
            if ( (dd.trim().length() == 0) ||
                (mm.trim().length() == 0) ||
                (tt.trim().length() == 0) ||
                (ChequearEntero(dd, 1, 31, 2) != true) ||
                (ChequearEntero(mm, 1, 12, 2) != true) ||
                (ChequearEntero(tt, 0, 10000, 2) != true)) {
              b = false;
            }

            //RESPECTO A LOS MESES Y DIAS
            //desechamos largos y 0. FORMATEAMOS A DOS CIFRAS, y
            if (mm.length() > 2 || dd.length() > 2
                || dd.equals("00") || mm.equals("00")) {
              b = false;
            }
            if (mm.length() == 1) {
              mm = "0" + mm;
            }
            if (dd.length() == 1) {
              dd = "0" + dd;
            }

            //descomponer los dias
            // Corregido ARS 10-07-01
            if (dd.length() > 0) {
              j1 = dd.substring(0, 1);
              j2 = dd.substring(1, 2);
            }
            else {
              j1 = "-1";
              j2 = "-1";
            }

            //dentro de cada mes
            if (mm.equals("02")) {
              if (j1.equals("3")) {
                b = false;
              }
              else if (j1.equals("2")
                       && j2.equals("9")) {
                alerta = 1;
              }
            }
            else if (mm.equals("04") || mm.equals("06") || mm.equals("09") ||
                     mm.equals("11")) {
              if (j1.equals("3") && !j2.equals("0")) {
                b = false;
              }
            }
            //sacar el a�o con 4 cifras=total.
            if (yy.length() > 4 || yy.length() == 3 || yy.length() == 0) {
              b = false;
            }
            else if (yy.length() == 2) {
              //leo el 1� caracter
              i1 = yy.substring(0, 1);

              if (i1.equals("7") || i1.equals("8") || i1.equals("9")) {
                yy = "19" + yy;
              }
              else {
                yy = "20" + yy;
              }
            } //fin elseif

            else if (yy.length() == 1) {
              yy = "200" + yy;
            }

            //a�o

            Integer Itotal = new Integer(yy);
            total = Itotal.intValue();

            resto4 = total % 4;
            resto100 = total % 100;
            resto400 = total % 400;

            //ESTUDIO DE BISIESTO
            //divisible entre 4
            if (resto4 == 0) {
              //no divis. entre 100
              if (resto100 != 0) {
                bisiesto = 1;
              }
              //si div. entre 100
              else {
                //div. entre 400
                if (resto400 == 0) {
                  bisiesto = 1;
                }
              }
            } //fin if

            //A�O BISIESTO
            if (alerta == 1 && bisiesto == 0) {
              b = false;
            }

          } //fin else aa, bb, cc
        } //fin del else (no barras)
      } //fin de scad
      // Corregido 10-07-01 ARS
      else if ( (sCad.length() > 0) && (sCad.length() < 7)) {
        b = false;

        //FIN DE FUNCION
      }
      if (!b) { //si mal
        sFechaAlmacen = "";
      }
      else {
        sFechaAlmacen = dd + "/" + mm + "/" + yy;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      b = false;
      sFechaAlmacen = "";
    }
    return (b);
  } //fin funcion--------------------------------------

  /* Salida:  -1  Esta borrando  o es igual
              i   El lugar donde esta a�adiendo (0,...)
      Ej: XBC --> XB0C : 2*/
  protected int CalcularIndice() {
    //lee las cadenas y descubre la diferencia
    int iReturn = -2;
    String sA, sD = "";

    //borrando
    if (sStringAntes.length() >= sStringDespues.length()) {
      iReturn = -1;
    }
    else {
      //intro del primero
      if (sStringAntes.length() == 0) {
        iReturn = 0;
      }
      else {
        for (int i = 0; i < sStringAntes.length(); i++) {
          sA = sStringAntes.substring(i, i + 1);
          sD = sStringDespues.substring(i, i + 1);
          if (sA.compareTo(sD) != 0) { //no son iguales
            iReturn = i;
            break;
          }
          if (iReturn == -2) { //a�ade al final
            iReturn = sStringAntes.length();
          }
        }
      }
    }
    return (iReturn);
  }

  /*Dejo solo parte entera*/
  protected void ReorganizarParteEntera() {

    if (sStringDespues.length() > iEnt) {
      int iPunto = sStringAntes.indexOf(".", 0);
      setText(sStringAntes.substring(0, iPunto));
      select(getText().length(), getText().length());
    }

  }

  /* Borra caracter (indice, indice +1)*/
  protected void BorrarCaracter(int indice) {

    String sDelante = getText().substring(0, indice);
    String sDetras = getText().substring(indice + 1, getText().length());

    setText(sDelante + sDetras);
    select(indice, indice);
  }

//Entradas: 	Cadena a chequear, Limite inferior permitido, Limite superior, uFlag(1,2)
//Return:	uFlag=1. False, cadena no numerica. True, vacio y cadena numerica
//		    uFlag=2. False, cadena no numerica y vacio. True, cadena numerica
//*************************************************************************************
   protected boolean ChequearEntero(String sDat, int uMin, int uMax, int uFlag) {
     String cChar = "";
     String sString = sDat;
     int uTipo = uFlag;
     boolean b = true;

     //si chequeamos un string que admita vacio.
     if (uTipo == 1) {
       if (sString == "") {
         b = true;
       }
     }
     //si chequeamos un string que no admita vacio.
     if (uTipo == 2) {
       if (sString == "") {
         b = false;
       }
     }
     //comprobar es un numero
     for (int i = 0; i < sString.length(); i++) {
       cChar = sString.substring(i, i + 1);
       try {
         Integer IChar = new Integer(cChar);
       }
       catch (Exception e) { // si no es un numero
         b = false;

       }

     } //fin del for

     //comprobar longitud
     // Corregido ARS 26-05-01 para que no casque con fechas err�neas.
     if ( (b) && (sString != null) && (sString.length() > 0)) {
       Integer Imin = new Integer(sString);
       Integer Imax = new Integer(sString);

       if (Imin.intValue() < uMin || uMax < Imax.intValue()) {
         b = false;
       }
     }

     return (b);
   } //fin de ChequearEntero

  protected boolean Valid_Dato_Intermedio(int indice, String s) {

    boolean b = true;
    int Len = getText().length() - 1; //sin el dato nuevo

    //BLOQUE 1
    if (indice < 2) {

      if (!ChequearEntero(s, 0, 9, 1)) { //No N
        b = false;
        //bloque inacabado y longantes=2 (XXX)
      }
      if ( (getText().indexOf("/", 0) == -1) && (Len == 2) && b) {
        b = false;
        //bloque inacabado y longantes<2 no se dara!! (X)-->a�ado, no modifico

        //bloque acabado y longbloqueantes = 2 (XXX/)
      }
      if ( (getText().indexOf("/", 0) != -1) &&
          (getText().substring(0, getText().indexOf("/", 0)).length() == 3) &&
          b) {
        b = false;

        //bloque acabado y longbloqueantes < 2 (XX/) true
      }
    }

    //SEPARADORES
    if (indice == 2 || indice == 5 && b) {

      if (!s.equals("/")) {
        b = false;
      }
      else {
        b = true;
      }
    }
    //BLOQUE 2. Existe una barra y el ind > '/' (X/XXX)
    //puede haber pasado por separador!--> b (X/XXX
    if ( (getText().indexOf("/", 0) != -1) &&
        indice > getText().indexOf("/", 0) && b) {

      int x1 = getText().indexOf("/", 0); //pos primera barra

      if (!ChequearEntero(s, 0, 9, 1)) { //No N
        b = false;

        //bloque inacabado y longantes=2 (X/XXX)
      }
      if ( (getText().indexOf("/", x1 + 1) == -1)
          && (getText().substring(x1 + 1, getText().length()).length() == 3)
          && b) {
        b = false;
        //bloque acabado y longbloqueantes=2 (X/XXX/)
      }
      if ( (getText().indexOf("/", x1 + 1) != -1) && b &&
          indice < getText().indexOf("/", x1 + 1)) {

        int x2 = getText().indexOf("/", x1 + 1);
        if (getText().substring(x1 + 1, x2).length() == 3) {
          b = false;
        }
      }
    } //fin bloque 2

    //BLOQUE 3. Existe dos barras y el indice > 2� barra
    if (getText().indexOf("/", 0) != -1) { //1 barra
      int x1 = getText().indexOf("/", 0);
      if (getText().indexOf("/", x1 + 1) != -1 &&
          indice > getText().indexOf("/", x1 + 1)) {

        if (!ChequearEntero(s, 0, 9, 1)) { //No N
          b = false;

        }
        int x2 = getText().indexOf("/", x1 + 1);
        if (getText().substring(x2 + 1, getText().length()).length() == 5) {

          b = false;
        }
      }
    } //fin bloque 3

    return (b);
  }

  protected void txt_keyPressed(KeyEvent e) {
    //capturo la fecha de antes
    sStringAntes = getText();
    // ARS 22-05-01, metemos esto para automatizar las "/".
    int posCursor = 0;
    int lim_select = 0;
    // ARG: Se controlan las '/' al escribir en el caso de que sea una fecha
    if (sTpreg.equals("F")) {
      try {
        if ( (e.getKeyCode() != KeyEvent.VK_LEFT) &&
            (e.getKeyCode() != KeyEvent.VK_RIGHT)) {
          posCursor = this.getSelectionStart();
          if (posCursor < this.getText().length()) {
            lim_select = posCursor;
          }
          else {
            lim_select = this.getText().length() + 1;
            // Fin correcci�n ppal ARS.
            //fecha
          }
          if (getText().length() < 10) {
            AutoEscribeFormatoFecha(e);
            select(lim_select, lim_select); // l�nea corregida ARS.
          }
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
    }

  } //fin Pressed

  protected void txt_textValueChanged(TextEvent e) {

    String s = "";

    //capturo la fecha de despues
    sStringDespues = getText();

    //capturo el indice
    int ind = CalcularIndice();

//!!!!!!!!!!!!!
//FECHA!!!!!!!!
//!!!!!!!!!!!!!
    if (sTpreg.equals("F")) {
      //a�adiendo
      if ( (ind != -1) && (ind == getText().length() - 1)) {
        //capturo el caracter que se ha introducido
        s = getText().substring(ind, ind + 1);

        if ( (ind == 2 || ind == 5) && !s.equals("/")) {
          BorrarCaracter(ind);

        }
        if (ind != 2 && ind != 5 && ind < 9) {
          if (!ChequearEntero(s, 0, 9, 1)) { //No N
            BorrarCaracter(ind);
          }
        }
        if (ind > 9) {
          BorrarCaracter(ind);
        }
      } //--------------------------

      // ARS (22-05-01) Se utiliza este c�digo para poner las barras autom�ticas.
      // ARG: Se controla que el ultimo caracter introducido sea valido
      if (getText().length() != 3 && getText().length() != 6) {
        if (!ChequearEntero(s, 0, 9, 1)) { //No N
          setText(getText().substring(0, getText().length() - 1));
          select(getText().length() + 1, getText().length() + 1);
        }
      }
// ARS 22-05-01 Se quita, al poner las barras autom�ticas.
      /*    //incluyendo en medio
          if ((ind != -1) && (ind < getText().length() -1)){
            s = getText().substring(ind, ind +1);
            if (!Valid_Dato_Intermedio(ind, s))
              BorrarCaracter(ind);
          }//--------------------------   */
    } //fin Fecha
//!!!!!!!!!!!!!
//NUMERO!!!!!!!!
//!!!!!!!!!!!!!
    if (sTpreg.equals("N") && bActivarChange) {
      //si he borrado y era el punto, reorganizo parte entera
      if (sStringAntes.length() > 0 && (ind == -1)) { //borrado
        if (sStringDespues.indexOf(".", 0) == -1
            && sStringAntes.indexOf(".", 0) != -1) { //FUE EL PUNTO!
          ReorganizarParteEntera();
        }
      }
      //a�adiendo---------------------------------------
      if ( (ind != -1) && (ind == getText().length() - 1)) {
        //capturo el caracter que se ha introducido
        s = getText().substring(ind, ind + 1);

        //No Numero
        if (!ChequearEntero(s, 0, 9, 1)) {
          if (s.equals(".")) { //es un pto
            if (sStringAntes.indexOf(".", 0) != -1) { //existe otro
              BorrarCaracter(ind);
            }
          }
          else { //no pto, no Numero
            BorrarCaracter(ind);
          }
        }
        //Numero
        else {

          if (sStringDespues.indexOf(".", 0) == -1) { //sin pto: ENTERO

            if (sStringDespues.length() > iEnt) {
              BorrarCaracter(ind);
            }
          }
          else { //DECIMAL
            String sDecimales = getText().substring
                (getText().indexOf(".", 0) + 1, getText().length());
            if (sDecimales.length() > iDec) {
              BorrarCaracter(ind);
            }
          }
        }

      } //--------------------------

      //incluyendo en medio
      if ( (ind != -1) && (ind < getText().length() - 1)) {
        s = getText().substring(ind, ind + 1);
        if (!Valid_Dato_Intermedio_Numero(ind, s)) {
          BorrarCaracter(ind);
        }
      } //--------------------------
    } //fin Numero

//!!!!!!!!!!!!!
//CARACTER!!!!!!!!
//!!!!!!!!!!!!!
    if (sTpreg.equals("C")) {
      if (getText().length() > iLong) {
        BorrarCaracter(ind);
      }
    } //fin caracter

  } //fin change

// ARS 10-07-01 formato de fecha.
  public void txt_focusLost(FocusEvent e) {
    try {
      this.ValidarFecha();
      if (this.getValid().trim().equals("S")) {
        this.setText(this.getFecha());
        select(getText().length() + 1, getText().length() + 1);
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } // Final del focus lost

//Chequea la fecha que hay en la caja
  public void ValidarFecha() {

    //OK
    if (ChequearFecha(getText(), 1)) {
      sValidaSN = "S";

      //KO
    }
    else {
      sValidaSN = "N";
    }
  }

//Proporciona si el valor es valido
  public String getValid() {
    return (sValidaSN);
  }

//Proporciona la fecha
  public String getFecha() {
    return (sFechaAlmacen);
  }

  protected boolean Valid_Dato_Intermedio_Numero(int indice, String s) {

    boolean b = true;

    //�Bloque 1 o 2? **********************
    String Bloque = "";
    String sDec = "";
    String sEnt = "";
    int iPunto = getText().indexOf(".", 0);

    if (iPunto == -1) { //no existe punto
      Bloque = "1";
      sEnt = getText(); //DESPUES del change
    }
    else { //existe punto
      if (indice < iPunto) {
        Bloque = "1";
        sEnt = getText().substring(0, iPunto); //DESPUES del change
      }
      else {
        Bloque = "2";
        sDec = getText().substring(iPunto + 1, getText().length()); //DESPUES del change

      }
    } //************************************

    //BLOQUE 1
    if (Bloque.equals("1")) {

      if (!ChequearEntero(s, 0, 9, 1)) { //No N
        b = false;
      }
      else { //Numero
        if (sEnt.length() > iEnt && b) {
          b = false;
        }
      }
    }
    //BLOQUE 2
    if (Bloque.equals("2")) {

      if (!ChequearEntero(s, 0, 9, 1)) { //No N
        b = false;
      }
      else { //Numero

        if (sDec.length() > iDec && b) {
          b = false;
        }
      }
    }

    return (b);
  }

//Numeros: Formatea
  protected String FormatearNumero() {

    String sNumero = getText(); //entrada
    int nPosicionPunto = -1;
    String sEntera = "";
    String sDecimal = "";
    int nLen = 0;
    String sNewCadena = "";

    //encontrar el punto
    nPosicionPunto = sNumero.indexOf(".", 0);

    //parte decimal
    if (nPosicionPunto != -1) { //89.1566 y .1313
      sDecimal = sNumero.substring(nPosicionPunto + 1, sNumero.length());
    }
    else { //si es -1 --> 2345
      sDecimal = "";
    }

    //parte entera
    if (nPosicionPunto != -1 && nPosicionPunto != 0) {
      sEntera = sNumero.substring(0, nPosicionPunto);
    }
    else if (nPosicionPunto == 0) { //ver .1235
      sEntera = "0";
    }
    else { //si es -1 --> 2345
      sEntera = sNumero;
    }

    //quitar los ceros que sobran************************
    String sLeo = "";
    int nCeros = 0;
    boolean bYaNoesCero = false;

    for (int iC = 0; iC < sEntera.length(); iC++) {
      sLeo = sEntera.substring(iC, iC + 1);

      if ( (sLeo.equals("0")) && (!bYaNoesCero)) {
        nCeros = nCeros + 1;

      }
      else {
        bYaNoesCero = true;
      }
    }

    if (nCeros != 0) {
      sEntera = "" + sEntera.substring(nCeros, sEntera.length());

      //**************************************************

       //longitud de cadena entera (sin decimal)
    }
    nLen = sEntera.length();

    //punteo
    if (nLen > 3) {
      for (int iContador = 1; iContador < nLen + 1; iContador++) {
        sNewCadena = sEntera.substring(nLen - iContador,
                                       nLen - iContador + 1) + sNewCadena;

        if (iContador != nLen) {

          if ( (iContador % 3) == 0) {
            sNewCadena = "," + sNewCadena;
          }
        }
      }
    }
    else { //<3
      sNewCadena = sEntera;
    }

    //si todo era ceros
    if (sNewCadena.equals("")) {
      sNewCadena = "0";
    }

    //componer la cadena
    if (sDecimal.length() == 0) {
      sNewCadena = sNewCadena;
    }
    else {
      sNewCadena = sNewCadena + "." + sDecimal;
    }

    //arreglo
    if (sNewCadena.equals(".00")) {
      sNewCadena = "0.00";

    }
    return (sNewCadena);
  }

//Calcula la longitud permitida del numero (con formato)
  protected int LongNumero() {

    int iLen = 0;
    int iResto = 0;
    int iDiv = 0;
    int iNumPtos = 0;

    if (iEnt == 0) { //N(0,?)
      iLen = 0;
    }
    else {
      iDiv = (int) (iEnt / 3);
      iResto = iEnt % 3;

      if (iResto == 0) {
        iNumPtos = iDiv - 1;
      }
      else {
        iNumPtos = iDiv;
      }
    }
    if (iDec == 0) {
      iLen = iEnt + iNumPtos;
    }
    else {
      iLen = iEnt + iNumPtos + 1 + iDec;

    }
    return (iLen);
  }

//Formatea el Numero que hay en la caja  (lostFocus)
  public void FormatNumero() {
    sNumSinFormato = getText(); //guardarlo!!!
    iLong = LongNumero();
    bActivarChange = false;
    setText(FormatearNumero());
  }

//Resetear  sNumSinFormato
  public void ResetsNumSinFormato() {
    sNumSinFormato = "";
  }

//debe llamarse despues de FormatNumero, que es donde se carga el valor
  public String getNumSinFormato() {
    return (sNumSinFormato);
  }

//DesFormatea el Numero
  public void DesFormatNumero() {
    bActivarChange = true;
    iLong = iEnt + 1 + iDec;
    setText(sNumSinFormato);
  }

//Generales
  public String getTipoPregunta() {
    return (sTpreg);
  }

  public String getObligatorio() {
    return (sOblig);
  }

  public String getCondicionada() {
    return (sCondicionada);
  }

  public String getCodM() {
    return (sCodM);
  }

  public String getNum() {
    return (sNum);
  }

  public String getNumC() {
    return (sNumC);
  }

  public String getDesC() {
    return (sDesC);
  }

  public String getCodPregunta() {
    return (sPregunta);
  }

} //endclass
