/**
 * Clase: DBPanDatosBas
 * Hereda: CPanel
 * Autor: MTR
 * Fecha Inicio: 19/05/2000
 * Analisis Funcional: Informe Final
 * Descripcion: Panel correspondiente a Datos B�sicos
 */
package brotes.cliente.dbpandatosbas;

import java.util.Hashtable;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.CHora;
import brotes.cliente.c_componentes.ChoiceCDDS;
import brotes.cliente.c_componentes.DatoRecogible;
import brotes.cliente.diabif.DiaBIF;
import capp2.CApp;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CPanel;
import capp2.CTextArea;
import sapp2.Data;
import sapp2.Lista;

public class DBPanDatosBas
    extends CPanel
    implements CInicializar, DatoRecogible {

  /*************** Datos propios del panel ****************/

  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;
  final int ESPERA = 4;
  final int NORMAL = 5;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 20;

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  /****************** Componentes del panel ********************/
  XYLayout xYLayout = new XYLayout();

  XYLayout xYLayout1 = new XYLayout();
  CApp applet = null;
  DiaBIF dlg = null;
  Data brote = null;

  Label lTNotificador = new Label();
  ChoiceCDDS choTNotif = null;

  Label lPersRiesgo = new Label();
  Label lExp = new Label();
  Label lIngHosp = new Label();
  Label lEnf = new Label();
  Label lDefun = new Label();
  CEntero txtExp = new CEntero(4);
  CEntero txtEnf = new CEntero(4);
  CEntero txtIH = new CEntero(4);
  CEntero txtDefun = new CEntero(4);

  Label lFecExp = new Label();
  fechas.CFecha fechaExp = new fechas.CFecha("N");
  CHora horaExp = new CHora("N");
  Label lMecTrans = new Label();
  ChoiceCDDS choMecTrans = null;

  Label lObserv = new Label();
  CTextArea textAreaObserv = new CTextArea(1000);

  Label lMani = new Label();
  CEntero txtMani = new CEntero(4);

  Label lEstVac = new Label();

  Label lVacNoEnf = new Label();
  CEntero txtVacNoEnf = new CEntero(4);
  Label lVacEnf = new Label();
  CEntero txtVacEnf = new CEntero(4);
  Label lNoVacNoEnf = new Label();
  CEntero txtNoVacNoEnf = new CEntero(4);
  Label lNoVacEnf = new Label();
  CEntero txtNoVacEnf = new CEntero(4);

  //Listas q se tratan en este panel
  Lista listaTNotificador = new Lista();
  Lista listaMecProbTransm = new Lista();

  // constructor

  public DBPanDatosBas(CApp a,
                       DiaBIF d,
                       Data b,
                       int modo,
                       Hashtable hsCompleta) {

    super(a);
    applet = a;
    dlg = d;
    brote = b;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;

    // Recuperacion de listas
    listaTNotificador = (Lista) hsCompleta.get("TNOTIFICADOR");
    listaMecProbTransm = (Lista) hsCompleta.get("MECTRANS");

    try {
      jbInit();
      Inicializar(modoOperacion);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor

  void jbInit() throws Exception {

    Color cObligatorio = new Color(255, 255, 150);

    this.setSize(new Dimension(755, 280));

    xYLayout.setHeight(270); //270
    xYLayout.setWidth(735);

    lTNotificador.setText("Tipo Notificador:");
    lPersRiesgo.setText("Personas a riesgo:");
    lExp.setText("Expuestos:");
    lIngHosp.setText("Ingrs. Hospitalarios:");
    lEnf.setText("Enfermos:");
    lDefun.setText("Defunciones:");
    lFecExp.setText("F-H. Exposici�n:");
    lMecTrans.setText("Mecanismo probable de transmisi�n:");
    lObserv.setText("Observaciones:");
    lMani.setText("N� de manipuladores:");
    lEstVac.setText("Estado Vacunal:");
    lVacNoEnf.setText("Vacunados no enfermos:");
    lVacEnf.setText("Vacunados enfermos:");
    lNoVacNoEnf.setText("No vacunados no enfermos:");
    lNoVacEnf.setText("No vacunados enfermos:");

    //Configuro los combos de Tipo de Notif y Mec prob de transmisi�n
    choTNotif = new ChoiceCDDS(true, true, true, "CD_TNOTIF", "DS_TNOTIF",
                               listaTNotificador);
    choMecTrans = new ChoiceCDDS(true, true, true, "CD_TRANSMIS", "DS_TRANSMIS",
                                 listaMecProbTransm);
    choTNotif.writeData();
    choTNotif.setBackground(cObligatorio);
    choMecTrans.writeData();

    this.setLayout(xYLayout);

    this.add(lTNotificador, new XYConstraints(MARGENIZQ, MARGENSUP, 100, ALTO));
    this.add(choTNotif,
             new XYConstraints(MARGENIZQ + 100 + INTERHOR, MARGENSUP, 200, ALTO));

    this.add(lPersRiesgo,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + 5 + INTERVERT, 107,
                               ALTO));

    this.add(lExp,
             new XYConstraints(MARGENIZQ + 107 + INTERHOR - 5,
                               MARGENSUP + ALTO + 5 + INTERVERT, 60, ALTO));
    this.add(txtExp,
             new XYConstraints(MARGENIZQ + 107 + INTERHOR - 5 + 60 + 5 + 2,
                               MARGENSUP + ALTO + 5 + INTERVERT, 50, ALTO));

    this.add(lEnf,
             new XYConstraints(MARGENIZQ + 107 + 2 * INTERHOR - 5 + 60 + 50 + 5 +
                               2, MARGENSUP + ALTO + 5 + INTERVERT, 60, ALTO));
    this.add(txtEnf,
             new XYConstraints(MARGENIZQ + 107 + 2 * INTERHOR - 5 + 60 + 60 +
                               50 + 2 * 5 + 2, MARGENSUP + ALTO + 5 + INTERVERT,
                               50, ALTO));

    this.add(lIngHosp,
             new XYConstraints(MARGENIZQ + 107 + 3 * INTERHOR - 5 + 60 + 60 +
                               2 * 50 + 2 * 5 + 2,
                               MARGENSUP + ALTO + 5 + INTERVERT, 115, ALTO));
    this.add(txtIH,
             new XYConstraints(MARGENIZQ + 107 + 3 * INTERHOR - 5 + 60 + 60 +
                               2 * 50 + 115 + 3 * 5 + 2,
                               MARGENSUP + ALTO + 5 + INTERVERT, 50, ALTO));

    this.add(lDefun,
             new XYConstraints(MARGENIZQ + 107 + 4 * INTERHOR - 5 + 60 + 60 +
                               3 * 50 + 115 + 3 * 5 + 2,
                               MARGENSUP + ALTO + 5 + INTERVERT, 73, ALTO));
    this.add(txtDefun,
             new XYConstraints(MARGENIZQ + 107 + 4 * INTERHOR - 5 + 60 + 60 +
                               3 * 50 + 115 + 73 + 4 * 5 + 2,
                               MARGENSUP + ALTO + 5 + INTERVERT, 50, ALTO));

    this.add(lFecExp,
             new XYConstraints(MARGENIZQ, MARGENSUP + 2 * ALTO + 5 + 2 * INTERVERT,
                               90, ALTO));
    this.add(fechaExp,
             new XYConstraints(MARGENIZQ + 90 + INTERHOR,
                               MARGENSUP + 2 * ALTO + 5 + 2 * INTERVERT, 90,
                               ALTO));
    this.add(horaExp,
             new XYConstraints(MARGENIZQ + 2 * 90 + INTERHOR + 8,
                               MARGENSUP + 2 * ALTO + 5 + 2 * INTERVERT, 60,
                               ALTO));

    this.add(lMecTrans,
             new XYConstraints(MARGENIZQ + 2 * 90 + 2 * INTERHOR + 11 + 60,
                               MARGENSUP + 2 * ALTO + 5 + 2 * INTERVERT, 207,
                               ALTO));
    this.add(choMecTrans,
             new XYConstraints(MARGENIZQ + 2 * 90 + 3 * INTERHOR + 8 + 60 + 207,
                               MARGENSUP + 2 * ALTO + 5 + 2 * INTERVERT, 189,
                               ALTO));

    this.add(lObserv,
             new XYConstraints(MARGENIZQ, MARGENSUP + 3 * ALTO + 5 + 3 * INTERVERT,
                               90, ALTO));
    this.add(textAreaObserv,
             new XYConstraints(MARGENIZQ,
                               MARGENSUP + 4 * ALTO + 5 + 4 * INTERVERT, 280,
                               200));

    this.add(lMani,
             new XYConstraints(MARGENIZQ + 300 + 6 * INTERHOR,
                               MARGENSUP + 4 * ALTO + 5 + 4 * INTERVERT, 120,
                               ALTO));
    this.add(txtMani,
             new XYConstraints(MARGENIZQ + 300 + 118 + 7 * INTERHOR,
                               MARGENSUP + 4 * ALTO + 5 + 4 * INTERVERT, 50,
                               ALTO));

    this.add(lEstVac,
             new XYConstraints(MARGENIZQ + 300 + 6 * INTERHOR,
                               MARGENSUP + 4 * ALTO + 5 + 4 * INTERVERT, 90,
                               ALTO));

    this.add(lVacNoEnf,
             new XYConstraints(MARGENIZQ + 300 + 3 * INTERHOR,
                               MARGENSUP + 5 * ALTO + 5 + 5 * INTERVERT, 154,
                               ALTO));
    this.add(txtVacNoEnf,
             new XYConstraints(MARGENIZQ + 300 + 4 * INTERHOR + 154,
                               MARGENSUP + 5 * ALTO + 5 + 5 * INTERVERT, 50,
                               ALTO));

    this.add(lVacEnf,
             new XYConstraints(MARGENIZQ + 300 + 3 * INTERHOR,
                               MARGENSUP + 6 * ALTO + 5 + 6 * INTERVERT, 154,
                               ALTO));
    this.add(txtVacEnf,
             new XYConstraints(MARGENIZQ + 300 + 4 * INTERHOR + 154,
                               MARGENSUP + 6 * ALTO + 5 + 6 * INTERVERT, 50,
                               ALTO));

    this.add(lNoVacNoEnf,
             new XYConstraints(MARGENIZQ + 300 + 3 * INTERHOR,
                               MARGENSUP + 7 * ALTO + 5 + 7 * INTERVERT, 154,
                               ALTO));
    this.add(txtNoVacNoEnf,
             new XYConstraints(MARGENIZQ + 300 + 4 * INTERHOR + 154,
                               MARGENSUP + 7 * ALTO + 5 + 7 * INTERVERT, 50,
                               ALTO));

    this.add(lNoVacEnf,
             new XYConstraints(MARGENIZQ + 300 + 3 * INTERHOR,
                               MARGENSUP + 8 * ALTO + 5 + 8 * INTERVERT, 154,
                               ALTO));
    this.add(txtNoVacEnf,
             new XYConstraints(MARGENIZQ + 300 + 4 * INTERHOR + 154,
                               MARGENSUP + 8 * ALTO + 5 + 8 * INTERVERT, 50,
                               ALTO));

    // Visibilidad de algunos componentes segun el grupo
    String grupo = "";
    switch (modoEntrada) {
      case ALTA: // Grupo TIA por defecto
        setVisibleToxAlim(false);
        setVisibleVacunable(false);
        break;
      case MODIFICACION:
      case BAJA:
      case CONSULTA:
        grupo = brote.getString("CD_GRUPO");
        if (grupo.equals("0")) {
          setVisibleToxAlim(true);
          setVisibleVacunable(false);
        }
        else if (grupo.equals("1")) {
          setVisibleToxAlim(false);
          setVisibleVacunable(true);
        }
        else {
          setVisibleToxAlim(false);
          setVisibleVacunable(false);
        }
        break;
    } // Fin switch
  } //end jbinit

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    if (dlg != null) {
      dlg.Inicializar(modoOperacion);
    }
  } // Fin Inicializar()

  public void InicializarDesdeFuera(int mod) {
    modoOperacion = mod;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      // modo espera
      case CInicializar.ESPERA:
        enableControls(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case ALTA:
            enableControls(true);
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            break;
          case MODIFICACION:
            enableControls(true);
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            break;
          case BAJA:
          case CONSULTA:
            this.setEnabled(false);
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            break;
        }
    }
  } //end inicializar

  public void enableControls(boolean state) {
    choTNotif.setEnabled(state);

    txtExp.setEnabled(state);
    txtEnf.setEnabled(state);
    txtIH.setEnabled(state);
    txtDefun.setEnabled(state);

    fechaExp.setEnabled(state);
    horaExp.setEnabled(state);

    choMecTrans.setEnabled(state);

    textAreaObserv.setEnabled(state);

    txtMani.setEnabled(state);

    txtVacNoEnf.setEnabled(state);
    txtVacEnf.setEnabled(state);
    txtNoVacNoEnf.setEnabled(state);
    txtNoVacEnf.setEnabled(state);
  } // Fin enableControls()

  public void rellenarDatos(Data dtReg) {

    //Tipo Notificador
    choTNotif.selectChoiceElement(dtReg.getString("CD_TNOTIF"));
    choMecTrans.selectChoiceElement(dtReg.getString("CD_TRANSMIS"));

    txtExp.setText(dtReg.getString("NM_EXPUESTOS"));
    txtEnf.setText(dtReg.getString("NM_ENFERMOS"));
    txtIH.setText(dtReg.getString("NM_INGHOSP"));
    txtDefun.setText(dtReg.getString("NM_DEFUNCION"));

    // Fecha/Hora de la notificaci�n
    String f = ( (String) dtReg.getString("FC_EXPOSICION")).trim();
    if (!f.equals("")) {
      fechaExp.setText(f.substring(0, f.indexOf(' ', 0)));
      horaExp.setText( (f.substring(f.indexOf(' ', 0) + 1)).substring(0, 5));
    }

    if (dtReg.getString("CD_GRUPO").equals("0")) {
      setVisibleToxAlim(true);
      setVisibleVacunable(false);
      txtMani.setText(dtReg.getString("NM_MANIPUL"));
    }
    else if (dtReg.getString("CD_GRUPO").equals("1")) {
      setVisibleVacunable(true);
      setVisibleToxAlim(false);
      txtVacNoEnf.setText(dtReg.getString("NM_VACNENF"));
      txtVacEnf.setText(dtReg.getString("NM_VACENF"));
      txtNoVacNoEnf.setText(dtReg.getString("NM_NVACNENF"));
      txtNoVacEnf.setText(dtReg.getString("NM_NVACENF"));
    }
    else {
      setVisibleVacunable(false);
      setVisibleToxAlim(false);
    }

    textAreaObserv.setText(dtReg.getString("DS_OBSERV"));
  } //end rellenardatos

  public void recogerDatos(Data dtDev) {

    dtDev.put("CD_TNOTIF", choTNotif.getChoiceCD());
    dtDev.put("CD_TRANSMIS", choMecTrans.getChoiceCD());

    dtDev.put("NM_EXPUESTOS", txtExp.getText().trim());
    dtDev.put("NM_ENFERMOS", txtEnf.getText().trim());
    dtDev.put("NM_INGHOSP", txtIH.getText().trim());
    dtDev.put("NM_DEFUNCION", txtDefun.getText().trim());

    String fExp = "";
    if (!fechaExp.getText().equals("")) {
      fExp = fechaExp.getText().trim();
      if (!horaExp.getText().equals("")) {
        fExp += " " + horaExp.getText().trim() + ":00";
      }
      else {
        fExp += " 00:00:00";
      }
    }
    dtDev.put("FC_EXPOSICION", fExp);

    if (txtMani.isVisible()) {
      dtDev.put("NM_MANIPUL", txtMani.getText().trim());
    }
    if (txtVacNoEnf.isVisible()) {
      dtDev.put("NM_VACNENF", txtVacNoEnf.getText().trim());
      dtDev.put("NM_VACENF", txtVacEnf.getText().trim());
      dtDev.put("NM_NVACNENF", txtNoVacNoEnf.getText().trim());
      dtDev.put("NM_NVACENF", txtNoVacEnf.getText().trim());
    }

    dtDev.put("DS_OBSERV", textAreaObserv.getText().trim());
  } //end recogerdatos

  public boolean validarDatos() {

    // Fecha debe ser una fecha v�lida
    if (fechaExp.getText().trim().length() != 0) {
      fechaExp.ValidarFecha();
      if (fechaExp.getValid().equals("N")) {
        this.getApp().showError("Fecha incorrecta");
        fechaExp.requestFocus();
        return (false);
      }
    }

    // La Hora  debe ser una fecha v�lida
    if (horaExp.getText().trim().length() != 0) {
      horaExp.ValidarFecha();
      if (horaExp.getValid().equals("N")) {
        this.getApp().showError("Hora incorrecta");
        horaExp.requestFocus();
        return (false);
      }
    }

    // Tipo de notificador
    if (choTNotif.getChoiceCD().equals("")) {
      this.getApp().showError("El tipo de notificador es un campo obligatorio");
      choTNotif.requestFocus();
      return (false);
    }

    // Los expuestos deben ser la suma de los enfermos + ingresados + espichados.

    int a = 0;
    int b = 0;
    int c = 0;
    int d = 0;

    if (!txtExp.getText().equals("")) {
      a = Integer.parseInt(txtExp.getText());

    }
    if (!txtEnf.getText().equals("")) {
      b = Integer.parseInt(txtEnf.getText());

    }
    if (!txtIH.getText().equals("")) {
      c = Integer.parseInt(txtIH.getText());

    }
    if (!txtDefun.getText().equals("")) {
      d = Integer.parseInt(txtDefun.getText());

    }
    if (a < b + c + d) {
      this.getApp().showError("El n� de expuestos ha de ser mayor/igual que la suma de enfermos, ingresados y difuntos.");
      return (false);
    }
    return true;
  } //end validardatos

  public void noVisible() {
    lMani.setVisible(false);
    txtMani.setVisible(false);
    lEstVac.setVisible(false);
    lVacNoEnf.setVisible(false);
    txtVacNoEnf.setVisible(false);
    lVacEnf.setVisible(false);
    txtVacEnf.setVisible(false);
    lNoVacNoEnf.setVisible(false);
    txtNoVacNoEnf.setVisible(false);
    lNoVacEnf.setVisible(false);
    txtNoVacEnf.setVisible(false);
  } //end   noVisible

  public void setVisibleToxAlim(boolean state) {
    lMani.setVisible(state);
    txtMani.setVisible(state);
  } //end setVisibleVacunable

  public void setVisibleVacunable(boolean state) {
    lEstVac.setVisible(state);
    lVacNoEnf.setVisible(state);
    txtVacNoEnf.setVisible(state);
    lVacEnf.setVisible(state);
    txtVacEnf.setVisible(state);
    lNoVacNoEnf.setVisible(state);
    txtNoVacNoEnf.setVisible(state);
    lNoVacEnf.setVisible(state);
    txtNoVacEnf.setVisible(state);
  } //end setVisibleToxAlim

  /****************** Funciones Auxiliares *****************/

  public void cambiarBIF(Data newBIF) {
    brote = newBIF;
  } // Fin cambiarAlerta()

  public void setModoEntrada(int m) {
    modoEntrada = m;
  } // Fin setModoEntrada()

} //end class
