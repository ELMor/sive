/**
 * Clase: DAPanSup
 * Hereda: CPanel
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 10/05/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Alertas
 * Descripcion: Panel superior del di�logo de alta/mod/baja de una alerta
 */

package brotes.cliente.dapansup;

import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.Vector;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.CHora;
import brotes.cliente.c_componentes.ChoiceCDDS;
import brotes.cliente.c_componentes.DatoRecogible;
import brotes.cliente.c_comuncliente.SincrEventos;
import brotes.cliente.diaalerta.DiaAlerta;
import capp2.CApp;
import capp2.CContextHelp;
import capp2.CInicializar;
import capp2.CListaValores;
import capp2.CPanel;
import capp2.CTexto;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class DAPanSup
    extends CPanel
    implements CInicializar, DatoRecogible {

  /*************** Datos propios del panel ****************/

  // Parametros de entrada
  CApp applet = null;
  DiaAlerta dlg = null;
  Data alerta = null;
  Lista lGrupo = null; // Lista Grupos
  Lista lSit = null; // Lista Situaciones

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  // Constantes del modoEntrada
  final int ALTA = 0;
  final int MODIFICACION = 1;
  final int BAJA = 2;
  final int CONSULTA = 3;

  // Nombres de servlets
  public final static String srvQT = "servlet/SrvQueryTool";

  /****************** Componentes del panel ********************/

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Organizaci�n del panel
  private XYLayout xYLayout1 = new XYLayout();

  // �rea
  Label lblArea = null;
  CTexto txtCodArea = null;
  ButtonControl btnArea = null;

  // Ano/Cod Alerta
  Label lblAno = null;
  CTexto txtAno = null;
  CTexto txtCod = null;
  Label lblDesc = null;
  CTexto txtDesc = null;

  // Fecha/Hora de la notificaci�n
  Label lblFNotif = new Label();
  // corregido para barras autom�ticas. (ARS 22-05-01)
  fechas.CFecha txtFNotif = new fechas.CFecha("S");
  CHora txtHNotif = new CHora("S");

  // Grupo de brote
  Label lblGrupo = null;
  ChoiceCDDS choGrupo = null;

  // Situaci�n del brote
  Label lblSit = null;
  ChoiceCDDS choSit = null;

  // Escuchadores (inicializados a null)

  // Botones
  DAPanSupActionAdapter actionAdapter = null;

  // P�rdidas de foco
  DAPanSupFocusAdapter focusAdapter = null;

  // Constructor
  // @param a: CApp
  // @param modo: modo de entrada al di�logo ()
  // @param listaGrupo: Lista Datas con CD y DS de los grupos
  // @param listaSit: Lista Datas con CD y DS de las situaciones
  public DAPanSup(CApp a, DiaAlerta d, Data al, int modo, Lista listaGrupo,
                  Lista listaSit) {

    super(a);
    applet = a;
    dlg = d;
    alerta = al;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    lGrupo = listaGrupo;
    lSit = listaSit;

    // Eliminamos los estados en los que no puede estar una alerta como tal
    if ( (modoEntrada == ALTA) ||
        (modoEntrada == MODIFICACION &&
         (alerta.getString("CD_SITALERBRO").equals("0") ||
          alerta.getString("CD_SITALERBRO").equals("1"))
         )
        ) {
      lSit = RegenerarListaSit(listaSit);

    }
    try {
      // Inicializaci�n
      jbInit();
      Inicializar();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  Lista RegenerarListaSit(Lista l) {
    Lista lRes = new Lista();
    for (int i = 0; i < l.size(); i++) {
      String sSit = ( (Data) l.elementAt(i)).getString("CD_SITALERBRO");
      if (sSit.equals("0") || sSit.equals("1")) {
        lRes.addElement( (Data) l.elementAt(i));
      }
    } // Fin for

    return lRes;
  } // Fin RegenerarListaSit()

  // Inicia el aspecto del panel superior de alertas
  public void jbInit() throws Exception {
    // Variables locales
    boolean nulo, bCD, bDS;
    String sCD, sDS;
    Color cObligatorio = new Color(255, 255, 150);

    // Un poco elevado
    setBorde(true);

    // Carga de imagenes
    getApp().getLibImagenes().put(constantes.imgLUPA);
    getApp().getLibImagenes().CargaImagenes();

    // Escuchadores: botones y p�rdidas de foco
    actionAdapter = new DAPanSupActionAdapter(this);
    focusAdapter = new DAPanSupFocusAdapter(this);

    // Organizacion del panel
    this.setSize(new Dimension(725, 80));
    this.setLayout(xYLayout1);
    xYLayout1.setWidth(725);
    xYLayout1.setHeight(80);

    // �rea
    lblArea = new Label("�rea:");
    txtCodArea = new CTexto(2);
    txtCodArea.setName("Area");
    txtCodArea.addFocusListener(focusAdapter);
    txtCodArea.setBackground(cObligatorio);
    btnArea = new ButtonControl();
    btnArea.setImage(this.getApp().getLibImagenes().get(constantes.imgLUPA));
    btnArea.setActionCommand("Area");
    btnArea.addActionListener(actionAdapter);
    if (modoEntrada == ALTA) {
      txtCodArea.setText(applet.getParametro("CD_NIVEL1_DEFECTO"));

      // Ano/Cod Alerta
    }
    lblAno = new Label("A�o-C�digo Alerta:");
    txtAno = new CTexto(4);
    txtAno.setBackground(cObligatorio);
    txtCod = new CTexto(7);
    txtCod.setBackground(cObligatorio);
    lblDesc = new Label("Descripci�n:");
    txtDesc = new CTexto(50);
    txtDesc.setBackground(cObligatorio);
    if (modoEntrada == ALTA) {
      int iYearAct = 0;
      Calendar cal = Calendar.getInstance();
      iYearAct = cal.get(cal.YEAR);

      if (!applet.getParametro("ANYO_DEFECTO").equals("")) {
        txtAno.setText(applet.getParametro("ANYO_DEFECTO"));
      }
      else {
        txtAno.setText(new Integer(iYearAct).toString());
      }
    }

    // Fecha/Hora de la notificaci�n
    lblFNotif = new Label("F-H. Notificaci�n:");
    txtFNotif = new fechas.CFecha("S"); // Corregido 22-05-01 (ARS)
    txtHNotif = new CHora("S");

    // Tool Tips
    new CContextHelp("�reas", btnArea);

    // Grupo de brote y Situaci�n del brote
    nulo = true;
    bCD = false;
    bDS = true;
    sCD = "CD_GRUPO";
    sDS = "DS_GRUPO";
    lblGrupo = new Label("Grupo:");
    choGrupo = new ChoiceCDDS(nulo, bCD, bDS, sCD, sDS, lGrupo);
    choGrupo.setBackground(cObligatorio);
    choGrupo.writeData();
    nulo = true;
    bCD = false;
    bDS = true;
    sCD = "CD_SITALERBRO";
    sDS = "DS_SITALERBRO";
    lblSit = new Label("Situaci�n:");
    choSit = new ChoiceCDDS(nulo, bCD, bDS, sCD, sDS, lSit);
    choSit.setBackground(cObligatorio);
    choSit.writeData();

    // Adici�n de componentes al di�logo
    int dVert = 20;

    this.add(lblArea, new XYConstraints(15, 15, 30, dVert));
    this.add(txtCodArea, new XYConstraints(55, 15, 35, dVert));
    this.add(btnArea, new XYConstraints(100, 15, dVert + 5, dVert + 5));

    this.add(lblAno, new XYConstraints(145, 15, 105, dVert));
    this.add(txtAno, new XYConstraints(260, 15, 45, dVert));
    this.add(txtCod, new XYConstraints(315, 15, 75, dVert));
    this.add(lblDesc, new XYConstraints(400, 15, 68, dVert));
    this.add(txtDesc, new XYConstraints(475, 15, 225, dVert));

    this.add(lblFNotif, new XYConstraints(15, 45, 92, dVert));
    this.add(txtFNotif, new XYConstraints(110, 45, 80, dVert));
    this.add(txtHNotif, new XYConstraints(195, 45, 45, dVert));
    this.add(lblGrupo, new XYConstraints(250, 45, 38, dVert));
    this.add(choGrupo, new XYConstraints(295, 45, 155, dVert));
    this.add(lblSit, new XYConstraints(465, 45, 53, dVert));
    this.add(choSit, new XYConstraints(530, 45, 170, dVert));

    doLayout();
  } // Fin jbInit()

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    if (dlg != null) {
      dlg.Inicializar(modoOperacion);
    }
  } // Fin Inicializar()

  public void InicializarDesdeFuera(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicia()

  public void Inicializar() {
    switch (modoOperacion) {
      // Modo espera
      case CInicializar.ESPERA:
        enableControls(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // Modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case ALTA:
            enableControls(true);
            break;
          case MODIFICACION:
            int sit = new Integer(alerta.getString("CD_SITALERBRO")).intValue();
            switch (sit) {
              case 0:
                enableControls(true);
                break;
              case 1:
              case 2:
              case 3:
              case 4:
                enableControls(false);

                // Situaci�n del brote en estado 1 habilitado
                if (sit == 1) {
                  choSit.setEnabled(true);

                }
                break;
            }

            break;
          case BAJA:
          case CONSULTA:
            enableControls(false);
            break;
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    } // Fin switch
  } // Fin Inicializar()

  public void enableControls(boolean state) {
    // �rea
    txtCodArea.setEnabled(state);
    btnArea.setEnabled(state);

    // Ano/Cod Alerta
    if (modoEntrada == ALTA) {
      txtAno.setEnabled(true);
    }
    else {
      txtAno.setEnabled(false);
    }
    txtCod.setEnabled(false);
    txtDesc.setEnabled(state);

    // Fecha/Hora de la notificaci�n
    txtFNotif.setEnabled(state);
    txtHNotif.setEnabled(state);

    // Grupo de brote
    choGrupo.setEnabled(state);

    // Situaci�n del brote
    choSit.setEnabled(state);

  } // Fin enableControls()

  /********** Sincronizador de Eventos ******************/

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  /********** Funciones Interfaz DatoRecogible *****************/

  public void rellenarDatos(Data d) {
    // �rea
    txtCodArea.setText( (String) d.getString("CD_NIVEL_1"));

    // Ano/Cod Alerta
    txtAno.setText( (String) d.getString("CD_ANO"));
    txtCod.setText( (String) d.getString("NM_ALERBRO"));
    txtDesc.setText( (String) d.getString("DS_ALERTA"));

    // Fecha/Hora de la notificaci�n
    String f = ( (String) d.getString("FC_FECHAHORA")).trim();
    if (!f.equals("")) {
      txtFNotif.setText(f.substring(0, f.indexOf(' ', 0)));
      txtHNotif.setText( (f.substring(f.indexOf(' ', 0) + 1)).substring(0, 5));
    }

    // Grupo de brote
    choGrupo.selectChoiceElement( (String) d.getString("CD_GRUPO"));

    // Situaci�n del brote
    choSit.selectChoiceElement( (String) d.getString("CD_SITALERBRO"));
  } // Fin rellenarDatos()

  public void recogerDatos(Data d) {
    d.put("CD_NIVEL_1", txtCodArea.getText().trim());
    d.put("CD_ANO", txtAno.getText().trim());
    d.put("NM_ALERBRO",
          alerta.getString("NM_ALERBRO") /*txtCod.getText().trim()*/);
    d.put("DS_ALERTA", txtDesc.getText().trim());
    String fExp = "";
    if (!txtFNotif.getText().equals("")) {
      fExp = txtFNotif.getText().trim();
      if (!txtHNotif.getText().equals("")) {
        fExp += " " + txtHNotif.getText().trim() + ":00";
      }
      else {
        fExp += " 00:00:00";
      }
    }
    d.put("FC_FECHAHORA", fExp);
    d.put("CD_GRUPO", choGrupo.getChoiceCD());
    d.put("CD_SITALERBRO", choSit.getChoiceCD());
  } // Fin recogerDatos()

  public boolean validarDatos() {
    /******* Campos obligatorios ********/

    // �rea
    if (txtCodArea.getText().equals("")) {
      this.getApp().showError("El �rea es un campo obligatorio");
      txtCodArea.requestFocus();
      return (false);
    }

    // Ano/Cod Alerta
    if (txtAno.getText().equals("")) {
      this.getApp().showError("El a�o/c�digo es un campo obligatorio");
      txtAno.requestFocus();
      return (false);
    }
    if (txtDesc.getText().equals("")) {
      this.getApp().showError("La descripci�n es un campo obligatorio");
      txtDesc.requestFocus();
      return (false);
    }

    // Fecha/Hora de la notificaci�n
    if (txtFNotif.getText().equals("")) {
      this.getApp().showError("La fecha es un campo obligatorio");
      txtFNotif.requestFocus();
      return (false);
    }
    if (txtHNotif.getText().equals("")) {
      this.getApp().showError("La hora es un campo obligatorio");
      txtHNotif.requestFocus();
      return (false);
    }

    // Grupo de brote
    if (choGrupo.getChoiceCD().equals("")) {
      this.getApp().showError("El grupo es un campo obligatorio");
      choGrupo.requestFocus();
      return (false);
    }

    // Situaci�n del brote
    if (choSit.getChoiceCD().equals("")) {
      this.getApp().showError("La situaci�n es un campo obligatorio");
      choSit.requestFocus();
      return (false);
    }

    /******* Fin Campos obligatorios ********/

    /*********** Tipos de datos ************/

    // Fecha Notificaci�n debe ser una fecha v�lida
    txtFNotif.ValidarFecha();
    if (txtFNotif.getValid().equals("N")) {
      this.getApp().showError("Fecha incorrecta");
      txtFNotif.requestFocus();
      return (false);
    }

    // La Hora Notificaci�n debe ser una fecha v�lida
    txtHNotif.ValidarFecha();
    if (txtHNotif.getValid().equals("N")) {
      this.getApp().showError("Hora incorrecta");
      txtHNotif.requestFocus();
      return (false);
    }

    /*********** Fin Tipos de datos ************/

    return true;
  } // Fin validarDatos()

  /**************** Funciones de acceso a vars. del panel *******/
  public void cambiarAlerta(Data newAlerta) {
    alerta = newAlerta;
    txtCod.setText(alerta.getString("NM_ALERBRO"));
  } // Fin cambiarAlerta()

  public void setModoEntrada(int m) {
    modoEntrada = m;
  }

  /****************** Manejadores de Eventos *****************/

  void btnAreaActionPerformed() {
    // Llamada al di�logo de obtenci�n de �reas (Maite)
    System.out.println("Atiende evento de pulsaci�n de bot�n");

    seleccionarAreas();
  } // Fin btnAreaActionPerformed()

  private void seleccionarAreas() {
    CListaValores clv = null;
    Vector vCod = null;
    QueryTool2 qt = new QueryTool2(); ;

    // SELECT CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1
    //  FROM SIVE_NIVEL1_S
    //  WHERE CD_NIVEL_1 IN (?,...)

    qt.putName("SIVE_NIVEL1_S");
    qt.putType("CD_NIVEL_1", QueryTool2.STRING);
    qt.putType("DS_NIVEL_1", QueryTool2.STRING);
    if (this.getApp().getParametro("PERFIL").equals("3")) {
      qt = anadirRestriccion(qt);
    }

    // Campos que se muestran en la lista de valores
    vCod = new Vector();
    vCod.addElement("CD_NIVEL_1");
    vCod.addElement("DS_NIVEL_1");

    // Lista de valores
    clv = new CListaValores(this.getApp(),
                            "Alertas",
                            qt,
                            vCod);
    clv.show();

    // Recupera el �rea seleccionada
    if (clv.getSelected() != null) {
      txtCodArea.setText( ( (String) clv.getSelected().getString("CD_NIVEL_1")));
    }

    clv = null;
  } // Fin seleccionarAreas

  private QueryTool2 anadirRestriccion(QueryTool2 qt) {
    String subquery = "";
    Vector vSubquery = new Vector();

    String autoriza = this.getApp().getParametro("CD_NIVEL_1_AUTORIZACIONES");

    // Si tiene autorizaciones para alg�n �rea
    if (! (autoriza.equals(""))) {
      Data dtAutoriza = new Data();
      StringTokenizer stAutor = new StringTokenizer(autoriza, ",", false);
      for (; stAutor.hasMoreTokens(); ) {
        String autorizacion = stAutor.nextToken();
        Data dtAutVect = new Data();
        subquery += "?,";
        dtAutVect.put(new Integer(QueryTool2.STRING), autorizacion);
        vSubquery.addElement(dtAutVect);
      }

      // Quita la ultima coma
      subquery = subquery.substring(0, subquery.length() - 1);

      // Establecemos la subquery y sus (tipos,valores)
      qt.putSubquery("CD_NIVEL_1", subquery);
      qt.putVectorSubquery("CD_NIVEL_1", vSubquery);
    } // Fin si existen autorizaciones.

    return qt;
  } // Fin anadirSubquery

  void txtAreaFocusLost() {
    String area = txtCodArea.getText().trim();

    if (area.equals("")) {
      return;
    }

    // Perfil 3 (Restricci�n de �reas)
    if ( (this.getApp().getParametro("PERFIL")).equals("3")) {
      boolean b = buscarAreaUsuario(area);
      if (b == false) {
        this.getApp().showAdvise("�rea no existe o usuario no autorizado");
        txtCodArea.setText("");
      }
    }
    else { // Perfil 2 (CA)
      boolean b = buscarAreaGlobal(area);
      if (b == false) {
        this.getApp().showAdvise("�rea no existe");
        txtCodArea.setText("");
      }
    }
  } // Fin txtAreaFocusLost()

  private boolean buscarAreaGlobal(String cdArea) {
    boolean resul = false;
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool2 qt = new QueryTool2(); ;
    try {
      qt.putName("SIVE_NIVEL1_S");
      qt.putType("CD_NIVEL_1", QueryTool2.STRING);
      qt.putWhereType("CD_NIVEL_1", QueryTool.STRING);
      qt.putWhereValue("CD_NIVEL_1", cdArea);
      qt.putOperator("CD_NIVEL_1", "=");
      p.addElement(qt);
      app.getStub().setUrl(srvQT);
      p1 = (Lista) app.getStub().doPost(1, p);
      if (p1.size() == 0) {
        resul = false;
      }
      else {
        resul = true;
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }

    return resul;
  } // Fin buscarAreaGlobal

  boolean buscarAreaUsuario(String cdArea) {
    boolean resul = false;
    boolean salir = false;
    String autoriza = this.getApp().getParametro("CD_NIVEL_1_AUTORIZACIONES");

    // si tiene autorizaciones para alg�n �rea.
    if (! (autoriza.equals(""))) {
      Data dtAutoriza = new Data();
      StringTokenizer stAutor = new StringTokenizer(autoriza, ",", false);
      for (; stAutor.hasMoreTokens() && salir == false; ) {
        String autorizacion = stAutor.nextToken();
        //si el �rea introducida es una de las autorizadas
        if (cdArea.equals(autorizacion)) {
          salir = true;
          resul = true;
        }
      } //end for
    } // Fin if tiene alguna autorizaci�n

    return resul;
  } // Fin buscarAreaUsuario()

} // Fin class DAPanSup

/******************* ESCUCHADORES **********************/

// Botones
class DAPanSupActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DAPanSup adaptee;
  ActionEvent evt;

  DAPanSupActionAdapter(DAPanSup adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if (evt.getActionCommand().equals("Area")) {
        adaptee.btnAreaActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  DAPanSupActionAdapter

// P�rdidas de foco
class DAPanSupFocusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  DAPanSup adaptee;
  FocusEvent evt;

  DAPanSupFocusAdapter(DAPanSup adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if ( ( (TextField) evt.getSource()).getName().equals("Area")) {
        adaptee.txtAreaFocusLost();
      }
      s.desbloquea(adaptee);
    }
  }
} // Fin clase DAPanSupFocusAdapter
