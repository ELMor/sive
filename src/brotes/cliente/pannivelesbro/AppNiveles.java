package brotes.cliente.pannivelesbro;

import java.util.Hashtable;

import capp2.CApp;
import capp2.CInicializar;

public class AppNiveles
    extends CApp {

  //int modoESPERA = 2;
  int modoALTA = 0;
  int modoMODIFICACION = 1;
  int modoBAJA = 2;
  int modoCONSULTA = 3;
  PanNivelesBro panel = null;
  Hashtable hash = new Hashtable();

  public void init() {
    super.init();
  }

  public void start() {
    setTitulo("PanNiveles");

    panel = new PanNivelesBro(this, null, modoALTA, null);
    VerPanel("", panel);
    panel.InicializarDesdeFuera(CInicializar.NORMAL);
  }
}