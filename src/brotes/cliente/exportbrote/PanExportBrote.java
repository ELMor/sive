package brotes.cliente.exportbrote;

import java.util.Calendar;
import java.util.Vector;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.FileDialog;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_comuncliente.BDatos;
import capp2.CApp;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CListaValores;
import capp2.CMessage;
import capp2.CPanel;
import capp2.CTexto;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import util_ficheros.EditorFichero;

public class PanExportBrote
    extends CPanel {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  XYLayout xyLayout = new XYLayout();

  //constantes para localizaciones
  final int MARGENIZQ = 25;
  final int MARGENSUP = 25;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;

  protected boolean fallo = false;

  //componentes q conforman el panel
  // Ano/Cod Brote
  Label lblAno = null;
  CEntero txtAno = null;
  CEntero txtCod = null;
  ButtonControl btnBrote = null;
  CTexto txtDesc = null;

  //a�adir car�cter de separaci�n
  Label lblCaracter = null;
  CTexto txtCaracter = null;
  //busqueda de directorio
  capp2.CFileName panFichero;

  ButtonControl btnAceptar = new ButtonControl();

  //vectores de Strings con los nombres de los campos
  Vector vCamposBro = new Vector();
  Vector vCamposProt = new Vector();
  Vector vCamposAgcausal = new Vector();
  Vector vCamposFaccontr = new Vector();
  Vector vCamposMedadopt = new Vector();
  Vector vCamposSintomas = new Vector();
  Vector vCamposDistedad = new Vector();
  Vector vCamposTataqali = new Vector();
  Vector vCamposMuestras = new Vector();
  Vector vCamposAlimimpl = new Vector();

  //vbles q guardan "" si su f�chero correspondiente no se ha creado y
  //el nombre en caso contrario
  String Brot = new String();
  String Prot = new String();
  String Agcausal = new String();
  String Faccontr = new String();
  String Medadopt = new String();
  String Sintomas = new String();
  String Distedad = new String();
  String Tataqali = new String();
  String Muestras = new String();
  String Alimimpl = new String();

  CApp apl = null;

  //pulsar una tecla
  PanExportBroteKeyListener keyListener = null;
  // Perdidas de foco
  PanExportBroteFocusAdapter focusAdapter = null;
  // Botones
  PanExportBroteActionAdapter actionAdapter = null;

  //constructor
  public PanExportBrote(CApp a) {
    super(a);
    apl = a;
    try {
      panFichero = new capp2.CFileName(a, FileDialog.SAVE, 2);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor

  void jbInit() throws Exception {
    final String imgAceptar = "images/aceptar.gif";
    final String imgLupa = "images/browser.gif";

    Color cObligatorio = new Color(255, 255, 150);

    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgLupa);
    this.getApp().getLibImagenes().CargaImagenes();

    //escuchadores
    actionAdapter = new PanExportBroteActionAdapter(this);
    keyListener = new PanExportBroteKeyListener(this);
    focusAdapter = new PanExportBroteFocusAdapter(this);

    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.addActionListener(actionAdapter);
//    btnAceptar.addActionListener(new PanExportBrote_btnAceptar_actionAdapter(this));

    // Ano/Cod Brote
    lblAno = new Label("A�o-C�digo Brote:");
    txtAno = new CEntero(4);
    txtAno.setBackground(cObligatorio);
    txtAno.addKeyListener(keyListener);
    txtAno.setName("ano");
    txtCod = new CEntero(7);
    txtCod.setBackground(cObligatorio);
    txtCod.setName("CodBrote");
    txtCod.addKeyListener(keyListener);
    txtCod.addFocusListener(focusAdapter);
    btnBrote = new ButtonControl();
    btnBrote.setImage(this.getApp().getLibImagenes().get(imgLupa));
    btnBrote.setActionCommand("Brote");
    btnBrote.addActionListener(actionAdapter);
    txtDesc = new CTexto(50);
    txtDesc.setEnabled(false);
    txtDesc.setEditable(false);
    lblCaracter = new Label("Car�cter de separaci�n entre campos:");
    txtCaracter = new CTexto(1);
    txtCaracter.setBackground(cObligatorio);
    txtCaracter.setText("$");

    //Introduzco el a�o q trae la aplicaci�n, o por defecto el actual
    int iYearAct = 0;
    Calendar cal = Calendar.getInstance();
    iYearAct = cal.get(cal.YEAR);
    if (! (this.getApp().getParametro("ANYO_DEFECTO").equals(""))) {
      String anoDef = this.getApp().getParametro("ANYO_DEFECTO");
      txtAno.setText(anoDef);
    }
    else {
      txtAno.setText(new Integer(iYearAct).toString());
    }

    xyLayout.setHeight(235);
    xyLayout.setWidth(526);
    this.setLayout(xyLayout);

    this.add(lblAno, new XYConstraints(MARGENIZQ, MARGENSUP, 105, ALTO));
    this.add(txtAno,
             new XYConstraints(MARGENIZQ + INTERHOR + 105, MARGENSUP, 45, ALTO));
    this.add(txtCod,
             new XYConstraints(MARGENIZQ + 2 * INTERHOR + 105 + 45, MARGENSUP,
                               75, ALTO));
    this.add(btnBrote,
             new XYConstraints(MARGENIZQ + 3 * INTERHOR + 105 + 45 + 75,
                               MARGENSUP, 25, ALTO));
    this.add(txtDesc,
             new XYConstraints(MARGENIZQ + 4 * INTERHOR + 105 + 45 + 75 + 25,
                               MARGENSUP, 175, ALTO));

    this.add(lblCaracter,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 225,
                               ALTO));
    this.add(txtCaracter,
             new XYConstraints(MARGENIZQ + INTERHOR + 225,
                               MARGENSUP + ALTO + INTERVERT, 30, ALTO));

    this.add(panFichero,
             new XYConstraints(MARGENIZQ - 12,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 425, 40));
    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 425 - 88 - 12,
                               MARGENSUP + 2 * ALTO + 3 * INTERVERT + 40, 88,
                               29));

    //configuro el vector vCamposBro:DataCampos
    rellenarVectorBro();
    rellenarVectorProt();
    rellenarVectorAgcausal();
    rellenarVectorFaccontr();
    rellenarVectorMedadopt();
    rellenarVectorSintomas();
    rellenarVectorDistedad();
    rellenarVectorTataqali();
    rellenarVectorMuestras();
    rellenarVectorAlimimpl();
  } //end jbinit

  void rellenarVectorProt() {
    vCamposProt.addElement("CD_ANO");
    vCamposProt.addElement("NM_ALERBRO");
    vCamposProt.addElement("CD_TSIVE");
    vCamposProt.addElement("CD_MODELO");
    vCamposProt.addElement("NM_LIN");
    vCamposProt.addElement("CD_PREGUNTA");
    vCamposProt.addElement("DS_RESPUESTA");
  }

  void rellenarVectorAgcausal() {
    vCamposAgcausal.addElement("NM_SECAGB");
    vCamposAgcausal.addElement("CD_ANO");
    vCamposAgcausal.addElement("NM_ALERBRO");
    vCamposAgcausal.addElement("CD_GRUPO");
    vCamposAgcausal.addElement("CD_AGENTC");
    vCamposAgcausal.addElement("IT_CONFIRMADO");
    vCamposAgcausal.addElement("DS_OBSERV");
    vCamposAgcausal.addElement("DS_MASAGCAUSAL");
  }

  void rellenarVectorFaccontr() {
    vCamposFaccontr.addElement("CD_GRUPO");
    vCamposFaccontr.addElement("CD_FACCONT");
    vCamposFaccontr.addElement("DS_FACCON");
    vCamposFaccontr.addElement("IT_MASFACCONT");
    vCamposFaccontr.addElement("IT_REPE");
  }

  void rellenarVectorMedadopt() {
    vCamposMedadopt.addElement("CD_GRUPO");
    vCamposMedadopt.addElement("CD_MEDIDA");
    vCamposMedadopt.addElement("DS_MEDIDA");
    vCamposMedadopt.addElement("IT_INFADIC");
    vCamposMedadopt.addElement("IT_REPE");
  }

  void rellenarVectorSintomas() {
    vCamposSintomas.addElement("CD_SINTOMA");
    vCamposSintomas.addElement("CD_ANO");
    vCamposSintomas.addElement("NM_ALERBRO");
    vCamposSintomas.addElement("NM_CASOS");
    vCamposSintomas.addElement("DS_MASINF");
  }

  void rellenarVectorDistedad() {
    vCamposDistedad.addElement("NM_DISTGEDAD");
    vCamposDistedad.addElement("CD_ANO");
    vCamposDistedad.addElement("NM_ALERBRO");
    vCamposDistedad.addElement("IT_EDAD");
    vCamposDistedad.addElement("CD_GEDAD");
    vCamposDistedad.addElement("DIST_EDAD_I");
    vCamposDistedad.addElement("DIST_EDAD_F");
    vCamposDistedad.addElement("NM_V_EXP");
    vCamposDistedad.addElement("NM_M_EXP");
    vCamposDistedad.addElement("NM_NC_EXP");
    vCamposDistedad.addElement("NM_V_ENF");
    vCamposDistedad.addElement("NM_M_ENF");
    vCamposDistedad.addElement("NM_NC_ENF");
    vCamposDistedad.addElement("NM_V_HOS");
    vCamposDistedad.addElement("NM_M_HOS");
    vCamposDistedad.addElement("NM_NC_HOS");
    vCamposDistedad.addElement("NM_V_DEF");
    vCamposDistedad.addElement("NM_M_DEF");
    vCamposDistedad.addElement("NM_NC_DEF");
  }

  void rellenarVectorTataqali() {
    vCamposTataqali.addElement("NM_ALIBROTE");
    vCamposTataqali.addElement("CD_ANO");
    vCamposTataqali.addElement("NM_ALERBRO");
    vCamposTataqali.addElement("CD_TALIMENTO");
    vCamposTataqali.addElement("IT_CALCTASA");
    vCamposTataqali.addElement("NM_EXPENF");
    vCamposTataqali.addElement("NM_EXPNOENF");
    vCamposTataqali.addElement("NM_NOEXPENF");
    vCamposTataqali.addElement("NM_NOEXPNOENF");
    vCamposTataqali.addElement("DS_ALI");
  }

  void rellenarVectorMuestras() {
    vCamposMuestras.addElement("NM_MUESTRA");
    vCamposMuestras.addElement("CD_MUESTRA");
    vCamposMuestras.addElement("CD_ANO");
    vCamposMuestras.addElement("NM_ALERBRO");
    vCamposMuestras.addElement("DS_RMUESTRA");
    vCamposMuestras.addElement("NM_MUESBROTE");
    vCamposMuestras.addElement("NM_POSITBROTE");
  }

  void rellenarVectorAlimimpl() {
    vCamposAlimimpl.addElement("NM_ALIBROTE");
    vCamposAlimimpl.addElement("CD_ANO");
    vCamposAlimimpl.addElement("NM_ALERBRO");
    vCamposAlimimpl.addElement("CD_TALIMENTO");
    vCamposAlimimpl.addElement("CD_TIMPLICACION");
    vCamposAlimimpl.addElement("DS_NOMCOMER");
    vCamposAlimimpl.addElement("DS_FABRICAN");
    vCamposAlimimpl.addElement("DS_LOTE");
    vCamposAlimimpl.addElement("CD_MCOMERCALI");
    vCamposAlimimpl.addElement("CD_TRATPREVALI");
    vCamposAlimimpl.addElement("CD_FINGERIRALI");
    vCamposAlimimpl.addElement("CD_MTVIAJEALI");
    vCamposAlimimpl.addElement("CD_LCONTAMIALI");
    vCamposAlimimpl.addElement("CD_PAIS_LCONA");
    vCamposAlimimpl.addElement("CD_LPREPALI");
    vCamposAlimimpl.addElement("CD_PAIS_LPA");
    vCamposAlimimpl.addElement("DS_NOMESTAB");
    vCamposAlimimpl.addElement("DS_DIRESTAB");
    vCamposAlimimpl.addElement("CD_POSESTAB");
    vCamposAlimimpl.addElement("CD_PROV_LPREP");
    vCamposAlimimpl.addElement("CD_MUN_LPREP");
    vCamposAlimimpl.addElement("DS_TELEF_LPREP");
    vCamposAlimimpl.addElement("FC_PREPARACION");
    vCamposAlimimpl.addElement("CD_CONSUMOALI");
    vCamposAlimimpl.addElement("CD_PAIS_LCON");
    vCamposAlimimpl.addElement("DS_ESTAB_LCON");
    vCamposAlimimpl.addElement("DS_DIRESTAB_LCON");
    vCamposAlimimpl.addElement("CD_POSESTAB_LCON");
    vCamposAlimimpl.addElement("CD_PROV_LCON");
    vCamposAlimimpl.addElement("CD_MUN_LCON");
    vCamposAlimimpl.addElement("DS_TELEF_LCON");
    vCamposAlimimpl.addElement("CD_PAIS_DE");
    vCamposAlimimpl.addElement("CD_PAIS_A");
    vCamposAlimimpl.addElement("IT_CALCTASA");
    vCamposAlimimpl.addElement("NM_EXPENF");
    vCamposAlimimpl.addElement("NM_EXPNOENF");
    vCamposAlimimpl.addElement("NM_NOEXPENF");
    vCamposAlimimpl.addElement("NM_NOEXPNOENF");
    vCamposAlimimpl.addElement("DS_ALI");
  }

  //rellena el vector de DataCampos con los nombres de los campos y su longitud
  void rellenarVectorBro() {
    vCamposBro.addElement("CD_ANO");
    vCamposBro.addElement("NM_ALERBRO");
    vCamposBro.addElement("CD_TIPOCOL");
    vCamposBro.addElement("CD_TNOTIF");
    vCamposBro.addElement("CD_TRANSMIS");
    vCamposBro.addElement("CD_GRUPO");
    vCamposBro.addElement("CD_TBROTE");
    vCamposBro.addElement("FC_FECHAHORA");
    vCamposBro.addElement("DS_BROTE");
    vCamposBro.addElement("DS_NOMCOL");
    vCamposBro.addElement("DS_DIRCOL");
    vCamposBro.addElement("CD_POSTALCOL");
    vCamposBro.addElement("CD_PROVCOL");
    vCamposBro.addElement("CD_MUNCOL");
    vCamposBro.addElement("DS_NMCALLE");
    vCamposBro.addElement("DS_PISOCOL");
    vCamposBro.addElement("DS_TELCOL");
    vCamposBro.addElement("CD_NIVEL_1_LCA");
    vCamposBro.addElement("CD_NIVEL_2_LCA");
    vCamposBro.addElement("CD_ZBS_LCA");
    vCamposBro.addElement("CD_NIVEL_1_LE");
    vCamposBro.addElement("CD_NIVEL_2_LE");
    vCamposBro.addElement("CD_ZBS_LE");
    vCamposBro.addElement("NM_MANIPUL");
    vCamposBro.addElement("IT_RESCALC");
    vCamposBro.addElement("NM_EXPUESTOS");
    vCamposBro.addElement("NM_ENFERMOS");
    vCamposBro.addElement("NM_INGHOSP");
    vCamposBro.addElement("NM_DEFUNCION");
    vCamposBro.addElement("FC_EXPOSICION");
    vCamposBro.addElement("FC_ISINPRIMC");
    vCamposBro.addElement("FC_FSINPRIMC");
    vCamposBro.addElement("NM_PERINMIN");
    vCamposBro.addElement("NM_PERINMAX");
    vCamposBro.addElement("NM_PERINMED");
    vCamposBro.addElement("NM_DCUACMIN");
    vCamposBro.addElement("NM_DCUACMAX");
    vCamposBro.addElement("NM_DCUACMED");
    vCamposBro.addElement("NM_VACNENF");
    vCamposBro.addElement("NM_VACENF");
    vCamposBro.addElement("NM_NVACNENF");
    vCamposBro.addElement("NM_NVACENF");
    vCamposBro.addElement("DS_OBSERV");
    vCamposBro.addElement("IT_PERIN");
    vCamposBro.addElement("IT_DCUAC");
    vCamposBro.addElement("CD_OPE");
    vCamposBro.addElement("FC_ULTACT");
  }

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
    }
  }

  void btnBroteActionPerformed() {
    CListaValores clv = null;
    Vector vCod = null;
    QueryTool qt = new QueryTool();

    String ano = txtAno.getText().trim();
    String aler = txtCod.getText().trim();
    if (ano.equals("")) {
      this.getApp().showAdvise("Debe introducir un a�o");
      txtAno.requestFocus();
      return;
    }

    qt.putName("SIVE_BROTES");

    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.STRING);
    qt.putType("DS_BROTE", QueryTool.STRING);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", ano);
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", aler);
    qt.putOperator("NM_ALERBRO", "=");

    qt.addOrderField("NM_ALERBRO");

    // Campos que se muestran en la lista de valores
    vCod = new Vector();
    vCod.addElement("NM_ALERBRO");
    vCod.addElement("DS_BROTE");

    // Lista de valores
    clv = new CListaValores(this.getApp(),
                            "Alertas disponibles",
                            qt,
                            vCod);
    clv.show();

    // Recupera el �rea seleccionada
    if (clv.getSelected() != null) {
      txtDesc.setText( ( (String) clv.getSelected().getString("DS_BROTE")));
      txtCod.setText(clv.getSelected().getString("NM_ALERBRO"));
    }

    clv = null;
  } // Fin btnAreaActionPerformed()

  QueryTool crearQTBrote() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_BROTES");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("CD_TIPOCOL", QueryTool.STRING);
    qt.putType("CD_TNOTIF", QueryTool.STRING);
    qt.putType("CD_TRANSMIS", QueryTool.STRING);
    qt.putType("CD_GRUPO", QueryTool.STRING);
    qt.putType("CD_TBROTE", QueryTool.STRING);
    qt.putType("FC_FECHAHORA", QueryTool.TIMESTAMP);
    qt.putType("DS_BROTE", QueryTool.STRING);
    qt.putType("DS_NOMCOL", QueryTool.STRING);
    qt.putType("DS_DIRCOL", QueryTool.STRING);
    qt.putType("CD_POSTALCOL", QueryTool.STRING);
    qt.putType("CD_PROVCOL", QueryTool.STRING);
    qt.putType("CD_MUNCOL", QueryTool.STRING);
    qt.putType("DS_NMCALLE", QueryTool.STRING);
    qt.putType("DS_PISOCOL", QueryTool.STRING);
    qt.putType("DS_TELCOL", QueryTool.STRING);
    qt.putType("CD_NIVEL_1_LCA", QueryTool.STRING);
    qt.putType("CD_NIVEL_2_LCA", QueryTool.STRING);
    qt.putType("CD_ZBS_LCA", QueryTool.STRING);
    qt.putType("CD_NIVEL_1_LE", QueryTool.STRING);
    qt.putType("CD_NIVEL_2_LE", QueryTool.STRING);
    qt.putType("CD_ZBS_LE", QueryTool.STRING);
    qt.putType("NM_MANIPUL", QueryTool.INTEGER);
    qt.putType("IT_RESCALC", QueryTool.STRING);
    qt.putType("NM_EXPUESTOS", QueryTool.INTEGER);
    qt.putType("NM_ENFERMOS", QueryTool.INTEGER);
    qt.putType("NM_INGHOSP", QueryTool.INTEGER);
    qt.putType("NM_DEFUNCION", QueryTool.INTEGER);
    qt.putType("FC_EXPOSICION", QueryTool.TIMESTAMP);
    qt.putType("FC_ISINPRIMC", QueryTool.TIMESTAMP);
    qt.putType("FC_FSINPRIMC", QueryTool.TIMESTAMP);
    qt.putType("NM_PERINMIN", QueryTool.INTEGER);
    qt.putType("NM_PERINMAX", QueryTool.INTEGER);
    qt.putType("NM_PERINMED", QueryTool.INTEGER);
    qt.putType("NM_DCUACMIN", QueryTool.INTEGER);
    qt.putType("NM_DCUACMAX", QueryTool.INTEGER);
    qt.putType("NM_DCUACMED", QueryTool.INTEGER);
    qt.putType("NM_VACNENF", QueryTool.INTEGER);
    qt.putType("NM_VACENF", QueryTool.INTEGER);
    qt.putType("NM_NVACNENF", QueryTool.INTEGER);
    qt.putType("NM_NVACENF", QueryTool.INTEGER);
    qt.putType("DS_OBSERV", QueryTool.STRING);
    qt.putType("IT_PERIN", QueryTool.STRING);
    qt.putType("IT_DCUAC", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", txtAno.getText().trim());
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", txtCod.getText().trim());
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end crearQT

  QueryTool crearQTProtocol() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_RESP_BROTES");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("CD_TSIVE", QueryTool.STRING);
    qt.putType("CD_MODELO", QueryTool.STRING);
    qt.putType("NM_LIN", QueryTool.INTEGER);
    qt.putType("CD_PREGUNTA", QueryTool.STRING);
    qt.putType("DS_RESPUESTA", QueryTool.STRING);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", txtAno.getText().trim());
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", txtCod.getText().trim());
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end

  QueryTool crearQTAgcausal() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_AGCAUSAL_BROTE");

    // Campos que se quieren leer (SELECT)
    qt.putType("NM_SECAGB", QueryTool.INTEGER);
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("CD_GRUPO", QueryTool.STRING);
    qt.putType("CD_AGENTC", QueryTool.STRING);
    qt.putType("IT_CONFIRMADO", QueryTool.STRING);
    qt.putType("DS_OBSERV", QueryTool.STRING);
    qt.putType("DS_MASAGCAUSAL", QueryTool.STRING);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", txtAno.getText().trim());
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", txtCod.getText().trim());
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end

  QueryTool crearQTFaccontr() {
    String ano = txtAno.getText().trim();
    String nmAler = txtCod.getText().trim();
    Vector vSubquery = new Vector();

    QueryTool qt = new QueryTool();
    qt.putName("SIVE_FACT_CONTRIB");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_GRUPO", QueryTool.STRING);
    qt.putType("CD_FACCONT", QueryTool.STRING);
    qt.putType("DS_FACCON", QueryTool.STRING);
    qt.putType("IT_MASFACCONT", QueryTool.STRING);
    qt.putType("IT_REPE", QueryTool.STRING);

    qt.putSubquery("CD_GRUPO",
        "select CD_GRUPO from SIVE_BROTES where CD_ANO=? AND NM_ALERBRO=?");
    Data dtSubquery1 = new Data();
    dtSubquery1.put(new Integer(QueryTool.STRING), ano);
    vSubquery.addElement(dtSubquery1);
    Data dtSubquery2 = new Data();
    dtSubquery2.put(new Integer(QueryTool.INTEGER), nmAler);
    vSubquery.addElement(dtSubquery2);

    qt.putVectorSubquery("CD_GRUPO", vSubquery);

    return qt;
  } //end

  QueryTool crearQTMedadopt() {
    String ano = txtAno.getText().trim();
    String nmAler = txtCod.getText().trim();
    Vector vSubquery = new Vector();
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_MEDIDAS");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_GRUPO", QueryTool.STRING);
    qt.putType("CD_MEDIDA", QueryTool.STRING);
    qt.putType("DS_MEDIDA", QueryTool.STRING);
    qt.putType("IT_INFADIC", QueryTool.STRING);
    qt.putType("IT_REPE", QueryTool.STRING);

    qt.putSubquery("CD_GRUPO",
        "select CD_GRUPO from SIVE_BROTES where CD_ANO=? AND NM_ALERBRO=?");
    Data dtSubquery1 = new Data();
    dtSubquery1.put(new Integer(QueryTool.STRING), ano);
    vSubquery.addElement(dtSubquery1);
    Data dtSubquery2 = new Data();
    dtSubquery2.put(new Integer(QueryTool.INTEGER), nmAler);
    vSubquery.addElement(dtSubquery2);

    qt.putVectorSubquery("CD_GRUPO", vSubquery);

    return qt;
  } //end

  QueryTool crearQTSintomas() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_SINTOMAS_BROTE");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_SINTOMA", QueryTool.STRING);
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("NM_CASOS", QueryTool.INTEGER);
    qt.putType("DS_MASINF", QueryTool.STRING);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", txtAno.getText().trim());
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", txtCod.getText().trim());
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end

  QueryTool crearQTDistedad() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_BROTES_DISTGEDAD");

    // Campos que se quieren leer (SELECT)
    qt.putType("NM_DISTGEDAD", QueryTool.INTEGER);
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("IT_EDAD", QueryTool.STRING);
    qt.putType("CD_GEDAD", QueryTool.STRING);
    qt.putType("DIST_EDAD_I", QueryTool.INTEGER);
    qt.putType("DIST_EDAD_F", QueryTool.INTEGER);
    qt.putType("NM_V_EXP", QueryTool.INTEGER);
    qt.putType("NM_M_EXP", QueryTool.INTEGER);
    qt.putType("NM_NC_EXP", QueryTool.INTEGER);
    qt.putType("NM_V_ENF", QueryTool.INTEGER);
    qt.putType("NM_M_ENF", QueryTool.INTEGER);
    qt.putType("NM_NC_ENF", QueryTool.INTEGER);
    qt.putType("NM_V_HOS", QueryTool.INTEGER);
    qt.putType("NM_M_HOS", QueryTool.INTEGER);
    qt.putType("NM_NC_HOS", QueryTool.INTEGER);
    qt.putType("NM_V_DEF", QueryTool.INTEGER);
    qt.putType("NM_M_DEF", QueryTool.INTEGER);
    qt.putType("NM_NC_DEF", QueryTool.INTEGER);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", txtAno.getText().trim());
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", txtCod.getText().trim());
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end

  QueryTool crearQTTataqali() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_ALI_BROTE");

    // Campos que se quieren leer (SELECT)
    qt.putType("NM_ALIBROTE", QueryTool.INTEGER);
    qt.putType("DS_ALI", QueryTool.STRING);
    qt.putType("IT_CALCTASA", QueryTool.STRING);
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("CD_TALIMENTO", QueryTool.STRING);
    qt.putType("NM_EXPENF", QueryTool.INTEGER);
    qt.putType("NM_EXPNOENF", QueryTool.INTEGER);
    qt.putType("NM_NOEXPENF", QueryTool.INTEGER);
    qt.putType("NM_NOEXPNOENF", QueryTool.INTEGER);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", txtAno.getText().trim());
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", txtCod.getText().trim());
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end

  QueryTool crearQTMuestras() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_MUESTRAS_BROTE");

    // Campos que se quieren leer (SELECT)
    qt.putType("NM_MUESTRA", QueryTool.INTEGER);
    qt.putType("CD_MUESTRA", QueryTool.STRING);
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("DS_RMUESTRA", QueryTool.STRING);
    qt.putType("NM_MUESBROTE", QueryTool.INTEGER);
    qt.putType("NM_POSITBROTE", QueryTool.INTEGER);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", txtAno.getText().trim());
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", txtCod.getText().trim());
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end

  QueryTool crearQTAlimimpl() {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_ALI_BROTE");

    qt.putType("NM_ALIBROTE", QueryTool.INTEGER);
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("CD_TALIMENTO", QueryTool.STRING);
    qt.putType("CD_TIMPLICACION", QueryTool.STRING);
    qt.putType("DS_NOMCOMER", QueryTool.STRING);
    qt.putType("DS_FABRICAN", QueryTool.STRING);
    qt.putType("DS_LOTE", QueryTool.STRING);
    qt.putType("CD_MCOMERCALI", QueryTool.STRING);
    qt.putType("CD_TRATPREVALI", QueryTool.STRING);
    qt.putType("CD_FINGERIRALI", QueryTool.STRING);
    qt.putType("CD_MTVIAJEALI", QueryTool.STRING);
    qt.putType("CD_LCONTAMIALI", QueryTool.STRING);
    qt.putType("CD_PAIS_LCONA", QueryTool.STRING);
    qt.putType("CD_LPREPALI", QueryTool.STRING);
    qt.putType("CD_PAIS_LPA", QueryTool.STRING);
    qt.putType("DS_NOMESTAB", QueryTool.STRING);
    qt.putType("DS_DIRESTAB", QueryTool.STRING);
    qt.putType("CD_POSESTAB", QueryTool.STRING);
    qt.putType("CD_PROV_LPREP", QueryTool.STRING);
    qt.putType("CD_MUN_LPREP", QueryTool.STRING);
    qt.putType("DS_TELEF_LPREP", QueryTool.STRING);
    qt.putType("FC_PREPARACION", QueryTool.TIMESTAMP);
    qt.putType("CD_CONSUMOALI", QueryTool.STRING);
    qt.putType("CD_PAIS_LCON", QueryTool.STRING);
    qt.putType("DS_ESTAB_LCON", QueryTool.STRING);
    qt.putType("DS_DIRESTAB_LCON", QueryTool.STRING);
    qt.putType("CD_POSESTAB_LCON", QueryTool.STRING);
    qt.putType("CD_PROV_LCON", QueryTool.STRING);
    qt.putType("CD_MUN_LCON", QueryTool.STRING);
    qt.putType("DS_TELEF_LCON", QueryTool.STRING);
    qt.putType("CD_PAIS_DE", QueryTool.STRING);
    qt.putType("CD_PAIS_A", QueryTool.STRING);
    qt.putType("IT_CALCTASA", QueryTool.STRING);
    qt.putType("NM_EXPENF", QueryTool.INTEGER);
    qt.putType("NM_EXPNOENF", QueryTool.INTEGER);
    qt.putType("NM_NOEXPENF", QueryTool.INTEGER);
    qt.putType("NM_NOEXPNOENF", QueryTool.INTEGER);
    qt.putType("DS_ALI", QueryTool.STRING);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", txtAno.getText().trim());
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", txtCod.getText().trim());
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end

  void btnAceptar_actionPerformed() {
    final String srvTrans = "servlet/SrvTransaccion";
    Lista lResultado = null;
    //Lista lDatos=new Lista();
    Lista lBd = new Lista();

    if (isDataValid()) {
      QueryTool qtBrote = crearQTBrote();
      Data dtBrote = new Data();
      dtBrote.put("1", qtBrote);
      lBd.addElement(dtBrote);

      QueryTool qtProtocol = crearQTProtocol();
      Data dtProt = new Data();
      dtProt.put("1", qtProtocol);
      lBd.addElement(dtProt);

      QueryTool qtAgcausal = crearQTAgcausal();
      Data dtAgcausal = new Data();
      dtAgcausal.put("1", qtAgcausal);
      lBd.addElement(dtAgcausal);

      QueryTool qtFaccontr = crearQTFaccontr();
      Data dtFaccontr = new Data();
      dtFaccontr.put("1", qtFaccontr);
      lBd.addElement(dtFaccontr);

      QueryTool qtMedadop = crearQTMedadopt();
      Data dtMedadop = new Data();
      dtMedadop.put("1", qtMedadop);
      lBd.addElement(dtMedadop);

      QueryTool qtSintomas = crearQTSintomas();
      Data dtSintomas = new Data();
      dtSintomas.put("1", qtSintomas);
      lBd.addElement(dtSintomas);

      QueryTool qtDistedad = crearQTDistedad();
      Data dtDistedad = new Data();
      dtDistedad.put("1", qtDistedad);
      lBd.addElement(dtDistedad);

      QueryTool qtTataqali = crearQTTataqali();
      Data dtTataqali = new Data();
      dtTataqali.put("1", qtTataqali);
      lBd.addElement(dtTataqali);

      QueryTool qtMuestras = crearQTMuestras();
      Data dtMuestra = new Data();
      dtMuestra.put("1", qtMuestras);
      lBd.addElement(dtMuestra);

      QueryTool qtALimimpl = crearQTAlimimpl();
      Data dtALimimpl = new Data();
      dtALimimpl.put("1", qtALimimpl);
      lBd.addElement(dtALimimpl);

      Inicializar(CInicializar.ESPERA);
      try {
        this.getApp().getStub().setUrl(srvTrans);
        lResultado = (Lista)this.getApp().getStub().doPost(0, lBd);

        crearFicheroBrote(lResultado);
        crearFicheroProtocol(lResultado);
        crearFicheroAgcausal(lResultado);
        crearFicheroFaccontr(lResultado);
        crearFicheroMedadopt(lResultado);
        crearFicheroSintomas(lResultado);
        crearFicheroDistedad(lResultado);
        crearFicheroTataqAli(lResultado);
        crearFicheroMuestras(lResultado);
        crearFicheroAlimipl(lResultado);

        Lista lMensaje = new Lista();
        if (! (Brot.equals(""))) {
          lMensaje.addElement(Brot);
        }
        if (! (Agcausal.equals(""))) {
          lMensaje.addElement(Agcausal);
        }
        if (! (Faccontr.equals(""))) {
          lMensaje.addElement(Faccontr);
        }
        if (! (Medadopt.equals(""))) {
          lMensaje.addElement(Medadopt);
        }
        if (! (Sintomas.equals(""))) {
          lMensaje.addElement(Sintomas);
        }
        if (! (Distedad.equals(""))) {
          lMensaje.addElement(Distedad);
        }
        if (! (Tataqali.equals(""))) {
          lMensaje.addElement(Tataqali);
        }
        if (! (Muestras.equals(""))) {
          lMensaje.addElement(Muestras);
        }
        if (! (Alimimpl.equals(""))) {
          lMensaje.addElement(Alimimpl);

        }
        DiaMsgAvisoFic dlg = new DiaMsgAvisoFic(this.getApp(), 0, lMensaje);
        dlg.show();

            /*            CMessage msgBox = new CMessage(apl,CMessage.msgAVISO,msgAvis);
                    msgBox.show();
                    msgBox = null;*/
        vaciarPantalla();
        Inicializar(CInicializar.NORMAL);
      }
      catch (Exception exc) {
        this.getApp().trazaLog(exc);
        this.getApp().showError(exc.getMessage());
      }
    } //end if isDataValid
  } //end btn_Aceptar

  void crearFicheroBrote(Lista lResultado) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "Brote.txt";
    EditorFichero edFic = new EditorFichero();
    Lista lResult = (Lista) lResultado.elementAt(0);
    if (lResult.size() == 0) {
      Brot = "";
    }
    else {
      Brot = "Brote.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposBro, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroProtocol(Lista lResultado) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "Protocol.txt";
    EditorFichero edFic = new EditorFichero();
    Lista lResult = (Lista) lResultado.elementAt(1);
    if (lResult.size() == 0) {
      Prot = "";
    }
    else {
      Prot = "Protocol.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposProt, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroAgcausal(Lista lResultado) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "Agcausal.txt";
    EditorFichero edFic = new EditorFichero();
    Lista lResult = (Lista) lResultado.elementAt(2);
    if (lResult.size() == 0) {
      Agcausal = "";
    }
    else {
      Agcausal = "Agcausal.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposAgcausal, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroFaccontr(Lista lResultado) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "Faccontr.txt";
    EditorFichero edFic = new EditorFichero();
    Lista lResult = (Lista) lResultado.elementAt(3);
    if (lResult.size() == 0) {
      Faccontr = "";
    }
    else {
      Faccontr = "Faccontr.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposFaccontr, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroMedadopt(Lista lResultado) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "Medadopt.txt";
    EditorFichero edFic = new EditorFichero();
    Lista lResult = (Lista) lResultado.elementAt(4);
    if (lResult.size() == 0) {
      Medadopt = "";
    }
    else {
      Medadopt = "Medadopt.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposMedadopt, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroSintomas(Lista lResultado) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "Sintomas.txt";
    EditorFichero edFic = new EditorFichero();
    Lista lResult = (Lista) lResultado.elementAt(5);
    if (lResult.size() == 0) {
      Sintomas = "";
    }
    else {
      Sintomas = "Sintomas.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposSintomas, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroDistedad(Lista lResultado) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "Distedad.txt";
    EditorFichero edFic = new EditorFichero();
    Lista lResult = (Lista) lResultado.elementAt(6);
    if (lResult.size() == 0) {
      Distedad = "";
    }
    else {
      Distedad = "Distedad.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposDistedad, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroTataqAli(Lista lResultado) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "Tataqali.txt";
    EditorFichero edFic = new EditorFichero();
    Lista lResult = (Lista) lResultado.elementAt(7);
    if (lResult.size() == 0) {
      Tataqali = "";
    }
    else {
      Tataqali = "Tataqali.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposTataqali, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroMuestras(Lista lResultado) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "Muestras.txt";
    EditorFichero edFic = new EditorFichero();
    Lista lResult = (Lista) lResultado.elementAt(8);
    if (lResult.size() == 0) {
      Muestras = "";
    }
    else {
      Muestras = "Muestras.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposMuestras, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroAlimipl(Lista lResultado) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "Alimimpl.txt";
    EditorFichero edFic = new EditorFichero();
    Lista lResult = (Lista) lResultado.elementAt(9);
    if (lResult.size() == 0) {
      Alimimpl = "";
    }
    else {
      Alimimpl = "Alimimpl.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposAlimimpl, apl,
                                        lResult, localiz, carSep);
  }

  //comprobar q se encuentran rellenos los campos
  boolean isDataValid() {
    boolean b = false;
    if (panFichero.txtFile.getText().length() == 0) {
      this.getApp().showAdvise(
          "Se debe indicar el directorio elegido para el nuevo fichero");
      b = false;
    }
    else {
      if ( (txtAno.getText().length() == 0) ||
          (txtCod.getText().length() == 0) ||
          (txtCaracter.getText().length() == 0)) {
        this.getApp().showAdvise("Debe completar todos los campos");
        b = false;
      }
      else {
        b = true;
      }
    }
    return b;
  }

  Lista buscarDescripcionBrote(String ano, String aler) {
    CMessage mensaje = null;
    String sDescDev = null;
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvQueryTool";

    try {
      QueryTool qt = new QueryTool();
      qt.putName("SIVE_BROTES");

      qt.putType("DS_BROTE", QueryTool.STRING);

      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", ano);
      qt.putOperator("CD_ANO", "=");

      qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putWhereValue("NM_ALERBRO", aler);
      qt.putOperator("NM_ALERBRO", "=");

      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }
    Inicializar(CInicializar.NORMAL);

    return p1;
  } //end buscarDescripcionBrote

  void txtCodBroteFocusLost() {
    //si el c�digo de brote pierde el foco y ya ha sido introducido uno
    //obtener la descripci�n del brote correspondiente.
    Lista desc = null;
    if ( (txtCod.getText().length() != 0) && (txtAno.getText().length() != 0)) {
      desc = buscarDescripcionBrote(txtAno.getText(), txtCod.getText());
      //si se obtiene una descripci�n.
      if (desc.size() != 0) {
        txtDesc.setText( ( (Data) desc.elementAt(0)).getString("DS_BROTE"));
        fallo = false;
      }
      else {
        //si ha fallado en a�o q no falle aqu� tambi�n.
        if (fallo) {
          fallo = false;
        }
        else {
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                         "No se encontraron datos.");
          msgBox.show();
          msgBox = null;
        }
      }
    } //end txtAno.getText().length()!=0
  } // Fin txtCodBroteFocusLost()

  void txtValueChangeAno() {
    txtCod.setText("");
    txtDesc.setText("");
  } // Fin txtValueChangeAno()

  void txtValueChangeCodBrote() {
    txtDesc.setText("");
  } // Fin txtValueChangeAno()

  void vaciarPantalla() {
    int iYearAct = 0;
    Calendar cal = Calendar.getInstance();
    iYearAct = cal.get(cal.YEAR);
    if (! (this.getApp().getParametro("ANYO_DEFECTO").equals(""))) {
      String anoDef = this.getApp().getParametro("ANYO_DEFECTO");
      txtAno.setText(anoDef);
    }
    else {
      txtAno.setText(new Integer(iYearAct).toString());
    }
    txtCod.setText("");
    txtDesc.setText("");
    txtCaracter.setText("$");
    panFichero.txtFile.setText("c:\\");

  }
} //end class

// Tecla presionada
class PanExportBroteKeyListener
    implements java.awt.event.KeyListener, Runnable {
  PanExportBrote adaptee = null;
  KeyEvent evt = null;

  PanExportBroteKeyListener(PanExportBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(java.awt.event.KeyEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void keyTyped(java.awt.event.KeyEvent e) {}

  public void keyReleased(java.awt.event.KeyEvent e) {}

  public void run() {
    String name2 = ( (Component) evt.getSource()).getName();
    if (name2.equals("ano")) {
      adaptee.txtValueChangeAno();
    }
    if (name2.equals("CodBrote")) {
      adaptee.txtValueChangeCodBrote();
    }
  }
} // Fin clase PanExportBroteKeyListener

// P�rdidas de foco
class PanExportBroteFocusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanExportBrote adaptee;
  FocusEvent evt;

  PanExportBroteFocusAdapter(PanExportBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    if ( ( (TextField) evt.getSource()).getName().equals("CodBrote")) {
      adaptee.txtCodBroteFocusLost();
    }
  }
} // Fin clase PanExportBroteFocusAdapter

// Botones
class PanExportBroteActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanExportBrote adaptee;
  ActionEvent evt;

  PanExportBroteActionAdapter(PanExportBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    if (evt.getActionCommand().equals("Brote")) {
      adaptee.btnBroteActionPerformed();
    }
    if (evt.getActionCommand().equals("Aceptar")) {
      adaptee.btnAceptar_actionPerformed();
    }
  }
} // endclass  DAPanSupActionAdapter
