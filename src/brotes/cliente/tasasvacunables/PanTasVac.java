package brotes.cliente.tasasvacunables;

import java.util.Vector;

import java.awt.Checkbox;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CDecimal;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import jclass.bwt.BWTEnum;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;
import sapp2.StubSrvBD;

//import brotes.servidor.tasasvacunables.*;
//import comun.CFechaSimple; import comun.CHora; import comun.CListaNiveles1; import comun.Comunicador; import comun.constantes; import comun.Data; import comun.DataAutoriza; import comun.DataCasIn; import comun.DataEnferedo; import comun.DataEnfermo; import comun.DataEntradaEDO; import comun.DataEqNot; import comun.DataGeneralCDDS; import comun.DataIndivEnfermedad; import comun.DataLongValid; import comun.DataMun; import comun.DataMunicipioEDO; import comun.DataNivel1; import comun.DataNotifEDO; import comun.DataNotifSem; import comun.DataRegistroEDO; import comun.DataValoresEDO; import comun.DataZBS; import comun.DataZBS2; import comun.DatoBase; import comun.datosParte; import comun.datosPreg; import comun.DialogoGeneral; import comun.DialogRegistroEDO; import comun.DialogSelDomi; import comun.DialogSelEnfermo; import comun.Fechas; import comun.IntContenedor; import comun.PanAnoSemFech; import comun.SrvDebugGeneral; import comun.SrvDlgBuscarRegistro; import comun.SrvEnfermedad; import comun.SrvEntradaEDO; import comun.SrvMaestroEDO; import comun.SrvMun; import comun.SrvMunicipioCont; import comun.SrvNivel1; import comun.SrvZBS2; import comun.UtilEDO; import comun.comun;
/**
 * Panel a trav�s del que se realizan los mantenimientos
 * de  Tasa de ataque de enfermedades vacunables.
 * @autor PDP
 * @version 1.0
 */

public class PanTasVac
    extends CDialog
    implements CInicializar, CFiltro {
//  BorderLayout borderLayout1 = new BorderLayout();

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;
  final public int CONSULTA = 3;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;
  public int modoActualizacion = ALTA;

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  // gesti�n de la linea de totales
  public boolean bTotales = false;

  //Retorno de alta
  public boolean bFirst = false;

  // estructuras para comprobar el bloqueo
  private QueryTool qtBloqueo = null;
  private Data dtBloqueo = null;

  public int indTotal = 0;

  public int indTotalIni = 0;

  private Lista lismanBaj = new Lista();

  CListaMantenimiento clmMantTasa = null;

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  DialogActionAdapter1 ActionAdapter = new DialogActionAdapter1(this);
  DialogchkitemAdapter chkItemAdapter = new DialogchkitemAdapter(this);
  //XYLayout xYLayout1 = new XYLayout();

  Label lblTasa = new Label();
  //CTexto cTasa=null;
//  CEntero cTasa=new CEntero(4);
  CDecimal cTasa = new CDecimal(4, 2);
  Label lblResultado = new Label();
  Checkbox chResultado = new Checkbox();

  Label lblBroteDesc = new Label();
  Label lblDatosBrote = new Label();

  final String srvTrans = "servlet/SrvTransaccion";
  private boolean OK = false;

  Data dtBrote = null;
  XYLayout xYLayout1 = new XYLayout();

  public PanTasVac(CApp a, int modo, Data dt) {
    super(a);

    //cTasa=new CTexto(6);
    bFirst = true;

    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    QueryTool2 qt = new QueryTool2();
    QueryTool qtP = new QueryTool();

    try {

      dtBrote = dt;

      modoActualizacion = modo;
      this.app = a;

      //botones
      vBotones.addElement(new CBoton("",
                                     "images/alta2.gif",
                                     "Nueva Tasa",
                                     false,
                                     false));

      vBotones.addElement(new CBoton("",
                                     "images/modificacion2.gif",
                                     "Modificar Tasa",
                                     true,
                                     true));

      vBotones.addElement(new CBoton("",
                                     "images/baja2.gif",
                                     "Borrar Tasa",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("Edad",
                                      "82",
                                      "DS_DIST_EDAD"));

      vLabels.addElement(new CColumna("Vac.Enfer",
                                      "69",
                                      "NM_VACENF"));

      vLabels.addElement(new CColumna("Vac.NoEnfer",
                                      "80",
                                      "NM_VACNOENF"));

      vLabels.addElement(new CColumna("Total",
                                      "52",
                                      "NM_VACENF + NM_VACNOENF"));

      vLabels.addElement(new CColumna("Tasa(%)",
                                      "50",
          "(NM_VACENF/(NM_VACENF + NM_VACNOENF))*100"));

      vLabels.addElement(new CColumna("NoVac.Enfer",
                                      "80",
                                      "NM_NOVACENF"));

      vLabels.addElement(new CColumna("NoVac.NoEnfer",
                                      "95",
                                      "NM_NOVACNOENF"));

      vLabels.addElement(new CColumna("Total",
                                      "52",
                                      "NM_NOVACENF + NM_NOVACNOENF"));

      vLabels.addElement(new CColumna("Tasa(%)",
                                      "50",
          "(NM_NOVACENF/(NM_NOVACENF + NM_NOVACNOENF))*100"));

      vLabels.addElement(new CColumna("Ef.vac.(%)",
                                      "68",
                                      "(1-(NM_VACENF*(NM_NOVACENF + NM_NOVACNOENF))/(NM_NOVACENF*(NM_VACENF + NM_VACNOENF)))*100"));

      int[] ajustar = new int[10];
      ajustar[0] = BWTEnum.TOPLEFT;
      ajustar[1] = BWTEnum.TOPRIGHT;
      ajustar[2] = BWTEnum.TOPRIGHT;
      ajustar[3] = BWTEnum.TOPRIGHT;
      ajustar[4] = BWTEnum.TOPRIGHT;
      ajustar[5] = BWTEnum.TOPRIGHT;
      ajustar[6] = BWTEnum.TOPRIGHT;
      ajustar[7] = BWTEnum.TOPRIGHT;
      ajustar[8] = BWTEnum.TOPRIGHT;
      ajustar[9] = BWTEnum.TOPRIGHT;

      clmMantTasa = new CListaMantenimiento(a,
                                            vLabels,
                                            vBotones,
                                            ajustar,
                                            this,
                                            this);
      jbInit();
      Lista lismanIni = this.clmMantTasa.getLista();
      indTotalIni = lismanIni.size();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setSize(750, 500);
    this.setTitle("Obtenci�n de la Tasa de ataque de enfermedades vacunables");

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    lblTasa.setText("Tasa de ataque global (%)");
    cTasa.setText("");
    lblResultado.setText("Obtener Resultado");
    lblBroteDesc.setText("Brote:");
    lblDatosBrote.setText(dtBrote.getString("CD_ANO") + "/" +
                          dtBrote.getString("NM_ALERBRO") + " - " +
                          dtBrote.getString("DS_BROTE"));

    this.setLayout(xYLayout1);
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(ActionAdapter);
    btnCancelar.addActionListener(ActionAdapter);
    chResultado.addItemListener(chkItemAdapter);

    btnAceptar.setEnabled(true);
    btnCancelar.setEnabled(true);

    this.add(lblTasa, new XYConstraints(39, 25, 150, -1));
    this.add(cTasa, new XYConstraints(200, 25, 85, -1));
    this.add(lblResultado, new XYConstraints(39, 62, 125, -1));
    this.add(chResultado, new XYConstraints(200, 62, 85, -1));
    this.add(lblBroteDesc, new XYConstraints(39, 92, 42, -1));
    this.add(lblDatosBrote, new XYConstraints(92, 92, 600, -1));
    this.add(clmMantTasa, new XYConstraints(20, 120, 800, 300));
    this.add(btnAceptar, new XYConstraints(511, 420, 88, 29));
    this.add(btnCancelar, new XYConstraints(622, 420, 88, 29));

    Inicializar(CInicializar.ESPERA);
    clmMantTasa.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
  }

  private String cogeDecimal(double dent, int num_dec, char decimal) {
    String salida = "";
    int pos_coma = 0;
    if (!Double.isNaN(dent)) {
      String cadena = String.valueOf(dent);
      for (int a = 0; a < cadena.length(); a++) {
        if (cadena.charAt(a) == decimal) {
          pos_coma = a;
        }
      }
      if ( (num_dec > 0) && (pos_coma + num_dec < cadena.length())) {
        salida = cadena.substring(0, pos_coma + num_dec + 1);
      }
      else if (num_dec == 0) {
        salida = cadena.substring(0, pos_coma);
      }
      else {
        salida = cadena;
      }
    }
    else {
      salida = "";
    }
    return salida;
  }

  /*
     public void GenerarNc(Lista lis){
   if(modoActualizacion==ALTA){
    Data fila = null;
    fila = new Data();
    fila.put("DS_DIST_EDAD","NC");
    lis.addElement(fila);
   }
     }
   */
  public Lista ObtenerTotales(Lista lis) {
    int vEnf = 0;
    int vnEnf = 0;
    int tvEnf = 0;
    double ttvEnf = 0.0;
//    int ittvEnf = 0;
    int nvEnf = 0;
    int nvnEnf = 0;
    int tnvEnf = 0;
    double ttnvEnf = 0.0;
//    int ittnvEnf = 0;
    double efVac = 0.0;
//    int iefVac = 0;

    String tasVac = "";
    String tasNoVac = "";
    String total = "Total";
    String sNM_VACENF = "";
    String sNM_VACNOENF = "";
    String sNM_NOVACENF = "";
    String sNM_NOVACNOENF = "";
    String sDS_DIST_EDAD = "";
    int iTotal = 0;
    Data fila = null;

    //if((modoActualizacion==MODIFICACION)||(modoActualizacion==BAJA)){
    for (int i = 0; i < lis.size(); i++) {
      iTotal = lis.size();
      fila = (Data) lis.elementAt(i);
      sDS_DIST_EDAD = (String) fila.getString("DS_DIST_EDAD");

      //N�mero de vacunados enfermos
      sNM_VACENF = (String) fila.getString("NM_VACENF");
      if (sNM_VACENF.equals("")) {
        sNM_VACENF = "";
      }
      else {
        vEnf += Integer.parseInt(fila.getString("NM_VACENF"));
      }

      //N�mero de vacunados no enfermos
      sNM_VACNOENF = (String) fila.getString("NM_VACNOENF");
      if (sNM_VACNOENF.equals("")) {
        sNM_VACNOENF = "";
      }
      else {
        vnEnf += Integer.parseInt(fila.getString("NM_VACNOENF"));
      }

      //N�mero total de vacunados
      if (i == iTotal - 1) {
        tvEnf = vEnf + vnEnf;
      }
      else {
        tvEnf += vEnf + vnEnf;
      }

      //Tasa de vacunados
      if (tvEnf == 0) {
        tvEnf = 0;
      }
      else {
        if (i == iTotal - 1) {
          ttvEnf = 100 *
              ( (new Integer(vEnf)).doubleValue() /
               (new Integer(tvEnf)).doubleValue());
          tasVac = cogeDecimal(ttvEnf, 1, '.');
        }
        else {
          ttvEnf += 100 *
              ( (new Integer(vEnf)).doubleValue() /
               (new Integer(tvEnf)).doubleValue());
          tasVac = cogeDecimal(ttvEnf, 1, '.');
        }
      }

      /*      if (tvEnf==0){
            tvEnf=0;
            }else{
              if (i == iTotal-1){
                ttvEnf = cogeDecimal(((vEnf*100)/tvEnf),1,".");
                ittvEnf = (int) ttvEnf;
              }else{
                ttvEnf +=  cogeDecimal(((vEnf*100)/tvEnf,1,".");
                ittvEnf = (int) ttvEnf;
              }
            }*/

      //N�mero de no vacunados enfermos
      sNM_NOVACENF = (String) fila.getString("NM_NOVACENF");
      if (sNM_NOVACENF.equals("")) {
        sNM_NOVACENF = "";
      }
      else {
        nvEnf += Integer.parseInt(fila.getString("NM_NOVACENF"));
      }

      //N�mero de no vacunados no enfermos
      sNM_NOVACNOENF = (String) fila.getString("NM_NOVACNOENF");
      if (sNM_NOVACNOENF.equals("")) {
        sNM_NOVACNOENF = "";
      }
      else {
        nvnEnf += Integer.parseInt(fila.getString("NM_NOVACNOENF"));
      }

      //N�mero total de no vacunados
      if (i == iTotal - 1) {
        tnvEnf = nvEnf + nvnEnf;
      }
      else {
        tnvEnf += nvEnf + nvnEnf;
      }

      //Tasa de no vacunados
      if (tnvEnf == 0) {
        tnvEnf = 0;
      }
      else {
        if (i == iTotal - 1) {
          ttnvEnf = 100 * ( (new Integer(nvEnf)).doubleValue() / tnvEnf);
          tasNoVac = cogeDecimal(ttnvEnf, 1, '.');
        }
        else {
          ttnvEnf += 100 * ( (new Integer(nvEnf)).doubleValue() / tnvEnf);
          tasNoVac = cogeDecimal(ttnvEnf, 1, '.');
        }
      }

      /*    if (tnvEnf==0){
             tnvEnf=0;
             }else{
        if (i == iTotal-1){
          ttnvEnf = ((nvEnf*100)/tnvEnf);
          ittnvEnf = (int) ttnvEnf;
        }else{
          ttnvEnf += ((nvEnf*100)/tnvEnf);
          ittnvEnf = (int) ttnvEnf;
        }
             }   */

      //Efectividad de la vacuna
      if ( (nvEnf == 0) && (tvEnf == 0)) {
        efVac = 0.0;
      }
      else {
        if (i == iTotal - 1) {
          efVac = 100 * (1 - (ttvEnf / ttnvEnf));
        }
        else {
          efVac += 100 * (1 - (ttvEnf / ttnvEnf));
        }
      }
    }

    //}

    fila = new Data();
    fila.put("DS_DIST_EDAD", sDS_DIST_EDAD);
    fila.put("NM_VACENF", (new Integer(vEnf)).toString());
    fila.put("NM_VACNOENF", (new Integer(vnEnf)).toString());
    fila.put("NM_VACENF + NM_VACNOENF", (new Integer(tvEnf)).toString());
    fila.put("(NM_VACENF/(NM_VACENF + NM_VACNOENF))*100",
             cogeDecimal(ttvEnf, 1, '.')); //Tasa
//    fila.put("(NM_VACENF/(NM_VACENF + NM_VACNOENF))*100", (new Integer(ittvEnf)).toString()); //Tasa
    fila.put("NM_NOVACENF", (new Integer(nvEnf)).toString());
    fila.put("NM_NOVACNOENF", (new Integer(nvnEnf)).toString());
    fila.put("NM_NOVACENF + NM_NOVACNOENF", (new Integer(tnvEnf)).toString());
    fila.put("(NM_NOVACENF/(NM_NOVACENF + NM_NOVACNOENF))*100",
             cogeDecimal(ttnvEnf, 1, '.')); //Tasa
//    fila.put("(NM_NOVACENF/(NM_NOVACENF + NM_NOVACNOENF))*100", (new Integer(ittnvEnf)).toString()); //Tasa
    fila.put("(1-(NM_VACENF*(NM_NOVACENF + NM_NOVACNOENF))/(NM_NOVACENF*(NM_VACENF + NM_VACNOENF)))*100",
             cogeDecimal(efVac, 1, '.'));
    fila.put("DS_DIST_EDAD", (new String(total)));
    lis.addElement(fila);
    return lis;
  }

  public void Inicializar() {}

  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if ( (modoActualizacion == BAJA) || (modoActualizacion == CONSULTA)) {
          this.setEnabled(false);
          btnAceptar.setVisible(false);
          btnCancelar.setLabel("Salir");
          btnCancelar.setEnabled(true);
          clmMantTasa.setEnabled(false);
          chResultado.setEnabled(false);
        }
        else {
          clmMantTasa.setEnabled(true);
          chResultado.setEnabled(true);
          btnAceptar.setEnabled(true);
          btnCancelar.setEnabled(true);
        }
    }
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvQueryTool";

    try {
      Data fila = null;
      fila = new Data();
      QueryTool2 qt = new QueryTool2();
      qt.putName("SIVE_TATAQ_ENFVAC");
      qt.putType("NM_TENFVAC", QueryTool.INTEGER);
      qt.putType("CD_ANO", QueryTool.STRING);
      qt.putType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putType("IT_EDAD", QueryTool.STRING);
      qt.putType("CD_GEDAD", QueryTool.STRING);
      qt.putType("DIST_EDAD_I", QueryTool.INTEGER);
      qt.putType("DIST_EDAD_F", QueryTool.INTEGER);
      qt.putType("NM_VACENF", QueryTool.INTEGER);
      qt.putType("NM_VACNOENF", QueryTool.INTEGER);
      qt.putType("NM_VACENF + NM_VACNOENF", QueryTool.INTEGER);
      //tasa de vacunados
      qt.putType("NM_NOVACENF", QueryTool.INTEGER);
      qt.putType("NM_NOVACNOENF", QueryTool.INTEGER);
      qt.putType("NM_NOVACENF + NM_NOVACNOENF", QueryTool.INTEGER);
      //tasa de no vacunados
      qt.putType("CD_GEDAD || '/' || DIST_EDAD_I || '-' || DIST_EDAD_F",
                 QueryTool.STRING);
      qt.putType("DIST_EDAD_I || '-' || DIST_EDAD_F", QueryTool.STRING);

      qt.putWhereType("IT_EDAD", QueryTool.STRING);
      qt.putWhereValue("IT_EDAD", "S");
      qt.putOperator("IT_EDAD", "=");

      qt.putWhereType("CD_ANO", QueryTool.STRING);
      qt.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
      qt.putOperator("CD_ANO", "=");

      qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
      qt.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
      qt.putOperator("NM_ALERBRO", "=");

      //otros datos
      QueryTool qtP = new QueryTool();
      qtP.putName("SIVE_DISTR_GEDAD");
      qtP.putType("DS_DIST_EDAD", QueryTool.STRING);
      Data dtA1 = new Data();
      dtA1.put("CD_GEDAD", QueryTool.STRING);
      dtA1.put("DIST_EDAD_I", QueryTool.INTEGER);
      dtA1.put("DIST_EDAD_F", QueryTool.INTEGER);
      qt.addQueryTool(qtP);
      qt.addColumnsQueryTool(dtA1);

      //otros datos
      QueryTool qtD = new QueryTool();
      qtD.putName("SIVE_GEDAD");
      qtD.putType("DS_GEDAD", QueryTool.STRING);
      Data dtA2 = new Data();
      dtA2.put("CD_GEDAD", QueryTool.STRING);
      qt.addQueryTool(qtD);
      qt.addColumnsQueryTool(dtA2);

      qt.addOrderField("CD_GEDAD");

      p.addElement(qt);

      this.getApp().getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      p1 = (Lista)this.getApp().getStub().doPost(1, p);

      /*          if (p1.size()!=0) {
                  p1=introducirTasas(p1);
                }*/

      // Aqu� se hacen los c�lculos necesarios.
      Data dtTmp = null;
      double nm_vacenf = 0.0;
      double nm_vacnoenf = 0.0;
      double nm_novacenf = 0.0;
      double nm_novacnoenf = 0.0;
      for (int i = 0; i < p1.size(); i++) {
        dtTmp = (Data) p1.elementAt(i);
        nm_vacenf = Double.valueOf(dtTmp.getString("NM_VACENF")).doubleValue();
        nm_vacnoenf = Double.valueOf(dtTmp.getString("NM_VACNOENF")).
            doubleValue();
        nm_novacenf = Double.valueOf(dtTmp.getString("NM_NOVACENF")).
            doubleValue();
        nm_novacnoenf = Double.valueOf(dtTmp.getString("NM_NOVACNOENF")).
            doubleValue();

        double tasa1 = 100 * nm_vacenf / (nm_vacenf + nm_vacnoenf);
        double tasa2 = 100 * nm_novacenf / (nm_novacenf + nm_novacnoenf);
        double tasa3 = 100 *
            (1 -
             (nm_vacenf * (nm_novacenf + nm_novacnoenf)) /
             (nm_novacenf * (nm_vacenf + nm_vacnoenf)));

        dtTmp.put("(NM_VACENF/(NM_VACENF + NM_VACNOENF))*100",
                  cogeDecimal(tasa1, 1, '.'));
        dtTmp.put("(NM_NOVACENF/(NM_NOVACENF + NM_NOVACNOENF))*100",
                  cogeDecimal(tasa2, 1, '.'));
        dtTmp.put("(1-(NM_VACENF*(NM_NOVACENF + NM_NOVACNOENF))/(NM_NOVACENF*(NM_VACENF + NM_VACNOENF)))*100",
                  cogeDecimal(tasa3, 1, '.'));
      }

    }
    catch (Exception ex) {
      ex.printStackTrace();
      this.getApp().trazaLog(ex);
    }
    //}//fin if

    ObtenerTotales(p1);

    return p1;
  }

  public void realizaOperacion(int j) {
    Lista lisman = this.clmMantTasa.getLista();
    int ind = clmMantTasa.getSelectedIndex();
    indTotal = lisman.size();
    DiaTasa dlg = null;
    Data dMantTasa = new Data();

    Data dtResultado = new Data();
    Lista lResultado = new Lista();
    Lista ltmpsal = new Lista();
    Data dtlisman = new Data();
    Data dtAlta = new Data();
    Lista lGedad = new Lista();
    Lista listaAlta = new Lista();
    Lista lItemRepe = new Lista();
    switch (j) {
      // bot�n de alta:data vacio
      case ALTA:
        dlg = new DiaTasa(this.getApp(), 0, dtBrote, null, lisman);
        dlg.show();
        // a�adir el nuevo elem a la lista
        if (dlg.bAceptar()) {
          lisman.removeElementAt(indTotal - 1);
          dtResultado = dlg.devuelveData();
          lisman.addElement(dtResultado);
          ObtenerTotales(lisman);
          clmMantTasa.setPrimeraPagina(lisman);
          chResultado.setState(false);
          cTasa.setText("");
        }

        //if (dlg.bAceptar()) {
        /*  //<----Alta de forma masiva desde el di�logo
                   //Lista con los registro nuevos
                   listaAlta = dlg.devuelveDataAlta();
                   int tlistaAlta = listaAlta.size();
                   int tlisman = lisman.size();
                   for (int k = 0; k<tlisman; k++){
         dtlisman = (Data) lisman.elementAt(k);
         dtAlta = (Data) listaAlta.elementAt(0);
         String sCD_GEDADlm = dtlisman.getString("CD_GEDAD");
         String sCD_GEDADlA = dtAlta.getString("CD_GEDAD");
         if (sCD_GEDADlm.equals(sCD_GEDADlA)){
            String sItem = String.valueOf(k);
            lItemRepe.addElement(sItem);
         }
                   }
                   //Borrado de la linea de totales
                   lisman.removeElementAt(tlisman-1);
                   //Borrado del los registros que coinciden con los que se dan de alta
                   for (int h = 0; h<lItemRepe.size(); h++){
            String ItemRepe = (String)lItemRepe.elementAt(h);
            int Item = new Integer(ItemRepe).intValue();
            Data dtBajas = (Data)lisman.elementAt(Item-h);
            lismanBaj.addElement(dtBajas);
            lisman.removeElementAt(Item-h);  //<---- ojo se le resta h porque la lista "lisman" va disminuyendo en componentes
                   }
                   //Se a�aden los nuevos registros a la lista total
                   for (int l = 0; l<tlistaAlta; l++){
         Data dtAltaT = (Data) listaAlta.elementAt(l);
         lisman.addElement(dtAltaT);
                   }
         ObtenerTotales(lisman);
         clmMantTasa.setPrimeraPagina(lisman);
         */

        /* Antes....
                    lisman.removeAllElements();
                    lResultado = dlg.devuelveDataAlta();
                    ObtenerTotales(lResultado);
                    clmMantTasa.setPrimeraPagina(lResultado);
         */
        // a�adir el nuevo elem a la lista
        //}
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        dMantTasa = clmMantTasa.getSelected();
        String sTotal = dMantTasa.getString("DS_DIST_EDAD");

        //si existe alguna fila seleccionada
        if ( (dMantTasa != null) && (!sTotal.equals("Total"))) {
          dlg = new DiaTasa(this.getApp(), 1, dtBrote, dMantTasa, lisman);
          dlg.show();
          //Tratamiento de bAceptar para Salir
          if (dlg.bAceptar()) {
            lisman.removeElementAt(ind);
            lisman.removeElementAt(indTotal - 2);
            dtResultado = dlg.devuelveData();
            lisman.addElement(dtResultado);
            ObtenerTotales(lisman);
            clmMantTasa.setPrimeraPagina(lisman);
            chResultado.setState(false);
            cTasa.setText("");
          }
        }
        else {
          if (dMantTasa == null) {
            this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
          }
          else {
            this.getApp().showAdvise(
                "Debe seleccionar un registro en la tabla diferente a la linea de totales");
          }
        }
        break;

      case BAJA:
        dMantTasa = clmMantTasa.getSelected();
        String sTotal2 = dMantTasa.getString("DS_DIST_EDAD");
        if ( (dMantTasa != null) && (!sTotal2.equals("Total"))) {
          dlg = new DiaTasa(this.getApp(), 2, dtBrote, dMantTasa, lisman);
          dlg.show();
          if (dlg.bAceptar()) {
            lismanBaj.addElement(clmMantTasa.getSelected());
            lisman.removeElementAt(ind);
            lisman.removeElementAt(indTotal - 2);
            ObtenerTotales(lisman);
            clmMantTasa.setPrimeraPagina(lisman);
            chResultado.setState(false);
            cTasa.setText("");

            //clmMantTasa.getLista().removeElementAt(ind);
            //clmMantTasa.setPrimeraPagina( this.primeraPagina() );
          }
        }
        else {
          if (dMantTasa == null) {
            this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
          }
          else {
            this.getApp().showAdvise(
                "Debe seleccionar un registro en la tabla diferente a la linea de totales");
          }
        }
        break;
    }
  }

  public Lista siguientePagina() {
    Lista v = null;
    return v;
  }

  void btn_actionPerformed(ActionEvent e) {
    Inicializar(CInicializar.ESPERA);
    Lista lstMant = new Lista();
    Lista lstOperar = new Lista();
    Lista vResultado = new Lista();
    if (e.getActionCommand().equals("Aceptar")) {
      try {
        this.getApp().getStub().setUrl("servlet/SrvTasVac");

        // Query para actualizar SIVE_BROTES (CD_OPE/FC_ULTACT)
        QueryTool qtBro = new QueryTool();
        Data dtBro = new Data();
        qtBro.putName("SIVE_BROTES");
        qtBro.putType("CD_OPE", QueryTool.STRING);
        qtBro.putType("FC_ULTACT", QueryTool.TIMESTAMP);
        qtBro.putValue("CD_OPE", dtBrote.getString("CD_OPE"));
        qtBro.putValue("FC_ULTACT", "");

        qtBro.putWhereType("CD_ANO", QueryTool.STRING);
        qtBro.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
        qtBro.putOperator("CD_ANO", "=");
        qtBro.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
        qtBro.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
        qtBro.putOperator("NM_ALERBRO", "=");

        dtBro.put("4", qtBro);
        lstOperar.addElement(dtBro);

        /************** ALTAS ***************/
        lstMant = this.clmMantTasa.getLista();

        Data dtTemp = null;

        for (int i = 0; i < lstMant.size(); i++) {
          dtTemp = (Data) lstMant.elementAt(i);
          if (dtTemp.getString("TIPO_OPERACION").equals("A")) {
            QueryTool qtAlt = new QueryTool();
            qtAlt = realizarAlta(dtTemp);

            Data dtAlt = new Data();
            dtAlt.put("3", qtAlt);

            lstOperar.addElement(dtAlt);

            dtTemp.remove("TIPO_OPERACION");
          }
        }

        /************** MODIFICAR ***************/
        for (int i = 0; i < lstMant.size(); i++) {
          dtTemp = (Data) lstMant.elementAt(i);
          if (dtTemp.getString("TIPO_OPERACION").equals("M")) {
            QueryTool qtUpd = new QueryTool();
            qtUpd = realizarUpdate(dtTemp);

            Data dtUpdate = new Data();
            dtUpdate.put("4", qtUpd);

            lstOperar.addElement(dtUpdate);

            dtTemp.remove("TIPO_OPERACION");
          }
        }
        /************** BAJAS ***************/
        if (lismanBaj.size() > 0) {
          for (int i = 0; i < (lismanBaj.size()); i++) {

            dtTemp = (Data) lismanBaj.elementAt(i);
            String sMod = dtTemp.getString("TIPO_OPERACION");
            if (!sMod.equals("A")) {
              QueryTool qtBaj = new QueryTool();
              qtBaj = realizarBaja(dtTemp);

              Data dtUpBaj = new Data();
              dtUpBaj.put("5", qtBaj);

              lstOperar.addElement(dtUpBaj);
            }
          }
        }

        if (modoActualizacion == BAJA) {
          for (int i = 0; i < lstMant.size() - 1; i++) {
            dtTemp = (Data) lstMant.elementAt(i);

            QueryTool qtBaj = new QueryTool();
            qtBaj = realizarBaja(dtTemp);

            Data dtUpBaj = new Data();
            dtUpBaj.put("5", qtBaj);

            lstOperar.addElement(dtUpBaj);
          }
        }

        //realizamos la actualizaci�n
        preparaBloqueo();
        if (lstOperar.size() > 0) {

          /*SrvTasVac servlet=new SrvTasVac();
               servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                        "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                        "sive_desa",
                        "sive_desa");
                       vResultado = (Lista) servlet.doDebug(0,lstOperar);
                  }*/
          vResultado = (Lista)this.getApp().getStub().doPost(10000,
              lstOperar,
              qtBloqueo,
              dtBloqueo,
              getApp());

          //obtener el nuevo cd_ope y fc_ultact de la base de datos.
        }
        int tamano = vResultado.size();
        String fecha = "";
        fecha = ( (Lista) vResultado.firstElement()).getFC_ULTACT();

        dtBrote.put("CD_OPE", dtBrote.getString("CD_OPE"));
        dtBrote.put("FC_ULTACT", fecha);

        dispose();

      }
      catch (Exception exc) {

        /*
                 this.getApp().trazaLog(exc);
                 this.getApp().showError(exc.getMessage());
         */

        dispose();
      } // del "trai cach"
      dispose();
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
    Inicializar(CInicializar.NORMAL);
  }

  // prepara la informaci�n para el control de bloqueo
  private void preparaBloqueo() {
    // query tool
    qtBloqueo = new QueryTool();
    qtBloqueo.putName("SIVE_BROTES");

    // campos que se leen
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // filtro
    qtBloqueo.putWhereType("CD_ANO", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANO", dtBrote.getString("CD_ANO"));
    qtBloqueo.putOperator("CD_ANO", "=");

    qtBloqueo.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBloqueo.putWhereValue("NM_ALERBRO", dtBrote.getString("NM_ALERBRO"));
    qtBloqueo.putOperator("NM_ALERBRO", "=");

    // informaci�n de bloqueo
    dtBloqueo = new Data();
    dtBloqueo.put("CD_OPE", dtBrote.getString("CD_OPE"));
    dtBloqueo.put("FC_ULTACT", dtBrote.getString("FC_ULTACT"));
  }

  QueryTool realizarUpdate(Data dtUpd) {

    QueryTool qtUpd = new QueryTool();

    // tabla de familias de tasas vacunables
    qtUpd.putName("SIVE_TATAQ_ENFVAC");

    // campos que se escriben
    qtUpd.putType("CD_GEDAD", QueryTool.STRING);
    qtUpd.putType("DIST_EDAD_I", QueryTool.INTEGER);
    qtUpd.putType("DIST_EDAD_F", QueryTool.INTEGER);
    qtUpd.putType("NM_VACENF", QueryTool.INTEGER);
    qtUpd.putType("NM_VACNOENF", QueryTool.INTEGER);
    qtUpd.putType("NM_NOVACENF", QueryTool.INTEGER);
    qtUpd.putType("NM_NOVACNOENF", QueryTool.INTEGER);

    // Valores de los campos
    qtUpd.putValue("CD_GEDAD", dtUpd.getString("CD_GEDAD"));
    qtUpd.putValue("DIST_EDAD_I", dtUpd.getString("DIST_EDAD_I"));
    qtUpd.putValue("DIST_EDAD_F", dtUpd.getString("DIST_EDAD_F"));
    qtUpd.putValue("NM_VACENF", dtUpd.getString("NM_VACENF"));
    qtUpd.putValue("NM_VACNOENF", dtUpd.getString("NM_VACNOENF"));
    qtUpd.putValue("NM_NOVACENF", dtUpd.getString("NM_NOVACENF"));
    qtUpd.putValue("NM_NOVACNOENF", dtUpd.getString("NM_NOVACNOENF"));

    //
    qtUpd.putWhereType("NM_TENFVAC", QueryTool.INTEGER);
    qtUpd.putWhereValue("NM_TENFVAC", dtUpd.getString("NM_TENFVAC"));
    qtUpd.putOperator("NM_TENFVAC", "=");
    qtUpd.putWhereType("CD_ANO", QueryTool.STRING);
    qtUpd.putWhereValue("CD_ANO", dtUpd.getString("CD_ANO"));
    qtUpd.putOperator("CD_ANO", "=");
    qtUpd.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtUpd.putWhereValue("NM_ALERBRO", dtUpd.getString("NM_ALERBRO"));
    qtUpd.putOperator("NM_ALERBRO", "=");

    return qtUpd;
  }

  QueryTool realizarAlta(Data dtAlta) {

    QueryTool qtAlt = new QueryTool();

    // tabla de familias de tasas vacunables
    qtAlt.putName("SIVE_TATAQ_ENFVAC");

    // campos que se escriben
    qtAlt.putType("NM_TENFVAC", QueryTool.INTEGER);
    qtAlt.putType("CD_ANO", QueryTool.STRING);
    qtAlt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qtAlt.putType("IT_EDAD", QueryTool.STRING);
    qtAlt.putType("CD_GEDAD", QueryTool.STRING);
    qtAlt.putType("DIST_EDAD_I", QueryTool.INTEGER);
    qtAlt.putType("DIST_EDAD_F", QueryTool.INTEGER);
    qtAlt.putType("NM_VACENF", QueryTool.INTEGER);
    qtAlt.putType("NM_VACNOENF", QueryTool.INTEGER);
    qtAlt.putType("NM_NOVACENF", QueryTool.INTEGER);
    qtAlt.putType("NM_NOVACNOENF", QueryTool.INTEGER);

    // Valores de los campos
    qtAlt.putValue("NM_TENFVAC", "");
    qtAlt.putValue("CD_ANO", dtAlta.getString("CD_ANO"));
    qtAlt.putValue("NM_ALERBRO", dtAlta.getString("NM_ALERBRO"));
    qtAlt.putValue("IT_EDAD", "S");
    qtAlt.putValue("CD_GEDAD", dtAlta.getString("CD_GEDAD"));
    qtAlt.putValue("DIST_EDAD_I", dtAlta.getString("DIST_EDAD_I"));
    qtAlt.putValue("DIST_EDAD_F", dtAlta.getString("DIST_EDAD_F"));
    qtAlt.putValue("NM_VACENF", dtAlta.getString("NM_VACENF"));
    qtAlt.putValue("NM_VACNOENF", dtAlta.getString("NM_VACNOENF"));
    qtAlt.putValue("NM_NOVACENF", dtAlta.getString("NM_NOVACENF"));
    qtAlt.putValue("NM_NOVACNOENF", dtAlta.getString("NM_NOVACNOENF"));

    return qtAlt;
  }

  QueryTool realizarBaja(Data dtBaj) {

    QueryTool qtBaj = new QueryTool();

    // tabla de familias de productos
    qtBaj.putName("SIVE_TATAQ_ENFVAC");
    //
    qtBaj.putWhereType("NM_TENFVAC", QueryTool.INTEGER);
    qtBaj.putWhereValue("NM_TENFVAC", dtBaj.getString("NM_TENFVAC"));
    qtBaj.putOperator("NM_TENFVAC", "=");
    qtBaj.putWhereType("CD_ANO", QueryTool.STRING);
    qtBaj.putWhereValue("CD_ANO", dtBaj.getString("CD_ANO"));
    qtBaj.putOperator("CD_ANO", "=");
    qtBaj.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qtBaj.putWhereValue("NM_ALERBRO", dtBaj.getString("NM_ALERBRO"));
    qtBaj.putOperator("NM_ALERBRO", "=");

    return qtBaj;
  }

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

  public void chkResultado_itemStateChanged(ItemEvent e) {
    //Indicador de selecci�n
    int IndSel = 0;
    IndSel = e.getStateChange();

    if (IndSel == 1) { //seleccionado
      int itataq = 0;
      double tataq = 0;
      int Ncomp = 0;
      int Rnm_venf = 0;
      int Rnm_vnenf = 0;
      int Rnm_nvenf = 0;
      int Rnm_nvnenf = 0;
      int Rnm_enf = 0;
      int Rnm_exp = 0;
      Lista filaTotal = null;
      filaTotal = new Lista();
      Data dtfilaTotal = null;

      filaTotal = clmMantTasa.getLista();
      dtfilaTotal = (Data) filaTotal.lastElement();
      Rnm_venf = Integer.parseInt(dtfilaTotal.getString("NM_VACENF"));
      Rnm_vnenf = Integer.parseInt(dtfilaTotal.getString("NM_VACNOENF"));
      Rnm_nvenf = Integer.parseInt(dtfilaTotal.getString("NM_NOVACENF"));
      Rnm_nvnenf = Integer.parseInt(dtfilaTotal.getString("NM_NOVACNOENF"));
      Rnm_enf = Rnm_venf + Rnm_nvenf;
      Rnm_exp = Rnm_venf + Rnm_vnenf + Rnm_nvenf + Rnm_nvnenf;
      tataq = ( (double) Rnm_enf / (double) Rnm_exp) * 100;
      cTasa.setText(cogeDecimal(tataq, 2, '.'));
//    itataq = (int) tataq;
//    cTasa.setText((new Integer(itataq)).toString());
    }
    else {
      cTasa.setText("");
    }
  }
}

class DialogActionAdapter1
    implements java.awt.event.ActionListener, Runnable {
  PanTasVac adaptee;
  ActionEvent e;

  DialogActionAdapter1(PanTasVac adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
    /*    if(SincrEventos.bloquea(adaptee.modoOperacion,adaptee)){
          adaptee.btn_actionPerformed(e);
          SincrEventos.desbloquea(adaptee);
        }*/

  }
}

// listener
class DialogchkitemAdapter
    implements java.awt.event.ItemListener {
  PanTasVac adaptee;
  DialogchkitemAdapter(PanTasVac adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkResultado_itemStateChanged(e);
  }
}
