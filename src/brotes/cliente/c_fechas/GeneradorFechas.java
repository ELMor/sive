
package brotes.cliente.c_fechas;

import java.net.URL;

import brotes.datos.c_fechas.DataConvFec;
import capp.CApp;
import capp.CLista;
import sapp.StubSrvBD;

public class GeneradorFechas {

  final String strSERVLET = "servlet/SrvConvFec";
  protected StubSrvBD stubCliente = new StubSrvBD();

  // modos de operación
  final int servletGENERAR_FECHAS = 0;
  final int servletOBTENER = 4;

  public GeneradorFechas(CApp app, String sFecInicial, String anoIni,
                         int numAnos) {
    CLista data;
    try {
      // obtiene y pinta la lista nueva
      data = new CLista();

      data.addElement(new DataConvFec(sFecInicial, anoIni, numAnos));

      // apunta al servlet principal
      stubCliente.setUrl(new URL(app.getURL() + strSERVLET));

      // obtiene la lista
      stubCliente.doPost(servletGENERAR_FECHAS, data);
      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }
}