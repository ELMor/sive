package brotes.cliente.c_fechas;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Vector;

import brotes.cliente.c_comuncliente.BDatosEDO;
import brotes.datos.c_fechas.DataConvFec;
import brotes.datos.c_fechas.FecyNum;
import capp.CLista;
//Clases nuevas
import capp2.CMessage;
import sapp.StubSrvBD;

public class conversorfechas {

  protected CLista listaConvFec = new CLista();
  final String strSERVLET = "servlet/SrvConvFecCent"; //Retocado
  protected StubSrvBD stubCliente = new StubSrvBD();

  // modos de operaci�n
  final int servletGENERAR_FECHAS = 0;
  final int servletOBTENER = 4;

  String sAno; //A�o al que se refiere este conversor
  Vector vFec = new Vector();
  java.util.Date fecComAno;
  String sComAno;

  boolean bHayFec = false;
  Enumeration enum;
  CMessage msgBox;

  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

  //Da el conversor del a�o epid correspondiente a una fecha dada en formato "dd/MM/yyyy"
  //Si no hay a�o epid para esa fecha devuelve ""
  public static conversorfechas getConvDeFecha(String fecha, capp2.CApp applet) {

    capp2.CApp app = applet;

    java.util.Date dFecha;
    int ano;
//    String res = "";
    Calendar calen = new GregorianCalendar();
    conversorfechas conv;
    conversorfechas res = null; //resultado
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    try {

      //Pasa el string de fecha a Date
      dFecha = formater.parse(fecha);
      //obtiene el a�o correspondiente a esa fecha
      calen.setTime(dFecha);
      ano = calen.get(Calendar.YEAR);

      //Si la fecha es de �ltimos de diciembre vemos si pertenece al a�o EPIDEMIOLOGICO sigte
      if (
          (calen.get(Calendar.MONTH) == Calendar.DECEMBER) &&
          (calen.get(Calendar.DAY_OF_MONTH) > 23)
          ) {
        //objeto para obtener fechas del a�o epid sigte
        conv = new conversorfechas(Integer.toString(ano + 1), app);
        if (conv.anoValido() == true) {
          //Si esa fecha pertenece a alguna sem epid. sigte se devolver� ese a�o
          if (conv.getNumSem(fecha) != -1) {
            res = conv;
          }
        }
      }

      //Si la fecha es de principios de enero vemos si pertenece al a�o EPIDEMIOLOGICO anterior
      else if (
          (calen.get(Calendar.MONTH) == Calendar.JANUARY) &&
          (calen.get(Calendar.DAY_OF_MONTH) < 9)
          ) {
        //objeto para obtener fechas del a�o epid sigte
        conv = new conversorfechas(Integer.toString(ano - 1), app);
        if (conv.anoValido() == true) {
          //Si esa fecha pertenece a alguna sem epid. de a�o anterior se devolver� ese a�o
          if (conv.getNumSem(fecha) != -1) {
            res = conv;
          }
        }
      }

      //Si a�n no se ha asignado a�o se prueba con el propio a�o cronol�gico
      if (res == null) {
        conv = new conversorfechas(Integer.toString(ano), app);
        if (conv.anoValido() == true) {
          //Si esa fecha pertenece a alguna sem e se devolver� ese a�o
          if (conv.getNumSem(fecha) != -1) {
            res = conv;
          }
        }
      }

      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return res;
  }

  //Constructor: Consulta b. datos para obtener fecha comienzo a�o
  // y construye vector con 51 o 52 fechas y su n�mero de semana correspondientes
  public conversorfechas(String ano, capp2.CApp applet) {

    capp2.CApp app = applet;

    DataConvFec datConvFec;
    CLista data;
    java.util.Date fecPriSem;
    //Consultar� a la b. datos para obtener la fecha del comAno;
    //Cambiar este c�digo !!!!!!!!!!!!????????????????????????????

    try {
      //Pasa el a�o al atributo de este objeto
      sAno = ano;

      listaConvFec.setState(CLista.listaNOVALIDA);

      // obtiene y pinta la lista nueva
      data = new CLista();

      data.addElement(new DataConvFec(ano));

      /*
              // apunta al servlet principal
              stubCliente.setUrl(new URL(sURL + strSERVLET)); //^^^^^^^^^^
              // obtiene la lista
              listaConvFec = (CLista) stubCliente.doPost(servletOBTENER, data);
              // obtiene la lista
              listaConvFec = (CLista) BDatosEDO.traerDebug(app,new SrvConvFec(),servletOBTENER, data);
       */

      // consulta el servidor
      listaConvFec = (CLista) BDatosEDO.ejecutaSQL(true, app, strSERVLET,
          servletOBTENER, data);

      if (listaConvFec != null) {
        //Obtiene datos de fechas del a�o
        datConvFec = (DataConvFec) (listaConvFec.firstElement());
        vFec = datConvFec.getFec();
        bHayFec = datConvFec.getHayFec();

        //*********************************************

         //Si ese a�o tiene fechas
        if (bHayFec == true) {
          //Deja preparada la fecha del primer d�a del a�o epid

          // Restando 6 dias a la fecha de primera semana ( se multiplica por num de milisegundos de un dia)
          fecPriSem = ( (FecyNum) (vFec.firstElement())).getFec();
          fecComAno = new java.util.Date(0);
          fecComAno.setTime(fecPriSem.getTime() - (long) (6 * 24 * 3600 * 1000));
          //El String se obtiene directamente del servidor (Falla func Format)
          sComAno = datConvFec.getComAno();
        } //if

      } //if

      data = null;
      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

  //Devuelve el num semana  que corresponde a la fecha especificada
  //Devuelve -1 si la fecha no est� en el a�o epidemiol�gico tratado
  public int getNumSem(String sFech) throws Exception {
    boolean encontrado = false;
    int indice = 0;
    java.util.Date fecha;
    java.util.Date fecFinAno;
    String sFinAno;

    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    try {

      fecha = formater.parse(sFech);

      fecFinAno = ( (FecyNum) (vFec.lastElement())).getFec();
      //      sComAno = formater.format(fecComAno);
      //      sFinAno = formater.format(fecFinAno);
      sFinAno = ( (FecyNum) (vFec.lastElement())).getsFec();

      //Para comparar iguladad entre 2 fechas se comparan sus Strings
      //La func

      //Primer d�a del a�o
      if (sFech.equals(sComAno)) {
        return (1);
      }
      //�ltimo dia del a�o
      else if (sFech.equals(sFinAno)) {
        indice = ( (FecyNum) (vFec.lastElement())).getnum();
        return (indice);
      }
      //entre el primer y el �ltimo d�a
      else if (
          (fecha.after(fecComAno)) &&
          (fecha.before( ( (FecyNum) (vFec.lastElement())).getFec()))
          ) {
        //Buscamos indice entre peimer y ult dia del a�o epid.
        //Tener en cuenta que fecha fin semana 1 se gyuarda en indice 0 del vector

        for (int k = 0; (k < vFec.size()) && (encontrado == false); k++) {

          if (fecha.equals( ( (FecyNum) (vFec.elementAt(k))).getFec())) {
            encontrado = true;
            indice = k + 1;

          }
          //p. ej si est� antes de fecha indicada en pos 0  es en primera semana  (ind 1)
          if (fecha.before( ( (FecyNum) (vFec.elementAt(k))).getFec())) {
            encontrado = true;
            indice = k + 1;
          }
        }
        if (encontrado == true) {
          return (indice);
        }
      }

      else {

        return ( -1);
      }

      // envia la excepci�n
    }
    catch (Exception e) {
      //# System.Out.println(e.getMessage() );
      e.printStackTrace();
      throw (e);
    }

    return ( -1); //Sentencia inlcanzable . Compilador obliga
  }

  //Devuelve un String indicando la fecha de fin de sem epid que corresponde
  // al n�mero de semana epid. indicado como par.
  //Devuelve "" si no existe esa fecha
  public String getFec(int numSem) throws Exception {

    //Devuelve el String solo si me indican num . semana v�lido
    //Si pej a�o tiene 52 sem , habr� en vector 52 indices ( del 0 al 51)
    // Num sem es v�lido si es mayor que 0 y menor que 53 (semanas entre 1 y 52)

    if (bHayFec == true) {
      if ( (numSem > 0) && (numSem < ( (vFec.size()) + 1))) {
        FecyNum elemento = (FecyNum) (vFec.elementAt(numSem - 1));
        return (elemento.getsFec());
      }
      else {
        return ("");
      }
    }
    else {
      return ("");
    }
  }

  public boolean anoValido() {
    return bHayFec;
  }

  public Object clone() {
    Object repetido = null;
    try {
      repetido = super.clone();
      // envia la excepci�n
    }
    catch (Exception e) {
      //# System.Out.println(e.getMessage() );
      e.printStackTrace();
    }
    return (repetido);
  }

  public String getAno() {
    return (sAno);
  }

  public String getFecComAno() {
//     sComAno = formater.format(fecComAno);
    return sComAno;
  }

  public String getFecFinPriSem() {
    String sDev;
    FecyNum elemento = (FecyNum) (vFec.firstElement());
    // sDev = formater.format(elemento.getFec());
    sDev = elemento.getsFec();
    return (sDev);
  }

  public int getNumUltSem() {
    return ( ( (FecyNum) (vFec.lastElement())).getnum());
  }

} //Clase
