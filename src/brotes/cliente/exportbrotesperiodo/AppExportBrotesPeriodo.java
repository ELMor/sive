package brotes.cliente.exportbrotesperiodo;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppExportBrotesPeriodo
    extends CApp
    implements CInicializar {

  PanExportBrotesPeriodo pan = null;

  public AppExportBrotesPeriodo() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo(
        "Volcado a fichero de la informaci�n general de brotes de un per�odo");
    pan = new PanExportBrotesPeriodo(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(false);
        break;
    }
  }
}
