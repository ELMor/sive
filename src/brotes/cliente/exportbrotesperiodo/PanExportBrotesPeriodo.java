package brotes.cliente.exportbrotesperiodo;

import java.util.Vector;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.FileDialog;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import brotes.cliente.c_componentes.CPnlCodigoExt;
import brotes.cliente.c_componentes.ContCPnlCodigoExt;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPanel;
import capp2.CTexto;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import util_ficheros.EditorFichero;

public class PanExportBrotesPeriodo
    extends CPanel
    implements CInicializar, ContCPnlCodigoExt {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  XYLayout xyLayout = new XYLayout();

  //constantes para localizaciones
  final int MARGENIZQ = 25;
  final int MARGENSUP = 25;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;

  protected boolean fallo = false;

  //componentes q conforman el panel
  Label lblFNotifDesde = null;
  Label lblFNotifHasta = null;
  Label lblGrupo = null;
  fechas.CFecha txtFNotifDesde = new fechas.CFecha("S");
  fechas.CFecha txtFNotifHasta = new fechas.CFecha("S");
  CPnlCodigoExt pnlGrupo = null;
  //a�adir car�cter de separaci�n
  Label lblCaracter = null;
  CTexto txtCaracter = null;
  //busqueda de directorio
  capp2.CFileName panFichero;

  ButtonControl btnAceptar = new ButtonControl();

  //vectores de Strings con los nombres de los campos
  Vector vCamposBro = new Vector();
  Vector vCamposProt = new Vector();
  Vector vCamposAgcausal = new Vector();
  Vector vCamposFaccontr = new Vector();
  Vector vCamposMedadopt = new Vector();
  Vector vCamposSintomas = new Vector();
  Vector vCamposDistedad = new Vector();
  Vector vCamposTataqali = new Vector();
  Vector vCamposMuestras = new Vector();
  Vector vCamposAlimimpl = new Vector();

  //vbles q guardan "" si su f�chero correspondiente no se ha creado y
  //el nombre en caso contrario
  String Brot = new String();
  String Agcausal = new String();
  String Faccontr = new String();
  String Medadopt = new String();
  String Sintomas = new String();
  String Distedad = new String();
  String Tataqali = new String();
  String Muestras = new String();
  String Alimimpl = new String();

  CApp apl = null;

  // Botones
  PanExportBrotesPeriodoActionAdapter actionAdapter = null;

  //constructor
  public PanExportBrotesPeriodo(CApp a) {
    super(a);
    apl = a;
    try {
      //configuro el panel de origen
      QueryTool qtGrupo = new QueryTool();
      qtGrupo.putName("SIVE_GRUPO_BROTE");
      qtGrupo.putType("CD_GRUPO", QueryTool.STRING);
      qtGrupo.putType("DS_GRUPO", QueryTool.STRING);

      pnlGrupo = new CPnlCodigoExt(a,
                                   this,
                                   qtGrupo,
                                   "CD_GRUPO",
                                   "DS_GRUPO",
                                   true,
                                   "Grupo de brotes",
                                   "Grupo de brotes",
                                   this);
      panFichero = new capp2.CFileName(a, FileDialog.SAVE, 2);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor

  void jbInit() throws Exception {
    final String imgAceptar = "images/aceptar.gif";

    Color cObligatorio = new Color(255, 255, 150);

    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().CargaImagenes();

    //escuchadores
    actionAdapter = new PanExportBrotesPeriodoActionAdapter(this);

    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.addActionListener(actionAdapter);

    lblFNotifDesde = new Label("Fecha desde:");
    lblFNotifHasta = new Label("hasta:");
    lblGrupo = new Label("Grupo:");

    lblCaracter = new Label("Car�cter de separaci�n entre campos:");
    txtCaracter = new CTexto(1);
    txtCaracter.setBackground(cObligatorio);
    txtCaracter.setText("$");

    txtFNotifDesde.setBackground(cObligatorio);
    txtFNotifHasta.setBackground(cObligatorio);

    xyLayout.setHeight(270);
    xyLayout.setWidth(500);
    this.setLayout(xyLayout);

    this.add(lblFNotifDesde, new XYConstraints(MARGENIZQ, MARGENSUP, 75, ALTO));
    this.add(txtFNotifDesde,
             new XYConstraints(MARGENIZQ + INTERHOR + 75, MARGENSUP, 100, ALTO));
    this.add(lblFNotifHasta,
             new XYConstraints(MARGENIZQ + 2 * INTERHOR + 75 + 100, MARGENSUP,
                               35, ALTO));
    this.add(txtFNotifHasta,
             new XYConstraints(MARGENIZQ + 3 * INTERHOR + 75 + 100 + 35,
                               MARGENSUP, 100, ALTO));

    this.add(lblGrupo,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 40,
                               ALTO));
    this.add(pnlGrupo,
             new XYConstraints(MARGENIZQ + INTERHOR + 40 + 30,
                               MARGENSUP + ALTO + INTERVERT, 360, ALTO + 10));

    this.add(lblCaracter,
             new XYConstraints(MARGENIZQ, MARGENSUP + 2 * ALTO + 2 * INTERVERT,
                               225, ALTO));
    this.add(txtCaracter,
             new XYConstraints(MARGENIZQ + INTERHOR + 225,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 30, ALTO));

    this.add(panFichero,
             new XYConstraints(MARGENIZQ - 12,
                               MARGENSUP + 3 * ALTO + 3 * INTERVERT, 425, 40));
    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 425 - 88 - 12,
                               MARGENSUP + 3 * ALTO + 4 * INTERVERT + 40, 88,
                               29));

    //configuro el vector vCamposBro:DataCampos
    rellenarVectorBro();
    rellenarVectorAgcausal();
    rellenarVectorFaccontr();
    rellenarVectorMedadopt();
    rellenarVectorSintomas();
    rellenarVectorDistedad();
    rellenarVectorTataqali();
    rellenarVectorMuestras();
    rellenarVectorAlimimpl();
  } //end jbinit

  void rellenarVectorAgcausal() {
    vCamposAgcausal.addElement("NM_SECAGB");
    vCamposAgcausal.addElement("CD_ANO");
    vCamposAgcausal.addElement("NM_ALERBRO");
    vCamposAgcausal.addElement("CD_GRUPO");
    vCamposAgcausal.addElement("CD_AGENTC");
    vCamposAgcausal.addElement("IT_CONFIRMADO");
    vCamposAgcausal.addElement("DS_OBSERV");
    vCamposAgcausal.addElement("DS_MASAGCAUSAL");
  }

  void rellenarVectorFaccontr() {
    vCamposFaccontr.addElement("CD_GRUPO");
    vCamposFaccontr.addElement("CD_FACCONT");
    vCamposFaccontr.addElement("DS_FACCON");
    vCamposFaccontr.addElement("IT_MASFACCONT");
    vCamposFaccontr.addElement("IT_REPE");
  }

  void rellenarVectorMedadopt() {
    vCamposMedadopt.addElement("CD_GRUPO");
    vCamposMedadopt.addElement("CD_MEDIDA");
    vCamposMedadopt.addElement("DS_MEDIDA");
    vCamposMedadopt.addElement("IT_INFADIC");
    vCamposMedadopt.addElement("IT_REPE");
  }

  void rellenarVectorSintomas() {
    vCamposSintomas.addElement("CD_SINTOMA");
    vCamposSintomas.addElement("CD_ANO");
    vCamposSintomas.addElement("NM_ALERBRO");
    vCamposSintomas.addElement("NM_CASOS");
    vCamposSintomas.addElement("DS_MASINF");
  }

  void rellenarVectorDistedad() {
    vCamposDistedad.addElement("NM_DISTGEDAD");
    vCamposDistedad.addElement("CD_ANO");
    vCamposDistedad.addElement("NM_ALERBRO");
    vCamposDistedad.addElement("IT_EDAD");
    vCamposDistedad.addElement("CD_GEDAD");
    vCamposDistedad.addElement("DIST_EDAD_I");
    vCamposDistedad.addElement("DIST_EDAD_F");
    vCamposDistedad.addElement("NM_V_EXP");
    vCamposDistedad.addElement("NM_M_EXP");
    vCamposDistedad.addElement("NM_NC_EXP");
    vCamposDistedad.addElement("NM_V_ENF");
    vCamposDistedad.addElement("NM_M_ENF");
    vCamposDistedad.addElement("NM_NC_ENF");
    vCamposDistedad.addElement("NM_V_HOS");
    vCamposDistedad.addElement("NM_M_HOS");
    vCamposDistedad.addElement("NM_NC_HOS");
    vCamposDistedad.addElement("NM_V_DEF");
    vCamposDistedad.addElement("NM_M_DEF");
    vCamposDistedad.addElement("NM_NC_DEF");
  }

  void rellenarVectorTataqali() {
    vCamposTataqali.addElement("NM_ALIBROTE");
    vCamposTataqali.addElement("CD_ANO");
    vCamposTataqali.addElement("NM_ALERBRO");
    vCamposTataqali.addElement("CD_TALIMENTO");
    vCamposTataqali.addElement("IT_CALCTASA");
    vCamposTataqali.addElement("NM_EXPENF");
    vCamposTataqali.addElement("NM_EXPNOENF");
    vCamposTataqali.addElement("NM_NOEXPENF");
    vCamposTataqali.addElement("NM_NOEXPNOENF");
    vCamposTataqali.addElement("DS_ALI");
  }

  void rellenarVectorMuestras() {
    vCamposMuestras.addElement("NM_MUESTRA");
    vCamposMuestras.addElement("CD_MUESTRA");
    vCamposMuestras.addElement("CD_ANO");
    vCamposMuestras.addElement("NM_ALERBRO");
    vCamposMuestras.addElement("DS_RMUESTRA");
    vCamposMuestras.addElement("NM_MUESBROTE");
    vCamposMuestras.addElement("NM_POSITBROTE");
  }

  void rellenarVectorAlimimpl() {
    vCamposAlimimpl.addElement("NM_ALIBROTE");
    vCamposAlimimpl.addElement("CD_ANO");
    vCamposAlimimpl.addElement("NM_ALERBRO");
    vCamposAlimimpl.addElement("CD_TALIMENTO");
    vCamposAlimimpl.addElement("CD_TIMPLICACION");
    vCamposAlimimpl.addElement("DS_NOMCOMER");
    vCamposAlimimpl.addElement("DS_FABRICAN");
    vCamposAlimimpl.addElement("DS_LOTE");
    vCamposAlimimpl.addElement("CD_MCOMERCALI");
    vCamposAlimimpl.addElement("CD_TRATPREVALI");
    vCamposAlimimpl.addElement("CD_FINGERIRALI");
    vCamposAlimimpl.addElement("CD_MTVIAJEALI");
    vCamposAlimimpl.addElement("CD_LCONTAMIALI");
    vCamposAlimimpl.addElement("CD_PAIS_LCONA");
    vCamposAlimimpl.addElement("CD_LPREPALI");
    vCamposAlimimpl.addElement("CD_PAIS_LPA");
    vCamposAlimimpl.addElement("DS_NOMESTAB");
    vCamposAlimimpl.addElement("DS_DIRESTAB");
    vCamposAlimimpl.addElement("CD_POSESTAB");
    vCamposAlimimpl.addElement("CD_PROV_LPREP");
    vCamposAlimimpl.addElement("CD_MUN_LPREP");
    vCamposAlimimpl.addElement("DS_TELEF_LPREP");
    vCamposAlimimpl.addElement("FC_PREPARACION");
    vCamposAlimimpl.addElement("CD_CONSUMOALI");
    vCamposAlimimpl.addElement("CD_PAIS_LCON");
    vCamposAlimimpl.addElement("DS_ESTAB_LCON");
    vCamposAlimimpl.addElement("DS_DIRESTAB_LCON");
    vCamposAlimimpl.addElement("CD_POSESTAB_LCON");
    vCamposAlimimpl.addElement("CD_PROV_LCON");
    vCamposAlimimpl.addElement("CD_MUN_LCON");
    vCamposAlimimpl.addElement("DS_TELEF_LCON");
    vCamposAlimimpl.addElement("CD_PAIS_DE");
    vCamposAlimimpl.addElement("CD_PAIS_A");
    vCamposAlimimpl.addElement("IT_CALCTASA");
    vCamposAlimimpl.addElement("NM_EXPENF");
    vCamposAlimimpl.addElement("NM_EXPNOENF");
    vCamposAlimimpl.addElement("NM_NOEXPENF");
    vCamposAlimimpl.addElement("NM_NOEXPNOENF");
    vCamposAlimimpl.addElement("DS_ALI");
  }

  //rellena el vector de DataCampos con los nombres de los campos y su longitud
  void rellenarVectorBro() {
    vCamposBro.addElement("CD_ANO");
    vCamposBro.addElement("NM_ALERBRO");
    vCamposBro.addElement("CD_TIPOCOL");
    vCamposBro.addElement("CD_TNOTIF");
    vCamposBro.addElement("CD_TRANSMIS");
    vCamposBro.addElement("CD_GRUPO");
    vCamposBro.addElement("CD_TBROTE");
    vCamposBro.addElement("FC_FECHAHORA");
    vCamposBro.addElement("DS_BROTE");
    vCamposBro.addElement("DS_NOMCOL");
    vCamposBro.addElement("DS_DIRCOL");
    vCamposBro.addElement("CD_POSTALCOL");
    vCamposBro.addElement("CD_PROVCOL");
    vCamposBro.addElement("CD_MUNCOL");
    vCamposBro.addElement("DS_NMCALLE");
    vCamposBro.addElement("DS_PISOCOL");
    vCamposBro.addElement("DS_TELCOL");
    vCamposBro.addElement("CD_NIVEL_1_LCA");
    vCamposBro.addElement("CD_NIVEL_2_LCA");
    vCamposBro.addElement("CD_ZBS_LCA");
    vCamposBro.addElement("CD_NIVEL_1_LE");
    vCamposBro.addElement("CD_NIVEL_2_LE");
    vCamposBro.addElement("CD_ZBS_LE");
    vCamposBro.addElement("NM_MANIPUL");
    vCamposBro.addElement("IT_RESCALC");
    vCamposBro.addElement("NM_EXPUESTOS");
    vCamposBro.addElement("NM_ENFERMOS");
    vCamposBro.addElement("NM_INGHOSP");
    vCamposBro.addElement("NM_DEFUNCION");
    vCamposBro.addElement("FC_EXPOSICION");
    vCamposBro.addElement("FC_ISINPRIMC");
    vCamposBro.addElement("FC_FSINPRIMC");
    vCamposBro.addElement("NM_PERINMIN");
    vCamposBro.addElement("NM_PERINMAX");
    vCamposBro.addElement("NM_PERINMED");
    vCamposBro.addElement("NM_DCUACMIN");
    vCamposBro.addElement("NM_DCUACMAX");
    vCamposBro.addElement("NM_DCUACMED");
    vCamposBro.addElement("NM_VACNENF");
    vCamposBro.addElement("NM_VACENF");
    vCamposBro.addElement("NM_NVACNENF");
    vCamposBro.addElement("NM_NVACENF");
    vCamposBro.addElement("DS_OBSERV");
    vCamposBro.addElement("IT_PERIN");
    vCamposBro.addElement("IT_DCUAC");
    vCamposBro.addElement("CD_OPE");
    vCamposBro.addElement("FC_ULTACT");
  }

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
    }
  }

  QueryTool crearQTBrote(String ano, String nmaler) {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_BROTES");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("CD_TIPOCOL", QueryTool.STRING);
    qt.putType("CD_TNOTIF", QueryTool.STRING);
    qt.putType("CD_TRANSMIS", QueryTool.STRING);
    qt.putType("CD_GRUPO", QueryTool.STRING);
    qt.putType("CD_TBROTE", QueryTool.STRING);
    qt.putType("FC_FECHAHORA", QueryTool.TIMESTAMP);
    qt.putType("DS_BROTE", QueryTool.STRING);
    qt.putType("DS_NOMCOL", QueryTool.STRING);
    qt.putType("DS_DIRCOL", QueryTool.STRING);
    qt.putType("CD_POSTALCOL", QueryTool.STRING);
    qt.putType("CD_PROVCOL", QueryTool.STRING);
    qt.putType("CD_MUNCOL", QueryTool.STRING);
    qt.putType("DS_NMCALLE", QueryTool.STRING);
    qt.putType("DS_PISOCOL", QueryTool.STRING);
    qt.putType("DS_TELCOL", QueryTool.STRING);
    qt.putType("CD_NIVEL_1_LCA", QueryTool.STRING);
    qt.putType("CD_NIVEL_2_LCA", QueryTool.STRING);
    qt.putType("CD_ZBS_LCA", QueryTool.STRING);
    qt.putType("CD_NIVEL_1_LE", QueryTool.STRING);
    qt.putType("CD_NIVEL_2_LE", QueryTool.STRING);
    qt.putType("CD_ZBS_LE", QueryTool.STRING);
    qt.putType("NM_MANIPUL", QueryTool.INTEGER);
    qt.putType("IT_RESCALC", QueryTool.STRING);
    qt.putType("NM_EXPUESTOS", QueryTool.INTEGER);
    qt.putType("NM_ENFERMOS", QueryTool.INTEGER);
    qt.putType("NM_INGHOSP", QueryTool.INTEGER);
    qt.putType("NM_DEFUNCION", QueryTool.INTEGER);
    qt.putType("FC_EXPOSICION", QueryTool.TIMESTAMP);
    qt.putType("FC_ISINPRIMC", QueryTool.TIMESTAMP);
    qt.putType("FC_FSINPRIMC", QueryTool.TIMESTAMP);
    qt.putType("NM_PERINMIN", QueryTool.INTEGER);
    qt.putType("NM_PERINMAX", QueryTool.INTEGER);
    qt.putType("NM_PERINMED", QueryTool.INTEGER);
    qt.putType("NM_DCUACMIN", QueryTool.INTEGER);
    qt.putType("NM_DCUACMAX", QueryTool.INTEGER);
    qt.putType("NM_DCUACMED", QueryTool.INTEGER);
    qt.putType("NM_VACNENF", QueryTool.INTEGER);
    qt.putType("NM_VACENF", QueryTool.INTEGER);
    qt.putType("NM_NVACNENF", QueryTool.INTEGER);
    qt.putType("NM_NVACENF", QueryTool.INTEGER);
    qt.putType("DS_OBSERV", QueryTool.STRING);
    qt.putType("IT_PERIN", QueryTool.STRING);
    qt.putType("IT_DCUAC", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", ano);
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", nmaler);
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end crearQT

  QueryTool crearQTAgcausal(String ano, String nmaler) {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_AGCAUSAL_BROTE");

    // Campos que se quieren leer (SELECT)
    qt.putType("NM_SECAGB", QueryTool.INTEGER);
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("CD_GRUPO", QueryTool.STRING);
    qt.putType("CD_AGENTC", QueryTool.STRING);
    qt.putType("IT_CONFIRMADO", QueryTool.STRING);
    qt.putType("DS_OBSERV", QueryTool.STRING);
    qt.putType("DS_MASAGCAUSAL", QueryTool.STRING);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", ano);
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", nmaler);
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end

  QueryTool crearQTFaccontr(String grupo) {

    QueryTool qt = new QueryTool();
    qt.putName("SIVE_FACT_CONTRIB");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_GRUPO", QueryTool.STRING);
    qt.putType("CD_FACCONT", QueryTool.STRING);
    qt.putType("DS_FACCON", QueryTool.STRING);
    qt.putType("IT_MASFACCONT", QueryTool.STRING);
    qt.putType("IT_REPE", QueryTool.STRING);

    qt.putWhereType("CD_GRUPO", QueryTool.STRING);
    qt.putWhereValue("CD_GRUPO", grupo);
    qt.putOperator("CD_GRUPO", "=");

    return qt;
  } //end

  QueryTool crearQTMedadopt(String grupo) {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_MEDIDAS");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_GRUPO", QueryTool.STRING);
    qt.putType("CD_MEDIDA", QueryTool.STRING);
    qt.putType("DS_MEDIDA", QueryTool.STRING);
    qt.putType("IT_INFADIC", QueryTool.STRING);
    qt.putType("IT_REPE", QueryTool.STRING);

    qt.putWhereType("CD_GRUPO", QueryTool.STRING);
    qt.putWhereValue("CD_GRUPO", grupo);
    qt.putOperator("CD_GRUPO", "=");

    return qt;
  } //end

  QueryTool crearQTSintomas(String ano, String nmaler) {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_SINTOMAS_BROTE");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_SINTOMA", QueryTool.STRING);
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("NM_CASOS", QueryTool.INTEGER);
    qt.putType("DS_MASINF", QueryTool.STRING);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", ano);
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", nmaler);
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end

  QueryTool crearQTDistedad(String ano, String nmaler) {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_BROTES_DISTGEDAD");

    // Campos que se quieren leer (SELECT)
    qt.putType("NM_DISTGEDAD", QueryTool.INTEGER);
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("IT_EDAD", QueryTool.STRING);
    qt.putType("CD_GEDAD", QueryTool.STRING);
    qt.putType("DIST_EDAD_I", QueryTool.INTEGER);
    qt.putType("DIST_EDAD_F", QueryTool.INTEGER);
    qt.putType("NM_V_EXP", QueryTool.INTEGER);
    qt.putType("NM_M_EXP", QueryTool.INTEGER);
    qt.putType("NM_NC_EXP", QueryTool.INTEGER);
    qt.putType("NM_V_ENF", QueryTool.INTEGER);
    qt.putType("NM_M_ENF", QueryTool.INTEGER);
    qt.putType("NM_NC_ENF", QueryTool.INTEGER);
    qt.putType("NM_V_HOS", QueryTool.INTEGER);
    qt.putType("NM_M_HOS", QueryTool.INTEGER);
    qt.putType("NM_NC_HOS", QueryTool.INTEGER);
    qt.putType("NM_V_DEF", QueryTool.INTEGER);
    qt.putType("NM_M_DEF", QueryTool.INTEGER);
    qt.putType("NM_NC_DEF", QueryTool.INTEGER);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", ano);
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", nmaler);
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end

  QueryTool crearQTTataqali(String ano, String nmaler) {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_ALI_BROTE");

    // Campos que se quieren leer (SELECT)
    qt.putType("NM_ALIBROTE", QueryTool.INTEGER);
    qt.putType("DS_ALI", QueryTool.STRING);
    qt.putType("IT_CALCTASA", QueryTool.STRING);
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("CD_TALIMENTO", QueryTool.STRING);
    qt.putType("NM_EXPENF", QueryTool.INTEGER);
    qt.putType("NM_EXPNOENF", QueryTool.INTEGER);
    qt.putType("NM_NOEXPENF", QueryTool.INTEGER);
    qt.putType("NM_NOEXPNOENF", QueryTool.INTEGER);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", ano);
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", nmaler);
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end

  QueryTool crearQTMuestras(String ano, String nmaler) {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_MUESTRAS_BROTE");

    // Campos que se quieren leer (SELECT)
    qt.putType("NM_MUESTRA", QueryTool.INTEGER);
    qt.putType("CD_MUESTRA", QueryTool.STRING);
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("DS_RMUESTRA", QueryTool.STRING);
    qt.putType("NM_MUESBROTE", QueryTool.INTEGER);
    qt.putType("NM_POSITBROTE", QueryTool.INTEGER);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", ano);
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", nmaler);
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end

  QueryTool crearQTAlimimpl(String ano, String nmaler) {
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_ALI_BROTE");

    qt.putType("NM_ALIBROTE", QueryTool.INTEGER);
    qt.putType("CD_ANO", QueryTool.STRING);
    qt.putType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putType("CD_TALIMENTO", QueryTool.STRING);
    qt.putType("CD_TIMPLICACION", QueryTool.STRING);
    qt.putType("DS_NOMCOMER", QueryTool.STRING);
    qt.putType("DS_FABRICAN", QueryTool.STRING);
    qt.putType("DS_LOTE", QueryTool.STRING);
    qt.putType("CD_MCOMERCALI", QueryTool.STRING);
    qt.putType("CD_TRATPREVALI", QueryTool.STRING);
    qt.putType("CD_FINGERIRALI", QueryTool.STRING);
    qt.putType("CD_MTVIAJEALI", QueryTool.STRING);
    qt.putType("CD_LCONTAMIALI", QueryTool.STRING);
    qt.putType("CD_PAIS_LCONA", QueryTool.STRING);
    qt.putType("CD_LPREPALI", QueryTool.STRING);
    qt.putType("CD_PAIS_LPA", QueryTool.STRING);
    qt.putType("DS_NOMESTAB", QueryTool.STRING);
    qt.putType("DS_DIRESTAB", QueryTool.STRING);
    qt.putType("CD_POSESTAB", QueryTool.STRING);
    qt.putType("CD_PROV_LPREP", QueryTool.STRING);
    qt.putType("CD_MUN_LPREP", QueryTool.STRING);
    qt.putType("DS_TELEF_LPREP", QueryTool.STRING);
    qt.putType("FC_PREPARACION", QueryTool.TIMESTAMP);
    qt.putType("CD_CONSUMOALI", QueryTool.STRING);
    qt.putType("CD_PAIS_LCON", QueryTool.STRING);
    qt.putType("DS_ESTAB_LCON", QueryTool.STRING);
    qt.putType("DS_DIRESTAB_LCON", QueryTool.STRING);
    qt.putType("CD_POSESTAB_LCON", QueryTool.STRING);
    qt.putType("CD_PROV_LCON", QueryTool.STRING);
    qt.putType("CD_MUN_LCON", QueryTool.STRING);
    qt.putType("DS_TELEF_LCON", QueryTool.STRING);
    qt.putType("CD_PAIS_DE", QueryTool.STRING);
    qt.putType("CD_PAIS_A", QueryTool.STRING);
    qt.putType("IT_CALCTASA", QueryTool.STRING);
    qt.putType("NM_EXPENF", QueryTool.INTEGER);
    qt.putType("NM_EXPNOENF", QueryTool.INTEGER);
    qt.putType("NM_NOEXPENF", QueryTool.INTEGER);
    qt.putType("NM_NOEXPNOENF", QueryTool.INTEGER);
    qt.putType("DS_ALI", QueryTool.STRING);

    qt.putWhereType("CD_ANO", QueryTool.STRING);
    qt.putWhereValue("CD_ANO", ano);
    qt.putOperator("CD_ANO", "=");

    qt.putWhereType("NM_ALERBRO", QueryTool.INTEGER);
    qt.putWhereValue("NM_ALERBRO", nmaler);
    qt.putOperator("NM_ALERBRO", "=");

    return qt;
  } //end

  void btnAceptar_actionPerformed() {
    final String srvExport = "servlet/SrvExportBrotesPeriodo";
    final String srvTrans = "servlet/SrvTransaccion";
    Lista lResultado = null;
    Lista lBd = new Lista();
    Lista lBroteDev = new Lista();
    String anoDev = new String();
    String codDev = new String();
    String grupoDev = new String();

    if (isDataValid()) {
      //1�-> Obtengo las claves de los brotes q cumplen las caracter�sticas
      //introducidas: cd_ano, nm_alerbro,cd_grupo
      Data dt = new Data();
      String fechaDesde = null;
      if (txtFNotifDesde.getText().trim().length() != 0) {
        fechaDesde = txtFNotifDesde.getText().trim() + " 00:00:00";
      }
      else {
        fechaDesde = "";
      }
      String fechaHasta = null;
      if (txtFNotifHasta.getText().trim().length() != 0) {
        fechaHasta = txtFNotifHasta.getText().trim() + " 00:00:00";
      }
      else {
        fechaHasta = "";
      }

      dt.put("FC_FECHAHORADESDE", fechaDesde);
      dt.put("FC_FECHAHORAHASTA", fechaHasta);
      dt.put("CD_GRUPO", ( (Data) pnlGrupo.getDatos()).getString("CD_GRUPO"));

      Lista lBrote = new Lista();
      lBrote.addElement(dt);
      Inicializar(CInicializar.ESPERA);
      try {

        //lBroteDev contiene una lista con las claves de los brotes
        //cuyos datos hay q obtener.
        this.getApp().getStub().setUrl(srvExport);
        lBroteDev = (Lista)this.getApp().getStub().doPost(1, lBrote);

        if (lBroteDev.size() != 0) {
          //hago tantas QT de cada tipo, como brotes existan
          for (int ilis = 0; ilis < lBroteDev.size(); ilis++) {
            Data dtBroteDev = (Data) lBroteDev.elementAt(ilis);
            anoDev = dtBroteDev.getString("CD_ANO");
            codDev = dtBroteDev.getString("NM_ALERBRO");
            grupoDev = dtBroteDev.getString("CD_GRUPO");

            QueryTool qtBrote = crearQTBrote(anoDev, codDev);
            Data dtBrote = new Data();
            dtBrote.put("1", qtBrote);
            lBd.addElement(dtBrote);
          }

          for (int ilis = 0; ilis < lBroteDev.size(); ilis++) {
            Data dtBroteDev = (Data) lBroteDev.elementAt(ilis);
            anoDev = dtBroteDev.getString("CD_ANO");
            codDev = dtBroteDev.getString("NM_ALERBRO");
            grupoDev = dtBroteDev.getString("CD_GRUPO");

            QueryTool qtAgcausal = crearQTAgcausal(anoDev, codDev);
            Data dtAgcausal = new Data();
            dtAgcausal.put("1", qtAgcausal);
            lBd.addElement(dtAgcausal);
          }

          QueryTool qtFaccontr = crearQTFaccontr( ( (Data) pnlGrupo.getDatos()).
                                                 getString("CD_GRUPO"));
          Data dtFaccontr = new Data();
          dtFaccontr.put("1", qtFaccontr);
          lBd.addElement(dtFaccontr);

          QueryTool qtMedadop = crearQTMedadopt( ( (Data) pnlGrupo.getDatos()).
                                                getString("CD_GRUPO"));
          Data dtMedadop = new Data();
          dtMedadop.put("1", qtMedadop);
          lBd.addElement(dtMedadop);

          for (int ilis = 0; ilis < lBroteDev.size(); ilis++) {
            Data dtBroteDev = (Data) lBroteDev.elementAt(ilis);
            anoDev = dtBroteDev.getString("CD_ANO");
            codDev = dtBroteDev.getString("NM_ALERBRO");
            grupoDev = dtBroteDev.getString("CD_GRUPO");

            QueryTool qtSintomas = crearQTSintomas(anoDev, codDev);
            Data dtSintomas = new Data();
            dtSintomas.put("1", qtSintomas);
            lBd.addElement(dtSintomas);
          }

          for (int ilis = 0; ilis < lBroteDev.size(); ilis++) {
            Data dtBroteDev = (Data) lBroteDev.elementAt(ilis);
            anoDev = dtBroteDev.getString("CD_ANO");
            codDev = dtBroteDev.getString("NM_ALERBRO");
            grupoDev = dtBroteDev.getString("CD_GRUPO");

            QueryTool qtDistedad = crearQTDistedad(anoDev, codDev);
            Data dtDistedad = new Data();
            dtDistedad.put("1", qtDistedad);
            lBd.addElement(dtDistedad);
          }

          for (int ilis = 0; ilis < lBroteDev.size(); ilis++) {
            Data dtBroteDev = (Data) lBroteDev.elementAt(ilis);
            anoDev = dtBroteDev.getString("CD_ANO");
            codDev = dtBroteDev.getString("NM_ALERBRO");
            grupoDev = dtBroteDev.getString("CD_GRUPO");

            QueryTool qtTataqali = crearQTTataqali(anoDev, codDev);
            Data dtTataqali = new Data();
            dtTataqali.put("1", qtTataqali);
            lBd.addElement(dtTataqali);
          }

          for (int ilis = 0; ilis < lBroteDev.size(); ilis++) {
            Data dtBroteDev = (Data) lBroteDev.elementAt(ilis);
            anoDev = dtBroteDev.getString("CD_ANO");
            codDev = dtBroteDev.getString("NM_ALERBRO");
            grupoDev = dtBroteDev.getString("CD_GRUPO");

            QueryTool qtMuestras = crearQTMuestras(anoDev, codDev);
            Data dtMuestra = new Data();
            dtMuestra.put("1", qtMuestras);
            lBd.addElement(dtMuestra);
          }

          for (int ilis = 0; ilis < lBroteDev.size(); ilis++) {
            Data dtBroteDev = (Data) lBroteDev.elementAt(ilis);
            anoDev = dtBroteDev.getString("CD_ANO");
            codDev = dtBroteDev.getString("NM_ALERBRO");
            grupoDev = dtBroteDev.getString("CD_GRUPO");

            QueryTool qtALimimpl = crearQTAlimimpl(anoDev, codDev);
            Data dtALimimpl = new Data();
            dtALimimpl.put("1", qtALimimpl);
            lBd.addElement(dtALimimpl);
          }

          try {
            this.getApp().getStub().setUrl(srvTrans);
            lResultado = (Lista)this.getApp().getStub().doPost(0, lBd);

            //si todo ha ido bien se crean los ficheros
            Lista lResultBrotes = new Lista();
            Lista lResultAg = new Lista();
            Lista lResultFac = new Lista();
            Lista lResultMed = new Lista();
            Lista lResultSin = new Lista();
            Lista lResultDis = new Lista();
            Lista lResultTat = new Lista();
            Lista lResultMue = new Lista();
            Lista lResultAli = new Lista();

            int contResult = 0;
            //Para obtener todos los datas para el fichero de brotes
            //tengo q obtener de la lista resultado total, aquellos
            //que pertenecen a Brotes->seg�n el n� de brotes obtenidos
            for (int cont = 0; cont < lBroteDev.size(); cont++) {
              Lista lResult = (Lista) lResultado.elementAt(contResult);

              //Meto los datas en otra lista q agrupa los datas de Brotes
              for (int s = 0; s < lResult.size(); s++) {
                lResultBrotes.addElement( (Data) lResult.elementAt(s));
              }
              contResult++;
            } //end for
            //creo el fichero pasando una lista con los resultados de
            //consulta de siv_brotes
            crearFicheroBrote(lResultBrotes);

            //obtengo una lista con rdos de AgCausales
            for (int cont = 0; cont < lBroteDev.size(); cont++) {
              Lista lResult = (Lista) lResultado.elementAt(contResult);

              //Meto los datas en otra lista q agrupa los datas de Brotes
              for (int s = 0; s < lResult.size(); s++) {
                lResultAg.addElement( (Data) lResult.elementAt(s));
              }
              contResult++;
            } //end for
            crearFicheroAgcausal(lResultAg);

            //obtengo una lista con rdos de Factores contribuyentes
            Lista lResult0 = (Lista) lResultado.elementAt(contResult);

            //Meto los datas en otra lista q agrupa los datas de Brotes
            for (int s = 0; s < lResult0.size(); s++) {
              lResultFac.addElement( (Data) lResult0.elementAt(s));
            }
            contResult++;
            crearFicheroFaccontr(lResultFac);

            //obtengo una lista con rdos de Medidas
            Lista lResult1 = (Lista) lResultado.elementAt(contResult);

            //Meto los datas en otra lista q agrupa los datas de Brotes
            for (int s = 0; s < lResult1.size(); s++) {
              lResultMed.addElement( (Data) lResult1.elementAt(s));
            }
            contResult++;
            crearFicheroMedadopt(lResultMed);

            //obtengo una lista con rdos de Sintomas
            for (int cont = 0; cont < lBroteDev.size(); cont++) {
              Lista lResult = (Lista) lResultado.elementAt(contResult);

              //Meto los datas en otra lista q agrupa los datas de Brotes
              for (int s = 0; s < lResult.size(); s++) {
                lResultSin.addElement( (Data) lResult.elementAt(s));
              }
              contResult++;
            } //end for
            crearFicheroSintomas(lResultSin);

            //obtengo una lista con rdos de Distribuci�n de edades
            for (int cont = 0; cont < lBroteDev.size(); cont++) {
              Lista lResult = (Lista) lResultado.elementAt(contResult);

              //Meto los datas en otra lista q agrupa los datas de Brotes
              for (int s = 0; s < lResult.size(); s++) {
                lResultDis.addElement( (Data) lResult.elementAt(s));
              }
              contResult++;
            } //end for
            crearFicheroDistedad(lResultDis);

            //obtengo una lista con rdos de Tasas de ataq
            for (int cont = 0; cont < lBroteDev.size(); cont++) {
              Lista lResult = (Lista) lResultado.elementAt(contResult);

              //Meto los datas en otra lista q agrupa los datas de Brotes
              for (int s = 0; s < lResult.size(); s++) {
                lResultTat.addElement( (Data) lResult.elementAt(s));
              }
              contResult++;
            } //end for
            crearFicheroTataqAli(lResultTat);

            //obtengo una lista con rdos de Muestras
            for (int cont = 0; cont < lBroteDev.size(); cont++) {
              Lista lResult = (Lista) lResultado.elementAt(contResult);

              //Meto los datas en otra lista q agrupa los datas de Brotes
              for (int s = 0; s < lResult.size(); s++) {
                lResultMue.addElement( (Data) lResult.elementAt(s));
              }
              contResult++;
            } //end for
            crearFicheroMuestras(lResultMue);

            //obtengo una lista con rdos de Alimentos implicados
            for (int cont = 0; cont < lBroteDev.size(); cont++) {
              Lista lResult = (Lista) lResultado.elementAt(contResult);

              //Meto los datas en otra lista q agrupa los datas de Brotes
              for (int s = 0; s < lResult.size(); s++) {
                lResultAli.addElement( (Data) lResult.elementAt(s));
              }
              contResult++;
            } //end for
            crearFicheroAlimimpl(lResultAli);

            Lista lMensaje = new Lista();
//            String msgAvis="Ficheros creados: ";
            if (! (Brot.equals(""))) {
              lMensaje.addElement(Brot);
            }
            if (! (Agcausal.equals(""))) {
              lMensaje.addElement(Agcausal);
            }
            if (! (Faccontr.equals(""))) {
              lMensaje.addElement(Faccontr);
            }
            if (! (Medadopt.equals(""))) {
              lMensaje.addElement(Medadopt);
            }
            if (! (Sintomas.equals(""))) {
              lMensaje.addElement(Sintomas);
            }
            if (! (Distedad.equals(""))) {
              lMensaje.addElement(Distedad);
            }
            if (! (Tataqali.equals(""))) {
              lMensaje.addElement(Tataqali);
            }
            if (! (Muestras.equals(""))) {
              lMensaje.addElement(Muestras);
            }
            if (! (Alimimpl.equals(""))) {
              lMensaje.addElement(Alimimpl);

            }
            DiaMsgAviso dlg = new DiaMsgAviso(this.getApp(), 0, lMensaje);
            dlg.show();

                /*            CMessage msgBox = new CMessage(apl,CMessage.msgAVISO,msgAvis);
                        msgBox.show();
                        msgBox = null;*/
            vaciarPantalla();
            Inicializar(CInicializar.NORMAL);

          }
          catch (Exception exc) { //catch de la SrvTransaccion
            this.getApp().trazaLog(exc);
            this.getApp().showError(exc.getMessage());
          }
        }
        else {
          this.getApp().showAdvise(
              "No existe ning�n brote con los criterios especificados");
          vaciarPantalla();
          Inicializar(CInicializar.NORMAL);

        }
      }
      catch (Exception exc) { //catch de la SrvExport..
        this.getApp().trazaLog(exc);
        this.getApp().showError(exc.getMessage());
      }
    } //end if isDataValid
  } //end btn_Aceptar

  void vaciarPantalla() {
    txtFNotifDesde.setText("");
    txtFNotifHasta.setText("");
    pnlGrupo.limpiarDatos();
    txtCaracter.setText("$");
    panFichero.txtFile.setText("c:\\");
  }

  void crearFicheroBrote(Lista lResult) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "GBrote.txt";
    EditorFichero edFic = new EditorFichero();
    //Lista lResult=(Lista)lResultado.elementAt(0);
    if (lResult.size() == 0) {
      Brot = "";
    }
    else {
      Brot = "GBrote.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposBro, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroAgcausal(Lista lResult) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "GAgcausal.txt";
    EditorFichero edFic = new EditorFichero();
    //Lista lResult=(Lista)lResultado.elementAt(2);
    if (lResult.size() == 0) {
      Agcausal = "";
    }
    else {
      Agcausal = "GAgcausal.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposAgcausal, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroFaccontr(Lista lResult) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "GFaccontr.txt";
    EditorFichero edFic = new EditorFichero();
    //Lista lResult=(Lista)lResultado.elementAt(3);
    if (lResult.size() == 0) {
      Faccontr = "";
    }
    else {
      Faccontr = "GFaccontr.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposFaccontr, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroMedadopt(Lista lResult) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "GMedadopt.txt";
    EditorFichero edFic = new EditorFichero();
    //Lista lResult=(Lista)lResultado.elementAt(4);
    if (lResult.size() == 0) {
      Medadopt = "";
    }
    else {
      Medadopt = "GMedadopt.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposMedadopt, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroSintomas(Lista lResult) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "GSintomas.txt";
    EditorFichero edFic = new EditorFichero();
    //Lista lResult=(Lista)lResultado.elementAt(5);
    if (lResult.size() == 0) {
      Sintomas = "";
    }
    else {
      Sintomas = "GSintomas.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposSintomas, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroDistedad(Lista lResult) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "GDistedad.txt";
    EditorFichero edFic = new EditorFichero();
    //Lista lResult=(Lista)lResultado.elementAt(6);
    if (lResult.size() == 0) {
      Distedad = "";
    }
    else {
      Distedad = "GDistedad.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposDistedad, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroTataqAli(Lista lResult) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "GTataqali.txt";
    EditorFichero edFic = new EditorFichero();
    //Lista lResult=(Lista)lResultado.elementAt(7);
    if (lResult.size() == 0) {
      Tataqali = "";
    }
    else {
      Tataqali = "GTataqali.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposTataqali, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroMuestras(Lista lResult) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "GMuestras.txt";
    EditorFichero edFic = new EditorFichero();
    //Lista lResult=(Lista)lResultado.elementAt(8);
    if (lResult.size() == 0) {
      Muestras = "";
    }
    else {
      Muestras = "GMuestras.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposMuestras, apl,
                                        lResult, localiz, carSep);
  }

  void crearFicheroAlimimpl(Lista lResult) {
    String localiz = this.panFichero.txtFile.getText();
    String carSep = txtCaracter.getText().trim();
    String nomFich = "GAlimimpl.txt";
    EditorFichero edFic = new EditorFichero();
    //Lista lResult=(Lista)lResultado.elementAt(9);
    if (lResult.size() == 0) {
      Alimimpl = "";
    }
    else {
      Alimimpl = "GAlimimpl.txt";
    }
    localiz = localiz + nomFich;
    edFic.escribirFicheroCarSeparSinMsg(vCamposAlimimpl, apl,
                                        lResult, localiz, carSep);
  }

  //comprobar q se encuentran rellenos los campos
  boolean isDataValid() {
    boolean b = false;
    if (panFichero.txtFile.getText().length() == 0) {
      this.getApp().showAdvise(
          "Se debe indicar el directorio elegido para el nuevo fichero");
      b = false;
    }
    else {
      if ( (txtFNotifDesde.getText().length() == 0) ||
          (txtFNotifHasta.getText().length() == 0) ||
          (pnlGrupo.getDatos() == null) ||
          (txtCaracter.getText().length() == 0)) {
        this.getApp().showAdvise("Debe completar todos los campos");
        b = false;
      }
      else {
        b = true;
      }
    }
    return b;
  }

  /****INTERFAZ CPNLCODIGOEXT****/
  public void trasEventoEnCPnlCodigoExt() {}

  public String getDescCompleta(Data dat) {
    return ("");
  }
  /*********************************/
} //end class

// Botones
class PanExportBrotesPeriodoActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanExportBrotesPeriodo adaptee;
  ActionEvent evt;

  PanExportBrotesPeriodoActionAdapter(PanExportBrotesPeriodo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    if (evt.getActionCommand().equals("Aceptar")) {
      adaptee.btnAceptar_actionPerformed();
    }
  }
} // endclass  PanExportBrotesPeriodoActionAdapter
