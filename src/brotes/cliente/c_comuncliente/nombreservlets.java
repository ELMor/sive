// Fichero de constantes

package brotes.cliente.c_comuncliente;

public class nombreservlets {

  // Especificos de brotes
  public static final String strSERVLET_ALERTA = "servlet/SrvAlerta";
  public static final String strSERVLET_BIF = "servlet/SrvBIF";
  /*  public static final String strSERVLET_CONV_FEC_CENT = "servlet/SrvConvFecCent";
    public static final String strSERVLET_MANT_MED_NOTIF = "servlet/SrvMantMedNotif";
    public static final String strSERVLET_MANT_P_CENTINELA = "servlet/SrvMantPCentinela";
    public static final String strSERVLET_CENT_APP = "servlet/SrvCentApp";
    public static final String strSERVLET_CENT_AUT = "servlet/SrvCentAut";
    public static final String strSERVLET_ACTUALIZAR_PROT = "servlet/SrvActualizarProt";
    public static final String strSERVLET_PROTOCOLO_CENT = "servlet/SrvProtocoloCent";
    public static final String strSERVLET_RESP_CENTI = "servlet/SrvRespCenti";
    public static final String strSERVLET_COBERTURA = "servlet/SrvCobertura";
    public static final String strSERVLET_ENVIO_CASOS_RMC_CNE = "servlet/SrvEnvioCasosRmcCne";
    public static final String strSERVLET_EXPORTACIONES = "servlet/SrvExportaciones";
    public static final String strSERVLET_GEN_ALA_AUTO_CENT = "servlet/SrvGenAlaAutoCent";
    public static final String strSERVLET_VOL_CASOS_IND_CENT = "servlet/SrvVolCasosIndCent";
    public static final String strSERVLET_MOD_PREG_CENT = "servlet/SrvModPregCent";*/

  // Comunes a otras aplicaciones
  //NOTA: Adem�s de los que aparecen en fichero comun.constantes
  public static final String strSERVLET_QUERY_TOOL = "servlet/SrvQueryTool";
  public static final String strSERVLET_TRANSACCION = "servlet/SrvTransaccion";
  //public static final String strSERVLET_PASSWORD_2 = "servlet/SrvPassword2";

}
