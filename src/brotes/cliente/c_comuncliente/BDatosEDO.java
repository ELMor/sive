package brotes.cliente.c_comuncliente;

//Clases nuevas: solo el applet
import java.net.URL;

/*Clases de EDO*/
import capp.CLista;
import sapp.StubSrvBD;

public class BDatosEDO {

  public BDatosEDO() {
  }

  public static boolean todoREAL = true;
  public static boolean todoDEBUG = false;

  /*
    public static CLista traer(capp2.CApp app,String strURL, int modoServlet, CLista parametros) {
      CLista result = null;
      try {
        // consulta el servidor
        app.getStub().setUrl(strURL);
        result = (Lista) app.getStub().doPost(modoServlet, parametros);
      // error en el servlet
      }
      catch (Exception ex) {
         //Muestra mensaje que viene del Servidor
         //(Nota: En SrvQueryTool pone Error inesperado)
         app.trazaLog(ex);
         // Muestra di�logo con mensaje
         app.showError(ex.getMessage());
      }
      return result;
    }
   */

  /* TraerDebug:
   *Trae datos de la base de datos
   */

  /*
    public static CLista traerDebug(capp2.CApp app ,sapp.DBServlet servlet, int modoServlet, CLista parametros) {
      CLista result = null;
      try {
         //Indica como conectarse a la b.datos
         servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                             "sive_desa",
                             "sive_desa");
          //Se conecta a b.datos y consulta
          result = servlet.doDebug(modoServlet, parametros);
      // error en el servlet
      }
      catch (Exception ex) {
         //Muestra mensaje que viene del Servidor
         app.trazaLog(ex);
         // Muestra di�logo con mensaje
         app.showError(ex.getMessage());
      }
      return result;
    }
   */

  //__________________________________________________________________________________
  /* execSQL:
   * Ejecuta una sentencia SQL conect�ndose al servidor Web
   */
  public static CLista execSQL(capp2.CApp app, String strURL, int modoServlet,
                               CLista parametros) throws Exception {

    CLista result = null;

    try {

      StubSrvBD stubCliente = new StubSrvBD();
      // apunta a la URL del servlet
      stubCliente.setUrl(new URL(app.getParametro("URL_SERVLET") + strURL));
      // obtiene la lista
      result = (CLista) stubCliente.doPost(modoServlet, parametros);

      // error en el servlet
    }
    catch (Exception ex) {
      //Muestra mensaje que viene del Servidor
      //(Nota: En SrvQueryTool pone Error inesperado)
      app.trazaLog(ex);
      // Muestra di�logo con mensaje
      app.showError(ex.getMessage());
      // Relanzamiento de la excepcion
      throw (ex);
    }
    return result;
  }

  //__________________________________________________________________________________

  /* execSQLDebug:
   * Ejecuta una sentencia SQL
   * Diferencia con el otro m�todo: aqu� se pasa el Servlet (el objeto) en vez de la URL donde se localiza
   */
  public static CLista execSQLDebug(capp2.CApp app, sapp.DBServlet servlet,
                                    int modoServlet, CLista parametros) throws
      Exception {

    CLista result = null;

    try {

      //Indica como conectarse a la b.datos
      servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
      //Se conecta a b.datos y consulta
      result = servlet.doDebug(modoServlet, parametros);

      // error en el servlet
    }
    catch (Exception ex) {
      //Muestra mensaje que viene del Servidor
      //(Nota: En SrvQueryTool pone Error inesperado)
      app.trazaLog(ex);
      // Muestra di�logo con mensaje
      app.showError(ex.getMessage());
      // Relanzamiento de la excepcion
      throw (ex);
    }
    return result;
  }

  //__________________________________________________________________________________
  /* execSQL:
   * Ejecuta una sentencia SQL conect�ndose al servidor Web o desde cliente, segun parametro debug
   */

  public static CLista ejecutaSQL(boolean debug, capp2.CApp app, String strURL,
                                  int modoServlet, CLista parametros) throws
      Exception {
    //Si se debe conectar siempre al Servidor Web (Por ejemplo :Se ense�a a la ICM )
    if (todoREAL == true) {
      return execSQL(app, strURL, modoServlet, parametros);
    }
    //Si se debe poner en debug (ej. Serv Web caido)
    else if (todoDEBUG == true) {
      return execSQLDebug(app, ListaDeServletsEDO.getServlet(strURL),
                          modoServlet, parametros);
    }

    //En resto de casos depende de par�metro debug de esta llamada
    else {
      if (debug == false) {
        return execSQL(app, strURL, modoServlet, parametros);
      }
      else {
        return execSQLDebug(app, ListaDeServletsEDO.getServlet(strURL),
                            modoServlet, parametros);
      }
    } //else

  } //Funcion

}
