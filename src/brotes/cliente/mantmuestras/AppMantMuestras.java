package brotes.cliente.mantmuestras;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppMantMuestras
    extends CApp
    implements CInicializar {

  PanMantMuestras pan = null;

  public AppMantMuestras() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Mantenimiento de las muestras del brote");
    pan = new PanMantMuestras(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }
}