// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   DataRespTuberCon.java

package brotes.datos.dbpanprotocolo;

import java.io.Serializable;
import java.sql.Timestamp;

import comun.Fechas;

public class DataRespTuberCon
    implements Serializable {

  private String Ano_RTBC;
  private String Reg_RTBC;
  private int Contacto;
  private String CdTsive;
  private String CdModelo;
  private String NmLinea;
  private String CdPregunta;
  private String DsRespuesta;
  private String ValorLista;
  private String sCodEq;
  private String sCdOpe;
  private String sFcUltAct;

  public DataRespTuberCon() {
    Ano_RTBC = null;
    Reg_RTBC = null;
    Contacto = -1;
    CdTsive = null;
    CdModelo = null;
    NmLinea = null;
    CdPregunta = null;
    DsRespuesta = null;
    ValorLista = null;
    sCodEq = null;
    sCdOpe = null;
    sFcUltAct = null;
  }

  public DataRespTuberCon(String cdTSive, String ano_RTBC, String reg_RTBC,
                          int contacto, String modelo, int linea,
                          String pregunta,
                          String respuesta, String valorlista, String equipo) {
    Ano_RTBC = null;
    Reg_RTBC = null;
    Contacto = -1;
    CdTsive = null;
    CdModelo = null;
    NmLinea = null;
    CdPregunta = null;
    DsRespuesta = null;
    ValorLista = null;
    sCodEq = null;
    sCdOpe = null;
    sFcUltAct = null;
    setCD_TSIVE(cdTSive);
    setANO_RTBC(ano_RTBC);
    setREG_RTBC(reg_RTBC);
    setCONTACTO(contacto);
    setCD_MODELO(modelo);
    setNM_LINEA(linea);
    setCD_PREGUNTA(pregunta);
    setDS_RESPUESTA(respuesta);
    ValorLista = valorlista;
    setCD_E_NOTIF(equipo);
  }

  public String getANO_RTBC() {
    String s = "";
    if (Ano_RTBC != null) {
      s = Ano_RTBC;
    }
    return s;
  }

  public String getREG_RTBC() {
    String s = "";
    if (Reg_RTBC != null) {
      s = Reg_RTBC;
    }
    return s;
  }

  public int getCONTACTO() {
    return Contacto;
  }

  public String getCD_TSIVE() {
    String s = "";
    if (CdTsive != null) {
      s = CdTsive;
    }
    return s;
  }

  public String getCD_MODELO() {
    String s = "";
    if (CdModelo != null) {
      s = CdModelo;
    }
    return s;
  }

  public int getNM_LINEA() {
    return (new Integer(NmLinea)).intValue();
  }

  public String getCD_PREGUNTA() {
    String s = "";
    if (CdPregunta != null) {
      s = CdPregunta;
    }
    return s;
  }

  public String getDS_RESPUESTA() {
    String s = "";
    if (DsRespuesta != null) {
      s = DsRespuesta;
    }
    return s;
  }

  public String getCodEq() {
    String s = "";
    if (sCodEq != null) {
      s = sCodEq;
    }
    return s;
  }

  public Timestamp getFC_ULTACT_EDOIND() {
    Timestamp ts = new Timestamp(0L);
    if (sFcUltAct != null && !sFcUltAct.equals("")) {
      ts = Fechas.string2Timestamp(sFcUltAct);
    }
    return ts;
  }

  public String getCD_OPE_EDOIND() {
    String s = "";
    if (sCdOpe != null) {
      s = sCdOpe;
    }
    return s;
  }

  public void setANO_RTBC(String s) {
    Ano_RTBC = "";
    if (s != null) {
      Ano_RTBC = s.trim();
    }
  }

  public void setREG_RTBC(String s) {
    Reg_RTBC = "";
    if (s != null) {
      Reg_RTBC = s.trim();
    }
  }

  public void setCONTACTO(int contacto) {
    Contacto = contacto;
  }

  public void setCD_TSIVE(String s) {
    CdTsive = "";
    if (s != null) {
      CdTsive = s.trim();
    }
  }

  public void setCD_MODELO(String s) {
    CdModelo = "";
    if (s != null) {
      CdModelo = s.trim();
    }
  }

  public void setNM_LINEA(int i) {
    NmLinea = String.valueOf(i);
  }

  public void setCD_PREGUNTA(String s) {
    CdPregunta = "";
    if (s != null) {
      CdPregunta = s.trim();
    }
  }

  public void setDS_RESPUESTA(String s) {
    DsRespuesta = "";
    if (s != null) {
      DsRespuesta = s.trim();
    }
  }

  public void setCD_E_NOTIF(String s) {
    sCodEq = "";
    if (s != null) {
      sCodEq = s.trim();
    }
  }

  public void setFC_ULTACT_EDOIND(Timestamp ts) {
    sFcUltAct = "";
    if (ts != null) {
      sFcUltAct = Fechas.timestamp2String(ts);
    }
  }

  public void setCD_OPE_EDOIND(String s) {
    sCdOpe = "";
    if (s != null) {
      sCdOpe = s.trim();
    }
  }
}
