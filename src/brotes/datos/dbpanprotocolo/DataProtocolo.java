package brotes.datos.dbpanprotocolo;

import java.io.Serializable;

public class DataProtocolo
    implements Serializable {

  public String sCodigo = "", sDescripcion = "";
  public String sNivel1 = "", sNivel2 = "", sCa = "";
  public String NumCaso = "";

  private String sCd_E_Notif = "";

  public DataProtocolo() {
  }

  public DataProtocolo(String codigo, String descripcion,
                       String nivel1, String nivel2, String ca,
                       String numcaso) {
    sCodigo = codigo;
    sDescripcion = descripcion;
    sNivel1 = nivel1;
    sNivel2 = nivel2;
    sCa = ca;
    NumCaso = numcaso;
  }

  public DataProtocolo(String codigo, String descripcion,
                       String nivel1, String nivel2, String ca,
                       String numcaso, String notificador) {
    sCodigo = codigo;
    sDescripcion = descripcion;
    sNivel1 = nivel1;
    sNivel2 = nivel2;
    sCa = ca;
    NumCaso = numcaso;
    setNotificador(notificador);
  }

  public String getCod() {
    return sCodigo;
  }

  public String getDes() {
    return sDescripcion;
  }

  public String getNivel1() {
    return sNivel1;
  }

  public String getNivel2() {
    return sNivel2;
  }

  public String getCa() {
    return sCa;
  }

  public String getCaso() {
    return NumCaso;
  }

  public String getNotificador() {
    return sCd_E_Notif;
  }

  public void setNotificador(String s) {
    sCd_E_Notif = s;
  }

}
