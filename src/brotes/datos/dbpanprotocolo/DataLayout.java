package brotes.datos.dbpanprotocolo;

import java.io.Serializable;
import java.util.Vector;

//soporta los datos del vector para pintar el panel con GroupBoxes:

public class DataLayout
    implements Serializable {

  protected String sCD_TPREG = "";
  protected String sDS_TEXTO = "";
  protected String sOBLIG = "";
  protected String sLONG = "";
  protected String sENT = "";
  protected String sDEC = "";

  protected String sCD_MODELO = "";
  protected String sNM_LIN = "";
  protected String sCD_PREGUNTA = "";

  protected String sIT_COND = "";
  protected String sNM_LIN_COND = ""; //EN EL MISMO MODELO
  protected String sCD_PREGUNTA_COND = "";
  protected String sDS_PREGUNTA_COND = "";

  protected String sCD_LISTA = "";
  protected Vector sVALORES = null;

  protected String sCD_CA = "";
  protected String sCD_NIVEL_1 = "";
  protected String sCD_NIVEL_2 = "";

  public DataLayout(String tipoPreg, String desTexto, String Obligatorio,
                    String Long, String Entero, String Decimal,
                    String codModelo, String nmLinea, String CodPreg,
                    String Condicionado, String nmLineaCond, String CodPregCond,
                    String DesPregCond,
                    String CodLista, Vector valores,
                    String ca, String n1, String n2) {

    sCD_TPREG = tipoPreg;
    sDS_TEXTO = desTexto;
    sOBLIG = Obligatorio;
    sLONG = Long;
    sENT = Entero;
    sDEC = Decimal;

    sCD_MODELO = codModelo;
    sNM_LIN = nmLinea;
    sCD_PREGUNTA = CodPreg;

    sIT_COND = Condicionado;
    sNM_LIN_COND = nmLineaCond;
    sCD_PREGUNTA_COND = CodPregCond;
    sDS_PREGUNTA_COND = DesPregCond;

    sCD_LISTA = CodLista;
    sVALORES = valores;

    sCD_CA = ca;
    sCD_NIVEL_1 = n1;
    sCD_NIVEL_2 = n2;
  }

  public String getCA() {
    return sCD_CA;
  }

  public String getNIVEL_1() {
    return sCD_NIVEL_1;
  }

  public String getNIVEL_2() {
    return sCD_NIVEL_2;
  }

  public String getTipoPreg() {
    return sCD_TPREG;
  }

  public String getDesTexto() {
    return sDS_TEXTO;
  }

  public String getOblig() {
    return sOBLIG;
  }

  public String getLongitud() {
    return sLONG;
  }

  public String getEntera() {
    return sENT;
  }

  public String getDecimal() {
    return sDEC;
  }

  public String getCodModelo() {
    return sCD_MODELO;
  }

  public String getNumLinea() {
    return sNM_LIN;
  }

  public String getCodPregunta() {
    return sCD_PREGUNTA;
  }

  public String getCondicionada() {
    return sIT_COND;
  }

  public String getNumLineaCond() {
    return sNM_LIN_COND;
  }

  public String getCodPreguntaCond() {
    return sCD_PREGUNTA_COND;
  }

  public String getDesPreguntaCond() {
    return sDS_PREGUNTA_COND;
  }

  public String getCodLista() {
    return sCD_LISTA;
  }

  public Vector getVALORES() {
    return sVALORES;
  }

}
