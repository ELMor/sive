package brotes.datos.dbpanprotocolo;

import java.io.Serializable;

//soporta los datos del vector
//resultado de la query sobre sive_Modelo, sive_lineasM etc

public class DataLineasM
    implements Serializable {

  protected String sCD_MODELO = "";
  protected String sNM_LIN = "";
  protected String sCD_TLINEA = "";
  protected String sDS_TEXTO = "";
  protected String sCA = "";
  protected String sN1 = "";
  protected String sN2 = "";

  public DataLineasM(String codModelo, String nmLinea,
                     String TipoLinea, String Texto,
                     String Comunidad, String Nivel1,
                     String Nivel2) {

    sCD_MODELO = codModelo;
    sNM_LIN = nmLinea;
    sCD_TLINEA = TipoLinea;
    sDS_TEXTO = Texto;
    sCA = Comunidad;
    sN1 = Nivel1;
    sN2 = Nivel2;
  }

  //para guardar los modelos diferentes
  public DataLineasM(String codModelo) {

    sCD_MODELO = codModelo;
    sNM_LIN = null;
    sCD_TLINEA = null;
    sDS_TEXTO = null;

  }

  public String getCodModelo() {
    return sCD_MODELO;
  }

  public String getNmLinea() {
    return sNM_LIN;
  }

  public String getTipoLinea() {
    return sCD_TLINEA;
  }

  public String getDesTexto() {
    return sDS_TEXTO;
  }

  public String getComunidad() {
    return sCA;
  }

  public String getNivel1() {
    return sN1;
  }

  public String getNivel2() {
    return sN2;
  }
}
