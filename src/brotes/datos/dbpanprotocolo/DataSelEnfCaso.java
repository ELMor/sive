package brotes.datos.dbpanprotocolo;

import java.io.Serializable;

//soporta los datos para hacer la query base en SrvSelProt
public class DataSelEnfCaso
    implements Serializable {

  protected String sCodEnf = "";
  protected String sCA = "";
  protected String sN1 = "";
  protected String sN2 = "";
  protected String sNM_EDO = "";
  //protected String sCD_E_NOTIF = "";
  protected String sCD_ANOEPI = "";
  protected String sCD_SEMEPI = "";

  protected String sCD_ARTBC = "";
  protected String sCD_NRTBC = "";
  protected int iContacto = -1;

  protected java.util.Hashtable hNotificador = null;

  public DataSelEnfCaso(String CodEnf,
                        String sComunidad,
                        String Nivel1,
                        String Nivel2,
                        String Expediente,
                        java.util.Hashtable hNot,
                        String CodAno,
                        String CodSem) {

    sCodEnf = CodEnf;
    sCA = sComunidad;
    sN1 = Nivel1;
    sN2 = Nivel2;
    sNM_EDO = Expediente;
    hNotificador = hNot;
    sCD_ANOEPI = CodAno;
    sCD_SEMEPI = CodSem;

  }

  public DataSelEnfCaso(String CodEnf,
                        String sComunidad,
                        String Nivel1,
                        String Nivel2,
                        String Expediente,
                        String Ano_RTBC,
                        String Reg_RTBC,
                        int Contacto,
                        java.util.Hashtable hNot,
                        String CodAno,
                        String CodSem) {

    sCodEnf = CodEnf;
    sCA = sComunidad;
    sN1 = Nivel1;
    sN2 = Nivel2;
    sNM_EDO = Expediente;
    sCD_ARTBC = Ano_RTBC;
    sCD_NRTBC = Reg_RTBC;
    iContacto = Contacto;
    hNotificador = hNot;
    sCD_ANOEPI = CodAno;
    sCD_SEMEPI = CodSem;
  }

  public String getCodEnfermedad() {
    return sCodEnf;
  }

  public String getComunidad() {
    return sCA;
  }

  public String getNivelUNO() {
    return sN1;
  }

  public String getNivelDOS() {
    return sN2;
  }

  public String getNM_EDO() {
    return sNM_EDO;
  }

  public String getCD_ARTBC() {
    return sCD_ARTBC;
  }

  public String getCD_NRTBC() {
    return sCD_NRTBC;
  }

  public int getContacto() {
    return iContacto;
  }

  public java.util.Hashtable getNOTIFICADOR() {
    return hNotificador;
  }

  public String getCD_ANOEPI() {
    return sCD_ANOEPI;
  }

  public String getCD_SEMEPI() {
    return sCD_SEMEPI;
  }
}
