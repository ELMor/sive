package brotes.datos.comun;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Secuenciador {

  public Secuenciador() {
  }

  public final static String getSecuenciador(Connection con,
                                             PreparedStatement st,
                                             ResultSet rs, String sNombreColum,
                                             int longcod) throws SQLException {
    //Secuenciador
    int secAlta = sapp.Funciones.SecGeneral(con, st, rs, sNombreColum);
    String s = Integer.toString(secAlta);
    for (int i = 1; s.length() < longcod; i++) {
      s = "0" + s;
    }
    return s;
  }

}