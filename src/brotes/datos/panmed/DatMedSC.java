/**
 * Clase: DatMedSC
 * Paquete: brotes.datos.panmed
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 20/12/1999
 * Analisis Funcional: Punto 2.2 Mantenimiento Informe Final del Brote.
 * Descripcion: Estructura de datos que contiene los datos asociados
 *   a una MEDIDA, resultado de la busqueda de medidas adoptadas
 *   asociadas a un BROTE.
 */

package brotes.datos.panmed;

import java.io.Serializable;

public class DatMedSC
    implements Serializable {

  // Datos resultado de la busqueda de una unica medida
  private String CD_ANO = "";
  private String NM_ALERBRO = "";
  private String CD_GRUPO = "";
  private String CD_MEDIDA = "";
  private String DS_MEDIDA = "";
  private String IT_INFADIC = "";
  private String IT_REPE = "";
  private String NM_SECMB = "";
  private String DS_INFADI = "";
  private String CD_OPE = "";
  private String FC_ULTACT = "";

  public DatMedSC(String Ano,
                  String AlerBro,
                  String Grupo,
                  String CodMed,
                  String DescMed,
                  String FlInfAdic,
                  String FlRepe,
                  String SecMB,
                  String DescInfAdi,
                  String Ope,
                  String FUlt) { // Constructor

    if (Ano != null) {
      CD_ANO = Ano;
    }
    else {
      CD_ANO = "";
    }
    if (AlerBro != null) {
      NM_ALERBRO = AlerBro;
    }
    else {
      NM_ALERBRO = "";
    }
    if (Grupo != null) {
      CD_GRUPO = Grupo;
    }
    else {
      CD_GRUPO = "";
    }
    if (CodMed != null) {
      CD_MEDIDA = CodMed;
    }
    else {
      CD_MEDIDA = "";
    }
    if (DescMed != null) {
      DS_MEDIDA = DescMed;
    }
    else {
      DS_MEDIDA = "";
    }
    if (FlInfAdic != null) {
      IT_INFADIC = FlInfAdic;
    }
    else {
      IT_INFADIC = "";
    }
    if (FlRepe != null) {
      IT_REPE = FlRepe;
    }
    else {
      IT_REPE = "";
    }
    if (SecMB != null) {
      NM_SECMB = SecMB;
    }
    else {
      NM_SECMB = "";
    }
    if (DescInfAdi != null) {
      DS_INFADI = DescInfAdi;
    }
    else {
      DS_INFADI = "";
    }
    if (Ope != null) {
      CD_OPE = Ope;
    }
    else {
      CD_OPE = "";
    }
    if (FUlt != null) {
      FC_ULTACT = FUlt;
    }
    else {
      FC_ULTACT = "";
    }
  }

  public String getCD_ANO() {
    return CD_ANO;
  }

  public String getNM_ALERBRO() {
    return NM_ALERBRO;
  }

  public String getCD_GRUPO() {
    return CD_GRUPO;
  }

  public String getCD_MEDIDA() {
    return CD_MEDIDA;
  }

  public String getDS_MEDIDA() {
    return DS_MEDIDA;
  }

  public String getIT_INFADIC() {
    return IT_INFADIC;
  }

  public String getIT_REPE() {
    return IT_REPE;
  }

  public String getNM_SECMB() {
    return NM_SECMB;
  }

  public String getDS_INFADI() {
    return DS_INFADI;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }

  public void setCD_OPE(String ope) {
    CD_OPE = ope;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public void setFC_ULTACT(String fultact) {
    FC_ULTACT = fultact;
  }

} // endclass DatSintSC
