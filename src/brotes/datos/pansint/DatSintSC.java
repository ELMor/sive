/**
 * Clase: DatSintSC
 * Paquete: brotes.datos.pansint
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 09/12/1999
 * Analisis Funcional: Punto 2. Mantenimiento Sintomas.
 * Descripcion: Estructura de datos que contiene los datos asociados
 *   a un SINTOMA, resultado de la busqueda de sintomas
 *   asociados a un BROTE.
 */

package brotes.datos.pansint;

import java.io.Serializable;

public class DatSintSC
    implements Serializable {

  // Datos resultado de la busqueda de un unico sintoma
  private String CD_ANO = "";
  private String NM_ALERBRO = "";
  private String CD_SINTOMA = "";
  private String DS_SINTOMA = "";
  private String IT_MASINF = "";
  private String DS_MASINF = "";
  private String NM_CASOS = "";
  private String CD_OPE = "";
  private String FC_ULTACT = "";

  public DatSintSC(String Ano,
                   String AlerBro,
                   String CodSint,
                   String DescSint,
                   String FlMasInf,
                   String DescMasInf,
                   String Casos,
                   String Ope,
                   String FUlt) { // Constructor

    if (Ano != null) {
      CD_ANO = Ano;
    }
    else {
      CD_ANO = "";
    }
    if (AlerBro != null) {
      NM_ALERBRO = AlerBro;
    }
    else {
      NM_ALERBRO = "";
    }
    if (CodSint != null) {
      CD_SINTOMA = CodSint;
    }
    else {
      CD_SINTOMA = "";
    }
    if (DescSint != null) {
      DS_SINTOMA = DescSint;
    }
    else {
      DS_SINTOMA = "";
    }
    if (FlMasInf != null) {
      IT_MASINF = FlMasInf;
    }
    else {
      IT_MASINF = "";
    }
    if (DescMasInf != null) {
      DS_MASINF = DescMasInf;
    }
    else {
      DS_MASINF = "";
    }
    if (Casos != null) {
      NM_CASOS = Casos;
    }
    else {
      NM_CASOS = "";
    }
    if (Ope != null) {
      CD_OPE = Ope;
    }
    else {
      CD_OPE = "";
    }
    if (FUlt != null) {
      FC_ULTACT = FUlt;
    }
    else {
      FC_ULTACT = "";
    }
  }

  public String getCD_ANO() {
    return CD_ANO;
  }

  public String getNM_ALERBRO() {
    return NM_ALERBRO;
  }

  public String getCD_SINTOMA() {
    return CD_SINTOMA;
  }

  public String getDS_SINTOMA() {
    return DS_SINTOMA;
  }

  public String getIT_MASINF() {
    return IT_MASINF;
  }

  public String getDS_MASINF() {
    return DS_MASINF;
  }

  public String getNM_CASOS() {
    return NM_CASOS;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }

  public void setCD_OPE(String ope) {
    CD_OPE = ope;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public void setFC_ULTACT(String fultact) {
    FC_ULTACT = fultact;
  }

} // endclass DatSintSC
