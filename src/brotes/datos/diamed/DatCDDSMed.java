/**
 * Clase: DatCDDSMed
 * Paquete: brotes.datos.diamed
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 20/12/1999
 * Analisis Funcional: Punto 2.2 Mantenimiento Informe Final del Brote.
 * Descripcion: Estructura de datos que contiene los datos asociados
 *   a un tipo de MEDIDA.
 */

package brotes.datos.diamed;

import java.io.Serializable;

public class DatCDDSMed
    implements Serializable {

  private String CD_GRUPO = "";
  private String CD_MEDIDA = "";
  private String DS_MEDIDA = "";
  private String IT_INFADIC = "";
  private String IT_REPE = "";

  public DatCDDSMed(String Grupo, String Medida, String DescMedida,
                    String FlInfAdic, String FlRepe) {
    CD_GRUPO = Grupo;
    CD_MEDIDA = Medida;
    DS_MEDIDA = DescMedida;
    IT_INFADIC = FlInfAdic;
    IT_REPE = FlRepe;
  }

  public String getCD_GRUPO() {
    return CD_GRUPO;
  }

  public String getCD_MEDIDA() {
    return CD_MEDIDA;
  }

  public String getDS_MEDIDA() {
    return DS_MEDIDA;
  }

  public String getIT_INFADIC() {
    return IT_INFADIC;
  }

  public String getIT_REPE() {
    return IT_REPE;
  }
}