package brotes.datos.bidata;

import java.util.Hashtable;

//Limite: 49 si, 50 NO
//Estructura para sive_brotes
public class DatAlBro
    extends Hashtable {

//CONSTRUCTORES
  public DatAlBro() {
    super();
  }

//Si es un nulo, no hara NADA, y al hacer el get tendre null
//Si no es nulo, hara un put y al hacer el get obtendre el dato
  public synchronized void insert(Object key, Object value) {
    if ( (key != null) && (value != null)) {
      put(key, value);
    }
  }

////////////// brote
  public String getCD_ANO() {
    return (String) get("CD_ANO");
  }

  public String getNM_ALERBRO() {
    return (String) get("NM_ALERBRO");
  }

  public String getFC_FSINPRIMC_F() {
    return (String) get("FC_FSINPRIMC_F");
  }

  public String getFC_FSINPRIMC_H() {
    return (String) get("FC_FSINPRIMC_H");
  }

  public String getFC_ISINPRIMC_F() {
    return (String) get("FC_ISINPRIMC_F");
  }

  public String getFC_ISINPRIMC_H() {
    return (String) get("FC_ISINPRIMC_H");
  }

  public String getNM_PERINMIN() {
    return (String) get("NM_PERINMIN");
  }

  public String getNM_PERINMAX() {
    return (String) get("NM_PERINMAX");
  }

  public String getNM_PERINMED() {
    return (String) get("NM_PERINMED");
  }

  public String getNM_ENFERMOS() {
    return (String) get("NM_ENFERMOS");
  }

  public String getFC_EXPOSICION_F() {
    return (String) get("FC_EXPOSICION_F");
  }

  public String getFC_EXPOSICION_H() {
    return (String) get("FC_EXPOSICION_H");
  }

  public String getNM_INGHOSP() {
    return (String) get("NM_INGHOSP");
  }

  public String getNM_DEFUNCION() {
    return (String) get("NM_DEFUNCION");
  }

  public String getNM_EXPUESTOS() {
    return (String) get("NM_EXPUESTOS");
  }

  public String getCD_TRANSMIS() {
    return (String) get("CD_TRANSMIS");
  }

  public String getNM_NVACENF() {
    return (String) get("NM_NVACENF");
  }

  public String getCD_OPE() {
    return (String) get("CD_OPE");
  }

  public String getFC_ULTACT() {
    return (String) get("FC_ULTACT");
  }

  public String getNM_DCUACMIN() {
    return (String) get("NM_DCUACMIN");
  }

  public String getNM_DCUACMED() {
    return (String) get("NM_DCUACMED");
  }

  public String getNM_DCUACMAX() {
    return (String) get("NM_DCUACMAX");
  }

  public String getNM_NVACNENF() {
    return (String) get("NM_NVACNENF");
  }

  public String getNM_VACNENF() {
    return (String) get("NM_VACNENF");
  }

  public String getNM_VACENF() {
    return (String) get("NM_VACENF");
  }

  public String getDS_OBSERV() {
    return (String) get("DS_OBSERV");
  }

  public String getCD_TIPOCOL() {
    return (String) get("CD_TIPOCOL");
  }

  public String getDS_NOMCOL() {
    return (String) get("DS_NOMCOL");
  }

  public String getDS_DIRCOL() {
    return (String) get("DS_DIRCOL");
  }

  public String getDS_PISOCOL() {
    return (String) get("DS_PISOCOL");
  }

  public String getDS_NMCALLE() {
    return (String) get("DS_NMCALLE");
  }

  public String getCD_POSTALCOL() {
    return (String) get("CD_POSTALCOL");
  }

  public String getCD_CACOL() {
    return (String) get("CD_CACOL");
  }

  public String getCD_PROVCOL() {
    return (String) get("CD_PROVCOL");
  }

  public String getDS_BROTE() {
    return (String) get("DS_BROTE");
  }

  public String getCD_GRUPO() {
    return (String) get("CD_GRUPO");
  }

  public String getFC_FECHAHORA_F() {
    return (String) get("FC_FECHAHORA_F");
  }

  public String getFC_FECHAHORA_H() {
    return (String) get("FC_FECHAHORA_H");
  }

  public String getNM_MANIPUL() {
    return (String) get("NM_MANIPUL");
  }

  public String getCD_ZBS_LE() {
    return (String) get("CD_ZBS_LE");
  }

  public String getCD_NIVEL_2_LE() {
    return (String) get("CD_NIVEL_2_LE");
  }

  public String getCD_TNOTIF() {
    return (String) get("CD_TNOTIF");
  }

  public String getIT_RESCALC() {
    return (String) get("IT_RESCALC");
  }

  public String getCD_MUNCOL() {
    return (String) get("CD_MUNCOL");
  }

  public String getCD_NIVEL_1_LCA() {
    return (String) get("CD_NIVEL_1_LCA");
  }

  public String getDS_TELCOL() {
    return (String) get("DS_TELCOL");
  }

  public String getCD_NIVEL_1_LE() {
    return (String) get("CD_NIVEL_1_LE");
  }

  public String getCD_NIVEL_2_LCA() {
    return (String) get("CD_NIVEL_2_LCA");
  }

  public String getCD_ZBS_LCA() {
    return (String) get("CD_ZBS_LCA");
  }

//descripciones
  public String getDS_ZBS_LE() {
    return (String) get("DS_ZBSLE");
  }

  public String getDS_NIVEL_2_LE() {
    return (String) get("DS_NIVEL_2_LE");
  }

  public String getDS_NIVEL_1_LE() {
    return (String) get("DS_NIVEL_1_LE");
  }

  public String getDS_NIVEL_1_LCA() {
    return (String) get("DS_NIVEL_1_LCA");
  }

  public String getDS_NIVEL_2_LCA() {
    return (String) get("DS_NIVEL_2_LCA");
  }

  public String getDS_ZBS_LCA() {
    return (String) get("DS_ZBS_LCA");
  }

  public String getDS_MUNCOL() {
    return (String) get("DS_MUNCOL");
  }

//Faltan algunas.... las que no aparecen en el documento
  public String getCD_TBROTE() {
    return (String) get("CD_TBROTE");
  }

  public String getIT_PERIN() {
    return (String) get("IT_PERIN");
  }

  public String getIT_DCUAC() {
    return (String) get("IT_DCUAC");
  }

//Datos de la alerta con la que se corresponde
  public String getCD_SITALERBRO() {
    return (String) get("CD_SITALERBRO");
  }

  public String getFC_ALERBRO() {
    return (String) get("FC_ALERBRO");
  }

} //FIN DE LA CLASE
