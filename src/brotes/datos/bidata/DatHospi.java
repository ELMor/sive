package brotes.datos.bidata;

import java.util.Hashtable;

//Limite: 49 si, 50 NO
//Estructura para sive_alerta_brotes, sive_alerta_adic, sive_alerta_colec
public class DatHospi
    extends Hashtable {

//CONSTRUCTORES
  public DatHospi() {
    super();
  }

//Si es un nulo, no hara NADA, y al hacer el get tendre null
//Si no es nulo, hara un put y al hacer el get obtendre el dato
  public synchronized void insert(Object key, Object value) {
    if ( (key != null) && (value != null)) {
      put(key, value);
    }
  }

  public String getNM_ALERHOSP() {
    return (String) get("NM_ALERHOSP");
  }

  public String getCD_ANO() {
    return (String) get("CD_ANO");
  }

  public String getNM_ALERBRO() {
    return (String) get("NM_ALERBRO");
  }

  public String getDS_HOSPITAL() {
    return (String) get("DS_HOSPITAL");
  }

  public String getFC_INGHOSP() {
    return (String) get("FC_INGHOSP");
  }

  public String getDS_OBSERV() {
    return (String) get("DS_OBSERV");
  }

} //FIN DE LA CLASE
