/*
 * Mantenimiento de usuarios de Brotes.
 * Es un Copy&Paste del mantenimiento de usuarios de Brotes.
 *      Fecha         Autor           Accion
 *    11/04/2000      JRM             La crea
 */
package brotes.datos.mantus;

import java.io.Serializable;
import java.util.Vector;

/**
 *  Datos de usuario necesitados en el proceso.
 *
 */
public class DataUsuario
    implements Serializable {

  /**
   * C�digo de usuario
   */
  protected String sCodUsu = "";
  /**
   * Nombre
   */
  protected String sDesNom = "";
  /**
   * Cominidad aut�noma
   */
  protected String sCodComAut = "";
  /**
   * Perfil
   */
  protected String sPer = "";
  /**
   * Flag que indica si el usuario puede validar �?
   */
  protected boolean bValidar;
  /**
   * �mbito de usuario
   */
  protected Vector vAmbUsu = new Vector();
  protected String sCodApl = "";

  /**
   * Constructor por defecto
   */
  public DataUsuario() {

  }

  /**
   * Constructor con par�metros
   */
  public DataUsuario(String codUsu,
                     String desNom,
                     String codComAut,
                     String per, boolean validar,
                     Vector ambUsu,
                     String CodApl) {

    sCodUsu = codUsu;
    sDesNom = desNom;
    sCodComAut = codComAut;
    sPer = per;
    bValidar = validar;
    vAmbUsu = ambUsu;
    sCodApl = CodApl;
  }

  // GET's
  public String getCodUsu() {
    return sCodUsu;
  }

  public String getDesNom() {
    return sDesNom;
  }

  public String getCodApl() {
    return sCodApl;
  }

  public String getCodComAut() {
    return sCodComAut;
  }

  public String getPer() {
    String s;

    s = sPer;
    if (s == null) {
      s = "";

    }
    return s;
  }

  public boolean getValidar() {
    return bValidar;
  }

  public Vector getAmbUsu() {
    return vAmbUsu;
  }

  // SET'S
  public void setAmbUsu(Vector v) {
    vAmbUsu = v;
  }

  public void setCodUsu(String codUsu) {
    sCodUsu = codUsu;
    if (codUsu == null) {
      sCodUsu = "";
    }
  }

  public void setDesNom(String desNom) {
    sDesNom = desNom;
    if (sDesNom == null) {
      sDesNom = "";
    }
  }

  public void setCodApl(String CodApl) {
    sCodApl = CodApl;
    if (CodApl == null) {
      sCodApl = "";
    }
  }

  public void setPer(String per) {
    sPer = per;
    if (per == null) {
      sPer = "";
    }
  }

  public void setValidar(boolean validar) {
    bValidar = validar;
  }

  public void setCodComAut(String CodAut) {
    sCodComAut = CodAut;
  }

}
