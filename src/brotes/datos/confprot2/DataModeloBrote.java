package brotes.datos.confprot2;

import java.io.Serializable;

public class DataModeloBrote
    implements Serializable {

  protected String sCD_TSIVE;
  protected String sCD_MODELO;
  protected String sDS_MODELO;
  protected String sDSL_MODELO;
  //Los 2 sgutes identifican el brote
  protected String sCD_ANO;
  protected int iNM_ALERBRO;
  protected String sIT_OK;
  protected String sCD_ENFCIE;
  protected String sPartir;
  protected String sBaja;
  protected String sCD_CA;

  public DataModeloBrote(String tSive, String cdMod, String dsMod,
                         String dslMod,
                         String cdAno, int nmAlerbro, String itOK,
                         String Partir, String deBaja, String cdEnfcie,
                         String cdCa) {

    sCD_TSIVE = tSive;
    sCD_MODELO = cdMod;
    sDS_MODELO = dsMod;
    sDSL_MODELO = dslMod;
    sCD_ANO = cdAno;
    iNM_ALERBRO = nmAlerbro;
    sIT_OK = itOK;
    sPartir = Partir;
    sBaja = deBaja;
    sCD_ENFCIE = cdEnfcie;
    sCD_CA = cdCa;
  }

  public String getBaja() {
    return sBaja;
  }

  public String getCD_TSIVE() {
    return sCD_TSIVE;
  }

  public String getCD_Modelo() {
    return sCD_MODELO;
  }

  public String getDS_Modelo() {
    return sDS_MODELO;
  }

  public String getDSL_Modelo() {
    return sDSL_MODELO;
  }

  public String getCD_Ano() {
    return sCD_ANO;
  }

  public int getNM_Alerbro() {
    return iNM_ALERBRO;
  }

  public String getIT_OK() {
    return sIT_OK;
  }

  public String getPartir() {
    return sPartir;
  }

  public String getCD_Enfcie() {
    return sCD_ENFCIE;
  }

  public String getCD_CA() {
    return sCD_CA;
  }

}
