package brotes.datos.confprot2;

import java.io.Serializable;

public class DataMantLineaBrote
    implements Serializable {

  public int iLinea = 0;
  public String sTipo = null;
  public String sDescGeneral = null;
  public String sDescLocal = null;
  public String sCodPregunta = null;
  public boolean bObligatoria = false;
  public boolean bCondicionada = false;
  public int iCondALinea = 0;
  public String sCondAValor = null;

  // parametros de la pregunta
  public String sTPreg = null;
  public int iLong = 0;
  public int iEnt = 0;
  public int iDec = 0;

  public final static String tipoETIQUETA = "E";
  public final static String tipoPREGUNTA = "P";

  public DataMantLineaBrote() {
  }

  public DataMantLineaBrote(int Linea,
                            String Tipo,
                            String DescGeneral,
                            String DescLocal,
                            String CodPregunta,
                            boolean Obligatoria,
                            boolean Condicionada,
                            int CondALinea,
                            String CondAValor) {
    iLinea = Linea;
    sTipo = Tipo;
    sDescGeneral = DescGeneral;
    if (DescLocal != null) {
      if (DescLocal.length() > 0) {
        sDescLocal = DescLocal;
      }
    }
    if (CodPregunta != null) {
      if (CodPregunta.length() > 0) {
        sCodPregunta = CodPregunta;
      }
    }
    bObligatoria = Obligatoria;
    bCondicionada = Condicionada;
    iCondALinea = CondALinea;
    sCondAValor = CondAValor;
  }

  public DataMantLineaBrote(int Linea,
                            String Tipo,
                            String DescGeneral,
                            String DescLocal) {
    iLinea = Linea;
    sTipo = Tipo;
    sDescGeneral = DescGeneral;
    sDescLocal = DescLocal;
    sCodPregunta = "";
    bObligatoria = false;
    bCondicionada = false;
    iCondALinea = 0;
    sCondAValor = "";
  }

}
