package brotes.datos.confprot2;

import java.io.Serializable;

public class DataBroteBasico
    implements Serializable {

  protected String sCD_ANO;
  protected String sNM_ALERBRO;
  protected String sCD_GRUPO;
  protected String sCD_TBROTE;
  protected String sDS_BROTE;

  public DataBroteBasico(String cdAno, String nmAlerbro,
                         String cdGrupo, String cdTBrote,
                         String dsBrote) {

    sCD_ANO = cdAno;
    sNM_ALERBRO = nmAlerbro;
    sCD_GRUPO = cdGrupo;
    sCD_TBROTE = cdTBrote;
    sDS_BROTE = dsBrote;
  }

  public String getCD_Ano() {
    return sCD_ANO;
  }

  public String getNM_Alerbro() {
    return sNM_ALERBRO;
  }

  public String getCD_GRUPO() {
    return sCD_GRUPO;
  }

  public String getCD_TBROTE() {
    return sCD_TBROTE;
  }

  public String getDS_BROTE() {
    return sDS_BROTE;
  }
}
