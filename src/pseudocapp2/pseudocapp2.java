// 07/04/2000 (JMT) Applet que extiende de capp2.CApp utilizado
//  en el enganche de capp.CApp y capp2.CApp

package pseudocapp2;

import capp2.CApp;

public class Pseudocapp2
    extends CApp {

  public void init() {
    super.init();
    setTitulo("PSEUDOCAPP2");
    this.setName("PseudoCapp2");
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
  }

} // Fin clase Pseudocapp2