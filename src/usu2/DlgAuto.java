package usu2;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CListaValores;
import capp2.CTabla;
import jclass.bwt.BWTEnum;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

/**
 * Di�logo a trav�s del cu�l se actualizan las autorizaciones
 * de un grupo de trabajo.
 * @autor LSR
 * @version 1.0
 */
public class DlgAuto
    extends CDialog {

  // modos de apertura de la ventana
  final public int modoUSUARIOS = 0;
  final public int modoACCIONES = 1;

  // parametros pasados al di�logo
  private Data dtGrupo = null;

  // modo de operaci�n
  private int modoApertura;

  // lista
  private Lista lista = new Lista();

  XYLayout xYLayout1 = new XYLayout();
  ButtonControl btnCancelar = new ButtonControl();
  GroupBox grp = new GroupBox();
  XYLayout xYLayout2 = new XYLayout();
  CTabla tbl = new CTabla();
  ButtonControl btnAdd = new ButtonControl();
  ButtonControl btnDel = new ButtonControl();

  // constructor del di�logo DlgAuto
  public DlgAuto(CApp a, int modoop, Data dt) {

    super(a);

    try {
      modoApertura = modoop;
      dtGrupo = dt;

      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void Inicializar() {}

  // inicia el aspecto del dialogo
  public void jbInit() throws Exception {
    final String imgCANCELAR = "images/cancelar.gif";
    final String imgBAJA = "images/baja.gif";
    final String imgALTA = "images/alta.gif";
    int i;
    Data dt = null;

    this.setLayout(xYLayout1);
    this.setSize(700, 275);
    xYLayout1.setHeight(285);
    xYLayout1.setWidth(700);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgCANCELAR);
    this.getApp().getLibImagenes().put(imgALTA);
    this.getApp().getLibImagenes().put(imgBAJA);
    this.getApp().getLibImagenes().CargaImagenes();
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));
    btnAdd.setImage(this.getApp().getLibImagenes().get(imgALTA));
    btnDel.setImage(this.getApp().getLibImagenes().get(imgBAJA));

    btnCancelar.setActionCommand("salir");
    btnCancelar.setLabel("Salir");
    grp.setLayout(xYLayout2);
    btnAdd.setActionCommand("add");
    btnDel.setActionCommand("del");
    btnAdd.addActionListener(new DlgAuto_actionAdapter(this));
    btnDel.addActionListener(new DlgAuto_actionAdapter(this));
    btnCancelar.addActionListener(new DlgAuto_actionAdapter(this));

    this.add(grp, new XYConstraints(3, 5, 685, 216));
    grp.add(tbl, new XYConstraints(7, 6, 591, 177));
    grp.add(btnAdd, new XYConstraints(616, 31, 26, 26));
    grp.add(btnDel, new XYConstraints(616, 62, 26, 26));
    this.add(btnCancelar, new XYConstraints(593, 224, 89, -1));

    // inicializa valores
    switch (modoApertura) {
      case modoUSUARIOS:
        setTitle("Usuarios del Grupo de Trabajo [" +
                 dtGrupo.getString("DESCRIPCION") + "]");
        grp.setLabel("Usuarios");
        tbl.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(
            "Usuario\nNombre\n", '\n'));
        tbl.setColumnWidths(jclass.util.JCUtilConverter.toIntList("170\n400",
            '\n'));
        tbl.setNumColumns(2);
        lista = getUsers(dtGrupo);
        break;

      case modoACCIONES:
        setTitle("Acciones del Grupo de Trabajo [" +
                 dtGrupo.getString("DESCRIPCION") + "]");
        grp.setLabel("Acciones");
        tbl.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(
            "Acci�n\nC�digo\n", '\n'));
        tbl.setColumnWidths(jclass.util.JCUtilConverter.toIntList("400\n170",
            '\n'));
        tbl.setNumColumns(2);
        lista = getActions(dtGrupo);
        break;
    }
    refreshCTabla();
    Inicializar(CInicializar.NORMAL);
  }

  // inicializar
  public void Inicializar(int modo) {
    switch (modo) {

      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        this.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
  }

  // lee los usuarios de un grupo
  private Lista getUsers(Data dtGrupo) {
    Lista v = new Lista();
    QueryTool qt = new QueryTool();

    qt.putName("V_APLIC_USU_GRU");
    qt.putType("COD_USUARIO", QueryTool.STRING);
    qt.putType("NOMBRE", QueryTool.STRING);

    // filtro de usuarios en aplic*grupo
    qt.putWhereType("COD_APLICACION", QueryTool.STRING);
    qt.putWhereValue("COD_APLICACION", getApp().getParameter("COD_APLICACION"));
    qt.putOperator("COD_APLICACION", "=");
    qt.putWhereType("COD_GRUPO", QueryTool.INTEGER);
    qt.putWhereValue("COD_GRUPO", dtGrupo.getString("COD_GRUPO"));
    qt.putOperator("COD_GRUPO", "=");

    // ordenacion
    qt.addOrderField("COD_USUARIO");

    // consulta la vista V_APLIC_USU_GRU
    try {
      this.getApp().getStub().setUrl("servlet/SrvQueryTool");
      v.addElement(qt);
      v = (Lista)this.getApp().getStub().doPost(1, v);

      // error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
      v = new Lista();
    }

    return v;
  }

  // lee las acciones de un grupo
  private Lista getActions(Data dtGrupo) {
    Lista v = new Lista();
    Vector vDatSubQuery = new Vector();
    Data dtSubQuery = null;
    QueryTool qt = new QueryTool();

    qt.putName("ACCION");
    qt.putType("COD_ACCION", QueryTool.INTEGER);
    qt.putType("DS_ACCION", QueryTool.STRING);
    qt.putWhereType("COD_APLICACION", QueryTool.STRING);
    qt.putWhereValue("COD_APLICACION", getApp().getParameter("COD_APLICACION"));
    qt.putOperator("COD_APLICACION", "=");

    // realiza el cruce con GRUPAUTO
    qt.putSubquery("COD_ACCION",
        "select COD_ACCION from GRUPAUTO where COD_APLICACION=? and COD_GRUPO=?");
    dtSubQuery = new Data();
    dtSubQuery.put(new Integer(QueryTool.STRING),
                   getApp().getParameter("COD_APLICACION"));
    vDatSubQuery.addElement(dtSubQuery);
    dtSubQuery = new Data();
    dtSubQuery.put(new Integer(QueryTool.INTEGER),
                   dtGrupo.getString("COD_GRUPO"));
    vDatSubQuery.addElement(dtSubQuery);
    qt.putVectorSubquery("COD_ACCION", vDatSubQuery);

    // ordenacion
    qt.addOrderField("DS_ACCION");

    // consulta la tabla ACCION
    try {
      this.getApp().getStub().setUrl("servlet/SrvQueryTool");
      v.addElement(qt);
      v = (Lista)this.getApp().getStub().doPost(1, v);

      // error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
      v = new Lista();
    }

    return v;
  }

  // graba una lista en una tabla
  private void writeCTabla(CTabla tbl,
                           Lista v,
                           String col1,
                           String col2) {
    int i;
    String sFila = null;
    Data dt = null;

    tbl.clear();

    for (i = 0; i < v.size(); i++) {

      dt = (Data) v.elementAt(i);

      sFila = dt.getString(col1) + "&" +
          dt.getString(col2) + "&";

      tbl.addItem(sFila, '&');
    }
  }

  // rellena la tabla
  private void refreshCTabla() {
    switch (modoApertura) {
      case modoACCIONES:
        writeCTabla(tbl,
                    lista,
                    "DS_ACCION",
                    "COD_ACCION");
        break;

      case modoUSUARIOS:
        writeCTabla(tbl,
                    lista,
                    "COD_USUARIO",
                    "NOMBRE");
        break;
    }
  }

  void btn_actionPerformed(ActionEvent e) {
    Data dt = null;
    QueryTool qt = null;
    CListaValores lupa = null;
    int i;
    Lista v = null;
    String sTitle = null;
    Vector vCod = null;
    boolean bAlta = true;

    Inicializar(CInicializar.ESPERA);

    // pulsar cancelar
    if (e.getActionCommand().equals("salir")) {
      dispose();

      // a�adir
    }
    else if (e.getActionCommand().equals("add")) {

      vCod = new Vector();
      qt = new QueryTool();

      // prepara la QT para seleccionarlos de la lupa
      switch (modoApertura) {
        case modoACCIONES:
          qt.putName("ACCION");
          qt.putType("COD_ACCION", QueryTool.INTEGER);
          qt.putType("DS_ACCION", QueryTool.STRING);
          qt.putWhereType("COD_APLICACION", QueryTool.STRING);
          qt.putWhereValue("COD_APLICACION",
                           getApp().getParameter("COD_APLICACION"));
          qt.putOperator("COD_APLICACION", "=");
          sTitle = "Acciones  [" + getApp().getParameter("COD_APLICACION") +
              "]";
          vCod.addElement("COD_ACCION");
          vCod.addElement("DS_ACCION");
          break;

        case modoUSUARIOS:
          qt.putName("USUARIO");
          qt.putType("COD_USUARIO", QueryTool.STRING);
          qt.putType("NOMBRE", QueryTool.STRING);
          qt.putWhereType("FC_BAJA", QueryTool.VOID);
          qt.putWhereValue("FC_BAJA", "");
          qt.putOperator("FC_BAJA", "is null");
          sTitle = "Usuarios";
          vCod.addElement("COD_USUARIO");
          vCod.addElement("NOMBRE");
          break;
      }

      lupa = new CListaValores(this.getApp(),
                               sTitle,
                               qt,
                               vCod);
      lupa.show();

      // graba el item
      if (lupa.getSelected() != null) {
        qt = new QueryTool();
        switch (modoApertura) {
          case modoACCIONES:

            // recorre las acciones para no repetirla
            for (i = 0; i < lista.size(); i++) {
              if ( ( (Data) lista.elementAt(i)).getString("COD_ACCION").equals(
                  lupa.getSelected().getString("COD_ACCION"))) {
                bAlta = false;
              }
            }
            qt.putName("GRUPAUTO");
            qt.putType("COD_APLICACION", QueryTool.STRING);
            qt.putValue("COD_APLICACION",
                        getApp().getParameter("COD_APLICACION"));
            qt.putType("COD_GRUPO", QueryTool.INTEGER);
            qt.putValue("COD_GRUPO", dtGrupo.getString("COD_GRUPO"));
            qt.putType("COD_ACCION", QueryTool.INTEGER);
            qt.putValue("COD_ACCION", lupa.getSelected().getString("COD_ACCION"));

            break;

          case modoUSUARIOS:

            // recorre los usuarios para no repetirla
            for (i = 0; i < lista.size(); i++) {
              if ( ( (Data) lista.elementAt(i)).getString("COD_USUARIO").equals(
                  lupa.getSelected().getString("COD_USUARIO"))) {
                bAlta = false;
              }
            }

            qt.putName("USUARIO_GRUPO");
            qt.putType("COD_APLICACION", QueryTool.STRING);
            qt.putValue("COD_APLICACION",
                        getApp().getParameter("COD_APLICACION"));
            qt.putType("COD_USUARIO", QueryTool.STRING);
            qt.putValue("COD_USUARIO",
                        lupa.getSelected().getString("COD_USUARIO"));
            qt.putType("COD_GRUPO", QueryTool.INTEGER);
            qt.putValue("COD_GRUPO", dtGrupo.getString("COD_GRUPO"));
            qt.putType("FC_ALTA", QueryTool.DATE);
            qt.putValue("FC_ALTA", getApp().getParameter("FC_ACTUAL"));
            break;
        }

        // si no est� repetida -> alta
        if (bAlta) {
          try {
            this.getApp().getStub().setUrl("servlet/SrvQueryTool");
            v = new Lista();
            v.addElement(qt);
            this.getApp().getStub().doPost(3, v);

            // a�ade el elemento a la tabla
            lista.addElement(lupa.getSelected());

            // vuelve a pintar la tabla
            refreshCTabla();

            // error en el servlet
          }
          catch (Exception ex) {
            this.getApp().trazaLog(ex);
            this.getApp().showError(ex.getMessage());
          }
        }

      }

      // borrar
    }
    else if (e.getActionCommand().equals("del")) {
      if (tbl.getSelectedIndex() != BWTEnum.NOTFOUND) {
        // item seleccionado
        dt = (Data) lista.elementAt(tbl.getSelectedIndex());

        qt = new QueryTool();

        // prepara la QT para darle de baja
        switch (modoApertura) {
          case modoACCIONES:
            qt.putName("GRUPAUTO");
            qt.putWhereType("COD_APLICACION", QueryTool.STRING);
            qt.putWhereValue("COD_APLICACION",
                             getApp().getParameter("COD_APLICACION"));
            qt.putOperator("COD_APLICACION", "=");
            qt.putWhereType("COD_ACCION", QueryTool.INTEGER);
            qt.putWhereValue("COD_ACCION", dt.getString("COD_ACCION"));
            qt.putOperator("COD_ACCION", "=");
            qt.putWhereType("COD_GRUPO", QueryTool.INTEGER);
            qt.putWhereValue("COD_GRUPO", dtGrupo.getString("COD_GRUPO"));
            qt.putOperator("COD_GRUPO", "=");
            break;

          case modoUSUARIOS:
            qt.putName("USUARIO_GRUPO");
            qt.putWhereType("COD_APLICACION", QueryTool.STRING);
            qt.putWhereValue("COD_APLICACION",
                             getApp().getParameter("COD_APLICACION"));
            qt.putOperator("COD_APLICACION", "=");
            qt.putWhereType("COD_USUARIO", QueryTool.STRING);
            qt.putWhereValue("COD_USUARIO", dt.getString("COD_USUARIO"));
            qt.putOperator("COD_USUARIO", "=");
            qt.putWhereType("COD_GRUPO", QueryTool.INTEGER);
            qt.putWhereValue("COD_GRUPO", dtGrupo.getString("COD_GRUPO"));
            qt.putOperator("COD_GRUPO", "=");
            break;
        }

        try {
          this.getApp().getStub().setUrl("servlet/SrvQueryTool");
          v = new Lista();
          v.addElement(qt);
          this.getApp().getStub().doPost(5, v);

          // elimina el elemento de la tabla
          lista.removeElementAt(tbl.getSelectedIndex());

          // vuelve a pintar la tabla
          refreshCTabla();

          // error en el servlet
        }
        catch (Exception ex) {
          this.getApp().trazaLog(ex);
          this.getApp().showError(ex.getMessage());
        }
      }
    }
    Inicializar(CInicializar.NORMAL);
  }
}

// escuchador de los botones
class DlgAuto_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DlgAuto adaptee;
  ActionEvent e;

  DlgAuto_actionAdapter(DlgAuto adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
