package usu2;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CCodigo;
import capp2.CDialog;
import capp2.CEntero;
import capp2.CInicializar;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

/**
 * Di�logo a trav�s del cu�l se insertan/modifican/borran datos
 * en tabla de grupos
 * @autor LSR
 * @version 1.0
 */
public class DlgGrupos
    extends CDialog {

  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  // parametros pasados al di�logo
  private Data dtGrupo = null;

  // gesti�n salir/grabar
  private boolean bAceptar = false;

  // modo de operaci�n
  private int modoApertura;

  XYLayout xYLayout1 = new XYLayout();

  Label lblCod = new Label();
  Label lblDes = new Label();
  CEntero txtCodigo = new CEntero(3);
  CCodigo txtDescripcion = new CCodigo(30);
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // constructor del di�logo DiaMant
  public DlgGrupos(CApp a, int modoop, Data dt) {

    super(a);

    try {
      modoApertura = modoop;
      dtGrupo = dt;

      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // comprueba los datos m�nimos
  private boolean isDataValid() {
    if ( (txtCodigo.getText().trim().length() == 0) &&
        (txtDescripcion.getText().trim().length() == 0)) {
      getApp().showAdvise("Debe informar todos los campos obligatorios");
      return false;
    }
    else {
      return true;
    }
  }

  public void Inicializar() {}

  // inicia el aspecto del dialogo
  public void jbInit() throws Exception {
    final String imgCANCELAR = "images/cancelar.gif";
    final String imgACEPTAR = "images/aceptar.gif";

    this.setLayout(xYLayout1);
    this.setSize(474, 143);
    xYLayout1.setHeight(156);
    xYLayout1.setWidth(474);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgACEPTAR);
    this.getApp().getLibImagenes().put(imgCANCELAR);
    this.getApp().getLibImagenes().CargaImagenes();
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));

    txtCodigo.setBackground(new Color(255, 255, 150));
    txtCodigo.setText("");
    txtDescripcion.setBackground(new Color(255, 255, 150));
    lblDes.setText("Descripci�n");
    lblCod.setText("C�digo");
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Grabar");
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnAceptar.addActionListener(new DlgGrupos_actionAdapter(this));
    btnCancelar.addActionListener(new DlgGrupos_actionAdapter(this));

    this.add(lblCod, new XYConstraints(21, 9, 47, -1));
    this.add(txtCodigo, new XYConstraints(99, 9, 106, -1));
    this.add(lblDes, new XYConstraints(21, 46, 76, -1));
    this.add(txtDescripcion, new XYConstraints(99, 46, 362, -1));
    this.add(btnAceptar, new XYConstraints(279, 93, 89, -1));
    this.add(btnCancelar, new XYConstraints(373, 93, 89, -1));

    // inicializa valores
    switch (modoApertura) {
      case modoALTA:
        setTitle("Nuevo Grupo de Trabajo");
        break;

      case modoMODIFICACION:
        txtCodigo.setText(dtGrupo.getString("COD_GRUPO"));
        txtDescripcion.setText(dtGrupo.getString("DESCRIPCION"));
        setTitle("Modifigar Grupo de Trabajo");
        break;

      case modoBAJA:
        txtCodigo.setText(dtGrupo.getString("COD_GRUPO"));
        txtDescripcion.setText(dtGrupo.getString("DESCRIPCION"));
        this.setTitle("Borrar Grupo de Trabajo");
        break;
    }
    Inicializar(CInicializar.NORMAL);
  }

  // inicializar
  public void Inicializar(int modo) {
    switch (modo) {

      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoApertura) {
          case modoALTA:
            this.setEnabled(true);
            break;

          case modoMODIFICACION:
            txtCodigo.setEnabled(false);
            txtDescripcion.setEnabled(true);
            btnCancelar.setEnabled(true);
            btnAceptar.setEnabled(true);
            break;

          case modoBAJA:
            txtCodigo.setEnabled(false);
            txtDescripcion.setEnabled(false);
            btnCancelar.setEnabled(true);
            btnAceptar.setEnabled(true);
            break;
        }
        break;
    }
  }

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

  void btn_actionPerformed(ActionEvent e) {

    final String servlet = "servlet/SrvQueryTool";
    Lista vParam = new Lista();
    QueryTool qtAux = null;
    Lista v = null;
    Data dt = null;
    String sMsg = null;

    if (e.getActionCommand().equals("Aceptar")) {
      Inicializar(CInicializar.ESPERA);
      if (isDataValid()) {

        // actualiza los valores
        dtGrupo = new Data();
        dtGrupo.put("COD_GRUPO", txtCodigo.getText().trim());
        dtGrupo.put("COD_APLICACION", getApp().getParameter("COD_APLICACION"));
        dtGrupo.put("DESCRIPCION", txtDescripcion.getText().trim());

        QueryTool qt = new QueryTool();

        // tabla de grupos
        qt.putName("GRUPO");

        switch (modoApertura) {

          // ----- NUEVO -----------
          case 0:

            // inserta el grupo
            try {
              qt.putType("COD_GRUPO", QueryTool.INTEGER);
              qt.putType("COD_APLICACION", QueryTool.STRING);
              qt.putType("DESCRIPCION", QueryTool.STRING);
              qt.putValue("COD_GRUPO", dtGrupo.getString("COD_GRUPO"));
              qt.putValue("COD_APLICACION", dtGrupo.getString("COD_APLICACION"));
              qt.putValue("DESCRIPCION", dtGrupo.getString("DESCRIPCION"));

              vParam.addElement(qt);
              this.getApp().getStub().setUrl(servlet);
              this.getApp().getStub().doPost(3, vParam);
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
            }
            break;

            // ---- MODIFICAR ----
          case 1:

            try {

              qt.putType("DESCRIPCION", QueryTool.STRING);
              qt.putValue("DESCRIPCION", dtGrupo.getString("DESCRIPCION"));

              qt.putWhereType("COD_GRUPO", QueryTool.INTEGER);
              qt.putWhereValue("COD_GRUPO", dtGrupo.getString("COD_GRUPO"));
              qt.putOperator("COD_GRUPO", "=");

              qt.putWhereType("COD_APLICACION", QueryTool.STRING);
              qt.putWhereValue("COD_APLICACION",
                               dtGrupo.getString("COD_APLICACION"));
              qt.putOperator("COD_APLICACION", "=");

              vParam.addElement(qt);
              this.getApp().getStub().setUrl(servlet);
              this.getApp().getStub().doPost(4, vParam);
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
            }
            break;
          case 2:

            // ---- BORRAR -------
            try {
              // comprueba que no tenga usuarios
              qtAux = new QueryTool();
              qtAux.putName("USUARIO_GRUPO");
              qtAux.putType("COUNT(COD_USUARIO)", QueryTool.INTEGER);
              qtAux.putWhereType("COD_GRUPO", QueryTool.INTEGER);
              qtAux.putWhereValue("COD_GRUPO", dtGrupo.getString("COD_GRUPO"));
              qtAux.putOperator("COD_GRUPO", "=");
              qtAux.putWhereType("COD_APLICACION", QueryTool.STRING);
              qtAux.putWhereValue("COD_APLICACION",
                                  dtGrupo.getString("COD_APLICACION"));
              qtAux.putOperator("COD_APLICACION", "=");
              v = new Lista();
              v.addElement(qtAux);
              this.getApp().getStub().setUrl(servlet);
              v = (Lista)this.getApp().getStub().doPost(1, v);
              if (v.size() > 0) {
                dt = (Data) v.elementAt(0);
                if ( (new Integer(dt.getString("COUNT(COD_USUARIO)"))).intValue() >
                    0) {
                  sMsg = "El grupo tiene usuarios asignados";
                }
              }

              // comprueba que no tenga acciones
              qtAux = new QueryTool();
              qtAux.putName("GRUPAUTO");
              qtAux.putType("COUNT(COD_ACCION)", QueryTool.INTEGER);
              qtAux.putWhereType("COD_GRUPO", QueryTool.INTEGER);
              qtAux.putWhereValue("COD_GRUPO", dtGrupo.getString("COD_GRUPO"));
              qtAux.putOperator("COD_GRUPO", "=");
              qtAux.putWhereType("COD_APLICACION", QueryTool.STRING);
              qtAux.putWhereValue("COD_APLICACION",
                                  dtGrupo.getString("COD_APLICACION"));
              qtAux.putOperator("COD_APLICACION", "=");
              v = new Lista();
              v.addElement(qtAux);
              this.getApp().getStub().setUrl(servlet);
              v = (Lista)this.getApp().getStub().doPost(1, v);
              if (v.size() > 0) {
                dt = (Data) v.elementAt(0);
                if ( (new Integer(dt.getString("COUNT(COD_ACCION)"))).intValue() >
                    0) {
                  sMsg = "El grupo tiene acciones asignadas";
                }
              }

              if (sMsg == null) {
                qt.putWhereType("COD_GRUPO", QueryTool.INTEGER);
                qt.putWhereValue("COD_GRUPO", dtGrupo.getString("COD_GRUPO"));
                qt.putOperator("COD_GRUPO", "=");

                qt.putWhereType("COD_APLICACION", QueryTool.STRING);
                qt.putWhereValue("COD_APLICACION",
                                 dtGrupo.getString("COD_APLICACION"));
                qt.putOperator("COD_APLICACION", "=");

                vParam.addElement(qt);
                this.getApp().getStub().setUrl(servlet);
                this.getApp().getStub().doPost(5, vParam);
                bAceptar = true;
                dispose();
              }
              else {
                getApp().showAdvise(sMsg);
              }

              // error en el servlet
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
            }
            break;
        }
      }

      Inicializar(CInicializar.NORMAL);

    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  }

}

class DlgGrupos_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DlgGrupos adaptee;
  ActionEvent e;

  DlgGrupos_actionAdapter(DlgGrupos adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
