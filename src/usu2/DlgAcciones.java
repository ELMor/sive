package usu2;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CEntero;
import capp2.CInicializar;
import capp2.CTexto;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

/**
 * Di�logo a trav�s del cu�l se insertan/modifican/borran datos
 * en tabla de acciones.
 * @autor LSR
 * @version 1.0
 */
public class DlgAcciones
    extends CDialog {

  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  // parametros pasados al di�logo
  private Data dtAccion = null;

  // gesti�n salir/grabar
  private boolean bAceptar = false;

  // modo de operaci�n
  private int modoApertura;

  XYLayout xYLayout1 = new XYLayout();

  Label lblCod = new Label();
  Label lblDes = new Label();
  CEntero txtCodigo = new CEntero(15);
  CTexto txtDescripcion = new CTexto(60);
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores de eventos...
  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);
  CheckboxGroup optEstado = new CheckboxGroup();
  Checkbox optOpen = new Checkbox();
  Checkbox optClosed = new Checkbox();
  Label lblEstado = new Label();

  // constructor del di�logo DiaMant
  public DlgAcciones(CApp a, int modoop, Data dt) {

    super(a);

    try {
      modoApertura = modoop;
      dtAccion = dt;

      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // comprueba los datos m�nimos
  private boolean isDataValid() {
    if ( (txtCodigo.getText().trim().length() == 0) &&
        (txtDescripcion.getText().trim().length() == 0)) {
      getApp().showAdvise("Debe informar todos los campos obligatorios");
      return false;
    }
    else {
      return true;
    }
  }

  // inicia el aspecto del dialogo
  public void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xYLayout1);
    this.setSize(400, 190);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    txtCodigo.setBackground(new Color(255, 255, 150));
    txtCodigo.setText("");
    txtDescripcion.setBackground(new Color(255, 255, 150));
    lblDes.setText("Descripci�n");
    lblCod.setText("C�digo");
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    optOpen.setLabel("Abierta");
    optOpen.setCheckboxGroup(optEstado);
    optClosed.setLabel("Cerrada");
    optClosed.setCheckboxGroup(optEstado);
    lblEstado.setText("Estado");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...
    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xYLayout1.setHeight(172);
    xYLayout1.setWidth(397);

    this.add(lblCod, new XYConstraints(21, 11, 62, -1));
    this.add(txtCodigo, new XYConstraints(129, 11, 106, -1));
    this.add(lblDes, new XYConstraints(21, 48, 76, -1));
    this.add(txtDescripcion, new XYConstraints(129, 48, 246, -1));
    this.add(lblEstado, new XYConstraints(21, 85, 84, 25));
    this.add(optOpen, new XYConstraints(126, 85, 108, 26));
    this.add(optClosed, new XYConstraints(249, 85, 114, 25));
    this.add(btnAceptar, new XYConstraints(187, 119, 89, -1));
    this.add(btnCancelar, new XYConstraints(281, 119, 89, -1));

    // inicializa valores
    switch (modoApertura) {
      case modoALTA:
        optOpen.setState(true);
        setTitle("Nueva acci�n");
        break;

      case modoMODIFICACION:
        txtCodigo.setText(dtAccion.getString("COD_ACCION"));
        txtDescripcion.setText(dtAccion.getString("DS_ACCION"));
        if (dtAccion.getString("SW_ESTADO").length() == 0) {
          optOpen.setState(true);
        }
        else {
          optClosed.setState(true);
        }
        this.setTitle("Modificar acci�n");
        break;

      case modoBAJA:
        txtCodigo.setText(dtAccion.getString("COD_ACCION"));
        txtDescripcion.setText(dtAccion.getString("DS_ACCION"));
        if (dtAccion.getString("SW_ESTADO").length() == 0) {
          optOpen.setState(true);
        }
        else {
          optClosed.setState(true);
        }
        this.setTitle("Borrar acci�n");
        break;
    }
    Inicializar(CInicializar.NORMAL);
  }

  // inicializar
  public void Inicializar(int modo) {
    switch (modo) {

      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoApertura) {
          case modoALTA:
            this.setEnabled(true);
            break;

          case modoMODIFICACION:
            txtCodigo.setEnabled(false);
            txtDescripcion.setEnabled(true);
            optOpen.setEnabled(true);
            optClosed.setEnabled(true);
            btnCancelar.setEnabled(true);
            btnAceptar.setEnabled(true);
            break;

          case modoBAJA:
            txtCodigo.setEnabled(false);
            txtDescripcion.setEnabled(false);
            optOpen.setEnabled(false);
            optClosed.setEnabled(false);
            btnCancelar.setEnabled(true);
            btnAceptar.setEnabled(true);
            break;
        }
        break;
    }
  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {

    final String servlet = "servlet/SrvQueryTool";
    Lista vParam = new Lista();
    QueryTool qtAux = null;
    Data dt = null;
    String sMsg = null;
    Lista v;

    if (e.getActionCommand().equals("Aceptar")) {
      Inicializar(CInicializar.ESPERA);
      if (isDataValid()) {

        // actualiza los valores
        dtAccion = new Data();
        dtAccion.put("COD_ACCION", txtCodigo.getText().trim());
        dtAccion.put("COD_APLICACION", getApp().getParameter("COD_APLICACION"));
        dtAccion.put("DS_ACCION", txtDescripcion.getText().trim());
        if (optOpen.getState()) {
          dtAccion.put("SW_ESTADO", "");
        }
        else {
          dtAccion.put("SW_ESTADO", "X");

        }
        QueryTool qt = new QueryTool();

        // tabla de acciones
        qt.putName("ACCION");
        qt.putType("COD_ACCION", QueryTool.INTEGER);
        qt.putType("COD_APLICACION", QueryTool.STRING);
        qt.putType("DS_ACCION", QueryTool.STRING);
        qt.putType("SW_ESTADO", QueryTool.STRING);

        switch (modoApertura) {

          // ----- NUEVO -----------
          case 0:

            // inserta la accion
            try {
              qt.putValue("COD_ACCION", dtAccion.getString("COD_ACCION"));
              qt.putValue("COD_APLICACION", dtAccion.getString("COD_APLICACION"));
              qt.putValue("DS_ACCION", dtAccion.getString("DS_ACCION"));
              qt.putValue("SW_ESTADO", dtAccion.getString("SW_ESTADO"));

              vParam.addElement(qt);
              this.getApp().getStub().setUrl(servlet);
              this.getApp().getStub().doPost(3, vParam);
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
            }
            break;

            // ---- MODIFICAR ----
          case 1:

            try {
              qt.putValue("COD_ACCION", dtAccion.getString("COD_ACCION"));
              qt.putValue("COD_APLICACION", dtAccion.getString("COD_APLICACION"));
              qt.putValue("DS_ACCION", dtAccion.getString("DS_ACCION"));
              qt.putValue("SW_ESTADO", dtAccion.getString("SW_ESTADO"));

              qt.putWhereType("COD_ACCION", QueryTool.INTEGER);
              qt.putWhereValue("COD_ACCION", dtAccion.getString("COD_ACCION"));
              qt.putOperator("COD_ACCION", "=");

              qt.putWhereType("COD_APLICACION", QueryTool.STRING);
              qt.putWhereValue("COD_APLICACION",
                               dtAccion.getString("COD_APLICACION"));
              qt.putOperator("COD_APLICACION", "=");

              vParam.addElement(qt);
              this.getApp().getStub().setUrl(servlet);
              this.getApp().getStub().doPost(4, vParam);
              bAceptar = true;
              dispose();
              // error en el servlet
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
            }
            break;
          case 2:

            // ---- BORRAR -------
            try {
              // comprueba que no est� vinculada
              qtAux = new QueryTool();
              qtAux.putName("GRUPAUTO");
              qtAux.putType("COUNT(COD_GRUPO)", QueryTool.INTEGER);
              qtAux.putWhereType("COD_ACCION", QueryTool.INTEGER);
              qtAux.putWhereValue("COD_ACCION", dtAccion.getString("COD_ACCION"));
              qtAux.putOperator("COD_ACCION", "=");
              qtAux.putWhereType("COD_APLICACION", QueryTool.STRING);
              qtAux.putWhereValue("COD_APLICACION",
                                  dtAccion.getString("COD_APLICACION"));
              qtAux.putOperator("COD_APLICACION", "=");
              v = new Lista();
              v.addElement(qtAux);
              this.getApp().getStub().setUrl(servlet);
              v = (Lista)this.getApp().getStub().doPost(1, v);
              if (v.size() > 0) {
                dt = (Data) v.elementAt(0);
                if ( (new Integer(dt.getString("COUNT(COD_GRUPO)"))).intValue() >
                    0) {
                  sMsg = "La acci�n tiene grupos vinculados";
                }
              }

              if (sMsg == null) {
                qt.putWhereType("COD_ACCION", QueryTool.INTEGER);
                qt.putWhereValue("COD_ACCION", dtAccion.getString("COD_ACCION"));
                qt.putOperator("COD_ACCION", "=");

                qt.putWhereType("COD_APLICACION", QueryTool.STRING);
                qt.putWhereValue("COD_APLICACION",
                                 dtAccion.getString("COD_APLICACION"));
                qt.putOperator("COD_APLICACION", "=");

                vParam.addElement(qt);
                this.getApp().getStub().setUrl(servlet);
                this.getApp().getStub().doPost(5, vParam);
                bAceptar = true;
                dispose();
              }
              else {
                getApp().showAdvise(sMsg);
              }

              // error en el servlet
            }
            catch (Exception ex) {
              this.getApp().trazaLog(ex);
              this.getApp().showError(ex.getMessage());
            }
            break;
        }
      }

      Inicializar(CInicializar.NORMAL);

    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  }

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

  // Devuelve los datos seleccionados en el di�logo.
  public Data getAccion() {
    return dtAccion;
  }
}

// botones de aceptar y cancelar.
class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DlgAcciones adaptee;
  ActionEvent e;

  DialogActionAdapter(DlgAcciones adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
