package usu2;

import capp2.CApp;
import capp2.CTabPanel;

public class AppUSU
    extends CApp {

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    CTabPanel tabPanel = new CTabPanel();
    tabPanel.InsertarPanel("Grupos de Trabajo", new PnlGrupos(this));
    tabPanel.InsertarPanel("Usuarios de la Aplicación", new PnlUsuarios(this));
    tabPanel.InsertarPanel("Acciones", new PnlAcciones(this));
    tabPanel.validate();
    tabPanel.VerPanel("Grupos de Trabajo");
    VerPanel("", tabPanel);
    setTitulo("Gestión de Perfiles, Usuarios y Acciones [" +
              getParameter("COD_APLICACION") + "]");
  }
}
