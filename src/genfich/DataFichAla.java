package genfich;

import java.io.Serializable;

public class DataFichAla
    implements Serializable {

  protected String sSep = "@"; //Caracter separador entre datos de una l�nea
  protected String sAnoDesde;
  protected String sAnoHasta;
  protected String sSemDesde;
  protected String sSemHasta;
  protected String sCodInd;
  protected String sCodEnf;

  public DataFichAla(String sep, String anoDesde, String anoHasta,
                     String semDesde, String semHasta, String codInd,
                     String codEnf) {

    sSep = sep;
    sAnoDesde = anoDesde;
    sAnoHasta = anoHasta;
    sSemDesde = semDesde;
    sSemHasta = semHasta;
    sCodInd = codInd;
    sCodEnf = codEnf;
  }

  //Cod util para replicar: datos obligatorios

  public String getAnoDesde() {
    return sAnoDesde;
  }

  public String getAnoHasta() {
    return sAnoHasta;
  }

  public String getSemDesde() {
    return sSemDesde;
  }

  public String getSemHasta() {
    return sSemHasta;
  }

  public String getSep() {
    return sSep;
  }

  public String getCodInd() {
    return sCodInd;
  }

  public String getCodEnf() {
    return sCodEnf;
  }

}
