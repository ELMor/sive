package genfich;

import java.io.Serializable;

public class DataCenNot
    implements Serializable {

  protected String sCodEquNot = "";
  protected String sCodAno = "";
  protected String sCodSem = "";
  protected String sNotTeo = "";
  protected String sNotRea = "";
  protected String sCodCenNot = "";
  protected String sCodNiv1 = "";
  protected String sCodNiv2 = "";
  protected String sCodZbs = "";
  protected String sCodPro = "";
  protected String sCodMun = "";

  public DataCenNot() {
  }

  public DataCenNot(String equNot, String codAno, String codSem, String notTeo,
                    String notRea) {

    sCodEquNot = equNot;
    sCodAno = codAno;
    sCodSem = codSem;
    sNotTeo = notTeo;
    sNotRea = notRea;

  }

  public void setCenNot(String cenNot, String codNiv1, String codNiv2,
                        String codZbs) {
    sCodCenNot = cenNot;
    sCodNiv1 = codNiv1;
    sCodNiv2 = codNiv2;
    sCodZbs = codZbs;
  }

  public void setProMun(String codPro, String codMun) {
    sCodPro = codPro;
    sCodMun = codMun;
  }

  // String codPro, String codMun

  //Cod util para replicar: datos obligatorios

  public String getCodEquNot() {
    return sCodEquNot;
  }

  public String getCodAno() {
    return sCodAno;
  }

  public String getCodSem() {
    return sCodSem;
  }

  public String getNotTeo() {
    return sNotTeo;
  }

  public String getNotRea() {
    return sNotRea;
  }

  public String getCodCenNot() {
    return sCodCenNot;
  }

  public String getCodNiv1() {
    return sCodNiv1;
  }

  public String getCodNiv2() {
    return sCodNiv2;
  }

  public String getCodZbs() {
    return sCodZbs;
  }

  public String getCodPro() {
    return sCodPro;
  }

  public String getCodMun() {
    return sCodMun;
  }

}
