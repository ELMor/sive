
package genfich;

import java.io.Serializable;

public class DataFich
    implements Serializable {

  protected String sSep = "@"; //Caracter separador entre datos de una l�nea
  protected String sAnoDesde;
  protected String sAnoHasta;
  protected String sSemDesde;
  protected String sSemHasta;

  public DataFich() {
  }

  public DataFich(String sep, String anoDesde, String anoHasta, String semDesde,
                  String semHasta) {

    sSep = sep;
    sAnoDesde = anoDesde;
    sAnoHasta = anoHasta;
    sSemDesde = semDesde;
    sSemHasta = semHasta;
  }

  //Cod util para replicar: datos obligatorios

  public String getAnoDesde() {
    return sAnoDesde;
  }

  public String getAnoHasta() {
    return sAnoHasta;
  }

  public String getSemDesde() {
    return sSemDesde;
  }

  public String getSemHasta() {
    return sSemHasta;
  }

  public String getSep() {
    return sSep;
  }
}
