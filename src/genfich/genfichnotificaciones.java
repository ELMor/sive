package genfich;

import capp.CApp;

public class genfichnotificaciones
    extends CApp {
  public genfichnotificaciones() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    setTitulo("Volcado de  Notificaciones ");
    CApp a = (CApp)this;
    // abrimos el panel con el modo oportuno
    VerPanel("", new PanelGenFich2(a, 5));
  }
}
