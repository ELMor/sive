package genfich;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;

import java.awt.Checkbox;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CFileName;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import sapp.StubSrvBD;
import utilidades.PanFechas;

public class PanelGenFich2
    extends CPanel {

  //ctes del panel

  //modos de consulta al servlet
  final int servletGEN_FICH_EDONUM = 3;
  final int servletGEN_FICH_C_NOTIF = 4;
  final int servletGEN_FICH_NOTIFICACIONES = 5;

  //rutas imagenes
  final String imgNAME[] = {
      "images/salvar.gif"};

  //modos de pantalla
  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  //Variables
  int modoOperacion;
  int modoServlet;

  protected CLista listaFich = new CLista();
  final String strSERVLET = "servlet/SrvGenFich";
  protected StubSrvBD stubCliente = new StubSrvBD();

  XYLayout xYLayout1 = new XYLayout();
  Label lblDesde = new Label();
  Label lblHasta = new Label();
  ButtonControl btnGenFic = new ButtonControl();
  PanFechas panIni;
  PanFechas panFin;
  CFileName panFile;

  PanelGenFich2ActionListener btnActionListener = new
      PanelGenFich2ActionListener(this);

  protected CCargadorImagen imgs = null;

  public PanelGenFich2(CApp a, int m) {
    try {
      setApp(a);
      modoServlet = m;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    panIni = new PanFechas(this, 1, false);
    panFin = new PanFechas(this, 2, false);
    panFile = new CFileName(this.app);

    this.setSize(new Dimension(569, 300));
    xYLayout1.setHeight(199);
    xYLayout1.setWidth(475);

    btnGenFic.addActionListener(btnActionListener);

    lblHasta.setText("Hasta:");
    btnGenFic.setLabel("Generar Fichero");
    lblDesde.setText("Desde:");
    this.setLayout(xYLayout1);

    this.add(lblDesde, new XYConstraints(26, 17, 51, -1));
    this.add(panIni, new XYConstraints(75, 15, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 56, 51, -1));
    this.add(panFin, new XYConstraints(75, 54, -1, -1));
    this.add(panFile, new XYConstraints(26, 95, -1, -1));
    this.add(btnGenFic, new XYConstraints(334, 150, -1, 26));

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnGenFic.setImage(imgs.getImage(0));

    //ponemos un 0 delante de la semana si es < 10
    if (panIni.txtCodSem.getText().length() == 1) {
      panIni.txtCodSem.setText("0" + panIni.txtCodSem.getText());
      panIni.sCodSemBk = "0" + panIni.sCodSemBk;
    }
    if (panFin.txtCodSem.getText().length() == 1) {
      panFin.txtCodSem.setText("0" + panFin.txtCodSem.getText());
      panFin.sCodSemBk = "0" + panFin.sCodSemBk;
    }

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        btnGenFic.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnGenFic.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
    doLayout();
  }

  void btnGenFic_actionPerformed(ActionEvent evt) {

    CMessage msgBox;
    DataFich datFich;
    CLista data;
    int j = 0;
    int numTok; //Num tokens quedadn por recorrer de una linea
    Checkbox chkbxElegido;
    //int modoServlet = -1;
    String sCodSemIniDefinitivo;
    String sCodSemFinDefinitivo;
    String sLinea;
    File miFichero = null;
    FileWriter fStream = null;

    try {

      panIni.txtAno.setText(panIni.txtAno.getText().trim());
      panIni.txtCodSem.setText(panIni.txtCodSem.getText().trim());
      panIni.txtFecSem.setText(panIni.txtFecSem.getText().trim());
      panFin.txtAno.setText(panFin.txtAno.getText().trim());
      panFin.txtCodSem.setText(panFin.txtCodSem.getText().trim());
      panFin.txtFecSem.setText(panFin.txtFecSem.getText().trim());

      if (modoServlet != -1) {

        if (isDataValid() == true) {

          this.modoOperacion = modoESPERA;
          Inicializar();

          data = new CLista();

          //Da formato a semana para que queden todas con 2 caracteres num�ricos
          //Pasando a entero se eliminan los ceros a la izqda y luego  si es necesario se a�ade un cero
          sCodSemIniDefinitivo = Integer.toString(Integer.parseInt(panIni.
              txtCodSem.getText()));
          if (sCodSemIniDefinitivo.length() == 1) {
            sCodSemIniDefinitivo = "0" + sCodSemIniDefinitivo;
          }
          sCodSemFinDefinitivo = Integer.toString(Integer.parseInt(panFin.
              txtCodSem.getText()));
          if (sCodSemFinDefinitivo.length() == 1) {
            sCodSemFinDefinitivo = "0" + sCodSemFinDefinitivo;

          }
          data.addElement(new DataFich("@", panIni.txtAno.getText(),
                                       panFin.txtAno.getText(),
                                       sCodSemIniDefinitivo,
                                       sCodSemFinDefinitivo));

          // apunta al servlet principal
          stubCliente.setUrl(new URL(app.getURL() + strSERVLET));

          // obtiene la lista
          listaFich = (CLista) stubCliente.doPost(modoServlet, data);

          /*         SrvGenFich servlet = new SrvGenFich();
               servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
               "jdbc:oracle:thin:@10.160.12.54:1521:ORCL",
                                             "pista",
                                             "loteb98");
               listaFich = (CLista) servlet.doDebug(this .modoServlet, data);
           */

          String sCab = new String();
          if (listaFich.size() > 0) {
            try {
              miFichero = new File(this.panFile.txtFile.getText());
              fStream = new FileWriter(miFichero);

              // Se escribe la cabecera, que esta al final del data
              sCab = (String) listaFich.elementAt(listaFich.size() - 1);
              fStream.write(sCab);
              fStream.write("\r");
              fStream.write("\n");

              for (j = 0; j < listaFich.size() - 1; j++) {
                sLinea = (String) listaFich.elementAt(j);
                fStream.write(sLinea);
                fStream.write("\r");
                fStream.write("\n");
              }

              fStream.close();
              fStream = null;
              miFichero = null;
              msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                    "Se ha generado el fichero ASCII");
              msgBox.show();
              msgBox = null;
            }
            catch (IOException ioEx) {
              ioEx.printStackTrace();
              msgBox = new CMessage(this.app, CMessage.msgERROR,
                  "Se han obtenido datos pero ha habido un error en la escritura del fichero");
              msgBox.show();
              msgBox = null;
            }

          } //if
          // fichero vacio
          else {
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                "No hay datos y por tanto no se genera el fichero ASCII");
            msgBox.show();
            msgBox = null;
          }
        } //if
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                "Datos no validados. ");
          msgBox.show();
          msgBox = null;
        }
      } //if
      else {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              "Falta dato de fichero ");
        msgBox.show();
        msgBox = null;
      }

      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }

    this.modoOperacion = modoNORMAL;
    Inicializar();

  }

  boolean isDataValid() {
    if (panIni.sCodSemBk.length() == 1) {
      panIni.sCodSemBk = "0" + panIni.sCodSemBk;
    }
    if (panFin.sCodSemBk.length() == 1) {
      panFin.sCodSemBk = "0" + panFin.sCodSemBk;

    }

    if (
        //Comprueba que los �ltimos datos "v�lidos" (las copias de seguridad) no son vac�os
        (!panIni.sAnoBk.equals("")) &&
        (!panIni.sCodSemBk.equals("")) &&
        (!panIni.sFecSemBk.equals("")) &&
        (!panFin.sAnoBk.equals("")) &&
        (!panFin.sCodSemBk.equals("")) &&
        (!panFin.sFecSemBk.equals("")) &&
        (!panFile.txtFile.getText().equals("")) &&
        //y comprueba que no se han cambiado los datos en cajas de texto respecto a datos v�lidos
        (panIni.txtAno.getText().trim().equals(panIni.sAnoBk)) &&
        (panIni.txtCodSem.getText().trim().equals(panIni.sCodSemBk)) &&
        (panIni.txtFecSem.getText().trim().equals(panIni.sFecSemBk)) &&
        (panFin.txtAno.getText().trim().equals(panFin.sAnoBk)) &&
        (panFin.txtCodSem.getText().trim().equals(panFin.sCodSemBk)) &&
        (panFin.txtFecSem.getText().trim().equals(panFin.sFecSemBk))

        ) {
      return true;
    }
    else {
      return false;
    }
  }
} //clase

// action listener para los botones
class PanelGenFich2ActionListener
    implements ActionListener, Runnable {
  PanelGenFich2 adaptee = null;
  ActionEvent e = null;

  public PanelGenFich2ActionListener(PanelGenFich2 adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.btnGenFic_actionPerformed(e);
  }
}
