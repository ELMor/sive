
package genfich;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Enumeration;

import capp.CLista;
import sapp.DBServlet;

public class SrvGenFich
    extends DBServlet {

  //modos de consulta al servlet
  final int servletGEN_FICH_PROCESOS = 2;
  final int servletGEN_FICH_EDONUM = 3;
  final int servletGEN_FICH_C_NOTIF = 4;
  final int servletGEN_FICH_NOTIFICACIONES = 5;

  final int servletGEN_FICH_ALARMAS = 6;

  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int k; //Para �ndice b�squeda columnas

    // objetos de datos
    CLista data = null; //lista de resultado al cliente (lineas, es decir, Strings)
    DataFich datFich = null; // datos entrada para generar fichero
    DataFichAla datFichAla = null; // datos entrada para generar fichero de alarmas
    String sLinea = null;
    Enumeration enum; //Para recorrer listas
    CLista dataAux = null; //lista de objetos de datos provisionales (depende de cada caso el tipo)

    //Par�metros recibidos para generar fichero
    String sSep = "@"; //Caracter separador entre datos de una l�nea
    String sAnoDesde;
    String sAnoHasta;
    String sSemDesde;
    String sSemHasta;

    //Parametros usados en edonum
    String sCodEnf = "";
    String sCodAno = "";
    String sCodSem = "";
    int iNumCas = 0;
    String sNumCas = "";
    String sCodNiv1 = "";
    String sCodNiv2 = "";
    String sCodZbs = "";
    String sCodPro = "";
    String sCodMun = "";

    //Parametros usados en centro notificador (Adem�s de algunos anbteriores)
    DataCenNot datCenNot = null;
    String sCodEquNot = "";
    int iNotTeo = 0;
    String sNotTeo = "";
    int iNotRea = 0;
    String sNotRea = "";
    String sCodCenNot = "";

    //Datos usados para notificaciones(Adem�s de algunos anbteriores)
    DataNotif datNotif = null;
    java.sql.Date dFecRec;
    String sFecRec = "";
    java.sql.Date dFecNot;
    String sFecNot = "";
    String sResSem = "";
    int iNumEdo = 0;
    String sNumEdo = "";

    //Datos usados para indicadores
    String sCodInd = "";
//   int iNumCoe ;
    float iNumCoe = (float) 0;
    String sNumCoe = "";
//   int iNumVal;
    float iNumVal = (float) 0;
    String sNumVal = "";

    //   String sCodEnf = "";
    String sDesPro = "";
    String sDesLPro = "";

    final String sLISTADO =
        "select CD_ENFCIE, DS_PROCESO, DSL_PROCESO  from SIVE_PROCESOS ";
    final String sLISTADO_EDONUM =
        "select   d.CD_ANOEPI, d.CD_SEMEPI, d.CD_ENFCIE, " +
        "d.NM_CASOS,  d.CD_NIVEL_1, d.CD_NIVEL_2, d.CD_ZBS , d.CD_PROV, d.CD_MUN from SIVE_RESUMEN_EDOS d " +
        " where ";

    final String sLISTADO_NOTIF_SEM =
        "select  d.CD_E_NOTIF, d.CD_ANOEPI, d.CD_SEMEPI, d.NM_NNOTIFT, " +
        "d.NM_NTOTREAL from SIVE_NOTIF_SEM d " +
        " where ";
    final String sCOND_NOTIF_SEM = " and ( d.CD_E_NOTIF in (select CD_E_NOTIF from SIVE_E_NOTIF where CD_CENTRO in ( select CD_CENTRO from SIVE_C_NOTIF  where IT_COBERTURA = 'S' ) ) )";

    final String sLISTADO_NOTIFEDO =
        "select  d.CD_E_NOTIF, d.CD_ANOEPI, d.CD_SEMEPI, d.FC_RECEP, " +
        "d.NM_NNOTIFR, d.FC_FECNOTIF, d.IT_RESSEM  from SIVE_NOTIFEDO d " +
        " where ";

    final String sLISTADO_EDONUM_AUX = "select  SUM(NM_CASOS) as TOT from SIVE_EDONUM  where  CD_E_NOTIF = ? and CD_ANOEPI = ?  and CD_SEMEPI = ?  and FC_RECEP = ?  and FC_FECNOTIF = ?";
    final String sLISTADO_NOTIF_EDOI = "select  COUNT(DISTINCT (NM_EDO)) from SIVE_NOTIF_EDOI where  CD_E_NOTIF = ? and CD_ANOEPI = ?  and CD_SEMEPI = ?  and FC_RECEP = ?  and FC_FECNOTIF = ? and CD_FUENTE = 'E' ";

    final String sLISTADO_ALARMAS = "select distinct a.NM_COEF, a.CD_ANOEPI, d.CD_SEMEPI, d.NM_VALOR from SIVE_IND_ANO a, SIVE_ALARMA d where ";
    final String sCOND_ALARMAS = " and (a.CD_ANOEPI = d.CD_ANOEPI) and (a.CD_INDALAR = d.CD_INDALAR) and (a.CD_INDALAR = ? )";

    final String sLISTADO_E_NOTIF = "select CD_CENTRO, CD_NIVEL_1, CD_NIVEL_2, CD_ZBS  from SIVE_E_NOTIF where CD_E_NOTIF = ? ";
    final String sLISTADO_C_NOTIF =
        "select CD_PROV, CD_MUN  from SIVE_C_NOTIF where CD_CENTRO = ? ";
    final String sEN_UN_ANO =
        " (d.CD_ANOEPI = ?) and (d.CD_SEMEPI >= ?) and (d.CD_SEMEPI <= ?)";
    final String sEN_VARIOS_ANOS = " (  ( d.CD_ANOEPI = ?  and d.CD_SEMEPI >= ? ) or ( d.CD_ANOEPI > ? and  d.CD_ANOEPI < ? ) or ( d.CD_ANOEPI = ?  and d.CD_SEMEPI <= ? )  )";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

//    datFich = (DataFich) param.firstElement();

    try {

      // modos de operaci�n
      switch (opmode) {

        // listado
        case servletGEN_FICH_PROCESOS:

          datFich = (DataFich) param.firstElement();

          st = con.prepareStatement(sLISTADO);
          // prepara la lista de resultados
          data = new CLista();

          sSep = datFich.getSep();

          // paginaci�n
          if (param.getFilter().length() > 0) {
            st.setString(3, param.getFilter().trim());
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            sCodEnf = rs.getString("CD_ENFCIE");
            sDesPro = rs.getString("DS_PROCESO");
            sDesLPro = rs.getString("DSL_PROCESO");

            sLinea = sCodEnf + sSep + sDesPro + sSep + sDesLPro;
            // a�ade un nodo
            data.addElement(sLinea);
            i++;
          }
          rs.close();
          st.close();
          rs = null;
          st = null;

          break;
//????????????????????????????????????????????????????????????????????
          // listado

        case servletGEN_FICH_EDONUM:

          datFich = (DataFich) param.firstElement();
          st = null;
          rs = null;

          sAnoDesde = datFich.getAnoDesde().trim();
          sAnoHasta = datFich.getAnoHasta().trim();
          sSemDesde = datFich.getSemDesde().trim();
          sSemHasta = datFich.getSemHasta().trim();

          // prepara la lista de resultados
          data = new CLista();

          // prepara la query
          //Si fechas se refieren la mismo a�o epid
          if (sAnoDesde.equals(sAnoHasta)) {
            st = con.prepareStatement(sLISTADO_EDONUM + sEN_UN_ANO);
            //Si a�o desde es menor que a�o hasta
          }
          else if (Integer.parseInt(sAnoDesde) < Integer.parseInt(sAnoHasta)) {
            st = con.prepareStatement(sLISTADO_EDONUM + sEN_VARIOS_ANOS);
            //Si a�o desde es mayor que a�o hasta se devolver� la lista vac�a
          }
          else {
            return data;
          }

          //Si fechas se refieren la mismo a�o epid
          if (sAnoDesde.equals(sAnoHasta)) {
            // c�digo a�o desde
            st.setString(1, sAnoDesde);
            // c�digo semana desde
            st.setString(2, sSemDesde);
            // c�digo semana hasta
            st.setString(3, sSemHasta);

          }
          //Si a�o desde es menor que a�o hasta
          else if (Integer.parseInt(sAnoDesde) < Integer.parseInt(sAnoHasta)) {
            // c�digo a�o desde
            st.setString(1, sAnoDesde);
            // c�digo semana desde
            st.setString(2, sSemDesde);

            // c�digo a�o desde
            st.setString(3, sAnoDesde);
            // c�digo a�o hasta
            st.setString(4, sAnoHasta);

            // c�digo a�o hasta
            st.setString(5, sAnoHasta);
            // c�digo sem hasta
            st.setString(6, sSemHasta);

          }

          sSep = datFich.getSep();

          rs = st.executeQuery();
          // extrae la p�gina requerida
          while (rs.next()) {

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // obtiene los campos
            sCodAno = rs.getString("CD_ANOEPI");
            sCodSem = rs.getString("CD_SEMEPI");
            sCodEnf = rs.getString("CD_ENFCIE");
            iNumCas = rs.getInt("NM_CASOS");
            sNumCas = Integer.toString(iNumCas);
            sCodNiv1 = rs.getString("CD_NIVEL_1");
            sCodNiv2 = rs.getString("CD_NIVEL_2");
            sCodZbs = rs.getString("CD_ZBS");
            sCodPro = rs.getString("CD_PROV");
            sCodMun = rs.getString("CD_MUN");

            sLinea = sCodEnf + sSep + sCodAno + sSep + sCodSem + sSep + sNumCas +
                sSep +
                sCodNiv1 + sSep + sCodNiv2 + sSep + sCodZbs + sSep +
                sCodPro + sSep + sCodMun;

            // a�ade un nodo
            data.addElement(sLinea);
            i++;
          }

          // Se construye la cabecera
          String strCabecera1 = "";
          strCabecera1 = "CD_ENFCIE" + sSep + "CD_ANOEPI" + sSep + "CD_SEMEPI" +
              sSep + "NM_CASOS" + sSep +
              "CD_NIVEL_1" + sSep + "CD_NIVEL_2" + sSep + "CD_ZBS" + sSep +
              "CD_PROV" + sSep + "CD_MUN";

          data.addElement(strCabecera1);

          rs.close();
          st.close();
          rs = null;
          st = null;

          break;

//????????????????????????????????????????????????????????????????????????
          // listado
        case servletGEN_FICH_C_NOTIF:

          datFich = (DataFich) param.firstElement();
          st = null;
          rs = null;

          sAnoDesde = datFich.getAnoDesde().trim();
          sAnoHasta = datFich.getAnoHasta().trim();
          sSemDesde = datFich.getSemDesde().trim();
          sSemHasta = datFich.getSemHasta().trim();

          // prepara la lista de resultados: ser� una lista de Strings lineas
          data = new CLista();

          //prepara la lista auxiliar
          dataAux = new CLista();

          //______________________ Primera consulta___________________________

          // prepara la query
          //Si fechas se refieren la mismo a�o epid
          if (sAnoDesde.equals(sAnoHasta)) {
            st = con.prepareStatement(sLISTADO_NOTIF_SEM + sEN_UN_ANO +
                                      sCOND_NOTIF_SEM);
            //Si a�o desde es menor que a�o hasta
          }
          else if (Integer.parseInt(sAnoDesde) < Integer.parseInt(sAnoHasta)) {
            st = con.prepareStatement(sLISTADO_NOTIF_SEM + sEN_VARIOS_ANOS +
                                      sCOND_NOTIF_SEM);
            //Si a�o desde es mayor que a�o hasta se devolver� la lista vac�a
          }
          else {
            return data;
          }

          //Si fechas se refieren la mismo a�o epid
          if (sAnoDesde.equals(sAnoHasta)) {
            // c�digo a�o desde
            st.setString(1, sAnoDesde);
            // c�digo semana desde
            st.setString(2, sSemDesde);
            // c�digo semana hasta
            st.setString(3, sSemHasta);

          }
          //Si a�o desde es menor que a�o hasta
          else if (Integer.parseInt(sAnoDesde) < Integer.parseInt(sAnoHasta)) {
            // c�digo a�o desde
            st.setString(1, sAnoDesde);
            // c�digo semana desde
            st.setString(2, sSemDesde);

            // c�digo a�o desde
            st.setString(3, sAnoDesde);
            // c�digo a�o hasta
            st.setString(4, sAnoHasta);

            // c�digo a�o hasta
            st.setString(5, sAnoHasta);
            // c�digo sem hasta
            st.setString(6, sSemHasta);

          }
          sSep = datFich.getSep();

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {

            // obtiene los campos

            //Tanto Strings obligatorios como no obligatorios se obtienen igual
            //En cliente se convertir�n los Strings nulos a cadenas""
            sCodEquNot = rs.getString("CD_E_NOTIF");
            sCodAno = rs.getString("CD_ANOEPI");
            sCodSem = rs.getString("CD_SEMEPI");
            iNotTeo = rs.getInt("NM_NNOTIFT");
            sNotTeo = Integer.toString(iNotTeo);
            iNotRea = rs.getInt("NM_NTOTREAL");
            sNotRea = Integer.toString(iNotRea);

            // a�ade un nodo
            dataAux.addElement(new DataCenNot(sCodEquNot, sCodAno, sCodSem,
                                              sNotTeo, sNotRea));
//# // System_out.println("a� elem" + sCodEquNot  );//*************************************
          }
          rs.close();
          st.close();
          rs = null;
          st = null;

          //______________________ Segunda consulta___________________________

//Busqueda de un vector de Strings obtenido de otra tabla distinta de la principa�

          enum = dataAux.elements();
          while (enum.hasMoreElements()) { //Recorremos lista
            datCenNot = (DataCenNot) (enum.nextElement());

            st = con.prepareStatement(sLISTADO_E_NOTIF);
//# // System_out.println(" prep listado   ????????????????????");  //*********************
            st.setString(1, datCenNot.getCodEquNot().trim());
            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {
              // obtiene los campos
              sCodCenNot = rs.getString("CD_CENTRO");
              sCodNiv1 = rs.getString("CD_NIVEL_1");
              sCodNiv2 = rs.getString("CD_NIVEL_2");
              sCodZbs = rs.getString("CD_ZBS");

              //# // System_out.println("a� vector" + datCenNot.getCodEquNot());//*************************************
            }
            datCenNot.setCenNot(sCodCenNot, sCodNiv1, sCodNiv2, sCodZbs);

            rs.close();
            st.close();
            st = null;
            rs = null;
          }

//# // System_out.println("Fin busqueda vector   ?????????????");  //*******************

           //______________________ Tercera consulta___________________________

//Busqueda de un vector de Strings obtenido de otra tabla distinta de la principa�

           enum = dataAux.elements();
          while (enum.hasMoreElements()) { //Recorremos lista
            datCenNot = (DataCenNot) (enum.nextElement());

            st = con.prepareStatement(sLISTADO_C_NOTIF);
//# // System_out.println(" prep listado   ????????????????????");  //*********************
            st.setString(1, datCenNot.getCodEquNot().trim());
            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {
              // obtiene los campos
              sCodPro = rs.getString("CD_PROV");
              sCodMun = rs.getString("CD_MUN");

              //# // System_out.println("a� vector" + datCenNot.getCodCenNot());//*************************************
            }
            datCenNot.setProMun(sCodPro, sCodMun);

            rs.close();
            st.close();
            st = null;
            rs = null;
          }

//# // System_out.println("Fin busqueda vector   ?????????????");  //*******************

           //______________________ A�ade las lineas___________________________

           enum = dataAux.elements();
          while (enum.hasMoreElements()) {
            datCenNot = (DataCenNot) enum.nextElement();

            sLinea =
                datCenNot.getCodNiv1() + sSep + datCenNot.getCodNiv2() + sSep
                + datCenNot.getCodZbs() + sSep
                + datCenNot.getCodPro() + sSep + datCenNot.getCodMun() + sSep
                + datCenNot.getCodCenNot() + sSep + datCenNot.getCodEquNot() +
                sSep
                + datCenNot.getCodAno() + sSep + datCenNot.getCodSem() + sSep
                + datCenNot.getNotTeo() + sSep + datCenNot.getNotRea();

            // a�ade un nodo
            data.addElement(sLinea);
          }

          // a�ado las cabeceras
          String strCabecera2 = "";
          strCabecera2 = "CD_NIVEL_1" + sSep + "CD_NIVEL_2" + sSep + "CD_ZBS" +
              sSep + "CD_PROV" + sSep +
              "CD_MUN" + sSep + "CD_CENTRO" + sSep + "CD_E_NOTIF" + sSep +
              "CD_ANOEPI" + sSep +
              "CD_SEMEPI" + sSep + "NM_NNOTIFT" + sSep + "NM_NTOTREAL";

          data.addElement(strCabecera2);

          break;

//?????????????????????????????????????????????????????????????????????????
//????????????????????????????????????????????????????????????????????????
          // listado
        case servletGEN_FICH_NOTIFICACIONES:

          datFich = (DataFich) param.firstElement();
          st = null;
          rs = null;

          sAnoDesde = datFich.getAnoDesde().trim();
          sAnoHasta = datFich.getAnoHasta().trim();
          sSemDesde = datFich.getSemDesde().trim();
          sSemHasta = datFich.getSemHasta().trim();

          // prepara la lista de resultados: ser� una lista de Strings lineas
          data = new CLista();

          //prepara la lista auxiliar
          dataAux = new CLista();

          //______________________ Primera consulta___________________________

          // prepara la query
          //Si fechas se refieren la mismo a�o epid
          if (sAnoDesde.equals(sAnoHasta)) {
            st = con.prepareStatement(sLISTADO_NOTIFEDO + sEN_UN_ANO);
            //Si a�o desde es menor que a�o hasta
          }
          else if (Integer.parseInt(sAnoDesde) < Integer.parseInt(sAnoHasta)) {
            st = con.prepareStatement(sLISTADO_NOTIFEDO + sEN_VARIOS_ANOS);
            //Si a�o desde es mayor que a�o hasta se devolver� la lista vac�a
          }
          else {
            return data;
          }

          //Si fechas se refieren la mismo a�o epid
          if (sAnoDesde.equals(sAnoHasta)) {
            // c�digo a�o desde
            st.setString(1, sAnoDesde);
            // c�digo semana desde
            st.setString(2, sSemDesde);
            // c�digo semana hasta
            st.setString(3, sSemHasta);

          }
          //Si a�o desde es menor que a�o hasta
          else if (Integer.parseInt(sAnoDesde) < Integer.parseInt(sAnoHasta)) {
            // c�digo a�o desde
            st.setString(1, sAnoDesde);
            // c�digo semana desde
            st.setString(2, sSemDesde);

            // c�digo a�o desde
            st.setString(3, sAnoDesde);
            // c�digo a�o hasta
            st.setString(4, sAnoHasta);

            // c�digo a�o hasta
            st.setString(5, sAnoHasta);
            // c�digo sem hasta
            st.setString(6, sSemHasta);

          }
          sSep = datFich.getSep();

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {

            // obtiene los campos

            //Tanto Strings obligatorios como no obligatorios se obtienen igual
            //En cliente se convertir�n los Strings nulos a cadenas""
            sCodEquNot = rs.getString("CD_E_NOTIF");
            sCodAno = rs.getString("CD_ANOEPI");
            sCodSem = rs.getString("CD_SEMEPI");
            dFecRec = rs.getDate("FC_RECEP");
            iNotRea = rs.getInt("NM_NNOTIFR");
            dFecNot = rs.getDate("FC_FECNOTIF");
            sResSem = rs.getString("IT_RESSEM");

            if (sResSem.equals("N")) {
              iNotRea = 0;

              // a�ade un nodo
            }
            dataAux.addElement(new DataNotif(sCodEquNot, sCodAno, sCodSem,
                                             dFecRec,
                                             iNotRea, dFecNot, sResSem));
//# // System_out.println("a� elem" + sCodEquNot  );//*************************************
          }
          rs.close();
          st.close();
          rs = null;
          st = null;

          //______________________ Segunda consulta___________________________

          enum = dataAux.elements();
          while (enum.hasMoreElements()) { //Recorremos lista
            datNotif = (DataNotif) (enum.nextElement());

            st = con.prepareStatement(sLISTADO_E_NOTIF);
//# // System_out.println(" prep listado   ????????????????????");  //*********************
            st.setString(1, datNotif.getCodEquNot().trim());
            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {
              // obtiene los campos
              sCodCenNot = rs.getString("CD_CENTRO");
              sCodNiv1 = rs.getString("CD_NIVEL_1");
              sCodNiv2 = rs.getString("CD_NIVEL_2");
              sCodZbs = rs.getString("CD_ZBS");

              //# // System_out.println("a� vector" + datNotif.getCodEquNot());//*************************************
            }
            datNotif.setCenNot(sCodCenNot, sCodNiv1, sCodNiv2, sCodZbs);

            rs.close();
            st.close();
            st = null;
            rs = null;
          }

//# // System_out.println("Fin busqueda vector   ?????????????");  //*******************

           //______________________ Tercera consulta___________________________

//Busqueda de un vector de Strings obtenido de otra tabla distinta de la principa�

           enum = dataAux.elements();
          while (enum.hasMoreElements()) { //Recorremos lista
            datNotif = (DataNotif) (enum.nextElement());

            st = con.prepareStatement(sLISTADO_C_NOTIF);
//# // System_out.println(" prep listado   ????????????????????");  //*********************
            st.setString(1, datNotif.getCodEquNot().trim());
            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {
              // obtiene los campos
              sCodPro = rs.getString("CD_PROV");
              sCodMun = rs.getString("CD_MUN");

              //# // System_out.println("a� vector" + datNotif.getCodCenNot());//*************************************
            }
            datNotif.setProMun(sCodPro, sCodMun);

            rs.close();
            st.close();
            st = null;
            rs = null;
          }

//# // System_out.println("Fin busqueda vector   ?????????????");  //*******************

           //______________________ Cuarta consulta___________________________

           enum = dataAux.elements();
          while (enum.hasMoreElements()) { //Recorremos lista
            datNotif = (DataNotif) (enum.nextElement());

            st = con.prepareStatement(sLISTADO_EDONUM_AUX);
//# // System_out.println(" prep listado   ????????????????????");  //*********************
            st.setString(1, datNotif.getCodEquNot().trim());
            st.setString(2, datNotif.getCodAno().trim());
            st.setString(3, datNotif.getCodSem().trim());
            st.setDate(4, datNotif.getFecRec());
            st.setDate(5, datNotif.getFecNot());
            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {
              // obtiene los campos
              iNumCas = rs.getInt("TOT");
              datNotif.setNumCas(iNumCas);
            }

            rs.close();
            st.close();
            st = null;
            rs = null;
          }

//# // System_out.println("Cuarta consulta************");

          //______________________ Quinta consulta___________________________

          enum = dataAux.elements();
          while (enum.hasMoreElements()) { //Recorremos lista
            datNotif = (DataNotif) (enum.nextElement());

            st = con.prepareStatement(sLISTADO_NOTIF_EDOI);
//# // System_out.println(" prep listado edoi  ????????????????????");  //*********************
            st.setString(1, datNotif.getCodEquNot().trim());
            st.setString(2, datNotif.getCodAno().trim());
            st.setString(3, datNotif.getCodSem().trim());
            st.setDate(4, datNotif.getFecRec());
            st.setDate(5, datNotif.getFecNot());
            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {
              // obtiene los campos
//# // System_out.println(" Antes NumEdo Ind con 1***********");
              iNumEdo = rs.getInt(1);
//# // System_out.println(" Despues NumEdo Ind***********");
              datNotif.setNumEdo(iNumEdo);
            }

            rs.close();
            st.close();
            st = null;
            rs = null;
          }

          //______________________ A�ade las lineas___________________________

          enum = dataAux.elements();
          while (enum.hasMoreElements()) {
            datNotif = (DataNotif) enum.nextElement();

            dFecRec = datNotif.getFecRec();
            sFecRec = formater.format(dFecRec);
            sNotRea = Integer.toString(datNotif.getNotRea());
            sNumCas = Integer.toString(datNotif.getNumCas());
            sNumEdo = Integer.toString(datNotif.getNumEdo());
            sLinea =

                datNotif.getCodNiv1() + sSep + datNotif.getCodNiv2() + sSep
                + datNotif.getCodZbs() + sSep
                + datNotif.getCodPro() + sSep + datNotif.getCodMun() + sSep
                + datNotif.getCodCenNot() + sSep + datNotif.getCodEquNot() +
                sSep
                + datNotif.getCodAno() + sSep + datNotif.getCodSem() + sSep
                + sFecRec + sSep + datNotif.getResSem() + sSep +
                datNotif.getNotRea() + sSep
                + sNumCas + sSep + sNumEdo;

            // a�ade un nodo
            data.addElement(sLinea);
          }

          // se a�aden las cabeceras
          String strCabecera3 = "";
          strCabecera3 = "CD_NIVEL_1" + sSep + "CD_NIVEL_2" + sSep + "CD_ZBS" +
              sSep + "CD_PROV"
              + sSep + "CD_MUN" + sSep + "CD_CENTRO" + sSep + "CD_E_NOTIF"
              + sSep + "CD_ANOEPI" + sSep + "CD_SEMEPI" + sSep + "FC_RECEP"
              + sSep + "IT_RESSEM" + sSep + "NM_NNOTIFR" + sSep + "NM_CASOSN"
              + sSep + "NM_CASOSI";

          data.addElement(strCabecera3);

          break;

//////////////
//????????????????????????????????????????????????????????????????????
          // listado

        case servletGEN_FICH_ALARMAS:

//# // System_out.println("Entra en genfichAl**************");
          datFichAla = (DataFichAla) param.firstElement();
          st = null;
          rs = null;

          sAnoDesde = datFichAla.getAnoDesde().trim();
          sAnoHasta = datFichAla.getAnoHasta().trim();
          sSemDesde = datFichAla.getSemDesde().trim();
          sSemHasta = datFichAla.getSemHasta().trim();
          sCodInd = datFichAla.getCodInd().trim();
          sCodEnf = datFichAla.getCodEnf().trim();
          sSep = datFichAla.getSep();

//# // System_out.println("parametros ajustados**************");

          // prepara la lista de resultados
          data = new CLista();

          // prepara la query
          //Si fechas se refieren la mismo a�o epid
          if (sAnoDesde.equals(sAnoHasta)) {
            st = con.prepareStatement(sLISTADO_ALARMAS + sEN_UN_ANO +
                                      sCOND_ALARMAS);
            //Si a�o desde es menor que a�o hasta
          }
          else if (Integer.parseInt(sAnoDesde) < Integer.parseInt(sAnoHasta)) {
            st = con.prepareStatement(sLISTADO_ALARMAS + sEN_VARIOS_ANOS +
                                      sCOND_ALARMAS);
            //Si a�o desde es mayor que a�o hasta se devolver� la lista vac�a
          }
          else {
            return data;
          }

//# // System_out.println("Querys prep  **************");
          //Si fechas se refieren la mismo a�o epid
          if (sAnoDesde.equals(sAnoHasta)) {
            // c�digo a�o desde
            st.setString(1, sAnoDesde);
            // c�digo semana desde
            st.setString(2, sSemDesde);
            // c�digo semana hasta
            st.setString(3, sSemHasta);
            // c�digo indicador
            st.setString(4, sCodInd);

//# // System_out.println("Mismo a�o **************");
          }
          //Si a�o desde es menor que a�o hasta
          else if (Integer.parseInt(sAnoDesde) < Integer.parseInt(sAnoHasta)) {
            // c�digo a�o desde
            st.setString(1, sAnoDesde);
            // c�digo semana desde
            st.setString(2, sSemDesde);

            // c�digo a�o desde
            st.setString(3, sAnoDesde);
            // c�digo a�o hasta
            st.setString(4, sAnoHasta);

            // c�digo a�o hasta
            st.setString(5, sAnoHasta);
            // c�digo sem hasta
            st.setString(6, sSemHasta);

            // c�digo indicador
            st.setString(7, sCodInd);

          }

//# // System_out.println("Distinytos a�os **************");

          rs = st.executeQuery();

          // extrae la p�gina requerida
//# // System_out.println("Ejectutada **************");
          while (rs.next()) {

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // obtiene los campos
            iNumCoe = rs.getFloat("NM_COEF");
            sNumCoe = Float.toString(iNumCoe);
            //sNumCoe = Integer.toString(iNumCoe);
            sCodAno = rs.getString("CD_ANOEPI");
            sCodSem = rs.getString("CD_SEMEPI");
            //iNumVal = rs.getInt("NM_VALOR");
            iNumVal = rs.getFloat("NM_VALOR");
            //sNumVal = Integer.toString(iNumVal);
            sNumVal = Float.toString(iNumVal);
////# // System_out.println("Campos obtenidos **************");
            sLinea = sCodInd + sSep + sNumCoe + sSep + sCodAno + sSep
                + sCodSem + sSep + sCodEnf + sSep + sNumVal + sSep;

            // a�ade un nodo
            data.addElement(sLinea);
            i++;
          }

//# // System_out.println("Fin while **************");
          rs.close();
          st.close();
          rs = null;
          st = null;

          // cabecera
          String strCabecera6 = "";
          strCabecera6 = "CD_INDALAR" + sSep + "NM_COEF" + sSep + "CD_ANOEPI" +
              sSep +
              "CD_SEMEPI" + sSep + "CD_ENFCIE" + sSep + "NM_VALOR";

          data.addElement(strCabecera6);

          break;

//????????????????????????????????????????????????????????????????????????

      } //fin switch

      // valida la transacci�n
      con.commit();

      //fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      throw ex;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }
}