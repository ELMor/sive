package genfich;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import alarmas.DataInd;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CFileName;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import sapp.StubSrvBD;
import utilidades.PanFechas;

public class PanelGenFichAlar
    extends CPanel {

  //modos de consulta al  servlet de ficheros
  final int servletGEN_FICH_ALARMAS = 6;

  //modos de consulta al  servlet de alarmas
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  //rutas imagenes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/salvar.gif"};

  //modos de pantalla
  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  //Variables
  int modoOperacion;
  int modoServlet = servletGEN_FICH_ALARMAS;

  protected CLista listaFich = new CLista();

  //Rutas de los servlet
  final String strSERVLET = "servlet/SrvGenFich";
  final String strSERVLET_IND = "servlet/SrvInd";

  protected StubSrvBD stubCliente = new StubSrvBD();

  XYLayout xYLayout1 = new XYLayout();
  Label lblDesde = new Label();
  Label lblHasta = new Label();
  ButtonControl btnGenFic = new ButtonControl();
  PanFechas panIni;
  PanFechas panFin;
  CFileName panFile;

  String sCodEnf = "";

  PanelGenFichAlarActionListener btnActionListener = new
      PanelGenFichAlarActionListener(this);
  PanelGenFichAlarTxtCodIndFocusAdapter txtCodIndFocusAdapter = new
      PanelGenFichAlarTxtCodIndFocusAdapter(this);

  protected CCargadorImagen imgs = null;
  ButtonControl btnSearchInd = new ButtonControl();
  Label lblIndicador = new Label();
  CCampoCodigo txtCodInd = new CCampoCodigo(); /*E*/
  TextField txtDesInd = new TextField();

  public PanelGenFichAlar(CApp a) {
    try {
      setApp(a);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    panIni = new PanFechas(this, 1, false);
    panFin = new PanFechas(this, 2, false);
    panFile = new CFileName(this.app);

    this.setSize(new Dimension(569, 300));
    xYLayout1.setHeight(263);
    xYLayout1.setWidth(475);

    btnGenFic.setActionCommand("GenFic");
    btnSearchInd.setActionCommand("BuscarInd");

    btnGenFic.addActionListener(btnActionListener);
    btnSearchInd.addActionListener(btnActionListener);
    txtCodInd.addFocusListener(txtCodIndFocusAdapter);

    lblHasta.setText("Hasta:");
    btnGenFic.setLabel("Generar Fichero");
    lblDesde.setText("Desde:");
    this.setLayout(xYLayout1);

    this.add(lblDesde, new XYConstraints(26, 17, 51, -1));
    this.add(panIni, new XYConstraints(75, 15, -1, -1));
    this.add(lblHasta, new XYConstraints(25, 63, 51, -1));
    this.add(panFin, new XYConstraints(73, 59, -1, -1));
    this.add(panFile, new XYConstraints(26, 112, -1, -1));
    //this.add(btnGenFic, new XYConstraints(338, 212, -1, 26));
    //this.add(btnSearchInd, new XYConstraints(244, 170, -1, -1));
    this.add(lblIndicador, new XYConstraints(26, 172, 78, -1));
    this.add(txtCodInd, new XYConstraints(108, 172, 129, -1));
    this.add(btnSearchInd, new XYConstraints(244, 170, -1, -1));
    this.add(txtDesInd, new XYConstraints(280, 172, 161, -1));
    this.add(btnGenFic, new XYConstraints(338, 212, -1, 26));

    txtCodInd.setBackground(new Color(255, 255, 150));

    txtCodInd.addKeyListener(new PanelGenFichAlar_txtCodInd_keyAdapter(this));
    lblIndicador.setText("Indicador:");

    //Carga im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnSearchInd.setImage(imgs.getImage(0));
    btnGenFic.setImage(imgs.getImage(1));

    //ponemos un 0 delante de la semana si es < 10
    if (panIni.txtCodSem.getText().length() == 1) {
      panIni.txtCodSem.setText("0" + panIni.txtCodSem.getText());
      panIni.sCodSemBk = "0" + panIni.sCodSemBk;
    }
    if (panFin.txtCodSem.getText().length() == 1) {
      panFin.txtCodSem.setText("0" + panFin.txtCodSem.getText());
      panFin.sCodSemBk = "0" + panFin.sCodSemBk;
    }

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        btnGenFic.setEnabled(true);
        txtCodInd.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnGenFic.setEnabled(false);
        txtCodInd.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
    doLayout();
  }

  void btnGenFic_actionPerformed(ActionEvent evt) {

    CMessage msgBox;
    DataFichAla datFich;
    CLista data;
    int j = 0;
    int numTok; //Num tokens quedadn por recorrer de una linea
    Checkbox chkbxElegido;
    //int modoServlet = -1;
    String sCodSemIniDefinitivo;
    String sCodSemFinDefinitivo;
    String sLinea;
    File miFichero = null;
    FileWriter fStream = null;

    try {

      panIni.txtAno.setText(panIni.txtAno.getText().trim());
      panIni.txtCodSem.setText(panIni.txtCodSem.getText().trim());
      panIni.txtFecSem.setText(panIni.txtFecSem.getText().trim());
      panFin.txtAno.setText(panFin.txtAno.getText().trim());
      panFin.txtCodSem.setText(panFin.txtCodSem.getText().trim());
      panFin.txtFecSem.setText(panFin.txtFecSem.getText().trim());

      if (modoServlet != -1) {

        if (isDataValid() == true) {

          this.modoOperacion = modoESPERA;
          Inicializar();

          data = new CLista();

          //Da formato a semana para que queden todas con 2 caracteres num�ricos
          //Pasando a entero se eliminan los ceros a la izqda y luego  si es necesario se a�ade un cero
          sCodSemIniDefinitivo = Integer.toString(Integer.parseInt(panIni.
              txtCodSem.getText()));
          if (sCodSemIniDefinitivo.length() == 1) {
            sCodSemIniDefinitivo = "0" + sCodSemIniDefinitivo;
          }
          sCodSemFinDefinitivo = Integer.toString(Integer.parseInt(panFin.
              txtCodSem.getText()));
          if (sCodSemFinDefinitivo.length() == 1) {
            sCodSemFinDefinitivo = "0" + sCodSemFinDefinitivo;

          }
          data.addElement(new DataFichAla("@", panIni.txtAno.getText(),
                                          panFin.txtAno.getText(),
                                          sCodSemIniDefinitivo,
                                          sCodSemFinDefinitivo,
                                          txtCodInd.getText(), sCodEnf));

          // apunta al servlet principal
          stubCliente.setUrl(new URL(app.getURL() + strSERVLET));

          //# // System_out.println("URL Apuntado*****************");

          // obtiene la lista
          listaFich = (CLista) stubCliente.doPost(servletGEN_FICH_ALARMAS, data);

          /*         SrvGenFich servlet = new SrvGenFich();
               servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
               "jdbc:oracle:thin:@10.160.12.54:1521:ORCL",
                                             "pista",
                                             "loteb98");
               listaFich = (CLista) servlet.doDebug(this .servletGEN_FICH_ALARMAS, data);
           */
          //# // System_out.println("do Post*****************");

          String sCab = new String();
          if (listaFich.size() > 0) {
            try {
              miFichero = new File(this.panFile.txtFile.getText());
              fStream = new FileWriter(miFichero);

              // Se escribe la cabecera, que esta al final del data
              sCab = (String) listaFich.elementAt(listaFich.size() - 1);
              fStream.write(sCab);
              fStream.write("\r");
              fStream.write("\n");

              for (j = 0; j < listaFich.size() - 1; j++) {
                sLinea = (String) listaFich.elementAt(j);
                fStream.write(sLinea);
                fStream.write("\r");
                fStream.write("\n");
              }

              fStream.close();
              fStream = null;
              miFichero = null;
              msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                    "Se ha generado el fichero ASCII");
              msgBox.show();
              msgBox = null;
            }
            catch (IOException ioEx) {
              ioEx.printStackTrace();
              msgBox = new CMessage(this.app, CMessage.msgERROR,
                  "Se han obtenido datos pero ha habido un error en la escritura del fichero");
              msgBox.show();
              msgBox = null;
            }

          } //if
          // fichero vacio
          else {
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                "No hay datos y por tanto no se genera el fichero ASCII");
            msgBox.show();
            msgBox = null;
          }
        } //if
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                "Datos no validados. ");
          msgBox.show();
          msgBox = null;
        }
      } //if
      else {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              "Falta dato de fichero ");
        msgBox.show();
        msgBox = null;
      }

      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }

    this.modoOperacion = modoNORMAL;
    Inicializar();

  }

  boolean isDataValid() {
    if (panIni.sCodSemBk.length() == 1) {
      panIni.sCodSemBk = "0" + panIni.sCodSemBk;
    }
    if (panFin.sCodSemBk.length() == 1) {
      panFin.sCodSemBk = "0" + panFin.sCodSemBk;

    }
    if (
        //Comprueba que los �ltimos datos "v�lidos" (las copias de seguridad) no son vac�os
        (!panIni.sAnoBk.equals("")) &&
        (!panIni.sCodSemBk.equals("")) &&
        (!panIni.sFecSemBk.equals("")) &&
        (!panFin.sAnoBk.equals("")) &&
        (!panFin.sCodSemBk.equals("")) &&
        (!panFin.sFecSemBk.equals("")) &&
        (!panFile.txtFile.getText().equals("")) &&
        //(! txtDesInd.getText().equals("")) &&   //Si descripci�n no es vac�a es que hay c�digo correcto
        //y comprueba que no se han cambiado los datos en cajas de texto respecto a datos v�lidos
        (panIni.txtAno.getText().trim().equals(panIni.sAnoBk)) &&
        (panIni.txtCodSem.getText().trim().equals(panIni.sCodSemBk)) &&
        (panIni.txtFecSem.getText().trim().equals(panIni.sFecSemBk)) &&
        (panFin.txtAno.getText().trim().equals(panFin.sAnoBk)) &&
        (panFin.txtCodSem.getText().trim().equals(panFin.sCodSemBk)) &&
        (panFin.txtFecSem.getText().trim().equals(panFin.sFecSemBk))

        ) {
      return true;
    }
    else {
      return false;
    }
  }

  public void btnBuscarInd_actionPerformed(ActionEvent evt) {

    CMessage msgBox = null;
    DataInd data;
//    this.modoOperacionBk = this.modoOperacion;

    try {
      // apunta al servlet auxiliar
      stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_IND));
      this.modoOperacion = modoESPERA;
      Inicializar();

      CListaIndicador lista = new CListaIndicador(app,
          "Indicadores para las Alarmas",
          stubCliente,
          strSERVLET_IND,
          servletOBTENER_X_CODIGO,
          servletOBTENER_X_DESCRIPCION,
          servletSELECCION_X_CODIGO,
          servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataInd) lista.getComponente();

      //he traido datos
      if (data != null) {
        txtCodInd.setText(data.getCodInd());
        txtDesInd.setText(data.getDesInd());
        sCodEnf = data.getCodEnf();
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  void txtCodInd_keyPressed(KeyEvent e) {
    txtDesInd.setText("");
  }

  void txtCodInd_focusLost() {

    // datos de envio
    DataInd data;
    CLista param = null;
    CMessage msg;

    if (txtCodInd.getText().length() > 0) {

      // consulta en modo espera
      modoOperacion = modoESPERA;
      Inicializar();

      try {
        param = new CLista();
        // JRM: A�ade tipo de aplicaci�n SIVE para que funcione alarmas.SrvInd
        param.setTSive(getApp().getTSive());
        param.setPerfil(getApp().getPerfil());
        param.setLogin(getApp().getLogin());
        param.addElement(new DataInd(txtCodInd.getText()));

        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_IND));

        param = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO, param);

        /*
                 alarmas.SrvInd srv = new alarmas.SrvInd();
                 srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                               "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                               "dba_edo",
                               "manager");
                 param = srv.doDebug(servletOBTENER_X_CODIGO, param);
         */

        // rellena los datos
        if (param.size() > 0) {
          data = (DataInd) param.elementAt(0);

          txtCodInd.setText(data.getCodInd());
          txtDesInd.setText(data.getDesInd());
          sCodEnf = data.getCodEnf();

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO, "No hay datos.");
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
    // modo normal
    modoOperacion = modoNORMAL;
    Inicializar();

  }

} //clase

// action listener para los botones
class PanelGenFichAlarActionListener
    implements ActionListener, Runnable {
  PanelGenFichAlar adaptee = null;
  ActionEvent e = null;

  public PanelGenFichAlarActionListener(PanelGenFichAlar adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("GenFic")) { // generar fichero
      adaptee.btnGenFic_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("BuscarInd")) { // indicador alarma
      adaptee.btnBuscarInd_actionPerformed(e);

    }

  }
}

// lista de valores: de la Pantalla
class CListaIndicador
    extends CListaValores {

  public CListaIndicador(CApp a,
                         String title,
                         StubSrvBD stub,
                         String servlet,
                         int obtener_x_codigo,
                         int obtener_x_descricpcion,
                         int seleccion_x_codigo,
                         int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataInd(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataInd) o).getCodInd());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataInd) o).getDesInd());
  }
}

//************************************************
 class PanelGenFichAlarTxtCodIndFocusAdapter
     extends java.awt.event.FocusAdapter
     implements Runnable {
   PanelGenFichAlar adaptee;

   PanelGenFichAlarTxtCodIndFocusAdapter(PanelGenFichAlar adaptee) {
     this.adaptee = adaptee;
   }

   public void focusLost(FocusEvent e) {
     Thread th = new Thread(this);
     th.start();
   }

   public void run() {

     adaptee.txtCodInd_focusLost();

   }
 }

class PanelGenFichAlar_txtCodInd_keyAdapter
    extends java.awt.event.KeyAdapter {
  PanelGenFichAlar adaptee;

  PanelGenFichAlar_txtCodInd_keyAdapter(PanelGenFichAlar adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCodInd_keyPressed(e);
  }
}
