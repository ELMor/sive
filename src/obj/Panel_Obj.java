package obj;

import java.awt.Button;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.FocusEvent;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CPanel;

public class Panel_Obj
    extends CPanel {
  XYLayout xYLayout1 = new XYLayout();
  Label label1 = new Label();
  TextField textField1 = new TextField();
  CFechaSimple Cfecha = new CFechaSimple("S");
  Button button1 = new Button();
  CHora Chora = new CHora("N");

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

  }

  // contructor
  public Panel_Obj(CApp a) {
    try {
      setApp(a);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //Component initialization
  private void jbInit() throws Exception {
    this.setSize(new Dimension(400, 279));
    label1.setText("Entrada por codigo a la caja de fecha (lostFocus)");
    button1.setLabel("Toma el foco");
    Chora.setText("99:99");

    textField1.addFocusListener(new java.awt.event.FocusAdapter() {
      public void focusLost(FocusEvent e) {
        textField1_focusLost(e);
      }
    });
    Cfecha.addFocusListener(new java.awt.event.FocusAdapter() {
      public void focusLost(FocusEvent e) {
        Cfecha_focusLost(e);
      }
    });

    Chora.addFocusListener(new java.awt.event.FocusAdapter() {
      public void focusLost(FocusEvent e) {
        Chora_focusLost(e);
      }
    });
    this.setLayout(xYLayout1);

    // establece el modo de operaci�n
    Inicializar();
    this.doLayout();
    this.add(label1, new XYConstraints(15, 61, 268, 22));
    this.add(textField1, new XYConstraints(302, 59, 87, 27));
    this.add(Cfecha, new XYConstraints(150, 110, 98, 27));
    this.add(button1, new XYConstraints(148, 150, 106, 26));
    this.add(Chora, new XYConstraints(19, 211, 40, 24));
  }

  //PARA LUIS RIVERA ------------------------
  void Cfecha_focusLost(FocusEvent e) {
    Cfecha.ValidarFecha(); //validamos el dato
    if (Cfecha.getValid().equals("N")) {
      //opcional vaciarlo
      Cfecha.setText(Cfecha.getFecha());
      //opcional si obligatorio
      //Cfecha.selectAll();
      //Cfecha.requestFocus();
    }
    else if (Cfecha.getValid().equals("S")) {
      Cfecha.setText(Cfecha.getFecha());
      //CODIGO LUIS
    }
  }

  //EQUIVALENTE A CUANDO SE INTRODUCE POR CODIGO
  void textField1_focusLost(FocusEvent e) {
    Cfecha.setText(textField1.getText()); //cargamos la caja
    Cfecha.ValidarFecha(); //validamos el dato

    if (Cfecha.getValid().equals("S")) {
      Cfecha.setText(Cfecha.getFecha());
    }
    if (Cfecha.getValid().equals("N")) {
      Cfecha.setText(Cfecha.getFecha());
      //opcional si obligatorio
      //Cfecha.selectAll();
      //Cfecha.requestFocus();
    }

  }

  void Chora_focusLost(FocusEvent e) {
    Chora.ValidarFecha(); //validamos el dato
    if (Chora.getValid().equals("N")) {
      //opcional vaciarlo
      Chora.setText(Chora.getFecha());
      //opcional si obligatorio
      //Cfecha.selectAll();
      //Cfecha.requestFocus();
    }
    else if (Chora.getValid().equals("S")) {
      Chora.setText(Chora.getFecha());
      //CODIGO LUIS
    }
  }

} //FIN DE CLASE PPAL******************************************
