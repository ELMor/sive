//LUIS RIVERA
//Ordenable . Interfaz que deben implementar los elementos de una lista
// que sea de clase ListaAgrupable,es decir, se pueda dividir en varias listas
//seg�n cierto criterio de agrupaci�n

package componentes;

public interface Agrupable {

  //Dice si este elemento pertenece al mismo grupo que el pasado como par�metro
  boolean mismoGrupoQue(Agrupable agru);

}