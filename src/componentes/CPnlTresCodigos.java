package componentes;

import java.util.Vector;

import java.awt.Cursor;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CInicializar;
import capp2.CPanel;
import capp2.CPnlCodigo;
import sapp2.Data;
import sapp2.QueryTool;

/**
 * Se trata de un componente con tres componentes de clase PnlCodigo
 * en los cuales la consulta del segundo va vinculada al c�digo del primero
 * en los cuales la consulta del tercero va vinculada al c�digo de los dos primeros
 * Estos componentes internos son creados externamente a este panel
 * Por tanto se pueden manipular sus propiedades desde fuera directamente
 * Pero este panel facilita la inicializaci�n (m�todo InicializarEnModo()
 *
 *  NOTA USO CLASE:
 *  La forma de vincular el Pnlcodigo2 con el c�digo 1 es atrav�s de la queryTool del CPnlCodigo2
 *  Al inicializar este CPnlDosCodigos, por defecto se pone el valorCodigo de CPnlCodigo1 en la
 *  queryTool del CPnlCodigo2, a�adiendo cl�usula where nombreCodigo = valorCodigo1
 *  Si ese no es el comportamiento deseado, y se quiere usar un panel de dos c�digos
 *  con otra querytool2, hay dos opciones:
 *  --Hacer setPonerCodigo a false y manejar la querytool2 desde fuera directamente
 *  --Derivar una clase de esta y sobreescribir m�todo setValorCodigo1()
 *
 * Los par�metros del constructor son los siguientes:
 *   PCod1 : panel de c�digo 1
 *   PCod2 : panel de c�digo 2
 *   PCod3 : panel de c�digo 3
     *   NomCampo Nombre campo de b.datos vincula a los dos paneles (c�digo panel 1)
 * 	CInicializar contenedor: instancia del contenedor
 *   modDib : Dibujo paneles en horizontal o vertical (uno debajo de otro)
 * @autor LRG
 * @version 1.0
 */

//Ejemplo uso en gv_mant.DialUnCenVac

public class CPnlTresCodigos
    extends CPanel
    implements CInicializar {

  XYLayout xyLayout = new XYLayout();
  //Componenentes internos
  CPnlCodigo panCod1 = null;
  CPnlCodigo panCod2 = null;
  CPnlCodigo panCod3 = null;

  // contenedor
  private CInicializar ciContenedor = null;

  //Campos de b.datos vinculan paneles
  String nomCampo1 = ""; //Nombre campo c�digo 1
  String nomCampo2 = ""; //Nombre campo c�digo 2

  //Booleana indica si se debe poner el c�digo 1 en querytool 2
  //y c�digos 1 y 2 en querytool 3 desde esta clase (Por defecto a true)
  boolean ponerCodigo = true;

  //Modos de dibujo
  public static final int modoHORIZONTAL = 1;
  public static final int modoVERTICAL = 2;
  int modoDibujo = modoHORIZONTAL;

  // modo de operaci�n
  private int modoOperacion = CInicializar.NORMAL;

  int width;
  int height;

  /**
   * Constructor
   */
  public CPnlTresCodigos(CPnlCodigo pCod1,
                         CPnlCodigo pCod2,
                         CPnlCodigo pCod3,
                         String nomCamp1,
                         String nomCamp2,
                         CInicializar contenedor,
                         int modoDib
                         ) {

    super(pCod1.getApp());
    try {
      panCod1 = pCod1;
      panCod2 = pCod2;
      panCod3 = pCod3;
      nomCampo1 = nomCamp1;
      nomCampo2 = nomCamp2;
      ciContenedor = contenedor;
      modoDibujo = modoDib;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Dibuja el componente
   */
  void jbInit() throws Exception {
    final String imgLUPA = "images/magnify.gif";

    switch (modoDibujo) {
      // tama�o de este componente
      case modoHORIZONTAL:
        width = panCod1.width + panCod2.width + panCod3.width;
        height = panCod1.height;
        break;
      case modoVERTICAL:
        width = panCod1.width;
        height = panCod1.height + panCod2.height + panCod3.height;
        break;
    }

    xyLayout.setWidth(width);
    xyLayout.setHeight(height);

    this.setLayout(xyLayout);
    this.setSize(width, height);

    switch (modoDibujo) {
      // tama�o de este componente
      case modoHORIZONTAL:

        // pinta  componentes en horizontal
        this.add(panCod1, new XYConstraints(0, 0, panCod1.width, panCod1.height));
        this.add(panCod2,
                 new XYConstraints(panCod1.width, 0, panCod2.width,
                                   panCod2.height));
        this.add(panCod3,
                 new XYConstraints(panCod1.width + panCod2.width, 0,
                                   panCod3.width, panCod3.height));
        break;
      case modoVERTICAL:

        // pinta componentes en vertical
        this.add(panCod1, new XYConstraints(0, 0, panCod1.width, panCod1.height));
        this.add(panCod2,
                 new XYConstraints(0, panCod1.height, panCod2.width,
                                   panCod2.height));
        this.add(panCod3,
                 new XYConstraints(0, panCod1.height + panCod2.height,
                                   panCod3.width, panCod3.height));
        break;
    }

  }

  //Por ser un CPanel debe estar implementado este m�todo (No se usa)
  public void Inicializar() {
  }

  //___________________________________________________________
  //IMPLEMENTACION metodo de INTERFAZ CINICIALIZAR

  /*Inicializar en modo Espera o Normal
   *Ser� LLAMADO desde un panel de c�digo CONTENIDO en este)
   */
  public void Inicializar(int modo) {

    //Deja preparado para posterior inicializaci�n
    this.modoOperacion = modo;

    //Inicializa el continente de este
    if (this.ciContenedor != null) {
      this.ciContenedor.Inicializar(modo);
    }

    //NOTA: Al inicializarse el continente de este debe provocarse una llamada
    //al m�todo Inicializar aqu� implementado con lo cual se inicializan tambi�n
    //los dos paneles de c�digo

  }

  //__________________________________________________________

  /*Ser� LLAMADO DESDE CONTINENTE de este
   * gesti�n del estado habilitado/deshabilitado de los componentes
   */
  public void InicializarEnModo(int modo) {
    Data d = null;
    Vector v = null;

    switch (modo) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        panCod3.backupDatos();
        panCod2.backupDatos();
        panCod1.backupDatos();
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        panCod1.setEnabled(true);

        // no hay un cod 1 -> panel codigo 2 y panel codigo 3 deshabilitados
        if (panCod1.getDatos() == null) {
          panCod3.setEnabled(false);
          panCod3.limpiarDatos();

          panCod2.setEnabled(false);
          panCod2.limpiarDatos();

          // hay cod1 : Se habilita panCod2
        }
        else {
          panCod2.setEnabled(true);

          //Si se debe poner c�digo (por defecto), se pone c�digo 1 en querytool de PnlCodigo2
          if (ponerCodigo == true) {
            setValorCodigo1();
          }

          // si hay cambios en el panCod1 -> se resetean el panCod2 y el panCod3
          if (panCod1.hayCambios()) {
            panCod3.limpiarDatos();
            panCod2.limpiarDatos();
          }

          //__________
          // no hay un cod 2 -> panel codigo 3 deshabilitado
          if (panCod2.getDatos() == null) {
            panCod3.setEnabled(false);
            panCod3.limpiarDatos();

            // hay cod2 : Se habilita panCod3
          }
          else {
            panCod3.setEnabled(true);

            //Si se debe poner c�digo (por defecto), se pone c�digo 2 en querytool de PnlCodigo3
            if (ponerCodigo == true) {
              setValorCodigo2();
            }

            // si hay cambios en el panCod2 -> se resetea el panCod3
            if (panCod2.hayCambios()) {
              panCod3.limpiarDatos();

            }
          } //fin else

          //___________
        } //fin else

        break;
    }
  }

  /**
   * setPonerCodigo
   *
       *@param poner --->Indica si se debe poner c�digo 1 en QueryTool del PnlCodigo 2
   * y c�digo 2 en QueryTool del PnlCodigo 3 desde esta clase
   */
  void setPonerCodigo(boolean poner) {
    ponerCodigo = poner;
  }

  /**
   * setValorCodigo1 --> Pone el c�digo 1 en la QueryTool2 (QueryTool del PnlCodigo 2)
   *
   */
  void setValorCodigo1() {
    QueryTool qt2 = null; //QueryTool del segundo panel
    // se establece el c�digo 1
    qt2 = panCod2.getQueryTool();
    qt2.putWhereValue(nomCampo1, ( (Data) panCod1.getDatos()).get(nomCampo1));
  }

  /**
   * setValorCodigo2 --> Pone el c�digo 1 y del c�digo 2 en la QueryTool3 (QueryTool del PnlCodigo 3)
   *
   */
  void setValorCodigo2() {
    QueryTool qt3 = null; //QueryTool del segundo panel
    // se establece el c�digo 1
    qt3 = panCod3.getQueryTool();
    qt3.putWhereValue(nomCampo1, ( (Data) panCod1.getDatos()).get(nomCampo1));
    qt3.putWhereValue(nomCampo2, ( (Data) panCod2.getDatos()).get(nomCampo2));
  }

  /**
   * getPnlCodigo1
   * @return Devuelve panel de c�digo 1
   */
  CPnlCodigo getPnlCodigo1() {
    return panCod1;
  }

  /**
   * getPnlCodigo2
   * @return Devuelve panel de c�digo 2
   */
  CPnlCodigo getPnlCodigo2() {
    return panCod2;
  }

  /**
   * getPnlCodigo3
   * @return Devuelve panel de c�digo 3
   */
  CPnlCodigo getPnlCodigo3() {
    return panCod3;
  }

}
