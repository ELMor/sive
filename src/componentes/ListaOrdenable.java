//AUTOR Luis Rivera
//Lista (Vector) en el cual se pueden ordenar sus elementos
//Los elementos del Vector deben implementar la interfaz Ordenable
// es , decir, deben ser ordenables segun algun criterio
//NOTA: Ejemplo uso : Paquete gesvac.cliente.gv_mantpres
//Clases DlgInformeDetPedidos y DataInfDetPedidos

package componentes;

import sapp2.Lista;

public class ListaOrdenable
    extends Lista {

  public ListaOrdenable() {
  }

  //Ordena esta lista de forma ascendente usando algoritmo Quicksort
  public void ordenaAsc() {
    quickSort(this, 0, this.size() - 1);
  }

  //ALgoritmo de ordenacion QUICKSORT (VER LIBROS) aplicado a Vector de elementos
  //que implementan interfaz Ordenable
  //Ordena el vector de casos de menor a mayor

  public static void quickSort(Lista vCasos, int primero, int ultimo) {
    int izq, der; //Variables auxiliares para indices recorrido
    //String elemRef=null;
    Ordenable elemRef = null;
    izq = primero; //inicialmente el menor indice del vector
    der = ultimo; //inicialmente el mayor indice del vector

    elemRef = (Ordenable) vCasos.elementAt( (izq + der) / 2); //Elemento de referencia

    while (izq < der) {
      Ordenable elemIzq = (Ordenable) vCasos.elementAt(izq); //Elemento de izqda
      Ordenable elemDer = (Ordenable) vCasos.elementAt(der); //Elemento de dcha

      while (elemIzq.menorQue(elemRef)) {
        izq++;
        elemIzq = (Ordenable) vCasos.elementAt(izq); //Elemento de izqda
      }

      while (elemDer.mayorQue(elemRef)) {
        der--;
        elemDer = (Ordenable) vCasos.elementAt(der); //Elemento de dcha
      }

      //Si indice de izqda es menor que el de la dche

      if (izq <= der) {
        //Se intercambian los elemntos izqda y decha

        //Se mete en elem de la izqda uno con valor del de la dche
        Ordenable auxIzq = (Ordenable) vCasos.elementAt(izq);
        Ordenable auxDer = (Ordenable) vCasos.elementAt(der);
        vCasos.removeElementAt(izq);
        vCasos.insertElementAt(auxDer, izq);
        //Se mete en elem de la dcha uno con valor del de la izqda
        vCasos.removeElementAt(der);
        vCasos.insertElementAt(auxIzq, der);

        izq++;
        der--;
      }

    } //while

    if (primero < der) {
      quickSort(vCasos, primero, der);

    }
    if (izq < ultimo) {
      quickSort(vCasos, izq, ultimo);
    }
  }

}