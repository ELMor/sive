/**
 * Se trata de un componente con dos componentes de clase PnlCodigo
 * en los cuales la consulta del segundo va vinculada al c�digo del primero
 * Estos componentes internos son creados externamente a este panel
 * Por tanto se pueden manipular sus propiedades desde fuera directamente
 * Pero este panel facilita la inicializaci�n (m�todo InicializarEnModo()
 *
 *  NOTA USO CLASE:
 *  La forma de vincular el Pnlcodigo2 con el c�digo 1 es atrav�s de la queryTool del CPnlCodigo2
 *  Al inicializar este CPnlDosCodigos, por defecto se pone el valorCodigo de CPnlCodigo1 en la
 *  queryTool del CPnlCodigo2, a�adiendo cl�usula where nombreCodigo = valorCodigo1
 *  Si ese no es el comportamiento deseado, y se quiere usar un panel de dos c�digos
 *  con otra querytool2, hay dos opciones:
 *  --Hacer setPonerCodigo a false y manejar la querytool2 desde fuera directamente
 *  --Derivar una clase de esta y sobreescribir m�todo setValorCodigo()
 *
 * Los par�metros del constructor son los siguientes:
 *   PCod1 : panel de c�digo 1
 *   PCod2 : panel de c�digo 2
     *   NomCampo Nombre campo de b.datos vincula a los dos paneles (c�digo panel 1)
 * 	CInicializar contenedor: instancia del contenedor
 *   modDib : Dibujo paneles en horizontal o vertical (uno debajo de otro)
 * @autor LRG
 * @version 1.0
 */

//Ejemplo uso en gv_mant.DialUnCenVac

package componentes;

import java.util.Vector;

import java.awt.Cursor;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CInicializar;
import capp2.CPanel;
import capp2.CPnlCodigo;
import sapp2.Data;
import sapp2.QueryTool;

public class CPnlDosCodigos
    extends CPanel
    implements CInicializar {

  XYLayout xyLayout = new XYLayout();
  //Componenentes internos
  CPnlCodigo panCod1 = null;
  CPnlCodigo panCod2 = null;

  // contenedor
  private CInicializar ciContenedor = null;

  //Campo de b.datos vincula paneles
  String nomCampo = "";

  //Booleana indica si se debe poner el c�digo 1 en querytool 2 desde esta clase
  //Por defecto a true
  boolean ponerCodigo = true;

  //Modos de dibujo
  public static final int modoHORIZONTAL = 1;
  public static final int modoVERTICAL = 2;
  int modoDibujo = modoHORIZONTAL;

  // modo de operaci�n
  private int modoOperacion = CInicializar.NORMAL;

  int width;
  int height;

  /**
   * Constructor
   */
  public CPnlDosCodigos(CPnlCodigo pCod1,
                        CPnlCodigo pCod2,
                        String nomCamp,
                        CInicializar contenedor,
                        int modoDib
                        ) {

    super(pCod1.getApp());
    try {
      panCod1 = pCod1;
      panCod2 = pCod2;
      nomCampo = nomCamp;
      ciContenedor = contenedor;
      modoDibujo = modoDib;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Dibuja el componente
   */
  void jbInit() throws Exception {
    final String imgLUPA = "images/magnify.gif";

    switch (modoDibujo) {
      // tama�o de este componente
      case modoHORIZONTAL:
        width = panCod1.width + panCod2.width;
        height = panCod1.height;
        break;
      case modoVERTICAL:
        width = panCod1.width;
        height = panCod1.height + panCod2.height;
        break;
    }

    xyLayout.setWidth(width);
    xyLayout.setHeight(height);

    this.setLayout(xyLayout);
    this.setSize(width, height);

    switch (modoDibujo) {
      // tama�o de este componente
      case modoHORIZONTAL:

        // pinta  componentes en horizontal
        this.add(panCod1, new XYConstraints(0, 0, panCod1.width, panCod1.height));
        this.add(panCod2,
                 new XYConstraints(panCod1.width, 0, panCod2.width,
                                   panCod2.height));
        break;
      case modoVERTICAL:

        // pinta componentes en vertical
        this.add(panCod1, new XYConstraints(0, 0, panCod1.width, panCod1.height));
        this.add(panCod2,
                 new XYConstraints(0, panCod1.height, panCod2.width,
                                   panCod2.height));
        break;
    }

  }

  //Por ser un CPanel debe estar implementado este m�todo (No se usa)
  public void Inicializar() {
  }

  //___________________________________________________________
  //IMPLEMENTACION metodo de INTERFAZ CINICIALIZAR

  /*Inicializar en modo Espera o Normal
   *Ser� LLAMADO desde un panel de c�digo CONTENIDO en este)
   */
  public void Inicializar(int modo) {

    //Deja preparado para posterior inicializaci�n
    this.modoOperacion = modo;

    //Inicializa el continente de este
    if (this.ciContenedor != null) {
      this.ciContenedor.Inicializar(modo);
    }

    //NOTA: Al inicializarse el continente de este debe provocarse una llamada
    //al m�todo Inicializar aqu� implementado con lo cual se inicializan tambi�n
    //los dos paneles de c�digo

  }

  //__________________________________________________________

  /*Ser� LLAMADO DESDE CONTINENTE de este
   * gesti�n del estado habilitado/deshabilitado de los componentes
   */
  public void InicializarEnModo(int modo) {
    Data d = null;
    Vector v = null;

    switch (modo) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        panCod2.backupDatos();
        panCod1.backupDatos();
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        panCod1.setEnabled(true);

        // no hay un cod 1 -> panel codigo 2 deshabilitado
        if (panCod1.getDatos() == null) {
          panCod2.setEnabled(false);
          panCod2.limpiarDatos();

          // hay cod1 : Se habilita panCod2
        }
        else {
          panCod2.setEnabled(true);

          //Si se debe poner c�digo (por defecto), se pone c�digo 1 en querytool de PnlCodigo2
          if (ponerCodigo == true) {
            setValorCodigo();
          }

          //Si se ha puesteo en espera y hay cambios en el panCod1 -> se resetea el panCod2
          if (panCod1.hayCambios()) {
            panCod2.limpiarDatos();
          }
        }

        break;
    }

  }

  //M�todo para la primera inicializaci�n
  //Debe ser llamado despu�s de hacer las instrucciones setCodigo(Data) en los CPnlCodigo
  //La primera inicializacion es distinta porque los dos c�digos pueden cambiar
  //a la vez con locual podr�a resetearse el segundo c�digo innecesaiamente
  //P ej al entrar en un di�logo

  public void inicializatePrimeraVezNormal(boolean habilitado) {

    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

    //En modo habilitado, panCod1 habilitao y panCod2 solo habilitado si hay datos en panCod1
    if (habilitado == true) {
      panCod1.setEnabled(true);

      if (panCod1.getDatos() == null) {
        panCod2.setEnabled(false);
        panCod2.limpiarDatos();
      }
      // hay cod1 : Se habilita panCod2
      else {
        panCod2.setEnabled(true);

        //Si se debe poner c�digo (por defecto), se pone c�digo 1 en querytool de PnlCodigo2
        if (ponerCodigo == true) {
          setValorCodigo();
        }
      } //else
    } //if habilitado

    //En modo no habilitado todo false indeptemente de los datos
    //Por ejemplo, para modo solo consulta)
    else {
      panCod1.setEnabled(false);
      panCod2.setEnabled(false);
    }

  } //metodo

  /**
   * setPonerCodigo
   *
   *@param poner --->Indica si se debe poner c�digo 1 en QueryTool del PnlCodigo 2 desde esta clase
   */
  void setPonerCodigo(boolean poner) {
    ponerCodigo = poner;
  }

  /**
   * setValorCodigo --> Pone el c�digo 1 en la QueryTool2 (QueryTool del PnlCodigo 2)
   *
   */
  void setValorCodigo() {
    QueryTool qt2 = null; //QueryTool del segundo panel
    // se establece el c�digo 1
    qt2 = panCod2.getQueryTool();
    qt2.putWhereValue(nomCampo, ( (Data) panCod1.getDatos()).get(nomCampo));
  }

  /**
   * getPnlCodigo1
   * @return Devuelve panel de c�digo 1
   */
  CPnlCodigo getPnlCodigo1() {
    return panCod1;
  }

  /**
   * getPnlCodigo2
   * @return Devuelve panel de c�digo 2
   */
  CPnlCodigo getPnlCodigo() {
    return panCod2;
  }

}
