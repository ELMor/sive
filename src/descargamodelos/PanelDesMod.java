
package descargamodelos;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CFileName;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import confprot.DataModelo;
import sapp.StubSrvBD;

//import confprot.*;

public class PanelDesMod
    extends CPanel {

  //modos de consulta al  servlet de ficheros
  final int servletDES_MOD = 0;
  final int servletDES_MOD_CNE = 1; //Para ficheros del CNE (lleva algunos datos de SIVE_MODELO)

  ResourceBundle res;

  //modos de consulta al  servlet de modelos
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  //rutas imagenes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/salvar.gif"};

  //modos de pantalla
  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  //Variables
  int modoOperacion;
  int modoServlet = servletDES_MOD;

  protected CLista listaFich = new CLista();

  //Rutas de los servlet
  final String strSERVLET = "servlet/SrvDesMod";
  final String strSERVLET_MOD = "servlet/SrvGesMod";

  protected StubSrvBD stubCliente = new StubSrvBD();

  //Usos posibles del panel : El fichero generado puede variar
  // en algunos detalles (consulta Servlet es distinta)
  static final int modoPanelNORMAL = 1;
  static final int modoPanelCNE = 2;
  int modoPanel;

  XYLayout xYLayout1 = new XYLayout();
  ButtonControl btnGenFic = new ButtonControl();
  CFileName panFile;

  String sCodEnf = "";

  PanelDesModActionListener btnActionListener = new PanelDesModActionListener(this);
  PanelDesModTxtCodModFocusAdapter txtCodModFocusAdapter = new
      PanelDesModTxtCodModFocusAdapter(this);

  protected CCargadorImagen imgs = null;
  ButtonControl btnSearchMod = new ButtonControl();
  Label lblModelo = new Label();
  CCampoCodigo txtCodMod = new CCampoCodigo();
  TextField txtDesMod = new TextField();

  public PanelDesMod(CApp a) {
    try {
      setApp(a);
      res = ResourceBundle.getBundle("descargamodelos.Res" + app.getIdioma());
      jbInit();
      modoPanel = modoPanelNORMAL;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public PanelDesMod(CApp a, int modoPan) {
    try {
      setApp(a);
      res = ResourceBundle.getBundle("descargamodelos.Res" + app.getIdioma());
      jbInit();
      modoPanel = modoPan;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    panFile = new CFileName(this.app);

    this.setSize(new Dimension(569, 300));
    xYLayout1.setHeight(263);
    xYLayout1.setWidth(475);

    btnGenFic.setActionCommand("GenFic");
    btnSearchMod.setActionCommand("BuscarMod");

    btnGenFic.addActionListener(btnActionListener);
    btnSearchMod.addActionListener(btnActionListener);
    txtCodMod.addFocusListener(txtCodModFocusAdapter);

    btnGenFic.setLabel(res.getString("btnGenFic.Label"));
    this.setLayout(xYLayout1);

    this.add(lblModelo, new XYConstraints(20, 43, 78, -1));
    this.add(txtCodMod, new XYConstraints(102, 43, 129, -1));
    this.add(btnSearchMod, new XYConstraints(238, 41, -1, -1));
    this.add(txtDesMod, new XYConstraints(274, 43, 161, -1));
    this.add(panFile, new XYConstraints(11, 86, -1, -1));
    this.add(btnGenFic, new XYConstraints(315, 160, -1, 26));

    txtCodMod.setBackground(new Color(255, 255, 150));

    txtCodMod.addKeyListener(new PanelDesMod_txtCodMod_keyAdapter(this));
    lblModelo.setText(res.getString("lblModelo.Text"));

    //Carga im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnSearchMod.setImage(imgs.getImage(0));
    btnGenFic.setImage(imgs.getImage(1));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        btnGenFic.setEnabled(true);
        txtCodMod.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnGenFic.setEnabled(false);
        txtCodMod.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
    doLayout();
  }

  void btnGenFic_actionPerformed(ActionEvent evt) {

    CMessage msgBox;
    CLista data;
    int j = 0;
    int numTok; //Num tokens quedadn por recorrer de una linea
    String sLinea;
    File miFichero = null;
    FileWriter fStream = null;

    try {

      if (modoServlet != -1) {

        if (isDataValid() == true) {

          this.modoOperacion = modoESPERA;
          Inicializar();

          data = new CLista();

          //Da formato a semana para que queden todas con 2 caracteres num�ricos
          //Pasando a entero se eliminan los ceros a la izqda y luego  si es necesario se a�ade un cero

          data.addElement(new DataModelo(this.app.getTSive(), txtCodMod.getText(), null, null, null, null,
                                         app.getCA(), null, null, null, null));
          // apunta al servlet principal
          stubCliente.setUrl(new URL(app.getURL() + strSERVLET));

          //# // System_out.println("URL Apuntado*****************");

          // obtiene la lista del CNE
          if (modoPanel == modoPanelCNE) {
            listaFich = (CLista) stubCliente.doPost(servletDES_MOD_CNE, data);
          }
          else {
            // obtiene la lista
            listaFich = (CLista) stubCliente.doPost(servletDES_MOD, data);
          }

          /*
                      // obtiene la lista
                      SrvDesMod servlet = new SrvDesMod();
                     //Indica como conectarse a la b.datos
               servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
               "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                     "pista_prod",
                                     "pista_prod");
                     //Indica como conectarse a la b.datos de Microsoft
                     servlet.setJdbcEnvironment("com.inet.tds.TdsDriver",
               "jdbc:inetdae:melkor:1433?database=MODULO1",
                                     "pista",
                                     "ppns");
               listaFich = (CLista) servlet.doDebug(servletDES_MOD_CNE, data);
           */

          //# // System_out.println("do Post*****************");

          if (listaFich.size() > 0) {
            try {
              miFichero = new File(this.panFile.txtFile.getText());
              fStream = new FileWriter(miFichero);

              for (j = 0; j < listaFich.size(); j++) {
                sLinea = (String) listaFich.elementAt(j);
                fStream.write(sLinea);
                fStream.write("\r");
                fStream.write("\n");
              }

              fStream.close();
              fStream = null;
              miFichero = null;
              msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                    res.getString("msg2.Text"));
              msgBox.show();
              msgBox = null;
            }
            catch (IOException ioEx) {
              ioEx.printStackTrace();
              msgBox = new CMessage(this.app, CMessage.msgERROR,
                                    res.getString("msg3.Text"));
              msgBox.show();
              msgBox = null;
            }

          } //if
          // fichero vacio
          else {
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                  res.getString("msg4.Text"));
            msgBox.show();
            msgBox = null;
          }
        } //if
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg5.Text"));
          msgBox.show();
          msgBox = null;
        }
      } //if
      else {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg6.Text"));
        msgBox.show();
        msgBox = null;
      }

      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }

    this.modoOperacion = modoNORMAL;
    Inicializar();

  }

  boolean isDataValid() {
    if (
        //Comprueba que los �ltimos datos "v�lidos" (las copias de seguridad) no son vac�os
        (!panFile.txtFile.getText().equals("")) &&
        (!txtDesMod.getText().equals("")) //Si descripci�n no es vac�a es que hay c�digo correcto

        ) {
      return true;
    }
    else {
      return false;
    }
  }

  public void btnBuscarMod_actionPerformed(ActionEvent evt) {

    CMessage msgBox = null;
    DataModelo data;
//    this.modoOperacionBk = this.modoOperacion;

    try {
      // apunta al servlet auxiliar
      stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_MOD));
      this.modoOperacion = modoESPERA;
      Inicializar();

      CListaModelo lista = new CListaModelo(app,
                                            res.getString("msg7.Text"),
                                            stubCliente,
                                            strSERVLET_MOD,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataModelo) lista.getComponente();

      //he traido datos
      if (data != null) {
        txtCodMod.setText(data.getCD_Modelo());
        txtDesMod.setText(data.getDS_Modelo());
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  void txtCodMod_keyPressed(KeyEvent e) {
    txtDesMod.setText("");
  }

  void txtCodMod_focusLost() {

    // datos de envio
    DataModelo data;
    CLista param = null;
    CMessage msg;

    if (txtCodMod.getText().length() > 0) {

      // consulta en modo espera
      modoOperacion = modoESPERA;
      Inicializar();

      try {
        param = new CLista();

        param.setPerfil(app.getPerfil());
        param.setIdioma(app.getIdioma());
        param.setLogin(app.getLogin());
        param.setTSive(app.getTSive());
        param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
        param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());

        param.addElement(new DataModelo(this.app.getTSive(), txtCodMod.getText(), null, null, null, null,
                                        app.getCA(), null, null, null, null));

//# // System_out.println("Antes de peticion***************");
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_MOD));

        param = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO, param);

        /*
                // obtiene la lista
                SrvGesMod servlet = new SrvGesMod();
                //Indica como conectarse a la b.datos
                servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                servlet.setJdbcEnvironment("com.inet.tds.TdsDriver",
             "jdbc:inetdae:melkor:1433?database=MODULO1",
                                   "pista",
                                   "ppns");
             param = (CLista) servlet.doDebug(servletOBTENER_X_CODIGO, param);
         */

        // rellena los datos
        if (param.size() > 0) {
          data = (DataModelo) param.elementAt(0);
          txtCodMod.setText(data.getCD_Modelo());
          txtDesMod.setText(data.getDS_Modelo());

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg8.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
    // modo normal
    modoOperacion = modoNORMAL;
    Inicializar();

  }

} //clase

// action listener para los botones
class PanelDesModActionListener
    implements ActionListener, Runnable {
  PanelDesMod adaptee = null;
  ActionEvent e = null;

  public PanelDesModActionListener(PanelDesMod adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("GenFic")) { // generar fichero
      adaptee.btnGenFic_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("BuscarMod")) { // modelo
      adaptee.btnBuscarMod_actionPerformed(e);

    }

  }
}

// listas de valores
class CListaModelo
    extends CListaValores {
  public CListaModelo(CApp a, String title, StubSrvBD stub, String servlet,
                      int obtener_x_codigo, int obtener_x_descripcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a, title, stub, servlet, obtener_x_codigo, obtener_x_descripcion,
          seleccion_x_codigo, seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataModelo(this.app.getTSive(), s, null, null, null, null,
                          app.getCA(), null, null, null, null);
  }

  public String getCodigo(Object o) {
    return ( ( (DataModelo) o).getCD_Modelo());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataModelo) o).getDS_Modelo());
  }

}

//************************************************
 class PanelDesModTxtCodModFocusAdapter
     extends java.awt.event.FocusAdapter
     implements Runnable {
   PanelDesMod adaptee;

   PanelDesModTxtCodModFocusAdapter(PanelDesMod adaptee) {
     this.adaptee = adaptee;
   }

   public void focusLost(FocusEvent e) {
     Thread th = new Thread(this);
     th.start();
   }

   public void run() {

     adaptee.txtCodMod_focusLost();

   }
 }

class PanelDesMod_txtCodMod_keyAdapter
    extends java.awt.event.KeyAdapter {
  PanelDesMod adaptee;

  PanelDesMod_txtCodMod_keyAdapter(PanelDesMod adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCodMod_keyPressed(e);
  }
}
