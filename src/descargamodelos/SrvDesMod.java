package descargamodelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import capp.CLista;
import confprot.DataModelo;
import listas.DataMantlis;
import preguntas.DataMantPreguntas;
import sapp.DBServlet;

public class SrvDesMod
    extends DBServlet {

  //modos de consulta al  servlet de ficheros
  final int servletDES_MOD = 0;
  final int servletDES_MOD_CNE = 1; //Para ficheros del CNE (lleva algunos datos de SIVE_MODELO)

  //Linea de resultado. Se define aqui para ser manipulable por funcion anadir
  String sLinea = "";

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    String query = null;

    int i = 1;
    int k; //Para �ndice b�squeda columnas

    // objetos de datos
    CLista data = null; //lista de resultado al cliente (lineas, es decir, Strings)
    Enumeration enum; //Para recorrer listas
    DataModelo datMod = null; // datos entrada para generar fichero

    //Datos quue se extraer�n de lista de petici�n
    String sCodMod = "";
    String sCodTsi = "";

    // Campo recogido en una selecci�n
    String sCam = null;
    int iCam;

    //__________Atributos usados para guardar datos provisionales antes de meterlos en lineas de fichero

    DataMantPreguntas datPre = null; //datos de una pregunta
    DataMantlis datLis = null; //Datos de una lista
    DataLisVal datLisVal = null; //Datos de una lista de valores

    Vector vecPre = new Vector(); //Vector de preguntas de un mocdelo
    Hashtable hasLis = new Hashtable(); // Hashtable de listas del modelo
    Vector vecLisVal = new Vector(); //Vector de listas de valores pertenecientes a listas del modelo

    //Para recoger datos de preguntas

    String sCodTSive;
    String sCodPregunta;
    String sDescPregunta;
    String sDescLPregunta;
    String sListaValores;
    String iTipo;
    int iLongitud;
    int iEnteros;
    int iDecimales;
    String sDescLista;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

// System_out.println("****Entrando");

    try {

      // modos de operaci�n
      switch (opmode) {

        // listado

        case servletDES_MOD:
        case servletDES_MOD_CNE:

          datMod = (DataModelo) param.firstElement();

          sCodTsi = datMod.getCD_TSIVE().trim();
          sCodMod = datMod.getCD_Modelo().trim();

          // prepara la lista de resultados
          data = new CLista();

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);

//________________ Consulta de preguntas_____________________________________

          }
          st = null;
          rs = null;

          /*query = "select distinct b.CD_PREGUNTA, b.DS_PREGUNTA, "+
                   " b.DSL_PREGUNTA, b.CD_LISTA, b.CD_TPREG, "+
                   " b.NM_LONG, b.NM_ENT, b.NM_DEC "+
                   "  from sive_linea_item a, sive_pregunta b "+
                   " where (a.cd_tsive = ? and a.cd_modelo = ? "+
                   " and a.cd_pregunta = b.cd_pregunta)";*/

          //mlm
          query = "select b.CD_PREGUNTA, b.DS_PREGUNTA, " +
              " b.DSL_PREGUNTA, b.CD_LISTA, b.CD_TPREG, " +
              " b.NM_LONG, b.NM_ENT, b.NM_DEC " +
              " from SIVE_LINEA_ITEM a, SIVE_PREGUNTA b " +
              " where (a.CD_TSIVE = b.CD_TSIVE " +
              " and a.CD_PREGUNTA = b.CD_PREGUNTA) and " +
              " a.CD_TSIVE = ? and a.CD_MODELO = ? " +
              " order by NM_LIN ";

          st = con.prepareStatement(query);

          // c�digo TSIVE  // c�digo modelo
          st.setString(1, sCodTsi);
          st.setString(2, sCodMod);

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // lee el registro
            sCodPregunta = rs.getString("CD_PREGUNTA");
            sDescPregunta = rs.getString("DS_PREGUNTA");
            sDescLPregunta = rs.getString("DSL_PREGUNTA");
            sListaValores = rs.getString("CD_LISTA");
            iTipo = rs.getString("CD_TPREG");
            iLongitud = rs.getInt("NM_LONG");
            iEnteros = rs.getInt("NM_ENT");
            iDecimales = rs.getInt("NM_DEC");

            // A�ade un nodo
            vecPre.addElement(new DataMantPreguntas(
                sCodPregunta,
                sDescPregunta,
                sDescLPregunta,
                sListaValores,
                iTipo,
                iLongitud,
                iEnteros,
                iDecimales));
          } //while
          rs.close();
          st.close();
// System_out.println("Recogidas  preguntas**********************");

          //______________________  Consultas de listas y listas de valores  ___________________________

//Se recorre la lista de preguntas y para las preguntas de tipo lista : (Su respta est� entre una lista de valores)
//Se meten los c�digos de lista en un hash (solo se meten los distintos entre s�
//Adem�s, usando como par�metro el c�digo de lista:
          //Se hace una consulta para buscar los otros campos de ese registro de lista
          //Se hace otra consulta para obtener las listas de valores

          //Recorremos lista de preguntas
          enum = vecPre.elements();
          while (enum.hasMoreElements()) {
            datPre = (DataMantPreguntas) (enum.nextElement());

            //Si la preguunta es de tipo lita
//         if  ( (datPre.getListaValores() != null ) && (datPre.getListaValores() != "" ) )
            if (datPre.getTipo().equals("L")) {

              // Si ese c�digo de lista aun no estaba en el hash
              if (! (hasLis.containsKey(datPre.getListaValores()))) {

                //Consulta para obtener resto de campos de SIVE_LISTA
                st = null;
                rs = null;

                query = "select CD_LISTA, DS_LISTA, DSL_LISTA " +
                    " from SIVE_LISTAS where CD_LISTA = ? ";
                st = con.prepareStatement(query);

                st.setString(1, datPre.getListaValores().trim());
                rs = st.executeQuery();

// System_out.println("Ejec query listas **********************");

                // extrae la p�gina requerida
                while (rs.next()) {

                  // crea el objeto del hash
                  datLis = new DataMantlis(rs.getString("CD_LISTA"),
                                           rs.getString("DS_LISTA"),
                                           rs.getString("DSL_LISTA"));
                  hasLis.put(datPre.getListaValores(), datLis);

                } //while rs para campos lista

                rs.close();
                st.close();
//# // System_out.println("Recogidas  listas**********************");
                //____________________________________________________

                //Consulta para obtener  campos de SIVE_LISTA_VALORES

                st = null;
                rs = null;
                query = "select CD_LISTA, CD_LISTAP, DS_LISTAP, DSL_LISTAP " +
                    " from SIVE_LISTA_VALORES where CD_LISTA = ? ";

                st = con.prepareStatement(query);

//# // System_out.println(" prep busca lista valores  ????????????????????");  //*********************
                st.setString(1, datPre.getListaValores().trim());
                rs = st.executeQuery();
//# // System_out.println("Ejec query listas valores**********************");
                // extrae la p�gina requerida
                while (rs.next()) {

                  //Aqu� metemos los valores por bucle
                  vecLisVal.addElement(new DataLisVal(rs.getString("CD_LISTA"),
                      rs.getString("CD_LISTAP"),
                      rs.getString("DS_LISTAP"),
                      rs.getString("DSL_LISTAP")));

                } //while rs para campos lista valores

                rs.close();
                st.close();
//# // System_out.println("A fichero  listas de valores**********************");
              } //if c�digo no estaba en hash
            } //if pregunta de tipo lista
          } //while recorrido preguntas

//# // System_out.println("Fin busqueda vector   ?????????????");  //*******************

           //_________  A�adido al fichero de Strings de listas   ___________________________

           //Cabecera
           sLinea = "[listas]";
           // a�ade un nodo (un registro) a la lista de lineas del fichero
          data.addElement(sLinea);

          //Recorremos lista de listas
          enum = hasLis.elements();
          while (enum.hasMoreElements()) {
            datLis = (DataMantlis) (enum.nextElement());

            sLinea = "";
            sCam = datLis.getCdlista();
            anadir(sCam, 11);
            sCam = datLis.getDslista();
            anadir(sCam, 40);
            sCam = datLis.getDsllista();
            if (sCam == null) {
              sCam = "";
            }
            anadir(sCam, 40);

            // a�ade un nodo (un registro) a la lista de lineas del fichero
            data.addElement(sLinea);
          }

          // System_out.println("A fichero listas **********************");

          //______________________  A�adido al fichero de Strings de listas valores  ___________________________

          //Cabecera
          sLinea = "[valores]";
          // a�ade un nodo (un registro) a la lista de lineas del fichero
          data.addElement(sLinea);

          //Recorremos lista de listas de valores
          enum = vecLisVal.elements();
          while (enum.hasMoreElements()) {
            datLisVal = (DataLisVal) (enum.nextElement());

            sLinea = "";
            sCam = datLisVal.getCodLista();
            anadir(sCam, 11);
            sCam = datLisVal.getCod();
            anadir(sCam, 13);
            sCam = datLisVal.getDes();
            anadir(sCam, 30);
            sCam = datLisVal.getDesL();
            if (sCam == null) {
              sCam = "";
            }
            anadir(sCam, 30);

            // a�ade un nodo (un registro) a la lista de lineas del fichero
            data.addElement(sLinea);
          }

//# // System_out.println("A fichero listas **********************");

          //_________  A�adido al fichero de Strings de preguntas   ___________________________

          //Cabecera
          sLinea = "[preguntas]";
          // a�ade un nodo (un registro) a la lista de lineas del fichero
          data.addElement(sLinea);

          //Recorremos hash de preguntas
          enum = vecPre.elements();
          while (enum.hasMoreElements()) {
            datPre = (DataMantPreguntas) (enum.nextElement());

            sLinea = "";
            //Se a�ade el tipo Sive (siempre el mismo)
            sCam = sCodTsi;
            anadir(sCam, 1);
            sCam = datPre.getCodPregunta();
            anadir(sCam, 11);
            sCam = datPre.getListaValores();
            anadir(sCam, 11);
            if (sCam == null) {
              sCam = "";
            }
            sCam = datPre.getDescPregunta();
            anadir(sCam, 40);
            sCam = datPre.getDescLPregunta();
            anadir(sCam, 40);
            if (sCam == null) {
              sCam = "";
            }
            sCam = datPre.getTipo();
            anadir(sCam, 1);
            //???????????????????????????????  Enteros ????????????
            //SE supone que si habia nul  en bdatos ahi se ha guardado un 0
            iLongitud = datPre.getLongitud();
            sCam = Integer.toString(iLongitud);
            anadir(sCam, 3);
            iEnteros = datPre.getEnteros();
            sCam = Integer.toString(iEnteros);
            anadir(sCam, 3);
            iDecimales = datPre.getDecimales();
            sCam = Integer.toString(iDecimales);
            anadir(sCam, 3);

            // a�ade un nodo (un registro) a la lista de lineas del fichero
            data.addElement(sLinea);
          } //while recorrido preguntas

          rs.close();
          st.close();

          // System_out.println("A fichero preguntas **********************");

          //Para modelos descargados desde el CNE tambi�n se meten algunos datos de SIVE_MODELO
          if (opmode == servletDES_MOD_CNE) {

            //________________ Consulta de campos del modelo y a�adido a fichero_____________________________________

            //Cabecera
            sLinea = "[modelo]";
            // a�ade un nodo (un registro) a la lista de lineas del fichero
            data.addElement(sLinea);

            // prepara
            query = "select a.CD_TSIVE, a.CD_MODELO, a.DS_MODELO, "
                + "a.CD_NIVEL_1, a.DSL_MODELO,a.CD_NIVEL_2, a.CD_CA, "
                + "a.IT_OK, b.CD_ENFERE"
                + " from SIVE_MODELO a, SIVE_ENFEREDO b "
                + "where a.CD_TSIVE = ? and a.CD_MODELO = ? "
                + "and a.CD_ENFCIE=b.CD_ENFCIE";

            st = con.prepareStatement(query);
            // c�digo TSIVE  // c�digo modelo
            st.setString(1, sCodTsi);
            st.setString(2, sCodMod);

            rs = st.executeQuery();
            // System_out.println("Ejec query modelos");
            // extrae la p�gina requerida
            while (rs.next()) {
              sLinea = "";
              sCam = rs.getString("CD_TSIVE");
              anadir(sCam, 1);
              sCam = rs.getString("CD_MODELO");
              anadir(sCam, 11);
              sCam = rs.getString("DS_MODELO");
              anadir(sCam, 60);
              sCam = rs.getString("CD_NIVEL_1");
              if (sCam == null) {
                sCam = "";
              }
              anadir(sCam, 2);
              sCam = rs.getString("DSL_MODELO");
              if (sCam == null) {
                sCam = "";
              }
              anadir(sCam, 60);
              sCam = rs.getString("CD_NIVEL_2");
              if (sCam == null) {
                sCam = "";
              }
              anadir(sCam, 2);
              sCam = rs.getString("CD_CA"); //FEchas??????????????????????
              if (sCam == null) {
                sCam = "";
              }
              anadir(sCam, 2);
              sCam = rs.getString("IT_OK");
              anadir(sCam, 1);
              sCam = rs.getString("CD_ENFERE");
              anadir(sCam, 3);

              // a�ade un nodo (una linea) a la lista de lineas
              data.addElement(sLinea);

            } //while
            rs.close();
            st.close();

          }

          // System_out.println("A fichero modelo **********************");

//________________ Consulta de lineas del modelo_____________________________________

          //Cabecera
          sLinea = "[lineas]";
          // a�ade un nodo (un registro) a la lista de lineas del fichero
          data.addElement(sLinea);

          st = null;
          rs = null;

          query = "select NM_LIN, CD_TLINEA, DS_TEXTO, DSL_TEXTO " +
              " from SIVE_LINEASM where CD_TSIVE = ? and CD_MODELO = ? " +
              " order by NM_LIN ";

          st = con.prepareStatement(query);
          // tsive  // modelo
          st.setString(1, sCodTsi.trim());
          st.setString(2, sCodMod.trim());

          rs = st.executeQuery();
//# // System_out.println("Ejec query lineas modelo**********************");
          // extrae las l�neas
          while (rs.next()) {
            sLinea = "";
            //A�ade los campos ya conocidos
            anadir(sCodTsi, 1);
            anadir(sCodMod, 11);
            // obtiene los campos
            iCam = rs.getInt("NM_LIN");
            sCam = Integer.toString(iCam);
            anadir(sCam, 3);
            sCam = rs.getString("CD_TLINEA");
            anadir(sCam, 1);
            sCam = rs.getString("DS_TEXTO");
            anadir(sCam, 40);
            sCam = rs.getString("DSL_TEXTO");
            if (sCam == null) {
              sCam = "";
            }
            anadir(sCam, 40);

            // a�ade un nodo (una linea) a la lista de lineas
            data.addElement(sLinea);

          }

          rs.close();
          st.close();
//# // System_out.println("A fichero lineas modelo**********************");

          //_______________ Consulta de detalle de las l�neas________________________

          //Cabecera
          sLinea = "[detalle linea]";
          // a�ade un nodo (un registro) a la lista de lineas del fichero
          data.addElement(sLinea);

          st = null;
          rs = null;

          // primero comprobamos que existe alg�n registro
          query = "select * from SIVE_LINEA_ITEM " +
              " where((CD_TSIVE=?)and(CD_MODELO=?)) order by NM_LIN";

          st = con.prepareStatement(query);
          // c�digo TSIVE
          st.setString(1, sCodTsi);
          // c�digo modelo
          st.setString(2, sCodMod);

          // lanza la query
          rs = st.executeQuery();
//# // System_out.println("Ejec query detalle lineas**********************");
          while (rs.next()) {
            sLinea = "";
            //A�ade los campos ya conocidos
            sCam = rs.getString("CD_TSIVE");
            anadir(sCam, 1);
            sCam = rs.getString("CD_MODELO");
            anadir(sCam, 11);
            iCam = rs.getInt("NM_LIN");
            sCam = Integer.toString(iCam);
            anadir(sCam, 3);
            sCam = rs.getString("CD_PREGUNTA");
            anadir(sCam, 11);
            sCam = rs.getString("CD_PREGUNTA_PC");
            if (sCam == null) {
              sCam = "";
            }
            anadir(sCam, 11);
            sCam = rs.getString("IT_OBLIGATORIO");
            anadir(sCam, 1);
            sCam = rs.getString("IT_CONDP");
            anadir(sCam, 1);
            iCam = rs.getInt("NM_LIN_PC");
            sCam = Integer.toString(iCam);
            anadir(sCam, 3);
            sCam = rs.getString("DS_VPREGUNTA_PC");
            if (sCam == null) {
              sCam = "";
            }
            anadir(sCam, 60);

            // a�ade un nodo (una linea) a la lista de lineas
            data.addElement(sLinea);

          }
          st.close();
          rs.close();
          st = null;
          rs = null;
//# // System_out.println("A fichero detalle lineas item **********************");
          break;

//????????????????????????????????????????????????????????????????????????

      } //fin switch

      // valida la transacci�n
      con.commit();

      //fall� la transacci�n
    }
    catch (Exception ex) {

      con.rollback();
      throw ex;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }

  //____________________________________________________________________________

  //A�ade al String sLinea (usado para cada linea del fichero) el String
  //indicado en parametro cual
  //Si cual no llega a ocupar longMax caracteres, estos caracteres se ponen como blancos
  void anadir(String cual, int longMax) {
    sLinea = sLinea + cual;
    for (int cont = 0; cont < (longMax - (cual.length())); cont++) {
      sLinea = sLinea + " ";
    }
  }

} //Clase