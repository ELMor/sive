
package descargamodelos;

import java.io.Serializable;

public class DataLisVal
    implements Serializable {
  protected String sCodLista = "";
  protected String sCod = "";
  protected String sDes = "";
  protected String sDesL = "";

  // constructor completo
  public DataLisVal(String codLista, String cod, String des, String desL) {
    sCodLista = codLista;
    sCod = cod;
    sDes = des;
    if (desL != null) {
      sDesL = desL;
    }
  }

  public String getCodLista() {
    return sCodLista;
  }

  public String getCod() {
    return sCod;
  }

  public String getDes() {
    return sDes;
  }

  public String getDesL() {
    String s;

    s = sDesL;
    if (s == null) {
      s = "";
    }
    return s;
  }
}