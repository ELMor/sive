package descargamodelos;

import java.util.ResourceBundle;

import capp.CApp;

public class DesMod
    extends CApp {

  ResourceBundle res;

  public DesMod() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("descargamodelos.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    // abrimos el panel
    VerPanel("", new PanelDesMod(a));
  }

}