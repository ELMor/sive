package descargamodelos;

import java.util.ResourceBundle;

import capp.CApp;

public class DesModCNE
    extends CApp {

  ResourceBundle res;

  public DesModCNE() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("descargamodelos.Res" + this.getIdioma());
    setTitulo(res.getString("msg20.Text"));
    CApp a = (CApp)this;
    // abrimos el panel
    VerPanel("", new PanelDesMod(a, PanelDesMod.modoPanelCNE));
  }

}
