
package centinelas.datos.c_not;

import centinelas.datos.centbasicos.NotifRMC;
import sapp2.Data;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class NotifRMCExt
    extends NotifRMC {

  public NotifRMCExt(Data d) {
    super();
  }

  public String getDS_PROCESO() {
    return getString("DS_PROCESO");
  } // Fin getDS_PROCESO()

  // SELECT * FROM SIVE_NOTIF_RMC where  and CD_PCENTI=?= and CD_MEDCENT=?
  // and CD_ANOEPI=? and CD_SEMEPI=?
  public static QueryTool2 getAllNotifRMCExtOfOnePtoMedAnoSem(
      String codPunto, String codMed, String ano, String sem) {
    //Se trae datos de tabla NotifRMC
    QueryTool2 qt = NotifRMC.getAllNotifRMC();

    //Se a�aden filtros para punto,m�dico,a�o,semana

    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", codPunto);
    qt.putOperator("CD_PCENTI", "=");

    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", codMed);
    qt.putOperator("CD_MEDCEN", "=");

    qt.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qt.putWhereValue("CD_ANOEPI", ano);
    qt.putOperator("CD_ANOEPI", "=");

    qt.putWhereType("CD_SEMEPI", QueryTool.STRING);
    qt.putWhereValue("CD_SEMEPI", sem);
    qt.putOperator("CD_SEMEPI", "=");

    //A�ade descripciones de las enfermedades
    QueryTool qtAdic = new QueryTool();
    qtAdic.putName("SIVE_PROCESOS");
    qtAdic.putType("DS_PROCESO", QueryTool.STRING);
    Data dtAdic = new Data();
    dtAdic.put("CD_ENFCIE", QueryTool.STRING);
    qt.addQueryTool(qtAdic);
    qt.addColumnsQueryTool(dtAdic);

    return qt;

  }
}
