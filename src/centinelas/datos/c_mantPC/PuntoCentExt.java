package centinelas.datos.c_mantPC;

import java.util.Enumeration;

import centinelas.datos.centbasicos.PuntoCent;
import sapp2.Data;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class PuntoCentExt
    extends PuntoCent {

  //constructor en blanco por defecto
  public PuntoCentExt() {}

  public PuntoCentExt(Data d) {
    Enumeration e1 = d.keys();
    Enumeration e2 = d.elements();

    for (e1 = d.keys(), e2 = d.elements(); e1.hasMoreElements(); ) {
      this.put( (String) e1.nextElement(), (String) e2.nextElement());
    }
  }

  public String getDS_CONGLO() {
    String s = getString("DS_CONGLO");
    return s;
  }

  public String getDS_TPCENTI() {
    getString("DS_TPCENTI");
    return getString("DS_TPCENTI");
  }

  public QueryTool2 getAllPuntoCentExt() {
    QueryTool2 qt = PuntoCent.getAllPuntoCent();
    // Datos adicionales
    QueryTool qtAdic = new QueryTool();
    qtAdic.putName("SIVE_TPCENTINELA");
    qtAdic.putType("DS_TPCENTI", QueryTool.STRING);
    Data dtAdic = new Data();
    dtAdic.put("CD_TPCENTI", QueryTool.STRING);
    qt.addQueryTool(qtAdic);
    qt.addColumnsQueryTool(dtAdic);

    qtAdic = new QueryTool();
    qtAdic.putName("SIVE_CONGLOM");
    qtAdic.putType("DS_CONGLO", QueryTool.STRING);
    dtAdic = new Data();
    dtAdic.put("CD_CONGLO", QueryTool.STRING);
    qt.addQueryTool(qtAdic);
    qt.addColumnsQueryTool(dtAdic);
    return qt;
  }

  public QueryTool2 getAllPuntoCentExtOfTipo(String codTipo) {
    QueryTool2 qt = PuntoCent.getAllPuntoCent();

    qt.putWhereType("CD_TPCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_TPCENTI", codTipo);
    qt.putOperator("CD_TPCENTI", "=");

    QueryTool qtAdic = new QueryTool();
    qtAdic.putName("SIVE_CONGLOM");
    qtAdic.putType("DS_CONGLO", QueryTool.STRING);
    Data dtAdic = new Data();
    dtAdic.put("CD_CONGLO", QueryTool.STRING);
    qt.addQueryTool(qtAdic);
    qt.addColumnsQueryTool(dtAdic);
    return qt;
  }

}