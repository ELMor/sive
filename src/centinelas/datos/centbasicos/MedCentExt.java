//Estructura de datos de un m�dico centinela a�adiendo descipci�n

package centinelas.datos.centbasicos;

import java.util.Enumeration;

import sapp2.Data;

public class MedCentExt
    extends MedCent {

  public MedCentExt() {
  }

  // Constructor a partir de un Data
  public MedCentExt(Data d) {
    Enumeration e1 = d.keys();
    Enumeration e2 = d.elements();

    for (e1 = d.keys(), e2 = d.elements(); e1.hasMoreElements(); ) {
      this.put( (String) e1.nextElement(), (String) e2.nextElement());
    }

    // Campos que pueden ser nulos
    if (d.get("CD_PCENTI") == null) {
      this.put("CD_PCENTI", "");
    }
    if (d.get("CD_MEDCEN") == null) {
      this.put("CD_MEDCEN", "");
    }
    if (d.get("CD_E_NOTIF") == null) {
      this.put("CD_E_NOTIF", "");
    }
    if (d.get("CD_SEXO") == null) {
      this.put("CD_SEXO", "");
    }
    if (d.get("CD_MASIST") == null) {
      this.put("CD_MASIST", "");
    }
    if (d.get("CD_ANOEPIA") == null) {
      this.put("CD_ANOEPIA", "");
    }
    if (d.get("CD_SEMEPIA") == null) {
      this.put("CD_SEMEPIA", "");
    }
    if (d.get("DS_APE1") == null) {
      this.put("DS_APE1", "");
    }
    if (d.get("DS_APE2") == null) {
      this.put("DS_APE2", "");
    }
    if (d.get("DS_NOMBRE") == null) {
      this.put("DS_NOMBRE", "");
    }
    if (d.get("DS_DNI") == null) {
      this.put("DS_DNI", "");
    }
    if (d.get("DS_TELEF") == null) {
      this.put("DS_TELEF", "");
    }
    if (d.get("DS_FAX") == null) {
      this.put("DS_FAX", "");
    }
    if (d.get("FC_NAC") == null) {
      this.put("FC_NAC", "");
    }
    if (d.get("FC_ALTA") == null) {
      this.put("FC_ALTA", "");
    }
    if (d.get("NMREDADI") == null) {
      this.put("NMREDADI", "");
    }
    if (d.get("NMREDADF") == null) {
      this.put("NMREDADF", "");
    }
    if (d.get("DS_HORAI") == null) {
      this.put("DS_HORAI", "");
    }
    if (d.get("DS_HORAF") == null) {
      this.put("DS_HORAF", "");
    }
    if (d.get("IT_BAJA") == null) {
      this.put("IT_BAJA", "");
    }
    if (d.get("FC_BAJA") == null) {
      this.put("FC_BAJA", "");
    }
    if (d.get("CD_MOTBAJA") == null) {
      this.put("CD_MOTBAJA", "");
    }
    if (d.get("CD_OPE") == null) {
      this.put("CD_OPE", "");
    }
    if (d.get("FC_ULTACT") == null) {
      this.put("FC_ULTACT", "");
    }

    //Campos a�adidos
    if (d.get("DS_PCENTI") == null) {
      this.put("DS_PCENTI", "");
    }
  }

}