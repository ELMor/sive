/**
 * Clase: CasoCent
 * Paquete: centinelas.datos.centbasicos
 * Hereda:
 * Autor: LRG
 * Fecha Inicio: 12/01/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Notificaciones/Casos
 * Descripcion: Estructura de datos de un caso
 * JMT (12/01/2000) Constructor por defecto y a partir de un Data
 * JMT (14/01/2000) Queries m�s utilizadas sobre CasoCent
 */

package centinelas.datos.centbasicos;

import java.util.Enumeration;
import java.util.Vector;

import sapp2.Data;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class CasoCent
    extends Data {

  // Constructor por defecto
  public CasoCent() {
  }

  // Constructor a partir de un Data
  public CasoCent(Data d) {
    Enumeration e1 = d.keys();
    Enumeration e2 = d.elements();

    for (e1 = d.keys(), e2 = d.elements(); e1.hasMoreElements(); ) {
      this.put( (String) e1.nextElement(), (String) e2.nextElement());
    }

    // Campos que pueden ser nulos
    if (d.get("CD_ENFCIE") == null) {
      this.put("CD_ENFCIE", "");
    }
    if (d.get("CD_PCENTI") == null) {
      this.put("CD_PCENTI", "");
    }
    if (d.get("CD_MEDCEN") == null) {
      this.put("CD_MEDCEN", "");
    }
    if (d.get("CD_ANOEPI") == null) {
      this.put("CD_ANOEPI", "");
    }
    if (d.get("CD_SEMEPI") == null) {
      this.put("CD_SEMEPI", "");
    }
    if (d.get("NM_ORDEN") == null) {
      this.put("NM_ORDEN", "");
    }
    if (d.get("CD_SEXO") == null) {
      this.put("CD_SEXO", "");
    }
    if (d.get("FC_NAC") == null) {
      this.put("FC_NAC", "");
    }
    if (d.get("IT_CALC") == null) {
      this.put("IT_CALC", "");
    }
    if (d.get("CD_OPE") == null) {
      this.put("CD_OPE", "");
    }
    if (d.get("FC_ULTACT") == null) {
      this.put("FC_ULTACT", "");
    }

  }

  public String getCD_ENFCIE() {
    return getString("CD_ENFCIE");
  } // Fin getCD_ENFCIE()

  public String getCD_PCENTI() {
    return getString("CD_PCENTI");
  } // Fin getCD_PCENTI()

  public String getCD_MEDCEN() {
    return getString("CD_MEDCEN");
  } // Fin getCD_MEDCEN()

  public String getCD_ANOEPI() {
    return getString("CD_ANOEPI");
  } // Fin getCD_ANOEPI()

  public String getCD_SEMEPI() {
    return getString("CD_SEMEPI");
  } // Fin getCD_SEMEPI()

  public String getNM_ORDEN() {
    return getString("NM_ORDEN");
  } // Fin getNM_ORDEN()

  public String getCD_SEXO() {
    return getString("CD_SEXO");
  } // Fin getCD_SEXO()

  public String getFC_NAC() {
    return getString("FC_NAC");
  } // Fin getFC_NAC()

  public String getIT_CALC() {
    return getString("IT_CALC");
  } // Fin getIT_CALC()

  public String getCD_OPE() {
    return getString("CD_OPE");
  } // Fin getCD_OPE()

  public String getFC_ULTACT() {
    return getString("FC_ULTACT");
  } // Fin getFC_ULTACT()

  public void setCD_OPE(String o) {
    remove("CD_OPE");
    put("CD_OPE", o);
  } // Fin getCD_OPE()

  public void setFC_ULTACT(String f) {
    remove("FC_ULTACT");
    put("FC_ULTACT", f);
  } // Fin getFC_ULTACT()

//_______________________________________________________

  // SELECT * FROM SIVE_CASOS_CENT
  public static QueryTool getAllCasoCent() {
    QueryTool qt = new QueryTool();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_CASOS_CENT");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("CD_ENFCIE", QueryTool.STRING);
    qt.putType("CD_ANOEPI", QueryTool.STRING);
    qt.putType("CD_SEMEPI", QueryTool.STRING);
    qt.putType("NM_ORDEN", QueryTool.INTEGER);
    qt.putType("CD_SEXO", QueryTool.STRING);
    qt.putType("FC_NAC", QueryTool.DATE);
    qt.putType("IT_CALC", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    return qt;
  } // Fin getAllCasoCent()

//_______________________________________________________

  // SELECT * FROM SIVE_CASOS_CENT WHERE CD_PCENTI=? AND
  //  AND CD_ENFCIE=? AND CD_MEDCEN=? AND CD_ANOEPI=? AND
  //  CD_SEMEPI=? AND NM_ORDEN=?
  public static QueryTool2 getOneCasoCent(String p,
                                          String e,
                                          String m,
                                          String a,
                                          String s,
                                          String o) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_CASOS_CENT");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("CD_ENFCIE", QueryTool.STRING);
    qt.putType("CD_ANOEPI", QueryTool.STRING);
    qt.putType("CD_SEMEPI", QueryTool.STRING);
    qt.putType("NM_ORDEN", QueryTool.INTEGER);
    qt.putType("CD_SEXO", QueryTool.STRING);
    qt.putType("FC_NAC", QueryTool.DATE);
    qt.putType("IT_CALC", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", p);
    qt.putOperator("CD_PCENTI", "=");
    qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qt.putWhereValue("CD_ENFCIE", e);
    qt.putOperator("CD_ENFCIE", "=");
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", m);
    qt.putOperator("CD_MEDCEN", "=");
    qt.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qt.putWhereValue("CD_ANOEPI", a);
    qt.putOperator("CD_ANOEPI", "=");
    qt.putWhereType("CD_SEMEPI", QueryTool.STRING);
    qt.putWhereValue("CD_SEMEPI", s);
    qt.putOperator("CD_SEMEPI", "=");
    qt.putWhereType("NM_ORDEN", QueryTool.INTEGER);
    qt.putWhereValue("NM_ORDEN", o);
    qt.putOperator("NM_ORDEN", "=");

    return qt;
  } // Fin getOneCasoCent()

  // INSERT INTO SIVE_CASOS_CENT (...) VALUES (...);
  public static QueryTool2 getInsertOneCasoCent(CasoCent c) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_CASOS_CENT");

    // Campos que se quieren insertar
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("CD_ENFCIE", QueryTool.STRING);
    qt.putType("CD_ANOEPI", QueryTool.STRING);
    qt.putType("CD_SEMEPI", QueryTool.STRING);
    qt.putType("NM_ORDEN", QueryTool.INTEGER);
    qt.putType("CD_SEXO", QueryTool.STRING);
    qt.putType("FC_NAC", QueryTool.DATE);
    qt.putType("IT_CALC", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Valores del nuevo registro
    qt.putValue("CD_PCENTI", c.getCD_PCENTI());
    qt.putValue("CD_MEDCEN", c.getCD_MEDCEN());
    qt.putValue("CD_ENFCIE", c.getCD_ENFCIE());
    qt.putValue("CD_ANOEPI", c.getCD_ANOEPI());
    qt.putValue("CD_SEMEPI", c.getCD_SEMEPI());
    qt.putValue("NM_ORDEN", c.getNM_ORDEN());
    qt.putValue("CD_SEXO", c.getCD_SEXO());
    qt.putValue("FC_NAC", c.getFC_NAC());
    qt.putValue("IT_CALC", c.getIT_CALC());
    qt.putValue("CD_OPE", c.getCD_OPE());
    qt.putValue("FC_ULTACT", c.getFC_ULTACT());

    return qt;
  } // Fin getInsertOneCasoCent()

  // UPDATE SIVE_CASOS_CENT SET ... WHERE CD_PCENTI=? AND
  //  AND CD_ENFCIE=? AND CD_MEDCEN=? AND CD_ANOEPI=? AND
  //  CD_SEMEPI=? AND NM_ORDEN=?
  public static QueryTool2 getUpdateOneCasoCent(CasoCent c) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_CASOS_CENT");

    // Campos que se quieren actualizar
    //qt.putType("CD_PCENTI",QueryTool.STRING);
    //qt.putType("CD_MEDCEN",QueryTool.STRING);
    //qt.putType("CD_ENFCIE",QueryTool.STRING);
    //qt.putType("CD_ANOEPI",QueryTool.STRING);
    //qt.putType("CD_SEMEPI",QueryTool.STRING);
    //qt.putType("NM_ORDEN",QueryTool.INTEGER);
    qt.putType("CD_SEXO", QueryTool.STRING);
    qt.putType("FC_NAC", QueryTool.DATE);
    qt.putType("IT_CALC", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Valores de actualizaci�n
    //qt.putValue("CD_PCENTI",c.getCD_PCENTI());
    //qt.putValue("CD_MEDCEN",c.getCD_MEDCEN());
    //qt.putValue("CD_ENFCIE",c.getCD_ENFCIE());
    //qt.putValue("CD_ANOEPI",c.getCD_ANOEPI());
    //qt.putValue("CD_SEMEPI",c.getCD_SEMEPI());
    //qt.putValue("NM_ORDEN",c.getNM_ORDEN());
    qt.putValue("CD_SEXO", c.getCD_SEXO());
    qt.putValue("FC_NAC", c.getFC_NAC());
    qt.putValue("IT_CALC", c.getIT_CALC());
    qt.putValue("CD_OPE", c.getCD_OPE());
    qt.putValue("FC_ULTACT", c.getFC_ULTACT());

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", c.getCD_PCENTI());
    qt.putOperator("CD_PCENTI", "=");
    qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qt.putWhereValue("CD_ENFCIE", c.getCD_ENFCIE());
    qt.putOperator("CD_ENFCIE", "=");
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", c.getCD_MEDCEN());
    qt.putOperator("CD_MEDCEN", "=");
    qt.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qt.putWhereValue("CD_ANOEPI", c.getCD_ANOEPI());
    qt.putOperator("CD_ANOEPI", "=");
    qt.putWhereType("CD_SEMEPI", QueryTool.STRING);
    qt.putWhereValue("CD_SEMEPI", c.getCD_SEMEPI());
    qt.putOperator("CD_SEMEPI", "=");
    qt.putWhereType("NM_ORDEN", QueryTool.INTEGER);
    qt.putWhereValue("NM_ORDEN", c.getNM_ORDEN());
    qt.putOperator("NM_ORDEN", "=");

    return qt;
  } // Fin getUpdateOneCasoCent()

  // DELETE FROM SIVE_CASOS_CENT WHERE CD_PCENTI=? AND
  //  AND CD_ENFCIE=? AND CD_MEDCEN=? AND CD_ANOEPI=? AND
  //  CD_SEMEPI=? AND NM_ORDEN=?
  public static QueryTool2 getDeleteOneCasoCent(CasoCent c) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_CASOS_CENT");

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", c.getCD_PCENTI());
    qt.putOperator("CD_PCENTI", "=");
    qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qt.putWhereValue("CD_ENFCIE", c.getCD_ENFCIE());
    qt.putOperator("CD_ENFCIE", "=");
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", c.getCD_MEDCEN());
    qt.putOperator("CD_MEDCEN", "=");
    qt.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qt.putWhereValue("CD_ANOEPI", c.getCD_ANOEPI());
    qt.putOperator("CD_ANOEPI", "=");
    qt.putWhereType("CD_SEMEPI", QueryTool.STRING);
    qt.putWhereValue("CD_SEMEPI", c.getCD_SEMEPI());
    qt.putOperator("CD_SEMEPI", "=");
    qt.putWhereType("NM_ORDEN", QueryTool.INTEGER);
    qt.putWhereValue("NM_ORDEN", c.getNM_ORDEN());
    qt.putOperator("NM_ORDEN", "=");

    return qt;
  } // Fin getDeleteOneCasoCent()

  // Otras queries

  // SELECT * FROM SIVE_CASOS_CENT WHERE CD_PCENTI=? AND
  //  AND CD_ENFCIE=? AND CD_MEDCEN=? AND CD_ANOEPI=? AND
  //  CD_SEMEPI=?
  public static QueryTool2 getCasoCentPEMAS(String p,
                                            String e,
                                            String m,
                                            String a,
                                            String s) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_CASOS_CENT");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("CD_ENFCIE", QueryTool.STRING);
    qt.putType("CD_ANOEPI", QueryTool.STRING);
    qt.putType("CD_SEMEPI", QueryTool.STRING);
    qt.putType("NM_ORDEN", QueryTool.INTEGER);
    qt.putType("CD_SEXO", QueryTool.STRING);
    qt.putType("FC_NAC", QueryTool.DATE);
    qt.putType("IT_CALC", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", p);
    qt.putOperator("CD_PCENTI", "=");
    qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qt.putWhereValue("CD_ENFCIE", e);
    qt.putOperator("CD_ENFCIE", "=");
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", m);
    qt.putOperator("CD_MEDCEN", "=");
    qt.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qt.putWhereValue("CD_ANOEPI", a);
    qt.putOperator("CD_ANOEPI", "=");
    qt.putWhereType("CD_SEMEPI", QueryTool.STRING);
    qt.putWhereValue("CD_SEMEPI", s);
    qt.putOperator("CD_SEMEPI", "=");

    return qt;
  } // Fin getCasoCentPEMAS()

  // SELECT NM_ORDEN FROM SIVE_CASOS_CENT WHERE NM_ORDEN IN
  //  (SELECT MAX(NM_ORDEN) FROM SIVE_CASOS_CENT WHERE
  //   CD_ENFCIE=? AND CD_PCENTI=? AND CD_MEDCEN=? AND CD_ANOEPI=? AND CD_SEMEPI=?)
  public static QueryTool2 getMaxNM_ORDEN(String eCent,
                                          String pCent,
                                          String mCent,
                                          String aEpi,
                                          String sEpi) {
    QueryTool2 qt = new QueryTool2();
    String subquery = "SELECT MAX(NM_ORDEN) FROM SIVE_CASOS_CENT WHERE CD_ENFCIE=? AND CD_PCENTI=? AND CD_MEDCEN=? AND CD_ANOEPI=? AND CD_SEMEPI=?";
    Vector vSubquery = new Vector();
    Data dtEnfCie = new Data();
    Data dtPCenti = new Data();
    Data dtMedCen = new Data();
    Data dtAnoEpi = new Data();
    Data dtSemEpi = new Data();

    // Tabla
    qt.putName("SIVE_CASOS_CENT");

    // Campos
    qt.putType("NM_ORDEN", QueryTool.INTEGER);

    // Establecemos la subquery y sus (tipos,valores)
    dtEnfCie.put(new Integer(QueryTool.STRING), eCent);
    vSubquery.addElement(dtEnfCie);
    dtPCenti.put(new Integer(QueryTool.STRING), pCent);
    vSubquery.addElement(dtPCenti);
    dtMedCen.put(new Integer(QueryTool.STRING), mCent);
    vSubquery.addElement(dtMedCen);
    dtAnoEpi.put(new Integer(QueryTool.STRING), aEpi);
    vSubquery.addElement(dtAnoEpi);
    dtSemEpi.put(new Integer(QueryTool.STRING), sEpi);
    vSubquery.addElement(dtSemEpi);
    qt.putSubquery("NM_ORDEN", subquery);
    qt.putVectorSubquery("NM_ORDEN", vSubquery);

    return qt;

  } // Fin getMaxNM_ORDEN()

  //Borra las respuestas de un caso
  // DELETE FROM SIVE_RESP_CENTI WHERE CD_PCENTI=? AND
  //  AND CD_ENFCIE=? AND CD_MEDCEN=? AND CD_ANOEPI=? AND
  //  CD_SEMEPI=? AND NM_ORDEN=?
  public static QueryTool2 getDeleteRespCentiOfOneCasoCent(CasoCent c) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_RESP_CENTI");

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", c.getCD_PCENTI());
    qt.putOperator("CD_PCENTI", "=");
    qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qt.putWhereValue("CD_ENFCIE", c.getCD_ENFCIE());
    qt.putOperator("CD_ENFCIE", "=");
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", c.getCD_MEDCEN());
    qt.putOperator("CD_MEDCEN", "=");
    qt.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qt.putWhereValue("CD_ANOEPI", c.getCD_ANOEPI());
    qt.putOperator("CD_ANOEPI", "=");
    qt.putWhereType("CD_SEMEPI", QueryTool.STRING);
    qt.putWhereValue("CD_SEMEPI", c.getCD_SEMEPI());
    qt.putOperator("CD_SEMEPI", "=");
    qt.putWhereType("NM_ORDEN", QueryTool.INTEGER);
    qt.putWhereValue("NM_ORDEN", c.getNM_ORDEN());
    qt.putOperator("NM_ORDEN", "=");

    return qt;
  } // Fin getDeleteOneCasoCent()

} // endclass CasoCent