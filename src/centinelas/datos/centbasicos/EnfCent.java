/**
 * Clase: EnfCent
 * Paquete: centinelas.datos.centbasicos
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 17/01/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Notificaciones/Casos
 * Descripcion: Estructura de datos de una enfermedad centinela.
 *   Incorpora la descripci�n del proceso al que representa.
 * JMT (17/01/2000) Queries m�s utilizadas sobre EnfCent
 */

package centinelas.datos.centbasicos;

import java.util.Enumeration;

import sapp2.Data;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class EnfCent
    extends Data {

  // Constructor por defecto
  public EnfCent() {
  }

  // Constructor con los valores de los campos
  public EnfCent(String sCD_ENFCIE, String sIT_ENVCNE, String sFC_ALTA,
                 String sIT_RUTINA, String sCD_SEMINIP, String sCD_SEMFINP,
                 String sFC_BAJA, String sCD_ANOBAJA, String sCD_SEMBAJA,
                 String sCD_OPE, String sFC_ULTACT) {

    if (sCD_ENFCIE == null) {
      put("CD_ENFCIE", "");
    }
    else {
      put("CD_ENFCIE", sCD_ENFCIE);
    }
    if (sIT_ENVCNE == null) {
      put("IT_ENVCNE", "");
    }
    else {
      put("IT_ENVCNE", sIT_ENVCNE);
    }
    if (sFC_ALTA == null) {
      put("FC_ALTA", "");
    }
    else {
      put("FC_ALTA", sFC_ALTA);
    }
    if (sIT_RUTINA == null) {
      put("IT_RUTINA", "");
    }
    else {
      put("IT_RUTINA", sIT_RUTINA);
    }
    if (sCD_SEMINIP == null) {
      put("CD_SEMINIP", "");
    }
    else {
      put("CD_SEMINIP", sCD_SEMINIP);
    }
    if (sCD_SEMFINP == null) {
      put("CD_SEMFINP", "");
    }
    else {
      put("CD_SEMFINP", sCD_SEMFINP);
    }
    if (sCD_OPE == null) {
      put("CD_OPE", "");
    }
    else {
      put("CD_OPE", sCD_OPE);
    }
    if (sFC_BAJA == null) {
      put("FC_BAJA", "");
    }
    else {
      put("FC_BAJA", sFC_BAJA);
    }
    if (sCD_ANOBAJA == null) {
      put("CD_ANOBAJA", "");
    }
    else {
      put("CD_ANOBAJA", sCD_ANOBAJA);
    }
    if (sCD_SEMBAJA == null) {
      put("CD_SEMBAJA", "");
    }
    else {
      put("CD_SEMBAJA", sCD_SEMBAJA);
    }
    if (sFC_ULTACT == null) {
      put("FC_ULTACT", "");
    }
    else {
      put("FC_ULTACT", sFC_ULTACT);
    }
  }

  // Constructor a partir de un Data
  public EnfCent(Data d) {
    Enumeration e1 = d.keys();
    Enumeration e2 = d.elements();

    for (e1 = d.keys(), e2 = d.elements(); e1.hasMoreElements(); ) {
      this.put( (String) e1.nextElement(), (String) e2.nextElement());
    }

    // Campos que pueden ser nulos
    if (d.get("CD_ENFCIE") == null) {
      this.put("CD_ENFCIE", "");
    }
    if (d.get("IT_ENVCNE") == null) {
      this.put("IT_ENVCNE", "");
    }
    if (d.get("FC_ALTA") == null) {
      this.put("FC_ALTA", "");
    }
    if (d.get("IT_RUTINA") == null) {
      this.put("IT_RUTINA", "");
    }
    if (d.get("CD_SEMINIP") == null) {
      this.put("CD_SEMINIP", "");
    }
    if (d.get("CD_SEMFINP") == null) {
      this.put("CD_SEMFINP", "");
    }
    if (d.get("FC_BAJA") == null) {
      this.put("FC_BAJA", "");
    }
    if (d.get("CD_ANOBAJA") == null) {
      this.put("CD_ANOBAJA", "");
    }
    if (d.get("CD_SEMBAJA") == null) {
      this.put("DS_SEMBAJA", "");
    }
    if (d.get("CD_OPE") == null) {
      this.put("CD_OPE", "");
    }
    if (d.get("FC_ULTACT") == null) {
      this.put("FC_ULTACT", "");
    }

  }

  public String getCD_ENFCIE() {
    return getString("CD_ENFCIE");
  } // Fin getCD_ENFCIE()

  public String getDS_ENFCIE() {
    return getString("DS_ENFCIE");
  } // Fin getDS_ENFCIE()

  public String getIT_ENVCNE() {
    return getString("IT_ENVCNE");
  } // Fin getIT_ENVCNE()

  public String getFC_ALTA() {
    return getString("FC_ALTA");
  } // Fin getFC_ALTA()

  public String getIT_RUTINA() {
    return getString("IT_RUTINA");
  } // Fin getIT_RUTINA()

  public String getCD_SEMINIP() {
    return getString("CD_SEMINIP");
  } // Fin getCD_SEMINIP()

  public String getCD_SEMFINP() {
    return getString("CD_SEMFINP");
  } // Fin getCD_SEMFINP()

  public String getFC_BAJA() {
    return getString("FC_BAJA");
  } // Fin getFC_BAJA()

  public String getCD_ANOBAJA() {
    return getString("CD_ANOBAJA");
  } // Fin getCD_ANOBAJA()

  public String getCD_SEMBAJA() {
    return getString("CD_SEMBAJA");
  } // Fin getCD_SEMBAJA()

  public String getCD_OPE() {
    return getString("CD_OPE");
  } // Fin getCD_OPE()

  public String getFC_ULTACT() {
    return getString("FC_ULTACT");
  } // Fin getFC_ULTACT()

  public void setCD_OPE(String o) {
    remove("CD_OPE");
    put("CD_OPE", o);
  } // Fin getCD_OPE()

  public void setFC_ULTACT(String f) {
    remove("FC_ULTACT");
    put("FC_ULTACT", f);
  } // Fin getFC_ULTACT()

  // SELECT a.*, b.DS_ENFCIE FROM SIVE_ENF_CENTI a, SIVE_PROCESOS b
  // WHERE a.CD_ENFCIE = b.CD_ENFCIE
  public static QueryTool2 getAllEnfCent() {
    QueryTool2 qtGral = new QueryTool2();
    QueryTool qtDesc = new QueryTool();
    Data dtWhere = new Data();

    // Relleno de qtDesc
    qtDesc.putName("SIVE_PROCESOS");
    qtDesc.putType("CD_ENFCIE", QueryTool.STRING);
    qtDesc.putType("DS_PROCESO", QueryTool.STRING);
    dtWhere.put("CD_ENFCIE", QueryTool.STRING);

    // Relleno de qtGral
    qtGral.putName("SIVE_ENF_CENTI");
    qtGral.putType("CD_ENFCIE", QueryTool.STRING);
    qtGral.putType("IT_ENVCNE", QueryTool.STRING);
    qtGral.putType("FC_ALTA", QueryTool.DATE);
    qtGral.putType("IT_RUTINA", QueryTool.STRING);
    qtGral.putType("CD_SEMINIP", QueryTool.STRING);
    qtGral.putType("CD_SEMFINP", QueryTool.STRING);
    qtGral.putType("FC_BAJA", QueryTool.DATE);
    qtGral.putType("CD_ANOBAJA", QueryTool.STRING);
    qtGral.putType("CD_SEMBAJA", QueryTool.STRING);
    qtGral.putType("CD_OPE", QueryTool.STRING);
    qtGral.putType("FC_ULTACT", QueryTool.TIMESTAMP);
    qtGral.addColumnsQueryTool(dtWhere);
    qtGral.addQueryTool(qtDesc);

    return qtGral;
  } // Fin getAllEnfCent()

  // SELECT * FROM SIVE_ENF_CENTI (+DS_PROCESO) WHERE CD_ENFCIE=?
  public static QueryTool2 getOneEnfCent(String cod) {
    QueryTool2 qtGral = new QueryTool2();
    QueryTool qtDesc = new QueryTool();
    Data dtWhere = new Data();

    // Relleno de qtDesc
    qtDesc.putName("SIVE_PROCESOS");
    qtDesc.putType("CD_ENFCIE", QueryTool.STRING);
    qtDesc.putType("DS_PROCESO", QueryTool.STRING);
    dtWhere.put("CD_ENFCIE", QueryTool.STRING);

    // Relleno de qtGral
    qtGral.putName("SIVE_ENF_CENTI");
    qtGral.putType("CD_ENFCIE", QueryTool.STRING);
    qtGral.putType("IT_ENVCNE", QueryTool.STRING);
    qtGral.putType("FC_ALTA", QueryTool.DATE);
    qtGral.putType("IT_RUTINA", QueryTool.STRING);
    qtGral.putType("CD_SEMINIP", QueryTool.STRING);
    qtGral.putType("CD_SEMFINP", QueryTool.STRING);
    qtGral.putType("FC_BAJA", QueryTool.DATE);
    qtGral.putType("CD_ANOBAJA", QueryTool.STRING);
    qtGral.putType("CD_SEMBAJA", QueryTool.STRING);
    qtGral.putType("CD_OPE", QueryTool.STRING);
    qtGral.putType("FC_ULTACT", QueryTool.TIMESTAMP);
    qtGral.addColumnsQueryTool(dtWhere);
    qtGral.addQueryTool(qtDesc);

    // Parte del where
    qtGral.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qtGral.putWhereValue("CD_ENFCIE", cod);
    qtGral.putOperator("CD_ENFCIE", "=");

    return qtGral;
  } // Fin getOneEnfCent()

  // INSERT INTO SIVE_ENF_CENTI ( ... ) VALUES ( ... );
  public static QueryTool2 getInsertOneEnfCent(EnfCent e) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (INTO)
    qt.putName("SIVE_ENF_CENTI");

    // Campos que se quieren insertar
    qt.putType("CD_ENFCIE", QueryTool.STRING);
    qt.putType("IT_ENVCNE", QueryTool.STRING);
    qt.putType("FC_ALTA", QueryTool.DATE);
    qt.putType("IT_RUTINA", QueryTool.STRING);
    qt.putType("CD_SEMINIP", QueryTool.STRING);
    qt.putType("CD_SEMFINP", QueryTool.STRING);
    qt.putType("FC_BAJA", QueryTool.DATE);
    qt.putType("CD_ANOBAJA", QueryTool.STRING);
    qt.putType("CD_SEMBAJA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Valores
    qt.putValue("CD_ENFCIE", e.getCD_ENFCIE());
    qt.putValue("IT_ENVCNE", e.getIT_ENVCNE());
    qt.putValue("FC_ALTA", e.getFC_ALTA());
    qt.putValue("IT_RUTINA", e.getIT_RUTINA());
    qt.putValue("CD_SEMINIP", e.getCD_SEMINIP());
    qt.putValue("CD_SEMFINP", e.getCD_SEMFINP());
    qt.putValue("FC_BAJA", e.getFC_BAJA());
    qt.putValue("CD_ANOBAJA", e.getCD_ANOBAJA());
    qt.putValue("CD_SEMBAJA", e.getCD_SEMBAJA());
    qt.putValue("CD_OPE", e.getCD_OPE());
    qt.putValue("FC_ULTACT", e.getFC_ULTACT());

    return qt;
  } // Fin getInsertOneEnfCent()

  // UPDATE SIVE_ENF_CENTI SET ... WHERE CD_ENFCIE=?
  public static QueryTool2 getUpdateOneEnfCent(EnfCent e) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (INTO)
    qt.putName("SIVE_ENF_CENTI");

    // Campos que se quieren actualizar
    //qt.putType("CD_ENFCIE",QueryTool.STRING);
    qt.putType("IT_ENVCNE", QueryTool.STRING);
    qt.putType("FC_ALTA", QueryTool.DATE);
    qt.putType("IT_RUTINA", QueryTool.STRING);
    qt.putType("CD_SEMINIP", QueryTool.STRING);
    qt.putType("CD_SEMFINP", QueryTool.STRING);
    qt.putType("FC_BAJA", QueryTool.DATE);
    qt.putType("CD_ANOBAJA", QueryTool.STRING);
    qt.putType("CD_SEMBAJA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Valores
    //qt.putValue("CD_ENFCIE",e.getCD_ENFCIE());
    qt.putValue("IT_ENVCNE", e.getIT_ENVCNE());
    qt.putValue("FC_ALTA", e.getFC_ALTA());
    qt.putValue("IT_RUTINA", e.getIT_RUTINA());
    qt.putValue("CD_SEMINIP", e.getCD_SEMINIP());
    qt.putValue("CD_SEMFINP", e.getCD_SEMFINP());
    qt.putValue("FC_BAJA", e.getFC_BAJA());
    qt.putValue("CD_ANOBAJA", e.getCD_ANOBAJA());
    qt.putValue("CD_SEMBAJA", e.getCD_SEMBAJA());
    qt.putValue("CD_OPE", e.getCD_OPE());
    qt.putValue("FC_ULTACT", e.getFC_ULTACT());

    // Parte del where
    qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qt.putWhereValue("CD_ENFCIE", e.getCD_ENFCIE());
    qt.putOperator("CD_ENFCIE", "=");

    return qt;
  } // Fin getUpdateOneEnfCent()

  // DELETE FROM SIVE_ENF_CENTI WHERE CD_ENFCIE=?
  public static QueryTool2 getDeleteOneEnfCent(EnfCent e) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_ENF_CENTI");

    // Parte del where
    qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qt.putWhereValue("CD_ENFCIE", e.getCD_ENFCIE());
    qt.putOperator("CD_ENFCIE", "=");

    return qt;
  } // Fin getDeleteOneEnfCent()

} // endclass EnfCent