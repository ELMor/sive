/**
 * Clase: PuntoCent
 * Paquete: centinelas.datos.centbasicos
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 12/01/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Notificaciones/Casos
 * Descripcion: Estructura de datos de un punto centinela.
 * JMT (12/01/2000) Constructor por defecto y a partir de un Data
 * JMT (18/01/2000) Queries m�s utilizadas sobre PuntoCent
 */

package centinelas.datos.centbasicos;

import java.util.Enumeration;

import sapp2.Data;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class PuntoCent
    extends Data {

  // Constructor por defecto
  public PuntoCent() {
  }

  // Constructor a partir de un Data
  public PuntoCent(Data d) {
    Enumeration e1 = d.keys();
    Enumeration e2 = d.elements();

    for (e1 = d.keys(), e2 = d.elements(); e1.hasMoreElements(); ) {
      this.put( (String) e1.nextElement(), (String) e2.nextElement());
    }

    // Campos que pueden ser nulos
    if (d.get("CD_PCENTI") == null) {
      this.put("CD_PCENTI", "");
    }
    if (d.get("DS_PCENTI") == null) {
      this.put("DS_PCENTI", "");
    }
    if (d.get("IT_GRIPE") == null) {
      this.put("IT_GRIPE", "");
    }
    if (d.get("CD_CONGLO") == null) {
      this.put("CD_CONGLO", "");
    }
    if (d.get("CD_TPCENTI") == null) {
      this.put("CD_TPCENTI", "");
    }
    if (d.get("FC_ALTA") == null) {
      this.put("FC_ALTA", "");
    }
    if (d.get("IT_BAJA") == null) {
      this.put("IT_BAJA", "");
    }
    if (d.get("CD_OPE") == null) {
      this.put("CD_OPE", "");
    }
    if (d.get("FC_ULTACT") == null) {
      this.put("FC_ULTACT", "");
    }

  }

  public String getCD_PCENTI() {
    return getString("CD_PCENTI");
  } // Fin getCD_PCENTI()

  public String getDS_PCENTI() {
    return getString("DS_PCENTI");
  } // Fin getDS_PCENTI()

  public String getIT_GRIPE() {
    return getString("IT_GRIPE");
  } // Fin getIT_GRIPE()

  public String getCD_CONGLO() {
    return getString("CD_CONGLO");
  } // Fin getCD_CONGLO()

  public String getCD_TPCENTI() {
    return getString("CD_TPCENTI");
  } // Fin getCD_TPCENTI()

  public String getFC_ALTA() {
    return getString("FC_ALTA");
  } // Fin getFC_ALTA()

  public String getIT_BAJA() {
    return getString("IT_BAJA");
  } // Fin getIT_BAJA()

  public String getCD_OPE() {
    return getString("CD_OPE");
  } // Fin getCD_OPE()

  public String getFC_ULTACT() {
    return getString("FC_ULTACT");
  } // Fin getFC_ULTACT()

  public void setCD_OPE(String o) {
    remove("CD_OPE");
    put("CD_OPE", o);
  } // Fin getCD_OPE()

  public void setFC_ULTACT(String f) {
    remove("FC_ULTACT");
    put("FC_ULTACT", f);
  } // Fin getFC_ULTACT()

  // SELECT * FROM SIVE_PCENTINELA
  public static QueryTool2 getAllPuntoCent() {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_PCENTINELA");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("DS_PCENTI", QueryTool.STRING);
    qt.putType("CD_CONGLO", QueryTool.STRING);
    qt.putType("CD_TPCENTI", QueryTool.STRING);
    qt.putType("IT_GRIPE", QueryTool.STRING);
    qt.putType("FC_ALTA", QueryTool.DATE);
    qt.putType("IT_BAJA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    return qt;
  } // Fin getAllPuntoCent()

  // SELECT * FROM SIVE_PCENTINELA WHERE CD_PCENTI=?
  public static QueryTool2 getOnePuntoCent(String cod) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_PCENTINELA");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("DS_PCENTI", QueryTool.STRING);
    qt.putType("CD_CONGLO", QueryTool.STRING);
    qt.putType("CD_TPCENTI", QueryTool.STRING);
    qt.putType("IT_GRIPE", QueryTool.STRING);
    qt.putType("FC_ALTA", QueryTool.DATE);
    qt.putType("IT_BAJA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", cod);
    qt.putOperator("CD_PCENTI", "=");

    return qt;
  } // Fin getOnePuntoCent()

  // INSERT INTO SIVE_PCENTINELA (CD_PCENTI,DS_PCENTI,CD_CONGLO,
  //  CD_TPCENTI,IT_GRIPE,FC_ALTA,IT_BAJA,CD_OPE,FC_ULTACT)
  //  VALUES (?,?,?,?,?,?,?,?,?);
  public static QueryTool2 getInsertOnePuntoCent(PuntoCent p) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_PCENTINELA");

    // Campos que se quieren insertar
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("DS_PCENTI", QueryTool.STRING);
    qt.putType("CD_CONGLO", QueryTool.STRING);
    qt.putType("CD_TPCENTI", QueryTool.STRING);
    qt.putType("IT_GRIPE", QueryTool.STRING);
    qt.putType("FC_ALTA", QueryTool.DATE);
    qt.putType("IT_BAJA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Valores del nuevo registro
    qt.putValue("CD_PCENTI", p.getCD_PCENTI());
    qt.putValue("DS_PCENTI", p.getDS_PCENTI());
    qt.putValue("CD_CONGLO", p.getCD_CONGLO());
    qt.putValue("CD_TPCENTI", p.getCD_TPCENTI());
    qt.putValue("IT_GRIPE", p.getIT_GRIPE());
    qt.putValue("FC_ALTA", p.getFC_ALTA());
    qt.putValue("IT_BAJA", p.getIT_BAJA());
    qt.putValue("CD_OPE", p.getCD_OPE());
    qt.putValue("FC_ULTACT", p.getFC_ULTACT());

    return qt;
  } // Fin getInsertOnePuntoCent()

  // UPDATE SIVE_PCENTINELA SET DS_PCENTI=? AND CD_CONGLO=?
  //  AND CD_TPCENTI=? AND IT_GRIPE=? AND FC_ALTA=?
  //  AND IT_BAJA=? AND CD_OPE=? AND FC_ULTACT=?
  //  WHERE CD_PCENTI=?
  public static QueryTool2 getUpdateOnePuntoCent(PuntoCent p) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_PCENTINELA");

    // Campos que se quieren actualizar
    //qt.putType("CD_PCENTI",QueryTool.STRING);
    qt.putType("DS_PCENTI", QueryTool.STRING);
    qt.putType("CD_CONGLO", QueryTool.STRING);
    qt.putType("CD_TPCENTI", QueryTool.STRING);
    qt.putType("IT_GRIPE", QueryTool.STRING);
    qt.putType("FC_ALTA", QueryTool.DATE);
    qt.putType("IT_BAJA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Valores de actualizaci�n
    //qt.putValue("CD_PCENTI",p.getCD_PCENTI());
    qt.putValue("DS_PCENTI", p.getDS_PCENTI());
    qt.putValue("CD_CONGLO", p.getCD_CONGLO());
    qt.putValue("CD_TPCENTI", p.getCD_TPCENTI());
    qt.putValue("IT_GRIPE", p.getIT_GRIPE());
    qt.putValue("FC_ALTA", p.getFC_ALTA());
    qt.putValue("IT_BAJA", p.getIT_BAJA());
    qt.putValue("CD_OPE", p.getCD_OPE());
    qt.putValue("FC_ULTACT", p.getFC_ULTACT());

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", p.getCD_PCENTI());
    qt.putOperator("CD_PCENTI", "=");

    return qt;
  } // Fin getUpdateOnePuntoCent()

  // DELETE FROM SIVE_PCENTINELA WHERE CD_PCENTI=?
  public static QueryTool2 getDeleteOnePuntoCent(PuntoCent p) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_PCENTINELA");

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", p.getCD_PCENTI());
    qt.putOperator("CD_PCENTI", "=");

    return qt;
  } // Fin getDeleteOnePuntoCent()

} // endclass PuntoCent