/**
 * Clase: NotifRMC
 * Paquete: centinelas.datos.centbasicos
 * Hereda:
 * Autor: LRG
 * Fecha Inicio: 12/01/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Notificaciones/Casos
 * Descripcion: Estructura de datos de una notificaci�n
 * JMT (12/01/2000) Constructor por defecto y a partir de un Data
 * JMT (14/01/2000) Queries m�s utilizadas sobre NotifRMC
 */

package centinelas.datos.centbasicos;

import java.util.Enumeration;

import sapp2.Data;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class NotifRMC
    extends Data {

  // Constructor por defecto
  public NotifRMC() {
  }

  public NotifRMC(String sCD_ENFCIE, String sCD_PCENTI, String sCD_MEDCEN,
                  String sCD_ANOEPI, String sCD_SEMEPI, String sNM_CASOSDEC,
                  String sFC_RECEP, String sIT_COBERTURA,
                  String sCD_OPE, String sFC_ULTACT) {

    if (sCD_ENFCIE == null) {
      put("CD_ENFCIE", "");
    }
    else {
      put("CD_ENFCIE", sCD_ENFCIE);
    }
    if (sCD_PCENTI == null) {
      put("CD_PCENTI", "");
    }
    else {
      put("CD_PCENTI", sCD_PCENTI);
    }
    if (sCD_MEDCEN == null) {
      put("CD_MEDCEN", "");
    }
    else {
      put("CD_MEDCEN", sCD_MEDCEN);
    }
    if (sCD_ANOEPI == null) {
      put("CD_ANOEPI", "");
    }
    else {
      put("CD_ANOEPI", sCD_ANOEPI);
    }
    if (sCD_SEMEPI == null) {
      put("CD_SEMEPI", "");
    }
    else {
      put("CD_SEMEPI", sCD_SEMEPI);
    }
    if (sNM_CASOSDEC == null) {
      put("NM_CASOSDEC", "");
    }
    else {
      put("NM_CASOSDEC", sNM_CASOSDEC);
    }
    if (sFC_RECEP == null) {
      put("FC_RECEP", "");
    }
    else {
      put("FC_RECEP", sFC_RECEP);
    }
    if (sIT_COBERTURA == null) {
      put("IT_COBERTURA", "");
    }
    else {
      put("IT_COBERTURA", sIT_COBERTURA);
    }
    if (sCD_OPE == null) {
      put("CD_OPE", "");
    }
    else {
      put("CD_OPE", sCD_OPE);
    }
    if (sFC_ULTACT == null) {
      put("FC_ULTACT", "");
    }
    else {
      put("FC_ULTACT", sFC_ULTACT);
    }
  }

  // Constructor a partir de un Data
  public NotifRMC(Data d) {
    Enumeration e1 = d.keys();
    Enumeration e2 = d.elements();

    for (e1 = d.keys(), e2 = d.elements(); e1.hasMoreElements(); ) {
      this.put( (String) e1.nextElement(), (String) e2.nextElement());
    }

    // Campos que pueden ser nulos
    if (d.get("CD_ENFCIE") == null) {
      this.put("CD_ENFCIE", "");
    }
    if (d.get("CD_PCENTI") == null) {
      this.put("CD_PCENTI", "");
    }
    if (d.get("CD_MEDCEN") == null) {
      this.put("CD_MEDCEN", "");
    }
    if (d.get("CD_ANOEPI") == null) {
      this.put("CD_ANOEPI", "");
    }
    if (d.get("CD_SEMEPI") == null) {
      this.put("CD_SEMEPI", "");
    }
    if (d.get("NM_CASOSDEC") == null) {
      this.put("NM_CASOSDEC", "");
    }
    if (d.get("FC_RECEP") == null) {
      this.put("FC_RECEP", "");
    }
    if (d.get("IT_COBERTURA") == null) {
      this.put("IT_COBERTURA", "");
    }
    if (d.get("CD_OPE") == null) {
      this.put("CD_OPE", "");
    }
    if (d.get("FC_ULTACT") == null) {
      this.put("FC_ULTACT", "");
    }

  }

  public String getCD_ENFCIE() {
    return getString("CD_ENFCIE");
  } // Fin getCD_ENFCIE()

  public String getCD_PCENTI() {
    return getString("CD_PCENTI");
  } // Fin getCD_PCENTI()

  public String getCD_MEDCEN() {
    return getString("CD_MEDCEN");
  } // Fin getCD_MEDCEN()

  public String getCD_ANOEPI() {
    return getString("CD_ANOEPI");
  } // Fin getCD_ANOEPI()

  public String getCD_SEMEPI() {
    return getString("CD_SEMEPI");
  } // Fin getCD_SEMEPI()

  public String getNM_CASOSDEC() {
    return getString("NM_CASOSDEC");
  } // Fin getNM_CASOSDEC()

  public String getFC_RECEP() {
    return getString("FC_RECEP");
  } // Fin getFC_RECEP()

  public String getIT_COBERTURA() {
    return getString("IT_COBERTURA");
  } // Fin getIT_COBERTURA()

  public String getCD_OPE() {
    return getString("CD_OPE");
  } // Fin getCD_OPE()

  public String getFC_ULTACT() {
    return getString("FC_ULTACT");
  } // Fin getFC_ULTACT()

  public void setCD_OPE(String o) {
    remove("CD_OPE");
    put("CD_OPE", o);
  } // Fin getCD_OPE()

  public void setFC_ULTACT(String f) {
    remove("FC_ULTACT");
    put("FC_ULTACT", f);
  } // Fin getFC_ULTACT()

  //________________________________________________________

  // SELECT * FROM SIVE_NOTIF_RMC
  public static QueryTool2 getAllNotifRMC() {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_NOTIF_RMC");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("CD_ENFCIE", QueryTool.STRING);
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("CD_ANOEPI", QueryTool.STRING);
    qt.putType("CD_SEMEPI", QueryTool.STRING);
    qt.putType("NM_CASOSDEC", QueryTool.INTEGER);
    qt.putType("FC_RECEP", QueryTool.DATE);
    qt.putType("IT_COBERTURA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    return qt;
  } // Fin getAllNotifRMC()

  //________________________________________________________

  // SELECT * FROM SIVE_NOTIF_RMC where CD_ENFCIE=? and CD_PCENTI=?= and CD_MEDCENT=?
  // and CD_ANOEPI=? and CD_SEMEPI=?
  public static QueryTool2 getOneNotifRMC(String codEnf, String codPunto,
                                          String codMed,
                                          String ano, String sem) {

    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_NOTIF_RMC");

    // Campos que se quieren leer (SELECT)
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("CD_ENFCIE", QueryTool.STRING);
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("CD_ANOEPI", QueryTool.STRING);
    qt.putType("CD_SEMEPI", QueryTool.STRING);
    qt.putType("NM_CASOSDEC", QueryTool.INTEGER);
    qt.putType("FC_RECEP", QueryTool.DATE);
    qt.putType("IT_COBERTURA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Parte del where
    qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qt.putWhereValue("CD_ENFCIE", codEnf);
    qt.putOperator("CD_ENFCIE", "=");

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", codPunto);
    qt.putOperator("CD_PCENTI", "=");

    // Parte del where
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", codMed);
    qt.putOperator("CD_MEDCEN", "=");

    // Parte del where
    qt.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qt.putWhereValue("CD_ANOEPI", ano);
    qt.putOperator("CD_ANOEPI", "=");

    // Parte del where
    qt.putWhereType("CD_SEMEPI", QueryTool.STRING);
    qt.putWhereValue("CD_SEMEPI", sem);
    qt.putOperator("CD_SEMEPI", "=");

    return qt;
  } // Fin getOneNotifRMC()

  //________________________________________________________

  // INSERT INTO SIVE_NOTIFRMC (...) VALUES (...)
  public static QueryTool2 getInsertOneNotifRMC(NotifRMC n) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_NOTIF_RMC");

    // Campos que se quieren insertar
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("CD_ENFCIE", QueryTool.STRING);
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("CD_ANOEPI", QueryTool.STRING);
    qt.putType("CD_SEMEPI", QueryTool.STRING);
    qt.putType("NM_CASOSDEC", QueryTool.INTEGER);
    qt.putType("FC_RECEP", QueryTool.DATE);
    qt.putType("IT_COBERTURA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Valores a insertar
    qt.putValue("CD_PCENTI", n.getCD_PCENTI());
    qt.putValue("CD_ENFCIE", n.getCD_ENFCIE());
    qt.putValue("CD_MEDCEN", n.getCD_MEDCEN());
    qt.putValue("CD_ANOEPI", n.getCD_ANOEPI());
    qt.putValue("CD_SEMEPI", n.getCD_SEMEPI());
    qt.putValue("NM_CASOSDEC", n.getNM_CASOSDEC());
    qt.putValue("FC_RECEP", n.getFC_RECEP());
    qt.putValue("IT_COBERTURA", n.getIT_COBERTURA());
    qt.putValue("CD_OPE", n.getCD_OPE());
    qt.putValue("FC_ULTACT", n.getFC_ULTACT());

    // Parte del where
    qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qt.putWhereValue("CD_ENFCIE", n.getCD_ENFCIE());
    qt.putOperator("CD_ENFCIE", "=");

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", n.getCD_PCENTI());
    qt.putOperator("CD_PCENTI", "=");

    // Parte del where
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", n.getCD_MEDCEN());
    qt.putOperator("CD_MEDCEN", "=");

    // Parte del where
    qt.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qt.putWhereValue("CD_ANOEPI", n.getCD_ANOEPI());
    qt.putOperator("CD_ANOEPI", "=");

    // Parte del where
    qt.putWhereType("CD_SEMEPI", QueryTool.STRING);
    qt.putWhereValue("CD_SEMEPI", n.getCD_SEMEPI());
    qt.putOperator("CD_SEMEPI", "=");

    return qt;
  } // Fin getInsertOneNotifRMC()

  //________________________________________________________

  // UPDATE SIVE_NOTIFRMC (...) SET (...)
  public static QueryTool2 getUpdateOneNotifRMC(NotifRMC n) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_NOTIF_RMC");

    // Campos que se quieren insertar
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putType("CD_ENFCIE", QueryTool.STRING);
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("CD_ANOEPI", QueryTool.STRING);
    qt.putType("CD_SEMEPI", QueryTool.STRING);
    qt.putType("NM_CASOSDEC", QueryTool.INTEGER);
    qt.putType("FC_RECEP", QueryTool.DATE);
    qt.putType("IT_COBERTURA", QueryTool.STRING);
    qt.putType("CD_OPE", QueryTool.STRING);
    qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Valores a actualizar
    qt.putValue("CD_PCENTI", n.getCD_PCENTI());
    qt.putValue("CD_ENFCIE", n.getCD_ENFCIE());
    qt.putValue("CD_MEDCEN", n.getCD_MEDCEN());
    qt.putValue("CD_ANOEPI", n.getCD_ANOEPI());
    qt.putValue("CD_SEMEPI", n.getCD_SEMEPI());
    qt.putValue("NM_CASOSDEC", n.getNM_CASOSDEC());
    qt.putValue("FC_RECEP", n.getFC_RECEP());
    qt.putValue("IT_COBERTURA", n.getIT_COBERTURA());
    qt.putValue("CD_OPE", n.getCD_OPE());
    qt.putValue("FC_ULTACT", n.getFC_ULTACT());

    // Parte del where
    qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qt.putWhereValue("CD_ENFCIE", n.getCD_ENFCIE());
    qt.putOperator("CD_ENFCIE", "=");

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", n.getCD_PCENTI());
    qt.putOperator("CD_PCENTI", "=");

    // Parte del where
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", n.getCD_MEDCEN());
    qt.putOperator("CD_MEDCEN", "=");

    // Parte del where
    qt.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qt.putWhereValue("CD_ANOEPI", n.getCD_ANOEPI());
    qt.putOperator("CD_ANOEPI", "=");

    // Parte del where
    qt.putWhereType("CD_SEMEPI", QueryTool.STRING);
    qt.putWhereValue("CD_SEMEPI", n.getCD_SEMEPI());
    qt.putOperator("CD_SEMEPI", "=");

    return qt;
  } // Fin getUpdateOneNotifRMC()

  //________________________________________________________

  //DELETE SIVE_NOTIF_RMC WHERE ...
  public static QueryTool2 getDeleteOneNotifRMC(NotifRMC n) {
    QueryTool2 qt = new QueryTool2();

    // Nombre de la tabla (FROM)
    qt.putName("SIVE_NOTIF_RMC");

    // Parte del where
    qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qt.putWhereValue("CD_ENFCIE", n.getCD_ENFCIE());
    qt.putOperator("CD_ENFCIE", "=");

    // Parte del where
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", n.getCD_PCENTI());
    qt.putOperator("CD_PCENTI", "=");

    // Parte del where
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", n.getCD_MEDCEN());
    qt.putOperator("CD_MEDCEN", "=");

    // Parte del where
    qt.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qt.putWhereValue("CD_ANOEPI", n.getCD_ANOEPI());
    qt.putOperator("CD_ANOEPI", "=");

    // Parte del where
    qt.putWhereType("CD_SEMEPI", QueryTool.STRING);
    qt.putWhereValue("CD_SEMEPI", n.getCD_SEMEPI());
    qt.putOperator("CD_SEMEPI", "=");

    return qt;
  } // Fin getDeleteOneNotifRMC()

  // Otras queries

  // SELECT  DISTINCT n.CD_SEMEPI, m.DatosMedico (Cod,Desc,PCent,ENotif,...)
  // FROM SIVE_NOTIF_RMC n, SIVE_MED_CEN m
  // WHERE n.CD_MEDCEN = m.CD_MEDCEN AND n.CD_PCENTI = ? AND n.CD_ANOEPI = ?
  public static QueryTool2 getNotifRMCPCentiAno(String pCent, String ano) {
    QueryTool2 qtGral = new QueryTool2();
    QueryTool qtDetalle = new QueryTool();
    Data dtWhere = new Data();

    // Relleno de qtDesc
    qtDetalle.putName("SIVE_MCENTINELA");
    qtDetalle.putType("CD_PCENTI", QueryTool.STRING);
    qtDetalle.putType("CD_MEDCEN", QueryTool.STRING);
    qtDetalle.putType("CD_E_NOTIF", QueryTool.STRING);
    qtDetalle.putType("CD_SEXO", QueryTool.STRING);
    qtDetalle.putType("CD_MASIST", QueryTool.STRING);
    qtDetalle.putType("CD_ANOEPIA", QueryTool.STRING);
    qtDetalle.putType("CD_SEMEPIA", QueryTool.STRING);
    qtDetalle.putType("DS_APE1", QueryTool.STRING);
    qtDetalle.putType("DS_APE2", QueryTool.STRING);
    qtDetalle.putType("DS_NOMBRE", QueryTool.STRING);
    qtDetalle.putType("DS_DNI", QueryTool.STRING);
    qtDetalle.putType("DS_TELEF", QueryTool.STRING);
    qtDetalle.putType("DS_FAX", QueryTool.STRING);
    qtDetalle.putType("FC_NAC", QueryTool.DATE);
    qtDetalle.putType("FC_ALTA", QueryTool.DATE);
    qtDetalle.putType("CD_OPE", QueryTool.STRING);
    qtDetalle.putType("FC_ULTACT", QueryTool.TIMESTAMP);
    qtDetalle.putType("NMREDADI", QueryTool.INTEGER);
    qtDetalle.putType("NMREDADF", QueryTool.INTEGER);
    qtDetalle.putType("DS_HORAI", QueryTool.STRING);
    qtDetalle.putType("DS_HORAF", QueryTool.STRING);
    qtDetalle.putType("IT_BAJA", QueryTool.STRING);
    qtDetalle.putType("FC_BAJA", QueryTool.DATE);
    qtDetalle.putType("CD_MOTBAJA", QueryTool.STRING);
    dtWhere.put("CD_PCENTI", QueryTool.STRING);
    dtWhere.put("CD_MEDCEN", QueryTool.STRING);

    // Nombre de la tabla (FROM)
    qtGral.putName("SIVE_NOTIF_RMC");

    // Moificador DISTINCT
    qtGral.setDistinct(true);

    // Campos que se quieren leer (SELECT)
    qtGral.putType("CD_PCENTI", QueryTool.STRING);
    qtGral.putType("CD_MEDCEN", QueryTool.STRING);
    qtGral.putType("CD_SEMEPI", QueryTool.STRING);

    // Parte del where
    qtGral.putWhereType("CD_PCENTI", QueryTool.STRING);
    qtGral.putWhereValue("CD_PCENTI", pCent);
    qtGral.putOperator("CD_PCENTI", "=");
    qtGral.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qtGral.putWhereValue("CD_ANOEPI", ano);
    qtGral.putOperator("CD_ANOEPI", "=");

    // Datos adicionales
    qtGral.addColumnsQueryTool(dtWhere);
    qtGral.addQueryTool(qtDetalle);

    return qtGral;
  } // Fin getNotifRMCPCentiAno()

} // endclass NotifRMC