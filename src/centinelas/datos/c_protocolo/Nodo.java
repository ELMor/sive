package centinelas.datos.c_protocolo;

//import java.net.*;
//import java.awt.*;
//import java.awt.event.*;
//import com.borland.jbcl.control.*;
//import com.borland.jbcl.layout.*;
//import capp.*;
//import sapp.*;
//import java.util.*;
//import notindiv.*;
//import bidial.*;

// configuración del árbol
public class Nodo {
  protected int y;
  protected String s;

  public Nodo(String s, int y) {
    this.y = y;
    this.s = s;
  }

  public String getDescripcion() {
    return s;
  }

  public int getY() {
    return y;
  }
}
