//Estructura de datos que representa el c�digo de una semana epidemiol�gica y su fecha de fin de semana
package centinelas.datos.c_fechas;

import java.io.Serializable;

public class FecyNum
    implements Cloneable, Serializable {
  int iNum; //numero de semana ep en el a�o
  java.util.Date dFec; //Fecha correspondte a la semana
  String sFec = null;

  public FecyNum(int num, java.util.Date fec) {
    iNum = num;
    dFec = fec;
  }

  public FecyNum(int num, java.util.Date fec, String s) {
    iNum = num;
    dFec = fec;
    sFec = s;
  }

  public int getnum() {
    return iNum;
  }

  public java.util.Date getFec() {
    return dFec;
  }

  public String getsFec() {
    return sFec;
  }
}
