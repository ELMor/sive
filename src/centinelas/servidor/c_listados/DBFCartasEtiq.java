package centinelas.servidor.c_listados;

import java.io.File;

import sapp2.Data;
import sapp2.Lista;
import xBaseJ.CharField;
import xBaseJ.DBF;
import xBaseJ.Field;

public class DBFCartasEtiq {

  //DBF que se devuelve al SrvCartasEtiq
  DBF DBDev = null;

  //Crea un directorio para DBFs (si no existe ya ) y en �l crea un fichero DBF
  // con nombre prefijado
  public DBFCartasEtiq(Lista vParam) throws Exception {

    String dirDBF = "dbfs";
    String rutaFich = "";

    //Crea dir
    String pathDefecto = System.getProperty("user.dir");
    System.out.println(pathDefecto);
    File miDir = new File(pathDefecto + File.separator + dirDBF);
    miDir.mkdir();

    //Creo un nuevo fichero DBF con nombre ? pasado desde cliente
    rutaFich = pathDefecto + File.separator + dirDBF + File.separator +
        "volcadoBd.dbf";
    creaDBF(vParam, rutaFich);

  }

  //Crea un fichero DBF en la ruta indicada por rutaFich
  //(rutaFich incluye directorios(ya existentes) y nombre fichero DBF que se crear�)
  public DBFCartasEtiq(Lista vParam, String rutaFich) throws Exception {

    creaDBF(vParam, rutaFich);
  }

  public void creaDBF(Lista vParam, String rutaFich) throws Exception {
    try {

      //Creo un nuevo fichero DBF en ruta rutaFich
//            DBF DBEtiq= new DBF(rutaFich,true);
      DBF DBEtiq = new DBF(rutaFich, xBaseJ.DBF.DBASEIV, true);

      //Creo los campos que va a contener el fichero DBF anterior
      //y los introduzco en un array
      Field[] nuevField = new Field[8];
      CharField cdPCenti = new CharField("CD_PCENTI", 6);
      CharField dsApe1 = new CharField("DS_APE1", 20);
      CharField dsApe2 = new CharField("DS_APE2", 20);
      CharField dsNombre = new CharField("DS_NOMBRE", 20);
      CharField dsDirec = new CharField("DIRECCION", 55);
      CharField cdPostal = new CharField("CD_POSTAL", 5);
      CharField dsMun = new CharField("DS_MUN", 50);
      CharField dsProv = new CharField("DS_PROV", 20);

      nuevField[0] = cdPCenti;
      nuevField[1] = dsApe1;
      nuevField[2] = dsApe2;
      nuevField[3] = dsNombre;
      nuevField[4] = dsDirec;
      nuevField[5] = cdPostal;
      nuevField[6] = dsMun;
      nuevField[7] = dsProv;

      //introduzco el array de campos en el fic DBF
      DBEtiq.addField(nuevField);

      //introduzco los datos de la lista que paso como
      //par�metro del constructor dentro de cada campo, conform�ndose
      //as� un registro, y luego lo escribo en el DBD
      for (int i = 0; i < vParam.size(); i++) {
        nuevField[0].put( ( (Data) vParam.elementAt(i)).getString("CD_PCENTI"));
        nuevField[1].put( ( (Data) vParam.elementAt(i)).getString("DS_APE1"));
        nuevField[2].put( ( (Data) vParam.elementAt(i)).getString("DS_APE2"));
        nuevField[3].put( ( (Data) vParam.elementAt(i)).getString("DS_NOMBRE"));
        nuevField[4].put( ( (Data) vParam.elementAt(i)).getString("DIRECCION"));
        nuevField[5].put( ( (Data) vParam.elementAt(i)).getString("CD_POSTAL"));
        nuevField[6].put( ( (Data) vParam.elementAt(i)).getString("DS_MUN"));
        nuevField[7].put( ( (Data) vParam.elementAt(i)).getString("DS_PROV"));

        DBEtiq.write();
      } //end for

      DBDev = DBEtiq;
    }
    catch (Exception e) {
      e.printStackTrace();
      System.out.println(e.getMessage());
      throw e;
    }
  }

  public DBF getDBF() {
    System.out.println("SALIDA DEL DBF" + DBDev);
    return DBDev;
  }
}