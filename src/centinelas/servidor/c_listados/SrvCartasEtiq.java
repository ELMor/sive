// Decompiled by Jad v1.5.7g. Copyright 2000 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/SiliconValley/Bridge/8617/jad.html
// Decompiler options: packimports(3)
// Source File Name:   SrvCartasEtiq.java

package centinelas.servidor.c_listados;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;
import xBaseJ.DBF;

// Referenced classes of package centinelas.servidor.c_listados:
//            DBFCartasEtiq

public class SrvCartasEtiq
    extends DBServlet {

  public SrvCartasEtiq() {
  }

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {
    boolean bError = false;
    String sMsg = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    Lista snapShot = new Lista();
    Lista lisPeticion = null;
    Data dtRec = null;
    Data dtDetalle = null;
    Lista lisPun = null;
    String cdPlantilla = null;
    String obs = null;
    String rutafich = null;
    boolean avisar = false;
    DBFCartasEtiq genDBF = null;
    DBF dbf = null;
    try {
      con.setAutoCommit(false);
      dtRec = (Data) vParametros.elementAt(0);
      lisPun = (Lista) dtRec.get("LISTA_PUNTOS");
      cdPlantilla = dtRec.getString("CD_PLANTILLA");
      obs = dtRec.getString("DS_OBSERV");
      rutafich = dtRec.getString("RUTA_FICH");
      switch (opmode) {
        case 0: // '\0'
          avisar = false;
          break;

        case 1: // '\001'
          avisar = true;
          break;
      }
      if (avisar) {
        for (int cont = 0; cont < lisPun.size(); cont++) {
          Data datPun = (Data) lisPun.elementAt(cont);
          st = con.prepareStatement(" delete from SIVE_AVISOS where CD_PCENTI = ?  and CD_MEDCEN = ? and FC_AVISO = ? and CD_PLANTILLA = ? ");
          st.setString(1, (String) datPun.get("CD_PCENTI"));
          st.setString(2, (String) datPun.get("CD_MEDCEN"));
          st.setDate(3, Format.string2Date(Format.fechaActual()));
          st.setString(4, cdPlantilla);
          st.executeUpdate();
          st.close();
          st = null;
          st = con.prepareStatement(" insert into SIVE_AVISOS ( CD_PCENTI, CD_MEDCEN, FC_AVISO, CD_PLANTILLA, DS_OBSERV)  values (?,?,?,?,?)");
          st.setString(1, (String) datPun.get("CD_PCENTI"));
          st.setString(2, (String) datPun.get("CD_MEDCEN"));
          st.setDate(3, Format.string2Date(Format.fechaActual()));
          st.setString(4, cdPlantilla);
          if (obs.equals("")) {
            st.setNull(5, 12);
          }
          else {
            st.setString(5, obs);
          }
          st.executeUpdate();
          st.close();
          st = null;
        }

      }
    }
    catch (SQLException e1) {
      trazaLog(e1);
      bError = true;
      sMsg = "Error al leer las peticiones";
    }
    catch (Exception e2) {
      trazaLog(e2);
      bError = true;
      sMsg = "Error inesperado";
    }
    if (bError) {
      con.rollback();
      throw new Exception(sMsg);
    }
    else {
      con.commit();
      snapShot.addElement(dtRec);
      return snapShot;
    }
  }

  protected final int servletCARTETIQ = 0;
  protected final int servletCARTETIQ_AVISOS = 1;
}
