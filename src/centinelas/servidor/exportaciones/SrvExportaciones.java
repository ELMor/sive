//Servlet que obtiene datos para varios subm�dulos de exportaciones
package centinelas.servidor.exportaciones;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;

public class SrvExportaciones
    extends DBServlet {

  final protected int servletSEL_ENFERMEDADES = 1; //Obtiene enfermedades declaradas por los puntos centinelas
  final protected int servletSEL_POBLACION = 3; //Obtiene poblaci�n de los puntos centinelas
  final protected int servletSEL_INDACTIVIDAD = 4; //Obtiene �ndices de actividad asistencial de los puntos centinelas
  final protected int servletSEL_INDALARMA = 5; //Obtiene indicadores de alarma de la RMC

  protected Lista doWork(int opmode, Lista param) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = null;

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    Lista listaSalida = new Lista();
    Data dataEntrada = null;
    Data dataSalida = null;
    boolean bFiltro = false;
    int iValorFiltro = 0;
    Lista lisPeticion = null;

    final String sSEL_ENFSEMANA = " SELECT CD_ENFCIE, CD_PCENTI, " +
        "CD_MEDCEN, CD_ANOEPI, CD_SEMEPI, NM_CASOSDEC " +
        "FROM SIVE_NOTIF_RMC " +
        "WHERE CD_ANOEPI=? AND " +
        "CD_SEMEPI>=? AND " +
        "CD_SEMEPI<=? " +
        "ORDER BY CD_ANOEPI, CD_SEMEPI";

    final String sSEL_ENFANOSEMANA = " SELECT CD_ENFCIE, CD_PCENTI, " +
        "CD_MEDCEN, CD_ANOEPI, CD_SEMEPI, NM_CASOSDEC " +
        "FROM SIVE_NOTIF_RMC " +
        "WHERE CD_ANOEPI=? AND CD_SEMEPI>=? OR " +
        "CD_ANOEPI>? AND CD_ANOEPI<? OR " +
        "CD_ANOEPI=? AND CD_SEMEPI<=? " +
        "ORDER BY CD_ANOEPI, CD_SEMEPI";

    final String sSEL_POBLACION = "SELECT p.CD_EDADF, p.CD_EDADI, " +
        "p.CD_SEXO, p.CD_PCENTI, p.CD_MEDCEN, p.NM_POBLAC, " +
        "p.CD_ANOEPI, m.CD_ANOEPIA, m.CD_SEMEPIA, m.CD_ANOBAJA, " +
        "m.CD_SEMBAJA " +
        "FROM SIVE_POBLAC_TS p, SIVE_MCENTINELA m " +
        "WHERE m.CD_MEDCEN = p.CD_MEDCEN " +
        "ORDER BY 4,5";

    final String sSEL_INDACTIVIDAD = "SELECT im.CD_PCENTI, im.CD_MEDCEN, " +
        "im.CD_INDRMC, im.NM_VINDRMC, i.DS_INDRMC, m.CD_ANOEPIA, " +
        "m.CD_SEMEPIA, m.CD_ANOBAJA, m.CD_SEMBAJA " +
        "FROM SIVE_INDACTMC_MEDCEN im, SIVE_INDACTMC i, " +
        "SIVE_MCENTINELA m " +
        "WHERE im.CD_MEDCEN= m.CD_MEDCEN " +
        "AND im.CD_INDRMC= i.CD_INDRMC " +
        "ORDER BY im.CD_PCENTI, im.CD_MEDCEN ";

    final String sSEL_INDALARMSEMANA = "SELECT a.CD_ANOEPI, a.CD_INDALAR, " +
        "a.CD_SEMEPI, a.NM_VALOR, ia.CD_ENFCIE, iaa.NM_COEF " +
        "FROM SIVE_ALARMA_RMC a, SIVE_IND_ALAR_RMC ia, " +
        "SIVE_IND_ALARRMC_ANO iaa " +
        "WHERE a.CD_INDALAR = ia.CD_INDALAR " +
        "AND ia.CD_INDALAR=iaa.CD_INDALAR " +
        "AND iaa.CD_ANOEPI = a.CD_ANOEPI " +
        "AND a.CD_INDALAR=? " +
        "AND a.CD_ANOEPI=? AND " +
        "a.CD_SEMEPI>=? AND " +
        "a.CD_SEMEPI<=? " +
        "ORDER BY 1, 3";

    final String sSEL_INDALARMANOSEMANA = "SELECT a.CD_ANOEPI, a.CD_INDALAR, " +
        "a.CD_SEMEPI, a.NM_VALOR, ia.CD_ENFCIE, iaa.NM_COEF " +
        "FROM SIVE_ALARMA_RMC a, SIVE_IND_ALAR_RMC ia, " +
        "SIVE_IND_ALARRMC_ANO iaa " +
        "WHERE a.CD_INDALAR = ia.CD_INDALAR " +
        "AND ia.CD_INDALAR=iaa.CD_INDALAR " +
        "AND iaa.CD_ANOEPI = a.CD_ANOEPI " +
        "AND a.CD_INDALAR=? " +
        "AND ((a.CD_ANOEPI=? AND a.CD_SEMEPI>=?) OR " +
        "(a.CD_ANOEPI>? AND a.CD_ANOEPI<?) OR " +
        "(a.CD_ANOEPI=? AND a.CD_SEMEPI<=? ))" +
        "ORDER BY a.CD_ANOEPI, a.CD_SEMEPI";

    try {
      con.setAutoCommit(false);
      if (param.size() != 0) {
        dataEntrada = (Data) param.elementAt(0);
      }

      switch (opmode) {

        //Data q recibe: anoIni, semIni, anoFin ,semFin
        case servletSEL_ENFERMEDADES:
          String anoIni = dataEntrada.getString("CD_ANOEPIINI");
          String semIni = dataEntrada.getString("CD_SEMEPIINI");
          String anoFin = dataEntrada.getString("CD_ANOEPIFIN");
          String semFin = dataEntrada.getString("CD_SEMEPIFIN");

          int nmAnoIni = Integer.parseInt(anoIni);
          int nmSemIni = Integer.parseInt(semIni);
          int nmAnoFin = Integer.parseInt(anoFin);
          int nmSemFin = Integer.parseInt(semFin);

          //caso1: a�os iguales->filtro por semana (sSEL_ENFSEMANA)
          if (nmAnoIni == nmAnoFin) {
            st = con.prepareStatement(sSEL_ENFSEMANA);

            st.setString(1, anoIni);
            st.setString(2, semIni);
            st.setString(3, semFin);

          }
          else { //caso2: a�os distintos->filtro por a�o-semana(sSEL_ENFANOSEMANA)
            st = con.prepareStatement(sSEL_ENFANOSEMANA);

            st.setString(1, anoIni);
            st.setString(2, semIni);
            st.setString(3, anoIni);
            st.setString(4, anoFin);
            st.setString(5, anoFin);
            st.setString(6, semFin);

          }
          rs = st.executeQuery();
          while (rs.next()) {
            dataSalida = new Data();
            dataSalida.put("CD_ENFCIE", rs.getString("CD_ENFCIE"));
            dataSalida.put("CD_PCENTI", rs.getString("CD_PCENTI"));
            dataSalida.put("CD_MEDCEN", rs.getString("CD_MEDCEN"));
            dataSalida.put("CD_ANOEPI", rs.getString("CD_ANOEPI"));
            dataSalida.put("CD_SEMEPI", rs.getString("CD_SEMEPI"));
            int iDeclar = rs.getInt("NM_CASOSDEC");
            if (rs.wasNull()) {
              dataSalida.put("NM_CASOSDEC", "");
            }
            else {
              dataSalida.put("NM_CASOSDEC", (new Integer(iDeclar)).toString());
            }

            listaSalida.addElement(dataSalida);
          } //end while
          break;

          //de momento sin filtro: no trae nada (sSEL_POBLACION)
        case servletSEL_POBLACION:
          st = con.prepareStatement(sSEL_POBLACION);

          rs = st.executeQuery();
          while (rs.next()) {
            dataSalida = new Data();
            dataSalida.put("CD_EDADF",
                           (new Integer(rs.getInt("CD_EDADF"))).toString());
            dataSalida.put("CD_EDADI",
                           (new Integer(rs.getInt("CD_EDADI"))).toString());
            dataSalida.put("CD_SEXO", rs.getString("CD_SEXO"));
            dataSalida.put("CD_PCENTI", rs.getString("CD_PCENTI"));
            dataSalida.put("CD_MEDCEN", rs.getString("CD_MEDCEN"));
            dataSalida.put("NM_POBLAC",
                           (new Integer(rs.getInt("NM_POBLAC"))).toString());
            dataSalida.put("CD_ANOEPI", rs.getString("CD_ANOEPI"));
            dataSalida.put("CD_ANOEPIA", rs.getString("CD_ANOEPIA"));
            dataSalida.put("CD_SEMEPIA", rs.getString("CD_SEMEPIA"));
            String sAnoBaja = rs.getString("CD_ANOBAJA");
            if (rs.wasNull()) {
              dataSalida.put("CD_ANOBAJA", "");
            }
            else {
              dataSalida.put("CD_ANOBAJA", sAnoBaja);
            }
            String sSemBaja = rs.getString("CD_SEMBAJA");
            if (rs.wasNull()) {
              dataSalida.put("CD_SEMBAJA", "");
            }
            else {
              dataSalida.put("CD_SEMBAJA", sSemBaja);
            }

            listaSalida.addElement(dataSalida);
          } //end while
          break;

          //Sin filtro: no trae nada(sSEL_INDACTIVIDAD)
        case servletSEL_INDACTIVIDAD:
          st = con.prepareStatement(sSEL_INDACTIVIDAD);

          rs = st.executeQuery();
          while (rs.next()) {
            dataSalida = new Data();
            dataSalida.put("CD_PCENTI", rs.getString("CD_PCENTI"));
            dataSalida.put("CD_MEDCEN", rs.getString("CD_MEDCEN"));
            dataSalida.put("CD_INDRMC", rs.getString("CD_INDRMC"));
            dataSalida.put("NM_VINDRMC",
                           (new Float(rs.getFloat("NM_VINDRMC"))).toString());
            dataSalida.put("DS_INDRMC", rs.getString("DS_INDRMC"));
            dataSalida.put("CD_ANOEPIA", rs.getString("CD_ANOEPIA"));
            dataSalida.put("CD_SEMEPIA", rs.getString("CD_SEMEPIA"));
            String sAnoBaja = rs.getString("CD_ANOBAJA");
            if (rs.wasNull()) {
              dataSalida.put("CD_ANOBAJA", "");
            }
            else {
              dataSalida.put("CD_ANOBAJA", sAnoBaja);
            }
            String sSemBaja = rs.getString("CD_SEMBAJA");
            if (rs.wasNull()) {
              dataSalida.put("CD_SEMBAJA", "");
            }
            else {
              dataSalida.put("CD_SEMBAJA", sSemBaja);
            }

            listaSalida.addElement(dataSalida);
          } //end while
          break;

          //Data q recibe: anoIniAlar, semIniAlar, anoFinAlar ,semFinAlar, cdIndalar
        case servletSEL_INDALARMA:
          String anoIniAlar = dataEntrada.getString("CD_ANOEPIINI");
          String semIniAlar = dataEntrada.getString("CD_SEMEPIINI");
          String anoFinAlar = dataEntrada.getString("CD_ANOEPIFIN");
          String semFinAlar = dataEntrada.getString("CD_SEMEPIFIN");

          int nmanoIniAlar = Integer.parseInt(anoIniAlar);
          int nmsemIniAlar = Integer.parseInt(semIniAlar);
          int nmanoFinAlar = Integer.parseInt(anoFinAlar);
          int nmsemFinAlar = Integer.parseInt(semFinAlar);

          //caso1: a�os iguales->filtro por semana (sSEL_INDALARMSEMANA)
          if (nmanoIniAlar == nmanoFinAlar) {
            st = con.prepareStatement(sSEL_INDALARMSEMANA);

            st.setString(1, dataEntrada.getString("CD_INDALAR"));
            st.setString(2, anoIniAlar);
            st.setString(3, semIniAlar);
            st.setString(4, semFinAlar);

          }
          else { //caso2: a�os distintos->filtro por a�o-semana(sSEL_INDALARMANOSEMANA)
            st = con.prepareStatement(sSEL_INDALARMANOSEMANA);

            st.setString(1, dataEntrada.getString("CD_INDALAR"));
            st.setString(2, anoIniAlar);
            st.setString(3, semIniAlar);
            st.setString(4, anoIniAlar);
            st.setString(5, anoFinAlar);
            st.setString(6, anoFinAlar);
            st.setString(7, semFinAlar);

          }
          rs = st.executeQuery();
          while (rs.next()) {
            dataSalida = new Data();
            dataSalida.put("CD_ANOEPI", rs.getString("CD_ANOEPI"));
            dataSalida.put("CD_INDALAR", rs.getString("CD_INDALAR"));
            dataSalida.put("CD_SEMEPI", rs.getString("CD_SEMEPI"));
            float iValor = rs.getFloat("NM_VALOR");
            dataSalida.put("NM_VALOR", (new Float(iValor)).toString());
            dataSalida.put("CD_ENFCIE", rs.getString("CD_ENFCIE"));
            float iCoef = rs.getFloat("NM_COEF");
            dataSalida.put("NM_COEF", (new Float(iCoef)).toString());

            listaSalida.addElement(dataSalida);
          } //end while
          break;
      } //end switch

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      ex.printStackTrace();
      listaSalida = null;
      throw ex;
    }

    // Cierre de la conexion con la BD
    closeConnection(con);
    return listaSalida;
  } // Fin doWork()
}
