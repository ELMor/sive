package centinelas.servidor.c_mantPC;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import centinelas.datos.comun.Secuenciador;
import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;

/*Servlet q realiza el alta de puntos centinelas:
  recoge los datos introducidos en la pantalla
  halla el secuenciador de cd_pcentinela, a trav�s de 1fx de sapp*/
public class SrvMantPCentinela
    extends DBServlet {

  final protected int servletALTA_PCENTINELA = 3;

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {
    // control de error
    boolean bError = false;
    String sMsg = null;

    // objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // vector con el resultado
    Lista snapShot = new Lista();

    //peticion
    Lista lisPeticion = null;
    Data dtRec = null;
    Data dtDetalle = null;

    // querys fijas
    final String sALTA_PCENTIN =
        " insert into SIVE_PCENTINELA  "
        + "( CD_PCENTI, IT_GRIPE, CD_CONGLO,"
        + " CD_TPCENTI, FC_ALTA, CD_OPE, "
        + " FC_ULTACT, IT_BAJA, DS_PCENTI) "
        + " values (?,?,?,?,?,?,?,?,?)";

    try {
      //No se realiza el commit automaticamente
      con.setAutoCommit(false);

      dtRec = (Data) vParametros.elementAt(0);

      switch (opmode) {

        /*data->todos los datos recogidos de la pantalla
                  /recoger el c�digo de la tabla de secuenciadores->data
                  /hacer el alta*/
        case servletALTA_PCENTINELA:

          //Secuenciador
          String cod = Secuenciador.getSecuenciador(con, st, rs,
              "NM_PCENTINELA", 6);
          //Puede existir incompatibilidad de tipos:cd(varchar),secAlta(integer)
          dtRec.put("CD_PCENTI", cod);
          dtRec.put("IT_BAJA", "N");

          // prepara la query
          st = con.prepareStatement(sALTA_PCENTIN);
          // completa los valores de la cl�usula WHERE
          st.setString(1, (String) dtRec.get("CD_PCENTI"));
          st.setString(2, (String) dtRec.get("IT_GRIPE"));
          st.setString(3, (String) dtRec.get("CD_CONGLO"));
          st.setString(4, (String) dtRec.get("CD_TPCENTI"));
          st.setDate(5, Format.string2Date( (String) dtRec.get("FC_ALTA")));
          st.setString(6, (String) dtRec.get("CD_OPE"));
          st.setTimestamp(7, Format.string2Timestamp(Format.fechaHoraActual()));
          st.setString(8, (String) dtRec.get("IT_BAJA"));
          st.setString(9, (String) dtRec.get("DS_PCENTI"));

          // ejecuta la query
          st.executeUpdate();
          // cierra los recursos
          st.close();
          st = null;
          break;
      } //fin del switch
    }
    catch (SQLException e1) {
      // traza el error
      trazaLog(e1);
      bError = true;
      sMsg = "Error al leer las peticiones";

    }
    catch (Exception e2) {
      // traza el error
      trazaLog(e2);
      bError = true;
      sMsg = "Error inesperado";
    }

    // devuelve el error recogido
    if (bError) {
      con.rollback();
      throw new Exception(sMsg);
    }

    // valida la transacci�n
    con.commit();
    snapShot.addElement(dtRec);
    return snapShot;
  }
}
