
package centinelas.servidor.c_mantcartasavisos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;

/*Servidor para realizar las consultas de los avisos a los centinelas*/

// Se le env�a:
/*
 Una lista conteniendo datas con los siguientes par�metros:
 CD_ANOEPIINI = a�o epidemiol�gico inicial
 CD_ANOEPIFIN = a�o epidemiol�gico final
 CD_SEMEPIINI = semana epidemiol�gica inicial
 CD_SEMEPIFIN = semana epidemiol�gica final
 NM_SEMCONSEC = n�mero de semanas consecutivas m�nimo
 NM_SEMAGREG = n�mero de semanas agregadas m�nimo
 CD_ENFCIE = enfermedad
 */
public class SrvMantCartasAvisos
    extends DBServlet {

  public SrvMantCartasAvisos() {}

  final protected int servletCONSULTA_MODELO = 0;

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {
    // control de error
    boolean bError = false;
    String sMsg = null;

    // objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // vector con el resultado
    Lista vResultado = new Lista();

    //peticion
    Lista lisPeticion = null;
    Data dtRec = null;
    Data dtDetalle = null;

    String usuario = "";

    Data dtTemp = null;
    Data dtTemp2 = null;
    Data dtSalida = null;
    Lista lNotificaciones = null;
    Lista lMedicos = null;

    // Par�metros de salida.

    Lista semanas_declaradas = null;

    final String consultaTablaSemana = "select CD_ENFCIE, CD_PCENTI, " +
        "CD_MEDCEN, CD_ANOEPI, CD_SEMEPI FROM SIVE_NOTIF_RMC " +
        "WHERE CD_ENFCIE = ? AND CD_ANOEPI=? AND " +
        "CD_SEMEPI>=? AND " +
        "CD_SEMEPI<=? " +
        "ORDER BY CD_ANOEPI, CD_SEMEPI";

    final String consultaTablaAnio = "select CD_ENFCIE, CD_PCENTI, " +
        "CD_MEDCEN, CD_ANOEPI, CD_SEMEPI FROM SIVE_NOTIF_RMC " +
        "WHERE CD_ENFCIE = ? AND CD_ANOEPI=? AND CD_SEMEPI>=? OR " +
        "CD_ENFCIE = ? AND CD_ANOEPI>? AND CD_ANOEPI<? OR " +
        "CD_ENFCIE = ? AND CD_ANOEPI=? AND CD_SEMEPI<=? " +
        "ORDER BY CD_ANOEPI, CD_SEMEPI";

    final String medicosCentinelas = "select CD_MEDCEN, CD_PCENTI, DS_NOMBRE, DS_APE1, DS_APE2 from SIVE_MCENTINELA where IT_BAJA = 'N'";

    try {
      //No se realiza el commit automaticamente
      con.setAutoCommit(false);

      dtRec = (Data) vParametros.elementAt(0);
      usuario = vParametros.getParameter("COD_USUARIO");
      String anioIni = dtRec.getString("CD_ANOEPIINI");
      String anioFin = dtRec.getString("CD_ANOEPIFIN");
      String semIni = dtRec.getString("CD_SEMEPIINI");
      String semFin = dtRec.getString("CD_SEMEPIFIN");
      String lim_cons = dtRec.getString("NM_SEMCONSEC");
      String lim_agre = dtRec.getString("NM_SEMAGREG");
      String enferm = dtRec.getString("CD_ENFCIE");

      switch (opmode) {

        case servletCONSULTA_MODELO:

          /**********************************/
          /***** COGEMOS NOTIFICACIONES *****/
          /**********************************/

          //caso1: a�os iguales->filtro por semana (sSEL_ENFSEMANA)
          if (Integer.parseInt(anioIni) == Integer.parseInt(anioFin)) {
            st = con.prepareStatement(consultaTablaSemana);

            st.setString(1, enferm);
            st.setString(2, anioIni);
            st.setString(3, semIni);
            st.setString(4, semFin);

          }
          else {
            //caso2: a�os distintos->filtro por a�o-semana(sSEL_ENFANOSEMANA)
            st = con.prepareStatement(consultaTablaAnio);

            st.setString(1, enferm);
            st.setString(2, anioIni);
            st.setString(3, semIni);
            st.setString(4, enferm);
            st.setString(5, anioIni);
            st.setString(6, anioFin);
            st.setString(7, enferm);
            st.setString(8, anioFin);
            st.setString(9, semFin);
          }

          // ejecuta la query
          rs = st.executeQuery();
          // Metemos los datos en una lista.
          lNotificaciones = new Lista();
          while (rs.next()) {
            dtTemp = new Data();
            dtTemp.put("CD_ENFCIE", rs.getString("CD_ENFCIE"));
            dtTemp.put("CD_PCENTI", rs.getString("CD_PCENTI"));
            dtTemp.put("CD_MEDCEN", rs.getString("CD_MEDCEN"));
            dtTemp.put("CD_ANOEPI", rs.getString("CD_ANOEPI"));
            dtTemp.put("CD_SEMEPI", rs.getString("CD_SEMEPI"));
            lNotificaciones.addElement(dtTemp);
            dtTemp = null;
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          /***************************/
          /***** COGEMOS M�DICOS *****/
          /***************************/

          st = con.prepareStatement(medicosCentinelas);
          // ejecuta la consulta
          lMedicos = new Lista();
          rs = st.executeQuery();
          while (rs.next()) {
            dtTemp = new Data();
            dtTemp.put("CD_MEDCEN", rs.getString("CD_MEDCEN"));
            dtTemp.put("CD_PCENTI", rs.getString("CD_PCENTI"));
            if (rs.getString("DS_NOMBRE") != null) {
              dtTemp.put("DS_NOMBRE", rs.getString("DS_NOMBRE"));
            }
            if (rs.getString("DS_APE1") != null) {
              dtTemp.put("DS_APE1", rs.getString("DS_APE1"));
            }
            if (rs.getString("DS_APE2") != null) {
              dtTemp.put("DS_APE2", rs.getString("DS_APE2"));
            }
            lMedicos.addElement(dtTemp);
            dtTemp = null;
          }
          rs.close();
          rs = null;
          st.close();
          st = null;
          // ****************************************************
          // ****************************************************
          // ****************************************************

          Data dt = null;
          // Ahora cruzamos los datos.
          for (int cm = 0; cm < lMedicos.size(); cm++) {
            dtTemp = (Data) lMedicos.elementAt(cm);
            semanas_declaradas = null;
            semanas_declaradas = new Lista();
            for (int cn = 0; cn < lNotificaciones.size(); cn++) {
              dtTemp2 = (Data) lNotificaciones.elementAt(cn);
              // Si el m�dico est� en notificaciones ...
              if ( (dtTemp2.getString("CD_MEDCEN").equals(dtTemp.getString(
                  "CD_MEDCEN"))) &&
                  (dtTemp2.getString("CD_PCENTI").equals(dtTemp.getString(
                  "CD_PCENTI")))) {
                dt = new Data();
                dt.put("CD_SEMEPI", dtTemp2.getString("CD_SEMEPI"));
                dt.put("CD_ANOEPI", dtTemp2.getString("CD_ANOEPI"));
                semanas_declaradas.addElement(dt);
                dt = null;
              }
            } // del for.
            dtSalida = new Data();
            String sem_consec = "0";
            String sem_agreg = "0";
            if (semanas_declaradas != null) {
              Lista semanasFiltradas = calcularSemanas(semanas_declaradas,
                  anioIni, semIni, anioFin, semFin);
              if ( (semanasFiltradas != null) && (semanasFiltradas.size() > 0)) {
                sem_consec = String.valueOf(semanasFiltradas.elementAt(0));
                sem_agreg = String.valueOf(semanasFiltradas.elementAt(1));
              }
            }
            if ( (lim_cons != null) && (lim_agre != null) &&
                (lim_cons.length() > 0) && (lim_agre.length() > 0)) {
              if ( (Integer.parseInt(sem_consec) >= Integer.parseInt(lim_cons)) &&
                  (Integer.parseInt(sem_agreg) >= Integer.parseInt(lim_agre))) {
                dtSalida.put("SEM_CONSEC", sem_consec);
                dtSalida.put("SEM_AGREG", sem_agreg);
                dtSalida.put("CD_PCENTI", dtTemp.getString("CD_PCENTI"));
                dtSalida.put("CD_ENFCIE", enferm);
                dtSalida.put("CD_MEDCEN", dtTemp.getString("CD_MEDCEN"));
                dtSalida.put("DS_MEDICO", dtTemp.getString("CD_MEDCEN") + " - " +
                             dtTemp.getString("DS_NOMBRE") + " " +
                             dtTemp.getString("DS_APE1") + " " +
                             dtTemp.getString("DS_APE2"));
                vResultado.addElement(dtSalida);
              } // del 2 if
            } // del 1 if
          }
          break;

      } //fin del switch
    }
    catch (SQLException e1) {
      // traza el error
      trazaLog(e1);
      bError = true;
      sMsg = "Error SQL:" + e1;

    }
    catch (Exception e2) {
      // traza el error
      trazaLog(e2);
      bError = true;
      sMsg = "Error inesperado:" + e2;
    }

    // devuelve el error recogido
    if (bError) {
      con.rollback();
      throw new Exception(sMsg);
    }

    // valida la transacci�n
    con.commit();
    return vResultado;
  }

  // Todo en un m�todo... quien lo tenga que corregir se va a reir.  :-(
  // Por lo menos se va a reir tanto como yo me he re�do haci�ndolo :-(
  private Lista calcularSemanas(Lista l, String an_ini, String s_ini,
                                String an_fin, String s_fin) {
    int anio_ini = Integer.parseInt(an_ini);
    int anio_fin = Integer.parseInt(an_fin);
    int sem_ini = Integer.parseInt(s_ini);
    int sem_fin = Integer.parseInt(s_fin);
    int consec = 0;
    int agreg = 0;
    Lista devuelto = new Lista();
    Data dtTmp = null;
    // Para incrementar la lista.
    int incremento = 0;
    // Variables internas.
    String anio_tmp = "";
    String sema_tmp = "";
    boolean es_consecutiva = false;
    boolean existe = false;
    int consec_tmp = 0;
    // Iniciamos los a�os y semanas. (no 'inicializamos', que no existe en castellano)
    int c_anio = anio_ini;
    int c_sem = sem_ini;
    // Contamos los a�os y semanas
    // Si el a�o inicial es el a�o final

    /*******************************************/
    /******** A�O FINAL == A�O INICIAL *********/
    /*******************************************/
    if (anio_ini == anio_fin) {
      while (c_sem <= sem_fin) {
        /* er c�digo */
        // Recorremos toda la lista para ver si existe la fecha en ella.
        if (l.size() > 0) {
          for (incremento = 0; incremento < l.size(); incremento++) {
            dtTmp = new Data();
            dtTmp = (Data) l.elementAt(incremento);
            anio_tmp = dtTmp.getString("CD_ANOEPI");
            sema_tmp = dtTmp.getString("CD_SEMEPI");
            if ( (c_anio == Integer.parseInt(anio_tmp)) &&
                (c_sem == Integer.parseInt(sema_tmp))) {
              existe = true;
            } // del if
            dtTmp = null;
          } // final del for
        } // final del if
        if (!existe) {
          agreg++;
          // Activamos la variable para ver si tiene consecutivas.
          es_consecutiva = true;
        }
        else {
          es_consecutiva = false;
        } // Del if y el else ese
        existe = false;
        if (es_consecutiva) {
          consec_tmp++;
          if (consec_tmp > consec) {
            consec = consec_tmp;
          }
        }
        else {
          consec_tmp = 0;
        } // idem de lo mismo: fin del if y el else ese.
        /**/
        // Los incrementos de turno.
        c_sem++;
      }
      // Bueno, pues ahora guardamos los datos a devolver.
      devuelto.addElement(String.valueOf(consec));
      devuelto.addElement(String.valueOf(agreg));

      /*******************************************/
      /******** A�O FINAL > A�O INICIAL **********/
      /*******************************************/
    }
    else if (anio_ini < anio_fin) {
      // Si el a�o inicial distinto del a�o final
      while (c_anio < anio_fin) {
        while (! ( (c_sem == sem_fin) && (c_anio == anio_fin))) {
          /* er c�digo */
          // Recorremos toda la lista para ver si existe la fecha en ella.
          if (l.size() > 0) {
            for (incremento = 0; incremento < l.size(); incremento++) {
              dtTmp = new Data();
              dtTmp = (Data) l.elementAt(incremento);
              anio_tmp = dtTmp.getString("CD_ANOEPI");
              sema_tmp = dtTmp.getString("CD_SEMEPI");
              if ( (c_anio == Integer.parseInt(anio_tmp)) &&
                  (c_sem == Integer.parseInt(sema_tmp))) {
                existe = true;
              } // del if
              dtTmp = null;
            } // final del for
          } // final del if
          if (!existe) {
            agreg++;
            // Activamos la variable para ver si tiene consecutivas.
            es_consecutiva = true;
          }
          else {
            es_consecutiva = false;
          } // Del if y el else ese
          existe = false;
          if (es_consecutiva) {
            consec_tmp++;
            if (consec_tmp > consec) {
              consec = consec_tmp;
            }
          }
          else {
            consec_tmp = 0;
          } // idem de lo mismo: fin del if y el else ese.
          /**/
          // Los incrementos de turno.
          c_sem++;
          // Llega a final de a�o
          if (c_sem == 53) {
            c_sem = 1;
            c_anio++;
          } // Del if
        } // del while segundo
      } // del g��il primero.
      // Bueno, pues ahora guardamos los datos a devolver.
      devuelto.addElement(String.valueOf(consec));
      devuelto.addElement(String.valueOf(agreg));
    } // del if de la comparaci�n del a�o.
    return devuelto;
  }

  public static void main(String args[]) {
    SrvMantCartasAvisos srv = new SrvMantCartasAvisos();
    Lista vParam = new Lista();
    Lista lLote = new Lista();
    Data dtDatos = new Data();

    // par�metros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");

    dtDatos.put("CD_ANOEPIINI", "1999");
    dtDatos.put("CD_ANOEPIFIN", "2000");
    dtDatos.put("CD_SEMEPIINI", "10");
    dtDatos.put("CD_SEMEPIFIN", "23");
    dtDatos.put("NM_SEMCONSEC", "54");
    dtDatos.put("NM_SEMAGREG", "13");
    dtDatos.put("CD_ENFCIE", "002");
    vParam.addElement(dtDatos);

    vParam.setParameter("COD_USUARIO", "88");

    lLote = srv.doDebug(0, vParam);
    srv = null;
    for (int i = 0; i < lLote.size(); i++) {
      System.out.println(lLote.elementAt(i));
    }
  }

}