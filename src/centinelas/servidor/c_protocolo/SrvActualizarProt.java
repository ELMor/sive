/*
 */
package centinelas.servidor.c_protocolo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import capp.CApp;
import capp.CLista;
import centinelas.datos.c_protocolo.DataLayout;
import centinelas.datos.c_protocolo.DataLineasM;
import centinelas.datos.c_protocolo.DataListaValores;
import centinelas.datos.c_protocolo.DataProtocolo;
import centinelas.datos.c_protocolo.DataValores;
import sapp.DBServlet;

//entrada: lista de listas: 1�: lista con DataProtocolo   (listaEntrada)
//salida  //listaSalida: listaLayoutParcial (layout del nuevo) y la listaInsertar (para modificar el respedo)
//si null, sale por catch  (error)
public class SrvActualizarProt
    extends DBServlet {

  protected CLista listaDataLayout = null; //lista para la total
  protected CLista listaDataLineasM = null; //lista para la primera query
  protected CLista listaModelos = null; //aprovecho DataLineasM para guardarlo
  protected CLista listaDataPregunta = null; // lista para la segunda query (DataLayout)
  protected CLista listaListas = null; //lista para cargar las Choices L que haya
  protected CLista listaListaValores = null; //ya cargadas

  final int servletVERACTIVOS = 0;
  final int servletVERACTIVOS_B = 1;
  final int servletVERACTIVOS_C = 2;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos de datos
    listaDataLayout = new CLista();
    listaDataLineasM = new CLista();
    listaModelos = new CLista();
    listaDataPregunta = new CLista();
    listaListas = new CLista();
    listaListaValores = new CLista();

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String sQuery = "";
    ResultSet rs = null;
    int i = 0;
    int j = 0;

    // objetos de datos
    DataProtocolo dataEntrada = null;
    CLista listaEntrada = new CLista();

    String des = "";
    String desL = "";
    // campos primera consulta
    String sCD_MODELO = "";
    String sNM_LIN = "";
    Integer iNM_LIN = null;
    String sCD_TLINEA = "";
    String sDS_TEXTO = "";
    String sCA = "";
    String sN1 = "";
    String sN2 = "";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    //para seguir la secuencia
    boolean b = true;

    try {

      // modos de operaci�n
      switch (opmode) {

        case servletVERACTIVOS:
        case servletVERACTIVOS_B:
        case servletVERACTIVOS_C:

          dataEntrada = (DataProtocolo) param.elementAt(0);

          if (dataEntrada.getDes().trim().equals("CNE")) {

            sQuery = " select "
                +
                "b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, b.DSL_TEXTO,  "
                + "a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 "
                + "from SIVE_MODELO a, SIVE_LINEASM b  "
                +
                "where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) "
                + "and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)"
                + "and (a.CD_ENFCIE = ?) "
                + "and ( "
                +
                "(a.CD_CA is null and a.CD_NIVEL_1 is null and a.CD_NIVEL_2 is null)  "
                + ") "
                + ") "
                + "order by b.CD_MODELO, b.NM_LIN";

            st = con.prepareStatement(sQuery);

            if (opmode == servletVERACTIVOS) {
              st.setString(1, "E");
            }
            else if (opmode == servletVERACTIVOS_B) {
              st.setString(1, "E");
// Cambio puesto el 30-11-2000
            }
            else if (opmode == servletVERACTIVOS_C) {
              st.setString(1, "C");

            }
            st.setString(2, "S");
            st.setString(3, dataEntrada.getCod().trim());
          }

          else if (dataEntrada.getDes().trim().equals("CA")) {

            sQuery = " select " +
                "b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, b.DSL_TEXTO,  " +
                "a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 " +
                "from SIVE_MODELO a, SIVE_LINEASM b  " +
                "where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) " +
                "and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)" +
                "and (a.CD_ENFCIE = ?) " +
                "and ( " +
                "(a.CD_CA = ? and a.CD_NIVEL_1 is null and a.CD_NIVEL_2 is null)  " +
                ") " +
                ") " +
                "order by b.CD_MODELO, b.NM_LIN";

            st = con.prepareStatement(sQuery);

            if (opmode == servletVERACTIVOS) {
              st.setString(1, "E");
            }
            else if (opmode == servletVERACTIVOS_B) {
              st.setString(1, "E");
// Cambio puesto el 30-11-2000
            }
            else if (opmode == servletVERACTIVOS_C) {
              st.setString(1, "C");

            }
            st.setString(2, "S");
            st.setString(3, dataEntrada.getCod().trim());
            st.setString(4, dataEntrada.getCa().trim());

          }

          else if (dataEntrada.getDes().trim().equals("N1")) {

            sQuery = " select " +
                "b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, b.DSL_TEXTO,  " +
                "a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 " +
                "from SIVE_MODELO a, SIVE_LINEASM b  " +
                "where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) " +
                "and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)" +
                "and (a.CD_ENFCIE = ?) " +
                "and ( " +
                "(a.CD_CA = ? and a.CD_NIVEL_1 = ? and a.CD_NIVEL_2 is null)  " +
                ") " +
                ") " +
                "order by b.CD_MODELO, b.NM_LIN";

            st = con.prepareStatement(sQuery);

            if (opmode == servletVERACTIVOS) {
              st.setString(1, "E");
            }
            else if (opmode == servletVERACTIVOS_B) {
              st.setString(1, "E");
// Cambio puesto el 30-11-2000
            }
            else if (opmode == servletVERACTIVOS_C) {
              st.setString(1, "C");

            }
            st.setString(2, "S");
            st.setString(3, dataEntrada.getCod().trim());
            st.setString(4, dataEntrada.getCa().trim());
            st.setString(5, dataEntrada.getNivel1().trim());
          }

          else if (dataEntrada.getDes().trim().equals("N2")) {

            sQuery = " select " +
                "b.CD_MODELO, b.NM_LIN, b.CD_TLINEA, b.DS_TEXTO, b.DSL_TEXTO,  " +
                "a.CD_CA, a.CD_NIVEL_1, a.CD_NIVEL_2 " +
                "from SIVE_MODELO a, SIVE_LINEASM b  " +
                "where ( (a.CD_TSIVE = b.CD_TSIVE and a.CD_MODELO = b.CD_MODELO ) " +
                "and (a.CD_TSIVE = ?) and  (a.IT_OK = ?)" +
                "and (a.CD_ENFCIE = ?) " +
                "and ( " +
                "(a.CD_CA = ? and a.CD_NIVEL_1 = ? and a.CD_NIVEL_2 = ?)  " +
                ") " +
                ") " +
                "order by b.CD_MODELO, b.NM_LIN";

            st = con.prepareStatement(sQuery);

            if (opmode == servletVERACTIVOS) {
              st.setString(1, "E");
            }
            else if (opmode == servletVERACTIVOS_B) {
              st.setString(1, "E");
// Cambio puesto el 30-11-2000
            }
            else if (opmode == servletVERACTIVOS_C) {
              st.setString(1, "C");

            }
            st.setString(2, "S");
            st.setString(3, dataEntrada.getCod().trim());
            st.setString(4, dataEntrada.getCa().trim());
            st.setString(5, dataEntrada.getNivel1().trim());
            st.setString(6, dataEntrada.getNivel2().trim());

          }

          //COMUN !!!!
          rs = st.executeQuery();

          // extrae el registro encontrado y lo carga en listaDataLineasM
          while (rs.next()) {

            des = rs.getString("DS_TEXTO");
            desL = rs.getString("DSL_TEXTO");

            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (desL != null)) {
              sDS_TEXTO = desL;
            }
            else {
              sDS_TEXTO = des;

            }
            sCD_MODELO = rs.getString("CD_MODELO");
            iNM_LIN = new Integer(rs.getInt("NM_LIN"));
            sNM_LIN = iNM_LIN.toString();

            sCD_TLINEA = rs.getString("CD_TLINEA");
            sCA = rs.getString("CD_CA");
            sN1 = rs.getString("CD_NIVEL_1");
            sN2 = rs.getString("CD_NIVEL_2");

            // a�ade un nodo
            DataLineasM dat = new DataLineasM(sCD_MODELO,
                                              sNM_LIN,
                                              sCD_TLINEA,
                                              sDS_TEXTO,
                                              sCA, sN1, sN2);

            listaDataLineasM.addElement(dat);
            dat = null;

          } //while
          rs.close();
          rs = null;
          st.close();
          st = null;

          if (listaDataLineasM.size() == 0) {
            b = false;
            listaDataLayout = null;
          }

          if (b) {
            //modelo
            DataLineasM datosParaBusqueda = (DataLineasM) listaDataLineasM.
                elementAt(0);
            listaModelos.addElement(datosParaBusqueda);

            //solo es un modelo
            sQuery = ConsultaPregunta();
            st = con.prepareStatement(sQuery);
            if (opmode == servletVERACTIVOS) {
              st.setString(1, "E");
// Cambio puesto el 30-11-2000
            }
            else if (opmode == servletVERACTIVOS_C) {
              st.setString(1, "C");

            }
            st.setString(2, datosParaBusqueda.getCodModelo());
            rs = st.executeQuery();
            while (rs.next()) {
              //vamos llenando la lista  listaDataPregunta en un obj DataLayout
              listaDataPregunta.addElement(new DataLayout
                                           (rs.getString("CD_TPREG"),
                                            "",
                                            rs.getString("IT_OBLIGATORIO"),
                                            new Integer(rs.getInt("NM_LONG")).
                                            toString(),
                                            new Integer(rs.getInt("NM_ENT")).
                                            toString(),
                                            new Integer(rs.getInt("NM_DEC")).
                                            toString(),
                                            rs.getString("CD_MODELO"),
                                            new Integer(rs.getInt("NM_LIN")).
                                            toString(),
                                            rs.getString("CD_PREGUNTA"),
                                            rs.getString("IT_CONDP"),
                                            new Integer(rs.getInt("NM_LIN_PC")).
                                            toString(),
                                            rs.getString("CD_PREGUNTA_PC"),
                                            rs.getString("DS_VPREGUNTA_PC"),
                                            rs.getString("CD_LISTA"), null,
                                            datosParaBusqueda.getComunidad(),
                                            datosParaBusqueda.getNivel1(),
                                            datosParaBusqueda.getNivel2()));

            } //fin del while
            rs.close();
            rs = null;
            st.close();
            st = null;

            if (listaDataPregunta.size() == 0) {
              b = false;
              listaDataLayout = null;
            }

          } //b

          //NUEVA ETAPA************
          if (b) { //si sigue siendo true
            RecomponerLista(dataEntrada.getDes().trim()); //le pasamos el nivel

          }

          //si en algun momento fue true,  listaDataLayout = null
          //en otro caso, esta cargada con datos
          if (listaDataLayout != null) {

            //existen listas en el protocolo
            if (listaListas != null) {

              Recomponer_solouncod_porLista();

              //CARGAMOS LISTALISTAVALORES-------------------
              String sCod = "";
              String sDes = "";

              sQuery = ConsultaLista();
              st = con.prepareStatement(sQuery);
              for (i = 0; i < listaListas.size(); i++) {
                DataListaValores dataListas = null;
                dataListas = (DataListaValores) listaListas.elementAt(i);
                st.setString(1, dataListas.getCodLista().trim());
                rs = st.executeQuery();

                // extrae LOS registroS encontradoS
                while (rs.next()) {

                  sCod = rs.getString("CD_LISTAP");
                  des = rs.getString("DS_LISTAP");
                  desL = rs.getString("DSL_LISTAP");

                  if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)
                      && (desL != null)) {
                    sDes = desL;
                  }
                  else {
                    sDes = des;

                    // a�ade un nodo
                  }
                  DataListaValores dataSalida = new DataListaValores
                      (dataListas.getCodLista().trim(),
                       sCod,
                       sDes);

                  listaListaValores.addElement(dataSalida);
                  dataSalida = null;

                } //fin del while

                rs.close();
                rs = null;
              } //for, para cada codlista
              st.close();
              st = null;

              //-----------------------------
              Recomponer_ListaValores_conVector();
              Recomponer_ListaDataLayout();
              listaDataLayout.trimToSize();

            }
            else {
              listaDataLayout.trimToSize();
            }
          }
          //no hay layout
          else {
            listaDataLayout = null;
          }

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaDataLayout = null;

      throw ex;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (listaDataLayout != null) {
      listaDataLayout.trimToSize();

    }
    return listaDataLayout;
  } //fin doWork

  //Consulta sobre sive_lineas_item y sive_pregunta
  protected String ConsultaPregunta() {

    return ("select " +
            " b.CD_MODELO, b.NM_LIN, b.CD_PREGUNTA, b.CD_PREGUNTA_PC, " +
            " b.IT_OBLIGATORIO, b.IT_CONDP, b.NM_LIN_PC, b.DS_VPREGUNTA_PC, " +
            " a.CD_TPREG, a.CD_LISTA, a.NM_LONG, a.NM_ENT, a.NM_DEC " +
            " from SIVE_LINEA_ITEM b, SIVE_PREGUNTA a " +
            " where ((b.CD_TSIVE = ?) and (b.CD_MODELO = ?) and " +
            "(b.CD_TSIVE = a.CD_TSIVE and  b.CD_PREGUNTA = a.CD_PREGUNTA )) " +
            " order by NM_LIN");

  }

//A partir de listaListas, puede existir cod de listas repetidas
  protected void Recomponer_solouncod_porLista() {

    String sCod = "";
    DataListaValores dataEntrada = null;
    DataListaValores dataUsar = null;
    CLista listaUsar = new CLista();

    for (int i = 0; i < listaListas.size(); i++) {

      dataEntrada = (DataListaValores) listaListas.elementAt(i);
      sCod = dataEntrada.getCodLista().trim();
      if (i == 0) {
        dataUsar = new DataListaValores(sCod, "", "");
        listaUsar.addElement(dataUsar);
      }
      else {
        boolean byaesta = false;
        for (int u = 0; u < listaUsar.size(); u++) {
          dataUsar = (DataListaValores) listaUsar.elementAt(u);
          if (dataUsar.getCodLista().trim().equals(sCod)) {
            byaesta = true;
          }
        } //for

        if (byaesta) { //si esta en listaUsar, no se hace nada
          //registro que ya sobra y esta almacenado
        }
        else {
          dataUsar = new DataListaValores(sCod, "", "");
          listaUsar.addElement(dataUsar);
        }
      }
    } //fin for ppal

    //COPIA   en listaListas, la de usar
    listaListas.removeAllElements();
    for (int i = 0; i < listaUsar.size(); i++) {
      dataUsar = (DataListaValores) listaUsar.elementAt(i);
      listaListas.addElement(dataUsar);
    }

  } //fin recomponercoduno

  protected void RecomponerLista(String nivel) {

    boolean b = true;
    //leemos una lineaM: si D, "D", null, null...add
    //                   si P, leemos...add

    int iListaLineasM = 0; //ppal
    int iListaPregunta = 0; //para movernos por la lista secundaria

    //tipos de registros
    String sTipoRegistro = "";
    String sAlmacen = "-1";

    for (iListaLineasM = 0; iListaLineasM < listaDataLineasM.size();
         iListaLineasM++) {
      if (b) {
        DataLineasM datosLinea = (DataLineasM) listaDataLineasM.elementAt(
            iListaLineasM);
        //si la primera no es descripcion, lista=null------------------------
        if (iListaLineasM == 0 && !datosLinea.getTipoLinea().equals("D")) {

          if (b) {
            b = false;
          }
        }
        //si es una descripcion, a a�adimos a listaDataLayout----------------
        if (datosLinea.getTipoLinea().equals("D")) {

          sTipoRegistro = nivel;
          //a�adimos una Descripcion de tipo X: CNE, CA, N1, N2
          //se a�ade cuando el registro es diferente del almacen
          if (!sAlmacen.trim().equals(sTipoRegistro.trim())) {
            listaDataLayout.addElement(new DataLayout
                                       ("X", sTipoRegistro.trim(), null, null, null, null,
                                        null, null,
                                        null, null, null, null, null, null, null,
                                        datosLinea.getComunidad(),
                                        datosLinea.getNivel1(),
                                        datosLinea.getNivel2()));
            sAlmacen = sTipoRegistro;
          }

          //a�adimos D en tipoPregunta

          listaDataLayout.addElement(new DataLayout
                                     ("D",
                                      datosLinea.getDesTexto(), null, null, null, null,
                                      datosLinea.getCodModelo(),
                                      datosLinea.getNmLinea(),
                                      null, null, null, null, null, null, null,
                                      datosLinea.getComunidad(),
                                      datosLinea.getNivel1(),
                                      datosLinea.getNivel2()));

        }
        //si es una pregunta--------------------------------------------------
        if (datosLinea.getTipoLinea().equals("P")) {

          DataLayout datosPregunta = (DataLayout) listaDataPregunta.elementAt(
              iListaPregunta);

          //comprobaciones  F-K
          if (!datosLinea.getCodModelo().equals(datosPregunta.getCodModelo()) ||
              !datosLinea.getNmLinea().equals(datosPregunta.getNumLinea())) {
            if (b) {
              b = false;
            }
          }

          //a�adimos a lista definitiva
          if (b) {

            //vemos el tipo de pregunta:
            if (datosPregunta.getTipoPreg().equals("L")) {

              listaDataLayout.addElement(new DataLayout
                                         (datosPregunta.getTipoPreg(),
                                          datosLinea.getDesTexto(),
                                          datosPregunta.getOblig(),
                                          null, null, null,
                                          datosPregunta.getCodModelo(),
                                          datosPregunta.getNumLinea(),
                                          datosPregunta.getCodPregunta(),
                                          datosPregunta.getCondicionada(),
                                          datosPregunta.getNumLineaCond(),
                                          datosPregunta.getCodPreguntaCond(),
                                          datosPregunta.getDesPreguntaCond(),
                                          datosPregunta.getCodLista(), null,
                                          datosLinea.getComunidad(),
                                          datosLinea.getNivel1(),
                                          datosLinea.getNivel2()));

              //cargamos listaListas con las choices L que encontramos
              listaListas.addElement(new DataListaValores
                                     (datosPregunta.getCodLista(), "", ""));

            }
            if (datosPregunta.getTipoPreg().equals("B")) {

              listaDataLayout.addElement(new DataLayout
                                         (datosPregunta.getTipoPreg(),
                                          datosLinea.getDesTexto(),
                                          datosPregunta.getOblig(),
                                          null, null, null,
                                          datosPregunta.getCodModelo(),
                                          datosPregunta.getNumLinea(),
                                          datosPregunta.getCodPregunta(),
                                          datosPregunta.getCondicionada(),
                                          datosPregunta.getNumLineaCond(),
                                          datosPregunta.getCodPreguntaCond(),
                                          datosPregunta.getDesPreguntaCond(),
                                          datosPregunta.getCodLista(), null,
                                          datosLinea.getComunidad(),
                                          datosLinea.getNivel1(),
                                          datosLinea.getNivel2()));
            }
            if (datosPregunta.getTipoPreg().equals("N")) {

              listaDataLayout.addElement(new DataLayout
                                         (datosPregunta.getTipoPreg(),
                                          datosLinea.getDesTexto(),
                                          datosPregunta.getOblig(),
                                          datosPregunta.getLongitud(),
                                          datosPregunta.getEntera(),
                                          datosPregunta.getDecimal(),
                                          datosPregunta.getCodModelo(),
                                          datosPregunta.getNumLinea(),
                                          datosPregunta.getCodPregunta(),
                                          datosPregunta.getCondicionada(),
                                          datosPregunta.getNumLineaCond(),
                                          datosPregunta.getCodPreguntaCond(),
                                          datosPregunta.getDesPreguntaCond(),
                                          datosPregunta.getCodLista(), null,
                                          datosLinea.getComunidad(),
                                          datosLinea.getNivel1(),
                                          datosLinea.getNivel2()));
            }
            if (datosPregunta.getTipoPreg().equals("C")) {

              listaDataLayout.addElement(new DataLayout
                                         (datosPregunta.getTipoPreg(),
                                          datosLinea.getDesTexto(),
                                          datosPregunta.getOblig(),
                                          datosPregunta.getLongitud(), null, null,
                                          datosPregunta.getCodModelo(),
                                          datosPregunta.getNumLinea(),
                                          datosPregunta.getCodPregunta(),
                                          datosPregunta.getCondicionada(),
                                          datosPregunta.getNumLineaCond(),
                                          datosPregunta.getCodPreguntaCond(),
                                          datosPregunta.getDesPreguntaCond(),
                                          datosPregunta.getCodLista(), null,
                                          datosLinea.getComunidad(),
                                          datosLinea.getNivel1(),
                                          datosLinea.getNivel2()));
            }
            if (datosPregunta.getTipoPreg().equals("F")) {

              listaDataLayout.addElement(new DataLayout
                                         (datosPregunta.getTipoPreg(),
                                          datosLinea.getDesTexto(),
                                          datosPregunta.getOblig(),
                                          null, null, null,
                                          datosPregunta.getCodModelo(),
                                          datosPregunta.getNumLinea(),
                                          datosPregunta.getCodPregunta(),
                                          datosPregunta.getCondicionada(),
                                          datosPregunta.getNumLineaCond(),
                                          datosPregunta.getCodPreguntaCond(),
                                          datosPregunta.getDesPreguntaCond(),
                                          datosPregunta.getCodLista(), null,
                                          datosLinea.getComunidad(),
                                          datosLinea.getNivel1(),
                                          datosLinea.getNivel2()));
            }

          } //fin de a�adir  (b)

          iListaPregunta = iListaPregunta + 1;
        } //P ---------------------------------------------------------------------
      } //continuamos si es true!!
    } //fin for
    if (!b) {
      listaDataLayout = null;
    }
    else {
      for (int i = 0; i < listaDataLayout.size(); i++) {
        DataLayout dat = (DataLayout) (listaDataLayout.elementAt(i));

      }
    }

  }

//tenemos listaListaValores + listaDataLayout = listaDataLayout;
  protected void Recomponer_ListaDataLayout() {

    DataLayout data = null;
    DataListaValores datalistavalores = null;
    DataListaValores datareserva = null;
    boolean bSalirfor = false;

    for (int i = 0; i < listaDataLayout.size(); i++) {
      data = (DataLayout) listaDataLayout.elementAt(i);
      if (data.getCodLista() != null) {
        //buscar en listaListaValores su vector
        for (int k = 0; k < listaListaValores.size(); k++) {
          if (!bSalirfor) {

            datalistavalores = (DataListaValores) listaListaValores.elementAt(k);
            if (datalistavalores.getCodLista().trim().equals(data.getCodLista().
                trim())) {
              listaDataLayout.removeElementAt(i);
              listaDataLayout.insertElementAt(new DataLayout(data.getTipoPreg(),
                  data.getDesTexto(), data.getOblig(),
                  data.getLongitud(), data.getEntera(),
                  data.getDecimal(), data.getCodModelo(),
                  data.getNumLinea(), data.getCodPregunta(),
                  data.getCondicionada(), data.getNumLineaCond(),
                  data.getCodPreguntaCond(), data.getDesPreguntaCond(),
                  data.getCodLista(), datalistavalores.getVALORES(),
                  data.getCA(), data.getNIVEL_1(), data.getNIVEL_2()), i);

            }
          }
        } //for
      }
    } //for
  }

//listaListaValores: DataListaValores(sCodLista, cod, des) --> listaListaValores: DataListaValores(sCodLista, vector)
  protected void Recomponer_ListaValores_conVector() {

    CLista lista = new CLista();
    DataListaValores dataEntrada = null;
    DataListaValores data = null;
    String sCodLista = "";
    String sCodListaBK = "";

    Vector vValores = new Vector();
    DataValores dat = null;
    for (int i = 0; i < listaListaValores.size(); i++) {

      dataEntrada = (DataListaValores) listaListaValores.elementAt(i);

      if (i == 0) {

        sCodLista = dataEntrada.getCodLista();
        vValores.addElement(new DataValores(dataEntrada.getCod(),
                                            dataEntrada.getDes()));
        sCodListaBK = sCodLista;
      }
      else {
        sCodLista = dataEntrada.getCodLista();

        if (sCodLista.equals(sCodListaBK)) { //la misma lista
          vValores.addElement(new DataValores(dataEntrada.getCod(),
                                              dataEntrada.getDes()));
          sCodListaBK = sCodLista;
        }
        else { //cambiamos a otro codlista
          //*******
           Vector vNuevo = new Vector();
          for (int k = 0; k < vValores.size(); k++) {
            dat = (DataValores) vValores.elementAt(k);
            vNuevo.addElement(dat);
          }
          //*******
           lista.addElement(new DataListaValores(sCodListaBK, vNuevo));
          vNuevo = null;

          vValores.removeAllElements();
          vValores.addElement(new DataValores(dataEntrada.getCod(),
                                              dataEntrada.getDes()));
          sCodListaBK = sCodLista;
        }
      } //if-else
    } //for

    //para el ultimo
    //*******
     Vector vNuevo = new Vector();
    for (int k = 0; k < vValores.size(); k++) {
      dat = (DataValores) vValores.elementAt(k);
      vNuevo.addElement(dat);
    }
    //*******
     lista.addElement(new DataListaValores(sCodListaBK, vNuevo));
    vNuevo = null;
    vValores = null;

    //volcamos lista a listaListaValores
    listaListaValores = lista;

  } //fin

  protected String ConsultaLista() {

    return ("select "
            + "CD_LISTAP, DS_LISTAP, DSL_LISTAP "
            + "from SIVE_LISTA_VALORES "
            + "where "
            + "CD_LISTA = ? ");
  }

} //fin de la clase
