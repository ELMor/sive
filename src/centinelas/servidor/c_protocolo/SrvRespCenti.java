/*
 Servlet para hacer inserciones o modificaciones de respuestas de un caso
 */
package centinelas.servidor.c_protocolo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import centinelas.datos.c_protocolo.DataResp;
import sapp.DBServlet;

/*Select, insert y updates sobre la tabla RESPEDO*/
public class SrvRespCenti
    extends DBServlet {

  // modos de operaci�n
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  /*final int servletSELECT = 2;*/

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    DataResp dataEntrada = null;
    CLista listaSalida = new CLista();
    DataResp dataSalida = null;

    // campos
    String Caso = "";
    String CodModelo = "";
    String Num = "";
    String CodPregunta = "";
    String Des = ""; //V
    String valor = ""; //Varon

    //Da de alta las respuestas de un caso
    final String sALTA_RESP = "insert into SIVE_RESP_CENTI "
        + "(CD_ENFCIE,NM_ORDEN,CD_PCENTI,CD_MEDCEN,CD_ANOEPI,CD_SEMEPI,"
        + "CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA,DS_RESPUESTA) "
        + "values (?, ?, ?, ?, ?, ?, ? , ?, ?, ?, ?)";

    //Borra las repuestas de un caso
    final String sBORRAR = "DELETE FROM SIVE_RESP_CENTI "
        + "WHERE (CD_ENFCIE = ? and NM_ORDEN = ? and CD_PCENTI = ?"
        + " and CD_MEDCEN = ? and CD_ANOEPI = ? and CD_SEMEPI = ?)";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {

      // modos de operaci�n
      switch (opmode) {

        //ALTA******************************************
        case servletALTA:

          //va bien: listaSalida.size() = 0
          //va mal: listaSalida = null y throws ex

          //recorremos la lista de entrada
          for (i = 0; i < param.size(); i++) {
            dataEntrada = (DataResp) param.elementAt(i);

            st = con.prepareStatement(sALTA_RESP);

            //todos OBLIGATORIOS *************************
            st.setString(1, dataEntrada.getNotifRMC().getCD_ENFCIE());

            Integer iCasoAlta = new Integer(dataEntrada.getCaso().trim());
            st.setInt(2, iCasoAlta.intValue());

            st.setString(3, dataEntrada.getNotifRMC().getCD_PCENTI());
            st.setString(4, dataEntrada.getNotifRMC().getCD_MEDCEN());
            st.setString(5, dataEntrada.getNotifRMC().getCD_ANOEPI());
            st.setString(6, dataEntrada.getNotifRMC().getCD_SEMEPI());
            st.setString(7, "C");
            st.setString(8, dataEntrada.getCodModelo().trim());

            Integer NumLinAlta = new Integer(dataEntrada.getNumLinea().trim());
            st.setInt(9, NumLinAlta.intValue());

            st.setString(10, dataEntrada.getCodPregunta().trim());
            st.setString(11, dataEntrada.getDesPregunta().trim()); //Respuesta
            st.executeUpdate();
            st.close();
            st = null;

            NumLinAlta = null;
            iCasoAlta = null;

          } //fin for

          break;

          //MODIFICAR******************************************
        case servletMODIFICAR:

          //va bien: listaSalida.size() = 0
          //va mal: listaSalida = null y throws ex

          //cargamos con lo que ha llegado en param (Lista)
          //(un registro) de momento
          dataEntrada = (DataResp) param.firstElement();
          //borro e inserto, para evitar que registros vacios queden
          //llenos y que no entren otros nuevos informados
          st = con.prepareStatement(sBORRAR);

          st.setString(1, dataEntrada.getNotifRMC().getCD_ENFCIE());

          Integer iCasoModificar = new Integer(dataEntrada.getCaso().trim());
          st.setInt(2, iCasoModificar.intValue());

          st.setString(3, dataEntrada.getNotifRMC().getCD_PCENTI());
          st.setString(4, dataEntrada.getNotifRMC().getCD_MEDCEN());
          st.setString(5, dataEntrada.getNotifRMC().getCD_ANOEPI());
          st.setString(6, dataEntrada.getNotifRMC().getCD_SEMEPI());

          st.executeUpdate();
          st.close();
          st = null;
          iCasoModificar = null;

          //modificando pero la lista viene con num caso,  + codmodelo=""
          //ie, han vaciado el protocolo, hay solo que borrar
          if (param.size() == 1
              && dataEntrada.getCodModelo().trim().equals("")) {
            //no debo hacer nada mas!!!
          }
          else {
            //recorremos la lista de entrada
            for (i = 0; i < param.size(); i++) {
              dataEntrada = (DataResp) param.elementAt(i);

              st = con.prepareStatement(sALTA_RESP);

              //todos OBLIGATORIOS *************************
              st.setString(1, dataEntrada.getNotifRMC().getCD_ENFCIE());

              Integer iCasoAlta = new Integer(dataEntrada.getCaso().trim());
              st.setInt(2, iCasoAlta.intValue());

              st.setString(3, dataEntrada.getNotifRMC().getCD_PCENTI());
              st.setString(4, dataEntrada.getNotifRMC().getCD_MEDCEN());
              st.setString(5, dataEntrada.getNotifRMC().getCD_ANOEPI());
              st.setString(6, dataEntrada.getNotifRMC().getCD_SEMEPI());
              st.setString(7, "C");
              st.setString(8, dataEntrada.getCodModelo().trim());

              Integer NumLinAlta = new Integer(dataEntrada.getNumLinea().trim());
              st.setInt(9, NumLinAlta.intValue());

              st.setString(10, dataEntrada.getCodPregunta().trim());
              st.setString(11, dataEntrada.getDesPregunta().trim()); //Respuesta
              st.executeUpdate();
              st.close();
              st = null;

              NumLinAlta = null;
              iCasoAlta = null;

              /*
                        st.setString(1, "E");
                        st.setString(2, dataEntrada.getCodModelo().trim());
                   Integer NumLinRecursivo = new Integer(dataEntrada.getNumLinea().trim());
                        st.setInt(3, NumLinRecursivo.intValue());
                        st.setString(4, dataEntrada.getCodPregunta().trim());
                   Integer iCasoRecursivo = new Integer(dataEntrada.getCaso().trim());
                        st.setInt(5, iCasoRecursivo.intValue());
                        st.setString(6, dataEntrada.getDesPregunta().trim());
                        st.executeUpdate();
                        st.close();
                        st=null;
                        NumLinRecursivo = null;
                        iCasoRecursivo = null;
               */
            } //fin for
          }

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;

      throw ex;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;
  } //fin doWork

} //fin de la clase