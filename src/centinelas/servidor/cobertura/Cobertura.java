/**
 * Clase: Cobertura
 * Paquete: centinelas.cliente.cobertura
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 31/01/2000
 * Analisis Funcional: Proceso de cobertura
 * Descripcion: Clase que contiene todos los m�todos necesarios
 *   para la generaci�n del proceso de cobertura.
 */

package centinelas.servidor.cobertura;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.Enumeration;

import centinelas.datos.centbasicos.EnfCent;
import centinelas.datos.centbasicos.MedCent;
import centinelas.datos.centbasicos.NotifRMC;
import centinelas.datos.comun.Secuenciador;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;

public class Cobertura {

  final static String CD_GRIPE = "487";

  public Cobertura() {
  }

  // Query de actualizacion de SIVE_SEMGEN_RMC
  final static String uAnoSemGen =
      "UPDATE SIVE_SEMGEN_RMC SET CD_ANOEPIG=?,CD_SEMEPIG=?";

  // Query de inserci�n de una NotifRMC
  final static String iNotifRMC =
      "INSERT INTO SIVE_NOTIF_RMC (CD_ENFCIE,CD_PCENTI," +
      "CD_MEDCEN,CD_ANOEPI,CD_SEMEPI,NM_CASOSDEC," +
      "FC_RECEP,IT_COBERTURA,CD_OPE,FC_ULTACT) " +
      "VALUES (?,?,?,?,?,?,?,?,?,?)";

  // Query de borrado de una NotifRMC
  final static String dNotifRMC =
      "DELETE FROM SIVE_NOTIF_RMC WHERE CD_ENFCIE = ? AND " +
      "CD_PCENTI = ? AND CD_MEDCEN = ? AND " +
      "CD_ANOEPI = ? AND CD_SEMEPI = ?";

  final static String dNotifRMCEntreSem =
      "DELETE FROM SIVE_NOTIF_RMC WHERE CD_PCENTI=? AND CD_MEDCEN=? AND (CD_ANOEPI||CD_SEMEPI) IN " +
      "(SELECT (CD_ANOEPI||CD_SEMEPI) FROM SIVE_NOTIF_RMC " +
      " WHERE CD_ANOEPI>=? AND CD_ANOEPI<=? MINUS " +
      " SELECT (CD_ANOEPI||CD_SEMEPI) FROM SIVE_NOTIF_RMC " +
      " WHERE CD_ANOEPI=? AND CD_SEMEPI<? MINUS " +
      " SELECT (CD_ANOEPI||CD_SEMEPI) FROM SIVE_NOTIF_RMC " +
      " WHERE CD_ANOEPI=? AND CD_SEMEPI>?)";

  // Inserci�n de un m�dico centinela
  final static String iMedCent =
      "INSERT INTO SIVE_MCENTINELA (CD_PCENTI,CD_MEDCEN,CD_E_NOTIF," +
      "CD_SEXO,CD_MASIST,CD_ANOEPIA,CD_SEMEPIA,DS_APE1,DS_APE2," +
      "DS_NOMBRE,DS_DNI,DS_TELEF,DS_FAX,FC_NAC,FC_ALTA,NMREDADI," +
      "NMREDADF,DS_HORAI,DS_HORAF,IT_BAJA,FC_BAJA,CD_ANOBAJA," +
      "CD_SEMBAJA,CD_MOTBAJA,CD_OPE,FC_ULTACT)" +
      "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

  // Modificaci�n de un m�dico centinela
  final static String mMedCent =
      "UPDATE SIVE_MCENTINELA SET CD_E_NOTIF=?,CD_SEXO=?,CD_MASIST=?," +
      "CD_ANOEPIA=?,CD_SEMEPIA=?,DS_APE1=?,DS_APE2=?," +
      "DS_NOMBRE=?,DS_DNI=?,DS_TELEF=?,DS_FAX=?,FC_NAC=?,FC_ALTA=?," +
      "NMREDADI=?,NMREDADF=?,DS_HORAI=?,DS_HORAF=?,IT_BAJA=?,FC_BAJA=?,CD_ANOBAJA=?," +
      "CD_SEMBAJA=?,CD_MOTBAJA=?,CD_OPE=?,FC_ULTACT=? " +
      "WHERE CD_PCENTI=? AND CD_MEDCEN=?";

  // Borrado de un m�dico
  final static String dMedCent =
      "DELETE FROM SIVE_MCENTINELA " +
      "WHERE CD_PCENTI=? AND CD_MEDCEN=?";

  // Seleccionar todas las enfermedades
  final static String sTodasEnf =
      "SELECT CD_ENFCIE, IT_ENVCNE, FC_ALTA, IT_RUTINA, " +
      "CD_SEMINIP, CD_SEMFINP, FC_BAJA, CD_ANOBAJA, CD_SEMBAJA," +
      "CD_OPE, FC_ULTACT FROM SIVE_ENF_CENTI";

  // Seleccionar todos los medicos
  final static String sTodosMed =
      "SELECT CD_PCENTI,CD_MEDCEN,CD_E_NOTIF,CD_SEXO," +
      "CD_MASIST,CD_ANOEPIA,CD_SEMEPIA,DS_APE1,DS_APE2," +
      "DS_NOMBRE,DS_DNI,DS_TELEF,DS_FAX,FC_NAC,FC_ALTA," +
      "CD_ANOBAJA,CD_SEMBAJA,NMREDADI,NMREDADF,DS_HORAI,DS_HORAF," +
      "IT_BAJA,FC_BAJA,CD_MOTBAJA,CD_OPE,FC_ULTACT " +
      "FROM SIVE_MCENTINELA";

  final static String sNotifRMCMed =
      "SELECT CD_PCENTI FROM SIVE_NOTIF_RMC " +
      "WHERE CD_PCENTI=? AND CD_MEDCEN=?";

  // Seleccionar el ano/sem gen
  final static String sAnoSemGen =
      "SELECT CD_ANOEPIG, CD_SEMEPIG " +
      "FROM SIVE_SEMGEN_RMC";

  // Seleccionar las ano/sem epidemiologicas entre 2 de ellas
  final static String sSemanas =
      "SELECT * FROM SIVE_SEMANA_EPI_RMC " +
      "WHERE cd_anoepi>=? AND cd_anoepi<=? " +
      "MINUS " +
      "SELECT * FROM SIVE_SEMANA_EPI_RMC " +
      "WHERE cd_anoepi=? AND cd_semepi<? " +
      "MINUS " +
      "SELECT * FROM SIVE_SEMANA_EPI_RMC " +
      "WHERE cd_anoepi=? AND cd_semepi>?";

  final static String sIT_GRIPE =
      "SELECT IT_GRIPE FROM SIVE_PCENTINELA " +
      "WHERE CD_PCENTI = ?";

  final static String sMinAnoSemBajaInsMed =
      "SELECT CD_ANOBAJA,CD_SEMBAJA FROM SIVE_MCENTINELA " +
      "WHERE CD_PCENTI=? AND (CD_ANOBAJA||CD_SEMBAJA) IN " +
      "(SELECT MAX(CD_ANOBAJA||CD_SEMBAJA) FROM SIVE_MCENTINELA " +
      "WHERE CD_PCENTI=?)";

  final static String sMaxAnoSemAltaModMed =
      "SELECT CD_ANOEPI,CD_SEMEPI FROM SIVE_NOTIF_RMC " +
      "WHERE CD_PCENTI=? AND CD_MEDCEN=? AND NM_CASOSDEC IS NOT NULL AND " +
      "(CD_ANOEPI||CD_SEMEPI) IN " +
      "(SELECT MIN(CD_ANOEPI||CD_SEMEPI) FROM SIVE_NOTIF_RMC " +
      " WHERE CD_PCENTI=? AND CD_MEDCEN=? AND NM_CASOSDEC IS NOT NULL)";

  final static String sMinAnoSemBajaBorMed =
      "SELECT CD_ANOEPI,CD_SEMEPI FROM SIVE_NOTIF_RMC " +
      "WHERE CD_PCENTI=? AND CD_MEDCEN=? AND NM_CASOSDEC IS NOT NULL AND " +
      "(CD_ANOEPI||CD_SEMEPI) IN " +
      "(SELECT MAX(CD_ANOEPI||CD_SEMEPI) FROM SIVE_NOTIF_RMC " +
      " WHERE CD_PCENTI=? AND CD_MEDCEN=? AND NM_CASOSDEC IS NOT NULL)";

  /***** Funciones b�sicas de inserci�n y borrado de NotifRMC *****/

  // Actualiza el ano/sem generada de SIVE_SEMGEN_RMC
  public static void actualizarAnoSemGen(Connection con,
                                         String nuevoAno,
                                         String nuevaSem) throws Exception {

    // Objetos JDBC
    PreparedStatement st = null;

    // Preparacion de la sentencia de inserci�n
    st = con.prepareStatement(uAnoSemGen);

    // Instanciamos la query con los valores de los datos de entrada
    st.setString(1, nuevoAno);
    st.setString(2, nuevaSem);

    // Ejecucion de la query, obteniendo el numero de inserciones
    int iUpdated = st.executeUpdate();
    if (iUpdated != 1) {
      throw (new Exception());
    }

    // Cierre del PreparedStatement
    st.close();
    st = null;
  } // Fin actualizarAnoSemGen()

  // Inserci�n de la notificaci�n para un a�o/semana/punto/medico/enf
  // Solo realiza la inserci�n a trav�s de la conexi�n que le pasan,
  //  no controla los errores ni hace commit ni rollback.
  public static void insertarNotifRMC(Connection con,
                                      NotifRMC n) throws Exception {

    // Objetos JDBC
    PreparedStatement st = null;

    // Preparacion de la sentencia de inserci�n
    st = con.prepareStatement(iNotifRMC);

    // Instanciamos la query con los valores de los datos de entrada
    // enf/punto/medico/ano/sem
    st.setString(1, n.getCD_ENFCIE());
    st.setString(2, n.getCD_PCENTI());
    st.setString(3, n.getCD_MEDCEN());
    st.setString(4, n.getCD_ANOEPI());
    st.setString(5, n.getCD_SEMEPI());

    // Numero casos declarados (puede ser null)
    if (n.getNM_CASOSDEC().equals("")) {
      st.setNull(6, Types.INTEGER);
    }
    else {
      st.setInt(6, (new Integer(n.getNM_CASOSDEC())).intValue());

      // Fecha Recepci�n (puede ser null)
    }
    if (n.getFC_RECEP().equals("")) {
      st.setNull(7, Types.DATE);
    }
    else {
      st.setDate(7, Format.string2Date(n.getFC_RECEP()));

      // De cobertura o Manual
    }
    st.setString(8, n.getIT_COBERTURA());

    // Bloqueo
    st.setString(9, n.getCD_OPE());
    st.setTimestamp(10, Format.string2Timestamp(n.getFC_ULTACT()));

    // Ejecucion de la query, obteniendo el numero de inserciones
    int iInserted = st.executeUpdate();
    if (iInserted != 1) {
      throw (new Exception());
    }

    // Cierre del PreparedStatement
    st.close();
    st = null;
  } // Fin insertarNotifRMC()

  // A partir de una lista de notificaciones l, y una conexi�n
  //  abierta, se inicializa e insertan las NotifRMC de la lista
  public static void insertarListaNotifRMC(Connection con,
                                           Lista l) throws Exception {

    // Inserci�n de todas las inserciones de la cobertura a la vez
    for (int i = 0; i < l.size(); i++) {
      insertarNotifRMC(con, (NotifRMC) l.elementAt(i));
    }
  } // Fin insertarListaNotifRMC()

  // Borrado de la notificaci�n para un a�o/semana/punto/medico/enf
  // Solo realiza el borrado a trav�s de la conexi�n que le pasan,
  //  no controla los errores ni hace commit ni rollback.
  public static void borrarNotifRMC(Connection con,
                                    NotifRMC n) throws Exception {

    // Objetos JDBC
    PreparedStatement st = null;

    // Preparacion de la sentencia de borrado
    st = con.prepareStatement(dNotifRMC);

    // Instanciamos la query con los valores de los datos de entrada
    // enf/punto/medico/ano/sem
    st.setString(1, n.getCD_ENFCIE());
    st.setString(2, n.getCD_PCENTI());
    st.setString(3, n.getCD_MEDCEN());
    st.setString(4, n.getCD_ANOEPI());
    st.setString(5, n.getCD_SEMEPI());

    // Ejecucion de la query, obteniendo el numero de borrados
    int iDeleted = st.executeUpdate();
    if (iDeleted != 1) {
      throw (new Exception());
    }

    // Cierre del PreparedStatement
    st.close();
    st = null;
  } // Fin borrarNotifRMC()

  // A partir de una lista de notificaciones l, y una conexi�n
  //  abierta, se inicializa y borran las NotifRMC de la lista
  public static void borrarListaNotifRMC(Connection con,
                                         Lista l) throws Exception {

    // Borrado de todas las inserciones de la cobertura a la vez
    for (int i = 0; i < l.size(); i++) {
      borrarNotifRMC(con, (NotifRMC) l.elementAt(i));

    }
  } // Fin borrarListaNotifRMC()

  // Borrado NotifRMC entre 2 semanas (inclusives)
  public static void borrarListaNotifRMCEntreSem(Connection con,
                                                 String pcenti, String medcen,
                                                 String anoIni, String semIni,
                                                 String anoFin, String semFin) throws
      Exception {

    // Objetos JDBC
    PreparedStatement st = null;

    // Preparacion de la sentencia de borrado
    st = con.prepareStatement(dNotifRMCEntreSem);

    // Instanciamos la query con los valores de los datos de entrada
    // enf/punto/medico/ano/sem

    st.setString(1, pcenti);
    st.setString(2, medcen);
    st.setString(3, anoIni);
    st.setString(4, anoFin);
    st.setString(5, anoIni);
    st.setString(6, semIni);
    st.setString(7, anoFin);
    st.setString(8, semFin);

    // Ejecucion de la query, obteniendo el numero de borrados
    int iDeleted = st.executeUpdate();

    // Cierre del PreparedStatement
    st.close();
    st = null;
  } // Fin borrarListaNotifRMCEntreSem()

  // Inserci�n del m�dico
  public static void insertarMedCent(Connection con, MedCent med,
                                     String sOpe, String fActual) throws
      Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Codigo secuencial del medico centinela
    String cd_medcen = Secuenciador.getSecuenciador(con, st, rs, "NM_MEDCENTI",
        6);
    med.put("CD_MEDCEN", cd_medcen);

    // CD_OPE/FC_ULTACT
    med.put("CD_OPE", sOpe);
    med.put("FC_ULTACT", fActual);

    // Preparacion de la sentencia de inserci�n
    st = con.prepareStatement(iMedCent);

    // Instanciamos la query con los valores de los datos de entrada
    st.setString(1, med.getCD_PCENTI());
    st.setString(2, med.getCD_MEDCEN());
    st.setString(3, med.getCD_E_NOTIF());
    st.setString(4, med.getCD_SEXO());
    st.setString(5, med.getCD_MASIST());
    st.setString(6, med.getCD_ANOEPIA());
    st.setString(7, med.getCD_SEMEPIA());
    st.setString(8, med.getDS_APE1());
    st.setString(9, med.getDS_APE2());
    st.setString(10, med.getDS_NOMBRE());
    st.setString(11, med.getDS_DNI());
    st.setString(12, med.getDS_TELEF());
    st.setString(13, med.getDS_FAX());
    if (med.getFC_NAC().equals("")) {
      st.setNull(14, Types.DATE);
    }
    else {
      st.setDate(14, Format.string2Date(med.getFC_NAC()));
    }
    st.setDate(15, Format.string2Date(med.getFC_ALTA()));
    st.setInt(16, Integer.parseInt(med.getNMREDADI()));
    st.setInt(17, Integer.parseInt(med.getNMREDADF()));
    st.setString(18, med.getDS_HORAI());
    st.setString(19, med.getDS_HORAF());
    st.setString(20, med.getIT_BAJA());
    st.setDate(21, Format.string2Date(med.getFC_BAJA()));
    st.setString(22, med.getCD_ANOBAJA());
    st.setString(23, med.getCD_SEMBAJA());
    st.setString(24, med.getCD_MOTBAJA());
    st.setString(25, med.getCD_OPE());
    st.setTimestamp(26, Format.string2Timestamp(med.getFC_ULTACT()));

    // Ejecucion de la query, obteniendo el numero de insertados
    int iInserted = st.executeUpdate();
    if (iInserted != 1) {
      throw (new Exception());
    }

    // Cierre del PreparedStatement
    st.close();
    st = null;
  } // Fin insertarMedCent()

  // Modificaci�n del m�dico
  public static void modificarMedCent(Connection con, MedCent med,
                                      String sOpe, String fActual) throws
      Exception {
    // Objetos JDBC
    PreparedStatement st = null;

    // Preparacion de la sentencia de modificaci�n
    st = con.prepareStatement(mMedCent);

    // Actualizacion de CD_OPE/FC_ULTACT
    med.put("CD_OPE", sOpe);
    med.put("FC_ULTACT", fActual);

    // Instanciamos la query: valores del SET
    st.setString(1, med.getCD_E_NOTIF());
    st.setString(2, med.getCD_SEXO());
    st.setString(3, med.getCD_MASIST());
    st.setString(4, med.getCD_ANOEPIA());
    st.setString(5, med.getCD_SEMEPIA());
    st.setString(6, med.getDS_APE1());
    st.setString(7, med.getDS_APE2());
    st.setString(8, med.getDS_NOMBRE());
    st.setString(9, med.getDS_DNI());
    st.setString(10, med.getDS_TELEF());
    st.setString(11, med.getDS_FAX());
    st.setDate(12, Format.string2Date(med.getFC_NAC()));
    st.setDate(13, Format.string2Date(med.getFC_ALTA()));
    st.setInt(14, Integer.parseInt(med.getNMREDADI()));
    st.setInt(15, Integer.parseInt(med.getNMREDADF()));
    st.setString(16, med.getDS_HORAI());
    st.setString(17, med.getDS_HORAF());
    st.setString(18, med.getIT_BAJA());
    st.setDate(19, Format.string2Date(med.getFC_BAJA()));
    st.setString(20, med.getCD_ANOBAJA());
    st.setString(21, med.getCD_SEMBAJA());
    st.setString(22, med.getCD_MOTBAJA());
    st.setString(23, med.getCD_OPE());
    st.setTimestamp(24, Format.string2Timestamp(med.getFC_ULTACT()));

    // Instanciamos la query: Valores del WHERE
    st.setString(25, med.getCD_PCENTI());
    st.setString(26, med.getCD_MEDCEN());

    // Ejecucion de la query, obteniendo el numero de modificados
    int iModified = st.executeUpdate();
    if (iModified != 1) {
      throw (new Exception());
    }

    // Cierre del PreparedStatement
    st.close();
    st = null;
  } // Fin modificarMedCent()

  // Borrado de un m�dico (fisico o por IT_BAJA dependiendo de si
  //  existen NotifRMC asociadas al medico o no)
  public static void borrarMedCent(Connection con, MedCent med,
                                   String sOpe, String fActual) throws
      Exception {

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    /************** Existen NotifRMC ? *************/
    st = con.prepareStatement(sNotifRMCMed);
    // Instanciamos la query: Valores del WHERE
    st.setString(1, med.getCD_PCENTI());
    st.setString(2, med.getCD_MEDCEN());
    // Ejecucion de la query
    rs = st.executeQuery();

    // Si existe al menos una notificaci�n: borrado por IT_BAJA
    if (rs.next()) {
      modificarMedCent(con, med, sOpe, fActual);
    }
    else { // Borrado fisico porque no existe NotiRMC asociada al medico
      // Preparacion de la sentencia de borrado
      st = con.prepareStatement(dMedCent);

      // Instanciamos la query: Valores del WHERE
      st.setString(1, med.getCD_PCENTI());
      st.setString(2, med.getCD_MEDCEN());

      // Ejecucion de la query, obteniendo el numero de modificados
      int iDeleted = st.executeUpdate();
      if (iDeleted != 1) {
        throw (new Exception());
      }

      // Cierre del PreparedStatement
      st.close();
      st = null;
    }
  } // Fin  borrarMedCent()

  /*** FIN Funciones b�sicas de inserci�n y borrado  ***/

  /********************************************************************/
  // Creaci�n de las notificaciones que implica la generaci�n de cobertura
  //  al correr hacia delante el ano/sem generados
  /********************************************************************/
  // Pseudoalgoritmo:
  // Recorrer las semanas epidemiologicas que haya entre SEM_GEN y la nueva
  //   Recorrer todas las enfermedades SIVE_ENF_CENTI
  //     Considerar las que est�n dentro de la cobertura (IT_RUTINA/FC_BAJA/CD_SEMINIP-CD_SEMFINP)
  //     Para cada medico activo en ese semana (FC_BAJA)
  //        Insertar una notificaci�n con:
  //          ano, sem, enf, punto, medico,
  //          NM_CASOSDEC=null, FC_RECEP=null, IT_COBERTURA="S",CD_OPE/FC_ULTACT
  //
  // Parametros:
  // @param con: conexion con la BD
  // @param ano/sem: ano/sem hasta los que hay que generar cobertura
  // @param sOpe: indica quien genera la cobertura
  // @return: 0 -> No se ha generado cobertura porque no procedia
  // @return: 1 -> Se ha generado cobertura correctamente
  // @return: 2 -> Error al generar la cobertura
  public static int generarCobertura0(Connection con,
                                      String sAno,
                                      String sSem,
                                      String sOpe) throws Exception {

    // Lista en la que se van a�adiendo las notificaciones que
    //  se a�adiran en el proceso de generacion de lacobertura
    Lista lNotifRMC = new Lista();
    NotifRMC nRMC = null;

    // Fecha actual: sera introducida como fecha para todas las
    //   notificaciones generadas en la cobertura
    String fActual = Format.fechaHoraActual();

    /*********************************************************/
    // Obtenci�n de la ultima semana generada (SIVE_SEMGEN_RMC)
    /*********************************************************/
    String sAnoGen = "";
    String sSemGen = "";
    Data dtAnoSemGen = obtenerAnoSemGen(con);
    sAnoGen = (String) dtAnoSemGen.get("ANO");
    sSemGen = (String) dtAnoSemGen.get("SEM");

    // Valores enteros de los a�os/semanas
    int iAno = Integer.parseInt(sAno);
    int iSem = Integer.parseInt(sSem);
    int iAnoGen = Integer.parseInt(sAnoGen);
    int iSemGen = Integer.parseInt(sSemGen);
    // Si el ano/sem es menor que sAnoGen/sSemGen no se genera cobertura
    if (iAno < iAnoGen) {
      return 0;
    }
    if (iAno == iAnoGen && iSem <= iSemGen) {
      return 0;
    }

    /*********************************************************/
    // Obtenci�n de todas enfermedades centinela (SIVE_ENF_CENTI)
    /*********************************************************/
    Lista lEnf = null; // Lista de enfermedades centinela con CD_ANOBAJA y CD_SEMBAJA
    EnfCent enf = null; // Enfermedad centinela
    lEnf = obtenerEnfermedades(con);

    /*********************************************************/
    // Obtenci�n de todos medicos (SIVE_MCENTINELA)
    /*********************************************************/
    Lista lMed = null; // Lista de medicos centinela con CD_ANOBAJA y CD_SEMBAJA
    MedCent med = null;
    lMed = obtenerMedicos(con);

    /*********************************************************/
    // Obtenci�n de las semanas desde ano_gen/sem_gen+1 hasta ano/sem
    /*********************************************************/
    Lista lSemanas = null;
    lSemanas = obtenerSemanas(con, sAnoGen, sSemGen, sAno, sSem);

    // Variables utilizadas en el recorrido que genera
    Data dtAnoSem = null;
    String ano_i;
    String sem_i;

    // Recorrido desde ano_gen/sem_gen+1 hasta ano/sem de todas la semanas
    for (int h = 1; h < lSemanas.size(); h++) {

      // Obtenci�n del ano/sem a tratar
      dtAnoSem = (Data) lSemanas.elementAt(h);
      ano_i = (String) dtAnoSem.get("ANO");
      sem_i = (String) dtAnoSem.get("SEM");

      // Recorrido de todas la enfermedades
      for (int i = 0; i < lEnf.size(); i++) {
        // Determinar si la enfermedad cae dentro de la cobertura
        enf = (EnfCent) lEnf.elementAt(i);
        if (enfEnCobertura(enf, ano_i, sem_i)) {
          // Recorrer los m�dicos centinelas y obtener los que est�n
          //  activos para el ano/sem bajo consideracion
          for (int j = 0; j < lMed.size(); j++) {
            // Determinar si el medico cae dentro de la cobertura
            med = (MedCent) lMed.elementAt(j);
            if (medEnCobertura(med, ano_i, sem_i)) {

              // Si es la gripe e IT_GRIPE del punto al que pertenece el
              //  medico='N', no se inserta la NotifRMC correspondiente
              if (enf.getCD_ENFCIE().equals(CD_GRIPE)) {
                if (obtenerIT_GRIPE(con, med).equals("N")) {
                  continue; // Proxima iteracion
                }
              }

              // Creacion de la NotifRMC
              nRMC = new NotifRMC(enf.getCD_ENFCIE(), med.getCD_PCENTI(),
                                  med.getCD_MEDCEN(),
                                  ano_i, sem_i, "", "", "S", sOpe, fActual);

              // Adici�n de la NotifRMC a la lista
              lNotifRMC.addElement(nRMC);
            } // medEnCobertura
          } // Fin for todos los medicos
        } // enfEnCobertura
      } // Fin for todas las enfermedades
    } // Fin for todas las semanas

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con.setAutoCommit(false);

    try {
      // Actualizacion de la ultima ano/sem generada
      actualizarAnoSemGen(con, sAno, sSem);

      // Adici�n de todas las NotifRMC creadas
      insertarListaNotifRMC(con, lNotifRMC);

      // Si todas las queries han ido bien se hace un commit
      con.commit();

      // Todo ha ido bien
      return 1;

    }
    catch (Exception ex) {
      // Si alguna query falla se hace rollback
      con.rollback();

      ex.printStackTrace();

      // Ha ido mal el proceso de generacion de la cobertura
      return 2;
    }

  } // Fin generarCobertura0()

  /********************************************************************/
  // Creaci�n de las notificaciones que implica la generaci�n de cobertura
  //  al insertar un m�dico
  /********************************************************************/
  // Pseudoalgoritmo:
  // Recorrer las semanas epidemiologicas que haya entre SemAlta y SEM_GEN
  //   Recorrer todas las enfermedades SIVE_ENF_CENTI
  //     Considerar las que est�n dentro de la cobertura (IT_RUTINA/FC_BAJA/CD_SEMINIP-CD_SEMFINP)
  //        Insertar una notificaci�n con:
  //          ano, sem, enf, punto_insertado, med_insertado,
  //          NM_CASOSDEC=null, FC_RECEP=null, IT_COBERTURA="S",CD_OPE/FC_ULTACT
  //
  // Parametros:
  // @param con: conexion con la BD
  // @param med: MedCent a insertar
  // @param sOpe: indica quien genera la cobertura
  // @return: 0 -> No se ha generado cobertura porque no procedia
  // @return: 1 -> Se ha generado cobertura correctamente
  // @return: 2 -> Error al generar la cobertura
  public static int generarCobertura1(Connection con,
                                      MedCent med,
                                      String sOpe) throws Exception {
    // Indica si se debe generar cobertura o no
    boolean bCob = true;

    // Lista en la que se van a�adiendo las notificaciones que
    //  se a�adiran en el proceso de generacion de la cobertura
    Lista lNotifRMC = new Lista();
    NotifRMC nRMC = null;

    // Fecha actual: sera introducida como fecha para todas las
    //   notificaciones generadas en la cobertura
    String fActual = Format.fechaHoraActual();

    // Ano/Sem de Alta
    String sAnoAlta = med.getCD_ANOEPIA();
    String sSemAlta = med.getCD_SEMEPIA();

    /*********************************************************/
    // Obtenci�n de la ultima semana generada (SIVE_SEMGEN_RMC)
    /*********************************************************/
    String sAnoGen = "";
    String sSemGen = "";
    Data dtAnoSemGen = obtenerAnoSemGen(con);
    sAnoGen = (String) dtAnoSemGen.get("ANO");
    sSemGen = (String) dtAnoSemGen.get("SEM");

    // Valores enteros de los a�os/semanas
    int iAnoAlta = Integer.parseInt(sAnoAlta);
    int iSemAlta = Integer.parseInt(sSemAlta);
    int iAnoGen = Integer.parseInt(sAnoGen);
    int iSemGen = Integer.parseInt(sSemGen);
    // Si sAnoGen/sSemGen es menor que sAnoAlta/sSemAlta no se genera cobertura
    if (iAnoGen < iAnoAlta) {
      bCob = false;
    }
    if (iAnoGen == iAnoAlta && iSemGen < iSemAlta) {
      bCob = false;

    }

    if (bCob) {
      /*********************************************************/
      // Obtenci�n de todas enfermedades centinela (SIVE_ENF_CENTI)
      /*********************************************************/
      Lista lEnf = null; // Lista de enfermedades centinela con CD_ANOBAJA y CD_SEMBAJA
      EnfCent enf = null; // Enfermedad centinela
      lEnf = obtenerEnfermedades(con);

      /*********************************************************/
      // Obtenci�n de las semanas desde anoAlta/semAlta hasta anoGen/semGen inclusives
      /*********************************************************/
      Lista lSemanas = null;
      lSemanas = obtenerSemanas(con, sAnoAlta, sSemAlta, sAnoGen, sSemGen);

      // Variables utilizadas en el recorrido que genera
      Data dtAnoSem = null;
      String ano_i;
      String sem_i;

      // Recorrido desde ano_alta/sem_alta+1 hasta ano_gen/sem_gen de todas la semanas
      for (Enumeration e = lSemanas.elements(); e.hasMoreElements(); ) {

        // Obtenci�n del ano/sem a tratar
        dtAnoSem = (Data) e.nextElement();
        ano_i = (String) dtAnoSem.get("ANO");
        sem_i = (String) dtAnoSem.get("SEM");

        // Recorrido de todas la enfermedades
        for (int i = 0; i < lEnf.size(); i++) {
          // Determinar si la enfermedad cae dentro de la cobertura
          enf = (EnfCent) lEnf.elementAt(i);
          if (enfEnCobertura(enf, ano_i, sem_i)) {

            // Si es la gripe e IT_GRIPE del punto al que pertenece el
            //  medico='N', no se inserta la NotifRMC correspondiente
            if (enf.getCD_ENFCIE().equals(CD_GRIPE)) {
              if (obtenerIT_GRIPE(con, med).equals("N")) {
                continue; // Proxima iteracion
              }
            }

            // Creacion de la NotifRMC
            nRMC = new NotifRMC(enf.getCD_ENFCIE(), med.getCD_PCENTI(),
                                med.getCD_MEDCEN(),
                                ano_i, sem_i, "", "", "S", sOpe, fActual);

            // Adici�n de la NotifRMC a la lista
            lNotifRMC.addElement(nRMC);

          } // enfEnCobertura
        } // Fin for todas las enfermedades
      } // Fin for todas las semanas
    } // Fin if(bCob)

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con.setAutoCommit(false);

    try {

      // Inserci�n del m�dico (previa a las notificaciones por integridad referencial)
      insertarMedCent(con, med, sOpe, fActual);

      // Adici�n de todas las NotifRMC creadas
      if (bCob) {
        // Para cada NotifRMC se debe establecer el medico
        NotifRMC n = null;
        for (int i = 0; i < lNotifRMC.size(); i++) {
          n = (NotifRMC) lNotifRMC.elementAt(i);
          n.put("CD_MEDCEN", med.getCD_MEDCEN());
        }

        insertarListaNotifRMC(con, lNotifRMC);
      }

      // Si todas las queries han ido bien se hace un commit
      con.commit();

      // Todo ha ido bien (Generando cobertura o sin generarla)
      if (bCob) {
        return 1;
      }
      else {
        return 0;
      }

    }
    catch (Exception ex) {
      // Si alguna query falla se hace rollback
      con.rollback();

      ex.printStackTrace();

      // Ha ido mal el proceso de generacion de la cobertura
      return 2;
    }

  } // Fin generarCobertura1()

  /********************************************************************/
  // Obtenci�n de la ano/sem epidemiologica menor a partir de la cual se
  //  puede dar de alta a un medico centinela para que no se solape con
  //  las semanas epidemiologicas de otro medico en un determinado punto
  /********************************************************************/
  // Pseudoalgoritmo:
  //  Para todos los medicos de un punto obtener la maxima ano/sem baja
  //    existente y sumarle uno
  // Parametros:
  // @param con: conexion con la BD
  // @return Data: key ANO (a�o o ""), key SEM (semana o "")
  // Si va mal la query salta una excepci�n
  public static Data obtenerMinAnoSemAltaInsMed(Connection con,
                                                String sPuntoCent) throws
      Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    String anoBaja = "";
    String semBaja = "";
    Data dtResult = new Data();

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con.setAutoCommit(false);

    try {
      // Preparacion de la sentencia SQL
      st = con.prepareStatement(sMinAnoSemBajaInsMed);

      // Instanciacion de los valores de la query
      st.setString(1, sPuntoCent);
      st.setString(2, sPuntoCent);

      // Ejecucion de la query, obteniendo un ResultSet
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      if (rs.next()) {
        anoBaja = rs.getString("CD_ANOBAJA");
        semBaja = rs.getString("CD_SEMBAJA");
        if (anoBaja == null || semBaja == null) {
          anoBaja = "";
          semBaja = "";
        }
      }

      // Si null se establecen con ""
      dtResult.put("ANO", anoBaja);
      dtResult.put("SEM", semBaja);

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

      // Return
      return incrementarAnoSem(con, dtResult);
    }
    catch (Exception e) {
      con.rollback();
      dtResult = null;
      throw e;
    }
  } // Fin obtenerMinAnoSemAltaInsMed()

  /********************************************************************/
  // Obtenci�n de la maxima y minima ano/sem epidemiologica entre las que
  //  se puede modificar la semana de alta de un medico centinela para que
  //  no se solape con las semanas epidemiologicas de otro medico en un
  //  determinado punto ni se solape con una semana en la que el m�dico ya
  //  haya notificado (NM_CASOSDEC>=0)
  /********************************************************************/
  // Parametros:
  // @param con: conexion con la BD
  // @return Data: key MAXANO (a�o o ""), key MAXSEM (semana o ""),
  //               key MINANO (a�o o ""), key MINSEM (semana o ""),
  // Si va mal alguna query salta una excepci�n
  public static Data obtenerMaxMinAnoSemAltaModMed(Connection con,
      MedCent med) throws Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    String maxAno = "";
    String maxSem = "";
    Data dtAnoSemMin = new Data();
    Data dtAnoSemMax = new Data();
    Data dtResult = new Data();

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con.setAutoCommit(false);

    // Obtencion de la minima ano/sem
    dtAnoSemMin = obtenerMinAnoSemAltaInsMed(con, med.getCD_PCENTI());

    // Obtencion de la m�xima ano/sem
    try {
      // Preparacion de la sentencia SQL
      st = con.prepareStatement(sMaxAnoSemAltaModMed);

      // Instanciacion de los valores de la query
      st.setString(1, med.getCD_PCENTI());
      st.setString(2, med.getCD_MEDCEN());
      st.setString(3, med.getCD_PCENTI());
      st.setString(4, med.getCD_MEDCEN());

      // Ejecucion de la query, obteniendo un ResultSet
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      if (rs.next()) {
        maxAno = rs.getString("CD_ANOEPI");
        maxSem = rs.getString("CD_SEMEPI");
        if (maxAno == null || maxSem == null) {
          maxAno = "";
          maxSem = "";
        }
      }

      // Si son null se establecen a ""
      dtAnoSemMax.put("ANO", maxAno);
      dtAnoSemMax.put("SEM", maxSem);
      //dtAnoSemMax = decrementarAnoSem(con,dtAnoSemMax);

      // Valor de retorno
      dtResult.put("MAXANO", dtAnoSemMax.get("ANO"));
      dtResult.put("MAXSEM", dtAnoSemMax.get("SEM"));
      dtResult.put("MINANO", dtAnoSemMin.get("ANO"));
      dtResult.put("MINSEM", dtAnoSemMin.get("SEM"));

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

      // Return
      return dtResult;
    }
    catch (Exception e) {
      con.rollback();
      dtResult = null;
      throw e;
    }
  } // Fin obtenerMaxMinAnoSemAltaModMed()

  /********************************************************************/
  // Creaci�n/Eliminaci�n de las notificaciones que implica la generaci�n
  //  de cobertura al modificar la semana de alta de un m�dico
  /********************************************************************/
  //
  // Parametros:
  // @param con: conexion con la BD
  // @param med: MedCent a modificar
  // @param oldAno/oldSem:  ano/sem de alta anteriores a la modificacion
  // @param sOpe: indica quien genera la cobertura
  // @return: 0 -> No se ha generado cobertura porque no procedia
  // @return: 1 -> Se ha generado cobertura correctamente
  // @return: 2 -> Error al generar la cobertura
  public static int generarCobertura2(Connection con,
                                      MedCent med,
                                      String oldAno,
                                      String oldSem,
                                      String sOpe) throws Exception {

    // Lista en la que se van a�adiendo las notificaciones que
    //  se a�adiran/borraran en el proceso de generacion de la cobertura
    Lista lNotifRMC = new Lista();
    NotifRMC nRMC = null;

    // Nuevos Ano/Sem de Alta
    String newAno = med.getCD_ANOEPIA();
    String newSem = med.getCD_SEMEPIA();

    // Fecha actual: sera introducida como fecha para todas las
    //   notificaciones generadas en la cobertura
    String fActual = Format.fechaHoraActual();

    // Si newAno/Sem == oldAno/Sem no se hace nada
    // Si newAno/Sem < oldAno/Sem inserci�n de notificaciones
    // Si newAno/Sem > oldAno/Sem borrado de notificaciones
    if ( (newAno + newSem).compareTo(oldAno + oldSem) == 0) {
      modificarMedCent(con, med, sOpe, fActual);
      con.commit();
      return 0;
    }
    else if ( (newAno + newSem).compareTo(oldAno + oldSem) < 0) {

      /*********************************************************/
      // Obtenci�n de todas enfermedades centinela (SIVE_ENF_CENTI)
      /*********************************************************/
      Lista lEnf = null; // Lista de enfermedades centinela con CD_ANOBAJA y CD_SEMBAJA
      EnfCent enf = null; // Enfermedad centinela
      lEnf = obtenerEnfermedades(con);

      /*********************************************************/
      // Obtenci�n de la ultima semana generada (SIVE_SEMGEN_RMC)
      /*********************************************************/
      String sAnoGen = "";
      String sSemGen = "";
      Data dtAnoSemGen = obtenerAnoSemGen(con);
      sAnoGen = (String) dtAnoSemGen.get("ANO");
      sSemGen = (String) dtAnoSemGen.get("SEM");

      Data dtAnoSemLimSup = new Data();
      Data dtSemLimSupMenos1 = null;
      if ( (sAnoGen + sSemGen).compareTo(oldAno + oldSem) < 0) {
        dtSemLimSupMenos1 = dtAnoSemGen;
      }
      else {
        dtAnoSemLimSup.put("ANO", oldAno);
        dtAnoSemLimSup.put("SEM", oldSem);
        dtSemLimSupMenos1 = decrementarAnoSem(con, dtAnoSemLimSup);
      }

      /*********************************************************/
      // Obtenci�n de las semanas desde newAno/newSem hasta
      //  oldAno/oldSem-1 inclusives
      /*********************************************************/
      Lista lSemanas = null;
      lSemanas = obtenerSemanas(con, newAno, newSem,
                                (String) dtSemLimSupMenos1.get("ANO"),
                                (String) dtSemLimSupMenos1.get("SEM"));

      // Variables utilizadas en el recorrido que genera
      Data dtAnoSem = null;
      String ano_i;
      String sem_i;

      // Recorrido desde oldAno/oldSem hasta newAno/newSem-1 de todas la semanas
      for (Enumeration e = lSemanas.elements(); e.hasMoreElements(); ) {

        // Obtenci�n del ano/sem a tratar
        dtAnoSem = (Data) e.nextElement();
        ano_i = (String) dtAnoSem.get("ANO");
        sem_i = (String) dtAnoSem.get("SEM");

        // Recorrido de todas la enfermedades
        for (int i = 0; i < lEnf.size(); i++) {
          // Determinar si la enfermedad cae dentro de la cobertura
          enf = (EnfCent) lEnf.elementAt(i);
          if (enfEnCobertura(enf, ano_i, sem_i)) {

            // Si es la gripe e IT_GRIPE del punto al que pertenece el
            //  medico='N', no se inserta la NotifRMC correspondiente
            if (enf.getCD_ENFCIE().equals(CD_GRIPE)) {
              if (obtenerIT_GRIPE(con, med).equals("N")) {
                continue; // Proxima iteracion
              }
            }

            // Creacion de la NotifRMC
            nRMC = new NotifRMC(enf.getCD_ENFCIE(), med.getCD_PCENTI(),
                                med.getCD_MEDCEN(),
                                ano_i, sem_i, "", "", "S", sOpe, fActual);

            // Adici�n de la NotifRMC a la lista
            lNotifRMC.addElement(nRMC);

          } // enfEnCobertura
        } // Fin for todas las enfermedades
      } // Fin for todas las semanas

      // Establece la conexi�n con la base de datos, sin que
      //  el commit se haga automaticamente sobre cada actualizacion
      con.setAutoCommit(false);

      try {
        // Modificaci�n del m�dico
        modificarMedCent(con, med, sOpe, fActual);

        // Adici�n de todas las NotifRMC creadas
        insertarListaNotifRMC(con, lNotifRMC);

        // Si todas las queries han ido bien se hace un commit
        con.commit();

        // Todo ha ido bien
        return 1;

      }
      catch (Exception ex) {
        // Si alguna query falla se hace rollback
        con.rollback();

        ex.printStackTrace();

        // Ha ido mal el proceso de generacion de la cobertura
        return 2;
      }

    }
    else if ( (newAno + newSem).compareTo(oldAno + oldSem) > 0) {

      /*********************************************************/
      // Obtenci�n de las semanas desde newAno/newSem hasta
      //  oldAno/oldSem-1 inclusives
      /*********************************************************/
      Data dtNewSem = new Data();
      dtNewSem.put("ANO", newAno);
      dtNewSem.put("SEM", newSem);
      Data dtNewSemMenos1 = decrementarAnoSem(con, dtNewSem);

      // LA LISTA DE QUERIES A BORRAR SE HACE DIRECTAMENTE MEDIANTE UNA QUERY

      // Establece la conexi�n con la base de datos, sin que
      //  el commit se haga automaticamente sobre cada actualizacion
      con.setAutoCommit(false);

      try {
        // Modificaci�n del m�dico
        modificarMedCent(con, med, sOpe, fActual);

        // Borrado de todas las NotifRMC que hay que eliminar
        borrarListaNotifRMCEntreSem(con, med.getCD_PCENTI(), med.getCD_MEDCEN(),
                                    oldAno, oldSem,
                                    (String) dtNewSemMenos1.get("ANO"),
                                    (String) dtNewSemMenos1.get("SEM"));

        // Si todas las queries han ido bien se hace un commit
        con.commit();

        // Todo ha ido bien
        return 1;

      }
      catch (Exception ex) {
        // Si alguna query falla se hace rollback
        con.rollback();

        ex.printStackTrace();

        // Ha ido mal el proceso de generacion de la cobertura
        return 2;
      }
    }
    else {
      return 0;
    }
  } // Fin generarCobertura2()

  /********************************************************************/
  // Obtenci�n de la ano/sem epidemiologica menor a partir de la cual se
  //  puede dar de baja a un medico centinela para que no existan semanas
  //  con NotifRMCs que tengan NM_CASOSDEC>=0
  /********************************************************************/
  // Pseudoalgoritmo:
  //  Para todas las NotifRMCs de un m�dico en un punto calcular el ano/sem
  //   posterior al ano/sem de la NotifRMC m�s reciente en el tiempo
  // Parametros:
  // @param con: conexion con la BD
  // @return Data: key ANO (a�o o ""), key SEM (semana o "")
  // Si va mal la query salta una excepci�n
  public static Data obtenerMinAnoSemBajaBorMed(Connection con, MedCent med) throws
      Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    String anoBaja = "";
    String semBaja = "";
    Data dtResult = new Data();

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con.setAutoCommit(false);

    try {
      // Preparacion de la sentencia SQL
      st = con.prepareStatement(sMinAnoSemBajaBorMed);

      // Instanciacion de los valores de la query
      st.setString(1, med.getCD_PCENTI());
      st.setString(2, med.getCD_MEDCEN());
      st.setString(3, med.getCD_PCENTI());
      st.setString(4, med.getCD_MEDCEN());

      // Ejecucion de la query, obteniendo un ResultSet
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      if (rs.next()) {
        anoBaja = rs.getString("CD_ANOEPI");
        semBaja = rs.getString("CD_SEMEPI");
        if (anoBaja == null || semBaja == null) {
          anoBaja = "";
          semBaja = "";
        }
      }

      // Si null s eestablecen con ""
      dtResult.put("ANO", anoBaja);
      dtResult.put("SEM", semBaja);

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

      // Return
      //return incrementarAnoSem(con,dtResult);
      return dtResult;
    }
    catch (Exception e) {
      con.rollback();
      dtResult = null;
      throw e;
    }
  } // Fin obtenerMinAnoSemBajaBorMed()

  /********************************************************************/
  // Eliminaci�n de las notificaciones que implica la generaci�n
  //  de cobertura al borrar un m�dico centinela
  /********************************************************************/
  //
  // Parametros:
  // @param con: conexion con la BD
  // @param med: MedCent a borrar
  // @param sOpe: indica quien genera la cobertura
  // @return: 1 -> Se ha generado cobertura correctamente
  // @return: 2 -> Error al generar la cobertura
  public static int generarCobertura3(Connection con,
                                      MedCent med,
                                      String sOpe) throws Exception {

    // Ano/Sem de baja
    Data dtAnoSemBaja = new Data();
    dtAnoSemBaja.put("ANO", med.getCD_ANOBAJA());
    dtAnoSemBaja.put("SEM", med.getCD_SEMBAJA());
    Data dtAnoSemBajaMas1 = incrementarAnoSem(con, dtAnoSemBaja);

    /*********************************************************/
    // Obtenci�n de la ultima semana generada (SIVE_SEMGEN_RMC)
    /*********************************************************/
    String sAnoGen = "";
    String sSemGen = "";
    Data dtAnoSemGen = obtenerAnoSemGen(con);
    sAnoGen = (String) dtAnoSemGen.get("ANO");
    sSemGen = (String) dtAnoSemGen.get("SEM");

    // Fecha actual: sera introducida como fecha para el m�dico
    //   en caso de ser borrado por IT_BAJA
    String fActual = Format.fechaHoraActual();

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con.setAutoCommit(false);

    try {
      // Borrado de todas las NotifRMC que hay que eliminar
      //  (previa al m�dico por integridad referencial)
      borrarListaNotifRMCEntreSem(con, med.getCD_PCENTI(),
                                  med.getCD_MEDCEN(),
                                  (String) dtAnoSemBajaMas1.get("ANO"),
                                  (String) dtAnoSemBajaMas1.get("SEM"),
                                  sAnoGen, sSemGen);

      // Borrado del m�dico
      borrarMedCent(con, med, sOpe, fActual);

      // Si todas las queries han ido bien se hace un commit
      con.commit();

      // Todo ha ido bien
      return 1;

    }
    catch (Exception ex) {
      // Si alguna query falla se hace rollback
      con.rollback();

      ex.printStackTrace();

      // Ha ido mal el proceso de generacion de la cobertura
      return 2;
    }
  } // Fin generarCobertura3()

  /**************** Funciones auxiliares ********************/

  // Acceso a BD para obtener el ANO/SEM de la tabla SIVE_SEMGEN_RMC
  // @return: Data con ("ANO",a�o) y ("SEM",sem)
  public static Data obtenerAnoSemGen(Connection con) throws Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    Data dtAnoSemGen = new Data();

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con.setAutoCommit(false);

    try {
      // Preparacion de la sentencia SQL
      st = con.prepareStatement(sAnoSemGen);

      // Ejecucion de la query, obteniendo un ResultSet
      rs = st.executeQuery();

      if (rs.next()) {
        dtAnoSemGen.put("ANO", rs.getString("CD_ANOEPIG"));
        dtAnoSemGen.put("SEM", rs.getString("CD_SEMEPIG"));
      }
      else {
        throw (new Exception());
      }

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();
    }
    catch (Exception e) {
      con.rollback();
      dtAnoSemGen = null;
      throw e;
    }

    return dtAnoSemGen;
  } // Fin obtenerAnoSemGen()

  // Obtener todas las enfermedades SIVE_ENF_CENTI
  // Inicializa la conexi�n
  public static Lista obtenerEnfermedades(Connection con) throws Exception {

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    String query = "";

    // Objetos de datos
    Lista lResult = new Lista();
    EnfCent enf = null;

    // Querys
    String sQuery = "";

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con.setAutoCommit(false);

    try {
      // Preparacion de la sentencia SQL
      st = con.prepareStatement(sTodasEnf);

      // Ejecucion de la query, obteniendo un ResultSet
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      while (rs.next()) {
        // Recuperacion de los datos a partir del ResultSet de la
        //  query realizada sobre la BD
        String sCD_ENFCIE = rs.getString("CD_ENFCIE");
        String sIT_ENVCNE = rs.getString("IT_ENVCNE");
        java.sql.Date dFC_ALTA = rs.getDate("FC_ALTA");
        String sFC_ALTA = Format.date2String(dFC_ALTA);
        String sIT_RUTINA = rs.getString("IT_RUTINA");
        String sCD_SEMINIP = rs.getString("CD_SEMINIP");
        String sCD_SEMFINP = rs.getString("CD_SEMFINP");
        java.sql.Date dFC_BAJA = rs.getDate("FC_BAJA");
        String sFC_BAJA = Format.date2String(dFC_BAJA);
        String sCD_ANOBAJA = rs.getString("CD_ANOBAJA");
        String sCD_SEMBAJA = rs.getString("CD_SEMBAJA");
        String sCD_OPE = rs.getString("CD_OPE");
        java.sql.Date dFC_ULTACT = rs.getDate("FC_ULTACT");
        String sFC_ULTACT = Format.date2String(dFC_ULTACT);

        // Relleno del registro de salida correspondiente a esta iteracion
        //  por los registros devueltos por la query
        enf = new EnfCent(sCD_ENFCIE, sIT_ENVCNE, sFC_ALTA, sIT_RUTINA,
                          sCD_SEMINIP, sCD_SEMFINP, sFC_BAJA,
                          sCD_ANOBAJA, sCD_SEMBAJA, sCD_OPE, sFC_ULTACT);

        // Adicion del registro obtenido a la lista resultado de salida hacia cliente
        lResult.addElement(enf);
      } // Fin for each registro

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      ex.printStackTrace();
      lResult = null;
      throw ex;
    }

    return lResult;
  } // Fin obtenerEnfermedades()

  // Obtener todos los medicos SIVE_MCENTINELA
  // Inicializa la conexi�n
  public static Lista obtenerMedicos(Connection con) throws Exception {

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    Lista lResult = new Lista();
    MedCent med = null;

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con.setAutoCommit(false);

    try {
      // Preparacion de la sentencia SQL
      st = con.prepareStatement(sTodosMed);

      // Ejecucion de la query, obteniendo un ResultSet
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      while (rs.next()) {
        // Recuperacion de los datos a partir del ResultSet de la
        //  query realizada sobre la BD
        String sCD_PCENTI = rs.getString("CD_PCENTI");
        String sCD_MEDCEN = rs.getString("CD_MEDCEN");
        String sCD_E_NOTIF = rs.getString("CD_E_NOTIF");
        String sCD_SEXO = rs.getString("CD_SEXO");
        String sCD_MASIST = rs.getString("CD_MASIST");
        String sCD_ANOEPIA = rs.getString("CD_ANOEPIA");
        String sCD_SEMEPIA = rs.getString("CD_SEMEPIA");
        String sDS_APE1 = rs.getString("DS_APE1");
        String sDS_APE2 = rs.getString("DS_APE2");
        String sDS_NOMBRE = rs.getString("DS_NOMBRE");
        String sDS_DNI = rs.getString("DS_DNI");
        String sDS_TELEF = rs.getString("DS_TELEF");
        String sDS_FAX = rs.getString("DS_FAX");
        java.sql.Date dFC_NAC = rs.getDate("FC_NAC");
        String sFC_NAC = Format.date2String(dFC_NAC);
        java.sql.Date dFC_ALTA = rs.getDate("FC_ALTA");
        String sFC_ALTA = Format.date2String(dFC_ALTA);
        int iNMREDADI = rs.getInt("NMREDADI");
        String sNMREDADI = "" + iNMREDADI;
        int iNMREDADF = rs.getInt("NMREDADF");
        String sNMREDADF = "" + iNMREDADF;
        String sDS_HORAI = rs.getString("DS_HORAI");
        String sDS_HORAF = rs.getString("DS_HORAF");
        String sIT_BAJA = rs.getString("IT_BAJA");
        java.sql.Date dFC_BAJA = rs.getDate("FC_BAJA");
        String sFC_BAJA = Format.date2String(dFC_BAJA);
        String sCD_ANOBAJA = rs.getString("CD_ANOBAJA");
        String sCD_SEMBAJA = rs.getString("CD_SEMBAJA");
        String sCD_MOTBAJA = rs.getString("CD_MOTBAJA");
        String sCD_OPE = rs.getString("CD_OPE");
        java.sql.Date dFC_ULTACT = rs.getDate("FC_ULTACT");
        String sFC_ULTACT = Format.date2String(dFC_ULTACT);

        // Relleno del registro de salida correspondiente a esta iteracion
        //  por los registros devueltos por la query
        med = new MedCent(sCD_PCENTI, sCD_MEDCEN, sCD_E_NOTIF, sCD_SEXO,
                          sCD_MASIST,
                          sCD_ANOEPIA, sCD_SEMEPIA, sDS_APE1, sDS_APE2,
                          sDS_NOMBRE, sDS_DNI,
                          sDS_TELEF, sDS_FAX, sFC_NAC, sFC_ALTA, sNMREDADI,
                          sNMREDADF,
                          sDS_HORAI, sDS_HORAF, sIT_BAJA, sFC_BAJA, sCD_ANOBAJA,
                          sCD_SEMBAJA,
                          sCD_MOTBAJA, sCD_OPE, sFC_ULTACT);

        // Adicion del registro obtenido a la lista resultado de salida hacia cliente
        lResult.addElement(med);
      } // Fin for each registro

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      ex.printStackTrace();
      lResult = null;
      throw ex;
    }

    return lResult;
  } // Fin obtenerMedicos()

  // Obtencion de las ano/sem epidemiologicas desde
  //  sAnoGen/sSemGen hasta sAno/sSem. Acceso a la BD a la tabla
  // SIVE_SEMANA_EPI_RMC
  public static Lista obtenerSemanas(Connection con,
                                     String sAnoGen, String sSemGen,
                                     String sAno, String sSem) throws Exception {

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    Lista lResult = new Lista();
    Data dtAnoSem = null;

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con.setAutoCommit(false);

    try {
      // Preparacion de la sentencia SQL
      st = con.prepareStatement(sSemanas);

      // Instanciacion de los 4 valores de la query
      st.setString(1, sAnoGen);
      st.setString(2, sAno);
      st.setString(3, sAnoGen);
      st.setString(4, sSemGen);
      st.setString(5, sAno);
      st.setString(6, sSem);

      // Ejecucion de la query, obteniendo un ResultSet
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      while (rs.next()) {
        // Recuperacion de los datos a partir del ResultSet de la
        //  query realizada sobre la BD
        String sCD_ANOEPI = rs.getString("CD_ANOEPI");
        String sCD_SEMEPI = rs.getString("CD_SEMEPI");
        java.sql.Date dFC_FINEPI = rs.getDate("FC_FINEPI");
        String sFC_FINEPI = Format.date2String(dFC_FINEPI);

        // Relleno del registro de salida correspondiente a esta iteracion
        //  por los registros devueltos por la query
        dtAnoSem = new Data();
        dtAnoSem.put("ANO", sCD_ANOEPI);
        dtAnoSem.put("SEM", sCD_SEMEPI);
        dtAnoSem.put("FINEPI", sFC_FINEPI);

        // Adicion del registro obtenido a la lista resultado de salida hacia cliente
        lResult.addElement(dtAnoSem);
      } // Fin for each registro

      // Cierre del ResultSet y del PreparedStatement
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();
    }
    catch (Exception e) {
      con.rollback();
      lResult = null;
      throw e;
    }

    return lResult;
  } // Fin obtenerSemanas()

  // true: dicha enfermedad pertenece a la cobertura en dicha semana
  public static boolean enfEnCobertura(EnfCent enf, String ano, String sem) {
    String sSeminip, sSemfinp;
    int iSem, iSeminip, iSemfinp;

    sSeminip = enf.getCD_SEMINIP();
    sSemfinp = enf.getCD_SEMFINP();

    // Comprobacion del IT_RUTINA
    if (enf.getIT_RUTINA().equals("S")) {
      // Comprobacion de la FC_BAJA: si FC_BAJA es null o mayor
      //  que la semana en la que estamos generando cobertura
      //int resCFB = comprobarFecha(enf.getFC_BAJA(),ano,sem);
      int resCFB = comprobarFecha(enf.getCD_ANOBAJA(),
                                  enf.getCD_SEMBAJA(),
                                  ano, sem);
      if (resCFB == 0 || resCFB == 1) {
        // Comprobacion del periodo
        if (sSeminip.equals("") && sSemfinp.equals("")) { // Todo el a�o
          return true;
        }
        else {
          iSem = Integer.parseInt(sem);
          iSeminip = Integer.parseInt(sSeminip);
          iSemfinp = Integer.parseInt(sSemfinp);

          // Comprobacion del periodo
          if (iSeminip <= iSemfinp) {
            if (iSeminip <= iSem && iSem <= iSemfinp) {
              return true;
            }
          }
          else {
            if ( (iSeminip <= iSem && iSem <= 53) ||
                (1 <= iSem && iSem <= iSemfinp)) {
              return true;
            }
          }
        } // Fin Comprobacion del periodo
      } // Fin comprobacion FC_BAJA
    } // Fin comprobacion IT_RUTINA

    // La enfermedad en esta semana no pertenece a la cobertura
    return false;
  } // Fin enfEnCobertura()

  // true: dicho medico pertenece a la cobertura en dicha semana
  public static boolean medEnCobertura(MedCent med, String ano, String sem) {
    String sSeminip, sSemfinp;
    int iSem, iSeminip, iSemfinp;

    // Comprobacion FC_ALTA: si ano/sem < FC_ALTA entonces outcob
    int resCFA = comprobarFecha(med.getCD_ANOEPIA(),
                                med.getCD_SEMEPIA(),
                                ano, sem);
    if (resCFA == 1) {
      return false;
    }

    // Comprobacion FC_BAJA: si FC_BAJA < ano/sem entonces outcob
    int resCFB = comprobarFecha(med.getCD_ANOBAJA(),
                                med.getCD_SEMBAJA(),
                                ano, sem);
    if (resCFB == 2) {
      return false;
    }

    // El medico en esta semana pertenece a la cobertura
    return true;
  } // Fin medEnCobertura()

  // return 0: ano/semFecha="" OR ano/semFecha=ano/sem
  // return 1: ano/semFecha > ano/sem
  // return 2: ano/semFecha < ano/sem
  public static int comprobarFecha(String anoFecha, String semFecha,
                                   String ano, String sem) {

    // Si anoFecha/semFecha = "" significa que la fecha de la que proceden es null
    if (anoFecha.equals("") || semFecha.equals("")) {
      return 0;
    }
    else {
      int iAnoFecha = Integer.parseInt(anoFecha);
      int iSemFecha = Integer.parseInt(semFecha);
      int iAno = Integer.parseInt(ano);
      int iSem = Integer.parseInt(sem);

      if (iAnoFecha == iAno && iSemFecha == iSem) {
        return 0;
      }
      else {
        if (iAnoFecha > iAno) {
          return 1;
        }
        if (iAnoFecha == iAno) {
          if (iSemFecha > iSem) {
            return 1;
          }
          else {
            return 2;
          }
        }
        else {
          return 2;
        }
      }
    }
  } // Fin comprobarFecha()

  // Incrementa en 1 la semana de anoSem
  public static Data incrementarAnoSem(Connection con, Data anoSem) throws
      Exception {

    // Si no tiene ano/sem se devuelve
    if ( ( (String) anoSem.get("ANO")).equals("") ||
        ( (String) anoSem.get("SEM")).equals("")) {
      return anoSem;
    }

    Data newAnoSem = new Data();
    String newAno = "";
    String newSem = "";
    String sAno = (String) anoSem.get("ANO");
    String sSem = (String) anoSem.get("SEM");
    int iAno = Integer.parseInt(sAno);
    int iSem = Integer.parseInt(sSem);

    if (sSem.equals("52")) {
      // Verificar si existe el 53 o hay que pasar de a�o epidemiologico
      Lista lSem53 = obtenerSemanas(con, sAno, "53", sAno, "53");
      if (lSem53.size() == 0) { // No hay semana epidemiologica 53
        newSem = "01";
        newAno = "" + (iAno + 1);
      }
      else { // Existe semana epidemiologica 53
        newSem = "53";
        newAno = sAno;
      }
    }
    else if (sSem.equals("53")) {
      newSem = "01";
      newAno = "" + (iAno + 1);
    }
    else {
      newSem = "" + (iSem + 1);
      newAno = sAno;
    }

    newAnoSem.put("ANO", newAno);
    if (newSem.length() == 1) {
      newSem = "0" + newSem;
    }
    newAnoSem.put("SEM", newSem);

    return newAnoSem;
  } // Fin incrementarAnoSem()

  // Decrementa en 1 la semana de anoSem
  public static Data decrementarAnoSem(Connection con, Data anoSem) throws
      Exception {

    // Si no tiene ano/sem se devuelve
    if ( ( (String) anoSem.get("ANO")).equals("") ||
        ( (String) anoSem.get("SEM")).equals("")) {
      return anoSem;
    }

    Data newAnoSem = new Data();
    String newAno = "";
    String newSem = "";
    String sAno = (String) anoSem.get("ANO");
    String sSem = (String) anoSem.get("SEM");
    int iAno = Integer.parseInt(sAno);
    int iSem = Integer.parseInt(sSem);

    if (sSem.equals("01")) {
      // Verificar si existe la 53 en el a�o anterior
      Lista lSem53 = obtenerSemanas(con, "" + (iAno - 1), "53", "" + (iAno - 1),
                                    "53");
      if (lSem53.size() == 0) { // No hay semana epidemiologica 53
        newSem = "52";
        newAno = "" + (iAno - 1);
      }
      else { // Existe semana epidemiologica 53
        newSem = "53";
        newAno = "" + (iAno - 1);
      }
    }
    else {
      newSem = "" + (iSem - 1);
      newAno = sAno;
    }

    newAnoSem.put("ANO", newAno);
    if (newSem.length() == 1) {
      newSem = "0" + newSem;
    }
    newAnoSem.put("SEM", newSem);

    return newAnoSem;
  } // Fin decrementarAnoSem()

  // Obtiene el valor de IT_GRIPE del unto al que pertenece el medico par.
  public static String obtenerIT_GRIPE(Connection con, MedCent med) throws
      Exception {
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // Objetos de datos
    String sResult = "";

    // Establece la conexi�n con la base de datos, sin que
    //  el commit se haga automaticamente sobre cada actualizacion
    con.setAutoCommit(false);

    try {
      // Preparacion de la sentencia SQL
      st = con.prepareStatement(sIT_GRIPE);

      // Instanciacion de los valores de la query
      st.setString(1, med.getCD_PCENTI());

      // Ejecucion de la query, obteniendo un ResultSet
      rs = st.executeQuery();

      // Procesamiento de cada registro obtenido de la BD
      if (rs.next()) {
        sResult = rs.getString("IT_GRIPE");

        // Cierre del ResultSet y del PreparedStatement
      }
      rs.close();
      rs = null;
      st.close();
      st = null;

      // Validacion de la transacci�n
      con.commit();

      // Return
      return sResult;
    }
    catch (Exception e) {
      con.rollback();
      sResult = null;
      throw e;
    }
  } // Fin obtenerIT_GRIPE()

  /****** NO HACE FALTA COBERTURA PARA LAS ENFERMEDADES ******/

  /********************************************************************/
  // Inserci�n de las notificaciones que implica la generaci�n
  //  de cobertura al insertar una enfermedad centinela
  /********************************************************************/
  //
  // Parametros:
  // @param con: conexion con la BD
  // @param enf: EnfCent a borrar
  // @param sOpe: indica quien genera la cobertura
  // @param anoSig: indica si la cobertura es para este a�o o para el siguiente
  /*  public static void generarCobertura4(Connection con,
                                         EnfCent enf,
                                         String sOpe,
                                         String anoSig) throws Exception {
      // Si es para el a�o siguiente no se genera cobertura
      if(anoSig.equals("S")) return;
      // Lista en la que se van a�adiendo las notificaciones que
      //  se a�adiran en el proceso de generacion de la cobertura
      Lista lNotifRMC = new Lista();
      NotifRMC nRMC = null;
      // Fecha actual: sera introducida como fecha para todas las
      //   notificaciones generadas en la cobertura
      String fActual = Format.fechaHoraActual();
      // Generaci�n de cobertura
      if(enf.getIT_RUTINA().equals("S")){
        // semIni y semFin
        String semIni = enf.getCD_SEMINIP();
        String semFin = enf.getCD_SEMFINP();
        // Obtenci�n de la ultima semana generada (SIVE_SEMGEN_RMC)
        String sAnoGen = "";
        String sSemGen = "";
        Data dtAnoSemGen = obtenerAnoSemGen(con);
        sAnoGen = (String)dtAnoSemGen.get("ANO");
        sSemGen = (String)dtAnoSemGen.get("SEM");
        // 3 casos:
        //  Periodo completo
        //  semIni es menor o igual que semFin
        //  semIni es mayor que semFin
        if(semIni.equals("") && semFin.equals("")){
          // Insertar las NotifRMC para el a�o de semGen desde la
          //  semana 1 hasta semGen
        } else if(semIni.compareTo(semFin)<=0){
          // 3 casos:
          //  semGen < (semIni<=semFin)
          //  semIni <= semGen <= semFin
          //  semGen > (semIni<=semFin)
          if(sSemGen.compareTo(semIni)<0){
            // No se genera cobertura: ya se generar� cuando avance semGen
       } else if(sSemGen.compareTo(semIni)>=0 && sSemGen.compareTo(semFin)<=0){
            // Insertar las NotifRMC para el a�o de semGen desde semIni
            //  hasta semGen ambas inclusives
          } else if(sSemGen.compareTo(semFin)>0){
            // Insertar las NotifRMC para el a�o de semGen desde semIni
            //  hasta semFin ambas inclusives
          }
        } else {
          // 3 casos:
          //  1 <= semGen < semFin
          //  semFin <= semGen <= semIni
          //  semIni < semGen <=53
          if(sSemGen.compareTo(semFin)<=0){
            // Insertar las NotifRMC para el a�o de semGen desde sem1
            //  hasta semGen ambas inclusives. Harian falta las semanas
            //  del a�o anterior desde semIni hasta la 53 ????
       } else if(semFin.compareTo(sSemGen)<0 && sSemGen.compareTo(semIni)<0){
            // Insertar las NotifRMC para el a�o de semGen desde sem1
            //  hasta semFin ambas inclusives
          } else if(semIni.compareTo(sSemGen)>=0){
            // Insertar las NotifRMC para el a�o de semGen desde sem1
            //  hasta semFin y desde semIni hasta semGen todas inclusives
          }
        }
      } // Fin if(IT_RUTINA)
      // Inserci�n de enfermedad + Inserci�n de NotifRMCs de la cobertura
    } // Fin generarCobertura4()*/

      /*  // Inserta NotifRMC para una enfermedad entre 2 semanas (ambas inclusives)
    public static void generarCoberturaInsEnfCent(Connection con,
                                         EnfCent enf,
                                         String sOpe) throws Exception {
      // Establece la conexi�n con la base de datos, sin que
      //  el commit se haga automaticamente sobre cada actualizacion
      con.setAutoCommit(false);
      try {
        // Actualizacion de la ultima ano/sem generada
        insertarEnfCent(con,sAno,sSem);
        // Adici�n de todas las NotifRMC creadas
        insertarListaNotifRMC(con,lNotifRMC);
        // Si todas las queries han ido bien se hace un commit
        con.commit();
      } catch (Exception ex) {
          // Si alguna query falla se hace rollback
          con.rollback();
          throw ex;
      }
    } // Fin generarCoberturaInsEnfCent()*/

} // enclass Cobertura
