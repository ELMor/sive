package centinelas.servidor.impresos;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.zip.ZipInputStream;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdbcpool.JDCConnectionDriver;
import sapp2.Data;
import sapp2.Lista;

public class SrvImpresos
    extends HttpServlet {

  /**
   * Modo debug
   */
  protected boolean debugMode = true;

  //M�ximo tama�o de transmisi�n
  final public static int maxSIZE = 50;

  //Initialize global variables
  protected String driverName = "";
  protected String dbUrl = "";
  protected String userName = "";
  protected String userPwd = "";

  protected JDCConnectionDriver jdcdriver;
  protected Connection conOpen;

  /**
   * Establece las variables JDBC para testear el servlet en local
   *
   * @param driver driver
   * @param url url
   * @param uid usuario
   * @param pwd password
   */
  public void setJdbcEnvironment(String driver, String url, String uid,
                                 String pwd) {
    driverName = driver;
    dbUrl = url;
    userName = uid;
    userPwd = pwd;
  }

  /**
   * Banco de pruebas en local de la l�gica de negocio del servlet
   *
   * @param opmode modo de operaci�n del servelt
   * @param vParametros vector de par�metros recibidos desde el applet
   * NOTA: No se abre y cierra la conexi�n porque eso se hace en cada Servlet
   */
  public Lista doDebug(int opmode, Lista vParametros) {
    Lista v = new Lista();
    try {
      jdcdriver = new JDCConnectionDriver(driverName, dbUrl, userName, userPwd);

      doTrabajo(opmode, vParametros);
    }
    catch (Exception e) {
      // error en modo debug
      e.printStackTrace();
    }
    return v;
  }

  /**
   * Traza en el log el mensaje leido
   *
   * @param msg Mensaje para trazar
   */
  public void trazaLog(Object msg) {
    if (debugMode) {
      System.out.println(msg.toString());
    }
  }

  public void init(ServletConfig config) throws ServletException {

    // carga las constantes de acceso JDBC
    driverName = config.getInitParameter("drivername");
    dbUrl = config.getInitParameter("dbUrl");
    userName = config.getInitParameter("userName");
    userPwd = config.getInitParameter("userPwd");

    try {
      // carga el driver JDBC
      jdcdriver = new JDCConnectionDriver(driverName, dbUrl, userName, userPwd);
    }
    catch (Exception e) {
      // error al abrir la conexi�n con la base de datos
      e.printStackTrace();
      throw new ServletException("Error al conectar con la base de datos.");
    }
  }

  public void destroy() {
    try {
      jdcdriver.clearPool();
      jdcdriver = null;
    }
    catch (Exception e) {
      ;
    }
    super.destroy();
  }

  // crea una conexi�n
  protected synchronized Connection openConnection() throws Exception {
    Connection con = jdcdriver.connect();
    conOpen = con; // se guarda una referencia para cerrarla si se produce una excepci�n
    return con;
  }

  // cierra una conexion
  protected synchronized void closeConnection(Connection con) {
    try {
      jdcdriver.disconnect(con);
    }
    catch (Exception e) {}
  }

  // funcionalidad del servlet
//  protected abstract Lista doWork(int opmode, Lista param) throws Exception;

  // servicio
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {

    // par�metros
    Lista param = null;
    Lista data = null;

    // streams

    ObjectInputStream in = null; //Entrada : Una lista (objeto)
    DataOutputStream datOut = null; //Salida : Flujo de bytes contiene el zip

    // modo de operaci�n
    int opmode;

    // comunicaciones servlet-applet
    try {

      // lee el modo de operaci�n
      in = new ObjectInputStream(request.getInputStream());
      opmode = in.readInt();

      // lee los par�metros
      param = (Lista) in.readObject();

      //_________________________________________________________

      //Abre conex JDBC
      conOpen = openConnection();
      // obtiene los resultados

      //Hace conexiones a b. datos Deja en el zip todo el contenido necesario
      doTrabajo(opmode, param);

      //Cierra con JDBC
      closeConnection(conOpen);

      //Fija el tipo de aplicacion que debe recibir los datos en cliente
      response.setContentType("application/x-visorwp");

      ZipInputStream zipIn = null;

      //Se crea stream conectado a fichero zip de entrada
      zipIn = new ZipInputStream(
          new FileInputStream("ejemplo_visor.zip"));

      //Se crea stream conectado al HttpServletResponse
      datOut = new DataOutputStream( (OutputStream) response.getOutputStream());
      datOut.flush();

      // A�ade el contenido de entrada al buffer y de ah� hacia  salida
      byte[] buf = new byte[1024];
      int len;
      while ( (len = zipIn.read(buf)) > 0) {
        datOut.write(buf, 0, len);
      }
      datOut.flush();
      datOut.close();

      // envia la excepci�n
    }
    catch (Exception e) {

      e.printStackTrace();
      datOut.flush();
      datOut.close();

      // cierra los recursos
      if (conOpen != null) {
        closeConnection(conOpen);
      }
    }
  } //Fin metodo

  protected void doTrabajo(int opmode, Lista param) throws Exception {

    //Deb hacer: Crear DBFs, obtener plantilla(si necesario,,insertar avisos,
    //y crear zip

    //Para hacer zips se usa la clase Zipeador
    /*
         Vector rutas = new Vector();
         rutas.addElement("C:" + File.separator + "dataCA.txt");
         rutas.addElement("C:" + File.separator + "databot.txt");
         Zipeador.creaZip("varios.zip" ,rutas);
     */

  }

  //Para depurar
  public static void main(String args[]) {
    SrvImpresos srv = new SrvImpresos();
    Lista vParam = new Lista();
    Data filtro = new Data();
    Data trama = new Data();
    Data peticion = new Data();

    // par�metros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sve_desa",
                           "sive_desa");

    // filtro �reas
    filtro.put("NM_ANOPE", "2000");
    filtro.put("NM_PEDIDOE", "58");

    vParam.addElement(filtro);
    srv.doDebug(1, vParam);

    srv = null;
  }

} //FIN DE CLASE
