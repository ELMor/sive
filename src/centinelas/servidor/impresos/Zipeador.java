//Clase con m�todos est�ticos para hacer zips por c�digo
package centinelas.servidor.impresos;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Zipeador {

  public Zipeador() {
  }

  //Crea zip a partir de un �nico fichero
  //rutaZip debe tener extensi�n .zip
  public static void creaZip(String rutaZip, String rutaFichEntrada) throws
      IOException {

    try {
      String outFilename = rutaZip;
      String inFilename = rutaFichEntrada;
      FileInputStream in = null;
      ZipOutputStream out = null;

      //Se crean los streams conectados a los ficheros de entrada y salida
      in = new FileInputStream(inFilename);
      out = new ZipOutputStream(
          new FileOutputStream(outFilename));

      out.putNextEntry(new ZipEntry(inFilename));
      // A�ade el contenido del fichero de entrada al buffer y de ah� hacia el zip de salida
      byte[] buf = new byte[1024];
      int len;
      while ( (len = in.read(buf)) > 0) {
        out.write(buf, 0, len);
      }
      out.closeEntry();
      //Cierra stream de entrada
      in.close();

      //Cierra el Streeam de salida
      out.close();

    }
    catch (IOException e) {
      throw e;
    }

  }

  //Crea zip empaquetando varios ficheros
  //rutaZip es el nombre del zip (y ruta) debe tener extensi�n .zip
  //Vector es un vector de Strings que contienen los nombres( y rutas) de los fich de entrada
  public static void creaZip(String rutaZip, Vector rutasFichEntrada) throws
      IOException {

    try {
      String outFilename = rutaZip;
      String inFilename = ""; //rutaFichEntrada;
      FileInputStream in = null;
      ZipOutputStream out = null;

      //Se crea stream conectado a fichero de salida
      out = new ZipOutputStream(
          new FileOutputStream(outFilename));

      //Se recorre lista d efich de entrada y se lleva cada uno al flujo de salida
      for (int cont = 0; cont < rutasFichEntrada.size(); cont++) {
        inFilename = (String) (rutasFichEntrada.elementAt(cont));
        in = new FileInputStream(inFilename);
        out.putNextEntry(new ZipEntry(inFilename));
        // A�ade el contenido del fichero de entrada al buffer y de ah� hacia el zip de salida
        byte[] buf = new byte[1024];
        int len;
        while ( (len = in.read(buf)) > 0) {
          out.write(buf, 0, len);
        }
        out.closeEntry();
        //Cierra stream de entrada
        in.close();

      } //Fin for

      //Cierra el Streeam de salida
      out.close();

    }
    catch (IOException e) {
      throw e;
    }

  }

}