/**
 * Clase: SrvCentAut
 * Paquete: centinelas.servidor.c_menu
 * Hereda: AppServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 11/02/2000
 * Analisis Funcional: Autorizaciones
 * Descripcion: Controla el acceso de un usuario a una aplicacion
 *  estableciendo los permisos sobre las acciones de la misma.
 * V 1.2 PDP 03/05/2000 El usuario s�lo se valida contra la conexion JDBC
 */

/*
 Solo tiene un modo de operaci�n. Recibe el login y el password del usuario
 y env�a una estructura (de clase Data) con los datos de las autorizaciones
 del usuario.Estas autorizaciones se refieren tanto a las opciones de men�
 permitidas como a los botones habilitados al entrar en cada opci�n.
 */

package centinelas.servidor.c_menu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Vector;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;
import sapp2.QueryTool;

public class SrvCentAut
    extends DBServlet {

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {

    // Buffers
    Lista snapShot = null;
    Lista vResultado = new Lista();
    Data dt = null;
    Data dtAutorizaciones = new Data();
    Vector vDatSubQuery = new Vector();
    int i;

    /** PDP 03/05/2000
     * Usuario para la conexi�n JDBC
     */
    //Variables para primera conexi�n que valida el password
    //Nota: No confundir con la conexi�n habitual a la base de datos
    //que usaria el mismo userName y userPwd para todos los usuarios
    String userNameEntrada = "";
    String userPwdEntrada = "";
    Connection conEntrada = null;

    //Valida la conexi�n con JDBC  PDP 03/05/2000
    // Par�metros
    Data dtLogin2 = (Data) vParametros.elementAt(0);
    userNameEntrada = dtLogin2.getString("COD_USUARIO");
    userPwdEntrada = dtLogin2.getString("PASSWORD");
    // Control de error
    boolean bError1 = false;
    String sMsg1 = null;

    // abre la conexi�n de este usuario
    try {
      Class.forName(driverName);
      conEntrada = DriverManager.getConnection(dbUrl, userNameEntrada,
                                               userPwdEntrada);
      conEntrada.close();
    }
    catch (Exception e) {

      trazaLog(e);
      bError1 = true;
      sMsg1 = "Usuario o password incorrecto";
    }
    // Devuelve el error recogido
    if (bError1) {
      throw new Exception(sMsg1);
    }

    // Control de error
    boolean bError = false;
    String sMsg = null;

    // Query tool
    QueryTool qt = null;

    try {
      // Par�metros
      Data dtLogin = (Data) vParametros.elementAt(0);

      con.setAutoCommit(true);

      // Cocmprueba que la aplicaci�n est� dada de alta
      //select ESTADO from APLICACION where COD_APLICACION = RMC
      qt = new QueryTool();
      qt.putName("APLICACION");
      qt.putType("ESTADO", QueryTool.STRING);

      // filtro de la aplicaci�n
      qt.putWhereType("COD_APLICACION", QueryTool.STRING);
      qt.putWhereValue("COD_APLICACION", dtLogin.getString("COD_APLICACION"));
      qt.putOperator("COD_APLICACION", "=");

      snapShot = (Lista) qt.doSelect(con);

      // Aplicaci�n inexistente
      if (snapShot.size() == 0) {
        sMsg = "La aplicaci�n [" + dtLogin.getString("COD_APLICACION") +
            "] no existe";
        bError = true;

        // Aplicaci�n existe
      }
      else {
        // Chequea el estado de la aplicaci�n
        dt = (Data) snapShot.elementAt(0);

        // Aplicaci�n abierta
        if (dt.getString("ESTADO").equals("")) {

          // Comprueba la acreditaci�n del usuario

          // select COD_USUARIO,COD_GRUPO from V_APLIC_USU_GRU
          // where COD_APLICACION=? and COD_USUARIO=?
          // and FC_BAJA is null

          qt = new QueryTool();
          qt.putName("V_APLIC_USU_GRU");
          qt.putType("COD_USUARIO", QueryTool.STRING);
          qt.putType("COD_GRUPO", QueryTool.INTEGER);

          // Filtro con las acreditaciones
          qt.putWhereType("COD_APLICACION", QueryTool.STRING);
          qt.putWhereValue("COD_APLICACION", dtLogin.getString("COD_APLICACION"));
          qt.putOperator("COD_APLICACION", "=");

          qt.putWhereType("COD_USUARIO", QueryTool.STRING);
          qt.putWhereValue("COD_USUARIO", dtLogin.getString("COD_USUARIO"));
          qt.putOperator("COD_USUARIO", "=");
          /*
                    qt.putWhereType("PASSWORD", QueryTool.STRING);
               qt.putWhereValue("PASSWORD", dtLogin.getString("PASSWORD"));
                    qt.putOperator("PASSWORD", "=");
           */
          qt.putWhereType("FC_BAJA", QueryTool.VOID);
          qt.putWhereValue("FC_BAJA", "");
          qt.putOperator("FC_BAJA", "is null");

          snapShot = (Lista) qt.doSelect(con);

          // Usuario-Grupo para la Aplicacion no encontrado
          if (snapShot.size() == 0) {
            sMsg = "Acceso denegado";
            bError = true;

            // Usuario-Grupo para la Aplicacion encontrado
          }
          else {

            // Graba el grupo
            dt = (Data) snapShot.elementAt(0);
            dtLogin.put("COD_GRUPO", dt.getString("COD_GRUPO"));

            // Lee las autorizaciones del usuario
            //select COD_ACCION,SW_ESTADO from ACCION where COD_APLICACION=RMC
            // and COD_ACCION in (select COD_ACCION from GRUPAUTO where COD_APLICACION=? and COD_GRUPO=?)
            qt = new QueryTool();
            qt.putName("ACCION");
            qt.putType("COD_ACCION", QueryTool.INTEGER);
            qt.putType("SW_ESTADO", QueryTool.STRING);
            qt.putWhereType("COD_APLICACION", QueryTool.STRING);
            qt.putWhereValue("COD_APLICACION",
                             dtLogin.getString("COD_APLICACION"));
            qt.putOperator("COD_APLICACION", "=");

            // Realiza el cruce con GRUPAUTO
            qt.putSubquery("COD_ACCION",
                "select COD_ACCION from GRUPAUTO where COD_APLICACION=? and COD_GRUPO=?");
            dt = new Data();
            dt.put(new Integer(QueryTool.STRING),
                   dtLogin.getString("COD_APLICACION"));
            vDatSubQuery.addElement(dt);
            dt = new Data();
            dt.put(new Integer(QueryTool.INTEGER),
                   dtLogin.getString("COD_GRUPO"));
            vDatSubQuery.addElement(dt);
            qt.putVectorSubquery("COD_ACCION", vDatSubQuery);

            snapShot = qt.doSelect(con);

            // Transforma el snapShot en una estructura Data
            for (i = 0; i < snapShot.size(); i++) {
              dt = (Data) snapShot.elementAt(i);

              dtAutorizaciones.put(dt.getString("COD_ACCION"),
                                   dt.getString("SW_ESTADO"));

            }

            // Agrega el par�metro FC_ACTUAL
            dtAutorizaciones.put("FC_ACTUAL", Format.fechaActual());

            // agrega la estructura Data con las autorizaciones
            vResultado.addElement(dtAutorizaciones);

          } // if(Usuario-Grupo para la Aplicacion encontrado)

          // Aplicaci�n cerrada por aver�a
        }
        else if (dt.getString("ESTADO").equals("T")) {
          sMsg = "La aplicaci�n [" + dtLogin.getString("COD_APLICACION") +
              "] est� cerrada por aver�a";
          bError = true;
          // Aplicaci�n cerrada bajo petici�n de usuario
        }
        else if (dt.getString("ESTADO").equals("V")) {
          sMsg = "La aplicaci�n [" + dtLogin.getString("COD_APLICACION") +
              "] est� cerrada por petici�n de usuario";
          bError = true;
          // Aplicaci�n cerrada por backup
        }
        else if (dt.getString("ESTADO").equals("B")) {
          sMsg = "La aplicaci�n [" + dtLogin.getString("COD_APLICACION") +
              "] est� cerrada por BACKUP";
          bError = true;
          // Aplicaci�n cerrada
        }
        else {
          sMsg = "La aplicaci�n [" + dtLogin.getString("COD_APLICACION") +
              "] est� cerrada";
          bError = true;
        }
      } // if(Aplicacion existe)

      vResultado.setParameter("FC_ACTUAL", Format.fechaActual());

    }
    catch (SQLException e1) {
      // Traza el error
      trazaLog(e1);
      bError = true;
      sMsg = "Error al comprobar las acreditaciones";
    }
    catch (Exception e2) {
      trazaLog(e2);
      bError = true;
      sMsg = "Error inesperado";
    } // Fin try .. catch

    // Devuelve el error recogido
    if (bError) {
      throw new Exception(sMsg);
    }

    return vResultado;
  } // Fin doWork()

} // endclass SrvCentAut
