/**
 * Clase: SrvCentApp
 * Paquete: centinelas.servidor.c_menu
 * Hereda: AppServlet
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 11/02/2000
 * Analisis Funcional: Gesti�n de men�s
 * Descripcion: Servidor de applets para el men� de centinelas
    Recibe como par�metro el n�mero de applet que quiere cargar
    Env�a al cliente la p�gina html que carga dicho applet
 */

package centinelas.servidor.c_menu;

import sapp2.AppServlet;
import sapp2.Format;

public class SrvCentApp
    extends AppServlet {

  // Constantes para los cat�logos generales
  public static final int catTPREGUNTA = 40;
  public static final int catTSIVE = 50;
  public static final int catPROCESOS = 90;
  public static final int catMOTBAJA = 70;
  public static final int catTLINEA = 30;
  public static final int catSEXO = 100;

  // Para m�dicos centinelas
  public static final int catCONGLOM = 300;
  public static final int catMASISTENCIA = 310;
  public static final int catTPCENTINELA = 320;
  public static final int catINDACTMC = 330;

  public void doConfig() {

    // Tama�o por defecto reservado a applet
    this.sWidth = "680";
    this.sHeight = "550";

    String sWidthMant = "650";
    String sHeightMant = "450";

    // Agregamos un login determinado: luego sobrara con la identificacion
    this.dtParametros.put("login", dtParametros.getString("COD_USUARIO"));

    //Par�metros para mantener compatibilidad con parte de EDO
    this.dtParametros.put("TSIVE", "C"); // Aplicaci�n Centinelas
    this.dtParametros.put("IDIOMA", "0"); // Castellano
    this.dtParametros.put("CA", "13"); // Comunidad de Madrid
    this.dtParametros.put("PERFIL", "2"); //Epid CA
    this.dtParametros.put("nivel1", "Area de salud");
    this.dtParametros.put("nivel2", "Distrito");

    // Autorizaciones de usuario (se dan todas por compatibilidad)
    this.dtParametros.put("IT_FG_MNTO", "S");
    this.dtParametros.put("IT_FG_MNTO_USU", "S");
    this.dtParametros.put("IT_FG_TCNE", "S");
    this.dtParametros.put("IT_FG_PROTOS", "S");
    this.dtParametros.put("IT_FG_ALARMAS", "S");
    this.dtParametros.put("IT_AUTALTA", "S");
    this.dtParametros.put("IT_AUTBAJA", "S");
    this.dtParametros.put("IT_AUTMOD", "S");
    // Agrega el par�metro FC_ACTUAL
    this.dtParametros.put("FC_ACTUAL", Format.fechaActual());

    // Applets de la aplicaci�n
    switch (Integer.parseInt(dtParametros.getString("NM_APPLET"))) {

      // Notificaciones Centinelas
      case 1:
        this.sArchive =
            "cent_util.zip, notif.zip, centinelas.datos.centbasicos.zip";
        this.sClass = "centinelas.cliente.c_not.AppNot.class";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_notif.html");
        this.sWidth = "680";
        this.sHeight = "450";
        break;

        //___________________________________________
        //Listado etiquetas
      case 91:
        this.sClass = "centinelas.cliente.c_impetiquetas.AppImpEtiquetas.class";
        this.sArchive = "listados.zip, cent_util.zip";
        this.sWidth = "550";
        this.sHeight = "250";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_expor.html");
        break;

        //Listado cartas
      case 92:
        this.sClass = "centinelas.cliente.c_impcartas.AppImpCartas.class";
        this.sArchive = " centinelas.cliente.c_impcartas.zip, centinelas.cliente.c_impetiquetas.zip, cent_util.zip, " +
            "listados.zip";
        this.sWidth = "500";
        this.sHeight = "360";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_expor.html");
        break;

        //NOTA : FALTA UN SUBMENU AQUI

        //Reimp cartas enviadas
      case 94:
        this.sClass = "centinelas.cliente.c_impcartasenv.AppImpCartasEnv.class";
        this.sArchive = " centinelas.cliente.c_impcartasenv.zip, centinelas.cliente.c_impetiquetas.zip, " +
            "cent_util.zip, listados.zip";
        this.sWidth = "550";
        this.sHeight = "350";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_expor.html");
        break;

        //Cartas de aviso seg�n modelo
      case 95:
        this.sClass =
            "centinelas.cliente.c_mantcartasavisos.AppMantCartasAvisos.class";
        this.sArchive =
            " centinelas.cliente.c_mantcartasavisos.zip, cent_util.zip, " +
            "listados.zip";
        this.sWidth = "630";
        this.sHeight = "480";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_expor.html");
        break;

        // Exportacion Casos Numericos
      case 21:
        this.sClass =
            "centinelas.cliente.exportaciones.AppExpEnfermDeclar.class";
        this.sArchive = " centinelas.cliente.exportaciones.zip, cent_util.zip";
        this.sWidth = "530";
        this.sHeight = "220";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_expor.html");
        break;

        // Exportacion Casos Inviduales
      case 22:
        this.sClass = "volCasosIndCent.volCasosIndCent.class";
        this.sArchive = " cent_util.zip,  enfcentinela.zip, fechas.zip, " +
            "utilidades.zip, volCasosIndCent.zip";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_expor.html");
        this.sWidth = "620";
        this.sHeight = "350";

        break;

        // Exportacion Poblacion Puntos Centinelas
      case 23:
        this.sClass =
            "centinelas.cliente.exportaciones.AppExpPoblacPuntos.class";
        this.sArchive = "centinelas.cliente.exportaciones.zip, cent_util.zip";
        this.sWidth = "480";
        this.sHeight = "150";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_expor.html");
        break;

        // Exportacion Indices Actividad
      case 24:
        this.sClass =
            "centinelas.cliente.exportaciones.AppExpIndicesActividad.class";
        this.sArchive = " centinelas.cliente.exportaciones.zip, cent_util.zip";
        this.sWidth = "480";
        this.sHeight = "150";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_expor.html");
        break;

        // Exportacion Indicadores de Alarma
      case 25:
        this.sClass = "centinelas.cliente.exportaciones.AppExpIndAlarma.class";
        this.sArchive = " cent_util.zip, centinelas.cliente.exportaciones.zip ";
        this.sWidth = "530";
        this.sHeight = "250";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_expor.html");
        break;

        // Envios al CNE
      case 31:
        this.sClass =
            "centinelas.cliente.enviocasosrmccne.AppEnvioCasosRmcCne.class";
        this.sWidth = "515";
        this.sHeight = "275";
        this.sArchive =
            "centinelas.cliente.enviocasosrmccne.zip, cent_util.zip, " +
            "componentes.zip";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_envio.html");
        break;

        // Generacion de Alarmas Automaticas
      case 411:
        this.sClass = "centinelas.cliente.genalauto.AppGenAlaAuto.class";
        this.sWidth = "500";
        this.sHeight = "200";
        this.sArchive = "cent_util.zip, centinelas.cliente.genalauto.zip, " +
            "centinelas.datos.genalauto.zip ";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_param.html");
        break;

        // Mantenimiento de Indicadores
      case 412:
        this.sArchive = "alarmas.zip, cent_util.zip, enfcentinela.zip";
        this.sClass = "alarmas.ApAlarmas.class";
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_param.html");
        break;

        // Mantenimiento de Modelos
      case 421:
        this.sArchive = "enfcentinela.zip, confprot.zip, cent_util.zip, cargamodelo.zip,preguntas.zip,listas.zip";
        this.sClass = "confprot.ApConfProt.class";
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_param.html");
        break;

        // Mantenimiento de Listas
      case 422:
        this.sArchive = "listas.zip, cent_util.zip";
        this.sClass = "listas.mantlis.class";
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_param.html");
        break;

        // Mantenimiento de Preguntas
      case 423:
        this.sArchive = "preguntas.zip, cent_util.zip, listas.zip";
        this.sClass = "preguntas.MantDePreguntas.class";
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_param.html");
        break;

        // Mantenimiento de Preguntas fijas (gripe)
      case 424:
        this.sArchive =
            "centinelas.cliente.c_mantpregenferm.zip, cent_util.zip ";
        this.sClass =
            "centinelas.cliente.c_mantpregenferm.AppMantPregEnferm.class";
        this.sWidth = sWidthMant;
        this.sHeight = "400";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_param.html");
        break;

        // Mantenimiento Puntos Centinelas
      case 51:
        this.sArchive = "centinelas.cliente.c_mantPC.zip, cent_util.zip";
        this.sClass = "centinelas.cliente.c_mantPC.AppMantpc.class";
        this.sWidth = "680";
        this.sHeight = "480";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        break;
        // Mantenimiento Medicos Centinelas
      case 52:
        this.sArchive =
            "cent_util.zip, centinelas.cliente.c_mantmednotif.zip, " +
            "componentes.zip ";
        this.sWidth = "680";
        this.sHeight = "480";
        this.sClass = "centinelas.cliente.c_mantmednotif.AppMantmednotif.class";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        break;

        // Mantenimiento Poblaciones de los Medicos Centinelas
      case 53:
        this.sArchive =
            "cent_util.zip, centinelas.cliente.c_mantpoblacmed.zip ";
        this.sClass =
            "centinelas.cliente.c_mantpoblacmed.AppMantPoblacMed.class";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = "680";
        this.sHeight = "480";
        break;

        // Mantenimiento Indices de Actividad
      case 54:
        this.sArchive = "centinelas.cliente.c_mantindact.zip, cent_util.zip";
        this.sClass = "centinelas.cliente.c_mantindact.AppMantIndAct.class";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = "680";
        this.sHeight = "480";
        break;

        // Mantenimiento Enfermedades Centinelas
      case 55:
        this.sArchive = "centinelas.cliente.c_mantenfcent.zip, cent_util.zip";
        this.sClass = "centinelas.cliente.c_mantenfcent.AppMantenfcent.class";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = "680";
        this.sHeight = "385";
        break;

        // Generar A�o Epidemiol�gico RMC
      case 56:
        this.sArchive = "genanocent.zip, cent_util.zip";
        this.sClass = "genanocent.GenAnoCent.class";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = "400";
        this.sHeight = "200";

        break;
        // Mantenimiento Grupos de Edad
      case 57:
        this.sArchive = "cent_util.zip, centinelas.cliente.c_mantgrupedad.zip";
        this.sClass = "centinelas.cliente.c_mantgrupedad.AppMantGrupEdad.class";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = "680";
        this.sHeight = "385";
        break;

        // Mantenimiento Modelos de Cartas
      case 58:
        this.sArchive = "cent_util.zip, centinelas.cliente.c_mantcartas.zip";
        this.sClass = "centinelas.cliente.c_mantcartas.AppCartas.class";
        this.dtParametros.put("AYUDA", "zip/ayuda/ayuda.html");
        this.sWidth = "680";
        this.sHeight = "400";
        break;
        // Mantenimiento Tipo de Pregunta
      case 591:
        this.sArchive = "cent_util.zip";
        this.sClass = "catalogo.Catalogo.class";
        dtParametros.put("CATALOGO", (new Integer(catTPREGUNTA)).toString());
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        break;

        // Mantenimiento Tipo SIVE
      case 592:
        this.sArchive = "cent_util.zip";
        this.sClass = "catalogo.Catalogo.class";
        dtParametros.put("CATALOGO", (new Integer(catTSIVE)).toString());
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        break;

        // Mantenimiento Enfermedad CIE
      case 593:
        this.sArchive = "enfcie.zip, cent_util.zip";
        this.sClass = "enfcie.EnfCie.class";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        break;

        // Mantenimiento Procesos
      case 594:
        this.sArchive = "cent_util.zip";
        this.sClass = "catalogo.Catalogo.class";
        dtParametros.put("CATALOGO", (new Integer(catPROCESOS)).toString());
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        break;

        // Mantenimiento Sexo
      case 595:
        this.sArchive = "cent_util.zip";
        this.sClass = "catalogo.Catalogo.class";
        dtParametros.put("CATALOGO", (new Integer(catSEXO)).toString());
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        break;

        // Mantenimiento Tipo de Linea
      case 596:
        this.sArchive = "cent_util.zip";
        this.sClass = "catalogo.Catalogo.class";
        dtParametros.put("CATALOGO", (new Integer(catTLINEA)).toString());
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        break;

        // Mantenimiento Motivos de Baja
      case 597:
        this.sArchive = "cent_util.zip";
        this.sClass = "catalogo.Catalogo.class";
        dtParametros.put("CATALOGO", (new Integer(catMOTBAJA)).toString());
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        break;

        // Mantenimiento Conglomerado
      case 5981:
        this.sArchive = "cent_util.zip";
        this.sClass = "catalogo.Catalogo.class";
        dtParametros.put("CATALOGO", (new Integer(catCONGLOM)).toString());
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        break;

        // Mantenimiento Modelos de Asistencia
      case 5982:
        this.sArchive = "cent_util.zip";
        this.sClass = "catalogo.Catalogo.class";
        dtParametros.put("CATALOGO", (new Integer(catMASISTENCIA)).toString());
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        break;

        // Mantenimiento Tipo Punto Centinela
      case 5983:
        this.sArchive = "cent_util.zip";
        this.sClass = "catalogo.Catalogo.class";
        dtParametros.put("CATALOGO", (new Integer(catTPCENTINELA)).toString());
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        break;

        // Mantenimiento Indices de Actividad Asistencial
      case 5984:
        this.sArchive = "cent_util.zip";
        this.sClass = "catalogo.Catalogo.class";
        dtParametros.put("CATALOGO", (new Integer(catINDACTMC)).toString());
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sWidth = sWidthMant;
        this.sHeight = "350";
        break;

        // Grupos de trabajo
      case 60:
        this.sArchive = "usu2.zip, cent_util.zip";
        this.sClass = "usu2.AppUSU.class";
        this.dtParametros.put("AYUDA", "ayuda/centinelas/ayuda_admin.html");
        this.sHeight = "450";
        break;

        // Seleccionar A�o: tratado en cliente

        // Sistema de Informaci�n RMC
      case 71:
        this.sClass = "";
        this.dtParametros.put("AYUDA", "zip/ayuda/ayuda.html");
        break;

        // Acerca de ...
      case 72:
        this.sClass = "";
        this.dtParametros.put("AYUDA", "zip/ayuda/ayuda.html");
        break;

    }
  }
} // endclass SrvCentApp