//Panel inicial que permite buscar puntos centinelas, a�adir nuevos,
// modificar o borrar uno seleccionado

package centinelas.cliente.c_mantPC;

import java.util.Vector;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CPanel;
import centinelas.cliente.c_componentes.CPnlCodigoExt;
import centinelas.cliente.c_componentes.ContCPnlCodigoExt;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.c_mantPC.PuntoCentExt;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

/**
 * Panel a trav�s del que se realiza el mantenimiento de los puntos centinelas
 *
 * @autor MTR.
 * @version 1.0
 */

public class PanMantpc
    extends CPanel
    implements CInicializar, CFiltro, ContCPnlCodigoExt {
  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  XYLayout xyLayout = new XYLayout();
  Label lblTipo = new Label();
  Label lblPuntoCent = new Label();
  CPnlCodigoExt pnlTipo = null;
  CPnlCodigoExt pnlPuntoCent = null;
  ButtonControl btnBuscar = new ButtonControl();
  CListaMantenimiento clmMantpc = null;

  Checkbox chActivos = new Checkbox();
  Checkbox chDesactivos = new Checkbox();
  Checkbox chTodos = new Checkbox();
  CheckboxGroup chkGrp1 = new CheckboxGroup();

  public PanMantpc(CApp a) {
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    QueryTool qtTipo = new QueryTool();
    QueryTool qtPuntoCent = new QueryTool();

    try {
      setApp(a);
      // configura el componente de tipos
      // querytool para los tipos
      qtTipo.putName("SIVE_TPCENTINELA");
      qtTipo.putType("CD_TPCENTI", QueryTool.STRING);
      qtTipo.putType("DS_TPCENTI", QueryTool.STRING);

      //configura el componente de puntos centinelas
      qtPuntoCent.putName("SIVE_PCENTINELA");
      qtPuntoCent.putType("CD_PCENTI", QueryTool.STRING);
      qtPuntoCent.putType("DS_PCENTI", QueryTool.STRING);

      //panel de punto centinela
      pnlPuntoCent = new CPnlCodigoExt(a,
                                       this,
                                       qtPuntoCent,
                                       "CD_PCENTI",
                                       "DS_PCENTI",
                                       false,
                                       "Puntos centinelas",
                                       "Puntos centinelas",
                                       this);

      // panel de tipo de punto centinela
      pnlTipo = new CPnlCodigoExt(a,
                                  this,
                                  qtTipo,
                                  "CD_TPCENTI",
                                  "DS_TPCENTI",
                                  false,
                                  "Especialidades de puntos centinelas",
                                  "Especialidades de puntos centinelas",
                                  this);

      // configura la lista de inspecciones
      // botones
      vBotones.addElement(new CBoton(constantes.keyBtnAnadir,
                                     "images/alta2.gif",
                                     "Nuevo punto centinela",
                                     false,
                                     false));

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion2.gif",
                                     "Modificar punto centinela",
                                     true,
                                     true));

      vBotones.addElement(new CBoton(constantes.keyBtnBorrar,
                                     "images/baja2.gif",
                                     "Borrar punto centinela",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("C�d. pto cent",
                                      "90",
                                      "CD_PCENTI"));

      vLabels.addElement(new CColumna("Descripci�n pto cent",
                                      "220",
                                      "DS_PCENTI"));

      vLabels.addElement(new CColumna("Descripci�n especialidad pto cent",
                                      "220",
                                      "DS_TPCENTI"));

      vLabels.addElement(new CColumna("Baja",
                                      "70",
                                      "IT_BAJA"));

      clmMantpc = new CListaMantenimiento(a,
                                          vLabels,
                                          vBotones,
                                          this,
                                          this,
                                          250,
                                          640);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgBUSCAR = "images/refrescar.gif";

    // carga la imagen
    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().CargaImagenes();

    xyLayout.setHeight(420);
    xyLayout.setWidth(680);
    this.setLayout(xyLayout);

    lblTipo.setText("Especialidad:");
    lblPuntoCent.setText("Pto Centinela:");
    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));
    btnBuscar.setLabel("Buscar");
    btnBuscar.addActionListener(new PanMantpc_btnBuscar_actionAdapter(this));

    // a�ade los componentes
    this.add(lblPuntoCent, new XYConstraints(MARGENIZQ, MARGENSUP, 80, ALTO));
    this.add(pnlPuntoCent,
             new XYConstraints(MARGENIZQ + 80 + INTERHOR - DESFPANEL, MARGENSUP,
                               TAMPANEL, 34));
    this.add(lblTipo,
             new XYConstraints(MARGENIZQ, MARGENSUP + 34 + INTERVERT, -1, ALTO));
    this.add(pnlTipo,
             new XYConstraints(MARGENIZQ + 80 + INTERHOR - DESFPANEL,
                               MARGENSUP + 34 + INTERVERT, TAMPANEL, 34));

    this.add(chActivos,
             new XYConstraints(MARGENIZQ + 80 + 6 * INTERHOR - DESFPANEL +
                               TAMPANEL, MARGENSUP, -1, ALTO));
    this.add(chDesactivos,
             new XYConstraints(MARGENIZQ + 80 + 6 * INTERHOR - DESFPANEL +
                               TAMPANEL, MARGENSUP + ALTO + 3, -1, ALTO));
    this.add(chTodos,
             new XYConstraints(MARGENIZQ + 80 + 6 * INTERHOR - DESFPANEL +
                               TAMPANEL, MARGENSUP + 2 * ALTO + 2 * 3, -1, ALTO));

    this.add(btnBuscar,
             new XYConstraints(540, MARGENSUP + 2 * 3 + INTERVERT + 3 * ALTO,
                               80, 24));
    this.add(clmMantpc,
             new XYConstraints(MARGENIZQ - DESFPANEL,
                               MARGENSUP + 2 * 3 + 3 * INTERVERT + 4 * ALTO,
                               650, 230));

    chActivos.setLabel("Activos");
    chActivos.setCheckboxGroup(chkGrp1);
    chActivos.addItemListener(new PanMantpc_chActivos_itemAdapter(this));
    chDesactivos.setLabel("Desactivos");
    chDesactivos.setCheckboxGroup(chkGrp1);
    chDesactivos.addItemListener(new PanMantpc_chDesactivos_itemAdapter(this));
    chTodos.setLabel("Todos");
    chTodos.setCheckboxGroup(chkGrp1);
    chTodos.addItemListener(new PanMantpc_chTodos_itemAdapter(this));
    chTodos.setState(true);

  }

  public void trasEventoEnCPnlCodigoExt() {}

  public String getDescCompleta(Data dat) {
    return ("");
  }

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera: se hace un backup de los datos
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        pnlTipo.backupDatos();
        pnlPuntoCent.backupDatos();
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        pnlTipo.setEnabled(true);
        pnlPuntoCent.setEnabled(true);
        clmMantpc.setEnabled(true);
        chActivos.setEnabled(true);
        chDesactivos.setEnabled(true);
        chTodos.setEnabled(true);
        lblTipo.setEnabled(true);
        lblPuntoCent.setEnabled(true);
        btnBuscar.setEnabled(true);
    }
  }

  private QueryTool2 obtenerActivos(QueryTool2 qt) {
    qt.putWhereType("IT_BAJA", QueryTool.STRING);
    qt.putWhereValue("IT_BAJA", "N");
    qt.putOperator("IT_BAJA", "=");
    return qt;
  }

  private QueryTool2 obtenerDesactivos(QueryTool2 qt) {
    qt.putWhereType("IT_BAJA", QueryTool.STRING);
    qt.putWhereValue("IT_BAJA", "S");
    qt.putOperator("IT_BAJA", "=");
    return qt;
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    //1� CASO: no se realiza ning�n filtro: QueryTool2 sin where
    if ( (pnlTipo.getDatos() == null) && (pnlPuntoCent.getDatos() == null)) {
      try {
        QueryTool2 qt = new QueryTool2();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_PCENTINELA");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_PCENTI", QueryTool.STRING);
        qt.putType("DS_PCENTI", QueryTool.STRING);
        qt.putType("CD_CONGLO", QueryTool.STRING);
        qt.putType("CD_TPCENTI", QueryTool.STRING);
        qt.putType("IT_GRIPE", QueryTool.STRING);
        qt.putType("FC_ALTA", QueryTool.DATE);
        qt.putType("IT_BAJA", QueryTool.STRING);
        qt.putType("CD_OPE", QueryTool.STRING);
        qt.putType("FC_ULTACT", QueryTool.DATE);

        if (chActivos.getState()) {
          qt = obtenerActivos(qt);
        }
        if (chDesactivos.getState()) {
          qt = obtenerDesactivos(qt);
        }
        //where de it_baja
        // Datos adicionales:descripci�n de tipo pto centinela
        //Para cada Data obtenido antes hace una b�squeda:
        //select DS_TPCENTI from SIVE_TPCENTINELA where CD_TPCENTI= ?
        //En la que el CD_TPCENTI ya est� en el Data obtenido antes
        QueryTool qtAdic = new QueryTool();
        qtAdic.putName("SIVE_TPCENTINELA");
        qtAdic.putType("DS_TPCENTI", QueryTool.STRING);
        Data dtAdic = new Data();
        dtAdic.put("CD_TPCENTI", QueryTool.STRING);
        qt.addQueryTool(qtAdic);
        qt.addColumnsQueryTool(dtAdic);

        // Datos adicionales:descripci�n de conglomerado
        QueryTool qtAdic1 = new QueryTool();
        qtAdic1.putName("SIVE_CONGLOM");
        qtAdic1.putType("DS_CONGLO", QueryTool.STRING);
        Data dtAdic1 = new Data();
        dtAdic1.put("CD_CONGLO", QueryTool.STRING);
        qt.addQueryTool(qtAdic1);
        qt.addColumnsQueryTool(dtAdic1);

        qt.addOrderField("CD_PCENTI");

        p.addElement(qt);
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          this.getApp().showAdvise("No se poseen elementos");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //Fin del primer caso

    //2� CASO: se realiza el filtro de Tipo pero no el de PuntoCent
    if ( (pnlTipo.getDatos() != null) && (pnlPuntoCent.getDatos() == null)) {
      try {
        QueryTool2 qt = new QueryTool2();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_PCENTINELA");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_PCENTI", QueryTool.STRING);
        qt.putType("DS_PCENTI", QueryTool.STRING);
        qt.putType("CD_CONGLO", QueryTool.STRING);
        qt.putType("CD_TPCENTI", QueryTool.STRING);
        qt.putType("IT_GRIPE", QueryTool.STRING);
        qt.putType("FC_ALTA", QueryTool.DATE);
        qt.putType("IT_BAJA", QueryTool.STRING);
        qt.putType("CD_OPE", QueryTool.STRING);
        qt.putType("FC_ULTACT", QueryTool.DATE);

        if (chActivos.getState()) {
          qt = obtenerActivos(qt);
        }
        if (chDesactivos.getState()) {
          qt = obtenerDesactivos(qt);
        }

        qt.putWhereType("CD_TPCENTI", QueryTool.STRING);
        qt.putWhereValue("CD_TPCENTI",
                         ( (Data) pnlTipo.getDatos()).get("CD_TPCENTI"));
        qt.putOperator("CD_TPCENTI", "=");

        // Datos adicionales: descripci�n de tipo de pto centinela
        QueryTool qtAdic = new QueryTool();
        qtAdic.putName("SIVE_TPCENTINELA");
        qtAdic.putType("DS_TPCENTI", QueryTool.STRING);
        Data dtAdic = new Data();
        dtAdic.put("CD_TPCENTI", QueryTool.STRING);
        qt.addQueryTool(qtAdic);
        qt.addColumnsQueryTool(dtAdic);

        // Datos adicionales:descripci�n de conglomerado
        QueryTool qtAdic1 = new QueryTool();
        qtAdic1.putName("SIVE_CONGLOM");
        qtAdic1.putType("DS_CONGLO", QueryTool.STRING);
        Data dtAdic1 = new Data();
        dtAdic1.put("CD_CONGLO", QueryTool.STRING);
        qt.addQueryTool(qtAdic1);
        qt.addColumnsQueryTool(dtAdic1);

        qt.addOrderField("CD_PCENTI");

        p.addElement(qt);
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          pnlTipo.limpiarDatos();
          chActivos.setState(false);
          chDesactivos.setState(false);
          chTodos.setState(true);
          this.getApp().showAdvise(
              "No se poseen elementos con los criterios especificados");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //Fin del segundo caso

    //3� CASO:se realiza el filtro de PuntoCentinela, pero no el de Tipo
    if ( (pnlTipo.getDatos() == null) && (pnlPuntoCent.getDatos() != null)) {
      try {
        QueryTool2 qt = new QueryTool2();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_PCENTINELA");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_PCENTI", QueryTool.STRING);
        qt.putType("DS_PCENTI", QueryTool.STRING);
        qt.putType("CD_CONGLO", QueryTool.STRING);
        qt.putType("CD_TPCENTI", QueryTool.STRING);
        qt.putType("IT_GRIPE", QueryTool.STRING);
        qt.putType("FC_ALTA", QueryTool.DATE);
        qt.putType("IT_BAJA", QueryTool.STRING);
        qt.putType("CD_OPE", QueryTool.STRING);
        qt.putType("FC_ULTACT", QueryTool.DATE);

        if (chActivos.getState()) {
          qt = obtenerActivos(qt);
        }
        if (chDesactivos.getState()) {
          qt = obtenerDesactivos(qt);
        }

        qt.putWhereType("CD_PCENTI", QueryTool.STRING);
        qt.putWhereValue("CD_PCENTI",
                         ( (Data) pnlPuntoCent.getDatos()).get("CD_PCENTI"));
        qt.putOperator("CD_PCENTI", "=");

        // Datos adicionales: descripci�n de tipo de pto centinela
        QueryTool qtAdic = new QueryTool();
        qtAdic.putName("SIVE_TPCENTINELA");
        qtAdic.putType("DS_TPCENTI", QueryTool.STRING);
        Data dtAdic = new Data();
        dtAdic.put("CD_TPCENTI", QueryTool.STRING);
        qt.addQueryTool(qtAdic);
        qt.addColumnsQueryTool(dtAdic);

        // Datos adicionales:descripci�n de conglomerado
        QueryTool qtAdic1 = new QueryTool();
        qtAdic1.putName("SIVE_CONGLOM");
        qtAdic1.putType("DS_CONGLO", QueryTool.STRING);
        Data dtAdic1 = new Data();
        dtAdic1.put("CD_CONGLO", QueryTool.STRING);
        qt.addQueryTool(qtAdic1);
        qt.addColumnsQueryTool(dtAdic1);

        qt.addOrderField("CD_PCENTI");

        p.addElement(qt);
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          chActivos.setState(false);
          chDesactivos.setState(false);
          chTodos.setState(true);
          pnlPuntoCent.limpiarDatos();
          this.getApp().showAdvise(
              "No se poseen elementos con los criterios especificados");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //Fin del tercer caso

    //4� CASO:se realiza el filtro de PuntoCentinela y el de Tipo (independientem)
    if ( (pnlTipo.getDatos() != null) && (pnlPuntoCent.getDatos() != null)) {
      try {
        QueryTool2 qt = new QueryTool2();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_PCENTINELA");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_PCENTI", QueryTool.STRING);
        qt.putType("DS_PCENTI", QueryTool.STRING);
        qt.putType("CD_CONGLO", QueryTool.STRING);
        qt.putType("CD_TPCENTI", QueryTool.STRING);
        qt.putType("IT_GRIPE", QueryTool.STRING);
        qt.putType("FC_ALTA", QueryTool.DATE);
        qt.putType("IT_BAJA", QueryTool.STRING);
        qt.putType("CD_OPE", QueryTool.STRING);
        qt.putType("FC_ULTACT", QueryTool.DATE);

        if (chActivos.getState()) {
          qt = obtenerActivos(qt);
        }
        if (chDesactivos.getState()) {
          qt = obtenerDesactivos(qt);
        }

        qt.putWhereType("CD_PCENTI", QueryTool.STRING);
        qt.putWhereValue("CD_PCENTI",
                         ( (Data) pnlPuntoCent.getDatos()).get("CD_PCENTI"));
        qt.putOperator("CD_PCENTI", "=");

        qt.putWhereType("CD_TPCENTI", QueryTool.STRING);
        qt.putWhereValue("CD_TPCENTI",
                         ( (Data) pnlTipo.getDatos()).get("CD_TPCENTI"));
        qt.putOperator("CD_TPCENTI", "=");

        // Datos adicionales: descripci�n de tipo de pto centinela
        QueryTool qtAdic = new QueryTool();
        qtAdic.putName("SIVE_TPCENTINELA");
        qtAdic.putType("DS_TPCENTI", QueryTool.STRING);
        Data dtAdic = new Data();
        dtAdic.put("CD_TPCENTI", QueryTool.STRING);
        qt.addQueryTool(qtAdic);
        qt.addColumnsQueryTool(dtAdic);

        // Datos adicionales:descripci�n de conglomerado
        QueryTool qtAdic1 = new QueryTool();
        qtAdic1.putName("SIVE_CONGLOM");
        qtAdic1.putType("DS_CONGLO", QueryTool.STRING);
        Data dtAdic1 = new Data();
        dtAdic1.put("CD_CONGLO", QueryTool.STRING);
        qt.addQueryTool(qtAdic1);
        qt.addColumnsQueryTool(dtAdic1);

        qt.addOrderField("CD_PCENTI");

        p.addElement(qt);
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          chActivos.setState(false);
          chDesactivos.setState(false);
          chTodos.setState(true);
          pnlPuntoCent.limpiarDatos();
          pnlTipo.limpiarDatos();
          this.getApp().showAdvise(
              "No se poseen elementos con los criterios especificados");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //Fin del cuarto caso
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  // solicita la siguiente trama de datos
  // es igual q el primeraPagina pero con una sentencia +
  public Lista siguientePagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    //1� CASO: no se realiza ning�n filtro: QueryTool2 sin where
    if ( (pnlTipo.getDatos() == null) && (pnlPuntoCent.getDatos() == null)) {
      try {
        QueryTool2 qt = new QueryTool2();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_PCENTINELA");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_PCENTI", QueryTool.STRING);
        qt.putType("DS_PCENTI", QueryTool.STRING);
        qt.putType("CD_CONGLO", QueryTool.STRING);
        qt.putType("CD_TPCENTI", QueryTool.STRING);
        qt.putType("IT_GRIPE", QueryTool.STRING);
        qt.putType("FC_ALTA", QueryTool.DATE);
        qt.putType("IT_BAJA", QueryTool.STRING);
        qt.putType("CD_OPE", QueryTool.STRING);
        qt.putType("FC_ULTACT", QueryTool.DATE);

        if (chActivos.getState()) {
          qt = obtenerActivos(qt);
        }
        if (chDesactivos.getState()) {
          qt = obtenerDesactivos(qt);
        }

        // Datos adicionales: descripci�n de tipo de pto centinela
        QueryTool qtAdic = new QueryTool();
        qtAdic.putName("SIVE_TPCENTINELA");
        qtAdic.putType("DS_TPCENTI", QueryTool.STRING);
        Data dtAdic = new Data();
        dtAdic.put("CD_TPCENTI", QueryTool.STRING);
        qt.addQueryTool(qtAdic);
        qt.addColumnsQueryTool(dtAdic);

        // Datos adicionales:descripci�n de conglomerado
        QueryTool qtAdic1 = new QueryTool();
        qtAdic1.putName("SIVE_CONGLOM");
        qtAdic1.putType("DS_CONGLO", QueryTool.STRING);
        Data dtAdic1 = new Data();
        dtAdic1.put("CD_CONGLO", QueryTool.STRING);
        qt.addQueryTool(qtAdic1);
        qt.addColumnsQueryTool(dtAdic1);

        qt.addOrderField("CD_PCENTI");

        p.addElement(qt);
        p.setTrama(this.clmMantpc.getLista().getTrama());
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          this.getApp().showAdvise("No se poseen elementos");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //Fin del primer caso

    //2� CASO: se realiza el filtro de Tipo pero no el de PuntoCent
    if ( (pnlTipo.getDatos() != null) && (pnlPuntoCent.getDatos() == null)) {
      try {
        QueryTool2 qt = new QueryTool2();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_PCENTINELA");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_PCENTI", QueryTool.STRING);
        qt.putType("DS_PCENTI", QueryTool.STRING);
        qt.putType("CD_CONGLO", QueryTool.STRING);
        qt.putType("CD_TPCENTI", QueryTool.STRING);
        qt.putType("IT_GRIPE", QueryTool.STRING);
        qt.putType("FC_ALTA", QueryTool.DATE);
        qt.putType("IT_BAJA", QueryTool.STRING);
        qt.putType("CD_OPE", QueryTool.STRING);
        qt.putType("FC_ULTACT", QueryTool.DATE);

        if (chActivos.getState()) {
          qt = obtenerActivos(qt);
        }
        if (chDesactivos.getState()) {
          qt = obtenerDesactivos(qt);
        }

        qt.putWhereType("CD_TPCENTI", QueryTool.STRING);
        qt.putWhereValue("CD_TPCENTI",
                         ( (Data) pnlTipo.getDatos()).get("CD_TPCENTI"));
        qt.putOperator("CD_TPCENTI", "=");

        // Datos adicionales: descripci�n de tipo de pto centinela
        QueryTool qtAdic = new QueryTool();
        qtAdic.putName("SIVE_TPCENTINELA");
        qtAdic.putType("DS_TPCENTI", QueryTool.STRING);
        Data dtAdic = new Data();
        dtAdic.put("CD_TPCENTI", QueryTool.STRING);
        qt.addQueryTool(qtAdic);
        qt.addColumnsQueryTool(dtAdic);

        // Datos adicionales:descripci�n de conglomerado
        QueryTool qtAdic1 = new QueryTool();
        qtAdic1.putName("SIVE_CONGLOM");
        qtAdic1.putType("DS_CONGLO", QueryTool.STRING);
        Data dtAdic1 = new Data();
        dtAdic1.put("CD_CONGLO", QueryTool.STRING);
        qt.addQueryTool(qtAdic1);
        qt.addColumnsQueryTool(dtAdic1);

        qt.addOrderField("CD_PCENTI");

        p.addElement(qt);
        p.setTrama(this.clmMantpc.getLista().getTrama());
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          chActivos.setState(false);
          chDesactivos.setState(false);
          chTodos.setState(true);
          pnlTipo.limpiarDatos();
          this.getApp().showAdvise(
              "No se poseen elementos con los criterios especificados");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //Fin del segundo caso

    //3� CASO:se realiza el filtro de PuntoCentinela, pero no el de Tipo
    if ( (pnlTipo.getDatos() == null) && (pnlPuntoCent.getDatos() != null)) {
      try {
        QueryTool2 qt = new QueryTool2();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_PCENTINELA");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_PCENTI", QueryTool.STRING);
        qt.putType("DS_PCENTI", QueryTool.STRING);
        qt.putType("CD_CONGLO", QueryTool.STRING);
        qt.putType("CD_TPCENTI", QueryTool.STRING);
        qt.putType("IT_GRIPE", QueryTool.STRING);
        qt.putType("FC_ALTA", QueryTool.DATE);
        qt.putType("IT_BAJA", QueryTool.STRING);
        qt.putType("CD_OPE", QueryTool.STRING);
        qt.putType("FC_ULTACT", QueryTool.DATE);

        if (chActivos.getState()) {
          qt = obtenerActivos(qt);
        }
        if (chDesactivos.getState()) {
          qt = obtenerDesactivos(qt);
        }

        qt.putWhereType("CD_PCENTI", QueryTool.STRING);
        qt.putWhereValue("CD_PCENTI",
                         ( (Data) pnlPuntoCent.getDatos()).get("CD_PCENTI"));
        qt.putOperator("CD_PCENTI", "=");

        // Datos adicionales: descripci�n de tipo de pto centinela
        QueryTool qtAdic = new QueryTool();
        qtAdic.putName("SIVE_TPCENTINELA");
        qtAdic.putType("DS_TPCENTI", QueryTool.STRING);
        Data dtAdic = new Data();
        dtAdic.put("CD_TPCENTI", QueryTool.STRING);
        qt.addQueryTool(qtAdic);
        qt.addColumnsQueryTool(dtAdic);

        // Datos adicionales:descripci�n de conglomerado
        QueryTool qtAdic1 = new QueryTool();
        qtAdic1.putName("SIVE_CONGLOM");
        qtAdic1.putType("DS_CONGLO", QueryTool.STRING);
        Data dtAdic1 = new Data();
        dtAdic1.put("CD_CONGLO", QueryTool.STRING);
        qt.addQueryTool(qtAdic1);
        qt.addColumnsQueryTool(dtAdic1);

        qt.addOrderField("CD_PCENTI");

        p.addElement(qt);
        p.setTrama(this.clmMantpc.getLista().getTrama());
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          chActivos.setState(false);
          chDesactivos.setState(false);
          chTodos.setState(true);
          pnlPuntoCent.limpiarDatos();
          this.getApp().showAdvise(
              "No se poseen elementos con los criterios especificados");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //Fin del tercer caso

    //4� CASO:se realiza el filtro de PuntoCentinela y el de Tipo (independientem)
    if ( (pnlTipo.getDatos() != null) && (pnlPuntoCent.getDatos() != null)) {
      try {
        QueryTool2 qt = new QueryTool2();

        // Nombre de la tabla (FROM)
        qt.putName("SIVE_PCENTINELA");

        // Campos que se quieren leer (SELECT)
        qt.putType("CD_PCENTI", QueryTool.STRING);
        qt.putType("DS_PCENTI", QueryTool.STRING);
        qt.putType("CD_CONGLO", QueryTool.STRING);
        qt.putType("CD_TPCENTI", QueryTool.STRING);
        qt.putType("IT_GRIPE", QueryTool.STRING);
        qt.putType("FC_ALTA", QueryTool.DATE);
        qt.putType("IT_BAJA", QueryTool.STRING);
        qt.putType("CD_OPE", QueryTool.STRING);
        qt.putType("FC_ULTACT", QueryTool.DATE);

        if (chActivos.getState()) {
          qt = obtenerActivos(qt);
        }
        if (chDesactivos.getState()) {
          qt = obtenerDesactivos(qt);
        }

        qt.putWhereType("CD_PCENTI", QueryTool.STRING);
        qt.putWhereValue("CD_PCENTI",
                         ( (Data) pnlPuntoCent.getDatos()).get("CD_PCENTI"));
        qt.putOperator("CD_PCENTI", "=");

        qt.putWhereType("CD_TPCENTI", QueryTool.STRING);
        qt.putWhereValue("CD_TPCENTI",
                         ( (Data) pnlTipo.getDatos()).get("CD_TPCENTI"));
        qt.putOperator("CD_TPCENTI", "=");

        // Datos adicionales: descripci�n de tipo de pto centinela
        QueryTool qtAdic = new QueryTool();
        qtAdic.putName("SIVE_TPCENTINELA");
        qtAdic.putType("DS_TPCENTI", QueryTool.STRING);
        Data dtAdic = new Data();
        dtAdic.put("CD_TPCENTI", QueryTool.STRING);
        qt.addQueryTool(qtAdic);
        qt.addColumnsQueryTool(dtAdic);

        // Datos adicionales:descripci�n de conglomerado
        QueryTool qtAdic1 = new QueryTool();
        qtAdic1.putName("SIVE_CONGLOM");
        qtAdic1.putType("DS_CONGLO", QueryTool.STRING);
        Data dtAdic1 = new Data();
        dtAdic1.put("CD_CONGLO", QueryTool.STRING);
        qt.addQueryTool(qtAdic1);
        qt.addColumnsQueryTool(dtAdic1);

        qt.addOrderField("CD_PCENTI");

        p.addElement(qt);
        p.setTrama(this.clmMantpc.getLista().getTrama());
        p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
        if (p1.size() == 0) {
          chActivos.setState(false);
          chDesactivos.setState(false);
          chTodos.setState(true);
          pnlPuntoCent.limpiarDatos();
          this.getApp().showAdvise(
              "No se poseen elementos con los criterios especificados");
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } //Fin del cuarto caso
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  // operaciones de la botonera
  //mirar en PanPetCen.
  //pasarlo como PuntoCentExt
  public void realizaOperacion(int j) {
    Lista lismanpc = this.clmMantpc.getLista();
    DiaMantpc di = null;
    //Este Data debe pasarse como par�metro al constructor de PuntoCentExt
    Data dMantpc = new Data();
    switch (j) {
      // bot�n de alta:data vacio
      case ALTA:

        //introduzco el cd_centrovac para hacer la alta con �l
        PuntoCentExt dPcealt = new PuntoCentExt(dMantpc);
        di = new DiaMantpc(this.getApp(), ALTA, dPcealt);
        di.show();
        // a�adir el nuevo elem a la lista
        if (di.bAceptar()) {
          clmMantpc.setPrimeraPagina(primeraPagina());
        }
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        dMantpc = clmMantpc.getSelected();
        //si existe alguna fila seleccionada
        if (dMantpc != null) {
          int ind = clmMantpc.getSelectedIndex();
          PuntoCentExt dPcemodif = new PuntoCentExt(dMantpc);
          di = new DiaMantpc(this.getApp(), MODIFICACION, dPcemodif);
          di.show();
          //Tratamiento de bAceptar para Salir
          if (di.bAceptar()) {
            clmMantpc.setPrimeraPagina(primeraPagina());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dMantpc = clmMantpc.getSelected();
        if (dMantpc != null) {
          int ind = clmMantpc.getSelectedIndex();
          PuntoCentExt dPce = new PuntoCentExt(dMantpc);
          di = new DiaMantpc(this.getApp(), BAJA, dPce);
          di.show();
          if (di.bAceptar()) {
            if (di.borrPto() == 0) {
              clmMantpc.getLista().removeElementAt(ind);
              clmMantpc.setPrimeraPagina(clmMantpc.getLista());
            }
            if (di.borrPto() == 1) {
              clmMantpc.setPrimeraPagina(primeraPagina());
            }
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
        }
        break;

    }
  }

  void btnBuscar_actionPerformed(ActionEvent e) {
    Inicializar(CInicializar.ESPERA);
    clmMantpc.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
  }

  void chActivos_itemStateChanged(ItemEvent e) {
    if (chActivos.getState()) {
      chDesactivos.setState(false);
      chTodos.setState(false);
    }
  }

  void chDesactivos_itemStateChanged(ItemEvent e) {
    if (chDesactivos.getState()) {
      chActivos.setState(false);
      chTodos.setState(false);
    }
  }

  void chTodos_itemStateChanged(ItemEvent e) {
    if (chTodos.getState()) {
      chActivos.setState(false);
      chDesactivos.setState(false);
    }
  }

}

class PanMantpc_btnBuscar_actionAdapter
    implements java.awt.event.ActionListener {
  PanMantpc adaptee;

  PanMantpc_btnBuscar_actionAdapter(PanMantpc adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnBuscar_actionPerformed(e);
  }
}

class PanMantpc_chActivos_itemAdapter
    implements java.awt.event.ItemListener {
  PanMantpc adaptee;

  PanMantpc_chActivos_itemAdapter(PanMantpc adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chActivos_itemStateChanged(e);
  }
}

class PanMantpc_chDesactivos_itemAdapter
    implements java.awt.event.ItemListener {
  PanMantpc adaptee;

  PanMantpc_chDesactivos_itemAdapter(PanMantpc adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chDesactivos_itemStateChanged(e);
  }
}

class PanMantpc_chTodos_itemAdapter
    implements java.awt.event.ItemListener {
  PanMantpc adaptee;

  PanMantpc_chTodos_itemAdapter(PanMantpc adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chTodos_itemStateChanged(e);
  }
}
