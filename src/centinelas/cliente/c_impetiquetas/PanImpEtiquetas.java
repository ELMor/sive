package centinelas.cliente.c_impetiquetas;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPanel;
import centinelas.cliente.c_creaciondbf.DBFCartasEtiq;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class PanImpEtiquetas
    extends CPanel {

  XYLayout xyLayout = new XYLayout();

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 1;
  final int INTERVERT = 20;
  final int INTERHOR = 10;
  final int ALTO = 13;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  //escuchadores
  chTodosItemAdapter itemAdapterTodos = new chTodosItemAdapter(this);
  chManualItemAdapter itemAdapterManual = new chManualItemAdapter(this);
  btnActionAdapter actionAdapter = new btnActionAdapter(this);

  Checkbox chTodos = new Checkbox();
  Checkbox chManual = new Checkbox();
  CheckboxGroup chkGrp1 = new CheckboxGroup();
  capp2.CFileName panFichero;

  ButtonControl btnAceptar = new ButtonControl();

  //Lista que se env�a al servlet, con los datos para el BDF
  Lista lServ = null;

  //modo en el que se llama al servlet
  final protected int servletCARTETIQ = 0;

  public PanImpEtiquetas(CApp a) {
    try {
      setApp(a);
      panFichero = new capp2.CFileName(a);
      panFichero.txtFile.setText("c:\\datos.dbf");

      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgAceptar = "images/aceptar.gif";

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().CargaImagenes();

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnAceptar.addActionListener(actionAdapter);

    xyLayout.setHeight(248);
    xyLayout.setWidth(420);
    this.setLayout(xyLayout);

    this.add(chTodos,
             new XYConstraints(MARGENIZQ + 2 * INTERHOR,
                               MARGENSUP + ALTO + INTERVERT, 247, 34));
    this.add(chManual,
             new XYConstraints(MARGENIZQ + 2 * INTERHOR,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 130, 34));
    this.add(panFichero,
             new XYConstraints(MARGENIZQ - 13,
                               MARGENSUP + ALTO + 4 * INTERVERT + 34, 515, 38));
    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 400,
                               MARGENSUP + ALTO + 5 * INTERVERT + 34 + 38, 88,
                               29));

    chTodos.setName("Todos");
    chTodos.setLabel("Selecci�n de todos los puntos centinelas");
    chTodos.setCheckboxGroup(chkGrp1);
    chTodos.addItemListener(itemAdapterTodos);

    chManual.setName("Manual");
    chManual.setLabel("Selecci�n manual");
    chManual.setCheckboxGroup(chkGrp1);
    chManual.addItemListener(itemAdapterManual);
    chTodos.setState(true);
  } //end jbinit

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera: se hace un backup de los datos
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
    }
  }

  void chTodos_itemStateChanged(ItemEvent e) {
  }

  void chManual_itemStateChanged(ItemEvent e) {
  }

  void crearDBF() {
    try {
      String localiz = this.panFichero.txtFile.getText();
      DBFCartasEtiq genDBF = new DBFCartasEtiq(lServ, localiz);
      //dbfCar= genDBFCar.getDBF();
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
  }

  boolean isDataValid() {
    if (panFichero.txtFile.getText().length() == 0) {
      this.getApp().showAdvise("Debe seleccionar el campo Nombre del fichero");
      return false;
    }
    else {
      return true;
    }
  }

  Lista anadirCampos(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = (Data) lCampo.elementAt(i);
      String s = "Dr. " + dCampo.getString("DS_NOMBRE") + " " +
          dCampo.getString("DS_APE1");
      String p = dCampo.getString("CD_PCENTI") + " - " +
          dCampo.getString("DS_PCENTI");
      String direc = null;
      if (dCampo.getString("DS_NUM").equals("")) {
        direc = dCampo.getString("DS_DIREC") + " " + dCampo.getString("DS_PISO");
      }
      else {
        direc = dCampo.getString("DS_DIREC") + " N�" +
            dCampo.getString("DS_NUM") + " " + dCampo.getString("DS_PISO");
      }
      ( (Data) lCampo.elementAt(i)).put("MEDICO_CENTINELA", s);
      ( (Data) lCampo.elementAt(i)).put("PUNTO_CENTINELA", p);
      ( (Data) lCampo.elementAt(i)).put("DIRECCION", direc);
    }
    return lCampo;
  }

  Lista buscarPuntos() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = "servlet/SrvQueryTool";

    Inicializar(CInicializar.ESPERA);

    //Hallamos los m�dicos asociados a ese pto:order by desc
    QueryTool2 qt = new QueryTool2();

    qt.putName("SIVE_MCENTINELA");
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("DS_NOMBRE", QueryTool.STRING);
    qt.putType("DS_APE1", QueryTool.STRING);
    qt.putType("DS_APE2", QueryTool.STRING);
    qt.putType("CD_E_NOTIF", QueryTool.STRING);
    qt.putType("CD_MASIST", QueryTool.STRING);
    qt.putType("DS_DNI", QueryTool.STRING);
    qt.putType("DS_TELEF", QueryTool.STRING);
    qt.putType("DS_FAX", QueryTool.STRING);
    qt.putType("FC_ALTA", QueryTool.DATE);
    qt.putType("IT_BAJA", QueryTool.STRING);
    qt.putType("CD_PCENTI", QueryTool.STRING);

    // El G�ear.
    qt.putWhereType("IT_BAJA", QueryTool.STRING);
    qt.putWhereValue("IT_BAJA", "N");
    qt.putOperator("IT_BAJA", "=");

    QueryTool qtAdic = new QueryTool();
    qtAdic.putName("SIVE_PCENTINELA");
    qtAdic.putType("DS_PCENTI", QueryTool.STRING);
    Data dtAdic = new Data();
    dtAdic.put("CD_PCENTI", QueryTool.STRING);
    qt.addQueryTool(qtAdic);
    qt.addColumnsQueryTool(dtAdic);

    QueryTool qtAdic1 = new QueryTool();
    qtAdic1.putName("SIVE_E_NOTIF");
    qtAdic1.putType("CD_CENTRO", QueryTool.STRING);
    Data dtAdic1 = new Data();
    dtAdic1.put("CD_E_NOTIF", QueryTool.STRING);
    qt.addQueryTool(qtAdic1);
    qt.addColumnsQueryTool(dtAdic1);

    QueryTool qtAdic2 = new QueryTool();
    qtAdic2.putName("SIVE_C_NOTIF");
    qtAdic2.putType("DS_DIREC", QueryTool.STRING);
    qtAdic2.putType("DS_NUM", QueryTool.STRING);
    qtAdic2.putType("DS_PISO", QueryTool.STRING);
    qtAdic2.putType("CD_POSTAL", QueryTool.STRING);
    qtAdic2.putType("CD_MUN", QueryTool.STRING);
    qtAdic2.putType("CD_PROV", QueryTool.STRING);
    Data dtAdic2 = new Data();
    dtAdic2.put("CD_CENTRO", QueryTool.STRING);
    qt.addQueryTool(qtAdic2);
    qt.addColumnsQueryTool(dtAdic2);

    QueryTool qtAdic3 = new QueryTool();
    qtAdic3.putName("SIVE_MUNICIPIO");
    qtAdic3.putType("DS_MUN", QueryTool.STRING);
    Data dtAdic3 = new Data();
    dtAdic3.put("CD_PROV", QueryTool.STRING);
    dtAdic3.put("CD_MUN", QueryTool.STRING);
    qt.addQueryTool(qtAdic3);
    qt.addColumnsQueryTool(dtAdic3);

    QueryTool qtAdic4 = new QueryTool();
    qtAdic4.putName("SIVE_PROVINCIA");
    qtAdic4.putType("DS_PROV", QueryTool.STRING);
    Data dtAdic4 = new Data();
    dtAdic4.put("CD_PROV", QueryTool.STRING);
    qt.addQueryTool(qtAdic4);
    qt.addColumnsQueryTool(dtAdic4);

    qt.addOrderField("CD_PCENTI");

    p.addElement(qt);

    try {
      this.getApp().getStub().setUrl(servlet);
      p1 = (Lista)this.getApp().getStub().doPost(1, p);
      if (p1.size() != 0) {
        p1 = anadirCampos(p1);
      }

    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  void btn_actionPerformed(ActionEvent e) {
    DiaImpEtiquetas di = null;
    boolean accesoServlet = true;
    Lista lResult = null;
    final String servEtiq = "servlet/SrvCartasEtiq";

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        Inicializar(CInicializar.ESPERA);

        //si est� seleccionado el modo manual
        if (chManual.getState()) {
          di = new DiaImpEtiquetas(this.getApp());
          di.show();

          //si se ha realizado la operaci�n
          if (di.bAcepta()) {
            lServ = di.listaDev();
            if (lServ.size() == 0) {
              accesoServlet = false;
              this.getApp().showAdvise(
                  "No se ha realizado ninguna selecci�n de puntos centinelas");
            }
          }
          else {
            accesoServlet = false;
            this.getApp().showAdvise(
                "No se ha realizado ninguna selecci�n de puntos centinelas");
          }
        }
        else { //end chManual y si chTodos el seleccionado
          lServ = buscarPuntos();
          if (lServ.size() == 0) {
            accesoServlet = false;
            this.getApp().showAdvise("No existen puntos centinelas");
          }
        }

        //si existen puntos centinelas seleccionados
        if (accesoServlet) {
          try {
            crearDBF();

                /*            this.getApp().getStub().setUrl("servlet/SrvCartasEtiq");
                        Lista lResult = (Lista)this.getApp().getStub().doPost(servletCARTETIQ_AVISOS,lParam);
             */
            this.getApp().showAdvise("Exportaci�n de fichero finalizada");

          }
          catch (Exception ex) {
            this.getApp().trazaLog(ex);
            this.getApp().showError(ex.getMessage());
          }
        }
        Inicializar(CInicializar.NORMAL);
      } //end if isDataValid
    } //end btnAceptar
  } //end      btn_actionPerformed

}

class chManualItemAdapter
    implements java.awt.event.ItemListener {
  PanImpEtiquetas adaptee = null;

  public chManualItemAdapter(PanImpEtiquetas adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(ItemEvent e) {
    adaptee.chManual_itemStateChanged(e);
  }
}

class chTodosItemAdapter
    implements java.awt.event.ItemListener {
  PanImpEtiquetas adaptee = null;

  public chTodosItemAdapter(PanImpEtiquetas adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(ItemEvent e) {
    adaptee.chTodos_itemStateChanged(e);
  }
}

class btnActionAdapter
    implements java.awt.event.ActionListener {
  PanImpEtiquetas adaptee;
  ActionEvent e;

  btnActionAdapter(PanImpEtiquetas adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("Aceptar")) {
      adaptee.btn_actionPerformed(e);
    }
  }
}
