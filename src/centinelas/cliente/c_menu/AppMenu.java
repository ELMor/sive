/**
 * Clase: AppMenu
 * Paquete: centinelas.cliente.c_menu
 * Hereda: CApp
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 11/02/2000
 * Analisis Funcional: Gesti�n de men�s
 * Descripcion: Applet que contiene/controla el men� de acceso
 *  a los diferentes apartados de centinelas
 */

package centinelas.cliente.c_menu;

import java.net.URL;

import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import capp2.CApp;
import capp2.CLogin;
import capp2.CMenuPanel;
import capp2.UMenu;
import capp2.UMenuItem;
import comun.constantes;
import sapp2.Data;

public class AppMenu
    extends CApp
    implements ActionListener {

  // Par�metros para invocar al servidor HTML
  private Data dtParametros = null;

  // Dialogo para obtener los valores por defecto
  DlgMenu dlgMenu = null;

  // Objetos del men�
  CMenuPanel mPanel = new CMenuPanel();
  MenuBar mBar = new MenuBar();

  public void init() {
    super.init();

    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  //___________________________________________________

  void jbInit() throws Exception {

    Data dtAut = null;

    // Solicita las acreditaciones del usuario
    CLogin dlg = new CLogin(this, "servlet/SrvCentAut",
                            "Control de Acceso [" +
                            getParameter("COD_APLICACION") + "]");
    dlg.show();
    dtAut = dlg.getAutorizaciones();

    // modificacion jlt para recuperar el url_servlet y
    // el url_html
    String ur = new String();
    String ur2 = new String();
    String ur3 = new String();
    String ur4 = new String();
    int ind = 0;
    int indb = 0;
    ur = (String)this.getDocumentBase().toString();
    ind = ur.lastIndexOf("/");
    ur2 = (String) ur.substring(0, ind + 1);
    ur3 = (String) ur2.substring(0, ind);
    indb = ur3.lastIndexOf("/");
    ur4 = (String) ur3.substring(0, indb + 1);
    System.out.println("urmenu2" + ur2);
    System.out.println("urmenu4" + ur4);
    /////////////////

    setAutorizaciones(dtAut);

    // Si el usuario est� acreditado tiene acceso
    if (dtAut != null) {

      // Prepara la estructura de par�metros para solicitar a las applets
      dtParametros = new Data();
      dtParametros.put("IT_USU", parseAutorizaciones());
      dtParametros.put("COD_APLICACION", getParametro("COD_APLICACION"));

      if (getParametro("URL_SERVLET").equals("") ||
          getParametro("URL_SERVLET").equals("/")
          || getParametro("URL_SERVLET").equals(null)) {

        dtParametros.put("URL_SERVLET", ur4);
      }
      else {
        dtParametros.put("URL_SERVLET", getParametro("URL_SERVLET"));
      }

      //dtParametros.put("URL_SERVLET", getParametro("URL_SERVLET"));

      if (getParametro("URL_HTML").equals("") ||
          getParametro("URL_HTML").equals("/")
          || getParametro("URL_HTML").equals(null)) {

        dtParametros.put("URL_HTML", ur2);
      }
      else {
        dtParametros.put("URL_HTML", getParametro("URL_HTML"));
      }
      //dtParametros.put("URL_HTML", getParametro("URL_HTML"));

      dtParametros.put("COD_USUARIO", dtAut.getString("COD_USUARIO"));
      dtParametros.put("FC_ACTUAL", dtAut.getString("FC_ACTUAL"));

      mPanel.removeAll();
      mBar = new MenuBar();

      // Notificaciones RMC
      if (isAccesible("1")) {
        UMenu m1 = new UMenu("Notificaciones", "1", this);
        mBar.add(m1);

        // Comprueba si esta cerrada
        if (isAllowed("1")) {
          UMenuItem mi1 = new UMenuItem("Notificaciones", "1", 1, this, this);
          m1.add(mi1);
        } // Fin if(allowed "1")
      } // Fin if(accesible "1")

      // Listados
      if (isAccesible("9")) {
        UMenu m9 = new UMenu("Listados", "9", this);
        mBar.add(m9);

        // Comprueba si esta cerrada
        if (isAllowed("9")) {

          UMenuItem mi91 = new UMenuItem("Etiquetas", "91", 91, this, this);
          m9.add(mi91);

          UMenuItem mi92 = new UMenuItem("Cartas", "92", 92, this, this);
          m9.add(mi92);

//          UMenuItem mi93 = new UMenuItem("Cartas de aviso segun modelo","93",93,this,this);
//          m9.add(mi93);   DE MOMENTO NO SE A�ADE ESTA OPCION

          UMenuItem mi94 = new UMenuItem("Reimpresi�n de cartas enviadas", "94",
                                         94, this, this);
          m9.add(mi94);

          UMenuItem mi95 = new UMenuItem("Cartas de aviso seg�n modelo", "95",
                                         95, this, this);
          m9.add(mi95);

        } // Fin if(allowed "9")
      } // Fin if(accesible "9")

      // Exportaciones
      if (isAccesible("2")) {
        UMenu m2 = new UMenu("Exportaciones", "2", this);
        mBar.add(m2);

        // Comprueba si esta cerrada
        if (isAllowed("2")) {
          // Casos Numericos
          UMenuItem mi21 = new UMenuItem("Casos Num�ricos", "21", 21, this, this);
          if (isAccesible("21")) {
            m2.add(mi21);

            // Casos Individuales
          }
          UMenuItem mi22 = new UMenuItem("Casos Individuales", "22", 22, this, this);
          if (isAccesible("22")) {
            m2.add(mi22);

            // Poblacion Puntos Centinelas
          }
          UMenuItem mi23 = new UMenuItem("Poblacion Puntos Centinelas", "23",
                                         23, this, this);
          if (isAccesible("23")) {
            m2.add(mi23);

            // Indices de Actividad
          }
          UMenuItem mi24 = new UMenuItem("Indices de Actividad", "24", 24, this, this);
          if (isAccesible("24")) {
            m2.add(mi24);

            // Indicadores de Alarma
          }
          UMenuItem mi25 = new UMenuItem("Indicadores de Alarma", "25", 25, this, this);
          if (isAccesible("25")) {
            m2.add(mi25);
          }
        } // Fin if(allowed("2"))
      } // Fin if(accesible("2"))

      // Envios al CNE
      if (isAccesible("3")) {
        UMenu m3 = new UMenu("Envios CNE", "3", this);
        mBar.add(m3);

        // Comprueba si esta cerrada
        if (isAllowed("3")) {

          UMenuItem mi31 = new UMenuItem("Envio de casos RMC", "31", 31, this, this);
          if (isAccesible("31")) {
            m3.add(mi31);

          }

        } // Fin if(allowed "3")
      } // Fin if(accesible "3")

      // Parametrizacion
      if (isAccesible("4")) {
        UMenu m4 = new UMenu("Parametrizaci�n", "4", this);
        mBar.add(m4);

        if (isAllowed("4")) {

          UMenu m41 = new UMenu("Indicadores de Alarma", "41", this);
          if (isAccesible("41")) {
            m4.add(m41);

            // Generaci�n de alarmas autom�ticas
            UMenuItem mi411 = new UMenuItem("Generaci�n de Alarmas Autom�ticas",
                                            "411", 411, this, this);
            m41.add(mi411);

            // Mantenimiento de Indicadores
            UMenuItem mi412 = new UMenuItem("Mantenimiento de Indicadores",
                                            "412", 412, this, this);
            m41.add(mi412);
          } // Fin if(accesible "41")

          UMenu m42 = new UMenu("Protocolos RMC", "42", this);
          if (isAccesible("42")) {
            m4.add(m42);

            // Mantenimiento de Modelos
            UMenuItem mi421 = new UMenuItem("Mantenimiento de Modelos", "421",
                                            421, this, this);
            m42.add(mi421);

            // Mantenimiento de Listas
            UMenuItem mi422 = new UMenuItem("Mantenimiento de Listas", "422",
                                            422, this, this);
            m42.add(mi422);

            // Mantenimiento de Preguntas
            UMenuItem mi423 = new UMenuItem("Mantenimiento de Preguntas", "423",
                                            423, this, this);
            m42.add(mi423);

            // Mantenimiento de Preguntas fijas
            UMenuItem mi424 = new UMenuItem("Mantenimiento de Preguntas fijas",
                                            "424", 424, this, this);
            m42.add(mi424);

          } // Fin if(accesible "42")

        } // Fin if(allowed "4")
      } // Fin if(accesible "4")

      // Administraci�n
      if (isAccesible("5")) {
        UMenu m5 = new UMenu("Administraci�n", "5", this);
        mBar.add(m5);

        // Comprueba si esta cerrada
        if (isAllowed("5")) {

          // Puntos Centinela
          UMenuItem mi51 = new UMenuItem("Puntos Centinela", "51", 51, this, this);
          if (isAccesible("51")) {
            m5.add(mi51);

            // Medicos Centinela
          }
          UMenuItem mi52 = new UMenuItem("Medicos Centinela", "52", 52, this, this);
          if (isAccesible("52")) {
            m5.add(mi52);

            // Poblaci�n M�dico Notificador
          }
          UMenuItem mi53 = new UMenuItem("Poblaci�n M�dico Notificador", "53",
                                         53, this, this);
          if (isAccesible("53")) {
            m5.add(mi53);

            // Indices Actividad Asistencial
          }
          UMenuItem mi54 = new UMenuItem("Indices Actividad Asistencial", "54",
                                         54, this, this);
          if (isAccesible("54")) {
            m5.add(mi54);

            // Enfermedades Centinela
          }
          UMenuItem mi55 = new UMenuItem("Enfermedades Centinela", "55", 55, this, this);
          if (isAccesible("55")) {
            m5.add(mi55);

            // Separador
          }
          MenuItem msep1 = new MenuItem("-");
          m5.add(msep1);

          // Generar A�o Epidemiol�gico
          UMenuItem mi56 = new UMenuItem("Generar A�o Epidemiol�gico", "56", 56, this, this);
          if (isAccesible("56")) {
            m5.add(mi56);

            // Separador
          }
          MenuItem msep2 = new MenuItem("-");
          m5.add(msep2);

          // Grupos de Edades
          UMenuItem mi57 = new UMenuItem("Grupos de Edades", "57", 57, this, this);
          if (isAccesible("57")) {
            m5.add(mi57);

            // Modelos de Cartas
          }
          UMenuItem mi58 = new UMenuItem("Modelos de Cartas", "58", 58, this, this);
          if (isAccesible("58")) {
            m5.add(mi58);

            // Separador
          }
          MenuItem msep3 = new MenuItem("-");
          m5.add(msep3);

          // Tablas de C�digos
          UMenu m59 = new UMenu("Tablas de C�digos", "59", this);
          if (isAccesible("59")) {
            m5.add(m59);

            // Tipo de Pregunta
            UMenuItem mi591 = new UMenuItem("Tipo de Pregunta", "591", 591, this, this);
            m59.add(mi591);

            // Tipo SIVE
            UMenuItem mi592 = new UMenuItem("Tipo SIVE", "592", 592, this, this);
            m59.add(mi592);

            // Enfermedad CIE
            UMenuItem mi593 = new UMenuItem("Enfermedad CIE", "593", 593, this, this);
            m59.add(mi593);

            // Procesos
            UMenuItem mi594 = new UMenuItem("Procesos", "594", 594, this, this);
            m59.add(mi594);

            // Sexo
            UMenuItem mi595 = new UMenuItem("Sexo", "595", 595, this, this);
            m59.add(mi595);

            // Tipo de Linea
            UMenuItem mi596 = new UMenuItem("Tipo de Linea", "596", 596, this, this);
            m59.add(mi596);

            // Motivos de Baja
            UMenuItem mi597 = new UMenuItem("Motivos de Baja", "597", 597, this, this);
            m59.add(mi597);

            // Separador
            MenuItem msep = new MenuItem("-");
            m59.add(msep);

            // Conglomerado
            UMenuItem mi5981 = new UMenuItem("Conglomerado", "5981", 5981, this, this);
            m59.add(mi5981);

            // Modelos de Asistencia
            UMenuItem mi5982 = new UMenuItem("Modelos de Asistencia", "5982",
                                             5982, this, this);
            m59.add(mi5982);

            // Tipo de Punto Centinela
            UMenuItem mi5983 = new UMenuItem("Tipo de Punto Centinela", "5983",
                                             5983, this, this);
            m59.add(mi5983);

            // Indices de Actividad Asistencial
            UMenuItem mi5984 = new UMenuItem("Indices de Actividad Asistencial",
                                             "5984", 5984, this, this);
            m59.add(mi5984);
          } // Fin if(accesible "59")

          // Separador
          MenuItem msep4 = new MenuItem("-");
          m5.add(msep4);

          // Grupos de trabajo
          UMenuItem mi60 = new UMenuItem("Grupos de trabajo", "60", 60, this, this);
          if (isAccesible("60")) {
            m5.add(mi60);
          }
        } // Fin if(allowed "5")
      } // Fin if(accesible "5")

      // General
      if (isAccesible("6")) {
        UMenu m6 = new UMenu("General", "6", this);
        mBar.add(m6);

        // Comprueba si esta cerrada
        if (isAllowed("6")) {
          UMenuItem mi61 = new UMenuItem("Seleccionar A�o", "", 61, this, this);
          m6.add(mi61);

          // Opci�n eliminada
          //UMenuItem mi62 = new UMenuItem("Cambio de contrase�a","62",62,this,this);
          //m6.add(mi62);
        } // Fin if(allowed "6")
      } // Fin if(accesible "6")

      // Ayuda
      if (isAccesible("7")) {
        UMenu m7 = new UMenu("Ayuda", "7", this);
        mBar.add(m7);

        // Comprueba si esta cerrada
        if (isAllowed("7")) {

          // Sistema de Informaci�n RMC
          UMenuItem mi71 = new UMenuItem("Sistema de Informaci�n RMC", "71", 71, this, this);
          if (isAccesible("71")) {
            m7.add(mi71);

            // Acerca de ...
          }
          UMenuItem mi72 = new UMenuItem("Acerca de ...", "72", 72, this, this);
          if (isAccesible("72")) {
            m7.add(mi72);

          }
        } // Fin if(allowed "7")
      } // Fin if(accesible "7")

      // Crea el di�logo de valores por defecto (a�o) y lo muestra
      dlgMenu = new DlgMenu(this, constantes.modoMODIFICACION,
                            dtAut.getString("FC_ACTUAL").substring(6));
      dlgMenu.show();

      // Completa los valores por defecto: A�o
      if (dlgMenu.getAceptar()) {
        dtParametros.put("CD_ANO", dlgMenu.getParametros().getString("CD_ANO"));
      }
      else {
        dtParametros.put("CD_ANO", dtAut.getString("FC_ACTUAL").substring(6));

      }
      dlgMenu = null;

    } // Fin if(dtAut!=null)

    // Pnta el men�
    mPanel.setMenuBar(mBar);
    add("North", mPanel);
    validate();
  }

  //___________________________________________________

  public void actionPerformed(ActionEvent e) {
    URL url;
    String get;

    // modificacion jlt para recuperar el url_servlet y
    // el url_html
    String ur = new String();
    String ur2 = new String();
    String ur3 = new String();
    String ur4 = new String();
    int ind = 0;
    int indb = 0;
    ur = (String)this.getDocumentBase().toString();
    ind = ur.lastIndexOf("/");
    ur2 = (String) ur.substring(0, ind + 1);
    ur3 = (String) ur2.substring(0, ind);
    indb = ur3.lastIndexOf("/");
    ur4 = (String) ur3.substring(0, indb + 1);
    System.out.println("urmenu2" + ur2);
    System.out.println("urmenu4" + ur4);
    /////////////////

    if (e.getSource()instanceof UMenuItem) {
      UMenuItem mi = (UMenuItem) e.getSource();
      try {

        // Agrega el c�digo como par�metro
        dtParametros.put("NM_APPLET", (new Integer(mi.getCode())).toString());

        // Recarga el applet
        switch (mi.getCode()) {

          // Selecci�n de valores por defecto
          case 61:

            // Muestra el dialogo de seleccion de valores por defecto
//            dlgMenu = new DlgMenu(this, constantes.modoMODIFICACION,dtParametros.getString("CD_ANO").substring(6));
            dlgMenu = new DlgMenu(this, constantes.modoMODIFICACION,
                                  dtParametros.getString("CD_ANO"));
            dlgMenu.show();

            // Completa los valores por defecto: A�o
            if (dlgMenu.getAceptar()) {
              dtParametros.put("CD_ANO",
                               dlgMenu.getParametros().getString("CD_ANO"));

            }
            dlgMenu = null;
            break;

          case 71:

            //Se obtiene la URL de la pag. html de ayuda

            // modificacion jlt para recuperar url_html
            if (getParametro("URL_HTML").equals("") ||
                getParametro("URL_HTML").equals("/")
                || getParametro("URL_HTML").equals(null)) {

              url = new URL(ur2 + "zip/ayuda/centinelas/" + "ayuda_pp.html");
            }
            else {
              url = new URL(getParametro("URL_HTML") + "zip/ayuda/centinelas/" +
                            "ayuda_pp.html");
            }

            //url=new URL(getParametro("URL_HTML") + "zip/ayuda/centinelas/"   + "ayuda_pp.html");
            //Se carga la p�gina en el frame "_blank" (nueva ventana)
            getAppletContext().showDocument(url, "_blank");
            break;

          case 72:

            // modificacion jlt para recuperar url_html
            if (getParametro("URL_HTML").equals("") ||
                getParametro("URL_HTML").equals("/")
                || getParametro("URL_HTML").equals(null)) {

              url = new URL(ur2 + "zip/ayuda/centinelas/" + "ayuda_ad.html");
            }
            else {
              url = new URL(getParametro("URL_HTML") + "zip/ayuda/centinelas/" +
                            "ayuda_ad.html");
            }

            //url=new URL(getParametro("URL_HTML") + "zip/ayuda/centinelas/"  + "ayuda_ad.html");
            getAppletContext().showDocument(url, "_blank");
            break;

            // Invoca URL del Applet de trabajo
          default:

            // modificacion jlt para recuperar url_servlet
            if (getParametro("URL_SERVLET").equals("") ||
                getParametro("URL_SERVLET").equals("/")
                || getParametro("URL_SERVLET").equals(null)) {

              url = new URL(ur4 + "servlet/SrvCentApp?" + dtParametros.toURL());
            }
            else {
              url = new URL(getParameter("URL_SERVLET") + "servlet/SrvCentApp?" +
                            dtParametros.toURL());
            }

            //url = new URL(getParameter("URL_SERVLET") + "servlet/SrvCentApp?" + dtParametros.toURL());
            getAppletContext().showDocument(url, "main");
            break;
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
        showError(ex.getMessage());
      } // Fin try .. catch

    } // if(instancia de UmenuItem)
  } // Fin actionPerformed()

} // endclass AppMenu
