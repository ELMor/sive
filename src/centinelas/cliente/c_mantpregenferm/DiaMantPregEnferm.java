package centinelas.cliente.c_mantpregenferm;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CInicializar;
import centinelas.cliente.c_componentes.CPnlCodigoExt;
import centinelas.cliente.c_componentes.ContCPnlCodigoExt;
import centinelas.cliente.c_comuncliente.nombreservlets;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class DiaMantPregEnferm
    extends CDialog
    implements CInicializar, ContCPnlCodigoExt {

  final String imgCancelar = "images/cancelar.gif";
  final String imgAceptar = "images/aceptar.gif";

  /*
    // modos de operaci�n de la ventana
    final public int modoALTA = 0;
    final public int modoMODIFICACION = 1;
    final public int modoBAJA = 2;
   */

  // modos de operaci�n de la ventana
  final public int modoMODIFICACION = 0;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;

  // parametros pasados al di�logo , y de retorno
  public Data dtDev = null;
  // gesti�n salir/grabar
  public boolean bAceptar = false;
  // modo de operaci�n
  public int modoOperacion;

  XYLayout xyLayout1 = new XYLayout();

  Label lblDescCorta = new Label();
  Label lblCdPregunta = new Label();
  Label lblDesLarga = new Label();
  TextField txtDesCorta = new TextField();
  CPnlCodigoExt pnlCdPregunta = null;
  TextField txtDesLarga = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores de eventos...
  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  //__________________________________________________________________

  public DiaMantPregEnferm(CApp a, int modoop, Data dt) {
    super(a);
    modoOperacion = modoop;
    QueryTool qtPregunta = new QueryTool();

    dtDev = dt;
    // longitud de los campos de texto y �rea...
    try {

      //configura el componente de pnlCdPregunta
      qtPregunta.putName("SIVE_PREGUNTA");
      qtPregunta.putType("CD_PREGUNTA", QueryTool.STRING);
      qtPregunta.putType("DS_PREGUNTA", QueryTool.STRING);
      // Parte del where
      qtPregunta.putWhereType("CD_TSIVE", QueryTool.STRING);
      qtPregunta.putWhereValue("CD_TSIVE", "C");
      qtPregunta.putOperator("CD_TSIVE", "=");

      pnlCdPregunta = new CPnlCodigoExt(a,
                                        this,
                                        qtPregunta,
                                        "CD_PREGUNTA",
                                        "DS_PREGUNTA",
                                        true,
                                        "Preguntas",
                                        "Preguntas",
                                        this);

      jbInit();
    }
    catch (Exception e) {
      app.showError("Error de conexi�n a base de datos");
      e.printStackTrace();
    }

  }

  //__________________________________________________________________

  void jbInit() throws Exception {

    this.setLayout(xyLayout1);
    this.setSize(550, 250);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno
    lblDescCorta.setText("Descripci�n corta: ");
    lblCdPregunta.setText("C�digo pregunta: ");
    lblDesLarga.setText("Descripci�n larga: ");

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xyLayout1.setHeight(900);
    xyLayout1.setWidth(675);

    this.add(lblDescCorta, new XYConstraints(MARGENIZQ, MARGENSUP, 140, ALTO));
    this.add(txtDesCorta,
             new XYConstraints(MARGENIZQ + 140 + INTERHOR, MARGENSUP, 304, ALTO));

    this.add(lblCdPregunta,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 140,
                               ALTO));
    this.add(pnlCdPregunta,
             new XYConstraints(MARGENIZQ + 140 + INTERHOR - 5,
                               MARGENSUP + ALTO + INTERVERT, 400, 34));
    this.add(lblDesLarga,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + 34 + 2 * INTERVERT,
                               140, ALTO));
    this.add(txtDesLarga,
             new XYConstraints(MARGENIZQ + 140 + INTERHOR,
                               MARGENSUP + ALTO + 34 + 2 * INTERVERT, 304, ALTO));

    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 140 + 304 - 2 * 88,
                               MARGENSUP + 2 * ALTO + 34 + 4 * INTERVERT, 88,
                               29));
    this.add(btnCancelar,
             new XYConstraints(MARGENIZQ + 140 + INTERHOR + 304 - 88,
                               MARGENSUP + 2 * ALTO + 34 + 4 * INTERVERT, 88,
                               29));

    //Habilitacion componentes invariables
    txtDesCorta.setEnabled(false);
    txtDesLarga.setEnabled(false);
    txtDesCorta.setEditable(false);
    txtDesLarga.setEditable(false);

    //m�todo temporal para poder ver los datos en el panel
    //pasados a trav�s de la applet
    verDatos();
    Inicializar(CInicializar.NORMAL);

    // ponemos el t�tulo
    this.setTitle("MODIFICACION");
  }

  //__________________________________________________________________

  public void rellenarDatos() {

    //Introducimos los datos en la pantalla
    txtDesCorta.setText(dtDev.getString("DS_CORTA"));
    pnlCdPregunta.setCodigo(dtDev);
    txtDesLarga.setText(dtDev.getString("DS_LARGA"));
  }

  //__________________________________________________________________

  // Pone los distintos campos en el estado que les corresponda
  // segun el modo.
  public void verDatos() {
    switch (modoOperacion) {

      case modoMODIFICACION:
        rellenarDatos();
        break;

    }
  }

  //______________________________________________________

  public void trasEventoEnCPnlCodigoExt() {
    if (pnlCdPregunta.getDatos() != null) {
      txtDesLarga.setText( (String) ( (Data) pnlCdPregunta.getDatos()).
                          getString("DS_PREGUNTA"));
    }
  }

  //______________________________________________________

  public String getDescCompleta(Data dat) {
    return ("");
  }

  //______________________________________________________

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;
    }
  }

  //______________________________________________________

  //Obligatorio rellenar todos los campos
  private boolean isDataValid() {
    if ( (pnlCdPregunta.getDatos() == null)
        || (txtDesCorta.getText().trim().length() == 0)) {
      this.getApp().showAdvise(
          "Debe completar todos los campos obligatoriamente");
      return false;
    }
    else {
      return true;
    }
  }

  //______________________________________________________

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {

        //Se pone el nuevo c�digo de pregunta asociado
        //Nota: dtdev apunta al Data de la propia lista
        dtDev.put("CD_PREGUNTA",
                  ( (Data) pnlCdPregunta.getDatos()).getString("CD_PREGUNTA"));
        dtDev.put("DS_PREGUNTA",
                  ( (Data) pnlCdPregunta.getDatos()).getString("DS_PREGUNTA"));
        dtDev.put("DS_LARGA", txtDesLarga.getText());
        if (dtDev.getString("DS_PREGUNTA").equals(dtDev.getString("DS_LARGA"))) {
          dtDev.put("CONTROL", this.getApp().getLibImagenes().get(imgAceptar));
        }
        else {
          dtDev.put("CONTROL", this.getApp().getLibImagenes().get(imgCancelar));
        }
        bAceptar = true;
        dispose();

      } //end if (isDataValid)
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  } //fin de btn_actionPerformed

  // Devuelve si se ha realizado la acci�n del bot�n o no
  public boolean bAceptar() {
    return bAceptar;
  }

  public void realizaOperacion(int j) {}

}

//______________________________________________________

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaMantPregEnferm adaptee;
  ActionEvent e;

  DialogActionAdapter(DiaMantPregEnferm adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
