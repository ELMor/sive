package centinelas.cliente.c_mantpregenferm;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppMantPregEnferm
    extends CApp
    implements CInicializar {

  PanMantPregEnferm pan = null;

  public AppMantPregEnferm() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Mantenimiento de preguntas fijas");
    pan = new PanMantPregEnferm(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }

}