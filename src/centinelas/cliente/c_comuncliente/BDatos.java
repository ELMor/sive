/* Clase: BDatos
 * Autor: Luis Rivera
 * Fecha Inicio: 13/01/2000
 * Descripcion: Usada para servir de interfaz con b. datos
 *  Permite crear servlets en cliente o acceder a los servlets del servidor
 * Modificaciones:
 *  JMT (18/01/2000): adici�n de las funciones execSQLBloqueo() y
 *  LRG (20/01/2000): Servlets vienen de fichero : m�todo ejecutaSQL
 *
 *
 **/

package centinelas.cliente.c_comuncliente;

import capp2.CApp;
import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

//import centinelas.debug.c_ServletsDebug.ListaDeServlets;

public class BDatos {

  public static boolean todoREAL = true;
  public static boolean todoDEBUG = false;

  public BDatos() {
  }

  //__________________________________________________________________________________
  /* execSQL:
   * Ejecuta una sentencia SQL conect�ndose al servidor Web
   */
  public static Lista execSQL(CApp app, String strURL, int modoServlet,
                              Lista parametros) throws Exception {

    Lista result = null;

    try {
      // consulta el servidor
      app.getStub().setUrl(strURL);
      result = (Lista) app.getStub().doPost(modoServlet, parametros);

      // error en el servlet
    }
    catch (Exception ex) {
      //Muestra mensaje que viene del Servidor
      //(Nota: En SrvQueryTool pone Error inesperado)
      app.trazaLog(ex);
      // Muestra di�logo con mensaje
      app.showError(ex.getMessage());
      // Relanzamiento de la excepcion
      throw (ex);
    }
    return result;
  }

  //__________________________________________________________________________________
  /* execSQLBloqueo:
   * Ejecuta una sentencia SQL en el servidor: tiene en cuenta el bloqueo
   */
  public static Lista execSQLBloqueo(CApp app, String strURL, int modoServlet,
                                     Lista parametros, QueryTool qtBlock,
                                     Data dtBlock) throws Exception {

    Lista result = null;

    try {
      // consulta el servidor
      app.getStub().setUrl(strURL);
      result = (Lista) app.getStub().doPost(modoServlet, parametros, qtBlock,
                                            dtBlock, app);

      // error en el servlet
    }
    catch (Exception ex) {
      //Muestra mensaje que viene del Servidor
      //(Nota: En SrvQueryTool pone Error inesperado)
      app.trazaLog(ex);
      // Muestra di�logo con mensaje
      app.showError(ex.getMessage());
      // Relanzamiento de la excepcion
      throw (ex);
    }
    return result;
  }

  //__________________________________________________________________________________

  /* execSQLDebug:
   * Ejecuta una sentencia SQL
   * Diferencia con el otro m�todo: aqu� se pasa el Servlet (el objeto) en vez de la URL donde se localiza
   */
  public static Lista execSQLDebug(CApp app, DBServlet servlet, int modoServlet,
                                   Lista parametros) throws Exception {

    Lista result = null;

    try {

      //Indica como conectarse a la b.datos
      servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
      //Se conecta a b.datos y consulta
      result = servlet.doDebug(modoServlet, parametros);

      // error en el servlet
    }
    catch (Exception ex) {
      //Muestra mensaje que viene del Servidor
      //(Nota: En SrvQueryTool pone Error inesperado)
      app.trazaLog(ex);
      // Muestra di�logo con mensaje
      app.showError(ex.getMessage());
      // Relanzamiento de la excepcion
      throw (ex);
    }
    return result;
  }

  //__________________________________________________________________________________

  /* execSQLDebugBloqueo:
   * Ejecuta una sentencia SQL en el servidor: tiene en cuenta el bloqueo
   * Diferencia con el otro m�todo: aqu� se pasa el Servlet (el objeto) en vez de la URL donde se localiza
   */
  public static Lista execSQLDebugBloqueo(CApp app, DBServlet servlet,
                                          int modoServlet, Lista parametros) throws
      Exception {
    Lista result = null;
    try {

      //Indica como conectarse a la b.datos
      servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
      //Se conecta a b.datos y consulta
      result = servlet.doDebug(modoServlet, parametros);

      // error en el servlet
    }
    catch (Exception ex) {
      //Muestra mensaje que viene del Servidor
      //(Nota: En SrvQueryTool pone Error inesperado)
      app.trazaLog(ex);
      // Muestra di�logo con mensaje
      app.showError(ex.getMessage());
      // Relanzamiento de la excepcion
      throw (ex);
    }
    return result;
  }

  //__________________________________________________________________________________
  /* execSQL:
   * Ejecuta una sentencia SQL conect�ndose al servidor Web o desde cliente, segun parametro debug
   */

  public static Lista ejecutaSQL(boolean debug, CApp app, String strURL,
                                 int modoServlet, Lista parametros) throws
      Exception {
    //Si se debe conectar siempre al Servidor Web (Por ejemplo :Se ense�a a la ICM )
    if (todoREAL == true) {
      return execSQL(app, strURL, modoServlet, parametros);
    }
    //Si se debe poner en debug (ej. Serv Web caido)
    else if (todoDEBUG == true) {
      return null;
      // return execSQLDebug(app,ListaDeServlets.getServlet(strURL), modoServlet, parametros);
    }

    //En resto de casos depende de par�metro debug de esta llamada
    else {
      if (debug == false) {
        return execSQL(app, strURL, modoServlet, parametros);
      }
      else {
        return null;
        //  return execSQLDebug(app,ListaDeServlets.getServlet(strURL), modoServlet, parametros);
      }
    } //else

  } //Funcion

}
