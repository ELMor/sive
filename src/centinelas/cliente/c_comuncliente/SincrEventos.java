/**
 * Clase: SincrEventos
 * Paquete: centinelas.cliente.c_comuncliente
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 14/01/2000
 * Analisis Funcional:
 * Descripcion: Funciones para la sincronizaci�n de eventos
 *   Uso: Un objeto para cada panel/di�logo.
 */

package centinelas.cliente.c_comuncliente;

import capp2.CInicializar;

public class SincrEventos {

  public SincrEventos() {
  }

  private boolean sinBloquear = true;
  private int modoAnterior;

  // Bloqueo MUTEX mediante la variable "sinBloquear"
  public synchronized boolean bloquea(int mAnt, CInicializar adaptee) {
    if (sinBloquear) {
      // No hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = mAnt;
      adaptee.Inicializar(CInicializar.ESPERA);

      return true;
    }
    else {
      // Ya est� bloqueado
      return false;
    }
  } // Fin bloquea()

  // Este m�todo desbloquea el sistema
  public synchronized void desbloquea(CInicializar CIni) {
    sinBloquear = true;
    CIni.Inicializar(modoAnterior);
  } // Fin desbloquea()

} // endclass SincrEventos
