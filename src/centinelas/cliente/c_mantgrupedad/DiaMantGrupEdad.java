//Di�logo para dar de alta un grupo de edad

package centinelas.cliente.c_mantgrupedad;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CCodigo;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CTexto;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class DiaMantGrupEdad
    extends CDialog
    implements CInicializar {
  // modos de operaci�n de la ventana
  final public int modoALTA = 0;

  // parametros pasados al di�logo
  public Data dtDev = null;

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  // modo de operaci�n
  public int modoOperacion;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  XYLayout xyLayout1 = new XYLayout();

  Label lblGrupo = new Label();
  Label lblDesc = new Label();

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  CCodigo txtCodGrup = new CCodigo(3);
  CTexto txtDescGrup = new CTexto(40);

  // Escuchadores de eventos...
  DialogActionAdapter1 actionAdapter = new DialogActionAdapter1(this);

  public DiaMantGrupEdad(CApp a, int modoop, Data dt) {
    super(a);
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(500, 170);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno
    txtCodGrup.setBackground(new Color(255, 255, 150));
    txtDescGrup.setBackground(new Color(255, 255, 150));
    txtCodGrup.setText("");
    txtDescGrup.setText("");
    lblGrupo.setText("C�digo de grupo:");
    lblDesc.setText("Descripci�n:");

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xyLayout1.setHeight(450);
    xyLayout1.setWidth(450);

    this.add(lblGrupo, new XYConstraints(MARGENIZQ, MARGENSUP, 94, ALTO));
    this.add(txtCodGrup,
             new XYConstraints(MARGENIZQ + 94 + INTERHOR, MARGENSUP, 50, ALTO));

    this.add(lblDesc,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 90,
                               ALTO));
    this.add(txtDescGrup,
             new XYConstraints(MARGENIZQ + 94 + INTERHOR,
                               MARGENSUP + ALTO + INTERVERT, 350, ALTO));

    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 94 + 350 - 2 * 88,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 88, 29));
    this.add(btnCancelar,
             new XYConstraints(MARGENIZQ + 94 + INTERHOR + 350 - 88,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 88, 29));

    setTitle("ALTA");
  }

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
    }
  }

  //Obligatorio rellenar todos los campos correctamente
  private boolean isDataValid() {
    boolean b = false;
    if ( (txtCodGrup.getText().equals("")) || (txtDescGrup.getText().equals(""))) {
      this.getApp().showAdvise("Debe introducir todos los datos");
      return false;
    }
    else {
      return true;
    }
  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        Inicializar(CInicializar.ESPERA);
        try {
          QueryTool qtAlta = new QueryTool();
          qtAlta.putName("SIVE_GRUPO_EDAD_RMC");
          qtAlta.putType("CD_GEDAD", QueryTool.STRING);
          qtAlta.putValue("CD_GEDAD", txtCodGrup.getText().trim());
          qtAlta.putType("DS_GEDAD", QueryTool.STRING);
          qtAlta.putValue("DS_GEDAD", txtDescGrup.getText().trim());

          // realiza la consulta al servlet
          this.getApp().getStub().setUrl(servlet);
          vFiltro.addElement(qtAlta);
          vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet, 3, vFiltro);
          bAceptar = true;
          dispose();
          // error en el servlet
        }
        catch (Exception ex) {
          this.getApp().trazaLog(ex);
          this.getApp().showError(ex.getMessage());
          bAceptar = false;
          dispose();
        }
        Inicializar(CInicializar.NORMAL);
      } //end if (isDataValid)
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  } //fin de btn_actionPerformed

  // Devuelve si se ha realizado la acci�n del bot�n o no
  public boolean bAceptar() {
    return bAceptar;
  }
}

class DialogActionAdapter1
    implements java.awt.event.ActionListener, Runnable {
  DiaMantGrupEdad adaptee;
  ActionEvent e;

  DialogActionAdapter1(DiaMantGrupEdad adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
