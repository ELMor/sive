//Applet de mantenimiento de grupos de edad
package centinelas.cliente.c_mantgrupedad;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppMantGrupEdad
    extends CApp
    implements CInicializar {

  PanMantGrupEdad pan = null;

  public AppMantGrupEdad() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Mantenimiento de grupos de edades para denominadores");
    pan = new PanMantGrupEdad(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }
}
