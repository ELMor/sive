//Di�logo con todos los datos de un grupo de edad, incluyendo la la lista de sus angos de edad

package centinelas.cliente.c_mantgrupedad;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.TextEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CTexto;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class DiaMantRangEdad
    extends CDialog
    implements CInicializar, CFiltro {

  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // parametros pasados al di�logo
  public Data dtDev = null;

  //par�metros devueltos por el di�logo
  public Data dtDevuelto = new Data();

  public boolean bAceptar = false;
  //indica si descripci�n ha sido modificada
  boolean modDesc = false;
  boolean noExisteRango = false;

  // modo de operaci�n
  public int modoOperacion;

  Lista lClm = new Lista();
  Lista lBaja = new Lista();

  XYLayout xyLayout1 = new XYLayout();

  Label lblGrupo = new Label();
  Label lblCodGrupo = new Label();

  CTexto txtDescGrupo = new CTexto(40);

  CListaMantenimiento clmRangEdad = null;

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  Lista lBd = new Lista();
  Lista lGedad = new Lista();

  // Escuchadores de eventos...
  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  public DiaMantRangEdad(CApp a, int modoop, Data dt) {
    super(a);
    modoOperacion = modoop;
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    QueryTool qt = new QueryTool();

    dtDev = dt;
    // longitud de los campos de texto y �rea...
    try {
      vBotones.addElement(new CBoton("",
                                     "images/alta2.gif",
                                     "Nuevo rango de edad",
                                     false,
                                     false));

      vBotones.addElement(new CBoton("",
                                     "images/modificacion2.gif",
                                     "Modificar rango de edad",
                                     true,
                                     true));

      vBotones.addElement(new CBoton("",
                                     "images/baja2.gif",
                                     "Borrar rango de edad",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("Descripci�n del rango",
                                      "370",
                                      "DS_DIST_EDAD"));

      vLabels.addElement(new CColumna("L�mite inferior",
                                      "115",
                                      "DIST_EDAD_I"));

      vLabels.addElement(new CColumna("L�mite superior",
                                      "115",
                                      "DIST_EDAD_F"));

      clmRangEdad = new CListaMantenimiento(a,
                                            vLabels,
                                            vBotones,
                                            this,
                                            this,
                                            250,
                                            640);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    this.setLayout(xyLayout1);
    this.setSize(670, 370);

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);
    txtDescGrupo.addTextListener(new DiaMantRangEdad_txtDescGrupo_textAdapter(this));

    lblGrupo.setText("Grupo de edad:");
    lblCodGrupo.setText("");

    xyLayout1.setHeight(900);
    xyLayout1.setWidth(675);

    this.add(lblGrupo, new XYConstraints(MARGENIZQ + 3, MARGENSUP, 100, ALTO));
    this.add(lblCodGrupo,
             new XYConstraints(MARGENIZQ + 100 + INTERVERT, MARGENSUP, 40, ALTO));
    this.add(txtDescGrupo,
             new XYConstraints(MARGENIZQ + 2 * 100 + 5 + 40, MARGENSUP, 260,
                               ALTO));
    this.add(clmRangEdad,
             new XYConstraints(MARGENIZQ - DESFPANEL,
                               MARGENSUP + ALTO + 2 * INTERVERT, 650, 230));
    this.add(btnAceptar,
             new XYConstraints(552 - 88 - INTERVERT,
                               MARGENSUP + ALTO + 3 * INTERVERT + 230 - 5, 88,
                               29));
    this.add(btnCancelar,
             new XYConstraints(552, MARGENSUP + 3 * INTERVERT + ALTO + 230 - 5,
                               88, 29));

    verDatos();
    buscarRangos();
    // ponemos el t�tulo

    switch (modoOperacion) {
      case modoMODIFICACION:
        this.setTitle("MODIFICACION");
        break;
      case modoBAJA:
        this.setTitle("BAJA");
        break;
    }
  }

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoMODIFICACION:
            this.setEnabled(true);
            break;
          case modoBAJA:
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        break;
    }
  }

  void colocarEnLista(Data dtColocar) {
    boolean encontrado = false;
    int edadInf = Integer.parseInt(dtColocar.getString("DIST_EDAD_I"));

    for (int i = 0; i < lClm.size() && encontrado == false; i++) {
      int edadTabla = Integer.parseInt( ( (Data) lClm.elementAt(i)).getString(
          "DIST_EDAD_I"));
      if (edadTabla > edadInf) {
        lClm.insertElementAt(dtColocar, i);
        encontrado = true;
      }
    }
    if (encontrado == false) {
      lClm.addElement(dtColocar);
    }
  }

  //Obligatorio rellenar todos los campos
  public void realizaOperacion(int j) {
    boolean op = false;
    DiaMantRang1 di = null;
    Data dMantRang = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:
        Lista lAlta = new Lista();
        lAlta = clmRangEdad.getLista();
        dMantRang.put("CD_GEDAD", dtDev.getString("CD_GEDAD"));
        di = new DiaMantRang1(this.getApp(), ALTA, dMantRang);
        di.show();
        // a�adir el nuevo elem a la lista
        if (di.bAceptar) {
          Data dtAlta = di.blDevuelto();
          colocarEnLista(dtAlta);
          clmRangEdad.setPrimeraPagina(lClm);
        }
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        dMantRang = clmRangEdad.getSelected();
        //si existe alguna fila seleccionada
        if (dMantRang != null) {
          int ind = clmRangEdad.getSelectedIndex();
          di = new DiaMantRang1(this.getApp(), MODIFICACION, dMantRang);
          di.show();
          //Tratamiento de bAceptar para Cancelar
          if (di.bAceptar) {
            Data dtModif = di.blDevuelto();
            lClm.removeElementAt(ind);
            colocarEnLista(dtModif);
            clmRangEdad.setPrimeraPagina(lClm);
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dMantRang = clmRangEdad.getSelected();
        if (dMantRang != null) {
          int ind = clmRangEdad.getSelectedIndex();
          di = new DiaMantRang1(this.getApp(), BAJA, dMantRang);
          di.show();
          if (di.bAceptar) {
            Data dtBaja = di.blDevuelto();
            lClm.removeElementAt(ind);
            if (dtBaja.getString("IT_OPERACION").equals("2")) {
              lBaja.addElement(dtBaja);
            }
            clmRangEdad.setPrimeraPagina(lClm);
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
        }
        break;

    }
  }

  private boolean isDataValid() {
    if (txtDescGrupo.getText().equals("")) {
      this.getApp().showAdvise(
          "Debe introducir una descripci�n para este grupo");
      return false;
    }
    else {
      return true;
    }
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    Inicializar(CInicializar.ESPERA);

    QueryTool qtInd = new QueryTool();

    qtInd.putName("SIVE_DISTR_GRUPO_EDAD_RMC");
    qtInd.putType("CD_GEDAD", QueryTool.STRING);
    qtInd.putType("DIST_EDAD_I", QueryTool.INTEGER);
    qtInd.putType("DIST_EDAD_F", QueryTool.INTEGER);
    qtInd.putType("DS_DIST_EDAD", QueryTool.STRING);

    qtInd.putWhereType("CD_GEDAD", QueryTool.STRING);
    qtInd.putWhereValue("CD_GEDAD", dtDev.getString("CD_GEDAD"));
    qtInd.putOperator("CD_GEDAD", "=");

    qtInd.addOrderField("DIST_EDAD_I");

    try {
      this.getApp().getStub().setUrl(servlet);
      p.addElement(qtInd);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() == 0) {
        noExisteRango = true;
      }
      else {
        for (int m = 0; m < p1.size(); m++) {
          ( (Data) p1.elementAt(m)).put("IT_BD", "S");
          ( (Data) p1.elementAt(m)).put("IT_OPERACION", "3");
        } //end for
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  public Lista siguientePagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    Inicializar(CInicializar.ESPERA);

    QueryTool qtInd = new QueryTool();

    qtInd.putName("SIVE_DISTR_GRUPO_EDAD_RMC");
    qtInd.putType("CD_GEDAD", QueryTool.STRING);
    qtInd.putType("DIST_EDAD_I", QueryTool.INTEGER);
    qtInd.putType("DIST_EDAD_F", QueryTool.INTEGER);
    qtInd.putType("DS_DIST_EDAD", QueryTool.STRING);

    qtInd.putWhereType("CD_GEDAD", QueryTool.STRING);
    qtInd.putWhereValue("CD_GEDAD", dtDev.getString("CD_GEDAD"));
    qtInd.putOperator("CD_GEDAD", "=");

    qtInd.addOrderField("DIST_EDAD_I");

    try {
      this.getApp().getStub().setUrl(servlet);
      p.addElement(qtInd);
      p.setTrama(this.clmRangEdad.getLista().getTrama());
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() != 0) {
        for (int m = 0; m < p1.size(); m++) {
          ( (Data) p1.elementAt(m)).put("IT_BD", "S");
          ( (Data) p1.elementAt(m)).put("IT_OPERACION", "3");
        } //end for
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  void buscarRangos() {
    Inicializar(CInicializar.ESPERA);
    clmRangEdad.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
    lClm = clmRangEdad.getLista();
  }

  QueryTool realizarUpdateDesc() {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_GRUPO_EDAD_RMC");
    qtModif.putType("DS_GEDAD", QueryTool.STRING);
    qtModif.putValue("DS_GEDAD", txtDescGrupo.getText().trim());

    qtModif.putWhereType("CD_GEDAD", QueryTool.STRING);
    qtModif.putWhereValue("CD_GEDAD", dtDev.getString("CD_GEDAD"));
    qtModif.putOperator("CD_GEDAD", "=");

    return qtModif;
  } //end realizarupdatedesc

  QueryTool realizarInsertGrup(Data dtUpd) {
    QueryTool qtIns = new QueryTool();
    qtIns.putName("SIVE_DISTR_GRUPO_EDAD_RMC");
    qtIns.putType("CD_GEDAD", QueryTool.STRING);
    qtIns.putValue("CD_GEDAD", dtDev.getString("CD_GEDAD"));
    qtIns.putType("DIST_EDAD_I", QueryTool.INTEGER);
    qtIns.putValue("DIST_EDAD_I", dtUpd.getString("DIST_EDAD_I"));
    qtIns.putType("DIST_EDAD_F", QueryTool.INTEGER);
    qtIns.putValue("DIST_EDAD_F", dtUpd.getString("DIST_EDAD_F"));
    qtIns.putType("DS_DIST_EDAD", QueryTool.STRING);
    qtIns.putValue("DS_DIST_EDAD", dtUpd.getString("DS_DIST_EDAD"));

    return qtIns;
  }

  QueryTool realizarDeleteGrup(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_DISTR_GRUPO_EDAD_RMC");

    qtModif.putWhereType("CD_GEDAD", QueryTool.STRING);
    qtModif.putWhereValue("CD_GEDAD", dtUpd.getString("CD_GEDAD"));
    qtModif.putOperator("CD_GEDAD", "=");
    return qtModif;
  }

  QueryTool realizarDeleteUnGrup(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_DISTR_GRUPO_EDAD_RMC");

    qtModif.putWhereType("CD_GEDAD", QueryTool.STRING);
    qtModif.putWhereValue("CD_GEDAD", dtUpd.getString("CD_GEDAD"));
    qtModif.putOperator("CD_GEDAD", "=");

    qtModif.putWhereType("DIST_EDAD_I", QueryTool.INTEGER);
    qtModif.putWhereValue("DIST_EDAD_I", dtUpd.getString("DIST_EDAD_I"));
    qtModif.putOperator("DIST_EDAD_I", "=");

    qtModif.putWhereType("DIST_EDAD_F", QueryTool.INTEGER);
    qtModif.putWhereValue("DIST_EDAD_F", dtUpd.getString("DIST_EDAD_F"));
    qtModif.putOperator("DIST_EDAD_F", "=");

    return qtModif;
  }

  //Valida q los rangos introducidos sean exhaustivos y no se superponen
  boolean validarRangos() {
    boolean b = true;
    boolean salir = false;
    for (int i = 1; i < lClm.size() && salir == false; i++) {
      int edadInfActual = Integer.parseInt( ( (Data) lClm.elementAt(i)).
                                           getString("DIST_EDAD_I"));
      int edadSupAnterior = Integer.parseInt( ( (Data) lClm.elementAt(i - 1)).
                                             getString("DIST_EDAD_F"));
      int edadSupActual = Integer.parseInt( ( (Data) lClm.elementAt(i)).
                                           getString("DIST_EDAD_F"));
      if ( (edadInfActual != edadSupAnterior + 1) ||
          (edadSupActual < edadInfActual)) {
        this.getApp().showAdvise(
            "El rango introducido se superpone o no es exhaustivo.");
        b = false;
        salir = true;
      }
      else {
        b = true;
      }
    } //end for
    return b;
  }

  void btn_actionPerformed(ActionEvent e) {
    final String srvTrans = nombreservlets.strSERVLET_TRANSACCION;
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        switch (modoOperacion) {
          case 1:

            // ---- MODIFICAR ----
            if (validarRangos()) {
              Inicializar(CInicializar.ESPERA);
              //si se ha modificado la descripci�n, modifico
              if (modDesc) {
                QueryTool qtUpdDesc = realizarUpdateDesc();
                Data dtUpdate = new Data();
                dtUpdate.put("4", qtUpdDesc);
                lBd.addElement(dtUpdate);
              }

              //recorro la lista de bajas
              for (int j = 0; j < lBaja.size(); j++) {
                Data dtDelete = (Data) lBaja.elementAt(j);
                QueryTool qtBaja = realizarDeleteUnGrup(dtDelete);
                Data dtBaja = new Data();
                dtBaja.put("5", qtBaja);
                lBd.addElement(dtBaja);
              }

              //relleno la lista de lGedad:grupos a borrar y volver a insertar
              for (int i = 0; i < lClm.size(); i++) {
                Data dtClm = (Data) lClm.elementAt(i);
                if (dtClm.getString("IT_OPERACION").equals("1")) {
                  String cdGedad = dtClm.getString("CD_GEDAD");
                  boolean salir = false;
                  for (int s = 0; s < lGedad.size() && salir == false; s++) {
                    if ( ( (Data) lGedad.elementAt(s)).getString("CD_GEDAD").
                        equals(cdGedad)) {
                      salir = true;
                    }
                  }
                  //si no existe en la lista lGedad lo pongo y en lClm vale 4
                  if (salir == false) {
                    lGedad.addElement(dtClm);
                    for (int m = 0; m < lClm.size(); m++) {
                      if ( ( (Data) lClm.elementAt(m)).getString("CD_GEDAD").
                          equals(cdGedad)) {
                        ( (Data) lClm.elementAt(m)).put("IT_OPERACION", "4");
                      }
                    }
                  }
                } //end if op=1
              } //end for

              //recorro lGedad:mando borrar de la bd
              for (int i = 0; i < lGedad.size(); i++) {
                Data dtGedad = (Data) lGedad.elementAt(i);
                String cdGedad = dtGedad.getString("CD_GEDAD");
                QueryTool qtBaja = realizarDeleteGrup(dtGedad);
                Data dtBaja = new Data();
                dtBaja.put("5", qtBaja);
                lBd.addElement(dtBaja);
              } //end for

              for (int i = 0; i < lClm.size(); i++) {
                Data dtClm = (Data) lClm.elementAt(i);
                if ( (dtClm.getString("IT_OPERACION").equals("0"))
                    || (dtClm.getString("IT_OPERACION").equals("4"))) {
                  QueryTool qtAlta = realizarInsertGrup(dtClm);
                  Data dtAlta = new Data();
                  dtAlta.put("3", qtAlta);
                  lBd.addElement(dtAlta);
                }
              } //end for

              try {
                BDatos.ejecutaSQL(false, this.getApp(), srvTrans, 0, lBd);
                bAceptar = true;
                dispose();
                // error en el servlet
              }
              catch (Exception ex) {
                this.getApp().trazaLog(ex);
                this.getApp().showError(ex.getMessage());
                bAceptar = false;
                dispose();
              }
              Inicializar(CInicializar.NORMAL);
            } //end if validarRangos
            break;

          case 2:

            // ---- BORRAR -------
            //solo se elimina si no posee rangos asociados
            if (noExisteRango) {
              Inicializar(CInicializar.ESPERA);
              try {
                //No tiene notificaciones pendientes:borrado por delete
                QueryTool qtBorr = new QueryTool();

                // tabla de familias de productos
                qtBorr.putName("SIVE_GRUPO_EDAD_RMC");
                // campos que se seleccionan
                qtBorr.putWhereType("CD_GEDAD", QueryTool.STRING);
                qtBorr.putWhereValue("CD_GEDAD", dtDev.getString("CD_GEDAD"));
                qtBorr.putOperator("CD_GEDAD", "=");

                vFiltro.addElement(qtBorr);
                this.getApp().getStub().setUrl(servlet);
                vPetic = BDatos.ejecutaSQL(false, this.getApp(), servlet, 5,
                                           vFiltro);
                bAceptar = true;
                dispose();
              }
              catch (Exception ex) {
                this.getApp().trazaLog(ex);
                this.getApp().showError(ex.getMessage());
                bAceptar = false;
                dispose();
              }
              Inicializar(CInicializar.NORMAL);
              break;
            }
            else {
              this.getApp().showAdvise("Baja no realizada. Contiene rangos");
              bAceptar = false;
              dispose();
              break;
            }
        } //end if switch
      } //end if (isDataValid)
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  } //fin de btn_actionPerformed

  public boolean bAceptar() {
    return bAceptar;
  }

  public void verDatos() {
    lblCodGrupo.setText(dtDev.getString("CD_GEDAD"));
    txtDescGrupo.setText(dtDev.getString("DS_GEDAD"));
  }

  void txtDescGrupo_textValueChanged(TextEvent e) {
    if (! (dtDev.getString("DS_GEDAD").equals(txtDescGrupo.getText().trim()))) {
      modDesc = true;
    }
    else {
      modDesc = false;
    }
  }
}

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaMantRangEdad adaptee;
  ActionEvent e;

  DialogActionAdapter(DiaMantRangEdad adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}

class DiaMantRangEdad_txtDescGrupo_textAdapter
    implements java.awt.event.TextListener {
  DiaMantRangEdad adaptee;

  DiaMantRangEdad_txtDescGrupo_textAdapter(DiaMantRangEdad adaptee) {
    this.adaptee = adaptee;
  }

  public void textValueChanged(TextEvent e) {
    adaptee.txtDescGrupo_textValueChanged(e);
  }
}
