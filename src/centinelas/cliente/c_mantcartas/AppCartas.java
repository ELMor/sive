package centinelas.cliente.c_mantcartas;

import capp2.CApp;

public class AppCartas
    extends CApp {

  public AppCartas() {
    super();
  }

  public void init() {
    super.init();
    setTitulo("Mantenimiento de modelos de cartas");
    VerPanel("", new PanMantCartas(this));
  }
}