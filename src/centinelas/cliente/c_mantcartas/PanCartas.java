package centinelas.cliente.c_mantcartas;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CFecha;
import capp2.CInicializar;
import capp2.CPanel;
import capp2.CPnlCodigo;
import sapp2.Format;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

/**
 * Panel a trav�s del que se realizan los mantenimientos
 * de  modelos de cartas.
 * @autor PDP
 * @version 1.0
 */

public class PanCartas
    extends CPanel
    implements CInicializar {
//  BorderLayout borderLayout1 = new BorderLayout();

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  // gesti�n salir/grabar
  public boolean bAceptar = false;

  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  DialogActionAdapter1 ActionAdapter = new DialogActionAdapter1(this);
  DialogchkitemAdapter chkItemAdapter = new DialogchkitemAdapter(this);

  Label lblModelo = new Label();
  CPnlCodigo pnlModelo = null;
  Label lblBaja = new Label();
  Checkbox chBaja = new Checkbox();
  Label lblFecBaj = new Label();
  CFecha txtFecBaj = new CFecha();
  Label lblMotivo = new Label();
  TextField txtMotivo = new TextField();
  XYLayout xYLayout1 = new XYLayout();

  public PanCartas(CApp a) {
    super(a);

    QueryTool2 qt = new QueryTool2();
    QueryTool qtP = new QueryTool();

    try {

      this.app = a;

      // configura el componente de modelos
      qt.putName("SIVE_PLANTILLA_RMC");
      qt.putType("CD_PLANTILLA", QueryTool.STRING);
      qt.putType("DS_PLANTILLA", QueryTool.STRING);

      // panel
      pnlModelo = new CPnlCodigo(a,
                                 this,
                                 qt,
                                 "CD_PLANTILLA",
                                 "DS_PLANTILLA",
                                 true,
                                 "Tipo de modelos de plantilla",
                                 "Tipo de modelos de plantilla");

      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setSize(556, 237);
    //this.setTitle("Mantenimiento de modelos de cartas");

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    lblModelo.setText("Modelo de carta");
    lblBaja.setText("Baja");
    lblFecBaj.setText("Fecha de baja:");
    lblMotivo.setText("Motivo baja:");
    xYLayout1.setHeight(232);
    xYLayout1.setWidth(533);

    this.setLayout(xYLayout1);
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));
    txtFecBaj.setBackground(new Color(255, 255, 150));
    txtMotivo.setBackground(new Color(255, 255, 150));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(ActionAdapter);
    btnCancelar.addActionListener(ActionAdapter);
    chBaja.addItemListener(chkItemAdapter);

    btnAceptar.setEnabled(true);
    btnCancelar.setEnabled(true);

    this.add(lblModelo, new XYConstraints(25, 20, 97, -1));
    this.add(pnlModelo, new XYConstraints(127, 15, 266, 30));
    this.add(lblBaja, new XYConstraints(26, 60, -1, -1));
    this.add(chBaja, new XYConstraints(72, 60, -1, -1));
    this.add(txtFecBaj, new XYConstraints(127, 98, 86, -1));
    this.add(lblMotivo, new XYConstraints(251, 98, 70, -1));
    this.add(lblFecBaj, new XYConstraints(26, 98, -1, -1));
    this.add(txtMotivo, new XYConstraints(325, 99, 190, -1));
    this.add(btnCancelar, new XYConstraints(444, 172, -1, -1));
    this.add(btnAceptar, new XYConstraints(383, 172, -1, -1));

    Inicializar(CInicializar.NORMAL);
  }

  public void Inicializar() {}

  public void Inicializar(int i) {
    // Data d = null;
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        pnlModelo.setEnabled(false);
        txtFecBaj.setEnabled(false);
        chBaja.setEnabled(false);
        txtMotivo.setEnabled(false);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);

        break;

        // modo normal
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
    }
  }

  void btn_actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        Inicializar(CInicializar.ESPERA);
        ActualizaPlantilla();
        this.getApp().showAdvise("Ha dado de baja el modelo");
        Inicializar(CInicializar.NORMAL);
        ///Se debe poner un mensaje que no indique que hemos dado de baja el reg
      }
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;

      //dispose();
    }
  }

  //Actualiza la entidad PLANTILLA_RMC
  public void ActualizaPlantilla() {
    final String servlet = "servlet/SrvQueryTool";
    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();

    try {
      QueryTool qt = new QueryTool();

      // tabla de familias de productos
      qt.putName("SIVE_PLANTILLA_RMC");

      // campos que se escriben
      qt.putType("FC_BAJA", QueryTool.DATE);
      qt.putType("IT_BAJA", QueryTool.STRING);
      qt.putType("DS_MOTIVO", QueryTool.STRING);

      // Valores de los campos
      qt.putValue("FC_BAJA", txtFecBaj.getText());
      qt.putValue("IT_BAJA", "S");
      qt.putValue("DS_MOTIVO", txtMotivo.getText());

      //
      qt.putWhereType("CD_PLANTILLA", QueryTool.STRING);
      qt.putWhereValue("CD_PLANTILLA", pnlModelo.getDatos().get("CD_PLANTILLA"));
      qt.putOperator("CD_PLANTILLA", "=");
      qt.putWhereType("DS_PLANTILLA", QueryTool.STRING);
      qt.putWhereValue("DS_PLANTILLA", pnlModelo.getDatos().get("DS_PLANTILLA"));
      qt.putOperator("DS_PLANTILLA", "=");

      vFiltro.addElement(qt);

      this.getApp().getStub().setUrl(servlet);
      vPetic = (Lista)this.getApp().getStub().doPost(4, vFiltro);
      //bAceptar = true;
      //dispose();
      // error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

  }

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean isDataValid() {

    String sMsg = null;
    boolean bCtrl1 = true;
    boolean bCtrl2 = true;
    boolean bCtrl3 = true;
    boolean bCtrl4 = true;
    boolean bCtrl5 = true;

    if (pnlModelo.getDatos() == null) {
      bCtrl1 = false;
      sMsg = "Debe cumplimentar el tipo de modelo de carta";
    }

    if (txtFecBaj.getText().length() == 0) {
      bCtrl2 = false;
      sMsg = "Debe cumplimentar la fecha";
    }
    else {
      txtFecBaj.ValidarFecha();
      if (txtFecBaj.getValid().equals("S")) {
        bCtrl3 = true;
      }
      else {
        sMsg = "Debe introducir una fecha correcta";
        bCtrl3 = false;
      }
    }

    if (txtMotivo.getText().length() == 0) {
      bCtrl4 = false;
      sMsg = "Debe cumplimentar motivo de baja";
    }

    if ( (bCtrl1 && bCtrl2 && bCtrl3 && bCtrl4) == false) {
      sMsg = "Debe cumplimentar los datos obligatorios";
    }

    if (sMsg == null) {
      return true;
    }
    else {
      this.getApp().showAdvise(sMsg);
      return false;
    }

  }

  // Devuelve si se ha pulsado aceptar o cancelar
  public boolean bAceptar() {
    return bAceptar;
  }

  public void chkResultado_itemStateChanged(ItemEvent e) {
    //Indicador de selecci�n
    int IndSel = 0;
    IndSel = e.getStateChange();

    // Fecha actual
    String sDate = Format.fechaActual();

    if (IndSel == 1) { //seleccionado
      txtFecBaj.setText(sDate);
    }
    else {
      txtFecBaj.setText("");
    }

  }

}

class DialogActionAdapter1
    implements java.awt.event.ActionListener, Runnable {
  PanCartas adaptee;
  ActionEvent e;

  DialogActionAdapter1(PanCartas adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}

// listener
class DialogchkitemAdapter
    implements java.awt.event.ItemListener {
  PanCartas adaptee;
  DialogchkitemAdapter(PanCartas adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkResultado_itemStateChanged(e);
  }
}
