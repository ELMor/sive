package centinelas.cliente.c_mantcartas;

import java.io.File;
import java.io.FileInputStream;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CCodigo;
import capp2.CDialog;
import capp2.CInicializar;
import capp2.CMessage;
import capp2.CTexto;
// Esta l�nea se debe comentar cuando quede corregido.
import centinelas.servidor.c_mantcartas.SrvMantCartas;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;

public class DiaMantCartas
    extends CDialog
    implements CInicializar {

  // modos de operaci�n de la ventana
  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  // parametros pasados al di�logo
  public Data dtDev = null;

  //par�metros devueltos por el di�logo
  public Data dtDevuelto = new Data();

  // gesti�n salir/grabar
  public boolean bAceptar = false;
  public int borrPto;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xyLayout1 = new XYLayout();

  Label lblcodpla = new Label();
  CCodigo txtCodPlantilla = null;
  CTexto cDescPlantilla = null;
  Checkbox chkBaja = new Checkbox();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores de eventos...
  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  //Applet
  CApp applet = null;

  byte[] arrayPlantilla; //Bytes extraidos del fichero
  TextField txtMotivoBaja = new TextField();
  Label lblMotivoBaja = new Label();
  Label lblBaja = new Label();

  //__________________________________________________________________

  //constructor del di�logo DiaMantCartas
  public DiaMantCartas(CApp a, int modoop, Data dt) {
    super(a);
    applet = a;
    dtDev = dt;
    modoOperacion = modoop;
    //Se crean comps
    txtCodPlantilla = new CCodigo(6);
    cDescPlantilla = new CTexto(40);
    try {

      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

  //__________________________________________________________________

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(454, 199);

    // carga las imagenes
    applet.getLibImagenes().put(imgAceptar);
    applet.getLibImagenes().put(imgCancelar);
    applet.getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno
    txtCodPlantilla.setBackground(new Color(255, 255, 150));
    txtCodPlantilla.setText("");
    cDescPlantilla.setBackground(new Color(255, 255, 150));
    cDescPlantilla.setText("");
    lblcodpla.setText("Plantilla:");

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(applet.getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    txtMotivoBaja.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        txtMotivoBaja_actionPerformed(e);
      }
    });
    lblMotivoBaja.setText("Motivo de baja:");
    lblBaja.setText("Baja");
    btnCancelar.setImage(applet.getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xyLayout1.setHeight(136);
    xyLayout1.setWidth(454);

    this.add(lblcodpla, new XYConstraints(8, 6, 55, 23));
    this.add(txtCodPlantilla, new XYConstraints(65, 6, 80, -1));
    this.add(cDescPlantilla, new XYConstraints(148, 6, 284, 23));
    this.add(chkBaja, new XYConstraints(6, 39, 21, -1));
    this.add(btnAceptar, new XYConstraints(278, 91, -1, -1));
    this.add(btnCancelar, new XYConstraints(367, 91, -1, -1));
    this.add(txtMotivoBaja, new XYConstraints(150, 39, 280, -1));
    this.add(lblMotivoBaja, new XYConstraints(63, 39, 87, -1));
    this.add(lblBaja, new XYConstraints(29, 39, 32, -1));

    //m�todo temporal para poder ver los datos en el panel
    //pasados a trav�s de la applet
    verDatos();
    // ponemos el t�tulo
    switch (modoOperacion) {
      case modoALTA:
        setTitle("ALTA");
        break;
      case modoMODIFICACION:
        this.setTitle("MODIFICACION");
        break;
      case modoBAJA:
        this.setTitle("BAJA");
        break;
    }
  }

  //__________________________________________________________________

  public Data recogerDatos() {

//    int resul=0;
    Data rec = new Data();
    rec.put("CD_PLANTILLA", txtCodPlantilla.getText());
    rec.put("DS_PLANTILLA", cDescPlantilla.getText());
    //Si el usuario ha indicado nombre fichero
    //Se introduce el array de bytes
    if (chkBaja.getState()) {
      rec.put("IT_BAJA", "S");
    }
    else {
      rec.put("IT_BAJA", "N");
    }
    rec.put("CD_OPE", applet.getParametro("COD_USUARIO"));
    if (tieneAvisos(txtCodPlantilla.getText())) {
      rec.put("TIENE_AVISOS", "S");
      rec.put("DS_MOTIVBAJA", txtMotivoBaja.getText());
    }
    else {
      rec.put("TIENE_AVISOS", "N");
    }

//    rec.put("FC_ULTACT","12/12/1999"); //******************

     return rec;

  }

  public void rellenarDatos() {

    //Introducimos los datos en la pantalla
    txtCodPlantilla.setText(dtDev.getString("CD_PLANTILLA"));
    cDescPlantilla.setText(dtDev.getString("DS_PLANTILLA"));
    if (dtDev.getString("IT_BAJA").equals("S")) {
      chkBaja.setState(true);
    }
    else {
      chkBaja.setState(false);
    }
    txtMotivoBaja.setText(dtDev.getString("DS_MOTIVBAJA"));
  }

  // Pone los distintos campos en el estado que les corresponda
  // segun el modo.
  public void verDatos() {
    switch (modoOperacion) {
      case modoALTA:
        txtCodPlantilla.setVisible(true);
        cDescPlantilla.setEnabled(true);
        chkBaja.setVisible(false);
        txtMotivoBaja.setVisible(false);
        lblMotivoBaja.setVisible(false);
        lblBaja.setVisible(false);
        break;

      case modoMODIFICACION:
        txtCodPlantilla.setVisible(true);
        txtCodPlantilla.setEnabled(false);
        chkBaja.setEnabled(false);
        txtMotivoBaja.setEnabled(false);
        //Se rellenan los datos en la pantalla
        rellenarDatos();
        break;

      case modoBAJA:

        //solo habilitados los botones de aceptar y cancelar
        this.setEnabled(false);
        txtCodPlantilla.setVisible(true);
        cDescPlantilla.setVisible(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        chkBaja.setVisible(false);
        lblBaja.setVisible(false);
        if (tieneAvisos(dtDev.getString("CD_PLANTILLA"))) {
          txtMotivoBaja.setEnabled(true);
          txtMotivoBaja.setVisible(true);
          lblMotivoBaja.setVisible(true);
        }
        else {
          txtMotivoBaja.setEnabled(false);
          txtMotivoBaja.setVisible(false);
          lblMotivoBaja.setVisible(false);
        }
        rellenarDatos();
        break;
    }
  }

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoALTA:
            this.setEnabled(true);
            chkBaja.setVisible(false);
            txtMotivoBaja.setVisible(false);
            lblMotivoBaja.setVisible(false);
            lblBaja.setVisible(false);
            break;

          case modoMODIFICACION:
            this.setEnabled(true);
            txtCodPlantilla.setVisible(true);
            txtCodPlantilla.setEnabled(false);
            chkBaja.setEnabled(false);
            txtMotivoBaja.setEnabled(false);
            break;

            // s�lo habilitado salir y grabar
          case modoBAJA:
            this.setEnabled(false);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            txtCodPlantilla.setVisible(true);
            cDescPlantilla.setVisible(true);
            chkBaja.setVisible(false);
            lblBaja.setVisible(false);
            if (tieneAvisos(dtDev.getString("CD_PLANTILLA"))) {
              txtMotivoBaja.setEnabled(true);
              txtMotivoBaja.setVisible(true);
              lblMotivoBaja.setVisible(true);
            }
            else {
              txtMotivoBaja.setEnabled(false);
              txtMotivoBaja.setVisible(false);
              lblMotivoBaja.setVisible(false);
            }
            break;
        }
        break;
    }
  }

  //Comprueba que est�n todos los datos
  //Si usuario ha elegido nombre de fichero lee bytes del fichero
  private boolean isDataValid() {
    CMessage msgBox = null;
    File miFichero = null;
    FileInputStream fiStream = null;
    int resul = 0;

    //Quita blancos en cajas de texto antes de hacer comprobaciones
    txtCodPlantilla.setText(txtCodPlantilla.getText().trim());
    cDescPlantilla.setText(cDescPlantilla.getText().trim());

    if ( (txtCodPlantilla.getText().length() == 0)
        || (cDescPlantilla.getText().length() == 0)) {
      applet.showAdvise("Faltan campos obligatorios");
      return false;
    }
    else {
      return true;
    }
  }

  //__________________________________________________________________

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {

    Lista vFiltro = new Lista();
    Lista vPetic = new Lista();

    final String servlet = "servlet/SrvMantCartas";

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        switch (modoOperacion) {

          // ----- ALTA -----------
          case 0:

            //Inicio el alta
            Inicializar(CInicializar.ESPERA);
            try {
              //Relleno los par�metros
              Data altaCarta = new Data();
              altaCarta = recogerDatos();
              // realiza la consulta al servlet
              this.applet.getStub().setUrl(servlet);
              vFiltro.addElement(altaCarta);
              // debug
              SrvMantCartas srv = new SrvMantCartas();

              // par�metros jdbc
              srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                  "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                  "sive_desa",
                  "sive_desa");
              vPetic = (Lista) srv.doDebug(1, vFiltro);

              //vPetic = (Lista) this.getApp().getStub().doPost(1, vFiltro);
              dtDevuelto = altaCarta;
              bAceptar = true;
              dispose();

              // error en el servlet
            }
            catch (Exception ex) {
              applet.trazaLog(ex);
              applet.showError(ex.getMessage());
              bAceptar = false;
              dispose();
            }
            Inicializar(CInicializar.NORMAL);
            break;

            // MODIFICACI�N.
          case 1:

            //Inicio la modificaci�n
            Inicializar(CInicializar.ESPERA);
            try {
              //Relleno los par�metros
              Data altaCarta = new Data();
              altaCarta = recogerDatos();
              // realiza la consulta al servlet
              this.applet.getStub().setUrl(servlet);
              vFiltro.addElement(altaCarta);
              // debug
              SrvMantCartas srv = new SrvMantCartas();

              // par�metros jdbc
              srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                  "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                  "sive_desa",
                  "sive_desa");
              vPetic = (Lista) srv.doDebug(2, vFiltro);

              //vPetic = (Lista) this.getApp().getStub().doPost(2, vFiltro);
              dtDevuelto = altaCarta;
              bAceptar = true;
              dispose();

              // error en el servlet
            }
            catch (Exception ex) {
              applet.trazaLog(ex);
              applet.showError(ex.getMessage());
              bAceptar = false;
              dispose();
            }
            Inicializar(CInicializar.NORMAL);
            break;

            // BAJA
          case 2:

            //Inicio la modificaci�n
            Inicializar(CInicializar.ESPERA);
            try {
              //Relleno los par�metros
              Data altaCarta = new Data();
              altaCarta = recogerDatos();
              if (!dtDev.getString("IT_BAJA").equals("S")) {
                // realiza la consulta al servlet
                this.applet.getStub().setUrl(servlet);
                vFiltro.addElement(altaCarta);
                /*                     // debug
                                      SrvMantCartas srv = new SrvMantCartas();
                                      // par�metros jdbc
                     srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                     "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                             "sive_desa",
                                             "sive_desa");
                                      vPetic =  (Lista)srv.doDebug(3, vFiltro);
                 */
                vPetic = (Lista)this.getApp().getStub().doPost(3, vFiltro);
                dtDevuelto = altaCarta;
                bAceptar = true;
                dispose();
              }
              else {
                this.getApp().showAdvise("Este registro est� ya dado de baja");
              }
            }
            catch (Exception ex) {
              applet.trazaLog(ex);
              applet.showError(ex.getMessage());
              bAceptar = false;
              dispose();
            }
            Inicializar(CInicializar.NORMAL);
            break;
        } //end if switch
      } //end if (isDataValid)
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }

  } //fin de btn_actionPerformed

  // Devuelve si se ha realizado la acci�n del bot�n o no
  public boolean bAceptar() {
    return bAceptar;
  }

  //Devuelve si el borrado se hace f�sico, por marca o no se hace
  public int borrPto() {
    return borrPto;
  }

  public Data devuelveData() {
    return dtDevuelto;
  }

  void txtMotivoBaja_actionPerformed(ActionEvent e) {

  }

  private boolean tieneAvisos(String plantilla) {
    boolean estado = false;
    Lista lParametros = new Lista();
    Lista lDevuelto = new Lista();
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_AVISOS");
    qt.putType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereType("CD_PLANTILLA", QueryTool.STRING);
    qt.putWhereValue("CD_PLANTILLA", plantilla);
    qt.putOperator("CD_PLANTILLA", "=");
    lParametros.addElement(qt);

    try {
      this.getApp().getStub().setUrl("servlet/SrvQueryTool");
      lDevuelto = (Lista)this.getApp().getStub().doPost(1, lParametros);
    }
    catch (Exception e) {
      this.getApp().trazaLog(e);
    }
    if (lDevuelto.size() > 0) {
      estado = true;
      borrPto = 1;
    }

    return estado;
  }

}

//__________________________________________________________________

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaMantCartas adaptee;
  ActionEvent e;

  DialogActionAdapter(DiaMantCartas adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
