//Di�logo que representa los �ndices de actividad de un m�dico notificador
package centinelas.cliente.c_mantindact;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import centinelas.cliente.c_componentes.ContCPnlCodigoExt;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.centbasicos.MedCent;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class DiaMantIndAct
    extends CDialog
    implements CInicializar, CFiltro, ContCPnlCodigoExt {

  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // parametros pasados al di�logo
  public MedCent dtDev = null;

  //par�metros devueltos por el di�logo
  public MedCent dtDevuelto = new MedCent();

  public boolean bAceptar = false;
  public int borrPto = 0;
  boolean noMensaje = false;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xyLayout1 = new XYLayout();

  Label lblPuntoCent = new Label();
  Label lblMedico = new Label();
  Label lblDesPto = new Label();
  Label lblDesMedico = new Label();
  Label lblEquipo = new Label();
  Label lblCentro = new Label();
  Label lblDesEquipo = new Label();
  Label lblDesCentro = new Label();

  CListaMantenimiento clmIndAct = null;

  ButtonControl btnSalir = new ButtonControl();

  // Escuchadores de eventos...
  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  public DiaMantIndAct(CApp a, int modoop, MedCent dt) {
    super(a);
    modoOperacion = modoop;
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    QueryTool qt = new QueryTool();

    dtDev = dt;
    // longitud de los campos de texto y �rea...
    try {
      qt.putName("SIVE_INDACTMC_MEDCEN");
      qt.putType("CD_INDRMC", QueryTool.STRING);
      qt.putType("NM_VINDRMC", QueryTool.STRING);

      // configura el hist�rico de medicos notificadores de punto centinela
      // botones
      vBotones.addElement(new CBoton("",
                                     "images/alta2.gif",
                                     "Nuevo �ndice de actividad",
                                     false,
                                     false));

      vBotones.addElement(new CBoton("",
                                     "images/modificacion2.gif",
                                     "Modificar �ndice de actividad",
                                     true,
                                     true));

      vBotones.addElement(new CBoton("",
                                     "images/baja2.gif",
                                     "Borrar �ndice de actividad",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("Indice de Actividad Asistencial",
                                      "425",
                                      "COD_DESC"));

      vLabels.addElement(new CColumna("Valor",
                                      "175",
                                      "NM_VINDRMC"));

      clmIndAct = new CListaMantenimiento(a,
                                          vLabels,
                                          vBotones,
                                          this,
                                          this,
                                          250,
                                          640);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoMODIFICACION:
            btnSalir.setEnabled(true);
            clmIndAct.setEnabled(true);
            break;
        }
        break;
    }
  }

  Lista anadirCampo(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = new Data();
      dCampo = (Data) lCampo.elementAt(i);
      String s = new String();
      s = dCampo.getString("CD_INDRMC") + " - " + dCampo.getString("DS_INDRMC");
      ( (Data) lCampo.elementAt(i)).put("COD_DESC", s);
    }
    return lCampo;
  }

  //Obligatorio rellenar todos los campos
  public void realizaOperacion(int j) {
    boolean op = false;
    DiaIndAct2 di = null;
    Data dMantIndAct = new Data();
    Lista lIndAct = null;
    lIndAct = clmIndAct.getLista();
    if (lIndAct.size() == 0) {
      op = false;
    }
    else {
      op = true;
    }

    switch (j) {
      // bot�n de alta
      case ALTA:
        dMantIndAct.put("CD_PCENTI", dtDev.getString("CD_PCENTI"));
        dMantIndAct.put("CD_MEDCEN", dtDev.getString("CD_MEDCEN"));
        di = new DiaIndAct2(this.getApp(), ALTA, dMantIndAct, lIndAct, op);
        di.show();
        // a�adir el nuevo elem a la lista
        if (di.bAceptar) {
          clmIndAct.setPrimeraPagina(primeraPagina());
        }
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        dMantIndAct = clmIndAct.getSelected();
        //si existe alguna fila seleccionada
        if (dMantIndAct != null) {
          int ind = clmIndAct.getSelectedIndex();
          di = new DiaIndAct2(this.getApp(), MODIFICACION, dMantIndAct, lIndAct,
                              op);
          di.show();
          //Tratamiento de bAceptar para Salir
          if (di.bAceptar) {
            clmIndAct.setPrimeraPagina(primeraPagina());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dMantIndAct = clmIndAct.getSelected();
        if (dMantIndAct != null) {
          int ind = clmIndAct.getSelectedIndex();
          di = new DiaIndAct2(this.getApp(), BAJA, dMantIndAct, lIndAct, op);
          di.show();
          if (di.bAceptar) {
            noMensaje = true;
            clmIndAct.getLista().removeElementAt(ind);
            clmIndAct.setPrimeraPagina(primeraPagina());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
        }
        break;

    }
  }

  public void trasEventoEnCPnlCodigoExt() {}

  public String getDescCompleta(Data dat) {
    return "";
  }

  private boolean isDataValid() {
    return true;
  }

  void jbInit() throws Exception {
    final String imgSalir = "images/cancelar.gif";

    this.setLayout(xyLayout1);
    this.setSize(670, 455);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgSalir);
    this.getApp().getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno
    lblPuntoCent.setText("Punto Centinela:");
    lblMedico.setText("M�dico:");
    lblEquipo.setText("Equipo Notificador:");
    lblCentro.setText("Centro:");
    lblDesEquipo.setText("");
    lblDesCentro.setText("");
    lblDesMedico.setText("");
    lblDesPto.setText("");

    btnSalir.setActionCommand("Salir");
    btnSalir.setLabel("Salir");
    btnSalir.setImage(this.getApp().getLibImagenes().get(imgSalir));

    // A�adimos los escuchadores...

    btnSalir.addActionListener(actionAdapter);

    xyLayout1.setHeight(900);
    xyLayout1.setWidth(675);

    this.add(lblPuntoCent, new XYConstraints(MARGENIZQ, MARGENSUP, 100, ALTO));
    this.add(lblDesPto,
             new XYConstraints(MARGENIZQ + 100 + INTERHOR, MARGENSUP, 400, ALTO));
    this.add(lblMedico,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 100,
                               ALTO));
    this.add(lblDesMedico,
             new XYConstraints(MARGENIZQ + 100 + INTERHOR,
                               MARGENSUP + ALTO + INTERVERT, 400, ALTO));
    this.add(lblCentro,
             new XYConstraints(MARGENIZQ, MARGENSUP + 2 * ALTO + 2 * INTERVERT,
                               100, ALTO));
    this.add(lblDesCentro,
             new XYConstraints(MARGENIZQ + 100 + INTERHOR,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 400, ALTO));
    this.add(lblEquipo,
             new XYConstraints(MARGENIZQ, MARGENSUP + 3 * ALTO + 3 * INTERVERT,
                               110, ALTO));
    this.add(lblDesEquipo,
             new XYConstraints(MARGENIZQ + 110 + INTERHOR,
                               MARGENSUP + 3 * ALTO + 3 * INTERVERT, 400, ALTO));

    this.add(clmIndAct,
             new XYConstraints(MARGENIZQ - DESFPANEL,
                               MARGENSUP + 4 * ALTO + 4 * INTERVERT, 650, 230));
    this.add(btnSalir,
             new XYConstraints(552,
                               MARGENSUP + 4 * ALTO + 5 * INTERVERT + 230 - 5,
                               88, 29));

    //m�todo temporal para poder ver los datos en el panel
    //pasados a trav�s de la applet
    verDatos();
    buscarIndices();

    // ponemos el t�tulo
    switch (modoOperacion) {
      case modoALTA:
        setTitle("ALTA");
        break;
      case modoMODIFICACION:
        this.setTitle("MODIFICACION");
        break;
      case modoBAJA:
        this.setTitle("BAJA");
        break;
    }
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    Inicializar(CInicializar.ESPERA);

    QueryTool2 qtInd = new QueryTool2();

    qtInd.putName("SIVE_INDACTMC_MEDCEN");
    qtInd.putType("CD_INDRMC", QueryTool.STRING);
    qtInd.putType("NM_VINDRMC", QueryTool.REAL);
    qtInd.putType("CD_PCENTI", QueryTool.STRING);
    qtInd.putType("CD_MEDCEN", QueryTool.STRING);

    qtInd.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qtInd.putWhereValue("CD_MEDCEN", dtDev.getCD_MEDCEN());
    qtInd.putOperator("CD_MEDCEN", "=");

    qtInd.putWhereType("CD_PCENTI", QueryTool.STRING);
    qtInd.putWhereValue("CD_PCENTI", dtDev.getCD_PCENTI());
    qtInd.putOperator("CD_PCENTI", "=");

    QueryTool qtAdic = new QueryTool();
    qtAdic.putName("SIVE_INDACTMC");
    qtAdic.putType("DS_INDRMC", QueryTool.STRING);
    Data dtAdic = new Data();
    dtAdic.put("CD_INDRMC", QueryTool.STRING);
    qtInd.addQueryTool(qtAdic);
    qtInd.addColumnsQueryTool(dtAdic);

    qtInd.addOrderField("CD_INDRMC");

    try {
      this.getApp().getStub().setUrl(servlet);
      p.addElement(qtInd);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      borrPto = 0;
      if ( (p1.size() == 0) && (noMensaje == false)) {
        String nom = new String();
        nom = (" " + dtDev.getDS_NOMBRE() + " " + dtDev.getDS_APE1());
        this.getApp().showAdvise("Actualmente " + nom +
                                 " no posee �ndices de actividad");
        noMensaje = false;
      }
      else {
        p1 = anadirCampo(p1);
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  public Lista siguientePagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    Inicializar(CInicializar.ESPERA);

    QueryTool2 qtInd = new QueryTool2();

    qtInd.putName("SIVE_INDACTMC_MEDCEN");
    qtInd.putType("CD_INDRMC", QueryTool.STRING);
    qtInd.putType("NM_VINDRMC", QueryTool.REAL);
    qtInd.putType("CD_PCENTI", QueryTool.STRING);
    qtInd.putType("CD_MEDCEN", QueryTool.STRING);

    qtInd.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qtInd.putWhereValue("CD_MEDCEN", dtDev.getCD_MEDCEN());
    qtInd.putOperator("CD_MEDCEN", "=");

    qtInd.putWhereType("CD_PCENTI", QueryTool.STRING);
    qtInd.putWhereValue("CD_PCENTI", dtDev.getCD_PCENTI());
    qtInd.putOperator("CD_PCENTI", "=");

    QueryTool qtAdic = new QueryTool();
    qtAdic.putName("SIVE_INDACTMC");
    qtAdic.putType("DS_INDRMC", QueryTool.STRING);
    Data dtAdic = new Data();
    dtAdic.put("CD_INDRMC", QueryTool.STRING);
    qtInd.addQueryTool(qtAdic);
    qtInd.addColumnsQueryTool(dtAdic);

    qtInd.addOrderField("CD_INDRMC");

    try {
      this.getApp().getStub().setUrl(servlet);
      p.addElement(qtInd);
      p.setTrama(this.clmIndAct.getLista().getTrama());
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() != 0) {
        p1 = anadirCampo(p1);
      }

      borrPto = 0;
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  public void rellenarDatos() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    //rellenamos la lista de m�dicos notificadores
    Inicializar(CInicializar.ESPERA);
    clmIndAct.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
  }

  private Data obtenerCentro() {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Data d = new Data();
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool2 qtCen = new QueryTool2();
    try {
      qtCen.putName("SIVE_E_NOTIF");

      qtCen.putType("CD_CENTRO", QueryTool.STRING);

      qtCen.putWhereType("CD_E_NOTIF", QueryTool.STRING);
      qtCen.putWhereValue("CD_E_NOTIF", dtDev.getCD_E_NOTIF());
      qtCen.putOperator("CD_E_NOTIF", "=");

      //Descripci�n del centro
      QueryTool qtAdic1 = new QueryTool();
      qtAdic1.putName("SIVE_C_NOTIF");
      qtAdic1.putType("DS_CENTRO", QueryTool.STRING);
      Data dtAdic1 = new Data();
      dtAdic1.put("CD_CENTRO", QueryTool.STRING);
      qtCen.addQueryTool(qtAdic1);
      qtCen.addColumnsQueryTool(dtAdic1);

      p.addElement(qtCen);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    d = (Data) p1.elementAt(0);
    return d;
  }

  void buscarIndices() {
    Inicializar(CInicializar.ESPERA);
    clmIndAct.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);

  }

  void btn_actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("Salir")) {
      bAceptar = false;
      dispose();
    }
  } //fin de btn_actionPerformed

  public MedCent devuelveData() {
    return dtDevuelto;
  }

  public boolean bAceptar() {
    return bAceptar;
  }

  public int borrPto() {
    return borrPto;
  }

  public void verDatos() {
    Data dtCent = new Data();
    dtCent = obtenerCentro();
    lblDesMedico.setText(dtDev.getString("CD_MEDCEN") + "   D. " +
                         dtDev.getDS_NOMBRE() + " " + dtDev.getDS_APE1() + " " +
                         dtDev.getDS_APE2());
    lblDesPto.setText(dtDev.getString("CD_PCENTI") + "   " +
                      dtDev.getString("DS_PCENTI"));
    lblDesCentro.setText(dtCent.getString("CD_CENTRO") + "   " +
                         dtCent.getString("DS_CENTRO"));
    lblDesEquipo.setText(dtDev.getString("CD_E_NOTIF") + "   " +
                         dtDev.getString("DS_E_NOTIF"));
  }
}

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaMantIndAct adaptee;
  ActionEvent e;

  DialogActionAdapter(DiaMantIndAct adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
