/*
 Clase que permite generar en la b. datos semanas epidemiol�gicas
 para uno o m�s a�os consecutivos
 */
package centinelas.cliente.c_fechas;

import java.net.URL;

import capp.CApp;
import capp.CLista;
import centinelas.datos.c_fechas.DataConvFec;
import sapp.StubSrvBD;

public class GeneradorFechas {

  final String strSERVLET = "servlet/SrvConvFec";
  protected StubSrvBD stubCliente = new StubSrvBD();

  // modos de operaci�n
  final int servletGENERAR_FECHAS = 0;
  final int servletOBTENER = 4;

  public GeneradorFechas(CApp app, String sFecInicial, String anoIni,
                         int numAnos) {
    CLista data;
    try {
      // obtiene y pinta la lista nueva
      data = new CLista();

      data.addElement(new DataConvFec(sFecInicial, anoIni, numAnos));

      // apunta al servlet principal
      stubCliente.setUrl(new URL(app.getURL() + strSERVLET));

      // obtiene la lista
      stubCliente.doPost(servletGENERAR_FECHAS, data);
      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }
}