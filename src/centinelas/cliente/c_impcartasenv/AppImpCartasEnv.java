package centinelas.cliente.c_impcartasenv;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppImpCartasEnv
    extends CApp
    implements CInicializar {

  PanImpCartasEnv pan = null;

  public AppImpCartasEnv() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Reimpresión de cartas enviadas");
    pan = new PanImpCartasEnv(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }

}
