package centinelas.cliente.c_impcartasenv;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CFileName;
import capp2.CInicializar;
import capp2.CPanel;
import capp2.CPnlCodigo;
import centinelas.cliente.c_componentes.ChoiceCDDS;
import centinelas.cliente.c_creaciondbf.DBFCartasEtiq;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

//import centinelas.servidor.listados.*;

public class PanImpCartasEnv
    extends CPanel
    implements CInicializar {

  final protected int servletCARTETIQ_AVISOS = 1;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 23;
  final int DESFPANEL = 5;
  final int TAMPANEL = 360;

  // parametros pasados al di�logo
  public Data dtDev = null;

  //par�metros devueltos por el di�logo
  public Data dtDevuelto = new Data();

  // gesti�n salir/grabar
  public boolean bAceptar = false;
  public boolean bBorrLista = false;

  //fechas en las que se han dado avisos
  Lista lFechas = null;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xyLayout1 = new XYLayout();

  Label lblMod = new Label();
  CPnlCodigo pnlMod = null;
  Checkbox chConEti = new Checkbox();
  Label lblFecha = new Label();
  ChoiceCDDS chFecha = null;
  Checkbox chSelTod = new Checkbox();
  Checkbox chSelMan = new Checkbox();
  CheckboxGroup chkGrp1 = new CheckboxGroup();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  CFileName pnlBuscarFichero = null;

  //Lista que se env�a al servlet, con los datos para el BDF
  Lista lServ = null;

  Lista lResult = null; //Lista con los resultados del servlet
  // Escuchadores de eventos...
  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  //________________________________________________________

  public PanImpCartasEnv(CApp a) {

    super(a);
    QueryTool qtMod = new QueryTool();

    try {
      lFechas = obtenerFechas();
      //configurar el componente del pnlMod
      qtMod.putName("SIVE_PLANTILLA_RMC");
      qtMod.putType("CD_PLANTILLA", QueryTool.STRING);
      qtMod.putType("DS_PLANTILLA", QueryTool.STRING);

      // panel de mant de modelos
      pnlMod = new CPnlCodigo(a,
                              this,
                              qtMod,
                              "CD_PLANTILLA",
                              "DS_PLANTILLA",
                              true,
                              "Modelos carta",
                              "Modelos carta");

      pnlBuscarFichero = new CFileName(a);

      //configuro la choice de fechas de avisos
      chFecha = new ChoiceCDDS(true, false, true, "", "FC_AVISO", lFechas);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  Lista obtenerFechas() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet1 = "servlet/SrvQueryTool";

    Inicializar(CInicializar.ESPERA);

    //Hallamos los m�dicos asociados a ese pto:order by desc
    QueryTool qt = new QueryTool();

    qt.putName("SIVE_AVISOS");
    qt.setDistinct(true);
    qt.putType("FC_AVISO", QueryTool.DATE);
    qt.addOrderField("FC_AVISO");

    p.addElement(qt);

    try {
      this.getApp().getStub().setUrl(servlet1);
      p1 = (Lista)this.getApp().getStub().doPost(1, p);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  } //end obtener fechas

  //________________________________________________________

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(520, 520);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno
    chFecha.setBackground(new Color(255, 255, 150));
    chFecha.writeData();

    chConEti.setLabel("Con etiquetas");
    chSelTod.setLabel("Toda la selecci�n");
    chSelMan.setLabel("Selecci�n manual");
    chSelTod.setCheckboxGroup(chkGrp1);
    chSelMan.setCheckboxGroup(chkGrp1);
    chSelTod.setState(true);

    lblMod.setText("Modelo:");
    lblFecha.setText("Fecha:");

    pnlBuscarFichero.txtFile.setText("c:\\datos.dbf");

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xyLayout1.setHeight(520);
    xyLayout1.setWidth(520);

    int x;
    int y;

    x = MARGENIZQ;
    y = MARGENSUP;
    this.add(lblMod, new XYConstraints(x, y, 80, ALTO));
    x = x + 120 + INTERHOR - DESFPANEL;
    this.add(pnlMod, new XYConstraints(x, y, TAMPANEL, 34));
    y = y + 2 * ALTO + INTERVERT;

    x = MARGENIZQ;
    this.add(chConEti, new XYConstraints(x, y, 140, ALTO));
    y = y + ALTO + INTERVERT;

    x = MARGENIZQ;
    this.add(lblFecha, new XYConstraints(x, y, 100, ALTO));
    x = x + 100 + INTERHOR;
    this.add(chFecha, new XYConstraints(x, y, 120, ALTO));
    y = y + ALTO + INTERVERT;

    x = MARGENIZQ;
    this.add(chSelTod, new XYConstraints(x, y, 140, ALTO));
    y = y + ALTO + INTERVERT;

    x = MARGENIZQ;
    this.add(chSelMan, new XYConstraints(x, y, 140, ALTO));
    y = y + ALTO + INTERVERT;

    x = MARGENIZQ;
    this.add(pnlBuscarFichero, new XYConstraints(x - 10, y - 10, 425, ALTO + 15));
    y = y + ALTO + INTERVERT;

    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 120 + INTERHOR + TAMPANEL - 10 - 15 -
                               45 - INTERHOR - 78 - 15 + 15 + 78 + INTERHOR +
                               45 + 15 - 88 - 3,
                               MARGENSUP + 6 * ALTO + 11 * INTERVERT, 88, 29));
    y = y + ALTO + INTERVERT;

  }

  Lista anadirCampos(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = (Data) lCampo.elementAt(i);
      String s = "Dr. " + dCampo.getString("DS_NOMBRE") + " " +
          dCampo.getString("DS_APE1");
      String p = dCampo.getString("CD_PCENTI") + " - " +
          dCampo.getString("DS_PCENTI");
      String direc = null;
      if (dCampo.getString("DS_NUM").equals("")) {
        direc = dCampo.getString("DS_DIREC") + " " + dCampo.getString("DS_PISO");
      }
      else {
        direc = dCampo.getString("DS_DIREC") + " N�" +
            dCampo.getString("DS_NUM") + " " + dCampo.getString("DS_PISO");
      }
      ( (Data) lCampo.elementAt(i)).put("MEDICO_CENTINELA", s);
      ( (Data) lCampo.elementAt(i)).put("PUNTO_CENTINELA", p);
      ( (Data) lCampo.elementAt(i)).put("DIRECCION", direc);
    }
    return lCampo;
  }

  //________________________________________________________

  public void Inicializar() {
  }

  //________________________________________________________

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
    }
  }

  void crearDBF() {
    try {
      String localiz = this.pnlBuscarFichero.txtFile.getText();
      DBFCartasEtiq genDBF = new DBFCartasEtiq(lServ, localiz);
      //dbfCar= genDBFCar.getDBF();
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
  }

  //________________________________________________________

  //Obligatorio rellenar todos los campos correctamente
  private boolean isDataValid() {
    boolean estado = true;
    if ( (pnlMod.getDatos() == null)) {
      this.getApp().showAdvise("Debe seleccionar el modelo de carta deseado");
      estado = false;
    }

    if (chFecha.getChoiceDS().equals("")) {
      this.getApp().showAdvise("Debe introducir una fecha v�lida");
      estado = false;
    }

    if (pnlBuscarFichero.getNombre().equals("")) {
      this.getApp().showAdvise("Debe introducir un nombre de fichero");
      estado = false;
    }
    return estado;
  }

  //________________________________________________________

  Lista buscarPuntos() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = "servlet/SrvQueryTool";

    Inicializar(CInicializar.ESPERA);

    //Hallamos los m�dicos asociados a ese pto:order by desc
    QueryTool2 qt = new QueryTool2();

    qt.putName("SIVE_AVISOS");
    qt.setDistinct(true);

    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("CD_PCENTI", QueryTool.STRING);

    qt.putWhereType("CD_PLANTILLA", QueryTool.STRING);
    qt.putWhereValue("CD_PLANTILLA",
                     ( (Data) pnlMod.getDatos()).getString("CD_PLANTILLA"));
    qt.putOperator("CD_PLANTILLA", "=");

    qt.putWhereType("FC_AVISO", QueryTool.DATE);
    qt.putWhereValue("FC_AVISO", chFecha.getChoiceDS());
    qt.putOperator("FC_AVISO", "=");

    QueryTool qtAdic0 = new QueryTool();
    qtAdic0.putName("SIVE_MCENTINELA");
    qtAdic0.putType("DS_NOMBRE", QueryTool.STRING);
    qtAdic0.putType("DS_APE1", QueryTool.STRING);
    qtAdic0.putType("DS_APE2", QueryTool.STRING);
    qtAdic0.putType("IT_BAJA", QueryTool.STRING);
    qtAdic0.putType("CD_E_NOTIF", QueryTool.STRING);

    Data dtAdic0 = new Data();
    dtAdic0.put("CD_MEDCEN", QueryTool.STRING);
    qt.addQueryTool(qtAdic0);
    qt.addColumnsQueryTool(dtAdic0);

    QueryTool qtAdic = new QueryTool();
    qtAdic.putName("SIVE_PCENTINELA");
    qtAdic.putType("DS_PCENTI", QueryTool.STRING);
    Data dtAdic = new Data();
    dtAdic.put("CD_PCENTI", QueryTool.STRING);
    qt.addQueryTool(qtAdic);
    qt.addColumnsQueryTool(dtAdic);

    QueryTool qtAdic1 = new QueryTool();
    qtAdic1.putName("SIVE_E_NOTIF");
    qtAdic1.putType("CD_CENTRO", QueryTool.STRING);
    Data dtAdic1 = new Data();
    dtAdic1.put("CD_E_NOTIF", QueryTool.STRING);
    qt.addQueryTool(qtAdic1);
    qt.addColumnsQueryTool(dtAdic1);

    QueryTool qtAdic2 = new QueryTool();
    qtAdic2.putName("SIVE_C_NOTIF");
    qtAdic2.putType("DS_DIREC", QueryTool.STRING);
    qtAdic2.putType("DS_NUM", QueryTool.STRING);
    qtAdic2.putType("DS_PISO", QueryTool.STRING);
    qtAdic2.putType("CD_POSTAL", QueryTool.STRING);
    qtAdic2.putType("CD_MUN", QueryTool.STRING);
    qtAdic2.putType("CD_PROV", QueryTool.STRING);
    Data dtAdic2 = new Data();
    dtAdic2.put("CD_CENTRO", QueryTool.STRING);
    qt.addQueryTool(qtAdic2);
    qt.addColumnsQueryTool(dtAdic2);

    QueryTool qtAdic3 = new QueryTool();
    qtAdic3.putName("SIVE_MUNICIPIO");
    qtAdic3.putType("DS_MUN", QueryTool.STRING);
    Data dtAdic3 = new Data();
    dtAdic3.put("CD_PROV", QueryTool.STRING);
    dtAdic3.put("CD_MUN", QueryTool.STRING);
    qt.addQueryTool(qtAdic3);
    qt.addColumnsQueryTool(dtAdic3);

    QueryTool qtAdic4 = new QueryTool();
    qtAdic4.putName("SIVE_PROVINCIA");
    qtAdic4.putType("DS_PROV", QueryTool.STRING);
    Data dtAdic4 = new Data();
    dtAdic4.put("CD_PROV", QueryTool.STRING);
    qt.addQueryTool(qtAdic4);
    qt.addColumnsQueryTool(dtAdic4);

    qt.addOrderField("CD_PCENTI");

    p.addElement(qt);

    try {
      this.getApp().getStub().setUrl(servlet);
      p1 = (Lista)this.getApp().getStub().doPost(1, p);
      if (p1.size() != 0) {
        p1 = anadirCampos(p1);
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {
    DiaImpCartasEnv di = null;
    boolean accesoServlet = true;
    Data dtEnv = new Data();

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        Inicializar(CInicializar.ESPERA);
        //si est� seleccionado el modo manual
        if (chSelMan.getState()) {
          dtEnv.put("CD_PLANTILLA",
                    ( (Data) pnlMod.getDatos()).getString("CD_PLANTILLA"));
          dtEnv.put("FC_AVISO", chFecha.getChoiceDS());
          di = new DiaImpCartasEnv(this.getApp(), dtEnv);
          di.show();

          //si se ha realizado la operaci�n
          if (di.bAcepta()) {
            lServ = di.listaDev();
            if (lServ.size() == 0) {
              accesoServlet = false;
              this.getApp().showAdvise(
                  "No se ha realizado ninguna selecci�n de puntos centinelas");
            }
          }
          else {
            accesoServlet = false;
            this.getApp().showAdvise(
                "No se ha realizado ninguna selecci�n de puntos centinelas");
          }
        }
        else { //end chManual y si chTodos el seleccionado
          lServ = buscarPuntos();
          if (lServ.size() == 0) {
            accesoServlet = false;
            this.getApp().showAdvise("No existen puntos centinelas");
          }
          else {
            lServ = filtraAltas(lServ);
          }
        }

        //si existen puntos centinelas seleccionados
        if (accesoServlet) {
          try {
            Data dtParam = new Data();

            dtParam.put("FC_AVISO", chFecha.getChoiceDS());

            dtParam.put("CD_PLANTILLA", pnlMod.getDatos().get("CD_PLANTILLA"));
//            dtParam.put("DS_OBSERV",txtObs.getText().trim());
            dtParam.put("LISTA_PUNTOS", lServ);
            dtParam.put("RUTA_FICH", this.pnlBuscarFichero.txtFile.getText());

            Lista lParam = new Lista();
            lParam.addElement(dtParam);

            crearDBF();
// No se si aqu� va servlet o no. En el caso de que vaya, descomentar
// estas dos l�neas.
//            this.getApp().getStub().setUrl("servlet/SrvCartasEtiq");
//            Lista lResult = (Lista)this.getApp().getStub().doPost(servletCARTETIQ_AVISOS,lParam);

            this.getApp().showAdvise("Exportaci�n de fichero finalizada");

            /*            if ( chConEti.getState() ) {
                          //modo 3->   CARTAS_ETIQUETAS
                 DiaCreacionDBF dia=new DiaCreacionDBF(this.getApp(),3,lParam);
                          dia.show();
                        }
                        else {
                          //modo 1->   CARTAS
                 DiaCreacionDBF dia=new DiaCreacionDBF(this.getApp(),1,lParam);
                          dia.show();
                        } */

          }
          catch (Exception ex) {
            this.getApp().trazaLog(ex);
            this.getApp().showError(ex.getMessage());
          }
        }
        Inicializar(CInicializar.NORMAL);
      } //end if isDataValid
    } //end btnAceptar

  } //fin de btn_actionPerformed

  private Lista filtraAltas(Lista entrada) {
    Data dtTemp = null;
    Lista salida = new Lista();
    for (int i = 0; i < entrada.size(); i++) {
      dtTemp = (Data) entrada.elementAt(i);
      if (dtTemp.getString("IT_BAJA").equals("N")) {
        salida.addElement(dtTemp);
      }
      dtTemp = null;
    }
    return salida;
  }

  // Devuelve si se ha realizado la acci�n del bot�n o no
  public boolean bAceptar() {
    return bAceptar;
  }

}

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanImpCartasEnv adaptee;
  ActionEvent e;

  DialogActionAdapter(PanImpCartasEnv adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
