//Applet principal de generación de alarmas automáticas

package centinelas.cliente.genalauto;

import java.util.ResourceBundle;

import capp.CApp;

public class AppGenAlaAuto
    extends CApp {

  ResourceBundle res;

  public void init() {
    super.init();

  }

  public void start() {
    res = ResourceBundle.getBundle("centinelas.cliente.genalauto.Res" +
                                   this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    PanGenAlaAuto panelGen = new PanGenAlaAuto(a);
    VerPanel("", panelGen);

  }

}
