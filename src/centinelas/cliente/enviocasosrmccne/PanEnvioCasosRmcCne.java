/*
 Panel inicial que permite elegir per�odo del cual se extraer�n los casos, enfermedad ,
 indicador, y ruta en la cual se grabar� el fichero generado.
 */
package centinelas.cliente.enviocasosrmccne;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Date;
import java.util.Vector;

import java.awt.Cursor;
import java.awt.FileDialog;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPanel;
import centinelas.cliente.c_componentes.CPnlCodigoExt;
import centinelas.cliente.c_componentes.ContCPnlCodigoExt;
import centinelas.cliente.c_componentes.ContPanAnoSemFecha;
import centinelas.cliente.c_componentes.PanAnoSemFecha;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import componentes.CPnlDosCodigos;
import comun.Fechas;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import util_ficheros.DataCampos;
import util_ficheros.EditorFichero;

// Quitar esta l�nea al terminar las correcciones.
//import centinelas.servidor.enviocasosrmccne.*;

public class PanEnvioCasosRmcCne
    extends CPanel
    implements CInicializar, ContCPnlCodigoExt, ContPanAnoSemFecha {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  XYLayout xyLayout = new XYLayout();

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;

  //solo si los campos no se separan por alg�n s�mbolo concreto:
  //constantes tama�o campos
  final public int tamCENTRO = 2;
  final public int tamSEMANA = 2;
  final public int tamANO = 4;
  final public int tamHAB0a4 = 5;
  final public int tamHAB5a14 = 5;
  final public int tamHAB15a64 = 5;
  final public int tamHAB65MAS = 5;
  final public int tamHABNC = 5;
  final public int tamACTGRIPAL = 1;
  final public int tamCLAVE = 8;

  //componentes q conforman el panel
  Label lDesde = new Label();
  Label lHasta = new Label();
  Label lIndic = new Label();
  Label lEnfer = new Label();
  PanAnoSemFecha panDesde = null;
  PanAnoSemFecha panHasta = null;
  CPnlCodigoExt pnlEnfCent = null;
  CPnlCodigoExt pnlIndicador = null;
  CPnlDosCodigos pnlEnfIndic = null;

  ButtonControl btnAceptar = new ButtonControl();
  capp2.CFileName panFichero;

  Lista ficheros_creados = new Lista();
  boolean crear_fichero = false;

  //vector de DataCampos con nbCampo y su longitud
  Vector vCampos = new Vector();
  CApp apl = null;
  boolean primeraInicializacion = true;

  public PanEnvioCasosRmcCne(CApp a) {
    super(a);
    apl = a;
    //configuro el panel de indicador y de enfermedad
    QueryTool qtInd = new QueryTool();
    QueryTool qtEnfCenti = new QueryTool();
    try {
      /*Configuro el panel de a�o-sem-fecha de Desde y Hasta
             Si no se tiene un a�o seleccionado-> se inicializa a vacio
             Si se tiene un a�os seleccionado-> se inicializa con ese a�o*/
      if (app.getParametro("CD_ANO").equals("")) {
        panDesde = new PanAnoSemFecha(this, PanAnoSemFecha.modoVACIO, false);
      }
      else {
        panDesde = new PanAnoSemFecha(this, false, app.getParametro("CD_ANO"), false);

      }
      if (app.getParametro("CD_ANO").equals("")) {
        panHasta = new PanAnoSemFecha(this, PanAnoSemFecha.modoVACIO, false);
      }
      else {
        panHasta = new PanAnoSemFecha(this, false, app.getParametro("CD_ANO"), false);

      }
      panFichero = new capp2.CFileName(a, FileDialog.SAVE, 2);

      qtEnfCenti.putName("SIVE_PROCESOS");
      qtEnfCenti.putType("CD_ENFCIE", QueryTool.STRING);
      qtEnfCenti.putType("DS_PROCESO", QueryTool.STRING);
      qtEnfCenti.putSubquery("CD_ENFCIE",
          "select CD_ENFCIE from SIVE_ENF_CENTI where IT_ENVCNE='S'");

      qtEnfCenti.putWhereType("CD_ENFCIE", QueryTool.STRING);
      qtEnfCenti.putWhereValue("CD_ENFCIE", "");
      qtEnfCenti.putOperator("CD_ENFCIE", "=");
      //panel de punto centinela
      pnlEnfCent = new CPnlCodigoExt(a,
                                     this,
                                     qtEnfCenti,
                                     "CD_ENFCIE",
                                     "DS_PROCESO",
                                     true,
                                     "Enfermedades centinelas",
                                     "Enfermedades centinelas",
                                     this);

      qtInd.putName("SIVE_IND_ALAR_RMC");
      qtInd.putType("CD_INDALAR", QueryTool.STRING);
      qtInd.putType("DS_INDALAR", QueryTool.STRING);
      /*Posible ampliaci�n: clausula WHERE dependiendo del
               perfil ->nivel_1 o nivel_2 */

      qtInd.putWhereType("CD_ENFCIE", QueryTool.STRING);
      qtInd.putWhereValue("CD_ENFCIE", "");
      qtInd.putOperator("CD_ENFCIE", "=");

      //paneles
      pnlIndicador = new CPnlCodigoExt(a,
                                       this,
                                       qtInd,
                                       "CD_INDALAR",
                                       "DS_INDALAR",
                                       true,
                                       "Indicadores",
                                       "Indicadores",
                                       this);
      pnlEnfIndic = new CPnlDosCodigos(pnlEnfCent, pnlIndicador, "CD_ENFCIE", this,
                                       CPnlDosCodigos.modoVERTICAL);

      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor

  public void trasEventoEnCPnlCodigoExt() {}

  public String getDescCompleta(Data dat) {
    return "";
  }

  //rellena el vector de DataCampos con los nombres de los campos y su longitud
  void rellenarVector() {
    DataCampos dtCentro = new DataCampos("CENTRODECLARANTE", tamCENTRO);
    DataCampos dtSem = new DataCampos("SEMANADECLARACION", tamSEMANA);
    DataCampos dtAno = new DataCampos("ANODECLARACION", tamANO);
    DataCampos dtHab0a4 = new DataCampos("HAB0-4", tamHAB0a4);
    DataCampos dtHab5a14 = new DataCampos("HAB5-14", tamHAB5a14);
    DataCampos dtHab15a64 = new DataCampos("HAB15-64", tamHAB15a64);
    DataCampos dtHab65mas = new DataCampos("HAB65MAS", tamHAB65MAS);
    DataCampos dtHabNc = new DataCampos("HABNC", tamHABNC);
    DataCampos dtActGrip = new DataCampos("ACTIVIDADGRIPAL", tamACTGRIPAL);
    DataCampos dtClave = new DataCampos("CLAVE", tamCLAVE);
    vCampos.addElement(dtCentro);
    vCampos.addElement(dtSem);
    vCampos.addElement(dtAno);
    vCampos.addElement(dtHab0a4);
    vCampos.addElement(dtHab5a14);
    vCampos.addElement(dtHab15a64);
    vCampos.addElement(dtHab65mas);
    vCampos.addElement(dtHabNc);
    vCampos.addElement(dtActGrip);
    vCampos.addElement(dtClave);
  }

  void jbInit() throws Exception {
    final String imgAceptar = "images/aceptar.gif";

    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().CargaImagenes();

    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnAceptar.setLabel("Aceptar");
    btnAceptar.addActionListener(new
                                 PanEnvioCasosRmcCne_btnAceptar_actionAdapter(this));

    xyLayout.setHeight(275);
    xyLayout.setWidth(515);
    this.setLayout(xyLayout);

    lDesde.setText("Desde:");
    lHasta.setText("Hasta:");
    lIndic.setText("Indicador:");
    lEnfer.setText("Enfermedad:");

    this.add(lDesde, new XYConstraints(MARGENIZQ, MARGENSUP, 102, ALTO));
    this.add(panDesde,
             new XYConstraints(MARGENIZQ + 102 + INTERHOR, MARGENSUP, 515, 34));
    this.add(lHasta,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 102,
                               ALTO));
    this.add(panHasta,
             new XYConstraints(MARGENIZQ + 102 + INTERHOR,
                               MARGENSUP + ALTO + INTERVERT, 515, 34));
    this.add(lEnfer,
             new XYConstraints(MARGENIZQ, MARGENSUP + 2 * ALTO + 2 * INTERVERT,
                               102, ALTO));
    this.add(lIndic,
             new XYConstraints(MARGENIZQ, MARGENSUP + 3 * ALTO + 3 * INTERVERT,
                               102, ALTO));
    this.add(pnlEnfIndic,
             new XYConstraints(MARGENIZQ + 102 + INTERHOR,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 651,
                               2 * 34));
    this.add(panFichero,
             new XYConstraints(MARGENIZQ - 13,
                               MARGENSUP + 4 * ALTO + 4 * INTERVERT, 515, 38));
    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 70 + 515 - 2 * 88 - 25,
                               MARGENSUP + 5 * ALTO + 6 * INTERVERT, 88, 29));

    Inicializar(CInicializar.NORMAL);

    //configuro el vector vCampos:DataCampos
    rellenarVector();
  } //end jbinit

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        pnlEnfIndic.InicializarEnModo(CInicializar.ESPERA);
        panDesde.Inicializate(CInicializar.ESPERA);
        panHasta.Inicializate(CInicializar.ESPERA);
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        panDesde.Inicializate(CInicializar.NORMAL);
        panHasta.Inicializate(CInicializar.NORMAL);
        this.setEnabled(true);
        if (primeraInicializacion) {
          pnlEnfIndic.inicializatePrimeraVezNormal(true);
          primeraInicializacion = false;
        }
        else {
          pnlEnfIndic.InicializarEnModo(CInicializar.NORMAL); //LRG
        }
    }
  }

  //Con la fecha de nacimiento te calcula la edad actual en a�os
  String calcularEdad(String fecNac) {
    String edadA = null;
    Integer aux = null;
    Date dateNac = Fechas.string2Date(fecNac);

    if (dateNac != null) {
      Date hoy = new Date();
      long mili_anio = 31557600000L;
      long mili_mes = 2626560000L;
      long edad_mili = hoy.getTime() - dateNac.getTime();

      if (edad_mili > 0) {
        aux = new Integer( (int) (edad_mili / mili_anio));
      }
      else {
        aux = new Integer(0);
      }
      edadA = aux.toString();
    }
    else {
      edadA = "N.C.";
    }
    return edadA;
  }

  Lista obtenerValorDeIndicador(String sAno, String sSem) {
    String sInd = pnlIndicador.getDatos().getString("CD_INDALAR");
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool qtVal = new QueryTool();
    try {
      qtVal.putName("SIVE_ALARMA_RMC");
      qtVal.putType("NM_VALOR", QueryTool.INTEGER);

      qtVal.putWhereType("CD_INDALAR", QueryTool.STRING);
      qtVal.putWhereValue("CD_INDALAR", sInd);
      qtVal.putOperator("CD_INDALAR", "=");

      qtVal.putWhereType("CD_ANOEPI", QueryTool.STRING);
      qtVal.putWhereValue("CD_ANOEPI", sAno);
      qtVal.putOperator("CD_ANOEPI", "=");

      qtVal.putWhereType("CD_SEMEPI", QueryTool.STRING);
      qtVal.putWhereValue("CD_SEMEPI", sSem);
      qtVal.putOperator("CD_SEMEPI", "=");

      p.addElement(qtVal);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    return p1;
  }

  //Calcula el par�metro Actividad Gripal
  String HallarActividadGripal(int contGen, String ano, String sem) {
    String sDev = null;
    Lista lValor = obtenerValorDeIndicador(ano, sem);
    int iInd = 0;
    if (lValor.size() != 0) {
      Data dtValor = (Data) lValor.elementAt(0);
      iInd = Integer.parseInt(dtValor.getString("NM_VALOR"));
    }
    if (contGen <= iInd) {
      sDev = "1";
    }
    else {
      sDev = "4";
    }
    return sDev;
  }

  /*recibe la lista q dev el servlet y trata sus datos generando una
     lista de Datas.Cada Data tiene datos de 1sem y a�o concretos y
     cada data representa 1 linea del fichero*/
  Lista obtenerDatosFichero(Lista lServ) {
    /*por cada a�o y semana del periodo mirar a ver si existen casos
         y si es as� generar un Data con ellos.*/
    Lista lFic = new Lista();
    Data dtRec = recogerDatos();
    String anoIni = dtRec.getString("CD_ANOEPIINI");
    String semIni = dtRec.getString("CD_SEMEPIINI");
    String anoFin = dtRec.getString("CD_ANOEPIFIN");
    String semFin = dtRec.getString("CD_SEMEPIFIN");

    int nmanoIni = Integer.parseInt(anoIni);
    int nmsemIni = Integer.parseInt(semIni);
    int nmanoFin = Integer.parseInt(anoFin);
    int nmsemFin = Integer.parseInt(semFin);

    for (int ano = nmanoIni; ano <= nmanoFin; ano++) {
      for (int sem = 1; sem <= 53; sem++) {
        int contGeneral = 0;
        int cont0a4 = 0;
        int cont5a14 = 0;
        int cont15a64 = 0;
        int cont65mas = 0;
        int contNC = 0;
        String anoInd = "";
        String semInd = "";
        for (int i = 0; i < lServ.size(); i++) {
          Data dtServ = (Data) lServ.elementAt(i);
          //lo mismo q con semana hacer con a�o
          int anoList = Integer.parseInt(dtServ.getString("CD_ANOEPI"));
          int semList = Integer.parseInt(dtServ.getString("CD_SEMEPI"));
          if ( (ano == anoList) && (sem == semList)) {
            anoInd = dtServ.getString("CD_ANOEPI");
            semInd = dtServ.getString("CD_SEMEPI");
            String sFecNac = dtServ.getString("FC_NAC");
            String sEdad = "";
            if (sFecNac.equals("")) {
              sEdad = "N.C.";
            }
            else {
              sEdad = calcularEdad(sFecNac);
            }
            //aumento contadores de casos individuales
            if (sEdad.equals("N.C.")) {
              contNC = contNC + 1;
            }
            else {
              int edad = Integer.parseInt(sEdad);
              if ( (edad >= 0) && (edad <= 4)) {
                cont0a4 = cont0a4 + 1;
              }
              if ( (edad >= 5) && (edad <= 14)) {
                cont5a14 = cont5a14 + 1;
              }
              if ( (edad >= 15) && (edad <= 64)) {
                cont15a64 = cont15a64 + 1;
              }
              if (edad >= 65) {
                cont65mas = cont65mas + 1;
              }
            } //end aumentar contadores individuales
            //aumento contador general
            contGeneral = contGeneral + 1;
          } //end semanas iguales
        } //end recorrer la lista
        //para 1 a�o y sem concreta tengo todos los casos si existen
        //Si existen casos para esa semana...
        if (contGeneral > 0) {
          String sCentro = app.getParametro("CA"); //"04";
          String sSemDeclar = semInd;
          String sAnoDeclar = anoInd;
          String sRango0a4 = new Integer(cont0a4).toString();
          String sRango5a14 = new Integer(cont5a14).toString();
          String sRango15a64 = new Integer(cont15a64).toString();
          String sRango65mas = new Integer(cont65mas).toString();
          String sRangoNC = new Integer(contNC).toString();
          String sActGrip = HallarActividadGripal(contGeneral, anoInd, semInd);
          String sClave = sCentro + sSemDeclar + sAnoDeclar;
          Data dtCasos = new Data();
          dtCasos.put("CENTRODECLARANTE", sCentro);
          dtCasos.put("SEMANADECLARACION", sSemDeclar);
          dtCasos.put("ANODECLARACION", sAnoDeclar);
          dtCasos.put("HAB0-4", sRango0a4);
          dtCasos.put("HAB5-14", sRango5a14);
          dtCasos.put("HAB15-64", sRango15a64);
          dtCasos.put("HAB65MAS", sRango65mas);
          dtCasos.put("HABNC", sRangoNC);
          dtCasos.put("ACTIVIDADGRIPAL", sActGrip);
          dtCasos.put("CLAVE", sClave);
          //introducir en la lista
          lFic.addElement(dtCasos);
        } //end if contadorGen>0
      } //end for semanas
    } //end for a�os
    return lFic;
  } //end func. obtenerDatosFichero

  void btnAceptar_actionPerformed(ActionEvent e) {
//    final String servlet = "servlet/SrvEnvioCasosRmcCne";
    final String servlet = nombreservlets.strSERVLET_ENVIO_CASOS_RMC_CNE;
    Lista lResultado = null;
    Data dtDatos = new Data();
    Lista lDatos = new Lista();
    Lista lFic = null;
    ficheros_creados = null;
    ficheros_creados = new Lista();
    if (isDataValid()) {
      //1� llamar al servlet y mandar ejecutar la operacion->dev lista
      Inicializar(CInicializar.ESPERA);
      dtDatos = recogerDatos();
      lDatos.addElement(dtDatos);

      // ======= Aqu� se genera el fichero PRIMERO ============

      try {

        /*                             // debug
             SrvEnvioCasosRmcCne srv = new SrvEnvioCasosRmcCne();
                            // par�metros jdbc
             srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                            lResultado = srv.doDebug(1, lDatos);  */

        lResultado = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, lDatos);
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
        ex.printStackTrace();
      }

      //Primero tratar informaci�n de salida del servlet
      lFic = obtenerDatosFichero(lResultado);
      //mandar escribir el fichero
      String localiz = this.panFichero.txtFile.getText();
      String nom_fich = "inf_por_zonas.txt";
      localiz += nom_fich;
      if (lFic.size() > 0) {
        ficheros_creados.addElement(nom_fich);
      }
      EditorFichero edFic = new EditorFichero();
      edFic.escribirFicheroSinMsgDer(vCampos, apl, lFic, localiz);

      // ======= Aqu� se genera el fichero SEGUNDO ============

      lResultado = null;
      lResultado = new Lista();

      // Comprobamos si hay modelos CNE para generar el segundo fichero.

      Lista vFiltro = new Lista();
      Lista vReg = new Lista();

      QueryTool qt = new QueryTool();
      qt.putName("SIVE_MODELO");
      qt.putType("CD_MODELO", QueryTool.STRING);
      qt.putWhereType("CD_ENFCIE", QueryTool.STRING);
      qt.putWhereValue("CD_ENFCIE", dtDatos.getString("CD_ENFCIE"));
      qt.putOperator("CD_ENFCIE", "=");
      qt.putWhereType("CD_TSIVE", QueryTool.STRING);
      qt.putWhereValue("CD_TSIVE", "C");
      qt.putOperator("CD_TSIVE", "=");
      qt.putWhereType("CD_NIVEL_1", QueryTool.VOID);
      qt.putWhereValue("CD_NIVEL_1", "");
      qt.putOperator("CD_NIVEL_1", "is null");
      qt.putWhereType("CD_NIVEL_2", QueryTool.VOID);
      qt.putWhereValue("CD_NIVEL_2", "");
      qt.putOperator("CD_NIVEL_2", "is null");
      qt.putWhereType("CD_CA", QueryTool.VOID);
      qt.putWhereValue("CD_CA", "");
      // Correcci�n 09-05-01, porque Centinelas no tiene
      // perfil CA
      qt.putOperator("CD_CA", "is not null");
      // Encontrado nombre de columna err�nea: CD_OK en vez de
      // IT_OK
      qt.putWhereType("IT_OK", QueryTool.STRING);
      qt.putWhereValue("IT_OK", "S");
      qt.putOperator("IT_OK", "=");

      try {

        this.getApp().getStub().setUrl("servlet/SrvQueryTool");
        vFiltro.addElement(qt);
        // Le ponemos la trama.
        vReg = (Lista)this.getApp().getStub().doPost(2, vFiltro);

        // muestra incidencia
        if (vReg.size() == 0) {
          crear_fichero = false;
        }
        else {
          crear_fichero = true;

          // error en el servlet
        }
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
        this.getApp().showError(ex.getMessage());
      }

      vReg = null;
      vFiltro = null;

      if (crear_fichero) {

        try {
          lDatos.setParameter("COD_CENTRO", app.getParametro("CA"));
          this.getApp().getStub().setUrl(servlet);
          lResultado = (Lista)this.getApp().getStub().doPost(2, lDatos);

          /*                     // debug
               SrvEnvioCasosRmcCne srv = new SrvEnvioCasosRmcCne();
                              // par�metros jdbc
               srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
               "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                     "sive_desa",
                                     "sive_desa");
                              lResultado = srv.doDebug(2, lDatos);*/

        }
        catch (Exception ex) {
          this.getApp().trazaLog(ex);
          ex.printStackTrace();
        }

        if (lResultado.size() > 0) {
          try {
            crearFicheroEnvioCNE(lResultado);
          }
          catch (Exception exc) {
            this.getApp().showError("Error al crear el fichero: " + exc);
          }
        }
        else {
          this.getApp().showAdvise("No hay datos");
        }
      } // Del if_crear_fichero.
      // Pos yast�. Ahora sacamos la informaci�n con los ficheros creados.
      DiaMsgAvisoFic informe = new DiaMsgAvisoFic(apl, 0, ficheros_creados);
      informe.show();
      limpiar_pantalla();
    } //end if isDataValid
    Inicializar(CInicializar.NORMAL);
  }

  Data recogerDatos() {
    Data dtRecoger = new Data();
    dtRecoger.put("CD_ANOEPIINI", panDesde.getCodAno());
    dtRecoger.put("CD_SEMEPIINI", panDesde.getCodSem());
    dtRecoger.put("CD_ANOEPIFIN", panHasta.getCodAno());
    dtRecoger.put("CD_SEMEPIFIN", panHasta.getCodSem());
    dtRecoger.put("CD_INDALAR", pnlIndicador.getDatos().getString("CD_INDALAR"));
    dtRecoger.put("CD_ENFCIE", pnlEnfCent.getDatos().getString("CD_ENFCIE"));
    dtRecoger.put("CD_TSIVE", "C");
    return dtRecoger;
  }

  //comprobar q se encuentran rellenos los campos
  boolean isDataValid() {
    if ( (panDesde.getCodAno().equals("")) ||
        (panDesde.getCodSem().equals("")) ||
        (panDesde.getFecSem().equals("")) ||
        (panHasta.getCodAno().equals("")) ||
        (panHasta.getCodSem().equals("")) ||
        (panHasta.getFecSem().equals("")) ||
        (pnlIndicador.getDatos() == null) ||
        (pnlEnfCent.getDatos() == null) ||
        (panFichero.txtFile.getText().length() == 0)) {
      this.getApp().showAdvise(
          "Se deben rellenar todos los campos obligatoriamente");
      return false;
    }
    else {
      boolean b = comprobarRangoFechas();
      if (b) {
        return true;
      }
      else {
        return false;
      }
    }
  }

  void limpiar_pantalla() {
    panDesde.sCodSemBk = "";
    panDesde.sFecSemBk = "";
    panDesde.txtCodSem.setText("");
    panDesde.txtFecSem.setText("");
    panHasta.sCodSemBk = "";
    panHasta.sFecSemBk = "";
    panHasta.txtCodSem.setText("");
    panHasta.txtFecSem.setText("");
    pnlEnfCent.limpiarDatos();
    panFichero.txtFile.setText("c:\\");
  }

  //comprueba que la fecha l�mite hasta sea mayor que la de desde
  boolean comprobarRangoFechas() {
    boolean b;
    int sAnoDesde = Integer.parseInt(panDesde.getCodAno());
    int sSemDesde = Integer.parseInt(panDesde.getCodSem());
    int sAnoHasta = Integer.parseInt(panHasta.getCodAno());
    int sSemHasta = Integer.parseInt(panHasta.getCodSem());
    if (sAnoHasta > sAnoDesde) {
      b = true;
    }
    else {
      if (sAnoHasta < sAnoDesde) {
        this.getApp().showAdvise(
            "El a�o l�mite superior es menor que el inferior");
        b = false;
      }
      else {
        if (sSemHasta < sSemDesde) {
          this.getApp().showAdvise(
              "La semana l�mite superior es menor que la inferior para un mismo a�o");
          b = false;
        }
        else {
          b = true;
        }
      }
    }
    return b;
  }

  void crearFicheroEnvioCNE(Lista lDatos) throws Exception {
    boolean primera_vuelta = false;
    String localiz = this.panFichero.txtFile.getText();
    String nomFich = "inf_de_casos.txt";
    // Se llama al modo del servlet que devuelve
    // una lista con dos listas dentro. Se utiliza la primera lista.
    Lista lResult = (Lista) lDatos.elementAt(0);
    if (lResult.size() > 0) {
      ficheros_creados.addElement("inf_de_casos.txt");
    }
    localiz = localiz + nomFich;
    // Procedemos a escribir el fichero en disco.
    try {
      BufferedWriter salida = new BufferedWriter(new FileWriter(localiz));
      for (int i = 0; i < lResult.size(); i++) {
        // Ejcribimos la l�nea de datos en el fichero.
        if (primera_vuelta) {
          salida.newLine();
        }
        salida.write( (String) lResult.elementAt(i));
        primera_vuelta = true;
      }
      salida.close();
    }
    catch (Exception ex) {
      throw new Exception(ex.toString());
    }
  }

//_____IMPLEMENTACION INTERFAZ CONTPANANOSEMFECHA ________________________________________________________

  /* Para obtener el applet*/
  public CApp getMiCApp() {
    return this.app;
  };

  /* M�todos ser�n llamados desde el PanAnoSemFecha despu�s de que el propio PanAnoSemFecha
   * ejecute su codigo en la p�rdida de foco */

  public void alPerderFocoAno() {}

  public void alPerderFocoSemana() {}

  public void alPerderFocoFecha() {}

//_____________________________________________________________

}

class PanEnvioCasosRmcCne_btnAceptar_actionAdapter
    implements java.awt.event.ActionListener {
  PanEnvioCasosRmcCne adaptee;

  PanEnvioCasosRmcCne_btnAceptar_actionAdapter(PanEnvioCasosRmcCne adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnAceptar_actionPerformed(e);
  }
}
