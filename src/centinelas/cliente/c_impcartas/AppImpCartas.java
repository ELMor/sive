//Applet para impresión de cartas
package centinelas.cliente.c_impcartas;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppImpCartas
    extends CApp
    implements CInicializar {

  PanImpCartas pan = null;

  public AppImpCartas() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Impresión de cartas");
    pan = new PanImpCartas(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }

}