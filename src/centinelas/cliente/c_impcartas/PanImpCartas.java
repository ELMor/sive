//Panel con slecci�n de datos para impresi�n de cartas
package centinelas.cliente.c_impcartas;

//import centinelas.servidor.c_listados.SrvCartasEtiq;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPanel;
import capp2.CPnlCodigo;
import capp2.CTexto;
import centinelas.cliente.c_creaciondbf.DBFCartasEtiq;
import centinelas.cliente.c_impetiquetas.DiaImpEtiquetas;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class PanImpCartas
    extends CPanel
    implements CInicializar {

  final protected int servletCARTETIQ_AVISOS = 1;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // parametros pasados al di�logo
  public Data dtDev = null;

  //par�metros devueltos por el di�logo
  public Data dtDevuelto = new Data();

  // gesti�n salir/grabar
  public boolean bAceptar = false;
  public boolean bBorrLista = false;

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xyLayout1 = new XYLayout();

  Label lblMod = new Label();
  CPnlCodigo pnlMod = null;
  Checkbox chConEti = new Checkbox();
  Label lblObs = new Label();
  CTexto txtObs = new CTexto(50);
  Checkbox chSelTod = new Checkbox();
  Checkbox chSelMan = new Checkbox();
  CheckboxGroup chkGrp1 = new CheckboxGroup();
  capp2.CFileName panFichero;
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  //Lista que se env�a al servlet, con los datos para el BDF
  Lista lServ = null;

  Object objResult = null; //Objeto resultado del  servlet
  // Escuchadores de eventos...
  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  //________________________________________________________

  public PanImpCartas(CApp a) {

    super(a);

    QueryTool qtMod = new QueryTool();

    try {
      //configurar el componente del pnlMod
      qtMod.putName("SIVE_PLANTILLA_RMC");
      qtMod.putType("CD_PLANTILLA", QueryTool.STRING);
      qtMod.putType("DS_PLANTILLA", QueryTool.STRING);

      // panel de mant de modelos
      pnlMod = new CPnlCodigo(a,
                              this,
                              qtMod,
                              "CD_PLANTILLA",
                              "DS_PLANTILLA",
                              true,
                              "Modelos carta",
                              "Modelos carta");

      panFichero = new capp2.CFileName(a);
      panFichero.txtFile.setText("c:\\datos.dbf");
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //________________________________________________________

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(520, 365);
    xyLayout1.setHeight(365);
    xyLayout1.setWidth(520);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno
    txtObs.setText("");

    chConEti.setLabel("Con etiquetas");
    chSelTod.setLabel("Selecci�n de todos los puntos centinelas");
    chSelMan.setLabel("Selecci�n manual");
    chSelTod.setCheckboxGroup(chkGrp1);
    chSelMan.setCheckboxGroup(chkGrp1);
    chSelTod.setState(true);

    lblMod.setText("Modelo:");
    lblObs.setText("Observaciones:");

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    int x;
    int y;

    x = MARGENIZQ;
    y = MARGENSUP;
    this.add(lblMod, new XYConstraints(x, y, 80, ALTO));
    x = x + 120 + INTERHOR - DESFPANEL;
    this.add(pnlMod, new XYConstraints(x, y, TAMPANEL, 34));
    y = y + 2 * ALTO + INTERVERT;

    x = MARGENIZQ;
    this.add(chConEti, new XYConstraints(x, y, 140, ALTO));
    y = y + ALTO + INTERVERT;

    x = MARGENIZQ;
    this.add(lblObs, new XYConstraints(x, y, 100, ALTO));
    x = x + 100 + INTERHOR;
    this.add(txtObs, new XYConstraints(x, y, 200, ALTO));
    y = y + ALTO + INTERVERT;

    x = MARGENIZQ;
    this.add(chSelTod, new XYConstraints(x, y, 300, ALTO));
    y = y + ALTO + INTERVERT;

    x = MARGENIZQ;
    this.add(chSelMan, new XYConstraints(x, y, 300, ALTO));
    y = y + ALTO + INTERVERT;

    this.add(panFichero, new XYConstraints(x - 10, y, 515, 38));
    y = y + 38 + INTERVERT;

//    this.add(btnAceptar, new XYConstraints(MARGENIZQ+120+INTERHOR+TAMPANEL-10-15-45-INTERHOR-78-15+15+78+INTERHOR+45+15-88-3,MARGENSUP+4*ALTO+38+10*INTERVERT, 88, 29));
    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 120 + INTERHOR + TAMPANEL - 10 - 15 -
                               45 - INTERHOR - 78 - 15 + 15 + 78 + INTERHOR +
                               45 + 15 - 88 - 3 - 20, y + 5, 88, 29));
    y = y + ALTO + INTERVERT;

  }

  //________________________________________________________

  public void Inicializar() {
  }

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
    }
  }

  //________________________________________________________

  //Obligatorio rellenar todos los campos correctamente
  private boolean isDataValid() {

    if ( (pnlMod.getDatos() == null)) {
      this.getApp().showAdvise("Debe seleccionar el modelo de carta deseado");
      return false;
    }
    else {
      if (panFichero.txtFile.getText().length() == 0) {
        this.getApp().showAdvise("Debe seleccionar el campo Nombre del fichero");
        return false;
      }
      else {
        return true;
      }
    }

  }

  //________________________________________________________

  Lista buscarPuntos() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = "servlet/SrvQueryTool";

    Inicializar(CInicializar.ESPERA);

    //Hallamos los m�dicos asociados a ese pto:order by desc
    QueryTool2 qt = new QueryTool2();

    qt.putName("SIVE_MCENTINELA");
    qt.putType("CD_MEDCEN", QueryTool.STRING);
    qt.putType("DS_NOMBRE", QueryTool.STRING);
    qt.putType("DS_APE1", QueryTool.STRING);
    qt.putType("DS_APE2", QueryTool.STRING);
    qt.putType("CD_E_NOTIF", QueryTool.STRING);
    qt.putType("CD_MASIST", QueryTool.STRING);
    qt.putType("DS_DNI", QueryTool.STRING);
    qt.putType("DS_TELEF", QueryTool.STRING);
    qt.putType("DS_FAX", QueryTool.STRING);
    qt.putType("FC_ALTA", QueryTool.DATE);
    qt.putType("IT_BAJA", QueryTool.STRING);
    qt.putType("CD_PCENTI", QueryTool.STRING);

    // El G�ear.
    qt.putWhereType("IT_BAJA", QueryTool.STRING);
    qt.putWhereValue("IT_BAJA", "N");
    qt.putOperator("IT_BAJA", "=");

    QueryTool qtAdic = new QueryTool();
    qtAdic.putName("SIVE_PCENTINELA");
    qtAdic.putType("DS_PCENTI", QueryTool.STRING);
    Data dtAdic = new Data();
    dtAdic.put("CD_PCENTI", QueryTool.STRING);
    qt.addQueryTool(qtAdic);
    qt.addColumnsQueryTool(dtAdic);

    QueryTool qtAdic1 = new QueryTool();
    qtAdic1.putName("SIVE_E_NOTIF");
    qtAdic1.putType("CD_CENTRO", QueryTool.STRING);
    Data dtAdic1 = new Data();
    dtAdic1.put("CD_E_NOTIF", QueryTool.STRING);
    qt.addQueryTool(qtAdic1);
    qt.addColumnsQueryTool(dtAdic1);

    QueryTool qtAdic2 = new QueryTool();
    qtAdic2.putName("SIVE_C_NOTIF");
    qtAdic2.putType("DS_DIREC", QueryTool.STRING);
    qtAdic2.putType("DS_NUM", QueryTool.STRING);
    qtAdic2.putType("DS_PISO", QueryTool.STRING);
    qtAdic2.putType("CD_POSTAL", QueryTool.STRING);
    qtAdic2.putType("CD_MUN", QueryTool.STRING);
    qtAdic2.putType("CD_PROV", QueryTool.STRING);
    Data dtAdic2 = new Data();
    dtAdic2.put("CD_CENTRO", QueryTool.STRING);
    qt.addQueryTool(qtAdic2);
    qt.addColumnsQueryTool(dtAdic2);

    QueryTool qtAdic3 = new QueryTool();
    qtAdic3.putName("SIVE_MUNICIPIO");
    qtAdic3.putType("DS_MUN", QueryTool.STRING);
    Data dtAdic3 = new Data();
    dtAdic3.put("CD_PROV", QueryTool.STRING);
    dtAdic3.put("CD_MUN", QueryTool.STRING);
    qt.addQueryTool(qtAdic3);
    qt.addColumnsQueryTool(dtAdic3);

    QueryTool qtAdic4 = new QueryTool();
    qtAdic4.putName("SIVE_PROVINCIA");
    qtAdic4.putType("DS_PROV", QueryTool.STRING);
    Data dtAdic4 = new Data();
    dtAdic4.put("CD_PROV", QueryTool.STRING);
    qt.addQueryTool(qtAdic4);
    qt.addColumnsQueryTool(dtAdic4);

    qt.addOrderField("CD_PCENTI");

    p.addElement(qt);

    try {
      this.getApp().getStub().setUrl(servlet);
      p1 = (Lista)this.getApp().getStub().doPost(1, p);
      if (p1.size() != 0) {
        p1 = anadirCampos(p1);
      }

    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  Lista anadirCampos(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = (Data) lCampo.elementAt(i);
      String s = "Dr. " + dCampo.getString("DS_NOMBRE") + " " +
          dCampo.getString("DS_APE1");
      String p = dCampo.getString("CD_PCENTI") + " - " +
          dCampo.getString("DS_PCENTI");
      String direc = null;
      if (dCampo.getString("DS_NUM").equals("")) {
        direc = dCampo.getString("DS_DIREC") + " " + dCampo.getString("DS_PISO");
      }
      else {
        direc = dCampo.getString("DS_DIREC") + " N�" +
            dCampo.getString("DS_NUM") + " " + dCampo.getString("DS_PISO");
      }
      ( (Data) lCampo.elementAt(i)).put("MEDICO_CENTINELA", s);
      ( (Data) lCampo.elementAt(i)).put("PUNTO_CENTINELA", p);
      ( (Data) lCampo.elementAt(i)).put("DIRECCION", direc);
    }
    return lCampo;
  }

  void vaciarPantalla() {
    pnlMod.limpiarDatos();
    chConEti.setState(false);
    txtObs.setText("");
    chSelTod.setState(true);
    panFichero.txtFile.setText("c:\\datos.dbf");
  }

  void crearDBF() {
    try {
      String localiz = this.panFichero.txtFile.getText();
      DBFCartasEtiq genDBF = new DBFCartasEtiq(lServ, localiz);
      //dbfCar= genDBFCar.getDBF();
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
  }

  // aceptar/cancelar
  void btn_actionPerformed(ActionEvent e) {

    DiaImpEtiquetas di = null;
    boolean accesoServlet = true;

    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        Inicializar(CInicializar.ESPERA);
        //si est� seleccionado el modo manual
        if (chSelMan.getState()) {
          di = new DiaImpEtiquetas(this.getApp());
          di.show();

          //si se ha realizado la operaci�n
          if (di.bAcepta()) {
            lServ = di.listaDev();
            if (lServ.size() == 0) {
              accesoServlet = false;
              this.getApp().showAdvise(
                  "No se ha realizado ninguna selecci�n de puntos centinelas");
            }
          }
          else {
            accesoServlet = false;
            this.getApp().showAdvise(
                "No se ha realizado ninguna selecci�n de puntos centinelas");
          }
        }
        else { //end chManual y si chTodos el seleccionado
          lServ = buscarPuntos();

          if (lServ.size() == 0) {
            accesoServlet = false;
            this.getApp().showAdvise("No existen puntos centinelas");
          }
        }

        //si existen puntos centinelas seleccionados
        if (accesoServlet) {
          //LLAMADA AL SERVLET

          try {
            //Se pasa al servlet el n�copias y la lista con la inf
            //para crear el fichero
            Data dtParam = new Data();

            dtParam.put("CD_PLANTILLA", pnlMod.getDatos().get("CD_PLANTILLA"));
            dtParam.put("DS_OBSERV", txtObs.getText().trim());
            //le pasamos el par�metro COPIAS con un valor 0 pq aunque
            //en este caso no se emplee, este dato es necesario para el .ini
//            dtParam.put("COPIAS","1");
            //le pasamos el parametro "OPERACION"  de momento
            //va a ser V en cualquier caso
//            dtParam.put("OPERACION","V");

            dtParam.put("LISTA_PUNTOS", lServ);
            dtParam.put("RUTA_FICH", this.panFichero.txtFile.getText());

            Lista lParam = new Lista();
            lParam.addElement(dtParam);

            crearDBF();
            this.getApp().getStub().setUrl("servlet/SrvCartasEtiq");
            Lista lResult = (Lista)this.getApp().getStub().doPost(
                servletCARTETIQ_AVISOS, lParam);

            /*                    SrvCartasEtiq srv = new SrvCartasEtiq();
                                // par�metros jdbc
                 srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                       "sive_desa",
                                       "sive_desa");
                 Lista lResult = srv.doDebug(servletCARTETIQ_AVISOS,lParam);*/

            this.getApp().showAdvise("Exportaci�n de fichero finalizada");
            vaciarPantalla();

          }
          catch (Exception ex) {
            this.getApp().trazaLog(ex);
            this.getApp().showError(ex.getMessage());
          }
        }
        Inicializar(CInicializar.NORMAL);
      } //end if isDataValid
    } //end btnAceptar

    //Lo comentado aqu� indica la creaci�n del DBF con servlets pero
//de momento esta parte se deja sin acabar y simplemente se muestran
//en cliente con la llamada al di�logo anterior
//            this.getApp().getStub().setUrl("servlet/SrvCartasEtiq");

    //Si usuario elige con etiquetas
    /*           if ( chConEti.getState() ) {
         this.getApp().getStub().doPost(servletCARTETIQ_AVISOS,lParam);
//              lResult = (Lista)this.getApp().getStub().doPost(servletCARTAS_ETIQUETAS_AVISOS,lParam);
                }*/
    /*****************************************************************/

    /*            else {
                  ObjectInputStream in = null;
                  ObjectOutputStream out = null;
                  String localiz= this.getApp().getParameter("URL_SERVLET")+
                                  "servlet/SrvCartasEtiq";
                  URL testServlet= new URL (localiz);
                  URLConnection con = testServlet.openConnection();
                  con.setDoInput(true);
                  con.setDoOutput(true);
                  con.setUseCaches(false);
                  con.setDefaultUseCaches(false);
         con.setRequestProperty("Content-type", "application/octet-stream");
                  out = new ObjectOutputStream(con.getOutputStream());
                  out.writeInt(servletCARTETIQ_AVISOS);
                  out.flush();
                  // envia los par�metros
                  if (lParam != null)
                    lParam.trimToSize();
                  out.writeObject(lParam);
                  out.flush();
                  out.close();*/

    /*****************************************************************/
    // lee los par�metros de retorno
//              in = new ObjectInputStream((InputStream) con.getInputStream());
//              objResult = in.readObject();

    // cierra los streams
//              in.close();

    // levanta la excepci�n producida en el servlet
    /*              if (objResult != null) {
         if (objResult.getClass().getName().equals("java.lang.Exception"))
                        throw (Exception) objResult;
                  }*/

    /*    Object vResultado = null;
         URLConnection con = null;
         ObjectInputStream in = null;
         ObjectOutputStream out = null;
         // abre la conexi�n con el servlet
         con = url.openConnection();
         con.setRequestProperty("Content-type", "application/octet-stream");
         con.setDoOutput(true);
         con.setUseCaches(false);
         // invoca el modo de operaci�n del servlet
         out = new ObjectOutputStream(con.getOutputStream());
         out.writeInt(opmode);
         out.flush();
         // envia los par�metros
         if (vParametros != null)
           vParametros.trimToSize();
         out.writeObject(vParametros);
         out.flush();
         out.close();
         // lee los par�metros de retorno
         in = new ObjectInputStream((InputStream) con.getInputStream());
         vResultado = in.readObject();
         // cierra los streams
         in.close();
         // levanta la excepci�n producida en el servlet
         if (vResultado != null) {
         if ((vResultado.getClass().getName().equals("java.lang.Exception")) ||
         (vResultado.getClass().getName().equals("sapp2.BlockException")))
             throw (Exception) vResultado;
         }
         return (vResultado);
//              this.getApp().getStub().doPost(servletCARTAS_AVISOS,lParam);
//              lResult = (Lista)this.getApp().getStub().doPost(servletCARTAS_AVISOS,lParam);
                 } */

//PARTE CLIENTE
    /*            //Si usuario elige con etiquetas
                if ( chConEti.getState() ) {
                  //modo 4->   CARTAS_ETIQUETAS_CON_AVISOS
         DiaCreacionDBF dia=new DiaCreacionDBF(this.getApp(),4,lParam);
                  dia.show();
                }
                else {
                  //modo 2->   CARTAS_CON_AVISOS
         DiaCreacionDBF dia=new DiaCreacionDBF(this.getApp(),2,lParam);
                  dia.show();
                }*/

    /*            //Si usuario elige con etiquetas
                 if ( chConEti.getState() ) {
      //modo 4->   CARTAS_ETIQUETAS_CON_AVISOS
      DiaCreacionDBF dia=new DiaCreacionDBF(this.getApp(),4,lParam);
      dia.show();
                 }
                 else {
      //modo 2->   CARTAS_CON_AVISOS
      DiaCreacionDBF dia=new DiaCreacionDBF(this.getApp(),2,lParam);
      dia.show();
                 }
     */
//Lo comentado aqu� indica la creaci�n del DBF con servlets pero
//de momento esta parte se deja sin acabar y simplemente se muestran
//en cliente con la llamada al di�logo anterior
    /*            this.getApp().getStub().setUrl("servlet/SrvCartasEtiq");
                //Si usuario elige con etiquetas
                if ( chConEti.getState() ) {
                  lResult = (Lista)this.getApp().getStub().doPost(servletCARTAS_ETIQUETAS_AVISOS,lParam);
                }
                else {
         lResult = (Lista)this.getApp().getStub().doPost(servletCARTAS_AVISOS,lParam);
                } */

  } //fin de btn_actionPerformed

  // Devuelve si se ha realizado la acci�n del bot�n o no
  public boolean bAceptar() {
    return bAceptar;
  }

}

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanImpCartas adaptee;
  ActionEvent e;

  DialogActionAdapter(PanImpCartas adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
