/**
 * Clase: DiaFRecep
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 10/02/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Notificaciones/Casos
 * Descripcion: Di�logo de modificaci�n de la FRecep de una NotifRMC
 */

package centinelas.cliente.c_not;

import java.awt.Checkbox;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CContextHelp;
import capp2.CDialog;
import capp2.CInicializar;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.SincrEventos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.centbasicos.NotifRMC;
import comun.CFechaSimple;
import comun.constantes;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class DiaFRecep
    extends CDialog
    implements CInicializar {

  /*************** Datos propios del panel ****************/

  // Parametros de entrada
  CApp applet = null;
  private NotifRMC nRMC; // NotifRMC a modificar

  // Vars. intermedias
  String fRecep = "";

  // Parametros de salida
  private boolean OK = false;
  private NotifRMC nRMCSal;

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  // Imagenes a cargar
  final String imgAceptar = "images/aceptar.gif";
  final String imgCancelar = "images/cancelar.gif";

  // Nombre del servlet
  final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

  /****************** Componentes del panel ********************/

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Organizaci�n del panel
  private XYLayout xYLayout1 = new XYLayout();

  // N�mero de Casos
  Label lblFRecep = null;
  CFechaSimple cfsFRecep = null;

  // Botones de salida
  private ButtonControl btnAceptar = new ButtonControl();
  private ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores
  DiaFRecepActionAdapter actionAdapter = null;

  // Constructor
  // @param a: CApp
  // @param modo: modo de entrada al di�logo
  // @param n: NotifRMC a modificar
  public DiaFRecep(CApp a, int modo, NotifRMC n) {

    super(a);
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    applet = a;
    nRMC = n;
    fRecep = nRMC.getFC_RECEP();

    // Titulo
    setTitle("Fecha Recepci�n");

    try {
      // Inicializaci�n
      jbInit();

      // Establecemos el valor inicial de la fecha de recepcion
      cfsFRecep.setText(fRecep);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicia el aspecto del dialogo DiaAltaNotifRMC
  public void jbInit() throws Exception {
    // Carga las imagenes
    getApp().getLibImagenes().put(constantes.imgACEPTAR);
    getApp().getLibImagenes().put(constantes.imgCANCELAR);
    getApp().getLibImagenes().CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaFRecepActionAdapter(this);

    // Organizacion del panel
    this.setSize(new Dimension(250, 150));
    this.setLayout(xYLayout1);
    xYLayout1.setWidth(250);
    xYLayout1.setHeight(150);

    // FRecep
    lblFRecep = new Label("Fecha Recepci�n:");
    cfsFRecep = new CFechaSimple("S");

    // Bot�n Aceptar
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(constantes.
        imgACEPTAR));
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.addActionListener(actionAdapter);

    // Bot�n Cancelar
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(constantes.
        imgCANCELAR));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);

    // Adici�n de componentes al di�logo
    this.add(lblFRecep, new XYConstraints(25, 25, 120, -1));
    this.add(cfsFRecep, new XYConstraints(145, 25, 80, -1));
    this.add(btnAceptar, new XYConstraints(55, 80, 80, -1));
    this.add(btnCancelar, new XYConstraints(145, 80, 80, -1));

    // Tool Tips
    new CContextHelp("Aceptar", btnAceptar);
    new CContextHelp("Cancelar", btnCancelar);
  }

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar()

  public void Inicializar() {
    switch (modoOperacion) {
      // Modo espera
      case CInicializar.ESPERA:
        cfsFRecep.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // Modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case constantes.modoALTA:
          case constantes.modoMODIFICACION:
            cfsFRecep.setEnabled(true);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
          case constantes.modoBAJA:
          case constantes.modoCONSULTA:
            cfsFRecep.setEnabled(false);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    } // Fin switch
  }

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  /******************** Funciones de mantenimiento ****************/

  // Recoge los datos existentes en los componentes del di�logo
  private NotifRMC recogerDatos() {

    nRMCSal = new NotifRMC(nRMC.getCD_ENFCIE(), nRMC.getCD_PCENTI(),
                           nRMC.getCD_MEDCEN(), nRMC.getCD_ANOEPI(),
                           nRMC.getCD_SEMEPI(), nRMC.getNM_CASOSDEC(),
                           nRMC.getFC_RECEP(), nRMC.getIT_COBERTURA(),
                           applet.getParameter("COD_USUARIO"), "");

    nRMCSal.put("CD_ENFCIE", nRMC.getCD_ENFCIE());
    nRMCSal.put("CD_PCENTI", nRMC.getCD_PCENTI());
    nRMCSal.put("CD_MEDCEN", nRMC.getCD_MEDCEN());
    nRMCSal.put("CD_ANOEPI", nRMC.getCD_ANOEPI());
    nRMCSal.put("CD_SEMEPI", nRMC.getCD_SEMEPI());
    nRMCSal.put("NM_CASOSDEC", nRMC.getNM_CASOSDEC());

    nRMCSal.put("FC_RECEP", cfsFRecep.getText());

    nRMCSal.put("IT_COBERTURA", nRMC.getIT_COBERTURA());
    nRMCSal.put("CD_OPE", applet.getParameter("COD_USUARIO"));
    nRMCSal.put("FC_ULTACT", "");

    return nRMCSal;
  } // Fin recogerDatos()

  // Rellena los componentes del di�logo con los datos existentes en la estructura
  private void rellenarDatos(String n) {
  } // Fin rellenarDatos()

  // Dev. true si los datos de los componentes del di�logo son v�lidos
  boolean validarDatos() {

    // Debe ser una fecha v�lida
    cfsFRecep.ValidarFecha();
    if (cfsFRecep.getValid().equals("N")) {
      return false;
    }

    // FRecep debe ser <= que la fecha actual
    if (Format.comparaFechas(cfsFRecep.getText(), Format.fechaActual()) == 1) {
      return false;
    }

    // Si llega aqui todo ha ido bien
    return true;
  } // Fin validarDatos()

  // Dev. true si son iguales ambas estructuras
  boolean compararCampos(String n1, String n2) {
    if (n1 == null || n2 == null) {
      return false;
    }

    if (!n1.equals(n2)) {
      return false;
    }

    return true;
  } // Fin compararCampos()

  /******************** Acceso a vars. salida *********************/

  // Devuelve true si se sale mediante el bot�n de Aceptar
  public boolean getAceptar() {
    return OK;
  } // Fin getAceptar()

  // Devuelve nRMCSal
  public NotifRMC getNotifRMC() {
    return nRMCSal;
  } // Fin getSinCasos()

  /******************** M�todos para el bloqueo *******************/

  // Devuelve una Lista cuyo primer elemento es la QueryTool del bloqueo
  //  y su segundo elemento es el Data del bloqueo
  private Lista prepararBloqueo() {
    Lista vResult = new Lista();
    QueryTool qtBloqueo = new QueryTool();
    Data dtBloqueo = new Data();

    // Tabala  a bloquear
    qtBloqueo.putName("SIVE_NOTIF_RMC");

    // Campos CD_OPE y FC_ULTACT
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Filtro del registro a bloquear
    qtBloqueo.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ENFCIE", nRMC.getCD_ENFCIE());
    qtBloqueo.putOperator("CD_ENFCIE", "=");
    qtBloqueo.putWhereType("CD_PCENTI", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_PCENTI", nRMC.getCD_PCENTI());
    qtBloqueo.putOperator("CD_PCENTI", "=");
    qtBloqueo.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_MEDCEN", nRMC.getCD_MEDCEN());
    qtBloqueo.putOperator("CD_MEDCEN", "=");
    qtBloqueo.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANOEPI", nRMC.getCD_ANOEPI());
    qtBloqueo.putOperator("CD_ANOEPI", "=");
    qtBloqueo.putWhereType("CD_SEMEPI", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_SEMEPI", nRMC.getCD_SEMEPI());
    qtBloqueo.putOperator("CD_SEMEPI", "=");

    // Valores del bloqueo
    dtBloqueo.put("CD_OPE", nRMC.getCD_OPE());
    dtBloqueo.put("FC_ULTACT", nRMC.getFC_ULTACT());

    // Devolucion
    vResult.addElement(qtBloqueo);
    vResult.addElement(dtBloqueo);
    return vResult;
  } // Fin prepararBloqueo()

  /******************** Funcs. Auxiliares ************************/

  // Determina si la fecha de recepci�n debe ser
  //  obligatoria: true:
  //  nula: false:
  boolean fechaRecepObligatoria(NotifRMC n) {
    if (n.getIT_COBERTURA().equals("N")) {
      // Si no es de cobertura: fecha obligatoria
      return true;
    }
    else {
      if (!n.getNM_CASOSDEC().equals("")) {

        // Si s� es de cobertura, y posee casos: fecha obligatoria
        return true;
      }
      else {

        // Si s� es de cobertura, y NO posee casos: fecha nula
        return false;
      }
    }
  } // Fin fechaRecepObligatoria()

  /******************** Manejadores *************************/

  void btnAceptarActionPerformed() {
    Checkbox chkb = null;
    QueryTool2 qt = null;
    Lista lBloqueo = null;
    Lista lQuery = new Lista();
    Lista lResult = null;

    if (compararCampos(cfsFRecep.getText(), fRecep)) {
      OK = false;
      dispose();
    }
    else {

      // Validez de la fecha
      if (!validarDatos()) {
        applet.showError(
            "La fecha de recepci�n debe ser menor que la fecha actual.");
        return;
      }

      // Recogida de datos
      nRMCSal = recogerDatos();

      // QueryTool
      qt = NotifRMC.getUpdateOneNotifRMC(nRMCSal);
      lQuery.addElement(qt);

      // Preparacion del bloqueo (QueryTool + Data)
      lBloqueo = prepararBloqueo();

      try {
        // Consulta el servidor
        lResult = BDatos.execSQLBloqueo(applet, servlet, 10006, lQuery,
                                        (QueryTool) lBloqueo.elementAt(0),
                                        (Data) lBloqueo.elementAt(1)); // 10006: DO_UPDATE_BLO

        /*// SOLODESARROLLO
                SrvQueryTool srv = new SrvQueryTool();
                lResult = BDatos.execSQLDebug(applet,srv,4,lQuery);*/

       // Se puede recoger el numero de registros modificados (lResult)
       //  y comprobar si realmente ha sido 1
        if (lResult.size() == 0) {
          throw (new Exception());
        }
        else {
          int rMod = ( (Integer) lResult.firstElement()).intValue();
          if (rMod != 1) {
            app.showError("No se ha modificado la Notificaci�n");
            throw (new Exception());
          }
        }

        // Actualizacion del CD_OPE y de la FC_ULTACT de nRMCSal
        nRMCSal.setCD_OPE(getApp().getParameter("COD_USUARIO"));
        nRMCSal.setFC_ULTACT(lResult.getFC_ULTACT());

        // Se ha modificado un NotifRMC
        OK = true;
        dispose();

      }
      catch (Exception e) {
        // Si sucede algo an�malo OK=false
        e.printStackTrace();
        applet.showError("No se ha podido modificar la notificaci�n");
        OK = false;
      } // Fin try .. catch

    } // Fin if(compararCampos)

  } // Fin btnAceptarActionPerformed()

  void btnCancelarActionPerformed() {
    // Se sale por cancelar
    OK = false;
    // Desaparece el dialogo
    dispose();
  } // Fin btnCancelarActionPerformed()

} // endclass DiaFRecep

/******************* ESCUCHADORES **********************/

// Botones
class DiaFRecepActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaFRecep adaptee;
  ActionEvent evt;

  DiaFRecepActionAdapter(DiaFRecep adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {

      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }

      s.desbloquea(adaptee);
    }
  }

} // endclass  DiaFRecepActionAdapter
