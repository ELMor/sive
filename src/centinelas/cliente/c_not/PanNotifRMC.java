/**
 * Clase: PanNotifRMC
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 17/01/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Notificaciones/Casos
 * Descripcion: Panel que contiene botones de acceso al di�logo
 *   de alta/mod/baja de una NotificacionRMC.
 * ESTE ES UN PANEL PARA PRUEBAS SOLAMENTE!!!!!!!!
 */

package centinelas.cliente.c_not;

import java.awt.Cursor;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CContextHelp;
import capp2.CInicializar;
import capp2.CPanel;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.SincrEventos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.centbasicos.MedCent;
import centinelas.datos.centbasicos.NotifRMC;
import centinelas.datos.centbasicos.PuntoCent;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool2;

public class PanNotifRMC
    extends CPanel
    implements CInicializar {

  // Nombre del servlet
  final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  // Modo de operaci�n
  public int modoOperacion = modoNORMAL;

  // Componentes
  XYLayout xYLayout1 = new XYLayout();
  ButtonControl btnAlta = new ButtonControl();
  ButtonControl btnMod = new ButtonControl();
  ButtonControl btnBaja = new ButtonControl();

  // constructor del panel PanNotifRMC
  public PanNotifRMC(CApp a) {

    try {
      setApp(a);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicia el aspecto del panel PanAltaNotifRMC
  public void jbInit() throws Exception {

    final String imgBuscar = "images/refrescar.gif";

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgBuscar);
    this.getApp().getLibImagenes().CargaImagenes();

    // Escuchadores de eventos
    PanNotifRMC_actionAdapter actionAdapter = new PanNotifRMC_actionAdapter(this);

    xYLayout1.setWidth(200);
    xYLayout1.setHeight(100);

    btnAlta.setActionCommand("alta");
    btnAlta.setImage(this.getApp().getLibImagenes().get(imgBuscar));
    btnAlta.setLabel("Alta");
    btnAlta.addActionListener(actionAdapter);

    btnMod.setActionCommand("mod");
    btnMod.setImage(this.getApp().getLibImagenes().get(imgBuscar));
    btnMod.setLabel("Mod");
    btnMod.addActionListener(actionAdapter);

    btnBaja.setActionCommand("baja");
    btnBaja.setImage(this.getApp().getLibImagenes().get(imgBuscar));
    btnBaja.setLabel("Baja");
    btnBaja.addActionListener(actionAdapter);

    this.setLayout(xYLayout1);
    this.add(btnAlta, new XYConstraints(15, 15, 70, 25));
    this.add(btnMod, new XYConstraints(15, 45, 70, 25));
    this.add(btnBaja, new XYConstraints(15, 75, 70, 25));

    // tool tips
    new CContextHelp("Alta NotifRMC", btnAlta);
    new CContextHelp("Mod NotifRMC", btnMod);
    new CContextHelp("Baja NotifRMC", btnBaja);

  }

  public void Inicializar() {}

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;
    }
  }

  String ano = "";
  String sem = "";
  PuntoCent p = null;
  MedCent m = null;
  String enf = "";
  Lista l = new Lista();
  NotifRMC nRMC = null;
  Lista lSexo = null;

  private void setDatos() {
    ano = "1999";
    sem = "01";

    p = new PuntoCent();
    p.put("CD_PCENTI", "000001");
    p.put("DS_PCENTI", "Pto centinela 1");

    m = new MedCent();
    m.put("CD_PCENTI", "000001");
    m.put("CD_MEDCEN", "000001");
    m.put("CD_E_NOTIF", "H01A");
    m.put("DS_APE1", "Rivera");

    enf = "Disenter�a";

    // Lista de enfermedades a no representar
    Data e1 = new Data();
    Data e2 = new Data();
    l = new Lista();
    e1.put("CD_ENFCIE", "023");
    l.addElement(e1);
    e2.put("CD_ENFCIE", "032");
    l.addElement(e2);

    // Lista sexos
    lSexo = new Lista();
    Data s1 = new Data();
    s1.put("CD_SEXO", "1");
    s1.put("DS_SEXO", "Var�n");
    Data s2 = new Data();
    s2.put("CD_SEXO", "6");
    s2.put("DS_SEXO", "Mujer");
    Data s3 = new Data();
    s3.put("CD_SEXO", "9");
    s3.put("DS_SEXO", "Noconsta");
    lSexo.addElement(s1);
    lSexo.addElement(s2);
    lSexo.addElement(s3);
  }

  void setDatosMod() {
    Lista lQuery = new Lista();
    QueryTool2 qt = NotifRMC.getOneNotifRMC("004", "000002", "000002", "1998",
                                            "01");
    //QueryTool2 qt = NotifRMC.getOneNotifRMC("023","000002","000002","1997","01");
    lQuery.addElement(qt);
    Lista lResult = null;

    try {
      // Traemos la NotifRMC
      lResult = BDatos.execSQL(getApp(), servlet, 1, lQuery); // 1: DO_SELECT
    }
    catch (Exception e) {

    }

    // Recogemos el NotifRMC
    nRMC = new NotifRMC( (Data) lResult.firstElement());

    // Punto centinela
    p = new PuntoCent();
    p.put("CD_PCENTI", "000002");
    p.put("DS_PCENTI", "Pto centinela 2");

    // Medico centinela
    m = new MedCent();
    m.put("CD_PCENTI", "000002");
    m.put("CD_MEDCEN", "000002");
    m.put("CD_E_NOTIF", "H01A");
    m.put("DS_APE1", "Rivera");

    // Descripcion enfermedad
    enf = "Disenter�a";

    // Lista sexos
    lSexo = new Lista();
    Data s1 = new Data();
    s1.put("CD_SEXO", "1");
    s1.put("DS_SEXO", "Var�n");
    Data s2 = new Data();
    s2.put("CD_SEXO", "6");
    s2.put("DS_SEXO", "Mujer");
    Data s3 = new Data();
    s3.put("CD_SEXO", "9");
    s3.put("DS_SEXO", "Noconsta");
    lSexo.addElement(s1);
    lSexo.addElement(s2);
    lSexo.addElement(s3);
  }

  // Bot�n Alta
  void btnAltaActionPerformed() {

    setDatos();

    // Creacion y muestra del dialogo para elegir una enfermedad
    DiaAltaNotifRMC danr = new DiaAltaNotifRMC(this.getApp(), ano, sem, p, m, l);

    danr.show();

  } // Fin btnAltaActionPerformed()

  // Bot�n Mod
  void btnModActionPerformed() {

    setDatosMod();

    // Creacion y muestra del dialogo para elegir una enfermedad
    DiaModBajaNotifRMC dmbnr = new DiaModBajaNotifRMC(this.getApp(),
        constantes.modoMODIFICACION,
        p.getDS_PCENTI(),
        m.getDS_APE1(),
        enf, nRMC, lSexo);
    dmbnr.show();

  } // Fin btnModActionPerformed

  // Bot�n Baja
  void btnBajaActionPerformed() {

    setDatosMod();

    // Creacion y muestra del dialogo para elegir una enfermedad
    DiaModBajaNotifRMC dmbnr = new DiaModBajaNotifRMC(this.getApp(),
        constantes.modoBAJA,
        p.getDS_PCENTI(),
        m.getDS_APE1(),
        enf, nRMC, lSexo);
    dmbnr.show();

  } // Fin btnBajaActionPerformed()

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

}

// botones de centro, almac�n y buscar
class PanNotifRMC_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanNotifRMC adaptee;
  ActionEvent evt;

  PanNotifRMC_actionAdapter(PanNotifRMC adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if (evt.getActionCommand().equals("alta")) {
        adaptee.btnAltaActionPerformed();
      }
      else if (evt.getActionCommand().equals("mod")) {
        adaptee.btnModActionPerformed();
      }
      else if (evt.getActionCommand().equals("baja")) {
        adaptee.btnBajaActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass PanAltaNotifRMC_actionAdapter
