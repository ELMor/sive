/**
 * Clase: DiaModBajaNotifRMC
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 19/01/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Notificaciones/Casos
 * Descripcion: Di�logo de mod/baja de una NotificacionRMC
 */

package centinelas.cliente.c_not;

import java.util.Enumeration;
import java.util.Vector;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CContextHelp;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.SincrEventos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.centbasicos.CasoCent;
import centinelas.datos.centbasicos.NotifRMC;
import centinelas.datos.comun.Fechas;
import comun.constantes;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class DiaModBajaNotifRMC
    extends CDialog
    implements CInicializar, CFiltro {

  /*************** Datos propios del panel ****************/

  // Parametros de entrada
  CApp applet = null;
  String ano = null;
  String sem = null;
  String pCent = null; // Desc. Punto Centinela
  String mCent = null; // Desc. M�dico Centinela
  String eCent = null; // Desc. Enfermedad Centinela
  NotifRMC nRMC = null; // NotifRMC
  Lista lSexo = null; // Lista Sexos

  // Vars. intermedias
  int iNumCasos; // Cuenta del n�mero de casos (-1: null)
  boolean bSinCasos = false; // NM_CASOSDEC es 0 o null
  boolean nRMCInCobertura = false; // Est� la notificaci�n en cobertura ?

  // Parametros de salida
  private NotifRMC nRMCSal = null;

  /*************** Constantes ****************/

  // Modo de operaci�n y Modo entrada
  public int modoOperacion = CInicializar.NORMAL;
  public int modoEntrada;

  // Imagenes a cargar
  final String imgAceptar = "images/aceptar.gif";
  final String imgCancelar = "images/cancelar.gif";

  // realizaOperacion()
  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  // Nombre del servlet
  final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

  /****************** Componentes del panel ********************/

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Organizaci�n del panel
  private XYLayout xYLayout1 = new XYLayout();

  // P.Centinela
  Label lblPCent = null;
  Label lblDsPCent = null;

  // M.Centinela
  Label lblMCent = null;
  Label lblDsMCent = null;

  // A�o/Sem
  Label lblAnoSem = null;
  Label lblDsAnoSem = null;

  // Fecha Recepci�n
  Label lblFRecep = null;
  Label lblFRecep2 = null;
  ButtonControl btnFRecep = null;

  // Enfermedad
  Label lblEnf = null;
  Label lblDsEnf = null;

  // Numero de Casos
  Label lblNCasos = null;
  Label lblNCasos2 = null;
  ButtonControl btnSinCasos = null;

  // CLista Mantenimiento de CasosCent
  CListaMantenimiento clmCasosCent = null;

  // Botones de salida
  private ButtonControl btnSalir = new ButtonControl();
  private ButtonControl btnAceptar = new ButtonControl();
  private ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores
  DiaModBajaNotifRMCActionAdapter actionAdapter = null;

  // Constructor
  // @param a: CApp
  // @param modo: modo de entrada al di�logo (constantes.*)
  // @param punto: Descripci�n del punto centinela
  // @param med: Descripci�n  del m�dico centinela
  // @param enf: Descripci�n de la enfermedad
  // @param n: NotifRMC de entrada
  // @param listaSexo: Lista Datas con CD y DS de los sexos
  public DiaModBajaNotifRMC(CApp a, int modo, String punto, String med,
                            String enf, NotifRMC n, Lista listaSexo) {

    super(a);
    applet = a;
    modoOperacion = CInicializar.NORMAL;
    modoEntrada = modo;
    ano = n.getCD_ANOEPI();
    sem = n.getCD_SEMEPI();
    pCent = punto;
    mCent = med;
    eCent = enf;
    nRMC = n;
    lSexo = listaSexo;
    // N�mero de casos
    if (n.getNM_CASOSDEC().equals("")) {
      iNumCasos = -1;
    }
    else {
      iNumCasos = Integer.parseInt(n.getNM_CASOSDEC());

      // Notificaci�n en cobertura
    }
    if (nRMC.getIT_COBERTURA().equals("S")) {
      nRMCInCobertura = true;
      bSinCasos = true;
    }

    switch (modoEntrada) {
      case constantes.modoMODIFICACION:
        setTitle("Modificaci�n Notificaci�n RMC");
        break;
      case constantes.modoBAJA:
        setTitle("Baja Notificaci�n RMC");
        break;
      case constantes.modoCONSULTA:
        setTitle("Consulta Notificaci�n RMC");
        break;
    }

    try {
      // Inicializaci�n
      jbInit();

      // Relleno de los campos de la NotifRMC (Fecha Recepcion)
      rellenarDatos(nRMC);

      // Determina si la notificacion se puede poner con NM_CASOSDEC=null
      if (iNumCasos > 0) {
        bSinCasos = false;

        // Primera trama de casos centinelas (si hay casos)
      }
      if (iNumCasos > 0) {
        clmCasosCent.setPrimeraPagina(this.primeraPagina());

      }
      Inicializar(modoOperacion);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicia el aspecto del dialogo DiaAltaNotifRMC
  public void jbInit() throws Exception {
    // Botones y etiquetas para un ClistaMantenimiento
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    // Carga las imagenes
    getApp().getLibImagenes().put(constantes.imgACEPTAR);
    getApp().getLibImagenes().put(constantes.imgCANCELAR);
    getApp().getLibImagenes().put(constantes.imgSALIR);
    getApp().getLibImagenes().CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaModBajaNotifRMCActionAdapter(this);

    // Organizacion del panel
    this.setSize(new Dimension(745, 480));
    this.setLayout(xYLayout1);
    xYLayout1.setWidth(745);
    xYLayout1.setHeight(480);

    // P.Centinela
    lblPCent = new Label("P. Centinela:");
    lblDsPCent = new Label(pCent);

    // M.Centinela
    lblMCent = new Label("M�dico:");
    lblDsMCent = new Label(mCent);

    // A�o/Sem
    lblAnoSem = new Label("A�o/Semana:");
    lblDsAnoSem = new Label(ano + "/" + sem);

    // Fecha Recepci�n
    lblFRecep = new Label("Fecha Recepci�n:");
    lblFRecep2 = new Label();

    // Bot�n FRecep
    btnFRecep = new ButtonControl("Modificar");
    btnFRecep.setEnabled(false);
    btnFRecep.setActionCommand("FRecep");
    btnFRecep.addActionListener(actionAdapter);

    // Enfermedad
    lblEnf = new Label("Enfermedad:");
    lblDsEnf = new Label(eCent);

    // NCasos
    lblNCasos = new Label("N�mero Casos: ");
    lblNCasos2 = new Label();

    // Boton Sin Casos
    btnSinCasos = new ButtonControl("Sin Casos");
    btnSinCasos.setEnabled(false);
    btnSinCasos.setActionCommand("SinCasos");
    btnSinCasos.addActionListener(actionAdapter);

    // CLista Mantenimiento de CasosCent
    // CListaMantenimiento: Botones: sin botones
    vBotones.addElement(new CBoton("",
                                   "images/alta2.gif",
                                   "Nueva petici�n",
                                   false,
                                   false));

    vBotones.addElement(new CBoton("",
                                   constantes.imgMODIFICAR2,
                                   "Modificar petici�n",
                                   true,
                                   true));

    vBotones.addElement(new CBoton("",
                                   constantes.imgBAJA2,
                                   "Borrar petici�n",
                                   false,
                                   true));

    // CListaMantenimiento: Etiquetas
    vLabels.addElement(new CColumna("Paciente",
                                    "150",
                                    "NM_ORDEN"));
    vLabels.addElement(new CColumna("Edad",
                                    "150",
                                    "EDAD"));
    vLabels.addElement(new CColumna("Fecha Nacimiento",
                                    "150",
                                    "FC_NAC"));
    vLabels.addElement(new CColumna("Sexo",
                                    "150",
                                    "SEXO"));

    /*
        // CListaMantenimiento: Panel con los CasosCent
        clmCasosCent = new CListaMantenimiento(applet,
                                               vLabels,
                                               vBotones,
                                               this,
                                               this);
     */
    clmCasosCent = new CListaMantenimiento(applet,
                                           vLabels,
                                           vBotones,
                                           this,
                                           this,
                                           250,
                                           640);

    if (modoEntrada == constantes.modoBAJA) {
      // Bot�n Aceptar
      btnAceptar.setLabel("Aceptar");
      btnAceptar.setImage(this.getApp().getLibImagenes().get(constantes.
          imgACEPTAR));
      btnAceptar.setActionCommand("Aceptar");
      btnAceptar.addActionListener(actionAdapter);

      // Bot�n Cancelar
      btnCancelar.setLabel("Cancelar");
      btnCancelar.setImage(this.getApp().getLibImagenes().get(constantes.
          imgCANCELAR));
      btnCancelar.setActionCommand("Cancelar");
      btnCancelar.addActionListener(actionAdapter);
    }
    else {
      // Bot�n Salir
      btnSalir.setLabel("Salir");
      btnSalir.setImage(this.getApp().getLibImagenes().get(constantes.imgSALIR));
      btnSalir.setActionCommand("Salir");
      btnSalir.addActionListener(actionAdapter);
    }

    // Adici�n de componentes al di�logo
    this.add(lblPCent, new XYConstraints(15, 15, 75, -1));
    this.add(lblDsPCent, new XYConstraints(100, 15, 150, -1));
    this.add(lblMCent, new XYConstraints(260, 15, 45, -1));
    this.add(lblDsMCent, new XYConstraints(315, 15, 250, -1));
    this.add(lblAnoSem, new XYConstraints(575, 15, 75, -1));
    this.add(lblDsAnoSem, new XYConstraints(660, 15, 50, -1));
    this.add(lblNCasos, new XYConstraints(15, 45, 100, -1));
    this.add(lblNCasos2, new XYConstraints(130, 45, 80, -1));
    this.add(btnSinCasos, new XYConstraints(220, 45, 70, -1));
    this.add(lblFRecep, new XYConstraints(15, 75, 100, -1));
    this.add(lblFRecep2, new XYConstraints(130, 75, 80, -1));
    this.add(btnFRecep, new XYConstraints(220, 75, 70, -1));
    this.add(lblEnf, new XYConstraints(370, 75, 75, -1));
    this.add(lblDsEnf, new XYConstraints(450, 75, 250, -1));
    this.add(clmCasosCent, new XYConstraints(6, 110, -1, -1));

    if (modoEntrada == constantes.modoBAJA) {
      this.add(btnAceptar, new XYConstraints(537, 415, 80, -1));
      this.add(btnCancelar, new XYConstraints(627, 415, 80, -1));
    }
    else {
      this.add(btnSalir, new XYConstraints(627, 415, 80, -1));
    }

    // Tool Tips
    new CContextHelp("Sin Casos", btnSinCasos);
    if (modoEntrada == constantes.modoBAJA) {
      new CContextHelp("Aceptar", btnAceptar);
      new CContextHelp("Cancelar", btnCancelar);
    }
    else {
      new CContextHelp("Salir", btnSalir);
    }
  }

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  } // Fin Inicializar()

  public void Inicializar() {
    switch (modoOperacion) {
      // Modo espera
      case CInicializar.ESPERA:
        enableControls(false);
        if (modoEntrada == constantes.modoBAJA) {
          btnAceptar.setEnabled(false);
          btnCancelar.setEnabled(false);
        }
        else {
          btnSalir.setEnabled(false);
        }
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        // Modo entrada
      case CInicializar.NORMAL:
        switch (modoEntrada) {
          case constantes.modoMODIFICACION:
            enableControls(true);
            btnSalir.setEnabled(true);
            break;
          case constantes.modoBAJA:
            enableControls(false);
            btnAceptar.setEnabled(true);
            btnCancelar.setEnabled(true);
            break;
          case constantes.modoCONSULTA:
            enableControls(false);
            btnSalir.setEnabled(true);
            break;
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    } // Fin switch
  }

  public void enableControls(boolean state) {
    // Boton de Fecha de Recepci�n
    boolean bEnabledFR = fechaRecepObligatoria(nRMC);
    btnFRecep.setEnabled(state && bEnabledFR);
    btnSinCasos.setEnabled(state && bSinCasos);
    clmCasosCent.setEnabled(state);
  } // Fin enableControls()

  // Determina si la fecha de recepci�n debe ser
  //  obligatoria: true:
  //  nula: false:
  boolean fechaRecepObligatoria(NotifRMC n) {
    if (n.getIT_COBERTURA().equals("N")) {
      // Si no es de cobertura: fecha obligatoria
      return true;
    }
    else {
      if (!n.getNM_CASOSDEC().equals("")) {

        // Si s� es de cobertura, y posee casos: fecha obligatoria
        return true;
      }
      else {

        // Si s� es de cobertura, y NO posee casos: fecha nula
        return false;
      }
    }
  } // Fin fechaRecepObligatoria()

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  /******************** M�todos interfaz CFiltro ****************/

  // Regenera una lista de Datas CasoCent, a�adiendo campos necesarios
  //  a sus Datas para su representacion en la tabla
  public Lista regeneraLista(Lista l) {
    CasoCent c = null;
    Data dt = null;
    int i = 0;

    for (Enumeration e = l.elements(); e.hasMoreElements(); i++) {
      // Elemento a tratar
      dt = (Data) e.nextElement();

      // CasoCent para poder acceder a sus campos
      c = new CasoCent(dt);

      // Calculo de la edad
      String edad = "";
      if (!c.getFC_NAC().equals("")) {
        edad += Fechas.edadAnios(Format.string2Date(c.getFC_NAC()));
      }
      c.put("EDAD", edad);

      // Establecimiento del sexo
      c.put("SEXO", lSexo.getDSData("CD_SEXO", c.getCD_SEXO(), "DS_SEXO"));

      // Modificaci�n de Data por el CasoCent
      l.setElementAt(c, i);

    }

    return l;
  } // Fin regeneraLista()

  // Solicita la primera trama de datos
  public Lista primeraPagina() {

    // Lista con las peticiones
    Lista lCasos = null;

    // QueryTool y lista para la query
    Lista lQuery = new Lista();
    QueryTool2 qtCasosCent = CasoCent.getCasoCentPEMAS(nRMC.getCD_PCENTI(),
        nRMC.getCD_ENFCIE(),
        nRMC.getCD_MEDCEN(),
        nRMC.getCD_ANOEPI(),
        nRMC.getCD_SEMEPI());

    lQuery.addElement(qtCasosCent);
    lQuery.setTrama("NM_ORDEN", "");

    Inicializar(CInicializar.ESPERA);

    // Consulta al servlet
    try {
      this.getApp().getStub().setUrl(servlet);
      lCasos = BDatos.execSQL(applet, servlet, 2, lQuery); // 2: GET_PAGE

      // SOLO DESARROLLO
      /*SrvQueryTool srv = new SrvQueryTool();
           lCasos = BDatos.execSQLDebug(applet,srv,2,lQuery); // 2: GET_PAGE*/

      // Lista vacia
      if (lCasos.size() == 0) {
        this.getApp().showAdvise("No hay casos con estos criterios.");
      }

      // Error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    // La lista debe estar creada
    if (lCasos == null) {
      lCasos = new Lista();

      // Vuelta al estado normal
    }
    Inicializar(CInicializar.NORMAL);

    // Devoluci�n de la lista de casos
    return regeneraLista(lCasos);
  } // Fin primeraPagina()

  // Solicita la siguiente trama de datos
  public Lista siguientePagina() {

    // Lista con las peticiones
    Lista lCasos = null;

    // QueryTool y lista para la query
    Lista lQuery = new Lista();
    QueryTool2 qtCasosCent = CasoCent.getCasoCentPEMAS(nRMC.getCD_PCENTI(),
        nRMC.getCD_ENFCIE(),
        nRMC.getCD_MEDCEN(),
        nRMC.getCD_ANOEPI(),
        nRMC.getCD_SEMEPI());
    lQuery.addElement(qtCasosCent);
    lQuery.setTrama(this.clmCasosCent.getLista().getTrama());

    Inicializar(CInicializar.ESPERA);

    // Consulta al servlet
    try {
      this.getApp().getStub().setUrl(servlet);
      lCasos = BDatos.execSQL(applet, servlet, 2, lQuery); // 2: GET_PAGE

      // SOLO DESARROLLO
      /*SrvQueryTool srv = new SrvQueryTool();
           lCasos = BDatos.execSQLDebug(applet,srv,2,lQuery); // 2: GET_PAGE*/

      // Lista vacia
      if (lCasos.size() == 0) {
        this.getApp().showAdvise("No hay casos con estos criterios.");
      }

      // Error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    // La lista debe estar creada
    if (lCasos == null) {
      lCasos = new Lista();

      // Vuelta al estado normal
    }
    Inicializar(CInicializar.NORMAL);

    // Devoluci�n de la lista de casos
    return regeneraLista(lCasos);
  } // Fin siguientePagina()

  // Operaciones de la botonera
  public void realizaOperacion(int j) {
    DiaCasoCent dcc = null;
    CasoCent cCent = new CasoCent();

    switch (j) {
      case ALTA:
        dcc = new DiaCasoCent(getApp(), constantes.modoALTA, eCent, nRMC, cCent,
                              0, iNumCasos, lSexo);
        dcc.show();
        if (dcc.getAceptar()) {
          // Cuenta de los casos se incr. en 1
          if (iNumCasos == -1 || iNumCasos == 0) {
            iNumCasos = 1;
            bSinCasos = false;
          }
          else {
            iNumCasos += 1;

            // Actualizaci�n del nRMC inicial
          }
          nRMC = dcc.getNotifRMC();

          // Numero de casos
          if (nRMC.getNM_CASOSDEC().equals("")) {
            lblNCasos2.setText("Indeterminado");
          }
          else {
            lblNCasos2.setText(nRMC.getNM_CASOSDEC());

            // FRecep
          }
          lblFRecep2.setText(nRMC.getFC_RECEP());

          // Se establece la primera p�gina de casos
          clmCasosCent.setPrimeraPagina(primeraPagina());
        }
        break;

      case MODIFICACION:
        cCent = new CasoCent( (Data) clmCasosCent.getSelected());
        if (cCent != null) {
          int ind = clmCasosCent.getSelectedIndex();
          dcc = new DiaCasoCent(getApp(), constantes.modoMODIFICACION, eCent,
                                nRMC, cCent, ind + 1, iNumCasos, lSexo);
          dcc.show();
          if (dcc.getAceptar()) {
            cCent = dcc.getCasoCent();
            clmCasosCent.getLista().setElementAt(cCent, ind);
            // No la vaciamos, porque este m�todo lo hace ya..
            clmCasosCent.setPrimeraPagina(regeneraLista(clmCasosCent.getLista()));

            // Actualizaci�n del nRMC inicial
            nRMC = dcc.getNotifRMC();
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

      case BAJA:
        cCent = new CasoCent( (Data) clmCasosCent.getSelected());
        if (cCent != null) {
          int ind = clmCasosCent.getSelectedIndex();
          dcc = new DiaCasoCent(getApp(), constantes.modoBAJA, eCent, nRMC,
                                cCent, ind + 1, iNumCasos, lSexo);
          dcc.show();
          if (dcc.getAceptar()) {
            // Cuenta de los casos se decr. en 1
            iNumCasos -= 1;
            if (iNumCasos == -1 || iNumCasos == 0) {
              if (nRMCInCobertura) {
                bSinCasos = true;

                // Actualizaci�n del nRMC inicial
              }
            }
            nRMC = dcc.getNotifRMC();

            // Numero de casos
            if (nRMC.getNM_CASOSDEC().equals("")) {
              lblNCasos2.setText("Indeterminado");
            }
            else {
              lblNCasos2.setText(nRMC.getNM_CASOSDEC());

              // Eliminaci�n del caso
            }
            clmCasosCent.getLista().removeElementAt(ind);
            clmCasosCent.setPrimeraPagina(clmCasosCent.getLista());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        } // Fin switch
        break;
    }

  } // Fin realizaOperacion()

  /******************** Funciones de mantenimiento ****************/

  // Recoge los datos existentes en los componentes del di�logo
  private NotifRMC recogerDatos() {
    NotifRMC nResul = new NotifRMC();

    nResul.put("CD_ENFCIE", nRMC.getCD_ENFCIE());
    nResul.put("CD_PCENTI", nRMC.getCD_PCENTI());
    nResul.put("CD_MEDCEN", nRMC.getCD_MEDCEN());
    nResul.put("CD_ANOEPI", nRMC.getCD_ANOEPI());
    nResul.put("CD_SEMEPI", nRMC.getCD_SEMEPI());
    if (iNumCasos == -1) {
      nResul.put("NM_CASOSDEC", "");
    }
    else {
      nResul.put("NM_CASOSDEC", "" + iNumCasos);
    }
    nResul.put("FC_RECEP", "");
    nResul.put("IT_COBERTURA", nRMC.getIT_COBERTURA());
    nResul.put("CD_OPE", getApp().getParameter("COD_USUARIO"));
    nResul.put("FC_ULTACT", "");

    return nResul;
  } // Fin recogerDatos()

  // Rellena los componentes del di�logo con los datos existentes en la estructura
  private void rellenarDatos(NotifRMC n) {
    // Fecha de recepci�n
    lblFRecep2.setText(n.getFC_RECEP());

    // Numero de casos
    if (n.getNM_CASOSDEC().equals("")) {
      lblNCasos2.setText("Indeterminado");
    }
    else {
      lblNCasos2.setText(n.getNM_CASOSDEC());
    }
  } // Fin rellenarDatos()

  // Dev. true si los datos de los componentes del di�logo son v�lidos
  boolean validarDatos() {
    // 1. PERDIDAS DE FOCO (Campos CD-Button-DS): No hay

    // 2. LONGITUDES: No hay

    // 3. DATOS NUMERICOS: No hay

    // 4. CAMPOS OBLIGATORIOS: No hay

    // 5. FECHAS
    /*if(!cfFRecep.getText().equals("")){
      cfFRecep.ValidarFecha();
      if(cfFRecep.getValid().equals("N"))
        return false;
         }*/

    // Si llega aqui todo ha ido bien
    return true;
  } // Fin validarDatos()

  // Dev. true si son iguales ambas estructuras
  boolean compararCampos(NotifRMC n1, NotifRMC n2) {
    if (n1 == null || n2 == null) {
      return false;
    }

    if (!n1.getCD_ENFCIE().equals(n2.getCD_ENFCIE())) {
      return false;
    }

    if (!n1.getCD_PCENTI().equals(n2.getCD_PCENTI())) {
      return false;
    }

    if (!n1.getCD_MEDCEN().equals(n2.getCD_MEDCEN())) {
      return false;
    }

    if (!n1.getCD_ANOEPI().equals(n2.getCD_ANOEPI())) {
      return false;
    }

    if (!n1.getCD_SEMEPI().equals(n2.getCD_SEMEPI())) {
      return false;
    }

    if (!n1.getNM_CASOSDEC().equals(n2.getNM_CASOSDEC())) {
      return false;
    }

    if (!n1.getFC_RECEP().equals(n2.getFC_RECEP())) {
      return false;
    }

    if (!n1.getIT_COBERTURA().equals(n2.getIT_COBERTURA())) {
      return false;
    }

    return true;
  } // Fin compararCampos()

  /******************** M�todos para el bloqueo *******************/

  // Devuelve una Lista cuyo primer elemento es la QueryTool del bloqueo
  //  y su segundo elemento es el Data del bloqueo
  private Lista prepararBloqueo() {
    Lista vResult = new Lista();
    QueryTool qtBloqueo = new QueryTool();
    Data dtBloqueo = new Data();

    // Tabala  a bloquear
    qtBloqueo.putName("SIVE_NOTIF_RMC");

    // Campos CD_OPE y FC_ULTACT
    qtBloqueo.putType("CD_OPE", QueryTool.STRING);
    qtBloqueo.putType("FC_ULTACT", QueryTool.TIMESTAMP);

    // Filtro del registro a bloquear
    qtBloqueo.putWhereType("CD_ENFCIE", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ENFCIE", nRMC.getCD_ENFCIE());
    qtBloqueo.putOperator("CD_ENFCIE", "=");
    qtBloqueo.putWhereType("CD_PCENTI", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_PCENTI", nRMC.getCD_PCENTI());
    qtBloqueo.putOperator("CD_PCENTI", "=");
    qtBloqueo.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_MEDCEN", nRMC.getCD_MEDCEN());
    qtBloqueo.putOperator("CD_MEDCEN", "=");
    qtBloqueo.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_ANOEPI", nRMC.getCD_ANOEPI());
    qtBloqueo.putOperator("CD_ANOEPI", "=");
    qtBloqueo.putWhereType("CD_SEMEPI", QueryTool.STRING);
    qtBloqueo.putWhereValue("CD_SEMEPI", nRMC.getCD_SEMEPI());
    qtBloqueo.putOperator("CD_SEMEPI", "=");

    // Valores del bloqueo
    dtBloqueo.put("CD_OPE", nRMC.getCD_OPE());
    dtBloqueo.put("FC_ULTACT", nRMC.getFC_ULTACT());

    // Devolucion
    vResult.addElement(qtBloqueo);
    vResult.addElement(dtBloqueo);
    return vResult;
  } // Fin prepararBloqueo()

  /******************** Acceso a vars. salida *********************/

  // Devuelve el NotifRMC dado de alta , o null si no se ha dado de alta ninguno
  public NotifRMC getNotifRMC() {
    return nRMCSal;
  } // Fin getNotifRMC()

  /******************** Manejadores *************************/

  void btnAceptarActionPerformed() {
    nRMCSal = nRMC;
    QueryTool2 qt = null;
    Lista lQuery = new Lista();

    // modoBAJA
    if (modoEntrada == constantes.modoBAJA) {
      Lista lResul;

      // QueryTool
      qt = NotifRMC.getDeleteOneNotifRMC(nRMC);
      lQuery.addElement(qt);

      // Transacci�n con el servidor
      try {
        lResul =
            BDatos.ejecutaSQL(false, applet, servlet, 5, lQuery); // 5: DO_DELETE
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
        this.getApp().showError(ex.getMessage());
      }
    }

    // Desaparece el dialogo
    dispose();
  } // Fin btnAceptarActionPerformed()

  void btnCancelarActionPerformed() {
    nRMCSal = nRMC;
    // Desaparece el dialogo
    dispose();
  } // Fin btnCancelarActionPerformed()

  void btnSalirActionPerformed() {
    nRMCSal = nRMC;
    // Desaparece el dialogo
    dispose();
  } // Fin btnSalirActionPerformed()

  void btnSinCasosActionPerformed() {
    String nCas;
    DiaSinCasos dsc = null;

    // Mostramos el di�logo de SinCasos con el valor actual de NM_CASOSDEC,
    //  y recogemos el valor puesto, estableciendo iNumCasos
    if (iNumCasos == 0) {
      nCas = "Cero";
    }
    else {
      nCas = "Indeterminado";
    }
    dsc = new DiaSinCasos(getApp(), constantes.modoMODIFICACION, nRMC);
    dsc.show();
    if (dsc.getAceptar()) {
      nCas = dsc.getSinCasos();

      // Actualizaci�n del nRMC inicial
      nRMC = dsc.getNotifRMC();

      // Actualizar la etiqueta de n�mero de casos
      if (nRMC.getNM_CASOSDEC().equals("")) {
        lblNCasos2.setText("Indeterminado");
      }
      else {
        lblNCasos2.setText(nRMC.getNM_CASOSDEC());

        // Actualizar la etiqueta de FRecep
      }
      lblFRecep2.setText(nRMC.getFC_RECEP());

      if (nCas.equals("Cero")) {
        iNumCasos = 0;
      }
      else {
        iNumCasos = -1;
      }

      // Se sale de la del dialogo de la notificacion
      dispose();
    } // Fin getAceptar()
  } // Fin btnSinCasosActionPerformed()

  void btnFRecepActionPerformed() {
    DiaFRecep dfr = null;

    // Mostramos el di�logo de FRecep con el valor actual de la fecha,
    //  y recogemos el valor puesto
    dfr = new DiaFRecep(getApp(), constantes.modoMODIFICACION, nRMC);
    dfr.show();
    if (dfr.getAceptar()) {
      // Actualizaci�n del nRMC inicial
      nRMC = dfr.getNotifRMC();

      // Actualizar la etiqueta de FRecep
      lblFRecep2.setText(nRMC.getFC_RECEP());
    } // Fin getAceptar()
  } // Fin btnFRecepActionPerformed()

} // endclass DiaModBajaNotifRMC

/******************* ESCUCHADORES **********************/

// Botones
class DiaModBajaNotifRMCActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaModBajaNotifRMC adaptee;
  ActionEvent evt;

  DiaModBajaNotifRMCActionAdapter(DiaModBajaNotifRMC adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Salir")) {
        adaptee.btnSalirActionPerformed();
      }
      else if (evt.getActionCommand().equals("SinCasos")) {
        adaptee.btnSinCasosActionPerformed();
      }
      else if (evt.getActionCommand().equals("FRecep")) {
        adaptee.btnFRecepActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  DiaModBajaNotifRMCActionAdapter
