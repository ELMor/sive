/**
 * Clase: DiaSelecSem
 * Hereda:
 * Autor: Jos� M� Torrecilla P�rez (JMT)
 * Fecha Inicio: 12/01/2000
 * Analisis Funcional: Punto 1. Mantenimiento de Notificaciones/Casos
 * Descripcion: Di�logo que muestra las semanas epidemiol�gicas con notificaciones
     * de un punto, m�dico, a�o determinado.Cuando el usuario selecciona una semana,
 * se devuelven todas las notificaiones de esa semana epidemiol�gica
 */

package centinelas.cliente.c_not;

import java.util.Enumeration;
import java.util.Vector;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CContextHelp;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.SincrEventos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.centbasicos.MedCent;
import centinelas.datos.centbasicos.NotifRMC;
import centinelas.datos.centbasicos.PuntoCent;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool2;

public class DiaSelecSem
    extends CDialog
    implements CInicializar, CFiltro {

  /*************** Datos propios del panel ****************/

  // Parametros de entrada
  CApp applet = null;
  PuntoCent pCent = null;
  String ano = null;

  // Parametros de salida
  boolean haySemanas = true; //Para indicar si hay semanas. Si no las hay , no se mostrar� este di�logo

  private boolean OK = false;
  private MedCent MedSelected = null;
  private String semanaSelected = "";

  /*************** Constantes ****************/

  // Modo de operaci�n
  public int modoOperacion = CInicializar.NORMAL;

  // Imagenes a cargar
  final String imgAceptar = "images/aceptar.gif";
  final String imgCancelar = "images/cancelar.gif";

  // Nombre del servlet
  final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

  /****************** Componentes del panel ********************/

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  // Organizaci�n del panel
  private XYLayout xYLayout1 = new XYLayout();

  // Lista con Ano/Sem-PuntoCentinela-MedicoCentinela
  private CListaMantenimiento clmMantenimiento = null;

  // Botones de salida
  private ButtonControl btnAceptar = new ButtonControl();
  private ButtonControl btnCancelar = new ButtonControl();

  // Escuchadores
  DiaSelecSemActionAdapter actionAdapter = null;

  // Constructor
  // @param a: CApp
  // @param ano: String con el a�o
  // @param pCent: PuntoCent
  public DiaSelecSem(CApp a, String anyo, PuntoCent punto) {

    super(a);
    setTitle("Selecci�n de Semana Epidemiol�gica");
    applet = a;
    pCent = punto;
    ano = anyo;

    try {
      // Inicializaci�n
      jbInit();

      // Primera trama de semanas epidemiol�gicas
      clmMantenimiento.setPrimeraPagina(this.primeraPagina());
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Inicia el aspecto del dialogo diaselecsem
  public void jbInit() throws Exception {
    // Botones y etiquetas para un ClistaMantenimiento
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    // Carga las imagenes
    getApp().getLibImagenes().put(constantes.imgACEPTAR);
    getApp().getLibImagenes().put(constantes.imgCANCELAR);
    getApp().getLibImagenes().CargaImagenes();

    // Escuchadores
    actionAdapter = new DiaSelecSemActionAdapter(this);

    // Organizacion del panel

    this.setSize(new Dimension(725, 405));
    this.setLayout(xYLayout1);
    xYLayout1.setWidth(725);
    xYLayout1.setHeight(405);

    // CListaMantenimiento
    // CListaMantenimiento: Botones: sin botones
    // CListaMantenimiento: Etiquetas
    vLabels.addElement(new CColumna("A�o/Semana",
                                    "100",
                                    "ANOSEMEPI"));
    vLabels.addElement(new CColumna("Punto Centinela",
                                    "250",
                                    "DS_PCENTI"));
    vLabels.addElement(new CColumna("M�dico Centinela",
                                    "320",
                                    "DS_MEDICO"));
    // botones
    vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                   constantes.imgMODIFICAR2,
                                   "Modificar",
                                   true,
                                   true));

    // CListaMantenimiento: Panel con las semanas epidemiol�gicas
    clmMantenimiento = new CListaMantenimiento(applet,
                                               vLabels,
                                               vBotones,
                                               this,
                                               this);

    // Adici�n de componentes al di�logo
    this.add(clmMantenimiento, new XYConstraints(15, 15, 710, 325));
    this.add(btnAceptar, new XYConstraints(530, 340, 80, -1));
    this.add(btnCancelar, new XYConstraints(625, 340, 80, -1));

    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(constantes.
        imgACEPTAR));
    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.addActionListener(actionAdapter);

    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(constantes.
        imgCANCELAR));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.addActionListener(actionAdapter);

    // Tool Tips
    new CContextHelp("Aceptar", btnAceptar);
    new CContextHelp("Cancelar", btnCancelar);
  }

  public void Inicializar() {}

  // Gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int modo) {
    switch (modo) {
      // Modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // Modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;
    } // Fin switch
  } // Fin Inicializar()

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

  // Regenera una lista de Datas NotifRMC, a�adiendo campos necesarios
  //  para su representacion en la tabla
  public Lista regeneraLista(Lista l) {
    NotifRMC n = null;
    Data dt = null;

    for (Enumeration e = l.elements(); e.hasMoreElements(); ) {
      // Elemento a tratar
      dt = (Data) e.nextElement();

      // NotifRMC para poder acceder a sus campos
      n = new NotifRMC(dt);

      dt.put("ANOSEMEPI", ano + "/" + n.getCD_SEMEPI());
      dt.put("DS_PCENTI", pCent.getDS_PCENTI());
      dt.put("DS_MEDICO",
             n.getString("DS_NOMBRE") + " " + n.getString("DS_APE1") + " " +
             n.getString("DS_APE2"));
    }

    return l;
  } // Fin regeneraLista()

  // Solicita la primera trama de datos
  public Lista primeraPagina() {

    // Lista con las peticiones
    Lista lNotifRMC = null;

    // QueryTool y lista para la query
    Lista lQuery = new Lista();
    QueryTool2 qtNotifRMC = NotifRMC.getNotifRMCPCentiAno(pCent.getString(
        "CD_PCENTI"), ano);
    lQuery.addElement(qtNotifRMC);
    lQuery.setTrama("CD_SEMEPI", "");

    Inicializar(CInicializar.ESPERA);

    // Consulta al servlet
    try {
      this.getApp().getStub().setUrl(servlet);
          /*lNotifRMC =  BDatos.execSQL(applet,servlet,8,lQuery); // 2: GET_PAGE_DESC*/

      // SOLO DESARROLLO
//      SrvQueryTool srv = new SrvQueryTool();
      lNotifRMC = BDatos.ejecutaSQL(false, applet, servlet, 8, lQuery); // 2: GET_PAGE_DESC

      // Lista vacia
      if (lNotifRMC.size() == 0) {
        this.getApp().showAdvise(
            "No hay semanas con notificaciones para el punto y a�o indicados");
        haySemanas = false;
      }

      // Error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    // La lista debe estar creada
    if (lNotifRMC == null) {
      lNotifRMC = new Lista();

      // Vuelta al estado normal
    }
    Inicializar(CInicializar.NORMAL);

    // Devoluci�n de la lista de NotifRMC
    return regeneraLista(lNotifRMC);
  } // Fin primeraPagina()

  // Solicita la siguiente trama de datos
  public Lista siguientePagina() {

    // Lista con las peticiones
    Lista lNotifRMC = null;

    // QueryTool y lista para la query
    Lista lQuery = new Lista();
    QueryTool2 qtNotifRMC = NotifRMC.getNotifRMCPCentiAno(pCent.getString(
        "CD_PCENTI"), ano);
    lQuery.addElement(qtNotifRMC);
    lQuery.setTrama(this.clmMantenimiento.getLista().getTrama());

    Inicializar(CInicializar.ESPERA);

    // Consulta al servlet
    try {
      this.getApp().getStub().setUrl(servlet);
          /*lNotifRMC =  BDatos.execSQL(applet,servlet,8,lQuery); // 2: GET_PAGE_DESC*/

      // SOLO DESARROLLO
//      SrvQueryTool srv = new SrvQueryTool();
      lNotifRMC = BDatos.ejecutaSQL(false, applet, servlet, 8, lQuery); // 2: GET_PAGE_DESC

      // Lista vacia
      if (lNotifRMC.size() == 0) {
        this.getApp().showAdvise("No hay semanas con estos criterios.");
      }

      // Error en el servlet
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

    // La lista debe estar creada
    if (lNotifRMC == null) {
      lNotifRMC = new Lista();

      // Vuelta al estado normal
    }
    Inicializar(CInicializar.NORMAL);

    // Devoluci�n de la lista de NotifRMC
    return regeneraLista(lNotifRMC);
  } // Fin siguientePagina()

  // Operaciones de la botonera
  public void realizaOperacion(int j) {
    // No hay alta/mod/baja
    //Simplemente hay selecci�n de alguna semana.
    // Hace por tanto lo  mismo que bot�n aceptar
    btnAceptarActionPerformed();

  }

  /******************** Acceso a vars. salida *********************/

  //  Devuelve true si se sale mediante el bot�n de Aceptar
  public boolean getAceptar() {
    return OK;
  } // Fin getAceptar()

  // Devuelve el MedCent elegido, o null si no se ha escogido ninguno
  public MedCent getMedCent() {
    return MedSelected;
  } // Fin getMedCent()

  // Devuelve la semana elegida, o ""si no se ha escogido ninguna
  public String getSemana() {
    return semanaSelected;
  } // Fin getSemana()

  /******************** Manejadores *************************/

  void btnAceptarActionPerformed() {
    // Salimos por Aceptar
    OK = true;

    // Se establece el MedCent seleccionado
    Data sel = clmMantenimiento.getSelected();
    if (sel != null) {
      MedSelected = new MedCent(sel);
      // Semana elegida
      semanaSelected = sel.getString("CD_SEMEPI");
      // Desaparece el dialogo
      dispose();
    }
  } // Fin btnAceptarActionPerformed()

  void btnCancelarActionPerformed() {
    // Desaparece el dialogo
    dispose();
  } // Fin btnCancelarActionPerformed()

} // endclass DiaSelecSem

/******************* ESCUCHADORES **********************/

// Botones
class DiaSelecSemActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaSelecSem adaptee;
  ActionEvent evt;

  DiaSelecSemActionAdapter(DiaSelecSem adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.evt = e;
    new Thread(this).start();
  }

  public void run() {
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      if (evt.getActionCommand().equals("Aceptar")) {
        adaptee.btnAceptarActionPerformed();
      }
      else if (evt.getActionCommand().equals("Cancelar")) {
        adaptee.btnCancelarActionPerformed();
      }
      s.desbloquea(adaptee);
    }
  }
} // endclass  DiaSelecSemActionAdapter
