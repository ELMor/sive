/* Clase: PanPpal
 * Autor: Luis Rivera
 * Fecha Inicio: 13/01/2000
 * Descripcion: Panel principal de notificaciones
 * Modificaciones:
 *   21/03/2000 (JMT) ano/sem de entrada no pueden ser menores que
 *     ano/sem de la fecha actual
 **/

package centinelas.cliente.c_not;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CContextHelp;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaValores;
import capp2.CPanel;
import capp2.CPnlCodigo;
import capp2.UButtonControl;
import centinelas.cliente.c_componentes.CListaMantenimientoExt;
import centinelas.cliente.c_componentes.CPnlCodigoExt;
import centinelas.cliente.c_componentes.ContCListaMantenimientoExt;
import centinelas.cliente.c_componentes.ContCPnlCodigoExt;
import centinelas.cliente.c_componentes.ContPanAnoSemFecha;
import centinelas.cliente.c_componentes.PanAnoSemFecha;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.SincrEventos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.c_not.NotifRMCExt;
import centinelas.datos.centbasicos.MedCent;
import centinelas.datos.centbasicos.NotifRMC;
import centinelas.datos.centbasicos.PuntoCent;
import comun.constantes;
import sapp2.Data;
import sapp2.Format;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;
import sapp2.StubSrvBD;

/**
 * Panel a trav�s del que se buscan las notificaciones
 * @autor LRG
 * @version 1.0
 */
public class PanPpal
    extends CPanel
    implements CInicializar, CFiltro, ContPanAnoSemFecha,
    ContCPnlCodigoExt, ContCListaMantenimientoExt {

  //Acceso a servlets
  final protected int servletOBTENER_MED_DE_PTO_ANO_SEM = 10; //Da el m�dico activo de un pto, a�o, sem
  final protected int servletOBTENER_MED_DE_PTO_ANO_SEM_MED_ALTA = 11; //Da el m�dico activo de un pto, a�o, sem, incluido el que est� de alta

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  // Sincronizador de Eventos
  private SincrEventos scr = new SincrEventos();

  XYLayout xYLayout1 = new XYLayout();
  Label lblPun = new Label();
  CPnlCodigo pnlPun = null;
  Label lblMed = new Label();
  Label lblNomMed = new Label();
  PanAnoSemFecha panAnoSemFecha = null;
  ButtonControl btnBuscarNot = new ButtonControl();
  ButtonControl btnBuscarSem = new ButtonControl();
  CListaMantenimientoExt clmNot = null;
  Lista listaSexos = null;

  // datos
  private Data dtCentro = null;
  // filtro
  private Data dtFiltro = null;

  //Datos del panel v�lidos
  PuntoCent punCen = null;
  MedCent medCen = null;
  String sMedico = "";

  //Lista de notificaciones
  Lista listaNotifRMCExt = new Lista(); ;
  //Listas generales
  Lista listaConListas = new Lista();

  //___________________________________________________________________

  // constructor del panel PanPpal
  public PanPpal(CApp a) {
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    try {
      setApp(a);

      // botones
      vBotones.addElement(new CBoton(constantes.keyBtnAnadir,
                                     constantes.imgALTA2,
                                     "Nueva petici�n",
                                     false,
                                     false));

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     constantes.imgMODIFICAR2,
                                     "Modificar petici�n",
                                     true,
                                     true));

      vBotones.addElement(new CBoton(constantes.keyBtnBorrar,
                                     constantes.imgBAJA2,
                                     "Borrar petici�n",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("Enfermedad",
                                      "220",
                                      "DS_PROCESO"));

      vLabels.addElement(new CColumna("N� Casos Declarados",
                                      "180",
                                      "NM_CASOSDEC"));

      vLabels.addElement(new CColumna("Fecha Recepci�n",
                                      "200",
                                      "FC_RECEP"));

      clmNot = new CListaMantenimientoExt(a,
                                          vLabels,
                                          vBotones,
                                          this,
                                          this,
                                          this,
                                          250,
                                          640);

      jbInit();

      Inicializar(CInicializar.NORMAL);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());

    }
  }

  //______________________________________________________________

  // inicializa el aspecto del panel PanPpal
  public void jbInit() throws Exception {
    final String imgLUPA = "images/magnify.gif";
    final String imgBUSCAR = "images/refrescar.gif";

    // im�genes
    this.getApp().getLibImagenes().put(imgLUPA);
    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().CargaImagenes();
    btnBuscarSem.setImage(this.getApp().getLibImagenes().get(imgLUPA));
    btnBuscarNot.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));

    PanPpal_actionAdapter actionAdapter = new PanPpal_actionAdapter(this);

    xYLayout1.setWidth(680);
    xYLayout1.setHeight(380);

    lblPun.setText("Punto Centinela:");
    btnBuscarSem.setActionCommand("btnBuscarSem");
    btnBuscarSem.addActionListener(actionAdapter);

    btnBuscarNot.setActionCommand("btnBuscarNot");
    btnBuscarNot.setLabel("Buscar");
    btnBuscarNot.addActionListener(actionAdapter);

    QueryTool2 qt = PuntoCent.getAllPuntoCent();
    pnlPun = new CPnlCodigoExt(getApp(),
                               this,
                               qt,
                               "CD_PCENTI",
                               "DS_PCENTI",
                               false,
                               "Punto Centinela:",
                               "Punto Centinela:",
                               this
                               );

    if (app.getParametro("CD_ANO").equals("")) {
      panAnoSemFecha = new PanAnoSemFecha(this, PanAnoSemFecha.modoVACIO, true);
    }
    else {
      panAnoSemFecha = new PanAnoSemFecha(this, true, app.getParametro("CD_ANO"), true);

    }

    lblMed.setText("Medico:");
    this.setLayout(xYLayout1);
    this.add(lblPun, new XYConstraints(7, 6, 110, 24));
    this.add(pnlPun, new XYConstraints(150, 6, 274, 40));
    this.add(lblMed, new XYConstraints(437, 5, 51, 24));
    this.add(lblNomMed, new XYConstraints(500, 6, 143, 24));
    this.add(panAnoSemFecha, new XYConstraints(6, 50, 400, 65));
    this.add(btnBuscarSem, new XYConstraints(437, 50, 24, 24));
    this.add(btnBuscarNot, new XYConstraints(555, 80, 79, 24));
    this.add(clmNot, new XYConstraints(7, 120, 650, 290));

    // tool tips
    new CContextHelp("Obtener semana", btnBuscarSem);
    new CContextHelp("Obtener notificaciones", btnBuscarNot);

    traerListas();

  }

  void traerListas() {
    Lista lQuery = new Lista();
    Data dtlistaSexos = null;
    QueryTool2 qtlistaSexos = null;

    try {
      // Lista de QueryTools con sus modos de operacion
      qtlistaSexos = PanPpal.getAllSexo();
      dtlistaSexos = new Data();
      dtlistaSexos.put("1", qtlistaSexos); // se aplicar� select a querytool
      lQuery.addElement(dtlistaSexos);

      listaConListas = BDatos.ejecutaSQL(true, app,
                                         nombreservlets.strSERVLET_TRANSACCION,
                                         0, lQuery); // Transaccion*/

      listaSexos = (Lista) (listaConListas.firstElement());
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }

  }

  // SELECT CD_SEXO,DS_SEXO from SIVE_SEXO
  public static QueryTool2 getAllSexo() {
    QueryTool2 qtGral = new QueryTool2();
    // Relleno de qtGral
    qtGral.putName("SIVE_SEXO");
    qtGral.putType("CD_SEXO", QueryTool.STRING);
    qtGral.putType("DS_SEXO", QueryTool.STRING);

    return qtGral;
  } // Fin getAllSexo()

  //______________________________________________________________

  public void Inicializar() {}

  //_____________________________________________________________

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {

    switch (i) {
      // modo espera
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        panAnoSemFecha.Inicializate(CInicializar.ESPERA);
        pnlPun.backupDatos();
        break;

        // modo entrada
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        panAnoSemFecha.Inicializate(CInicializar.NORMAL);

        //Si hay cambios en el punto se resetea la lista de notif
        if (pnlPun.hayCambios()) {
          listaNotifRMCExt = new Lista(); ;
          clmNot.setPrimeraPagina(listaNotifRMCExt);
          if (pnlPun.getDatos() == null) {
            punCen = null;
            medCen = null;
            lblNomMed.setText("");
          }
          //A�o pilla el foco
          panAnoSemFecha.txtAno.requestFocus();

        }

        //Se habilita todo (importante.Pod�a estar algo deshabilitado)
        panAnoSemFecha.setEnabled(true);
        btnBuscarSem.setEnabled(true);
        btnBuscarNot.setEnabled(true);
        clmNot.setEnabled(true);

        //Se deshabilita lo necesario seg�n el caso
        //Si no hay punto cent
//        if (punCen==null) {
        if (pnlPun.getDatos() == null) {
          panAnoSemFecha.setEnabled(false);
          btnBuscarSem.setEnabled(false);
          btnBuscarNot.setEnabled(false);
          clmNot.setEnabled(false);
        }
        //Si hay punto pero no hay semana
//        else if (panAnoSemFecha.getCodSem().equals("")) {   +++ (ARS 03-04-01)
        else if (panAnoSemFecha.txtCodSem.getText().equals("")) {
          btnBuscarNot.setEnabled(false);
          clmNot.setEnabled(false);
        }
        //Si no hay pto y sem pero no hay notificaciones
        else if (listaNotifRMCExt.size() == 0) {

        }

        break;
    }

  }

  //______________________________________________________________

  //HAbilita o deshabilita botones en funci�n de si son de cobertura o hay item elegido
  public void cambiarBotones(Vector vectBotones) {
    UButtonControl btnControlMod = (UButtonControl) vectBotones.elementAt(1);
    UButtonControl btnControlBaja = (UButtonControl) vectBotones.elementAt(2);

    //Si hay dato
    if (clmNot.getSelected() != null) {
      NotifRMC dato = new NotifRMC(clmNot.getSelected());

      //Solo se habilita bot�n de baja si no es de cobertura y no hay casos
      if ( (dato.getIT_COBERTURA().equals("N")
            && (dato.getNM_CASOSDEC().trim()).equals("0"))) {
        btnControlBaja.setEnabled(true);
      }
      else {
        btnControlBaja.setEnabled(false);
      }
    }
    //Si no hay elegido, botones de mod y borrar deshabilitados
    else {
      btnControlBaja.setEnabled(false);
      btnControlMod.setEnabled(false);
    }

  }

  //______________________________________________________________

  public String getDescCompleta(Data dat) {
    return ( (String) (dat.get("CD_PCENTI")) + (String) (dat.get("DS_PCENTI")));
  }

  public void trasEventoEnCPnlCodigoExt() {

    boolean bPuntoBaja = false;

    if (pnlPun.getDatos() != null) {
      try {
        //Se actualiza variable puntoCent (02-04-01 ARS)
        if (pnlPun.hayCambios()) {
          // Lo iniciamos todo.
          panAnoSemFecha.sFecSemBk = "";
          panAnoSemFecha.sFecSemBkAntiguo = "";
          panAnoSemFecha.txtCodSem.setText("");
          panAnoSemFecha.txtFecSem.setText("");
          panAnoSemFecha.sCodSemBk = "";
          panAnoSemFecha.sCodSemBkAntiguo = "";
        }
        punCen = new PuntoCent(pnlPun.getDatos());

        // 02-04-01 ARS ****
        if (punCen.getIT_BAJA().equals("S")) {
          this.getApp().showAdvise(
              "Este punto centinela no tiene m�dico activo");
          bPuntoBaja = true;
          sMedico = "";
          medCen = null;
          lblNomMed.setText(sMedico);
          clmNot.vaciarPantalla();
        }
        // ****

        // Realizar esto, si el punto est� dado de alta.  (ARS 02-04-01)
        if (!bPuntoBaja) {
          //Se rellenan datos de m�dico traidos de panel c�digo si los hay
          QueryTool2 qt = MedCent.getMedCentActivoOfOnePuntoCent(punCen.
              getString("CD_PCENTI"));
          Lista v = new Lista();
          v.addElement(qt);
          // consulta el servidor
          v = BDatos.ejecutaSQL(true, app, StubSrvBD.SRV_QUERY_TOOL, 1, v);

          // Lista vacia
          if (v.size() == 0) {
            this.getApp().showAdvise(
                "Este punto centinela no tiene m�dico activo");
            // (ARS 02-04-01) ****
            medCen = null;
            clmNot.vaciarPantalla();
//             pnlPun.limpiarDatos();
//             punCen=null;
            // ****
          }
          else {
            //Se crea el objeto m�dico a partir del Data recibido y se pone la label
            Data res = (Data) v.firstElement();
            medCen = new MedCent(res);
            sMedico = "D " + res.getString("DS_NOMBRE") + " " +
                res.getString("DS_APE1");
            lblNomMed.setText(sMedico);
          }
        } // Del if
      }

      catch (Exception ex) {
        this.getApp().trazaLog(ex);
        this.getApp().showError(ex.getMessage());
      }

    } //if
    else {
      punCen = null;
    }
    // Iniciamos todo, para que queden las cosas
    // bien habilitadas y deshabilitadas.
    Inicializar(CInicializar.NORMAL);
  }

  //______________________________________________________________

  // gesti�n de los botones
  void btn_actionPerformed(ActionEvent e) {

    CListaValores clv = null;
    QueryTool qt = null;
    Vector v = null;

    // lupa de centros
    if (e.getActionCommand().equals("btnBuscarSem")) {

      // Creacion y muestra del dialogo para elegir la semana epi
      DiaSelecSem dss = new DiaSelecSem(this.getApp(), panAnoSemFecha.sAnoBk,
                                        punCen);

      if (dss.haySemanas) {
        ( (DiaSelecSem) dss).show();

        // Comprobaci�n de la semana seleccionada
        if (dss.getAceptar()) {
          medCen = dss.getMedCent();
          if (medCen != null) {
            sMedico = "D. " + medCen.getDS_NOMBRE() + " " + medCen.getDS_APE1();
            lblNomMed.setText(sMedico);
            //Busca fecha de semana elegida
            panAnoSemFecha.setCodSem(dss.getSemana());

            //Se buscan las notificaciones de ese a�o y sem
            clmNot.setPrimeraPagina(this.primeraPagina());
          }
        } //if aceptar
      }
      // bot�n buscar
    }
    else if (e.getActionCommand().equals("btnBuscarNot")) {

      //Pone la primera p�gina en la tabla
//      if(SincrEventos.bloquea(CInicializar.NORMAL,this)){
      clmNot.setPrimeraPagina(this.primeraPagina());
//       SincrEventos.desbloquea(this);
//      }
    }
  }

  //______________________________________________________________

  // solicita la primera trama de datos
  public Lista primeraPagina() {

    Lista v = null;
    Data dt = null;
    boolean bTraer = true;

    // solicita el centro
    if (bTraer) {

      Inicializar(CInicializar.ESPERA);
      // accede a la base de datos
      try {

        //Se rellenan datos de m�dico traidos de panel c�digo si los hay
        QueryTool2 qt = NotifRMCExt.getAllNotifRMCExtOfOnePtoMedAnoSem(
            punCen.getCD_PCENTI(), medCen.getCD_MEDCEN(),
            panAnoSemFecha.getCodAno(),
            panAnoSemFecha.getCodSem());

        v = new Lista();
        v.addElement(qt);
        listaNotifRMCExt = BDatos.ejecutaSQL(true, app,
                                             StubSrvBD.SRV_QUERY_TOOL, 1, v);

        // Lista vacia
        if (listaNotifRMCExt.size() == 0) {
          this.getApp().showAdvise(
              "No hay registros de notificaciones para el punto, a�o y semana indicados");
        }

      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
        this.getApp().showError(ex.getMessage());
      }

      Inicializar(CInicializar.NORMAL);
    } //If

    // la lista debe estar creada
    if (listaNotifRMCExt == null) {
      listaNotifRMCExt = new Lista();

    }
    return listaNotifRMCExt;
  }

  //______________________________________________________________
  public Lista siguientePagina() {
    return null;

  }

  //______________________________________________________________

  // operaciones de la botonera
  public void realizaOperacion(int j) {
    NotifRMC dato = null;

    switch (j) {
      // bot�n de alta
      case ALTA:

        DiaAltaNotifRMC dialAltaNot =
            new DiaAltaNotifRMC(app, panAnoSemFecha.getCodAno(),
                                panAnoSemFecha.getCodSem(), punCen, medCen,
                                clmNot.getLista());

        //Solo se muestra dialogo si hay enfemedades
        if (dialAltaNot.hayEnfermedades) {

          ( (DiaAltaNotifRMC) dialAltaNot).show();

          //a�adirlo a la lista
          if (dialAltaNot.getAceptar()) {
            //a�ado en la lista la nueva peticion

            /*
                      listaNotifRMCExt.addElement(dialAltaNot.getNotifRMC());
                      //Se refrescan los datos
                      clmNot.setPrimeraPagina(listaNotifRMCExt);
             */
            clmNot.vaciarPantalla();
            //Se refrescan los datos
            clmNot.setPrimeraPagina(primeraPagina());
          }

        }

        break;

        // bot�n de modificaci�n
      case MODIFICACION:

        if (clmNot.getSelected() != null) {
          dato = new NotifRMC(clmNot.getSelected());
          int ind = clmNot.getSelectedIndex();

          DiaModBajaNotifRMC diaModBaja = new DiaModBajaNotifRMC(app,
              constantes.modoMODIFICACION, (String) (punCen.getDS_PCENTI()),
              sMedico,
              (String) (dato.get("DS_PROCESO")), dato, listaSexos);

          diaModBaja.show();

          clmNot.vaciarPantalla();
          //Se refrescan los datos
          clmNot.setPrimeraPagina(primeraPagina());
          /*
                      //modifico en la lista la peticion
               listaNotifRMCExt.setElementAt(diaPetMod.getCabPeticion(), ind);
             //         clmNot.vaciarPantalla();
                      clmNot.setPrimeraPagina(lisNot);
           */

        }
        else {
          this.getApp().showAdvise(
              "Debe seleccionar una notificaci�n en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:

        if (clmNot.getSelected() != null) {
          dato = new NotifRMC(clmNot.getSelected());
          int ind = clmNot.getSelectedIndex();

          DiaModBajaNotifRMC diaModBaja = new DiaModBajaNotifRMC(app,
              constantes.modoBAJA, (String) (punCen.get("DS_PCENTINELA")),
              sMedico,
              (String) (dato.get("DS_PROCESO")), dato, listaSexos);

          diaModBaja.show();

          clmNot.vaciarPantalla();
          //Se refrescan los datos
          clmNot.setPrimeraPagina(primeraPagina());
          /*
                      //modifico en la lista la peticion
                      lisNot.removeElementAt(ind);
                      clmNot.vaciarPantalla();
                      clmNot.setPrimeraPagina(lisNot);
           */

        }

        else {
          this.getApp().showAdvise("Debe seleccionar una petici�n en la tabla.");
        }
        break;

    } //Fin switch

  }

  //______________________________________________________________

//_____IMPLEMENTACION INTERFAZ CONTPANANOSEMFECHA________________________________________________________

  /* Para obtener el applet*/
  public CApp getMiCApp() {
    return this.app;
  };

  /* M�todos ser�n llamados desde el PanAnoSemFecha despu�s de que el propio PanAnoSemFecha
   * ejecute su codigo en la p�rdida de foco */

  public void alPerderFocoAno() {
    if (panAnoSemFecha.hayCambioCodAno()) {
      listaNotifRMCExt = new Lista();
      //Pone la primera p�gina en la tabla
      clmNot.setPrimeraPagina(listaNotifRMCExt);
    }
  };

  public void alPerderFocoSemana() {
    boolean bExisteMedico = true;
    if (panAnoSemFecha.hayCambioCodSem()) {
      Lista lResult = null;
      Integer elemResult = null;
      Lista lResMed = null;

      //Recoge fecha ya validada y comprueba si es mayor que la actual
      //En ese caso se recupera fecha anterior y se muestra di�logo
      if (Format.comparaFechas(panAnoSemFecha.getFecSem(), Format.fechaActual()) ==
          1) {
        //CUIDADO!! Recuperar datos antes de mostrar di�logo para que
        //l a recuperarci�n sea antes de p�rdida de foco de fecha
        panAnoSemFecha.recuperarDatos();
        this.getApp().showError(
            "Fecha notificaci�n deber ser menor o igual que la fecha actual");
        return;
      }

      // Proceso de cobertura
      else {
        try {

          Inicializar(CInicializar.ESPERA);

          Lista lParametros = new Lista();
          lParametros.addElement(panAnoSemFecha.getCodAno());
          lParametros.addElement(panAnoSemFecha.getCodSem());
          lParametros.addElement(getApp().getParameter("COD_USUARIO"));
          lResult = BDatos.ejecutaSQL(true, getApp(),
                                      nombreservlets.strSERVLET_COBERTURA, 0,
                                      lParametros); // 0: Avance semGen

          elemResult = (Integer) (lResult.firstElement());
          if (elemResult.intValue() == 1) {
            getApp().showConfirm(
                "Se ha ejecutado el proceso de generaci�n de cobertura");
//          getApp().showStatus("Se ha ejecutado el proceso de generaci�n de cobertura");
          }
          else if (elemResult.intValue() == 2) {
            getApp().showConfirm(
                "Error al ejecutarse el proceso de generaci�n de cobertura");
//          getApp().showStatus("Error al ejecutarse el proceso de generaci�n de cobertura");

            //Se trae el m�dico de esa semana.Si no lo hay deja el actual
          }
          Lista lParam = new Lista();
          Data datPeticion = new Data();
          datPeticion.put("CD_PCENTI", punCen.getCD_PCENTI());
          datPeticion.put("CD_ANOEPI", panAnoSemFecha.getCodAno());
          datPeticion.put("CD_SEMEPI", panAnoSemFecha.getCodSem());
          lParam.addElement(datPeticion);
          lResMed = BDatos.ejecutaSQL(true, getApp(),
                                      nombreservlets.strSERVLET_MANT_MED_NOTIF,
              servletOBTENER_MED_DE_PTO_ANO_SEM_MED_ALTA, lParam);
          //Si hemos encontrado med activo en esa semana
          if (lResMed.size() != 0) {
            medCen = (MedCent) (lResMed.firstElement());
            if (medCen != null) {
              if (medCen.getIT_BAJA().equals("S")) {
                this.getApp().showAdvise(
                    "Este m�dico est� actualmente dado de baja");
              }
              sMedico = "D. " + medCen.getDS_NOMBRE() + " " + medCen.getDS_APE1();
              lblNomMed.setText(sMedico);
              clmNot.setEnabled(true);
            }
          }
          else {
            sMedico = "";
            lblNomMed.setText(sMedico);
            panAnoSemFecha.sFecSemBk = "";
            panAnoSemFecha.sFecSemBkAntiguo = "";
            panAnoSemFecha.txtCodSem.setText("");
            panAnoSemFecha.txtFecSem.setText("");
            bExisteMedico = false;
            this.getApp().showAdvise("No existe m�dico activo para esa semana");
          }

          //Se buscan las notificaciones de ese a�o y sem

          if (bExisteMedico) {
            clmNot.setPrimeraPagina(this.primeraPagina());

          }

        }
        catch (Exception ex) {
          // No ha habido avance de semGen
          this.getApp().trazaLog(ex);
          this.getApp().showError(ex.getMessage());

        }

        /*
              listaNotifRMCExt=  new Lista();
              //Pone la primera p�gina en la tabla
              clmNot.setPrimeraPagina( listaNotifRMCExt );
         */
        Inicializar(CInicializar.NORMAL);
        if (!bExisteMedico) {
          clmNot.setEnabled(false);
          clmNot.vaciarPantalla();
        }
      }
    } //else
  }

  public void alPerderFocoFecha() {
    if (panAnoSemFecha.hayCambioFecSem()) {
      listaNotifRMCExt = new Lista();
      //Pone la primera p�gina en la tabla
      clmNot.setPrimeraPagina(listaNotifRMCExt);
    }
  };

  // Acceso al SincrEventos
  public SincrEventos getSincrEventos() {
    return scr;
  } // Fin getSincrEventos()

//_____________________________________________________________

} //CLASE

//_________________________________________________________________________

// clase eventos botones
class PanPpal_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanPpal adaptee;
  ActionEvent e;

  PanPpal_actionAdapter(PanPpal adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    //Sincroniza eventos: Solo se ejecuta m�todo si no se
    // est� ejecutando el c�digo correspondiente a otro evento
    SincrEventos s = adaptee.getSincrEventos();
    if (s.bloquea(adaptee.modoOperacion, adaptee)) {
      adaptee.btn_actionPerformed(e);
      s.desbloquea(adaptee);
    }
  }
}
