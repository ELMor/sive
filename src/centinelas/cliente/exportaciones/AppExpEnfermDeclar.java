//Applet principal de enfermedades declaradas por los puntos centinelas.
package centinelas.cliente.exportaciones;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppExpEnfermDeclar
    extends CApp
    implements CInicializar {

  PanExpEnfermDeclar pan = null;

  public AppExpEnfermDeclar() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo(
        "Volcado a fichero de las enfermedades declaradas por los puntos centinelas");
    pan = new PanExpEnfermDeclar(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(false);
        break;
    }
  }
}
