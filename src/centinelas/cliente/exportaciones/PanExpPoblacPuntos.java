/*
 Panel principal de poblaci�n de los puntos centinelas.
     Permite elegir per�odo del cual se extraer�n los datos de poblaci�n y ruta en la
 cual se grabar� el fichero generado. Tras recibir los datos del servidor, crea el
 fichero correspondiente en la ruta indicada
 */
package centinelas.cliente.exportaciones;

import java.util.Vector;

import java.awt.Cursor;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CInicializar;
import capp2.CPanel;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import sapp2.Data;
import sapp2.Lista;
import util_ficheros.DataCampos;
import util_ficheros.EditorFichero;

public class PanExpPoblacPuntos
    extends CPanel
    implements CInicializar {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  XYLayout xyLayout = new XYLayout();

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;

  //constantes tama�o campos
  //para enteros->edades tammax=3 y nm_poblac tammax=5
  final public int tamCDEDADF = 3;
  final public int tamCDEDADI = 3;
  final public int tamCDSEXO = 1;
  final public int tamCDPCENTI = 6;
  final public int tamCDMEDCEN = 6;
  final public int tamNMPOBLAC = 5;
  final public int tamCDANOEPI = 4;
  final public int tamCDANOEPIA = 4;
  final public int tamCDSEMEPIA = 2;
  final public int tamCDANOBAJA = 4;
  final public int tamCDSEMBAJA = 2;

  //componentes q conforman el panel

  ButtonControl btnAceptar = new ButtonControl();
  capp2.CFileName panFichero;

  //vector de DataCampos con nbCampo y su longitud
  Vector vCampos = new Vector();
  CApp apl = null;

  public PanExpPoblacPuntos(CApp a) {
    super(a);
    apl = a;
    try {
      panFichero = new capp2.CFileName(a);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  } //end constructor

  void jbInit() throws Exception {
    final String imgAceptar = "images/aceptar.gif";

    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().CargaImagenes();

    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnAceptar.setLabel("Aceptar");
    btnAceptar.addActionListener(new
                                 PanExpPoblacPuntos_btnAceptar_actionAdapter(this));

    xyLayout.setHeight(150);
    xyLayout.setWidth(480);
    this.setLayout(xyLayout);

    this.add(panFichero, new XYConstraints(MARGENIZQ, MARGENSUP, 427, 40));
    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 427 - 88,
                               MARGENSUP + ALTO + 2 * INTERVERT, 88, 29));

    //configuro el vector vCampos:DataCampos
    rellenarVector();
  } //end jbinit

  //rellena el vector de DataCampos con los nombres de los campos y su longitud
  void rellenarVector() {
    DataCampos dtCdPcenti = new DataCampos("CD_PCENTI", tamCDPCENTI);
    DataCampos dtMedcen = new DataCampos("CD_MEDCEN", tamCDMEDCEN);
    DataCampos dtCdAnoepia = new DataCampos("CD_ANOEPIA", tamCDANOEPIA);
    DataCampos dtCdSemepia = new DataCampos("CD_SEMEPIA", tamCDSEMEPIA);
    DataCampos dtCdAnoBaja = new DataCampos("CD_ANOBAJA", tamCDANOBAJA);
    DataCampos dtCdSemBaja = new DataCampos("CD_SEMBAJA", tamCDSEMBAJA);
    DataCampos dtCdAnoepi = new DataCampos("CD_ANOEPI", tamCDANOEPI);
    DataCampos dtCdEdadi = new DataCampos("CD_EDADI", tamCDEDADI);
    DataCampos dtCdEdadf = new DataCampos("CD_EDADF", tamCDEDADF);
    DataCampos dtCdSexo = new DataCampos("CD_SEXO", tamCDSEXO);
    DataCampos dtNmPoblac = new DataCampos("NM_POBLAC", tamNMPOBLAC);
    vCampos.addElement(dtCdPcenti);
    vCampos.addElement(dtMedcen);
    vCampos.addElement(dtCdAnoepia);
    vCampos.addElement(dtCdSemepia);
    vCampos.addElement(dtCdAnoBaja);
    vCampos.addElement(dtCdSemBaja);
    vCampos.addElement(dtCdAnoepi);
    vCampos.addElement(dtCdEdadi);
    vCampos.addElement(dtCdEdadf);
    vCampos.addElement(dtCdSexo);
    vCampos.addElement(dtNmPoblac);
  }

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
    }
  }

  void btnAceptar_actionPerformed(ActionEvent e) {
    final String servlet = nombreservlets.strSERVLET_EXPORTACIONES; // "servlet/SrvExportaciones";
    Lista lResultado = null;
    Data dtDatos = new Data();
    Lista lDatos = new Lista();
    if (isDataValid()) {
      //1� llamar al servlet y mandar ejecutar la operacion->dev lista
      Inicializar(CInicializar.ESPERA);
      try {
        lResultado = BDatos.ejecutaSQL(true, this.getApp(), servlet, 3, lDatos);
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
        ex.printStackTrace();
      }
      Inicializar(CInicializar.NORMAL);
      //mandar escribir el fichero
      String localiz = this.panFichero.txtFile.getText();
      EditorFichero edFic = new EditorFichero();
      edFic.escribirFichero(vCampos, apl, lResultado, localiz);
    } //end if isDataValid
  } //end btn_Aceptar

  //comprobar q se encuentran rellenos los campos
  boolean isDataValid() {
    if (panFichero.txtFile.getText().length() == 0) {
      this.getApp().showAdvise("Se debe indicar el directorio elegido");
      return false;
    }
    else {
      return true;
    }
  }
}

class PanExpPoblacPuntos_btnAceptar_actionAdapter
    implements java.awt.event.ActionListener {
  PanExpPoblacPuntos adaptee;

  PanExpPoblacPuntos_btnAceptar_actionAdapter(PanExpPoblacPuntos adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnAceptar_actionPerformed(e);
  }
}
