//Applet para mantenimiento de enfermedades centinelas

package centinelas.cliente.c_mantenfcent;

import capp2.CApp;
import capp2.CInicializar;
import sapp2.Data;

public class AppMantenfcent
    extends CApp
    implements CInicializar {

  PanMantenfcent pan = null;

  public AppMantenfcent() {
  }

  public void init() {
    super.init();
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    Data datos = new Data();
    setTitulo("Mantenimiento de enfermedades centinelas");
    pan = new PanMantenfcent(this);
    VerPanel("", pan);
  }

  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        pan.setEnabled(false);
        break;
      case CInicializar.NORMAL:
        pan.setEnabled(true);
        break;
    }
  }

}