//Title:        Mantenimiento de M�dicos Notificadores Centinelas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       MTR
//Company:
//Description:
/*
 Panel inicial que permite buscar m�dicos notificadores, a�adir nuevos, modificar o borrar
                      uno seleccionado.
 */
package centinelas.cliente.c_mantmednotif;

import java.util.Vector;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CPanel;
import capp2.CTexto;
import centinelas.cliente.c_componentes.CPnlCodigoExt;
import centinelas.cliente.c_componentes.ContCPnlCodigoExt;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.centbasicos.MedCent;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class PanMantmednotif
    extends CPanel
    implements CInicializar, CFiltro, ContCPnlCodigoExt {
  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  XYLayout xyLayout = new XYLayout();
  CPnlCodigoExt pnlCodMed = null;
  CPnlCodigoExt pnlPtoCent = null;

  ButtonControl btnBuscar = new ButtonControl();
  CListaMantenimiento clmMantMedNotif = null;
  Label lblCodMed = new Label();
  Label lblPtoCent = new Label();
  Label lblNombre = new Label();
  Label lblApe1 = new Label();
  Label lblApe2 = new Label();

  Checkbox chActivos = new Checkbox();
  Checkbox chDesactivos = new Checkbox();
  Checkbox chTodos = new Checkbox();
  CheckboxGroup chkGrp1 = new CheckboxGroup();

  CTexto txtNombre = new CTexto(20);
  CTexto txtApe1 = new CTexto(20);
  CTexto txtApe2 = new CTexto(20);

  public PanMantmednotif(CApp a) {
    //configuro los paneles:c�digo m�dico, nombre, ptoscentinelas
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();

    QueryTool qtCodMed = new QueryTool();
    QueryTool qtPtoCent = new QueryTool();

    setApp(a);
    try {
      qtPtoCent.putName("SIVE_PCENTINELA");
      qtPtoCent.putType("CD_PCENTI", QueryTool.STRING);
      qtPtoCent.putType("DS_PCENTI", QueryTool.STRING);

      //paneles
      pnlPtoCent = new CPnlCodigoExt(a,
                                     this,
                                     qtPtoCent,
                                     "CD_PCENTI",
                                     "DS_PCENTI",
                                     false,
                                     "Puntos Centinelas",
                                     "Puntos Centinelas",
                                     this);
      //configuraci�n del panel m�dico, con constructor2 de CPnlCodExt
      qtCodMed.putName("SIVE_MCENTINELA");
      qtCodMed.putType("CD_MEDCEN", QueryTool.STRING);
      qtCodMed.putType("DS_APE1", QueryTool.STRING);
      pnlCodMed = new CPnlCodigoExt(a,
                                    this,
                                    qtCodMed,
                                    "CD_MEDCEN",
                                    "DS_APE1",
                                    false,
                                    "M�dicos Notificadores Centinelas",
                                    "M�dicos Notificadores Centinelas",
                                    this,
                                    true);

      // configura la lista de inspecciones
      // botones
      vBotones.addElement(new CBoton(constantes.keyBtnAnadir,
                                     "images/alta2.gif",
                                     "Nuevo m�dico notificador centinela",
                                     false,
                                     false));

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion2.gif",
                                     "Modificar m�dico notificador centinela",
                                     true,
                                     true));

      vBotones.addElement(new CBoton(constantes.keyBtnBorrar,
                                     "images/baja2.gif",
                                     "Borrar m�dico notificador centinela",
                                     false,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("C�digo m�dico",
                                      "105",
                                      "CD_MEDCEN"));

      vLabels.addElement(new CColumna("Nombre y Apellido",
                                      "315",
                                      "NOM_APE"));

      vLabels.addElement(new CColumna("Pto Centinela",
                                      "115",
                                      "CD_PCENTI"));

      vLabels.addElement(new CColumna("Baja",
                                      "65",
                                      "IT_BAJA"));

      clmMantMedNotif = new CListaMantenimiento(a,
                                                vLabels,
                                                vBotones,
                                                this,
                                                this,
                                                250,
                                                640);
      jbInit();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgBUSCAR = "images/refrescar.gif";

    // carga la imagen
    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().CargaImagenes();

    xyLayout.setHeight(440);
    xyLayout.setWidth(680);
    this.setLayout(xyLayout);

    lblCodMed.setText("C�d M�dico:");
    lblPtoCent.setText("Pto Centinela:");
    lblNombre.setText("Nombre:");
    lblApe1.setText("Apell1:");
    lblApe2.setText("Apell2:");
    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));
    btnBuscar.setLabel("Buscar");
    btnBuscar.addActionListener(new PanMantmednotif_btnBuscar_actionAdapter(this));

    // a�ade los componentes
    this.add(lblPtoCent, new XYConstraints(MARGENIZQ, MARGENSUP, 80, ALTO));
    this.add(pnlPtoCent,
             new XYConstraints(MARGENIZQ + 80 + INTERHOR - DESFPANEL, MARGENSUP,
                               TAMPANEL, 34));
    this.add(lblCodMed,
             new XYConstraints(MARGENIZQ, MARGENSUP + 34 + INTERVERT, 80, ALTO));
    this.add(pnlCodMed,
             new XYConstraints(MARGENIZQ + 80 + INTERHOR - DESFPANEL,
                               MARGENSUP + 34 + INTERVERT, TAMPANEL, 34));

    this.add(chActivos, new XYConstraints(535, MARGENSUP, -1, ALTO));
    this.add(chDesactivos,
             new XYConstraints(535, MARGENSUP + ALTO + 3, -1, ALTO));
    this.add(chTodos,
             new XYConstraints(535, MARGENSUP + 2 * ALTO + 2 * 3, -1, ALTO));

    this.add(lblNombre,
             new XYConstraints(MARGENIZQ, MARGENSUP + 2 * 34 + 2 * INTERVERT,
                               50, ALTO));
    this.add(txtNombre,
             new XYConstraints(MARGENIZQ + 80 + INTERHOR,
                               MARGENSUP + 2 * 34 + 2 * INTERVERT, 130, ALTO));

    this.add(lblApe1,
             new XYConstraints(MARGENIZQ + 80 + 130 + 2 * INTERHOR,
                               MARGENSUP + 2 * 34 + 2 * INTERVERT, 40, ALTO));
    this.add(txtApe1,
             new XYConstraints(MARGENIZQ + 80 + 40 + 130 + 3 * INTERHOR,
                               MARGENSUP + 2 * 34 + 2 * INTERVERT, 130, ALTO));
    this.add(lblApe2,
             new XYConstraints(MARGENIZQ + 80 + 40 + 2 * 130 + 4 * INTERHOR,
                               MARGENSUP + 2 * 34 + 2 * INTERVERT, 40, ALTO));
    this.add(txtApe2,
             new XYConstraints(MARGENIZQ + 80 + 2 * 40 + 2 * 130 + 5 * INTERHOR,
                               MARGENSUP + 2 * 34 + 2 * INTERVERT, 130, ALTO));

    this.add(btnBuscar,
             new XYConstraints(535, MARGENSUP + 2 * 34 + ALTO + 3 * INTERVERT,
                               80, ALTO));
    this.add(clmMantMedNotif,
             new XYConstraints(CPnlCodigoExt.LONG_SEP,
                               MARGENSUP + 2 * 34 + 2 * ALTO + 4 * INTERVERT,
                               650, 230));

    chActivos.setLabel("Activos");
    chActivos.setCheckboxGroup(chkGrp1);
    chActivos.addItemListener(new PanMantmednotif_chActivos_itemAdapter(this));
    chDesactivos.setLabel("Desactivos");
    chDesactivos.setCheckboxGroup(chkGrp1);
    chDesactivos.addItemListener(new PanMantmednotif_chDesactivos_itemAdapter(this));
    chTodos.setLabel("Todos");
    chTodos.setCheckboxGroup(chkGrp1);
    chTodos.addItemListener(new PanMantmednotif_chTodos_itemAdapter(this));
    chTodos.setState(true);

  }

  public void trasEventoEnCPnlCodigoExt() {}

  public String getDescCompleta(Data dat) {
    return (dat.getString("DS_APE1") + " " + dat.getString("DS_APE2"));
  }

  public void Inicializar() {
  }

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera: se hace un backup de los datos
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        pnlCodMed.backupDatos();
        pnlPtoCent.backupDatos();
        this.setEnabled(false);
        break;

        // modo normal
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        pnlCodMed.setEnabled(true);
        pnlPtoCent.setEnabled(true);
        pnlCodMed.setEnabled(true);
        clmMantMedNotif.setEnabled(true);
        lblCodMed.setEnabled(true);
        lblNombre.setEnabled(true);
        lblApe1.setEnabled(true);
        lblApe2.setEnabled(true);
        txtNombre.setEnabled(true);
        txtApe1.setEnabled(true);
        txtApe2.setEnabled(true);
        lblPtoCent.setEnabled(true);
        chActivos.setEnabled(true);
        chDesactivos.setEnabled(true);
        chTodos.setEnabled(true);
        btnBuscar.setEnabled(true);
    }
  }

  private QueryTool2 obtenerActivos(QueryTool2 qt) {
    qt.putWhereType("IT_BAJA", QueryTool.STRING);
    qt.putWhereValue("IT_BAJA", "N");
    qt.putOperator("IT_BAJA", "=");
    return qt;
  }

  private QueryTool2 obtenerDesactivos(QueryTool2 qt) {
    qt.putWhereType("IT_BAJA", QueryTool.STRING);
    qt.putWhereValue("IT_BAJA", "S");
    qt.putOperator("IT_BAJA", "=");
    return qt;
  }

  private QueryTool2 obtenerPuntoCentinela(QueryTool2 qt) {
    qt.putWhereType("CD_PCENTI", QueryTool.STRING);
    qt.putWhereValue("CD_PCENTI", pnlPtoCent.getDatos().getString("CD_PCENTI"));
    qt.putOperator("CD_PCENTI", "=");
    return qt;
  }

  private QueryTool2 obtenerNombre(QueryTool2 qt) {
    qt.putWhereType("DS_NOMBRE", QueryTool.STRING);
    qt.putWhereValue("DS_NOMBRE", txtNombre.getText());
    qt.putOperator("DS_NOMBRE", "=");
    return qt;
  }

  private QueryTool2 obtenerApellido1(QueryTool2 qt) {
    qt.putWhereType("DS_APE1", QueryTool.STRING);
    qt.putWhereValue("DS_APE1", txtApe1.getText());
    qt.putOperator("DS_APE1", "=");
    return qt;
  }

  private QueryTool2 obtenerApellido2(QueryTool2 qt) {
    qt.putWhereType("DS_APE2", QueryTool.STRING);
    qt.putWhereValue("DS_APE2", txtApe2.getText());
    qt.putOperator("DS_APE2", "=");
    return qt;
  }

  private QueryTool2 obtenerMedico(QueryTool2 qt) {
    qt.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qt.putWhereValue("CD_MEDCEN", pnlCodMed.getDatos().getString("CD_MEDCEN"));
    qt.putOperator("CD_MEDCEN", "=");
    return qt;
  }

  Lista anadirCampo(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = new Data();
      dCampo = (Data) lCampo.elementAt(i);
      String s = new String();
      s = dCampo.getString("DS_APE1") + " " + dCampo.getString("DS_APE2") + " " +
          dCampo.getString("DS_NOMBRE");
      ( (Data) lCampo.elementAt(i)).put("NOM_APE", s);
    }
    return lCampo;
  }

  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    try {
      QueryTool2 qt = new QueryTool2();

      // Nombre de la tabla (FROM)
      qt.putName("SIVE_MCENTINELA");

      // Campos que se quieren leer (SELECT)
      qt.putType("CD_PCENTI", QueryTool.STRING);
      qt.putType("CD_MEDCEN", QueryTool.STRING);
      qt.putType("CD_E_NOTIF", QueryTool.STRING);
      qt.putType("CD_SEXO", QueryTool.STRING);
      qt.putType("CD_MASIST", QueryTool.STRING);
      qt.putType("CD_ANOEPIA", QueryTool.STRING);
      qt.putType("CD_SEMEPIA", QueryTool.STRING);
      qt.putType("DS_APE1", QueryTool.STRING);
      qt.putType("DS_APE2", QueryTool.STRING);
      qt.putType("DS_NOMBRE", QueryTool.STRING);
      qt.putType("DS_DNI", QueryTool.STRING);
      qt.putType("DS_TELEF", QueryTool.STRING);
      qt.putType("DS_FAX", QueryTool.STRING);
      qt.putType("FC_NAC", QueryTool.DATE);
      qt.putType("FC_ALTA", QueryTool.DATE);
      qt.putType("CD_OPE", QueryTool.STRING);
      qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);
      qt.putType("NMREDADI", QueryTool.INTEGER);
      qt.putType("NMREDADF", QueryTool.INTEGER);
      qt.putType("DS_HORAI", QueryTool.STRING);
      qt.putType("DS_HORAF", QueryTool.STRING);
      qt.putType("IT_BAJA", QueryTool.STRING);
      qt.putType("FC_BAJA", QueryTool.DATE);
      qt.putType("CD_MOTBAJA", QueryTool.STRING);
      qt.putType("CD_ANOBAJA", QueryTool.STRING);
      qt.putType("CD_SEMBAJA", QueryTool.STRING);

      if (chActivos.getState()) {
        qt = obtenerActivos(qt);
      }
      if (chDesactivos.getState()) {
        qt = obtenerDesactivos(qt);
      }
      if (pnlPtoCent.getDatos() != null) {
        qt = obtenerPuntoCentinela(qt);
      }
      if (pnlCodMed.getDatos() != null) {
        qt = obtenerMedico(qt);
      }
      if (! (txtNombre.getText().equals(""))) {
        qt = obtenerNombre(qt);
      }
      if (! (txtApe1.getText().equals(""))) {
        qt = obtenerApellido1(qt);
      }
      if (! (txtApe2.getText().equals(""))) {
        qt = obtenerApellido2(qt);
      }
      //Descripci�n de pto centinela para sacarla en la tabla
      QueryTool qtAdic = new QueryTool();
      qtAdic.putName("SIVE_PCENTINELA");
      qtAdic.putType("DS_PCENTI", QueryTool.STRING);
      Data dtAdic = new Data();
      dtAdic.put("CD_PCENTI", QueryTool.STRING);
      qt.addQueryTool(qtAdic);
      qt.addColumnsQueryTool(dtAdic);

      //Descripci�n de equipo notificador
      QueryTool qtAdic1 = new QueryTool();
      qtAdic1.putName("SIVE_E_NOTIF");
      qtAdic1.putType("DS_E_NOTIF", QueryTool.STRING);
      Data dtAdic1 = new Data();
      dtAdic1.put("CD_E_NOTIF", QueryTool.STRING);
      qt.addQueryTool(qtAdic1);
      qt.addColumnsQueryTool(dtAdic1);

      qt.addOrderField("CD_MEDCEN");

      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() == 0) {
        this.getApp().showAdvise(
            "No se poseen elementos con los criterios especificados");
        chActivos.setState(false);
        chDesactivos.setState(false);
        chTodos.setState(true);
        pnlPtoCent.limpiarDatos();
        pnlCodMed.limpiarDatos();
        txtNombre.setText("");
        txtApe1.setText("");
        txtApe2.setText("");
      }
      else {
        p1 = anadirCampo(p1);
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    return p1;
  }

  public Lista siguientePagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    try {
      QueryTool2 qt = new QueryTool2();

      // Nombre de la tabla (FROM)
      qt.putName("SIVE_MCENTINELA");

      // Campos que se quieren leer (SELECT)
      qt.putType("CD_PCENTI", QueryTool.STRING);
      qt.putType("CD_MEDCEN", QueryTool.STRING);
      qt.putType("CD_E_NOTIF", QueryTool.STRING);
      qt.putType("CD_SEXO", QueryTool.STRING);
      qt.putType("CD_MASIST", QueryTool.STRING);
      qt.putType("CD_ANOEPIA", QueryTool.STRING);
      qt.putType("CD_SEMEPIA", QueryTool.STRING);
      qt.putType("DS_APE1", QueryTool.STRING);
      qt.putType("DS_APE2", QueryTool.STRING);
      qt.putType("DS_NOMBRE", QueryTool.STRING);
      qt.putType("DS_DNI", QueryTool.STRING);
      qt.putType("DS_TELEF", QueryTool.STRING);
      qt.putType("DS_FAX", QueryTool.STRING);
      qt.putType("FC_NAC", QueryTool.DATE);
      qt.putType("FC_ALTA", QueryTool.DATE);
      qt.putType("CD_OPE", QueryTool.STRING);
      qt.putType("FC_ULTACT", QueryTool.TIMESTAMP);
      qt.putType("NMREDADI", QueryTool.INTEGER);
      qt.putType("NMREDADF", QueryTool.INTEGER);
      qt.putType("DS_HORAI", QueryTool.STRING);
      qt.putType("DS_HORAF", QueryTool.STRING);
      qt.putType("IT_BAJA", QueryTool.STRING);
      qt.putType("FC_BAJA", QueryTool.DATE);
      qt.putType("CD_MOTBAJA", QueryTool.STRING);
      qt.putType("CD_ANOBAJA", QueryTool.STRING);
      qt.putType("CD_SEMBAJA", QueryTool.STRING);

      if (chActivos.getState()) {
        qt = obtenerActivos(qt);
      }
      if (chDesactivos.getState()) {
        qt = obtenerDesactivos(qt);
      }
      if (pnlPtoCent.getDatos() != null) {
        qt = obtenerPuntoCentinela(qt);
      }
      if (pnlCodMed.getDatos() != null) {
        qt = obtenerMedico(qt);
      }

      if (! (txtNombre.getText().equals(""))) {
        qt = obtenerNombre(qt);
      }
      if (! (txtApe1.getText().equals(""))) {
        qt = obtenerApellido1(qt);
      }
      if (! (txtApe2.getText().equals(""))) {
        qt = obtenerApellido2(qt);
      }

      //Descripci�n de pto centinela para sacarla en la tabla
      QueryTool qtAdic = new QueryTool();
      qtAdic.putName("SIVE_PCENTINELA");
      qtAdic.putType("DS_PCENTI", QueryTool.STRING);
      Data dtAdic = new Data();
      dtAdic.put("CD_PCENTI", QueryTool.STRING);
      qt.addQueryTool(qtAdic);
      qt.addColumnsQueryTool(dtAdic);

      qt.addOrderField("CD_MEDCEN");

      p.addElement(qt);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() == 0) {
        this.getApp().showAdvise(
            "No se poseen elementos con los criterios especificados");
        chActivos.setState(false);
        chDesactivos.setState(false);
        chTodos.setState(true);
        pnlPtoCent.limpiarDatos();
        pnlCodMed.limpiarDatos();
        txtNombre.setText("");
        txtApe1.setText("");
        txtApe2.setText("");
      }
      else {
        p1 = anadirCampo(p1);
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    //  }//Fin del primer caso
    return p1;
  }

  public void realizaOperacion(int j) {
    DiaMantmednotif di = null;
    Data dMantMedNotif = new Data();

    switch (j) {
      // bot�n de alta
      case ALTA:
        MedCent dAlta = new MedCent(dMantMedNotif);
        di = new DiaMantmednotif(this.getApp(), ALTA, dAlta);
        di.show();
        // a�adir el nuevo elem a la lista
        if (di.bAceptar) {
          clmMantMedNotif.setPrimeraPagina(primeraPagina());
        }
        break;

        // bot�n de modificaci�n
      case MODIFICACION:
        dMantMedNotif = clmMantMedNotif.getSelected();
        //si existe alguna fila seleccionada
        if (dMantMedNotif != null) {
          MedCent dModif = new MedCent(dMantMedNotif);
          int ind = clmMantMedNotif.getSelectedIndex();
          di = new DiaMantmednotif(this.getApp(), MODIFICACION, dModif);
          di.show();
          //Tratamiento de bAceptar para Salir
          if (di.bAceptar) {
            Data nom = new Data();
            nom = di.devuelveData();
            if (! (txtNombre.getText().equals(""))) {
              txtNombre.setText(nom.getString("DS_NOMBRE"));
            }
            if (! (txtApe1.getText().equals(""))) {
              txtApe1.setText(nom.getString("DS_APE1"));
            }
            if (! (txtApe2.getText().equals(""))) {
              txtApe2.setText(nom.getString("DS_APE2"));
            }
            clmMantMedNotif.setPrimeraPagina(primeraPagina());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;

        // bot�n de baja
      case BAJA:
        dMantMedNotif = clmMantMedNotif.getSelected();
        if (dMantMedNotif != null) {
          int ind = clmMantMedNotif.getSelectedIndex();
          MedCent dBaja = new MedCent(dMantMedNotif);
          di = new DiaMantmednotif(this.getApp(), BAJA, dBaja);
          di.show();
          if (di.bAceptar) {
            clmMantMedNotif.getLista().removeElementAt(ind);
            clmMantMedNotif.setPrimeraPagina(primeraPagina());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla");
        }
        break;
    }
  }

  void btnBuscar_actionPerformed(ActionEvent e) {
    Inicializar(CInicializar.ESPERA);
    clmMantMedNotif.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
  }

  void chActivos_itemStateChanged(ItemEvent e) {
    if (chActivos.getState()) {
      chDesactivos.setState(false);
      chTodos.setState(false);
    }
  }

  void chDesactivos_itemStateChanged(ItemEvent e) {
    if (chDesactivos.getState()) {
      chActivos.setState(false);
      chTodos.setState(false);
    }
  }

  void chTodos_itemStateChanged(ItemEvent e) {
    if (chTodos.getState()) {
      chActivos.setState(false);
      chDesactivos.setState(false);
    }
  }

}

class PanMantmednotif_btnBuscar_actionAdapter
    implements java.awt.event.ActionListener {
  PanMantmednotif adaptee;

  PanMantmednotif_btnBuscar_actionAdapter(PanMantmednotif adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnBuscar_actionPerformed(e);
  }
}

class PanMantmednotif_chActivos_itemAdapter
    implements java.awt.event.ItemListener {
  PanMantmednotif adaptee;

  PanMantmednotif_chActivos_itemAdapter(PanMantmednotif adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chActivos_itemStateChanged(e);
  }
}

class PanMantmednotif_chDesactivos_itemAdapter
    implements java.awt.event.ItemListener {
  PanMantmednotif adaptee;

  PanMantmednotif_chDesactivos_itemAdapter(PanMantmednotif adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chDesactivos_itemStateChanged(e);
  }
}

class PanMantmednotif_chTodos_itemAdapter
    implements java.awt.event.ItemListener {
  PanMantmednotif adaptee;

  PanMantmednotif_chTodos_itemAdapter(PanMantmednotif adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chTodos_itemStateChanged(e);
  }
}
