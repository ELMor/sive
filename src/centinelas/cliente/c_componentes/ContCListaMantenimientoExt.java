//Interfaz que debe implementar el contenedor de una CListaMantenimientoExt

package centinelas.cliente.c_componentes;

import java.util.Vector;

public interface ContCListaMantenimientoExt {

  void cambiarBotones(Vector vectBotones);

}