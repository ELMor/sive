//Interfaz que debe implementar cualquier contenedor del PanFecha

package centinelas.cliente.c_componentes;

import capp2.CApp;
import capp2.CInicializar;

public interface ContPanFecha
    extends CInicializar {
  /* Para obtener el applet*/
  public CApp getMiCAppFec();

  /* M�todos ser�n llamados desde el PanFecha despu�s de que el propio PanFecha
   * ejecute su codigo en la p�rdida de foco */

  public void alPerderFocoAnoFec();

  public void alPerderFocoSemanaFec();

  public void alPerderFocoFechaFec();

}
