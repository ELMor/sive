/*Clase que permite mostrar columnas con im�genes en la CListaMantenimiento
 Forma de uso: Como la CListaMantenimiento pero:
  Al a�adir una columna que sea de tipo imagen se pone
     datPregunta.put("CONTROL",this.getApp().getLibImagenes().get(imgAceptar));
 */
package centinelas.cliente.c_componentes;

import java.util.Vector;

import java.awt.Image;

import capp2.CApp;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import jclass.util.JCVector;
import sapp2.Data;
import sapp2.Lista;

public class CListaMantConImagenes
    extends CListaMantenimiento {

  // constructor : Mismo que el del padre
  public CListaMantConImagenes(CApp a,
                               Vector labels,
                               Vector botones,
                               CInicializar ci,
                               CFiltro cf,
                               int altura,
                               int anchura) {
    super(a,
          labels,
          botones,
          ci,
          cf,
          altura,
          anchura);
    //Fija la altura de las filas a un valor que permita albergar la imagen
    tabla.setRowHeight(22);
  }

  // constructor : Mismo que el del padre a�adienndo altura max de imagenes
  public CListaMantConImagenes(CApp a,
                               Vector labels,
                               Vector botones,
                               CInicializar ci,
                               CFiltro cf,
                               int altura,
                               int anchura,
                               int alturaMaxImagen) {
    super(a,
          labels,
          botones,
          ci,
          cf,
          altura,
          anchura);
    //Fija la altura de las filas a un valor que permita albergar la imagen
    tabla.setRowHeight(alturaMaxImagen);
  }

  // pinta la primera trama en la tabla
  public void setPrimeraPagina(Lista v) {
    String sFila = null;
    Data dt = null;
    CColumnaImagen lbl = null;
    JCVector jcvTabla = new JCVector();
    vSnapShot = v;

    // escribe las l�neas en la tabla
    tabla.clear();
    if (vSnapShot.size() > 0) {
      for (int j = 0; j < vSnapShot.size(); j++) {
        sFila = "";
        dt = (Data) vSnapShot.elementAt(j);

        JCVector jcvLinea = new JCVector();

        //Busca datos de cada linea en un elemento del vector vSnapShot
        for (int i = 0; i < vLabels.size(); i++) {
          lbl = (CColumnaImagen) vLabels.elementAt(i);

          if (lbl.isImage()) {

            //Si la columna representa una imagen
            //En el campo correspondiente de la Hash debe ir una imagen, que es lo que se recoge
            jcvLinea.addElement( (Image) dt.get(lbl.getField()));
          }
          else {
            jcvLinea.addElement(dt.get(lbl.getField()));
          }
        } //for de cada linea

        jcvTabla.addElement(jcvLinea);

      } //For recorre lista datos
      tabla.setItems(jcvTabla);

      // verifica que sea la �ltima trama
      if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
        tabla.addItem("M�s datos ...");

        //Selecci�n de elementos
      }
      setCurrent(0);
    } //For recorre lista datos
  }

  // pinta las siguientes tramas
  public void setSiguientePagina(Lista v) {
    String sFila = null;
    Data dt = null;
    CColumnaImagen lbl = null;
    JCVector jcvTabla = new JCVector();

    int iLast = tabla.countItems() - 1;
    vSnapShot.addElements(v);

    // escribe las l�neas en la tabla
    tabla.deleteItem(iLast);
    if (vSnapShot.size() >= iLast) {
      for (int j = iLast; j < vSnapShot.size(); j++) {
        sFila = "";
        dt = (Data) vSnapShot.elementAt(j);

        JCVector jcvLinea = new JCVector();

        //Busca datos de cada linea en un elemento del vector vSnapShot
        for (int i = 0; i < vLabels.size(); i++) {
          lbl = (CColumnaImagen) vLabels.elementAt(i);

          if (lbl.isImage()) {

            //Si la columna representa una imagen
            //En el campo correspondiente de la Hash debe ir una imagen, que es lo que se recoge
            jcvLinea.addElement( (Image) dt.get(lbl.getField()));
          }
          else {
            jcvLinea.addElement(dt.get(lbl.getField()));
          }
        } //for de cada linea

        jcvTabla.addElement(jcvLinea);

      } //for lista recorre datos
      tabla.setItems(jcvTabla);

      // verifica que sea la �ltima trama
      if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
        tabla.addItem("M�s datos ...");

        //Selecci�n de elementos
      }
      setCurrent(iLast);
    } //if
  }

}