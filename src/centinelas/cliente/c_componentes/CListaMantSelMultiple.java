package centinelas.cliente.c_componentes;

import java.util.Vector;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CContextHelp;
import capp2.CInicializar;
import capp2.CPanel;
import capp2.CTabla;
import capp2.UButtonControl;
import jclass.bwt.JCItemEvent;
import jclass.bwt.JCItemListener;
import sapp2.Data;
import sapp2.Lista;

public class CListaMantSelMultiple
    extends CPanel {

  // lista
  protected Lista vSnapShot = new Lista();

  // indica la fila seleccionada en la tabla
  public int m_itemSelecc = -1;

  // vectores de la botonera
  protected Vector vCnfBotones = null;
  protected Vector vBotones = new Vector();

  // configuraci�n de la tabla
  protected Vector vLabels = null;

  // contenedor
  protected CInicializar ciContenedor = null;

  // filtro
//  protected CFiltro cfFiltro = null;

  // modo de operaci�n
  protected int modoOperacion = CInicializar.NORMAL;

  //Indica si el tama�o de la lista es el est�ndar o ha sido introducido por
  //el usuario.

  //Datos del tama�o del componente
  protected int Altura = 304;
  protected int Anchura = 715;
  protected static int AnchoBotones = 35;

  XYLayout xYLayout1 = new XYLayout();
  public CTabla tabla = new CTabla();
  ButtonControl btnPrimero = new ButtonControl();
  ButtonControl btnAnterior = new ButtonControl();
  ButtonControl btnSiguiente = new ButtonControl();
  ButtonControl btnUltimo = new ButtonControl();

  btnActionListener actionListener = new btnActionListener(this);
//  tblActionAdapter actionAdapter = new tblActionAdapter(this);
  tblItemListener itemListener = new tblItemListener(this);

  //funciones que devuelven las posiciones de los componentes de
  //la lista en funci�n del tama�o del contenedor.

  protected int AnchoTabla() {
    return (Anchura - 20);
  }

  protected int AltoTabla() {
    return (Altura - 68);
  }

  protected int PosXBotones() {
    return (AnchoTabla() - 120);
  }

  protected int PosYBotones() {
    return (AltoTabla() + 8);
  }

  // constructores
  public CListaMantSelMultiple(CApp a,
                               Vector labels,
                               //                            Vector botones,
                               CInicializar ci) {
//                             CFiltro cf) {

    try {
      setApp(a);
      vLabels = labels;
//      vCnfBotones = botones;
      ciContenedor = ci;
//      cfFiltro = cf;
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public CListaMantSelMultiple(CApp a,
                               Vector labels,
//                             Vector botones,
                               CInicializar ci,
//                             CFiltro cf,
                               int altura,
                               int anchura) {

    try {
      setApp(a);
      vLabels = labels;

//      vCnfBotones = botones;
      ciContenedor = ci;
//      cfFiltro = cf;
      Anchura = anchura;
      Altura = altura;
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicializa el aspecto del panel
  public void jbInit() throws Exception {
    // im�genes
    final String imgPRIMERO = "images/primero.gif";
    final String imgANTERIOR = "images/anterior.gif";
    final String imgSIGUIENTE = "images/siguiente.gif";
    final String imgULTIMO = "images/ultimo.gif";

    CBoton btn = null;
    CColumna lbl = null;
    UButtonControl btnControl = null;
    int j;
    String sBtnStrings = "";
    String sWidths = "";

    xYLayout1.setWidth(Anchura);
    xYLayout1.setHeight(Altura);
    this.setLayout(xYLayout1);

    // botones de control en la tabla
    btnPrimero.setActionCommand("primero");
    this.getApp().getLibImagenes().put(imgPRIMERO);
    new CContextHelp("Primero", btnPrimero);
    btnAnterior.setActionCommand("anterior");
    this.getApp().getLibImagenes().put(imgANTERIOR);
    new CContextHelp("Anterior", btnAnterior);
    btnSiguiente.setActionCommand("siguiente");
    this.getApp().getLibImagenes().put(imgSIGUIENTE);
    new CContextHelp("Siguiente", btnSiguiente);
    btnUltimo.setActionCommand("ultimo");
    this.getApp().getLibImagenes().put(imgULTIMO);
    new CContextHelp("Ultimo", btnUltimo);

    // crea la botonera
    /*    for(j=0; j<vCnfBotones.size(); j++) {
          // configuracion
          btn = (CBoton) vCnfBotones.elementAt(j);
          // imagen
          this.getApp().getLibImagenes().put(btn.getImage());
          // crea el control
          btnControl = new UButtonControl(btn.getName(), getApp());
          btnControl.setActionCommand((new Integer(j)).toString());
          new CContextHelp(btn.getToolTip(), btnControl);
          vBotones.addElement(btnControl);
          // a�ade el escuchador
          btnControl.addActionListener(actionListener);
        }*/

    // carga las imagenes
    this.getApp().getLibImagenes().CargaImagenes();

    // a�ade la tabla

    this.add(tabla, new XYConstraints(9, 2, AnchoTabla(), AltoTabla()));

    this.add(btnPrimero, new XYConstraints(PosXBotones(),
                                           PosYBotones(), 22, 22));
    this.add(btnAnterior, new XYConstraints(PosXBotones() + AnchoBotones,
                                            PosYBotones(), 22, 22));
    this.add(btnSiguiente, new XYConstraints(PosXBotones() + (2 * AnchoBotones),
                                             PosYBotones(), 22, 22));
    this.add(btnUltimo, new XYConstraints(PosXBotones() + (3 * AnchoBotones),
                                          PosYBotones(), 22, 22));

    // copia las im�genes en los botones
    btnPrimero.setImage(this.getApp().getLibImagenes().get(imgPRIMERO));
    btnAnterior.setImage(this.getApp().getLibImagenes().get(imgANTERIOR));
    btnSiguiente.setImage(this.getApp().getLibImagenes().get(imgSIGUIENTE));
    btnUltimo.setImage(this.getApp().getLibImagenes().get(imgULTIMO));

    // configuraci�n de la tabla
    for (j = 0; j < vLabels.size(); j++) {
      lbl = (CColumna) vLabels.elementAt(j);
      sBtnStrings = sBtnStrings + lbl.getLabel() + "\n";
      sWidths = sWidths + lbl.getSize() + "\n";
    }
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(
        sBtnStrings, '\n'));
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(sWidths, '\n'));
    tabla.setNumColumns(vLabels.size());
    //Se permite la selecci�n de m�ltiples elementos de la lista
    tabla.setAllowMultipleSelections(true);

    // a�ade los escuchadores
//    tabla.addActionListener(actionAdapter);
    tabla.addItemListener(itemListener);
    btnPrimero.addActionListener(actionListener);
    btnAnterior.addActionListener(actionListener);
    btnSiguiente.addActionListener(actionListener);
    btnUltimo.addActionListener(actionListener);

    setBorde(false);

  }

  // establece la fila actual
  public void setCurrent(int i) {

    if (i >= tabla.countItems()) {
      i = tabla.countItems() - 1;

    }
    if (i > 0) {
      tabla.select(i);
      m_itemSelecc = i;
    }
  }

  public void Inicializar() {
    if (this.ciContenedor != null) {
      this.ciContenedor.Inicializar(this.modoOperacion);
    }
  }

  // limpia la pantalla
  public void vaciarPantalla() {
    if (tabla.countItems() > 0) {
      tabla.deleteItems(0, tabla.countItems() - 1);
    }
  }

  // Botonera
  /*  public void btn_actionPermormed(int i) {
      if (this.cfFiltro != null) {
        this.modoOperacion = CInicializar.ESPERA;
        Inicializar();
        // Esto filtra, por si se ha pulsado el bot�n de modificar
        // o borrar, teniendo seleccionado "mas datos"
          //Si no hay ninguna seleccionada o est� el m�s selecionado
           //y estamos en modificar o borrar (i!=0) , no hace nada
          if ((getSelected()== null)&&
              (i != 0)) {}               //(LRG)
          else{
            this.cfFiltro.realizaOperacion(i);
          }
        this.modoOperacion = CInicializar.NORMAL;
        Inicializar();
      }
    }*/

  // gesti�n de los botones de control
  public void btn_actionPerformed(String actionCommand) {
    CBoton btn = null;

    // ir al primer registro de la tabla
    if (actionCommand.equals("primero")) {
      if (tabla.countItems() > 0) {
        m_itemSelecc = 0;
        tabla.select(m_itemSelecc);
        tabla.setTopRow(m_itemSelecc);
      }

      // anterior
    }
    else if (actionCommand.equals("anterior")) {
      if (m_itemSelecc > 0) {
        m_itemSelecc--;
        tabla.select(m_itemSelecc);
        if (m_itemSelecc - tabla.getTopRow() <= 0) {
          tabla.setTopRow(tabla.getTopRow() - 1);
        }
      }

      // siguiente
    }
    else if (actionCommand.equals("siguiente")) {
      if (m_itemSelecc < tabla.countItems() - 1) {
        m_itemSelecc++;
        tabla.select(m_itemSelecc);
        if (m_itemSelecc - tabla.getTopRow() >= 8) {
          tabla.setTopRow(tabla.getTopRow() + 1);
        }
      }

      // �ltimo
    }
    else if (actionCommand.equals("ultimo")) {
      if (tabla.countItems() - 1 >= 0) {
        m_itemSelecc = tabla.countItems() - 1;
        tabla.select(m_itemSelecc);
        if (tabla.countItems() >= 8) {
          tabla.setTopRow(tabla.countItems() - 8);
        }
        else {
          tabla.setTopRow(0);
        }
      }
    }

    // botonera
    /*    } else {
          btn_actionPermormed(Integer.parseInt(actionCommand));
        }*/
  }

  // devuelve las filas seleccionadas
  public Lista getSelected() {
    Lista lSel = new Lista();
    int[] indice = null;
    indice = tabla.getSelectedIndexes();

//    if (indice != BWTEnum.NOTFOUND)
    if (indice != null) {
      for (int i = 0; i < indice.length; i++) {
        Data dFilSel = (Data) vSnapShot.elementAt(indice[i]);
        lSel.addElement(dFilSel);
      }
    }

    return lSel;
  }

  // devuelve los �ndices de las filas seleccionadas
  public int[] getSelectedIndexes() {
    return tabla.getSelectedIndexes();
  }

  // establece el �ndice seleccionado
  public void tabla_itemStateChanged(JCItemEvent evt) {
    m_itemSelecc = tabla.getSelectedIndex();
  }

  // devuelve la lista
  public Lista getLista() {
    return vSnapShot;
  }

  // pinta la primera trama en la tabla
  public void setPrimeraPagina(Lista v) {
    String sFila = null;
    Data dt = null;
    CColumna lbl = null;

    vSnapShot = v;

    // escribe las l�neas en la tabla
    tabla.clear();
    if (vSnapShot.size() > 0) {
      for (int j = 0; j < vSnapShot.size(); j++) {
        sFila = "";
        dt = (Data) vSnapShot.elementAt(j);

        for (int i = 0; i < vLabels.size(); i++) {
          lbl = (CColumna) vLabels.elementAt(i);
          sFila = sFila + dt.getString(lbl.getField()) + "&";
        }
        tabla.addItem(sFila, '&');
      }

      // verifica que sea la �ltima trama
      if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
        tabla.addItem("M�s datos ...");

        //Selecci�n de elementos
      }
      setCurrent(0);
    }
  }

  // pinta las siguientes tramas
  public void setSiguientePagina(Lista v) {
    String sFila = null;
    Data dt = null;
    CColumna lbl = null;
    int iLast = tabla.countItems() - 1;

    vSnapShot.addElements(v);

    // escribe las l�neas en la tabla
    tabla.deleteItem(iLast);
    if (vSnapShot.size() >= iLast) {
      for (int j = iLast; j < vSnapShot.size(); j++) {
        sFila = "";
        dt = (Data) vSnapShot.elementAt(j);

        for (int i = 0; i < vLabels.size(); i++) {
          lbl = (CColumna) vLabels.elementAt(i);
          sFila = sFila + dt.get(lbl.getField()) + "&";
        }
        tabla.addItem(sFila, '&');
      }

      // verifica que sea la �ltima trama
      if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
        tabla.addItem("M�s datos ...");

        //Selecci�n de elementos
      }
      setCurrent(iLast);
    }
  }

}

    /******************************************************************************/

// action listener de evento en botones
class btnActionListener
    implements ActionListener, Runnable {
  CListaMantSelMultiple adaptee = null;
  String actionCommand = null;

  public btnActionListener(CListaMantSelMultiple adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    actionCommand = e.getActionCommand();
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.btn_actionPerformed(actionCommand);
  }
}

// navegaci�n por la tabla con los botones de ayuda
class tblItemListener
    implements JCItemListener {
  CListaMantSelMultiple adaptee = null;

  public tblItemListener(CListaMantSelMultiple adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(JCItemEvent e) {
    if (e.getStateChange() == JCItemEvent.SELECTED) { //evento seleccion (no deseleccion)
      adaptee.tabla_itemStateChanged(e);
    }
  }
}
// escuchador de los click en la tabla
/*class tblActionAdapter implements jclass.bwt.JCActionListener, Runnable{
  CListaMantSelMultiple adaptee;
  public tblActionAdapter(CListaMantSelMultiple adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(JCActionEvent e) {
    new Thread(this).start();
  }
  public void run() {
    adaptee.tabla_actionPerformed();
  }
 }   */
