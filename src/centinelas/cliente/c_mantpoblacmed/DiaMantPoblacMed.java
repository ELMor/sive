package centinelas.cliente.c_mantpoblacmed;

import java.util.Vector;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CColumna;
import capp2.CDialog;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import centinelas.cliente.c_componentes.ChoiceCDDS;
import centinelas.cliente.c_comuncliente.BDatos;
import centinelas.cliente.c_comuncliente.nombreservlets;
import centinelas.datos.centbasicos.MedCent;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

public class DiaMantPoblacMed
    extends CDialog
    implements CInicializar, CFiltro {

  final public int modoALTA = 0;
  final public int modoMODIFICACION = 1;
  final public int modoBAJA = 2;

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;
  final int DESFPANEL = 5;
  final int TAMPANEL = 320;

  final public int ALTA = 0;
  final public int MODIFICACION = 1;
  final public int BAJA = 2;

  // parametros pasados al di�logo
  public MedCent dtDev = null;
  //par�metro q te pasa el DiaAgrupEdad
  public String devAgrup = null;

  private boolean tieneDatos = false;
  private String interv = new String();

  public boolean bAceptar = false;
  public int borrPto = 0;
  boolean noMensaje = false;
  Lista lClm = new Lista();

  // modo de operaci�n
  public int modoOperacion;

  XYLayout xyLayout1 = new XYLayout();

  Label lblPuntoCent = new Label();
  Label lblMedico = new Label();
  Label lblDesPto = new Label();
  Label lblDesMedico = new Label();
  Label lblEquipo = new Label();
  Label lblCentro = new Label();
  Label lblDesEquipo = new Label();
  Label lblDesCentro = new Label();
  Label lblAno = new Label();
  Label lblAgrupacion = new Label();

  ChoiceCDDS chAno = null;
  // Lista con agrupaciones.
  ChoiceCDDS chListaAgrupacion = null;

  CListaMantenimiento clmRangoEdad = null;

  ButtonControl btnBorrar = new ButtonControl();
  ButtonControl btnCrear = new ButtonControl();
  ButtonControl btnModificar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();

//  final String srvTrans = "servlet/SrvTransaccion";
  final String srvTrans = nombreservlets.strSERVLET_TRANSACCION;

  Lista lBorrar = null;
  boolean borrLBorrar = false;
  boolean creadoNuevoRango = false;
  // ver si existe o no rango para ese a�o. (ARS 24-04-01)
  boolean rangoEdadExistente = false;
  boolean modificarRangoAno = false;

  // Escuchadores de eventos...
  DialogActionAdapter actionAdapter = new DialogActionAdapter(this);

  public DiaMantPoblacMed(CApp a, int modoop, MedCent dt) {
    super(a);
    modoOperacion = modoop;
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    QueryTool qt = new QueryTool();

    dtDev = dt;
    // longitud de los campos de texto y �rea...
    try {
      //Configuro el combo de A�o
      Lista lAno = obtenerAno();
      chAno = new ChoiceCDDS(true, true, false, "CD_ANOEPI", "", lAno);
      lAno = null;

      // Ponemos la lista desplegable asociada al tipo de agrupaci�n
      // 23-04-01 (ARS)
      Lista tmp = obtenTipoAgrupacion();
      chListaAgrupacion = new ChoiceCDDS(true, false, true, "CD_TIPORANGO",
                                         "DS_TIPORANGO", tmp);
      tmp = null;

      // etiquetas
      vLabels.addElement(new CColumna("Edad",
                                      "200",
                                      "CD_EDADES"));

      vLabels.addElement(new CColumna("Sexo",
                                      "200",
                                      "DS_SEXO"));

      vLabels.addElement(new CColumna("Poblaci�n",
                                      "200",
                                      "NM_POBLAC"));

      clmRangoEdad = new CListaMantenimiento(a,
                                             vLabels,
                                             vBotones,
                                             this,
                                             this,
                                             250,
                                             640);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Pone el di�logo en modo de espera o normal, seg�n param
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.ESPERA:
        this.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        // ajusta la ventana al modo que tenga
        switch (modoOperacion) {
          case modoMODIFICACION:
            this.setEnabled(true);
            if ( (clmRangoEdad.getLista() == null)) {
              btnCrear.setEnabled(true);
              btnBorrar.setEnabled(false);
              btnModificar.setEnabled(false);
            }
            else {
              btnCrear.setEnabled(false);
              btnBorrar.setEnabled(true);
              btnModificar.setEnabled(true);
            }
            if (rangoEdadExistente) {
              chListaAgrupacion.setEnabled(false);
            }
            else {
              chListaAgrupacion.setEnabled(true);
            }
            break;
        }
        break;
    }
  }

  //Obligatorio rellenar todos los campos
  public void realizaOperacion(int j) {
  }

  private boolean isDataValid() {
    if (chAno.getChoiceCD() == null) {
      return false;
    }
    else {
      return true;
    }
  }

  private String obtenIntervaloCodigo(String codigo) {
    Lista p = new Lista();
    Lista q = new Lista();
    String resultado = "";
    String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    QueryTool qt = new QueryTool();
    qt.putName("SIVE_TIPO_RANGO");
    qt.putType("NM_INTERVALO", QueryTool.INTEGER);
    qt.putWhereType("CD_TIPORANGO", QueryTool.STRING);
    qt.putWhereValue("CD_TIPORANGO", codigo);
    qt.putOperator("CD_TIPORANGO", "=");
    try {
      this.getApp().getStub().setUrl(servlet);
      p.addElement(qt);
      q = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (q.size() > 0) {
        resultado = ( (Data) q.elementAt(0)).getString("NM_INTERVALO");
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    return resultado;
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(670, 495);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno

    lblPuntoCent.setText("Punto Centinela:");
    lblMedico.setText("M�dico:");
    lblEquipo.setText("Equipo Notific:");
    lblCentro.setText("Centro:");
    lblDesEquipo.setText("");
    lblDesCentro.setText("");
    lblAgrupacion.setText("Agrupaci�n:");
    lblDesMedico.setText("");
    lblDesPto.setText("");
    lblAno.setText("A�o:");

    chAno.setBackground(new Color(255, 255, 150));
    chAno.addItemListener(new DiaMantPoblacMed_chAno_itemAdapter(this));
    chAno.addFocusListener(new DiaMantPoblacMed_chAno_focusAdapter(this));
    chAno.writeData();

    chListaAgrupacion.addItemListener(new
        DiaMantPoblacMed_chListaAgrupacion_itemAdapter(this));
    chListaAgrupacion.writeData();

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.addActionListener(new DiaMantPoblacMed_btnAceptar_actionAdapter(this));
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.addActionListener(new
                                  DiaMantPoblacMed_btnCancelar_actionAdapter(this));
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    btnBorrar.setActionCommand("Borrar");
    btnBorrar.setLabel("Borrar");
    btnBorrar.addActionListener(new DiaMantPoblacMed_btnBorrar_actionAdapter(this));
    btnCrear.setActionCommand("Crear");
    btnCrear.setLabel("Crear");
    btnCrear.addActionListener(new DiaMantPoblacMed_btnCrear_actionAdapter(this));
    btnModificar.setActionCommand("Modificar");
    btnModificar.setLabel("Modificar");

    // A�adimos los escuchadores...
    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);
    btnModificar.addActionListener(actionAdapter);
    btnCrear.addActionListener(actionAdapter);
    btnBorrar.addActionListener(actionAdapter);

    xyLayout1.setHeight(450);
    xyLayout1.setWidth(450);

    this.add(lblPuntoCent, new XYConstraints(MARGENIZQ, MARGENSUP, 100, ALTO));
    this.add(lblDesPto,
             new XYConstraints(MARGENIZQ + 100 + INTERHOR, MARGENSUP, 400, ALTO));
    this.add(lblMedico,
             new XYConstraints(MARGENIZQ, MARGENSUP + ALTO + INTERVERT, 100,
                               ALTO));
    this.add(lblDesMedico,
             new XYConstraints(MARGENIZQ + 100 + INTERHOR,
                               MARGENSUP + ALTO + INTERVERT, 400, ALTO));
    this.add(lblCentro,
             new XYConstraints(MARGENIZQ, MARGENSUP + 2 * ALTO + 2 * INTERVERT,
                               100, ALTO));
    this.add(lblDesCentro,
             new XYConstraints(MARGENIZQ + 100 + INTERHOR,
                               MARGENSUP + 2 * ALTO + 2 * INTERVERT, 400, ALTO));
    this.add(lblEquipo,
             new XYConstraints(MARGENIZQ, MARGENSUP + 3 * ALTO + 3 * INTERVERT,
                               100, ALTO));
    this.add(lblDesEquipo,
             new XYConstraints(MARGENIZQ + 100 + INTERHOR,
                               MARGENSUP + 3 * ALTO + 3 * INTERVERT, 400, ALTO));
    this.add(lblAgrupacion,
             new XYConstraints(MARGENIZQ, MARGENSUP + 4 * ALTO + 4 * INTERVERT,
                               110, ALTO));
    this.add(lblAno,
             new XYConstraints(505 - INTERHOR, MARGENSUP + 4 * ALTO + 4 * INTERVERT,
                               30, ALTO));
    this.add(chAno,
             new XYConstraints(540, MARGENSUP + 4 * ALTO + 4 * INTERVERT, 80,
                               ALTO));
    this.add(chListaAgrupacion,
             new XYConstraints(MARGENIZQ + 110,
                               MARGENSUP + 4 * ALTO + 4 * INTERVERT, 130, ALTO));

    this.add(clmRangoEdad,
             new XYConstraints(MARGENIZQ - DESFPANEL,
                               MARGENSUP + 5 * ALTO + 5 * INTERVERT, 650, 220));

    this.add(btnCrear,
             new XYConstraints(547 - 2 * INTERHOR - 2 * 88,
                               MARGENSUP + 5 * ALTO + 5 * INTERVERT + 230 - 5,
                               88, 29));
    this.add(btnModificar,
             new XYConstraints(547 - INTERHOR - 88,
                               MARGENSUP + 5 * ALTO + 5 * INTERVERT + 230 - 5,
                               88, 29));
    this.add(btnBorrar,
             new XYConstraints(547,
                               MARGENSUP + 5 * ALTO + 5 * INTERVERT + 230 - 5,
                               88, 29));
    this.add(btnCancelar,
             new XYConstraints(MARGENIZQ + 88 + INTERHOR,
                               MARGENSUP + 5 * ALTO + 5 * INTERVERT + 230 - 5,
                               88, 29));
    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ,
                               MARGENSUP + 5 * ALTO + 5 * INTERVERT + 230 - 5,
                               88, 29));

    verDatos();

    switch (modoOperacion) {
      case modoALTA:
        setTitle("ALTA");
        break;
      case modoMODIFICACION:
        this.setTitle("MODIFICACION");
        break;
      case modoBAJA:
        this.setTitle("BAJA");
        break;
    }
  }

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    Inicializar(CInicializar.ESPERA);

    QueryTool2 qtInd = new QueryTool2();

    qtInd.putName("SIVE_POBLAC_TS");
    qtInd.putType("CD_EDADF", QueryTool.INTEGER);
    qtInd.putType("CD_EDADI", QueryTool.INTEGER);
    qtInd.putType("CD_SEXO", QueryTool.STRING);
    qtInd.putType("CD_PCENTI", QueryTool.STRING);
    qtInd.putType("CD_MEDCEN", QueryTool.STRING);
    qtInd.putType("NM_POBLAC", QueryTool.INTEGER);
    qtInd.putType("CD_ANOEPI", QueryTool.STRING);

    qtInd.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qtInd.putWhereValue("CD_MEDCEN", dtDev.getCD_MEDCEN());
    qtInd.putOperator("CD_MEDCEN", "=");

    qtInd.putWhereType("CD_PCENTI", QueryTool.STRING);
    qtInd.putWhereValue("CD_PCENTI", dtDev.getCD_PCENTI());
    qtInd.putOperator("CD_PCENTI", "=");

    qtInd.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qtInd.putWhereValue("CD_ANOEPI", chAno.getChoiceCD());
    qtInd.putOperator("CD_ANOEPI", "=");

    QueryTool qtAdic = new QueryTool();
    qtAdic.putName("SIVE_SEXO");
    qtAdic.putType("DS_SEXO", QueryTool.STRING);
    Data dtAdic = new Data();
    dtAdic.put("CD_SEXO", QueryTool.STRING);
    qtInd.addQueryTool(qtAdic);
    qtInd.addColumnsQueryTool(dtAdic);

    qtInd.addOrderField("CD_EDADI");

    try {
      this.getApp().getStub().setUrl(servlet);
      p.addElement(qtInd);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() != 0) {
        p1 = crearCampo(p1);
        tieneDatos = true;
      }
      else {
        tieneDatos = false;
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  public Lista siguientePagina() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;

    Inicializar(CInicializar.ESPERA);

    QueryTool2 qtInd = new QueryTool2();

    qtInd.putName("SIVE_POBLAC_TS");
    qtInd.putType("CD_EDADF", QueryTool.INTEGER);
    qtInd.putType("CD_EDADI", QueryTool.INTEGER);
    qtInd.putType("CD_SEXO", QueryTool.STRING);
    qtInd.putType("CD_PCENTI", QueryTool.STRING);
    qtInd.putType("CD_MEDCEN", QueryTool.STRING);
    qtInd.putType("NM_POBLAC", QueryTool.INTEGER);
    qtInd.putType("CD_ANOEPI", QueryTool.STRING);

    qtInd.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qtInd.putWhereValue("CD_MEDCEN", dtDev.getCD_MEDCEN());
    qtInd.putOperator("CD_MEDCEN", "=");

    qtInd.putWhereType("CD_PCENTI", QueryTool.STRING);
    qtInd.putWhereValue("CD_PCENTI", dtDev.getCD_PCENTI());
    qtInd.putOperator("CD_PCENTI", "=");

    qtInd.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qtInd.putWhereValue("CD_ANOEPI", chAno.getChoiceCD());
    qtInd.putOperator("CD_ANOEPI", "=");

    QueryTool qtAdic = new QueryTool();
    qtAdic.putName("SIVE_SEXO");
    qtAdic.putType("DS_SEXO", QueryTool.STRING);
    Data dtAdic = new Data();
    dtAdic.put("CD_SEXO", QueryTool.STRING);
    qtInd.addQueryTool(qtAdic);
    qtInd.addColumnsQueryTool(dtAdic);

    qtInd.addOrderField("CD_EDADI");

    try {
      this.getApp().getStub().setUrl(servlet);
      p.addElement(qtInd);
      p.setTrama(this.clmRangoEdad.getLista().getTrama());
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
      if (p1.size() != 0) {
        p1 = crearCampo(p1);
        btnCrear.setEnabled(false);
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  private Lista obtenTipoAgrupacion() {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista parametros = new Lista();
    Lista devuelto = new Lista();
    QueryTool qtRango = new QueryTool();
    try {
      qtRango.putName("SIVE_TIPO_RANGO");
      qtRango.putType("DS_TIPORANGO", QueryTool.STRING);
      qtRango.putType("CD_TIPORANGO", QueryTool.STRING);
      qtRango.addOrderField("CD_TIPORANGO desc");
      parametros.addElement(qtRango);
      devuelto = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, parametros);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    return devuelto;
  }

  private Lista obtenerAno() {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool qtAno = new QueryTool();
    try {
      qtAno.putName("SIVE_ANO_EPI_RMC");
      qtAno.putType("CD_ANOEPI", QueryTool.STRING);
      //Solo se consideran a�os hasta el a�o de trabajo de aplicaci�n
      if (! (app.getParametro("CD_ANO").equals(""))) {
        qtAno.putWhereType("CD_ANOEPI", QueryTool.STRING);
        qtAno.putWhereValue("CD_ANOEPI", app.getParametro("CD_ANO"));
        qtAno.putOperator("CD_ANOEPI", "<=");
      }
      qtAno.addOrderField("CD_ANOEPI desc");
      p.addElement(qtAno);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);

    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    return p1;
  }

  public void rellenarDatos() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Inicializar(CInicializar.ESPERA);
    clmRangoEdad.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
  }

  private boolean existePoblacAgrupAnio(String anyo) {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista p = new Lista();
    Lista s = new Lista();
    boolean resultado = false;
    QueryTool qt = new QueryTool();
    try {
      qt.putName("SIVE_POBLAC_TS");
      qt.putType("CD_PCENTI", QueryTool.STRING);
      qt.putType("CD_MEDCEN", QueryTool.STRING);

      qt.putWhereType("CD_ANOEPI", QueryTool.STRING);
      qt.putWhereValue("CD_ANOEPI", anyo);
      qt.putOperator("CD_ANOEPI", "=");

      p.addElement(qt);
      s = (Lista) BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    if (s.size() != 0) {
      resultado = true;
    }
    return resultado;

  }

  private Data obtenerAgrup() {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Data d = new Data();
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool2 qtAgrup = new QueryTool2();
    try {
      qtAgrup.putName("SIVE_RANGO_ANO");
      qtAgrup.putType("CD_TIPORANGO", QueryTool.STRING);

      qtAgrup.putWhereType("CD_ANOEPI", QueryTool.STRING);
      qtAgrup.putWhereValue("CD_ANOEPI", chAno.getChoiceCD());
      qtAgrup.putOperator("CD_ANOEPI", "=");

      QueryTool qtAdic1 = new QueryTool();
      qtAdic1.putName("SIVE_TIPO_RANGO");
      qtAdic1.putType("DS_TIPORANGO", QueryTool.STRING);
      qtAdic1.putType("NM_INTERVALO", QueryTool.INTEGER);
      Data dtAdic1 = new Data();
      dtAdic1.put("CD_TIPORANGO", QueryTool.STRING);
      qtAgrup.addQueryTool(qtAdic1);
      qtAgrup.addColumnsQueryTool(dtAdic1);

      p.addElement(qtAgrup);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    if (p1.size() != 0) {
      d = (Data) p1.elementAt(0);
    }
    return d;
  }

  private Data obtenerCentro() {
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Data d = new Data();
    Lista p = new Lista();
    Lista p1 = new Lista();
    QueryTool2 qtCen = new QueryTool2();
    try {
      qtCen.putName("SIVE_E_NOTIF");

      qtCen.putType("CD_CENTRO", QueryTool.STRING);

      qtCen.putWhereType("CD_E_NOTIF", QueryTool.STRING);
      qtCen.putWhereValue("CD_E_NOTIF", dtDev.getCD_E_NOTIF());
      qtCen.putOperator("CD_E_NOTIF", "=");

      //Descripci�n del centro
      QueryTool qtAdic1 = new QueryTool();
      qtAdic1.putName("SIVE_C_NOTIF");
      qtAdic1.putType("DS_CENTRO", QueryTool.STRING);
      Data dtAdic1 = new Data();
      dtAdic1.put("CD_CENTRO", QueryTool.STRING);
      qtCen.addQueryTool(qtAdic1);
      qtCen.addColumnsQueryTool(dtAdic1);

      p.addElement(qtCen);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    d = (Data) p1.elementAt(0);
    return d;
  }

  //crea el campo edad:edad_i-edad_f
  Lista crearCampo(Lista lCampo) {
    for (int i = 0; i < lCampo.size(); i++) {
      Data dCampo = new Data();
      dCampo = (Data) lCampo.elementAt(i);
      String s = new String();
      s = dCampo.getString("CD_EDADI") + " - " + dCampo.getString("CD_EDADF");
      ( (Data) lCampo.elementAt(i)).put("CD_EDADES", s);
    }
    return lCampo;
  }

  //Controla si ese m�dico tiene ya una estructura de rangos creada
  void buscarRango() {
    Inicializar(CInicializar.ESPERA);
    clmRangoEdad.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);

  }

  QueryTool realizarUpdate(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_POBLAC_TS");
    qtModif.putType("NM_POBLAC", QueryTool.INTEGER);
    qtModif.putValue("NM_POBLAC", dtUpd.getString("NM_POBLAC"));

    qtModif.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qtModif.putWhereValue("CD_MEDCEN", dtDev.getCD_MEDCEN());
    qtModif.putOperator("CD_MEDCEN", "=");

    qtModif.putWhereType("CD_PCENTI", QueryTool.STRING);
    qtModif.putWhereValue("CD_PCENTI", dtDev.getCD_PCENTI());
    qtModif.putOperator("CD_PCENTI", "=");

    qtModif.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qtModif.putWhereValue("CD_ANOEPI", chAno.getChoiceCD());
    qtModif.putOperator("CD_ANOEPI", "=");

    qtModif.putWhereType("CD_EDADF", QueryTool.INTEGER);
    qtModif.putWhereValue("CD_EDADF", dtUpd.getString("CD_EDADF"));
    qtModif.putOperator("CD_EDADF", "=");

    qtModif.putWhereType("CD_EDADI", QueryTool.INTEGER);
    qtModif.putWhereValue("CD_EDADI", dtUpd.getString("CD_EDADI"));
    qtModif.putOperator("CD_EDADI", "=");

    qtModif.putWhereType("CD_SEXO", QueryTool.STRING);
    qtModif.putWhereValue("CD_SEXO", dtUpd.getString("CD_SEXO"));
    qtModif.putOperator("CD_SEXO", "=");
    return qtModif;
  }

  QueryTool realizarInsert(Data dtUpd) {
    QueryTool qtIns = new QueryTool();
    qtIns.putName("SIVE_POBLAC_TS");
    qtIns.putType("CD_EDADF", QueryTool.INTEGER);
    qtIns.putValue("CD_EDADF", dtUpd.getString("CD_EDADF"));
    qtIns.putType("CD_EDADI", QueryTool.INTEGER);
    qtIns.putValue("CD_EDADI", dtUpd.getString("CD_EDADI"));
    qtIns.putType("CD_SEXO", QueryTool.STRING);
    qtIns.putValue("CD_SEXO", dtUpd.getString("CD_SEXO"));
    qtIns.putType("CD_PCENTI", QueryTool.STRING);
    qtIns.putValue("CD_PCENTI", dtUpd.getString("CD_PCENTI"));
    qtIns.putType("CD_MEDCEN", QueryTool.STRING);
    qtIns.putValue("CD_MEDCEN", dtUpd.getString("CD_MEDCEN"));
    qtIns.putType("NM_POBLAC", QueryTool.INTEGER);
    qtIns.putValue("NM_POBLAC", dtUpd.getString("NM_POBLAC"));
    qtIns.putType("CD_ANOEPI", QueryTool.STRING);
    qtIns.putValue("CD_ANOEPI", dtUpd.getString("CD_ANOEPI"));

    return qtIns;
  }

  QueryTool realizarDelete(Data dtUpd) {
    QueryTool qtModif = new QueryTool();
    qtModif.putName("SIVE_POBLAC_TS");

    qtModif.putWhereType("CD_MEDCEN", QueryTool.STRING);
    qtModif.putWhereValue("CD_MEDCEN", dtDev.getCD_MEDCEN());
    qtModif.putOperator("CD_MEDCEN", "=");

    qtModif.putWhereType("CD_PCENTI", QueryTool.STRING);
    qtModif.putWhereValue("CD_PCENTI", dtDev.getCD_PCENTI());
    qtModif.putOperator("CD_PCENTI", "=");

    qtModif.putWhereType("CD_ANOEPI", QueryTool.STRING);
    qtModif.putWhereValue("CD_ANOEPI", chAno.getChoiceCD());
    qtModif.putOperator("CD_ANOEPI", "=");

    qtModif.putWhereType("CD_EDADF", QueryTool.INTEGER);
    qtModif.putWhereValue("CD_EDADF", dtUpd.getString("CD_EDADF"));
    qtModif.putOperator("CD_EDADF", "=");

    qtModif.putWhereType("CD_EDADI", QueryTool.INTEGER);
    qtModif.putWhereValue("CD_EDADI", dtUpd.getString("CD_EDADI"));
    qtModif.putOperator("CD_EDADI", "=");
    return qtModif;
  }

  void btn_actionPerformed(ActionEvent e) {
    //Modifico el valor de poblaci�n en la tabla
    if (e.getActionCommand().equals("Modificar")) {
      int ind = clmRangoEdad.getSelectedIndex();
      Data dtPoblac = clmRangoEdad.getSelected();
      Data dtAnt = new Data();
      dtAnt.put("NM_POBLAC", dtPoblac.getString("NM_POBLAC"));
      DiaIntrodPoblac di = new DiaIntrodPoblac(this.getApp(), dtAnt);
      di.show();
      String poblac = di.bdevPoblac();
      if (di.bAcept()) {
        if (tieneDatos == true) {
          dtPoblac.put("IT_MODIF", "S");
        }
        dtPoblac.put("NM_POBLAC", poblac);
        lClm.setElementAt(dtPoblac, ind);
// ARS 16-07-01 Corregido, para que no necesite refrescar toda la lista.
//        clmRangoEdad.vaciarPantalla();
//        Inicializar(CInicializar.ESPERA);
        clmRangoEdad.getLista().setElementAt(dtPoblac, ind);
        clmRangoEdad.setPrimeraPagina(clmRangoEdad.getLista());
//        clmRangoEdad.setPrimeraPagina(lClm);
//        Inicializar(CInicializar.NORMAL);
// Fin correcci�n ARS.
        chAno.setEnabled(false);
      }
    }
  } //fin de btn_actionPerformed

  public boolean bAceptar() {
    return bAceptar;
  }

  public int borrPto() {
    return borrPto;
  }

  public void verDatos() {
    Data dtCent = new Data();
    dtCent = obtenerCentro();
    lblDesMedico.setText(dtDev.getString("CD_MEDCEN") + "   D. " +
                         dtDev.getDS_NOMBRE() + " " + dtDev.getDS_APE1() + " " +
                         dtDev.getDS_APE2());
    lblDesPto.setText(dtDev.getString("CD_PCENTI") + "   " +
                      dtDev.getString("DS_PCENTI"));
    lblDesCentro.setText(dtCent.getString("CD_CENTRO") + "   " +
                         dtCent.getString("DS_CENTRO"));
    lblDesEquipo.setText(dtDev.getString("CD_E_NOTIF") + "   " +
                         dtDev.getString("DS_E_NOTIF"));
    btnCrear.setEnabled(false);
    btnModificar.setEnabled(false);
    btnBorrar.setEnabled(false);

  }

  //busco c�digo y descripci�n de sexo
  Lista obtenerSex() {
    Lista p = new Lista();
    Lista p1 = new Lista();
    final String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Inicializar(CInicializar.ESPERA);

    QueryTool qtInd = new QueryTool();

    qtInd.putName("SIVE_SEXO");
    qtInd.putType("CD_SEXO", QueryTool.INTEGER);
    qtInd.putType("DS_SEXO", QueryTool.STRING);

    qtInd.addOrderField("CD_SEXO");

    try {
      this.getApp().getStub().setUrl(servlet);
      p.addElement(qtInd);
      p1 = BDatos.ejecutaSQL(false, this.getApp(), servlet, 1, p);
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
    }
    Inicializar(CInicializar.NORMAL);
    return p1;
  }

  //Crea los rangos de edad y los visualiza en la tabla
  void crearRangos() {
    int edMin = Integer.parseInt(dtDev.getString("NMREDADI"));
    int edMax = Integer.parseInt(dtDev.getString("NMREDADF"));
    int maxim = Integer.parseInt(devAgrup);
    int intervalo = Integer.parseInt(interv);
    int i = 0;
    Lista lRango = new Lista();
    Lista lSexo = obtenerSex();

    //for para crear los datas
    for (i = edMin; i + intervalo < maxim; i = i + intervalo) {
      for (int sex = 0; sex < 2; sex++) {
        Data dtRango = new Data();
        int n = i + intervalo;
        String edadf = "" + n;
        dtRango.put("CD_EDADF", edadf);
        String edadi = "" + i;
        dtRango.put("CD_EDADI", edadi);
        dtRango.put("CD_SEXO",
                    ( (Data) lSexo.elementAt(sex)).getString("CD_SEXO"));
        dtRango.put("DS_SEXO",
                    ( (Data) lSexo.elementAt(sex)).getString("DS_SEXO"));
        dtRango.put("CD_PCENTI", dtDev.getString("CD_PCENTI"));
        dtRango.put("CD_MEDCEN", dtDev.getString("CD_MEDCEN"));
        dtRango.put("NM_POBLAC", "0");
        dtRango.put("CD_ANOEPI", chAno.getChoiceCD());
        lRango.addElement(dtRango);
      }
    }
    //introduzco el �ltimo rango
    if (i + intervalo == maxim) {
      for (int sex = 0; sex < 2; sex++) {
        Data dtRango = new Data();
        String edadf = "" + maxim;
        dtRango.put("CD_EDADF", edadf);
        int s = maxim - intervalo;
        String edadi = "" + s;
        dtRango.put("CD_EDADI", edadi);

        dtRango.put("CD_SEXO",
                    ( (Data) lSexo.elementAt(sex)).getString("CD_SEXO"));
        dtRango.put("DS_SEXO",
                    ( (Data) lSexo.elementAt(sex)).getString("DS_SEXO"));
        dtRango.put("CD_PCENTI", dtDev.getString("CD_PCENTI"));
        dtRango.put("CD_MEDCEN", dtDev.getString("CD_MEDCEN"));
        dtRango.put("NM_POBLAC", "0");
        dtRango.put("CD_ANOEPI", chAno.getChoiceCD());
        lRango.addElement(dtRango);
      }
    }
    if (i + intervalo > maxim) {
      int j = i - maxim;
      for (int sex = 0; sex < 2; sex++) {
        Data dtRango = new Data();
        String edadf = "" + maxim;
        dtRango.put("CD_EDADF", edadf);
        String edadi = "" + i;
        dtRango.put("CD_EDADI", edadi);

        dtRango.put("CD_SEXO",
                    ( (Data) lSexo.elementAt(sex)).getString("CD_SEXO"));
        dtRango.put("DS_SEXO",
                    ( (Data) lSexo.elementAt(sex)).getString("DS_SEXO"));
        dtRango.put("CD_PCENTI", dtDev.getString("CD_PCENTI"));
        dtRango.put("CD_MEDCEN", dtDev.getString("CD_MEDCEN"));
        dtRango.put("NM_POBLAC", "0");
        dtRango.put("CD_ANOEPI", chAno.getChoiceCD());
        lRango.addElement(dtRango);
      }
    }
    for (int sex = 0; sex < 2; sex++) {
      Data dtRango = new Data();
      dtRango.put("CD_EDADF", "999");
      String edadi = "" + maxim;
      dtRango.put("CD_EDADI", edadi);
      dtRango.put("CD_SEXO", ( (Data) lSexo.elementAt(sex)).getString("CD_SEXO"));
      dtRango.put("DS_SEXO", ( (Data) lSexo.elementAt(sex)).getString("DS_SEXO"));
      dtRango.put("CD_PCENTI", dtDev.getString("CD_PCENTI"));
      dtRango.put("CD_MEDCEN", dtDev.getString("CD_MEDCEN"));
      dtRango.put("NM_POBLAC", "0");
      dtRango.put("CD_ANOEPI", chAno.getChoiceCD());
      lRango.addElement(dtRango);
    }

    //Introduzco cd_edades
    lRango = crearCampo(lRango);
    btnCrear.setEnabled(false);
    //Escribo los rangos en la lista
    Inicializar(CInicializar.ESPERA);
    clmRangoEdad.setPrimeraPagina(lRango);
    Inicializar(CInicializar.NORMAL);

  }

  void chAno_focusLost(FocusEvent e) {
  }

  void chListaAgrupacion_itemStateChanged(ItemEvent e) {
    interv = obtenIntervaloCodigo(chListaAgrupacion.getChoiceCD());
  }

  void chAno_itemStateChanged(ItemEvent e) {

    boolean seguir = false;
    // iniciamos rangoEdadExistente y limpiamos chListaAgrupacion
    // si antes se han encontrado datos.
    if (rangoEdadExistente) {
      rangoEdadExistente = false;
      chListaAgrupacion.select(0);
      clmRangoEdad.vaciarPantalla();
    }

    if ( (dtDev.getString("NMREDADF").equals("")) ||
        (dtDev.getString("NMREDADF").equals("0"))
        || (dtDev.getString("NMREDADI").equals(""))) {
      this.getApp().showAdvise(
          "Este m�dico no tiene definido un rango de edades");
      bAceptar = false;
      dispose();
    }
    else {
      Data dtAgrup = new Data();
      dtAgrup = obtenerAgrup();
      if (dtAgrup.size() == 0) {
        this.getApp().showAdvise("No existe agrupaci�n creada para ese a�o");
      }
      else {
//       lblAgrupacion.setText("Agrupaci�n:    "+ dtAgrup.getString("CD_TIPORANGO")+" - "+ dtAgrup.getString("DS_TIPORANGO")+ "  Intervalo: "+dtAgrup.getString("NM_INTERVALO"));
        if (existePoblacAgrupAnio(chAno.getChoiceCD())) {
          chListaAgrupacion.select(dtAgrup.getString("DS_TIPORANGO"));
          interv = dtAgrup.getString("NM_INTERVALO");
          seguir = true; // Esto porque ha encontrado datos, y puede seguir el proceso
          rangoEdadExistente = true;
        }
        else {
          this.getApp().showAdvise("No existe agrupaci�n para ese a�o");
        }

        modificarRangoAno = true; // Esto, porque en ning�n caso debe insertar.
      }
      // En este caso no hay datos, as� que habr� que crear la
      // agrupaci�n.
      if ( (seguir == false) && (chListaAgrupacion.getChoiceCD().equals(""))) {
        chAno.select(0);
        this.getApp().showAdvise("Seleccione una agrupaci�n");
      }
      else {
        seguir = true; // Del if hay algo en chListaAgrupacion
        // Esta parte es com�n, tanto si existe rango como si no.
        // en el caso de que no exista, se crea el rango

      }
      if (seguir) {
        buscarRango();
        if (tieneDatos == false) {
          Data dtEdad = new Data();
          dtEdad.put("NMREDADF", dtDev.getString("NMREDADF"));
          dtEdad.put("NMREDADI", dtDev.getString("NMREDADI"));
          DiaAgrupEdad di = new DiaAgrupEdad(this.getApp(), dtEdad);
          di.show();
          if (di.bAceptar()) {
            devAgrup = di.bDevAgrup();
            crearRangos();
            // Deshabilitamos el tipo de agrupaci�n, para
            // que ya no se pueda cambiar, pues se han creado
            // los rangos. 24-04-01 (ARS)
            rangoEdadExistente = true;
            // ---
            creadoNuevoRango = true;
            btnCrear.setEnabled(false);
            btnBorrar.setEnabled(true);
            btnModificar.setEnabled(true);
            chAno.setEnabled(false);
          }
          else {
            btnCrear.setEnabled(true);
            btnBorrar.setEnabled(false);
            btnModificar.setEnabled(false);
          }
        }
        lClm = clmRangoEdad.getLista();

        if (tieneDatos == true) {
          for (int i = 0; i < lClm.size(); i++) {
            Data dtLista = new Data();
            dtLista = (Data) lClm.elementAt(i);
            dtLista.put("IT_MODIF", "N");
          }
          btnCrear.setEnabled(false);
          btnBorrar.setEnabled(true);
          btnModificar.setEnabled(true);
        }
      }
    }
    // Repintamos, para dejar las cosas bien.
    Inicializar(CInicializar.NORMAL);
  }

  void btnAceptar_actionPerformed(ActionEvent e) {
    // Lo primero de todo es insertar el rango de a�o, si no existe.
    String servlet = nombreservlets.strSERVLET_QUERY_TOOL;
    Lista ent = new Lista();
    Lista s = new Lista();
    QueryTool qt = new QueryTool();

    if (!modificarRangoAno) {
      try {
        qt.putName("SIVE_RANGO_ANO");
        qt.putType("CD_ANOEPI", QueryTool.STRING);
        qt.putValue("CD_ANOEPI", chAno.getSelectedItem());
        qt.putType("CD_TIPORANGO", QueryTool.STRING);
        qt.putValue("CD_TIPORANGO", chListaAgrupacion.getChoiceCD());
        ent.addElement(qt);
        s = BDatos.ejecutaSQL(false, this.getApp(), servlet, 3, ent);
        rangoEdadExistente = true;
      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    }

    // Si ya existiera, lo actualizamos, por si acaso ha cambiado.
    if (modificarRangoAno) {
      try {
        qt.putName("SIVE_RANGO_ANO");
        qt.putType("CD_TIPORANGO", QueryTool.STRING);
        qt.putValue("CD_TIPORANGO", chListaAgrupacion.getChoiceCD());
        qt.putWhereType("CD_ANOEPI", QueryTool.STRING);
        qt.putWhereValue("CD_ANOEPI", chAno.getSelectedItem());
        qt.putOperator("CD_ANOEPI", "=");
        ent.addElement(qt);
        s = BDatos.ejecutaSQL(false, this.getApp(), servlet, 4, ent);

      }
      catch (Exception ex) {
        this.getApp().trazaLog(ex);
      }
    } // del if
    qt = null;
    ent = null;
    s = null;
    servlet = null;
    modificarRangoAno = false;

    Lista lBd = new Lista();
    //Si se ha eliminado la lista la borra de la bd
    Inicializar(CInicializar.ESPERA);
    if (borrLBorrar) {
      Lista lDelet = new Lista();
      for (int i = 0; i < lBorrar.size(); i++) {
        Data dtDel = (Data) lBorrar.elementAt(i);
        QueryTool qtDel = new QueryTool();
        qtDel = realizarDelete(dtDel);
        Data dtDelete = new Data();
        dtDelete.put("5", qtDel);
        lBd.addElement(dtDelete);
      }
      tieneDatos = false;
    } //end if borrLBorrar

    //si son datos de la bd:UPDATE
    if (tieneDatos) {
      for (int i = 0; i < lClm.size(); i++) {
        Data dtUpd = (Data) lClm.elementAt(i);
        if (dtUpd.getString("IT_MODIF").equals("S")) {
          QueryTool qtUpd = new QueryTool();
          qtUpd = realizarUpdate(dtUpd);
          Data dtUpdate = new Data();
          dtUpdate.put("4", qtUpd);
          lBd.addElement(dtUpdate);
        }
      }
    }
    else { //si son rangos creados nuevos:INSERT
      //si ha sido creado despu�s de borrar
      if (creadoNuevoRango) {
        for (int i = 0; i < lClm.size(); i++) {
          Data dtIns = (Data) lClm.elementAt(i);
          QueryTool qtIns = new QueryTool();
          qtIns = realizarInsert(dtIns);
          Data dtInsert = new Data();
          dtInsert.put("3", qtIns);
          lBd.addElement(dtInsert);
        }
      } //end creadoNuevoRango
    }
    try {
      BDatos.ejecutaSQL(false, this.getApp(), srvTrans, 0, lBd);
      bAceptar = true;
      dispose();
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
      this.getApp().showError(ex.getMessage());
      bAceptar = false;
      dispose();
    }
    Inicializar(CInicializar.NORMAL);

  }

  void btnCancelar_actionPerformed(ActionEvent e) {
    bAceptar = false;
    dispose();
  }

  //borra la lista (incluida de la bd si viene de ah�)
  void btnBorrar_actionPerformed(ActionEvent e) {
    if (tieneDatos) {
      lBorrar = lClm;
      borrLBorrar = true;
      tieneDatos = false;
    } //end if tienedatos
    chAno.setEnabled(false);
    clmRangoEdad.vaciarPantalla();
    creadoNuevoRango = false;
    btnCrear.setEnabled(true);
    btnBorrar.setEnabled(false);
    btnModificar.setEnabled(false);
  }

  void btnCrear_actionPerformed(ActionEvent e) {
    if (tieneDatos == false) {
      Data dtEdad = new Data();
      dtEdad.put("NMREDADF", dtDev.getString("NMREDADF"));
      dtEdad.put("NMREDADI", dtDev.getString("NMREDADI"));
      DiaAgrupEdad di = new DiaAgrupEdad(this.getApp(), dtEdad);
      di.show();
      if (di.bAceptar()) {
        devAgrup = di.bDevAgrup();
        crearRangos();
        lClm = clmRangoEdad.getLista();
        creadoNuevoRango = true;
        btnCrear.setEnabled(false);
        btnBorrar.setEnabled(true);
        btnModificar.setEnabled(true);
        chAno.setEnabled(false);
      }
      else {
        btnCrear.setEnabled(true);
        btnBorrar.setEnabled(false);
        btnModificar.setEnabled(false);
      } //end bAceptar
    } //end tieneDatos==false
  } //end btnCrear
}

class DialogActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DiaMantPoblacMed adaptee;
  ActionEvent e;

  DialogActionAdapter(DiaMantPoblacMed adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}

class DiaMantPoblacMed_chAno_focusAdapter
    extends java.awt.event.FocusAdapter {
  DiaMantPoblacMed adaptee;

  DiaMantPoblacMed_chAno_focusAdapter(DiaMantPoblacMed adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.chAno_focusLost(e);
  }
}

class DiaMantPoblacMed_chAno_itemAdapter
    implements java.awt.event.ItemListener {
  DiaMantPoblacMed adaptee;

  DiaMantPoblacMed_chAno_itemAdapter(DiaMantPoblacMed adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chAno_itemStateChanged(e);
  }
}

class DiaMantPoblacMed_chListaAgrupacion_itemAdapter
    implements java.awt.event.ItemListener {
  DiaMantPoblacMed adaptee;

  DiaMantPoblacMed_chListaAgrupacion_itemAdapter(DiaMantPoblacMed adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chListaAgrupacion_itemStateChanged(e);
  }
}

class DiaMantPoblacMed_btnAceptar_actionAdapter
    implements java.awt.event.ActionListener {
  DiaMantPoblacMed adaptee;

  DiaMantPoblacMed_btnAceptar_actionAdapter(DiaMantPoblacMed adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnAceptar_actionPerformed(e);
  }
}

class DiaMantPoblacMed_btnCancelar_actionAdapter
    implements java.awt.event.ActionListener {
  DiaMantPoblacMed adaptee;

  DiaMantPoblacMed_btnCancelar_actionAdapter(DiaMantPoblacMed adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnCancelar_actionPerformed(e);
  }
}

class DiaMantPoblacMed_btnBorrar_actionAdapter
    implements java.awt.event.ActionListener {
  DiaMantPoblacMed adaptee;

  DiaMantPoblacMed_btnBorrar_actionAdapter(DiaMantPoblacMed adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnBorrar_actionPerformed(e);
  }
}

class DiaMantPoblacMed_btnCrear_actionAdapter
    implements java.awt.event.ActionListener {
  DiaMantPoblacMed adaptee;

  DiaMantPoblacMed_btnCrear_actionAdapter(DiaMantPoblacMed adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnCrear_actionPerformed(e);
  }
}
