package centinelas.cliente.c_mantpoblacmed;

import java.awt.Color;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;
import capp2.CEntero;
import sapp2.Data;

public class DiaAgrupEdad
    extends CDialog {

  //constantes para localizaciones
  final int MARGENIZQ = 15;
  final int MARGENSUP = 15;
  final int INTERVERT = 10;
  final int INTERHOR = 10;
  final int ALTO = 25;

  public boolean bAceptar = false;
  public String devAgrup = null;

  Data dtDev = new Data();

  XYLayout xyLayout1 = new XYLayout();

  Label lblAgrup = new Label();
  CEntero txtAgrup = null;

  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();

  // Escuchadores de eventos...
  DialogActionAdapter2 actionAdapter = new DialogActionAdapter2(this);

  public DiaAgrupEdad(CApp a, Data dtEdad) {
    super(a);
    dtDev = dtEdad;
    txtAgrup = new CEntero(3);
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private boolean isDataValid() {
    if (txtAgrup.getText().trim().equals("")) {
      this.getApp().showAdvise("Debe rellenar el campo visualizado");
      return false;
    }
    else {
      int edMax = Integer.parseInt(dtDev.getString("NMREDADF"));
      int edMin = Integer.parseInt(dtDev.getString("NMREDADI"));
      int agrup = Integer.parseInt(txtAgrup.getText().trim());
      if (agrup > edMax) {
        this.getApp().showAdvise("Edad introducida superior a la permitida");
        return false;
      }
      else {
        if (agrup < edMin) {
          this.getApp().showAdvise("Edad introducida inferior a la permitida");
          return false;
        }
        else {
          return true;
        }
      }
    }
  }

  void jbInit() throws Exception {
    final String imgCancelar = "images/cancelar.gif";
    final String imgAceptar = "images/aceptar.gif";

    this.setLayout(xyLayout1);
    this.setSize(300, 120);

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgAceptar);
    this.getApp().getLibImagenes().put(imgCancelar);
    this.getApp().getLibImagenes().CargaImagenes();

    // marcar como campos obligatorios de relleno
    lblAgrup.setText("Agrupaci�n de edad hasta:");

    txtAgrup.setBackground(new Color(255, 255, 150));
    txtAgrup.setText(dtDev.getString("NMREDADF"));

    btnAceptar.setActionCommand("Aceptar");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgAceptar));
    btnCancelar.setActionCommand("Cancelar");
    btnCancelar.setLabel("Cancelar");
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCancelar));

    // A�adimos los escuchadores...

    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    xyLayout1.setHeight(450);
    xyLayout1.setWidth(450);

    this.add(lblAgrup, new XYConstraints(MARGENIZQ, MARGENSUP, 180, ALTO));
    this.add(txtAgrup,
             new XYConstraints(MARGENIZQ + INTERHOR + 180, MARGENSUP, 60, ALTO));

    this.add(btnAceptar,
             new XYConstraints(MARGENIZQ + 180 + 60 - 2 * 88,
                               MARGENSUP + ALTO + INTERVERT, 88, 29));
    this.add(btnCancelar,
             new XYConstraints(MARGENIZQ + INTERHOR + 180 + 60 - 88,
                               MARGENSUP + ALTO + INTERVERT, 88, 29));

    setTitle("Alta agrupaci�n");
  }

  void btn_actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("Aceptar")) {
      if (isDataValid()) {
        devAgrup = txtAgrup.getText().trim();
        bAceptar = true;
        dispose();
      } //end if (isDataValid)
    }
    else if (e.getActionCommand().equals("Cancelar")) {
      bAceptar = false;
      dispose();
    }
  } //fin de btn_actionPerformed

  public boolean bAceptar() {
    return bAceptar;
  }

  public String bDevAgrup() {
    return devAgrup;
  }
}

class DialogActionAdapter2
    implements java.awt.event.ActionListener, Runnable {
  DiaAgrupEdad adaptee;
  ActionEvent e;

  DialogActionAdapter2(DiaAgrupEdad adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.btn_actionPerformed(e);
  }
}
