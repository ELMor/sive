package centinelas.cliente.c_mantcartasavisos;

import java.util.Vector;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CBoton;
import capp2.CColumna;
import capp2.CEntero;
import capp2.CFiltro;
import capp2.CInicializar;
import capp2.CListaMantenimiento;
import capp2.CPanel;
import capp2.CPnlCodigo;
import centinelas.cliente.c_componentes.ContPanAnoSemFecha;
import centinelas.cliente.c_componentes.PanAnoSemFecha;
import comun.constantes;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.QueryTool2;

// Quitar cuando no se use.
//import centinelas.servidor.c_mantcartasavisos.*;

/**
 * Panel mediante el cual se realiza el mantenimiento de las cartas de avisos.
 *
 * @autor ARS.
 * @version 1.0
 */

public class PanMantCartasAvisos
    extends CPanel
    implements CInicializar, CFiltro, ContPanAnoSemFecha {
  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  final public int MODIFICACION = 0;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  XYLayout xyLayout = new XYLayout();
  ButtonControl btnBuscar = new ButtonControl();
  CListaMantenimiento clmMantCartas = null;
  PanAnoSemFecha panDesde = null;
  PanAnoSemFecha panHasta = null;
  CPnlCodigo pnlEnfermedad = null;
  Label lblEnfermedad = new Label();
  Label lblSemanasCons = new Label();
  CEntero txtSemanasConsecutivas = new CEntero(3);
  Label lblSemanasAgregadas = new Label();
  CEntero txtSemanasAgregadas = new CEntero(3);
  Label lblSemanasPeriodo = new Label();
  Label lblSemPer = new Label();
  //_________________________________________________________

  public PanMantCartasAvisos(CApp a) {
    Vector vBotones = new Vector();
    Vector vLabels = new Vector();
    QueryTool qtEnfermedad = new QueryTool2();

    try {
      setApp(a);

//      panDesde = new PanAnoSemFecha(this, PanAnoSemFecha.modoSIN_SEMANA, false);
//      panHasta = new PanAnoSemFecha(this, PanAnoSemFecha.modoSIN_SEMANA, false);
      // Correcci�n problema tama�o 21-05-01 (ARS)
      panDesde = new PanAnoSemFecha(this, false,
                                    this.getApp().getParametro("CD_ANO"), "", false);
      panHasta = new PanAnoSemFecha(this, false,
                                    this.getApp().getParametro("CD_ANO"), "", false);
      String subconsulta = "select CD_ENFCIE from SIVE_ENF_CENTI ";

// Correcci�n incidencia 02-02-2001;
//      qtEnfermedad.putName("SIVE_ENFER_CIE");
      qtEnfermedad.putName("SIVE_PROCESOS");
      qtEnfermedad.putType("CD_ENFCIE", QueryTool.STRING);
//      qtEnfermedad.putType("DS_ENFERCIE",QueryTool.STRING);
      qtEnfermedad.putType("DS_PROCESO", QueryTool.STRING);
      qtEnfermedad.putSubquery("CD_ENFCIE", subconsulta);

      pnlEnfermedad = new CPnlCodigo(a,
                                     this,
                                     qtEnfermedad,
                                     "CD_ENFCIE",
                                     "DS_PROCESO",
                                     true,
                                     "Enfermedad",
                                     "Enfermedad");

      // configura la lista de inspecciones
      // botones

      vBotones.addElement(new CBoton(constantes.keyBtnModificar,
                                     "images/modificacion2.gif",
                                     "Modificar punto centinela",
                                     true,
                                     true));

      // etiquetas
      vLabels.addElement(new CColumna("Pto. Centi.",
                                      "70",
                                      "CD_PCENTI"));

      vLabels.addElement(new CColumna("M�dico",
                                      "330",
                                      "DS_MEDICO"));

      vLabels.addElement(new CColumna("Sem. Consec.",
                                      "90",
                                      "SEM_CONSEC"));

      vLabels.addElement(new CColumna("Sem. Agreg.",
                                      "80",
                                      "SEM_AGREG"));

      clmMantCartas = new CListaMantenimiento(a,
                                              vLabels,
                                              vBotones,
                                              this,
                                              this,
                                              270,
                                              610);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  //_________________________________________________________

  void jbInit() throws Exception {
    final String imgBUSCAR = "images/refrescar.gif";

    // carga la imagen
    this.getApp().getLibImagenes().put(imgBUSCAR);
    this.getApp().getLibImagenes().CargaImagenes();

    xyLayout.setHeight(446);
    xyLayout.setWidth(617);
    this.setLayout(xyLayout);

    btnBuscar.setImage(this.getApp().getLibImagenes().get(imgBUSCAR));
    btnBuscar.setLabel("Buscar");
    lblEnfermedad.setText("Enfermedad:");
    lblSemanasCons.setText("Semanas consecutivas:");
    lblSemanasAgregadas.setText("Semanas agregadas:");
    btnBuscar.addActionListener(new PanMantCartasAvisos_btnBuscar_actionAdapter(this));
    txtSemanasAgregadas.setBackground(new Color(250, 250, 150));
    lblSemanasPeriodo.setText("Total semanas del per�odo:");
    txtSemanasConsecutivas.setBackground(new Color(250, 250, 150));
    // a�ade los componentes
    this.add(lblSemanasPeriodo, new XYConstraints(9, 179, 158, 26));
    this.add(lblSemPer, new XYConstraints(182, 179, 67, 26));
    this.add(lblEnfermedad, new XYConstraints(12, 135, 81, 40));
    this.add(lblSemanasCons, new XYConstraints(12, 107, 137, 25));
    this.add(lblSemanasAgregadas, new XYConstraints(384, 107, 129, 25));
    this.add(panDesde, new XYConstraints(9, 2, 259, 104));
    this.add(panHasta, new XYConstraints(383, 2, 259, 104));
    this.add(txtSemanasConsecutivas, new XYConstraints(158, 108, 53, 25));
    this.add(txtSemanasAgregadas, new XYConstraints(530, 108, 53, 25));
    this.add(pnlEnfermedad, new XYConstraints(99, 135, 494, 40));
    this.add(btnBuscar, new XYConstraints(504, 178, 80, 24));
    this.add(clmMantCartas, new XYConstraints(7, 205, 602, 239));
  }

  //_________________________________________________________

  public String getDescCompleta(Data dat) {
    return ("");
  }

  //_________________________________________________________

  // M�todos que hay que definir para la interfaz ContPanAnoSemFecha.

  public void alPerderFocoAno() {
    // Actualizamos la cantidad de semanas.
    if ( (panDesde != null) &&
        (panHasta != null) &&
        (panDesde.getCodAno().length() > 0) &&
        (panDesde.getCodSem().length() > 0) &&
        (panHasta.getCodAno().length() > 0) &&
        (panHasta.getCodSem().length() > 0)) {
      cuentaSemanas(panDesde.getCodAno(), panDesde.getCodSem(),
                    panHasta.getCodAno(), panHasta.getCodSem());
    }
    else {
      lblSemPer.setText("");
    }
  }

  //_________________________________________________________
  public void alPerderFocoSemana() {
    // Actualizamos la cantidad de semanas.
    if ( (panDesde != null) &&
        (panHasta != null) &&
        (panDesde.getCodAno().length() > 0) &&
        (panDesde.getCodSem().length() > 0) &&
        (panHasta.getCodAno().length() > 0) &&
        (panHasta.getCodSem().length() > 0)) {
      cuentaSemanas(panDesde.getCodAno(), panDesde.getCodSem(),
                    panHasta.getCodAno(), panHasta.getCodSem());
    }
    else {
      lblSemPer.setText("");
    }
  }

  //_________________________________________________________
  public void alPerderFocoFecha() {
    // Actualizamos la cantidad de semanas.
    if ( (panDesde != null) &&
        (panHasta != null) &&
        (panDesde.getCodAno().length() > 0) &&
        (panDesde.getCodSem().length() > 0) &&
        (panHasta.getCodAno().length() > 0) &&
        (panHasta.getCodSem().length() > 0)) {
      cuentaSemanas(panDesde.getCodAno(), panDesde.getCodSem(),
                    panHasta.getCodAno(), panHasta.getCodSem());
    }
    else {
      lblSemPer.setText("");
    }
  }

  //_________________________________________________________
  public CApp getMiCApp() {
    return this.app;
  };
  //_________________________________________________________

  public void Inicializar() {
  }

  //_________________________________________________________

  // gesti�n del estado habilitado/deshabilitado de los componentes
  public void Inicializar(int i) {
    switch (i) {
      // modo espera: se hace un backup de los datos
      case CInicializar.ESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;

        // modo normal: si se ha modificado area, resetear CV y rellenar
      case CInicializar.NORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
    }
  }

  //_________________________________________________________

  // solicita la primera trama de datos
  public Lista primeraPagina() {
    Lista vFiltro = new Lista();
    Lista vReg = new Lista();
    Inicializar(CInicializar.ESPERA);
    final String servlet = "servlet/SrvMantCartasAvisos";

    try {
      if (datosValidos()) {
        Data dtEnvio = recogeDatos();
        vFiltro.addElement(dtEnvio);
        vFiltro.setParameter("CD_OPE", this.getApp().getParameter("COD_USUARIO"));
        /*                   // debug
             SrvMantCartasAvisos srv = new SrvMantCartasAvisos();
                            // par�metros jdbc
             srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                            vReg = srv.doDebug(0, vFiltro);*/
        this.getApp().getStub().setUrl("servlet/SrvMantCartasAvisos");
        vReg = (Lista)this.getApp().getStub().doPost(0, vFiltro);
        if (vReg.size() == 0) {
          this.getApp().showAdvise("No hay datos con estos criterios.");
        }
      }
    }
    catch (Exception ex) {
      this.getApp().trazaLog(ex);
    }
    // devuelve la lista para que la procese el componente clm
    Inicializar(CInicializar.NORMAL);
    return vReg;
  }

  //_________________________________________________________

  // solicita la siguiente trama de datos
  // es igual q el primeraPagina pero con una sentencia +
  public Lista siguientePagina() {
    Lista p = new Lista();
    return p;
  }

  //_________________________________________________________
  // operaciones de la botonera
  //mirar en PanPetCen.
  //pasarlo como Data
  public void realizaOperacion(int j) {

    Lista lismanpc = this.clmMantCartas.getLista();
    DiaMantCartasAvisos di = null;
    //Este Data debe pasarse como par�metro al constructor de PuntoCentExt
    Data dAvisos = new Data();
    switch (j) {

      // bot�n de modificaci�n
      case MODIFICACION:
        dAvisos = clmMantCartas.getSelected();
        //si existe alguna fila seleccionada
        if (dAvisos != null) {
          int ind = clmMantCartas.getSelectedIndex();
          di = new DiaMantCartasAvisos(this.getApp(), MODIFICACION, dAvisos);
          di.show();
          //Tratamiento de bAceptar para Salir
          if (di.bAceptar()) {
            clmMantCartas.setPrimeraPagina(primeraPagina());
          }
        }
        else {
          this.getApp().showAdvise("Debe seleccionar un registro en la tabla.");
        }
        break;
    }

  }

  //_________________________________________________________

  void btnBuscar_actionPerformed(ActionEvent e) {
    Inicializar(CInicializar.ESPERA);
    clmMantCartas.setPrimeraPagina(this.primeraPagina());
    Inicializar(CInicializar.NORMAL);
  }

  //_________________________________________________________

  private Data recogeDatos() {
    Data dtSalida = new Data();
    dtSalida.put("CD_ANOEPIINI", panDesde.getCodAno());
    dtSalida.put("CD_ANOEPIFIN", panHasta.getCodAno());
    dtSalida.put("CD_SEMEPIINI", panDesde.getCodSem());
    dtSalida.put("CD_SEMEPIFIN", panHasta.getCodSem());
    dtSalida.put("NM_SEMCONSEC", txtSemanasConsecutivas.getText());
    dtSalida.put("NM_SEMAGREG", txtSemanasAgregadas.getText());
    dtSalida.put("CD_ENFCIE", pnlEnfermedad.getDatos().getString("CD_ENFCIE"));
    return dtSalida;
  }

  private boolean datosValidos() {
    String sMensaje = "";
    boolean ta_gueno = true;
    if (panDesde.getCodAno().length() == 0) {
      sMensaje = "Debe introducir un a�o de inicio v�lido";
      ta_gueno = false;
    }
    if (panHasta.getCodAno().length() == 0) {
      sMensaje = "Debe introducir un a�o de final v�lido";
      ta_gueno = false;
    }
    if (panDesde.getCodSem().length() == 0) {
      sMensaje = "Debe introducir una semana de inicio v�lida";
      ta_gueno = false;
    }
    if (panHasta.getCodSem().length() == 0) {
      sMensaje = "Debe introducir una semana de final v�lida";
      ta_gueno = false;
    }
    if (txtSemanasConsecutivas.getText().length() == 0) {
      sMensaje = "Debe introducir un n� de semanas consecutivas v�lido";
      ta_gueno = false;
    }
    if (txtSemanasAgregadas.getText().length() == 0) {
      sMensaje = "Debe introducir un n� de semanas agregadas v�lido";
      ta_gueno = false;
    }
    if ( (pnlEnfermedad.getDatos() == null) ||
        (pnlEnfermedad.getDatos().getString("CD_ENFCIE").length() == 0)) {
      sMensaje = "Debe introducir un c�digo de enfermedad";
      ta_gueno = false;
    }
    if ( (ta_gueno) &&
        (Integer.parseInt(panDesde.getCodAno()) >
         Integer.parseInt(panHasta.getCodAno()))) {
      sMensaje = "El a�o inicial ha de ser menor o igual que el final";
      ta_gueno = false;
    }
    if ( (ta_gueno) &&
        (Integer.parseInt(panDesde.getCodAno()) ==
         Integer.parseInt(panHasta.getCodAno())) &&
        (Integer.parseInt(panDesde.getCodSem()) >=
         Integer.parseInt(panHasta.getCodSem()))) {
      sMensaje = "La semana inicial ha de ser menor que la semana final";
      ta_gueno = false;
    }
    if (sMensaje.length() > 0) {
      this.getApp().showError(sMensaje);
    }
    return ta_gueno;
  }

  private void cuentaSemanas(String anioini, String semanaini, String aniofin,
                             String semanafin) {
    int a1 = 0;
    int a2 = 0;
    int s1 = 0;
    int s2 = 0;
    int contador = 0;
    a1 = Integer.parseInt(anioini);
    a2 = Integer.parseInt(aniofin);
    s1 = Integer.parseInt(semanaini);
    s2 = Integer.parseInt(semanafin);
    if (a1 < a2) {
      for (int i = a1 + 1; i < a2; i++) {
        contador += 52;
      }
      for (int i = s1; i < 53; i++) {
        contador++;
      }
      for (int i = 1; i < s2; i++) {
        contador++;
      }
      lblSemPer.setText(String.valueOf(contador));
    }
    else
    if ( (a1 == a2) && (s1 < s2)) {
      for (int i = a1; i < a2; i++) {
        contador += 52;
      }
      for (int i = s1; i < s2; i++) {
        contador++;
      }
      lblSemPer.setText(String.valueOf(contador));
    }
  }
}

class PanMantCartasAvisos_btnBuscar_actionAdapter
    implements java.awt.event.ActionListener {
  PanMantCartasAvisos adaptee;

  PanMantCartasAvisos_btnBuscar_actionAdapter(PanMantCartasAvisos adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnBuscar_actionPerformed(e);
  }
}
