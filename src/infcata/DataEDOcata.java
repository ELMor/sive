
package infcata;

import java.io.Serializable;

public class DataEDOcata
    implements Serializable {

  public String sAnoDesde = "";
  public String sSemDesde = "";
  public String sAnoHasta = "";
  public String sSemHasta = "";
  public String sCdEnfermedad = "";
  public String sDsEnfermedad = "";
  public String sCdArea = "";
  public String sDsArea = "";
  public String sCdNiv1 = "";
  public String sDsNiv1 = "";
  public String sCdNiv2 = "";
  public String sDsNiv2 = "";
  public String sCdDistrito = "";
  public String sDsDistrito = "";
  public int numSemanas = 0;

  public boolean bInformeCompleto = false;
  public boolean bCasos = true;

  public DataEDOcata(String anoDesde, String semDesde, String anoHasta,
                     String semHasta, String cdEnf, String dsEnf, String cdArea,
                     String dsArea,
                     String cdNiv1, String dsNiv1, String cdNiv2, String dsNiv2,
                     String cdDistrito, String dsDistrito, int nsem) {

    sAnoDesde = anoDesde;
    sSemDesde = semDesde;
    sAnoHasta = anoHasta;
    sSemHasta = semHasta;
    sCdEnfermedad = cdEnf;
    sDsEnfermedad = dsEnf;
    sCdArea = cdArea;
    sDsArea = dsArea;
    sCdNiv1 = cdNiv1;
    sDsNiv1 = dsNiv1;
    sCdNiv2 = cdNiv2;
    sDsNiv2 = dsNiv2;
    sCdDistrito = cdDistrito;
    sDsDistrito = dsDistrito;
    numSemanas = nsem;
  }

  public DataEDOcata() {
    sAnoDesde = "";
    sSemDesde = "";
    sAnoHasta = "";
    sSemHasta = "";
    sCdEnfermedad = "";
    sDsEnfermedad = "";
    sCdArea = "";
    sDsArea = "";
    sCdNiv1 = "";
    sDsNiv1 = "";
    sCdNiv2 = "";
    sDsNiv2 = "";
    sCdDistrito = "";
    sDsDistrito = "";
    bInformeCompleto = false;
    bCasos = true;
  }

}
