
package infcata;

import java.io.Serializable;

public class Param_C9_3_14
    implements Serializable {
  public String CD_ENFERMEDAD = "";
  public String DS_ENFERMEDAD = "";
  public String C1 = "";
  public String C2 = "";
  public String C3 = "";
  public String C4 = "";
  public String C5 = "";
  public String C6 = "";
  public String C7 = "";
  public String C8 = "";
  public String C9 = "";
  public String C10 = "";
  public String C11 = "";
  public String C12 = "";
  public String C13 = "";
  /*  public float C1 = 0;
    public float C2 = 0;
    public float C3 = 0;
    public float C4 = 0;
    public float C5 = 0;
    public float C6 = 0;
    public float C7 = 0;
    public float C8 = 0;
    public float C9 = 0;
    public float C10 = 0;
    public float C11 = 0;
    public float C12 = 0;
    public float C13 = 0; */
  public String CTOTAL = "";
  public String CD_AREA = "";
  public String DS_AREA = "";
  public String CD_DISTRITO = "";
  public String DS_DISTRITO = "";
  public String CD_TVIGI = "";

  public int iPagina = 0;
  public boolean bInformeCompleto = false;

  public Param_C9_3_14() {

  }

}
