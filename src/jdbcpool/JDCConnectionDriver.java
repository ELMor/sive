package jdbcpool;

import java.sql.Connection;
import java.sql.SQLException;

public class JDCConnectionDriver {

  private JDCConnectionPool pool;

  public JDCConnectionDriver(String driver, String url,
                             String user, String password) throws
      ClassNotFoundException,
      InstantiationException, IllegalAccessException,
      SQLException {
    Class.forName(driver).newInstance();
    pool = new JDCConnectionPool(url, user, password);
  }

  // abre una conexi�n
  public Connection connect() throws SQLException {
    return pool.openConnection();
  }

  /**
   * ARG 12/06/2001
   * Abre una conexi�n a un usuario en concreto
   *
   * @param usr    - El usuario
   * @param passwd - Su password
   * @return Conexion abierta
   */
  public Connection connect(String usr, String passwd) throws SQLException {
    return pool.openConnection(usr, passwd);
  } // connect()

  // libera una conexi�n
  public void disconnect(Connection conn) throws SQLException {
    pool.returnConnection(conn);
  }

  // libera la chache
  public void clearPool() {
    pool.closeConnections();
    pool = null;
  }
}
