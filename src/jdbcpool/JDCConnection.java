package jdbcpool;

import java.sql.Connection;

public class JDCConnection {

  private JDCConnectionPool pool;
  private Connection conn;
  private boolean inuse;
  private long timestamp;

  /** Usuario de la conexion */
  private String usuario;

  /** Password */
  private String password;

  public JDCConnection(Connection conn, JDCConnectionPool pool) {
    this.conn = conn;
    this.pool = pool;
    this.inuse = false;
    this.timestamp = 0;
    this.usuario = "";
  }

  /**
   * ARG 12/06/2001
   * Constructor para una conexi�n de un usuario en concreto
   *
   * @param conn   - La conexion
   * @param pool   - El pool de conaxiones
   * @param usr    - El usuario
   * @return Conexion abierta
   */
  public JDCConnection(Connection conn, JDCConnectionPool pool, String usr) {
    this.conn = conn;
    this.pool = pool;
    this.inuse = false;
    this.timestamp = 0;
    this.usuario = usr;
  }

  // petici�n de alquiler
  public synchronized boolean lease() {
    if (inuse) {
      return false;
    }
    else {
      inuse = true;
      timestamp = System.currentTimeMillis();
      return true;
    }
  }

  /**
   * ARG 12/06/2001
   * Petici�n de alquiler a un usuario en concreto
   *
   * @param usr - El usuario
   * @return Posibilidad de alquiler de la conexion
   */
  public synchronized boolean lease(String usr) {
    if (inUse(usr)) {
      return false;
    }
    else {
      inuse = true;
      timestamp = System.currentTimeMillis();
      return true;
    }
  } // lease()

  // determian si es v�lida
  public boolean validate() {
    try {
      conn.getMetaData();
    }
    catch (Exception e) {
      return false;
    }
    return true;
  }

  // devuelve si est� en uso o no
  public boolean inUse() {
    return inuse;
  }

  /**
   * ARG 12/06/2001
   * Comprueba si la conexio esta en uso para un usuario en concreto
   *
   * @param usr - El usuario
   * @return Si la conexion esta en uso
   */
  public boolean inUse(String usr) {
    if (this.usuario.equals(usr)) {
      // Si el usuario coincide se devuelve si esta en uso
      return inuse;
    }
    else {
      // Si el usuario no coincide se devuelve que si esta en uso para
      // que no se use esta conexion, ya que pertenece a otro usuario
      return true;
    }
  } // inUse()

  // devuelve el tiempo de uso
  public long getLastUse() {
    return timestamp;
  }

  // fin de alquiler
  public void expireLease() {
    inuse = false;
  }

  // deveuelve la conexi�n JDBC
  public Connection getConnection() {
    return conn;
  }

}
