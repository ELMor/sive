package jdbcpool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Vector;

// tread de refresco de conexiones en mal estado
class ConnectionReaper
    extends Thread {

  private JDCConnectionPool pool;
  private long delay = 60000;

  public ConnectionReaper(JDCConnectionPool pool) {
    this.pool = pool;
  }

  public void run() {

    // loop de refresco
    while (true) {
      try {
        sleep(delay);
      }
      catch (InterruptedException e) {}
      pool.reapConnections();
    }
  }
}

// tread de liberaci�n de chache
class CacheReaper
    extends Thread {

  private JDCConnectionPool pool;
  private long delay = 60000;

  public CacheReaper(JDCConnectionPool pool) {
    this.pool = pool;
  }

  public void run() {

    // loop de refresco
    while (true) {
      try {
        sleep(delay);
      }
      catch (InterruptedException e) {}
      pool.reapCache();
    }
  }
}

public class JDCConnectionPool {

  private Vector connections; // conexiones
  private String url, user, password; // par�metros jdbc
  private long timeout = 60000; // tiempo m�ximo por conexi�n
  private ConnectionReaper killer; // conexiones en mal estado
  private CacheReaper reaper; // refresco
//   private int poolsize=20; // tama�o del pool

  // ARG: Aumento el tama�o del pool.
  private int poolsize = 40;

  public JDCConnectionPool(String url, String user, String password) {
    this.url = url;
    this.user = user;
    this.password = password;
    connections = new Vector(poolsize);
    killer = new ConnectionReaper(this);
    killer.start();
    reaper = new CacheReaper(this);
    reaper.start();
  }

  // chequea las conexiones para cerrar las que cumplan lo siguiente:
  //   - este en uso
  //   - se haya producido un timeout
  //   - est� no v�lida
  public synchronized void reapConnections() {

    long stale = System.currentTimeMillis() - timeout;
    Enumeration connlist = connections.elements();

    while ( (connlist != null) && (connlist.hasMoreElements())) {
      JDCConnection conn = (JDCConnection) connlist.nextElement();

      // conexiones en mal estado
      if ( (conn.inUse()) && // conexi�n de uso
          (stale > conn.getLastUse()) && // timeout
          (!conn.validate())) { // conexi�n no v�lida

        //System_out.println("JDBCPOOL >> conexi�n destruida");
        removeConnection(conn);
      }
    }
  }

  // chequea las conexiones para cerrar las que cumplan lo siguiente:
  //   - no este en uso
  //   - lleve sin usarse un tiempo m�ximo
  public synchronized void reapCache() {

    long stale = System.currentTimeMillis() - timeout;
    Enumeration connlist = connections.elements();

    while ( (connlist != null) && (connlist.hasMoreElements())) {
      JDCConnection conn = (JDCConnection) connlist.nextElement();

      // conexi�n sin usarse
      if ( (!conn.inUse()) &&
          (stale > conn.getLastUse())) {

        //System_out.println("JDBCPOOL >> conexi�n cerrada");
        removeConnection(conn);
      }
    }
  }

  // elimina todas las conexiones del pool
  public synchronized void closeConnections() {

    Enumeration connlist = connections.elements();

    while ( (connlist != null) && (connlist.hasMoreElements())) {
      JDCConnection conn = (JDCConnection) connlist.nextElement();
      removeConnection(conn);
    }
  }

  // elimina una conexi�n del pool
  private synchronized void removeConnection(JDCConnection c) {
    Connection conn;

    connections.removeElement(c);
    conn = c.getConnection();
    try {
      conn.close();
    }
    catch (Exception e) {
      ;
    }
    conn = null;
    c = null;
  }

  // abre una conexi�n con las siguientes premisas:
  //   - devuelve una de la cache si la encuentra
  //   - no permite superar el tama�o del pool
  public synchronized Connection openConnection() throws SQLException {
    int i;
    JDCConnection c;

    for (i = 0; i < connections.size(); i++) {
      c = (JDCConnection) connections.elementAt(i);
      if (c.lease()) {
        //System_out.println("JDBCPOOL >> conexi�n cedida");
        return c.getConnection();
      }
    }

    if (connections.size() <= poolsize) {
      Connection conn = DriverManager.getConnection(url, user, password);
      c = new JDCConnection(conn, this);
      c.lease();
      connections.addElement(c);
      //System_out.println("JDBCPOOL >> conexi�n abierta (" + Integer.toString(i+1) + ")");
      return conn;
    }
    else {
      throw new SQLException("Se ha superado el tama�o m�ximo de conexiones");
    }
  }

  /**
   * ARG 12/06/2001
   * Abre una conexi�n a un usuario en concreto
   *
   * @param usr    - El usuario
   * @param passwd - Su password
   * @return Conexion abierta
   */
  public synchronized Connection openConnection(String usr, String passwd) throws
      SQLException {
    int i;
    JDCConnection c;

    // Comprueba si hay conexiones abiertas para el usuario en concreto
    for (i = 0; i < connections.size(); i++) {
      c = (JDCConnection) connections.elementAt(i);
      if (c.lease(usr)) {
        //System_out.println("JDBCPOOL >> conexi�n cedida");
        return c.getConnection();
      }
    }

    if (connections.size() <= poolsize) {
      // ARG: Ahora la conexion se abre para un usuario en concreto
      Connection conn = DriverManager.getConnection(url, usr, passwd);
      c = new JDCConnection(conn, this, usr);
      c.lease();
      connections.addElement(c);
      //System_out.println("JDBCPOOL >> conexi�n abierta (" + Integer.toString(i+1) + ")");
      return conn;
    }
    else {
      throw new SQLException("Se ha superado el tama�o m�ximo de conexiones");
    }
  } // openConnection()

  // libera una conexi�n para ser cacheada
  public synchronized void returnConnection(Connection conn) {
    JDCConnection c;

    for (int i = 0; i < connections.size(); i++) {
      c = (JDCConnection) connections.elementAt(i);
      if (c.getConnection() == conn) {
        //System_out.println("JDBCPOOL >> conexi�n liberada");
        c.expireLease();
      }
    }
  }
}
