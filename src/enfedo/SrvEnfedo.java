
package enfedo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.util.Enumeration;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvEnfedo
    extends DBServlet {

  // modos de los servlets
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    CLista data = new CLista();
    DataCodigoCIE datEnfCIE = null;
    DataPan_MantEnferEDO datEnfEDO = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    datEnfEDO = (DataPan_MantEnferEDO) param.firstElement();

    // gesti�n de commit -----------
    try {
      // modos de operaci�n
      switch (opmode) {

        // alta de enfermedad CIE
        case servletALTA:

          // prepara la query

          query = "insert into SIVE_ENFEREDO (CD_ENFERE, CD_ENFCIE, CD_TVIGI, IT_RESSEM,IT_INDSEM,IT_INDURG,IT_RESANU,CD_OPE,FC_ULTACT,IT_BAJA, IT_REGESP) values (?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?)";
          st = con.prepareStatement(query);

          st.setString(1, datEnfEDO.sEnfermedadEDO.trim());
          st.setString(2, datEnfEDO.sCodigoCIE.trim());
          st.setString(3, datEnfEDO.sTipoDeclaracion.trim());
          if (datEnfEDO.bResumenSemanal == true) {
            st.setString(4, "S");
          }
          else {
            st.setString(4, "N");
          }
          if (datEnfEDO.bIndivSemanal == true) {
            st.setString(5, "S");
          }
          else {
            st.setString(5, "N");
          }
          if (datEnfEDO.bIndividualUrgente == true) {
            st.setString(6, "S");
          }
          else {
            st.setString(6, "N");
          }
          if (datEnfEDO.bResumenAnual == true) {
            st.setString(7, "S");
          }
          else {
            st.setString(7, "N");
          }
          st.setString(8, param.getLogin().trim());
          st.setDate(9, new java.sql.Date(new java.util.Date().getTime()));
          if (datEnfEDO.bBorrar == true) {
            st.setString(10, "S"); //Con marca de baja( No est� disponible)
          }
          else {
            st.setString(10, "N"); //No tiene marca de baja

          }
          st.setString(11, "N");
          // lanza la query
          st.executeUpdate();
          st.close();
          break;

          // b�squeda de enfermedad EDO
        case servletSELECCION_X_CODIGO:
        case servletSELECCION_X_DESCRIPCION:

          // prepara la query
          // ARG: upper (9/5/02)
          if (param.getFilter().length() > 0) {
            if (opmode == servletSELECCION_X_CODIGO) {
              query = "select CD_ENFEREDO,DS_ENFEREDO,DSL_ENFEREDO from SIVE_EDO_CNE  where CD_ENFEREDO like ? and CD_ENFEREDO > ? order by CD_ENFEREDO";
            }
            else {
              query = "select CD_ENFEREDO,DS_ENFEREDO,DSL_ENFEREDO from SIVE_EDO_CNE  where upper(DS_ENFEREDO) like upper(?) and upper(DS_ENFEREDO) > upper(?) order by DS_ENFEREDO";
            }
          }
          else {
            if (opmode == servletSELECCION_X_CODIGO) {
              query = "select CD_ENFEREDO,DS_ENFEREDO,DSL_ENFEREDO from SIVE_EDO_CNE where CD_ENFEREDO like ?  order by CD_ENFEREDO";
            }
            else {
              query = "select CD_ENFEREDO,DS_ENFEREDO,DSL_ENFEREDO from SIVE_EDO_CNE where upper(DS_ENFEREDO) like upper(?)  order by DS_ENFEREDO";
            }
          }

          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);

          // filtro
          if (opmode == servletSELECCION_X_CODIGO) {
            st.setString(1, datEnfEDO.sEnfermedadEDO.trim() + "%");
          }
          else {
            st.setString(1, "%" + datEnfEDO.sEnfermedadEDO.trim() + "%");

            // paginaci�n
          }
          if (param.getFilter().length() > 0) {
            st.setString(2, param.getFilter().trim());

          }
          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);

              if (opmode == servletSELECCION_X_CODIGO) {
                data.setFilter( ( (DataPan_MantEnferEDO) data.lastElement()).
                               sEnfermedadEDO);
              }
              else {
                data.setFilter( ( (DataPan_MantEnferEDO) data.lastElement()).
                               sDescEnfermedadEDO);

              }
              break;
            }

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            String sCodEDO;
            String sDescEDO;
            String sDescLEDO;
            //Recogemos datos de tabla ENFEREDO
            sCodEDO = rs.getString("CD_ENFEREDO");
            sDescEDO = rs.getString("DS_ENFEREDO");
            sDescLEDO = rs.getString("DSL_ENFEREDO");

            //Obtiene la descripci�n auxiliar en funci�n del idioma
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDescLEDO != null)) {
              sDescEDO = sDescLEDO;

            }

            data.addElement(new DataPan_MantEnferEDO(false, sCodEDO, sDescEDO,
                "", "", "", false, false, false, false, false, "", ""));
            i++;
          } //while que recorre el ResultSet

          rs.close();
          st.close();

          ////////////////////////////////////
          //Busco m�s datos, si los hay  (detalle)  en tabla SIVE_ENFEREDO

          Enumeration enum = data.elements();
          while (enum.hasMoreElements()) { //Recorro mi vector

            DataPan_MantEnferEDO elemActual = (DataPan_MantEnferEDO) (enum.
                nextElement());
            PreparedStatement stDet = null;
            String queryDet = null;
            ResultSet rsDet = null;

            queryDet = "select a.CD_ENFCIE,b.DS_PROCESO,b.DSL_PROCESO, a.CD_TVIGI, a.IT_RESSEM, a.IT_INDSEM, a.IT_INDURG,a.IT_RESANU, a.CD_OPE, a.FC_ULTACT, a.IT_BAJA from SIVE_ENFEREDO a, SIVE_PROCESOS b where CD_ENFERE = ?   and a.CD_ENFCIE=b.CD_ENFCIE";

            //No buscamos cod operador ni fecha
            // lanza la query
            stDet = con.prepareStatement(queryDet);

            stDet.setString(1, elemActual.sEnfermedadEDO);

            rsDet = stDet.executeQuery();

            if (rsDet.next() == false) { //No estaba ese elem en  tabla
              elemActual.bEstaEnTabla = false;

            }
            else { //Consulta debe haber devuelto un elemento

              elemActual.bEstaEnTabla = true;

              elemActual.sCodigoCIE = rsDet.getString("CD_ENFCIE");

              String sDescCodigo;
              String sDescLCodigo;
              sDescCodigo = rsDet.getString("DS_PROCESO");
              sDescLCodigo = rsDet.getString("DSL_PROCESO");

              //Obtiene la descrpci�n auxiliar en funci�n del idioma
              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                  (sDescLCodigo != null)) {
                elemActual.sDescCodigo = sDescLCodigo;

              }
              else {
                elemActual.sDescCodigo = sDescCodigo;
              }

              elemActual.sTipoDeclaracion = rsDet.getString("CD_TVIGI");

              if ( (rsDet.getString("IT_RESSEM")).equals("S")) {
                elemActual.bResumenSemanal = true;
              }
              else {
                elemActual.bResumenSemanal = false;

              }
              if ( (rsDet.getString("IT_INDSEM")).equals("S")) {
                elemActual.bIndivSemanal = true;
              }
              else {
                elemActual.bIndivSemanal = false;

              }
              if ( (rsDet.getString("IT_INDURG")).equals("S")) {
                elemActual.bIndividualUrgente = true;
              }
              else {
                elemActual.bIndividualUrgente = false;

              }
              if ( (rsDet.getString("IT_RESANU")).equals("S")) {
                elemActual.bResumenAnual = true;
              }
              else {
                elemActual.bResumenAnual = false;

              }
              elemActual.sCodOperario = rsDet.getString("CD_OPE");

              java.sql.Date miFecha = rsDet.getDate("FC_ULTACT");
              String strFecha = DateFormat.getDateInstance().format(miFecha);
              elemActual.sFecha = strFecha;

              String strBaja = rsDet.getString("IT_BAJA");
              if (strBaja.equals("S")) {
                elemActual.bBorrar = true;
              }
              else {
                elemActual.bBorrar = false;

              }
            } //else
            rsDet.close();
            stDet.close();

          } //while que recorre la CLista

          break;

          // modificaci�n de enfermedad
        case servletMODIFICAR:

          // prepara la query
          query = "UPDATE SIVE_ENFEREDO SET CD_TVIGI=?, IT_RESSEM=?, IT_INDSEM=?, IT_INDURG=?,IT_RESANU=?, CD_OPE=?, FC_ULTACT=?, IT_BAJA=?  WHERE CD_ENFCIE=?";

          // lanza la query
          st = con.prepareStatement(query);

          st.setString(1, datEnfEDO.sTipoDeclaracion.trim());

          if (datEnfEDO.bResumenSemanal == true) {
            st.setString(2, "S");
          }
          else {
            st.setString(2, "N");

          }
          if (datEnfEDO.bIndivSemanal == true) {
            st.setString(3, "S");
          }
          else {
            st.setString(3, "N");
          }
          if (datEnfEDO.bIndividualUrgente == true) {
            st.setString(4, "S");
          }
          else {
            st.setString(4, "N");
          }
          if (datEnfEDO.bResumenAnual == true) {
            st.setString(5, "S");
          }
          else {
            st.setString(5, "N");

          }
          st.setString(6, param.getLogin().trim());

          st.setDate(7, new java.sql.Date(new java.util.Date().getTime()));

          if (datEnfEDO.bBorrar == true) {
            st.setString(8, "S");
          }
          else {
            st.setString(8, "N");
          }
          st.setString(9, datEnfEDO.sCodigoCIE.trim());

          st.executeUpdate();
          st.close();
          break;

        case servletOBTENER_X_CODIGO:
        case servletOBTENER_X_DESCRIPCION:

          if (opmode == servletOBTENER_X_CODIGO) {
            // prepara la query
            query = "select CD_ENFEREDO,DS_ENFEREDO,DSL_ENFEREDO from SIVE_EDO_CNE where CD_ENFEREDO = ?";
          }
          else {
            query = "select CD_ENFEREDO,DS_ENFEREDO,DSL_ENFEREDO from SIVE_EDO_CNE where DS_ENFEREDO = ?";
          }

          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);

          // filtro
          st.setString(1, datEnfEDO.sEnfermedadEDO.trim());

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {

            String sCodEDO;
            String sDescEDO;
            String sDescLEDO;
            //Recogemos datos de tabla ENFEREDO
            sCodEDO = rs.getString("CD_ENFEREDO");

            sDescEDO = rs.getString("DS_ENFEREDO");
            sDescLEDO = rs.getString("DSL_ENFEREDO");

            //Obtiene la descrpci�n auxiliar en funci�n del idioma
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDescLEDO != null)) {
              sDescEDO = sDescLEDO;
            }

            data.addElement(new DataPan_MantEnferEDO(false, sCodEDO, sDescEDO,
                "", "", "", false, false, false, false, false, "", ""));

          } //while que recorre el ResultSet

          rs.close();
          st.close();

          ////////////////////////////////////
          //Busco m�s datos, si los hay  (detalle)  en tabla SIVE_ENFEREDO

          Enumeration enum2 = data.elements();
          while (enum2.hasMoreElements()) { //Recorro mi vector(en este caso puede tener 0 o un elementos)
            DataPan_MantEnferEDO elemActual = (DataPan_MantEnferEDO) (enum2.
                nextElement());
            PreparedStatement stDet = null;
            String queryDet = null;
            ResultSet rsDet = null;

            queryDet = "select a.CD_ENFCIE,b.DS_PROCESO,b.DSL_PROCESO, a.CD_TVIGI, a.IT_RESSEM, a.IT_INDSEM, a.IT_INDURG,a.IT_RESANU, a.CD_OPE, a.FC_ULTACT, a.IT_BAJA from SIVE_ENFEREDO a, SIVE_PROCESOS b where CD_ENFERE = ?   and a.CD_ENFCIE=b.CD_ENFCIE";
            //No buscamos cod operador ni fecha
            // lanza la query
            stDet = con.prepareStatement(queryDet);

            stDet.setString(1, elemActual.sEnfermedadEDO);
            rsDet = stDet.executeQuery();

            if (rsDet.next() == false) { //No estaba ese elem en  tabla
              elemActual.bEstaEnTabla = false;
              //# // System_out.println("Elem no est� en tabla");//*********************************
            }
            else { //Consulta debe haber devuelto un elemento

              elemActual.bEstaEnTabla = true;
              elemActual.sCodigoCIE = rsDet.getString("CD_ENFCIE");
              String sDescCodigo;
              String sDescLCodigo;
              sDescCodigo = rsDet.getString("DS_PROCESO");
              sDescLCodigo = rsDet.getString("DSL_PROCESO");

              //Obtiene la descrpci�n auxiliar en funci�n del idioma
              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                  (sDescLCodigo != null)) {
                elemActual.sDescCodigo = sDescLCodigo;
              }
              else {
                elemActual.sDescCodigo = sDescCodigo;
              }
              //# // System_out.println("descrip");//*********************************

               elemActual.sTipoDeclaracion = rsDet.getString("CD_TVIGI");
               //# // System_out.println("tipo vigi");//*********************************

              if ( (rsDet.getString("IT_RESSEM")).equals("S")) {
                elemActual.bResumenSemanal = true;
              }
              else {
                elemActual.bResumenSemanal = false;
                //# // System_out.println("res sem");//*********************************

              }
              if ( (rsDet.getString("IT_INDSEM")).equals("S")) {
                elemActual.bIndivSemanal = true;
              }
              else {
                elemActual.bIndivSemanal = false;
                //# // System_out.println("ind sem");//*********************************

              }
              if ( (rsDet.getString("IT_INDURG")).equals("S")) {
                elemActual.bIndividualUrgente = true;
              }
              else {
                elemActual.bIndividualUrgente = false;
                //# // System_out.println("ind urg");//*********************************

              }
              if ( (rsDet.getString("IT_RESANU")).equals("S")) {
                elemActual.bResumenAnual = true;
              }
              else {
                elemActual.bResumenAnual = false;
                //# // System_out.println("res anu");//*********************************

                 //# // System_out.println("booleanos");//*********************************

              }
              elemActual.sCodOperario = rsDet.getString("CD_OPE");
              //# // System_out.println("operador");//*********************************

              java.sql.Date miFecha = rsDet.getDate("FC_ULTACT");
              String strFecha = DateFormat.getDateInstance().format(miFecha);
              elemActual.sFecha = strFecha;
              //# // System_out.println("fecha");//*********************************

              String strBaja = rsDet.getString("IT_BAJA");
              if (strBaja.equals("S")) {
                elemActual.bBorrar = true;
                // System_out.print("Obtenido baja S en");
                //# // System_out.println(elemActual.sEnfermedadEDO);
              }
              else {
                elemActual.bBorrar = false;
                //# // System_out.println("Obtenido baja N en");
                //# // System_out.println(elemActual.sEnfermedadEDO);
              }
              //# // System_out.println("baja");//*********************************

            } //else
            rsDet.close();
            stDet.close();
          } //while que recorre la CLista

          break;

      } //switch

    }
    catch (Exception exception) { // rollback
      con.rollback();
      throw exception;
    }

    // commit
    con.commit();

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    data.trimToSize();
    return data;
  } // proc doWork
} //clase
