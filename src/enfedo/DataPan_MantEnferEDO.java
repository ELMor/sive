package enfedo;

import java.io.Serializable;

public class DataPan_MantEnferEDO
    implements Serializable {

  boolean bEstaEnTabla;
  //Normalmente a false: s�lo es significativo cuando se selecciona un item o evento obtener
  //En ese caso lo rellenamos en el servlett e indica si el elemento
  // ya est� en tabla(pasamos a modo modificar en cliente)) o no (seguimos en modo alta)
  public String sEnfermedadEDO;
  public String sDescEnfermedadEDO;
  public String sCodigoCIE;
  public String sDescCodigo;
  public String sTipoDeclaracion;
  public boolean bResumenSemanal;
  public boolean bIndivSemanal;
  public boolean bIndividualUrgente;
  public boolean bResumenAnual;
  public boolean bBorrar;
  public String sFecha;
  public String sCodOperario;

  public DataPan_MantEnferEDO() {
  }

  public DataPan_MantEnferEDO(boolean EstaEnTabla, String EnfermedadEDO,
                              String DescEnfermedadEDO,
                              String CodigoCIE, String DescCodigo,
                              String TipoDeclaracion,
                              boolean ResumenSemanal, boolean IndivSemanal,
                              boolean IndividualUrgente, boolean ResumenAnual,
                              boolean Borrar, String Fecha, String CodOperario) {
    bEstaEnTabla = EstaEnTabla;
    sEnfermedadEDO = EnfermedadEDO;
    sDescEnfermedadEDO = DescEnfermedadEDO;
    sCodigoCIE = CodigoCIE;
    sDescCodigo = DescCodigo;
    sTipoDeclaracion = TipoDeclaracion;
    bResumenSemanal = ResumenSemanal;
    bIndivSemanal = IndivSemanal;
    bIndividualUrgente = IndividualUrgente;
    bResumenAnual = ResumenAnual;
    bBorrar = Borrar;
    sFecha = Fecha;
    sCodOperario = CodOperario;

  }
} //clase
