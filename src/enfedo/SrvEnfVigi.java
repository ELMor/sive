
package enfedo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import catalogo.DataCat;
import sapp.DBServlet;

public class SrvEnfVigi
    extends DBServlet {

  // modos de los servlets
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletOBTENERindiv_X_CODIGO = 7;
  final int servletOBTENERindiv_X_DESCRIPCION = 8;
  final int servletSELECCIONindiv_X_CODIGO = 9;
  final int servletSELECCIONindiv_X_DESCRIPCION = 10;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    CLista data = new CLista();
    DataCat datEnfCIE = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    datEnfCIE = (DataCat) param.firstElement();

    // modos de operaci�n
    switch (opmode) {

      case servletSELECCION_X_CODIGO:
      case servletSELECCION_X_DESCRIPCION:

        // prepara la query
        // ARG: upper (13/5/02)
        if (param.getFilter().length() > 0) {
          if (opmode == servletSELECCION_X_CODIGO) {
            query = "select CD_ENFCIE,DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS  where CD_ENFCIE like ? and CD_ENFCIE > ? and CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where it_baja<>'S') order by CD_ENFCIE";
          }
          else {
            query = "select CD_ENFCIE,DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS  where upper(DS_PROCESO) like upper(?) and upper(DS_PROCESO) > upper(?) and CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where it_baja<>'S') order by CD_ENFCIE";
          }
        }
        else {
          if (opmode == servletSELECCION_X_CODIGO) {
            query = "select CD_ENFCIE,DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS  where CD_ENFCIE like ? and CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where it_baja<>'S') order by CD_ENFCIE";
          }
          else {
            query = "select CD_ENFCIE,DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS  where upper(DS_PROCESO) like upper(?) and CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where it_baja<>'S') order by CD_ENFCIE";
          }
        }

        // prepara la lista de resultados
        data = new CLista();

        // lanza la query
        st = con.prepareStatement(query);

        // filtro
        if (opmode == servletSELECCION_X_CODIGO) {
          st.setString(1, datEnfCIE.getCod().trim() + "%");
        }
        else {
          st.setString(1, "%" + datEnfCIE.getCod().trim() + "%");

          // paginaci�n
        }
        if (param.getFilter().length() > 0) {
          st.setString(2, param.getFilter().trim());

        }

        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {
          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataCat) data.lastElement()).getCod());
            break;
          }

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }

          String sCodEDO;
          String sDescEDO;
          String sDescLEDO;

          //Recogemos datos de tabla ENFEREDO
          sCodEDO = rs.getString("CD_ENFCIE");
          sDescEDO = rs.getString("DS_PROCESO");
          sDescLEDO = rs.getString("DSL_PROCESO");

          //Obtiene la descrpci�n auxiliar en funci�n del idioma
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDescLEDO != null)) {
            sDescEDO = sDescLEDO;
          }

          data.addElement(new DataCat(sCodEDO, sDescEDO, ""));
          i++;
        } //while que recorre el ResultSet

        rs.close();
        st.close();

        break;

      case servletOBTENER_X_CODIGO:
      case servletOBTENER_X_DESCRIPCION:

        // prepara la query
        if (opmode == servletOBTENER_X_CODIGO) {
          query = "select CD_ENFCIE,DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS  where CD_ENFCIE = ? and CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where it_baja<>'S')";
        }
        else {
          query = "select CD_ENFCIE,DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS  where DS_PROCESO = ? and CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where it_baja<>'S')";

          // prepara la lista de resultados
        }
        data = new CLista();

        // lanza la query
        st = con.prepareStatement(query);

        // filtro
        st.setString(1, datEnfCIE.getCod().trim());

        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {

          String sCodEDO;
          String sDescEDO;
          String sDescLEDO;

          //Recogemos datos de tabla ENFEREDO
          sCodEDO = rs.getString("CD_ENFCIE");
          sDescEDO = rs.getString("DS_PROCESO");
          sDescLEDO = rs.getString("DSL_PROCESO");

          //Obtiene la descrpci�n auxiliar en funci�n del idioma
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDescLEDO != null)) {
            sDescEDO = sDescLEDO;
          }

          data.addElement(new DataCat(sCodEDO, sDescEDO, ""));
        } //while que recorre el ResultSet

        rs.close();
        st.close();
        break;

      case servletSELECCIONindiv_X_CODIGO:
      case servletSELECCIONindiv_X_DESCRIPCION:

        // prepara la query
        // ARG upper (15/5/02)
        if (param.getFilter().length() > 0) {
          if (opmode == servletSELECCIONindiv_X_CODIGO) {
            query = "select CD_ENFCIE,DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS  where CD_ENFCIE like ? and CD_ENFCIE > ? and CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where it_baja<>'S' and((cd_tvigi='I')or(cd_tvigi='A'))) order by CD_ENFCIE";
          }
          else {
            query = "select CD_ENFCIE,DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS  where upper(DS_PROCESO) like upper(?) and upper(DS_PROCESO) > upper(?) and CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where it_baja<>'S' and((cd_tvigi='I')or(cd_tvigi='A'))) order by DS_PROCESO";
          }
        }
        else {
          if (opmode == servletSELECCIONindiv_X_CODIGO) {
            query = "select CD_ENFCIE,DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS  where CD_ENFCIE like ? and CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where it_baja<>'S' and((cd_tvigi='I')or(cd_tvigi='A'))) order by CD_ENFCIE";
          }
          else {
            query = "select CD_ENFCIE,DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS  where upper(DS_PROCESO) like upper(?) and CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where it_baja<>'S' and((cd_tvigi='I')or(cd_tvigi='A'))) order by DS_PROCESO";
          }
        }

        // prepara la lista de resultados
        data = new CLista();

        // lanza la query
        st = con.prepareStatement(query);

        // filtro
        if (opmode == servletSELECCIONindiv_X_CODIGO) {
          st.setString(1, datEnfCIE.getCod().trim() + "%");
        }
        else {
          st.setString(1, "%" + datEnfCIE.getCod().trim() + "%");

          // paginaci�n
        }
        if (param.getFilter().length() > 0) {
          st.setString(2, param.getFilter().trim());

        }

        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {
          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataCat) data.lastElement()).getCod());
            break;
          }

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }

          String sCodEDO;
          String sDescEDO;
          String sDescLEDO;

          //Recogemos datos de tabla ENFEREDO
          sCodEDO = rs.getString("CD_ENFCIE");
          sDescEDO = rs.getString("DS_PROCESO");
          sDescLEDO = rs.getString("DSL_PROCESO");

          //Obtiene la descrpci�n auxiliar en funci�n del idioma
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDescLEDO != null)) {
            sDescEDO = sDescLEDO;
          }

          data.addElement(new DataCat(sCodEDO, sDescEDO, ""));
          i++;
        } //while que recorre el ResultSet

        rs.close();
        st.close();

        break;

      case servletOBTENERindiv_X_CODIGO:
      case servletOBTENERindiv_X_DESCRIPCION:

        // prepara la query
        if (opmode == servletOBTENERindiv_X_CODIGO) {
          query = "select CD_ENFCIE,DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS  where CD_ENFCIE = ? and CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where it_baja<>'S' and((cd_tvigi='I')or(cd_tvigi='A')))";
        }
        else {
          query = "select CD_ENFCIE,DS_PROCESO,DSL_PROCESO from SIVE_PROCESOS  where DS_PROCESO = ? and CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where it_baja<>'S' and((cd_tvigi='I')or(cd_tvigi='A')))";

          // prepara la lista de resultados
        }
        data = new CLista();

        // lanza la query
        st = con.prepareStatement(query);

        // filtro
        st.setString(1, datEnfCIE.getCod().trim());

        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {

          String sCodEDO;
          String sDescEDO;
          String sDescLEDO;

          //Recogemos datos de tabla ENFEREDO
          sCodEDO = rs.getString("CD_ENFCIE");
          sDescEDO = rs.getString("DS_PROCESO");
          sDescLEDO = rs.getString("DSL_PROCESO");

          //Obtiene la descrpci�n auxiliar en funci�n del idioma
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDescLEDO != null)) {
            sDescEDO = sDescLEDO;
          }

          data.addElement(new DataCat(sCodEDO, sDescEDO, ""));
        } //while que recorre el ResultSet

        rs.close();
        st.close();
        break;

    } //switch

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    data.trimToSize();
    return data;
  } // proc doWork
} //clase
