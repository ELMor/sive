package enfedo;

import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CListaMantenimiento;
import sapp.StubSrvBD;

public class CMantenimientoEDO
    extends CListaMantenimiento {
  XYLayout xYLayout1 = new XYLayout();
  XYLayout xYLayout2 = new XYLayout();

  public CMantenimientoEDO(MantEnferEDO a) {
    super( (CApp) a,
          3,
          "100\n400\n62",
          a.res.getString("msg1.Text"),
          new StubSrvBD(),
          a.strSERVLET,
          a.servletSELECCION_X_CODIGO,
          a.servletSELECCION_X_DESCRIPCION);
    ConfigModo(false, true, false);
  }

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {

  }

  // modifica l�nea de la lista
  public void btnModificar_actionPerformed(int i) {
    int elModo = modoOperacion;
    DataPan_MantEnferEDO losDatos = null;

    losDatos = (DataPan_MantEnferEDO) lista.elementAt(i);

    modoOperacion = modoESPERA;
    Inicializar();

    if (losDatos.bEstaEnTabla) {
      elModo = Pan_MantEnferEDO.modoMODIFICAR;
    }
    else {
      elModo = Pan_MantEnferEDO.modoALTA;

    }
    Pan_MantEnferEDO panel = new Pan_MantEnferEDO(this.app,
                                                  elModo,
                                                  losDatos);

    panel.show();

    if (panel.bAceptar == true) {
      ////# // System_out.println("Pregunta en panel **** "+ panel.pregunta);
      ////# // System_out.println("CodCIE  **** "+ panel.pregunta.sCodigoCIE);
      lista.removeElementAt(i);
      lista.insertElementAt(panel.pregunta, i);
      RellenaTabla();

    }

    panel = null;

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed(int i) {

  }

  // rellena los par�metros para enviar al servlet
  public Object setComponente(String s) {
    return new DataPan_MantEnferEDO(false, s, "", "", "", "", false, false, false, false, false,
                                    "", "");
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(Object o) {
    DataPan_MantEnferEDO preg = (DataPan_MantEnferEDO) o;

    return preg.sEnfermedadEDO + "&" + preg.sDescEnfermedadEDO + "&" +
        preg.sTipoDeclaracion;
  }

}
