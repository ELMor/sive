package enfedo;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.TextEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import catalogo.CListaCat;
import catalogo.Catalogo;
import catalogo.DataCat;
import sapp.StubSrvBD;

public class Pan_MantEnferEDO
    extends CDialog {

//modos de pantalla
  public final static int modoALTA = 0;

  ResourceBundle res = ResourceBundle.getBundle("enfedo.Res" + app.getIdioma());
  public final static int modoMODIFICAR = 1;
  public final static int modoESPERA = 5;

  // modos de los servlets
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  protected int modoAnterior; //Para volver al modo anterior tras selecci�n c�digo CIE
  protected int modoOperacion = modoALTA;

  //Constantes del panel
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/aceptar.gif",
      "images/cancelar.gif"};

  final String strSERVLET = "servlet/SrvEnfedo";
  final String strSERVLETCIE = "servlet/SrvCat";
  final String strSERVLET_TIPO_VIGILANCIA = "servlet/SrvCat";

  // par�metros
  protected CLista listaChoice = null; //Para recibir items del choice
  protected StubSrvBD stubCliente = new StubSrvBD();
  public boolean bAceptar = false;
  protected CCargadorImagen imgs = null;
  public DataPan_MantEnferEDO pregunta;

  //controles
  XYLayout xYLayout1 = new XYLayout();
  Label lblCodigoCIE = new Label();
  Label lblEnfermedadEDOCNE = new Label();
  Label lblTipoDeclaracion = new Label();
  Label lblEnvios = new Label();
  Checkbox chckbxResumenSemanal = new Checkbox();
  Checkbox chckbxIndivSemanal = new Checkbox();
  Checkbox chckbxIndividualUrgente = new Checkbox();
  Checkbox chckbxResumenAnual = new Checkbox();
  TextField txtDescCodigo = new TextField();
  TextField txtDescEnfermedadEDO = new TextField();
  Choice choiceTipoDeclaracion = new Choice();
  ButtonControl btnCancelar = new ButtonControl();
  TextField txtCodigoCIE = new TextField();
  CCampoCodigo txtEnfermedadEDO = new CCampoCodigo();
  ButtonControl btnCtrlCodigoCIE = new ButtonControl();
  Panel pnl = new Panel();

  Pan_MantEnferEDOTextAdapter textAdapter = new Pan_MantEnferEDOTextAdapter(this);

  Label lblFecha = new Label();
  TextField txtFecha = new TextField();
  Label lblCodOperario = new Label();
  CCampoCodigo txtCodOperario = new CCampoCodigo();
  Checkbox chckbxBorrar = new Checkbox();
  ButtonControl btnAceptar = new ButtonControl();
  XYLayout xYLayout2 = new XYLayout();
  FlowLayout flowLayout1 = new FlowLayout();

  // contructor
  public Pan_MantEnferEDO(CApp a,
                          int modo,
                          DataPan_MantEnferEDO preg) {
    super(a);

    try {

      modoOperacion = modo;
      pregunta = preg;
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:

        // modo alta
        txtEnfermedadEDO.setEnabled(false);
        this.txtCodigoCIE.setEnabled(true);

        //btnCtrlEnfermedadEDO.setEnabled(true);
        btnCtrlCodigoCIE.setEnabled(true);
        choiceTipoDeclaracion.setEnabled(true);
        chckbxResumenSemanal.setEnabled(true);
        chckbxIndivSemanal.setEnabled(true);
        chckbxIndividualUrgente.setEnabled(true);
        chckbxResumenAnual.setEnabled(true);
        chckbxBorrar.setEnabled(false);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:

        // modo modificaci�n y baja
        txtEnfermedadEDO.setEnabled(false);
        this.txtCodigoCIE.setEnabled(false);

        //btnCtrlEnfermedadEDO.setEnabled(true);
        btnCtrlCodigoCIE.setEnabled(false);
        choiceTipoDeclaracion.setEnabled(true);
        chckbxResumenSemanal.setEnabled(true);
        chckbxIndivSemanal.setEnabled(true);
        chckbxIndividualUrgente.setEnabled(true);
        chckbxResumenAnual.setEnabled(true);
        chckbxBorrar.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:

        //Todos deshabilitados : cursor en espera
        txtEnfermedadEDO.setEnabled(true);
        btnCtrlCodigoCIE.setEnabled(false);
        //btnCtrlEnfermedadEDO.setEnabled(false);
        txtDescCodigo.setEnabled(false);
        txtDescEnfermedadEDO.setEnabled(false);
        choiceTipoDeclaracion.setEnabled(false);
        chckbxResumenSemanal.setEnabled(false);
        chckbxIndivSemanal.setEnabled(false);
        chckbxIndividualUrgente.setEnabled(false);
        chckbxResumenAnual.setEnabled(false);
        chckbxBorrar.setEnabled(false);
        txtFecha.setEnabled(false);
        txtCodOperario.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  public void jbInit() throws Exception {
    this.setLayout(flowLayout1);
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    xYLayout1.setWidth(543);
    xYLayout1.setHeight(350);
    setSize(543, 350);

    this.setTitle(res.getString("msg2.Text"));
    lblCodigoCIE.setText(res.getString("lblCodigoCIE.Text"));
    lblEnfermedadEDOCNE.setText(res.getString("lblEnfermedadEDOCNE.Text"));
    lblTipoDeclaracion.setText(res.getString("lblTipoDeclaracion.Text"));
    lblEnvios.setText(res.getString("lblEnvios.Text"));
    chckbxResumenSemanal.setLabel(res.getString("chckbxResumenSemanal.Label"));
    chckbxIndivSemanal.setLabel(res.getString("chckbxIndivSemanal.Label"));
    chckbxIndividualUrgente.setLabel(res.getString(
        "chckbxIndividualUrgente.Label"));
    chckbxResumenAnual.setLabel(res.getString("chckbxResumenAnual.Label"));
    chckbxBorrar.setLabel(res.getString("chckbxBorrar.Label"));
    lblFecha.setText(res.getString("lblFecha.Text"));

    lblCodOperario.setText(res.getString("lblCodOperario.Text"));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));

    btnCtrlCodigoCIE.setImage(imgs.getImage(0));
    btnCtrlCodigoCIE.setActionCommand("BuscarCodigoCIE");
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");

    /*pnl.setFont(new Font("Dialog", 3, 12));
         pnl.setLabel("Mantenimiento de enfermedades EDO");
         pnl.setForeground(new Color(0, 0, 0));*/

    pnl.setLayout(xYLayout1);
    pnl.add(txtCodigoCIE, new XYConstraints(145, 41, 73, -1));
    pnl.add(txtEnfermedadEDO, new XYConstraints(145, 6, 73, -1));
    pnl.add(lblCodigoCIE, new XYConstraints(13, 41, 73, -1));
    pnl.add(lblEnfermedadEDOCNE, new XYConstraints(13, 6, 131, -1));
    pnl.add(lblTipoDeclaracion, new XYConstraints(13, 75, -1, -1));
    pnl.add(lblEnvios, new XYConstraints(13, 110, 258, -1));
    pnl.add(chckbxResumenSemanal, new XYConstraints(292, 110, -1, -1));
    pnl.add(chckbxIndivSemanal, new XYConstraints(292, 138, -1, -1));
    pnl.add(chckbxIndividualUrgente, new XYConstraints(292, 167, -1, -1));
    pnl.add(chckbxResumenAnual, new XYConstraints(292, 195, -1, -1));
    pnl.add(txtDescCodigo, new XYConstraints(271, 41, 260, -1));
    pnl.add(txtDescEnfermedadEDO, new XYConstraints(271, 6, 260, -1));
    pnl.add(choiceTipoDeclaracion, new XYConstraints(145, 74, 357, -1));
    pnl.add(btnCtrlCodigoCIE, new XYConstraints(225, 41, -1, -1));
    pnl.add(lblFecha, new XYConstraints(132, 232, 35, -1));
    pnl.add(txtFecha, new XYConstraints(207, 232, 99, -1));
    pnl.add(lblCodOperario, new XYConstraints(345, 232, 66, -1));
    pnl.add(txtCodOperario, new XYConstraints(451, 232, 75, -1));
    pnl.add(chckbxBorrar, new XYConstraints(13, 232, 90, -1));
    pnl.add(btnAceptar, new XYConstraints(308, 280, 101, 26));
    pnl.add(btnCancelar, new XYConstraints(434, 280, 95, 26));

    this.add(pnl);

    //Color a los campos obligatorios
    txtEnfermedadEDO.setForeground(Color.black);
    txtEnfermedadEDO.setBackground(new Color(255, 255, 150));
    txtCodigoCIE.setForeground(Color.black);
    txtCodigoCIE.setBackground(new Color(255, 255, 150));
    choiceTipoDeclaracion.setForeground(Color.black);
    choiceTipoDeclaracion.setBackground(new Color(255, 255, 150));

    btnAceptar.setImage(imgs.getImage(1));
    btnCancelar.setImage(imgs.getImage(2));
    btnCtrlCodigoCIE.setImage(imgs.getImage(0));

    // Recibimos datos para la lista del choice
    CLista dataChoice = new CLista();
    dataChoice.addElement(new DataCat(""));

    //Establece el idioma en el que se visualizar�n los datos
    dataChoice.setIdioma(this.app.getIdioma());

    //apunta al sevlett auxiliar
    stubCliente.setUrl(new URL(app.getURL() + strSERVLET_TIPO_VIGILANCIA));
    listaChoice = (CLista)this.stubCliente.doPost(servletSELECCION_X_CODIGO +
                                                  Catalogo.catTVIGILANCIA,
                                                  dataChoice);

    txtCodigoCIE.addFocusListener(new
                                  Pan_MantEnferEDO_txtCodigoCIE_focusAdapter(this));
    txtCodigoCIE.addKeyListener(new Pan_MantEnferEDO_txtCodigoCIE_keyAdapter(this));
    btnCtrlCodigoCIE.addActionListener(new
        Pan_MantEnferEDO_btnCtrlCodigoCIE_actionAdapter(this));
    btnAceptar.addActionListener(new Pan_MantEnferEDO_btnAceptar_actionAdapter(this));
    btnCancelar.addActionListener(new
                                  Pan_MantEnferEDO_btnCancelar_actionAdapter(this));
    txtEnfermedadEDO.addTextListener(textAdapter);

    choiceTipoDeclaracion.add("");
    // vuelca la lista
    for (int j = 0; j < listaChoice.size(); ++j) {
      DataCat elemLista;
      elemLista = (DataCat) listaChoice.elementAt(j);
      // Ponemos la descripci�n , bien sea en idioma local o en idioma por defecto
      if ( (this.app.getIdioma() != CApp.idiomaPORDEFECTO) &&
          (elemLista.getDesL().length() > 0)) {
        choiceTipoDeclaracion.add(elemLista.getDesL());
      }
      else {
        choiceTipoDeclaracion.add(elemLista.getDes());
      }
    }

    //Controles con estado fijo
    txtCodigoCIE.setEnabled(false); //Cod CIE se trae siempre de b.datos
    //Las descripciones como no editables en vez de no habilitadas para que se vean grises
    txtDescEnfermedadEDO.setEditable(false);
    txtDescCodigo.setEditable(false);
    txtFecha.setEditable(false);
    txtCodOperario.setEditable(false);

    Inicializar();

    if (pregunta != null) {
      if (modoOperacion == modoMODIFICAR) { // modo modificar
//# // System_out.println("Entra en modo modificar************");
        modoOperacion = modoESPERA;
        Inicializar();
        txtCodigoCIE.setText(pregunta.sCodigoCIE);
        txtCodOperario.setText(pregunta.sCodOperario);
        txtDescCodigo.setText(pregunta.sDescCodigo);
        txtDescEnfermedadEDO.setText(pregunta.sDescEnfermedadEDO);
        txtEnfermedadEDO.setText(pregunta.sEnfermedadEDO);
        txtFecha.setText(pregunta.sFecha);
        boolean encontrado = false;
        for (int j = 0; (j < listaChoice.size()) && (encontrado == false); ++j) {
          DataCat elemLista;
          elemLista = (DataCat) listaChoice.elementAt(j);
          if ( (elemLista.getCod()).equals(pregunta.sTipoDeclaracion)) {
            encontrado = true;
            choiceTipoDeclaracion.select(j + 1);
          }
        }

        if (pregunta.bIndividualUrgente) {
          chckbxIndividualUrgente.setState(true);
        }
        else {
          chckbxIndividualUrgente.setState(false);

        }
        if (pregunta.bIndivSemanal) {
          chckbxIndivSemanal.setState(true);
        }
        else {
          chckbxIndivSemanal.setState(false);

        }
        if (pregunta.bResumenAnual) {
          chckbxResumenAnual.setState(true);
        }
        else {
          chckbxResumenAnual.setState(false);

        }
        if (pregunta.bResumenSemanal) {
          chckbxResumenSemanal.setState(true);
        }
        else {
          chckbxResumenSemanal.setState(false);

        }
        if (pregunta.bBorrar) {
          chckbxBorrar.setState(true);
        }
        else {
          chckbxBorrar.setState(false);

        }
        modoOperacion = modoMODIFICAR;
        Inicializar();
      }
      else { // modo alta
//# // System_out.println("Entra en modo ALTA**************");
        modoOperacion = this.modoESPERA;
        Inicializar();
        txtEnfermedadEDO.setText(pregunta.sEnfermedadEDO);
        txtDescEnfermedadEDO.setText(pregunta.sDescEnfermedadEDO);
        modoOperacion = modoALTA;
        Inicializar();
      }
    }
  }

  void vaciarTextoPantalla() {
    txtCodigoCIE.setText("");
    txtEnfermedadEDO.setText("");
    txtDescCodigo.setText("");
    txtDescEnfermedadEDO.setText("");
    txtCodOperario.setText("");
    txtFecha.setText("");
    choiceTipoDeclaracion.select(0); //La posici�n que contiene el blanco
    chckbxResumenSemanal.setState(false);
    chckbxIndivSemanal.setState(false);
    chckbxIndividualUrgente.setState(false);
    chckbxResumenAnual.setState(false);
    chckbxBorrar.setState(false);
  }

//****************Tratamiento de eventos*******************************************
//******************************************************************************

    void btnCtrlCodigoCIE_actionPerformed(ActionEvent evt) {

      DataCat data;
      int modo = modoOperacion;

      modoOperacion = modoESPERA;
      Inicializar();

      CListaCat lista = new CListaCat(app,
                                      res.getString("msg3.Text"),
                                      stubCliente,
                                      strSERVLETCIE,
                                      servletOBTENER_X_CODIGO +
                                      Catalogo.catPROCESOS,
                                      servletOBTENER_X_DESCRIPCION +
                                      Catalogo.catPROCESOS,
                                      servletSELECCION_X_CODIGO +
                                      Catalogo.catPROCESOS,
                                      servletSELECCION_X_DESCRIPCION +
                                      Catalogo.catPROCESOS);
      lista.show();
      data = (DataCat) lista.getComponente();

      if (data != null) {
        txtCodigoCIE.setText(data.getCod());
        txtDescCodigo.setText(data.getDes());

        // cambia al idioma local, si procede
        if ( (this.app.getIdioma() != CApp.idiomaPORDEFECTO) &&
            (data.getDesL().length() > 0)) {
          txtDescCodigo.setText(data.getDesL());
        }
      }

      modoOperacion = modo;
      Inicializar();
    }

  // seleccionar enfermedad EDO
  void btnCtrlEnfermedadEDO_actionPerformed(ActionEvent evt) {
    /*
      public CListaEnfEDO(CApp a,
                          String title,
                          StubSrvBD stub,
                          String servlet,
                          int obtener_x_codigo,
                          int obtener_x_descricpcion,
                          int seleccion_x_codigo,
                          int seleccion_x_descripcion) {
     */

    DataPan_MantEnferEDO datosPantalla;
    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    Inicializar();

    CListaEnfEDO lista = new CListaEnfEDO(app,
                                          res.getString("msg4.Text"),
                                          stubCliente,
                                          strSERVLET,
                                          servletOBTENER_X_CODIGO,
                                          servletOBTENER_X_DESCRIPCION,
                                          servletSELECCION_X_CODIGO,
                                          servletSELECCION_X_DESCRIPCION);
    lista.show();
    datosPantalla = (DataPan_MantEnferEDO) lista.getComponente();

    if (datosPantalla != null) {

      if (datosPantalla.bEstaEnTabla == true) { //metemos todos los datos

        String strTipoDeclaracion = datosPantalla.sTipoDeclaracion;
        boolean encontrado = false;
        for (int j = 0; (j < listaChoice.size()) && (encontrado == false); ++j) {
          DataCat elemLista;
          elemLista = (DataCat) listaChoice.elementAt(j);
          if ( (elemLista.getCod()).equals(datosPantalla.sTipoDeclaracion)) {
            encontrado = true;
            choiceTipoDeclaracion.select(j + 1);
          }
        }

        txtEnfermedadEDO.setText(datosPantalla.sEnfermedadEDO);
        txtDescEnfermedadEDO.setText(datosPantalla.sDescEnfermedadEDO);
        txtCodigoCIE.setText(datosPantalla.sCodigoCIE);
        txtDescCodigo.setText(datosPantalla.sDescCodigo);
        chckbxResumenSemanal.setState(datosPantalla.bResumenSemanal);
        chckbxIndivSemanal.setState(datosPantalla.bIndivSemanal);
        chckbxIndividualUrgente.setState(datosPantalla.bIndividualUrgente);
        chckbxResumenAnual.setState(datosPantalla.bResumenAnual);
        chckbxBorrar.setState(datosPantalla.bBorrar);
        txtFecha.setText(datosPantalla.sFecha);
        txtCodOperario.setText(datosPantalla.sCodOperario);
        this.modoOperacion = modoMODIFICAR;
      }
      else { //metemos s�lo el codigo y la descripci�n
        vaciarTextoPantalla();
        txtEnfermedadEDO.setText(datosPantalla.sEnfermedadEDO);
        txtDescEnfermedadEDO.setText(datosPantalla.sDescEnfermedadEDO);
        this.modoOperacion = modoALTA;
      } //else

    }
    else {
      modoOperacion = modo;
    }

    Inicializar();
  }

  void Modificar() {
    CMessage msgBox = null;
    CLista data = null;

    if (this.isDataValid()) {

      // modifica la enfermedad
      try {
        //modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        //Tipo de declaraci�n
        int indiceLista = (choiceTipoDeclaracion.getSelectedIndex() - 1);
        String codListaChoice = ( (DataCat) (listaChoice.elementAt(indiceLista))).
            getCod();

        //Resto de par�metros
        data = new CLista();
        pregunta = new DataPan_MantEnferEDO(true, txtEnfermedadEDO.getText(),
                                            txtDescEnfermedadEDO.getText(),
                                            txtCodigoCIE.getText(),
                                            txtDescCodigo.getText(),
                                            codListaChoice,
                                            chckbxResumenSemanal.getState(),
                                            chckbxIndivSemanal.getState(),
                                            chckbxIndividualUrgente.getState(),
                                            chckbxResumenAnual.getState(),
                                            chckbxBorrar.getState(), "", "");

        data.addElement(pregunta);
        //usuario
        data.setLogin(app.getLogin());

        //apunta al servlettt principal
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
        this.stubCliente.doPost(servletMODIFICAR, data);
        dispose();
        // error en el proceso
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

      this.modoOperacion = modoMODIFICAR;
      Inicializar();
    }

  }

  // obtener una lista de valores
  void focusLost() {

    CMessage msgBox = null;
    DataPan_MantEnferEDO datEnferEDO = null;
    CLista datos = null;
    String sDes;
    int modoAnterior;

    modoAnterior = modoOperacion;

    if (txtEnfermedadEDO.getText().length() > 0) {

      try {

        // par�metros
        datos = new CLista();
        datos.setIdioma(app.getIdioma());
        datos.addElement(new DataPan_MantEnferEDO(false,
                                                  txtEnfermedadEDO.getText(),
                                                  "", "", "", "", false, false, false, false,
                                                  false, "", ""));

        // modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET));
        datos = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO, datos);

        //determina el modo del applet
        if (datos.size() > 0) {
          // escribe la descripci�n de la lista
          datEnferEDO = (DataPan_MantEnferEDO) datos.firstElement();
          sDes = datEnferEDO.sDescEnfermedadEDO;
          /*if ((app.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (datEnferEDO.getDsllista().length() > 0))
            sDes = datMantlis.getDsllista();*/
          txtDescEnfermedadEDO.setText(sDes);
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg5.Text"));
          msgBox.show();
          msgBox = null;
        }
      } //try
      catch (Exception ex) {
        ex.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
        msgBox.show();
        msgBox = null;
      }

      modoOperacion = modoAnterior;
      Inicializar();
    }
  }

  void A�adir() {
    CMessage msgBox = null;
    CLista data = null;

    //determina si los datos est�n completos
    if (this.isDataValid()) {

      try {

        //modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        //prepara los par�metros de env�o

        //Tipo de declaraci�n
        int indiceLista = (choiceTipoDeclaracion.getSelectedIndex() - 1);
        String codListaChoice = ( (DataCat) (listaChoice.elementAt(indiceLista))).
            getCod();

        //Resto de par�metros
        data = new CLista();
        pregunta = new DataPan_MantEnferEDO(true, txtEnfermedadEDO.getText(),
                                            txtDescEnfermedadEDO.getText(),
                                            txtCodigoCIE.getText(),
                                            txtDescCodigo.getText(),
                                            codListaChoice,
                                            chckbxResumenSemanal.getState(),
                                            chckbxIndivSemanal.getState(),
                                            chckbxIndividualUrgente.getState(),
                                            chckbxResumenAnual.getState(),
                                            chckbxBorrar.getState(), "", "");

        data.addElement(pregunta);
        //usuario
        data.setLogin(app.getLogin());

        //apunta la servlett principal
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
        this.stubCliente.doPost(servletALTA, data);

        // limpia los datos
        vaciarTextoPantalla();
        dispose();
        // error en el proceso
      }
      catch (Exception e) {

        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
      this.modoOperacion = modoALTA;
      Inicializar();
    }

  }

  void txtEnfermedadEDO_textValueChanged(TextEvent e) {
    if (this.modoOperacion == modoMODIFICAR) {
      vaciarTextoPantalla();
      this.modoOperacion = modoALTA;
      Inicializar();
    }

  }

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;

    boolean b = false;

    // comprueba que esten informados los campos obligatorios
    if (
        (txtCodigoCIE.getText().length() > 0) &&
        (txtDescCodigo.getText().length() > 0)
        && (txtEnfermedadEDO.getText().length() > 0) &&
        (txtDescEnfermedadEDO.getText().length() > 0)
        && (choiceTipoDeclaracion.getSelectedItem().length() > 0)

        ) {
      b = true;

      // comprueba la longitud m�xima de los campos
      if (txtEnfermedadEDO.getText().length() > 3) {
        b = false;
        msg = res.getString("msg6.Text");
        txtEnfermedadEDO.selectAll();

      }

      if (txtCodigoCIE.getText().length() > 6) {
        b = false;
        msg = res.getString("msg6.Text");
        txtCodigoCIE.selectAll();
      }

      // advertencia de requerir datos
    }
    else {
      msg = res.getString("msg7.Text");
    }

    if (!b) {
      msgBox = new CMessage(app, CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }
    bAceptar = b;
    return b;
  }

  /*
    // comprueba que los datos sean v�lidos
    protected boolean isDataValid() {
      CMessage msgBox;
      boolean b = false;
      // comprueba que esten informados los campos obligatorios
      if (
       (txtCodigoCIE.getText().length() > 0) && (txtDescCodigo.getText().length() > 0)
        && (txtEnfermedadEDO.getText().length() > 0) && (txtDescEnfermedadEDO.getText().length() > 0)
        && (choiceTipoDeclaracion.getSelectedItem().length() >0)
        ) {
        b = true;
      // advertencia de requerir datos
      } else {
       msgBox = new CMessage(app,CMessage.msgAVISO, "Faltan campos obligatorios.");
        msgBox.show();
        msgBox = null;
      }
      return b;
    }
   */

  void txtCodigoCIE_keyPressed(KeyEvent e) {
    this.txtDescCodigo.setText("");
  }

  void txtCodigoCIE_focusLost(FocusEvent e) {

    // datos de envio
    DataCat data;
    CLista param = null;
    CMessage msg;
    String sDes1;
    int modo = modoOperacion;

    if (txtCodigoCIE.getText().length() > 0) {

      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();

      try {
        param = new CLista();
        param.setIdioma(app.getIdioma());
        param.addElement(new DataCat(txtCodigoCIE.getText().toUpperCase()));

        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strSERVLETCIE));
        param = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO +
                                            Catalogo.catPROCESOS, param);

        // rellena los datos
        if (param.size() > 0) {
          data = (DataCat) param.elementAt(0);

          txtCodigoCIE.setText(data.getCod());
          sDes1 = data.getDes();

          txtDescCodigo.setText(sDes1);

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg8.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
    // modo normal
    modoOperacion = modo;
    Inicializar();
  }
} //Fin clase

/*********************************************************************/
/*********************************************************************/

/*
// action listener de evento en bot�nes
 class Pan_MantEnferEDOActionListener implements ActionListener, Runnable {
  Pan_MantEnferEDO adaptee = null;
  ActionEvent e = null;
  public Pan_MantEnferEDOActionListener(Pan_MantEnferEDO adaptee) {
    this.adaptee = adaptee;
  }
  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }
  // hilo de ejecuci�n para servir el evento
  public void run() {
    // DESactiva el control de cambio del codigo auxiliar
    adaptee.txtEnfermedadEDO.removeTextListener(adaptee.textAdapter);
    if (e.getActionCommand() == "A�adir") // a�adir
      adaptee.btnAceptar_actionPerformed(e);
    else if (e.getActionCommand() == "Modificar") // modificar
      adaptee.btnCancelar_actionPerformed(e);
    else if (e.getActionCommand() == "BuscarCodigoCIE") // buscar c�d. CIE
      adaptee.btnCtrlCodigoCIE_actionPerformed(e);
    else if (e.getActionCommand() == "BuscarEnfermedadEDO") // buscar c�d. CIE
      adaptee.btnCtrlEnfermedadEDO_actionPerformed(e);
    // activa el control de cambio del codigo auxiliar
    adaptee.txtEnfermedadEDO.addTextListener(adaptee.textAdapter);
  }
 }
 */

// cambio del c�digo auxiliar
class Pan_MantEnferEDOTextAdapter
    implements java.awt.event.TextListener {
  Pan_MantEnferEDO adaptee;

  Pan_MantEnferEDOTextAdapter(Pan_MantEnferEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void textValueChanged(TextEvent e) {
    // DESactiva el control de cambio del codigo auxiliar
    adaptee.txtEnfermedadEDO.removeTextListener(adaptee.textAdapter);

    adaptee.txtEnfermedadEDO_textValueChanged(e);
    // activa el control de cambio del codigo auxiliar
    adaptee.txtEnfermedadEDO.addTextListener(adaptee.textAdapter);

  }
}

// lista de valores para EDO
class CListaEnfEDO
    extends CListaValores {

  public CListaEnfEDO(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataPan_MantEnferEDO(false, s, "", "", "", "", false, false, false, false, false,
                                    "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataPan_MantEnferEDO) o).sEnfermedadEDO);
  }

  public String getDescripcion(Object o) {
    return ( ( (DataPan_MantEnferEDO) o).sDescEnfermedadEDO);
  }
}

class Pan_MantEnferEDO_btnCancelar_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_MantEnferEDO adaptee = null;
  ActionEvent e = null;

  Pan_MantEnferEDO_btnCancelar_actionAdapter(Pan_MantEnferEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.bAceptar = false;
    adaptee.dispose();

  }
}

class Pan_MantEnferEDO_btnAceptar_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_MantEnferEDO adaptee = null;
  ActionEvent e = null;

  Pan_MantEnferEDO_btnAceptar_actionAdapter(Pan_MantEnferEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.bAceptar = true;

    // realiza la operaci�n
    switch (adaptee.modoOperacion) {
      case Pan_MantEnferEDO.modoALTA:
        adaptee.A�adir();
        break;
      case Pan_MantEnferEDO.modoMODIFICAR:
        adaptee.Modificar();
        break;

    }

  }
}

class Pan_MantEnferEDO_btnCtrlCodigoCIE_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_MantEnferEDO adaptee = null;
  ActionEvent e = null;

  Pan_MantEnferEDO_btnCtrlCodigoCIE_actionAdapter(Pan_MantEnferEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    adaptee.btnCtrlCodigoCIE_actionPerformed(e);

  }
}

class Pan_MantEnferEDO_txtCodigoCIE_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_MantEnferEDO adaptee;

  Pan_MantEnferEDO_txtCodigoCIE_keyAdapter(Pan_MantEnferEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCodigoCIE_keyPressed(e);
  }
}

class Pan_MantEnferEDO_txtCodigoCIE_focusAdapter
    extends java.awt.event.FocusAdapter {
  Pan_MantEnferEDO adaptee;

  Pan_MantEnferEDO_txtCodigoCIE_focusAdapter(Pan_MantEnferEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.txtCodigoCIE_focusLost(e);
  }
}
