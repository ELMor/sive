package alarmas;

import java.util.ResourceBundle;

import capp.CApp;
import comun.constantes;

public class ApAlarmas
    extends CApp {

  // constantes del panel
//  final String strSERVLET = "servlet/SrvInd";
  final String strSERVLET = constantes.strSERVLET_IND;

  ResourceBundle res;

  // modos de operación del servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  public ApAlarmas() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("alarmas.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    VerPanel("", new CListaMantAlarmas(this));
  }
}
