package alarmas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

//obtiene las enfermedades centinelas
//y las descripciones de Proceso
public class SrvEnfCenti
    extends DBServlet {

  // modos de operaci�n
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 1;

    // campos
    String sCod = "";
    String sDes = "";
    String sDesL = "";

    // objetos de datos
    CLista data = new CLista();
    DataEnfCenti Enfcenti = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    Enfcenti = (DataEnfCenti) param.firstElement();

    // modos de operaci�n
    switch (opmode) {

      // b�squeda de enfermedad (like)*****************************************
      case servletSELECCION_X_CODIGO:
      case servletSELECCION_X_DESCRIPCION:

        query = "select a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO "
            + " from SIVE_ENF_CENTI a, SIVE_PROCESOS b "
            + " where  a.CD_ENFCIE = b.CD_ENFCIE and ";

        // peticion de trama
        if (param.getFilter().length() > 0) {

          if (opmode == servletSELECCION_X_CODIGO) {
            query = query +
                " a.CD_ENFCIE like ? and a.CD_ENFCIE > ? order by a.CD_ENFCIE";

          }
          else {
            if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
              query = query +
                  " b.DSL_PROCESO like ? and a.CD_ENFCIE > ? order by a.CD_ENFCIE";
            }
            else {
              query = query +
                  " b.DS_PROCESO like ? and a.CD_ENFCIE > ? order by a.CD_ENFCIE";

            }
          }
          // peticion de la primera trama
        }
        else {
          if (opmode == servletSELECCION_X_CODIGO) {
            query = query + "a.CD_ENFCIE like ? order by a.CD_ENFCIE";
          }
          else {
            if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
              query = query + " b.DSL_PROCESO like ? order by a.CD_ENFCIE";
            }
            else {
              query = query + " b.DS_PROCESO like ? order by a.CD_ENFCIE";
            }
          }
        }

        // prepara la lista de resultados
        data = new CLista();
        DataEnfCenti datosBusqueda;

        // lanza la query
        st = con.prepareStatement(query);
        // caja de texto
        st.setString(1, Enfcenti.getCod().trim() + "%");
        // paginaci�n
        if (param.getFilter().length() > 0) {
          st.setString(2, param.getFilter().trim());
        }
        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {
          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataEnfCenti) data.lastElement()).getCod());
            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          String desPro = rs.getString("DS_PROCESO");
          String desLPro = rs.getString("DSL_PROCESO");

          // a�ade un nodo (esta condicion es generica)
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (desLPro != null)) {
            sDes = desLPro;
          }
          else {
            sDes = desPro;

          }
          sCod = rs.getString("CD_ENFCIE");

          datosBusqueda = new DataEnfCenti(sCod, sDes);
          data.addElement(datosBusqueda);
          i++;
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

        // obtener la enfermedad (select)*****************************************
      case servletOBTENER_X_CODIGO:
      case servletOBTENER_X_DESCRIPCION:

        query = "select a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO "
            + " from SIVE_ENF_CENTI a, SIVE_PROCESOS b "
            + " where  a.CD_ENFCIE = b.CD_ENFCIE and ";

        // prepara la query
        if (opmode == servletOBTENER_X_CODIGO) {
          query = query + " a.CD_ENFCIE = ?";
        }
        else {
          if (param.getIdioma() != CApp.idiomaPORDEFECTO) { //local
            query = query + " b.DSL_PROCESO = ? order by a.CD_ENFCIE";
          }
          else {
            query = query + " b.DS_PROCESO = ? order by a.CD_ENFCIE";
          }
        }

        // prepara la lista de resultados
        data = new CLista();
        DataEnfCenti datosBusquedaUnica;

        // lanza la query
        st = con.prepareStatement(query);
        //caja de texto de la lista de valores
        st.setString(1, Enfcenti.getCod().trim());
        rs = st.executeQuery();

        // extrae los registros encontrados
        while (rs.next()) {

          String desPro = rs.getString("DS_PROCESO");
          String desLPro = rs.getString("DSL_PROCESO");

          // a�ade un nodo (esta condicion es generica)
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (desLPro != null)) {
            sDes = desLPro;
          }
          else {
            sDes = desPro;

          }
          sCod = rs.getString("CD_ENFCIE");

          datosBusquedaUnica = new DataEnfCenti(sCod, sDes);
          data.addElement(datosBusquedaUnica);
        } //fin del while
        rs.close();
        rs = null;
        st.close();
        st = null;

        break;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    data.trimToSize();
    return data;
  }
}