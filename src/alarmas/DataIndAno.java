package alarmas;

import java.io.Serializable;

public class DataIndAno
    implements Serializable {
  protected String sCod = "";
  protected String sDes = "";
  protected String sAno = "";

  public DataIndAno() {
  }

  public DataIndAno(String cod) {
    sCod = cod;
  }

  public DataIndAno(String cod, String des, String ano) {
    sCod = cod;
    sDes = des;
    sAno = ano;
  }

  public String getCod() {
    return sCod;
  }

  public String getDes() {
    return sDes;
  }

  public String getAno() {
    return sAno;
  }
}
