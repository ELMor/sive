
package alarmas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Enumeration;

import capp.CLista;
import sapp.DBServlet;

//sobre las TABLAS: SIVE_IND_ANO Y SIVE_ALARMA

public class SrvAlar
    extends DBServlet {

  // modos de operaci�n
  final int modoMODIFICAR_MAXIVO = 0; //COMPROBAR SI TODAS LINEAS VACIAS--> TABLA COEF=VACIO
  final int modoSELECT_ALAR = 3;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    //Nombres de tablas parametrizados seg�n la aplicaci�n
    //Por defecto toman nombres de EDO
    String sTablaIndAno = "SIVE_IND_ANO";
    String sTablaAlarma = "SIVE_ALARMA";
    String sTablaSemana = "SIVE_SEMANA_EPI";

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    CLista data = null;
    CLista listaValoresPantalla = null;
    DataAlarma datosConsulta = null;
    DataAlarma datosActualizacion = null;
    DataAlarma datUnaLinea = null;

    String sMaxSem = "";

    /*
        final String sBUSQUEDA ="select "
        + "a.CD_INDALAR, a.CD_ANOEPI, a.NM_COEF, "
        + "b.CD_SEMEPI, b.NM_VALOR "
        + "from "+ sTablaAlarma +" b, "+ sTablaIndAno +" a "
        + "where "
        + "a.CD_INDALAR = b.CD_INDALAR and "
        + "a.CD_ANOEPI = b.CD_ANOEPI and "
        + "a.CD_INDALAR = ? and a.CD_ANOEPI = ? ORDER BY CD_SEMEPI";
        final String sBORRAR_DETALLE ="DELETE FROM "+ sTablaAlarma +" "
                + "WHERE (CD_INDALAR = ? AND CD_ANOEPI=?) ";
        final String sBORRAR_PADRE ="DELETE FROM "+ sTablaIndAno +" "
                + "WHERE (CD_INDALAR = ? AND CD_ANOEPI=?) ";
        final String sALTA_PADRE ="insert into "+ sTablaIndAno +" "
                + "(CD_INDALAR, CD_ANOEPI, NM_COEF )"
                + "values (?, ?, ?) ";
        final String sALTA_DETALLE ="insert into "+ sTablaAlarma +" "
                + "(CD_INDALAR, CD_ANOEPI, CD_SEMEPI, NM_VALOR)"
                + "values (?, ?, ?, ?) ";
        final String sBUSQUEDA_SEM_ULTIMA = "select MAX(CD_SEMEPI) FROM "+sTablaSemana+" where CD_ANOEPI = ?";
     */
    // campos
    String sCodInd = "";
    String sAno = "";
    String sSem = "";
    float fValor = 0;
    float fCoef = 0;

    String sCodUltSem = "";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    //Segun el tipo SIVE utilizado tendremos una aplicaci�n distinta
    // y por tanto se utilizar�n tablas distintas
    String sTSive = param.getTSive();
    //Aplicaci�n centinelas
    if (sTSive.equals("C")) {
      sTablaIndAno = "SIVE_IND_ALARRMC_ANO";
      sTablaAlarma = "SIVE_ALARMA_RMC";
      sTablaSemana = "SIVE_SEMANA_EPI_RMC";
    }

    //Ya sabemos los nombres de las tablas: Se forman lo s Strings de las querys
    final String sBUSQUEDA = "select "
        + "a.CD_INDALAR, a.CD_ANOEPI, a.NM_COEF, "
        + "b.CD_SEMEPI, b.NM_VALOR "
        + "from " + sTablaAlarma + " b, " + sTablaIndAno + " a "
        + "where "
        + "a.CD_INDALAR = b.CD_INDALAR and "
        + "a.CD_ANOEPI = b.CD_ANOEPI and "
        + "a.CD_INDALAR = ? and a.CD_ANOEPI = ? ORDER BY b.CD_SEMEPI";

    final String sBORRAR_DETALLE = "DELETE FROM " + sTablaAlarma + " "
        + "WHERE (CD_INDALAR = ? AND CD_ANOEPI=?) ";
    final String sBORRAR_PADRE = "DELETE FROM " + sTablaIndAno + " "
        + "WHERE (CD_INDALAR = ? AND CD_ANOEPI=?) ";

    final String sALTA_PADRE = "insert into " + sTablaIndAno + " "
        + "(CD_INDALAR, CD_ANOEPI, NM_COEF )"
        + "values (?, ?, ?) ";
    final String sALTA_DETALLE = "insert into " + sTablaAlarma + " "
        + "(CD_INDALAR, CD_ANOEPI, CD_SEMEPI, NM_VALOR)"
        + "values (?, ?, ?, ?) ";

    final String sBUSQUEDA_SEM_ULTIMA = "select MAX(CD_SEMEPI) as MAXSEM FROM " +
        sTablaSemana + " where CD_ANOEPI = ?";

    try {

      // modos de operaci�n
      switch (opmode) {

        //SELECT******************************************
        case modoSELECT_ALAR:

          //cargamos ind, con que ha llegado en param
          datosConsulta = (DataAlarma) param.firstElement(); //un elemento

          data = new CLista();
          DataAlarma datosPantalla;

          // lanza la query
          st = con.prepareStatement(sBUSQUEDA_SEM_ULTIMA);

          st.setString(1, datosConsulta.getAno().trim());

          rs = st.executeQuery();

          // extrae el registro encontrado
          while (rs.next()) {

            sCodUltSem = rs.getString("MAXSEM");
          } //fin del while

          if (sCodUltSem == null) {
            sCodUltSem = "";

          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //Si existe ese a�o en b. datos va a por las alarmas de ese a�o
          if (! (sCodUltSem.equals(""))) {

            //# System_Out.println("NM_COEF... existe a�o");

            // lanza la query
            st = con.prepareStatement(sBUSQUEDA);
            st.setString(1, datosConsulta.getCodInd().trim());
            st.setString(2, datosConsulta.getAno().trim());

            rs = st.executeQuery();

            //# System_Out.println("sBUSQUEDA " +sBUSQUEDA);

            // extrae el registro encontrado
            while (rs.next()) {

              sCodInd = rs.getString("CD_INDALAR");
              sAno = rs.getString("CD_ANOEPI");
              sSem = rs.getString("CD_SEMEPI");
              fValor = rs.getFloat("NM_VALOR");
              fCoef = rs.getFloat("NM_COEF");
              //# System_Out.println("NM_COEF " +fCoef);

              // a�ade un nodo
              datosPantalla = new DataAlarma(sCodInd, sAno,
                                             sSem, new Float(fValor),
                                             new Float(fCoef));

              data.addElement(datosPantalla);
              i++;
            } //fin del while

            rs.close();
            rs = null;
            st.close();
            st = null;

            //Si no habia datos se mete un elemento con el num de semanas
            // y se indica en el que no hay datos de ese a�o
            if (data.size() == 0) {
              //# System_Out.println("NM_COEF... data cero");

              // a�ade un nodo
              datosPantalla = new DataAlarma();
              datosPantalla.setCodUltSem(sCodUltSem);
              datosPantalla.setHayDatos(false);
              data.addElement(datosPantalla);
            }
            //Si habia datos de ese a�o se indica primer elemento del vector
            else {
              //# System_Out.println("NM_COEF... data NO cero");

              ( (DataAlarma) (data.firstElement())).setHayDatos(true);
              ( (DataAlarma) (data.firstElement())).setCodUltSem(sCodUltSem);
            }
          } //fin del if

          //Si no hay a�o epid. generado
          else {
            //# System_Out.println("NM_COEF... No existe a�o");

            datosPantalla = new DataAlarma();
            datosPantalla.setCodUltSem("");
            datosPantalla.setHayDatos(false);
            data.addElement(datosPantalla);
          }

          break;

          //borra de detalle, borra de padre,
          //inserta en padre e inserta en detalle
        case modoMODIFICAR_MAXIVO:

          listaValoresPantalla = (CLista) param; //lo que me ha llegado
          datosActualizacion = (DataAlarma) listaValoresPantalla.firstElement(); //cargo uno
          //(siempre hay al menos un elemento)
          //detalle
          st = con.prepareStatement(sBORRAR_DETALLE);
          st.setString(1, datosActualizacion.getCodInd().trim());
          st.setString(2, datosActualizacion.getAno().trim());
          st.executeUpdate();
          st.close();
          st = null;

          //padre
          st = con.prepareStatement(sBORRAR_PADRE);
          st.setString(1, datosActualizacion.getCodInd().trim());
          st.setString(2, datosActualizacion.getAno().trim());
          st.executeUpdate();
          st.close();
          st = null;

          //NO es listaVacia con solo Ind y Ano --> A�ADO,
          //SI listaVacia, --> NO A�ADO NADA
          datUnaLinea = (DataAlarma) listaValoresPantalla.firstElement(); //cargo uno
          if (datosActualizacion.getSem() != null) {

            //insertamos en la tabla padre (un unico registro)
            st = con.prepareStatement(sALTA_PADRE);
            st.setString(1, datUnaLinea.getCodInd().trim());
            st.setString(2, datUnaLinea.getAno().trim());
            st.setFloat(3, datUnaLinea.getCoef().floatValue());
            st.executeUpdate();
            st.close();
            st = null;

            datUnaLinea = null;

            //insertamos en la tabla detalle
            listaValoresPantalla = (CLista) param;
            Enumeration enumDetalle = listaValoresPantalla.elements(); //again
            datUnaLinea = (DataAlarma) listaValoresPantalla.firstElement();

            while (enumDetalle.hasMoreElements()) {
              //Recogemos los datos de una l�nea
              datUnaLinea = (DataAlarma) (enumDetalle.nextElement());
              st = con.prepareStatement(sALTA_DETALLE);
              st.setString(1, datUnaLinea.getCodInd().trim());
              st.setString(2, datUnaLinea.getAno().trim());
              st.setString(3, datUnaLinea.getSem().trim());
              st.setFloat(4, datUnaLinea.getValor().floatValue());
              st.executeUpdate();
              st.close();
              st = null;
            }

          } //fin de lista vacia
          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();

      data = null; //para comprobar que la transaccion ha ido mal!
      throw ex;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }
}
