/*
 Parametrizado para servir en EDO y en centinelas
 Pero hay que cambiar las sentencias import
 */
package alarmas;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import catalogo.DataCat;
import comun.CListaNiveles1; //Cambiado para ICM99
import comun.DataEntradaEDO; //Cambiado para ICM99 Clases en comun
import comun.constantes;
import enfcentinela.DataEnfCenti;
import sapp.StubSrvBD;

public class Dial_Entrada
    extends CDialog {

  public boolean valido = false;
  ResourceBundle res = ResourceBundle.getBundle("alarmas.Res" + app.getIdioma());

  //Guarda el c�digo generado en servidor
  public String codAnadido = "";

  //modos de operaci�n de la ventana
  final static int modoALTA = 0;
  final static int modoMODIFICAR = 1;
  final static int modoBAJA = 2;
  final static int modoMODIFICAR_A = 5;
  final static int modoESPERA = 3;
  final static int modoENTRADA = 4;

  //AIC
  /*public final int servletSELECCION_ENFERMEDADES_X_CODIGO = 5;
     public final int servletSELECCION_ENFERMEDADES_X_DESCRIPICION = 6;
     public final int servletOBTENCION_ENFERMEDADES_X_CODIGO = 7;
     public final int servletOBTENCION_ENFERMEDADES_X_DESCRIPICION = 8;*/

  static final int servletSELECCION_ALARMAS_X_CODIGO = 300;
  static final int servletOBTENER_ALARMAS_X_DESCRIPCION = 303;
  static final int servletSELECCION_ALARMAS_X_DESCRIPCION = 301;
  static final int servletOBTENER_ALARMAS_X_CODIGO = 302;

  //modos servlets
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  /*
    //Modos op servlet Entrada EDO
    final String strSERVLET_NIVELES = "servlet/SrvEntradaEDO";
    final String strSERVLET_IND = "servlet/SrvInd";
    final String strSERVLET_ENF = "servlet/SrvEnfermedad";
    final String strSERVLET_NIV2 = "servlet/SrvZBS2";
    //Para centinelas
    final String strSERVLET_ENF_CENTI = "servlet/SrvEnfCenti";
   */

  //Modos op servlet Entrada EDO, estan en paquete de constantes
  final String strSERVLET_NIVELES = constantes.strSERVLET_NIV;
  final String strSERVLET_IND = constantes.strSERVLET_IND;
//  final String strSERVLET_ENF = constantes.strSERVLET_ENF;

  final String strSERVLET_ENF = "servlet/SrvEnfermedad";
  //final String strSERVLET_ENF = "servlet/SrvEnferedo";

  final String strSERVLET_NIV2 = constantes.strSERVLET_NIV2;
  //Para buscar enf. centinelas
  final String strSERVLET_ENF_CENTI = constantes.strSERVLET_ENF_CENTI;

  // Consultas de servlet EntradaEdo `paq notificaciones)
  final int servletOBTENER_NIV1_X_CODIGO = 3;
  final int servletOBTENER_NIV1_X_DESCRIPCION = 4;
  final int servletSELECCION_NIV1_X_CODIGO = 5;
  final int servletSELECCION_NIV1_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletRESTRICCION_NIVEL2 = 11;

  //constantes del panel.
  final String imgLUPA = "images/Magnify.gif";
  final String imgALTA = "images/alta.gif";
//  final String imgMODIFICACION= "images/modificacion.gif";
//  final String imgBORRAR= "images/baja.gif";
  final String imgLIMPIAR = "images/limpiar.gif";

  public boolean bAceptar = false;

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  // im�genes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/aceptar.gif",
      "images/cancelar.gif"};

  // par�metros
  protected int modoOperacion;
  protected int modoOperacionBk;

  protected StubSrvBD stubCliente = null;

  // controles
  XYLayout xYLayout1 = new XYLayout();
  Label lblIndicador = new Label();
  Label lblEnfermedades = new Label();
  Label lblNivel1 = new Label();
  Label lblNivel2 = new Label();
  TextField txtCodInd = new TextField();
  TextField txtCodEnf = new TextField();
  TextField txtCodNivel1 = new TextField();
  TextField txtCodNivel2 = new TextField();
  TextField txtDesInd = new TextField();
  TextField txtDesEnf = new TextField();
  TextField txtDesNivel1 = new TextField();
  TextField txtDesNivel2 = new TextField();
  ButtonControl btn2 = new ButtonControl();
  ButtonControl btn3 = new ButtonControl();
  ButtonControl btnBuscarEnf = new ButtonControl();
  ButtonControl btnBuscarNivel2 = new ButtonControl();
  ButtonControl btnBuscarNivel1 = new ButtonControl();
  Checkbox chkActivo = new Checkbox();

  //  ESCUCHADOR PULSAR BOTONES:
  AlmActionListener btnActionListener = new AlmActionListener(this);
  txt_keyAdapter txtKeyAdapter = new txt_keyAdapter(this);
  focusAdapter txtFocusAdapter = new focusAdapter(this);

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {

      case modoALTA:
        txtDesInd.setEnabled(true);
        btn2.setEnabled(true);
        btn3.setEnabled(true);
        btnBuscarEnf.setEnabled(true);
        btnBuscarNivel1.setEnabled(true);
        btnBuscarNivel2.setEnabled(false);
        txtCodEnf.setEnabled(true);
        txtCodNivel1.setEnabled(true);
        InicializaNivel2();
        chkActivo.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        //this.doLayout();
        break;

      case modoESPERA:
        btn2.setEnabled(false);
        btn3.setEnabled(false);
        btnBuscarEnf.setEnabled(false);
        btnBuscarNivel1.setEnabled(false);
        btnBuscarNivel2.setEnabled(false);
        txtCodEnf.setEnabled(false);
        txtCodNivel1.setEnabled(false);
        txtCodNivel2.setEnabled(false);
        txtDesInd.setEnabled(false);
        chkActivo.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        //this.doLayout();  */
        break;
      case modoMODIFICAR_A:

        //solo puede modif chk
        chkActivo.setEnabled(true);
        txtDesInd.setEnabled(false);
        btn2.setEnabled(true);
        btn3.setEnabled(true);
        btnBuscarEnf.setEnabled(false);
        btnBuscarNivel1.setEnabled(false);
        btnBuscarNivel2.setEnabled(false);
        txtCodEnf.setEnabled(false);
        txtCodNivel1.setEnabled(false);
        txtCodNivel2.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        //this.doLayout();
        break;

      case modoMODIFICAR:

        //solo puede modif descripcion y chk
        btn2.setEnabled(true);
        btn3.setEnabled(true);
        btnBuscarEnf.setEnabled(false);
        btnBuscarNivel1.setEnabled(false);
        btnBuscarNivel2.setEnabled(false);
        txtCodEnf.setEnabled(false);
        txtCodNivel1.setEnabled(false);
        txtCodNivel2.setEnabled(false);
        chkActivo.setEnabled(true);
        txtDesInd.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        //this.doLayout();
        break;

      case modoBAJA:

        //Todo deshabiliatdo salvo aceptar y cancelar
        btn2.setEnabled(true);
        btn3.setEnabled(true);
        btnBuscarEnf.setEnabled(false);
        btnBuscarNivel1.setEnabled(false);
        btnBuscarNivel2.setEnabled(false);
        txtCodEnf.setEnabled(false);
        txtCodNivel1.setEnabled(false);
        txtCodNivel2.setEnabled(false);
        txtDesInd.setEnabled(false);
        chkActivo.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        //this.doLayout();  */
        break;

    }
  }

  // actualiza el estado del nivel2, condicionado al nivel1
  protected void InicializaNivel2() {
    if (txtDesNivel1.getText().length() == 0) {
      txtCodNivel2.setText("");
      txtDesNivel2.setText("");
      txtCodNivel2.setEnabled(false);
      btnBuscarNivel2.setEnabled(false);
    }
    else {
      txtCodNivel2.setEnabled(true);
      btnBuscarNivel2.setEnabled(true);
    }
  }

  // contructor
  public Dial_Entrada(CApp a, int modo) {
    super(a);
    try {
      this.setTitle(res.getString("this.Title"));
      modoOperacion = modo;
      jbInit();
      //apunta a SrvInd  p.ej.
      stubCliente = new StubSrvBD(new URL(this.app.getURL() + strSERVLET_IND));
      btn2.setFocusAware(false);
      btn3.setFocusAware(false);
      btnBuscarEnf.setFocusAware(false);
      btnBuscarNivel1.setFocusAware(false);
      btnBuscarNivel2.setFocusAware(false);

      if (app.getTSive().equals("E")) {
        this.txtCodNivel1.setText(a.getCD_NIVEL1_DEFECTO());
        this.txtDesNivel1.setText(a.getDS_NIVEL1_DEFECTO());
        this.txtCodNivel2.setText(a.getCD_NIVEL2_DEFECTO());
        this.txtDesNivel2.setText(a.getDS_NIVEL2_DEFECTO());
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //Component initialization
  private void jbInit() throws Exception {

    this.setLayout(xYLayout1);

    //Centinelas. De momento no hay area dist.zbs
    if (app.getTSive().equals("C")) {
      xYLayout1.setHeight(225 - 58);
      xYLayout1.setWidth(501 - 58);
      this.setSize(501, 225 - 58);
    }
    //Edo
    else if (app.getTSive().equals("E")) {
      xYLayout1.setHeight(225);
      xYLayout1.setWidth(501);
      this.setSize(501, 225);
    }

    lblIndicador.setText(res.getString("lblIndicador.Text"));
    lblEnfermedades.setText(res.getString("lblEnfermedades.Text"));
    lblNivel1.setText(this.app.getNivel1() + ":");
    lblNivel2.setText(this.app.getNivel2() + ":");
    btn2.setLabel(res.getString("btn2.Label"));
    btn2.setActionCommand("Aceptar");
    btn3.setLabel(res.getString("btn3.Label"));
    btn3.setActionCommand("Cancelar");

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnBuscarEnf.setImage(imgs.getImage(0));
    btnBuscarNivel2.setImage(imgs.getImage(0));
    btnBuscarNivel1.setImage(imgs.getImage(0));
    btn2.setImage(imgs.getImage(1));
    btn3.setImage(imgs.getImage(2));

    btnBuscarEnf.setActionCommand("BuscarEnf"); //     Enf
    btnBuscarNivel1.setActionCommand("BuscarNivel1"); // Nivel1
    btnBuscarNivel2.setActionCommand("BuscarNivel2"); // Nivel2
    chkActivo.setLabel(res.getString("chkActivo.Label"));
    chkActivo.setState(true);

    txtCodInd.setName("codi");
    txtDesInd.setName("desi");
    txtCodEnf.setName("txtCodEnf");
    txtDesEnf.setName("txtDesEnf");
    txtCodNivel1.setName("txtCodNivel1");
    txtDesNivel1.setName("txtDesNivel1");
    txtCodNivel2.setName("txtCodNivel2");
    txtDesNivel2.setName("txtDesNivel2");

    this.add(lblIndicador, new XYConstraints(27, 27, 61, -1));
    this.add(txtCodInd, new XYConstraints(105, 28, 125, 22));
    this.add(txtDesInd, new XYConstraints(243, 28, 210, -1));
    this.add(lblEnfermedades, new XYConstraints(27, 60, 94, 24));
    this.add(txtCodEnf, new XYConstraints(132, 61, 77, -1));
    this.add(btnBuscarEnf, new XYConstraints(213, 62, -1, -1));
    this.add(txtDesEnf, new XYConstraints(242, 61, 210, -1));

    //Centinelas. De momento no hay area dist.zbs
    if (app.getTSive().equals("C")) {
      this.add(chkActivo, new XYConstraints(28, 92, 109, 28));
      this.add(btn2, new XYConstraints(263, 110, 90, -1));
      this.add(btn3, new XYConstraints(366, 110, 90, -1));
    }
    //Edo
    else if (app.getTSive().equals("E")) {
      this.add(lblNivel1, new XYConstraints(27, 92, 99, -1));
      this.add(txtCodNivel1, new XYConstraints(132, 92, 77, -1));
      this.add(btnBuscarNivel1, new XYConstraints(213, 93, -1, -1));
      this.add(txtDesNivel1, new XYConstraints(242, 92, 210, -1));
      this.add(lblNivel2, new XYConstraints(27, 120, 99, -1));
      this.add(txtCodNivel2, new XYConstraints(132, 121, 77, -1));
      this.add(btnBuscarNivel2, new XYConstraints(213, 121, -1, -1));
      this.add(txtDesNivel2, new XYConstraints(242, 121, 210, -1));
      this.add(chkActivo, new XYConstraints(28, 151, 109, 28));
      this.add(btn2, new XYConstraints(263, 168, 90, -1));
      this.add(btn3, new XYConstraints(366, 168, 90, -1));
    }

    // establece los escuchadores
    btnBuscarEnf.addActionListener(btnActionListener);
    btnBuscarNivel1.addActionListener(btnActionListener);
    btnBuscarNivel2.addActionListener(btnActionListener);
    btn2.addActionListener(btnActionListener);
    btn3.addActionListener(btnActionListener);

    txtCodEnf.addKeyListener(txtKeyAdapter);
    txtCodNivel1.addKeyListener(txtKeyAdapter);
    txtCodNivel2.addKeyListener(txtKeyAdapter);

    txtCodEnf.addFocusListener(txtFocusAdapter);
    txtCodNivel1.addFocusListener(txtFocusAdapter);
    txtCodNivel2.addFocusListener(txtFocusAdapter);

    // controles con estado fijo
    txtCodInd.setEnabled(false);
    txtCodInd.setEditable(false);
    txtDesInd.setBackground(new Color(255, 255, 150));

    txtDesEnf.setEnabled(false);
    txtDesEnf.setEditable(false);
    txtCodEnf.setBackground(new Color(255, 255, 150));

    txtDesNivel1.setEnabled(false);
    txtDesNivel1.setEditable(false);

    txtDesNivel2.setEnabled(false);
    txtDesNivel2.setEditable(false);

    chkActivo.setEnabled(true);

    //Centinelas
    if (app.getTSive().equals("C")) {
      //Soluci�n provisional (De momento no hay area, distrito en Red Med Cent)
      lblNivel1.setVisible(false);
      lblNivel2.setVisible(false);
      txtCodNivel1.setVisible(false);
      txtCodNivel2.setVisible(false);
      txtDesNivel1.setVisible(false);
      txtDesNivel2.setVisible(false);
      btnBuscarNivel1.setVisible(false);
      btnBuscarNivel2.setVisible(false);
    }
    // establece el modo de operaci�n
    Inicializar();
    this.doLayout();
  }

  // comprueba que los datos sean v�lidos********************
  protected boolean isDataValid() {

    CMessage msgBox;
    boolean b = true;
    String sMsg = "";

    if (txtCodEnf.getText().length() == 0 ||
        txtDesInd.getText().length() == 0) {
      sMsg = res.getString("msg6.Text");
      b = false;
    }

    switch (this.getCApp().getPerfil()) {

      //Epid . de area (niv1 siempre informado)
      case 3:

        if (txtDesNivel1.getText().length() == 0) {
          sMsg = res.getString("msg6.Text");
          txtDesNivel1.selectAll();
          b = false;
        }
        break;
        //Epid . de distrito (niv1, niv2 siempre informados)
      case 4:
        if (txtDesNivel1.getText().length() == 0) {

          sMsg = res.getString("msg6.Text");
          txtDesNivel1.selectAll();
          b = false;
        }
        if (txtDesNivel2.getText().length() == 0) {

          sMsg = res.getString("msg6.Text");
          txtDesNivel2.selectAll();
          b = false;
        }
        break;
    }

    if (txtDesInd.getText().length() > 40 && b) {
      sMsg = res.getString("msg7.Text");
      txtDesInd.selectAll();
      if (b) {
        b = false;
      }
    }
    //mensaje:
    if (b == false) {
      msgBox = new CMessage(app, CMessage.msgAVISO, sMsg);
      msgBox.show();
      msgBox = null;
    }
    valido = b;
    return b;
  }

  //BOTONES*****************************************
  // procesa opci�n a�adir un item*******************
  public void A�adir() {
    CMessage msgBox = null;
    CLista data = null;
    String sChk;
    CLista datDevueltos = null;

    // determina si los datos est�n completos
    if (this.isDataValid()) {

      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        // prepara los par�metros
        data = new CLista();
        if (chkActivo.getState() == true) {
          sChk = "S";
        }
        else {
          sChk = "N";

        }
        data.addElement(new DataInd("",
                                    txtDesInd.getText(), txtCodEnf.getText(),
                                    txtCodNivel1.getText(),
                                    txtCodNivel2.getText(), sChk, "M"));

        // apunta al servlet principal
        data.setIdioma(this.app.getIdioma());
        data.setLogin(this.app.getLogin());
        data.setPerfil(this.app.getPerfil());
        data.setTSive(app.getTSive());
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_IND));
        datDevueltos = (CLista)this.stubCliente.doPost(servletALTA, data);
        if (datDevueltos.size() > 0) {
          codAnadido = ( (DataInd) (datDevueltos.firstElement())).getCodInd();

          // oculta el dialogo
        }
        this.dispose();

//        BorrarPantalla();
//        this.modoOperacion = modoENTRADA;

        // error en el proceso
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.modoOperacion = modoALTA;
        Inicializar();
      }
//      Inicializar();
    }
  }

  // procesa opci�n modificar***********************
  public void Modificar() {
    CMessage msgBox = null;
    CLista data = null;
    String sChk;

    //puede venir de modificar y de modificar automatico
    this.modoOperacionBk = this.modoOperacion;

    if (this.isDataValid()) {

      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        // prepara los par�metros de env�o
        data = new CLista();
        if (chkActivo.getState() == true) {
          sChk = "S";
        }
        else {
          sChk = "N";

        }
        data.addElement(new DataInd(txtCodInd.getText(),
                                    txtDesInd.getText(), "", "", "", sChk, ""));

        // apunta al servlet principal
        data.setIdioma(this.app.getIdioma());
        data.setLogin(this.app.getLogin());
        data.setPerfil(this.app.getPerfil());
        data.setTSive(app.getTSive());
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_IND));
        this.stubCliente.doPost(servletMODIFICAR, data);

        // oculta el dialogo
        this.dispose();

//        this.modoOperacion = this.modoOperacionBk;

        // error en el proceso
      }
      catch (Exception e) {
        e.printStackTrace();
        this.modoOperacion = this.modoOperacionBk;
        Inicializar();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
//      Inicializar();
    }
  } //fin de modificar

  public void Borrar() {
    CMessage msgBox = null;
    CLista data = null;

    msgBox = new CMessage(app, CMessage.msgPREGUNTA, res.getString("msg8.Text"));
    msgBox.show();

    // el usuario confirma la operaci�n
    if (msgBox.getResponse()) {

      msgBox = null;

      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        // par�metros de env�o
        data = new CLista();
        //para borrar solo necesitamos el codigo de Indicador
        data.addElement(new DataInd(txtCodInd.getText()));

        // apunta al servlet principal
        data.setIdioma(this.app.getIdioma());
        data.setLogin(this.app.getLogin());
        data.setPerfil(this.app.getPerfil());
        data.setTSive(app.getTSive());
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_IND));
        this.stubCliente.doPost(servletBAJA, data);

        // pasa a modo ENTRADA
        BorrarPantalla();
//        this.modoOperacion = modoENTRADA;

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoMODIFICAR; //estabamos en modificar!
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

//      Inicializar();

    }
    else { //si no confirma el borrado, no hacemos nada
      msgBox = null;
    }
  } //fin de borrar

  void BorrarPantalla() {
    txtCodInd.setText("");
    txtDesInd.setText("");
    txtCodEnf.setText("");
    txtDesEnf.setText("");
    txtCodNivel1.setText("");
    txtDesNivel1.setText("");
    txtCodNivel2.setText("");
    txtDesNivel2.setText("");
    chkActivo.setState(true);
  }

  public void PintarPantalla(DataInd data) {

    CLista datosUsar = new CLista();
    datosUsar.addElement(data);

    txtCodInd.setText(data.getCodInd());
    txtDesInd.setText(data.getDesInd());
    txtCodEnf.setText(data.getCodEnf());
    txtDesEnf.setText(data.getDesEnf());
    txtCodNivel1.setText(data.getCodNivel1());
    txtDesNivel1.setText(data.getDesNivel1());
    txtCodNivel2.setText(data.getCodNivel2());
    txtDesNivel2.setText(data.getDesNivel2());

    if (data.getChkActivo().equals("S")) {
      chkActivo.setState(true);
    }
    else {
      chkActivo.setState(false);
    }
  } //fin Pintar

  // BOTONES DE BUSQUEDA
  // procesa la b�squeda en la lista de INDICADORES****
  public void btnBuscarInd_actionPerformed(ActionEvent evt) {

    CMessage msgBox = null;
    DataInd data;
    this.modoOperacionBk = this.modoOperacion;

    try {
      // apunta al servlet auxiliar
      stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_IND));
      this.modoOperacion = modoESPERA;
      Inicializar();

      CListaIndicador lista = new CListaIndicador(app,
                                                  res.getString("msg9.Text"),
                                                  stubCliente,
                                                  strSERVLET_IND,
                                                  servletOBTENER_X_CODIGO,
                                                  servletOBTENER_X_DESCRIPCION,
                                                  servletSELECCION_X_CODIGO,
          servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataInd) lista.getComponente();

      //he traido datos
      if (data != null) {
        BorrarPantalla();
        PintarPantalla(data);
        if (data.getiTipo().equals("A")) {
          this.modoOperacion = modoMODIFICAR_A;
        }
        else {
          this.modoOperacion = modoMODIFICAR;
        }
      }
      else {
        this.modoOperacion = this.modoOperacionBk;
        if (this.modoOperacionBk != modoMODIFICAR &&
            this.modoOperacionBk != modoMODIFICAR_A) {
          BorrarPantalla();
        }
      }

      Inicializar();

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;

      this.modoOperacion = modoOperacionBk;
      ;
      Inicializar();
    }
  }

//__________________

  // procesa opci�n seleccionar las listas de ENFERMEDADES**********
  public void btnBuscarEnf_actionPerformed(ActionEvent evt) {
    CMessage msgBox = null;
    DataEnferedo data;
    DataEnfCenti dataCenti; //Para centinelas

    this.modoOperacionBk = this.modoOperacion;

    try {

      if (app.getTSive().equals("E")) {

        // apunta al servlet auxiliar
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_ENF));
        this.modoOperacion = modoESPERA;
        Inicializar();

        CListaEnfermedad lista = new CListaEnfermedad(app,
            res.getString("msg10.Text"),
            stubCliente,
            strSERVLET_ENF,
            servletOBTENER_ALARMAS_X_CODIGO,
            servletOBTENER_ALARMAS_X_DESCRIPCION,
            servletSELECCION_ALARMAS_X_CODIGO,
            servletSELECCION_ALARMAS_X_DESCRIPCION);

        lista.show();
        data = (DataEnferedo) lista.getComponente();

        //he traido datos
        if (data != null) {
          txtCodEnf.setText(data.getCod());
          txtDesEnf.setText(data.getDes());
          this.modoOperacion = modoOperacionBk;
        }
        else {
          this.modoOperacion = modoOperacionBk;
        }

        Inicializar();

      } //Fin if

      //Centinelas
      if (app.getTSive().equals("C")) {
        // apunta al servlet auxiliar
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_ENF_CENTI));
        this.modoOperacion = modoESPERA;
        Inicializar();

        CListaEnfCenti lista = new CListaEnfCenti(app,
                                                  res.getString("msg10.Text"),
                                                  stubCliente,
                                                  strSERVLET_ENF_CENTI,
                                                  servletOBTENER_X_CODIGO,
                                                  servletOBTENER_X_DESCRIPCION,
                                                  servletSELECCION_X_CODIGO,
            servletSELECCION_X_DESCRIPCION);
        lista.show();
        dataCenti = (DataEnfCenti) lista.getComponente();

        //he traido datos
        if (dataCenti != null) {
          txtCodEnf.setText(dataCenti.getCod());
          txtDesEnf.setText(dataCenti.getDes());
          this.modoOperacion = modoOperacionBk;
        }
        else {
          this.modoOperacion = modoOperacionBk;
        }

        Inicializar();

      } //Fin else if

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;

      this.modoOperacion = modoOperacionBk;
      Inicializar();
    }
  }

//__________________

  // procesa opci�n seleccionar las listas de niveles 1***********
  public void btnBuscarNivel1_actionPerformed(ActionEvent evt) {

    DataEntradaEDO data;
    CMessage msgBox;
    this.modoOperacionBk = this.modoOperacion;

    try {
      txtCodNivel1.setText("");
      txtDesNivel1.setText("");
      txtCodNivel2.setText("");
      txtDesNivel2.setText("");
      // apunta al servlet auxiliar
      stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_NIVELES));
      this.modoOperacion = modoESPERA;
      Inicializar();

      CListaNiveles1 lista = new CListaNiveles1(app,
                                                res.getString("msg11.Text") +
                                                app.getNivel1(),
                                                stubCliente,
                                                strSERVLET_NIVELES,
                                                servletOBTENER_NIV1_X_CODIGO,
          servletOBTENER_NIV1_X_DESCRIPCION,
          servletSELECCION_NIV1_X_CODIGO,
          servletSELECCION_NIV1_X_DESCRIPCION);

      /*
            CListaCat lista = new CListaCat(app,
              res.getString("msg12.Text")+ app.getNivel1(),
                stubCliente,
                strSERVLET_NIV1,
                servletOBTENER_X_CODIGO+Catalogo.catNIVEL1,
                servletOBTENER_X_DESCRIPCION+Catalogo.catNIVEL1,
                servletSELECCION_X_CODIGO+Catalogo.catNIVEL1,
                servletSELECCION_X_DESCRIPCION+Catalogo.catNIVEL1);
          lista.show();
          data = (DataCat) lista.getComponente();
          modoOperacion = this.modoOperacionBk;
          if (data != null) {
            txtCodNivel1.setText(data.getCod());
            txtDesNivel1.setText(data.getDes());
            this.modoOperacion = this.modoOperacionBk;
          }
       */

      lista.show();
      data = (DataEntradaEDO) lista.getComponente();
      modoOperacion = this.modoOperacionBk;
      if (data != null) {
        txtCodNivel1.setText(data.getCod());
        txtDesNivel1.setText(data.getDes());
        this.modoOperacion = this.modoOperacionBk;
      }

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
      this.modoOperacion = this.modoOperacionBk;
    }
    Inicializar();
  } //fin de nivel1

  // procesa opci�n seleccionar las listas de niveles 1***********
  public void btnBuscarNivel2_actionPerformed(ActionEvent evt) {

    DataEntradaEDO data;
    CMessage msgBox;
    this.modoOperacionBk = this.modoOperacion;

    try {
      // apunta al servlet auxiliar
      stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_NIV2));
      modoOperacion = modoESPERA;
      Inicializar();
      /*
          CListaZBS2 lista = new CListaZBS2(this,
           res.getString("msg12.Text")+app.getNivel2(),
                                            stubCliente,
                                            strSERVLET_NIV2,
                                            servletOBTENER_NIV2_X_CODIGO,
                                            servletOBTENER_NIV2_X_DESCRIPCION,
                                            servletSELECCION_NIV2_X_CODIGO,
           servletSELECCION_NIV2_X_DESCRIPCION);
       */

      CListaNiveles2 lista = new CListaNiveles2(this,
                                                res.getString("msg12.Text") +
                                                app.getNivel2(),
                                                stubCliente,
                                                strSERVLET_NIVELES,
                                                servletOBTENER_NIV2_X_CODIGO,
          servletOBTENER_NIV2_X_DESCRIPCION,
          servletSELECCION_NIV2_X_CODIGO,
          servletSELECCION_NIV2_X_DESCRIPCION);

      lista.show();
      data = (DataEntradaEDO) lista.getComponente();
      this.modoOperacion = this.modoOperacionBk;
      if (data != null) {
        txtCodNivel2.setText(data.getCod());
        txtDesNivel2.setText(data.getDes());
        this.modoOperacion = this.modoOperacionBk;
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
      this.modoOperacion = this.modoOperacionBk;
    }
    Inicializar();
  } //fin nivel2

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    //Datos de envio
    DataEntradaEDO data;
    DataEnferedo datEnf;
    DataEnfCenti datEnfCenti;

    CLista param = null;
    CMessage msg;
    int modo = modoOperacion; //Modo regreso de pantalla
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();
    param = new CLista();
    param.setIdioma(app.getIdioma());
    param.setPerfil(app.getPerfil());
    param.setLogin(app.getLogin());
    param.setTSive(app.getTSive());
    param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
    param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());

    // gestion de datos
    if ( (txt.getName().equals("txtCodNivel1")) &&
        (txtCodNivel1.getText().length() > 0)) {
      param.addElement(new DataEntradaEDO(txtCodNivel1.getText().toUpperCase(),
                                          "", ""));
      strServlet = strSERVLET_NIVELES;
      modoServlet = servletOBTENER_NIV1_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodNivel2")) &&
             (txtCodNivel2.getText().length() > 0)) {
      param.addElement(new DataEntradaEDO(txtCodNivel2.getText(), "",
                                          txtCodNivel1.getText()));
      strServlet = strSERVLET_NIVELES;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodEnf")) &&
             (txtCodEnf.getText().length() > 0)) {

      //Edo
      if (app.getTSive().equals("E")) {
        param.addElement(new DataEnferedo(txtCodEnf.getText()));
        strServlet = strSERVLET_ENF;
        modoServlet = servletOBTENER_ALARMAS_X_CODIGO;
      }
      //Centinelas
      else if (app.getTSive().equals("C")) {
        param.addElement(new DataEnfCenti(txtCodEnf.getText()));
        strServlet = strSERVLET_ENF_CENTI;
        modoServlet = servletOBTENER_X_CODIGO;
      }
    }

    // busca el item
    if (param.size() > 0) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        param.setIdioma(this.app.getIdioma());
        param.setLogin(this.app.getLogin());
        param.setPerfil(this.app.getPerfil());
        param.setTSive(app.getTSive());
        param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
        param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        /*
                 SrvEnfermedad servlet = new SrvEnfermedad();
                 //Indica como conectarse a la b.datos
                 servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                 param = (CLista) servlet.doDebug(modoServlet, param);
         */

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtCodNivel1")) {
            data = (DataEntradaEDO) param.firstElement();
            txtCodNivel1.setText(data.getCod());
            txtDesNivel1.setText(data.getDes());

          }
          else if (txt.getName().equals("txtCodNivel2")) {
            data = (DataEntradaEDO) param.firstElement();
            txtCodNivel2.setText(data.getCod());
            txtDesNivel2.setText(data.getDes());

          }
          else if (txt.getName().equals("txtCodEnf")) {

            if (app.getTSive().equals("E")) {
              datEnf = (DataEnferedo) param.firstElement();
              txtCodEnf.setText(datEnf.getCod());
              txtDesEnf.setText(datEnf.getDes());
            }
            else if (app.getTSive().equals("C")) {
              datEnfCenti = (DataEnfCenti) param.firstElement();
              txtCodEnf.setText(datEnfCenti.getCod());
              txtDesEnf.setText(datEnfCenti.getDes());
            }

          }

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg14.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modo;
      Inicializar();
    }

  }

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtCodNivel1")) &&
        (txtDesNivel1.getText().length() > 0)) {
      txtCodNivel2.setText("");
      txtDesNivel1.setText("");
      txtDesNivel2.setText("");

    }
    else if ( (txt.getName().equals("txtCodNivel2")) &&
             (txtDesNivel2.getText().length() > 0)) {
      txtDesNivel2.setText("");

    }
    else if ( (txt.getName().equals("txtCodEnf")) &&
             (txtDesEnf.getText().length() > 0)) {
      txtDesEnf.setText("");

    }
    Inicializar();
  }

} //FIN DE CLASE PPAL******************************************

// lista de valores: de la Pantalla
class CListaIndicador
    extends CListaValores {

  public CListaIndicador(CApp a,
                         String title,
                         StubSrvBD stub,
                         String servlet,
                         int obtener_x_codigo,
                         int obtener_x_descricpcion,
                         int seleccion_x_codigo,
                         int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataInd(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataInd) o).getCodInd());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataInd) o).getDesInd());
  }
}

// lista de valores: de Enfermedades Cie
class CListaEnfermedad
    extends CListaValores {

  public CListaEnfermedad(CApp a,
                          String title,
                          StubSrvBD stub,
                          String servlet,
                          int obtener_x_codigo,
                          int obtener_x_descricpcion,
                          int seleccion_x_codigo,
                          int seleccion_x_descripcion) {

    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEnferedo(s, s); // El servlet se encarga de tomar la cadena
    // necesaria.
  }

  public String getCodigo(Object o) {
    return ( ( (DataEnferedo) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEnferedo) o).getDes());
  }
}

// lista de valores de Enfermedades Centinelas
class CListaEnfCenti
    extends CListaValores {

  public CListaEnfCenti(CApp a,
                        String title,
                        StubSrvBD stub,
                        String servlet,
                        int obtener_x_codigo,
                        int obtener_x_descricpcion,
                        int seleccion_x_codigo,
                        int seleccion_x_descripcion) {

    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEnfCenti(s, s); // El servlet se encarga de tomar la cadena
    // necesaria.
  }

  public String getCodigo(Object o) {
    return ( ( (DataEnfCenti) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEnfCenti) o).getDes());
  }
}

// clase para nivel1
class CListaCat
    extends CListaValores {

  public CListaCat(CApp a,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataCat(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataCat) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCat) o).getDes());
  }
}

class CListaNiveles2
    extends CListaValores {

  protected Dial_Entrada panel;

  public CListaNiveles2(Dial_Entrada p,
                        String title,
                        StubSrvBD stub,
                        String servlet,
                        int obtener_x_codigo,
                        int obtener_x_descricpcion,
                        int seleccion_x_codigo,
                        int seleccion_x_descripcion) {
    super(p.getCApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s, "", panel.txtCodNivel1.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

// ESCUCHADORES******PULSAR BOTONES***************************
// action listener para los botones
class AlmActionListener
    implements ActionListener, Runnable {
  Dial_Entrada adaptee = null;
  ActionEvent e = null;

  public AlmActionListener(Dial_Entrada adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;

    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("BuscarInd")) { // Indicadores
      adaptee.btnBuscarInd_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("BuscarEnf")) { // Enfermedad
      adaptee.btnBuscarEnf_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("BuscarNivel1")) { // nivel 1
      adaptee.btnBuscarNivel1_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("BuscarNivel2")) { // nivel 2
      adaptee.btnBuscarNivel2_actionPerformed(e);

    }
    else if (e.getActionCommand() == "Aceptar") { // aceptar
      adaptee.bAceptar = true;
      // realiza la operaci�n
      switch (adaptee.modoOperacion) {
        case Dial_Entrada.modoALTA:
          adaptee.A�adir();
          break;
        case Dial_Entrada.modoMODIFICAR:
          adaptee.Modificar();
          break;
        case Dial_Entrada.modoMODIFICAR_A:
          adaptee.Modificar();
          break;

        case Dial_Entrada.modoBAJA:
          adaptee.Borrar();
          break;
      }

    }
    else if (e.getActionCommand() == "Cancelar") { // cancelar
      adaptee.bAceptar = false;
      adaptee.dispose();
    }

  }
}

class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  Dial_Entrada adaptee;

  txt_keyAdapter(Dial_Entrada adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Dial_Entrada adaptee;
  FocusEvent event;

  focusAdapter(Dial_Entrada adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {

    adaptee.focusLost(event);
  }
}
