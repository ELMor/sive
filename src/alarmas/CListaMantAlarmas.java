//Title:        Mantenimiento de alarmas
//Version:
//Copyright:    Copyright (c) 1998
//Author:
//Company:      Norsistemas, S.A.
//Description:

package alarmas;

import capp.CApp;
import capp.CListaMantenimiento;
import comun.constantes;
import sapp.StubSrvBD;

public class CListaMantAlarmas
    extends CListaMantenimiento {

  protected String sTitulo;

  final String R = "-"; //Car�cter de relleno para c�digos de nivel 1 o nivel 2 que no lleguen a su valor m�ximo

  public CListaMantAlarmas(ApAlarmas a) {

    /*
        super((CApp) a,
              4,
              "200\n150\n150\n62\n",
              a.res.getString("msg2.Text")+a.res.getString("msg3.Text")+ a.res.getString("msg4.Text")+ a.res.getString("msg5.Text") ,
              new StubSrvBD(),
              a.strSERVLET,
              a.servletSELECCION_X_CODIGO ,
              a.servletSELECCION_X_DESCRIPCION );
        ActivarAuxiliar();
     */

    //Con el USU
    super( (CApp) a,
          4,
          "200\n150\n150\n62\n",
          a.res.getString("msg2.Text") + a.res.getString("msg3.Text") +
          a.res.getString("msg4.Text") + a.res.getString("msg5.Text"),
          new StubSrvBD(),
          a.strSERVLET,
          a.servletSELECCION_X_CODIGO,
          a.servletSELECCION_X_DESCRIPCION,
          constantes.keyBtnAnadir,
          constantes.keyBtnModificar,
          constantes.keyBtnBorrar,
          constantes.keyBtnAux
          );
    ActivarAuxiliar();

  }

  // confeccionar modelo
  public void btnAuxiliar_actionPerformed(int i) {

    modoOperacion = modoESPERA;
    Inicializar();
    DataInd dat = (DataInd) lista.elementAt(i);
    /*
        Panel_Mantalus dial = new Panel_Mantalus(this.app,
                                            new StubSrvBD(),
                                            dat.getCD_Modelo(),
                                            dat.getDS_Modelo(),
                                            dat.getIT_OK());
     */

    Panel_Mantalus dial = new Panel_Mantalus(this.app, dat);
    dial.txtCodInd.setText(dat.getCodInd());
    dial.txtDesInd.setText(dat.getDesInd());

    dial.show();
    dial = null;

    modoOperacion = modoNORMAL;
    Inicializar();

  }

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {
    DataInd cat;
    String sChk;

    modoOperacion = modoESPERA;
    Inicializar();
    Dial_Entrada dial = new Dial_Entrada(this.app, Dial_Entrada.modoALTA);
    dial.show();
    /*
      public DataInd(String cod, String des,
                      String codenf, String desenf,
                      String cod1, String des1,
                      String cod2, String des2,
         String chk, String itipo, String izquierda, String secuencial) {
     */

    if ( (dial.bAceptar) && (dial.valido)) {
      if (dial.chkActivo.getState() == true) {
        sChk = "S";
      }
      else {
        sChk = "N";

      }

      cat = new DataInd(dial.codAnadido,
                        dial.txtDesInd.getText(),
                        dial.txtCodEnf.getText(),
                        dial.txtDesEnf.getText(),
                        dial.txtCodNivel1.getText(),
                        dial.txtDesNivel1.getText(),
                        dial.txtCodNivel2.getText(),
                        dial.txtDesNivel2.getText(),
                        sChk,
                        "M",
                        "",
                        "");
      lista.addElement(cat);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // modifica l�nea de la lista
  public void btnModificar_actionPerformed(int i) {
    DataInd cat;
    String sChk;
    Dial_Entrada dial;
    String tipo;

    modoOperacion = modoESPERA;
    Inicializar();

    //Extrae elemento de lista
    cat = (DataInd) lista.elementAt(i);

    //Crea dialogo en modo que depende del atributo tipo del elemento
    if (cat.getiTipo().equals("A")) {
      tipo = "A";
      dial = new Dial_Entrada(this.app, Dial_Entrada.modoMODIFICAR_A);
    }
    else {
      dial = new Dial_Entrada(this.app, Dial_Entrada.modoMODIFICAR);
      tipo = "M";
    }

    // rellena los datos
    dial.PintarPantalla(cat);
    dial.show();

    if ( (dial.bAceptar) && (dial.valido)) {

      if (dial.chkActivo.getState() == true) {
        sChk = "S";
      }
      else {
        sChk = "N";

      }
      cat = new DataInd(dial.txtCodInd.getText(),
                        dial.txtDesInd.getText(),
                        dial.txtCodEnf.getText(),
                        dial.txtDesEnf.getText(),
                        dial.txtCodNivel1.getText(),
                        dial.txtDesNivel1.getText(),
                        dial.txtCodNivel2.getText(),
                        dial.txtDesNivel2.getText(),
                        sChk,
                        tipo,
                        "",
                        "");

      /*
        cat = new DataInd(dial.txtCod.getText(),
                          dial.txtDes.getText(),
                          dial.txtDesL.getText(),
                          dial.txtBuzon1.getText(),
                          dial.txtBuzon2.getText(),
                          dial.txtBuzon3.getText());
       */
      lista.removeElementAt(i);
      lista.insertElementAt(cat, i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed(int i) {
    DataInd cat;

    modoOperacion = modoESPERA;
    Inicializar();
    Dial_Entrada dial = new Dial_Entrada(this.app, Dial_Entrada.modoBAJA);

    //Extrae elemento de lista y rellena los datos
    cat = (DataInd) lista.elementAt(i);
    dial.PintarPantalla(cat);

    /*
        dial.txtCod.setText(cat.getCod());
        dial.txtDes.setText(cat.getDes());
        dial.txtDesL.setText(cat.getDesL());
        dial.txtBuzon1.setText(cat.getEMail1());
        dial.txtBuzon2.setText(cat.getEMail2());
        dial.txtBuzon3.setText(cat.getEMail3());
     */
    dial.show();

    if (dial.bAceptar) {
      lista.removeElementAt(i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // rellena los par�metros para enviar al servlet
  public Object setComponente(String s) {
    return new DataInd(s);
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(Object o) {
    DataInd cat = (DataInd) o;

    return cat.getCodInd() + "&" + cat.getDesInd() + "&" + cat.getDesEnf() +
        "&" + cat.getiTipo();
  }

  //_________________ Funciones para construir el c�digo a visualizar a partir de los datos
  //Solo utilizadas en el caso de a�adir
  //Motivo : al a�adir no se mete nada en el c�digo. Es en el Servlet donde se forma el c�digo
  //En cliente es necesario repetir ese c�digo para que se pueda ver en la tabla

  // Formatea con ceros a la izquierda
  protected String Formatear_Ceros_Izquierda(String sValor, int iHasta) {
    String sCadena = "";
    int iNum = 0;

    sCadena = sValor;
    iNum = iHasta;

    while (sCadena.length() < iNum) {
      sCadena = "0" + sCadena;
    }
    return sCadena;
  } //fin Formatear

  // Formatea con UNDERSCORES a la izquierda
  protected String Formatear_Under_Izquierda(String sValor, int iHasta) {
    String sCadena = "";
    int iNum = 0;

    sCadena = sValor;
    iNum = iHasta;

    while (sCadena.length() < iNum) {
      sCadena = R + sCadena;
    }
    return sCadena;
  } //fin Formatear

  // Formatea Secuencial
  protected String Formatear_Secuencial(int iSiguiente) {
    String sVolver = "";
    int iNum = iSiguiente;
    Integer iIntermedio = new Integer(iSiguiente);

    sVolver = iIntermedio.toString();

    while (sVolver.length() < 3) {
      sVolver = "0" + sVolver;
    }
    return sVolver;
  } //fin Formatear

}