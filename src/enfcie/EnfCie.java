
package enfcie;

import java.net.URL;
import java.util.ResourceBundle;

import capp.CApp;
import comun.constantes;
import sapp.StubSrvBD;

public class EnfCie
    extends CApp {

  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  StubSrvBD stubCliente = null;
//  final String strSERVLET = "servlet/SrvEnfCie";
  final String strSERVLET = constantes.strSERVLET_ENF_CIE;

  public void init() {
    super.init();

  }

  public void start() {
    try {

      CApp a = (CApp)this;
      res = ResourceBundle.getBundle("enfcie.Res" + getIdioma());
      setTitulo(res.getString("applet.Title"));
      stubCliente = new StubSrvBD(new URL(a.getURL() + strSERVLET));

      //Centinelas: Constructor con el USU
      if (this.getTSive().equals("C")) {

        VerPanel("", new CListaMantEnfCie(a, 2, "70\n492",
                                          res.getString("applet.Columns"),
                                          stubCliente, strSERVLET,
                                          servletSELECCION_X_CODIGO,
                                          servletSELECCION_X_DESCRIPCION, true));
      }
      else {
        VerPanel("", new CListaMantEnfCie(a, 2, "70\n492",
                                          res.getString("applet.Columns"),
                                          stubCliente, strSERVLET,
                                          servletSELECCION_X_CODIGO,
                                          servletSELECCION_X_DESCRIPCION));
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

}
