
package enfcie;

import java.io.Serializable;

public class DataEnfCie
    implements Serializable {
  protected String sCod = "";
  protected String sDes = "";
  protected String sDesL = "";
  protected String sDesI = "";

  public DataEnfCie() {
  }

  public DataEnfCie(String cod) {
    sCod = cod;
  }

  public DataEnfCie(String cod, String des, String desL, String desI) {
    sCod = cod;
    sDes = des;
    sDesL = desL;
    if (sDesL == null) {
      sDesL = "";
    }
    sDesI = desI;
    if (sDesI == null) {
      sDesI = "";
    }
  }

  public String getCod() {
    return sCod;
  }

  public String getDes() {
    return sDes;
  }

  public String getDesL() {
    return sDesL;
  }

  public String getDesI() {
    return sDesI;
  }
}
