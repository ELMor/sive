package enfcie;

import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import comun.constantes;
import sapp.StubSrvBD;

public class DialEnfCie
    extends CDialog {

  protected CListaMantEnfCie pnlModelo;
  ResourceBundle res;
  // datos recibidos por el di�logo
  protected DataEnfCie datLinea = null;
  public boolean bAceptar = false;

  // modos de operaci�n de la ventana
  final static int modoALTA = 0;
  final static int modoMODIFICAR = 1;
  final static int modoESPERA = 2;
  final static int modoBAJA = 3;

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  // im�genes
  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

//  final String strSERVLET = "servlet/SrvEnfCie";
  final String strSERVLET = constantes.strSERVLET_ENF_CIE;

  // modos de operaci�n del servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // par�metros
  protected int modoOperacion = modoALTA;
  protected StubSrvBD stubCliente = null;

  // controles
  XYLayout xYLayout1 = new XYLayout();
  Label lbl1 = new Label();
  CCampoCodigo txtCod = new CCampoCodigo();
  Label lbl2 = new Label();
  TextField txtDes = new TextField();
  Label lbl3 = new Label();
  TextField txtDesL = new TextField();
  Label lbl4 = new Label();
  TextField txtDesI = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  enfcieActionListener btnActionListener = new enfcieActionListener(this);

  // constructor
  public DialEnfCie(CApp a,
                    CListaMantEnfCie p,
                    DataEnfCie datEnfCie,
                    int modoInicio) {
    super(a);

    try {
      res = ResourceBundle.getBundle("enfcie.Res" + app.getIdioma());
      modoOperacion = modoInicio;
      this.setTitle(res.getString("this.Title"));
      this.pnlModelo = p;
      stubCliente = ( (EnfCie) a).stubCliente;
      this.datLinea = datEnfCie;
      jbInit();
      InicializarDatos();
      pack();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    xYLayout1.setHeight(196);
    xYLayout1.setWidth(470);
    setLayout(xYLayout1);
    this.setSize(470, 196);

    lbl1.setText(res.getString("lbl1.Text"));
    lbl2.setText(res.getString("lbl2.Text"));
    lbl3.setText(res.getString("lbl3.Text") + app.getIdiomaLocal() + "):");
    lbl4.setText(res.getString("lbl4.Text"));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");

    this.add(lbl1, new XYConstraints(7, 5, -1, -1));
    this.add(txtCod, new XYConstraints(101, 5, 77, -1));
    this.add(lbl2, new XYConstraints(7, 37, -1, -1));
    this.add(txtDes, new XYConstraints(101, 39, 356, -1));
    this.add(lbl4, new XYConstraints(7, 77, -1, -1));
    this.add(txtDesI, new XYConstraints(177, 79, 280, -1));
    this.add(lbl3, new XYConstraints(7, 117, -1, -1));
    this.add(txtDesL, new XYConstraints(177, 119, 280, -1));
    this.add(btnCancelar, new XYConstraints(377, 159, 80, 26));
    this.add(btnAceptar, new XYConstraints(280, 159, 80, 26));

//    this.add(pnl, new XYConstraints(10, 10, 485, 235));

    txtCod.setForeground(Color.black);
    txtCod.setBackground(new Color(255, 255, 150));
    txtDes.setForeground(Color.black);
    txtDes.setBackground(new Color(255, 255, 150));

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));

    // establece los escuchadores
    btnAceptar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);

    // idioma local no es visible si no tengo
    if (app.getIdiomaLocal().length() == 0) {
      txtDesL.setVisible(false);
      lbl3.setVisible(false);
    }
    else {
      lbl3.setText(res.getString("lbl3.Text") + app.getIdiomaLocal() + "):");
    }

    // establece el modo de operaci�n
    Inicializar();
//    this.doLayout();

  }

  void InicializarDatos() {
    if (datLinea != null) {
      txtCod.setText(datLinea.getCod());
      txtDes.setText(datLinea.getDes());
      txtDesI.setText(datLinea.getDesI());
      txtDesL.setText(datLinea.getDesL());

    }

  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:

        // modo alta
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtCod.setEnabled(true);
        txtDes.setEnabled(true);
        txtDesL.setEnabled(true);
        txtDesI.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:

        // modo modificaci�n y baja

        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtCod.setEnabled(false);
        txtDes.setEnabled(true);
        txtDesL.setEnabled(true);
        txtDesI.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:

        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtCod.setEnabled(false);
        txtDes.setEnabled(false);
        txtDesL.setEnabled(false);
        txtDesI.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:

        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        txtCod.setEnabled(false);
        txtDes.setEnabled(false);
        txtDesL.setEnabled(false);
        txtDesI.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;

    boolean b = false;

    // comprueba que esten informados los campos obligatorios
    if ( (txtCod.getText().length() > 0) && (txtDes.getText().length() > 0)) {
      b = true;

      // comprueba la longitud m�xima de los campos
      if (txtCod.getText().length() > 6) {
        b = false;
        msg = res.getString("msg1.Text");
        txtCod.selectAll();
      }

      if (txtDes.getText().length() > 40) {
        b = false;
        msg = res.getString("msg1.Text");
        txtDes.selectAll();
      }

      if (txtDesL.getText().length() > 40) {
        b = false;
        msg = res.getString("msg1.Text");
        txtDesL.selectAll();
      }

      if (txtDesI.getText().length() > 40) {
        b = false;
        msg = res.getString("msg1.Text");
        txtDesI.selectAll();
      }

      // advertencia de requerir datos
    }
    else {
      msg = res.getString("msg2.Text");
    }

    if (!b) {
      msgBox = new CMessage(app, CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }
    bAceptar = b;
    return b;
  }

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  // procesa opci�n a�adir
  public void A�adir() {
    CMessage msgBox = null;
    CLista data = null;

    if (this.isDataValid()) {

      // a�ade la enfermedad
      try {
        this.modoOperacion = modoESPERA;
        Inicializar();

        data = new CLista();
        data.addElement(new DataEnfCie(txtCod.getText().toUpperCase(),
                                       txtDes.getText(), txtDesL.getText(),
                                       txtDesI.getText()));
        this.stubCliente.doPost(servletALTA, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoALTA;
        Inicializar();

        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
  }

  // procesa opci�n modificar
  public void Modificar() {
    CMessage msgBox = null;
    CLista data = null;

    if (this.isDataValid()) {

      // modifica la enfermedad
      try {
        this.modoOperacion = modoESPERA;
        Inicializar();

        data = new CLista();
        data.addElement(new DataEnfCie(txtCod.getText(), txtDes.getText(),
                                       txtDesL.getText(), txtDesI.getText()));
        this.stubCliente.doPost(servletMODIFICAR, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoMODIFICAR;
        Inicializar();

        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

    }
  }

  // procesa opci�n borrar
  public void Borrar() {

    CMessage msgBox = null;
    CLista data = null;

    msgBox = new CMessage(app, CMessage.msgPREGUNTA, res.getString("msg3.Text"));
    msgBox.show();

    if (msgBox.getResponse()) {

      msgBox = null;
      // borra la enfermedad
      try {

        this.modoOperacion = modoESPERA;
        Inicializar();

        data = new CLista();
        data.addElement(new DataEnfCie(txtCod.getText()));
        stubCliente.doPost(servletBAJA, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoBAJA;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
    else {
      msgBox = null;
    }
  }

} //CLASE

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// action listener
class enfcieActionListener
    implements ActionListener, Runnable {
  DialEnfCie adaptee = null;
  ActionEvent e = null;

  public enfcieActionListener(DialEnfCie adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand() == "aceptar") { // aceptar
      adaptee.bAceptar = true;

      // realiza la operaci�n
      switch (adaptee.modoOperacion) {
        case DialEnfCie.modoALTA:
          adaptee.A�adir();
          break;
        case DialEnfCie.modoMODIFICAR:
          adaptee.Modificar();
          break;
        case DialEnfCie.modoBAJA:
          adaptee.Borrar();
          break;
      }

    }
    else if (e.getActionCommand() == "cancelar") { // cancelar
      adaptee.bAceptar = false;
      adaptee.dispose();
    }
  }
}
