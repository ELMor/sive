package enfcie;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvEnfCie
    extends DBServlet {

  // modos de operaci�n
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRICPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRICPCION = 6;

  /* QQ: An�lisis de tiempos: */

  long lInicio = 0;
  long lFin = 0;
  long lIniConexion = 0;
  long lFinConexion = 0;
  long lIniSelect = 0;
  long lFinSelect = 0;
  long lIniProceso = 0;
  long lFinProceso = 0;

  //Para descripciones
  String sDes = "";
  String sDesL = "";

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

//    lInicio=System.currentTimeMillis();
//    // System_out.println("Trazando Servlet enfcie...");
    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    CLista data = new CLista();
    DataEnfCie enfcie = null;

    // establece la conexi�n con la base de datos

//    lIniConexion=System.currentTimeMillis();
    con = openConnection();
//    lFinConexion=System.currentTimeMillis();
    con.setAutoCommit(true);

    enfcie = (DataEnfCie) param.firstElement();

    // modos de operaci�n
    switch (opmode) {

      // alta de enfermedad CIE
      case servletALTA:

        // prepara la query
        query = "insert into SIVE_ENFER_CIE (CD_ENFCIE, DS_ENFERCIE, DSL_ENFERCIE, DSI_ENFERCIE) values (?, ?, ?, ?)";
        st = con.prepareStatement(query);
        // codigo
        st.setString(1, enfcie.getCod().trim().toUpperCase());
        // descripci�n
        st.setString(2, enfcie.getDes().trim());
        // descripci�n local
        if (enfcie.getDesL().trim().length() > 0) {
          st.setString(3, enfcie.getDesL().trim());
        }
        else {
          st.setNull(3, java.sql.Types.VARCHAR);
          // descripci�n en ingl�s
        }
        if (enfcie.getDesI().trim().length() > 0) {
          st.setString(4, enfcie.getDesI().trim());
        }
        else {
          st.setNull(4, java.sql.Types.VARCHAR);

          // lanza la query
        }
        st.executeUpdate();
        st.close();

        break;

        // baja de enfermedad CIE
      case servletBAJA:

        // prepara la query
        query = "DELETE FROM SIVE_ENFER_CIE WHERE CD_ENFCIE = ?";

        // lanza la query
        st = con.prepareStatement(query);
        st.setString(1, enfcie.getCod().trim());
        st.executeUpdate();
        st.close();

        break;

        // modificaci�n de enfermedad CIE
      case servletMODIFICAR:

        // prepara la query
        query = "UPDATE SIVE_ENFER_CIE SET DS_ENFERCIE=?, DSL_ENFERCIE=?, DSI_ENFERCIE=? WHERE CD_ENFCIE=?";

        // lanza la query
        st = con.prepareStatement(query);
        st.setString(1, enfcie.getDes().trim());
        // descripci�n local
        if (enfcie.getDesL().trim().length() > 0) {
          st.setString(2, enfcie.getDesL().trim());
        }
        else {
          st.setNull(2, java.sql.Types.VARCHAR);
          // descripci�n en ingl�s
        }
        if (enfcie.getDesI().trim().length() > 0) {
          st.setString(3, enfcie.getDesI().trim());
        }
        else {
          st.setNull(3, java.sql.Types.VARCHAR);
          // codigo
        }
        st.setString(4, enfcie.getCod().trim());
        st.executeUpdate();
        st.close();

        break;

        // b�squeda de enfermedad CIE
      case servletSELECCION_X_CODIGO:
      case servletSELECCION_X_DESCRICPCION:

        // peticion de trama
        if (param.getFilter().length() > 0) {

          if (opmode == servletSELECCION_X_CODIGO) {
            query = "select CD_ENFCIE, DS_ENFERCIE, DSL_ENFERCIE, DSI_ENFERCIE from SIVE_ENFER_CIE where CD_ENFCIE like ? and CD_ENFCIE > ? order by CD_ENFCIE";
          }
          else {
            query = "select CD_ENFCIE, DS_ENFERCIE, DSL_ENFERCIE, DSI_ENFERCIE from SIVE_ENFER_CIE where upper(DS_ENFERCIE) like upper(?) and upper(DS_ENFERCIE) > upper(?) order by DS_ENFERCIE";

            // peticion de la primera trama
          }
        }
        else {
          if (opmode == servletSELECCION_X_CODIGO) {
            query = "select CD_ENFCIE, DS_ENFERCIE, DSL_ENFERCIE, DSI_ENFERCIE from SIVE_ENFER_CIE where CD_ENFCIE like ? order by CD_ENFCIE";
          }
          else {
            query = "select CD_ENFCIE, DS_ENFERCIE, DSL_ENFERCIE, DSI_ENFERCIE from SIVE_ENFER_CIE where upper(DS_ENFERCIE) like upper(?) order by DS_ENFERCIE";
          }
        }

        // prepara la lista de resultados
        data = new CLista();

        // lanza la query
        st = con.prepareStatement(query);
        // filtro
        if (opmode == servletSELECCION_X_CODIGO) {
          st.setString(1, enfcie.getCod().trim() + "%");
        }
        else {
          st.setString(1, "%" + enfcie.getCod().trim() + "%");
          // paginaci�n
        }
        if (param.getFilter().length() > 0) {
          st.setString(2, param.getFilter().trim());
        }

//        lIniSelect=System.currentTimeMillis();
        rs = st.executeQuery();
//        lFinSelect=System.currentTimeMillis();
//        lIniProceso=System.currentTimeMillis();
        // extrae la p�gina requerida
        while (rs.next()) {
          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            if (opmode == servletSELECCION_X_CODIGO) {
              data.setFilter( ( (DataEnfCie) data.lastElement()).getCod());
            }
            else {
              data.setFilter( ( (DataEnfCie) data.lastElement()).getDes());
            }
            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }

          //___________________

          sDes = rs.getString("DS_ENFERCIE");
          sDesL = rs.getString("DSL_ENFERCIE");

          // obtiene la descripcion principal en funci�n del idioma si el cliente no es un mantenimiento
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (param.getMantenimiento() == false)) {
            if (sDesL != null) {
              sDes = sDesL;
            }
          }

          // a�ade un nodo
          data.addElement(new DataEnfCie(rs.getString("CD_ENFCIE"), sDes, sDesL,
                                         rs.getString("DSI_ENFERCIE")));

          //_____________________

          i++;
        }

        rs.close();
        st.close();
//        lIniProceso=System.currentTimeMillis();
        break;

      case servletOBTENER_X_CODIGO:
      case servletOBTENER_X_DESCRICPCION:

        // prepara la query
        if (opmode == servletOBTENER_X_CODIGO) {
          query = "select CD_ENFCIE, DS_ENFERCIE, DSL_ENFERCIE, DSI_ENFERCIE from SIVE_ENFER_CIE where CD_ENFCIE = ?";
        }
        else {
          query = "select CD_ENFCIE, DS_ENFERCIE, DSL_ENFERCIE, DSI_ENFERCIE from SIVE_ENFER_CIE where DS_ENFERCIE = ?";

          // lanza la query
        }
        st = con.prepareStatement(query);
        st.setString(1, enfcie.getCod().trim());
        rs = st.executeQuery();

        // extrae el registro encontrado
        while (rs.next()) {

          sDes = rs.getString("DS_ENFERCIE");
          sDesL = rs.getString("DSL_ENFERCIE");

          // obtiene la descripcion principal en funci�n del idioma si el cliente no es un mantenimiento
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (param.getMantenimiento() == false)) {
            if (sDesL != null) {
              sDes = sDesL;
            }
          }

          // a�ade un nodo
          data.addElement(new DataEnfCie(rs.getString("CD_ENFCIE"), sDes, sDesL,
                                         rs.getString("DSI_ENFERCIE")));
        }
        rs.close();
        st.close();

        break;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    data.trimToSize();
//    lFinProceso=System.currentTimeMillis();
//    lFin=System.currentTimeMillis();
//    // System_out.println("TT:"+(lFin-lInicio)+"-TC:"+(lFinConexion-lIniConexion)+"-TS:"+(lFinSelect-lIniSelect)+"-TP:"+(lFinProceso-lIniProceso));
    return data;
  }
}
