//Title:        Enfermedades CIE
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Norsistemas S.A.
//Company:      Norsistemas, S.A.
//Description:  Mantenimiento de enfermedades CIE

package enfcie;

import capp.CApp;
import capp.CLista;
import capp.CListaMantenimiento;
import capp.CMessage;
import comun.constantes;
import sapp.StubSrvBD;

public class CListaMantEnfCie
    extends CListaMantenimiento {

  // modos de operaci�n del servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  public CListaMantEnfCie(CApp a, int numColumnas, String tamanosColumnas,
                          String nombresColumnas,
                          StubSrvBD stubApplet, String servlet,
                          int seleccion_x_codigo, int seleccion_x_descripcion) {
    super(a, numColumnas, tamanosColumnas, nombresColumnas, stubApplet,
          servlet, seleccion_x_codigo, seleccion_x_descripcion);
  }

  //Constructor que activa/desativa botones en funci�n de autorizaciones
  // del usuario
  public CListaMantEnfCie(CApp a, int numColumnas, String tamanosColumnas,
                          String nombresColumnas,
                          StubSrvBD stubApplet, String servlet,
                          int seleccion_x_codigo, int seleccion_x_descripcion,
                          boolean USU) {
    super(a, numColumnas, tamanosColumnas, nombresColumnas, stubApplet,
          servlet, seleccion_x_codigo, seleccion_x_descripcion,
          constantes.keyBtnAnadir,
          constantes.keyBtnModificar,
          constantes.keyBtnBorrar,
          constantes.keyBtnAux
          );
  }

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {

    DialEnfCie dial;
    DataEnfCie datLinea;
    CMessage msgBox = null;
    CLista data = null;

    modoOperacion = modoESPERA;
    Inicializar();
    dial = new DialEnfCie(app, this, null, DialEnfCie.modoALTA);
    dial.show();

    if (dial.bAceptar) {
      datLinea = new DataEnfCie(dial.txtCod.getText(),
                                dial.txtDes.getText(),
                                dial.txtDesL.getText(),
                                dial.txtDesI.getText());
      lista.addElement(datLinea);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();

  }

  // modifica l�nea de la lista
  public void btnModificar_actionPerformed(int i) {
    DialEnfCie dial;
    DataEnfCie datLinea;
    CMessage msgBox = null;
    CLista data = null;

    modoOperacion = modoESPERA;
    Inicializar();

    datLinea = (DataEnfCie) (lista.elementAt(i));

    dial = new DialEnfCie(app, this, datLinea, DialEnfCie.modoMODIFICAR);
    dial.show();

    if (dial.bAceptar) {
      datLinea = new DataEnfCie(dial.txtCod.getText(),
                                dial.txtDes.getText(),
                                dial.txtDesL.getText(),
                                dial.txtDesI.getText());
      lista.removeElementAt(i);
      lista.insertElementAt(datLinea, i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();

  }

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed(int i) {
    DataEnfCie cat;

    modoOperacion = modoESPERA;
    Inicializar();

    //  los datos
    cat = (DataEnfCie) lista.elementAt(i);

    DialEnfCie panel = new DialEnfCie(this.app, this, cat, DialEnfCie.modoBAJA);

    panel.show();

    if (panel.bAceptar) {
      lista.removeElementAt(i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();

  }

  // rellena los par�metros
  public Object setComponente(String s) {
    return new DataEnfCie(s);
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(Object o) {
    DataEnfCie enf = (DataEnfCie) o;
    String sCod = enf.getCod();
    String sDes = enf.getDes();
//    String sDesI = enf.getDesI();

//    if ((this.app.getIdioma() > 0) && (enf.getDesL().length() > 0))
//      sDes = enf.getDesL();

    return (sCod + "&" + sDes);
  }

}
