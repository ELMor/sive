package infconsenf;

import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import enfermo.DataEnfermo;
import sapp.StubSrvBD;

public class Pan_InfConsEnf
    extends EPanel {
  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  final String strLOGO_CCAA = "images/ccaa.gif";

  final int erwCASOS_EDO = 1;

  public DataEnfermo datosPar = new DataEnfermo("CD_ENFERMO");

  // estructuras de datos
  protected Vector vAgrupaciones;
  protected Vector vTotales;
  protected Vector vCasos;
  protected CLista lista;
  public CApp app;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // Para pedir mas paginas
  protected Vector ultSemana = new Vector();

  // report
  ERW erw = null;

  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;
  Integer regMostrados = new Integer(0);

  public Pan_InfConsEnf(CApp a) {
    super(a);
    app = a;
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + "erw/ERWConsEnf.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  public void MostrarGrafica() {

  }

  // todo el informe
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // siguiente trama
  public void MasDatos() {

  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  protected boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    CLista param = new CLista();
    boolean elBoolean = true;

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      PrepararInforme();
      erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

      // repintado
      //AIC
      this.pack();
      erwClient.prv_setActivePage(0);
      //erwClient.refreshReport(true);

      elBoolean = true;
      this.setTotalRegistros("1");
      this.setRegistrosMostrados("1");
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();
      elBoolean = false;
    }

    modoOperacion = modoNORMAL;
    Inicializar();
    return elBoolean;
  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;
    String cadena = "";

    int iEtiqueta = 0;

    TemplateManager tm = null;

    // plantilla
    tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

    //E Cargar� el login del usuario
    tm.SetLabel("LOGIN", app.getLogin());

    // carga los citerios
    tm.SetLabel("LAB001", datosPar.get("CD_ENFERMO").toString());
    cadena = datosPar.getDS_APE1();
    if ( (datosPar.getDS_APE2() != null) && (!datosPar.getDS_APE2().equals(""))) {
      cadena += " " + datosPar.getDS_APE2();
    }
    if ( (datosPar.getDS_NOMBRE() != null) &&
        (!datosPar.getDS_NOMBRE().equals(""))) {
      cadena += ", " + datosPar.getDS_NOMBRE();
    }
    tm.SetLabel("LAB004", cadena);

    cadena = " ";
    if ( (datosPar.get("DS_DIREC") != null) &&
        (!datosPar.get("DS_DIREC").toString().equals(""))) {
      cadena = datosPar.get("DS_DIREC").toString();
    }
    tm.SetLabel("LAB007", cadena);

    cadena = " ";
    if ( (datosPar.get("DS_NUM") != null) &&
        (!datosPar.get("DS_NUM").toString().equals(""))) {
      cadena = datosPar.get("DS_NUM").toString();
    }
    tm.SetLabel("LAB009", cadena);

    cadena = " ";
    if ( (datosPar.get("DS_PISO") != null) &&
        (!datosPar.get("DS_PISO").toString().equals(""))) {
      cadena = datosPar.get("DS_PISO").toString();
    }
    tm.SetLabel("LAB011", cadena);

    cadena = " ";
    if ( (datosPar.get("DS_TELEF") != null) &&
        (!datosPar.get("DS_TELEF").toString().equals(""))) {
      cadena = datosPar.get("DS_TELEF").toString();
    }
    tm.SetLabel("LAB013", cadena);

    cadena = " ";
    if ( (datosPar.get("CD_POSTAL") != null) &&
        (!datosPar.get("CD_POSTAL").toString().equals(""))) {
      cadena = datosPar.get("CD_POSTAL").toString();
    }
    tm.SetLabel("LAB015", cadena);

    cadena = " ";
    if ( (datosPar.get("DS_MUN") != null) &&
        (!datosPar.get("DS_MUN").toString().equals(""))) {
      cadena = datosPar.get("DS_MUN").toString();
    }
    tm.SetLabel("LAB017", cadena);

    cadena = " ";
    if ( (datosPar.get("DS_SEXO") != null) &&
        (!datosPar.get("DS_SEXO").toString().equals(""))) {
      cadena = datosPar.get("DS_SEXO").toString();
    }
    tm.SetLabel("LAB019", cadena);

    cadena = datosPar.getFechaNacimiento();
    tm.SetLabel("LAB021", cadena);

    //tm.SetLabel("LAB023", datosPar.get("CD_VIA").toString());

    cadena = " ";
    if ( (datosPar.get("DS_ZBS") != null) &&
        (!datosPar.get("DS_ZBS").toString().equals(""))) {
      cadena = datosPar.get("DS_ZBS").toString();
    }
    tm.SetLabel("LAB025", cadena);

    cadena = " ";
    if ( (datosPar.get("DS_NIVEL_2") != null) &&
        (!datosPar.get("DS_NIVEL_2").toString().equals(""))) {
      cadena = datosPar.get("DS_NIVEL_2").toString();
    }
    tm.SetLabel("LAB027", cadena);

    cadena = " ";
    if ( (datosPar.get("DS_NIVEL_1") != null) &&
        (!datosPar.get("DS_NIVEL_1").toString().equals(""))) {
      cadena = datosPar.get("DS_NIVEL_1").toString();
    }
    tm.SetLabel("LAB029", cadena);

  }
}
