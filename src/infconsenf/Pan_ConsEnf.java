package infconsenf;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

//import capp.CPanel;
//import com.borland.jbcl.control.*;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
//import com.borland.jbcl.layout.*;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import enfermo.DataEnfermo;
import enfermo.DialBusEnfermo;
import notutil.Comunicador;
import sapp.StubSrvBD;

public class Pan_ConsEnf
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  // modos de operaci�n del servlet SrvEnfermo
  final int modoDATOSENFERMO = 10;
  // servlet
  final String strSERVLET_ENFERMO = "servlet/SrvEnfermo";

  //rutas imagenes
  final String imgLUPA = "images/Magnify.gif";
  final String imgLIMPIAR = "images/vaciar.gif";
  final String imgGENERAR = "images/grafico.gif";

  protected StubSrvBD stubCliente = new StubSrvBD();

  protected int modoOperacion = modoNORMAL;

  protected Pan_InfConsEnf informe;

  public String sStringBk = "";
  CLista data = new CLista();
  protected CLista listaEnfermo = null;
  public DataEnfermo paramC2 = null;
  CLista datan = null;
  CLista listaNivAsis = null;

  // datos del enfermo seleccionado
  DataEnfermo elEnfermo = new DataEnfermo("CD_ENFERMO");

  XYLayout xYLayout = new XYLayout();

  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  CheckboxGroup checkboxGroup2 = new CheckboxGroup();
  TextField txtCodigo = new CCampoCodigo();
  ButtonControl btnEnfermo = new ButtonControl();
  TextField txtNombre = new TextField();
  Label lblCodigo = new Label();
  Label lblNombre = new Label();

  public Pan_ConsEnf(CApp a) {
    try {

      setApp(a);
      res = ResourceBundle.getBundle("infconsenf.Res" + a.getIdioma());
      informe = new Pan_InfConsEnf(a);
      jbInit();
      informe.setEnabled(false);

      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(150);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));
    txtCodigo.setBackground(new Color(255, 255, 150));
    txtCodigo.addKeyListener(new Pan_ConsEnf_txtCodigo_keyAdapter(this));
    txtCodigo.addFocusListener(new Pan_ConsEnf_txtCodigo_focusAdapter(this));
    btnEnfermo.setActionCommand("buscarEnfermo");
    txtNombre.setEditable(false);
    lblCodigo.setText(res.getString("lblCodigo.Text"));
    lblNombre.setText(res.getString("lblNombre.Text"));
    btnEnfermo.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnEnfermo.addActionListener(new Pan_ConsEnf_btnEnfermo_actionAdapter(this));

    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    xYLayout.setWidth(442);

    this.setLayout(xYLayout);

    this.add(btnLimpiar, new XYConstraints(277, 111, -1, -1));
    this.add(btnInforme, new XYConstraints(363, 111, -1, -1));
    this.add(txtCodigo, new XYConstraints(133, 7, 77, -1));
    this.add(btnEnfermo, new XYConstraints(215, 7, -1, -1));
    this.add(txtNombre, new XYConstraints(133, 40, 287, -1));
    this.add(lblCodigo, new XYConstraints(13, 7, -1, -1));
    this.add(lblNombre, new XYConstraints(13, 40, 116, -1));

    btnLimpiar.setImageURL(new URL(app.getCodeBase(), imgLIMPIAR));
    btnLimpiar.addActionListener(new Pan_ConsEnf_btnLimpiar_actionAdapter(this));
    btnInforme.setImageURL(new URL(app.getCodeBase(), imgGENERAR));
    btnInforme.addActionListener(new Pan_ConsEnf_btnInforme_actionAdapter(this));

    this.modoOperacion = modoNORMAL;
    Inicializar();

  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;
    String msg = "";
    CMessage msgBox;

    // Comprobacion de las fechas
    if ( (txtNombre.getText().length() == 0)) {
      bDatosCompletos = false;
      msg = res.getString("msg3.Text");
    }

    if (!bDatosCompletos) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, msg);
      msgBox.show();
      msgBox = null;
    }
    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtCodigo.setEnabled(true);
        btnEnfermo.setEnabled(true);
        txtNombre.setEnabled(true);
        txtNombre.setEditable(false);

        if (this.txtNombre.getText().length() > 0) {

        }
        else {

        }

        //txtDesDistrito.setEnabled(true);

        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCodigo.setEnabled(false);
        btnEnfermo.setEnabled(false);
        txtNombre.setEnabled(false);
        txtNombre.setEditable(false);

        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
    this.doLayout();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    modoOperacion = modoESPERA;
    Inicializar();

    txtCodigo.setText("");
    txtNombre.setText("");
    sStringBk = "";

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnInforme_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    //paramC2 = new DataEnfermo("CD_ENFERMO");

    if (isDataValid()) {
      modoOperacion = modoESPERA;
      Inicializar();
      // habilita el panel
      //elEnfermo = (DataEnfermo) listaEnfermo.firstElement();
      modoOperacion = modoESPERA;
      Inicializar();
      informe.setEnabled(true);
      informe.datosPar = elEnfermo;
      if (informe.GenerarInforme()) {
        informe.show();
      }
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg4.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  void btnEnfermo_actionPerformed(ActionEvent e) {
    DialBusEnfermo dlgEnfermo = null;
    CMessage msgBox = null;
    String cadena = "";

    modoOperacion = modoESPERA;
    Inicializar();

    dlgEnfermo = new DialBusEnfermo(this.getApp());
    dlgEnfermo.show();
    listaEnfermo = new CLista();
    listaEnfermo = dlgEnfermo.getListaDatosEnfermo();

    if (listaEnfermo != null) {
      elEnfermo = (DataEnfermo) listaEnfermo.firstElement();
      cadena = elEnfermo.getDS_APE1() + " " + elEnfermo.getDS_APE2();

      if (elEnfermo.getDS_NOMBRE() != null) {
        cadena += ", " + elEnfermo.getDS_NOMBRE();
      }
      txtCodigo.setText(elEnfermo.get("CD_ENFERMO").toString());
      txtNombre.setText(cadena);
      sStringBk = txtCodigo.getText();
    }

    modoOperacion = modoNORMAL;
    Inicializar();

  }

  void txtCodigo_keyPressed(KeyEvent e) {
    txtNombre.setText("");
  }

  void txtCodigo_focusLost(FocusEvent e) {
    String sStringDespues = txtCodigo.getText();
    String cadena = "";
    CMessage msgBox;

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();

    if (sStringBk.equals(sStringDespues)) {
      return;
    }

    if (txtCodigo.getText().equals("")) {
      txtCodigo.setText(sStringBk);
      modoOperacion = modo;
      Inicializar();
      return;
    }

    //COMPROBAR NUMERICO ****
    boolean bEnf = true;
    try {
      int iCodEnfermo = Integer.parseInt(txtCodigo.getText());
      if (iCodEnfermo < 0) {
        bEnf = false;
      }
      if (iCodEnfermo > 999999) {
        bEnf = false;
      }
    }
    catch (Exception erf) {
      bEnf = false;
    }

    if (!bEnf) {
      msgBox = new CMessage(this.app,
                            CMessage.msgAVISO, res.getString("msg5.Text"));
      msgBox.show();
      msgBox = null;
      txtCodigo.selectAll();
      txtCodigo.setText(sStringBk);
      //Reset_Enfermo();
      modoOperacion = modo;
      Inicializar();
      return;
    }

    data = new CLista();
    DataEnfermo hash = new DataEnfermo("CD_ENFERMO");
    hash.put("CD_ENFERMO", txtCodigo.getText());

    data.addElement(hash);

    data.setIdioma(app.getIdioma());
    try {

      listaEnfermo = Comunicador.Communicate(this.getApp(),
                                             stubCliente,
                                             modoDATOSENFERMO,
                                             strSERVLET_ENFERMO,
                                             data);

      if (listaEnfermo != null) {
        if (listaEnfermo.size() == 0) {
          CMessage elMensaje = new CMessage(this.app, CMessage.msgERROR,
                                            res.getString("msg6.Text"));
          elMensaje.show();
          txtCodigo.setText(sStringBk);

        }
        else {
          elEnfermo = (DataEnfermo) listaEnfermo.firstElement();
          cadena = elEnfermo.getDS_APE1();

          if (elEnfermo.getDS_APE2() != null) {
            cadena += " " + elEnfermo.getDS_APE2();

          }
          if (elEnfermo.getDS_NOMBRE() != null) {
            cadena += ", " + elEnfermo.getDS_NOMBRE();

          }
          txtNombre.setText(cadena);
          sStringBk = txtCodigo.getText();
        }
      }
    }
    catch (Exception erf) {
      erf.printStackTrace();
      txtCodigo.setText(sStringBk);
    }

    sStringBk = txtCodigo.getText(); //grabamos
    modoOperacion = modo;
    Inicializar();
  }

} //clase

class Pan_ConsEnf_btnLimpiar_actionAdapter
    implements java.awt.event.ActionListener {
  Pan_ConsEnf adaptee;

  Pan_ConsEnf_btnLimpiar_actionAdapter(Pan_ConsEnf adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnLimpiar_actionPerformed(e);
  }
}

class Pan_ConsEnf_btnInforme_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_ConsEnf adaptee;
  ActionEvent e;

  Pan_ConsEnf_btnInforme_actionAdapter(Pan_ConsEnf adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnInforme_actionPerformed(e);
  }
}

class Pan_ConsEnf_btnEnfermo_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_ConsEnf adaptee;
  ActionEvent e;

  Pan_ConsEnf_btnEnfermo_actionAdapter(Pan_ConsEnf adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnEnfermo_actionPerformed(e);
  }
}

class Pan_ConsEnf_txtCodigo_focusAdapter
    extends java.awt.event.FocusAdapter {
  Pan_ConsEnf adaptee;

  Pan_ConsEnf_txtCodigo_focusAdapter(Pan_ConsEnf adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.txtCodigo_focusLost(e);
  }
}

class Pan_ConsEnf_txtCodigo_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_ConsEnf adaptee;

  Pan_ConsEnf_txtCodigo_keyAdapter(Pan_ConsEnf adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCodigo_keyPressed(e);
  }
}
