package conscasoper;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class PanelInforme
    extends EPanel {

  PnlParamC13 pan;
  ResourceBundle res;

  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  final String strSERVLET = "servlet/SrvConsCasoPer";
  final String strLOGO_CCAA = "images/ccaa.gif";

  final int erwCASOS_EDO_PERIODO = 1;

  // estructuras de datos
  protected Vector vCasos;
  protected Vector vTotales;
  protected CLista lista;

  // modo de operaci�n
  protected int modoOperacion;
  protected int modoServlet;

  // conexion con el servlet
  protected StubSrvBD stub;

  // parametros de consulta
  DataC13 paramC1 = null;

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  public PanelInforme(CApp a, PnlParamC13 miPanel) {
    super(a);
    res = ResourceBundle.getBundle("conscasoper.Res" + a.getIdioma());
    pan = (PnlParamC13) miPanel;
    try {
      stub = new StubSrvBD();
      jbInit();
      //paramC1 = (DataC13) miPanel.paramC1;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + "erw/ERWCasosNotPer.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar como un dialogo
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;

    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        PrepararInforme();

        // obtiene los datos del servidor
        paramC1.iPagina++;
        param.addElement(paramC1);
        param.setIdioma(app.getIdioma());
        param.trimToSize();
        stub.setUrl(new URL(app.getURL() + strSERVLET));

        // JRM (28/06/01): Para LORTAD
        param.setLogin(app.getLogin());
        param.setLortad(app.getParameter("LORTAD"));

        lista = (CLista) stub.doPost(modoServlet, param);
        v = (Vector) lista.elementAt(0);
        vTotales = (Vector) lista.elementAt(1);

        for (int j = 0; j < v.size(); j++) {
          vCasos.addElement(v.elementAt(j));

          // control de registros
        }
        this.setTotalRegistros(vTotales.elementAt(0).toString());
        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // repintado
        erwClient.refreshReport(true);

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // informe comleto
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  protected boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    CLista param = new CLista();

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      PrepararInforme();

      // obtiene los datos del servidor
      paramC1.iPagina = 0;
      paramC1.bInformeCompleto = conTodos;
      param.addElement(paramC1);
      param.setIdioma(app.getIdioma());
      param.trimToSize();

      modoServlet = erwCASOS_EDO_PERIODO;
      stub.setUrl(new URL(app.getURL() + strSERVLET));

      // JRM(28/06/01): Para LORTAD
      param.setLogin(app.getLogin());
      param.setLortad(app.getParameter("LORTAD"));

      lista = (CLista) stub.doPost(modoServlet, param);

      vCasos = (Vector) lista.elementAt(0);
      vTotales = (Vector) lista.elementAt(1);

      // control de registros
      if (vTotales.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg3.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return false;
      }
      else {

        this.setTotalRegistros(vTotales.elementAt(0).toString());
        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // establece las matrices de datos
        Vector retval = new Vector();
        retval.addElement("CD_CENTRO = CD_CENTRO");
        retval.addElement("DS_CENTRO = DS_CENTRO");
        retval.addElement("CD_E_NOTIF = CD_E_NOTIF");
        retval.addElement("DS_E_NOTIF = DS_E_NOTIF");
        retval.addElement("CD_ENFCIE1 = CD_ENFCIE1");
        retval.addElement("DS_ENFCIE1 = DS_ENFCIE1");
        retval.addElement("SUMA1 = SUMA1");
        retval.addElement("CD_ENFCIE2 = CD_ENFCIE2");
        retval.addElement("DS_ENFCIE2 = DS_ENFCIE2");
        retval.addElement("SUMA2 = SUMA2");
        retval.addElement("CD_ENFCIE3 = CD_ENFCIE3");
        retval.addElement("DS_ENFCIE3 = DS_ENFCIE3");
        retval.addElement("SUMA3 = SUMA3");
        retval.addElement("CD_ENFCIE4 = CD_ENFCIE4");
        retval.addElement("DS_ENFCIE4 = DS_ENFCIE4");
        retval.addElement("SUMA4 = SUMA4");
        retval.addElement("CD_ENFCIE5 = CD_ENFCIE5");
        retval.addElement("DS_ENFCIE5 = DS_ENFCIE5");
        retval.addElement("SUMA5 = SUMA5");
        retval.addElement("CD_ENFCIE6 = CD_ENFCIE6");
        retval.addElement("DS_ENFCIE6 = DS_ENFCIE6");
        retval.addElement("SUMA6 = SUMA6");
        retval.addElement("CD_ENFCIE7 = CD_ENFCIE7");
        retval.addElement("DS_ENFCIE7 = DS_ENFCIE7");
        retval.addElement("SUMA7 = SUMA7");
        retval.addElement("CD_ENFCIE8 = CD_ENFCIE8");
        retval.addElement("DS_ENFCIE8 = DS_ENFCIE8");
        retval.addElement("SUMA8 = SUMA8");
        retval.addElement("CD_ENFCIE9 = CD_ENFCIE9");
        retval.addElement("DS_ENFCIE9 = DS_ENFCIE9");
        retval.addElement("SUMA9 = SUMA9");
        retval.addElement("CD_ENFCIE10 = CD_ENFCIE10");
        retval.addElement("DS_ENFCIE10 = DS_ENFCIE10");
        retval.addElement("SUMA10 = SUMA10");
        retval.addElement("CD_ENFCIE11 = CD_ENFCIE11");
        retval.addElement("DS_ENFCIE11 = DS_ENFCIE11");
        retval.addElement("SUMA11 = SUMA11");
        retval.addElement("CD_ENFCIE12 = CD_ENFCIE12");
        retval.addElement("DS_ENFCIE12 = DS_ENFCIE12");
        retval.addElement("SUMA12 = SUMA12");
        dataHandler.RegisterTable(vCasos, "SIVE_C_9_3_13", retval, null);
        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        modoOperacion = modoNORMAL;
        Inicializar();
        return true;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();

      modoOperacion = modoNORMAL;
      Inicializar();
      return false;
    }

  }

  // este informe no tiene grafica
  public void MostrarGrafica() {
  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;
    int iEtiqueta = 0;
    CMessage msgBox;

    String sEtiqueta[] = {
        "CRITERIO1",
        "CRITERIO2",
        "CRITERIO3"};

    String sColumna[] = {
        "SUM1",
        "SUM2",
        "SUM3",
        "SUM4",
        "SUM5",
        "SUM6",
        "SUM7",
        "SUM8",
        "SUM9",
        "SUM10",
        "SUM11",
        "SUM12"};

    String sCampo[] = {
        "FIE000",
        "FIE001",
        "FIE002",
        "FIE003",
        "FIE004",
        "FIE005",
        "FIE006",
        "FIE007",
        "FIE008",
        "FIE009",
        "FIE010",
        "FIE011"};

    String sLinea[] = {
        "LIN1",
        "LIN2",
        "LIN3",
        "LIN4",
        "LIN5",
        "LIN6",
        "LIN7",
        "LIN8",
        "LIN9",
        "LIN10",
        "LIN11",
        "LIN12"};

    String sFormula[] = {
        "FOR003",
        "FOR004",
        "FOR005",
        "FOR006",
        "FOR007",
        "FOR008",
        "FOR009",
        "FOR010",
        "FOR011",
        "FOR012",
        "FOR013",
        "FOR014"};

    String sCodigo[] = {
        "LABCOD1", "LABCOD2", "LABCOD3",
        "LABCOD4", "LABCOD5", "LABCOD6",
        "LABCOD7", "LABCOD8", "LABCOD9",
        "LABCOD10", "LABCOD11", "LABCOD12"};

    String sDescripcion[] = {
        "LABDES1", "LABDES2", "LABDES3",
        "LABDES4", "LABDES5", "LABDES6",
        "LABDES7", "LABDES8", "LABDES9",
        "LABDES10", "LABDES11", "LABDES12"};

    try {
      // plantilla
      TemplateManager tm = erw.GetTemplateManager();

      // carga los logos
      tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
      tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

      // carga los citerios

      tm.SetLabel(sEtiqueta[iEtiqueta],
                  res.getString("msg4.Text") + paramC1.sAnyoDesde
                  + res.getString("msg5.Text") + paramC1.sSemDesde + " , "
                  + res.getString("msg6.Text") + paramC1.sAnyoHasta +
                  res.getString("msg7.Text") + paramC1.sSemHasta);
      iEtiqueta++;

      // nivel1
      if (paramC1.sNivel1.length() > 0) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    app.getNivel1() + ": " + paramC1.sNivel1 + " " +
                    pan.txtDesAre.getText());
        // nivel2
        if (paramC1.sNivel2.length() > 0) {
          tm.SetLabel(sEtiqueta[iEtiqueta],
                      tm.GetLabel(sEtiqueta[iEtiqueta]) + " " + app.getNivel2() +
                      ": " + paramC1.sNivel2 + " " + pan.txtDesDis.getText());
          // zbs
          if (paramC1.sZBS.length() > 0) {
            // Corregido "28-06-2001 ARS, para que ponga la descripci�n de la zona B.S.
            tm.SetLabel(sEtiqueta[iEtiqueta],
                        tm.GetLabel(sEtiqueta[iEtiqueta]) + " " +
                        res.getString("msg8.Text") + ": " + paramC1.sZBS + " " +
                        pan.txtDesZBS.getText());
          }
        }
        iEtiqueta++;
      }

      // prov
      if (paramC1.sProvincia.length() > 0) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    res.getString("msg9.Text") + paramC1.sProvincia + " " +
                    pan.txtDesPro.getText());
        //mun
        if (paramC1.sMunicipio.length() > 0) {
          tm.SetLabel(sEtiqueta[iEtiqueta],
                      tm.GetLabel(sEtiqueta[iEtiqueta]) + " " +
                      res.getString("msg10.Text") + paramC1.sMunicipio + " " +
                      pan.txtDesMun.getText());
        }
        iEtiqueta++;
      }

      // oculta criterios no informados
      for (int i = iEtiqueta; i < 3; i++) {
        tm.SetLabel(sEtiqueta[i], "");
      }

      // oculta columnas que no contengan datos
      for (int i = 0; i < 12; i++) {
        tm.SetVisible(sColumna[i], (i < paramC1.iNumEnf));
        tm.SetVisible(sCampo[i], (i < paramC1.iNumEnf));
        tm.SetVisible(sLinea[i], (i < paramC1.iNumEnf));
        tm.SetVisible(sFormula[i], (i < paramC1.iNumEnf));
        tm.SetVisible(sCodigo[i], (i < paramC1.iNumEnf));
        tm.SetVisible(sDescripcion[i], (i < paramC1.iNumEnf));
      }

      // Se escribe el r�tulo de la codificaci�n de enfermedades
      tm.SetLabel("LABTEXCOD", res.getString("msg11.Text"));

      // Se ponen en el informe los c�digos de las enfermedades y sus descripciones
      for (int ku = 0; ku < paramC1.iNumEnf; ku++) {
        tm.SetLabel(sCodigo[ku],
                    (String) ( (java.util.Hashtable) paramC1.listaEnfermedades.
                              elementAt(ku)).get("CD_ENFCIE") + ":");
        tm.SetLabel(sDescripcion[ku],
                    (String) ( (java.util.Hashtable) paramC1.listaEnfermedades.
                              elementAt(ku)).get("DS_PROCESO"));
        tm.SetLabel(sColumna[ku],
                    (String) ( (java.util.Hashtable) paramC1.listaEnfermedades.
                              elementAt(ku)).get("CD_ENFCIE"));
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

  }

}
