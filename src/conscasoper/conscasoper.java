package conscasoper;

import java.util.ResourceBundle;

import capp.CApp;
import sapp.StubSrvBD;

public class conscasoper
    extends CApp {

  protected StubSrvBD stubCliente = new StubSrvBD();
  ResourceBundle res;

  public conscasoper() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("conscasoper.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;

    VerPanel(res.getString("msg2.Text"), new PnlParamC13(a), false);
    VerPanel(res.getString("msg2.Text"));

  }

  public void stop() {
  }

} //__________________________________________________ END_CLASS
