package cargamodelo;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import capp.CApp;
import capp.CLista;
import capp.CMessage;
import sapp.StubSrvBD;

public class partirFichero {
  // applet
  CApp app;
  ResourceBundle res;

  // vectores de datos
  Vector vectorLin;
  Vector vLineasItem;
  Vector vLineasM;
  Vector vListas;
  Vector vListaValores;
  Vector vPreguntas;
  CLista vectorBloque;

  // arrays de datos
  Object[] arrayNombres;
  Object[] arrayRegAux;
  Object[][] arrayRegistros;

  // variables de las tablas
  int numCampos = 0;
  int numRegistros = 0;
  int numCamposClave = 0;

  // acceso a la bbdd
  protected StubSrvBD stubCliente = null;
  final String strSERVLET = "servlet/SrvCarga";
  final int servletALTA = 0;

  public boolean cargaVectorNoCod(String theFichero, String theCodigo) {
    CMessage msgBox;

    File miFichero;
    FileReader fStream;
    String sLinea;
    LineNumberReader lector;
    boolean elBoolean = true;
    int tablaNum = 0;
    int contador = 0;
    int caracteres = 0;
    String tablaActual = "";
    String[] arrayLinea;
    java.util.Date fechaActual;
    int anyo = 0;
    int mes = 0;
    int dia = 0;

    try {
      miFichero = new File(theFichero);
      fStream = new FileReader(miFichero);
      lector = new LineNumberReader(fStream);

      vectorLin = new Vector();
      vLineasItem = new Vector();
      vLineasM = new Vector();
      vListas = new Vector();
      vListaValores = new Vector();
      vPreguntas = new Vector();

      contador = 0;

      while ( (sLinea = lector.readLine()) != null) {
        contador++;

        if (sLinea.equals("[listas]")) {
          tablaNum = 0;
          tablaActual = "SIVE_LISTAS";
          caracteres = 91;
          numCampos = 3;
          numCamposClave = 0;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_LISTA";
          arrayNombres[1] = "DS_LISTA";
          arrayNombres[2] = "DSL_LISTA";
          arrayRegAux = null;
          arrayRegAux = new Object[numCampos];
        }
        else if (sLinea.equals("[valores]")) {
          tablaNum = 1;
          tablaActual = "SIVE_LISTA_VALORES";
          caracteres = 84;
          numCampos = 4;
          numCamposClave = 0;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_LISTA";
          arrayNombres[1] = "CD_LISTAP";
          arrayNombres[2] = "DS_LISTAP";
          arrayNombres[3] = "DSL_LISTAP";
          arrayRegAux = null;
          arrayRegAux = new Object[numCampos];
        }
        else if (sLinea.equals("[preguntas]")) {
          tablaNum = 2;
          tablaActual = "SIVE_PREGUNTA";
          caracteres = 113;
          numCampos = 11;
          numCamposClave = 0;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_TSIVE";
          arrayNombres[1] = "CD_PREGUNTA";
          arrayNombres[2] = "CD_LISTA";
          arrayNombres[3] = "DS_PREGUNTA";
          arrayNombres[4] = "DSL_PREGUNTA";
          arrayNombres[5] = "CD_TPREG";
          arrayNombres[6] = "NM_LONG";
          arrayNombres[7] = "NM_ENT";
          arrayNombres[8] = "NM_DEC";
          arrayNombres[9] = "CD_OPE";
          arrayNombres[10] = "FC_ULTACT";
          arrayRegAux = null;
          arrayRegAux = new Object[numCampos];
        }
        else if (sLinea.equals("[modelo]")) {
          tablaNum = 3;
          tablaActual = "SIVE_MODELO";
          caracteres = 145;
          numCampos = 13;
          numCamposClave = 0;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_TSIVE";
          arrayNombres[1] = "CD_MODELO";
          arrayNombres[2] = "DS_MODELO";
          arrayNombres[3] = "CD_NIVEL_1";
          arrayNombres[4] = "DSL_MODELO";
          arrayNombres[5] = "CD_NIVEL_2";
          arrayNombres[6] = "CD_CA";
          arrayNombres[7] = "FC_ALTAM";
          arrayNombres[8] = "IT_OK";
          arrayNombres[9] = "FC_BAJAM";
          arrayNombres[10] = "CD_OPE";
          arrayNombres[11] = "CD_ENFCIE";
          arrayNombres[12] = "FC_ULTACT";
          arrayRegAux = null;
          arrayRegAux = new Object[numCampos];

        }
        else if (sLinea.equals("[lineas]")) {
          tablaNum = 4;
          tablaActual = "SIVE_LINEASM";
          caracteres = 96;
          numCampos = 6;
          numCamposClave = 0;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_TSIVE";
          arrayNombres[1] = "CD_MODELO";
          arrayNombres[2] = "NM_LIN";
          arrayNombres[3] = "CD_TLINEA";
          arrayNombres[4] = "DS_TEXTO";
          arrayNombres[5] = "DSL_TEXTO";
          arrayRegAux = null;
          arrayRegAux = new Object[numCampos];
        }
        else if (sLinea.equals("[detalle linea]")) {
          tablaNum = 5;
          tablaActual = "SIVE_LINEA_ITEM";
          caracteres = 102;
          numCampos = 9;
          numCamposClave = 0;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_TSIVE";
          arrayNombres[1] = "CD_MODELO";
          arrayNombres[2] = "NM_LIN";
          arrayNombres[3] = "CD_PREGUNTA";
          arrayNombres[4] = "CD_PREGUNTA_PC";
          arrayNombres[5] = "IT_OBLIGATORIO";
          arrayNombres[6] = "IT_CONDP";
          arrayNombres[7] = "NM_LIN_PC";
          arrayNombres[8] = "DS_VPREGUNTA_PC";
          arrayRegAux = null;
          arrayRegAux = new Object[numCampos];
        }
        else if (sLinea.length() == caracteres) { // es un registro a a�adir
          switch (tablaNum) {
            case 0: //tabla listas

              // c�digo
              arrayRegAux[0] = (String) sLinea.substring(0, 11).trim();
              // descripci�n
              arrayRegAux[1] = (String) sLinea.substring(11, 51).trim();
              // descripci�n local
              arrayRegAux[2] = (String) sLinea.substring(51, 91).trim();
              if (arrayRegAux[2].equals("")) {
                arrayRegAux[2] = null;
                // a�adimos el dato
              }
              vListas.addElement(arrayRegAux);
              arrayRegAux = null;
              arrayRegAux = new Object[numCampos];
              break;
            case 1: //tabla lista valores

              // c�digo
              arrayRegAux[0] = (String) sLinea.substring(0, 11).trim();
              // c�digo listap
              arrayRegAux[1] = (String) sLinea.substring(11, 24).trim();
              // descripci�n
              arrayRegAux[2] = (String) sLinea.substring(24, 54).trim();
              // descripci�n local
              arrayRegAux[3] = (String) sLinea.substring(54, 84).trim();
              if (arrayRegAux[3].equals("")) {
                arrayRegAux[3] = null;
                // guardamos el elemento
              }
              vListaValores.addElement(arrayRegAux);
              arrayRegAux = null;
              arrayRegAux = new Object[numCampos];
              break;
            case 2: //tabla preguntas

              // c�digo tsive
              arrayRegAux[0] = (String) sLinea.substring(0, 1).trim();
              // c�digo pregunta
              arrayRegAux[1] = (String) sLinea.substring(1, 12).trim();
              // c�digo lista
              arrayRegAux[2] = (String) sLinea.substring(12, 23).trim();
              //# System_Out.println("l�nea: "+Integer.toString(contador));
              //# System_Out.println(arrayRegAux[2]);
              //# System_Out.println(((String)arrayRegAux[2]).length());
              if (arrayRegAux[2].equals("")) {
                //# System_Out.println("Contador: "+Integer.toString(contador));
                //# System_Out.println("antes");
                arrayRegAux[2] = null;
                //# System_Out.println("desp");
              }

              // descripci�n pregunta
              arrayRegAux[3] = (String) sLinea.substring(23, 63).trim();
              // descripci�n local
              arrayRegAux[4] = (String) sLinea.substring(63, 103).trim();
              if (arrayRegAux[4].equals("")) {
                //# System_Out.println("antes");
                arrayRegAux[4] = null;
                //# System_Out.println("desp");
              }

              // c�digo tpreg
              arrayRegAux[5] = sLinea.substring(103, 104).trim();
              // nm_long
              arrayRegAux[6] = new Integer(Integer.parseInt(sLinea.substring(
                  104, 107).trim()));
              // nm_ent
              arrayRegAux[7] = new Integer(Integer.parseInt(sLinea.substring(
                  107, 110).trim()));
              // nm_dec
              arrayRegAux[8] = new Integer(Integer.parseInt(sLinea.substring(
                  110, 113).trim()));
              // c�digo operador
              arrayRegAux[9] = (String)this.app.getLogin();
              // fecha actual
              arrayRegAux[10] = new java.sql.Date( (new java.util.Date()).
                                                  getTime());
              // guardamos
              vPreguntas.addElement(arrayRegAux);
              arrayRegAux = null;
              arrayRegAux = new Object[numCampos];
              break;

            case 4: //tabla l�neas modelo

              // c�digo tsive
              arrayRegAux[0] = (String) sLinea.substring(0, 1).trim();
              // c�digo modelo
              arrayRegAux[1] = theCodigo;
              // n�mero l�nea
              arrayRegAux[2] = new Integer(Integer.parseInt(sLinea.substring(12,
                  15).trim()));
              // c�digo tlinea
              arrayRegAux[3] = (String) sLinea.substring(15, 16).trim();
              // descripci�n texto
              arrayRegAux[4] = (String) sLinea.substring(16, 56).trim();
              // descripci�n local texto
              arrayRegAux[5] = sLinea.substring(56, 96).trim();
              if (arrayRegAux[5].equals("")) {
                arrayRegAux[5] = null;
                // guardamos
              }
              vLineasM.addElement(arrayRegAux);
              arrayRegAux = null;
              arrayRegAux = new Object[numCampos];
              break;
            case 5: //tabla l�neas �tem

              // c�digo tsive
              arrayRegAux[0] = (String) sLinea.substring(0, 1).trim();
              // c�digo modelo
              arrayRegAux[1] = theCodigo;
              // n�mero l�nea
              arrayRegAux[2] = (Integer)new Integer(Integer.parseInt(sLinea.
                  substring(12, 15).trim()));
              // c�digo pregunta
              arrayRegAux[3] = (String) sLinea.substring(15, 26).trim();
              // c�digo pregunta pc
              arrayRegAux[4] = (String) sLinea.substring(26, 37).trim();
              if (arrayRegAux[4].equals("")) {
                arrayRegAux[4] = null;
                // it obligatorio
              }
              arrayRegAux[5] = sLinea.substring(37, 38).trim();
              // it condp
              arrayRegAux[6] = sLinea.substring(38, 39).trim();
              // nm lin pc
              arrayRegAux[7] = (Integer)new Integer(Integer.parseInt(sLinea.
                  substring(39, 42).trim()));
              if (arrayRegAux[7].equals("")) {
                arrayRegAux[7] = null;
                // ds vpreg pc
              }
              arrayRegAux[8] = (String) sLinea.substring(42, 102).trim();
              if (arrayRegAux[8].equals("")) {
                arrayRegAux[8] = null;
                // guardamos
              }
              vLineasItem.addElement(arrayRegAux);
              arrayRegAux = null;
              arrayRegAux = new Object[numCampos];
              break;
          }

        }
        else { // ���el tama�o de la l�nea es err�nea!!!
          elBoolean = false;
          //# System_Out.println("Error en el fichero l�nea: "+Integer.toString(contador));
        }
      }

      fStream.close();
      // ahora se hace un bucle que rellene el vector
      // metiendo todos los datos que tenemos en los vectores
      vectorBloque = null;
      vectorBloque = new CLista();

      if (vListas.size() > 0) { // listas
        numCampos = 3;
        numRegistros = vListas.size();
        tablaActual = "SIVE_LISTAS";
        arrayRegistros = null;
        arrayRegistros = new Object[numCampos][numRegistros];
        arrayNombres = null;
        arrayNombres = new String[numCampos];
        arrayNombres[0] = "CD_LISTA";
        arrayNombres[1] = "DS_LISTA";
        arrayNombres[2] = "DSL_LISTA";
        // recorremos el vector
        for (int j = 0; j < numRegistros; j++) {
          arrayRegAux = null;
          arrayRegAux = new Object[numCampos];
          arrayRegAux = (Object[]) vListas.elementAt(j);
          for (int z = 0; z < numCampos; z++) {
            arrayRegistros[z][j] = arrayRegAux[z];
          }
        }
        // almacenamos el array
        vectorLin = null;
        vectorLin = new Vector();

        // a�adimos el nombre de la tabla
        vectorLin.addElement(tablaActual);
        // a�adimos el n�mero de campos
        vectorLin.addElement(Integer.toString(numCampos));
        // a�adimos los nombres de los campos
        vectorLin.addElement(arrayNombres);
        // a�adimos el n�mero de registros
        vectorLin.addElement(Integer.toString(numRegistros));
        // a�adimos los registros
        vectorLin.addElement(arrayRegistros);
        // a�adimos el n�mero de campos c�digo
        vectorLin.addElement("1");

        // a�adimos todo al vector principal
        vectorBloque.addElement(vectorLin);
        vectorLin = null;
      }

      if (vListaValores.size() > 0) { // lista valores
        numCampos = 4;
        numRegistros = vListaValores.size();
        tablaActual = "SIVE_LISTA_VALORES";
        arrayRegistros = null;
        arrayRegistros = new Object[numCampos][numRegistros];
        arrayNombres = null;
        arrayNombres = new String[numCampos];
        arrayNombres[0] = "CD_LISTA";
        arrayNombres[1] = "CD_LISTAP";
        arrayNombres[2] = "DS_LISTAP";
        arrayNombres[3] = "DSL_LISTAP";
        // recorremos el vector
        for (int j = 0; j < numRegistros; j++) {
          arrayRegAux = null;
          arrayRegAux = new Object[numCampos];
          arrayRegAux = (Object[]) vListaValores.elementAt(j);
          for (int z = 0; z < numCampos; z++) {
            arrayRegistros[z][j] = arrayRegAux[z];
          }
        }
        // almacenamos el array
        vectorLin = null;
        vectorLin = new Vector();

        // a�adimos el nombre de la tabla
        vectorLin.addElement(tablaActual);
        // a�adimos el n�mero de campos
        vectorLin.addElement(Integer.toString(numCampos));
        // a�adimos los nombres de los campos
        vectorLin.addElement(arrayNombres);
        // a�adimos el n�mero de registros
        vectorLin.addElement(Integer.toString(numRegistros));
        // a�adimos los registros
        vectorLin.addElement(arrayRegistros);
        // a�adimos el n�mero de campos c�digo
        vectorLin.addElement("1");

        // a�adimos todo al vector principal
        vectorBloque.addElement(vectorLin);
        vectorLin = null;
      }

      if (vPreguntas.size() > 0) { // preguntas
        numCampos = 11;
        numRegistros = vPreguntas.size();
        tablaActual = "SIVE_PREGUNTA";
        arrayRegistros = null;
        arrayRegistros = new Object[numCampos][numRegistros];
        arrayNombres = null;
        arrayNombres = new String[numCampos];
        arrayNombres[0] = "CD_TSIVE";
        arrayNombres[1] = "CD_PREGUNTA";
        arrayNombres[2] = "CD_LISTA";
        arrayNombres[3] = "DS_PREGUNTA";
        arrayNombres[4] = "DSL_PREGUNTA";
        arrayNombres[5] = "CD_TPREG";
        arrayNombres[6] = "NM_LONG";
        arrayNombres[7] = "NM_ENT";
        arrayNombres[8] = "NM_DEC";
        arrayNombres[9] = "CD_OPE";
        arrayNombres[10] = "FC_ULTACT";
        // recorremos el vector
        for (int j = 0; j < numRegistros; j++) {
          arrayRegAux = null;
          arrayRegAux = new Object[numCampos];
          arrayRegAux = (Object[]) vPreguntas.elementAt(j);
          for (int z = 0; z < numCampos; z++) {
            arrayRegistros[z][j] = arrayRegAux[z];
          }
        }
        // almacenamos el array
        vectorLin = null;
        vectorLin = new Vector();

        // a�adimos el nombre de la tabla
        vectorLin.addElement(tablaActual);
        // a�adimos el n�mero de campos
        vectorLin.addElement(Integer.toString(numCampos));
        // a�adimos los nombres de los campos
        vectorLin.addElement(arrayNombres);
        // a�adimos el n�mero de registros
        vectorLin.addElement(Integer.toString(numRegistros));
        // a�adimos los registros
        vectorLin.addElement(arrayRegistros);
        // a�adimos el n�mero de campos c�digo
        vectorLin.addElement("2");

        // a�adimos todo al vector principal
        vectorBloque.addElement(vectorLin);
        vectorLin = null;
      }

      if (vLineasM.size() > 0) { // lineas modelo
        numCampos = 6;
        numRegistros = vLineasM.size();
        tablaActual = "SIVE_LINEASM";
        arrayRegistros = null;
        arrayRegistros = new Object[numCampos][numRegistros];
        arrayNombres = null;
        arrayNombres = new String[numCampos];
        arrayNombres[0] = "CD_TSIVE";
        arrayNombres[1] = "CD_MODELO";
        arrayNombres[2] = "NM_LIN";
        arrayNombres[3] = "CD_TLINEA";
        arrayNombres[4] = "DS_TEXTO";
        arrayNombres[5] = "DSL_TEXTO";
        // recorremos el vector
        for (int j = 0; j < numRegistros; j++) {
          arrayRegAux = null;
          arrayRegAux = new Object[numCampos];
          arrayRegAux = (Object[]) vLineasM.elementAt(j);
          for (int z = 0; z < numCampos; z++) {
            arrayRegistros[z][j] = arrayRegAux[z];
          }
        }
        // almacenamos el array
        vectorLin = null;
        vectorLin = new Vector();

        // a�adimos el nombre de la tabla
        vectorLin.addElement(tablaActual);
        // a�adimos el n�mero de campos
        vectorLin.addElement(Integer.toString(numCampos));
        // a�adimos los nombres de los campos
        vectorLin.addElement(arrayNombres);
        // a�adimos el n�mero de registros
        vectorLin.addElement(Integer.toString(numRegistros));
        // a�adimos los registros
        vectorLin.addElement(arrayRegistros);
        // a�adimos el n�mero de campos c�digo
        vectorLin.addElement("3");

        // a�adimos todo al vector principal
        vectorBloque.addElement(vectorLin);
        vectorLin = null;
      }

      if (vLineasItem.size() > 0) { // l�neas �tem
        numCampos = 9;
        numRegistros = vLineasItem.size();
        tablaActual = "SIVE_LINEA_ITEM";
        arrayRegistros = null;
        arrayRegistros = new Object[numCampos][numRegistros];
        arrayNombres = null;
        arrayNombres = new String[numCampos];
        arrayNombres[0] = "CD_TSIVE";
        arrayNombres[1] = "CD_MODELO";
        arrayNombres[2] = "NM_LIN";
        arrayNombres[3] = "CD_PREGUNTA";
        arrayNombres[4] = "CD_PREGUNTA_PC";
        arrayNombres[5] = "IT_OBLIGATORIO";
        arrayNombres[6] = "IT_CONDP";
        arrayNombres[7] = "NM_LIN_PC";
        arrayNombres[8] = "DS_VPREGUNTA_PC";
        // recorremos el vector
        for (int j = 0; j < numRegistros; j++) {
          arrayRegAux = null;
          arrayRegAux = new Object[numCampos];
          arrayRegAux = (Object[]) vLineasItem.elementAt(j);
          for (int z = 0; z < numCampos; z++) {
            arrayRegistros[z][j] = arrayRegAux[z];
          }
        }
        // almacenamos el array
        vectorLin = null;
        vectorLin = new Vector();

        // a�adimos el nombre de la tabla
        vectorLin.addElement(tablaActual);
        // a�adimos el n�mero de campos
        vectorLin.addElement(Integer.toString(numCampos));
        // a�adimos los nombres de los campos
        vectorLin.addElement(arrayNombres);
        // a�adimos el n�mero de registros
        vectorLin.addElement(Integer.toString(numRegistros));
        // a�adimos los registros
        vectorLin.addElement(arrayRegistros);
        // a�adimos el n�mero de campos c�digo
        vectorLin.addElement("3");

        // a�adimos todo al vector principal
        vectorBloque.addElement(vectorLin);
        vectorLin = null;
      }

      // debemos ver si da error
    }
    catch (IOException ioEx) {
      ioEx.printStackTrace();
      elBoolean = false;
      msgBox = new CMessage(this.app, CMessage.msgERROR, ioEx.getMessage());
      msgBox.show();
      msgBox = null;
    }
    return elBoolean;
  }

  public partirFichero(CApp app, String fichero, String codigo) {
    CMessage msgBox;
    this.app = app;
    res = ResourceBundle.getBundle("cargamodelo.Res" + this.app.getIdioma());
    if (cargaVectorNoCod(fichero, codigo)) {
      try {
        // Inicializamos el servlet
        stubCliente = new StubSrvBD(new URL(this.app.getURL() + strSERVLET));
        // llamada al servlet
        stubCliente.doPost(servletALTA, vectorBloque);
      }
      catch (Exception er) {
        msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgERROR,
                            res.getString("msg2.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

}