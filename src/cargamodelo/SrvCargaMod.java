package cargamodelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import sapp.DBServlet;

public class SrvCargaMod
    extends DBServlet {

  // modos de operación
  final int servletOBTENERedo = 0;
  final int servletOBTENERcie = 1;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    String query = null;

    // objetos de datos
    CLista data = new CLista();

    // contador de registros
    int i;

    // establece la conexión con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    // modos de operación
    switch (opmode) {

      case servletOBTENERedo:

        // proceso global
        try {
          // prepara los inserts
          query = "select CD_ENFERE from sive_enferedo where CD_ENFCIE=?";
          st = con.prepareStatement(query);
          st.setString(1, (String) param.firstElement());
          st.executeQuery();
          rs = st.getResultSet();
          while (rs.next()) {
            data.addElement( (String) rs.getString("CD_ENFERE"));
          }
          st.close();
          rs.close();
          rs = null;
          st = null;
          if (data.size() > 0) {
            data.trimToSize();
          }
          else {
            data = null;
          }
        }
        catch (Exception erf) { // error general
          erf.printStackTrace();
        }
        break;

      case servletOBTENERcie:

        // proceso global
        try {
          // prepara los inserts
          query = "select CD_ENFCIE from sive_enferedo where CD_ENFERE=?";
          st = con.prepareStatement(query);
          st.setString(1, (String) param.firstElement());
          st.executeQuery();
          rs = st.getResultSet();
          while (rs.next()) {
            data.addElement( (String) rs.getString("CD_ENFCIE"));
          }
          st.close();
          rs.close();
          rs = null;
          st = null;
          if (data.size() > 0) {
            data.trimToSize();
          }
          else {
            data = null;
          }
        }
        catch (Exception erf) { // error general
          erf.printStackTrace();
        }
        break;
    }
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return data;
  }

}
