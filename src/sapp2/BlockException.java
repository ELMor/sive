package sapp2;

/**
 * Excepci�n de bloqueo de datos.
 *
 * @autor LSR
 * @version 1.0
 */
public class BlockException
    extends Exception {

  private Data dtBlock = null;
  private Lista lisBlock = null;

  public BlockException() {
    super();
  }

  public BlockException(String message, Data dt) {
    super(message);
    dtBlock = dt;
  }

  public BlockException(String message, Lista lis) {
    super(message);
    lisBlock = lis;
  }

  public Data getBlockInfo() {
    return dtBlock;
  }

  public Lista getLisBlockInfo() {
    return lisBlock;
  }
}
