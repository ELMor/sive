package sapp2;

import java.sql.SQLException;
import java.util.Enumeration;

/**
 * Servlet basado en la clase QueryTool que ejecuta varias
 * queries a la vez a modo de transaccion.
 * Si una de ellas falla, se hace rollback de todas.
 * Admite los modos de bloqueo de SrvQueryTool
 *
 * @param opmode: no tiene significado
 * @param vParametros: lista de Datas. La key de cada Data es
 *   el modo de la query, mientras que el value es la QueryTool
 *
 * @return: lista de listas con los datos devueltos por cada QueryTool
 *
 * @autor JMT
 * @version 1.0
 */

public class SrvTransaccion
    extends DBServlet {

  final protected int servletDO_SELECT = 1;
  final protected int servletDO_GETPAGE = 2;
  final protected int servletDO_INSERT = 3;
  final protected int servletDO_UPDATE = 4;
  final protected int servletDO_DELETE = 5;
  final protected int servletDO_INSERT_BLO = 10003;
  final protected int servletDO_UPDATE_BLO = 10006;
  final protected int servletDO_DELETE_BLO = 10007;
  final protected int servletDO_GETPAGE_DESC = 8;

  protected Lista doWork(int opmode, Lista vParametros) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = null;

    // Vector con el resultado, Data con la QueryTool, opmode, QueryTool
    Lista vResulset = new Lista();
    Data dtQT = null;
    int modoOp;
    QueryTool queryTool = null;
    Enumeration eKey = null;
    Enumeration eValue = null;

    try {
      con.setAutoCommit(false);

      for (int i = 0; i < vParametros.size(); i++) {
        dtQT = (Data) vParametros.elementAt(i);
        eKey = dtQT.keys();
        eValue = dtQT.elements();

        modoOp = Integer.parseInt( (String) eKey.nextElement());
        queryTool = (QueryTool) eValue.nextElement();

        switch (modoOp) {
          case servletDO_SELECT:

            vResulset.addElement(queryTool.doSelect(con));
            break;

          case servletDO_GETPAGE:
            vResulset.addElement(queryTool.doGetPage(vParametros.getCampoTrama(),
                vParametros.getValorTrama(),
                ">",
                "",
                con));
            break;

          case servletDO_GETPAGE_DESC:
            vResulset.addElement(queryTool.doGetPage(vParametros.getCampoTrama(),
                vParametros.getValorTrama(),
                "<",
                "DESC",
                con));
            break;

          case servletDO_INSERT:
          case servletDO_INSERT_BLO:
            vResulset.addElement(queryTool.doInsert(con));
            break;

          case servletDO_UPDATE_BLO:
          case servletDO_UPDATE:
            vResulset.addElement(queryTool.doUpdate(con));
            break;

          case servletDO_DELETE_BLO:
          case servletDO_DELETE:
            vResulset.addElement(queryTool.doDelete(con));
            break;
        }
      } // Fin for

      // Si todo ha ido bien se hace el commit
      con.commit();
    }
    catch (SQLException e1) {
      // rollback
      con.rollback();

      // traza el error
      trazaLog(e1);
      bError = true;

      // selecciona el mensaje de error
      switch (opmode) {
        case servletDO_SELECT:
        case servletDO_GETPAGE:
          sMsg = "Error al leer " + queryTool.getName();
          break;

        case servletDO_INSERT:
        case servletDO_INSERT_BLO:
          sMsg = "Error al insertar " + queryTool.getName();
          break;

        case servletDO_UPDATE:
        case servletDO_UPDATE_BLO:
          sMsg = "Error al actualizar " + queryTool.getName();
          break;

        case servletDO_DELETE:
        case servletDO_DELETE_BLO:
          sMsg = "Error al borrar " + queryTool.getName();
          break;
      }
    }
    catch (Exception e2) {
      // rollback
      con.rollback();

      // traza el error
      trazaLog(e2);
      bError = true;

      // selecciona el mensaje de error
      sMsg = "Error inesperado";
    }

    // devuelve el error recogido
    if (bError) {
      throw new Exception(sMsg);
    }

    return vResulset;
  }
}
