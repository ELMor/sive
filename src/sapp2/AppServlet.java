package sapp2;

import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servidor de p�ginas HTML de la aplicaci�n
 * Las peticiones son del tipo GET
 * Para cada p�gina deben determinarse los siguientes par�metros:
 *   sClass        Nombre de la clase
 *   sArchive      zips que requiere
 *   sWidth        Ancho
 *   sHeight       Alto
 *   dtParametros  Data con los par�metros que requiere el aplet
 *
 * @autor LSR
 * @version 1.0
 */
public abstract class AppServlet
    extends HttpServlet {

  // par�metros de configuraci�n
  protected String sClass = "";
  protected String sArchive = "";
  protected String sWidth = "";
  protected String sHeight = "";

  // vector de par�metros
  protected Data dtParametros = new Data();

  public abstract void doConfig();

  // servicio
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws
      ServletException, IOException {

    // streams
    ServletOutputStream out;

    // buffers
    Enumeration enum = null;
    String param = null;
    String value = null;

    // comunicaciones servlet-applet
    try {

      // carga los par�metros recibidos desde el men�
      for (enum = request.getParameterNames(); enum.hasMoreElements(); ) {
        param = (String) enum.nextElement();
        dtParametros.put(param, request.getParameter(param));
      }

      // abre el stream de salida
      response.setContentType("text/html");
      out = response.getOutputStream();

      // cabecera
      out.println("<HTML><HEAD>");
      out.println("<BASE HREF=\"" + dtParametros.getString("URL_HTML") + "\">");
      out.println("</HEAD><BODY BGCOLOR=\"#FFFFFF\">");

      // completa los datos del applet
      doConfig();

      // completa la p�gina HTML
      out.println("<TABLE WIDTH=\"100%\" HEIGHT=\"100%\" BORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"\"0>");
      out.println("<TR><TD ALIGN=\"CENTER\" VALIGN=\"MIDDLE\">");

      // TAG APPLET
      out.println("<APPLET");
      out.println(" MAYSCRIPT");
      //out.println(" CODEBASE = \".\"");
      //out.println(" CODEBASE = \"applet/\"");
      out.println(" CODEBASE = \"zip/\"");
      if (sArchive.length() > 0) {
        out.println(" ARCHIVE = \"" + sArchive + "\"");
      }
      out.println(" CODE     = \"" + sClass + "\"");
      out.println(" WIDTH    = \"" + sWidth + "\"");
      out.println(" HEIGHT   = \"" + sHeight + "\"");
      out.println(" HSPACE   = \"0\"");
      out.println(" VSPACE   = \"0\"");
      out.println(" ALIGN   = \"CENTER\"");
      out.println(">");

      // completa los par�metros
      for (enum = dtParametros.keys(); enum.hasMoreElements(); ) {
        param = (String) enum.nextElement();
        value = (String) dtParametros.getString(param);
        out.println("<PARAM NAME=\"" + param +
                    "\" VALUE=\"" + value + "\">");

      }

      // finaliza la p�gina
      out.println("</APPLET>");
      out.println("</TD></TR>");
      out.println("</TABLE></BODY></HTML>");
      out.flush();
      out.close();

      // envia la excepci�n
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
}
