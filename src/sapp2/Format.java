
package sapp2;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Clase b�sica para dar formato a fechas y n�meros.
 *
 * @autor LSR
 * @version 1.0
 */
public class Format {

  /**
   * Tranforma una fecha con hora en una cadena
   *
   * @param ts  Timestamp
   * @return La fecha en formato dd/mm/yyyy hh:mm:ss
   */
  public static String timestamp2String(java.sql.Timestamp ts) {
    if (ts == null) {
      return null;
    }
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String buffer = ts.toString();
    String res = buffer.substring(11, 19); // hh:mm:ss
    res = buffer.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = buffer.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = buffer.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss

    return res;
  }

  /**
   * Tranforma una cadena en una fecha con hora
   *
   * @param sFecha  La fecha en formato dd/mm/yyyy hh:mm:ss
   * @return Timestamp
   */
  public static java.sql.Timestamp string2Timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);

    return TSFec;
  }

  /**
   * Tranforma una cadena en una fecha
   *
   * Nota Cristina 3/11/99: no funciona bien, con una fecha "08/11/199"
   *                        devuelve la fecha 22/05/1914.
   *                        Con el parse s� funciona.
   *
   * @param sFecha  La fecha en formato dd/mm/yyyy
   * @return Date
   */
  public static java.sql.Date string2Date(String sFecha) {
    /*
         int dd = (new Integer(sFecha.substring(0,2))).intValue();
         int mm = (new Integer(sFecha.substring(3,5))).intValue();
         int yyyy = (new Integer(sFecha.substring(6,10))).intValue();
         return new java.sql.Date(dd, mm, yyyy);
     */
    SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
    java.util.Date fecha = new java.util.Date();
    try {
      fecha = formatDate.parse(sFecha);
    }
    catch (Exception e) {
    }

    return new java.sql.Date(fecha.getTime());
  }

  /**
   * Tranforma una fecha en una cadena
   *
   * @param dFecha  Date
   * @return La fecha en formato dd/mm/yyyy
   */
  public static String date2String(java.sql.Date dFecha) {
    if (dFecha == null) {
      return null;
    }
    SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
    return formatDate.format(dFecha);
  }

  /**
   * Tranforma un n�mero en una cadena
   *
   * @param dNumber  Double
   * @return El n�mero con formato ##########.##
   */
  public static String double2String(Double dNumber) {
    if (dNumber == null) {
      return null;
    }
    DecimalFormat formatDouble = new DecimalFormat("##########.##");
    DecimalFormatSymbols formatSimbols = formatDouble.getDecimalFormatSymbols();
    formatSimbols.setDecimalSeparator('.');
    formatDouble.setDecimalFormatSymbols(formatSimbols);
    return formatDouble.format(dNumber.doubleValue());
  }

  /**
   * Tranforma una cadena en un n�mero
   *
   * @param sNumber  N�mero
   * @return Double
   */
  public static Double string2Double(String sNumber) {
    return new Double(sNumber);
  }

  /**
   * Devuelve la fecha actual de la forma mm/dd/yyyy
   *
   * @return Fecha
   */
  public static String fechaActual() {
    java.util.Date dFecha_Actual = new java.util.Date();
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    return sdf.format(dFecha_Actual);
  }

  /**
   * Devuelve la fecha actual de la forma mm/dd/yyyy hh:mm:ss
   *
   * @return Fecha
   */
  public static String fechaHoraActual() {
    java.util.Date dFecha_Actual = new java.util.Date();
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                                new Locale("es", "ES"));
    return sdf.format(dFecha_Actual);
  }

  /**
   * Devuelve el a�o inicial
   *
   * @return A�o
   */
  public static String anyoActual() {
    java.util.Date dFecha_Actual = new java.util.Date();
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    return sdf.format(dFecha_Actual).substring(6, 10);
  }

  /**
   * Compara dos fechas (string)
   * Formato: fechas dd/mm/aa e informadas!!!
   *
   * @return 0: si iguales, 1: si Fecha1 mayor, 2: si Fecha2 mayor
   */
  public static int comparaFechas(String Fecha1, String Fecha2) {
    int iresult = 0;
    String Fec1 = "";
    String Fec2 = "";
    int x1 = 0, x2 = 0;
    String dd = "", mm = "", aa = "";

    String sCadena = "";
    sCadena = Fecha1;
    x1 = sCadena.indexOf("/", 0);
    x2 = sCadena.indexOf("/", x1 + 1);
    dd = sCadena.substring(0, x1);
    mm = sCadena.substring(x1 + 1, x2);
    aa = sCadena.substring(x2 + 1, sCadena.length());
    Fec1 = aa + mm + dd;
    sCadena = Fecha2;
    x1 = sCadena.indexOf("/", 0);
    x2 = sCadena.indexOf("/", x1 + 1);
    dd = sCadena.substring(0, x1);
    mm = sCadena.substring(x1 + 1, x2);
    aa = sCadena.substring(x2 + 1, sCadena.length());
    Fec2 = aa + mm + dd;

    int iCompara = 0;
    iCompara = Fec1.compareTo(Fec2);
    if (iCompara == 0) {
      iresult = 0;
    }
    else if (iCompara < 0) {
      iresult = 2;
    }
    else if (iCompara > 0) {
      iresult = 1;

    }
    return (iresult);
  }

}
