package sapp2;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.net.URLConnection;

import capp2.CApp;
import capp2.CMessage;
import comun.constantes;

/**
 * Stub de comunicaciones applet/servlet.
 *
 * Nota: los modos de los servlets que exijan bloqueos deben ser 1x.xxx
 *
 * @autor LSR
 * @version 1.0
 */
public class StubSrvBD {

//  public static final String SRV_QUERY_TOOL = "servlet/SrvQueryTool";
  public static final String SRV_QUERY_TOOL = constantes.strSERVLET_QUERY_TOOL;

  private String ref = null;
  private URL url = null;

  // contruye la url que apunta al servlet
  public StubSrvBD(String r) {
    ref = r;
  }

  // ejecuta el servicio
  public Object doPost(int opmode, Lista vParametros) throws Exception {
    Object vResultado = null;
    URLConnection con = null;
    ObjectInputStream in = null;
    ObjectOutputStream out = null;

    // abre la conexi�n con el servlet
    con = url.openConnection();
    con.setRequestProperty("Content-type", "application/octet-stream");
    con.setDoOutput(true);
    con.setUseCaches(false);

    // invoca el modo de operaci�n del servlet
    out = new ObjectOutputStream(con.getOutputStream());
    out.writeInt(opmode);
    out.flush();

    // envia los par�metros
    if (vParametros != null) {
      vParametros.trimToSize();
    }
    out.writeObject(vParametros);
    out.flush();
    out.close();

    // lee los par�metros de retorno
    in = new ObjectInputStream( (InputStream) con.getInputStream());
    vResultado = in.readObject();

    // cierra los streams
    in.close();

    // levanta la excepci�n producida en el servlet
    if (vResultado != null) {
      if ( (vResultado.getClass().getName().equals("java.lang.Exception")) ||
          (vResultado.getClass().getName().equals("sapp2.BlockException"))) {
        throw (Exception) vResultado;
      }
    }

    return (vResultado);
  }

  // gestiona una operaci�n que exige bloqueo de datos
  public Object doPost(int opmode,
                       Lista vData,
                       QueryTool qtBlock,
                       Data dtBlock,
                       CApp app) throws Exception {

    Lista vParam = new Lista();
    Object vResultado = null;
    CMessage box = null;

    // prepara la lista para transmitir
    vParam.addElement(qtBlock);
    vParam.addElement(dtBlock);
    vParam.addElement(vData);

    // invoca al servlet
    try {
      vResultado = doPost(opmode, vParam);

      // tratamiento del bloqueo
    }
    catch (BlockException e1) {

      // el registro ha sido borrado
      if (e1.getBlockInfo() == null) {

        box = new CMessage(app, CMessage.msgAVISO, e1.getMessage());
        box.show();
        vResultado = new Lista();

        // el registro ha sido modificado
      }
      else {

        // pregunta al usuario
        box = new CMessage(app, CMessage.msgADVERTENCIA, e1.getMessage());
        box.show();

        // sobreescribe los datos
        if (box.getResponse()) {

          // prepara la lista para transmitir
          vParam.insertElementAt(e1.getBlockInfo(), 1);

          // realiza la consulta
          //vResultado = doPost(opmode, vParam);
          vResultado = doPost(opmode, vData, qtBlock,
                              e1.getBlockInfo(), app);

          // cancela la operaci�n
        }
        else {
          vResultado = new Lista();
        }
      }

      box = null;
    }
    catch (Exception e2) {
      throw e2;
    }

    return vResultado;
  }

  // gestiona una operaci�n que exige bloqueo de datos
  // y en la que estan implicados mas de un registro de
  // una tabla
  public Object doPost(int opmode,
                       Lista vData,
                       Lista qtBlock,
                       Lista lisBlock,
                       CApp app) throws Exception {

    Lista vParam = new Lista();
    Object vResultado = null;
    CMessage box = null;

    // prepara la lista para transmitir
    vParam.addElement(qtBlock);
    vParam.addElement(lisBlock);
    vParam.addElement(vData);

    // invoca al servlet
    try {
      vResultado = doPost(opmode, vParam);

      // tratamiento del bloqueo
    }
    catch (BlockException e1) {

      // el registro ha sido borrado
      if (e1.getBlockInfo() == null) {

        box = new CMessage(app, CMessage.msgAVISO, e1.getMessage());
        box.show();
        vResultado = new Lista();

        // el registro ha sido modificado
      }
      else {

        // pregunta al usuario
        box = new CMessage(app, CMessage.msgADVERTENCIA, e1.getMessage());
        box.show();

        // sobreescribe los datos
        if (box.getResponse()) {

          // prepara la lista para transmitir
          vParam.insertElementAt(e1.getBlockInfo(), 1);

          // realiza la consulta
          //vResultado = doPost(opmode, vParam);
          vResultado = doPost(opmode, vData, qtBlock,
                              e1.getLisBlockInfo(), app);

          // cancela la operaci�n
        }
        else {
          vResultado = new Lista();
        }
      }

      box = null;
    }
    catch (Exception e2) {
      throw e2;
    }

    return vResultado;
  }

  // cambio de url
  public void setUrl(String servlet) {
    try {
      url = new URL(ref + servlet);
    }
    catch (Exception e) {
      e.printStackTrace();
      url = null;
    }
  }
}
