package sapp2;

import java.net.URLEncoder;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Clase b�sica de datos.
 *
 * @autor LSR
 * @version 1.0
 */
public class Data
    extends Hashtable {

  /**
   *
   *
   */
  public void put(String key, ResultSet rs, int i, int tipo) throws java.sql.
      SQLException {
    String value = null;

    switch (tipo) {
      case QueryTool.DATE:
        value = Format.date2String(rs.getDate(i));
        break;
      case QueryTool.INTEGER:
        value = (new Integer(rs.getInt(i))).toString();
        break;
      case QueryTool.REAL:
        value = Format.double2String(new Double(rs.getDouble(i)));
        break;
      case QueryTool.STRING:
        value = rs.getString(i);
        break;
      case QueryTool.TIMESTAMP:
        value = Format.timestamp2String(rs.getTimestamp(i));
        break;
    }

    if (value != null) {
      this.put(key, value);
    }
  }

  /**
   * Graba un valor int como un Integer
   *
   * @return valor
   */
  public void put(String key, int value) {
    put(key, new Integer(value));
  }

  /**
   * Devuelve una cadena. Si no esxiste el valor devuelve ""
   *
   * @return valor
   */
  public String getString(String key) {
    String sValue = (String) get(key);
    if (sValue == null) {
      sValue = new String("");
    }
    return sValue;
  }

  /**
   * Devuelve una cadena con el formato nombre=valor&...
   *
   * @return cadena URL
   */
  public String toURL() {
    Enumeration enum = null;
    String url = "";
    String key = null;

    for (enum = keys(); enum.hasMoreElements(); ) {
      key = (String) enum.nextElement();

      if (url.length() > 0) {
        url += "&";
      }

      url += key + "=" + URLEncoder.encode(getString(key));
    }

    return url;
  }

  public Object clone() {
    Data dtResul = new Data();
    Enumeration ek = null;
    Enumeration ev = null;

    for (ek = keys(), ev = elements(); ek.hasMoreElements(); ) {
      dtResul.put( (String) ek.nextElement(), (Object) ev.nextElement());

    }
    return dtResul;
  }

}
