package sapp2;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Clase utilizada para la transmisi�n de informaci�n.
 *
 * @autor LSR
 * @version 1.0
 */
public class Lista
    extends Vector {

  public final static int PAGE_SIZE = 50;
  public final static int COMPLETA = 0;
  public final static int INCOMPLETA = 1;

  /**
   * Indicador de lista completa para determinar si faltan tramas
   */
  protected int iESTADO = 0;

  /**
   * Par�metros internos de la aplicaci�n
   */
  protected Hashtable hash = null;

  /**
   * Par�metros de la trama
   */
  protected Hashtable trama = null;

  /**
   * JMT (27/01/2000) Fecha Ultima Actualizacion
   */
  protected String fUltAct = "";

  /**
   * Constructor
   */
  public Lista() {
    hash = new Hashtable();
  }

  /**
   * Graba el campo y el valor de la �ltima trama
   *
   * @param campo Campo
   * @param valor Trama
   */
  public void setTrama(String campo, String valor) {
    trama = new Hashtable();
    trama.put(campo, valor);
  }

  /**
   * Graba la trama
   *
   * @param tr Trama
   */
  public void setTrama(Hashtable tr) {
    trama = tr;
  }

  /**
   * Devuelve la trama
   *
   * @return Trama
   */
  public Hashtable getTrama() {
    return trama;
  }

  /**
   * Devuelve el campo de la trama
   *
   * @return campo
   */
  public String getCampoTrama() {
    String campo = "";
    if (trama != null) {
      Enumeration e = trama.keys();
      campo = (String) e.nextElement();
    }
    return campo;
  }

  /**
   * Devuelve el valor de la �ltima trama
   *
   * @return Trama
   */
  public String getValorTrama() {
    String valor = "";
    if (trama != null) {
      Enumeration e = trama.elements();
      valor = (String) e.nextElement();
    }
    return valor;
  }

  /**
   * Devuelve el estado
   *
   * @return Estado
   */
  public int getEstado() {
    return iESTADO;
  }

  /**
   * Establece lista incompleta
   */
  public void setIncomplet() {
    iESTADO = Lista.INCOMPLETA;
  }

  /**
   * Establece lista completa
   */
  public void setComplet() {
    iESTADO = Lista.COMPLETA;
  }

  /**
   * Graba una propiedad interna (siempre como cadenas)
   *
   * @param name Nombre de la propiedad
   * @param value Propiedad
   */
  public void setParameter(String name, String value) {
    hash.put(name, value);
  }

  /**
   * Lee una propiedad interna (siempre como cadenas)
   *
   * @param name Nombre de la propiedad
   * @return value Propiedad
   */
  public String getParameter(String name) {
    String value = (String) hash.get(name);
    if (value == null) {
      value = new String("");
    }
    return value;
  }

  /**
   * A�ade los componenetes del vector y reemplaza la trama
   *
   * @param v nuevos componentes
   */
  public void addElements(Lista v) {
    this.iESTADO = v.getEstado();
    this.trama = v.getTrama();
    for (int j = 0; j < v.size(); j++) {
      addElement(v.elementAt(j));
    }
  }

  /** JMT (19/01/2000)
   * Busca en una lista de Datas (CD-DS) el valor del Data cuya clave
   *  coincide con vkeyCod. Devuelve "" si no se encuentra
   *
   * @param keyCod: clave para acceder al valor del CD
   * @param vkeyCod: valor del CD utilizado para detectar el Data
   * @param keyDesc: clave para acceder al valor del DS
   */
  public String getDSData(String keyCod, String vkeyCod, String keyDesc) {
    for (Enumeration e = this.elements(); e.hasMoreElements(); ) {
      Data dt = (Data) e.nextElement();
      String cod = dt.getString(keyCod);

      if (cod.equals(vkeyCod)) {
        return dt.getString(keyDesc);
      }
    }

    return "";
  } // Fin getDSData()

  /** JMT (27/01/2000)
   *
   * Devuelve la fecha de ultima actualizacion
   */
  public String getFC_ULTACT() {
    return fUltAct;
  } // Fin getFC_ULTACT()

  /** JMT (27/01/2000)
   *
   * Establece la fecha de ultima actualizacion
   */
  public void setFC_ULTACT(String f) {
    fUltAct = f;
  } // Fin setFC_ULTACT()

}
