package notduplic;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import capp.CTabla;
import jclass.bwt.BWTEnum;
import notdata.DataCasoEDO;
import sapp.StubSrvBD;

public class DialogCasoEDO
    extends CDialog {

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;
  ResourceBundle res;

  //confidencial
  protected boolean bConfidencial = false;

  // im�genes
  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

  Panel panel1 = new Panel();
  XYLayout xYLayout1 = new XYLayout();

  BorderLayout borderLayout1 = new BorderLayout();

  // Servlet
  final int servletSELECCION_DIALOGO_EDO = 21;
  final String strSERVLET_CasoEDO = "servlet/SrvEDOIndiv";
  StubSrvBD stubCliente;
  String cadena = "";
  protected String sDesEnfermedad_Entrada = "";

  // Datos de salida
  public DataCasoEDO datos = null;

  // Datos de salida
  public DataCasoEDO Resultados = null;
  CLista listaSalida = new CLista();
  CLista listaParametros = new CLista();
  TextField txtFecNac = new TextField();
  TextField txtNombre = new TextField();
  TextField txtEnfermedad = new TextField();
  Label label1 = new Label();
  Label label2 = new Label();
  Label label3 = new Label();
  Label label4 = new Label();
  TextField txtEnfermo = new TextField();
  ButtonControl btnSeleccionar = new ButtonControl();
  ButtonControl btnSalir = new ButtonControl();
  //QQ: Adaptar CTabla
  CTabla tabla = new CTabla();

  public DialogCasoEDO(CApp a, DataCasoEDO valores) {
    super(a);
    res = ResourceBundle.getBundle("notduplic.Res" + a.getIdioma());

    txtEnfermedad.setText(valores.sDesEnfermedad);
    sDesEnfermedad_Entrada = valores.sDesEnfermedad;
    txtEnfermo.setText(valores.sCodEnfermo);
    txtFecNac.setText(valores.sFechaNac);

    //mlm confidencial
    bConfidencial = valores.bConfidencial;
    if (bConfidencial) { //true=normal
      txtNombre.setText(valores.sApe1 + " " + valores.sApe2 + ", " +
                        valores.sNombre);
    }
    else {
      txtNombre.setText(valores.sSiglas);
      //-----------------------------------

      //QQ: Inicializar con todos los campos
    }
    Resultados = new DataCasoEDO("", "", "", "", "", 0, "", "",
                                 false, false, valores.bConfidencial, "", "",
                                 "", "", "", "",
                                 "", "", "", "", "");
    // ahora implementamos el proceso del Servlet

    try {
      jbInit();
      pack();

      stubCliente = new StubSrvBD(new URL(this.app.getURL() +
                                          strSERVLET_CasoEDO));

      // par�metros
      //QQ: Inicializar con todos los campos
      datos = new DataCasoEDO(valores.sCodEnfermo,
                              "",
                              "",
                              "", "",
                              0,
                              "",
                              valores.sCodEnfermedad,
                              false, false, valores.bConfidencial,
                              "", "", "", "", "", "", "", "", "", "", "");

      listaParametros = new CLista();
      listaParametros.addElement(datos);

      // Por LORTAD
      listaParametros.setLogin(app.getLogin());
      listaParametros.setLortad(app.getParameter("LORTAD"));

      listaSalida = (CLista) stubCliente.doPost(servletSELECCION_DIALOGO_EDO,
                                                listaParametros);

      //# // System_out.println("listaSalida del dlg "+listaSalida);

      Resultados.bHayDatos = false;
      if (listaSalida.size() != 0) { // hay datos
        Resultados.bHayDatos = true;

        // se recuperan los datos de cabecera por si acaso
        // no se han pasado como par�metros
        // QQ: Atenci�n, ya no inclu�mos datos del enfermo
        datos = (DataCasoEDO) listaSalida.firstElement();
        //txtEnfermedad.setText(sDesEnfermedad_Entrada);
        //txtEnfermo.setText(datos.sCodEnfermo);
        //txtFecNac.setText(datos.sFechaNac);
        //txtNombre.setText(datos.sApe1+" "+datos.sApe2+", "+datos.sNombre);

        tabla.clear();
        for (int r = 0; r < listaSalida.size(); r++) {
          datos = (DataCasoEDO) listaSalida.elementAt(r);
          cadena = "";
          cadena = Integer.toString(datos.iCodCaso) + "&";
          // fecha notificaci�n
          cadena += datos.sFechaNotif + "&";
          // fecha inicio de s�ntomas
          if (datos.sFechaIni == null) {
            cadena += " " + "&";
          }
          else {
            cadena += datos.sFechaIni + "&";
            // Clasificaci�n
          }
          if (datos.sDesClasif == null) {
            cadena += " " + "&";
          }
          else {
            cadena += datos.sDesClasif + "&";
            // a�adimos la l�nea de texto a la tabla

            //QQ: Nuevas columnas del primer notificador
          }
          if (datos.sDesCentro == null) {
            cadena += " " + "&";
          }
          else {
            cadena += datos.sDesCentro + "&";

          }
          if (datos.sDesEquipo == null) {
            cadena += " " + "&";
          }
          else {
            cadena += datos.sDesEquipo + "&";

          }
          if (datos.sCodN1 == null) {
            cadena += "  -";
          }
          else {
            cadena += datos.sCodN1 + "-";
          }
          if (datos.sCodN2 == null) {
            cadena += " " + "&";
          }
          else {
            cadena += datos.sCodN2 + "&";

          }
          tabla.addItem(cadena, '&');
        }
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void Inicializar() {
  }

  void jbInit() throws Exception {

    xYLayout1.setHeight(350);
    xYLayout1.setWidth(750);
    panel1.setLayout(xYLayout1);
    this.setLayout(borderLayout1);
    this.setSize(new Dimension(750, 350));

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnSeleccionar.setImage(imgs.getImage(0));
    btnSalir.setImage(imgs.getImage(1));

    this.setTitle(res.getString("this.Title"));

    //    xYLayout1.setHeight(350);
    this.setSize(new Dimension(750, 350));
    //    xYLayout1.setWidth(450);
    txtFecNac.setEditable(false);
    txtNombre.setEditable(false);
    txtEnfermedad.setBackground(new Color(250, 250, 150));
    txtEnfermedad.setForeground(Color.black);
    txtEnfermedad.setFont(new Font("Dialog", 0, 12));
    txtEnfermedad.setEditable(false);
    if (bConfidencial) { //true=normal
      label1.setText(res.getString("label1.Text"));
    }
    else {
      label1.setText(res.getString("label1.Text1"));
    }
    label2.setForeground(Color.black);
    label2.setFont(new Font("Dialog", 0, 12));
    label2.setText(res.getString("label2.Text"));
    label3.setForeground(Color.black);
    label3.setFont(new Font("Dialog", 0, 12));
    label3.setText(res.getString("label3.Text"));
    label4.setForeground(Color.black);
    label4.setFont(new Font("Dialog", 0, 12));
    label4.setText(res.getString("label4.Text"));
    txtEnfermo.setBackground(new Color(255, 255, 150));
    txtEnfermo.setForeground(Color.black);
    txtEnfermo.setFont(new Font("Dialog", 0, 12));
    txtEnfermo.setEditable(false);
    btnSeleccionar.setFont(new Font("Dialog", 0, 12));
    btnSeleccionar.setLabel(res.getString("btnSeleccionar.Label"));
    btnSalir.addActionListener(new DialogCasoEDO_btnSalir_actionAdapter(this));
    btnSalir.setLabel(res.getString("btnSalir.Label"));

    tabla.setNumColumns(7);
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "40\n80\n80\n120\n150\n140\n80"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tabla.ColumnButtonsStrings")), '\n'));

    btnSalir.setFont(new Font("Dialog", 0, 12));
    btnSeleccionar.addActionListener(new
                                     DialogCasoEDO_btnSeleccionar_actionAdapter(this));
    label1.setFont(new Font("Dialog", 0, 12));
    label1.setForeground(Color.black);
    txtNombre.setFont(new Font("Dialog", 0, 12));
    txtNombre.setBackground(new Color(255, 255, 150));
    txtNombre.setForeground(Color.black);
    txtFecNac.setFont(new Font("Dialog", 0, 12));
    txtFecNac.setForeground(Color.black);
//    panel1.setLayout(xYLayout1);
//    this.add(panel1);

    this.add(panel1, BorderLayout.CENTER);

    panel1.add(label3, new XYConstraints(20, 15, 80, -1));
    panel1.add(txtEnfermo, new XYConstraints(135, 15, 70, -1));
    panel1.add(label1, new XYConstraints(20, 45, 116, -1));
    panel1.add(txtNombre, new XYConstraints(135, 45, 246, -1));
    panel1.add(label2, new XYConstraints(20, 75, 103, -1));
    panel1.add(txtFecNac, new XYConstraints(135, 75, 81, -1));
    panel1.add(label4, new XYConstraints(20, 105, -1, -1));
    panel1.add(txtEnfermedad, new XYConstraints(135, 105, 246, -1));
    panel1.add(tabla, new XYConstraints(20, 145, 710, 150));
    panel1.add(btnSeleccionar, new XYConstraints(550, 307, -1, -1));
    panel1.add(btnSalir, new XYConstraints(660, 307, -1, -1));
  }

  public boolean hayResultados() {
    return Resultados.bHayDatos;
  }

  void btnSeleccionar_actionPerformed(ActionEvent e) {
    CMessage mensaje = null;

    if (tabla.getSelectedIndex() != BWTEnum.NOTFOUND) {
      // se obtiene la l�nea seleccionada
      Resultados = (DataCasoEDO) listaSalida.elementAt(tabla.getSelectedIndex());
      // hay resultados
      Resultados.bHayDatos = true;
      // Se ha aceptado
      Resultados.bEsOK = true;
      // se cierra el di�logo
      dispose();
    }
    else { // no se ha seleccionado nada
      mensaje = new CMessage(this.app, CMessage.msgERROR,
                             res.getString("msg2.Text"));
      mensaje.show();
      mensaje = null;
    }
  }

  void btnSalir_actionPerformed(ActionEvent e) {
    Resultados.bEsOK = false;
    dispose();
  }

}

class DialogCasoEDO_btnSeleccionar_actionAdapter
    implements java.awt.event.ActionListener {
  DialogCasoEDO adaptee;

  DialogCasoEDO_btnSeleccionar_actionAdapter(DialogCasoEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnSeleccionar_actionPerformed(e);
  }
}

class DialogCasoEDO_btnSalir_actionAdapter
    implements java.awt.event.ActionListener {
  DialogCasoEDO adaptee;

  DialogCasoEDO_btnSalir_actionAdapter(DialogCasoEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnSalir_actionPerformed(e);
  }
}
