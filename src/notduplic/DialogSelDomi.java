package notduplic;

import java.util.ResourceBundle;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CTabla;
import jclass.bwt.JCActionEvent;

public class DialogSelDomi
    extends CDialog {

  // im�genes
  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};
  ResourceBundle res;

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  XYLayout xYLayout1 = new XYLayout();
  CTabla tablaDom = new CTabla();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // pulsac��n
  public boolean pulsaOK = false;
  final String imgACEPTAR = "images/aceptar.gif";
  final String imgCANCELAR = "images/cancelar.gif";

  public DialogSelDomi(CApp a) {
    super(a);
    res = ResourceBundle.getBundle("notduplic.Res" + a.getIdioma());
    try {
      jbInit();
      pack();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void Inicializar() {

  }

  void jbInit() throws Exception {

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));

    this.setTitle(res.getString("this.TitleOtrasDir"));

    this.setSize(new Dimension(640, 280));
    setLayout(xYLayout1);
    tablaDom.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "90\n200\n300"), '\n'));
    tablaDom.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tablaDom.ColumnButtonsStrings")), '\n'));
    tablaDom.addActionListener(new jclass.bwt.JCActionListener() {
      public void actionPerformed(JCActionEvent e) {
        tablaDom_actionPerformed(e);
      }
    });
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnAceptar.addActionListener(new DialogSelDomi_btnAceptar_actionAdapter(this));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    btnCancelar.addActionListener(new DialogSelDomi_btnCancelar_actionAdapter(this));
    add(tablaDom, new XYConstraints(15, 7, 610, 188));
    add(btnAceptar, new XYConstraints(228, 209, -1, -1));
    add(btnCancelar, new XYConstraints(316, 209, -1, -1));
  }

  void btnAceptar_actionPerformed(ActionEvent e) {

    pulsaOK = true;
    dispose();
  }

  void btnCancelar_actionPerformed(ActionEvent e) {
    pulsaOK = false;
    dispose();
  }

  void tablaDom_actionPerformed(JCActionEvent e) {
    btnAceptar_actionPerformed(null);
  }
}

class DialogSelDomi_btnAceptar_actionAdapter
    implements java.awt.event.ActionListener {
  DialogSelDomi adaptee;

  DialogSelDomi_btnAceptar_actionAdapter(DialogSelDomi adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnAceptar_actionPerformed(e);
  }
}

class DialogSelDomi_btnCancelar_actionAdapter
    implements java.awt.event.ActionListener {
  DialogSelDomi adaptee;

  DialogSelDomi_btnCancelar_actionAdapter(DialogSelDomi adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnCancelar_actionPerformed(e);
  }
}
