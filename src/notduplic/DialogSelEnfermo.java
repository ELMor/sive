package notduplic;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import capp.CTabla;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import jclass.util.JCVector;
import notdata.DataValoresEDO;
import sapp.StubSrvBD;

public class DialogSelEnfermo
    extends CDialog {

  // im�genes
  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/enter.gif"};
  ResourceBundle res;

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  public static final int modoDENFER = 180;
  public static final int modoDENFERDATOS = 185;
  public DataValoresEDO Resultados = null;
  public DataValoresEDO Entrada = null;
  protected boolean hayDatos = false;

  //salida BOTON aceptar o cancelar
  protected boolean bFlag_OK = false;

  // par�metros
  JCVector items = new JCVector();
  JCVector row1 = new JCVector();
  CLista listaSalida = new CLista();
  CLista listaParametros = new CLista();
  enfermo.DataEnfermo datos = null;
  String cadena = "";
  Date fecha;
  int anyoFec = 0;
  int mesFec = 0;
  int diaFec = 0;
  int ultIndice = -1;
  int ultDomi = -1;

  //stubs
  final String strSERVLET_ValoresEDO = "servlet/SrvEnfermo";
  StubSrvBD stubCliente;

  // componentes del di�logo
  BorderLayout borderLayout1 = new BorderLayout();
  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();
  CTabla tabla = new CTabla();
  ButtonControl btnDomi = new ButtonControl();
  XYLayout xYLayout2 = new XYLayout();

  public DialogSelEnfermo(CApp a, DataValoresEDO valores) {

    super(a);
    res = ResourceBundle.getBundle("notduplic.Res" + a.getIdioma());
    CLista lisEnfer = null;
    Entrada = (DataValoresEDO) valores;
    try {
      jbInit();
      pack();

      stubCliente = new StubSrvBD
          (new URL(this.app.getURL() + strSERVLET_ValoresEDO));

      // leemos los valores
      datos = new enfermo.DataEnfermo("CD_ENFERMO");

      // claves foneticas
      if (valores.sApe1.compareTo("") != 0) {
        datos.put("DS_FONOAPE1",
                  enfermo.comun.traduccionFonetica(valores.sApe1).trim() + "%");

      }
      if (valores.sApe2.compareTo("") != 0) {
        datos.put("DS_FONOAPE2",
                  enfermo.comun.traduccionFonetica(valores.sApe2).trim() + "%");
      }
      if (valores.sNombre.compareTo("") != 0) {
        datos.put("DS_FONONOMBRE",
                  enfermo.comun.traduccionFonetica(valores.sNombre).trim() +
                  "%");

        // municipio
      }
      if (valores.sCodMun.compareTo("") != 0) {
        datos.put("CD_MUN", valores.sCodMun);

        //TRAMERO -------------------
      }
      if (!valores.CDVIAL.equals("")) {
        // cdVial
        datos.put("CDVIAL", valores.CDVIAL.trim());
      }
      else {
        // calle
        if (valores.sDesCalle.compareTo("") != 0) {
          datos.put("DS_DIREC", valores.sDesCalle.trim() + "%");
        }
      }
      //-------------------

      /*// piso
            if(valores.sPiso.compareTo("") != 0)
       datos.put("DS_PISO", valores.sPiso);
            // n�mero
            if(valores.sNumero.compareTo("") != 0)
       datos.put("DS_NUM", valores.sNumero); */

     // provincia
     if (valores.sCodProv.compareTo("") != 0) {
       datos.put("CD_PROV", valores.sCodProv);

       // fecha nacimiento (  setFechaNacimiento - String)
     }
      if (valores.sFechaNac.compareTo("") != 0) {
        datos.setFechaNacimiento(valores.sFechaNac);

        // Sexo
      }
      if (valores.sCodSexo.compareTo("") != 0) {
        datos.put("CD_SEXO", valores.sCodSexo);

        // tipo doc
      }
      if (valores.sTDoc.compareTo("") != 0) {
        datos.put("CD_TDOC", valores.sTDoc);

        // numero doc
        if (valores.sNDoc.compareTo("") != 0) {
          datos.put("DS_NDOC", valores.sNDoc);
        }
      }

      //datos ------JOSE MIGUEL ----- ----- -----

      listaParametros.addElement(datos);

      //ENVIO DE DATOS QUERY
      listaSalida = (CLista) stubCliente.doPost(modoDENFERDATOS,
                                                listaParametros);
      hayDatos = false;

      if (listaSalida.size() > 0) { // hay datos
        hayDatos = true;
        tabla.clear();

        //SALIDA DE DATOS HACIA LA TABLA*** *** ** ** ** * * * * *
        for (int r = 0; r < listaSalida.size(); r++) {
          datos = (enfermo.DataEnfermo) listaSalida.elementAt(r);
          // creamos una l�nea para la tabla
          row1 = new JCVector();

          cadena = "";

          // a�adimos ape1 + ape2 + nombre  o siglas ---
          if (valores.bFlag_Siglas) { //PONER SIGLAS $$$$
            if (datos.get("SIGLAS") != null) {
              cadena = (datos.get("SIGLAS")).toString();
            }
            else {
              cadena = "";
            }
          }
          else { //DATOS NORMAL

            cadena = (datos.get("DS_APE1")).toString();

            if (datos.get("DS_APE2") != null) {
              cadena += " " + (datos.get("DS_APE2")).toString();
            }
            if (datos.get("DS_NOMBRE") != null) {
              cadena += ", " + (datos.get("DS_NOMBRE")).toString();
            }
          } //----

          row1.addElement(cadena);

          // a�adimos fecha nacimiento
          if (datos.get("FC_NAC") != null) {
            // cadena = ((datos.getFechaNacimiento()).toString());
            cadena = datos.getFechaNacimiento();
            row1.addElement(cadena);
          }
          else {
            row1.addElement(new String(""));
          }
          // a�adimos el municipio
          if (datos.get("DS_MUN") != null) {
            cadena = (datos.get("DS_MUN")).toString();
            row1.addElement(cadena);

          }
          else {
            row1.addElement(new String(""));
          }
          // a�adimos el domicilio
          if (datos.get("DS_DIREC") != null) {
            cadena = (datos.get("DS_DIREC")).toString();
            row1.addElement(cadena);

          }
          else {
            row1.addElement(new String(""));
          }
          // y ahora el gr�fico PROBleMA!!!
          //# // System_out.println("direc2 " + datos.get("DS_DIREC2"));

          if (datos.get("DS_DIREC2") != null) {
            row1.addElement(imgs.getImage(2));
          }
          //*******
           // a�adimos la l�nea
           items.addElement(row1);
        }
        tabla.setItems(items);
      }

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public boolean hayResultados() {
    return hayDatos;
  }

  public boolean BotonSalida() {
    return bFlag_OK;
  }

  void jbInit() throws Exception {

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnDomi.setImage(imgs.getImage(2));
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));

    this.setTitle(res.getString("this.Title1"));

    this.setSize(new Dimension(720, 300));
    this.setLayout(xYLayout2);
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    btnCancelar.addActionListener(new
                                  DialogSelEnfermo_btnCancelar_actionAdapter(this));
    btnAceptar.setLabel(res.getString("btnAceptarB.Label"));
    btnAceptar.addActionListener(new DialogSelEnfermo_btnAceptar_actionAdapter(this));
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "290\n75\n125\n155\n-998"), '\n'));
    if (Entrada.bFlag_Siglas) { //PONER SIGLAS $$$$
      tabla.setColumnLabelsStrings(jclass.util.JCUtilConverter.toStringList(new
          String(res.getString("tabla.ColumnLabelsStrings")), '\n'));
    }
    else {
      tabla.setColumnLabelsStrings(jclass.util.JCUtilConverter.toStringList(new
          String(res.getString("tabla.ColumnLabelsStrings1")), '\n'));

    }
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tabla.ColumnLabelsStrings1")), '\n'));
    tabla.setNumColumns(5);
    tabla.addActionListener(new jclass.bwt.JCActionListener() {
      public void actionPerformed(JCActionEvent e) {
        tabla_actionPerformed(e);
      }
    });
    btnDomi.setLabel(res.getString("btnDomi.Label"));
    xYLayout2.setWidth(724);
    xYLayout2.setHeight(299);
    btnDomi.addActionListener(new DialogSelEnfermo_btnDomi_actionAdapter(this));
    //this.add(panel1);
    this.add(tabla, new XYConstraints(10, 13, 700, 226));
    this.add(btnDomi, new XYConstraints(175, 250, -1, -1));
    this.add(btnAceptar, new XYConstraints(475, 250, -1, -1));
    this.add(btnCancelar, new XYConstraints(590, 250, -1, -1));

  }

  void btnDomi_actionPerformed(ActionEvent e) {
    CMessage mensaje = null;
    DialogSelDomi dialogoDomi = null;
    String lineaTexto = "";
    JCVector lineaCompleja = null;
    int indice = 0;
    int indiceDomi = 0;
    int tam = 0;

    if (tabla.getSelectedIndex() != BWTEnum.NOTFOUND) {
      indice = tabla.getSelectedIndex();

      datos = (enfermo.DataEnfermo) listaSalida.elementAt(indice);
      if (datos.get("DS_DIREC2") != null) {
        dialogoDomi = new DialogSelDomi(this.app);
        dialogoDomi.tablaDom.clear();
        // Primera direcci�n
        //dialogoDomi.tablaDom.addItem(datos.get("DS_DIREC").toString());

        String cd_mun = " ";
        String ds_mun = " ";
        String num = " ";
        String piso = " ";

        if ( (datos.get("CD_MUN")) != null) {
          cd_mun = datos.get("CD_MUN").toString();
        }
        else {
          cd_mun = " ";
        }
        if ( (datos.get("DS_MUN")) != null) {
          ds_mun = datos.get("DS_MUN").toString();
        }
        else {
          ds_mun = " ";
        }
        if ( (datos.get("DS_NUM")) != null) {
          num = datos.get("DS_NUM").toString();
        }
        else {
          num = " ";
        }
        if ( (datos.get("DS_PISO")) != null) {
          piso = datos.get("DS_PISO").toString();
        }
        else {
          piso = " ";
        }
        dialogoDomi.tablaDom.addItem(cd_mun + '&' + ds_mun + '&'
                                     + datos.get("DS_DIREC").toString() + ", " +
                                     num + ", " + piso,
                                     '&');

        // Segunda direcci�n
        if ( (datos.get("DS_DIREC2")) != null) {
          if ( (datos.get("CD_MUNI2")) != null) {
            cd_mun = datos.get("CD_MUNI2").toString();
          }
          else {
            cd_mun = " ";
          }
          if ( (datos.get("DS_MUNI2")) != null) {
            ds_mun = datos.get("DS_MUNI2").toString();
          }
          else {
            ds_mun = " ";
          }
          if ( (datos.get("DS_NUM2")) != null) {
            num = datos.get("DS_NUM2").toString();
          }
          else {
            num = " ";
          }
          if ( (datos.get("DS_PISO2")) != null) {
            piso = datos.get("DS_PISO2").toString();
          }
          else {
            piso = " ";
          }
          dialogoDomi.tablaDom.addItem(cd_mun + '&' + ds_mun + '&'
                                       + datos.get("DS_DIREC2").toString() +
                                       ", " + num + ", " + piso,
                                       '&');
        }
        // Tercera direcci�n
        if ( (datos.get("DS_DIREC3")) != null) {
          if ( (datos.get("CD_MUNI3")) != null) {
            cd_mun = datos.get("CD_MUNI3").toString();
          }
          else {
            cd_mun = " ";
          }
          if ( (datos.get("DS_MUNI3")) != null) {
            ds_mun = datos.get("DS_MUNI3").toString();
          }
          else {
            ds_mun = " ";
          }
          if ( (datos.get("DS_NUM3")) != null) {
            num = datos.get("DS_NUM3").toString();
          }
          else {
            num = " ";
          }
          if ( (datos.get("DS_PISO3")) != null) {
            piso = datos.get("DS_PISO3").toString();
          }
          else {
            piso = " ";
          }
          dialogoDomi.tablaDom.addItem(cd_mun + '&' + ds_mun + '&'
                                       + datos.get("DS_DIREC3").toString() +
                                       ", " + num + ", " + piso,
                                       '&');
        }

        // Ahora mostramos el di�logo
        dialogoDomi.show();
        // leemos lo pulsado
        indiceDomi = dialogoDomi.tablaDom.getSelectedIndex();

        if ( (dialogoDomi.pulsaOK) && (indiceDomi != BWTEnum.NOTFOUND)) { // se seleccion� un item

          ultIndice = indice;
          ultDomi = indiceDomi;
          // Reescribimos la tabla
          tam = tabla.countItems();
          if (tam > 0) {
            tabla.deleteItems(0, tam - 1);
          }
          for (int r = 0; r < listaSalida.size(); r++) {
            datos = (enfermo.DataEnfermo) listaSalida.elementAt(r);
            // creamos una l�nea para la tabla
            row1 = new JCVector();

            cadena = "";
            // a�adimos ape1 + ape2 + nombre
            if (Entrada.bFlag_Siglas) {
              if (datos.get("SIGLAS") != null) {
                cadena = (datos.get("SIGLAS")).toString();
              }
              else {
                cadena = "";
              }
            }
            else {
              cadena = (datos.get("DS_APE1")).toString();
              if (datos.get("DS_APE2") != null) {
                cadena += " " + (datos.get("DS_APE2")).toString();
              }
              if (datos.get("DS_NOMBRE") != null) {
                cadena += ", " + (datos.get("DS_NOMBRE")).toString();
              }
            }
            row1.addElement(cadena);

            // a�adimos fecha nacimiento
            if (datos.get("FC_NAC") != null) {
              // cadena = ((datos.getFechaNacimiento()).toString());
              cadena = datos.getFechaNacimiento();
              row1.addElement(cadena);
            }
            else {
              row1.addElement(new String(""));
            }
            // a�adimos el municipio
            if (datos.get("DS_MUN") != null) {
              cadena = ( (datos.get("DS_MUN")).toString());
              row1.addElement(cadena);
            }
            else {
              row1.addElement(new String(""));
            }
            cadena = "";
            if (indice == r) {
              // a�adimos el domicilio elegido
              if (indiceDomi == 0) {
                cadena = (datos.get("DS_DIREC")).toString();
              }
              else if (indiceDomi == 1) {
                cadena = (datos.get("DS_DIREC2")).toString();
              }
              else if (indiceDomi == 2) {
                cadena = (datos.get("DS_DIREC3")).toString();
              }
            }
            else {
              //a�adimos el registro normal
              // a�adimos el domicilio
              if (datos.get("DS_DIREC") != null) {
                cadena = (datos.get("DS_DIREC")).toString();
              }
              else {
                cadena = "";
              }
            }
            row1.addElement(cadena);

            // y ahora el gr�fico
            //# // System_out.println("direc2 " + datos.get("DS_DIREC2"));

            if (datos.get("DS_DIREC2") != null) {
              row1.addElement(imgs.getImage(2));
            }

            // a�adimos la l�nea
            items.addElement(row1);
          }
          tabla.setItems(items);
        }

      }
      else { // mostrar mensaje de error
        mensaje = new CMessage(this.app, CMessage.msgAVISO,
                               res.getString("msg1.Text"));
        mensaje.show();
        mensaje = null;
      }
    }
    else { // mostrar mensaje de error
      mensaje = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg3.Text"));
      mensaje.show();
      mensaje = null;
    }
  }

  //CARGAMOS LOS DATOS PARA LA SALIDA EN PANTALLA EDOINDIV
  void btnAceptar_actionPerformed(ActionEvent e) {
    CMessage mensaje = null;
    int dirActual = 0;
    String dirDS = "";

    if (tabla.getSelectedIndex() != BWTEnum.NOTFOUND) {

      Resultados = new DataValoresEDO();

      datos = (enfermo.DataEnfermo)
          listaSalida.elementAt(tabla.getSelectedIndex());

      if (tabla.getSelectedIndex() == ultIndice) {

        switch (ultDomi) {

          case 0:

            //DIREC, MUN ,PROV, POST, NUM, PISO, TELEFONO
            Resultados.sCodCalle = "1";

            if (datos.get("DS_DIREC") != null) {
              Resultados.sDesCalle = datos.get("DS_DIREC").toString();
            }
            if (datos.get("CD_MUN") != null) {
              Resultados.sCodMun = datos.get("CD_MUN").toString();
            }
            if (datos.get("DS_MUN") != null) {
              Resultados.sDesMun = datos.get("DS_MUN").toString();
            }
            if (datos.get("CD_PROV") != null) {
              Resultados.sCodProv = datos.get("CD_PROV").toString();
            }
            if (datos.get("DS_PROV") != null) {
              Resultados.sDesProv = datos.get("DS_PROV").toString();
            }

            // c�digo de CA
            if (datos.get("CD_CA") != null) {
              Resultados.sCodCA = datos.get("CD_CA").toString();
            }
            if (datos.get("DS_CA") != null) {
              Resultados.sDesCA = datos.get("DS_CA").toString();

            }
            if (datos.get("CD_POSTAL") != null) {
              Resultados.sCPostal = datos.get("CD_POSTAL").toString();
            }

            if (datos.get("DS_NUM") != null) {
              Resultados.sNumero = datos.get("DS_NUM").toString();
            }

            if (datos.get("DS_PISO") != null) {
              Resultados.sPiso = datos.get("DS_PISO").toString();
            }

            if (datos.get("DS_TELEF") != null) {
              Resultados.sTelefono = datos.get("DS_TELEF").toString();
            }

            //campos de suca ----   para domicilio 1
            if (datos.get("CDTVIA") != null) {
              Resultados.CDTVIA = datos.get("CDTVIA").toString();
            }
            if (datos.get("CDVIAL") != null) {
              Resultados.CDVIAL = datos.get("CDVIAL").toString();
            }
            if (datos.get("CDTNUM") != null) {
              Resultados.CDTNUM = datos.get("CDTNUM").toString();
            }
            if (datos.get("DSCALNUM") != null) {
              Resultados.DSCALNUM = datos.get("DSCALNUM").toString();
            }

            //-----------------------

            break;

          case 1:

            //DIREC, MUN ,PROV, POST, NUM, PISO, TELEFONO
            Resultados.sCodCalle = "2";

            if (datos.get("DS_DIREC2") != null) {
              Resultados.sDesCalle = datos.get("DS_DIREC2").toString();
            }
            if (datos.get("CD_MUN2") != null) {
              Resultados.sCodMun = datos.get("CD_MUN2").toString();
            }
            if (datos.get("DS_MUN2") != null) {
              Resultados.sDesMun = datos.get("DS_MUN2").toString();
            }
            if (datos.get("CD_PROV2") != null) {
              Resultados.sCodProv = datos.get("CD_PROV2").toString();
            }
            if (datos.get("DS_PROV2") != null) {
              Resultados.sDesProv = datos.get("DS_PROV2").toString();
            }

            // c�digo de CA
            if (datos.get("CD_CA2") != null) {
              Resultados.sCodCA = datos.get("CD_CA2").toString();
            }
            if (datos.get("DS_CA2") != null) {
              Resultados.sDesCA = datos.get("DS_CA2").toString();

            }
            if (datos.get("CD_POST2") != null) {
              Resultados.sCPostal = datos.get("CD_POST2").toString();
            }
            if (datos.get("DS_NUM2") != null) {
              Resultados.sNumero = datos.get("DS_NUM2").toString();
            }
            if (datos.get("DS_PISO2") != null) {
              Resultados.sPiso = datos.get("DS_PISO2").toString();
            }

            if (datos.get("DS_TELEF2") != null) {
              Resultados.sTelefono = datos.get("DS_TELEF2").toString();
            }

            break;

          case 2:

            //DIREC, MUN ,PROV, POST, NUM, PISO, TELEFONO
            Resultados.sCodCalle = "3";

            if (datos.get("DS_DIREC3") != null) {
              Resultados.sDesCalle = datos.get("DS_DIREC3").toString();
            }
            if (datos.get("CD_MUN3") != null) {
              Resultados.sCodMun = datos.get("CD_MUN3").toString();
            }

            if (datos.get("DS_MUN3") != null) {
              Resultados.sDesMun = datos.get("DS_MUN3").toString();
            }
            if (datos.get("CD_PROV2") != null) {
              Resultados.sCodProv = datos.get("CD_PROV2").toString();
            }
            if (datos.get("DS_PROV2") != null) {
              Resultados.sDesProv = datos.get("DS_PROV2").toString();
            }

            // c�digo de CA
            if (datos.get("CD_CA3") != null) {
              Resultados.sCodCA = datos.get("CD_CA3").toString();
            }
            if (datos.get("DS_CA3") != null) {
              Resultados.sDesCA = datos.get("DS_CA3").toString();

            }
            if (datos.get("CD_POST3") != null) {
              Resultados.sCPostal = datos.get("CD_POST3").toString();
            }
            if (datos.get("DS_NUM3") != null) {
              Resultados.sNumero = datos.get("DS_NUM3").toString();
            }
            if (datos.get("DS_PISO3") != null) {
              Resultados.sPiso = datos.get("DS_PISO3").toString();
            }

            if (datos.get("DS_TELEF3") != null) {
              Resultados.sTelefono = datos.get("DS_TELEF3").toString();
            }

            break;
        }
      }
      else { //el domicilio es el primero
        Resultados.sCodCalle = "1";

        if (datos.get("DS_DIREC") != null) {
          Resultados.sDesCalle = datos.get("DS_DIREC").toString();
        }
        if (datos.get("CD_MUN") != null) {
          Resultados.sCodMun = datos.get("CD_MUN").toString();
        }
        if (datos.get("DS_MUN") != null) {
          Resultados.sDesMun = datos.get("DS_MUN").toString();
        }
        if (datos.get("CD_PROV") != null) {
          Resultados.sCodProv = datos.get("CD_PROV").toString();
        }
        if (datos.get("DS_PROV") != null) {
          Resultados.sDesProv = datos.get("DS_PROV").toString();
        }
        // c�digo de CA
        if (datos.get("CD_CA") != null) {
          Resultados.sCodCA = datos.get("CD_CA").toString();
        }
        if (datos.get("DS_CA") != null) {
          Resultados.sDesCA = datos.get("DS_CA").toString();

        }
        if (datos.get("CD_POSTAL") != null) {
          Resultados.sCPostal = datos.get("CD_POSTAL").toString();
        }

        if (datos.get("DS_NUM") != null) {
          Resultados.sNumero = datos.get("DS_NUM").toString();
        }

        if (datos.get("DS_PISO") != null) {
          Resultados.sPiso = datos.get("DS_PISO").toString();
        }

        if (datos.get("DS_TELEF") != null) {
          Resultados.sTelefono = datos.get("DS_TELEF").toString();
        }
        //campos de suca ----   para domicilio 1
        if (datos.get("CDTVIA") != null) {
          Resultados.CDTVIA = datos.get("CDTVIA").toString();
        }
        if (datos.get("CDVIAL") != null) {
          Resultados.CDVIAL = datos.get("CDVIAL").toString();
        }
        if (datos.get("CDTNUM") != null) {
          Resultados.CDTNUM = datos.get("CDTNUM").toString();
        }
        if (datos.get("DSCALNUM") != null) {
          Resultados.DSCALNUM = datos.get("DSCALNUM").toString();
        }
        //-----------------------

      } //fin 1-2-3 ...else

      // Se ha pulsado OK
      bFlag_OK = true;

      // NOMBRE ------ ---- --- -- -- - - - - -  -
      if (datos.get("DS_NOMBRE") != null) {
        Resultados.sNombre = datos.get("DS_NOMBRE").toString();
      }
      if (datos.get("DS_APE1") != null) {
        Resultados.sApe1 = datos.get("DS_APE1").toString();
      }
      if (datos.get("DS_APE2") != null) {
        Resultados.sApe2 = datos.get("DS_APE2").toString();
      }
      //siglas
      if (datos.get("SIGLAS") != null) {
        Resultados.sSiglas = datos.get("SIGLAS").toString();
      }
      // c�digo enfermo
      Resultados.sCodEnfermo = (datos.get("CD_ENFERMO")).toString();

      // fecha nacimiento  -----
      if (datos.get("FC_NAC") != null) {
        Resultados.sFechaNac = datos.getFechaNacimiento();
      }
      if (datos.get("IT_CALC") != null) {
        Resultados.sItCalc = (datos.get("IT_CALC")).toString();
      }
      //--------

      // c�digo nivel 1
      if (datos.get("CD_NIVEL_1") != null) {
        Resultados.sCodNivel1 = datos.get("CD_NIVEL_1").toString();

        // descripci�n nivel 1
        if (datos.get("DS_NIVEL_1") != null) {
          Resultados.sDesNivel1 = datos.get("DS_NIVEL_1").toString();
        }
      }

      // c�digo nivel 2
      if (datos.get("CD_NIVEL_2") != null) {
        Resultados.sCodNivel2 = datos.get("CD_NIVEL_2").toString();

        // descripci�n nivel 2
        if (datos.get("DS_NIVEL_2") != null) {
          Resultados.sDesNivel2 = datos.get("DS_NIVEL_2").toString();
        }
      }

      // c�digo zbs
      if (datos.get("CD_ZBS") != null) {
        Resultados.sCodZBS = datos.get("CD_ZBS").toString();

        // descripci�n zbs
        if (datos.get("DS_ZBS") != null) {
          Resultados.sDesZBS = datos.get("DS_ZBS").toString();
        }
      }

      //SEXO ----
      if (datos.get("CD_SEXO") != null) {
        Resultados.sCodSexo = datos.get("CD_SEXO").toString();

        // descripci�n nivel 2
        if (datos.get("DS_SEXO") != null) {
          Resultados.sDesSexo = datos.get("DS_SEXO").toString();
        }
      } //-------

      //DOCUMENTO  -----
      // tipo documento
      if (datos.get("CD_TDOC") != null) {
        Resultados.sTDoc = datos.get("CD_TDOC").toString(); //sCodTDoc
        // descripci�n nivel 2
        if (datos.get("DS_TIPODOC") != null) {
          Resultados.sDesTDoc = datos.get("DS_TIPODOC").toString();
        }
      }
      //documento
      if (datos.get("DS_NDOC") != null) {
        Resultados.sNDoc = datos.get("DS_NDOC").toString();
      } //----------------

      dispose();

    }
    else {
      mensaje = new CMessage(this.app, CMessage.msgERROR,
                             res.getString("msg4.Text"));
      mensaje.show();
      mensaje = null;
    }
  }

  void btnCancelar_actionPerformed(ActionEvent e) {
    // Se ha pulsado CANCEL
    bFlag_OK = false;
    dispose();
  }

  void tabla_actionPerformed(JCActionEvent e) {
    btnAceptar_actionPerformed(null);
  }
}

class DialogSelEnfermo_btnDomi_actionAdapter
    implements java.awt.event.ActionListener {
  DialogSelEnfermo adaptee;

  DialogSelEnfermo_btnDomi_actionAdapter(DialogSelEnfermo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnDomi_actionPerformed(e);
  }
}

class DialogSelEnfermo_btnAceptar_actionAdapter
    implements java.awt.event.ActionListener {
  DialogSelEnfermo adaptee;

  DialogSelEnfermo_btnAceptar_actionAdapter(DialogSelEnfermo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnAceptar_actionPerformed(e);
  }
}

class DialogSelEnfermo_btnCancelar_actionAdapter
    implements java.awt.event.ActionListener {
  DialogSelEnfermo adaptee;

  DialogSelEnfermo_btnCancelar_actionAdapter(DialogSelEnfermo adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnCancelar_actionPerformed(e);
  }
}
