
package envressem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Vector;

import capp.CLista;
import enfermo.Fechas;
import sapp.DBServlet;

public class SrvEnvBro
    extends DBServlet {

  final int servletGENFICH_BRO = 1;

  String sLinea = null; //Linea de un fichero

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int k; //Para �ndice b�squeda columnas

    // objetos de datos
    CLista data = null; //lista de resultado al cliente (lineas, es decir, Strings)
    DataFich datFich = null; //Para datos de petici�n
    CLista dataAux; //lista auxiliar local (uso en Servlet) para brotes o brotes hist�ricos
    DataBrote datBrote = null; //Elementos lista local (uso en Servlet)
    Enumeration enum; //Para recorrer listas

    //Par�metros recibidos para generar fichero
    String sSep = "|"; //Caracter separador entre datos de una l�nea
    String sAno = "";
    String sComAut = "";
    String sTSive = "";

    //Para seleccion de tabla CATALOGOS
    String sCodTab = "";
    String sCodVer = "";
    String sItOk = "";

    //Datos de brote
    String sCodAno = ""; //A�o
    int iAleBro; //N�mero de Alerta de brote
    String sCodProCol = ""; //Provincia
    String sCodMunCol = ""; //Municipio
    int iExp; //Expuestos
    int iEnf; //Enfermos
    int iIngHos; //Ingresos en hospital
    int iDef; //Defunciones
    String sFecIniSin = ""; //Fecha inicio sintomas
    String sFecFinSin = ""; //Fecha de fin de s�ntomas
    int iPerInMin; //Per�odo de incubaci�n m�nimo
    int iPerInMax; //Per�odo de incubaci�n m�ximo
    int iPerInMed; //Per�odo de incubaci�n medio
    int iDurCuaMin; //Duraci�n cuadro cl�nico m�nimo
    int iDurCuaMax; //Duraci�n cuadro cl�nico m�ximo
    int iDurCuaMed; //Duraci�n cuadro cl�nico medio
    String sCodTra = ""; //C�d de transmisi�n
    String sDesObs = ""; //Observaci�n
    String sEn = ""; //Depende de IT_PERIN y de IT_DCUAC

    final int iNUMBER_NULO = -1; //Indica el NUMBER en b.datos est� a null

    //Para Conversi�n de enteros a String
    String sAleBro; //N�mero de Alerta de brote
    String sExp; //Expuestos
    String sEnf; //Enfermos
    String sIngHos; //Ingresos en hospital
    String sDef; //Defunciones
    String sPerInMin; //Per�odo de incubaci�n m�nimo
    String sPerInMax; //Per�odo de incubaci�n m�ximo
    String sPerInMed; //Per�odo de incubaci�n medio
    String sDurCuaMin; //Duraci�n cuadro cl�nico m�nimo
    String sDurCuaMax; //Duraci�n cuadro cl�nico m�ximo
    String sDurCuaMed; //Duraci�n cuadro cl�nico medio
    //Para recibir fechas de b.datos (DAtes: luego se pasan a String)
    java.sql.Date dFecIniSin = null; //Fecha inicio sintomas
    java.sql.Date dFecFinSin = null; //Fecha de fin de s�ntomas
    //Para calcular campo En
    String sItPerIn = "";
    String sItDurCua = "";

    //Datos del modelo
    String sCodMod = "";
    int iNumLin = 0;
    String sDesRes = "";
    Vector vLineas = new Vector();

    //________________________________________________________

    //Consulta que recoge los brotes
    //Corresponden a alertas no anuladas: en tabla SIVE_ALERTA_BROTES van tienen campo CD_SITALERBRO a '3' o '4'
    final String sLISTADO_BROTES = "select CD_ANO, NM_ALERBRO, CD_PROVCOL, CD_MUNCOL, NM_EXPUESTOS, NM_ENFERMOS, NM_INGHOSP, NM_DEFUNCION, " +
        " FC_ISINPRIMC, FC_FSINPRIMC, NM_PERINMIN, NM_PERINMAX, NM_PERINMED, NM_DCUACMIN, NM_DCUACMAX, NM_DCUACMED, CD_TRANSMIS, DS_OBSERV, IT_PERIN, IT_DCUAC from SIVE_BROTES where CD_ANO = ? ";

    final String sLISTADO_HIST_BROTES = "select CD_ANO, NM_ALERBRO, CD_PROVCOL, CD_MUNCOL, NM_EXPUESTOS, NM_ENFERMOS, NM_INGHOSP, NM_DEFUNCION, " +
        " FC_ISINPRIMC, FC_FSINPRIMC, NM_PERINMIN, NM_PERINMAX, NM_PERINMED, NM_DCUACMIN, NM_DCUACMAX, NM_DCUACMED, CD_TRANSMIS, DS_OBSERV, IT_PERIN, IT_DCUAC from SIVE_HIST_BROTES where CD_ANO = ? ";

    //Para recoger campos version y OK de tabla catalogos
    final String sBUSQUEDA_CATALOGOS =
        "select * from SIVE_CATALOGOS where CD_TABLA = ? ";

    //Para recoger el modelo con respuestas correspondiente a un brote y al CNE
    final String sLISTADO_MODELOS = "select CD_MODELO from SIVE_MODELO where CD_TSIVE = 'B' and CD_NIVEL_1 is null and CD_NIVEL_2 is null and CD_CA is null and CD_MODELO in " +
        " (select CD_MODELO from SIVE_RESP_BROTES where CD_ANO = ? and NM_ALERBRO = ?)";

    final String sLISTADO_LINEAS = "select NM_LIN from SIVE_LINEA_ITEM where CD_MODELO = ? and CD_TSIVE='B' order by NM_LIN";
    final String sLISTADO_RESPUESTAS = "select DS_RESPUESTA from SIVE_RESP_BROTES where CD_ANO = ? and NM_ALERBRO = ? and CD_MODELO = ?  and NM_LIN = ? ";

    //___________________________________________________________________

    // establece la conexi�n con la base de datos
    con = openConnection();
    datFich = (DataFich) param.firstElement();
    // prepara la lista de resultados
    data = new CLista();

    // modos de operaci�n
    switch (opmode) {

      //======================== Modo gen fich edo ind===================================

      // listado
      case servletGENFICH_BRO:

        // System_out.println("Entra en servlet resBro*********************");

        //Se crean listas que se van a enviar
        data.addElement(new DataResInd());
        ( (DataResInd) (data.firstElement())).resumen = new CLista();
        ( (DataResInd) (data.firstElement())).errores = new CLista();
        ( (DataResInd) (data.firstElement())).catalogos = new CLista();

        //Recogida Par�mettros enviados
        if (opmode == servletGENFICH_BRO) {
          sAno = datFich.getAno();
        }

        //Recogida Par�mettros enviados
        sSep = datFich.getSep();
        sComAut = datFich.getComAut();
        sTSive = datFich.getTSive();

        // System_out.println("Recogidos paramentros*********************");

        try {

          //Se a�aden lineas con datos de tabla CATALOGOS_____________________
          //____  Varias Consultas en cat�logos, una para cada c�digo de tabla
          //Si es necesario Se a�aden las correspondientes tres lineas de fichero en cada caso

          for (int contad = 0; contad < 4; contad++) {

            // prepara la query
            st = con.prepareStatement(sBUSQUEDA_CATALOGOS);

            // System_out.println("Prep catalogos*********************");

            //Codigo Tabla
            if (contad == 0) {
              sCodTab = "SIVE_COM_AUT";
            }
            else if (contad == 1) {
              sCodTab = "SIVE_PROVINCIA";
            }
            else if (contad == 2) {
              sCodTab = "SIVE_MUNICIPIO";
            }
            else if (contad == 3) {
              sCodTab = "SIVE_MEC_TRANS";

            }
            st.setString(1, sCodTab);

            rs = st.executeQuery();

            // extrae la p�gina requerida (como m�ximo ser� una en cada consulta
            while (rs.next()) {

              // obtiene los campos
              sCodVer = rs.getString("CD_VERSION");
              sItOk = rs.getString("IT_OK");

              // a�ade linea indica tabla
              sLinea = "[" + sCodTab + "]";
              ( ( (DataResInd) (data.firstElement())).catalogos).addElement(
                  sLinea);

              // a�ade linea indica version
              sLinea = "version=";
              sLinea = sLinea + sCodVer;
              ( ( (DataResInd) (data.firstElement())).catalogos).addElement(
                  sLinea);

              // a�ade linea indica OK
              sLinea = "OK=";
              sLinea = sLinea + sItOk;
              ( ( (DataResInd) (data.firstElement())).catalogos).addElement(
                  sLinea);
            }
            rs.close();
            st.close();
            rs = null;
            st = null;
            // System_out.println("fin catalogos*********************");
          } //Fin for

          //For para recorrer brotes e hist�rico brotes
          for (int contador = 0; contador < 2; contador++) {

            //No se va a modificar la b. datos
            con.setAutoCommit(true);
            //Se crea la nueva lista para brotes o brotes hist�ricos
            dataAux = new CLista();

            //______________________  Consulta brotes o hist�rico brotes___________________________
            if (contador == 0) {
              // prepara la query
              st = con.prepareStatement(sLISTADO_BROTES);
              // System_out.println("Prep listado brotes*********************");
            }
            else {
              // prepara la query
              st = con.prepareStatement(sLISTADO_HIST_BROTES);
              // System_out.println("Prep listado historico brotes*********************");
            }

            st.setString(1, sAno);
            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {

              // obtiene los campos

              sCodAno = rs.getString("CD_ANO");
              iAleBro = rs.getInt("NM_ALERBRO");

              datBrote = new DataBrote(sCodAno, iAleBro);

              sCodProCol = rs.getString("CD_PROVCOL");
              if (sCodProCol == null) {
                sCodProCol = "";
              }
              datBrote.setCodProCol(sCodProCol);

              sCodMunCol = rs.getString("CD_MUNCOL");
              if (sCodMunCol == null) {
                sCodMunCol = "";
              }
              datBrote.setCodMunCol(sCodMunCol);

              iExp = rs.getInt("NM_EXPUESTOS");
              if (rs.wasNull()) {
                iExp = iNUMBER_NULO;
              }
              datBrote.setExp(iExp);

              iEnf = rs.getInt("NM_ENFERMOS");
              if (rs.wasNull()) {
                iEnf = iNUMBER_NULO;
              }
              datBrote.setEnf(iEnf);

              iIngHos = rs.getInt("NM_INGHOSP");
              if (rs.wasNull()) {
                iIngHos = iNUMBER_NULO;
              }
              datBrote.setIngHos(iIngHos);

              iDef = rs.getInt("NM_DEFUNCION");
              if (rs.wasNull()) {
                iDef = iNUMBER_NULO;
              }
              datBrote.setDef(iDef);

              dFecIniSin = rs.getDate("FC_ISINPRIMC");
              if (rs.wasNull()) {
                sFecIniSin = "";
              }
              else {
                sFecIniSin = Fechas.date2String(dFecIniSin);
              }
              datBrote.setFecIniSin(sFecIniSin);

              dFecFinSin = rs.getDate("FC_FSINPRIMC");
              if (rs.wasNull()) {
                sFecFinSin = "";
              }
              else {
                sFecFinSin = Fechas.date2String(dFecFinSin);
              }
              datBrote.setFecFinSin(sFecFinSin);

              iPerInMin = rs.getInt("NM_PERINMIN");
              if (rs.wasNull()) {
                iPerInMin = iNUMBER_NULO;
              }
              datBrote.setPerInMin(iPerInMin);

              iPerInMax = rs.getInt("NM_PERINMAX");
              if (rs.wasNull()) {
                iPerInMax = iNUMBER_NULO;
              }
              datBrote.setPerInMax(iPerInMax);

              iPerInMed = rs.getInt("NM_PERINMED");
              if (rs.wasNull()) {
                iPerInMed = iNUMBER_NULO;
              }
              datBrote.setPerInMed(iPerInMed);

              iDurCuaMin = rs.getInt("NM_DCUACMIN");
              if (rs.wasNull()) {
                iDurCuaMin = iNUMBER_NULO;
              }
              datBrote.setDurCuaMin(iDurCuaMin);

              iDurCuaMax = rs.getInt("NM_DCUACMAX");
              if (rs.wasNull()) {
                iDurCuaMax = iNUMBER_NULO;
              }
              datBrote.setDurCuaMax(iDurCuaMax);

              iDurCuaMed = rs.getInt("NM_DCUACMED");
              if (rs.wasNull()) {
                iDurCuaMed = iNUMBER_NULO;
              }
              datBrote.setDurCuaMed(iDurCuaMed);

              //C�lculo campo En:
              sItPerIn = rs.getString("IT_PERIN");
              sItDurCua = rs.getString("IT_DCUAC");
              if ( (sItPerIn.equals("H")) && (sItDurCua.equals("H"))) {
                sEn = "H";
              }
              else if ( (sItPerIn.equals("H")) && (sItDurCua.equals("D"))) {
                sEn = "Y";
              }
              else if ( (sItPerIn.equals("D")) && (sItDurCua.equals("H"))) {
                sEn = "Z";
              }
              else if ( (sItPerIn.equals("D")) && (sItDurCua.equals("D"))) {
                sEn = "C";
              }

              datBrote.setEn(sEn);

              sCodTra = rs.getString("CD_TRANSMIS");
              if (sCodTra == null) {
                sCodTra = "";
              }
              datBrote.setCodTra(sCodTra);

              sDesObs = rs.getString("DS_OBSERV");
              if (sDesObs == null) {
                sDesObs = "";
              }
              datBrote.setDesObs(sDesObs);

              // a�ade un nodo
              dataAux.addElement(datBrote);

            }

            rs.close();
            st.close();
            rs = null;
            st = null;
            // System_out.println("Fin losedos  *********************");
            // System_out.println("dataAux  *********************" + dataAux.toString());

            //______________________  recorrido lista___________________________

            enum = dataAux.elements();
            while (enum.hasMoreElements()) { //Recorremos lista
              datBrote = (DataBrote) (enum.nextElement());
              // System_out.println("extrae elem de lista  *********************");

              //___________________________________________________________________

              //Se pasan los enteros a String antes de meterlos en linea
              sAleBro = Integer.toString(datBrote.getAleBro());

              if (datBrote.getExp() == iNUMBER_NULO) {
                sExp = "";
              }
              else {
                sExp = Integer.toString(datBrote.getExp());

              }
              if (datBrote.getEnf() == iNUMBER_NULO) {
                sEnf = "";
              }
              else {
                sEnf = Integer.toString(datBrote.getEnf());

              }
              if (datBrote.getIngHos() == iNUMBER_NULO) {
                sIngHos = "";
              }
              else {
                sIngHos = Integer.toString(datBrote.getIngHos());

              }
              if (datBrote.getDef() == iNUMBER_NULO) {
                sDef = "";
              }
              else {
                sDef = Integer.toString(datBrote.getDef());

              }
              if (datBrote.getPerInMax() == iNUMBER_NULO) {
                sPerInMax = "";
              }
              else {
                sPerInMax = Integer.toString(datBrote.getPerInMax());

              }
              if (datBrote.getPerInMin() == iNUMBER_NULO) {
                sPerInMin = "";
              }
              else {
                sPerInMin = Integer.toString(datBrote.getPerInMin());

              }
              if (datBrote.getPerInMed() == iNUMBER_NULO) {
                sPerInMed = "";
              }
              else {
                sPerInMed = Integer.toString(datBrote.getPerInMed());

              }
              if (datBrote.getDurCuaMin() == iNUMBER_NULO) {
                sDurCuaMin = "";
              }
              else {
                sDurCuaMin = Integer.toString(datBrote.getDurCuaMin());

              }
              if (datBrote.getDurCuaMax() == iNUMBER_NULO) {
                sDurCuaMax = "";
              }
              else {
                sDurCuaMax = Integer.toString(datBrote.getDurCuaMax());

              }
              if (datBrote.getDurCuaMed() == iNUMBER_NULO) {
                sDurCuaMed = "";
              }
              else {
                sDurCuaMed = Integer.toString(datBrote.getDurCuaMed());

                //Si estamos con los brotes actuales
              }
              if (contador == 0) {

                vLineas = new Vector();
                //______________________  Consulta en SIVE_MODELO___________________________

                int numModelos = 0; //N�mero de modelos que cumplen la query LISTADO_MODELO
                //Si no hay error debe ser solo uno
                st = con.prepareStatement(sLISTADO_MODELOS);
                // System_out.println("Prep modelo*********************");
                st.setString(1, datBrote.getCodAno());
                st.setInt(2, datBrote.getAleBro());

                rs = st.executeQuery();

                // extrae la p�gina requerida
                while (rs.next()) {
                  // obtiene los campos
                  sCodMod = rs.getString("CD_MODELO");
                  numModelos++;
                }
                rs.close();
                st.close();
                st = null;
                rs = null;
                // System_out.println("Fin modelo  *********************");

                //Si hay un solo modelo todo OK
                if (numModelos == 1) {

                  sLinea = sAleBro + sSep + datBrote.getCodAno() + sSep +
                      sComAut + sSep
                      + "M" + sSep + "N" + sSep + datBrote.getCodProCol() +
                      sSep + datBrote.getCodMunCol() + sSep
                      + sExp + sSep + sEnf + sSep + sIngHos + sSep + sDef +
                      sSep + sFecIniSin + sSep + sFecFinSin + sSep
                      + sEn + sSep + sPerInMin + sSep + sPerInMax + sSep +
                      sPerInMed + sSep
                      + sDurCuaMin + sSep + sDurCuaMax + sSep + sDurCuaMed +
                      sSep
                      + datBrote.getDesObs() + sSep + sCodMod + sSep;

                  //______________________  Consulta en  SIVE_LINEA_ITEM___________________________

                  st = con.prepareStatement(sLISTADO_LINEAS);
                  // System_out.println("Prep lineas*********************");
                  st.setString(1, sCodMod);

                  rs = st.executeQuery();

                  // extrae la p�gina requerida
                  while (rs.next()) {
                    // obtiene los campos
                    iNumLin = rs.getInt("NM_LIN");
                    vLineas.addElement(new DataLinea(iNumLin));

                  }
                  rs.close();
                  st.close();
                  st = null;
                  rs = null;
                  // System_out.println(" Fin lineas *********************");

                  //______________________  Consulta en  SIVE_RESP_EDO para cada linea ___________________________

                  for (int j = 0; j < vLineas.size(); j++) {

                    st = con.prepareStatement(sLISTADO_RESPUESTAS);
// System_out.println("Prep respuestas*********************");
                    st.setString(1, datBrote.getCodAno());
                    st.setInt(2, datBrote.getAleBro());
                    st.setString(3, sCodMod);
                    st.setInt(4,
                              ( (DataLinea) (vLineas.elementAt(j))).getNumLin());

                    rs = st.executeQuery();

                    // extrae la p�gina requerida
                    while (rs.next()) {
                      // obtiene los campos
                      sDesRes = rs.getString("DS_RESPUESTA");

                      //A�ade a la linea del fichero  la respta de la pregunta
                      sLinea = sLinea + sDesRes;
                    }
                    //Se a�ade separador independientemente de si se ha encontrado respta a la pregunta o no
                    sLinea = sLinea + sSep;

                    rs.close();
                    st.close();
                    st = null;
                    rs = null;
                    // System_out.println(" Fin respuestas *********************");

                  } //For recorre lineas

                  // a�ade una linea
                  ( ( (DataResInd) (data.firstElement())).resumen).addElement(
                      sLinea);
                  // System_out.println(" A�adida linea *********************");

                } //if hay un solo modelo

                //Si no hay un solo modelo
                else {

                  //if hay  m�s de un modelo , error
                  if (numModelos > 1) {
                    throw new Exception();
                  }

                  // No hay modelos para brote (numModelos = 0). Se meten datos del modelo
                  else {
                    sLinea = sAleBro + sSep + datBrote.getCodAno() + sSep +
                        sComAut + sSep
                        + "M" + sSep + "N" + sSep + datBrote.getCodProCol() +
                        sSep + datBrote.getCodMunCol() + sSep
                        + sExp + sSep + sEnf + sSep + sIngHos + sSep + sDef +
                        sSep + sFecIniSin + sSep + sFecFinSin + sSep
                        + sEn + sSep + sPerInMin + sSep + sPerInMax + sSep +
                        sPerInMed + sSep
                        + sDurCuaMin + sSep + sDurCuaMax + sSep + sDurCuaMed +
                        sSep
                        + datBrote.getDesObs() + sSep;

                    // a�ade una linea
                    ( ( (DataResInd) (data.firstElement())).resumen).addElement(
                        sLinea);

                  } //fin else de No hay modelos
                } //fin else hay m�s de uno o ninguno

              } //fin if contador es 0 (brotes)

              //Si el contador es 1 (hist�rico) no se busca modelo ni resptas
              else {

                sLinea = sAleBro + sSep + datBrote.getCodAno() + sSep + sComAut +
                    sSep
                    + "M" + sSep + "N" + sSep + datBrote.getCodProCol() + sSep +
                    datBrote.getCodMunCol() + sSep
                    + sExp + sSep + sEnf + sSep + sIngHos + sSep + sDef + sSep +
                    sFecIniSin + sSep + sFecFinSin + sSep
                    + sEn + sSep + sPerInMin + sSep + sPerInMax + sSep +
                    sPerInMed + sSep
                    + sDurCuaMin + sSep + sDurCuaMax + sSep + sDurCuaMed + sSep
                    + datBrote.getDesObs() + sSep;

                // a�ade una linea
                ( ( (DataResInd) (data.firstElement())).resumen).addElement(
                    sLinea);

              } //fin else de contador es 1

            } //while recorrido de vector
          } //For para recorrer brotes e hist�rico brotes

//____________________________________________________________________________

        }
        catch (Exception ex) {
          throw ex;
        }

        break;

        //____________________________________________________________________
        //____________________________________________________________________

    } //fin switch

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  } //doWork

  //____________________________________________________________________________

} //CLASE
