package envressem;

import java.io.Serializable;
import java.util.Vector;

public class DataNotInd
    implements Serializable {

  //Datos generales
  protected int iNumEdo;
  protected String sCodPro;
  protected String sCodMun;
  protected int iCodEnfermo;
  protected String sCodEnfCie;
  protected java.sql.Date dFecNot;
  protected String sTipCas;

  //Datos de a�o y semana
  protected String sCodAno;
  protected String sCodSem;

  //Datos de enfermedad EDO
  protected String sCodEnfEdo = "";

  //Datos de tabla ENFERMO
  protected String sEda; //Edad
  protected int iTipEda; //Tipo de edad
  protected String sSex; //Sexo
  protected String sSiglas = ""; //Siglas
  protected String sNombre; // Nombre
  protected String sApe1; // Primer apellido
  protected String sApe2; // Segundo apellido
  protected String sProceso; // Proceso

  //Datos del modelo
  protected String sCodMod;
  Vector vLineas = new Vector();

//Fecha de notificaci�n
  protected String sFecNot;
  protected String sFecRec = ""; //Fecha recep dada como String

  //Datos de com aut. de la provincia (para env�os a otras com aut)
  protected String sCodComAut = "";
  protected String sDesComAut = "";

  public DataNotInd() {
  }

  //Constructor (Datos de tabla SIVE_EDOIND)
  public DataNotInd(int numEdo, String codPro, String codMun,
                    int codEnfermo, String codEnfCie, java.sql.Date fecNot,
                    String tipCas) {
    iNumEdo = numEdo;
    sCodPro = codPro;
    sCodMun = codMun;
    iCodEnfermo = codEnfermo;
    sCodEnfCie = codEnfCie;
    dFecNot = fecNot;
    sTipCas = tipCas;
  }

  //Constructor (Datos de tabla SIVE_EDOIND)
  public DataNotInd(int numEdo, String codPro, String codMun,
                    int codEnfermo, String codEnfCie, java.sql.Date fecNot,
                    String tipCas,
                    String ano, String sem) {
    iNumEdo = numEdo;
    sCodPro = codPro;
    sCodMun = codMun;
    iCodEnfermo = codEnfermo;
    sCodEnfCie = codEnfCie;
    dFecNot = fecNot;
    sTipCas = tipCas;
    sCodAno = ano;
    sCodSem = sem;
  }

  public int getNumEdo() {
    return iNumEdo;
  }

  public String getCodPro() {
    return sCodPro;
  }

  public String getCodMun() {
    return sCodMun;
  }

  public int getCodEnfermo() {
    return iCodEnfermo;
  }

  public String getCodEnfCie() {
    return sCodEnfCie;
  }

  public java.sql.Date getFecNot() {
    return dFecNot;
  }

  public String getTipCas() {
    return sTipCas;
  }

  public String getCodAno() {
    return sCodAno;
  }

  public String getCodSem() {
    return sCodSem;
  }

  //Datos de nef. edo

  public void setCodEnfEdo(String codEnfEdo) {
    sCodEnfEdo = codEnfEdo;
  }

  public String getCodEnfEdo() {
    return sCodEnfEdo;
  }

  //Datos de edad y sexo ( En SIVE_ENFERMO)

  public void setEdadySexo(String eda, int tipEda, String sex) {
    sEda = eda;
    iTipEda = tipEda;
    sSex = sex;
  }

  public String getEda() {
    return sEda;
  }

  public int getTipEda() {
    return iTipEda;
  }

  public String getSex() {
    return sSex;
  }

  //Modelo de la enfemedad (SIVE_MODELO)

  public void setCodMod(String codMod) {
    sCodMod = codMod;
  }

  public String getCodMod() {
    return sCodMod;
  }

  public String getsFecNot() {
    return sFecNot;
  }

  //Fecha de recepci�n : para mostrarla en tabla de env�os urgentes

  public void setFecRec(String fecRec) {
    sFecRec = fecRec;
  }

  public String getFecRec() {
    return sFecRec;
  }

  public void setSiglas(String sigl) {
    sSiglas = sigl;
  }

  public String getSiglas() {
    return sSiglas;
  }

  // ARG: Nombre, apellidos y proceso
  public void setNombre(String nombre) {
    sNombre = nombre;
  }

  public void setApe1(String ape1) {
    sApe1 = ape1;
  }

  public void setApe2(String ape2) {
    sApe2 = ape2;
  }

  public void setProceso(String Proceso) {
    sProceso = Proceso;
  }

  public String getNombre() {
    return sNombre;
  }

  public String getApe1() {
    return sApe1;
  }

  public String getApe2() {
    return sApe2;
  }

  public String getProceso() {
    return sProceso;
  }

  //COm. Aut�noma (para env�os a otras comunidades)
  public void setCodComAut(String codComAut) {
    sCodComAut = codComAut;
  }

  public String getCodComAut() {
    return sCodComAut;
  }

  //Desc COm. Aut�noma (para env�os a otras comunidades)
  public void setDesComAut(String desComAut) {
    sDesComAut = desComAut;
  }

  public String getDesComAut() {
    return sDesComAut;
  }

}