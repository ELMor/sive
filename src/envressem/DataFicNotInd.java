
package envressem;

import java.io.Serializable;

public class DataFicNotInd
    implements Serializable {

  protected int iNumEdo;
  protected String sCodPro;
  protected String sCodMun;
  protected int iCodEnfermo;
  protected String sCodEnfCie;

  public DataFicNotInd(int numEdo, String codPro, String codMun,
                       int codEnfermo, String codEnfCie) {
    iNumEdo = numEdo;
    sCodPro = codPro;
    sCodMun = codMun;
    iCodEnfermo = codEnfermo;
    sCodEnfCie = codEnfCie;
  }

  public int getNumEdo() {
    return iNumEdo;
  }

  public String getCodPro() {
    return sCodPro;
  }

  public String getCodMun() {
    return sCodMun;
  }

  public int getCodEnfermo() {
    return iCodEnfermo;
  }

  public String getEnfCie() {
    return sCodEnfCie;
  }

}