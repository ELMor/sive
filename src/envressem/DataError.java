package envressem;

import java.io.Serializable;

//información del error para la generación de un caso EDO

public class DataError
    implements Serializable {

  int iNumEdo; //Num Caso EDO
  String sEnf = ""; //Enfermedad EDO
  String sFecRec = ""; //Fecha recepción
  String sFecNot = ""; //Fecah notificación
  String sMot = ""; //Motivo

  public DataError(int numEdo, String enf, String fecRec, String fecNot,
                   String motivo) {
    iNumEdo = numEdo;
    sEnf = enf;
    sFecRec = fecRec;
    sFecNot = fecNot;
    sMot = motivo;
  }

  int getNumEdo() {
    return iNumEdo;
  }

  String getEnf() {
    return sEnf;
  }

  String getFecRec() {
    return sFecRec;
  }

  String getFecNot() {
    return sFecNot;
  }

  String getMot() {
    return sMot;
  }

}