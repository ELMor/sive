
package envressem;

import java.io.Serializable;

public class DataResNum
    implements Serializable {

  public String sCodAno = "";
  public String sCodSem = "";
  public String sCodPro = "";
  public String sCodEnf = "";
  public String sNumCas = "";

  public DataResNum() {
  }

  public DataResNum(String codAno, String codSem, String codPro,
                    String codEnf, String numCas) {
    sCodAno = codAno;
    sCodSem = codSem;
    sCodPro = codPro;
    sCodEnf = codEnf;
    sNumCas = numCas;
  }

}