
package envressem;

import java.io.Serializable;

public class DataGeo
    implements Serializable {

  protected String sCodNiv1 = "";
  protected String sCodNiv2 = "";
  protected String sCodZbs = "";
  protected String sCodPro = "";
  protected String sCodMun = "";

  public DataGeo(String codNiv1, String codNiv2, String codZbs, String codPro,
                 String codMun) {

    sCodNiv1 = codNiv1;
    sCodNiv2 = codNiv2;
    sCodZbs = codZbs;
    sCodPro = codPro;
    sCodMun = codMun;
  }

  public String getCodNiv1() {
    return sCodNiv1;
  }

  public String getCodNiv2() {
    return sCodNiv2;
  }

  public String getCodZbs() {
    return sCodZbs;
  }

  public String getCodPro() {
    return sCodPro;
  }

  public String getCodMun() {
    return sCodMun;
  }

}
