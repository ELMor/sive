
package envressem;

import java.io.Serializable;

public class DataEnf
    implements Serializable {

  protected String sCodEnf = "";
  protected String sCodTVi = "";

  public DataEnf(String codEnf, String codTVi) {

    sCodEnf = codEnf;
    sCodTVi = codTVi;

  }

  public String getCodEnf() {
    return sCodEnf;
  }

  public String getCodTVi() {
    return sCodTVi;
  }

}