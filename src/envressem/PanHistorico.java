package envressem;

import java.util.Enumeration;
import java.util.ResourceBundle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CDialog;
import capp.CLista;
import capp.CTabla;

public class PanHistorico
    extends CDialog {
  ResourceBundle res;
  CLista listaFich;

  /**
   * Controla si la ventana es cerrada con el aspa
   */
  private boolean cerradaConAspa = false;

  // controles
  XYLayout xyLyt = new XYLayout();
  ButtonControl btnAceptar = new ButtonControl();
  public CTabla tabla = new CTabla();

  actionListener btnActionListener = new actionListener(this);

//  public PanHistorico(CApp a, DataHistorico datHistorico, String t)
  public PanHistorico(CApp a, CLista listaFich, String t) {
    super(a);
    try {
      res = ResourceBundle.getBundle("envressem.Res" + app.getIdioma());
      this.listaFich = listaFich;
      this.setTitle(t);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // aspecto de la ventana
  private void jbInit() throws Exception {
    String sCodPet = "";
    String sSemIni = "";
    String sSemFin = "";
    String sFechaIni = "";
    String sFechaFin = "";
    String sEstado = "";
    String sDesError = "";
    Enumeration enum; //Para recorrer listas

    setSize(500, 250);
    xyLyt.setHeight(250);
    xyLyt.setWidth(500);
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnAceptar.addActionListener(new PanHistorico_btnAceptar_actionAdapter(this));
    btnAceptar.setActionCommand("Aceptar");

    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "75\n75\n80\n80\n80\n2000"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tabla.ColumnButtonsStrings2")), '\n'));
    tabla.setNumColumns(6);
    tabla.setScrollbarDisplay(1); // Scroll horizontal y vertical

    this.setLayout(xyLyt);
    this.add(btnAceptar, new XYConstraints(400, 195, 80, 26));
    this.add(tabla, new XYConstraints(10, 10, 480, 180));

    // establece el escuchador
    btnAceptar.addActionListener(btnActionListener);

    this.addWindowListener(new EventosVentana(this));

    enum = listaFich.elements();
    while (enum.hasMoreElements()) {
      DataHistorico datHistorico = (DataHistorico) (enum.nextElement());

      sSemIni = datHistorico.sSemIni;
      sSemFin = datHistorico.sSemFin;
      sFechaIni = datHistorico.sFechaIni;
      sFechaFin = datHistorico.sFechaFin;
      sEstado = datHistorico.sEstado;
      sDesError = datHistorico.sDesError;

      tabla.addItem(sSemIni + "&" + sSemFin + "&" + sFechaIni + "&" +
                    sFechaFin + "&" + sEstado + "&" + sDesError, '&');
    }
  }

  public void btnAceptar_actionPerformed(ActionEvent e) {
    cerradaConAspa = false;
    dispose();
  }

  /**
   * Determina si la ventana es cerrada con el aspa
   * @return true si la ventana ha sido cerrada con el aspa
   */
  public boolean cancelada() {
    return cerradaConAspa;
  }

  /** Cierra el objeto gr�fico activo.
   *
   */
  protected void this_windowClosing() {
    cerradaConAspa = true;
    dispose(); // Se cierra con la cruz
  } //end this_windowClosing

}

/** Clase que se encarga de la gesti�n de los eventos de la ventana.
 *
 * @Author  LARG
 * @version 06/06/2000
 */

class EventosVentana
    implements WindowListener {
  PanHistorico ventana = null;

  /** La ventana actual se actualiza en el constructor
   *
   */
  public EventosVentana(PanHistorico ventana) {
    this.ventana = ventana;
  }

  /** Se cierra la ventana con el aspa.
   *
   */
  public void windowClosing(WindowEvent e) {
    ventana.this_windowClosing();
  }

  public void windowDeactivated(WindowEvent e) {
    ;
  }

  public void windowActivated(WindowEvent e) {
    ;
  }

  public void windowDeiconified(WindowEvent e) {
    ;
  }

  public void windowIconified(WindowEvent e) {
    ;
  }

  public void windowClosed(WindowEvent e) {
    ;
  }

  public void windowOpened(WindowEvent e) {
    ;
  }
}

// action listener
class actionListener
    implements ActionListener, Runnable {
  PanHistorico adaptee = null;
  ActionEvent e = null;

  public actionListener(PanHistorico adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  /**
   * Ejecuci�n de la accion del bot�n
   *
   * @author  ARG
   * @version 16/01/01
   */
  public void run() {
    if (e.getActionCommand().equals("Aceptar")) {
      adaptee.btnAceptar_actionPerformed(e);
    }
  }

} // actionListener

class PanHistorico_btnAceptar_actionAdapter
    implements java.awt.event.ActionListener {
  PanHistorico adaptee;

  PanHistorico_btnAceptar_actionAdapter(PanHistorico adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnAceptar_actionPerformed(e);
  }
}
