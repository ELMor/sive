//Title:        Your Product Name
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Norsistemas S.A.
//Company:      Norsistemas, S.A.
//Description:  Your description

package envressem;

import java.util.Enumeration;
import java.util.ResourceBundle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CTabla;

public class DialErrores
    extends CDialog {

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;
  ResourceBundle res = ResourceBundle.getBundle("envressem.Res" + app.getIdioma());
  // im�genes
  final String imgNAME[] = {
      "images/aceptar.gif"};

  CLista listaErrores = null;

  /*
    protected final String strCabeceraLista [] = {res.getString("msg1.Text"),
                                  "CA\nCA\nCA",
                                  "EU\nEU\nEU",
                                  "GA\nGA\nGA"};
   */

  protected final String strCabeceraLista = res.getString("msg1.Text");

  //controles
  XYLayout xYLayout1 = new XYLayout();
  ButtonControl btn4 = new ButtonControl();
  CTabla tbl = new CTabla();

  //  ESCUCHADOR PULSAR BOTONES:
  DialErroresbtnActionListener btnActionListener = new
      DialErroresbtnActionListener(this);

  // contructor
  public DialErrores(CApp a, CLista errores) {
    super(a);

    try {
      listaErrores = errores;
      this.setTitle(res.getString("this.Title"));
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    xYLayout1.setWidth(669);
    xYLayout1.setHeight(389);
    this.setLayout(xYLayout1);
    this.setSize(669, 389);

    btn4.setActionCommand("Aceptar");
    btn4.setLabel(res.getString("btn4.Label"));

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);

    imgs.CargaImagenes();

    btn4.setImage(imgs.getImage(0));

    // configuraci�n de la tabla
//    tbl.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(strCabeceraLista[app.getIdioma()], '\n') );
    tbl.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(
        strCabeceraLista, '\n'));
    tbl.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "50\n60\n80\n80\n370"), '\n'));
    tbl.setNumColumns(5);

    //
    this.add(tbl, new XYConstraints(34, 65, 616, 142));
    this.add(btn4, new XYConstraints(496, 219, 98, 30));

    //establece los escuchadores
    btn4.addActionListener(btnActionListener);

    Enumeration enum = listaErrores.elements();
    DataError datUnaLinea;
    String datLineaEscribir;
    while (enum.hasMoreElements()) {
      //Recogemos los datos de una l�nea
      datUnaLinea = (DataError) (enum.nextElement());
// System_out.println("%% CIE " + datUnaLinea.getEnf());
      datLineaEscribir = Integer.toString(datUnaLinea.getNumEdo()) + "&"
          + datUnaLinea.getEnf() + "&"
          + datUnaLinea.getFecRec() + "&"
          + datUnaLinea.getFecNot() + "&"
          + datUnaLinea.getMot();

      //A�adimos la l�nea a la tabla
      tbl.addItem(datLineaEscribir, '&'); //'&' es el car�cter separador de datos (cada datos va a una columnna)
    }

  }

  public void btn4_actionPerformed(ActionEvent evt) {
    this.dispose();

  } //fin btn4

// action listener para los botones
  class DialErroresbtnActionListener
      implements ActionListener {
    DialErrores adaptee = null;
    ActionEvent e = null;

    public DialErroresbtnActionListener(DialErrores adaptee) {
      this.adaptee = adaptee;
    }

    // evento
    public void actionPerformed(ActionEvent e) {
      this.e = e;
      if (e.getActionCommand().equals("Aceptar")) { // Aceptar
        adaptee.btn4_actionPerformed(e);
      }
    }
  }

} //CLASE
