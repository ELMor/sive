
package envressem;

import java.io.Serializable;

public class DataBrote
    implements Serializable {
  //Claves en tabla
  String sCodAno = ""; //A�o
  int iAleBro; //N�mero de Alerta de brote

  //Resto de campos
  String sCodProCol = ""; //Provincia
  String sCodMunCol = ""; //Municipio
  int iExp; //Expuestos
  int iEnf; //Enfermos
  int iIngHos; //Ingresos en hospital
  int iDef; //Defunciones
  String sFecIniSin = ""; //Fecha inicio sintomas
  String sFecFinSin = ""; //Fecha de fin de s�ntomas
  int iPerInMin; //Per�odo de incubaci�n m�nimo
  int iPerInMax; //Per�odo de incubaci�n m�ximo
  int iPerInMed; //Per�odo de incubaci�n medio
  int iDurCuaMin; //Duraci�n cuadro cl�nico m�nimo
  int iDurCuaMax; //Duraci�n cuadro cl�nico m�ximo
  int iDurCuaMed; //Duraci�n cuadro cl�nico medio
  String sCodTra = ""; //C�d de transmisi�n
  String sDesObs = ""; //Observaci�n

  //Calculado
  String sEn = ""; //Depende de IT_PERIN y de IT_DCUAC

  //Constructor
  public DataBrote(String codAno, int aleBro) {

    sCodAno = codAno;
    iAleBro = aleBro;
  }

  //M�todos escritura

  public void setCodProCol(String CodProCol) {
    sCodProCol = CodProCol;
  }

  public void setCodMunCol(String CodMunCol) {
    sCodMunCol = CodMunCol;
  }

  public void setExp(int Exp) {
    iExp = Exp;
  }

  public void setEnf(int Enf) {
    iEnf = Enf;
  }

  public void setIngHos(int IngHos) {
    iIngHos = IngHos;
  }

  public void setDef(int Def) {
    iDef = Def;
  }

  public void setFecIniSin(String FecIniSin) {
    sFecIniSin = FecIniSin;
  }

  public void setFecFinSin(String FecFinSin) {
    sFecFinSin = FecFinSin;
  }

  public void setPerInMin(int PerInMin) {
    iPerInMin = PerInMin;
  }

  public void setPerInMax(int PerInMax) {
    iPerInMax = PerInMax;
  }

  public void setPerInMed(int PerInMed) {
    iPerInMed = PerInMed;
  }

  public void setDurCuaMin(int DurCuaMin) {
    iDurCuaMin = DurCuaMin;
  }

  public void setDurCuaMax(int DurCuaMax) {
    iDurCuaMax = DurCuaMax;
  }

  public void setDurCuaMed(int DurCuaMed) {
    iDurCuaMed = DurCuaMed;
  }

  public void setCodTra(String CodTra) {
    sCodTra = CodTra;
  }

  public void setDesObs(String DesObs) {
    sDesObs = DesObs;
  }

  public void setEn(String En) {
    sEn = En;
  }

  //M�todos de lectura

  public String getCodAno() {
    return sCodAno;
  }

  public int getAleBro() {
    return iAleBro;
  }

  public String getCodProCol() {
    return sCodProCol;
  }

  public String getCodMunCol() {
    return sCodMunCol;
  }

  public int getExp() {
    return iExp;
  }

  public int getEnf() {
    return iEnf;
  }

  public int getIngHos() {
    return iIngHos;
  }

  public int getDef() {
    return iDef;
  }

  public String getFecIniSin() {
    return sFecIniSin;
  }

  public String getFecFinSin() {
    return sFecFinSin;
  }

  public int getPerInMin() {
    return iPerInMin;
  }

  public int getPerInMax() {
    return iPerInMax;
  }

  public int getPerInMed() {
    return iPerInMed;
  }

  public int getDurCuaMin() {
    return iDurCuaMin;
  }

  public int getDurCuaMax() {
    return iDurCuaMax;
  }

  public int getDurCuaMed() {
    return iDurCuaMed;
  }

  public String getCodTra() {
    return sCodTra;
  }

  public String getDesObs() {
    return sDesObs;
  }

  public String getEn() {
    return sEn;
  }

} //Fin CLASE