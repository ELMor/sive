package envressem;

import java.util.ResourceBundle;

import capp.CApp;

public class EnvUrg
    extends CApp {

  ResourceBundle res;

  public EnvUrg() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("envressem.Res" + this.getIdioma());
    setTitulo(res.getString("msg3.Text"));
    CApp a = (CApp)this;
    // abrimos el panel con el modo oportuno
    VerPanel("", new PanEnvUrg(a));
  }

}
