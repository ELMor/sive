//Clase para representar un env�o

package envressem;

import java.io.Serializable;

public class NumyFecEnv
    implements Serializable {

  protected int iNumEnv; //N�mero de env�o
  protected String sFecEnv = ""; //Fecha de env�o dada como String

  public NumyFecEnv(int numEnv) {
    iNumEnv = numEnv;
  }

  public NumyFecEnv(int numEnv, String fecEnv) {
    iNumEnv = numEnv;
    sFecEnv = fecEnv;
  }

  public int getNumEnv() {
    return iNumEnv;
  }

  public String getFecEnv() {
    return sFecEnv;

  }

}