package util_ficheros;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Vector;

import capp2.CApp;
import capp2.CMessage;
import sapp2.Data;
import sapp2.Lista;

public class EditorFichero
    implements Serializable {

  File miFichero = null;
  FileWriter fStream = null;
  CMessage msgBox;

  public EditorFichero() {
  }

  //escritura de ficheros por longitud m�xima de sus campos
  /*v->vector de DataCampos(nbCampo,longCampo), a->applet,
     lCampos->lista con rdo del servlet, path->ruta del nuevo fichero*/
  public void escribirFichero(Vector v, CApp a,
                              Lista lCampos, String path) {
    if (lCampos.size() > 0) {
      //Se crea fichero en directorio indicado
      try {
        miFichero = new File(path);
        fStream = new FileWriter(miFichero);
        for (int i = 0; i < lCampos.size(); i++) {
          Data dtList = new Data();
          String sLinea = "";
          dtList = (Data) lCampos.elementAt(i);
          for (int j = 0; j < v.size(); j++) {
            DataCampos dtCamp = (DataCampos) v.elementAt(j);
            String nom = dtCamp.getNomCampo();
            String valCampo = dtList.getString(nom);
            int lon = dtCamp.getLonCampo();
            while (valCampo.length() < lon) {
              valCampo = valCampo + " ";
            }
            sLinea = sLinea + valCampo;
          } //end for vector
          // Le quitamos el espacio en blanco, si lo hay.
          if (sLinea.endsWith(" ")) {
            int longitud = sLinea.length() - 1;
            sLinea = sLinea.substring(0, longitud);
          }
          fStream.write(sLinea);
          fStream.write("\r");
          fStream.write("\n");
        } //end for lista

        //SE liberan recursos
        fStream.close();
        fStream = null;
        miFichero = null;
        msgBox = new CMessage(a, CMessage.msgAVISO, "El fichero ha sido creado");
        msgBox.show();
        msgBox = null;
      }
      catch (IOException ioEx) {
        ioEx.printStackTrace();
        msgBox = new CMessage(a, CMessage.msgERROR,
                              "No se ha podido crear el fichero");
        msgBox.show();
        msgBox = null;
      }
    }
    else { //si no hay datos que escribir en el fichero
      msgBox = new CMessage(a, CMessage.msgAVISO, "No existen datos");
      msgBox.show();
      msgBox = null;
    }
  } //fin de escribir fichero

  //escritura de ficheros por longitud m�xima de sus campos
  /*v->vector de DataCampos(nbCampo,longCampo), a->applet,
     lCampos->lista con rdo del servlet, path->ruta del nuevo fichero*/
  public void escribirFicheroSinMsg(Vector v, CApp a,
                                    Lista lCampos, String path) {
    if (lCampos.size() > 0) {
      //Se crea fichero en directorio indicado
      try {
        miFichero = new File(path);
        fStream = new FileWriter(miFichero);
        for (int i = 0; i < lCampos.size(); i++) {
          Data dtList = new Data();
          String sLinea = "";
          dtList = (Data) lCampos.elementAt(i);
          for (int j = 0; j < v.size(); j++) {
            DataCampos dtCamp = (DataCampos) v.elementAt(j);
            String nom = dtCamp.getNomCampo();
            String valCampo = dtList.getString(nom);
            int lon = dtCamp.getLonCampo();
            while (valCampo.length() < lon) {
              valCampo = valCampo + " ";
            }
            sLinea = sLinea + valCampo;
          } //end for vector
          // Le quitamos el espacio en blanco, si lo hay.
          if (sLinea.endsWith(" ")) {
            int longitud = sLinea.length() - 1;
            sLinea = sLinea.substring(0, longitud);
          }
          fStream.write(sLinea);
          fStream.write("\r");
          fStream.write("\n");
        } //end for lista

        //SE liberan recursos
        fStream.close();
        fStream = null;
        miFichero = null;

      }
      catch (IOException ioEx) {
        ioEx.printStackTrace();
        msgBox = new CMessage(a, CMessage.msgERROR,
                              "No se ha podido crear el fichero");
        msgBox.show();
        msgBox = null;
      }
    }
  } //fin de escribir fichero

  //escritura de ficheros por longitud m�xima de sus campos
  // En este caso se hace la justificaci�n de los campos a
  // la derecha de la columna.
  /*v->vector de DataCampos(nbCampo,longCampo), a->applet,
     lCampos->lista con rdo del servlet, path->ruta del nuevo fichero*/
  public void escribirFicheroSinMsgDer(Vector v, CApp a,
                                       Lista lCampos, String path) {
    if (lCampos.size() > 0) {
      //Se crea fichero en directorio indicado
      try {
        miFichero = new File(path);
        fStream = new FileWriter(miFichero);
        for (int i = 0; i < lCampos.size(); i++) {
          Data dtList = new Data();
          String sLinea = "";
          dtList = (Data) lCampos.elementAt(i);
          for (int j = 0; j < v.size(); j++) {
            DataCampos dtCamp = (DataCampos) v.elementAt(j);
            String nom = dtCamp.getNomCampo();
            String valCampo = dtList.getString(nom);
            int lon = dtCamp.getLonCampo();
            while (valCampo.length() < lon) {
              valCampo = " " + valCampo;
            }
            sLinea = sLinea + valCampo;
          } //end for vector
          // Le quitamos el espacio en blanco, si lo hay.
          if (sLinea.endsWith(" ")) {
            int longitud = sLinea.length() - 1;
            sLinea = sLinea.substring(0, longitud);
          }
          fStream.write(sLinea);
          fStream.write("\r");
          fStream.write("\n");
        } //end for lista

        //SE liberan recursos
        fStream.close();
        fStream = null;
        miFichero = null;

      }
      catch (IOException ioEx) {
        ioEx.printStackTrace();
        msgBox = new CMessage(a, CMessage.msgERROR,
                              "No se ha podido crear el fichero");
        msgBox.show();
        msgBox = null;
      }
    }
  } //fin de escribir fichero

  //escritura de ficheros por car�cter de separaci�n
  /*v->vector de strings con el nb de los campos a mostrar ordenados,
     a->applet, lCampos->lista con rdo del servlet
     path->localizaci�n del fichero, carSep->el car�cter q separa los campos*/
  public void escribirFicheroCarSepar(Vector v, CApp a, Lista lCampos,
                                      String path,
                                      String carSep) {
    if (lCampos.size() > 0) {
      //Se crea fichero en directorio indicado
      try {
        miFichero = new File(path);
        fStream = new FileWriter(miFichero);
        for (int i = 0; i < lCampos.size(); i++) {
          Data dtList = new Data();
          String sLinea = "";
          dtList = (Data) lCampos.elementAt(i);
          for (int j = 0; j < v.size(); j++) {
            if (j != 0) {
              sLinea = sLinea + carSep;
            }
            String nom = (String) v.elementAt(j);
            String valCampo = dtList.getString(nom);
            sLinea = sLinea + valCampo;
          } //end for vector
          // Le quitamos el �ltimo espacio en blanco, si lo hay.
          if (sLinea.endsWith(" ")) {
            int longitud = sLinea.length() - 1;
            sLinea = sLinea.substring(0, longitud);
          }
          fStream.write(sLinea);
          fStream.write("\r");
          fStream.write("\n");
        } //end for lista

        //SE liberan recursos
        fStream.close();
        fStream = null;
        miFichero = null;
        msgBox = new CMessage(a, CMessage.msgAVISO, "El fichero ha sido creado");
        msgBox.show();
        msgBox = null;
      }
      catch (IOException ioEx) {
        ioEx.printStackTrace();
        msgBox = new CMessage(a, CMessage.msgERROR,
                              "No se ha podido crear el fichero");
        msgBox.show();
        msgBox = null;
      }
    }
    else { //si no hay datos que escribir en el fichero
      msgBox = new CMessage(a, CMessage.msgAVISO, "No existen datos");
      msgBox.show();
      msgBox = null;
    }
  } //fin de escribirFicheroCarSepar

  //escritura de ficheros por car�cter de separaci�n sin mensaje
  /*v->vector de strings con el nb de los campos a mostrar ordenados,
     a->applet, lCampos->lista con rdo del servlet
     path->localizaci�n del fichero, carSep->el car�cter q separa los campos*/
  public void escribirFicheroCarSeparSinMsg(Vector v, CApp a, Lista lCampos,
                                            String path,
                                            String carSep) {
    if (lCampos.size() > 0) {
      //Se crea fichero en directorio indicado
      try {
        miFichero = new File(path);
        fStream = new FileWriter(miFichero);
        for (int i = 0; i < lCampos.size(); i++) {
          Data dtList = new Data();
          String sLinea = "";
          dtList = (Data) lCampos.elementAt(i);
          for (int j = 0; j < v.size(); j++) {
            if (j != 0) {
              sLinea = sLinea + carSep;
            }
            String nom = (String) v.elementAt(j);
            String valCampo = dtList.getString(nom);
            sLinea = sLinea + valCampo;
          } //end for vector
          // Le quitamos el �ltimo espacio en blanco, si lo hay.
          if (sLinea.endsWith(" ")) {
            int longitud = sLinea.length() - 1;
            sLinea = sLinea.substring(0, longitud);
          }
          fStream.write(sLinea);
          fStream.write("\r");
          fStream.write("\n");
        } //end for lista

        //SE liberan recursos
        fStream.close();
        fStream = null;
        miFichero = null;
      }
      catch (IOException ioEx) {
        ioEx.printStackTrace();
        msgBox = new CMessage(a, CMessage.msgERROR,
                              "No se ha podido crear el fichero");
        msgBox.show();
        msgBox = null;
      }
    }
  } //fin de escribirFicheroCarSeparSinMsg

}
