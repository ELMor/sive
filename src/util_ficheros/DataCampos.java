/*clase utilizada para la escritura de ficheros separando los
 campos seg�n su tama�o m�ximo*/
package util_ficheros;

import java.io.Serializable;

public class DataCampos
    implements Serializable {

  protected String nomCampo;
  protected int longCampo;

  public DataCampos() {}

  public DataCampos(String nombre, int longitud) {
    nomCampo = nombre;
    longCampo = longitud;
  }

  public void setNomCampo(String nombreCamp) {
    nomCampo = nombreCamp;
  }

  public void setLonCampo(int longitudCamp) {
    longCampo = longitudCamp;
  }

  public String getNomCampo() {
    return nomCampo;
  }

  public int getLonCampo() {
    return longCampo;
  }

}
