
package eqNot;

import java.io.Serializable;

public class DataAutoriza
    implements Serializable {

  //Datos referentes a una autorizacione del usuario
  protected String sCod_Usu = "";
  protected String sCod_Niv1 = "";
  protected String sCod_Secuencial = "";
  protected String sCod_Niv2 = "";
  protected String sCod_CCAA = "";

  //Datos para poder traer las descripciones ( NADA QUE VER CON AUTORIZACIONES)
  //Esto se trae a la vez para aumentar velocidad
  protected String sCodNiv1 = "";
  protected String sCodNiv2 = "";
  protected String sCodZbs = "";

  protected String sDesNiv1 = "";
  protected String sDesNiv2 = "";
  protected String sDesZbs = "";

  public DataAutoriza() {
  }

  public DataAutoriza(String usuario, String niv1, String codsec, String niv2,
                      String ccaa) {
    sCod_Usu = usuario;
    sCod_Niv1 = niv1;
    sCod_Secuencial = codsec;
    sCod_Niv2 = niv2;
    sCod_CCAA = ccaa;
  }

  public String getUsuario() {
    return sCod_Usu;
  }

  public String getNivel1() {
    return sCod_Niv1;
  }

  public String getCodSec() {
    return sCod_Secuencial;
  }

  public String getNivel2() {
    return sCod_Niv2;
  }

  public String getCCAA() {
    return sCod_CCAA;
  }

  //Datos para traer descripciones

  public void setCodNiv1(String codNiv1) {
    sCodNiv1 = codNiv1;
    if (sCodNiv1 == null) {
      sCodNiv1 = "";
    }
  }

  public void setCodNiv2(String codNiv2) {
    sCodNiv2 = codNiv2;
    if (sCodNiv2 == null) {
      sCodNiv2 = "";

    }
  }

  public void setCodZbs(String codZbs) {
    sCodZbs = codZbs;
    if (sCodZbs == null) {
      sCodZbs = "";

    }
  }

  public String getCodNiv1() {
    return sCodNiv1;
  }

  public String getCodNiv2() {
    return sCodNiv2;
  }

  public String getCodZbs() {
    return sCodZbs;
  }

  public void setDesNiv1(String desNiv1) {
    sDesNiv1 = desNiv1;
    if (sDesNiv1 == null) {
      sDesNiv1 = "";

    }
  }

  public void setDesNiv2(String desNiv2) {
    sDesNiv2 = desNiv2;
    if (sDesNiv2 == null) {
      sDesNiv2 = "";

    }
  }

  public void setDesZbs(String desZbs) {
    sDesZbs = desZbs;
    if (sDesZbs == null) {
      sDesZbs = "";

    }
  }

  public String getDesNiv1() {
    return sDesNiv1;
  }

  public String getDesNiv2() {
    return sDesNiv2;
  }

  public String getDesZbs() {
    return sDesZbs;
  }

}