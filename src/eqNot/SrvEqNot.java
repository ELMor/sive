package eqNot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import sapp.DBServlet;
import zbs.DataZBS;

public class SrvEqNot
    extends DBServlet {

  public static final String name = "servlet/SrvEqNot";

  //Modos de operaci�n del Servlet
  public static final int servletALTA = 0;
  public static final int servletMODIFICAR = 1;
  public static final int servletBAJA = 2;

  public static final int servletOBTENER_X_CODIGO = 3;
  public static final int servletOBTENER_X_DESCRIPCION = 4;
  public static final int servletSELECCION_X_CODIGO = 5;
  public static final int servletSELECCION_X_DESCRIPCION = 6;

  public static final int servletOBTENER_ZBS_X_CODIGO = 7;
  public static final int servletOBTENER_ZBS_X_DESCRIPCION = 8;

  public static final int servletOBTENER_PERFIL_3_X_DESCRIPCION = 9;
  public static final int servletOBTENER_PERFIL_4_X_DESCRIPCION = 10;
  public static final int servletSELECCION_PERFIL_3_X_DESCRIPCION = 11;
  public static final int servletSELECCION_PERFIL_4_X_DESCRIPCION = 12;

  public static final int servletOBTENER_PERFIL_3_X_CODIGO = 13;
  public static final int servletOBTENER_PERFIL_4_X_CODIGO = 14;
  public static final int servletSELECCION_PERFIL_3_X_CODIGO = 15;
  public static final int servletSELECCION_PERFIL_4_X_CODIGO = 16;

  public static final int servletSELECCION_TODO_X_CODIGO = 17; //equipos usando filtro incompleto de su centro
  public static final int servletSELECCION_TODO_X_DESCRIPCION = 18; //equipos usando filtro incompleto de su centro
//  public static final int servletOBTENER_TODO_X_CODIGO = 19;
//  public static final int servletOBTENER_TODO_X_DESCRIPCION = 20;

  public static final int servletSELECCION_EQUIPOS_UN_CENTRO_X_CODIGO = 21; //equipos de un centro determinado
  public static final int servletSELECCION_EQUIPOS_UN_CENTRO_X_DESCRIPCION = 22; //equipos de un centro determinado
  public static final int servletOBTENER_EQUIPOS_UN_CENTRO_X_CODIGO = 23; //equipos de un centro determinado
  public static final int servletOBTENER_EQUIPOS_UN_CENTRO_X_DESCRIPCION = 24; //equipos de un centro determinado

  String anoHasta = null;
  String semHasta = null;

  String desCentro = null;
  DataEqNot dEqNot = null;

  //AIC
  protected boolean hayNotificacionesAsociadas(DataEqNot datos) throws
      Exception {
    String anyoBaja, semBaja, eqNotificacion;

    boolean resultadoFinal = true;

    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    con = openConnection();

    anyoBaja = datos.getAnyoBaja();
    semBaja = datos.getSemanaBaja();
    eqNotificacion = datos.getEquipo();

    if (semBaja.length() == 1) {
      semBaja = "0" + semBaja;
    }

    query = "Select count(*) from SIVE_NOTIFEDO " +
        " where CD_E_NOTIF= '" + eqNotificacion + "' and " +
        " ((CD_ANOEPI = '" + anyoBaja + "' and " +
        " CD_SEMEPI >= '" + semBaja + "') or " +
        " (CD_ANOEPI > '" + anyoBaja + "'))";

    //ELIMINAR
    //// System_out.println(query);

    st = con.prepareStatement(query);

    rs = st.executeQuery();

    if (rs.next()) {
      if (rs.getInt(1) != 0) {
        resultadoFinal = true;
      }
      else {
        rs.close();
        st.close();

        query = "Delete from SIVE_NOTIF_SEM " +
            " where CD_E_NOTIF= '" + eqNotificacion + "' and " +
            " ((CD_ANOEPI = '" + anyoBaja + "' and " +
            " CD_SEMEPI >= '" + semBaja + "') or " +
            " (CD_ANOEPI > '" + anyoBaja + "'))";
        //ELIMINAR
        //// System_out.println(query);

        st = con.prepareStatement(query);

        st.executeUpdate();

        resultadoFinal = false;

      }
    }

    rs.close();
    st.close();
    closeConnection(con);

    return resultadoFinal;

  }

  //AIC
  //Devuelve la fecha de la siguiente semana a la �ltima que tiene el equipo
  //registrada en SIVE_NOTIF_SEM. Tiene para equipos dados de baja.
  protected dataFecha ObtenerFechaBaja(String CodEquipo) throws Exception {
    int semana;
    String anyoBaja, semBaja;

    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    con = openConnection();

    query =
        " Select max(cd_semepi) as SEM, cd_anoepi as ANYO from sive_notif_sem " +
        " where cd_e_notif = '" + CodEquipo + "'" +
        " and cd_anoepi in( select max(cd_anoepi) " +
        " from sive_notif_sem where cd_e_notif = '" + CodEquipo + "') " +
        " group by cd_anoepi ";

    //ELIMINAR
    // System_out.println(query);

    st = con.prepareStatement(query);

    rs = st.executeQuery();

    if (rs.next()) {
      semana = rs.getInt("SEM") + 1;
      if (semana > 52) {
        semana = 1;
        anyoBaja = String.valueOf(rs.getInt("ANYO") + 1);
      }
      else {
        anyoBaja = rs.getString("ANYO");
      }

      if (semana < 10) {
        semBaja = "0" + String.valueOf(semana);
      }
      else {
        semBaja = String.valueOf(semana);
      }

    }
    else {
      semBaja = "";
      anyoBaja = "";
    }

    rs.close();
    st.close();

    closeConnection(con);

    return new dataFecha(anyoBaja, semBaja);

  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    CLista data = new CLista();
    DataEqNot datos = null;
    DataZBS datos2 = null;
    DataEqNot datos3 = null;
    DataNotifSem datosSemana = null;

    //AIC
    dataFecha fecBaja;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

//    // System_out.println("^^^^^^^^^^Las 17:30");

    try {

      // modos de operaci�n
      switch (opmode) {

        // alta
        case servletALTA:

          datos = (DataEqNot) param.firstElement();

          // primero insertamos el registro en la tabla de equipos
          // prepara la query
          query =
              "insert into sive_e_notif(cd_e_notif, cd_centro, cd_nivel_1, cd_nivel_2," +
              "cd_semepi, cd_anoepi, cd_zbs, ds_respequip, ds_telef, ds_fax, nm_notift, fc_alta, cd_ope," +
              "fc_ultact, it_baja, ds_e_notif) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
          st = con.prepareStatement(query);
          // c�digo equipo notificador
          //# // System_out.println(datos.getEquipo().trim());
          st.setString(1, datos.getEquipo().trim());
          // c�digo centro notificador
          //# // System_out.println(datos.getCentro().trim());
          st.setString(2, datos.getCentro().trim());
          // c�digo nivel 1
          //# // System_out.println(datos.getNiv1().trim());
          st.setString(3, datos.getNiv1().trim());
          // c�digo nivel 2
          //# // System_out.println(datos.getNiv2().trim());
          st.setString(4, datos.getNiv2().trim());
          // c�digo sem. epid.
          //# // System_out.println("EQNOT:SEMANA"+datos.getSemana().trim());
          st.setString(5, datos.getSemana().trim());
          // c�digo a�o epid.
          st.setString(6, datos.getAnyo().trim());
          //# // System_out.println("EQNOT:A�O"+datos.getAnyo().trim());
          // c�digo zona
          st.setString(7, datos.getZBS().trim());
          // responsable
          if (datos.getResponsable().trim().length() > 0) {
            st.setString(8, datos.getResponsable().trim());
          }
          else {
            st.setNull(8, java.sql.Types.VARCHAR);
          }

          // tel�fono
          if (datos.getTelefono().trim().length() > 0) {
            st.setString(9, datos.getTelefono().trim());
          }
          else {
            st.setNull(9, java.sql.Types.VARCHAR);
          }

          // fax
          if (datos.getFax().trim().length() > 0) {
            st.setString(10, datos.getFax().trim());
          }
          else {
            st.setNull(10, java.sql.Types.VARCHAR);
          }

          // n�mero de notificadores
          st.setInt(11, datos.getNotift());
          // fecha alta
          st.setDate(12, new java.sql.Date( (new java.util.Date()).getTime()));
          // operador
          st.setString(13, datos.getCodOpe().trim());
          // fecha �ltima actualizaci�n
          st.setDate(14, new java.sql.Date( (new java.util.Date()).getTime()));
          // �es baja?
          st.setString(15, datos.getBaja().trim());
          // Descripci�n del equipo notificador
          st.setString(16, datos.getDesEquipo().trim());
          // lanza la query
          st.executeUpdate();
          st.close();

//         // System_out.println("Hecha alta equipo " + datos.getEquipo().trim()+"**********");

          // Ahora hay que comprobar si hay cobertura
          if (datos.estaEnCobertura()) {
            // Hay cobertura
            genCobertura(con, st, rs, datos, param);
          } //if cobertura

          break;

          // baja
        case servletBAJA:
          datos = (DataEqNot) param.firstElement();
          // NOTA: Primero debemos comprobar si el equipo declara o no
          // si no declara
          // prepara la query
          query = "DELETE FROM SIVE_E_NOTIF WHERE CD_E_NOTIF = ?";

          // lanza la query
          st = con.prepareStatement(query);
          st.setString(1, datos.getEquipo().trim());
          st.executeUpdate();
          st.close();

          break;

          // b�squeda
        case servletSELECCION_X_CODIGO:
        case servletSELECCION_X_DESCRIPCION:

          datos = (DataEqNot) param.firstElement();
          // prepara la query
          // ARG: (10/5/02)
          if (param.getFilter().length() > 0) {
            if (opmode == servletSELECCION_X_CODIGO) {
              query = "select * from sive_e_notif where CD_E_NOTIF like ? and nlssort(CD_E_NOTIF) > nlssort(?) order by CD_E_NOTIF";
            }
            else {

              // query = "select * from sive_e_notif where upper(DS_E_NOTIF) like upper(?) and nlssort(DS_E_NOTIF) > nlssort(?) order by DS_E_NOTIF";
              query = "select * from sive_e_notif where upper(DS_E_NOTIF) like upper(?) and nlssort(CD_E_NOTIF) > nlssort(?) order by CD_E_NOTIF";
            }
          }
          else {
            if (opmode == servletSELECCION_X_CODIGO) {
              query =
                  "select * from sive_e_notif where CD_E_NOTIF like ? order by CD_E_NOTIF";
            }
            else {

              // query = "select * from sive_e_notif where upper(DS_E_NOTIF) like upper(?) order by DS_E_NOTIF";
              query = "select * from sive_e_notif where upper(DS_E_NOTIF) like upper(?) order by CD_E_NOTIF";
            }
          }

          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);
          // filtro
          if (opmode == servletSELECCION_X_CODIGO) {
            st.setString(1, datos.getEquipo().trim() + "%");
          }
          else {
            st.setString(1, "%" + datos.getEquipo().trim() + "%");
            // paginaci�n
          }
          if (param.getFilter().length() > 0) {
            st.setString(2, param.getFilter().trim());
          }
          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (opmode == servletSELECCION_X_CODIGO) {
                data.setFilter( ( (DataEqNot) data.lastElement()).getEquipo());
              }
              else {

                //data.setFilter( ((DataEqNot)data.lastElement()).getDesEquipo() );
                data.setFilter( ( (DataEqNot) data.lastElement()).getEquipo());

              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // a�ade un nodo

            //AIC buscamos la fecha de baja si existe.
            fecBaja = new dataFecha("", "");
            if (rs.getString("IT_BAJA").equals("S")) {
              fecBaja = ObtenerFechaBaja(rs.getString("CD_E_NOTIF"));
            }
            data.addElement(new DataEqNot(rs.getString("CD_E_NOTIF"),
                                          rs.getString("DS_E_NOTIF"),
                                          rs.getString("CD_CENTRO"),
                                          rs.getString("CD_NIVEL_1"),
                                          rs.getString("CD_NIVEL_2"),
                                          rs.getString("CD_SEMEPI"),
                                          rs.getString("CD_ANOEPI"),
                                          rs.getString("CD_ZBS"),
                                          rs.getString("DS_RESPEQUIP"),
                                          rs.getString("DS_TELEF"),
                                          rs.getString("DS_FAX"),
                                          rs.getInt("NM_NOTIFT"),
                                          rs.getString("CD_OPE"),
                                          rs.getString("IT_BAJA"), false,
                                          fecBaja.getAnyo(), fecBaja.getSemana()));
            i++;
          }

          rs.close();
          st.close();

          //DSR:Copy-Paste c�digo de descricipones de centros
          //nos traemos todas las descripciones de centros
          query =
              "select DS_CENTRO,IT_COBERTURA from sive_c_notif where CD_CENTRO = ?";
          // lanza la query
          //# // System_out.println("LAnzamos " + query);
          st = con.prepareStatement(query);
          desCentro = null;
          dEqNot = null;
          for (int k = 0; k < data.size(); k++) {
            dEqNot = (DataEqNot) data.elementAt(k);
            st.setString(1, dEqNot.getCentro().trim());
            rs = st.executeQuery();
            if (rs.next()) {
              desCentro = rs.getString(1);
              if (desCentro != null) {
                dEqNot.setDesCentro(desCentro);
              }
              if (rs.getString(2).equals("S")) {
                dEqNot.setCobertura(true);
              }
            }
            rs.close();
          }
          st.close();
          //DSR:Copy-Paste c�digo de descricipones de centros

          break;

        case servletSELECCION_TODO_X_CODIGO:
        case servletSELECCION_TODO_X_DESCRIPCION:

          datos = (DataEqNot) param.firstElement();
          // prepara la query
          // ARG: (10/5/02)
          if (param.getFilter().length() > 0) {
            if (opmode == servletSELECCION_TODO_X_CODIGO) {
              query = "select * from sive_e_notif where CD_E_NOTIF like ? and "
                  +
                  "CD_CENTRO like ? and nlssort(CD_E_NOTIF) > nlssort(?) order by CD_E_NOTIF";
            }
            else {
              query =
                  "select * from sive_e_notif where upper(DS_E_NOTIF) like upper(?) and "
                  // +"CD_CENTRO like ? and nlssort(DS_E_NOTIF) > nlssort(?) order by DS_E_NOTIF";
                  +
                  "CD_CENTRO like ? and nlssort(CD_E_NOTIF) > nlssort(?) order by CD_E_NOTIF";
            }
          }
          else {
            if (opmode == servletSELECCION_TODO_X_CODIGO) {
              query = "select * from sive_e_notif where CD_E_NOTIF like ? and "
                  + "CD_CENTRO like ? order by CD_E_NOTIF";
            }
            else {
              query =
                  "select * from sive_e_notif where upper(DS_E_NOTIF) like upper(?) and "
                  // +"CD_CENTRO like ? order by DS_E_NOTIF";
                  + "CD_CENTRO like ? order by CD_E_NOTIF";
            }
          }

          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);
          // filtro
          if (opmode == servletSELECCION_TODO_X_CODIGO) {
            st.setString(1, datos.getEquipo().trim() + "%");
          }
          else {
            st.setString(1, "%" + datos.getEquipo().trim() + "%");
            //# // System_out.println("query " + query);
            //# // System_out.println("Para: " + datos.getEquipo().trim());
            // centro
          }
          st.setString(2, datos.getCentro().trim() + "%");
          // paginaci�n
          if (param.getFilter().length() > 0) {
            st.setString(3, param.getFilter().trim());
            //# // System_out.println("Para: " + param.getFilter().trim());
          }
          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if ( (opmode == servletSELECCION_X_CODIGO) ||
                  opmode == servletSELECCION_TODO_X_CODIGO) {
                data.setFilter( ( (DataEqNot) data.lastElement()).getEquipo());
              }
              else {

                //data.setFilter( ((DataEqNot)data.lastElement()).getDesEquipo() );
                data.setFilter( ( (DataEqNot) data.lastElement()).getEquipo());

              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // a�ade un nodo

            //AIC buscamos la fecha de baja si existe.
            fecBaja = new dataFecha("", "");
            if (rs.getString("IT_BAJA").equals("S")) {
              fecBaja = ObtenerFechaBaja(rs.getString("CD_E_NOTIF"));
            }

            data.addElement(new DataEqNot(rs.getString("CD_E_NOTIF"),
                                          rs.getString("DS_E_NOTIF"),
                                          rs.getString("CD_CENTRO"),
                                          rs.getString("CD_NIVEL_1"),
                                          rs.getString("CD_NIVEL_2"),
                                          rs.getString("CD_SEMEPI"),
                                          rs.getString("CD_ANOEPI"),
                                          rs.getString("CD_ZBS"),
                                          rs.getString("DS_RESPEQUIP"),
                                          rs.getString("DS_TELEF"),
                                          rs.getString("DS_FAX"),
                                          rs.getInt("NM_NOTIFT"),
                                          rs.getString("CD_OPE"),
                                          rs.getString("IT_BAJA"), false,
                                          fecBaja.getAnyo(), fecBaja.getSemana()));
            i++;
          }

          rs.close();
          st.close();

          //nos traemos todas las descripciones de centros
          query =
              "select DS_CENTRO,IT_COBERTURA from sive_c_notif where CD_CENTRO = ?";
          // lanza la query
          //# // System_out.println("LAnzamos " + query);
          st = con.prepareStatement(query);
          desCentro = null;
          dEqNot = null;
          for (int k = 0; k < data.size(); k++) {
            dEqNot = (DataEqNot) data.elementAt(k);
            st.setString(1, dEqNot.getCentro().trim());
            rs = st.executeQuery();
            if (rs.next()) {
              desCentro = rs.getString(1);
              if (desCentro != null) {
                dEqNot.setDesCentro(desCentro);
              }
              if (rs.getString(2).equals("S")) {
                dEqNot.setCobertura(true);
              }
            }
            rs.close();
          }
          st.close();

          break;
          /*
                case servletOBTENER_TODO_X_CODIGO:
                case servletOBTENER_TODO_X_DESCRIPCION:
                  datos = (DataEqNot) param.firstElement();
                  // prepara la query
                  if (param.getFilter().length() > 0){
                    if (opmode == servletSELECCION_TODO_X_CODIGO)
               query = "select * from sive_e_notif where CD_E_NOTIF = ? and "
               +"CD_CENTRO like ? and CD_E_NOTIF > ? order by CD_E_NOTIF";
                    else
               query = "select * from sive_e_notif where DS_E_NOTIF = ? and "
               +"CD_CENTRO like ? and DS_E_NOTIF > ? order by DS_E_NOTIF";
                  }else{
                    if (opmode == servletSELECCION_TODO_X_CODIGO)
               query = "select * from sive_e_notif where CD_E_NOTIF = ? and "
                        +"CD_CENTRO like ? order by CD_E_NOTIF";
                    else
               query = "select * from sive_e_notif where DS_E_NOTIF = ? and "
                        +"CD_CENTRO like ? order by DS_E_NOTIF";
                  }
                  // prepara la lista de resultados
                  data = new CLista();
                  // lanza la query
                  st = con.prepareStatement(query);
                  // filtro
                  st.setString(1, datos.getEquipo().trim());
                  //# // System_out.println("query " + query);
                  //# // System_out.println("Para: " + datos.getEquipo().trim());
                  // centro
                  st.setString(2, datos.getCentro().trim()+"%");
                  // paginaci�n
                  if (param.getFilter().length() > 0) {
                    st.setString(3, param.getFilter().trim());
                    //# // System_out.println("Para: " + param.getFilter().trim());
                  }
                  rs = st.executeQuery();
                  // extrae la p�gina requerida
                  while (rs.next()) {
                    // control de tama�o
                    if (i > DBServlet.maxSIZE) {
                      data.setState(CLista.listaINCOMPLETA);
                      if (opmode == servletSELECCION_X_CODIGO)
               data.setFilter( ((DataEqNot)data.lastElement()).getEquipo() );
                      else
               data.setFilter( ((DataEqNot)data.lastElement()).getDesEquipo() );
                      break;
                    }
                    // control de estado
                    if (data.getState() == CLista.listaVACIA) {
                      data.setState(CLista.listaLLENA);
                    }
                    // a�ade un nodo
                    data.addElement(new DataEqNot(rs.getString("CD_E_NOTIF"),rs.getString("DS_E_NOTIF"),rs.getString("CD_CENTRO"),
               rs.getString("CD_NIVEL_1"),rs.getString("CD_NIVEL_2"),rs.getString("CD_SEMEPI"),
                    rs.getString("CD_ANOEPI"),rs.getString("CD_ZBS"),rs.getString("DS_RESPEQUIP"),rs.getString("DS_TELEF"),
                    rs.getString("DS_FAX"),rs.getInt("NM_NOTIFT"),rs.getString("CD_OPE"),rs.getString("IT_BAJA"),false));
                    i++;
                  }
                  rs.close();
                  st.close();
                  //nos traemos todas las descripciones de centros
               query = "select DS_CENTRO from sive_c_notif where CD_CENTRO = ?";
                  // lanza la query
                  //# // System_out.println("LAnzamos " + query);
                  st = con.prepareStatement(query);
                  desCentro=null;
                  dEqNot = null;
                  for(int k=0; k< data.size(); k++){
                      dEqNot = (DataEqNot) data.elementAt(k);
                      st.setString(1, dEqNot.getCentro().trim());
                      rs = st.executeQuery();
                      if (rs.next()) {
                          desCentro = rs.getString(1);
                          if (desCentro != null){
                              dEqNot.setDesCentro(desCentro);
                          }
                          if (rs.getString(2).equals("S"))
                            dEqNot.setCobertura(true);
                      }
                      rs.close();
                  }
                  st.close();
                  break;
           */

          // obtener
        case servletOBTENER_X_CODIGO:
        case servletOBTENER_X_DESCRIPCION:

          datos = (DataEqNot) param.firstElement();
          // prepara la query

          if (opmode == servletOBTENER_X_CODIGO) {
            query =
                "select * from sive_e_notif where CD_E_NOTIF = ? order by CD_E_NOTIF";
          }
          else {
            query =
                "select * from sive_e_notif where DS_E_NOTIF = ? order by CD_E_NOTIF";

            // prepara la lista de resultados
          }
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);
          // filtro
          st.setString(1, datos.getEquipo().trim());

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              data.setFilter( ( (DataEqNot) data.lastElement()).getEquipo());
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // a�ade un nodo

            //AIC buscamos la fecha de baja si existe.
            fecBaja = new dataFecha("", "");
            if (rs.getString("IT_BAJA").equals("S")) {
              fecBaja = ObtenerFechaBaja(rs.getString("CD_E_NOTIF"));
            }

            data.addElement(new DataEqNot(rs.getString("CD_E_NOTIF"),
                                          rs.getString("DS_E_NOTIF"),
                                          rs.getString("CD_CENTRO"),
                                          rs.getString("CD_NIVEL_1"),
                                          rs.getString("CD_NIVEL_2"),
                                          rs.getString("CD_SEMEPI"),
                                          rs.getString("CD_ANOEPI"),
                                          rs.getString("CD_ZBS"),
                                          rs.getString("DS_RESPEQUIP"),
                                          rs.getString("DS_TELEF"),
                                          rs.getString("DS_FAX"),
                                          rs.getInt("NM_NOTIFT"),
                                          rs.getString("CD_OPE"),
                                          rs.getString("IT_BAJA"), false,
                                          fecBaja.getAnyo(), fecBaja.getSemana()));
            i++;
          }

          rs.close();
          st.close();

          break;

          // modificaci�n
        case servletMODIFICAR:
          boolean borradoFisico = false;
          datos = (DataEqNot) param.firstElement();

          //Si hay marca de baja borrado de los registros de cobertura
          if (datos.getBaja().trim().compareTo("S") == 0) {
            if (hayNotificacionesAsociadas(datos)) {
              datos.setBaja(false);
              data.addElement(datos);
              //ELIMINAR
              //// System_out.println("SrvEqNot : Hay datos en data");
            }
            else {
              //hay que borrar el registro
              anoHasta = "";
              semHasta = "";

              // primero se buscan todas las semanas del rango
                  /*query = "select * from sive_semgen order by cd_anoepig desc, cd_semepig desc";
                           // lanza la query
                           st = con.prepareStatement(query);
                           rs = st.executeQuery();
                           if (rs.next()){
                anoHasta = rs.getString("cd_anoepig");
                semHasta = rs.getString("cd_semepig");
                rs.close();
                st.close();
                rs = null;
                st = null;
                           }
                           else{
                rs.close();
                st.close();
                rs = null;
                st = null;
                   query = "select * from sive_semana_epi order by cd_anoepi desc, cd_semepi desc";
                // lanza la query
                st = con.prepareStatement(query);
                rs = st.executeQuery();
                anoHasta = rs.getString("cd_anoepi");
                semHasta = rs.getString("cd_semepi");
                rs.close();
                st.close();
                rs = null;
                st = null;
                           }
                           //# // System_out.println("A�o hasta:"+anoHasta);
                           //# // System_out.println("Semana hasta:"+semHasta);
                           //# // System_out.println("A�o desde:"+datos.getAnyo());
                           //# // System_out.println("Semana desde:"+datos.getSemana());
                           //Borrado de registros en sive_notif_sem si rango semanas est� en mismo a�o
                           if(datos.getAnyo().compareTo(anoHasta) == 0){
                query = "DELETE from sive_notif_sem where "+
                   "cd_anoepi=? and cd_semepi>=? and cd_semepi<=? and cd_e_notif=? and "+
                   " not exists(select cd_e_notif, cd_anoepi, cd_semepi from sive_notifedo " +
                " where cd_e_notif=sive_notif_sem.cd_e_notif " +
                " and cd_anoepi=sive_notif_sem.cd_anoepi " +
                " and cd_semepi=sive_notif_sem.cd_semepi)";        //LRG
                // lanza la query
                st = con.prepareStatement(query);
                // a�o
                st.setString(1, datos.getAnyo().trim());
                // semana desde
                st.setString(2, datos.getSemana().trim());
                // semana hasta
                st.setString(3, semHasta.trim());
                // equipo
                st.setString(4, datos.getEquipo().trim());
                           }
                           //Borrado de registros en sive_notif_sem si rango semanas est� en varios a�os
                           else{
                query = "DELETE from sive_notif_sem where "+
                   "(( cd_anoepi = ? and cd_semepi >= ? ) or ( cd_anoepi > ? and cd_anoepi < ? )"+
                   " or ( cd_anoepi = ? and cd_semepi <= ? ))and cd_e_notif=? and "+
                   " not exists(select cd_e_notif, cd_anoepi, cd_semepi from sive_notifedo " +
                " where cd_e_notif=sive_notif_sem.cd_e_notif " +
                " and cd_anoepi=sive_notif_sem.cd_anoepi " +
                " and cd_semepi=sive_notif_sem.cd_semepi)";        //LRG
                // lanza la query
                st = con.prepareStatement(query);
                // a�o desde
                st.setString(1, datos.getAnyo().trim());
                st.setString(3, datos.getAnyo().trim());
                // semana desde
                st.setString(2, datos.getSemana().trim());
                // semana hasta
                st.setString(6, semHasta.trim());
                // a�o hasta
                st.setString(4, anoHasta.trim());
                st.setString(5, anoHasta.trim());
                // equipo
                st.setString(7, datos.getEquipo().trim());
                           }
                           st.executeUpdate();
                           st.close();
                           st = null;
                           //# // System_out.println("Es la salida:"+query); */

              // ahora comprobamos si el borrado es f�sico en sive_e_notif o l�gico
              //S�lo ser� f�sico si ahora no existen registros notif_sem asociados a ese equipo
              rs = null;
              //# // System_out.println("a");
              query = "select count(*) from sive_notif_sem where CD_E_NOTIF=?";
              // lanza la query
              //# // System_out.println("b");
              st = con.prepareStatement(query);
              //# // System_out.println("c");
              st.setString(1, datos.getEquipo());
              //# // System_out.println("d");
              rs = st.executeQuery();
              //# // System_out.println("e");
              anoHasta = "logico";

              if (rs.next()) {
                if (rs.getInt(1) == 0) { //borrado f�sico
                  //# // System_out.println("f");
                  anoHasta = "fisico";
                  //# // System_out.println("g");
                }
                else {
                  //# // System_out.println("h");
                  anoHasta = "logico";
                  //# // System_out.println("i");
                }
              }
              //# // System_out.println("1");
              rs.close();
              //# // System_out.println("2");
              st.close();
              //# // System_out.println("3");
              rs = null;
              //# // System_out.println("4");
              st = null;
              //# // System_out.println("5");

              //Si borrado es f�sico ejecuta sent sql delete.
              // Si es l�gico no hace nada (marca de baja ya est� puesta)
              if (anoHasta.compareTo("fisico") == 0) {
                borradoFisico = true;
                //# // System_out.println("6");
                query = "delete from sive_e_notif where cd_e_notif=?";
                //# // System_out.println("7");
                // lanza la query
                st = con.prepareStatement(query);
                //# // System_out.println("8");
                st.setString(1, datos.getEquipo());
                //# // System_out.println("9");
                st.executeUpdate();
                //# // System_out.println("10");
                st.close();
                //# // System_out.println("11");
                st = null;
                //# // System_out.println("12");
                data.setFilter("fisico");
              }
            } //else de hayNotificacionesAsociadas()

            if (!borradoFisico) {
              query = "UPDATE sive_e_notif SET cd_nivel_1=?, cd_nivel_2=?," +
                  "cd_semepi=?, cd_anoepi=?, cd_zbs=?, ds_respequip=?, ds_telef=?, ds_fax=?," +
                  "nm_notift=?, cd_ope=?, fc_ultact=?, it_baja=?, ds_e_notif=? WHERE CD_CENTRO=? AND  CD_E_NOTIF=? ";
              // lanza la query
              st = con.prepareStatement(query);
              // nivel 1
              st.setString(1, datos.getNiv1().trim());
              // nivel 2
              st.setString(2, datos.getNiv2().trim());
              // semana
              st.setString(3, datos.getSemana().trim());
              // a�o
              st.setString(4, datos.getAnyo().trim());
              // zona b�sica
              st.setString(5, datos.getZBS().trim());
              // responsable
              st.setString(6, datos.getResponsable().trim());
              // tel�fono
              st.setString(7, datos.getTelefono().trim());
              // fax
              st.setString(8, datos.getFax().trim());
              // cantidad de notificadores
              st.setInt(9, datos.getNotift());
              // c�digo de operador
              st.setString(10, datos.getCodOpe().trim());
              // fecha �ltima actualizaci�n
              st.setDate(11, new java.sql.Date( (new java.util.Date()).getTime()));
              // �es baja?
              st.setString(12, datos.getBaja().trim());
              // c�digo centro notificador
              st.setString(13, datos.getDesEquipo().trim());
              // c�digo centro notificador
              st.setString(14, datos.getCentro().trim());
              // c�digo equipo notificador
              st.setString(15, datos.getEquipo().trim());
              st.executeUpdate();
              //# // System_out.println("Realiozada1 " + query);
              st.close();
              st = null;
            }
          }

          //Si no hay que dar de baja el equipo determina si debe ampliar los registros de la cobertura
          else {
            query = "UPDATE sive_e_notif SET cd_nivel_1=?, cd_nivel_2=?," +
                "cd_semepi=?, cd_anoepi=?, cd_zbs=?, ds_respequip=?, ds_telef=?, ds_fax=?," +
                "nm_notift=?, cd_ope=?, fc_ultact=?, it_baja=?, ds_e_notif=? WHERE CD_CENTRO=? AND  CD_E_NOTIF=? ";
            // lanza la query
            st = con.prepareStatement(query);
            // nivel 1
            st.setString(1, datos.getNiv1().trim());
            // nivel 2
            st.setString(2, datos.getNiv2().trim());
            // semana
            st.setString(3, datos.getSemana().trim());
            // a�o
            st.setString(4, datos.getAnyo().trim());
            // zona b�sica
            st.setString(5, datos.getZBS().trim());
            // responsable
            st.setString(6, datos.getResponsable().trim());
            // tel�fono
            st.setString(7, datos.getTelefono().trim());
            // fax
            st.setString(8, datos.getFax().trim());
            // cantidad de notificadores
            st.setInt(9, datos.getNotift());
            // c�digo de operador
            st.setString(10, datos.getCodOpe().trim());
            // fecha �ltima actualizaci�n
            st.setDate(11, new java.sql.Date( (new java.util.Date()).getTime()));
            // �es baja?
            st.setString(12, "N");
            // c�digo centro notificador
            st.setString(13, datos.getDesEquipo().trim());
            // c�digo centro notificador
            st.setString(14, datos.getCentro().trim());
            // c�digo equipo notificador
            st.setString(15, datos.getEquipo().trim());
            st.executeUpdate();
            //# // System_out.println("Realiozada1 " + query);
            st.close();
            st = null;

            // Ahora hay que comprobar si hay cobertura
            //# // System_out.println(datos.estaEnCobertura());
            if (datos.estaEnCobertura()) {
              // Hay cobertura
              genCobertura(con, st, rs, datos, param);
            } //if cobertura
          }
          break;

        case servletOBTENER_ZBS_X_CODIGO:
        case servletOBTENER_ZBS_X_DESCRIPCION:

          datos2 = (DataZBS) param.firstElement();
          // prepara la query
          if (opmode == servletOBTENER_X_CODIGO) {
            query = "select * from sive_zona_basica where CD_NIVEL_1 = ? AND CD_NIVEL_2 = ? AND CD_ZBS = ? ";
          }
          else {
            query = "select * from sive_zona_basica where CD_NIVEL_1 = ? AND CD_NIVEL_2 = ? AND DS_ZBS = ? ";
            // lanza la query
          }
          st = con.prepareStatement(query);
          // nivel 1
          st.setString(1, datos2.getCod1().trim());
          // nivel 2
          st.setString(2, datos2.getCod2().trim());
          // zona b�sica
          st.setString(3, datos2.getCod().trim());
          rs = st.executeQuery();

          // extrae el registro encontrado
          while (rs.next()) {
            // a�ade un nodo

            data.addElement(new DataZBS(rs.getString("CD_ZBS"),
                                        rs.getString("DS_ZBS"),
                                        rs.getString("DSL_ZBS"),
                                        rs.getString("CD_NIVEL_1"),
                                        rs.getString("CD_NIVEL_2")));
          }
          rs.close();
          st.close();

          break;

//________________________________________________________________

        case servletSELECCION_EQUIPOS_UN_CENTRO_X_CODIGO:
        case servletSELECCION_EQUIPOS_UN_CENTRO_X_DESCRIPCION:
          datos = (DataEqNot) param.firstElement();
          // prepara la query
          // ARG: (10/5/02)
          if (param.getFilter().length() > 0) {
            if (opmode == servletSELECCION_EQUIPOS_UN_CENTRO_X_CODIGO) {
              query = "select * from sive_e_notif where CD_E_NOTIF like ? and "
                  + "CD_CENTRO = ? and CD_E_NOTIF > ? order by CD_E_NOTIF";
            }
            else {
              query =
                  "select * from sive_e_notif where upper(DS_E_NOTIF) like upper(?) and "
                  // +"CD_CENTRO = ? and upper(DS_E_NOTIF) > upper(?) order by DS_E_NOTIF";
                  +
                  "CD_CENTRO = ? and upper(CD_E_NOTIF) > upper(?) order by CD_E_NOTIF";
            }
          }
          else {
            if (opmode == servletSELECCION_EQUIPOS_UN_CENTRO_X_CODIGO) {
              query = "select * from sive_e_notif where CD_E_NOTIF like ? and "
                  + "CD_CENTRO = ? order by CD_E_NOTIF";
            }
            else {
              query =
                  "select * from sive_e_notif where upper(DS_E_NOTIF) like upper(?) and "
                  // +"CD_CENTRO = ? order by DS_E_NOTIF";
                  + "CD_CENTRO = ? order by CD_E_NOTIF";
            }
          }

          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);
          // filtro
          if (opmode == servletSELECCION_EQUIPOS_UN_CENTRO_X_CODIGO) {
            st.setString(1, datos.getEquipo().trim() + "%");
          }
          else {
            st.setString(1, "%" + datos.getEquipo().trim() + "%");
            //# // System_out.println("query " + query);
            //# // System_out.println("Para: " + datos.getEquipo().trim());
            // centro
          }
          st.setString(2, datos.getCentro().trim());
          // paginaci�n
          if (param.getFilter().length() > 0) {
            st.setString(3, param.getFilter().trim());
            //# // System_out.println("Para: " + param.getFilter().trim());
          }
          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (opmode == servletSELECCION_X_CODIGO) {
                data.setFilter( ( (DataEqNot) data.lastElement()).getEquipo());
              }
              else {

                //data.setFilter( ((DataEqNot)data.lastElement()).getDesEquipo() );
                data.setFilter( ( (DataEqNot) data.lastElement()).getEquipo());

              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // a�ade un nodo
            //AIC buscamos la fecha de baja si existe.
            fecBaja = new dataFecha("", "");
            if (rs.getString("IT_BAJA").equals("S")) {
              fecBaja = ObtenerFechaBaja(rs.getString("CD_E_NOTIF"));
            }

            data.addElement(new DataEqNot(rs.getString("CD_E_NOTIF"),
                                          rs.getString("DS_E_NOTIF"),
                                          rs.getString("CD_CENTRO"),
                                          rs.getString("CD_NIVEL_1"),
                                          rs.getString("CD_NIVEL_2"),
                                          rs.getString("CD_SEMEPI"),
                                          rs.getString("CD_ANOEPI"),
                                          rs.getString("CD_ZBS"),
                                          rs.getString("DS_RESPEQUIP"),
                                          rs.getString("DS_TELEF"),
                                          rs.getString("DS_FAX"),
                                          rs.getInt("NM_NOTIFT"),
                                          rs.getString("CD_OPE"),
                                          rs.getString("IT_BAJA"), false,
                                          fecBaja.getAnyo(), fecBaja.getSemana()));
            i++;
          }

          rs.close();
          st.close();

          //DSR:Copy-Paste c�digo de descricipones de centros
          //nos traemos todas las descripciones de centros
          query =
              "select DS_CENTRO,IT_COBERTURA from sive_c_notif where CD_CENTRO = ?";
          // lanza la query
          //# // System_out.println("LAnzamos " + query);
          st = con.prepareStatement(query);
          desCentro = null;
          dEqNot = null;
          for (int k = 0; k < data.size(); k++) {
            dEqNot = (DataEqNot) data.elementAt(k);
            st.setString(1, dEqNot.getCentro().trim());
            rs = st.executeQuery();
            if (rs.next()) {
              desCentro = rs.getString(1);
              if (desCentro != null) {
                dEqNot.setDesCentro(desCentro);
              }
              if (rs.getString(2).equals("S")) {
                dEqNot.setCobertura(true);
              }
            }
            rs.close();
          }
          st.close();
          //DSR:Copy-Paste c�digo de descricipones de centros

          break;

//________________________________________________________________

        case servletOBTENER_EQUIPOS_UN_CENTRO_X_CODIGO:
        case servletOBTENER_EQUIPOS_UN_CENTRO_X_DESCRIPCION:

          datos = (DataEqNot) param.firstElement();

          if (opmode == servletOBTENER_EQUIPOS_UN_CENTRO_X_CODIGO) {
            query = "select * from sive_e_notif where CD_E_NOTIF = ? and "
                + "CD_CENTRO = ?  order by CD_E_NOTIF";
          }
          else {
            query = "select * from sive_e_notif where DS_E_NOTIF = ? and "
                //+"CD_CENTRO = ?  order by DS_E_NOTIF";
                + "CD_CENTRO = ?  order by CD_E_NOTIF";

            // prepara la lista de resultados
          }
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);
          // filtro
          st.setString(1, datos.getEquipo().trim());
          //# // System_out.println("query " + query);
//         // System_out.println("Equipo: " + datos.getEquipo().trim());
          // centro
          st.setString(2, datos.getCentro().trim());

          // paginaci�n
          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (opmode == servletOBTENER_EQUIPOS_UN_CENTRO_X_CODIGO) {
                data.setFilter( ( (DataEqNot) data.lastElement()).getEquipo());
              }
              else {

                //data.setFilter( ((DataEqNot)data.lastElement()).getDesEquipo() );
                data.setFilter( ( (DataEqNot) data.lastElement()).getEquipo());

              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            // a�ade un nodo

            //AIC buscamos la fecha de baja si existe.
            fecBaja = new dataFecha("", "");
            if (rs.getString("IT_BAJA").equals("S")) {
              fecBaja = ObtenerFechaBaja(rs.getString("CD_E_NOTIF"));
            }

            data.addElement(new DataEqNot(rs.getString("CD_E_NOTIF"),
                                          rs.getString("DS_E_NOTIF"),
                                          rs.getString("CD_CENTRO"),
                                          rs.getString("CD_NIVEL_1"),
                                          rs.getString("CD_NIVEL_2"),
                                          rs.getString("CD_SEMEPI"),
                                          rs.getString("CD_ANOEPI"),
                                          rs.getString("CD_ZBS"),
                                          rs.getString("DS_RESPEQUIP"),
                                          rs.getString("DS_TELEF"),
                                          rs.getString("DS_FAX"),
                                          rs.getInt("NM_NOTIFT"),
                                          rs.getString("CD_OPE"),
                                          rs.getString("IT_BAJA"), false,
                                          fecBaja.getAnyo(), fecBaja.getSemana()));

            i++;
          }

          rs.close();
          st.close();

          break;

//___________________________________________________

//______________________________________________________________

      }
      con.commit();
      //# // System_out.println("EqNot: commit");
    }
    catch (Exception er) {
      con.rollback();
      //# // System_out.println("EqNot: rollback");
      throw er;
    }
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    data.trimToSize();
    return data;
  }

  // proceso para generar los registros de cobertura de un EQ
  protected void genCobertura(Connection con, PreparedStatement st,
                              ResultSet rs, DataEqNot datos, CLista param) throws
      java.sql.SQLException {
    String query;
    String anoHasta;
    String semHasta;
    int i;
    CLista data;
    DataNotifSem datosSemana;

    data = new CLista();

    // primero se buscan todas las semanas del rango
    query =
        "select * from sive_semgen order by cd_anoepig desc, cd_semepig desc";
    // lanza la query
    st = con.prepareStatement(query);
    rs = st.executeQuery();
    if (rs.next()) {
      anoHasta = rs.getString("cd_anoepig");
      semHasta = rs.getString("cd_semepig");
      //# // System_out.println("Hay cobertura:"+anoHasta+" "+semHasta);
    }
    else {
      rs.close();
      st.close();
      rs = null;
      st = null;
      query =
          "select * from sive_semana_epi order by cd_anoepi desc, cd_semepi desc";
      // lanza la query
      st = con.prepareStatement(query);
      rs = st.executeQuery();
      rs.next();
      anoHasta = rs.getString("cd_anoepi");
      semHasta = rs.getString("cd_semepi");
    }
    rs.close();
    st.close();
    rs = null;
    st = null;

    if (datos.getAnyo().compareTo(anoHasta) == 0) {
      query = "select * from sive_semana_epi where " +
          "cd_anoepi=? and cd_semepi>=? and cd_semepi<=?";

      // lanza la query
      st = con.prepareStatement(query);
      // a�o inicio
      st.setString(1, datos.getAnyo().trim());
      // semana desde
      st.setString(2, datos.getSemana().trim());
      // semana hasta
      st.setString(3, semHasta);
    }
    else {
      query = "select * from sive_semana_epi " +
          "where (cd_anoepi = ? and cd_semepi >= ?) or " +
          "(cd_anoepi > ? and cd_anoepi < ?) or " +
          "(cd_anoepi = ? and cd_semepi <= ?)";

      // lanza la query
      st = con.prepareStatement(query);

      // a�o inicio
      st.setString(1, datos.getAnyo().trim());
      st.setString(3, datos.getAnyo().trim());
      // a�o hasta
      st.setString(4, anoHasta);
      st.setString(5, anoHasta);
      // semana desde
      st.setString(2, datos.getSemana().trim());
      // semana hasta
      st.setString(6, semHasta);
    }
    rs = st.executeQuery();

    i = 0;
    // extrae la p�gina requerida
    while (rs.next()) {
      // a�ade un nodo
      data.addElement(new DataNotifSem(0,
                                       datos.getNotift(),
                                       param.getLogin(),
                                       datos.getEquipo(),
                                       rs.getString("CD_SEMEPI"),
                                       rs.getString("CD_ANOEPI")));

      i++;
    }

    rs.close();
    st.close();
    rs = null;
    st = null;

    // Ahora insertamos en la cobertura los registros de antes
    // prepara la query
    query = "insert into sive_notif_sem(nm_ntotreal,nm_nnotift,cd_ope," +
        "fc_ultact,cd_e_notif,cd_semepi,cd_anoepi) values (?,?,?,?,?,?,?)";

    st = con.prepareStatement(query);

    if (i != 0) {

      for (int j = 0; j < i; j++) {
        try {

          datosSemana = (DataNotifSem) data.elementAt(j);

          // n�mero notificadores reales
          st.setInt(1, datosSemana.getNotifReal());
          // n�mero notificadores totales
          st.setInt(2, datosSemana.getNotifTotal());
          // c�digo operador
          st.setString(3, datosSemana.getCdOperador().trim());
          // fecha �ltima actualizaci�n
          st.setDate(4, new java.sql.Date( (new java.util.Date()).getTime()));
          // c�d. Equipo Notificador
          st.setString(5, datosSemana.getCdEquipo().trim());
          // c�digo semana epid.
          st.setString(6, datosSemana.getSemana().trim());
          // c�digo a�o epid.
          st.setString(7, datosSemana.getAnyo().trim());

          // lanza la query
          st.executeUpdate();
          //# // System_out.println("Hay cobertura: Un dato.");
        }
        catch (Exception e) {}
      } // bucle for
      st.close();
      st = null;
    }
  }

}

//AIC
class dataFecha {
  protected String sAnyo = "";
  protected String sSemana = "";

  public dataFecha(String anyo, String semana) {
    sAnyo = anyo;
    sSemana = semana;
  }

  public String getAnyo() {
    return sAnyo;
  }

  public String getSemana() {
    return sSemana;
  }
}
