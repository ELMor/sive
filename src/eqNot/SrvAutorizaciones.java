package eqNot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;
import zbs.DataZBS;

public class SrvAutorizaciones
    extends DBServlet {
//Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletOBTENER_ZBS_X_CODIGO = 7;
  final int servletOBTENER_ZBS_X_DESCRIPCION = 8;
  final int servletOBTENER_PERFIL_3_X_DESCRIPCION = 9;
  final int servletOBTENER_PERFIL_4_X_DESCRIPCION = 10;
  final int servletSELECCION_PERFIL_3_X_DESCRIPCION = 11;
  final int servletSELECCION_PERFIL_4_X_DESCRIPCION = 12;
  final int servletOBTENER_PERFIL_3_X_CODIGO = 13;
  final int servletOBTENER_PERFIL_4_X_CODIGO = 14;
  final int servletSELECCION_PERFIL_3_X_CODIGO = 15;
  final int servletSELECCION_PERFIL_4_X_CODIGO = 16;
  final int servletSELECCION_PERFIL_2_X_CODIGO = 17;
  final int servletSELECCION_PERFIL_2_X_DESCRIPCION = 18;
  final int servletOBTENER_PERFIL_2_X_CODIGO = 19;
  final int servletOBTENER_PERFIL_2_X_DESCRIPCION = 20;
  final int servletOBTENER_MAX_SEMANA = 30;

  // busca un nivel 1
  final String sBUSQUEDA_NIV1 = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where CD_NIVEL_1= ? ";
  // busca un nivel 2
  final String sBUSQUEDA_NIV2 = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_2 = ? and CD_NIVEL_1 = ?";
  // busca un item
  final String sBUSQUEDA_ZBS = "select CD_ZBS, DS_ZBS, DSL_ZBS from SIVE_ZONA_BASICA where CD_ZBS = ? and CD_NIVEL_1 = ? and CD_NIVEL_2 = ?";

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    CLista data = new CLista();
    DataAutoriza datos = null;
    DataAutoriza elem = null; //Para meter descripciones
    DataZBS datos2 = null;
    DataNotifSem datosNoti = null;

    //Variables para recoger descripciones
    String sNiv1 = "";
    String sNiv2 = "";
    String sDes = "";
    String sDesL = "";
    String sCod = "";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    datos = (DataAutoriza) param.firstElement();

//# // System_out.println("Recofge elemento*********************************");

    try {
      // modos de operaci�n
      switch (opmode) {

        /*
              // obtener por perfiles de usuario
              case servletOBTENER_PERFIL_2_X_CODIGO:
              case servletOBTENER_PERFIL_3_X_CODIGO:
              case servletOBTENER_PERFIL_4_X_CODIGO:
              case servletOBTENER_PERFIL_2_X_DESCRIPCION:
              case servletOBTENER_PERFIL_3_X_DESCRIPCION:
              case servletOBTENER_PERFIL_4_X_DESCRIPCION:
                // prepara la query
                  if (opmode == servletOBTENER_PERFIL_2_X_CODIGO)
             query = "select * from sive_zona_basica where CD_ZBS = ? order by CD_ZBS";
                  else if (opmode == servletOBTENER_PERFIL_2_X_DESCRIPCION)
             query = "select * from sive_zona_basica where DS_ZBS = ? order by CD_ZBS";
                  else if (opmode == servletOBTENER_PERFIL_4_X_CODIGO)
             query = "select c.ds_zbs, c.cd_nivel_1, c.cd_nivel_2, c.cd_zbs, c.ds_zbs "+
                    "from sive_autorizaciones a, sive_zona_basica c "+
             "where c.CD_NIVEL_1 = a.CD_NIVEL_1 and a.CD_USUARIO = ? and c.CD_ZBS = ? and "+
                    "c.CD_NIVEL_2 = a.CD_NIVEL_2  order by c.CD_ZBS";
                  else if (opmode == servletOBTENER_PERFIL_4_X_DESCRIPCION)
             query = "select c.ds_zbs, c.cd_nivel_1, c.cd_nivel_2, c.cd_zbs, c.ds_zbs "+
                    "from sive_autorizaciones a, sive_zona_basica c "+
             "where c.CD_NIVEL_1 = a.CD_NIVEL_1 and a.CD_USUARIO = ? and c.DS_ZBS = ? and "+
                    "c.CD_NIVEL_2 = a.CD_NIVEL_2 order by c.CD_ZBS";
                  else if (opmode == servletOBTENER_PERFIL_3_X_DESCRIPCION)
             query = "select c.ds_zbs, c.cd_nivel_1, c.cd_nivel_2, c.cd_zbs, c.ds_zbs "+
                    "from sive_autorizaciones a, sive_zona_basica c "+
                    "where c.CD_NIVEL_1 = a.CD_NIVEL_1 and a.CD_USUARIO = ? and c.DS_ZBS = ? order by c.CD_ZBS";
                  else // servletOBTENER_PERFIL_3_X_CODIGO
             query = "select c.ds_zbs, c.cd_nivel_1, c.cd_nivel_2, c.cd_zbs, c.ds_zbs "+
                    "from sive_autorizaciones a, sive_zona_basica c "+
                    "where c.CD_NIVEL_1 = a.CD_NIVEL_1 and a.CD_USUARIO = ? and c.CD_ZBS = ? order by c.CD_ZBS";
                // prepara la lista de resultados
                data = new CLista();
                // lanza la query
                st = con.prepareStatement(query);
                if ((opmode == servletOBTENER_PERFIL_2_X_DESCRIPCION)||
                    (opmode == servletOBTENER_PERFIL_2_X_CODIGO)){
                  // c�digo o descripci�n
                  st.setString(1, datos.getCodSec().trim());
                }else{
                  // c�digo usuario
                  st.setString(1, datos.getUsuario().trim());
                  // descripci�n o c�digo
                  st.setString(2, datos.getCodSec().trim());
                }
                rs = st.executeQuery();
                // extrae la p�gina requerida
                while (rs.next()) {
                  // control de estado
                  if (data.getState() == CLista.listaVACIA) {
                    data.setState(CLista.listaLLENA);
                  }
                  // a�ade un nodo
                  data.addElement(new DataZBS(rs.getString("CD_ZBS"),
                  rs.getString("DS_ZBS"),"",
                  rs.getString("CD_NIVEL_1"),rs.getString("CD_NIVEL_2")));
                  i++;
                }
                rs.close();
                st.close();
                break;
                case servletSELECCION_PERFIL_2_X_DESCRIPCION:
                case servletSELECCION_PERFIL_3_X_DESCRIPCION:
                case servletSELECCION_PERFIL_4_X_DESCRIPCION:
                case servletSELECCION_PERFIL_2_X_CODIGO:
                case servletSELECCION_PERFIL_3_X_CODIGO:
                case servletSELECCION_PERFIL_4_X_CODIGO:
                if (param.getFilter().length() > 0){
                  if (opmode == servletSELECCION_PERFIL_4_X_CODIGO)
             query = "select c.ds_zbs, c.cd_nivel_1, c.cd_nivel_2, c.cd_zbs, c.ds_zbs "+
                    "from sive_autorizaciones a, sive_zona_basica c "+
                    "where c.CD_NIVEL_1 = a.CD_NIVEL_1 and a.CD_USUARIO = ? and c.CD_ZBS like ? and "+
             "c.CD_ZBS > ? and c.CD_NIVEL_2 = a.CD_NIVEL_2  order by c.CD_ZBS";
                  else if (opmode == servletSELECCION_PERFIL_4_X_DESCRIPCION)
             query = "select c.ds_zbs, c.cd_nivel_1, c.cd_nivel_2, c.cd_zbs, c.ds_zbs "+
                    "from sive_autorizaciones a, sive_zona_basica c "+
                    "where c.CD_NIVEL_1 = a.CD_NIVEL_1 and a.CD_USUARIO = ? and c.DS_ZBS like ? and "+
             "c.CD_ZBS > ? and c.CD_NIVEL_2 = a.CD_NIVEL_2  order by c.CD_ZBS";
                  else if (opmode == servletSELECCION_PERFIL_3_X_CODIGO)
             query = "select c.ds_zbs, c.cd_nivel_1, c.cd_nivel_2, c.cd_zbs, c.ds_zbs "+
                    "from sive_autorizaciones a, sive_zona_basica c "+
                    "where c.CD_NIVEL_1 = a.CD_NIVEL_1 and a.CD_USUARIO = ? and c.CD_ZBS like ? and "+
                    "c.CD_ZBS > ? order by c.CD_ZBS";
                  else if (opmode == servletSELECCION_PERFIL_3_X_DESCRIPCION)
             query = "select c.ds_zbs, c.cd_nivel_1, c.cd_nivel_2, c.cd_zbs, c.ds_zbs "+
                    "from sive_autorizaciones a, sive_zona_basica c "+
                    "where c.CD_NIVEL_1 = a.CD_NIVEL_1 and a.CD_USUARIO = ? and c.DS_ZBS like ? and "+
                    "c.CD_ZBS > ? order by c.CD_ZBS";
                  else if (opmode == servletSELECCION_PERFIL_2_X_CODIGO)
             query = "select * from sive_zona_basica where CD_ZBS like ? and "+
                    "CD_ZBS > ? order by c.CD_ZBS";
                  else if (opmode == servletSELECCION_PERFIL_2_X_DESCRIPCION)
             query = "select * from sive_zona_basica where DS_ZBS like ? and "+
                    "CD_ZBS > ? order by c.CD_ZBS";
                }else{  //no hay filtro
                  if (opmode == servletSELECCION_PERFIL_4_X_CODIGO)
             query = "select c.ds_zbs, c.cd_nivel_1, c.cd_nivel_2, c.cd_zbs, c.ds_zbs "+
                    "from sive_autorizaciones a, sive_zona_basica c "+
                    "where c.CD_NIVEL_1 = a.CD_NIVEL_1 and a.CD_USUARIO = ? and c.CD_ZBS like ? and "+
                    "c.CD_NIVEL_2 = a.CD_NIVEL_2  order by c.CD_ZBS";
                  else if (opmode == servletSELECCION_PERFIL_4_X_DESCRIPCION)
             query = "select c.ds_zbs, c.cd_nivel_1, c.cd_nivel_2, c.cd_zbs, c.ds_zbs "+
                    "from sive_autorizaciones a, sive_zona_basica c "+
                    "where c.CD_NIVEL_1 = a.CD_NIVEL_1 and a.CD_USUARIO = ? and c.DS_ZBS like ? and "+
                    "c.CD_NIVEL_2 = a.CD_NIVEL_2  order by c.CD_ZBS";
                  else if (opmode == servletSELECCION_PERFIL_3_X_CODIGO)
             query = "select c.ds_zbs, c.cd_nivel_1, c.cd_nivel_2, c.cd_zbs, c.ds_zbs "+
                    "from sive_autorizaciones a, sive_zona_basica c "+
             "where c.CD_NIVEL_1 = a.CD_NIVEL_1 and a.CD_USUARIO = ? and c.CD_ZBS like ? "+
                    "order by c.CD_ZBS";
                  else if (opmode == servletSELECCION_PERFIL_3_X_DESCRIPCION)
             query = "select c.ds_zbs, c.cd_nivel_1, c.cd_nivel_2, c.cd_zbs, c.ds_zbs "+
                    "from sive_autorizaciones a, sive_zona_basica c "+
             "where c.CD_NIVEL_1 = a.CD_NIVEL_1 and a.CD_USUARIO = ? and c.DS_ZBS like ? "+
                    "order by c.CD_ZBS";
                  else if (opmode == servletSELECCION_PERFIL_2_X_CODIGO)
             query = "select * from sive_zona_basica where CD_ZBS like ? "+
                    "order by c.CD_ZBS";
                  else if (opmode == servletSELECCION_PERFIL_2_X_DESCRIPCION)
             query = "select * from sive_zona_basica where DS_ZBS like ? "+
                    "order by c.CD_ZBS";
                }
                // prepara la lista de resultados
                data = new CLista();
                // lanza la query
                st = con.prepareStatement(query);
                if ((opmode == servletSELECCION_PERFIL_2_X_DESCRIPCION)||
                    (opmode == servletSELECCION_PERFIL_2_X_DESCRIPCION)){
                    // descripci�n o c�digo
                    st.setString(1, datos.getCodSec().trim()+ "%");
                }else{
                    // c�digo usuario
                    st.setString(1, datos.getUsuario().trim());
                    // descripci�n o c�digo
                    st.setString(2, datos.getCodSec().trim()+ "%");
                    // paginaci�n
                    if (param.getFilter().length() > 0) {
                      st.setString(3, param.getFilter().trim());
                    }
                }
                rs = st.executeQuery();
                // extrae la p�gina requerida
                while (rs.next()) {
                  // control de tama�o
                  if (i > DBServlet.maxSIZE) {
                    data.setState(CLista.listaINCOMPLETA);
                    data.setFilter( ((DataZBS)data.lastElement()).getCod() );
                    break;
                  }
                  // control de estado
                  if (data.getState() == CLista.listaVACIA) {
                    data.setState(CLista.listaLLENA);
                  }
                  // a�ade un nodo
                  data.addElement(new DataZBS(rs.getString("CD_ZBS"),
                  rs.getString("DS_ZBS"),"",
                  rs.getString("CD_NIVEL_1"),rs.getString("CD_NIVEL_2")));
                  i++;
                }
                rs.close();
                st.close();
                break;
         */
        case servletOBTENER_MAX_SEMANA:

          // prepara la query
          query = "select * from sive_semana_epi where CD_SEMEPI >= ? and CD_ANOEPI = ? order by CD_SEMEPI";

          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);
          // semana
          st.setString(1, datos.getUsuario().trim());
          // A�o
          st.setString(2, datos.getCodSec().trim());

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // a�ade un nodo
            data.addElement(new DataAutoriza(rs.getString("CD_SEMEPI"),
                                             "", "", "", ""));
            i++;
          }

          rs.close();
          st.close();
          break;

        case servletOBTENER_X_CODIGO:

          // prepara la lista de resultados
          data = new CLista();

          data.addElement(new DataAutoriza());

          if (data.size() > 0) {
            elem = (DataAutoriza) (data.firstElement());

            //# // System_out.println("Hay algun elemento**********************");

            //___________________ A por datos de descripciones____________________

            //___________________ A por datos de Nivel 1____________________

            // lanza la query  para N1

            rs = null;
            st = null;

            st = con.prepareStatement(sBUSQUEDA_NIV1);

            st.setString(1, datos.getCodNiv1().trim());
            rs = st.executeQuery();

            // extrae el registro encontrado
            while (rs.next()) {
              // obtiene los campos
              sNiv1 = rs.getString("CD_NIVEL_1");
              sDes = rs.getString("DS_NIVEL_1");
              sDesL = rs.getString("DSL_NIVEL_1");

              // control de estado
              if (data.getState() == CLista.listaVACIA) {
                data.setState(CLista.listaLLENA);
              }

              // obtiene la descripcion auxiliar en funci�n del idioma
              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesL != null)) {
                sDes = sDesL;

                // a�ade la des en un nodo
              }
              elem.setDesNiv1(sDes);
            }
            rs.close();
            st.close();

            //___________________________A por el nivel 2_____________________

            // lanza la query  para N2

            rs = null;
            st = null;

            st = con.prepareStatement(sBUSQUEDA_NIV2);

            st.setString(1, datos.getCodNiv2().trim());
            st.setString(2, datos.getCodNiv1().trim());
            rs = st.executeQuery();

            // extrae el registro encontrado
            while (rs.next()) {
              // obtiene los campos
              sNiv2 = rs.getString("CD_NIVEL_2");
              sDes = rs.getString("DS_NIVEL_2");
              sDesL = rs.getString("DSL_NIVEL_2");

              // obtiene la descripcion auxiliar en funci�n del idioma
              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesL != null)) {
                sDes = sDesL;

                // a�ade la des en un nodo
              }
              elem.setDesNiv2(sDes);
            }
            rs.close();
            st.close();

            //__________________________ A Por el ZBS_________________________

            rs = null;
            st = null;

            st = con.prepareStatement(sBUSQUEDA_ZBS);

            // filtro
            st.setString(1, datos.getCodZbs().trim());
            st.setString(2, datos.getCodNiv1().trim());
            st.setString(3, datos.getCodNiv2().trim());

            //# // System_out.println(" El ZBS******" + datos.getCodZbs().trim() );
            //# // System_out.println(" El N1******" + datos.getCodNiv1().trim() );
            //# // System_out.println(" El N2******" + datos.getCodNiv2().trim() );

            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {

              // obtiene los campos
              sCod = rs.getString("CD_ZBS");
              sDes = rs.getString("DS_ZBS");
              sDesL = rs.getString("DSL_ZBS");

              // obtiene la descripcion auxiliar en funci�n del idioma
              if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesL != null)) {
                sDes = sDesL;

                // a�ade la des en un nodo
              }
              elem.setDesZbs(sDes);

              //# // System_out.println("Des ZBS Elmento *******" + elem.getDesZbs() );

            }
            rs.close();
            st.close();

          } //If hay autorizaciones

          break;

      }
      con.commit();
    }
    catch (Exception er) {
      con.rollback();
      throw er;
    }
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }

} //CLASE
