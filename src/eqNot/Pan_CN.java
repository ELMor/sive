package eqNot;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import capp.CPanel;

public class Pan_CN
    extends CPanel {

//Modos de servlet para cambio

  public static final int servletOBTENER_X_CODIGO = 3;
  public static final int servletOBTENER_X_DESCRIPCION = 4;
  public static final int servletSELECCION_X_CODIGO = 5;
  public static final int servletSELECCION_X_DESCRIPCION = 6;

  public static final int servletSELECCION_EQUIPOS_UN_CENTRO_X_CODIGO = 21; //equipos de un centro determinado
  public static final int servletSELECCION_EQUIPOS_UN_CENTRO_X_DESCRIPCION = 22; //equipos de un centro determinado
  public static final int servletOBTENER_EQUIPOS_UN_CENTRO_X_CODIGO = 23; //equipos de un centro determinado
  public static final int servletOBTENER_EQUIPOS_UN_CENTRO_X_DESCRIPCION = 24; //equipos de un centro determinado

  BorderLayout borderLayout1 = new BorderLayout();
  PanelBusca PanAbajo = null;
  Pan_SupEq PanArriba = null;

  public Pan_CN(eqNot a) {

    try {
      this.app = a;
      PanAbajo = new PanelBusca( (eqNot)this.app);
      PanArriba = new Pan_SupEq( (eqNot)this.app);
      PanArriba.txtCentro.addFocusListener(new escuchaFoco(this));
      PanArriba.txtCentro.addKeyListener(new escuchaTecla(this));
      PanArriba.btnCentro.addActionListener(new escuchaBoton(this));
      jbInit();
      PanArriba.txtCentro.requestFocus();
      PanArriba.txtDesCentro.setEnabled(false);
      PanArriba.txtDesCentro.setEditable(false);
      PanArriba.btnCentro.setFocusAware(false);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setLayout(borderLayout1);
    this.add(PanArriba, BorderLayout.NORTH);
    this.add(PanAbajo, BorderLayout.CENTER);
  }

  public void Inicializar() {
  }
}

class escuchaBoton
    implements java.awt.event.ActionListener, Runnable {
  Pan_CN adaptee;
  ActionEvent e;

  escuchaBoton(Pan_CN adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.PanAbajo.modoOperacion = adaptee.PanAbajo.modoESPERA;
    adaptee.PanAbajo.Inicializar();

    adaptee.PanArriba.btnCentro_actionPerformed(e);

    if (adaptee.PanArriba.txtDesCentro.getText().compareTo("") != 0) {
      adaptee.PanAbajo.elCentro = adaptee.PanArriba.txtCentro.getText();
      adaptee.PanAbajo.modoSeleccion_x_codigo = SrvEqNot.
          servletSELECCION_EQUIPOS_UN_CENTRO_X_CODIGO;
      adaptee.PanAbajo.modoSeleccion_x_descripcion = SrvEqNot.
          servletSELECCION_EQUIPOS_UN_CENTRO_X_DESCRIPCION;
    }
    else {
      adaptee.PanAbajo.elCentro = "";
      adaptee.PanAbajo.modoSeleccion_x_codigo = SrvEqNot.
          servletSELECCION_X_CODIGO;
      adaptee.PanAbajo.modoSeleccion_x_descripcion = SrvEqNot.
          servletSELECCION_X_DESCRIPCION;
    }

    adaptee.PanAbajo.modoOperacion = adaptee.PanAbajo.modoNORMAL;
    adaptee.PanAbajo.Inicializar();
  }
}

class escuchaTecla
    extends java.awt.event.KeyAdapter {
  Pan_CN adaptee;

  escuchaTecla(Pan_CN adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.PanAbajo.modoOperacion = adaptee.PanAbajo.modoESPERA;
    adaptee.PanAbajo.Inicializar();

    adaptee.PanArriba.txtCentro_keyPressed(e);
    adaptee.PanAbajo.elCentro = "";

    adaptee.PanAbajo.modoSeleccion_x_codigo = SrvEqNot.
        servletSELECCION_X_CODIGO;
    adaptee.PanAbajo.modoSeleccion_x_descripcion = SrvEqNot.
        servletSELECCION_X_DESCRIPCION;

    adaptee.PanAbajo.modoOperacion = adaptee.PanAbajo.modoNORMAL;
    adaptee.PanAbajo.Inicializar();
  }
}

class escuchaFoco
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_CN adaptee;
  FocusEvent e;

  escuchaFoco(Pan_CN adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.PanAbajo.modoOperacion = adaptee.PanAbajo.modoESPERA;
    adaptee.PanAbajo.Inicializar();

    adaptee.PanArriba.txtCentro_focusLost(e);

    if (adaptee.PanArriba.txtDesCentro.getText().compareTo("") != 0) {
      adaptee.PanAbajo.elCentro = adaptee.PanArriba.txtCentro.getText();
      adaptee.PanAbajo.modoSeleccion_x_codigo = SrvEqNot.
          servletSELECCION_EQUIPOS_UN_CENTRO_X_CODIGO;
      adaptee.PanAbajo.modoSeleccion_x_descripcion = SrvEqNot.
          servletSELECCION_EQUIPOS_UN_CENTRO_X_DESCRIPCION;

    }
    else {
      adaptee.PanAbajo.elCentro = "";
      adaptee.PanAbajo.modoSeleccion_x_codigo = SrvEqNot.
          servletSELECCION_X_CODIGO;
      adaptee.PanAbajo.modoSeleccion_x_descripcion = SrvEqNot.
          servletSELECCION_X_DESCRIPCION;

    }

    /*if (!adaptee.PanArriba.check){
      adaptee.PanAbajo.btnCtrlCodModelo_actionPerformed();
      adaptee.PanArriba.check = false;
         }*/

    //DSR
    if (adaptee.PanArriba.txtDesCentro.getText().compareTo("") != 0) {
      adaptee.PanAbajo.btnCtrlCodModelo_actionPerformed();
    }

    adaptee.PanAbajo.modoOperacion = adaptee.PanAbajo.modoNORMAL;
    adaptee.PanAbajo.Inicializar();
  }
}
