package catalogo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvCat
    extends DBServlet {

  // Uso de campo DSL
  boolean bHayDesL = true;

  // modos de operaci�n
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    //Descripciones
    String sDescrip = "";
    String sDescripL = "";
    bHayDesL = true;

    // contador
    int i = 1;

    // objetos de datos
    CLista data = null;
    DataCat cat = null;

    // nombres de campos y tablas
    String sTab = null;
    String sCod = null;
    String sDes = null;
    String sDesL = null;

    // modos de operaci�n y tipo de cat�logo
    int iModo = opmode % 10;
    int iCatalogo = opmode - iModo;

//    System_out.println("^^^^^^20:40");

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    // obtiene los par�metros enviados desde el servidor
    cat = (DataCat) param.firstElement();

    // determina el cat�logo de trabajo

    switch (iCatalogo) {
      case Catalogo.catCCAA:
        sTab = "SIVE_COM_AUT";
        sCod = "CD_CA";
        sDes = "DS_CA";
        sDesL = "DSL_CA";
        break;

      case Catalogo.catEDO:
        sTab = "SIVE_EDO_CNE";
        sCod = "CD_ENFEREDO";
        sDes = "DS_ENFEREDO";
        sDesL = "DSL_ENFEREDO";
        break;

        /*      case Catalogo.catESPECIALIDAD:
                sTab = "SIVE_ESPECIALIDAD";
                sCod = "CD_ESPEC";
                sDes = "DS_ESPEC";
                sDesL = "DSL_ESPEC";
                break;
      */
      case Catalogo.catMOTBAJA:
        sTab = "SIVE_MOTIVO_BAJA";
        sCod = "CD_MOTBAJA";
        sDes = "DS_MOTBAJA";
        sDesL = "DSL_MOTBAJA";
        break;

      case Catalogo.catNIVASISTENCIAL:
        sTab = "SIVE_NIV_ASIST";
        sCod = "CD_NIVASIS";
        sDes = "DS_NIVASIS";
        sDesL = "DSL_NIVASIS";
        break;

      case Catalogo.catPROCESOS:
        sTab = "SIVE_PROCESOS";
        sCod = "CD_ENFCIE";
        sDes = "DS_PROCESO";
        sDesL = "DSL_PROCESO";
        break;

      case Catalogo.catSEXO:
        sTab = "SIVE_SEXO";
        sCod = "CD_SEXO";
        sDes = "DS_SEXO";
        sDesL = "DSL_SEXO";
        break;

        /*      case Catalogo.catTASISTENCIA:
                sTab = "SIVE_TASISTENCIA";
                sCod = "CD_TASIST";
                sDes = "DS_TASIST";
                sDesL = "DSL_TASIST";
                break;
      */
      case Catalogo.catTLINEA:
        sTab = "SIVE_TLINEA";
        sCod = "CD_TLINEA";
        sDes = "DS_TLINEA";
        sDesL = "DSL_TLINEA";
        break;

      case Catalogo.catTPREGUNTA:
        sTab = "SIVE_TPREGUNTA";
        sCod = "CD_TPREG";
        sDes = "DS_TPREG";
        sDesL = "DSL_TPREG";
        break;

      case Catalogo.catTVIGILANCIA:
        sTab = "SIVE_T_VIGILANCIA";
        sCod = "CD_TVIGI";
        sDes = "DS_TVIGI";
        sDesL = "DSL_TVIGI";
        break;

      case Catalogo.catTSIVE:
        sTab = "SIVE_TSIVE";
        sCod = "CD_TSIVE";
        sDes = "DS_TSIVE";
        sDesL = "DSL_TSIVE";
        break;

      case Catalogo.catNIVEL1:
        sTab = "SIVE_NIVEL1_S";
        sCod = "CD_NIVEL_1";
        sDes = "DS_NIVEL_1";
        sDesL = "DSL_NIVEL_1";
        break;

      case Catalogo.catNIVEL2:
        sTab = "SIVE_NIVEL2_S";
        sCod = "CD_NIVEL_2";
        sDes = "DS_NIVEL_2";
        sDesL = "DSL_NIVEL_2";
        break;

      case Catalogo.catPREGUNTA:
        sTab = "SIVE_PREGUNTA";
        sCod = "CD_PREGUNTA";
        sDes = "DS_PREGUNTA";
        sDesL = "DSL_PREGUNTA";
        break;

      case Catalogo.catLISTAS:
        sTab = "SIVE_LISTAS";
        sCod = "CD_LISTA";
        sDes = "DS_LISTA";
        sDesL = "DSL_LISTA";
        break;

      case Catalogo.catINDICADOR:
        sTab = "SIVE_INDICADOR";
        sCod = "CD_INDALAR";
        sDes = "DS_INDALAR";
        sDesL = "IT_ACTIVO";
        break;

      case Catalogo.catCLASIFDIAG:
        sTab = "SIVE_CLASIF_EDO";
        sCod = "CD_CLASIFDIAG";
        sDes = "DS_CLASIFDIAG";
        sDesL = "DSL_CLASIFDIAG";
        break;

        // brotes
      case Catalogo.catGBROTE:
        sTab = "SIVE_GRUPO_BROTE";
        sCod = "CD_GRUPO";
        sDes = "DS_GRUPO";
        sDesL = "DSL_GRUPO";
        break;

      case Catalogo.catTNOTIF:
        sTab = "SIVE_T_NOTIFICADOR";
        sCod = "CD_TNOTIF";
        sDes = "DS_TNOTIF";
        sDesL = "DSL_TNOTIF";
        break;

      case Catalogo.catMTRANSMISION:
        sTab = "SIVE_MEC_TRANS";
        sCod = "CD_TRANSMIS";
        sDes = "DS_TRANSMIS";
        sDesL = "DSL_TRANSMIS";
        break;

      case Catalogo.catTCOLECTIVO:
        sTab = "SIVE_TIPOCOL";
        sCod = "CD_TIPOCOL";
        sDes = "DS_TIPOCOL";
        sDesL = "DSL_TIPOCOL";
        break;

      case Catalogo.catSALERTA:
        sTab = "SIVE_T_SIT_ALERTA";
        sCod = "CD_SITALERBRO";
        sDes = "DS_SITALERBRO";
        sDesL = "DSL_SITALERBRO";
        break;

        //Centinelas
      case Catalogo.catCONGLOM:
        sTab = "SIVE_CONGLOM";
        sCod = "CD_CONGLO";
        sDes = "DS_CONGLO";
        sDesL = "";
        bHayDesL = false;
        break;

      case Catalogo.catMASISTENCIA:
        sTab = "SIVE_MASISTENCIA";
        sCod = "CD_MASIST";
        sDes = "DS_MASIST";
        sDesL = "";
        bHayDesL = false;
        break;

      case Catalogo.catTPCENTINELA:
        sTab = "SIVE_TPCENTINELA";
        sCod = "CD_TPCENTI";
        sDes = "DS_TPCENTI";
        sDesL = "";
        bHayDesL = false;
        break;

      case Catalogo.catINDACTMC:
        sTab = "SIVE_INDACTMC";
        sCod = "CD_INDRMC";
        sDes = "DS_INDRMC";
        sDesL = "";
        bHayDesL = false;
        break;

        // Tuberculosis
      case Catalogo.catFUENTES:
        sTab = "SIVE_FUENTES_NOTIF";
        sCod = "CD_FUENTE";
        sDes = "DS_FUENTE";
        sDesL = "";
        bHayDesL = false;
        break;

      case Catalogo.catMOTIVOS_SALIDA:
        sTab = "SIVE_MOTIVOS_SALRTBC";
        sCod = "CD_MOTSALRTBC";
        sDes = "DS_MOTSALRTBC";
        sDesL = "";
        bHayDesL = false;
        break;

      case Catalogo.catMOTIVOS_INICIO:
        sTab = "SIVE_MOTIVO_TRAT";
        sCod = "CD_MOTRAT";
        sDes = "DS_MOTRAT";
        sDesL = "";
        bHayDesL = false;
        break;

      case Catalogo.catMUESTRAS:
        sTab = "SIVE_MUESTRA_LAB";
        sCod = "CD_MUESTRA";
        sDes = "DS_MUESTRA";
        sDesL = "";
        bHayDesL = false;
        break;

      case Catalogo.catTECNICAS:
        sTab = "SIVE_TIPO_TECNICALAB";
        sCod = "CD_TTECLAB";
        sDes = "DS_TTECLAB";
        sDesL = "";
        bHayDesL = false;
        break;

      case Catalogo.catVALORES_MUESTRAS:
        sTab = "SIVE_VALOR_MUESTRA";
        sCod = "CD_VMUESTRA";
        sDes = "DS_VMUESTRA";
        sDesL = "";
        bHayDesL = false;
        break;

      case Catalogo.catMICOBACTERIAS:
        sTab = "SIVE_TMICOBACTERIA";
        sCod = "CD_TMICOB";
        sDes = "DS_TMICOB";
        sDesL = "";
        bHayDesL = false;
        break;

      case Catalogo.catESTUDIOS_RESISTENCIAS:
        sTab = "SIVE_ESTUDIORESIS";
        sCod = "CD_ESTREST";
        sDes = "DS_ESTREST";
        sDesL = "";
        bHayDesL = false;
        break;

    }

    // modos de operaci�n
    switch (iModo) {

      // inserci�n de un item en el cat�logo
      case servletALTA:
        if (bHayDesL) {
          // prepara la query
          query = "insert into " + sTab + " (" + sCod + ", " + sDes + ", " +
              sDesL + ") values (?, ?, ?)";
          st = con.prepareStatement(query);

          // codigo
          st.setString(1, cat.getCod().trim().toUpperCase());

          // descripci�n
          st.setString(2, cat.getDes().trim());

          // descripci�n local
          if (cat.getDesL().trim().length() > 0) {
            st.setString(3, cat.getDesL().trim());
          }
          else {
            st.setNull(3, java.sql.Types.VARCHAR);

          }
        }
        else {
          // prepara la query
          query = "insert into " + sTab + " (" + sCod + ", " + sDes +
              ") values (?, ?)";
          st = con.prepareStatement(query);

          // codigo
          st.setString(1, cat.getCod().trim().toUpperCase());

          // descripci�n
          st.setString(2, cat.getDes().trim());
        }

        // lanza la query
        st.executeUpdate();
        st.close();

        break;

        // baja de un item en el catalogo
      case servletBAJA:

        // prepara la query
        query = "DELETE FROM " + sTab + " WHERE " + sCod + " = ?";

        // lanza la query
        st = con.prepareStatement(query);
        st.setString(1, cat.getCod().trim());
        st.executeUpdate();
        st.close();

        break;

        // listado de items del cat�logo
      case servletSELECCION_X_CODIGO:
      case servletSELECCION_X_DESCRIPCION:

        if (bHayDesL) {
          // prepara la query
          if (param.getFilter().length() > 0) {
            if (iModo == servletSELECCION_X_CODIGO) {
              query = "select " + sCod + ", " + sDes + ", " + sDesL + " from " +
                  sTab + " where " + sCod + " like ? and " + sCod +
                  " > ? order by " + sCod;
            }
            else {
              query = "select " + sCod + ", " + sDes + ", " + sDesL + " from " +
                  sTab + " where upper(" + sDes + ") like upper(?) and upper(" +
                  sDes + ") > upper(?) order by " + sDes;
            }
          }
          else {
            if (iModo == servletSELECCION_X_CODIGO) {
              query = "select " + sCod + ", " + sDes + ", " + sDesL + " from " +
                  sTab + " where " + sCod + " like ? order by " + sCod;
            }
            else {
              query = "select " + sCod + ", " + sDes + ", " + sDesL + " from " +
                  sTab + " where upper(" + sDes + ") like upper(?) order by " +
                  sDes;
            }
          }

          // prepara la lista de items
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);

          // filtro
          if (iModo == servletSELECCION_X_CODIGO) {
            st.setString(1, cat.getCod().trim() + "%");
          }
          else {
            st.setString(1, "%" + cat.getCod().trim() + "%");

            // paginaci�n
          }
          if (param.getFilter().length() > 0) {
            st.setString(2, param.getFilter().trim());
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (iModo == servletSELECCION_X_CODIGO) {
                data.setFilter( ( (DataCat) data.lastElement()).getCod());
              }
              else {
                data.setFilter( ( (DataCat) data.lastElement()).getDes());
              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            //__________________

            sDescrip = rs.getString(sDes);
            sDescripL = rs.getString(sDesL);

            // Transporta una sola descripci�n si idioma no es el defecto y no estamos en un mantenimiento
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (param.getMantenimiento() == false)) {
              if (sDescripL != null) {
                sDescrip = sDescripL;
              }
            }

            // a�ade un item
            data.addElement(new DataCat(rs.getString(sCod), sDescrip, sDescripL));
            //__________________
            i++;
          }
        }
        else {
          // prepara la query
          if (param.getFilter().length() > 0) {
            if (iModo == servletSELECCION_X_CODIGO) {
              query = "select " + sCod + ", " + sDes + " from " + sTab +
                  " where " + sCod + " like ? and " + sCod + " > ? order by " +
                  sCod;
            }
            else {
              query = "select " + sCod + ", " + sDes + " from " + sTab +
                  " where upper(" + sDes + ") like upper(?) and upper(" + sDes +
                  ") > upper(?) order by " + sDes;
            }
          }
          else {
            if (iModo == servletSELECCION_X_CODIGO) {
              query = "select " + sCod + ", " + sDes + " from " + sTab +
                  " where " + sCod + " like ? order by " + sCod;
            }
            else {
              query = "select " + sCod + ", " + sDes + " from " + sTab +
                  " where upper(" + sDes + ") like upper(?) order by " + sDes;
            }
          }

          // prepara la lista de items
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);

          // filtro
          if (iModo == servletSELECCION_X_CODIGO) {
            st.setString(1, cat.getCod().trim() + "%");
          }
          else {
            st.setString(1, "%" + cat.getCod().trim() + "%");

            // paginaci�n
          }
          if (param.getFilter().length() > 0) {
            st.setString(2, param.getFilter().trim());
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (iModo == servletSELECCION_X_CODIGO) {
                data.setFilter( ( (DataCat) data.lastElement()).getCod());
              }
              else {
                data.setFilter( ( (DataCat) data.lastElement()).getDes());
              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            //__________________

            sDescrip = rs.getString(sDes);

            // a�ade un item
            data.addElement(new DataCat(rs.getString(sCod), sDescrip, ""));
            //__________________
            i++;
          }

        }

        rs.close();
        st.close();

        break;

        // modificaci�n de un item
      case servletMODIFICAR:
        if (bHayDesL) {
          // prepara la query
          query = "UPDATE " + sTab + " SET " + sDes + "=?, " + sDesL +
              "=? WHERE " + sCod + "=?";

          // lanza la query
          st = con.prepareStatement(query);

          // descripci�n
          st.setString(1, cat.getDes().trim());

          // descripci�n local
          if (cat.getDesL().trim().length() > 0) {
            st.setString(2, cat.getDesL().trim());
          }
          else {
            st.setNull(2, java.sql.Types.VARCHAR);

            // codigo
          }
          st.setString(3, cat.getCod().trim());

        }
        else {
          // prepara la query
          query = "UPDATE " + sTab + " SET " + sDes + "=? WHERE " + sCod + "=?";

          // lanza la query
          st = con.prepareStatement(query);

          // descripci�n
          st.setString(1, cat.getDes().trim());

          // codigo
          st.setString(2, cat.getCod().trim());

        }

        st.executeUpdate();
        st.close();

        break;

        // extrae un item del cat�logo por su c�digo
      case servletOBTENER_X_CODIGO:
      case servletOBTENER_X_DESCRIPCION:
        if (bHayDesL) {
          // prepara la query
          if (iModo == servletOBTENER_X_CODIGO) {
            query = "select " + sCod + ", " + sDes + ", " + sDesL + " from " +
                sTab + " where " + sCod + " = ?";
          }
          else {
            query = "select " + sCod + ", " + sDes + ", " + sDesL + " from " +
                sTab + " where upper(" + sDes + ") = upper(?)";

            // lanza la query
          }
          st = con.prepareStatement(query);
          st.setString(1, cat.getCod().trim());
          rs = st.executeQuery();

          data = new CLista();

          // extrae el item encontrado
          while (rs.next()) {
            // a�ade un nodo

            //_____________________

            sDescrip = rs.getString(sDes);
            sDescripL = rs.getString(sDesL);

            // Transporta una sola descripci�n si idioma no es el defecto y no estamos en un mantenimiento
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (param.getMantenimiento() == false)) {
              if (sDescripL != null) {
                sDescrip = sDescripL;
              }
            }

            data.addElement(new DataCat(rs.getString(sCod), sDescrip, sDescripL));

            //_____________________
          }
        }
        else {
          // prepara la query
          if (iModo == servletOBTENER_X_CODIGO) {
            query = "select " + sCod + ", " + sDes + " from " + sTab +
                " where " + sCod + " = ?";
          }
          else {
            query = "select " + sCod + ", " + sDes + " from " + sTab +
                " where upper(" + sDes + ") = upper(?)";

            // lanza la query
          }
          st = con.prepareStatement(query);
          st.setString(1, cat.getCod().trim());
          rs = st.executeQuery();

          data = new CLista();

          // extrae el item encontrado
          while (rs.next()) {
            // a�ade un nodo

            //_____________________

            sDescrip = rs.getString(sDes);

            data.addElement(new DataCat(rs.getString(sCod), sDescrip, ""));

            //_____________________
          }
        }

        rs.close();
        st.close();

        break;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }
}
