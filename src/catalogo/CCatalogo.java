package catalogo;

import capp.CLista;

public class CCatalogo
    extends CLista {

  // catalogos
  public static final int catCCAA = 1;
  public static final int catEDO = 2;
  public static final int catTLINEA = 3;
  public static final int catTPREGUNTA = 4;
  public static final int catTSIVE = 5;
  public static final int catTVIGILANCIA = 6;
  public static final int catMOTBAJA = 7;
  public static final int catNIVASISTENCIAL = 8;
  public static final int catPROCESOS = 9;
  public static final int catSEXO = 10;
  public static final int catTASISTENCIA = 11;
  public static final int catESPECIALIDAD = 12;

  protected int iCatalogo;

  public CCatalogo() {
    super();
  }

  public CCatalogo(int c) {
    super();
    iCatalogo = c;
  }

  // establece el tipo de cat�logo
  public void setCatalogo(int c) {
    iCatalogo = c;
  }

  // recupera el tipo de cat�logo
  public int getCatalogo() {
    return iCatalogo;
  }
}