//Muestra mensaje de confirmaci�n de PWD

package mantus;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CDialog;

public class DialConfirPwd
    extends CDialog {

  boolean valido = false;
  ResourceBundle res = ResourceBundle.getBundle("mantus.Res" + app.getIdioma());

  //Constantes del panel

  final String imgACEPTAR = "images/aceptar.gif";
  final String imgCANCELAR = "images/cancelar.gif";

  // par�metros
  public boolean bAceptar = false;

  // controles
  XYLayout xyLyt = new XYLayout();
  Label lbl1 = new Label();
  TextField txtConfPwd = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  DialConfirPwdActionListener btnActionListener = new
      DialConfirPwdActionListener(this);

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    btnAceptar.setEnabled(true);
    btnCancelar.setEnabled(true);
    txtConfPwd.setEnabled(true);
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

  }

  // contructor
  public DialConfirPwd(CApp a) {

    super(a);

    try {
      this.setTitle(res.getString("this.Title"));
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // aspecto de la ventana
  private void jbInit() throws Exception {
    //xyLyt.setHeight(90);
    //xyLyt.setWidth(265);
    setSize(265, 130);
    lbl1.setText(res.getString("lbl1.Text"));
    txtConfPwd.setBackground(new Color(255, 255, 150));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnAceptar.setImageURL(new URL(app.getCodeBase(), imgACEPTAR));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    btnCancelar.setImageURL(new URL(app.getCodeBase(), imgCANCELAR));
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");
    this.setLayout(xyLyt);
    this.add(lbl1, new XYConstraints(4, 5, -1, -1));
    this.add(txtConfPwd, new XYConstraints(100, 5, 100, -1));
    this.add(btnAceptar, new XYConstraints(70, 50, 80, 26));
    this.add(btnCancelar, new XYConstraints(170, 50, 80, 26));

    // establece los escuchadores
    btnAceptar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);
    txtConfPwd.addActionListener(btnActionListener);

    //Se fija el car�cter de eco al usuario para caja del password
    txtConfPwd.setEchoChar('*');

    // establece el modo de operaci�n
    Inicializar();
  }

  public String getConfPwd() {
    return txtConfPwd.getText();
  }

} //CLASE

// action listener
class DialConfirPwdActionListener
    implements ActionListener {
  DialConfirPwd adaptee = null;
  ActionEvent e = null;

  public DialConfirPwdActionListener(DialConfirPwd adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    if ( (e.getActionCommand() == "aceptar")) { // aceptar
      adaptee.bAceptar = true;

    }
    else if (e.getActionCommand() == "cancelar") { // cancelar
      adaptee.bAceptar = false;
    }
    adaptee.dispose();
  }
}
