/**
 * @ADAPTACION DEL ANTIGUO MANTUS-PISTA AL UTILIZADO EN ICM99 PDP 14/04/2000
 * @version 1.0
 */
package mantus;

//import javax.swing.*;
//import enfermo.comun;
import java.util.ResourceBundle;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Panel;

import com.borland.jbcl.control.ButtonControl;
import capp2.CApp;
import capp2.CCargadorImagen;
import capp2.CMessage;
import capp2.CPanel;
import comun.Common;

public class Panel3
    extends CPanel {
  BorderLayout layout = new BorderLayout();
  ResourceBundle res;
  ButtonControl btA�adir = new ButtonControl();
  ButtonControl btModificar = new ButtonControl();
  Panel pnl = new Panel();
  public CMessage mensaje;

//modos de pantalla y consultas
  final int modoALTA = 0;
  final int modoMODIFICAR = 1;
  final int modoSELECCION = 2; //Usado s�lo como modo oper. con sevlett
  final int modoOBTENER = 4;
  final int modoESPERA = 5; // Usado solo como modo pantalla
  final int modoSELECCION_EQU_NOT = 6; // Usado solo como modo pantalla
  //El opmode de consulta ser� el correspondiente al servlett que consulta el eq not y variar� la URL: Va al url del eq not)
  final int modoSELECCION_NIVEL = 7; //// Usado solo como modo pantalla
  final int modoGENERAR_LOGIN = 8;

  public int modoOperacion = modoALTA;

  public Panel3(CApp a, DialUsuario a_diusu) {
    try {
      setApp(a);
      res = ResourceBundle.getBundle("mantus.Res" + app.getIdioma());
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgNAME[] = {
        Common.imgACEPTAR,
        Common.imgCANCELAR};
    CCargadorImagen imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    this.setSize(new Dimension(400, 62));

    btA�adir.setLabel(res.getString("btAnadir.Label"));
    btModificar.setLabel(res.getString("btModificar.Label"));
    btA�adir.setActionCommand("A�adir");
    btModificar.setActionCommand("Cancelar");
    btA�adir.setImage(imgs.getImage(0));
    btModificar.setImage(imgs.getImage(1));
    pnl.setLayout(new FlowLayout());
    pnl.add(btA�adir, null);
    pnl.add(btModificar, null);
    this.setLayout(layout);
    this.add(pnl, BorderLayout.EAST);
    this.modoOperacion = modoALTA;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:
        btA�adir.setEnabled(true);
        btModificar.setEnabled(true);
        break;

      case modoMODIFICAR:
        btA�adir.setEnabled(true);
        btModificar.setEnabled(true);
        break;

      default:
        btA�adir.setEnabled(false);
        btModificar.setEnabled(false);
        break;
    }

  }

}
