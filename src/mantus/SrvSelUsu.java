/**
 * Clase: SrvSelUsu
 * Paquete: mantus
 * Hereda: DBServlet
 * Autor: Pedro Antonio D�az (PDP)
 * Fecha Inicio: 30/03/2000
 * Descripcion: Implementacion del servlet que recupera de la BD los datos
 * a mostrar en la tabla del PanBusca muestras todos los usuarios.
 */

package mantus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Vector;

import capp.CLista;
import comun.Fechas;
import comun.constantes;
import sapp.DBServlet;

public class SrvSelUsu
    extends DBServlet {

  // modos de operaci�n
  final int modoSELECCION_COD = constantes.sSELECCION_X_CODIGO;
  final int modoSELECCION_DES = constantes.sSELECCION_X_DESCRIPCION;
  final int modoMOD = constantes.modoMODIFICACION;
  final int modoALTA = constantes.modoALTA;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int k; //Para �ndice b�squeda columnas
    // objetos de datos
    CLista data = null;
    DataUsuario datUsuario = null;

    //Campos que se reciben/envian en la DataUsuario

    String sCodUsu = ""; //C�digo de usuario
    String sCodComAut = ""; //Com. aut�noma
    String sCodEquNot = ""; //Equipo notificador
    String sCodTipUsu = ""; // Tipo de usuario
    String sDesNom = ""; //Nombre
    String sDesApe = ""; //apellidos
    boolean bAutAlt = false; //Autorizaci�n para dar altas
    boolean bAutMod = false; //Autorizaci�n para hacer modificaciones
    boolean bAutBaj = false; //Autorizaci�n para dar bajas
    String sPer = ""; //perfil
    boolean bEnf = false; //Flag indica si usuario puede ver datos del enfermo
    boolean bManCat = false; //Flag de autorizaci�n a mantenimiento de cat�logos
    boolean bManUsu = false; //Flag de autorizaci�n a mantenimiento de usuarios
    boolean bTraPro = false; //Flag de autorizaci�n de transmisi�n a Madrid e import protocolos
    boolean bDefPro = false; //Flag de autorizaci�n de definici�n de protocolos
    boolean bAla = false; //Flag de autorizaci�n de creaci�n de definiciones de alarmas
    boolean bVal = false; //Flag de autorizaci�n de validaci�n de notificaciones
    String sDesTel = ""; //Tel�fonos de contacto
    String sDesDir = ""; //Direcci�n de contacto
    String sDesCor = ""; //Correo electr�nico
    String sDesAno = ""; //Otras anotaciones
    String sFecAlt = ""; //Fecha de alta
    String sFecBaj = ""; //Fecha de baja
    boolean bBaj = false; //Marca de baja
    String sDesPas = ""; //Password
    Vector vAmbUsu = new Vector(); //Ambito del usuario (varios String)
    String sDesEquNot = ""; //Descripci�n del equipo notificador

    boolean bResConf = false; //Autorizaci�n para resolver conflictos

    //QQ: Nuevos Flags:
    boolean bExport = false; //Autorizaci�n para env�os a otras instituciones
    boolean bGenAlauto = false; //Autorizaci�n para generaci�n de alarmas autom�ticas
    boolean bNotifs = false; //Autorizaci�n para mantenimiento de norificadores
    boolean bConsRes = false; //Autorizaci�n para Consultas restringidas

    Enumeration enum;
    String sAmbUsu = "";
    String sEnf = "";
    String sCodApl = "";

    // Querys
    String sQueryDel = "";
    String sQueryDel1 = "";
    String sQuery = "";
    String sQuery1 = "";
    //Date para recoger y meter fechas en b.datos
    java.sql.Date dFecAlt, dFecBaj;
    boolean bBajAnt = false; //Marca de baja anterior a una modificaci�n
    java.sql.Date dFecAltAnt = null; //Fecha �ltima alta de este usuario anterior a una modificaci�n
    java.sql.Date dFecBajAnt = null; //Fecha �ltima baja de este usuario anterior a una modificaci�n

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {

      datUsuario = (DataUsuario) param.firstElement();

      // modos de operaci�n
      switch (opmode) {

        // listado selecci�n por c�digo
        case modoSELECCION_COD:

          //Borrado previo de autorizaciones que no est�n autorizados en USUARIO
          sQueryDel = "delete SIVE_AUTORIZACIONES_PISTA where CD_USUARIO not in (select COD_USUARIO from USUARIO)";
          // prepara la query
          st = con.prepareStatement(sQueryDel);

          st.executeUpdate();

          st.close();
          st = null;

          //Borrado previo de usuarios que no est�n autorizados en USUARIO
          sQueryDel1 = " delete SIVE_USUARIO_PISTA where " +
              " CD_USUARIO not in (select COD_USUARIO CD_USUARIO from USUARIO) ";

          // prepara la query
          st = con.prepareStatement(sQueryDel1);

          int u = st.executeUpdate();

          st.close();
          st = null;

          //Selecci�n de la lista de usuarios por codigo
          String sUsu = datUsuario.getCodUsu();
          if (sUsu.equals("")) {
            sQuery = "select CD_USUARIO, DS_NOMBRE, DS_DIRECCION, DS_EMAIL," +
                "DS_TELEF, DS_ANOTACIONES, IT_PERFILUSU, FC_ALTA," +
                "FC_BAJA, IT_BAJA, IT_AUTALTA, IT_AUTBAJA, IT_AUTMOD, IT_FG_VALIDAR," +
                "IT_FG_MNTO_USU, IT_FG_MNOTIFS, IT_FG_MNTO, IT_FG_PROTOS," +
                "IT_FG_ALARMAS, IT_FG_GENALAUTO, IT_FG_TCNE, IT_FG_EXPORT," +
                "IT_FG_CONSREST, IT_FG_ENFERMO, CD_E_NOTIF from SIVE_USUARIO_PISTA" +
                " order by CD_USUARIO";
          }
          else {
            // ARG: upper (8/5/02)
            sQuery = "select CD_USUARIO, DS_NOMBRE, DS_DIRECCION, DS_EMAIL," +
                "DS_TELEF, DS_ANOTACIONES, IT_PERFILUSU, FC_ALTA," +
                "FC_BAJA, IT_BAJA, IT_AUTALTA, IT_AUTBAJA, IT_AUTMOD, IT_FG_VALIDAR," +
                "IT_FG_MNTO_USU, IT_FG_MNOTIFS, IT_FG_MNTO, IT_FG_PROTOS," +
                "IT_FG_ALARMAS, IT_FG_GENALAUTO, IT_FG_TCNE, IT_FG_EXPORT," +
                "IT_FG_CONSREST, IT_FG_ENFERMO, CD_E_NOTIF from SIVE_USUARIO_PISTA" +
                " where upper(CD_USUARIO) like upper(?)" +
                " order by CD_USUARIO";
          }

          // prepara la query
          st = con.prepareStatement(sQuery);

          // prepara la lista de resultados
          data = new CLista();

          // filtros
          if (!sUsu.equals("")) {
            st.setString(1, datUsuario.getCodUsu() + "%");
          }
          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {

            // obtiene los campos
            //C�digo de usuario
            sCodUsu = rs.getString("CD_USUARIO");
            //Descripci�n del nombre
            sDesNom = rs.getString("DS_NOMBRE");
            //Direcci�n
            sDesDir = rs.getString("DS_DIRECCION");
            //Email
            sDesCor = rs.getString("DS_EMAIL");
            //Tel�fono
            sDesTel = rs.getString("DS_TELEF");
            //Anotaciones
            sDesAno = rs.getString("DS_ANOTACIONES");

            //Perfil de usuario
            sPer = rs.getString("IT_PERFILUSU");
            if (sPer == null) {
              sPer = "";
            }

            //Fecha de alta
            dFecAlt = rs.getDate("FC_ALTA");
            if (dFecAlt != null) {
              sFecAlt = Fechas.date2String(dFecAlt);
            }
            else {
              sFecAlt = "";

              //Fecha de baja
            }
            dFecBaj = rs.getDate("FC_BAJA");
            if (dFecBaj != null) {
              sFecBaj = Fechas.date2String(dFecBaj);
            }
            else {
              sFecBaj = "";

              //Baja
            }
            String sBaj = rs.getString("IT_BAJA");
            if (sBaj.equals("S")) {
              bBaj = true;
            }
            else {
              bBaj = false;
            }

            //Autorizaci�n de Alta
            String sAutAlt = rs.getString("IT_AUTALTA");
            if (sAutAlt.equals("S")) {
              bAutAlt = true;
            }
            else {
              bAutAlt = false;
            }

            //Autorizaci�n de Baja
            String sAutBaj = rs.getString("IT_AUTBAJA");
            if (sAutBaj.equals("S")) {
              bAutBaj = true;
            }
            else {
              bAutBaj = false;
            }

            //Autorizaci�n de Modificaci�n
            String sAutMod = rs.getString("IT_AUTMOD");
            if (sAutMod.equals("S")) {
              bAutMod = true;
            }
            else {
              bAutMod = false;
            }

            //Validaci�n de Autorizaciones
            String sVal = rs.getString("IT_FG_VALIDAR");
            if (sVal.equals("S")) {
              bVal = true;
            }
            else {
              bVal = false;
            }

            //Mantenimiento de usuarios
            String sMantUsu = rs.getString("IT_FG_MNTO_USU");
            if (sMantUsu.equals("S")) {
              bManUsu = true;
            }
            else {
              bManUsu = false;
            }

            //Mantenimiento de notificadores
            String sManNotifs = rs.getString("IT_FG_MNOTIFS");
            if (sManNotifs.equals("S")) {
              bNotifs = true;
            }
            else {
              bNotifs = false;
            }

            //Mantenimiento de tablas de codigo
            String sManCat = rs.getString("IT_FG_MNTO");
            if (sManCat.equals("S")) {
              bManCat = true;
            }
            else {
              bManCat = false;
            }

            //Dise�o de protocolos
            String sDefPro = rs.getString("IT_FG_PROTOS");
            if (sDefPro.equals("S")) {
              bDefPro = true;
            }
            else {
              bDefPro = false;
            }

            //Dise�o de indicadores
            String sAla = rs.getString("IT_FG_ALARMAS");
            if (sAla.equals("S")) {
              bAla = true;
            }
            else {
              bAla = false;
            }

            //Genearaci�n de alarmas autom�ticas
            String sGenAlauto = rs.getString("IT_FG_GENALAUTO");
            if (sGenAlauto.equals("S")) {
              bGenAlauto = true;
            }
            else {
              bGenAlauto = false;
            }

            //Exportaci�n de datos
            String sTraPro = rs.getString("IT_FG_TCNE");
            if (sTraPro.equals("S")) {
              bTraPro = true;
            }
            else {
              bTraPro = false;
            }

            //Env�os a otras instituciones
            String sExport = rs.getString("IT_FG_EXPORT");
            if (sExport == null) {
              sExport = "";
            }
            if (sExport.equals("S")) {
              bExport = true;
            }
            else {
              bExport = false;
            }

            //Consultas restringidas
            String sConsRes = rs.getString("IT_FG_CONSREST");
            if (sConsRes.equals("S")) {
              bConsRes = true;
            }
            else {
              bConsRes = false;
            }

            //Confidencialidad
            sEnf = rs.getString("IT_FG_ENFERMO");
            if (sEnf == null) {
              sEnf = "";
            }
            if (sEnf.equals("S")) {
              bEnf = true;
            }
            else {
              bEnf = false;

              //C�digo de notificador
            }
            sCodEquNot = rs.getString("CD_E_NOTIF");
            if (sCodEquNot == null) {
              sCodEquNot = "";
            }

            //Niveles
            getNiv(con, sCodUsu);

            vAmbUsu = getNiv(con, sCodUsu);

            // a�ade un nodo
            data.addElement(new DataUsuario(sCodUsu, sCodComAut, sCodEquNot,
                                            sDesNom, sDesApe, bAutAlt, bAutMod,
                                            bAutBaj, sPer, bEnf, bManCat,
                                            bManUsu, bTraPro, bDefPro, bAla,
                                            bVal, sDesTel, sDesDir, sDesCor,
                                            sDesAno, sFecAlt, sFecBaj, bBaj,
                                            sDesPas, vAmbUsu, sDesEquNot,
                                            bResConf, bExport, bGenAlauto,
                                            bNotifs, bConsRes, sCodApl));

          }
          rs.close();
          st.close();
          rs = null;
          st = null;

          break;

          // listado selecci�n por nombre
        case modoSELECCION_DES:

          //Borrado previo de autorizaciones que no est�n autorizados en USUARIO
          sQueryDel = "delete SIVE_AUTORIZACIONES_PISTA where CD_USUARIO not in (select COD_USUARIO CD_USUARIO from USUARIO)";
          // prepara la query
          st = con.prepareStatement(sQueryDel);

          st.executeUpdate();

          st.close();
          st = null;

          //Borrado previo de usuarios que no est�n autorizados en USUARIO
          sQueryDel1 = " delete SIVE_USUARIO_PISTA where " +
              " CD_USUARIO not in (select COD_USUARIO CD_USUARIO from USUARIO)";

          // prepara la query
          st = con.prepareStatement(sQueryDel1);

          st.executeUpdate();

          st.close();
          st = null;

          //Selecci�n de usuarios por nombre
          String sNom = datUsuario.getDesNom();
          if (sNom.equals("")) {
            sQuery = "select CD_USUARIO, DS_NOMBRE, DS_DIRECCION, DS_EMAIL," +
                "DS_TELEF, DS_ANOTACIONES, IT_PERFILUSU, FC_ALTA," +
                "FC_BAJA, IT_BAJA, IT_AUTALTA, IT_AUTBAJA, IT_AUTMOD, IT_FG_VALIDAR," +
                "IT_FG_MNTO_USU, IT_FG_MNOTIFS, IT_FG_MNTO, IT_FG_PROTOS," +
                "IT_FG_ALARMAS, IT_FG_GENALAUTO, IT_FG_TCNE, IT_FG_EXPORT," +
                "IT_FG_CONSREST, IT_FG_ENFERMO, CD_E_NOTIF from SIVE_USUARIO_PISTA" +
                " order by DS_NOMBRE";
          }
          else {
            // ARG: upper (8/5/02)
            sQuery = "select CD_USUARIO, DS_NOMBRE, DS_DIRECCION, DS_EMAIL," +
                "DS_TELEF, DS_ANOTACIONES, IT_PERFILUSU, FC_ALTA," +
                "FC_BAJA, IT_BAJA, IT_AUTALTA, IT_AUTBAJA, IT_AUTMOD, IT_FG_VALIDAR," +
                "IT_FG_MNTO_USU, IT_FG_MNOTIFS, IT_FG_MNTO, IT_FG_PROTOS," +
                "IT_FG_ALARMAS, IT_FG_GENALAUTO, IT_FG_TCNE, IT_FG_EXPORT," +
                "IT_FG_CONSREST, IT_FG_ENFERMO, CD_E_NOTIF from SIVE_USUARIO_PISTA" +
                " where upper(DS_NOMBRE) like upper(?)" +
                " order by DS_NOMBRE";
          }

          // prepara la query
          st = con.prepareStatement(sQuery);

          // prepara la lista de resultados
          data = new CLista();

          // filtros
          if (!sNom.equals("")) {
            //st.setString(1, datUsuario.getCodApl()); //Lo debe coger del applet.
            //}else{
            //st.setString(1, datUsuario.getCodApl()); //Lo debe coger del applet.
            st.setString(1, "%" + datUsuario.getDesNom() + "%");
          }
          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {

            // obtiene los campos
            //C�digo de usuario
            sCodUsu = rs.getString("CD_USUARIO");
            //Descripci�n del nombre
            sDesNom = rs.getString("DS_NOMBRE");
            //Direcci�n
            sDesDir = rs.getString("DS_DIRECCION");
            //Email
            sDesCor = rs.getString("DS_EMAIL");
            //Tel�fono
            sDesTel = rs.getString("DS_TELEF");
            //Anotaciones
            sDesAno = rs.getString("DS_ANOTACIONES");

            //Perfil de usuario
            sPer = rs.getString("IT_PERFILUSU");
            if (sPer == null) {
              sPer = "";
            }

            //Fecha de alta
            dFecAlt = rs.getDate("FC_ALTA");
            if (dFecAlt != null) {
              sFecAlt = Fechas.date2String(dFecAlt);
            }
            else {
              sFecAlt = "";

              //Fecha de baja
            }
            dFecBaj = rs.getDate("FC_BAJA");
            if (dFecBaj != null) {
              sFecBaj = Fechas.date2String(dFecBaj);
            }
            else {
              sFecBaj = "";

              //Baja
            }
            String sBaj = rs.getString("IT_BAJA");
            if (sBaj.equals("S")) {
              bBaj = true;
            }
            else {
              bBaj = false;
            }

            //Autorizaci�n de Alta
            String sAutAlt = rs.getString("IT_AUTALTA");
            if (sAutAlt.equals("S")) {
              bAutAlt = true;
            }
            else {
              bAutAlt = false;
            }

            //Autorizaci�n de Baja
            String sAutBaj = rs.getString("IT_AUTBAJA");
            if (sAutBaj.equals("S")) {
              bAutBaj = true;
            }
            else {
              bAutBaj = false;
            }

            //Autorizaci�n de Modificaci�n
            String sAutMod = rs.getString("IT_AUTMOD");
            if (sAutMod.equals("S")) {
              bAutMod = true;
            }
            else {
              bAutMod = false;
            }

            //Validaci�n de Autorizaciones
            String sVal = rs.getString("IT_FG_VALIDAR");
            if (sVal.equals("S")) {
              bVal = true;
            }
            else {
              bVal = false;
            }

            //Mantenimiento de usuarios
            String sMantUsu = rs.getString("IT_FG_MNTO_USU");
            if (sMantUsu.equals("S")) {
              bManUsu = true;
            }
            else {
              bManUsu = false;
            }

            //Mantenimiento de notificadores
            String sManNotifs = rs.getString("IT_FG_MNOTIFS");
            if (sManNotifs.equals("S")) {
              bNotifs = true;
            }
            else {
              bNotifs = false;
            }

            //Mantenimiento de tablas de codigo
            String sManCat = rs.getString("IT_FG_MNTO");
            if (sManCat.equals("S")) {
              bManCat = true;
            }
            else {
              bManCat = false;
            }

            //Dise�o de protocolos
            String sDefPro = rs.getString("IT_FG_PROTOS");
            if (sDefPro.equals("S")) {
              bDefPro = true;
            }
            else {
              bDefPro = false;
            }

            //Dise�o de indicadores
            String sAla = rs.getString("IT_FG_ALARMAS");
            if (sAla.equals("S")) {
              bAla = true;
            }
            else {
              bAla = false;
            }

            //Genearaci�n de alarmas autom�ticas
            String sGenAlauto = rs.getString("IT_FG_GENALAUTO");
            if (sGenAlauto.equals("S")) {
              bGenAlauto = true;
            }
            else {
              bGenAlauto = false;
            }

            //Exportaci�n de datos
            String sTraPro = rs.getString("IT_FG_TCNE");
            if (sTraPro.equals("S")) {
              bTraPro = true;
            }
            else {
              bTraPro = false;
            }

            //Env�os a otras instituciones
            String sExport = rs.getString("IT_FG_EXPORT");
            if (sExport == null) {
              sExport = "";
            }
            if (sExport.equals("S")) {
              bExport = true;
            }
            else {
              bExport = false;
            }

            //Consultas restringidas
            String sConsRes = rs.getString("IT_FG_CONSREST");
            if (sConsRes.equals("S")) {
              bConsRes = true;
            }
            else {
              bConsRes = false;
            }

            //Confidencialidad
            sEnf = rs.getString("IT_FG_ENFERMO");
            if (sEnf == null) {
              sEnf = "";
            }
            if (sEnf.equals("S")) {
              bEnf = true;
            }
            else {
              bEnf = false;

              //C�digo de notificador
            }
            sCodEquNot = rs.getString("CD_E_NOTIF");
            if (sCodEquNot == null) {
              sCodEquNot = "";
            }

            //Niveles
            getNiv(con, sCodUsu);

            vAmbUsu = getNiv(con, sCodUsu);

            //a�ade un nodo
            data.addElement(new DataUsuario(sCodUsu, sCodComAut, sCodEquNot,
                                            sDesNom, sDesApe, bAutAlt, bAutMod,
                                            bAutBaj, sPer, bEnf, bManCat,
                                            bManUsu, bTraPro, bDefPro, bAla,
                                            bVal, sDesTel, sDesDir, sDesCor,
                                            sDesAno, sFecAlt, sFecBaj, bBaj,
                                            sDesPas, vAmbUsu, sDesEquNot,
                                            bResConf, bExport, bGenAlauto,
                                            bNotifs, bConsRes, sCodApl));

          }
          rs.close();
          st.close();
          rs = null;
          st = null;

          break;

        case modoMOD:

          //B�squeda de marca de baja
          sQuery =
              "select FC_ALTA, FC_BAJA, IT_BAJA from SIVE_USUARIO_PISTA where CD_USUARIO = ?";
          //prepara la query
          st = con.prepareStatement(sQuery);
          // filtros
          st.setString(1, datUsuario.getCodUsu());
          //st.setString(2, datUsuario.getCodApl()); //Lo debe coger del applet.

          rs = st.executeQuery();

          // obtiene los campos
          while (rs.next()) {
            //Marca de baja
            String stBajAnt;
            stBajAnt = rs.getString("IT_BAJA");
            if (stBajAnt.equals("S")) {
              bBajAnt = true;
            }
            else {
              bBajAnt = false;
              //Fecha �ltima alta
            }
            dFecAltAnt = rs.getDate("FC_ALTA");
            //Fecha �ltima baja
            dFecBajAnt = rs.getDate("FC_BAJA");
          }

          rs.close();
          st.close();
          rs = null;
          st = null;

          //modifica el registro existente
          sQuery1 = "update SIVE_USUARIO_PISTA SET CD_E_NOTIF = ?, DS_DIRECCION = ?, DS_EMAIL = ?, DS_TELEF = ?," +
              " DS_ANOTACIONES = ?, IT_PERFILUSU = ?," +
              " IT_AUTALTA = ?, IT_AUTBAJA = ?, IT_AUTMOD = ?, IT_FG_VALIDAR = ?," +
              " IT_FG_MNTO_USU = ?, IT_FG_MNOTIFS = ?, IT_FG_MNTO = ?, IT_FG_PROTOS = ?," +
              " IT_FG_ALARMAS = ?, IT_FG_GENALAUTO = ?, IT_FG_TCNE = ?, FC_ALTA = ?, FC_BAJA = ?, IT_BAJA = ?, IT_FG_EXPORT = ?," +
              " IT_FG_CONSREST = ?, IT_FG_ENFERMO = ? where CD_USUARIO = ? and CD_CA = ?";

          //prepara la query
          st = con.prepareStatement(sQuery1);

          //filtros
          //C�digo de equipo notificador
          sCodEquNot = datUsuario.getCodEquNot();
          if (sCodEquNot.trim().length() > 0) {
            st.setString(1, sCodEquNot);
          }
          else {
            st.setNull(1, java.sql.Types.VARCHAR);
          }

          //Direcci�n
          sDesDir = datUsuario.getDesDir();
          if (sDesDir.trim().length() > 0) {
            st.setString(2, sDesDir);
          }
          else {
            st.setNull(2, java.sql.Types.VARCHAR);
          }

          //Email
          sDesCor = datUsuario.getDesCor();
          if (sDesCor.trim().length() > 0) {
            st.setString(3, sDesCor);
          }
          else {
            st.setNull(3, java.sql.Types.VARCHAR);
          }

          //Tel�fono
          sDesTel = datUsuario.getDesTel();
          if (sDesTel.trim().length() > 0) {
            st.setString(4, sDesTel);
          }
          else {
            st.setNull(4, java.sql.Types.VARCHAR);
          }

          //Anotaciones
          sDesAno = datUsuario.getDesAno();
          if (sDesAno.trim().length() > 0) {
            st.setString(5, sDesAno);
          }
          else {
            st.setNull(5, java.sql.Types.VARCHAR);
          }

          //Perfil de usuario
          sPer = datUsuario.getPer();
          if (sPer.trim().length() > 0) {
            st.setString(6, datUsuario.getPer());
          }
          else {
            st.setNull(6, java.sql.Types.VARCHAR);
          }

          //Autorizaci�n altas
          if (datUsuario.getAutAlt() == true) {
            st.setString(7, "S");
          }
          else {
            st.setString(7, "N");
          }

          //Autorizaci�n bajas
          if (datUsuario.getAutBaj() == true) {
            st.setString(8, "S");
          }
          else {
            st.setString(8, "N");
          }

          //Autorizaci�n modificaciones
          if (datUsuario.getAutMod() == true) {
            st.setString(9, "S");
          }
          else {
            st.setString(9, "N");
          }

          //Autor. validaci�n de notificaciones
          if (datUsuario.getVal() == true) {
            st.setString(10, "S");
          }
          else {
            st.setString(10, "N");
          }

          //Autor. mat. usuarios
          if (datUsuario.getManUsu() == true) {
            st.setString(11, "S");
          }
          else {
            st.setString(11, "N");
          }

          //Autor. mant. notificaciones
          if (datUsuario.getManNotifs() == true) {
            st.setString(12, "S");
          }
          else {
            st.setString(12, "N");
          }

          //Autor mant. cat�logos
          if (datUsuario.getManCat() == true) {
            st.setString(13, "S");
          }
          else {
            st.setString(13, "N");
          }

          //Autor. def. protocolos
          if (datUsuario.getDefPro() == true) {
            st.setString(14, "S");
          }
          else {
            st.setString(14, "N");
          }

          //Aut. definiciones de alarmas
          if (datUsuario.getAla() == true) {
            st.setString(15, "S");
          }
          else {
            st.setString(15, "N");
          }

          //Aut. generacion de alarmas autom�ticas
          if (datUsuario.getGenAlauto() == true) {
            st.setString(16, "S");
          }
          else {
            st.setString(16, "N");
          }

          //Autor. transmisi�n protocolos
          if (datUsuario.getTraPro() == true) {
            st.setString(17, "S");
          }
          else {
            st.setString(17, "N");
          }

          if ( (bBajAnt == true) && (datUsuario.getBaj() == false)) { //Pasa a alta
            //Fecha de alta (fecha actual)

            st.setDate(18, new java.sql.Date(new java.util.Date().getTime()));
            //Fecha de baja ,la que habia

            if (dFecBajAnt != null) {
              st.setDate(19, dFecBajAnt); //???????????????????    VEr que pasa
            }
            else {
              st.setNull(19, java.sql.Types.VARCHAR);
              //Marca de baja a N

            }
            st.setString(20, "N");
          }
          else if ( (bBajAnt == false) && (datUsuario.getBaj() == true)) { //Pasa a baja
            //Fecha de alta (la que habia)
            if (dFecAltAnt != null) {
              st.setDate(18, dFecAltAnt); //???????????????????    VEr que pasa
            }
            else {
              st.setNull(18, java.sql.Types.VARCHAR);
              //Fecha de baja ,la actual
            }
            st.setDate(19, new java.sql.Date(new java.util.Date().getTime()));
            //Marca de baja a S
            st.setString(20, "S");
          }
          else { //No hay cambios
            //Fecha de alta (la que habia)
            if (dFecAltAnt != null) {
              st.setDate(18, dFecAltAnt); //???????????????????    VEr que pasa
            }
            else {
              st.setNull(18, java.sql.Types.VARCHAR);
              //Fecha de baja ,la que habia
            }
            if (dFecBajAnt != null) {
              st.setDate(19, dFecBajAnt); //???????????????????    VEr que pasa
            }
            else {
              st.setNull(19, java.sql.Types.VARCHAR);

              //Marca de baja (la que hab�a)
            }
            if (bBajAnt == true) {
              st.setString(20, "S");
            }
            else {
              st.setString(20, "N");
            }
          }

          //Autor. Exportacion de datos
          if (datUsuario.getExport() == true) {
            st.setString(21, "S");
          }
          else {
            st.setString(21, "N");
          }

          //Autor. Consultas Restringidas
          if (datUsuario.getConsRes() == true) {
            st.setString(22, "S");
          }
          else {
            st.setString(22, "N");
          }

          //Autor. ver datos enfermo
          if (datUsuario.getEnf() == true) {
            st.setString(23, "S");
          }
          else {
            st.setString(23, "N");
          }

          st.setString(24, datUsuario.getCodUsu());
          //st.setString(25, datUsuario.getCodApl());
          st.setString(25, datUsuario.getCodComAut());

          rs = st.executeQuery();

          rs.close();
          st.close();
          rs = null;
          st = null;

          vAmbUsu = datUsuario.getAmbUsu();
          //if (vAmbUsu.size()>0){
          //borra y actualiza
          ActualizarAmbitos(con, vAmbUsu, datUsuario);
          //}
          break;

        case modoALTA:

          //hace el insert
          sQuery1 = "insert into SIVE_USUARIO_PISTA (CD_USUARIO, CD_CA, CD_E_NOTIF, DS_NOMBRE, DS_APELLIDOS, IT_AUTALTA, IT_AUTMOD,IT_AUTBAJA, IT_PERFILUSU, IT_FG_ENFERMO, IT_FG_MNTO, IT_FG_MNTO_USU, IT_FG_TCNE, IT_FG_PROTOS, IT_FG_ALARMAS, IT_FG_VALIDAR, DS_TELEF, DS_DIRECCION, DS_EMAIL, DS_ANOTACIONES, FC_ALTA, FC_BAJA,IT_BAJA, DS_PASSWORD, IT_FG_EXPORT, IT_FG_GENALAUTO, IT_FG_MNOTIFS, IT_FG_CONSREST) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

          // prepara la query
          st = con.prepareStatement(sQuery1);

          // filtros
          // cod usario
          st.setString(1, datUsuario.getCodUsu().trim());
          // cod Com Autonoma
          st.setString(2, datUsuario.getCodComAut().trim());
          // eq notificador
          if (datUsuario.getCodEquNot().trim().length() > 0) {
            st.setString(3, datUsuario.getCodEquNot().trim());
          }
          else {
            st.setNull(3, java.sql.Types.VARCHAR);
            //Nombre
          }
          st.setString(4, datUsuario.getDesNom().trim());
          //Apellidos
          st.setString(5, " ");
          /*
                          if (datUsuario.getDesApe().trim().length() > 0)
             st.setString(5,datUsuario.getDesApe().trim());
                          else
             st.setNull(5, java.sql.Types.VARCHAR);
           */
          //Autorizaci�n altas
          if (datUsuario.getAutAlt() == true) {
            st.setString(6, "S");
          }
          else {
            st.setString(6, "N");
            //Autorizaci�n modificaciones
          }
          if (datUsuario.getAutMod() == true) {
            st.setString(7, "S");
          }
          else {
            st.setString(7, "N");
            //Autorizaci�n bajas
          }
          if (datUsuario.getAutBaj() == true) {
            st.setString(8, "S");
          }
          else {
            st.setString(8, "N");
            //Perfil
          }
          if (datUsuario.getPer().trim().length() > 0) {
            st.setString(9, datUsuario.getPer().trim());
          }
          else {
            st.setNull(9, java.sql.Types.VARCHAR);
            //Autor. ver datos enfermo
          }
          if (datUsuario.getEnf() == true) {
            st.setString(10, "S");
          }
          else {
            st.setString(10, "N");
            //Autor mant. cat�logos
          }
          if (datUsuario.getManCat() == true) {
            st.setString(11, "S");
          }
          else {
            st.setString(11, "N");
            //Autor. mat. usuarios
          }
          if (datUsuario.getManUsu() == true) {
            st.setString(12, "S");
          }
          else {
            st.setString(12, "N");
            //Autor. transmisi�n protocolos
          }
          if (datUsuario.getTraPro() == true) {
            st.setString(13, "S");
          }
          else {
            st.setString(13, "N");
            //Autor. def. protocolos
          }
          if (datUsuario.getDefPro() == true) {
            st.setString(14, "S");
          }
          else {
            st.setString(14, "N");
            //Aut. definiciones de alarmas
          }
          if (datUsuario.getAla() == true) {
            st.setString(15, "S");
          }
          else {
            st.setString(15, "N");
            //Autor. validaci�n de notificaciones
          }
          if (datUsuario.getVal() == true) {
            st.setString(16, "S");
          }
          else {
            st.setString(16, "N");
            //Tel�fono
          }
          if (datUsuario.getDesTel().trim().length() > 0) {
            st.setString(17, datUsuario.getDesTel().trim());
          }
          else {
            st.setNull(17, java.sql.Types.VARCHAR);
            //Direcci�n
          }
          if (datUsuario.getDesDir().trim().length() > 0) {
            st.setString(18, datUsuario.getDesDir().trim());
          }
          else {
            st.setNull(18, java.sql.Types.VARCHAR);
            //Correo electr�nico
          }
          if (datUsuario.getDesCor().trim().length() > 0) {
            st.setString(19, datUsuario.getDesCor().trim());
          }
          else {
            st.setNull(19, java.sql.Types.VARCHAR);
            //Anotaciones
          }
          if (datUsuario.getDesAno().trim().length() > 0) {
            st.setString(20, datUsuario.getDesAno().trim());
          }
          else {
            st.setNull(20, java.sql.Types.VARCHAR);

            //Marca de baja como usuario
            //En fechas se pone fech actual en las dos

          }
          if (datUsuario.getBaj() == false) { //No est� como baja
            //Fecha de alta (fecha actual)
            st.setDate(21, new java.sql.Date(new java.util.Date().getTime()));
            //Fecha de baja   (null)
            st.setNull(22, java.sql.Types.VARCHAR);
            //Marca de baja a false
            st.setString(23, "N");
          }

          else {
            //Fecha de alta (null)
            //Fecha de baja ,(fecha actual)
            st.setNull(21, java.sql.Types.VARCHAR);
            st.setDate(22, new java.sql.Date(new java.util.Date().getTime()));
            //Marca de baja a false
            st.setString(23, "S");
          }

          // password
          st.setString(24, " ");

          //***
           //QQ: Nuevos Flags de Autorizaciones
          if (datUsuario.getExport() == true) {
            st.setString(25, "S");
          }
          else {
            st.setString(25, "N");

          }
          if (datUsuario.getGenAlauto() == true) {
            st.setString(26, "S");
          }
          else {
            st.setString(26, "N");

          }
          if (datUsuario.getManNotifs() == true) {
            st.setString(27, "S");
          }
          else {
            st.setString(27, "N");

          }
          if (datUsuario.getConsRes() == true) {
            st.setString(28, "S");
          }
          else {
            st.setString(28, "N");

          }

          rs = st.executeQuery();

          rs.close();
          st.close();
          rs = null;
          st = null;

          //borra y actualiza autorizaciones
          vAmbUsu = datUsuario.getAmbUsu();
          //if (vAmbUsu.size()>0){
          ActualizarAmbitos(con, vAmbUsu, datUsuario);
          //}
          break;

      } //fin switch

      // Validacion de la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      data = null;
      throw ex;
    }
    finally {
      closeConnection(con);
    }

    // Cierre de la conexion con la BD
    closeConnection(con);

    return data;
  } // Fin doWork()

  Vector getNiv(Connection c, String CodUsu) throws Exception {
    //if(CodUsu == null || CodUsu.equals("")) return "";
    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;
    Vector vUsu = null;
    // Query
    String Query = "select CD_NIVEL_1,CD_NIVEL_2 from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?";

    try {
      // Preparacion de la sentencia SQL
      st = c.prepareStatement(Query);
      st.setString(1, CodUsu.trim());

      rs = st.executeQuery();

      // extrae la p�gina requerida
      String sCodNiv1, sCodNiv2;
      //DataUsuario datUsuario = new DataUsuario();
      vUsu = new Vector();
      while (rs.next()) {
        // obtiene los campos
        sCodNiv1 = rs.getString("CD_NIVEL_1");
        sCodNiv2 = rs.getString("CD_NIVEL_2");
        // a�ade un nodo
        vUsu.addElement(sCodNiv1);
        vUsu.addElement(sCodNiv2);
      }

      rs.close();
      st.close();
      st = null;
      rs = null;
    }
    catch (Exception ex) {
      throw ex;
    }
    return vUsu;
  }

  void ActualizarAmbitos(Connection c, Vector AmbUsu, DataUsuario datUsuarioA) throws
      Exception {
    //DataUsuario datUsuario = new DataUsuario();
    //Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    Vector vAmbUsu = new Vector();
    c.setAutoCommit(false);
    //BORRADO PREVIOD DE SIVE_AUTORIZACIONES_PISTA
    String sQueryDel =
        "delete from SIVE_AUTORIZACIONES_PISTA where CD_USUARIO = ?";
        /*and COD_APLICACION = ?";*/

    try {
      // prepara la query
      st = c.prepareStatement(sQueryDel);

      // filtros
      st.setString(1, datUsuarioA.getCodUsu());
      //st.setString(2, datUsuarioA.getCodApl());

      st.executeUpdate();

      st.close();
      st = null;

      if (AmbUsu != null) {
        //Nuevo item en tabla SIVE_AUTORIZACIONES_PISTA
        String sQueryA = "insert into SIVE_AUTORIZACIONES_PISTA(CD_CA, CD_NIVEL_1, CD_NIVEL_2, CD_SECUENCIAL, CD_USUARIO) values( ?, ?, ?, ?, ?)";

        //Recorremos el vector de ambitos e insertamos una linea por cada ambito en tabla autorizaciones
        Enumeration enum;
        vAmbUsu = AmbUsu;
        //vAmbUsu= datUsuario.getAmbUsu();
        String ambito = null;
        String nivel2 = null;

        if (vAmbUsu != null) {
          enum = vAmbUsu.elements();
          int z = 0;
          while (enum.hasMoreElements()) {
            ambito = (String) (enum.nextElement()); // nivel1
            nivel2 = (String) (enum.nextElement()); // nivel2

            // prepara la query
            st = c.prepareStatement(sQueryA);

            // cod Com Aut�noma
            st.setString(1, datUsuarioA.getCodComAut().trim());

            //Codigo en perfil para CD_NIVEL_1 CD_NIVEL_2*********************
            if (datUsuarioA.getPer().equals("3")) {
              st.setString(2, ambito.trim());
              st.setNull(3, java.sql.Types.VARCHAR);
            }
            else if (datUsuarioA.getPer().equals("4")) {
              st.setString(2, ambito.trim());
              st.setString(3, nivel2.trim());
            }

            //Cod secuencial
            z++;
            st.setInt(4, z); //????????????????????????????????????????Secuencial*******
            //Cod usuario
            st.setString(5, datUsuarioA.getCodUsu().trim());
            //Cod Aolicacion
            //st.setString(6,datUsuarioA.getCodApl().trim());

            // lanza la query
            st.executeUpdate();

            st.close();
            st = null;

          }
        }
      } //fin if
    }
    catch (Exception ex) {
      throw ex;
    }

  }

  static public void main(String args[]) {

    SrvSelUsu srv = new SrvSelUsu();
    CLista listaSalida = new CLista();
    DataUsuario hs = new DataUsuario();
    //DataUsuario hs = new DataUsuario(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
    CLista resultado = null;

    // par�metros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");

    //hs.setCodUsu("");

    hs.setCodUsu("CA_1");
    //hs.setDesNom("Pedro Antonio Diaz");  //si entro por nombre
    //hs.setPer("2");
    //hs.setEnf(true);

    hs.setCodApl("RTBC");

    listaSalida.addElement(hs);
    resultado = srv.doDebug(constantes.sSELECCION_X_CODIGO, listaSalida);
    resultado = null;

  }

} // Fin SrvSelUsu