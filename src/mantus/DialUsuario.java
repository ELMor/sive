/**
 * Di�logo a trav�s del cu�l se insertan/modifican/ datos
 * en tablas de mantenimiento de usuarios.
 * @ADAPTACION DEL ANTIGUO MANTUS-PISTA AL UTILIZADO EN ICM99 PDP 14/04/2000
 * @version 1.0
 */
package mantus;

import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import capp2.CApp;
import capp2.CDialog;
import capp2.CMessage;
import capp2.CTabPanel;
import comun.constantes;

//import comun.comun;

public class DialUsuario
    extends CDialog {

  protected CTabPanel tabPanel = new CTabPanel();
  ResourceBundle res = ResourceBundle.getBundle("mantus.Res" + app.getIdioma());

//modos de pantalla y consultas
  public static final int modoALTA = 0;
  public static final int modoMODIFICAR = 1;
  public static final int modoSELECCION = 2; //Usado s�lo como modo oper. con sevlett
  public static final int modoOBTENER = 4;
  public static final int modoESPERA = 5; // Usado solo como modo pantalla
  public static final int modoSELECCION_EQU_NOT = 6; // Usado solo como modo pantalla
  //El opmode de consulta ser� el correspondiente al servlett que consulta el eq not y variar� la URL: Va al url del eq not)

  final int modoGENERAR_LOGIN = 8;

  PsApMantUsu PsAppDlg = null;

  // modos de operaci�n de la ventana
  final static public int MODIFICACION = 1;

  // modo de apertura de la ventana
  public int modoApertura;

  public sapp2.Data dtSel = null;

  // control aceptar/cancelar
  private boolean bAceptar = false;

  protected int modoOperacionBk; //Para volver al modo anterior tras selecci�n c�digo CIE
  protected int modoOperacion = modoALTA;

  // constantes del panel para rutas
  final String imgLUPA = "images/Magnify.gif";

  private final String strSERVLET_SRVMNTUSU = "servlet/SrvSelUsu";

  // par�metros
  protected capp.CLista listaUsuario = null;
  protected capp.CLista listaLogin = null; //Para el login de un usuario
  protected capp.CLista listaNivel1 = null;
  protected capp.CLista listaNivel2 = null;

  protected capp.CLista listaNiv1 = null; //Lista de c�digos nivel 1
  protected capp.CLista listaNiv2 = null; //Lista de c�digos nivel 2
  protected capp.CLista listaNiv = null; //Lista de c�digos nivel 2
  protected String strCode = null; // es el nombre del campo code de la lista seleccionada
  public Vector vAmbUsu = new Vector(); //Strings elegidos del ambito
  protected Vector vAmbUsuSel = new Vector(); //Strings elegidos del ambito

  /** indica el modo en que se va a ejecutar */
  protected int m_opcion = -1;

  protected sapp.StubSrvBD stubCliente = null;

  protected DataUsuario usuSeleccionado = null;
  public DataUsuario datoA�adido = null;

  escuchador escuchador1 = new escuchador(this);

  public Panel1 tPanel1;
  public Panel2 tPanel2;
  public Panel3 panelbot;
  public PanelBusca panBus;

  //public CMessage mensaje;

  public DialUsuario(CApp a,
                     int modo,
                     sapp2.Data dtReg, Vector AmbUsuSel, PsApMantUsu PsApp) {
    super(a);

    vAmbUsuSel = AmbUsuSel;

    dtSel = dtReg;

    PsAppDlg = PsApp;

    try {

      modoApertura = modo;
      if (modoApertura == modoALTA) {
        setTitle("Alta de un nuevo Usuario");
      }
      else if (modoApertura == modoMODIFICAR) {
        setTitle("Modificaci�n del Usuario");
      }

      listaUsuario = new capp.CLista();
      listaUsuario.setState(capp.CLista.listaNOVALIDA);
      //stubCliente = new sapp.StubSrvBD();
      //stubCliente.setUrl(new java.net.URL(app.getParametro("URL_SERVLET") + strSERVLET_SRVMNTUSU) );

      this.panBus = panBus;

      jbInit(); //Dejarlo aqui, pues usa el stubCliente y algunas listas

      /*
             if (dtSel.getString("COD_USUARIO") != null){
          tPanel1.txtCodUsu.setText(dtSel.getString("COD_USUARIO"));
          tPanel1.txtCodUsu_actionPerformed(null);
             } */

      rellenarDatosUsuSeleccionado();

    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

  public void jbInit() throws Exception {
    // fijamos las dimensiones del di�logo
    setSize(654, 500);

    this.setLayout(new BorderLayout());
    add("Center", tabPanel);

    panelbot = new Panel3(app, this);
    add("South", panelbot);
    tPanel1 = new Panel1(app, this, PsAppDlg);
    tPanel2 = new Panel2(app, this);

    tabPanel.InsertarPanel(res.getString("msg2.Text"), tPanel1);
    tabPanel.InsertarPanel(res.getString("msg3.Text"), tPanel2);
    tabPanel.validate();
    tabPanel.VerPanel(res.getString("msg2.Text"));

    //Se activa el escuchador...
    panelbot.btA�adir.addActionListener(escuchador1);
    panelbot.btModificar.addActionListener(escuchador1);

    //this.modoOperacion= modoALTA; PDP 27/04/2000
    this.modoOperacion = modoApertura;
    Inicializar();
  }

  public void Inicializar() {

//No invalido lista aqui porque llamo a Inicializa tambien al elegir perfil!!!!!

    panelbot.modoOperacion = this.modoOperacion;
    panelbot.Inicializar();
    tPanel1.modoOperacion = this.modoOperacion;
    tPanel1.Inicializar();
    tPanel2.modoOperacion = this.modoOperacion;
    tPanel2.Inicializar();

  }

  protected boolean realiza_actionPerformed(ActionEvent evt) {
    //modoApertura = modo;
    if (modoApertura == modoALTA) {
      return btAnadir_actionPerformed(evt);
    }
    else if (modoApertura == modoMODIFICAR) {
      return btModificar_actionPerformed(evt);
    }
    else {
      return false;
    }
  }

  // procesa opci�n a�adir
  protected boolean btAnadir_actionPerformed(ActionEvent evt) {
    CMessage msgBox = null;
    capp.CLista data = null;
    boolean result = false;

    // modo espera
    this.modoOperacion = modoESPERA;
    Inicializar();
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    tPanel1.setCursor(new Cursor(Cursor.WAIT_CURSOR));

    // determina si los datos est�n completos
    if (this.isDataValid()) {

      try {

        tPanel1.ponerAmbitosEnVector();

        // prepara los par�metros
        data = new capp.CLista();

        DataUsuario datUsu = new DataUsuario(tPanel1.txtCodUsu.getText().
                                             toUpperCase(),
                                             app.getParametro("CA"),
                                             tPanel1.txtCodEquNot.getText().
                                             toUpperCase(),
                                             tPanel1.txtDesNom.getText(),
                                             "ll", //tPanel1.txtDesApe.getText(),
                                             tPanel2.chkbxAutAlt.getState(),
                                             tPanel2.chkbxAutMod.getState(),
                                             tPanel2.chkbxAutBaj.getState(),
                                             Integer.toString(tPanel1.choicePer.
            getSelectedIndex()),
                                             tPanel2.chkbxEnf.getState(),
                                             tPanel2.chkbxManCat.getState(),
                                             tPanel2.chkbxManUsu.getState(),
                                             tPanel2.chkbxTraPro.getState(),
                                             tPanel2.chkbxDefPro.getState(),
                                             tPanel2.chkbxAla.getState(),
                                             tPanel2.chkbxVal.getState(),
                                             tPanel1.txtDesTel.getText(),
                                             tPanel1.txtDesDir.getText(),
                                             tPanel1.txtDesCor.getText(),
                                             tPanel1.txtDesAno.getText(),
                                             "", //el servlet pone la fecha de alta
                                             "",
                                             tPanel1.chkbxBaj.getState(),
                                             tPanel1.txtDesPas.getText(),
                                             tPanel1.vAmbUsu,
                                             //QQ:Nuevos Flags
                                             tPanel1.txtDesEquNot.getText(), //DESCRIPCION DE EQUIPO NOTIF
                                             false, //RESULOCION DE CONFLICTOS
                                             tPanel2.chkbxExport.getState(),
                                             tPanel2.chkbxGenAlauto.getState(),
                                             tPanel2.chkbxManNotifs.getState(),
                                             tPanel2.chkbxConsRes.getState(),
                                             app.getParametro("COD_APLICACION"));

        data.addElement(datUsu);

        // A�ade un usuario nuevo
        // apunta al servlet principal
        stubCliente = new sapp.StubSrvBD();
        stubCliente.setUrl(new java.net.URL(app.getParametro("URL_SERVLET") +
                                            strSERVLET_SRVMNTUSU));
        this.stubCliente.doPost(constantes.modoALTA, data);

        /*
                  mantus.SrvSelUsu servlet = new mantus.SrvSelUsu();
                  //Indica como conectarse a la b.datos
                  servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                  servlet.doDebug(constantes.modoALTA, data); */

        tPanel2.borra();
        tPanel1.borra();

        datoA�adido = datUsu;
        // loa �adimso en la lista del panel base
        //CLista listaUsuario = panBus.getListaUsuario();
        //listaUsuario.addElement(datUsu);

        result = true;
        // error en el proceso
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
    // modo alta
    listaUsuario.setState(capp.CLista.listaNOVALIDA);
    this.modoOperacion = modoALTA;
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    tPanel1.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    Inicializar();
    return result;
  }

  // procesa opci�n modificar
  protected boolean btModificar_actionPerformed(ActionEvent evt) {
    CMessage msgBox = null;
    capp.CLista data = null;
    boolean result = false;

    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    tPanel1.setCursor(new Cursor(Cursor.WAIT_CURSOR));
    if (this.isDataValid()) {

      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        tPanel1.ponerAmbitosEnVector();

        // prepara los par�metros de env�o
        data = new capp.CLista();
        datoA�adido = new DataUsuario(tPanel1.txtCodUsu.getText().toUpperCase(),
                                      app.getParametro("CA").toUpperCase(),
                                      tPanel1.txtCodEquNot.getText().
                                      toUpperCase(),
                                      tPanel1.txtDesNom.getText(),
                                      tPanel1.txtDesApe.getText(),
                                      tPanel2.chkbxAutAlt.getState(),
                                      tPanel2.chkbxAutMod.getState(),
                                      tPanel2.chkbxAutBaj.getState(),
                                      Integer.toString(tPanel1.choicePer.
            getSelectedIndex()),
                                      tPanel2.chkbxEnf.getState(),
                                      tPanel2.chkbxManCat.getState(),
                                      tPanel2.chkbxManUsu.getState(),
                                      tPanel2.chkbxTraPro.getState(),
                                      tPanel2.chkbxDefPro.getState(),
                                      tPanel2.chkbxAla.getState(),
                                      tPanel2.chkbxVal.getState(),
                                      tPanel1.txtDesTel.getText(),
                                      tPanel1.txtDesDir.getText(),
                                      tPanel1.txtDesCor.getText(),
                                      tPanel1.txtDesAno.getText(),
                                      "", "",
                                      tPanel1.chkbxBaj.getState(),
                                      tPanel1.txtDesPas.getText(),
                                      tPanel1.vAmbUsu,
                                      "",
                                      false,
                                      //QQ:Nuevos Flags
                                      tPanel2.chkbxExport.getState(),
                                      tPanel2.chkbxGenAlauto.getState(),
                                      tPanel2.chkbxManNotifs.getState(),
                                      tPanel2.chkbxConsRes.getState(),
                                      app.getParametro("COD_APLICACION"));
        data.addElement(datoA�adido);

        // Modificamos los datos del usuario
        // apunta al servlet principal
        stubCliente = new sapp.StubSrvBD();
        stubCliente.setUrl(new java.net.URL(app.getParametro("URL_SERVLET") +
                                            strSERVLET_SRVMNTUSU));
        this.stubCliente.doPost(constantes.modoMODIFICACION, data);

        /*
                 mantus.SrvSelUsu servlet = new mantus.SrvSelUsu();
                 //Indica como conectarse a la b.datos
                 servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                  "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                  "sive_desa",
                                  "sive_desa");
                 servlet.doDebug(constantes.modoMODIFICACION, data);*/

        // establece listas no v�lidas
        listaUsuario.setState(capp.CLista.listaNOVALIDA);
        tPanel1.listaEquNot.setState(capp.CLista.listaNOVALIDA);

        result = true;
        bAceptar = true;
        // error en el proceso
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
    // modo modificar
    this.modoOperacion = modoMODIFICAR;
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    tPanel1.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    Inicializar();

    return result;
  }

  public boolean bAceptar() {
    return bAceptar;
  }

  // comprueba que los datos sean v�lidos al a�adir o modificar
  protected boolean isDataValid() {
    CMessage msgBox;

    boolean b = false;

    // comprueba que esten informados los campos obligatorios
    if ( (tPanel1.isDataValid())) {
      b = true;

      // advertencia de requerir datos
    }

    return b;
  }

  void rellenarDatosUsuSeleccionado() {
    CMessage msgBox;
    try {
      if (dtSel != null) {
        //rellenamos datos panel principal
        if (modoApertura == modoALTA) {
          tPanel1.txtCodUsu.setText(dtSel.getString("COD_USUARIO"));
          tPanel1.txtDesNom.setText(dtSel.getString("NOMBRE"));
          //tPanel1.txtDesPas.setText(dtSel.getString("PASSWORD"));
        }
        else {
          tPanel1.txtCodUsu.setText(dtSel.getString("COD_USUARIO"));
          tPanel1.txtDesNom.setText(dtSel.getString("DS_NOMBRE"));
        }

        tPanel1.txtDesDir.setText(dtSel.getString("DS_DIRECCION"));
        tPanel1.txtDesTel.setText(dtSel.getString("DS_TELEF"));
        tPanel1.txtDesCor.setText(dtSel.getString("DS_EMAIL"));
        tPanel1.txtDesAno.setText(dtSel.getString("DS_ANOTACIONES"));

        String perfilSel = dtSel.getString("IT_PERFIL_USU"); //??????????? Quiza innecesario Guardamos caracter "0" si no hay perfil
        if (perfilSel.equals("")) {
          tPanel1.choicePer.select(0); //No hab�a perfil seleccionado
        }
        else {
          tPanel1.choicePer.select(Integer.parseInt(perfilSel)); //???????????
        }

        tPanel1.trasSeleccionarPerfil(dtSel.getString("IT_PERFIL_USU"));

        tPanel1.vAmbUsu = vAmbUsuSel; //???????????
        if (tPanel1.vAmbUsu != null) {
          tPanel1.seleccionarAmbitos();
        }

        tPanel1.txtCodEquNot.setText(dtSel.getString("CD_E_NOTIF"));
        tPanel1.txtBuscarEquNot_actionPerformed(null);
        //tPanel1.txtDesEquNot.setText(dtSel.getString("DS_E_NOTIF"));   //(dtSel.getString("DS_E_NOTIF"));
        String sBaj = dtSel.getString("IT_BAJA");
        if (sBaj.equals("S")) {
          tPanel1.chkbxBaj.setState(true);
        }
        else {
          tPanel1.chkbxBaj.setState(false);
        }
        tPanel1.txtFecAlt.setText(dtSel.getString("FC_ALTA"));
        tPanel1.txtFecBaj.setText(dtSel.getString("FC_BAJA"));

        //rellenamos datos panel autorizaciones
        String sAla = dtSel.getString("IT_FG_ALARMAS");
        if (sAla.equals("S")) {
          tPanel2.chkbxAla.setState(true);
        }
        else {
          tPanel2.chkbxAla.setState(false);
        }
        String sAutAlt = dtSel.getString("IT_AUTALTA");
        if (sAutAlt.equals("S")) {
          tPanel2.chkbxAutAlt.setState(true);
        }
        else {
          tPanel2.chkbxAutAlt.setState(false);
        }
        String sAutBaj = dtSel.getString("IT_AUTBAJA");
        if (sAutBaj.equals("S")) {
          tPanel2.chkbxAutBaj.setState(true);
        }
        else {
          tPanel2.chkbxAutBaj.setState(false);
        }
        String sDefPro = dtSel.getString("IT_FG_PROTOS");
        if (sDefPro.equals("S")) {
          tPanel2.chkbxDefPro.setState(true);
        }
        else {
          tPanel2.chkbxDefPro.setState(false);
        }
        String sManCat = dtSel.getString("IT_FG_MNTO");
        if (sManCat.equals("S")) {
          tPanel2.chkbxManCat.setState(true);
        }
        else {
          tPanel2.chkbxManCat.setState(false);
        }
        String sManUsu = dtSel.getString("IT_FG_MNTO_USU");
        if (sManUsu.equals("S")) {
          tPanel2.chkbxManUsu.setState(true);
        }
        else {
          tPanel2.chkbxManUsu.setState(false);
        }
        String sAutMod = dtSel.getString("IT_AUTMOD");
        if (sAutMod.equals("S")) {
          tPanel2.chkbxAutMod.setState(true);
        }
        else {
          tPanel2.chkbxAutMod.setState(false);
        }
        String sTratPro = dtSel.getString("IT_FG_TCNE");
        if (sTratPro.equals("S")) {
          tPanel2.chkbxTraPro.setState(true);
        }
        else {
          tPanel2.chkbxTraPro.setState(false);
        }
        String sVal = dtSel.getString("IT_FG_VALIDAR");
        if (sVal.equals("S")) {
          tPanel2.chkbxVal.setState(true);
        }
        else {
          tPanel2.chkbxVal.setState(false);
        }
        String sEnf = dtSel.getString("IT_FG_ENFERMO");
        if (sEnf.equals("S")) {
          tPanel2.chkbxEnf.setState(true);
        }
        else {
          tPanel2.chkbxEnf.setState(false);
        }
        //QQ:Nuevos Flags
        String sExport = dtSel.getString("IT_FG_EXPORT");
        if (sExport.equals("S")) {
          tPanel2.chkbxExport.setState(true);
        }
        else {
          tPanel2.chkbxExport.setState(false);
        }
        String sGenAlauto = dtSel.getString("IT_FG_GENALAUTO");
        if (sGenAlauto.equals("S")) {
          tPanel2.chkbxGenAlauto.setState(true);
        }
        else {
          tPanel2.chkbxGenAlauto.setState(false);
        }
        String sManNotifs = dtSel.getString("IT_FG_MNOTIFS");
        if (sManNotifs.equals("S")) {
          tPanel2.chkbxManNotifs.setState(true);
        }
        else {
          tPanel2.chkbxManNotifs.setState(false);
        }
        String sConsRes = dtSel.getString("IT_FG_CONSREST");
        if (sConsRes.equals("S")) {
          tPanel2.chkbxConsRes.setState(true);
        }
        else {
          tPanel2.chkbxConsRes.setState(false);
        }

        // rellenamos el texfield con txtDesEquNot

        //this.modoOperacion= modoMODIFICAR; PDP 27/04/2000
        listaUsuario.setState(capp.CLista.listaNOVALIDA);

      }

      // error en el proceso
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

  }

}

//Implementaci�n del listener

class escuchador
    implements ActionListener {
  DialUsuario adaptee = null;
  ActionEvent e = null;
  public escuchador(DialUsuario adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;

    //Se activa el escuchador...
    adaptee.panelbot.btA�adir.removeActionListener(adaptee.escuchador1);
    adaptee.panelbot.btModificar.removeActionListener(adaptee.escuchador1);

    if (e.getActionCommand() == "A�adir") {
      if (adaptee.realiza_actionPerformed(e)) {
        adaptee.dispose();

      }
    }
    else if (e.getActionCommand() == "Cancelar") {
      adaptee.dispose();
    }

    //Se activa el escuchador...
    adaptee.panelbot.btA�adir.addActionListener(adaptee.escuchador1);
    adaptee.panelbot.btModificar.addActionListener(adaptee.escuchador1);
  }
}
