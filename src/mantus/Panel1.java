/**
 * @ADAPTACION DEL ANTIGUO MANTUS-PISTA AL UTILIZADO EN ICM99 PDP 14/04/2000
 * @version 1.0
 */

package mantus;

//SvrZBS2, DataZBS2. para obtener el nivel2.
import java.net.URL;
import java.util.Date;
import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CCargadorImagen;
import capp2.CMessage;
import capp2.CPanel;
import catalogo.Catalogo;
import catalogo.DataCat;
import enfermo.CListaEnfermedad;
import enfermo.DataEnfermo;
import enfermo.Fechas;
import enfermo.SrvEnfermo;
import enfermo.SrvGeneral;
import enfermo.comun;
import notdata.DataEntradaEDO; //Restituido

//import capp.CPanel;
//import comun.CFechaSimple; import comun.CHora; import comun.CListaNiveles1; import comun.Comunicador; import comun.constantes; import comun.Data; import comun.DataAutoriza; import comun.DataCasIn; import comun.DataEnferedo; import comun.DataEnfermo; import comun.DataEntradaEDO; import comun.DataEqNot; import comun.DataGeneralCDDS; import comun.DataIndivEnfermedad; import comun.DataLongValid; import comun.DataMun; import comun.DataMunicipioEDO; import comun.DataNivel1; import comun.DataNotifEDO; import comun.DataNotifSem; import comun.DataRegistroEDO; import comun.DataValoresEDO; import comun.DataZBS; import comun.DataZBS2; import comun.DatoBase; import comun.datosParte; import comun.datosPreg; import comun.DialogoGeneral; import comun.DialogRegistroEDO; import comun.DialogSelDomi; import comun.DialogSelEnfermo; import comun.Fechas; import comun.IntContenedor; import comun.PanAnoSemFech; import comun.SrvDebugGeneral; import comun.SrvDlgBuscarRegistro; import comun.SrvEnfermedad; import comun.SrvEntradaEDO; import comun.SrvMaestroEDO; import comun.SrvMun; import comun.SrvMunicipioCont; import comun.SrvNivel1; import comun.SrvZBS2; import comun.UtilEDO; import comun.comun;
//import catalogo.*;

public class Panel1
    extends CPanel {

  //Constantes del panel
  final String imgLUPA = "images/Magnify.gif";
  ResourceBundle res;
  final String strSERVLET_EQU_NOT = "servlet/SrvEntradaEDO";
  final String strSERVLET_NIVELES = "servlet/SrvCat";

  /// modos del servlet de notificacion
  final int servletOBTENER_EQUIPO_X_CODIGO = 4;
  final int servletOBTENER_EQUIPO_X_DESCRIPCION = 5;
  final int servletSELECCION_EQUIPO_X_CODIGO = 6;
  final int servletSELECCION_EQUIPO_X_DESCRIPCION = 7;
  public static final int servletOBTENER_X_CODIGO = 3;
  public static final int servletOBTENER_X_DESCRIPCION = 4;
  public static final int servletSELECCION_X_CODIGO = 5;
  public static final int servletSELECCION_X_DESCRIPCION = 6;

  PsApMantUsu PsAppPnl1 = null;
  //Variables para los items del choice (dependen del idioma)
  String strSERVICIOS_CENTRALES = "";
  String strEPIDEMIOLOGO_CA = "";
  String strEPIDEMIOLOGO_NIVEL1 = "";
  String strEPIDEMIOLOGO_NIVEL2 = "";
  String strFUENTE_NOTIFICADORA = "";

//modos de pantalla y consultas
  final int modoALTA = 0;
  final int modoMODIFICAR = 1;
  final int modoSELECCION = 2; //Usado s�lo como modo oper. con sevlett
  final int modoOBTENER = 4;
  final int modoESPERA = 5; // Usado solo como modo pantalla
  final int modoSELECCION_EQU_NOT = 6; // Usado solo como modo pantalla
  //El opmode de consulta ser� el correspondiente al servlett que consulta el eq not y variar� la URL: Va al url del eq not)

  final int modoGENERAR_LOGIN = 8;

  protected int modoOperacion = modoALTA;
//  protected int modoOperacionBk;

  /** para la comunicacion con el servlet */
  public sapp.StubSrvBD stubCliente = new sapp.StubSrvBD();

  //par�metros
  protected String sCodEquNotBk;
  protected capp.CLista listaEquNot;
  protected capp.CLista listaNiv1 = null; //Lista de c�digos nivel 1
  protected capp.CLista listaNiv2 = null; //Lista de c�digos nivel 2
  protected capp.CLista listaNiv = null; //Lista de c�digos nivel 2
  protected String strCode = null; // es el nombre del campo code de la lista seleccionada
  protected Vector vAmbUsu = new Vector(); //Strings elegidos del ambito

  Panel1ItemListener lstItemListener = new Panel1ItemListener(this);
  Panel1ActionListener btnActionListener = new Panel1ActionListener(this);
//  Panel1BotonesNormalesActionListener btnNormalActionListener = new Panel1BotonesNormalesActionListener(this);
  Panel1txtActionListener txtActionListener = new Panel1txtActionListener(this);
  Panel1txtFocusListener txtFocusListener = new Panel1txtFocusListener(this);
  Panel1TextListener txtTextListener = new Panel1TextListener(this);
  Panel1_choicePer_itemAdapter chcItemAdapter = new
      Panel1_choicePer_itemAdapter(this);

  boolean lstAmbUsuEnabled = false; //Dice si est� habilitado el control lstAmbUsu
  boolean equNotEnabled = false; //Dice si est�n habilitados los controles de eq. notificador
//  public Dialog1 dia1;

  protected DialUsuario diusu = null;

  //Dato de back up password. Si var�a el contenido de caja texto pwd respecto a este pwdBk
  // es necesario pedir confirmaci�n
  protected String pwdBk = "";

  //public String sDS_E_NOTIF = "";

  XYLayout xYLayout1 = new XYLayout();
  Label label1 = new Label();
  capp.CCampoCodigo txtCodUsu = new capp.CCampoCodigo();

  TextField txtDesPas = new TextField();
  Label label2 = new Label();

  TextField txtDesNom = new TextField();
  Label label3 = new Label();

  TextField txtDesApe = new TextField();
  Label label4 = new Label();

  TextField txtDesDir = new TextField();
  Label label5 = new Label();
  TextField txtDesTel = new TextField();
  Label label6 = new Label();
  TextField txtDesCor = new TextField();
  Label label7 = new Label();
  capp.CCampoCodigo txtCodEquNot = new capp.CCampoCodigo();
  Label label8 = new Label();
  Label label9 = new Label();
  Label label10 = new Label();
  TextArea txtDesAno = new TextArea();

//  ButtonControl btnCtrlMostrarDialogo = new ButtonControl();
  Label label11 = new Label();
  Choice choicePer = new Choice();
  ButtonControl btnCtrlBuscarEquNot = new ButtonControl();
  java.awt.List lstAmbUsu = new java.awt.List();
  TextField txtDesEquNot = new TextField();
  Checkbox chkbxBaj = new Checkbox();
  Label label12 = new Label();
  TextField txtFecAlt = new TextField();
  Label label13 = new Label();
  TextField txtFecBaj = new TextField();

  public Panel1(CApp a, DialUsuario a_diusu, PsApMantUsu PsApp) {
    try {

      setApp(a);
      diusu = a_diusu;
      PsAppPnl1 = PsApp;

      listaEquNot = new capp.CLista();
      listaEquNot.setState(capp.CLista.listaNOVALIDA);

      res = ResourceBundle.getBundle("mantus.Res" + app.getIdioma());

      //Asignaci�n Variables para los items del choice (dependen del idioma)
      strSERVICIOS_CENTRALES = res.getString("msg5.Text");
      strEPIDEMIOLOGO_CA = res.getString("msg6.Text");
      strEPIDEMIOLOGO_NIVEL1 = "Epidemi�logo de Area";
      strEPIDEMIOLOGO_NIVEL2 = "Epidemi�logo de Distrito";
      strFUENTE_NOTIFICADORA = res.getString("msg8.Text");

      new Thread(new LeerNiveles(this)).start();

      jbInit();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    final String imgNAME[] = {
        comun.imgLUPA};
    CCargadorImagen imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    xYLayout1.setHeight(421);
    xYLayout1.setWidth(654);

    label1.setText(res.getString("label1.Text"));
    chkbxBaj.setLabel(res.getString("chkbxBaj.Label"));
    label12.setText(res.getString("label12.Text"));
    label13.setText(res.getString("label13.Text"));
    label2.setText(res.getString("label2.Text"));
    label3.setText(res.getString("label3.Text"));
    label4.setText(res.getString("label4.Text"));
    label5.setText(res.getString("label5.Text"));
    label6.setText(res.getString("label6.Text"));
    label7.setText(res.getString("label7.Text"));
    label8.setText(res.getString("label8.Text"));
    label9.setText(res.getString("label9.Text"));
    label10.setText(res.getString("label10.Text"));
    label11.setText(res.getString("label11.Text"));

    btnCtrlBuscarEquNot.setImage(imgs.getImage(0));

    btnCtrlBuscarEquNot.setActionCommand("BuscarEquNot");
    lstAmbUsu.setName("lstAmbUsu");
    lstAmbUsu.setMultipleMode(true);
    txtCodEquNot.setName("txtCodEqNot");
    txtCodEquNot.addKeyListener(new Panel1_txtCodEquNot_keyAdapter(this));
    txtCodUsu.setName("txtCodUsu");
    // a�adimos el nombre de los choices
    choicePer.setName("choicePer");
    chkbxBaj.setName("chkbxBaj");

    //Se fija el car�cter de eco al usuario para caja del password
    //Tanto cuando usruario escribe como al
    txtDesPas.setEchoChar('*');

    this.setLayout(xYLayout1);
    this.add(lstAmbUsu, new XYConstraints(149, 258, 255, -1));
    this.add(label1, new XYConstraints(8, 22, 80, 20));
    this.add(txtCodUsu, new XYConstraints(97, 21, 185, 21));
    //this.add(txtDesPas, new XYConstraints(388, 21, 137, 21));
    //this.add(label2, new XYConstraints(294, 21, 88, 20));
    this.add(txtDesNom, new XYConstraints(97, 52, 185, 21));
    this.add(label3, new XYConstraints(8, 53, 80, 20));
    //this.add(txtDesApe, new XYConstraints(388, 51, 225, 21));
    //this.add(label4, new XYConstraints(294, 52, 80, 20));
    this.add(txtDesDir, new XYConstraints(97, 84, 294, 21));
    this.add(label5, new XYConstraints(8, 85, 80, 20));
    this.add(txtDesTel, new XYConstraints(510, 83, 104, 21));
    this.add(label6, new XYConstraints(421, 84, 80, 20));
    this.add(txtDesCor, new XYConstraints(97, 114, 294, 21));
    this.add(label7, new XYConstraints(8, 115, 80, 20));
    this.add(txtCodEquNot, new XYConstraints(147, 328, 71, 21));
    this.add(choicePer, new XYConstraints(149, 228, 255, -1));
    this.add(label8, new XYConstraints(6, 328, 120, 20));
    this.add(label9, new XYConstraints(5, 230, 80, 20));
    this.add(label10, new XYConstraints(8, 145, 89, 20));
    this.add(txtDesAno, new XYConstraints(97, 144, 474, 69));
//    this.add(btnCtrlMostrarDialogo, new XYConstraints(251, 13, 31, 33));
    this.add(label11, new XYConstraints(7, 265, 121, -1));
    this.add(btnCtrlBuscarEquNot, new XYConstraints(226, 328, -1, -1));
    this.add(txtDesEquNot, new XYConstraints(256, 328, 212, -1));
//    this.add(lstAmbUsu, new XYConstraints(152, 290, -1, -1));
    this.add(chkbxBaj, new XYConstraints(7, 375, 57, -1));
    this.add(label12, new XYConstraints(121, 375, 94, -1));
    this.add(txtFecAlt, new XYConstraints(232, 375, 95, -1));
    this.add(label13, new XYConstraints(354, 375, 98, -1));
    this.add(txtFecBaj, new XYConstraints(467, 375, 93, -1));
//    this.add(lstEquNot, new XYConstraints(149, 329, -1, -1));

    choicePer.addItem("");
    choicePer.addItem(strSERVICIOS_CENTRALES);
    choicePer.addItem(strEPIDEMIOLOGO_CA);
    choicePer.addItem(strEPIDEMIOLOGO_NIVEL1); //getNivel1())
    choicePer.addItem(strEPIDEMIOLOGO_NIVEL2); //getNivel2());
    choicePer.addItem(strFUENTE_NOTIFICADORA);

    txtCodUsu.addActionListener(txtActionListener);
    txtCodUsu.addFocusListener(txtFocusListener);
    btnCtrlBuscarEquNot.addActionListener(btnActionListener);
//    btnCtrlMostrarDialogo.addActionListener(btnNormalActionListener);
    lstAmbUsu.addItemListener(lstItemListener);

    txtCodEquNot.addActionListener(txtActionListener);
    txtCodEquNot.addFocusListener(txtFocusListener);
    txtCodEquNot.addTextListener(txtTextListener);

    // seleccion del choice
    choicePer.addItemListener(chcItemAdapter);
    // seleccion del checkbox
    chkbxBaj.addItemListener(chcItemAdapter);

    txtFecAlt.setEditable(false);
    txtFecBaj.setEditable(false);

    // ponemos las claves en amarillo
    txtCodUsu.setBackground(comun.amarillo);
    txtDesNom.setBackground(comun.amarillo);
    txtDesPas.setBackground(comun.amarillo);
    txtDesApe.setBackground(comun.amarillo);
    choicePer.setBackground(comun.amarillo);

    //this.modoOperacion= modoALTA;  //PDP 27/04/2000
    Inicializar();

  }

  public void borra() {

    txtDesApe.setText("");
    txtDesPas.setText("");
    txtDesDir.setText("");
    txtDesCor.setText("");
    txtCodEquNot.setText("");
    txtDesEquNot.setText("");
    txtDesNom.setText("");
    txtDesAno.setText("");
    txtDesTel.setText("");
    txtCodUsu.setText("");
    choicePer.select(0);
    lstAmbUsu.removeAll();
    txtFecAlt.setText("");
    txtFecBaj.setText("");

  }

  public void Inicializar() {

//??????????????? Repasar controles ambito y eq notificador
    switch (modoOperacion) {
      case modoALTA:
        txtCodUsu.setEnabled(false);
        txtDesPas.setEnabled(true);
        txtDesNom.setEnabled(false);
        txtDesApe.setEnabled(true);
        txtDesDir.setEnabled(true);
        txtDesTel.setEnabled(true);
        txtDesCor.setEnabled(true);
        txtDesAno.setEnabled(true);
        choicePer.setEnabled(true);
        lstAmbUsu.setEnabled(lstAmbUsuEnabled);
        if (lstAmbUsuEnabled) {
          lstAmbUsu.setBackground(Color.white);
        }
        else {
          lstAmbUsu.setBackground(Color.gray);
        }
        txtCodEquNot.setEnabled(equNotEnabled);
        txtCodEquNot.setEditable(equNotEnabled);
        btnCtrlBuscarEquNot.setEnabled(equNotEnabled);
        txtDesEquNot.setEnabled(false);
        txtDesEquNot.setEditable(false);
        chkbxBaj.setEnabled(false); //PDP 27/04/2000
        txtFecAlt.setEnabled(false);
        txtFecBaj.setEnabled(false);
//        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
//        listaZBS.setState(CLista.listaNOVALIDA);

        break;

      case modoMODIFICAR:
        txtCodUsu.setEnabled(false);
        txtDesPas.setEnabled(true);
        txtDesNom.setEnabled(false);
        txtDesApe.setEnabled(true);
        txtDesDir.setEnabled(true);
        txtDesTel.setEnabled(true);
        txtDesCor.setEnabled(true);
        txtDesAno.setEnabled(true);
        choicePer.setEnabled(true);
        lstAmbUsu.setEnabled(lstAmbUsuEnabled);
        if (lstAmbUsuEnabled) {
          lstAmbUsu.setBackground(Color.white);
        }
        else {
          lstAmbUsu.setBackground(Color.gray);
        }
        txtCodEquNot.setEnabled(equNotEnabled);
        txtCodEquNot.setEditable(equNotEnabled);
        btnCtrlBuscarEquNot.setEnabled(equNotEnabled);
        txtDesEquNot.setEnabled(false);
        txtDesEquNot.setEditable(false);
        chkbxBaj.setEnabled(true);
        txtFecAlt.setEnabled(false);
        txtFecBaj.setEnabled(false);
//        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        break;

      case modoSELECCION_EQU_NOT:

        // modo selecci�n eq. notificador
        txtCodUsu.setEnabled(false);
//        btnCtrlMostrarDialogo.setEnabled(false);
        txtDesPas.setEnabled(false);
        txtDesNom.setEnabled(false);
        txtDesApe.setEnabled(false);
        txtDesDir.setEnabled(false);
        txtDesTel.setEnabled(false);
        txtDesCor.setEnabled(false);
        txtDesAno.setEnabled(false);
        choicePer.setEnabled(false);
        lstAmbUsu.setEnabled(false);
        txtCodEquNot.setEnabled(false);
        btnCtrlBuscarEquNot.setEnabled(false);
        txtDesEquNot.setEnabled(false);
        chkbxBaj.setEnabled(false);
        txtFecAlt.setEnabled(false);
        txtFecBaj.setEnabled(false);

//        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.doLayout();
        break;

      case modoESPERA:

        // modo espera
        txtCodUsu.setEnabled(false);
//        btnCtrlMostrarDialogo.setEnabled(false);
        txtDesPas.setEnabled(false);
        txtDesNom.setEnabled(false);
        txtDesApe.setEnabled(false);
        txtDesDir.setEnabled(false);
        txtDesTel.setEnabled(false);
        txtDesCor.setEnabled(false);
        txtDesAno.setEnabled(false);
        choicePer.setEnabled(false);
        lstAmbUsu.setEnabled(false);
        txtCodEquNot.setEnabled(false);
        btnCtrlBuscarEquNot.setEnabled(false);
        txtDesEquNot.setEnabled(false);
        chkbxBaj.setEnabled(false);
        txtFecAlt.setEnabled(false);
        txtFecBaj.setEnabled(false);
        //Dejamos visibilidad controles igual
//        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
  }

  public boolean isDataValid() {

    CMessage msgBox;
    boolean result = true;
    String fallo = null;
    int[] indSel = lstAmbUsu.getSelectedIndexes();
    //DialConfirPwd dialPwd = null;

    if (txtCodUsu.getText().length() <= 0) {
      result = false;
      //fallo = "C�digo de usuario es un campo obligatorio";
      fallo = res.getString("msg11.Text");
    }
    else if (txtCodUsu.getText().length() > 12) {
      result = false;
      fallo = res.getString("msg12.Text");
    }

    if (txtDesNom.getText().length() <= 0) {
      result = false;
      //fallo = "Nombre es un campo obligatorio";
      fallo = res.getString("msg11.Text");
    }
    else if (txtDesNom.getText().length() > 40) {
      result = false;
      fallo = res.getString("msg13.Text");
    }

    if (txtDesTel.getText().length() > 18) {
      result = false;
      fallo = res.getString("msg17.Text");
    }

    if (txtDesDir.getText().length() > 100) {
      result = false;
      fallo = res.getString("msg18.Text");
    }

    if (txtDesCor.getText().length() > 25) {
      result = false;
      fallo = res.getString("msg19.Text");
    }

    if (choicePer.getSelectedIndex() == 0) {
      result = false;
      fallo = res.getString("msg20.Text");
    }
    else if ( (choicePer.getSelectedIndex() == 3 ||
               choicePer.getSelectedIndex() == 4) &&
             indSel.length == 0) {
      // comprobamos que exista alg�n item de la lista seleccionado
      result = false;
      fallo = res.getString("msg21.Text");
    }
    else if (choicePer.getSelectedIndex() == 5 &&
             (txtCodEquNot.getText() == null ||
              txtCodEquNot.getText().trim().length() == 0)) {
      // comprobamos que exista notificador
      result = false;
      fallo = res.getString("msg22.Text");
    }

    if (!result) {
      msgBox = new CMessage(app, CMessage.msgAVISO, fallo);
      msgBox.show();
      msgBox = null;
    }

    return result;

  }

  /** accion cuando se pulsa el boton de buscar  */
  public void txtBuscarEquNot_actionPerformed(ActionEvent evt) {
    CMessage msgBox = null;
    capp.CLista data = null;
    String codEq = null;
    capp.CLista result = null;

    // almacena el modo de operaci�n
    ( (DialUsuario) diusu).modoOperacionBk = ( (DialUsuario) diusu).
        modoOperacion;
    // modo espera
    ( (DialUsuario) diusu).modoOperacion = modoESPERA;
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    ( (DialUsuario) diusu).Inicializar();

    try {

      codEq = txtCodEquNot.getText();
      String desEq = txtDesEquNot.getText();
      if (codEq != null && codEq.trim().length() > 0 &&
          (desEq == null || desEq.trim().length() == 0)) {

        DataEnfermo dato = new DataEnfermo("CD_E_NOTIF");
        dato.setFiltro(codEq);
        data = new capp.CLista();
        data.addElement(dato);
        data.setFilter("");

        stubCliente.setUrl(new java.net.URL(app.getParametro("URL_SERVLET") +
                                            comun.strSERVLET_ENFERMO));
        result = (capp.CLista) stubCliente.doPost(SrvEnfermo.modoE_NOTIF +
                                                  SrvGeneral.
                                                  servletOBTENER_X_CODIGO, data);

        if (result != null && result.size() > 0) {
          dato = (DataEnfermo) result.firstElement();
          txtCodEquNot.setText( (String) dato.get("CD_E_NOTIF"));
          txtDesEquNot.setText( (String) dato.get("DS_E_NOTIF"));
        }
        else {
          msgBox = new CMessage(app, CMessage.msgERROR,
                                res.getString("msg26.Text"));
          msgBox.show();
          msgBox = null;
        }

        ( (DialUsuario) diusu).modoOperacion = ( (DialUsuario) diusu).
            modoOperacionBk;
      }
      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;

      // vuelve al modo de operaci�n anterior
      ( (DialUsuario) diusu).modoOperacion = ( (DialUsuario) diusu).
          modoOperacionBk;
    }

    ( (DialUsuario) diusu).modoOperacion = ( (DialUsuario) diusu).
        modoOperacionBk;
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    ( (DialUsuario) diusu).Inicializar();
  }

  /** accion cuando se pulsa el boton de buscar  */
  public void btnBuscarEquNot_actionPerformed(ActionEvent evt) {
    CListaEnfermedad lista = null;
    CMessage msgBox = null;
    capp.CLista data = null;

    try {

      // almacena el modo de operaci�n
      ( (DialUsuario) diusu).modoOperacionBk = ( (DialUsuario) diusu).
          modoOperacion;

      // modo espera
      ( (DialUsuario) diusu).modoOperacion = modoESPERA;
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      ( (DialUsuario) diusu).Inicializar();

      stubCliente = new sapp.StubSrvBD();
      stubCliente.setUrl(new java.net.URL(app.getParametro("URL_SERVLET") +
                                          comun.strSERVLET_ENFERMO));

      /// lanzamos la lista

      lista = new CListaEnfermedad(PsAppPnl1,
                                   res.getString("msg27.Text"), stubCliente,
                                   comun.strSERVLET_ENFERMO,
                                   SrvEnfermo.modoE_NOTIF +
                                   SrvGeneral.servletOBTENER_X_CODIGO,
                                   SrvEnfermo.modoE_NOTIF +
                                   SrvGeneral.servletOBTENER_X_DESCRIPCION,
                                   SrvEnfermo.modoE_NOTIF +
                                   SrvGeneral.servletSELECCION_X_CODIGO,
                                   SrvEnfermo.modoE_NOTIF +
                                   SrvGeneral.servletSELECCION_X_DESCRIPCION,
                                   "CD_E_NOTIF", "DS_E_NOTIF");

      lista.show();

      try {
        DataEnfermo dato = (DataEnfermo) lista.getComponente();
        lista = null;
        if (dato != null) {
          txtCodEquNot.setText( (String) dato.get("CD_E_NOTIF"));
          // guardamos le valor del c�digo
          txtDesEquNot.setText( (String) dato.get("DS_E_NOTIF"));
          //sDS_E_NOTIF = (String)dato.get("DS_E_NOTIF");
        }
      }
      catch (Exception e) {
      }

      /*      // lanzamos la lista
              CListaEntradaEDO lista = new CListaEntradaEDO(app,
           res.getString("msg28.Text") , stubCliente,
                                              strSERVLET_EQU_NOT,
                                              servletOBTENER_EQUIPO_X_CODIGO,
           servletOBTENER_EQUIPO_X_DESCRIPCION,
                                              servletSELECCION_EQUIPO_X_CODIGO,
           servletSELECCION_EQUIPO_X_DESCRIPCION);
              lista.show();
              DataEntradaEDO dato = (DataEntradaEDO) lista.getComponente();
              lista = null;
              if (dato != null) {
                     txtCodEquNot.setText(dato.getCod());
                     ///************  problema del idioma
                      txtDesEquNot.setText(dato.getDes());
               }
        */
//      ((DialUsuario)diusu).modoOperacion = modoSELECCION_EQU_NOT;
       ( (DialUsuario) diusu).modoOperacion = ( (DialUsuario) diusu).
           modoOperacionBk;

       // error al procesar
    }
    catch (Exception e) {
      // System_out.println("@@@Mantus.Panel1: Catch e..." + e.getMessage());
      e.printStackTrace();
      msgBox = new CMessage(app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;

      // vuelve al modo de operaci�n anterior
      ( (DialUsuario) diusu).modoOperacion = ( (DialUsuario) diusu).
          modoOperacionBk;
    }

    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    ( (DialUsuario) diusu).Inicializar();

  }

  /*
     public String getDS_E_NOTIF(){
     return sDS_E_NOTIF;
     }
   */

  public synchronized void setListaNivel1(capp.CLista a_lista) {
    listaNiv1 = a_lista;
  }

  public synchronized capp.CLista getListaNivel1() {
    return listaNiv1;
  }

  public synchronized void setListaNivel2(capp.CLista a_lista) {
    listaNiv2 = a_lista;
  }

  public synchronized capp.CLista getListaNivel2() {
    return listaNiv2;
  }

  /*
    public void btnBuscarEquNot_actionPerformed(ActionEvent evt) {
      CMessage msgBox = null;
      CLista data = null;
      try {
        // almacena el modo de operaci�n
       ((DialUsuario)diusu).modoOperacionBk = ((DialUsuario)diusu).modoOperacion;
        // modo espera
        ((DialUsuario)diusu).modoOperacion = modoESPERA;
        ((DialUsuario)diusu).Inicializar();
        // si procede obtiene la lista
        if ((listaEquNot.getState() == CLista.listaNOVALIDA) || (sCodEquNotBk != txtCodEquNot.getText())) {
          // prepara los par�metros
          data = new CLista();
          // apunta al servlet auxiliar
          ((DialUsuario)diusu).stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_EQU_NOT));
          sCodEquNotBk=txtCodEquNot.getText();
          data.addElement(new DataCat(sCodEquNotBk));
          listaEquNot = (CLista) ((DialUsuario)diusu).stubCliente.doPost(modoSELECCION + Catalogo.catNIVEL1, data);
//?????????????????????????????????????? Luego cambiar  el num  catalogo
          data = null;
        }
        writeLista(lstEquNot);
        ((DialUsuario)diusu).modoOperacion = modoSELECCION_EQU_NOT;
      // error al procesar
      } catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        // vuelve al modo de operaci�n anterior
       ((DialUsuario)diusu).modoOperacion = ((DialUsuario)diusu).modoOperacionBk;
      }
      ((DialUsuario)diusu).Inicializar();
    }
   */

  /*  //Realiza una consulta al servlet para ver si hay datos del item
      //seleccionado. Con el componente CListaMantenimiento no es necesario
      //ya que en el metodos primerapagina se empaquetan los datos y se pasan al
      //di�logo DialUsuario
    // procesa opci�n obtener un item principal
    public void txtCodUsu_actionPerformed(ActionEvent evt) {
      CMessage msgBox = null;
      capp.CLista data = null;
      try {
        // modo espera
        ((DialUsuario)diusu).modoOperacion = modoESPERA;
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        ((DialUsuario)diusu).Inicializar();
        // prepara los par�metros de env�o
        data = new capp.CLista();
        // c�digo de filtrado
        data.addElement(new DataUsuario(txtCodUsu.getText(),"", "",
                "","",false,
                false,false,
                "", false, false,
                false,false,false,
                false,false,"",
                "","", "","","",false,
                "",null,"",
                //QQ: Nuevos Flags
                false,false, false, false, false, ""
               ));
        // idioma
        data.setIdioma(app.getIdioma() );
        // apunta al servlet principal
        ((DialUsuario)diusu).stubCliente.setUrl(new URL(app.getParametro("URL_SERVLET") + ((DialUsuario)diusu).strSERVLET));
        ((DialUsuario)diusu).listaUsuario = (capp.CLista)((DialUsuario)diusu).stubCliente.doPost(modoOBTENER, data); //Apuntar al modo de busqueda del servlet nuevo por codigo usuario
        if (((DialUsuario)diusu).listaUsuario.size() > 0) {
          // el item existe
       ((DialUsuario)diusu).modoOperacion = ((DialUsuario)diusu).modoMODIFICAR;
          ((DialUsuario)diusu).usuSeleccionado = (DataUsuario)  ((DialUsuario)diusu).listaUsuario.firstElement() ;
          //Desde el applet se rellenar�n los datos del usuario seleccionado
          ((DialUsuario)diusu).rellenarDatosUsuSeleccionado();
        }
        else {
          // el item no existe
          ((DialUsuario)diusu).modoOperacion = modoALTA;
       msgBox = new CMessage(app, CMessage.msgAVISO, res.getString("msg29.Text"));
          msgBox.show();
          msgBox = null;
        }
      // error en el proceso
      } catch (Exception e) {
        ((DialUsuario)diusu).modoOperacion = modoALTA;
        e.printStackTrace();
        msgBox = new CMessage(app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      ((DialUsuario)diusu).Inicializar();
    }
   */

  public void txtCodEquNot_actionPerformed(ActionEvent evt) {
    CMessage msgBox = null;
    DataCat cat = null;
    capp.CLista data = null;

    try {

      // guarda el modo de operaci�n
      ( (DialUsuario) diusu).modoOperacionBk = ( (DialUsuario) diusu).
          modoOperacion;

      // modo espera
      ( (DialUsuario) diusu).modoOperacion = modoESPERA;
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      ( (DialUsuario) diusu).Inicializar();

      // prepara los par�metros
      data = new capp.CLista();

      data.addElement(new DataCat(txtCodEquNot.getText()));

      // apunta al servlet auxiliar
      ( (DialUsuario) diusu).stubCliente.setUrl(new URL(this.app.getParametro(
          "URL_SERVLET") + strSERVLET_EQU_NOT));
      listaEquNot = (capp.CLista) ( (DialUsuario) diusu).stubCliente.doPost(
          modoOBTENER + Catalogo.catNIVEL1, data);
//        ???????????????????????????????????????????????????????

      // se encontr� el item
      if (listaEquNot.size() > 0) {
        cat = (DataCat) listaEquNot.firstElement();
      }
      else {
        cat = new DataCat();
        msgBox = new CMessage(app, CMessage.msgAVISO,
                              res.getString("msg29.Text"));
        msgBox.show();
        msgBox = null;
      }

      txtCodEquNot.setText(cat.getCod());
      txtDesEquNot.setText(cat.getDes());
      /*
            // cambia al idioma local, si procede
            if ((this.app.getIdioma() != CApp.idiomaPORDEFECTO) && (cat.getDesL().length() > 0))
              txtDesEqNot.setText(cat.getDesL());
       */
      // error en el proceso
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

    // regresa al modo correspondiente
    ( (DialUsuario) diusu).modoOperacion = ( (DialUsuario) diusu).
        modoOperacionBk;
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    ( (DialUsuario) diusu).Inicializar();

  }

  /** cambios en la lista  */
  void lstAmbUsu_itemStateChanged(ItemEvent evt) {
  }

  /** cambios en la lista  *
     void lstAmbUsu_itemStateChanged(ItemEvent evt) {
    CMessage msgBox = null;
    DataCat cat = null;
    CLista data = null;
    try {
      // modo espera
      this.modoOperacion = modoESPERA;
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      Inicializar();
          // a�ade una p�gina a la lista
          if ((lstAmbUsu.getSelectedIndex() == lstAmbUsu.getItemCount()-1) && (listaNiv.getState() == CLista.listaINCOMPLETA)) {
              int catalogo;
              // prepara los parametros
              data = new CLista();
              if (listaNiv == listaNiv1){
                 data.addElement(new DataEnfermo("CD_NIVEL_1"));
                 catalogo = SrvEnfermo.modoNivel1;
              }else{
                 data.addElement(new DataEnfermo("CD_NIVEL_2"));
                 catalogo = SrvEnfermo.modoNivel2;
              }
              // almacena el modo de operaci�n  //???????????????????? A�adido : No hay btn Ctrl Luego partimos de modo alta o modificar
       ((DialUsuario)diusu).modoOperacionBk = ((DialUsuario)diusu).modoOperacion;
               // modo espera
               ( (DialUsuario)diusu).modoOperacion = modoESPERA;
               ((DialUsuario)diusu).Inicializar();
               // estado incompleto
               data.setState( listaNiv.getState() );
               // apunta al servlet auxiliar
              ((DialUsuario)diusu).stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_NIVELES));
               // ultimo codigo
               data.setFilter( listaNiv.getFilter() );
               // a�ade la lista
               CLista result = comun.traerDatos(app, stubCliente, comun.strSERVLET_ENFERMO, catalogo, data);
               listaNiv.appendData(result);
                //dibuja la lista
                writeListaNiveles(listaNiv,lstAmbUsu);
                //Deselecciona el �ltimo (el que acabo de seleccionar)
                lstAmbUsu.deselect(lstAmbUsu.getItemCount()-1)  ;
                //selecciona los ambitos
                seleccionarAmbitos();
                // modo selecci�n auxiliar
       ( (DialUsuario)diusu).modoOperacion = ((DialUsuario)diusu).modoOperacionBk;
          }  else {
                //Volcamos en vAmbUsu los c�digos correspondientes a los Strings seleccionados en lstAmbUsu
                //Quitamos de vAmbUsu los c�digos correspondientes a los Strings NO seleccionados en lstAmbUsu
                //Como busco c�digos recorremos listaNiveles en vez de lstAmbUsu .
                //En lstAmbUsu simplemente comprobamos si est� seleccionado o no
                //Nota:  No necesario regresar a modo anterior pues no he cambiado de modo (no ha habido busqueda)
                //Mejor que esto seria si supiera en que item ha habido cambio de estado ???????????
                Enumeration enum;
                Vector repositorio =new Vector();
                for(int j=0; (j<listaNiv.size())  ; j++) {
                    boolean encontrado=false;
                    if (vAmbUsu!=null)   {
       for (int k=0; (k<vAmbUsu.size())  && (encontrado == false);k++) {
                         String ambito = (String)(vAmbUsu.elementAt(k));
       if (ambito.equals(  ((DataCat)(listaNiv.elementAt(j))).getCod()  ) ) {
                             encontrado=true;
                             if ( (lstAmbUsu.isIndexSelected(j))== false )
                                //Encontrado en vector uno no seleccionado
                                vAmbUsu.removeElementAt(k);
                             }
                         }
                         //No encontrado en vector vAmbUsu uno seleccionado : luego lo a�adiremos
       repositorio.addElement(  ((DataCat)(listaNiv.elementAt(j))) .getCod());
                       }
                    }
                    enum = repositorio.elements();
                    while (enum.hasMoreElements() ) {
                       String elem= (String)enum.nextElement();
                       vAmbUsu.addElement(elem);
                    }
                }
          } catch (Exception e) {
            e.printStackTrace();
            msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
            msgBox.show();
            msgBox = null;
       ( (DialUsuario)diusu).modoOperacion =( (DialUsuario)diusu).modoOperacionBk;
          }
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          ((DialUsuario)diusu).Inicializar();
     }
   */
//Al usar esta funcion suponemos que lst contiene todos los elem de la lista
//Por tanto para indicar seleccionados basta volcar la lst al vector
  void ponerAmbitosEnVector() {
    if (choicePer.getSelectedIndex() == 3 || choicePer.getSelectedIndex() == 4) {
      String codigo = null, nivel2 = null;
      DataEnfermo denf = null;

      int[] indices = lstAmbUsu.getSelectedIndexes();
      if ( (vAmbUsu != null) && (listaNiv != null)) {
        vAmbUsu.removeAllElements();

        // rellenamos los c�digo de esos indices
        for (int j = 0; (j < indices.length); j++) {
          denf = ( (DataEnfermo) (listaNiv.elementAt(indices[j])));
          codigo = (String) denf.get("CD_NIVEL_1");
          nivel2 = (String) denf.get("CD_NIVEL_2");
          ////#// System_out.println("indice: " + indices[j] + codigo + nivel2);
          vAmbUsu.addElement(codigo); // nivel1
          vAmbUsu.addElement(nivel2); // nivel2
        }
      }
    }
    else {
      vAmbUsu = null;
    }
  }

  /*
//Al usar esta funcion suponemos que lst contiene todos los elem de la lista
//Por tanto para indicar seleccionados basta volcar la lst al vector
   void ponerAmbitosEnVector() {
      if ( (vAmbUsu!=null) && (listaNiv!=null) )  {
      vAmbUsu.removeAllElements();
       for(int j=0; (j<listaNiv.size())  ; j++) {
        String codigo=((DataCat)(listaNiv.elementAt(j))).getCod();
        if ( (lstAmbUsu.isIndexSelected(j))== true )
             vAmbUsu.addElement(codigo);
       }
//#// System_out.println("Vector ambitos   ????????" + vAmbUsu ); //*************************
       }
    }
    */

   // procesa el cambio de c�digo auxiliar
   public void txt_textValueChanged(TextEvent e) {
     if ( ( (TextField) e.getSource()).getName() == "txtCodEquNot") {
       txtDesEquNot.setText("");
     }
   }

  /*
    void btnCtrlMostrarDialogo_actionPerformed(ActionEvent e) {
      String cadena;
//    int pos2,pos3;
      dia1=new Dialog1(app, txtCodUsu.getText());
      dia1.show();
      //Si se ha seleccionado un usuario
      if (dia1.valor){
       ((DialUsuario)diusu).rellenarDatosUsuSeleccionado();    //Desde el applet se rellenar�n los datos del usuario seleccionado
      }//if
    }
   */

  // a�ade los datos de CLista  lista al control l
  protected void writeListaNiveles(capp.CLista lista, java.awt.List l) {
    DataCat datCat;
    CMessage msgBox;
    capp.CLista data;

    // vacia el contenido
    l.removeAll();

    data = lista;

    // agrega lso items
    if (data.size() > 0) {

      // vuelca la lista, dependiendo de su tipo
      for (int j = 0; j < data.size(); ++j) {
        if (l.getName() == "lstAmbUsu") {
          datCat = (DataCat) data.elementAt(j);

          // Se a�ade la descrpci�n bien sea en  idioma local o poer defecto
          if (datCat.getDesL().length() > 0) {
            l.add(datCat.getDesL());
            //}else{
            //   l.add( datCat.getDes() );
          }

        }
      }

      // opci�n m�s datos
      if (data.getState() == capp.CLista.listaINCOMPLETA) {
        l.add(res.getString("msg30.Text"));
      }

      // mensaje de lista vacia
    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            res.getString("msg29.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  // a�ade los datos de CLista  lista al control l
  protected void writeListaNiveles(capp.CLista lista, java.awt.List l,
                                   String des, String desL,
                                   String cod1, String cod2) {
    DataEnfermo datEnfermo;
    CMessage msgBox;
    capp.CLista data;

    // vacia el contenido
    l.removeAll();

    data = lista;

    // agrega lso items
    if (data.size() > 0) {

      // vuelca la lista, dependiendo de su tipo
      for (int j = 0; j < data.size(); ++j) {
        if (l.getName() == "lstAmbUsu") {
          datEnfermo = (DataEnfermo) data.elementAt(j);

          String strDesL = (String) datEnfermo.get(desL);
          if ( (strDesL != null) && (strDesL.length() > 0)) {
            l.add(strDesL);
          }
          else
          if (cod2.equals("")) {
            l.add( (String) datEnfermo.get(cod1) + " - " +
                  (String) datEnfermo.get(des));
          }
          else {
            l.add( (String) datEnfermo.get(cod1) + "-" +
                  (String) datEnfermo.get(cod2) + " - " +
                  (String) datEnfermo.get(des));
          }
        }
      }

      // opci�n m�s datos
      if (data.getState() == capp.CLista.listaINCOMPLETA) {
        l.add(res.getString("msg30.Text"));
      }

      // mensaje de lista vacia
    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            res.getString("msg29.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  /** cambia el valor de la fecha de baja */
  void chkbxBaj_itemStateChanged(ItemEvent e) {
    /// si es true se a�ade la fecha
    if (chkbxBaj.getState()) {
      txtFecBaj.setText(Fechas.date2String(new Date()));
    }
    else { // se borra la fecha
      txtFecBaj.setText("");
    }
  }

  /** cambios en el choice de Perfiles */
  void choicePer_itemStateChanged(ItemEvent e) {
    trasSeleccionarPerfil(choicePer.getSelectedItem());
  }

  /**
   * Toma las acciones oportunas en panel tras la selecci�n de perfil , bien sea por un evento en choice
   * o por la llegada de datos de un usuario con un perfil determinado
   */
  void trasSeleccionarPerfil(String per) {

    try { //PDP 27/04/2000  Retardo necesario para que no de problemas el 'trhead' de LeerNiveles
      Thread.sleep(1000);
    }
    catch (Exception exc) {
      //#// System_out.println ("Fallo esperando la lista Nivel 2");
    }

    if (choicePer.getSelectedItem().equals(strEPIDEMIOLOGO_NIVEL1)) {

      // comprobamos que los datos de la listaNivel1 esten listos
      /*
               try {
         //while(listaNiv1 == null) {
            Thread.sleep(1000);
         //}
               }catch (Exception exc){
         //#// System_out.println ("Fallo esperando la lista Nivel 1");
               }
       */
      lstAmbUsuEnabled = true;
      equNotEnabled = false;
      txtCodEquNot.setText("");
      txtDesEquNot.setText("");

      ( (DialUsuario) diusu).Inicializar(); //Aunque no hemos cambiado modo , necesario para que se materializen los cambios en los controles
      //vAmbUsu.removeAllElements(); PDP 26/04/2000
//        writeListaNiveles(listaNiv1,lstAmbUsu);
      writeListaNiveles(listaNiv1, lstAmbUsu, "DS_NIVEL_1", "DSL_NIVEL_1",
                        "CD_NIVEL_1", "");

      //Dejamos como lista en uso lista de niveles 1  Necesario pues funcion seleccionarAmbitos solo usa listaNiv
      strCode = "CD_NIVEL_1";
      listaNiv = listaNiv1;
    }
    else if (choicePer.getSelectedItem().equals(strEPIDEMIOLOGO_NIVEL2)) {
      // comprobamos que los datos de la listaNivel2 esten listos
      /*
               try {
         //while(listaNiv2 == null) {
            Thread.sleep(1000);
         //}
               }catch (Exception exc){
         //#// System_out.println ("Fallo esperando la lista Nivel 2");
               }
       */

      lstAmbUsuEnabled = true;
      equNotEnabled = false;
      txtCodEquNot.setText("");
      txtDesEquNot.setText("");

      ( (DialUsuario) diusu).Inicializar(); //Aunque no hemos cambiado modo , necesario para que se materializen los cambios en los controles
      //vAmbUsu.removeAllElements(); PDP 26/04/2000
//        writeListaNiveles(listaNiv2,lstAmbUsu);
      writeListaNiveles(listaNiv2, lstAmbUsu, "DS_NIVEL_2", "DSL_NIVEL_2",
                        "CD_NIVEL_1", "CD_NIVEL_2");

      //Dejamos como lista en uso lista de niveles 1
      strCode = "CD_NIVEL_2";
      listaNiv = listaNiv2;
    }
    else if (choicePer.getSelectedItem().equals(strFUENTE_NOTIFICADORA)) {
      lstAmbUsuEnabled = false;
      equNotEnabled = true;
      ( (DialUsuario) diusu).Inicializar(); //Aunque no hemos cambiado modo , necesario para que se materializen los cambios en los controles
      //vAmbUsu.removeAllElements(); PDP 26/04/2000
      lstAmbUsu.removeAll();
      strCode = null;
      listaNiv = null;
    }
    else if (choicePer.getSelectedItem().equals(strEPIDEMIOLOGO_CA)) {
      lstAmbUsuEnabled = false;
      equNotEnabled = false;
      ( (DialUsuario) diusu).Inicializar(); //Aunque no hemos cambiado modo , necesario para que se materializen los cambios en los controles
      txtCodEquNot.setText("");
      txtDesEquNot.setText("");
      //vAmbUsu.removeAllElements(); PDP 26/04/2000
      // a�adimos una l�nea que es la queindica que es de provincia
      //vAmbUsu.addElement("");
      lstAmbUsu.removeAll();

      strCode = null;
      listaNiv = null;
    }
    else {
      lstAmbUsuEnabled = false;
      equNotEnabled = false;
      ( (DialUsuario) diusu).Inicializar(); //Aunque no hemos cambiado modo , necesario para que se materializen los cambios en los controles
      txtCodEquNot.setText("");
      txtDesEquNot.setText("");
      //vAmbUsu.removeAllElements(); PDP 26/04/2000
      lstAmbUsu.removeAll();

      //strCode = null;
      //listaNiv = null;
    }
  }

  //Selecciona en el control lstAmbUsu todos los items cuyo c�digo corresponda con alguno
  //del vector de Strings vAmbUsu . La comparaci�n se busca en la listaAmbUsu pues es la que tiene
  //los codigos
  //Se supone que partimos de lstAmbUsu sin ninguno seleccionado
  void seleccionarAmbitos() {
    Enumeration enum;
    String listaNivel1 = null, listaNivel2 = null;
    String vAmbUsuNivel1 = null, vAmbUsuNivel2 = null;
    boolean encontrado = false;
    DataEnfermo denf = null;

    if ( (vAmbUsu != null) && (listaNiv != null)) {
      enum = vAmbUsu.elements();
      while (enum.hasMoreElements()) {
        vAmbUsuNivel1 = (String) (enum.nextElement());
        vAmbUsuNivel2 = (String) (enum.nextElement());

        ////#// System_out.println("Fijamos ambito "+vAmbUsuNivel1+vAmbUsuNivel2);
        encontrado = false;
        if (vAmbUsuNivel2 != null) {
          for (int j = 0; (j < listaNiv.size()) && (encontrado == false); j++) {
            denf = (DataEnfermo) (listaNiv.elementAt(j));
            listaNivel1 = (String) denf.get("CD_NIVEL_1");
            listaNivel2 = (String) denf.get("CD_NIVEL_2");
            if (vAmbUsuNivel1.equals(listaNivel1) &&
                vAmbUsuNivel2.equals(listaNivel2)) {
              lstAmbUsu.select(j);
              encontrado = true;
            }
          }
        }
        else if (vAmbUsuNivel1 != null) {
          for (int j = 0; (j < listaNiv.size()) && (encontrado == false); j++) {
            denf = (DataEnfermo) (listaNiv.elementAt(j));
            listaNivel1 = (String) denf.get("CD_NIVEL_1");
            if (vAmbUsuNivel1.equals(listaNivel1)) {
              lstAmbUsu.select(j);
              encontrado = true;
            }
          }
        }
      } //while
    } //if
  }

  void txtCodEquNot_keyPressed(KeyEvent e) {
    txtDesEquNot.setText("");
  }

} //clase

// action listener para los botones
class Panel1ActionListener
    implements ActionListener { //, Runnable {
  Panel1 adaptee = null;
  ActionEvent e = null;

  public Panel1ActionListener(Panel1 adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;

    // elimina el control de cambio del codigo auxiliar desde el codigo
    adaptee.txtCodEquNot.removeTextListener(adaptee.txtTextListener);

    if (e.getActionCommand() == "BuscarEquNot") { //B�squeda auxiliar
      adaptee.btnBuscarEquNot_actionPerformed(e);

      // activa el control de cambio del codigo auxiliar
    }
    adaptee.txtCodEquNot.addTextListener(adaptee.txtTextListener);
  }
}

// action listener para las cajas de texto
class Panel1txtActionListener
    implements ActionListener { //, Runnable {
  Panel1 adaptee = null;
  ActionEvent e = null;

  public Panel1txtActionListener(Panel1 adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;

    //new Thread(this).start();
    //}

    // hilo de ejecuci�n para servir el evento
    //public void run() {
    // elimina el control de cambio del codigo auxiliar desde el codigo
    adaptee.txtCodEquNot.removeTextListener(adaptee.txtTextListener);

    if ( ( (TextField) e.getSource()).getName() == "txtCodEqNot") {
      adaptee.txtCodEquNot_actionPerformed(e); //PDP 12/04/2000
      adaptee.txtBuscarEquNot_actionPerformed(null);
    }

    // activa el control de cambio del codigo auxiliar

    adaptee.txtCodEquNot.addTextListener(adaptee.txtTextListener);
  }
}

// Focus listener para las cajas de texto
class Panel1txtFocusListener
    implements FocusListener { //, Runnable {
  Panel1 adaptee = null;
  FocusEvent e = null;

  public Panel1txtFocusListener(Panel1 adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void focusGained(FocusEvent e) {
  }

  // evento
  public void focusLost(FocusEvent e) {
    this.e = e;

    //new Thread(this).start();
    //}

    // hilo de ejecuci�n para servir el evento
    //public void run() {
    // elimina el control de cambio del codigo auxiliar desde el codigo
    adaptee.txtCodEquNot.removeTextListener(adaptee.txtTextListener);

    if ( ( (TextField) e.getSource()).getName() == "txtCodEqNot") {
      //adaptee.txtCodEquNot_actionPerformed(null);
      adaptee.txtBuscarEquNot_actionPerformed(null);
    }

    // activa el control de cambio del codigo auxiliar

    adaptee.txtCodEquNot.addTextListener(adaptee.txtTextListener);
  }
}

// cambio del c�digo auxiliar
class Panel1TextListener
    implements TextListener {
  Panel1 adaptee = null;
  TextEvent e = null;

  public Panel1TextListener(Panel1 adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void textValueChanged(TextEvent e) {
    adaptee.txt_textValueChanged(e);
  }
}

class Panel1_choicePer_itemAdapter
    implements java.awt.event.ItemListener {
  Panel1 adaptee;

  Panel1_choicePer_itemAdapter(Panel1 adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    String name2 = ( (Component) e.getSource()).getName();
    if (name2.equals("choicePer")) {
      adaptee.choicePer_itemStateChanged(e);
    }
    else if (name2.equals("chkbxBaj")) {
      adaptee.chkbxBaj_itemStateChanged(e);
    }
  }
} //_______ END CLASS

// operaci�nes sobre las listas
class Panel1ItemListener
    implements ItemListener { //, Runnable {
  Panel1 adaptee = null;
  ItemEvent e = null;

  public Panel1ItemListener(Panel1 adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(ItemEvent e) {
    if (e.getStateChange() == ItemEvent.SELECTED) {
      this.e = e;

      //new Thread(this).start();
      //}
      //}

      // hilo de ejecuci�n para servir el evento
      //public void run() {
      // elimina el control de cambio del codigo auxiliar desde el codigo
      adaptee.txtCodEquNot.removeTextListener(adaptee.txtTextListener);

      if ( ( (java.awt.List) e.getSource()).getName() == "lstAmbUsu") {
        adaptee.lstAmbUsu_itemStateChanged(e);

        // activa el control de cambio del codigo auxiliar
      }
      adaptee.txtCodEquNot.addTextListener(adaptee.txtTextListener);
    }
  }
}

class CListaEntradaEDO
    extends capp.CListaValores {

  public CListaEntradaEDO(capp.CApp a,
                          String title,
                          sapp.StubSrvBD stub,
                          String servlet,
                          int obtener_x_codigo,
                          int obtener_x_descripcion,
                          int seleccion_x_codigo,
                          int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String filtro) {
    return new DataEntradaEDO(filtro);
  }

  public String getCodigo(Object o) {
    return ( (DataEntradaEDO) o).getCod();
  }

  public String getDescripcion(Object o) {
    return ( (DataEntradaEDO) o).getDes();
  }
} //_____ END CLASS

/*
 *  esta clase se lanza como un thread para traer los datos
 */

class LeerNiveles
    implements Runnable {
  protected Panel1 adaptee = null;
  // Comunicaci�n con el servlet
  public sapp.StubSrvBD stubCliente = null;

  //se inicia la lista no puede ser null

  public LeerNiveles(Panel1 app) {
    adaptee = app;
    run();
  }

  public void run() {

    capp.CLista parametros = null;
    capp.CLista result = null;
    stubCliente = new sapp.StubSrvBD();
    /// lista de Nivel1
    try {
      parametros = new capp.CLista();
      result = new capp.CLista();
      parametros.addElement(new DataEnfermo("CD_NIVEL_1"));

//          URL u = new URL (adaptee.getApp().getParametro("URL_SERVLET") + constantes.strSERVLET_ENFERMO);

      URL u = new URL(adaptee.getApp().getParametro("URL_SERVLET") +
                      comun.strSERVLET_ENFERMO);
      stubCliente.setUrl(u);

      /*
                 enfermo.SrvEnfermo servlet = new enfermo.SrvEnfermo();
                 //Indica como conectarse a la b.datos
                 servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                 "sive_desa",
                                 "sive_desa");
           result = (capp.CLista) servlet.doDebug(SrvEnfermo.modoNivel1, parametros);
       */

      result = (capp.CLista) stubCliente.doPost(SrvEnfermo.modoNivel1,
                                                parametros);

      adaptee.setListaNivel1(result);

    }
    catch (Exception exc) {
      //#// System_out.println("Error trayendo Nivel 1");
    }

    /// lista de Nivel2
    try {
      parametros = new capp.CLista();
      result = new capp.CLista();
      parametros.addElement(new DataEnfermo("CD_NIVEL_2"));

//          URL u = new URL (adaptee.getApp().getParametro("URL_SERVLET") + constantes.strSERVLET_ENFERMO);
      URL u = new URL(adaptee.getApp().getParametro("URL_SERVLET") +
                      comun.strSERVLET_ENFERMO);
      stubCliente.setUrl(u);

      result = (capp.CLista) stubCliente.doPost(SrvEnfermo.modoNivel2,
                                                parametros);

      //result = (capp.CLista) comun.traerDatos(adaptee.getApp(), stubCliente, constantes.strSERVLET_ENFERMO, SrvEnfermo.modoNivel1, parametros);
      adaptee.setListaNivel2(result);

    }
    catch (Exception exc) {
      //#// System_out.println("Error trayendo Nivel 2");
    }
  }

} // ENd Class

class Panel1_txtCodEquNot_keyAdapter
    extends java.awt.event.KeyAdapter {
  Panel1 adaptee;

  Panel1_txtCodEquNot_keyAdapter(Panel1 adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCodEquNot_keyPressed(e);
  }
}
