//Title:        MAntenimiento de Usuarios
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Jesus Garcia  & Luis Rivera
//Company:      Norsistemas
//Description:  Applet que se encarga del mantenimiento de usuarios (en carpetas)
/**
 * @ADAPTACION DEL ANTIGUO MANTUS-PISTA AL UTILIZADO EN ICM99 PDP 14/04/2000
 * @version 1.0
 */
package mantus;

import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Label;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CPanel;

public class Panel2
    extends CPanel {

//modos de pantalla y consultas
  final int modoALTA = 0;
  ResourceBundle res;
  final int modoMODIFICAR = 1;
  final int modoSELECCION = 2; //Usado s�lo como modo oper. con sevlett
  final int modoOBTENER = 4;
  final int modoESPERA = 5; // Usado solo como modo pantalla
  final int modoSELECCION_EQU_NOT = 6; // Usado solo como modo pantalla
  //El opmode de consulta ser� el correspondiente al servlett que consulta el eq not y variar� la URL: Va al url del eq not)

  final int modoGENERAR_LOGIN = 8;

  protected int modoOperacion = modoALTA;

  XYLayout xYLayout1 = new XYLayout();
  Checkbox chkbxAutAlt = new Checkbox();
  Checkbox chkbxAutMod = new Checkbox();
  Checkbox chkbxAutBaj = new Checkbox();
  Checkbox chkbxEnf = new Checkbox();
  Checkbox chkbxManCat = new Checkbox();
  Checkbox chkbxManUsu = new Checkbox();
  Checkbox chkbxTraPro = new Checkbox();
  Checkbox chkbxDefPro = new Checkbox();
  Checkbox chkbxAla = new Checkbox();
  Checkbox chkbxVal = new Checkbox();
  Checkbox chkbxGenAlauto = new Checkbox();
  Checkbox chkbxManNotifs = new Checkbox();
  Checkbox chkbxConsRes = new Checkbox();
  Label label1 = new Label();
  Label label2 = new Label();
  Label label3 = new Label();
  Checkbox chkbxExport = new Checkbox();
  Label label4 = new Label();

  public Panel2(CApp a, DialUsuario a_diusu) {
    try {
      setApp(a);
      res = ResourceBundle.getBundle("mantus.Res" + app.getIdioma());
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    xYLayout1.setHeight(398);
    xYLayout1.setWidth(452);
    chkbxAutAlt.setLabel(res.getString("chkbxAutAlt.Label"));
    chkbxAutMod.setLabel(res.getString("chkbxAutMod.Label"));
    chkbxAutBaj.setLabel(res.getString("chkbxAutBaj.Label"));
    chkbxEnf.setLabel(res.getString("chkbxEnf.Label"));
    chkbxManCat.setLabel(res.getString("chkbxManCat.Label"));
    chkbxManUsu.setLabel(res.getString("chkbxManUsu.Label"));
    chkbxTraPro.setLabel(res.getString("chkbxTraPro.Label"));
    chkbxDefPro.setLabel(res.getString("chkbxDefPro.Label"));
    chkbxAla.setLabel(res.getString("chkbxAla.Label"));
    chkbxVal.setLabel(res.getString("chkbxVal.Label"));
    chkbxGenAlauto.setLabel(res.getString("chkbxGenAlauto.Label"));
    chkbxManNotifs.setLabel(res.getString("chkbxManNotifs.Label"));
    chkbxConsRes.setLabel(res.getString("chkbxConsRes.Label"));
    label1.setFont(new Font("Dialog", 1, 12));
    label1.setText(res.getString("label1B.Text"));
    label2.setFont(new Font("Dialog", 1, 12));
    label2.setText(res.getString("label2B.Text"));
    label3.setFont(new Font("Dialog", 1, 12));
    label3.setText(res.getString("label3B.Text"));
    chkbxExport.setLabel(res.getString("chkbxExport.Label"));
    label4.setFont(new Font("Dialog", 1, 12));
    label4.setText(res.getString("label4B.Text"));
    this.setLayout(xYLayout1);
    this.add(chkbxAutAlt, new XYConstraints(19, 52, -1, -1));
    this.add(chkbxAutMod, new XYConstraints(101, 52, -1, -1));
    this.add(chkbxAutBaj, new XYConstraints(221, 52, -1, -1));
    this.add(chkbxEnf, new XYConstraints(18, 367, 270, -1));
    this.add(chkbxManCat, new XYConstraints(220, 126, 133, -1));
    this.add(chkbxManUsu, new XYConstraints(20, 127, 75, -1));
    this.add(chkbxTraPro, new XYConstraints(18, 303, 190, -1));
    this.add(chkbxDefPro, new XYConstraints(20, 199, 166, -1));
    this.add(chkbxAla, new XYConstraints(19, 230, 193, -1));
    this.add(chkbxVal, new XYConstraints(294, 52, 96, -1));
    this.add(chkbxGenAlauto, new XYConstraints(222, 231, 225, -1));
    this.add(chkbxManNotifs, new XYConstraints(100, 127, 108, -1));
    this.add(chkbxConsRes, new XYConstraints(222, 304, 165, 22));
    this.add(label4, new XYConstraints(18, 274, 111, -1));
    this.add(label3, new XYConstraints(18, 169, 108, -1));
    this.add(label2, new XYConstraints(18, 96, 102, -1));
    this.add(label1, new XYConstraints(21, 23, 107, -1));
    this.add(chkbxExport, new XYConstraints(18, 335, 190, -1));

    this.modoOperacion = modoALTA;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {

      case modoALTA:

        chkbxAla.setEnabled(true);
        chkbxAutAlt.setEnabled(true);
        chkbxAutBaj.setEnabled(true);
        chkbxDefPro.setEnabled(true);
        chkbxManCat.setEnabled(true);
        chkbxManUsu.setEnabled(true);
        chkbxAutMod.setEnabled(true);
        chkbxTraPro.setEnabled(true);
        chkbxVal.setEnabled(true);
        chkbxEnf.setEnabled(true);
        //QQ:
        chkbxExport.setEnabled(true);
        chkbxGenAlauto.setEnabled(true);
        chkbxManNotifs.setEnabled(true);
        chkbxConsRes.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR)); //??????????????????
        break;

      case modoMODIFICAR:

        chkbxAla.setEnabled(true);
        chkbxAutAlt.setEnabled(true);
        chkbxAutBaj.setEnabled(true);
        chkbxDefPro.setEnabled(true);
        chkbxManCat.setEnabled(true);
        chkbxManUsu.setEnabled(true);
        chkbxAutMod.setEnabled(true);
        chkbxTraPro.setEnabled(true);
        chkbxVal.setEnabled(true);
        chkbxEnf.setEnabled(true);
        //QQ:
        chkbxExport.setEnabled(true);
        chkbxGenAlauto.setEnabled(true);
        chkbxManNotifs.setEnabled(true);
        chkbxConsRes.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR)); //??????????????????
        break;

      case modoESPERA:

        chkbxAla.setEnabled(false);
        chkbxAutAlt.setEnabled(false);
        chkbxAutBaj.setEnabled(false);
        chkbxDefPro.setEnabled(false);
        chkbxManCat.setEnabled(false);
        chkbxManUsu.setEnabled(false);
        chkbxAutMod.setEnabled(false);
        chkbxTraPro.setEnabled(false);
        chkbxVal.setEnabled(false);
        chkbxEnf.setEnabled(false);
        //QQ:
        chkbxExport.setEnabled(false);
        chkbxGenAlauto.setEnabled(false);
        chkbxManNotifs.setEnabled(false);
        chkbxConsRes.setEnabled(false);
        break;
    }

  }

  public void borra() {
    this.chkbxAla.setState(false);
    this.chkbxAutAlt.setState(false);
    this.chkbxAutBaj.setState(false);
    this.chkbxDefPro.setState(false);
    this.chkbxManCat.setState(false);
    this.chkbxManUsu.setState(false);
    this.chkbxAutMod.setState(false);
    this.chkbxTraPro.setState(false);
    this.chkbxVal.setState(false);
    this.chkbxEnf.setState(false);
    //QQ
    this.chkbxExport.setState(false);
    this.chkbxGenAlauto.setState(false);
    this.chkbxManNotifs.setState(false);
    this.chkbxConsRes.setState(false);
  }

}
