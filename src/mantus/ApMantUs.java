package mantus;

import capp2.CApp;

public class ApMantUs
    extends CApp {

  public ApMantUs() {
    super();
  }

  public void init() {
    super.init();
    setTitulo("Mantenimiento de usuarios");
    PanelBusca pnlUsuarios = new PanelBusca( (CApp)this);
    //pnlUsuariosTub.setBorde(false);
    VerPanel("", pnlUsuarios);
  }
}
