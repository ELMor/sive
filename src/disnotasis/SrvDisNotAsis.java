
package disnotasis;

import java.sql.SQLException;
import java.util.Vector;

import Registro.RegistroConsultas;
import capp.CApp;
import capp.CLista;
import conspacenf.Data;
import conspacenf.SrvGen;
import sapp.DBServlet;

/**
 *  Esta clase est� en el servidor recibe una CLista
 *  recoge de ella el nombre de la clase de datos y los par�metros
 *  ejecuta la sentencia SQL
 *  rellena una CLISTA con objetos de datos
 *  y finaliza la conexi�n
 */
public class SrvDisNotAsis
    extends SrvGen {

  /** lista con las posibles enfermedades */
  protected CLista lenfermedades = null;

  public static final int A_ESPECIALIZADA = 1;
  public static final int A_PRIMARIA = 2;
  public static final int A_OTROS = 3;

  public static final String STR_A_ESPECIALIZADA = "E";
  public static final String STR_A_PRIMARIA = "P";

  //Total registros
  String str_elto_count = null;

  // ARG:
  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  public void SrvDisNotAsis() {
    con = null;
  }

  protected CLista doWork(int opmode, CLista param) throws Exception {
    this.param = param;
    String descrip = null;
    Data hash = null;
    CLista result = null;
    this.param = param;
    Data paramDat = null;
    String num_reg = null;
    Object o = null;

    String user = "";
    String passwd = "";
    boolean lortad;

    //E //# // System_out.println("ReportC3:2 bienvenido al servlet Report");

    // System_out.println("______________________ Comienzo servlet SRVDISNOTASIS");

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;

    }

    if (param == null || param.size() < 1) {
      // no nos han pasado datos iniciales
      return null;
    }
    else {
      paramDat = (Data) param.firstElement();
    }

    if (breal) {
      con = openConnection();
      /*
                 if (aplicarLortad)
                 {
         // Se obtiene la clave de acceso del usuario que se ha conectado
         passwd = sapp.Funciones.getPassword(con, st, rs, user);
         closeConnection(con);
         con=null;
         // Se abre una nueva conexion para el usuario
         con = openConnection(user, passwd);
                }
       */
    }

    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    try {
      ///establecemos las propiedades indicadas
      ///______________________________________
      con.setAutoCommit(true);

      if (opmode == DataDisNotAsis.modoDISNOTASISCOUNT ||
          opmode == DataDisNotAsis.modoDISNOTASIS) {
        DataDisNotAsis disNotAsis = null;
        //Se crea lista que se devolver�
        result = new CLista();
        String strDS_PROCESO, strDS_AP_1, strDS_AP_2, strDS_AE_1;
        ///______________________________________
        String strDSL_PROCESO; //L Rivera (idiomas)

        String strDS_AE_2, strDS_ON_1, strDS_ON_2;
        Vector vEquiposAP, vEquiposAE, vEquiposON;
        Vector vCasosEdo;
        int inipag = 0, finpag = 0;

        // traemos la lista de enfermedades individuales
        //Si se pagina s�lo llegan las que van en una p�gina
        lenfermedades = new CLista();
        DataEnfermedad denf = new DataEnfermedad(DataEnfermedad.CD_ENFCIE);
        denf.setNumParam(1);
        denf.put(DataEnfermedad.P_CD_TVIGI, DataEnfermedad.TGVIGI_SI);
        //LRG ___
        denf.put(DataEnfermedad.P_PAGINA, paramDat.get(DataEnfermedad.P_PAGINA));
        denf.bInformeCompleto = paramDat.bInformeCompleto;
        //______
        lenfermedades.addElement(denf);
        StringBuffer strSQL = new StringBuffer();
        int iNumReg = denf.prepareSQLSentencia(DataEnfermedad.modoENFERMEDAD,
                                               strSQL, lenfermedades);

        //NOTA: Se llama al m�todos implementados en clase padre SrvGen
        // Se extraen par�metros de consulta del primer elemento de lenfermedades
        lenfermedades = realiza_SQL(DataEnfermedad.modoENFERMEDAD, 99999,
                                    strSQL.toString(), lenfermedades);

        // traemos los equipos de atenci�n primaria, distrito y area
        vEquiposAP = traeEquipos(paramDat, A_PRIMARIA);
        // traemos los equipos de atenci�n especializada, distrito y area
        vEquiposAE = traeEquipos(paramDat, A_ESPECIALIZADA);
        // traemos los equipos de otros notificadores
        vEquiposON = traeEquipos(paramDat, A_OTROS);

        //_______________________________________________________________

        // recorremos la lista de enfermedades
        for (int i = 0; i < lenfermedades.size(); i++) { //LRG

          denf = (DataEnfermedad) lenfermedades.elementAt(i);
          strDS_PROCESO = (String) denf.get(DataEnfermedad.DS_PROCESO);
          // obtiene la descripcion auxiliar en funci�n del idioma
          if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
            strDSL_PROCESO = (String) denf.get(DataEnfermedad.DSL_PROCESO);
            if (strDSL_PROCESO != null) {
              strDS_PROCESO = strDSL_PROCESO;
            }
          }

          // traemos los casos edo SIVE_EDOIND filtrado por CD_ENFCIE, CD_ANOEPI, CD_SEMEPI
          vCasosEdo = traeCasos(paramDat,
                                (String) denf.get(DataEnfermedad.CD_ENFCIE));

          trazaLog("traigo informaci�n de count de primaria 1� notificador");
          strDS_AP_1 = traeCount(vCasosEdo, vEquiposAP, true);
          strDS_AP_2 = traeCount(vCasosEdo, vEquiposAP, false);
          strDS_AE_1 = traeCount(vCasosEdo, vEquiposAE, true);
          strDS_AE_2 = traeCount(vCasosEdo, vEquiposAE, false);
          strDS_ON_1 = traeCount(vCasosEdo, vEquiposON, true);
          strDS_ON_2 = traeCount(vCasosEdo, vEquiposON, false);

          // rellenamos el dato
          disNotAsis = new DataDisNotAsis(DataDisNotAsis.DS_PROCESO);
          disNotAsis.put(DataDisNotAsis.COUNT, str_elto_count);
          disNotAsis.put(DataDisNotAsis.DS_PROCESO, strDS_PROCESO);
          disNotAsis.put(DataDisNotAsis.DS_AP_1, strDS_AP_1);
          disNotAsis.put(DataDisNotAsis.DS_AP_2, strDS_AP_2);
          disNotAsis.put(DataDisNotAsis.DS_AE_1, strDS_AE_1);
          disNotAsis.put(DataDisNotAsis.DS_AE_2, strDS_AE_2);
          disNotAsis.put(DataDisNotAsis.DS_ON_1, strDS_ON_1);
          disNotAsis.put(DataDisNotAsis.DS_ON_2, strDS_ON_2);
          result.addElement(disNotAsis);

          // System_out.println("**** En i " + i + " a�adida a result " + disNotAsis.get(DataDisNotAsis.DS_PROCESO) );

        }
      }

      CLista l = ( (CLista) result); // prueba

      //Se pasan los atributos de lista lenfermedades (temporal)
      //a lista que se devuelve    (LRG)
      result.setState(lenfermedades.getState());
      result.setFilter(lenfermedades.getFilter());

      ////# // System_out.println ("compreobamos");
    }
    catch (Exception exc) {
      String dd = exc.toString();
      throw exc; //LRG
      //E //# // System_out.println("SrvDisNotAsis Error: "+ exc.toString());
    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    ////# // System_out.println("SrvDisNotAsis adios" + result);
    return result;
  }

  //__________________________________________________________________________

  /**
   *  esta funci�n realiza fetch de los datos leidos
   * y los va metiendo en la estructura solicitada
   *  pagina seg�n el par�metro 0
   *
   * @param paramData es un clase Data que tiene los par�metros iniciales
   * @return devuelve una CLista con todos los datos
   */
  public CLista recogerDatos(Data paramData) {
    CLista result = new CLista();
    Data h;
    Object o;
    int num_datos = 0;
    int campo_filtro = 0;
    int i;

    ////# // System_out.println("Report: entramos en recoger datos");
    //recogemos el par�metro de paginaci�n

    //CUIDADO : paramData ES LOCAL A ESTE METODO, NO GLOBAL
    //ESTRUCTURA PARAMDATA ES EL PRIMER ELEMENTEO DE LISTA PASADA AL METODO realiza_SQL

    String pag = (String) paramData.get(DataDisNotAsis.P_PAGINA);
    int ini_pagina = 0;
    int fin_pagina = 0;
    if (pag != null) {
      ini_pagina = Integer.parseInt(pag);
      // System_out.println("*******En query  pagina "+ ini_pagina );
    }
    ini_pagina = (ini_pagina * DBServlet.maxSIZE);
    // System_out.println("*******En query ini_pagina "+ ini_pagina );
    fin_pagina = ini_pagina + DBServlet.maxSIZE;
    // System_out.println("*******En query fin_pagina "+ fin_pagina );

    try {
      for (i = 0; rs.next(); i++) {
        // control de tama�o
        if ( (i >= fin_pagina) && (! (paramData.bInformeCompleto))) { //DBServlet.maxSIZE) {
          result.setState(CLista.listaINCOMPLETA);
          campo_filtro = ( (Data) result.lastElement()).getCampoFiltro();
          o = ( (Data) result.lastElement()).get(campo_filtro);
          result.setFilter(o.toString());
          // System_out.println("*******En i "+i + " lista INCOMPLETA . Filtro  " + o.toString());
          // no guardamos los datos (se enviar�n en p�ginas siguientes)
          //break;  (LRG)
          continue;
        }
        //LRG
        else if ( (i < ini_pagina) && (! (paramData.bInformeCompleto))) {
          // no guardamos los datos (ya enviados en p�ginas anteriores)
          continue;
        }

        // de la clase hija nos da los datos del servlet
        // copiamosla clase hija paraque conserve el filtrado
        h = (Data) paramData.getNewData();
        int num_cols = rs.getMetaData().getColumnCount();
        for (int j = 1; j <= num_cols; j++) {
          o = rs.getObject(j);
          if (o != null) { // si es null no lo metemos
            h.put(j, o);
            // System_out.println("*******En i "+ i+ " A�adido elemento " + o.toString());
          }
        }
        result.addElement(h);
      } //orf

      // vemos si est� llena la lista
      if (i < fin_pagina) {
        result.setState(CLista.listaLLENA);
      }

      //N�mero total de registros (LRG)
      str_elto_count = Integer.toString(i);

      ////# // System_out.println("Report:salimos bien");
      return result;
    }
    catch (SQLException exc) {
      String dd = exc.toString();
      exc.printStackTrace();
      return null;

    }
  }

  //__________________________________________________________________________

  /**
   *  esta funci�n va leyendo los par�metros de Data de entrada
   *  y los prepara, no incluye paginaci�n
   */
  protected void fijarParametros(Data paramData, int a_modo) {
    int tipoDato = 0;
    Object dato = null;
    int i = 0;

    boolean is_like = (a_modo == servletSELECCION_X_CODIGO) ||
        (a_modo == servletSELECCION_X_DESCRIPCION);
////# // System_out.println("Report:: fijaParametros" );
    // ponemos los par�metros fijados por el usuario
    for (i = 1; i <= paramData.getNumParam(); i++) {
      try {
        dato = paramData.get(i);
        if (dato != null) {
          // System_out.println("******** Param " +i + ": " + dato);
          st.setObject(i, dato);
        }
        else {
          // System_out.println("******* Param " +i + ":   Nulo");
          st.setNull(i, java.sql.Types.VARCHAR);
        }
      }
      catch (Exception exc) {
        String dd = exc.toString();
        //E //# // System_out.println("Report: Fallo paramtero "+ exc.toString());
      }
    }
  }

  //__________________________________________________________________________
  /**
   *   esta funci�n trae el c�digo de los equipos cuyos centros pertenecen a una zona y calificacion
   *
   * @param vCasosEdo ,  es el vector que indica los casos a filtrar
       * @param vEquipos ,  es el vetor que indica los c�digos de equipos que filtran
   * @param bItPrimero, es true si se quieren los notificadores primeros
   * @return devuelve un array con los c�digos de los equipos
   */
  public Vector traeEquipos(Data param, int especializada) {
    Object o = null;
    Vector result = new Vector();
    String strNivel1 = (String) param.get(DataDisNotAsis.P_CD_NIVEL_1);
    String strNivel2 = (String) param.get(DataDisNotAsis.P_CD_NIVEL_2);

    StringBuffer strBufSQL = new StringBuffer(
        "SELECT CD_E_NOTIF FROM SIVE_E_NOTIF WHERE CD_CENTRO IN " +
        " (SELECT CD_CENTRO FROM SIVE_C_NOTIF WHERE ");
    //Si es atenci�n especializada o primaria se a�ade filtro con el c�digo corresponediente
    if (especializada == A_ESPECIALIZADA || especializada == A_PRIMARIA) {
      strBufSQL.append(" CD_NIVASIS = ? ) ");
    }
    //Si es otras atenciones se pone filtro para indicar no es especializada ni primaria (LRG)
    else {
      strBufSQL.append(" CD_NIVASIS != ? AND CD_NIVASIS != ?  ) ");
    }
    if (strNivel1 != null) {
      strBufSQL.append(" AND CD_NIVEL_1 = ? ");
    }
    if (strNivel2 != null) {
      strBufSQL.append(" AND CD_NIVEL_2 = ? ");
    }

    try {

      // System_out.println("QUERY**** " + strBufSQL.toString() );

      st = con.prepareStatement(strBufSQL.toString());

      //ponemos los par�metros
      int iNumParam = 1;
      if (especializada == A_ESPECIALIZADA) {
        st.setString(iNumParam++, STR_A_ESPECIALIZADA);

      }
      else if (especializada == A_PRIMARIA) {
        st.setString(iNumParam++, STR_A_PRIMARIA);
      }
      else {
        st.setString(iNumParam++, STR_A_ESPECIALIZADA);
        st.setString(iNumParam++, STR_A_PRIMARIA);
      }
      if (strNivel1 != null) {
        st.setString(iNumParam++, strNivel1);
      }
      if (strNivel2 != null) {
        st.setString(iNumParam++, strNivel1);
      }

      // ejecutamos la sentencia
      rs = st.executeQuery();

      // leemos el count
      while (rs.next()) {
        result.addElement(rs.getObject(1));
        trazaLog("traigo equipos de " + especializada);
      }
      rs.close();
      rs = null;
      st.close();
      st = null;
    }
    catch (Exception exc) {
      //E //# // System_out.println("ERROR traeEpquipos " + exc.toString());
    }

    return result;
  }

  //__________________________________________________________________________
  /**
   *   esta funci�n ejecuta la sentencia de count segun las condiciones que se le pasan
   *
   * @param vCasosEdo ,  es el vector que indica los casos a filtrar
       * @param vEquipos ,  es el vetor que indica los c�digos de equipos que filtran
   * @param bItPrimero, es true si se quieren los notificadores primeros
   * @return devuelve en formato String, la cantidad de datos que hay
   */
  public String traeCount(Vector vCasosEdo, Vector vEquipos, boolean bItPrimero) {
    StringBuffer strBufSQL = new StringBuffer(
        "SELECT COUNT(*) FROM SIVE_NOTIF_EDOI WHERE CD_FUENTE = 'E' AND NM_EDO IN ( ? ");
    Object o = null;

    if (vCasosEdo == null || vCasosEdo.size() == 0 ||
        vEquipos == null || vEquipos.size() == 0) {
      return "0";
    }

    // ponemos las interrogaciones de los par�metros
    // empezamos en uno porque ya hemos puesto la primera interrogacion
    for (int i = 1; i < vCasosEdo.size(); i++) {
      strBufSQL.append(", ? ");
    }

    strBufSQL.append(") AND CD_E_NOTIF IN ( ? ");
    for (int i = 1; i < vEquipos.size(); i++) {
      strBufSQL.append(", ? ");
    }
    strBufSQL.append(") AND IT_PRIMERO = ? ");

    try {

      // System_out.println("QUERY**** " + strBufSQL.toString() );

      st = con.prepareStatement(strBufSQL.toString());

      //ponemos los par�metros
      int lenCasosEdo = vCasosEdo.size();
      for (int i = 0; i < lenCasosEdo; i++) {
        o = vCasosEdo.elementAt(i);
        if (o != null) {
          // System_out.println("******** Param " +(i+1) + ": " + o);
          st.setObject(i + 1, o);
        }
        else {
          // System_out.println("******** Param " +(i+1) + ": " + "Nulo");
          st.setNull(i + 1, java.sql.Types.VARCHAR);
        }
      }
      lenCasosEdo++;
      for (int i = 0; i < vEquipos.size(); i++) {
        o = vEquipos.elementAt(i);
        if (o != null) {
          // System_out.println("******** Param " +(i+lenCasosEdo) + ": " + o);
          st.setObject(i + lenCasosEdo, o);
        }
        else {
          // System_out.println("******** Param " +(i+lenCasosEdo) + ": " + "Nulo");
          st.setNull(i + lenCasosEdo, java.sql.Types.VARCHAR);
        }
      }

      if (bItPrimero) {
        st.setString(vEquipos.size() + lenCasosEdo, "S");
      }
      else {
        st.setString(vEquipos.size() + lenCasosEdo, "N");
      }

      // ejecutamos la sentencia
      rs = st.executeQuery();

      // leemos el count
      if (rs.next()) {
        o = rs.getObject(1);
        trazaLog("count de primaria 1� notificador = " + o.toString());
      }
      rs.close();
      rs = null;
      st.close();
      st = null;
    }
    catch (Exception exc) {
      String dd = exc.toString();
      o = null;
    }

    if (o != null) {
      return o.toString();
    }
    else {
      return "0";
    }
  }

  //__________________________________________________________________________

  /**
   *   esta funci�n trae los c�digos de los casos cuyos incluidos en la fecha y enfermedad
   *
   * @return devuelve un vector con los c�digos de los casos
   */
  public Vector traeCasos(Data param, String str_cd_enfcie) {
    Object o = null;
    Vector result = new Vector();

    StringBuffer strBufSQL = new StringBuffer(
        "SELECT NM_EDO FROM SIVE_EDOIND WHERE CD_ENFCIE = ? AND ");

    String hasta_ano = (String) param.get(DataDisNotAsis.P_HASTA_CD_ANOEPI5);
    String desde_ano = (String) param.get(DataDisNotAsis.P_DESDE_CD_ANOEPI1);

    if (hasta_ano.equals(desde_ano)) {
      strBufSQL.append(DataEnfermedad.filtroIguales);

    }
    else {
      strBufSQL.append(DataEnfermedad.filtroNoIguales);
    }

    try {
      // System_out.println("QUERY**** " + strBufSQL.toString() );
      st = con.prepareStatement(strBufSQL.toString());

      //ponemos los par�metros
      int iNumParam = 1;
      // System_out.println("******** Param " +(iNumParam) + ": " + str_cd_enfcie);
      st.setString(iNumParam++, str_cd_enfcie);

      // ARG: Se a�aden parametros
      registroConsultas.insertarParametro(str_cd_enfcie);

      for (int i = 1; i < 7; i++) {
        st.setString(iNumParam++, (String) param.get(i));

        // ARG: Se a�aden parametros
        registroConsultas.insertarParametro( (String) param.get(i));

        // System_out.println("******** Param " +(iNumParam) + ": " + (String) param.get(i));

      }

      // ejecutamos la sentencia
      rs = st.executeQuery();

      registroConsultas.registrar("SIVE_EDOIND", strBufSQL.toString(),
                                  "CD_ENFERMO",
                                  "", "SrvDisNotAsis", true);

      // leemos el count
      while (rs.next()) {
        result.addElement(rs.getObject(1));
      }
      rs.close();
      rs = null;
      st.close();
      st = null;
    }
    catch (Exception exc) {
      //E //# // System_out.println("ERROR traeEpquipos " + exc.toString());
    }

    return result;
  }

  //__________________________________________________________________________

  /** prueba del servlet */
  static public void main(String[] args) {
    try {
      CLista data = new CLista();
      DataDisNotAsis d = new DataDisNotAsis(DataDisNotAsis.DS_PROCESO);
      d.setNumParam(7); //11);   //DataDisNotAsis.P_NUM_C3);
      d.put(DataDisNotAsis.P_PAGINA, "0");
      d.put(DataDisNotAsis.P_DESDE_CD_ANOEPI1, "1999");
      d.put(DataDisNotAsis.P_DESDE_CD_SEMEPI2, "01");
      d.put(DataDisNotAsis.P_DESDE_CD_ANOEPI3, "1999");
      d.put(DataDisNotAsis.P_HASTA_CD_ANOEPI4, "1999");
      d.put(DataDisNotAsis.P_HASTA_CD_ANOEPI5, "1999");
      d.put(DataDisNotAsis.P_HASTA_CD_SEMEPI6, "14");
      /*     d.put(DataDisNotAsis.P_CD_C_NOTIF, "'2%'");
           d.put(DataDisNotAsis.P_CD_E_NOTIF, "'%'");
           d.put(DataDisNotAsis.P_CD_PROV, "'%'");
           d.put(DataDisNotAsis.P_CD_MUN, "'%'");
           d.put(DataDisNotAsis.P_CD_NIVEL_1, "'%'");
           d.put(DataDisNotAsis.P_CD_NIVEL_2, "'%'");
           d.put(DataDisNotAsis.P_CD_ZBS, "'%'");
       */data.addElement(d);

      SrvDisNotAsis srv = new SrvDisNotAsis();

      CLista dd = srv.doPrueba(DataDisNotAsis.modoDISNOTASISCOUNT, data);
      String ss = "Lista: " + dd;
      ////# // System_out.println(ss);

      int i = 33;

    }
    catch (Exception exc) {
      String ss = "fallo " + exc.toString();
      //E //# // System_out.println("C3 Error " + ss);
    }
  }

} //________________________________________________ END_CLASS
