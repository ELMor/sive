
package disnotasis;

import capp.CLista;
import conspacenf.Data;
import sapp.DBServlet;

/** Distribuci�n de los casos notificados seg�n nivel asistencial
 *
 *  filtros:  desde a�o y semana
 *            hasta a�o y semana
 *            enfermedad
 *            distrito
 *            �rea  // por defecto Comunidad de Madrid
 *
 */
public class DataEnfermedad
    extends Data {

  /** campos que va a tener */
  public static final int COUNT = 0;
  public static final int DS_PROCESO = 1; // enfermedad
  public static final int CD_ENFCIE = 2;
//___________ L Rivera
  public static final int DSL_PROCESO = 3; // enfermedad
//________________________

  /** parametros que tiene */
  // n�mero de par�metros de esta sentencia
  public static final int P_NUM_C3 = 3;

  // posici�n de los diferentes par�metrso en la select
  public static final int P_PAGINA = 0; // indica la p�gina de datos que se quiere traer
  public static final int P_CD_TVIGI = 1; // sis e vigila o no
  /** modos que tiene */
  public static final int modoENFERMEDAD = 1;
  public static final int modoENFERMEDADCOUNT = 2;

  /** texto para indicar bajo vigilancia CD_TVIGI */
  public static final String TGVIGI_SI = "I";

  public static String filtroNoIguales =
      " ( ( CD_ANOEPI = ? AND CD_SEMEPI >= ? ) OR " +
      " ( CD_ANOEPI > ? AND CD_ANOEPI < ? ) OR " +
      " ( CD_ANOEPI = ? AND CD_SEMEPI <= ? ) ) ";

  public static String filtroIguales =
      " ( CD_ANOEPI = ? AND CD_SEMEPI >= ? AND " +
      "  CD_ANOEPI = ? AND CD_ANOEPI = ?  AND " +
      "  CD_ANOEPI = ? AND CD_SEMEPI <= ?  ) ";

  public DataEnfermedad(int a_campo_filtro) {
    super(a_campo_filtro);
    // siempre hay que reservar
    arrayDatos = new Object[10];
  }

  public Object getNewData() {
    return new DataEnfermedad(campo_filtro);
  }

  /** devuelve el c�digo de un modo e funcionamiento */
  public String getCode(int a_modo) {
    return "CD_ENFCIE";
  }

  /** devuelve la descripci�n de un modo de funcionamiento */
  public String getDes(int a_modo) {
    return null;
  }

  /** indica si ya lleva where la select */
  public boolean getWhere(int a_modo) {
    return true;
  }

  /** esta funci�n devuelve la sentencia a ejecutar */
  public int prepareSQLSentencia(int opmode, StringBuffer strSQL,
                                 CLista ini_param) {

    switch (opmode) {
      case modoENFERMEDAD:

        /*
                     strSQL.append("SELECT DS_PROCESO, CD_ENFCIE FROM SIVE_PROCESOS WHERE CD_ENFCIE IN "+
             " (SELECT CD_ENFCIE FROM SIVE_ENFEREDO WHERE CD_TVIGI = ? ) ");
         */
//__________________________________
        //Cambio Rivera(descL)
        //strSQL.append("SELECT DS_PROCESO, CD_ENFCIE, DSL_PROCESO FROM SIVE_PROCESOS WHERE CD_ENFCIE IN "+
        //    " (SELECT CD_ENFCIE FROM SIVE_ENFEREDO WHERE CD_TVIGI = ? ) ");

        // modificacion jlt 24/10/2001
        // ordenamos por descripci�n de enfermedad
        strSQL.append("SELECT DS_PROCESO, CD_ENFCIE, DSL_PROCESO FROM SIVE_PROCESOS WHERE CD_ENFCIE IN " +
            " (SELECT CD_ENFCIE FROM SIVE_ENFEREDO WHERE CD_TVIGI = ? ) order by DS_PROCESO");
//__________________________________
        break;

    }

    // indicamos le n�merod e registros que queremos
    return DBServlet.maxSIZE; // por defecto
  }

} //____________________________________ END_CLASS