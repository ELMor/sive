package disnotasis;

import java.util.ResourceBundle;

import capp.CApp;
import sapp.StubSrvBD;

public class disnotasis
    extends CApp {

  protected StubSrvBD stubCliente = new StubSrvBD();
  ResourceBundle res;

  public disnotasis() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    PnlParam pParamC3 = null;
    CApp a = (CApp)this;

    res = ResourceBundle.getBundle("disnotasis.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    pParamC3 = new PnlParam(a);
    VerPanel("", pParamC3);

  }

  public void stop() {
  }

} //__________________________________________________ END_CLASS
