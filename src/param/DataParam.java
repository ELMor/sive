package param;

import java.io.Serializable;

public class DataParam
    implements Serializable {
  protected String sCD_CA = "";
  protected String sURL = "";
  protected String sDS_NIVEL_1 = "";
  protected String sDSL_NIVEL_1 = "";
  protected String sDS_NIVEL_2 = "";
  protected String sDSL_NIVEL_2 = "";
  protected String sDS_NIVEL_3 = "";
  protected String sDSL_NIVEL_3 = "";
  protected String sIT_TRAMERO = "";
  protected String sDescCCAA = "";
  protected String sIT_MODULO_3 = "";
  protected String sDSL_IDLOCAL = "";
  protected String sURL_CNE = "";

  public DataParam(String codCA, String webURL, String dsNivel1,
                   String dslNivel1,
                   String dsNivel2, String dslNivel2, String dsNivel3,
                   String dslNivel3,
                   String tram, String descCCAA, String itmod3, String idlocal,
                   String cneURL) {

    sCD_CA = codCA;
    sURL = webURL;
    sDS_NIVEL_1 = dsNivel1;
    sDSL_NIVEL_1 = dslNivel1;
    sDS_NIVEL_2 = dsNivel2;
    sDSL_NIVEL_2 = dslNivel2;
    sDS_NIVEL_3 = dsNivel3;
    sDSL_NIVEL_3 = dslNivel3;
    sIT_TRAMERO = tram;
    sDescCCAA = descCCAA;
    sIT_MODULO_3 = itmod3;
    sDSL_IDLOCAL = idlocal;
    sURL_CNE = cneURL;
  }

  String getURLcne() {
    return sURL_CNE;
  }

  String getDescCCAA() {
    return sDescCCAA;
  }

  String getCodCA() {
    return sCD_CA;
  }

  String getWebURL() {
    return sURL;
  }

  String getDsNivel1() {
    return sDS_NIVEL_1;
  }

  String getDslNivel1() {
    return sDSL_NIVEL_1;
  }

  String getDsNivel2() {
    return sDS_NIVEL_2;
  }

  String getDslNivel2() {
    return sDSL_NIVEL_2;
  }

  String getDsNivel3() {
    return sDS_NIVEL_3;
  }

  String getDslNivel3() {
    return sDSL_NIVEL_3;
  }

  String getTramero() {
    return sIT_TRAMERO;
  }

  String getFun() {
    return sIT_MODULO_3;
  }

  String getIdLocal() {
    return sDSL_IDLOCAL;
  }
}
