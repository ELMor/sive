package param;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import sapp.DBServlet;

public class SrvParam
    extends DBServlet {

  // modos de operaci�n
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int iValor = 0;
    DataParam dman;
    int contador;

    // objetos de datos
    CLista data = null;
    DataParam dParam = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {
      // modos de operaci�n
      switch (opmode) {

        // alta de lista de valores
        case servletALTA:

          // prepara la query
          dParam = (DataParam) param.firstElement();

          // modificacion jlt 01/10/01
          // hay que tener en cuenta el campo de la lortad
          //query = "insert into sive_ca_parametros values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

          query =
              "insert into sive_ca_parametros values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
          st = con.prepareStatement(query);
          // codigo
          st.setString(1, dParam.getCodCA().trim());
          // URL del SERVIDOR WEB
          st.setString(2, dParam.getWebURL().trim());
          // descripci�n nivel1
          st.setString(3, dParam.getDsNivel1().trim());
          // descripci�n local nivel1
          if (dParam.getDslNivel1().trim().length() > 0) {
            st.setString(4, dParam.getDslNivel1().trim());
          }
          else {
            st.setNull(4, java.sql.Types.VARCHAR);
            // descripci�n nivel2
          }
          st.setString(5, dParam.getDsNivel2().trim());
          // descripci�n local nivel2
          if (dParam.getDslNivel2().trim().length() > 0) {
            st.setString(6, dParam.getDslNivel2().trim());
          }
          else {
            st.setNull(6, java.sql.Types.VARCHAR);
            // descripci�n m�dulo 3
          }
          st.setString(7, dParam.getDsNivel3());
          // descripci�n local m�dulo 3
          if (dParam.getDslNivel3().trim().length() > 0) {
            st.setString(8, dParam.getDslNivel3().trim());
          }
          else {
            st.setNull(8, java.sql.Types.VARCHAR);
            // Tramero disponible
          }
          st.setString(9, dParam.getTramero());
          // it m�dulo 3
          st.setString(10, dParam.getFun());
          // idioma local
          if (dParam.getIdLocal().trim().length() > 0) {
            st.setString(11, dParam.getIdLocal().trim());
          }
          else {
            st.setNull(11, java.sql.Types.VARCHAR);
          }
          st.setString(12, dParam.getURLcne().trim());

          // modificacion jlt 01/10/01
          st.setString(13, "");
          // lanza la query
          st.executeUpdate();
          st.close();
          data = new CLista();
          data.addElement(new DataParam("", "", "", "", "", "", "", "", "", "",
                                        "", "", ""));
          break;

          // Baja de par�metros
        case servletBAJA:
          query = "DELETE FROM SIVE_CA_PARAMETROS";
          st = con.prepareStatement(query);
          st.executeUpdate();
          st.close();
          data = new CLista();
          data.addElement(new DataParam("", "", "", "", "", "", "", "", "", "",
                                        "", "", ""));
          break;

          // b�squeda de par�metros
        case servletOBTENER_X_CODIGO:

          // prepara la query
          /*query = "select CD_CA, URL, DS_NIVEL_1, DSL_NIVEL_1, DS_NIVEL_2,"+
                   "DSL_NIVEL_2, IT_TRAMERO from SIVE_CA_PARAMETROS";
           */
          query = "select * from sive_ca_parametros, sive_com_aut where (sive_com_aut.cd_ca=sive_ca_parametros.cd_ca)";
          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);

          // paginaci�n

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // A�ade un nodo
            data.addElement(new DataParam(rs.getString("CD_CA"),
                                          rs.getString("URL"),
                                          rs.getString("DS_NIVEL_1"),
                                          rs.getString("DSL_NIVEL_1"),
                                          rs.getString("DS_NIVEL_2"),
                                          rs.getString("DSL_NIVEL_2"),
                                          rs.getString("DS_NIVEL_3"),
                                          rs.getString("DSL_NIVEL_3"),
                                          rs.getString("IT_TRAMERO"),
                                          rs.getString("DS_CA"),
                                          rs.getString("IT_MODULO_3"),
                                          rs.getString("DSL_IDLOCAL"),
                                          rs.getString("DS_URL_CNE")));
          }
          rs.close();
          st.close();

          break;
      }
      con.commit();
    }
    catch (Exception ef) {
      con.rollback();
      throw ef;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    data.trimToSize();
    return data;
  }
}
