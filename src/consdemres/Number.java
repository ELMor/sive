
package consdemres;

import java.text.DecimalFormat;

public class Number {

  /** variable de formateo de fechas */
  protected static DecimalFormat formatter = new DecimalFormat("##0.0#");
  public Number() {
  }

  /**
   *  esta funcion formatea un n�mero  a cadena
   *
   * @param a_double es el n�mero a formatear
   * @return es la cadena de textocon el formato formatter
   */
  public final static String double2String(double a_double) {
    try {
      return formatter.format(a_double);
    }
    catch (Exception exc) {
      return "";
    }
  }

  /**
   *  esta funcion formatea un n�mero
   *
   * @param a_String es la cadena de texto a traducir a fecha
   * @return es el n�mero que describ�a la cadena
   *
     public final static double string2double (String a_String) {
     try {
       ParsePosition pos = new ParsePosition (0);
       return formatter.parse(a_String, pos);
     }catch (Exception exc) {
        return null;
     }
     }
   */

  // funci�n de prueba
  public static void main(String[] args) {
    double hoy = 2.2222222222;

    //# System_Out.println((new Double(hoy)).toString());
    String result1 = double2String(hoy);
    //# // System_out.println(result1);

    hoy = 0.0222222222;
    String result2 = double2String(hoy);
    //# System_Out.println(result2);

    int i = 33;
  }

} ///______________________________________ END CLASS