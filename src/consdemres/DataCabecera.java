
package consdemres;

import capp.CLista;
import conspacenf.Data;
import sapp.DBServlet;

public class DataCabecera
    extends Data {

  //  MODOS
  /** devuelve ls notificaciones semanales */
  public static final int modoLEENOTIFEDO = 1;

  // CAMPOS  que se van a devolver
  public static final int COUNT = 0;
  public static final int FC_RECEP = 1;
  public static final int NM_NNOTIFR = 2;
  public static final int CD_ANOEPI = 3;
  public static final int CD_SEMEPI = 4;

  // PARAMETROS
  public static final int P_EQ_IN = 0; // indica la p�gina de datos que se quiere traer
  public static final int P_DESDE_CD_ANOEPI = 1;
  public static final int P_DESDE_CD_SEMEPI = 2;
  /*
    public static final int P_DESDE_CD_ANOEPI1   = 1;
    public static final int P_DESDE_CD_SEMEPI2   = 2;
    public static final int P_DESDE_CD_ANOEPI3   = 3;
    public static final int P_HASTA_CD_ANOEPI4   = 4;
    public static final int P_HASTA_CD_ANOEPI5   = 5;
    public static final int P_HASTA_CD_SEMEPI6   = 6;
   */
  /** recoge la cantidad de semanas de ese periodo */
  protected static String notifSQL =
      " SELECT FC_RECEP, NM_NNOTIFR, CD_ANOEPI, CD_SEMEPI FROM SIVE_NOTIFEDO " +
      " WHERE IT_RESSEM = 'S' AND CD_E_NOTIF IN ";

  public static String filtroNoIguales =
      " ( ( CD_ANOEPI = ? AND CD_SEMEPI >= ? ) OR " +
      " ( CD_ANOEPI > ? AND CD_ANOEPI < ? ) OR " +
      " ( CD_ANOEPI = ? AND CD_SEMEPI <= ? ) ) ";

  public static String filtroIguales =
      " ( CD_ANOEPI = ? AND CD_SEMEPI >= ? AND " +
      "  CD_ANOEPI = ? AND CD_ANOEPI = ?  AND " +
      "  CD_ANOEPI = ? AND CD_SEMEPI <= ?  ) ";

  public static String filtro = " CD_ANOEPI = ? AND CD_SEMEPI = ? ";

  public DataCabecera(int a_campo_filtro) {
    super(a_campo_filtro);
    // siempre hay que reservar
    arrayDatos = new Object[8];
  }

  public Object getNewData() {
    return new DataCabecera(campo_filtro);
  }

  /** devuelve el c�digo de un modo e funcionamiento */
  public String getCode(int a_modo) {
    return "NOMBRE";
  }

  /** devuelve la descripci�n de un modo de funcionamiento */
  public String getDes(int a_modo) {
    return null;
  }

  /** indica si ya lleva where la select */
  public boolean getWhere(int a_modo) {
    return true;
  }

  public int prepareSQLSentencia(int opmode, StringBuffer strSQL,
                                 CLista ini_param) {
    int num_result = DBServlet.maxSIZE;
    Data param_item = (Data) ini_param.firstElement();
    /*      String hasta_ano = (String) param_item.get(DataCabecera.P_HASTA_CD_ANOEPI5);
         String desde_ano = (String) param_item.get(DataCabecera.P_DESDE_CD_ANOEPI1);
          switch (opmode){
            case modoLEENOTIFEDO:
                 String eq_IN = (String) param_item.get(DataCabecera.P_EQ_IN);
                 if (hasta_ano.equals(desde_ano)){
                     strSQL.append(notifSQL +  eq_IN + " AND " +filtroIguales);
                 }else{
                     strSQL.append(notifSQL + eq_IN +" AND " +filtroNoIguales);
                 }
                 num_result = 99999;
                 break;
          }
     */
    switch (opmode) {
      case modoLEENOTIFEDO:
        String eq_IN = (String) param_item.get(DataCabecera.P_EQ_IN);
        strSQL.append(notifSQL + eq_IN + " AND " + filtro);
        num_result = 99999;
        break;
    }

    // indicamos le n�merod e registros que queremos
    return num_result; // por defecto
  }

} //________________________________________  END CLASS
