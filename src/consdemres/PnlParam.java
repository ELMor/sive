package consdemres;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo.DataCat;
import catalogo2.DataCat2;
import cn.DataCN;
import conspacenf.DataPer;
import eapp.DataGraf;
import eapp.GPanel;
import eapp.PanelChart;
import enfermo.comun;
import eqNot.DataEqNot;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
import utilidades.PanFechas;
import vCasIn.DataMun;
import zbs.DataZBS;
import zbs.DataZBS2;

public class PnlParam
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  //Servlet de este paquete y sus modos
  final String strSERVLET = "servlet/SrvDemSem";
  final int MODO_SERVLET = DataDemSem.modoEQUIPO_COUNT;
  final int MODO_SERVLETCOUNT = DataDemSem.modoEQUIPO_COUNT;

  //rutas imagenes
  final String strSERVLETenf = "servlet/SrvEnfVigi";

  protected StubSrvBD stubCliente = new StubSrvBD();

  public DataDemSem paramC1 = null;

  protected int modoOperacion = modoNORMAL;

  protected PanelInforme informe;

  protected boolean bPermiso = false;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETMun = "servlet/SrvMun";
  final String strSERVLETProv = "servlet/SrvCat2";
  final String strSERVLETEquipo = "servlet/SrvEqNot";
  final String strSERVLETCentro = "servlet/SrvCN";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  //Modos consulta servlet de equipos
  public static final int servletSELECCION_EQUIPOS_UN_CENTRO_X_CODIGO = 21; //equipos de un centro determinado
  public static final int servletSELECCION_EQUIPOS_UN_CENTRO_X_DESCRIPCION = 22; //equipos de un centro determinado
  public static final int servletOBTENER_EQUIPOS_UN_CENTRO_X_CODIGO = 23; //equipos de un centro determinado
  public static final int servletOBTENER_EQUIPOS_UN_CENTRO_X_DESCRIPCION = 24; //equipos de un centro determinado

  XYLayout xYLayout = new XYLayout();

  Label lblHasta = new Label();
  Label lblDesde = new Label();
  Label lblProvincia = new Label();
  CCampoCodigo txtCodPro = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarPro = null;
  TextField txtDesPro = new TextField();
  Label lblMunicipio = new Label();
  CCampoCodigo txtCodMun = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarMun = null;
  TextField txtDesMun = new TextField();
  Label lblArea = new Label();
  CCampoCodigo txtCodAre = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarAre = null;
  TextField txtDesAre = new TextField();
  Label lblDistrito = new Label();
  CCampoCodigo txtCodDis = new CCampoCodigo(); /*E*/
  TextField txtDesDis = new TextField();
  Label lblZonaBasica = new Label();
  CCampoCodigo txtCodZBS = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarDis = null;
  ButtonControl btnCtrlBuscarZBS = null;
  TextField txtDesZBS = new TextField();
  Label lblCenNot = new Label();
  CCampoCodigo txtCodCenNot = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarCenNot = null;
  TextField txtDesCenNot = new TextField();
  Label lblEquNot = new Label();
  CCampoCodigo txtCodEquNot = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarEquNot = null;
  TextField txtDesEquNot = new TextField();
  PanFechas fechasDesde;
  PanFechas fechasHasta;
  ButtonControl btnLimpiar = null;
  ButtonControl btnInforme = null;
  ButtonControl btnGrafico = null;

  focusAdapter txtFocusAdapter = new focusAdapter(this);
  textAdapter txtTextAdapter = new textAdapter(this);
  actionListener btnActionListener = new actionListener(this);
  Label lblAgrupado = new Label();
  TextField txtNumSemanas = new TextField();
  Label lblSemana = new Label();
  Checkbox chckbEqNot = new Checkbox();
  Checkbox chckbCN = new Checkbox();
  CheckboxGroup checkboxGroup1 = new CheckboxGroup();

  public PnlParam(CApp a) {
    try {

      setApp(a);
      res = ResourceBundle.getBundle("consdemres.Res" + a.getIdioma());

      DataDemSem d = new DataDemSem(DataDemSem.NOMBRE);
      //pInfor = new PanelInforme(new StubSrvBD(),this, "erw/DemSem.erw", "CONSDEMRES", d.getFieldVector());
      informe = new PanelInforme(new StubSrvBD(), a, "erw/DemRes.erw",
                                 "CONSDEMRES", d.getFieldVector(), this);
      d = null;

      fechasDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      fechasHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);

      // lanzamos un thread que trae los datos del choice
      new Thread(new LeerPermiso(this)).start();

      jbInit();
      informe.setEnabled(false);
      lblArea.setText(this.app.getNivel1() + ":");
      lblDistrito.setText(this.app.getNivel2() + ":");

      this.txtCodAre.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesAre.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtCodDis.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesDis.setText(this.app.getDS_NIVEL2_DEFECTO());

      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        comun.imgLUPA,
        comun.imgLIMPIAR,
        comun.imgGENERAR};
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(490);
    xYLayout.setWidth(596);

    btnCtrlBuscarPro = new ButtonControl(imgs.getImage(0));
    btnCtrlBuscarMun = new ButtonControl(imgs.getImage(0));
    btnCtrlBuscarAre = new ButtonControl(imgs.getImage(0));
    btnCtrlBuscarDis = new ButtonControl(imgs.getImage(0));
    btnCtrlBuscarZBS = new ButtonControl(imgs.getImage(0));
    btnCtrlBuscarCenNot = new ButtonControl(imgs.getImage(0));
    btnCtrlBuscarEquNot = new ButtonControl(imgs.getImage(0));
    btnLimpiar = new ButtonControl(imgs.getImage(1));
    btnInforme = new ButtonControl(imgs.getImage(2));
    btnGrafico = new ButtonControl(imgs.getImage(2));

    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));
    btnGrafico.setLabel(res.getString("btnGrafico.Label"));
    lblAgrupado.setText(res.getString("lblAgrupado.Text"));
    lblSemana.setText(res.getString("lblSemana.Text"));
    chckbEqNot.setLabel(res.getString("chckbEqNot.Label"));
    chckbCN.setLabel(res.getString("msg17.Text"));
    chckbEqNot.setCheckboxGroup(checkboxGroup1);
    chckbCN.setCheckboxGroup(checkboxGroup1);
    checkboxGroup1.setSelectedCheckbox(chckbEqNot);

    // gestores de eventos
    btnCtrlBuscarPro.addActionListener(btnActionListener);
    btnCtrlBuscarMun.addActionListener(btnActionListener);
    btnCtrlBuscarAre.addActionListener(btnActionListener);
    btnCtrlBuscarDis.addActionListener(btnActionListener);
    btnCtrlBuscarZBS.addActionListener(btnActionListener);
    btnCtrlBuscarCenNot.addActionListener(btnActionListener);
    btnCtrlBuscarEquNot.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);
    btnGrafico.addActionListener(btnActionListener);

    txtCodPro.addKeyListener(txtTextAdapter);
    txtCodMun.addKeyListener(txtTextAdapter);
    txtCodAre.addKeyListener(txtTextAdapter);
    txtCodDis.addKeyListener(txtTextAdapter);
    txtCodZBS.addKeyListener(txtTextAdapter);
    txtCodCenNot.addKeyListener(txtTextAdapter);
    txtCodEquNot.addKeyListener(txtTextAdapter);

    txtCodPro.addFocusListener(txtFocusAdapter);
    txtCodMun.addFocusListener(txtFocusAdapter);
    txtCodAre.addFocusListener(txtFocusAdapter);
    txtCodDis.addFocusListener(txtFocusAdapter);
    txtCodZBS.addFocusListener(txtFocusAdapter);
    txtCodCenNot.addFocusListener(txtFocusAdapter);
    txtCodEquNot.addFocusListener(txtFocusAdapter);

    btnCtrlBuscarPro.setActionCommand("buscarPro");
    txtDesPro.setEditable(false);
    txtDesPro.setEnabled(false);
    btnCtrlBuscarMun.setActionCommand("buscarMun");
    txtDesMun.setEditable(false);
    txtDesMun.setEnabled(false);
    btnCtrlBuscarAre.setActionCommand("buscarAre");
    txtDesAre.setEditable(false);
    txtDesAre.setEnabled(false);
    btnCtrlBuscarDis.setActionCommand("buscarDis");
    btnCtrlBuscarZBS.setActionCommand("buscarZBS");
    txtDesZBS.setEditable(false);
    txtDesZBS.setEnabled(false);
    btnCtrlBuscarCenNot.setActionCommand("buscarCenNot");
    txtDesCenNot.setEditable(false);
    txtDesCenNot.setEnabled(false);
    btnCtrlBuscarEquNot.setActionCommand("buscarEquNot");
    txtDesEquNot.setEditable(false);
    txtDesEquNot.setEnabled(false);
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");
    btnGrafico.setActionCommand("grafico");

    txtCodPro.setName("txtCodPro");
    txtCodMun.setName("txtCodMun");
    txtCodAre.setName("txtCodAre");
    txtCodDis.setName("txtCodDis");
    txtDesDis.setEditable(false);
    txtDesDis.setEnabled(false);
    txtCodZBS.setName("txtCodZBS");
    txtCodCenNot.setName("txtCodCenNot");
    txtCodEquNot.setName("txtCodEquNot");

    lblHasta.setText(res.getString("lblHasta.Text"));
    lblProvincia.setText(res.getString("lblProvincia.Text"));
    lblMunicipio.setText(res.getString("lblMunicipio.Text"));
    lblZonaBasica.setText(res.getString("lblZonaBasica.Text"));
    lblCenNot.setText(res.getString("msg17.Text"));
    lblEquNot.setText(res.getString("lblEquNot.Text"));
    lblArea.setText(res.getString("lblArea.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));
    txtNumSemanas.setText("1");
    this.setLayout(xYLayout);

    // ponemos la enfermedad como clave

    this.add(lblDesde, new XYConstraints(26, 5, 51, -1));
    this.add(fechasDesde, new XYConstraints(40, 5, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 35, 51, -1));
    this.add(fechasHasta, new XYConstraints(40, 35, -1, -1));

    this.add(lblAgrupado, new XYConstraints(25, 65, -1, -1));
    this.add(txtNumSemanas, new XYConstraints(128, 65, 31, 24));
    this.add(lblSemana, new XYConstraints(169, 65, 61, 25));
    this.add(chckbEqNot, new XYConstraints(254, 65, 132, 26));
    this.add(chckbCN, new XYConstraints(397, 65, 141, 28));

    this.add(lblProvincia, new XYConstraints(24, 95, 64, -1));
    this.add(txtCodPro, new XYConstraints(141, 95, 77, -1));
    this.add(btnCtrlBuscarPro, new XYConstraints(223, 95, -1, -1));
    this.add(txtDesPro, new XYConstraints(252, 95, 287, -1));

    this.add(lblMunicipio, new XYConstraints(24, 125, 66, -1));
    this.add(txtCodMun, new XYConstraints(141, 125, 77, -1));
    this.add(btnCtrlBuscarMun, new XYConstraints(223, 125, -1, -1));
    this.add(txtDesMun, new XYConstraints(252, 125, 287, -1));

    this.add(lblArea, new XYConstraints(24, 155, 97, -1));
    this.add(txtCodAre, new XYConstraints(141, 155, 77, -1));
    this.add(btnCtrlBuscarAre, new XYConstraints(223, 155, -1, -1));
    this.add(txtDesAre, new XYConstraints(252, 155, 287, -1));

    this.add(lblDistrito, new XYConstraints(24, 185, 99, -1));
    this.add(txtCodDis, new XYConstraints(141, 185, 77, -1));
    this.add(btnCtrlBuscarDis, new XYConstraints(223, 185, -1, -1));
    this.add(txtDesDis, new XYConstraints(252, 185, 287, -1));

    this.add(lblZonaBasica, new XYConstraints(24, 215, 77, -1));
    this.add(txtCodZBS, new XYConstraints(141, 215, 77, -1));
    this.add(btnCtrlBuscarZBS, new XYConstraints(223, 215, -1, -1));
    this.add(txtDesZBS, new XYConstraints(252, 215, 287, -1));

    this.add(lblCenNot, new XYConstraints(24, 245, 101, -1));
    this.add(txtCodCenNot, new XYConstraints(141, 245, 77, -1));
    this.add(btnCtrlBuscarCenNot, new XYConstraints(223, 245, -1, -1));
    this.add(txtDesCenNot, new XYConstraints(252, 245, 287, -1));

    this.add(lblEquNot, new XYConstraints(24, 275, 110, -1));
    this.add(txtCodEquNot, new XYConstraints(141, 275, 77, -1));
    this.add(btnCtrlBuscarEquNot, new XYConstraints(223, 275, -1, -1));
    this.add(txtDesEquNot, new XYConstraints(252, 275, 287, -1));

    /*
        this.add(btnLimpiar, new XYConstraints(387, 305, -1, -1));
        this.add(btnInforme, new XYConstraints(473, 305, -1, -1));
        this.add(btnGrafico, new XYConstraints(569, 335, -1, -1));
     */
    this.add(btnLimpiar, new XYConstraints(301, 305, -1, -1));
    this.add(btnInforme, new XYConstraints(387, 305, -1, -1));
    this.add(btnGrafico, new XYConstraints(473, 305, -1, -1));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  public synchronized void setPermiso(boolean bool) {
    bPermiso = bool;
  }

  public synchronized boolean getPermiso() {
    return bPermiso;
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    if ( (fechasDesde.txtAno.getText().length() == 0) ||
        (fechasHasta.txtAno.getText().length() == 0) ||
        (fechasDesde.txtCodSem.getText().length() == 0) ||
        (fechasHasta.txtCodSem.getText().length() == 0)) {
      bDatosCompletos = false;

    }
    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtCodPro.setEnabled(true);
        txtCodAre.setEnabled(true);
        txtCodCenNot.setEnabled(true);
        btnCtrlBuscarPro.setEnabled(true);

        // control municipio
        if (!txtDesPro.getText().equals("")) {
          btnCtrlBuscarMun.setEnabled(true);
          txtCodMun.setEnabled(true);
        }
        else {
          btnCtrlBuscarMun.setEnabled(false);
          txtCodMun.setEnabled(false);
        }

        btnCtrlBuscarAre.setEnabled(true);
        // control area
        if (!txtDesAre.getText().equals("")) {
          btnCtrlBuscarDis.setEnabled(true);
          txtCodDis.setEnabled(true);
        }
        else {
          btnCtrlBuscarDis.setEnabled(false);
          txtCodDis.setEnabled(false);
        }

        // control distrito
        if (!txtDesDis.getText().equals("")) {
          btnCtrlBuscarZBS.setEnabled(true);
          txtCodZBS.setEnabled(true);
        }
        else {
          btnCtrlBuscarZBS.setEnabled(false);
          txtCodZBS.setEnabled(false);
        }

        // control eq
        if (!txtDesCenNot.getText().equals("")) {
          btnCtrlBuscarEquNot.setEnabled(true);
          txtCodEquNot.setEnabled(true);
        }
        else {
          txtCodEquNot.setEnabled(false);
          btnCtrlBuscarEquNot.setEnabled(false);
        }

        btnCtrlBuscarCenNot.setEnabled(true);
        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);
        btnGrafico.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCodPro.setEnabled(false);
        txtCodMun.setEnabled(false);
        txtCodAre.setEnabled(false);
        txtCodDis.setEnabled(false);
        txtCodZBS.setEnabled(false);
        txtCodCenNot.setEnabled(false);
        txtCodEquNot.setEnabled(false);
        btnCtrlBuscarPro.setEnabled(false);
        btnCtrlBuscarMun.setEnabled(false);
        btnCtrlBuscarAre.setEnabled(false);
        btnCtrlBuscarDis.setEnabled(false);
        btnCtrlBuscarZBS.setEnabled(false);
        btnCtrlBuscarCenNot.setEnabled(false);
        btnCtrlBuscarEquNot.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        btnGrafico.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //busca la provincia
  void btnCtrlbuscarPro_actionPerformed(ActionEvent evt) {
    DataCat2 data = null;
    DataCat2 datProv = null;
    CMessage msgBox = null;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      catalogo2.CListaCat2 lista = new catalogo2.CListaCat2(app,
          res.getString("msg19.Text"),
          stubCliente,
          strSERVLETProv,
          servletOBTENER_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA,
          servletOBTENER_X_DESCRIPCION + catalogo2.Catalogo2.catPROVINCIA,
          servletSELECCION_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA,
          servletSELECCION_X_DESCRIPCION + catalogo2.Catalogo2.catPROVINCIA);
      lista.show();
      datProv = (DataCat2) lista.getComponente();
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (datProv != null) {
      txtCodPro.removeKeyListener(txtTextAdapter);
      txtCodPro.setText(datProv.getCod());
      txtDesPro.setText(datProv.getDes());
      txtCodMun.setText("");
      txtDesMun.setText("");

      txtCodPro.addKeyListener(txtTextAdapter);
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca el municipio
  void btnCtrlbuscarMun_actionPerformed(ActionEvent evt) {
    DataMun data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaMun lista = new CListaMun(this,
                                      res.getString("msg20.Text"),
                                      stubCliente,
                                      strSERVLETMun,
                                      servletOBTENER_X_CODIGO,
                                      servletOBTENER_X_DESCRIPCION,
                                      servletSELECCION_X_CODIGO,
                                      servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataMun) lista.getComponente();

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodMun.removeKeyListener(txtTextAdapter);
      txtCodMun.setText(data.getMunicipio());
      txtDesMun.setText(data.getDescMun());
      txtCodMun.addKeyListener(txtTextAdapter);
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //Busca los niveles1 (area de salud)
  void btnCtrlbuscarAre_actionPerformed(ActionEvent evt) {
    DataNivel1 data = null;
    CMessage mensaje = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaNivel1 lista = new CListaNivel1(app,
                                            res.getString("msg21.Text") +
                                            this.app.getNivel1(),
                                            stubCliente,
                                            strSERVLETNivel1,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }

    if (data != null) {
      txtCodAre.removeKeyListener(txtTextAdapter);
      txtCodAre.setText(data.getCod());
      txtDesAre.setText(data.getDes());
      txtCodDis.setText("");
      txtDesDis.setText("");
      txtCodZBS.setText("");
      txtDesZBS.setText("");

      txtCodAre.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca el distrito (Niv2)
  void btnCtrlbuscarDis_actionPerformed(ActionEvent evt) {

    DataZBS2 data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg22.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLETNivel2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodDis.removeKeyListener(txtTextAdapter);
      txtCodDis.setText(data.getNiv2());
      txtDesDis.setText(data.getDes());
      txtCodZBS.setText("");
      txtDesZBS.setText("");
      txtCodDis.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnCtrlbuscarZBS_actionPerformed(ActionEvent evt) {
    DataZBS data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZona lista = new CListaZona(this,
                                        res.getString("msg23.Text"),
                                        stubCliente,
                                        strSERVLETZona,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS) lista.getComponente();

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    if (data != null) {
      txtCodZBS.removeKeyListener(txtTextAdapter);
      txtCodZBS.setText(data.getCod());
      txtDesZBS.setText(data.getDes());
      txtCodZBS.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnCtrlbuscarCenNot_actionPerformed(ActionEvent evt) {
    DataCN data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaCN lista = new CListaCN(this.app,
                                    res.getString("msg24.Text"),
                                    stubCliente,
                                    strSERVLETCentro,
                                    servletOBTENER_X_CODIGO,
                                    servletOBTENER_X_DESCRIPCION,
                                    servletSELECCION_X_CODIGO,
                                    servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataCN) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodCenNot.removeKeyListener(txtTextAdapter);
      txtCodCenNot.setText(data.getCodCentro());
      txtDesCenNot.setText(data.getCentroDesc());
      txtCodEquNot.setText("");
      txtDesEquNot.setText("");

      txtCodCenNot.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnCtrlbuscarEquNot_actionPerformed(ActionEvent evt) {

    DataEqNot data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaEqNot lista = new CListaEqNot(this,
                                          res.getString("msg25.Text"),
                                          stubCliente,
                                          strSERVLETEquipo,
          servletOBTENER_EQUIPOS_UN_CENTRO_X_CODIGO,
          servletOBTENER_EQUIPOS_UN_CENTRO_X_DESCRIPCION,
          servletSELECCION_EQUIPOS_UN_CENTRO_X_CODIGO,
          servletSELECCION_EQUIPOS_UN_CENTRO_X_DESCRIPCION);
      lista.show();
      data = (DataEqNot) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodEquNot.removeKeyListener(txtTextAdapter);
      txtCodEquNot.setText(data.getEquipo());
      txtDesEquNot.setText(data.getDesEquipo());
      txtCodEquNot.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    txtCodAre.setText("");
    txtDesAre.setText("");
    txtCodDis.setText("");
    txtDesDis.setText("");
    txtCodZBS.setText("");
    txtDesZBS.setText("");
    txtCodPro.setText("");
    txtDesPro.setText("");
    txtCodMun.setText("");
    txtDesMun.setText("");
    txtCodEquNot.setText("");
    txtDesEquNot.setText("");
    txtCodCenNot.setText("");
    txtDesCenNot.setText("");
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    paramC1 = new DataDemSem(DataDemSem.NOMBRE);

    if (isDataValid()) {

      if (!txtCodPro.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_PROV, txtCodPro.getText());
      }
      else {
        paramC1.put(DataDemSem.P_CD_PROV, "%");
      }
      if (!txtCodMun.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_MUN, txtCodMun.getText());
      }
      else {
        paramC1.put(DataDemSem.P_CD_MUN, "%");
      }
      if (!txtCodAre.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_NIVEL_1, txtCodAre.getText());
      }
      else {
        paramC1.put(DataDemSem.P_CD_NIVEL_1, "%");
      }
      if (!txtCodDis.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_NIVEL_2, txtCodDis.getText());
      }
      else {
        paramC1.put(DataDemSem.P_CD_NIVEL_2, "%");
      }
      if (!txtCodZBS.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_ZBS, txtCodZBS.getText());
      }
      else {
        paramC1.put(DataDemSem.P_CD_ZBS, "%");
      }
      if (!txtCodEquNot.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_E_NOTIF, txtCodEquNot.getText());
      }
      else {
        paramC1.put(DataDemSem.P_CD_E_NOTIF, "%");
      }
      if (!txtCodCenNot.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_C_NOTIF, txtCodCenNot.getText() + "%");
      }
      else {
        paramC1.put(DataDemSem.P_CD_C_NOTIF, "%");
      }
      if (!txtNumSemanas.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_AGRUPADO, txtNumSemanas.getText());
      }
      else {
        paramC1.put(DataDemSem.P_AGRUPADO, "1");
      }
      if (chckbCN.getState()) {
        paramC1.put(DataDemSem.P_AGRU_EQ, "N");
      }
      else {
        paramC1.put(DataDemSem.P_AGRU_EQ, "S");
      }

      // parametros obligatorios
      paramC1.put(DataDemSem.P_DESDE_CD_ANOEPI1, fechasDesde.txtAno.getText());
      paramC1.put(DataDemSem.P_DESDE_CD_SEMEPI2, fechasDesde.txtCodSem.getText());
      paramC1.put(DataDemSem.P_DESDE_CD_ANOEPI3, fechasDesde.txtAno.getText());
      paramC1.put(DataDemSem.P_HASTA_CD_ANOEPI4, fechasHasta.txtAno.getText());
      paramC1.put(DataDemSem.P_HASTA_CD_ANOEPI5, fechasHasta.txtAno.getText());
      paramC1.put(DataDemSem.P_HASTA_CD_SEMEPI6, fechasHasta.txtCodSem.getText());

      // ponemos el n�mero de par�metros
      paramC1.setNumParam(13);

      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();
      informe.setEnabled(true);
      CLista param = new CLista();
      param.addElement(paramC1);
      informe.setListaParam(param);
      boolean hay = informe.GenerarInforme();
      if (hay) {
        informe.show();
      }
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg26.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    // datos de envio
    DataNivel1 nivel1;
    DataZBS2 nivel2;
    DataZBS zbs;
    DataMun mun;
    DataEqNot eqnot;
    DataCN cnnot;
    DataCat2 prov;
    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtCodAre")) &&
        (txtCodAre.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataNivel1(txtCodAre.getText()));
      strServlet = strSERVLETNivel1;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodDis")) &&
             (txtCodDis.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS2(txtCodAre.getText(), txtCodDis.getText(),
                                    "", ""));
      strServlet = strSERVLETNivel2;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodZBS")) &&
             (txtCodZBS.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS(txtCodZBS.getText(), "", "",
                                   txtCodAre.getText(), txtCodDis.getText()));
      strServlet = strSERVLETZona;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodPro")) &&
             (txtCodPro.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataCat2(txtCodPro.getText()));
      strServlet = strSERVLETProv;
      modoServlet = servletOBTENER_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA;

    }
    else if ( (txt.getName().equals("txtCodMun")) &&
             (txtCodMun.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataMun(txtCodPro.getText(), txtCodMun.getText(), "",
                                   "", "", ""));
      strServlet = strSERVLETMun;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodEquNot")) &&
             (txtCodEquNot.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataEqNot(txtCodEquNot.getText(), "",
                                     txtCodCenNot.getText(), "", "", "", "", "",
                                     "", "",
                                     "", 0, "", "", false));
      strServlet = strSERVLETEquipo;
      modoServlet = servletOBTENER_EQUIPOS_UN_CENTRO_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodCenNot")) &&
             (txtCodCenNot.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataCN(txtCodCenNot.getText(), "", "", "", "", "",
                                  "",
                                  "", "", "", "", "", "", "", "", "", ""));
      strServlet = strSERVLETCentro;
      modoServlet = servletSELECCION_X_CODIGO;
    }

    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtCodAre")) {
            nivel1 = (DataNivel1) param.firstElement();
            txtCodAre.removeKeyListener(txtTextAdapter);
            txtCodAre.setText(nivel1.getCod());
            txtDesAre.setText(nivel1.getDes());
            txtCodAre.addKeyListener(txtTextAdapter);

          }
          else if (txt.getName().equals("txtCodDis")) {
            nivel2 = (DataZBS2) param.firstElement();
            txtCodDis.removeKeyListener(txtTextAdapter);
            txtCodDis.setText(nivel2.getNiv2());
            txtDesDis.setText(nivel2.getDes());
            txtCodDis.addKeyListener(txtTextAdapter);

          }
          else if (txt.getName().equals("txtCodZBS")) {
            zbs = (DataZBS) param.firstElement();
            txtCodZBS.removeKeyListener(txtTextAdapter);
            txtCodZBS.setText(zbs.getCod());
            txtDesZBS.setText(zbs.getDes());
            txtCodZBS.addKeyListener(txtTextAdapter);

          }
          else if (txt.getName().equals("txtCodPro")) {
            prov = (DataCat2) param.firstElement();
            txtCodPro.removeKeyListener(txtTextAdapter);
            txtCodPro.setText(prov.getCod());
            txtDesPro.setText(prov.getDes());
            txtCodPro.addKeyListener(txtTextAdapter);

          }
          else if (txt.getName().equals("txtCodMun")) {
            mun = (DataMun) param.firstElement();
            txtCodMun.removeKeyListener(txtTextAdapter);
            txtCodMun.setText(mun.getMunicipio());
            txtDesMun.setText(mun.getDescMun());
            txtCodMun.addKeyListener(txtTextAdapter);

          }
          else if (txt.getName().equals("txtCodEquNot")) {
            eqnot = (DataEqNot) param.firstElement();
            txtCodEquNot.removeKeyListener(txtTextAdapter);
            txtCodEquNot.setText(eqnot.getEquipo());
            txtDesEquNot.setText(eqnot.getDesEquipo());
            txtCodEquNot.addKeyListener(txtTextAdapter);

          }
          else if (txt.getName().equals("txtCodCenNot")) {
            cnnot = (DataCN) param.firstElement();
            txtCodCenNot.removeKeyListener(txtTextAdapter);
            txtCodCenNot.setText(cnnot.getCodCentro());
            txtDesCenNot.setText(cnnot.getCentroDesc());
            txtCodCenNot.addKeyListener(txtTextAdapter);

          }

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg27.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  /*
          // obtiene los datos del servidor
          paramC2.numPagina = 0;
          paramC2.bInformeCompleto = true;
          param.addElement(paramC2);
          param.trimToSize();
          informe.stub.setUrl(new URL(app.getURL() + informe.strSERVLET));
          CLista lista = (CLista) informe.stub.doPost(paramC2.criterio, param);
          //Tratamiento de datos en el consedoagru_______________
          Vector vAgrupaciones = (Vector) lista.elementAt(0);
          Vector vTotalLineas = (Vector) lista.elementAt(2);
          boolean tasas = ((Boolean)lista.elementAt(3)).booleanValue();
          // control de registros
          if (vTotalLineas.size() == 0) {
       msgBox = new CMessage(this.app, CMessage.msgAVISO, "No se encontraron datos.");
            msgBox.show();
            msgBox = null;
            mostrar = false;
          }
   */

  void btnGrafico_actionPerformed(ActionEvent evt) {

    //Validacion datos como en propio informe___________________________
    CMessage msgBox;
    CLista lista = null; //Envio y recepci�n de datos
    Vector vAgrupaciones = null;
    paramC1 = new DataDemSem(DataDemSem.NOMBRE);
    // criterio
    String c1 = "";
    String c2 = "";
    String c3 = "";
    boolean mostrar = false;
    String centrosOEquipos = "";

    if (isDataValid()) {

      if (!txtCodPro.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_PROV, txtCodPro.getText());
      }
      else {
        paramC1.put(DataDemSem.P_CD_PROV, "%");
      }
      if (!txtCodMun.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_MUN, txtCodMun.getText());
      }
      else {
        paramC1.put(DataDemSem.P_CD_MUN, "%");
      }
      if (!txtCodAre.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_NIVEL_1, txtCodAre.getText());
      }
      else {
        paramC1.put(DataDemSem.P_CD_NIVEL_1, "%");
      }
      if (!txtCodDis.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_NIVEL_2, txtCodDis.getText());
      }
      else {
        paramC1.put(DataDemSem.P_CD_NIVEL_2, "%");
      }
      if (!txtCodZBS.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_ZBS, txtCodZBS.getText());
      }
      else {
        paramC1.put(DataDemSem.P_CD_ZBS, "%");
      }
      if (!txtCodEquNot.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_E_NOTIF, txtCodEquNot.getText());
      }
      else {
        paramC1.put(DataDemSem.P_CD_E_NOTIF, "%");
      }
      if (!txtCodCenNot.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_CD_C_NOTIF, txtCodCenNot.getText() + "%");
      }
      else {
        paramC1.put(DataDemSem.P_CD_C_NOTIF, "%");
      }
      if (!txtNumSemanas.getText().trim().equals("")) {
        paramC1.put(DataDemSem.P_AGRUPADO, txtNumSemanas.getText());
      }
      else {
        paramC1.put(DataDemSem.P_AGRUPADO, "1");
      }
      if (chckbCN.getState()) {
        paramC1.put(DataDemSem.P_AGRU_EQ, "N");
      }
      else {
        paramC1.put(DataDemSem.P_AGRU_EQ, "S");
      }

      // parametros obligatorios
      paramC1.put(DataDemSem.P_DESDE_CD_ANOEPI1, fechasDesde.txtAno.getText());
      paramC1.put(DataDemSem.P_DESDE_CD_SEMEPI2, fechasDesde.txtCodSem.getText());
      paramC1.put(DataDemSem.P_DESDE_CD_ANOEPI3, fechasDesde.txtAno.getText());
      paramC1.put(DataDemSem.P_HASTA_CD_ANOEPI4, fechasHasta.txtAno.getText());
      paramC1.put(DataDemSem.P_HASTA_CD_ANOEPI5, fechasHasta.txtAno.getText());
      paramC1.put(DataDemSem.P_HASTA_CD_SEMEPI6, fechasHasta.txtCodSem.getText());

      // ponemos el n�mero de par�metros
      paramC1.setNumParam(13);

      //_________ Ajuste criterios de etiquetas_______________________

      // etiqueta c1: periodo temporal, agrupacion y empresa

      c1 = res.getString("msg2.Text") + fechasDesde.txtAno.getText() +
          res.getString("msg5.Text") + fechasDesde.txtCodSem.getText() + " , "
          + res.getString("msg4.Text") + fechasHasta.txtAno.getText() +
          res.getString("msg5.Text") + fechasHasta.txtCodSem.getText()
          + res.getString("msg28.Text") + txtNumSemanas.getText();

      if (Integer.parseInt(txtNumSemanas.getText()) == 1) {
        c1 = c1 + res.getString("msg29.Text");
      }
      else {
        c1 = c1 + res.getString("msg30.Text");

      }
      if (chckbCN.getState() == true) {
        centrosOEquipos = res.getString("msg31.Text");
      }
      else {
        centrosOEquipos = res.getString("msg32.Text");

      }
      c1 = c1 + centrosOEquipos;

      // etiqueta c2: provincia, municipio
      if (!txtDesPro.getText().trim().equals("")) {
        c2 = res.getString("msg33.Text") + txtCodPro.getText() + " " +
            txtDesPro.getText();
      }
      if (!txtDesMun.getText().trim().equals("")) {
        c2 = c2 + res.getString("msg34.Text") + txtCodMun.getText() + " " +
            txtDesMun.getText();
      }

      // etiqueta c3: area, distrito, zbs
      if (!txtDesAre.getText().trim().equals("")) {
        c3 = res.getString("msg35.Text") + txtCodAre.getText() + " " +
            txtDesAre.getText();
      }
      if (!txtDesDis.getText().equals("")) {
        c3 = c3 + res.getString("msg36.Text") + txtCodDis.getText() + " " +
            txtDesDis.getText();
      }

      if (!txtDesZBS.getText().equals("")) {
        c3 = c3 + res.getString("msg37.Text") + txtCodZBS.getText() + " " +
            txtDesZBS.getText();
      }

      // c2 centro
      if (!txtDesCenNot.getText().trim().equals("")) {
        c2 = res.getString("msg38.Text") + txtCodCenNot.getText() + " " +
            txtDesCenNot.getText();
        c3 = "";

      }
      // c3 equipo
      if (!txtDesEquNot.getText().equals("")) {
        c3 = res.getString("msg39.Text") + txtCodEquNot.getText() + " " +
            txtDesEquNot.getText();
      }

      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();
      CLista param = new CLista();
      param.addElement(paramC1);

      try {

        //Obtenci�n datos como en propio informe___________________________
        // obtiene los datos del servidor
        DataDemSem data = (DataDemSem) param.firstElement();
        //Con todos
        data.put(DataDemSem.P_PAGINA, "-1");

        param.trimToSize();
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
        lista = (CLista) stubCliente.doPost(MODO_SERVLETCOUNT, param);

        /*      SrvDemSem srv = new SrvDemSem();
              srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@10.160.12.54:1521:ORCL",
                                     "pista",
                                     "loteb98");
              lista = srv.doDebug(MODO_SERVLETCOUNT, param);
         */
        // control de registros
        if (lista != null && lista.size() > 0) {

          // quitamos de la lista la hashtable
          vAgrupaciones = new Vector();
          for (int i = 0; i < lista.size() - 1; i++) {

            vAgrupaciones.addElement( (DataDemSem) lista.elementAt(i));

          }

          //vAgrupaciones = (Vector) lista;
          data = (DataDemSem) lista.firstElement();
          mostrar = true;
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg18.Text"));
          msgBox.show();
          msgBox = null;
          mostrar = false;
        }

        // muestra la gr�fica
        if (mostrar) {

          for (int cont = 0; cont < vAgrupaciones.size(); cont++) {
            String anodes = (String) ( ( (DataDemSem) vAgrupaciones.elementAt(
                cont)).get(DataDemSem.DESDE_ANIO));
            String semdes = (String) ( ( (DataDemSem) vAgrupaciones.elementAt(
                cont)).get(DataDemSem.DESDE_SEM));
            String anohas = (String) ( ( (DataDemSem) vAgrupaciones.elementAt(
                cont)).get(DataDemSem.HASTA_ANIO));
            String semhas = (String) ( ( (DataDemSem) vAgrupaciones.elementAt(
                cont)).get(DataDemSem.HASTA_SEM));
            // System_out.println(res.getString("msg40.Text") + anodes + " " + semdes + " hasta " + anohas + " " +  semhas );

          }

          //Calcula el n�mero de centros o equipos distintos viendo los primeros
          int numDistintos = 1; //Num de centros o equipos distintos
          String primero = "";
          String siguiente = "";
          data = (DataDemSem) lista.firstElement();
          primero = (String) data.get(DataDemSem.COD);
          for (int j = 1; j < (lista.size()) && (!siguiente.equals(primero)); j++) {
            data = (DataDemSem) lista.elementAt(j);
            siguiente = (String) data.get(DataDemSem.COD);
            if (!siguiente.equals(primero)) {
              numDistintos++;
            }
          }
          // System_out.println("Distintos " + numDistintos);  //***************

           // leyenda: depende de qu� equipos o centros distintos haya
           String leyenda[] = new String[numDistintos];
          for (int k = 0; k < numDistintos; k++) {
            data = (DataDemSem) lista.elementAt(k);
            leyenda[k] = (String) data.get(DataDemSem.NOMBRE);
          }

          // datos de la gr�fica
          double rawData[][] = null;
          String seriesX[] = null;
          String desde, hasta;
          String sCob = "";

          rawData = new double[numDistintos +
              1][ (vAgrupaciones.size() / (numDistintos)) + 1];
          seriesX = new String[ (vAgrupaciones.size() / (numDistintos)) + 1];

          /*
                    System_out.println("Elementos de la serie" + vAgrupaciones.size()/(numDistintos));
                    System_out.println(" LOS SCOB");
           */
          //AIC
          seriesX[0] = "";
          //Primera fila del rawData: �ndices columnas
          for (int k = 0; k < vAgrupaciones.size() / numDistintos; k++) {
            //AIC
            //rawData[0][k] = k;
            rawData[0][k + 1] = k + 1;
          }

          //Siguientes filas del rawdata: cada una representa un centro o equipo
          for (int i = 0; i < (numDistintos); i++) {
            //System_out.println("Equipo " + i +" __________");
            for (int k = 0; k < vAgrupaciones.size() / numDistintos; k++) {
              //sCob =  (String)  (((DataDemSem)vAgrupaciones.elementAt(numDistintos* k + i)).get(DataDemSem.COBERTURA) ) ;
              //AIC
              //rawData[i+1][k] = ((Double) (((DataDemSem)vAgrupaciones.elementAt(numDistintos* k + i)).get(DataDemSem.MED_DEMORA))).doubleValue();
              rawData[i + 1][k +
                  1] = ( (Double) ( ( (DataDemSem) vAgrupaciones.
                                     elementAt(numDistintos * k + i)).get(
                  DataDemSem.MED_DEMORA))).doubleValue();
              // System_out.println(rawData[i+1][k]);

              //Se ajustan las labels del eje X solo una vez
              if (i == 0) {
                if (Integer.parseInt(txtNumSemanas.getText()) == 1) {
                  desde = (String) ( ( (DataDemSem) vAgrupaciones.elementAt(
                      numDistintos * k)).get(DataDemSem.DESDE_SEM));
                  //AIC
                  //seriesX[k] = desde.substring(desde.length()-2,desde.length());
                  seriesX[k +
                      1] = desde.substring(desde.length() - 2, desde.length());
                }
                else {
                  desde = (String) ( ( (DataDemSem) vAgrupaciones.elementAt(
                      numDistintos * k)).get(DataDemSem.DESDE_SEM));
                  hasta = (String) ( ( (DataDemSem) vAgrupaciones.elementAt(
                      numDistintos * k)).get(DataDemSem.HASTA_SEM));
                  //AIC
                  //seriesX[k] = desde.substring(desde.length()-2,desde.length()) +  "-" +
                  //         hasta.substring(hasta.length()-2,hasta.length());
                  seriesX[k +
                      1] = desde.substring(desde.length() - 2, desde.length()) +
                      " " +
                      hasta.substring(hasta.length() - 2, hasta.length());
                }
              }
            } //for
          } //for

          /*
                     System_out.println("Series X");
               for (int l =0; l<(vAgrupaciones.size()/numDistintos);l++) {
            System_out.println(seriesX[l]);
                     }
                     for (int i =0; i<numDistintos+1; i++ ) {
            System_out.println("Equipo " + i +" __________");
            for (int k =0; k < (vAgrupaciones.size()/numDistintos) ; k++ ) {
              System_out.println (rawData[i][k] );
            }
                     }
           */

          DataGraf dataGraf = new DataGraf(res.getString("msg41.Text"), rawData,
                                           leyenda, seriesX);

          GPanel panel = new GPanel(this.getApp(), PanelChart.BARRAS);
          panel.chart.setDatosLabels(dataGraf, seriesX);
          panel.chart.setCriterios(c3, c2, c1);
          panel.chart.setTitulo(res.getString("this.chart.Titulo"));
          panel.chart.setTituloEje(res.getString("this.chart.TituloEje"),
                                   res.getString("this1.chart.TituloEje"));
          panel.show();
        }
      }
      catch (Exception e) {

        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
      modoOperacion = modoNORMAL;
      Inicializar();
    }

    else { //Si no es DataValid
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg26.Text"));
      msgBox.show();
      msgBox = null;
    }

  }

  // cambio en una caja de texto
  void textValueChanged(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtCodAre")) &&
        (txtDesAre.getText().length() > 0)) {
      txtCodDis.setText("");
      txtDesDis.setText("");
      txtCodAre.setText("");
      txtDesAre.setText("");
      txtCodZBS.setText("");
      txtDesZBS.setText("");
      btnCtrlBuscarZBS.setEnabled(false);
      btnCtrlBuscarDis.setEnabled(false);
    }
    else if ( (txt.getName().equals("txtCodDis")) &&
             (txtDesDis.getText().length() > 0)) {
      txtCodZBS.setText("");
      btnCtrlBuscarZBS.setEnabled(false);
      txtDesDis.setText("");
    }
    else if ( (txt.getName().equals("txtCodZBS")) &&
             (txtDesZBS.getText().length() > 0)) {
      txtDesZBS.setText("");
    }
    else if ( (txt.getName().equals("txtCodPro")) &&
             (txtDesPro.getText().length() > 0)) {
      txtCodMun.setText("");
      txtDesMun.setText("");
      txtDesPro.setText("");
      btnCtrlBuscarMun.setEnabled(false);
    }
    else if ( (txt.getName().equals("txtCodMun")) &&
             (txtDesMun.getText().length() > 0)) {
      txtDesMun.setText("");
    }
    else if ( (txt.getName().equals("txtCodEquNot")) &&
             (txtDesEquNot.getText().length() > 0)) {
      txtDesEquNot.setText("");
    }
    else if ( (txt.getName().equals("txtCodCenNot")) &&
             (txtDesCenNot.getText().length() > 0)) {
      txtCodEquNot.setText("");
      txtDesEquNot.setText("");
      txtDesCenNot.setText("");
      btnCtrlBuscarEquNot.setEnabled(false);
    }

    Inicializar();
  }
} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  PnlParam adaptee = null;
  ActionEvent e = null;

  public actionListener(PnlParam adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarPro")) {
      adaptee.btnCtrlbuscarPro_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarMun")) {
      adaptee.btnCtrlbuscarMun_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarAre")) {
      adaptee.btnCtrlbuscarAre_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarDis")) {
      adaptee.btnCtrlbuscarDis_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarZBS")) {
      adaptee.btnCtrlbuscarZBS_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarCenNot")) {
      adaptee.btnCtrlbuscarCenNot_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarEquNot")) {
      adaptee.btnCtrlbuscarEquNot_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("grafico")) {
      adaptee.btnGrafico_actionPerformed(e);

    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PnlParam adaptee;
  FocusEvent event;

  focusAdapter(PnlParam adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

// cambio en una caja de codigos
class textAdapter
    extends java.awt.event.KeyAdapter {
  PnlParam adaptee;

  textAdapter(PnlParam adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.textValueChanged(e);
  }
}

////////////////////// Clases para listas

// lista de valores
class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

// lista de valores
class CListaZBS2
    extends CListaValores {

  protected PnlParam panel;

  public CListaZBS2(PnlParam p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtCodAre.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

// lista de valores
class CListaZona
    extends CListaValores {

  protected PnlParam panel;

  public CListaZona(PnlParam p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS(s, "", "", panel.txtCodAre.getText(),
                       panel.txtCodDis.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS) o).getDes());
  }
}

class CListaMun
    extends CListaValores {

  protected PnlParam panel;

  public CListaMun(PnlParam p,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataMun(panel.txtCodPro.getText(), s, "", "", "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataMun) o).getMunicipio());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataMun) o).getDescMun());
  }
}

class CListaEqNot
    extends CListaValores {
  protected PnlParam panel;

  public CListaEqNot(PnlParam pnl,
                     String title,
                     StubSrvBD stub,
                     String servlet,
                     int obtener_x_codigo,
                     int obtener_x_descricpcion,
                     int seleccion_x_codigo,
                     int seleccion_x_descripcion) {
    super(pnl.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = pnl;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataEqNot(s, "", panel.txtCodCenNot.getText(), "", "", "", "",
                         "", "", "",
                         "", 0, this.app.getLogin(), "", false);
  }

  public String getCodigo(Object o) {
    return ( ( (DataEqNot) o).getEquipo());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEqNot) o).getDesEquipo());
  }
}

// lista de valores para EDO
class CListaEnfEDO
    extends CListaValores {

  public CListaEnfEDO(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataCat(s);

  }

  public String getCodigo(Object o) {
    return ( ( (DataCat) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCat) o).getDes());
  }
}

// lista de valores
class CListaCN
    extends CListaValores {

  public CListaCN(CApp a,
                  String title,
                  StubSrvBD stub,
                  String servlet,
                  int obtener_x_codigo,
                  int obtener_x_descricpcion,
                  int seleccion_x_codigo,
                  int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataCN(s, "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                      "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataCN) o).getCodCentro());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCN) o).getCentroDesc());
  }
}

/**
 *  esta clase se lanza como un thread para traer los datos
 */
class LeerPermiso
    implements Runnable {
  protected PnlParam adaptee = null;
  /** para la comunicacion con el servlet */
  public StubSrvBD stubCliente = new StubSrvBD();

  /**
   *  se inicia la lista no puede ser null
   */
  public LeerPermiso(PnlParam app) {
    adaptee = app;
  }

  public void run() {
    try {
      CLista parametros = new CLista();
      CLista result = null;

      DataPer d = new DataPer(DataPer.IT_FG_ENFERMO);
      d.setNumParam(1);
      d.put(1, adaptee.getApp().getLogin()); // ponemos el c�digo del enfermo
      parametros.addElement(d);
      result = comun.traerDatos(adaptee.getApp(), stubCliente,
                                comun.strSERVLET_GEN, DataPer.modoPERMISO,
                                parametros);

      if (result != null && result.size() > 0) {
        DataPer p = (DataPer) result.firstElement();
        adaptee.setPermiso(p.getPERMISO());
        ////# System_Out.println("fijamos " +p.getPERMISO());
      }
      else {
        adaptee.setPermiso(false);
      }

    }
    catch (Exception exc) {
      //E //# System_Out.println("Error : LeerChoices " + exc.toString());
      adaptee.setPermiso(false);
    }
  }

} // ENd Class
