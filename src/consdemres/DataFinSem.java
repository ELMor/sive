
package consdemres;

import capp.CLista;
import conspacenf.Data;
import sapp.DBServlet;

public class DataFinSem
    extends Data {

  //  MODOS
  /** devuelve la cantidad de semanas que hay en esa fecha */
  public static final int modoANOS = 18;
  /** devuelve la fecha de fin de semana que hay en esa fecha */
  public static final int modoDATAANOS = 19;

  // CAMPOS  que se van a devolver
  public static final int COUNT = 0;
  public static final int FC_FINEPI = 1;
  public static final int CD_ANOEPI = 2;
  public static final int CD_SEMEPI = 3;

  public static final int NUM_SEM = 1;

  // PARAMETROS
  public static final int P_PAGINA = 0; // indica la p�gina de datos que se quiere traer
  public static final int P_DESDE_CD_ANOEPI1 = 1;
  public static final int P_DESDE_CD_SEMEPI2 = 2;
  public static final int P_DESDE_CD_ANOEPI3 = 3;
  public static final int P_HASTA_CD_ANOEPI4 = 4;
  public static final int P_HASTA_CD_ANOEPI5 = 5;
  public static final int P_HASTA_CD_SEMEPI6 = 6;

  /** recoge la cantidad de semanas de ese periodo */
  protected static String select_num_sem =
      " SELECT COUNT(*) FROM SIVE_SEMANA_EPI WHERE ";

  /** recoge el a�o, la semana uy la fechxa de fin de semana de cada semana */
  public static String select_data_sem =
      " SELECT FC_FINEPI, CD_ANOEPI, CD_SEMEPI FROM SIVE_SEMANA_EPI WHERE ";

  public static String filtroNoIguales =
      " ( ( CD_ANOEPI = ? AND CD_SEMEPI >= ? ) OR " +
      " ( CD_ANOEPI > ? AND CD_ANOEPI < ? ) OR " +
      " ( CD_ANOEPI = ? AND CD_SEMEPI <= ? ) ) ";

  public static String filtroIguales =
      " ( CD_ANOEPI = ? AND CD_SEMEPI >= ? AND " +
      "  CD_ANOEPI = ? AND CD_ANOEPI = ?  AND " +
      "  CD_ANOEPI = ? AND CD_SEMEPI <= ?  ) ";

  public static String orderBy = " ORDER BY CD_ANOEPI,CD_SEMEPI";

  public DataFinSem(int a_campo_filtro) {
    super(a_campo_filtro);
    // siempre hay que reservar
    arrayDatos = new Object[7];
  }

  public Object getNewData() {
    return new DataFinSem(campo_filtro);
  }

  /** devuelve el c�digo de un modo e funcionamiento */
  public String getCode(int a_modo) {
    return "NOMBRE";
  }

  /** devuelve la descripci�n de un modo de funcionamiento */
  public String getDes(int a_modo) {
    return null;
  }

  /** indica si ya lleva where la select */
  public boolean getWhere(int a_modo) {
    return true;
  }

  public int prepareSQLSentencia(int opmode, StringBuffer strSQL,
                                 CLista ini_param) {
    int num_result = DBServlet.maxSIZE;
    Data param_item = (Data) ini_param.firstElement();
    String hasta_ano = (String) param_item.get(DataFinSem.P_HASTA_CD_ANOEPI5);
    String desde_ano = (String) param_item.get(DataFinSem.P_DESDE_CD_ANOEPI1);

    switch (opmode) {
      case modoANOS:
        if (hasta_ano.equals(desde_ano)) {
          strSQL.append(select_num_sem + filtroIguales);
        }
        else {
          strSQL.append(select_num_sem + filtroNoIguales);
        }
        num_result = 1;
        break;
      case modoDATAANOS:
        if (hasta_ano.equals(desde_ano)) {
          strSQL.append(select_data_sem + filtroIguales + orderBy);
        }
        else {
          strSQL.append(select_data_sem + filtroNoIguales + orderBy);
        }
        num_result = 9999;
        break;
    }

    // indicamos le n�merod e registros que queremos
    return num_result; // por defecto
  }

} //________________________________________  END CLASS
