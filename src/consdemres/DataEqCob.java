
package consdemres;

import capp.CLista;
import conspacenf.Data;
import sapp.DBServlet;

public class DataEqCob
    extends Data {

  //  MODOS
  public static final int modoEQUIPOS_ACTIVOS = 20;
  public static final int modoCENTROS_ACTIVOS = 30;
  public static final int modoCENTRO_EQ = 1;
  public static final int modoCENTRO_CN = 2;
  public static final int modoEQUIPO_EQ = 11;
  public static final int modoEQUIPO_CN = 12;

  // CAMPOS  que se van a devolver
  public static final int COUNT = 0;
  public static final int CD_E_NOTIF = 1;
  public static final int CD_CENTRO = 2;

  public static final int CODIGO = 1;
  public static final int DESCRIPCION = 2;
  public static final int IN = 3; // condici�n IN de los equipos

  // PARAMETROS
  public static final int P_PAGINA = 0; // indica la p�gina de datos que se quiere traer
  public static final int P_DESDE_CD_ANOEPI1 = 1;
  public static final int P_DESDE_CD_SEMEPI2 = 2;
  public static final int P_DESDE_CD_ANOEPI3 = 3;
  public static final int P_HASTA_CD_ANOEPI4 = 4;
  public static final int P_HASTA_CD_ANOEPI5 = 5;
  public static final int P_HASTA_CD_SEMEPI6 = 6;
  public static final int P_CD_E_NOTIF = 7;
  public static final int P_CD_C_NOTIF = 8;
  public static final int P_CD_NIVEL_1 = 9;
  public static final int P_CD_NIVEL_2 = 10;
  public static final int P_CD_ZBS = 11;
  public static final int P_CD_PROV = 12;
  public static final int P_CD_MUN = 13;
  public static final int P_AGRUPADO = 14;
  public static final int P_AGRU_EQ = 15; // indica si se agrupa por equipos o no

  public static String filtroNoIguales =
      " ( ( CD_ANOEPI = ? AND CD_SEMEPI >= ? ) OR " +
      " ( CD_ANOEPI > ? AND CD_ANOEPI < ? ) OR " +
      " ( CD_ANOEPI = ? AND CD_SEMEPI <= ? ) ) ";

  public static String filtroIguales =
      " ( CD_ANOEPI = ? AND CD_SEMEPI >= ? AND " +
      "  CD_ANOEPI = ? AND CD_ANOEPI = ?  AND " +
      "  CD_ANOEPI = ? AND CD_SEMEPI <= ?  ) ";

  /** recoge los equipos en cobertura */
  protected static String select_eq_cob =
      " SELECT DISTINCT CD_E_NOTIF FROM SIVE_NOTIF_SEM WHERE ";

  protected static String select_eq_cob_cn =
      " SELECT DISTINCT CD_CENTRO FROM SIVE_E_NOTIF WHERE" +
      " CD_E_NOTIF IN ";

  protected static String eNotif_eq = " AND CD_E_NOTIF IN " +
      " ( SELECT CD_E_NOTIF FROM SIVE_E_NOTIF " +
      "      WHERE CD_E_NOTIF LIKE ? AND " +
      "            CD_CENTRO LIKE ? AND " +
      "            CD_NIVEL_1 LIKE ? AND " +
      "            CD_NIVEL_2 LIKE ? AND " +
      "            CD_ZBS LIKE ?  ";

  protected static String eNotif_cn = " AND CD_CENTRO IN " + // parametros opcionales
      " ( SELECT CD_CENTRO FROM SIVE_C_NOTIF " +
      "      WHERE CD_PROV LIKE ? AND " +
      "            CD_MUN LIKE ? ";

  protected static String cNotif_eq = " AND a.CD_E_NOTIF IN " +
      " ( SELECT CD_E_NOTIF, CD_CENTRO FROM SIVE_E_NOTIF " +
      "      WHERE CD_E_NOTIF LIKE ? AND " +
      "            CD_CENTRO LIKE ? AND " +
      "            CD_NIVEL_1 LIKE ? AND " +
      "            CD_NIVEL_2 LIKE ? AND " +
      "            CD_ZBS LIKE ?  ";

  protected static String cNotif_cn = " AND CD_CENTRO IN " + // parametros opcionales
      " ( SELECT CD_CENTRO FROM SIVE_C_NOTIF " +
      "      WHERE CD_PROV LIKE ? AND " +
      "            CD_MUN LIKE ? ";

  public DataEqCob(int a_campo_filtro) {
    super(a_campo_filtro);
    // siempre hay que reservar
    arrayDatos = new Object[16];
  }

  public Object getNewData() {
    return new DataEqCob(campo_filtro);
  }

  /** devuelve el c�digo de un modo e funcionamiento */
  public String getCode(int a_modo) {
    return "NOMBRE";
  }

  /** devuelve la descripci�n de un modo de funcionamiento */
  public String getDes(int a_modo) {
    return null;
  }

  /** indica si ya lleva where la select */
  public boolean getWhere(int a_modo) {
    return true;
  }

  public int prepareSQLSentencia(int opmode, StringBuffer strSQL,
                                 CLista ini_param) {
    int num_result = DBServlet.maxSIZE;
    Data param_item = (Data) ini_param.firstElement();
    String hasta_ano = (String) param_item.get(DataFinSem.P_HASTA_CD_ANOEPI5);
    String desde_ano = (String) param_item.get(DataFinSem.P_DESDE_CD_ANOEPI1);

    switch (opmode) {
      case modoEQUIPOS_ACTIVOS:
        if (hasta_ano.equals(desde_ano)) {
          strSQL.append(select_eq_cob + filtroIguales);
        }
        else {
          strSQL.append(select_eq_cob + filtroNoIguales);
        }
        break;

      case modoCENTROS_ACTIVOS:
        strSQL.append(select_eq_cob_cn);
        break;

      case modoEQUIPO_EQ:
        strSQL.append(eNotif_eq);
        break;
      case modoEQUIPO_CN:
        strSQL.append(eNotif_cn);
        break;

      case modoCENTRO_EQ:
        strSQL.append(cNotif_eq);
        break;
      case modoCENTRO_CN:
        strSQL.append(cNotif_cn);
        break;

    }

    // indicamos le n�merod e registros que queremos
    return num_result; // por defecto
  }

} //________________________________________  END CLASS
