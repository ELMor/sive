
package consdemres;

import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;

import capp.CLista;
import conspacenf.Data;
import conspacenf.SrvGen;
import sapp.DBServlet;

/**
 *  Esta clase est� en el servidor recibe una CLista
 *  recoge de ella el nombre de la clase de datos y los par�metros
 *  ejecuta la sentencia SQL
 *  rellena una CLISTA con objetos de datos
 *  y finaliza la conexi�n
 */
public class SrvDemSem
    extends SrvGen {

  /** esta hashtable recoge el a�o y la semana epidemiol�gica
   *  y devuelve el fc_finepi, el fin de la semana */
  protected Hashtable sive_semana_epi = null;

  //apa�o para las notificaciones a tiempo y cuando no hay notificaciones
  public double notificadores = 0.0;

  //25/06/02
  public double NTEOR = 0.0;
  public double NREAL = 0.0;

  public double NTEORT = 0.0;
  public double NREALT = 0.0;
  //

  /** n�mero de semanas que componen una p�gina */
  protected static final int SEM_POR_PAG = DBServlet.maxSIZE; //1;//10;

  public void SrvDemSem() {
    con = null;
  }

  protected CLista doWork(int opmode, CLista param) throws Exception {
    this.param = param;
    String descrip = null;
    Data hash = null;
    CLista result = null;
    this.param = param;
    Data paramData = null;
//      String num_reg=null;
    String strSQL = null;
    Object o = null;
//        // System_out.println("Empieza el Servlett GRANDIOSO  ******************");
    //E //# // System_out.println("ReportSrvDemSem:9 bienvenido al servlet Report");

    if (param == null || param.size() < 1) {
      // no nos han pasado datos iniciales
      return null;
    }
    else {
      paramData = (Data) param.firstElement();

      //for (int i = 0; i < 16; i++){
      //E //# // System_out.println("Par 1: " + paramData.get(i));
      //}

    }

    if (breal) {
      con = openConnection();
    }

    try {
      ///establecemos las propiedades indicadas
      ///______________________________________
      con.setAutoCommit(true);

      if (opmode == DataDemSem.modoEQUIPO ||
          opmode == DataDemSem.modoEQUIPO_COUNT) {
        //E //# // System_out.println("SrvDemSem modoEquipo" );
        // vemos los equipos activos que hay
        // la lista tiene la descripci�n y el c�digo de la l�neas
        CLista equipos_cob = new CLista();
        result = new CLista();

        // rellenamos las listas anteriores

        //# // System_out.println("SrvDemRes: voy a por los equipos activos");
        int num_equipos = equipos_activos(equipos_cob);
        //# // System_out.println("SrvDemSem: numeqiupos"+ num_equipos);
        // el tama�o de cada agruapci�n
        String strAgrupa = (String) paramData.get(DataDemSem.P_AGRUPADO);
        int iAgrupa = 1;
        if (strAgrupa != null) {
          iAgrupa = Integer.parseInt(strAgrupa);
        }

        // vemos cuantas semanas hasy en ese per�odo de tiempo
        // modifica los par�metros iniciales por la paginaci�n
        StringBuffer strTotalSem = new StringBuffer();
        //# // System_out.println("SrvDemRes: voy a por las semanas");
        CLista fin_semanas = num_semanas(strTotalSem,
                                         (String) paramData.get(DataDemSem.
            P_PAGINA), iAgrupa, result);

//             // System_out.println("SrvDemRes: semanas " + fin_semanas.size());

        // calculamos el n�mero total de registros
        int num_total_registros = Integer.parseInt(strTotalSem.toString());
        int resto = num_total_registros % iAgrupa;
        num_total_registros = num_total_registros / iAgrupa;
        if (resto > 0) {
          // se a�ade la l�nea de los que quedan sueltos
          num_total_registros++;
        }
        num_total_registros = num_total_registros * num_equipos;

        // calculamos la cantidad de semanas de una p�gina
        int numero_semanas = fin_semanas.size();
//            // System_out.println("SrvDemSem: numsemanas " + numero_semanas);
        // creamos la CLista que va a contener el resultado
        // y vamos rellenado sus valores
        DataDemSem elemento = null;
        DataEqCob equipoCen = null;
        double med_demora = 0.0;
        double cobertura = 0.0;

        String strDesdeAno, strDesdeSem, strHastaSem, strHastaAno, strAno,
            strSem;
        java.util.Date dateSabado = null;
        DataFinSem desdefines_sem = null, hastafines_sem = null;
        int hastSem = 0;
        DataFinSem dfs_semana = null;
        String eq_IN = null;
        // bucle
        for (int i = 0; i < numero_semanas; i += iAgrupa) {
//               // System_out.println("********Entra en primer for");
          desdefines_sem = (DataFinSem) fin_semanas.elementAt(i);
          hastSem = i + iAgrupa - 1;
          if (hastSem >= numero_semanas) {
            hastSem = numero_semanas - 1;
          }
          hastafines_sem = (DataFinSem) fin_semanas.elementAt(hastSem);
          // calculamos la fecha de inicio y de fin
          strDesdeAno = (String) desdefines_sem.get(DataFinSem.CD_ANOEPI);
          strDesdeSem = (String) desdefines_sem.get(DataFinSem.CD_SEMEPI);
          strHastaAno = (String) hastafines_sem.get(DataFinSem.CD_ANOEPI);
          strHastaSem = (String) hastafines_sem.get(DataFinSem.CD_SEMEPI);

//               // System_out.println("SrvDemSem Fechas: " + strDesdeAno + " " + strDesdeSem +" " + strHastaAno + " " +strHastaSem);
          //# // System_out.println("SrvDemSem: en el for para todos los equipos");

          for (int j = 0; j < num_equipos; j++) {
//                   // System_out.println("********Entra en segndo for");
            equipoCen = (DataEqCob) equipos_cob.elementAt(j);

            eq_IN = (String) equipoCen.get(DataEqCob.IN);
            med_demora = 0.0;
            cobertura = 0.0;
//                     // System_out.println("SrvDemSem equipo " + eq_IN);
            notificadores = 0.0;
            for (int k = i; k < (i + iAgrupa) && k < fin_semanas.size(); k++) {
//                   // System_out.println("********Entra en ultimo for");
              //E //# // System_out.println("A�ad " +" " + i+" " +j+" "+ k );
              // calculamos cada demora por semana
              dfs_semana = (DataFinSem) fin_semanas.elementAt(k);
              // calculamos la fecha de inicio y de fin
              //E //# // System_out.println("dfs_semana0");
              strAno = (String) dfs_semana.get(DataFinSem.CD_ANOEPI);
              strSem = (String) dfs_semana.get(DataFinSem.CD_SEMEPI);
              dateSabado = (java.util.Date) dfs_semana.get(DataFinSem.FC_FINEPI);
              //# // System_out.println("SrvDemSem: sabado " + dateSabado.toString() + " y voy a por la demora antes " + med_demora);
              //med_demora +=  calcula_demora(strAno, strSem, strAno, strSem, eq_IN.toString(), fin_semanas);
              //cobertura += calcula_cobertura(strAno, strSem, strAno, strSem, eq_IN.toString());
              //E //# // System_out.println("dfs_semana1");
              med_demora +=
                  calcula_demora(strAno, strSem, eq_IN.toString(), dateSabado);

              // 25/06/02
              //cobertura += calcula_cobertura(strAno, strSem, eq_IN.toString());

              NTEOR += calcula_teor(strAno, strSem, eq_IN.toString());
              NREAL += calcula_real(strAno, strSem, eq_IN.toString());

            }

            //// JLT
            NTEORT += NTEOR;
            NREALT += NREAL;
            ///////

            med_demora = med_demora / iAgrupa;

            //cobertura = cobertura / iAgrupa;

            //cobertura = cobertura * 100.0;

            if (NTEOR != 0) {
              cobertura = (NREAL / NTEOR) * 100;
            }
            NREAL = 0.0;
            NTEOR = 0.0;

            //a�adimos una agrupaci�n
            elemento = new DataDemSem(DataDemSem.NOMBRE);
            elemento.put(DataDemSem.COUNT, Integer.toString(num_total_registros));
            elemento.put(DataDemSem.COD,
                         (String) equipoCen.get(DataEqCob.CODIGO));
            elemento.put(DataDemSem.NOMBRE,
                         (String) equipoCen.get(DataEqCob.DESCRIPCION));
            elemento.put(DataDemSem.DESDE_ANIO, strDesdeAno);
            elemento.put(DataDemSem.DESDE_SEM, strDesdeSem);
            elemento.put(DataDemSem.HASTA_ANIO, strHastaAno);
            elemento.put(DataDemSem.HASTA_SEM, strHastaSem);
            Double Ddem = new Double(med_demora);
            Double Dcob = new Double(cobertura);
            elemento.put(DataDemSem.MED_DEMORA, Ddem);
            elemento.put(DataDemSem.COBERTURA, Dcob);
            String demora = Ddem.toString();
            String cober = Dcob.toString();

//                    // System_out.println("Demora  "+ med_demora);
//                    // System_out.println("Demora  String "+ Number.double2String(med_demora));
//                    // System_out.println("Cobertura  "+ cobertura);
//                    // System_out.println("cobertura  String "+ Number.double2String(cobertura));

            String dec_dem = "";
            String ent_dem = "";
            if (demora.indexOf('.') != -1) {
              ent_dem = demora.substring(0, demora.indexOf('.'));
              dec_dem = demora.substring(demora.indexOf('.') + 1);
              /////////////  modificacion jlt

              //String d = dec_dem.substring(a.indexOf('.')+1, a.length());
              if (dec_dem.length() >= 3) {
                String d1 = dec_dem.substring(0, 1);
                String d2 = dec_dem.substring(1, 2);
                String d3 = dec_dem.substring(2, 3);
                Integer i3 = new Integer(d3);
                Integer i2 = new Integer(d2);
                Integer i1 = new Integer(d1);
                int suma = 0;
                if (i3.intValue() >= 5) {

                  if (i2.intValue() == 9) {
                    suma = i1.intValue() + 1;
                    dec_dem = (new Integer(suma)).toString() +
                        (new Integer(0)).toString();
                  }
                  else {

                    suma = i2.intValue() + 1;
                    dec_dem = d1 + (new Integer(suma)).toString();
                  }

                }
                else {
                  dec_dem = d1 + d2;
                }
              }
              //////////////

              //if (dec_dem.length()>2)
              //  dec_dem = dec_dem.substring(0,2);

            }
            else {
              ent_dem = demora;
              dec_dem = "0";
            }

            String dec_cob = "";
            String ent_cob = "";
            if (cober.indexOf('.') != -1) {
              ent_cob = cober.substring(0, cober.indexOf('.'));
              dec_cob = cober.substring(cober.indexOf('.') + 1);

              /////////////  modificacion jlt

              if (dec_cob.length() >= 3) {
                String d1 = dec_cob.substring(0, 1);
                String d2 = dec_cob.substring(1, 2);
                String d3 = dec_cob.substring(2, 3);
                Integer i3 = new Integer(d3);
                Integer i2 = new Integer(d2);
                Integer i1 = new Integer(d1);
                int suma = 0;
                if (i3.intValue() >= 5) {
                  if (i2.intValue() == 9) {
                    suma = i1.intValue() + 1;
                    dec_cob = (new Integer(suma)).toString() +
                        (new Integer(0)).toString();

                  }
                  else {
                    suma = i2.intValue() + 1;
                    dec_cob = d1 + (new Integer(suma)).toString();
                  }

                }
                else {
                  dec_cob = d1 + d2;
                }

              }
              //////////////

              //if (dec_cob.length()>2)
              //  dec_cob = dec_cob.substring(0,2);

            }
            else {
              ent_cob = cober;
              dec_cob = "0";
            }

            if (notificadores == 0.0) {
              // modificacion jlt
              //elemento.put(DataDemSem.ENT_DEM, "-0,");
              elemento.put(DataDemSem.ENT_DEM, "0,");
              elemento.put(DataDemSem.DEC_DEM, "0");
            }
            else {
              elemento.put(DataDemSem.ENT_DEM, ent_dem + ",");
              elemento.put(DataDemSem.DEC_DEM, dec_dem);
            }
            elemento.put(DataDemSem.ENT_COB, ent_cob + ",");
            elemento.put(DataDemSem.DEC_COB, dec_cob);
            /*
                                 elemento.put(DataDemSem.ENT_DEM, "234");
                                 elemento.put(DataDemSem.ENT_COB, "444");
                                 elemento.put(DataDemSem.DEC_DEM, "00");
                                 elemento.put(DataDemSem.DEC_COB, "33");
             */
            //elemento.put(DataDemSem.TOTAL_DEMORA, Number.double2String(total_cobertura));

            result.addElement(elemento);

          } //orf
        } //orf

      } // end if

    }
    catch (Exception exc) {
      String dd = exc.toString();
//        // System_out.println("SrvDemSem Error: "+ exc.toString());
      throw exc; //A�adido############

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    // a�ado los totales en una hash
    Hashtable totalescobertura = new Hashtable();
    totalescobertura.put("NTEORT", (new Double(NTEORT)).toString());
    totalescobertura.put("NREALT", (new Double(NREALT)).toString());
    if (result != null && result.size() > 0) {
      result.addElement(totalescobertura);
    }
    return result;
  }

  //_________________________________________________________________

  public String createSQLCENTRO(Data paramData, int a_modo, boolean order) {
    StringBuffer strSQL = new StringBuffer();
    int iNumReg = paramData.prepareSQLSentencia(a_modo, strSQL, param); //DataDemSem.modoCENTRO_COUNT
    if (paramData.getNumParam() > 6) {
      paramData.prepareSQLSentencia(DataDemSem.modoCENTRO_EQ, strSQL, param);
      strSQL.append(paramData.get(DataDemSem.P_CD_E_NOTIF));
      if (paramData.getNumParam() > 11) {
        paramData.prepareSQLSentencia(DataDemSem.modoCENTRO_CN, strSQL, param);
        strSQL.append(paramData.get(DataDemSem.P_CD_C_NOTIF));
        strSQL.append(")");
      }
      strSQL.append(")");
    }

    if (order) {
      strSQL.append(DataDemSem.order_cn);
    }

    return strSQL.toString();
  }

  public String num_regCENTRO(DataDemSem paramData) {
    try {
      String inum_pag = (String) ( (DataDemSem) param.firstElement()).get(
          DataDemSem.COUNT);
      ( (DataDemSem) param.firstElement()).put(DataDemSem.COUNT,
                                               (Object)new String("0"));
      String strSQLCENTRO = createSQLCENTRO(paramData,
                                            DataDemSem.modoCENTRO_COUNT, false);

      CLista result = realiza_SQL(DataDemSem.modoCENTRO_COUNT, 1,
                                  strSQLCENTRO + DataDemSem.group_cn, param);
      ( (DataDemSem) param.firstElement()).put(DataDemSem.COUNT,
                                               (Object) inum_pag);

      if (result != null && result.size() > 0) {

        Object o = ( (DataDemSem) result.firstElement()).get(1);
        return o.toString();
      }

    }
    catch (Exception exc) {
      String ss = exc.toString();
      //// System_out.println("ReportC3: SELECT Count "+ exc.toString());
    }

    return new String("0");

  }

  public String createSQLEQUIPO(Data paramData, int a_modo, boolean order) {
    StringBuffer strSQL = new StringBuffer();
    int iNumReg = paramData.prepareSQLSentencia(a_modo, strSQL, param); //DataDemSem.modoEQUIPO_COUNT
    if (paramData.getNumParam() > 6) {
      paramData.prepareSQLSentencia(DataDemSem.modoEQUIPO_EQ, strSQL, param);
      if (paramData.getNumParam() > 11) {
        paramData.prepareSQLSentencia(DataDemSem.modoEQUIPO_CN, strSQL, param);
        strSQL.append(")");
      }
      strSQL.append(")");
    }

    if (order) {
      strSQL.append(DataDemSem.order_eq);
    }

    return strSQL.toString();
  }

  /**
   *  rellena los datos del a�o y semana sive_semana_epi
   *  devuelve cuantas l�neas va a haber en los registros
   *
     public int fin_semana_epi (DataDemSem paramData){
      java.util.Date sabado=null;
      DataDemSem parametros = new DataDemSem(DataDemSem.P_DESDE_CD_ANOEPI1);
      CLista result = null;
      String semana, ano;
      DataDemSem dato= null;
      CLista lista_parametros = new CLista();
      try{
            String inum_pag = (String) ((DataDemSem)param.firstElement()).get(DataDemSem.COUNT);
            ((DataDemSem)param.firstElement()).put(DataDemSem.COUNT, (Object) new String("0"));
           // rellenamos el hashtable con las semanas del estudio
           parametros.setNumParam(6);
           parametros.put(DataDemSem.P_DESDE_CD_ANOEPI1, paramData.get(DataDemSem.P_DESDE_CD_ANOEPI1));
           parametros.put(DataDemSem.P_DESDE_CD_SEMEPI2, paramData.get(DataDemSem.P_DESDE_CD_SEMEPI2));
           parametros.put(DataDemSem.P_DESDE_CD_ANOEPI3, paramData.get(DataDemSem.P_DESDE_CD_ANOEPI3));
           parametros.put(DataDemSem.P_HASTA_CD_ANOEPI4, paramData.get(DataDemSem.P_HASTA_CD_ANOEPI4));
           parametros.put(DataDemSem.P_HASTA_CD_ANOEPI5, paramData.get(DataDemSem.P_HASTA_CD_ANOEPI5));
           parametros.put(DataDemSem.P_HASTA_CD_SEMEPI6, paramData.get(DataDemSem.P_HASTA_CD_SEMEPI6));
           strSQL = new StringBuffer();
           lista_parametros.addElement(parametros);
           int num_result = parametros.prepareSQLSentencia(DataDemSem.modoDATAANOS, strSQL, param);
           result = realiza_SQL(DataDemSem.modoDATAANOS, num_result, strSQL.toString() , lista_parametros);
           // recogemos los datos
           for( int i = 0; i < result.size(); i++){
                dato = (DataDemSem) result.elementAt(i);
                semana = (String) dato.get(DataDemSem.F_CD_SEMEPI);
                ano = (String) dato.get(DataDemSem.F_CD_ANOEPI);
                sabado = (java.util.Date) dato.get(DataDemSem.F_FC_FINEPI);
                sive_semana_epi.put(ano+"-"+semana, sabado);
           }
      }catch (Exception exc){
          //# // System_out.println("Error trayendo los s�bados " +exc.toString());
      }
      return result.size();
     } */

  /** calcula el n�mero de equipos agrupados por semana y a�o */
  public String num_regEQUIPO(DataDemSem paramData) {
    try {
      String inum_pag = (String) ( (DataDemSem) param.firstElement()).get(
          DataDemSem.COUNT);
      ( (DataDemSem) param.firstElement()).put(DataDemSem.COUNT,
                                               (Object)new String("0"));
      String strSQL = createSQLEQUIPO(paramData, DataDemSem.modoEQUIPO_COUNT, false);
      CLista result = realiza_SQL(DataDemSem.modoEQUIPO_COUNT, 1,
                                  strSQL + DataDemSem.group_eq, param);
      ( (DataDemSem) param.firstElement()).put(DataDemSem.COUNT,
                                               (Object) inum_pag);

      if (result != null && result.size() > 0) {
        Object o = ( (DataDemSem) result.firstElement()).get(1);
        return o.toString();
      }

    }
    catch (Exception exc) {
      String ss = exc.toString();
      //// System_out.println("ReportC3: SELECT Count "+ exc.toString());
    }

    return new String("0");

  }

  /**
   *   calcula el tiempo de demora
   */
  public float tiempoDemora(java.util.Date fec_recep, java.util.Date sabado,
                            Float num_notif) {
    long mili_dia = 86400000L;
    if (num_notif == null) {
      num_notif = new Float(0);
    }
    /*/# // System_out.println("SrvDemRes: recep " +  fec_recep.toString()
                       + " sabado " + sabado.toString()
                       + " notif " + num_notif);*/
   float dias_demora = (float) ( (fec_recep.getTime() - sabado.getTime()) /
                                mili_dia);
    if (dias_demora > 0) {
      //# // System_out.println("SrvDemRes: demora "  + dias_demora *  num_notif.floatValue());
      return (dias_demora * num_notif.floatValue());
    }
    else {
      return 0;
    }

  }

  /**
   *  esta funci�n agrupa todos los registros de un mismo a�o, semana y centro o equipo
   *  se supone que el ResultSet tiene datos y es v�lido
   *
     public float tiempoDemoraSemana( ResultSet rs) {
         String semana=null, ano =null, nombre = null;
         String a_semana, a_ano, a_nombre; // par�metros iniciales
         float demora_total = 0;
         java.util.Date fec_recep, sabado;
         float num_notif=0, num_total_notif=0;
         try {
           // se recogen los par�metros iniciales
           // que servir�n de filtro poara ir trayendo m�s datos
           a_nombre = rs.getString(DataDemSem.C_NOMBRE);
           a_semana = rs.getString(DataDemSem.C_CD_SEMEPI);
           a_ano = rs.getString(DataDemSem.C_CD_ANOEPI);
           nombre = a_nombre;
           ano = a_ano;
           semana = a_semana;
           // recogemos el d�a que es el fin de la semana epidemiol�gica
           sabado = (java.util.Date) sive_semana_epi.get(ano+"-"+semana);
           // traemos datos mientras conserven el mismo a�o, semana y nombre
           while ( a_semana.equals(nombre) &&
                   a_ano.equals(ano) &&
                   a_nombre.equals(semana) ) {
              fec_recep = rs.getDate(DataDemSem.C_FC_RECEP);
              num_notif = (float) rs.getInt(DataDemSem.C_NM_NNOTIFR);
              //semana = rs.getString(DataDemSem.CD_E_NOTIF);
              //semana = rs.getString(DataDemSem.CD_CENTRO);
              demora_total += tiempoDemora( fec_recep, sabado, num_notif);
              num_total_notif += num_notif;
              if (rs.next()){
                 nombre = rs.getString(DataDemSem.C_NOMBRE);
                 ano = rs.getString(DataDemSem.C_CD_SEMEPI);
                 semana = rs.getString(DataDemSem.C_CD_ANOEPI);
              }else{
                 nombre = new String();
                 ano = new String();
                 semana = new String();
              }
           }// end while();
         }catch (Exception exc){
             //# // System_out.println("Error " + exc.toString());
         }
         // realizamos la divisi�n
         if (num_total_notif != 0) {
             return  demora_total / num_total_notif;
         }else{
             return 0;
         }
     } */

  /**
   *  esta funci�n devuevle el n�mero de semanas que hay  en el
   *  per�odo de tiempo que indica el par�metro inicial
   *  y la fecha de fin de cada semana
   *
   *  @param a_strTotalSem , es el n�mero total de semanas
   *  @param a_strPagina , una string que indica la p�gina que desea el cliente
   *  @param a_agrupa, indica como se agrupan las semanas
   *  @param a_result, es la lista donde se insertar� el resultado para indicarle el estado
   *  @return devuelve una cLista con las semanas de la p�gina
   */
  public CLista num_semanas(StringBuffer a_strTotalSem, String a_strPagina,
                            int a_agrupa, CLista a_result) {
    int num_equipos = 0;
    DataFinSem finSem = new DataFinSem(DataFinSem.CD_SEMEPI);
    CLista result = null;
    int num_pagina = 0;
    int i;

    try {
      // pasamos los par�metros iniciales
      DataDemSem demSem_param = (DataDemSem) param.firstElement();
      finSem.setNumParam(6);
      finSem.put(DataEqCob.P_PAGINA, demSem_param.get(DataDemSem.P_PAGINA));
      finSem.put(DataEqCob.P_DESDE_CD_ANOEPI1,
                 demSem_param.get(DataDemSem.P_DESDE_CD_ANOEPI1));
      finSem.put(DataEqCob.P_DESDE_CD_SEMEPI2,
                 demSem_param.get(DataDemSem.P_DESDE_CD_SEMEPI2));
      finSem.put(DataEqCob.P_DESDE_CD_ANOEPI3,
                 demSem_param.get(DataDemSem.P_DESDE_CD_ANOEPI3));
      finSem.put(DataEqCob.P_HASTA_CD_ANOEPI4,
                 demSem_param.get(DataDemSem.P_HASTA_CD_ANOEPI4));
      finSem.put(DataEqCob.P_HASTA_CD_ANOEPI5,
                 demSem_param.get(DataDemSem.P_HASTA_CD_ANOEPI5));
      finSem.put(DataEqCob.P_HASTA_CD_SEMEPI6,
                 demSem_param.get(DataDemSem.P_HASTA_CD_SEMEPI6));

      CLista finSemParam = new CLista();
      finSemParam.addElement(finSem);

      StringBuffer strBufSQL = new StringBuffer();
      int num_reg = finSem.prepareSQLSentencia(DataFinSem.modoDATAANOS,
                                               strBufSQL, param);
      //??????????????????
      /*// System_out.println("Opmode " + DataFinSem.modoDATAANOS);
             // System_out.println("strbufsql " + strBufSQL);
             // System_out.println("param" + param);
             // System_out.println("num_reg" + num_reg);*/

     result = realiza_SQL(DataFinSem.modoDATAANOS, num_reg, strBufSQL.toString(),
                          finSemParam);

     //// System_out.println("Tras RealizasQL"); //????????

     //// System_out.println("RESULTADOS CONSULTA result 11:50************");
      for (int cont = 0; cont < result.size(); cont++) {
        String ano = (String) ( ( (DataFinSem) result.elementAt(cont)).get(
            DataFinSem.CD_ANOEPI));
        String sem = (String) ( ( (DataFinSem) result.elementAt(cont)).get(
            DataFinSem.CD_SEMEPI));
        //// System_out.println("ano " + ano + " sem " + sem );
      }

    }
    catch (Exception exc) {
      //// System_out.println("Error: num_semanas " + exc.toString());
      result = null;
    }

    if (result == null || result.size() == 0) {
      a_strTotalSem.append("0");
      //// System_out.println("Result sin semanas");
      return null;
    }

    // ponemos el n�mero de semaans totales
    a_strTotalSem.append(Integer.toString(result.size()));
    // miramos qu� p�gina es la que desean
    if (a_strPagina != null) {
      num_pagina = Integer.parseInt(a_strPagina);
    }

    if (num_pagina >= 0) {
      /// borramos los elementos que pertenecen a otras p�ginas
      int num_fechas = result.size();
      int semanas_grupo = SEM_POR_PAG * a_agrupa;
      for (i = 0; i < (num_pagina * semanas_grupo) && i < num_fechas; i++) {
        result.removeElementAt(0);
      }
      num_fechas = result.size();
      for (i = semanas_grupo; i < num_fechas; i++) {
        result.removeElementAt(semanas_grupo);
      }
      if (i != semanas_grupo) {
        // se han borrado semanas de despu�s
        a_result.setState(CLista.listaINCOMPLETA);
      }
      else {
        a_result.setState(CLista.listaLLENA);
      }
    }
    else {
      // traemos ltodos os datos
      a_result.setState(CLista.listaLLENA);
    }

    //// System_out.println("RESULTADOS CONSULTA tras borrar lo de otras pg result 9:20***********************");
    for (int cont = 0; cont < result.size(); cont++) {
      String ano = (String) ( ( (DataFinSem) result.elementAt(cont)).get(
          DataFinSem.CD_ANOEPI));
      String sem = (String) ( ( (DataFinSem) result.elementAt(cont)).get(
          DataFinSem.CD_SEMEPI));
      //// System_out.println("ano " + ano + " sem " + sem );
    }

    return result;
  }

  /**
   *  esta funci�n lee los equipos activos en SIVE_NOTIF_SEM
   *
   *  @param descripciones, lista de equipos o centros con sus descripciones y la cl�usula IN
   *  @return devuelve el n�mero de l�neas por cada agrupaci�n
   */
  public int equipos_activos(CLista descripciones) {
    int num_equipos = 0;
    DataEqCob eqCob = new DataEqCob(DataEqCob.P_AGRU_EQ);

    try {
      // pasamos los par�metros iniciales
      DataDemSem demSem_param = (DataDemSem) param.firstElement();
      eqCob.setNumParam(demSem_param.getNumParam());
      eqCob.put(DataEqCob.P_PAGINA, demSem_param.get(DataDemSem.P_PAGINA));
      eqCob.put(DataEqCob.P_DESDE_CD_ANOEPI1,
                demSem_param.get(DataDemSem.P_DESDE_CD_ANOEPI1));
      eqCob.put(DataEqCob.P_DESDE_CD_SEMEPI2,
                demSem_param.get(DataDemSem.P_DESDE_CD_SEMEPI2));
      eqCob.put(DataEqCob.P_DESDE_CD_ANOEPI3,
                demSem_param.get(DataDemSem.P_DESDE_CD_ANOEPI3));
      eqCob.put(DataEqCob.P_HASTA_CD_ANOEPI4,
                demSem_param.get(DataDemSem.P_HASTA_CD_ANOEPI4));
      eqCob.put(DataEqCob.P_HASTA_CD_ANOEPI5,
                demSem_param.get(DataDemSem.P_HASTA_CD_ANOEPI5));
      eqCob.put(DataEqCob.P_HASTA_CD_SEMEPI6,
                demSem_param.get(DataDemSem.P_HASTA_CD_SEMEPI6));
      eqCob.put(DataEqCob.P_CD_E_NOTIF,
                demSem_param.get(DataDemSem.P_CD_E_NOTIF));
      eqCob.put(DataEqCob.P_CD_C_NOTIF,
                demSem_param.get(DataDemSem.P_CD_C_NOTIF));
      eqCob.put(DataEqCob.P_CD_NIVEL_1,
                demSem_param.get(DataDemSem.P_CD_NIVEL_1));
      eqCob.put(DataEqCob.P_CD_NIVEL_2,
                demSem_param.get(DataDemSem.P_CD_NIVEL_2));
      eqCob.put(DataEqCob.P_CD_ZBS, demSem_param.get(DataDemSem.P_CD_ZBS));
      eqCob.put(DataEqCob.P_CD_PROV, demSem_param.get(DataDemSem.P_CD_PROV));
      eqCob.put(DataEqCob.P_CD_MUN, demSem_param.get(DataDemSem.P_CD_MUN));
      eqCob.put(DataEqCob.P_AGRUPADO, demSem_param.get(DataDemSem.P_AGRUPADO));
      eqCob.put(DataEqCob.P_AGRU_EQ, demSem_param.get(DataDemSem.P_AGRU_EQ));

      CLista eqParam = new CLista();
      eqParam.addElement(eqCob);

      String sentenciaSQL = createSQLEQUIPO(eqCob,
                                            DataEqCob.modoEQUIPOS_ACTIVOS, false);
      CLista result = realiza_SQL(DataEqCob.modoEQUIPOS_ACTIVOS, 9999,
                                  sentenciaSQL, eqParam);

      // ponemos los equipos con la casilla
      String agru_eq = (String) eqCob.get(DataDemSem.P_AGRU_EQ);
      DataEqCob des_eqCob = null;
      String cd_eq = null, descrip = null;
      if (agru_eq != null && agru_eq.equals("N")) {
        // se agrupa por centros
        num_equipos = centros_activos(result, descripciones);
      }
      else {
        if (result != null && result.size() > 0) {
          num_equipos = result.size();
          DataEqCob resulteqCob;
          for (int i = 0; i < result.size(); i++) {
            resulteqCob = (DataEqCob) result.elementAt(i);
            cd_eq = (String) resulteqCob.get(DataEqCob.CD_E_NOTIF);
            if (cd_eq != null) {
              // buscamos la descripcion
              descrip = (String) traeDescripcion(
                  "SELECT DS_E_NOTIF FROM  SIVE_E_NOTIF WHERE CD_E_NOTIF = ?",
                  cd_eq);
              if (descrip != null) {
                // metemos en los equipos el centro y la descripcion
                des_eqCob = new DataEqCob(DataEqCob.CD_E_NOTIF);

                des_eqCob.put(DataEqCob.CODIGO, cd_eq);
                des_eqCob.put(DataEqCob.DESCRIPCION, descrip);
                des_eqCob.put(DataEqCob.IN, " ('" + cd_eq + "') ");
                descripciones.addElement(des_eqCob);
              }
            }

          }
        }
      }

    }
    catch (Exception exc) {
      String ss = exc.toString();
      //// System_out.println ("Error equipos_activos " + exc.toString());
      num_equipos = 0;
    }

    return num_equipos;
  }

  /**
   *  recibe una lista con los equipos
   *  devuelve una hashtable con los equipos y su posici�n
   *  y una lista con los centros
   * @param equipos, contiene los equipos que est�n activos
   * @param centros, es una lsita con los c�digos de los centros, sus descripciones y la cl�usula IN
   */
  public int centros_activos(CLista equipos, CLista a_centros) {
    // recorremos toda la lista poniendo los centros de esos equipos
    String code_centro = null;
    DataEqCob eqCob = null;
    Integer posicion = null;
    String descrip = null;
    // esta hashtable sirve para enumerar, ordenar y discriminar los centros
    Hashtable centros = new Hashtable();

    // metemos en la lista el centro correspondiente a cada equipo
    // se hace alrev�s para que en la hashtable que numera los centros se queden los m�s bajos
    String code_eq = null;
    StringBuffer IN_eq = null;
    for (int i = equipos.size() - 1; i >= 0; i--) {
      eqCob = (DataEqCob) equipos.elementAt(i);
      code_eq = (String) eqCob.get(DataEqCob.CD_E_NOTIF);
      descrip = (String) traeDescripcion(
          "SELECT CD_CENTRO FROM  SIVE_E_NOTIF WHERE CD_E_NOTIF = ? ", code_eq);
      if (descrip != null) {
        // a�adimos al centro que corresponde
        eqCob.put(DataEqCob.CD_CENTRO, descrip);
        // asignamos al centro una colocaci�n
        IN_eq = (StringBuffer) centros.get(descrip);
        if (IN_eq != null) {
          IN_eq.append(",'" + code_eq + "'");
          centros.put(descrip, IN_eq);
        }
        else {
          centros.put(descrip, new StringBuffer(" ('" + code_eq + "'"));
        }
      }
    }

    // buscamos las descripciones de cada centros
    String key = null;
    StringBuffer descripcionIN = null;
    for (Enumeration e = centros.keys(); e.hasMoreElements(); ) {
      key = (String) e.nextElement();
      descripcionIN = (StringBuffer) centros.get(key);
      descrip = (String) traeDescripcion(
          "SELECT DS_CENTRO FROM  SIVE_C_NOTIF WHERE CD_CENTRO = ?", key); //eqCob.get(DataEqCob.CD_CENTRO)
      // reunimos los equipos de ese

      if (descrip != null) {
        eqCob = new DataEqCob(DataEqCob.CD_CENTRO);

        eqCob.put(DataEqCob.CODIGO, key);
        eqCob.put(DataEqCob.DESCRIPCION, descrip);
        eqCob.put(DataEqCob.IN, descripcionIN.toString() + ") ");
        a_centros.addElement(eqCob);
      }
    }

    if (a_centros != null) {
      return a_centros.size();
    }
    else {
      return 0;
    }
  }

  public java.util.Date buscaFecha(CLista fin_semanas, String a_ano,
                                   String a_sem) {
    String cd_ano, cd_sem;
    Data finSem = null;

    if (a_ano == null || a_sem == null || fin_semanas == null) {
      return null;
    }

    for (int i = 0; i < fin_semanas.size(); i++) {
      finSem = (Data) fin_semanas.elementAt(i);
      cd_ano = (String) finSem.get(DataFinSem.CD_ANOEPI);
      cd_sem = (String) finSem.get(DataFinSem.CD_SEMEPI);
      if (a_ano.equals(cd_ano) && a_sem.equals(cd_sem)) {
        return (java.util.Date) finSem.get(DataFinSem.FC_FINEPI);
      }
    }

    return null;
  }

  /**
   *  esta funci�n calcula el tiempo medio de demora
   *  lanza la select para recoger todos las notificaciones del centro y fechas
   *  calcula la formula para cada semana y las va agrupando
   *
   * @param desdeano,  es el a�o en que se inica el agrupamiento
   * @param desdesem,  es la semana en que se inica el agrupamiento
   * @param hastaano,  es el a�o en que se termina el agrupamiento
   * @param hastasem,  es la semana en que se termina el agrupamiento
   * @param in, equipos que se deben mirar
   * @return devuelve la cobertura
   *
       public double calcula_demora(String desdeano, String desdesem, String hastaano,
                           String hastasem, String in , CLista fin_semanas){
     double dDemora = 0.0;
     double dTotalNotif = 0.0;
     java.util.Date fec_recep, sabado;
     String cd_anoepi, cd_semepi;
     float num_notif = 0;
     DataCabecera registro = null;
     Object o = null;
     try{
    //# // System_out.println("calcula_demora: " + desdeano+ " " + desdesem+ " " + hastaano+ " " + hastasem + " " + in );
       DataCabecera notifedo = new  DataCabecera(DataCabecera.NM_NNOTIFR);
       notifedo.setNumParam(6);
       notifedo.put(DataSemana.P_EQ_IN , in);
       notifedo.put(DataSemana.P_DESDE_CD_ANOEPI1 ,desdeano);
       notifedo.put(DataSemana.P_DESDE_CD_SEMEPI2 ,desdesem);
       notifedo.put(DataSemana.P_DESDE_CD_ANOEPI3 ,desdeano);
       notifedo.put(DataSemana.P_HASTA_CD_ANOEPI4 ,hastaano);
       notifedo.put(DataSemana.P_HASTA_CD_ANOEPI5 ,hastaano);
       notifedo.put(DataSemana.P_HASTA_CD_SEMEPI6 ,hastasem);
       CLista eqParam = new CLista();
       eqParam.addElement(notifedo);
       StringBuffer sentenciaSQL = new StringBuffer();
       int num_sentencias = notifedo.prepareSQLSentencia(DataCabecera.modoLEENOTIFEDO, sentenciaSQL, eqParam);
       CLista result = realiza_SQL(DataCabecera.modoLEENOTIFEDO, num_sentencias, sentenciaSQL.toString() , eqParam);
    //# // System_out.println("calcula_demora:2 " + result);
       if (result != null ){
           //calculamois y agrupamos los resultados
           for (int i=0; i< result.size(); i++ ) {
               registro = (DataCabecera) result.elementAt(i);
       fec_recep = (java.util.Date) registro.get(DataCabecera.FC_RECEP);
               cd_anoepi = (String) registro.get(DataCabecera.CD_ANOEPI);
               cd_semepi = (String) registro.get(DataCabecera.CD_SEMEPI);
               sabado  =  buscaFecha(fin_semanas, cd_anoepi, cd_semepi);
               o  =  registro.get(DataCabecera.NM_NNOTIFR);
             //# // System_out.println("DEMORA: "+ fec_recep + " "+ sabado + " "+ o);
       dDemora += tiempoDemora( fec_recep, sabado, Float.valueOf(o.toString()));
               dTotalNotif += Double.valueOf(o.toString()).doubleValue();
           }
           if (dTotalNotif != 0.0){
               dDemora = dDemora /  dTotalNotif;
           }else{
               dDemora = 0.0;
           }
       }
     }catch(Exception exc) {
          dDemora = 0.0;
          //# // System_out.println("Error calcula_demora " + exc.toString());
     }
     return dDemora;
     }  */
  public double calcula_demora(String desdeano, String desdesem, String in,
                               java.util.Date sabado) {
    double dDemora = 0.0;
    double dTotalNotif = 0.0;
    java.util.Date fec_recep;
    String cd_anoepi, cd_semepi;
    float num_notif = 0;
    DataCabecera registro = null;
    Object o = null;

    //# // System_out.println("SrvDemRes: en calcula demora " + desdeano + " " + desdesem + " " + in + " " + sabado.toString());

    try {
      //E //# // System_out.println("entramos en calcula demora");
      DataCabecera notifedo = new DataCabecera(DataCabecera.NM_NNOTIFR);
      notifedo.setNumParam(2);
      notifedo.put(DataSemana.P_EQ_IN, in);
      notifedo.put(DataSemana.P_DESDE_CD_ANOEPI, desdeano);
      notifedo.put(DataSemana.P_DESDE_CD_SEMEPI, desdesem);

      CLista eqParam = new CLista();
      eqParam.addElement(notifedo);

      StringBuffer sentenciaSQL = new StringBuffer();
      int num_sentencias = notifedo.prepareSQLSentencia(DataCabecera.
          modoLEENOTIFEDO, sentenciaSQL, eqParam);
      CLista result = realiza_SQL(DataCabecera.modoLEENOTIFEDO, num_sentencias,
                                  sentenciaSQL.toString(), eqParam);
      //E //# // System_out.println("calcula_demora:2 " + result);
      if (result != null) {
        //E //# // System_out.println("calcula_demora:3 " + result.size());
        //calculamois y agrupamos los resultados
        for (int i = 0; i < result.size(); i++) {
          //E //# // System_out.println("calcula_demora:4 " );
          registro = (DataCabecera) result.elementAt(i);
          //E //# // System_out.println("calcula_demora:5 ");
          fec_recep = (java.util.Date) registro.get(DataCabecera.FC_RECEP);
          //E //# // System_out.println("calcula_demora:6 ");
          o = registro.get(DataCabecera.NM_NNOTIFR);
          //E //# // System_out.println("calcula_demora:7 ");
          //E //# // System_out.println("DEMORA: ");

          //# // System_out.println("SrvDemRes: en calcula demora recep " + fec_recep.toString());

          if (fec_recep != null && sabado != null && o != null) {
            //# // System_out.println("SrvDemRes: en calcula demora " + " notif " + Double.valueOf(o.toString()).doubleValue());
            dDemora += tiempoDemora(fec_recep, sabado, Float.valueOf(o.toString()));
            dTotalNotif += Double.valueOf(o.toString()).doubleValue();
          }
        }
        notificadores += dTotalNotif;

        if (dTotalNotif != 0.0) {
          dDemora = dDemora / dTotalNotif;
        }
        else {
          dDemora = 0.0;
        }
      }

    }
    catch (Exception exc) {
      dDemora = 0.0;
      //E //# // System_out.println("Error calcula_demora " + exc.toString());
    }

    return dDemora;
  }

  /**
   *  esta funion calcula la cobertura
   *
   * @param desdeano,  es el a�o en que se inica el agrupamiento
   * @param desdesem,  es la semana en que se inica el agrupamiento
   * @param hastaano,  es el a�o en que se termina el agrupamiento
   * @param hastasem,  es la semana en que se termina el agrupamiento
   * @param in, equipos que se deben mirar
   * @return devuelve la cobertura
   */
  public double calcula_cobertura(String desdeano, String desdesem, String in) {
    double dCobertura = 0.0;
    double not_real, not_teo;
    String strNotReal, strNotTeo;
    ///////////
    double not_teo_totales = 0;
    double not_real_totales = 0;
    ///////////
    DataSemana registro = null;
    Object o = null;

    try {
      //E //# // System_out.println("entramos en calcula cobertura");
      DataSemana notifSem = new DataSemana(DataSemana.NM_NNOTIFT);
      notifSem.setNumParam(2);
      notifSem.put(DataSemana.P_EQ_IN, in);
      notifSem.put(DataSemana.P_DESDE_CD_ANOEPI, desdeano);
      notifSem.put(DataSemana.P_DESDE_CD_SEMEPI, desdesem);
      /*       notifSem.put(DataSemana.P_DESDE_CD_ANOEPI1 ,desdeano);
             notifSem.put(DataSemana.P_DESDE_CD_SEMEPI2 ,desdesem);
             notifSem.put(DataSemana.P_DESDE_CD_ANOEPI3 ,desdeano);
             notifSem.put(DataSemana.P_HASTA_CD_ANOEPI4 ,hastaano);
             notifSem.put(DataSemana.P_HASTA_CD_ANOEPI5 ,hastaano);
             notifSem.put(DataSemana.P_HASTA_CD_SEMEPI6 ,hastasem);
       */
      CLista eqParam = new CLista();
      eqParam.addElement(notifSem);

      StringBuffer sentenciaSQL = new StringBuffer();
      int num_sentencias = notifSem.prepareSQLSentencia(DataSemana.modoLEENOTIF,
          sentenciaSQL, eqParam);
      String ss = sentenciaSQL.toString();
      CLista result = realiza_SQL(DataSemana.modoLEENOTIF, num_sentencias,
                                  sentenciaSQL.toString(), eqParam);
      //E //# // System_out.println("calcula_cobertura");
      if (result != null) {
        //calculamois y agrupamos los resultados
        for (int i = 0; i < result.size(); i++) {
          registro = (DataSemana) result.elementAt(i);
          o = registro.get(DataSemana.NM_NTOTREAL);
          if (o != null) {
            not_real = Integer.parseInt(o.toString());
          }
          else {
            not_real = 0;
          }
          o = registro.get(DataSemana.NM_NNOTIFT);
          if (o != null) {
            not_teo = Integer.parseInt(o.toString());
          }
          else {
            not_teo = 0;
          }
          ///////////
          not_real_totales += not_real;
          not_teo_totales += not_teo;
          ////////////
          /*if (not_teo != 0){
              dCobertura += (not_real/ not_teo);
                          }else{
              dCobertura += 0;
                          } */
        }
        ////////// JLT
        //dCobertura = not_teo_totales/not_real_totales;
        if (not_teo_totales != 0) {
          dCobertura = not_real_totales / not_teo_totales;
        }
        else {
          dCobertura = 0;
        }
        //////////
      }
    }
    catch (Exception exc) {
      dCobertura = 0.0;
      // System_out.println("Error calcula_cobertura " + exc.toString());
    }

    return dCobertura;
  }

  ///// calculo de notificadores teoricos

  public double calcula_teor(String desdeano, String desdesem, String in) {
    double dCobertura = 0.0;
    double not_real, not_teo;
    String strNotReal, strNotTeo;
    ///////////
    double not_teo_totales = 0;
    double not_real_totales = 0;
    ///////////
    DataSemana registro = null;
    Object o = null;

    try {
      //E //# // System_out.println("entramos en calcula cobertura");
      DataSemana notifSem = new DataSemana(DataSemana.NM_NNOTIFT);
      notifSem.setNumParam(2);
      notifSem.put(DataSemana.P_EQ_IN, in);
      notifSem.put(DataSemana.P_DESDE_CD_ANOEPI, desdeano);
      notifSem.put(DataSemana.P_DESDE_CD_SEMEPI, desdesem);
      /*       notifSem.put(DataSemana.P_DESDE_CD_ANOEPI1 ,desdeano);
             notifSem.put(DataSemana.P_DESDE_CD_SEMEPI2 ,desdesem);
             notifSem.put(DataSemana.P_DESDE_CD_ANOEPI3 ,desdeano);
             notifSem.put(DataSemana.P_HASTA_CD_ANOEPI4 ,hastaano);
             notifSem.put(DataSemana.P_HASTA_CD_ANOEPI5 ,hastaano);
             notifSem.put(DataSemana.P_HASTA_CD_SEMEPI6 ,hastasem);
       */
      CLista eqParam = new CLista();
      eqParam.addElement(notifSem);

      StringBuffer sentenciaSQL = new StringBuffer();
      int num_sentencias = notifSem.prepareSQLSentencia(DataSemana.modoLEENOTIF,
          sentenciaSQL, eqParam);
      String ss = sentenciaSQL.toString();
      CLista result = realiza_SQL(DataSemana.modoLEENOTIF, num_sentencias,
                                  sentenciaSQL.toString(), eqParam);
      //E //# // System_out.println("calcula_cobertura");
      if (result != null) {
        //calculamois y agrupamos los resultados
        for (int i = 0; i < result.size(); i++) {
          registro = (DataSemana) result.elementAt(i);
          o = registro.get(DataSemana.NM_NTOTREAL);
          if (o != null) {
            not_real = Integer.parseInt(o.toString());
          }
          else {
            not_real = 0;
          }
          o = registro.get(DataSemana.NM_NNOTIFT);
          if (o != null) {
            not_teo = Integer.parseInt(o.toString());
          }
          else {
            not_teo = 0;
          }
          ///////////
          not_real_totales += not_real;
          not_teo_totales += not_teo;
          ////////////
          /*if (not_teo != 0){
              dCobertura += (not_real/ not_teo);
                          }else{
              dCobertura += 0;
                          } */
        }
        ////////// JLT
        //dCobertura = not_teo_totales/not_real_totales;
        if (not_teo_totales != 0) {
          dCobertura = not_real_totales / not_teo_totales;
        }
        else {
          dCobertura = 0;
        }
        //////////
      }
    }
    catch (Exception exc) {
      dCobertura = 0.0;
      // System_out.println("Error calcula_cobertura " + exc.toString());
    }

    return not_teo_totales;
  }

  //////

  //// calculo notificadores reales
  public double calcula_real(String desdeano, String desdesem, String in) {
    double dCobertura = 0.0;
    double not_real, not_teo;
    String strNotReal, strNotTeo;
    ///////////
    double not_teo_totales = 0;
    double not_real_totales = 0;
    ///////////
    DataSemana registro = null;
    Object o = null;

    try {
      //E //# // System_out.println("entramos en calcula cobertura");
      DataSemana notifSem = new DataSemana(DataSemana.NM_NNOTIFT);
      notifSem.setNumParam(2);
      notifSem.put(DataSemana.P_EQ_IN, in);
      notifSem.put(DataSemana.P_DESDE_CD_ANOEPI, desdeano);
      notifSem.put(DataSemana.P_DESDE_CD_SEMEPI, desdesem);
      /*       notifSem.put(DataSemana.P_DESDE_CD_ANOEPI1 ,desdeano);
             notifSem.put(DataSemana.P_DESDE_CD_SEMEPI2 ,desdesem);
             notifSem.put(DataSemana.P_DESDE_CD_ANOEPI3 ,desdeano);
             notifSem.put(DataSemana.P_HASTA_CD_ANOEPI4 ,hastaano);
             notifSem.put(DataSemana.P_HASTA_CD_ANOEPI5 ,hastaano);
             notifSem.put(DataSemana.P_HASTA_CD_SEMEPI6 ,hastasem);
       */
      CLista eqParam = new CLista();
      eqParam.addElement(notifSem);

      StringBuffer sentenciaSQL = new StringBuffer();
      int num_sentencias = notifSem.prepareSQLSentencia(DataSemana.modoLEENOTIF,
          sentenciaSQL, eqParam);
      String ss = sentenciaSQL.toString();
      CLista result = realiza_SQL(DataSemana.modoLEENOTIF, num_sentencias,
                                  sentenciaSQL.toString(), eqParam);
      //E //# // System_out.println("calcula_cobertura");
      if (result != null) {
        //calculamois y agrupamos los resultados
        for (int i = 0; i < result.size(); i++) {
          registro = (DataSemana) result.elementAt(i);
          o = registro.get(DataSemana.NM_NTOTREAL);
          if (o != null) {
            not_real = Integer.parseInt(o.toString());
          }
          else {
            not_real = 0;
          }
          o = registro.get(DataSemana.NM_NNOTIFT);
          if (o != null) {
            not_teo = Integer.parseInt(o.toString());
          }
          else {
            not_teo = 0;
          }
          ///////////
          not_real_totales += not_real;
          not_teo_totales += not_teo;
          ////////////
          /*if (not_teo != 0){
              dCobertura += (not_real/ not_teo);
                          }else{
              dCobertura += 0;
                          } */
        }
        ////////// JLT
        //dCobertura = not_teo_totales/not_real_totales;
        if (not_teo_totales != 0) {
          dCobertura = not_real_totales / not_teo_totales;
        }
        else {
          dCobertura = 0;
        }
        //////////
      }
    }
    catch (Exception exc) {
      dCobertura = 0.0;
      // System_out.println("Error calcula_cobertura " + exc.toString());
    }

    return not_real_totales;
  }

  /////

  /**
   *  esta funci�n realiza fetch de los datos leidos
   * y los va metiendo en la estructura solicitada
   *  pagina seg�n el par�metro 0
   *
   * @param paramData es un clase Data que tiene los par�metros iniciales
   * @return devuelve una CLista con todos los datos
   */
  public CLista recogerDatos(Data paramData) {
    CLista result = new CLista();
    Data h;
    Object o;
    int num_datos = 0;
    int campo_filtro = 0;
    int i;

    ////# // System_out.println("Report: entramos en recoger datos");
    //recogemos el par�metro de paginaci�n
    /*       String pag = (String) paramData.get(DataDemSem.P_PAGINA);
           int ini_pagina =0;
           int fin_pagina =0;
           if ( pag != null){
              ini_pagina = Integer.parseInt(pag);
           }
           ini_pagina  = (ini_pagina * DBServlet.maxSIZE);
           fin_pagina = ini_pagina + DBServlet.maxSIZE;
     */
    try {
      for (i = 0; rs.next(); i++) {
        // control de tama�o
        /*                if ((i >= fin_pagina)&&(!(paramData.bInformeCompleto))) {   //DBServlet.maxSIZE) {
                           result.setState(CLista.listaINCOMPLETA);
             campo_filtro = ((Data)result.lastElement()).getCampoFiltro();
                           o =  ((Data)result.lastElement()).get(campo_filtro);
                           result.setFilter(o.toString());
                           break;
                        }else if (i < ini_pagina){
                           // no guardamos los datos
                           continue;
                        }
         */
        // de la clase hija nos da los datos del servlet
        // copiamosla clase hija paraque conserve el filtrado
        h = (Data) paramData.getNewData();
        int num_cols = rs.getMetaData().getColumnCount();
        for (int j = 1; j <= num_cols; j++) {
          o = rs.getObject(j);
          if (o != null) { // si es null no lo metemos
            h.put(j, o);
          }
        }
        result.addElement(h);
      } //orf

      // vemos si est� llena la lista
      /*            if (i < fin_pagina) {
                      result.setState(CLista.listaLLENA);
                  }
       */
      ////# // System_out.println("Report:salimos bien");
      return result;
    }
    catch (SQLException exc) {
      String dd = exc.toString();
      exc.printStackTrace();
      return null;
    }
  }

  /**
   *  esta funci�n va leyendo los par�metros de Data de entrada
   *  y los prepara, no incluye paginaci�n
   */
  protected void fijarParametros(Data paramData, int a_modo) {
    int tipoDato = 0;
    Object dato = null;
    int i = 0;

    boolean is_like = (a_modo == servletSELECCION_X_CODIGO) ||
        (a_modo == servletSELECCION_X_DESCRIPCION);
////# // System_out.println("Report:: fijaParametros" );
    // ponemos los par�metros fijados por el usuario
    for (i = 1; i <= paramData.getNumParam(); i++) {
      try {
        dato = paramData.get(i);
        if (dato != null) {
          st.setObject(i, dato);
          ////# // System_out.println("Param" +i + ": " + dato);
        }
        else {
          st.setNull(i, java.sql.Types.VARCHAR);
        }
      }
      catch (Exception exc) {
        String dd = exc.toString();
        // System_out.println("Report: Fallo parametro "+ exc.toString());
      }
    }
  }

  /** prueba del servlet */
  static public void main(String[] args) {
    try {
      CLista data = new CLista();
      DataDemSem d = new DataDemSem(DataDemSem.NOMBRE);
      d.setNumParam(6); //13);   //DataDemSem.P_NUM_C3);
      d.put(DataDemSem.P_PAGINA, "0");
      d.put(DataDemSem.P_AGRUPADO, "1");
      d.put(DataDemSem.P_AGRU_EQ, "N");
      d.put(DataDemSem.P_DESDE_CD_ANOEPI1, "1999");
      d.put(DataDemSem.P_DESDE_CD_SEMEPI2, "01");
      d.put(DataDemSem.P_DESDE_CD_ANOEPI3, "1999");
      d.put(DataDemSem.P_HASTA_CD_ANOEPI4, "1999");
      d.put(DataDemSem.P_HASTA_CD_ANOEPI5, "1999");
      d.put(DataDemSem.P_HASTA_CD_SEMEPI6, "01");
      d.put(DataDemSem.P_CD_C_NOTIF, "%");
      d.put(DataDemSem.P_CD_E_NOTIF, "%");
      d.put(DataDemSem.P_CD_PROV, "%");
      d.put(DataDemSem.P_CD_MUN, "%");
      d.put(DataDemSem.P_CD_NIVEL_1, "%");
      d.put(DataDemSem.P_CD_NIVEL_2, "%");
      d.put(DataDemSem.P_CD_ZBS, "%");
      data.addElement(d);

      SrvDemSem srv = new SrvDemSem();

//     CLista dd = srv.doPrueba(DataDemSem.modoEQUIPO_COUNT, data);
      CLista dd = srv.doPrueba(DataDemSem.modoEQUIPO_COUNT, data);
      //E //# // System_out.println("Lista0 : " + dd);
      d.put(DataDemSem.P_PAGINA, "3");
      CLista dd1 = srv.doPrueba(DataDemSem.modoEQUIPO_COUNT, data);
      //E //# // System_out.println("Lista1 : " + dd1);

      //String ss = "Lista: " + dd;
      ////# // System_out.println(ss);

      int i = 33;
    }
    catch (Exception exc) {
      String ss = "fallo " + exc.toString();
      // System_out.println(ss);
    }
  }

} //________________________________________________ END_CLASS
