
package consdemres;

import java.net.URL;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class PanelInforme
    extends EPanel {

  PnlParam pan;
  ResourceBundle res;
  final String strLOGO_CCAA = "images/ccaa.gif";
  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  final String strSERVLET = "servlet/SrvDemSem";

  final int MODO_SERVLET = DataDemSem.modoEQUIPO_COUNT;
  final int MODO_SERVLETCOUNT = DataDemSem.modoEQUIPO_COUNT;

  /** guarda el n�mero de registros */
  protected String num_reg = "0";

  // estructuras de datos con todos los datos que se muestran
  protected Vector vCasos = null;

  /** esta es la lista que recoge los datos */
  protected CLista lista;

  /** esta es la lista que contiene los par�metros */
  protected CLista param = null;

  /** n�mero de p�gina por el que se va */
  int num_pag = 0;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // vector y nombre de la tabla y fichero
  String m_file = null;
  String m_table = null;
  Vector m_column = null;

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  CApp app = null;

  public PanelInforme(StubSrvBD s, CApp a, String a_file, String a_nombreTabla,
                      Vector a_column, PnlParam miPanel) {
    super(a);
    app = a;
    res = ResourceBundle.getBundle("consdemres.Res" + a.getIdioma());
    pan = miPanel;
    m_file = a_file;
    m_table = a_nombreTabla;
    m_column = a_column;
    vCasos = new Vector();

    try {
      stub = s;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + m_file);

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;

    // incrementamos el n�mero de p�gina
    num_pag++;
    //E //# System_Out.println("Pedimos la p�gina " + num_pag);

    ////# System_Out.println("Mas Datos" + lista);
    if (lista != null && lista.getState() == CLista.listaINCOMPLETA) {
      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        // obtiene los datos del servidor
        ( (DataDemSem) param.firstElement()).put(DataDemSem.P_PAGINA,
                                                 Integer.toString(num_pag));
        stub.setUrl(new URL(app.getURL() + strSERVLET));
        lista = (CLista) stub.doPost(MODO_SERVLET, param);

        if (lista != null) {
          for (int j = 0; j < lista.size(); j++) {
            vCasos.addElement(lista.elementAt(j));
          }
          //E //# System_Out.println("Total casos " + vCasos.size());

          // ponemos las etiquetas del report
          fijarEtiqueas();

          // control de registros
          this.setTotalRegistros( (new Integer(vCasos.size())).toString());
          this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

          // repintado
          ////# System_Out.println("Se refresca ");
          erwClient.refreshReport(true);
        }
        else {
          this.setTotalRegistros("0");
          this.setRegistrosMostrados("0");
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  protected void fijarEtiqueas() {
    String dato = null;
    StringBuffer linea = null;

    int iEtiqueta = 0;
    CMessage msgBox;

    String sEtiqueta[] = {
        "CRITERIO1",
        "CRITERIO2",
        "CRITERIO3",
        "CRITERIO4",
        "CRITERIO5",
        "CRITERIO6"};

    // cambiamos las etiquetas
    DataDemSem DataDemSem = (DataDemSem) param.firstElement();
    TemplateManager tm = erw.GetTemplateManager();

    /*
        linea = new StringBuffer();
        dato  = (String) DataDemSem.get(DataDemSem.P_DESDE_CD_ANOEPI1);
        if (dato != null){
            linea.append(EPanel.PERIODO);
            linea.append(dato);
        }
        dato  = (String) DataDemSem.get(DataDemSem.P_DESDE_CD_SEMEPI2);
        if (dato != null){
            linea.append(EPanel.SEPARADOR_ANO_SEM + dato);
        }
        //tm.SetLabel("LAB005", linea.toString());  // DESDE
        dato  = (String) DataDemSem.get(DataDemSem.P_HASTA_CD_ANOEPI5);
        if (dato != null){
            linea.append(EPanel.HASTA);
            linea.append(dato);
        }
        dato  = (String) DataDemSem.get(DataDemSem.P_HASTA_CD_SEMEPI6);
        if (dato != null){
            linea.append(EPanel.SEPARADOR_ANO_SEM + dato);
        }
        dato  = (String) DataDemSem.get(DataDemSem.P_AGRUPADO);
        if (dato != null){
            linea.append(EPanel.AGRUPACION);
            linea.append(dato);
        }
        dato  = (String) DataDemSem.get(DataDemSem.P_CD_C_NOTIF);
        if (dato != null && !dato.equals("%")){
            //linea.append(dato.substring(1, dato.length()-2));
            linea.append(EPanel.CENTRO);
            linea.append(dato);
        }
        dato  = (String) DataDemSem.get(DataDemSem.P_CD_E_NOTIF);
        if (dato != null && !dato.equals("%")){
            //linea.append(dato.substring(1, dato.length()-2));
            linea.append(EPanel.EQUIPO);
            linea.append(dato);
        }
        dato  = (String) DataDemSem.get(DataDemSem.P_CD_MUN);
        if (dato != null && !dato.equals("%")){
            linea.append(EPanel.MUNICIPIO);
            linea.append(dato);
        }
        dato  = (String) DataDemSem.get(DataDemSem.P_CD_PROV);
        if (dato != null && !dato.equals("%")){
            linea.append(EPanel.PROVINCIA);
            linea.append(dato);
        }
        dato  = (String) DataDemSem.get(DataDemSem.P_CD_NIVEL_1);
        if (dato != null && !dato.equals("%")){
            linea.append(EPanel.AREA);
            linea.append(dato);
        }
        dato  = (String) DataDemSem.get(DataDemSem.P_CD_NIVEL_2);
        if (dato != null && !dato.equals("%")){
            linea.append(EPanel.DISTRITO);
            linea.append(dato);
        }
        //tm.SetLabel("LAB009", linea.toString()); // NIVEL2
        dato  = (String) DataDemSem.get(DataDemSem.P_CD_ZBS);
        if (dato != null && !dato.equals("%")){
            linea.append(EPanel.ZBS);
            linea.append(dato);
        }
        //tm.SetLabel("LAB010", linea.toString()); // ZBS
        tm.SetLabel("CRITERIO", linea.toString()); // CRITERIO
        linea = new StringBuffer();
        dato  = (String) DataDemSem.get(DataDemSem.P_AGRU_EQ);
        if (dato != null && dato.equals("N")){
            linea.append(" Centro Notificador");
        }else{
            linea.append(" Equipo Notificador");
        }
        //tm.SetLabel("LABEQCN", linea.toString()); // EQUIPOS-CENTROS
     */

//***********************************************************

    try {

      // carga los citerios
      dato = (String) DataDemSem.get(DataDemSem.P_DESDE_CD_ANOEPI1);

      tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

      if (dato != null && !dato.equals("%")) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    res.getString("msg2.Text") +
                    (String) DataDemSem.get(DataDemSem.P_DESDE_CD_ANOEPI1)
                    + res.getString("msg3.Text") +
                    (String) DataDemSem.get(DataDemSem.P_DESDE_CD_SEMEPI2) +
                    " , "
                    + res.getString("msg4.Text") +
                    (String) DataDemSem.get(DataDemSem.P_HASTA_CD_ANOEPI5)
                    + res.getString("msg5.Text") +
                    (String) DataDemSem.get(DataDemSem.P_HASTA_CD_SEMEPI6));

        dato = (String) DataDemSem.get(DataDemSem.P_AGRUPADO);
        if (dato != null) {
          if (dato.equals("1")) {
            tm.SetLabel(sEtiqueta[iEtiqueta],
                        tm.GetLabel(sEtiqueta[iEtiqueta]) +
                        res.getString("msg6.Text") + " " + dato + " " +
                        res.getString("msg7.Text"));
          }
          else {
            tm.SetLabel(sEtiqueta[iEtiqueta],
                        tm.GetLabel(sEtiqueta[iEtiqueta]) +
                        res.getString("msg8.Text") + " " + dato + " " +
                        res.getString("msg9.Text"));
          }
        }
        iEtiqueta++;
      }

      dato = (String) DataDemSem.get(DataDemSem.P_CD_PROV);
      if (dato != null && !dato.equals("%")) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    res.getString("msg10.Text") + dato + " " +
                    pan.txtDesPro.getText());
        dato = (String) DataDemSem.get(DataDemSem.P_CD_MUN);
        if (dato != null && !dato.equals("%")) {
          tm.SetLabel(sEtiqueta[iEtiqueta],
                      tm.GetLabel(sEtiqueta[iEtiqueta]) + ", " +
                      res.getString("msg11.Text") + dato + " " +
                      pan.txtDesMun.getText());
        }
        iEtiqueta++;
      }

      //nivel1
      dato = (String) DataDemSem.get(DataDemSem.P_CD_NIVEL_1);
      if (dato != null && !dato.equals("%")) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    app.getNivel1() + ": " + dato + " " + pan.txtDesAre.getText());
        dato = (String) DataDemSem.get(DataDemSem.P_CD_NIVEL_2);
        //nivel2
        if (dato != null && !dato.equals("%")) {
          tm.SetLabel(sEtiqueta[iEtiqueta],
                      tm.GetLabel(sEtiqueta[iEtiqueta]) + ", " + app.getNivel2() +
                      ": " + dato + " " + pan.txtDesDis.getText());
          dato = (String) DataDemSem.get(DataDemSem.P_CD_ZBS);
          //ZBS
          if (dato != null && !dato.equals("%")) {
            tm.SetLabel(sEtiqueta[iEtiqueta],
                        tm.GetLabel(sEtiqueta[iEtiqueta]) + ", " +
                        res.getString("msg12.Text") + ": " + dato + " " +
                        pan.txtDesZBS.getText());
          }
        }
        iEtiqueta++;
      }

      //CNot
      dato = (String) DataDemSem.get(DataDemSem.P_CD_C_NOTIF);
      if (dato != null && !dato.equals("%")) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    res.getString("msg13.Text") +
                    pan.txtCodCenNot.getText().trim() //dato.substring(1, dato.length()-2)
                    + " " + pan.txtDesCenNot.getText());
        //Eq not
        dato = (String) DataDemSem.get(DataDemSem.P_CD_E_NOTIF);
        if (dato != null && !dato.equals("%")) {
          tm.SetLabel(sEtiqueta[iEtiqueta],
                      tm.GetLabel(sEtiqueta[iEtiqueta]) +
                      res.getString("msg14.Text") +
                      pan.txtCodEquNot.getText().trim() //dato.substring(1, dato.length()-2)
                      + " " + pan.txtDesEquNot.getText());
        }
        iEtiqueta++;
      }

      dato = (String) DataDemSem.get(DataDemSem.P_AGRU_EQ);

      if (dato != null && dato.equals("N")) {
        tm.SetLabel(sEtiqueta[iEtiqueta], res.getString("msg15.Text"));
        iEtiqueta++;
      }
      else {
        tm.SetLabel(sEtiqueta[iEtiqueta], res.getString("msg16.Text"));
        iEtiqueta++;
      }

      // oculta criterios no informados
      for (int i = iEtiqueta; i < 6; i++) {
        tm.SetLabel(sEtiqueta[i], "");
      }

      //Cambia etiqueta de columna Centro/Equipo si es necesario
      if (pan.chckbCN.getState() == true) {
        tm.SetLabel("LABEQCN", res.getString("msg17.Text"));

//*****************************

         // calculamos los totale smedios
      }
      StringBuffer demora_total = new StringBuffer();
      StringBuffer cobertura_total = new StringBuffer();
      calcula_totales(demora_total, cobertura_total);

      // fijamos los totales
      tm.SetLabel("LABDEMTOT", demora_total.toString()); // TOTALES
      tm.SetLabel("LABCOBTOT", cobertura_total.toString()); // TOTALES

    }

    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

  }

  /**
   *   recorreo todos los datos traidos y va sumando los totales
   *   al final hayar la media
   */
  public void calcula_totales(StringBuffer demora_total,
                              StringBuffer cobertura_total) {
    double demorat = 0.0, coberturat = 0.0;
    DataDemSem demSem = null;
    int i;

    Hashtable totalescobertura = new Hashtable();

//PRUEBA_______________
    for (i = 0; i < vCasos.size() - 1; i++) {
      demSem = (DataDemSem) vCasos.elementAt(i);
      String entDemora = (String) demSem.get(DataDemSem.ENT_DEM);
      String decDemora = (String) demSem.get(DataDemSem.DEC_DEM);
      //String entCobertura = (String) demSem.get(DataDemSem.ENT_COB);
      //String decCobertura = (String) demSem.get(DataDemSem.DEC_COB);

    }

    for (i = 0; i < vCasos.size() - 1; i++) {
      demSem = (DataDemSem) vCasos.elementAt(i);
      demorat += ( (Double) demSem.get(DataDemSem.MED_DEMORA)).doubleValue();
      //coberturat += ( (Double) demSem.get(DataDemSem.COBERTURA)).doubleValue();
    }

    if (i > 0) {
      demorat = demorat / i;
      //coberturat = coberturat / i;
    }

    // calculamos los totales de cobertura
    totalescobertura = (Hashtable) vCasos.lastElement();
    String teor = (String) totalescobertura.get("NTEORT");
    String real = (String) totalescobertura.get("NREALT");
    double teori = (new Double(teor)).doubleValue();
    double reali = (new Double(real)).doubleValue();
    coberturat = (reali / teori) * 100;
    ////////////

    demora_total.append(Number.double2String(demorat));
    cobertura_total.append(Number.double2String(coberturat));
  }

  // informe comleto
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  public boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    String dato = null;

    // lo ponemo al inico de la p�gina y  borramos los datos que hab�a
    num_pag = 0;
    vCasos.setSize(0);

    ////# System_Out.println("GenerarInforme " );
    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      // obtiene los datos del servidor
      DataDemSem data = (DataDemSem) param.firstElement();
      if (conTodos) {
        data.put(DataDemSem.P_PAGINA, "-1");
        //E //# System_Out.println("Inicializamos datos -1 " + conTodos);
      }
      else {
        data.put(DataDemSem.P_PAGINA, Integer.toString(num_pag));
        data.bInformeCompleto = conTodos;
        //E //# System_Out.println("Inicializamos datos "+ num_pag + conTodos);
      }

      param.trimToSize();
      stub.setUrl(new URL(app.getURL() + strSERVLET));
      lista = (CLista) stub.doPost(MODO_SERVLETCOUNT, param);

      ///////////////// JLT
      /*      SrvDemSem srv = new SrvDemSem();
            srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                   "jdbc:oracle:thin:@10.160.12.54:1521:ORCL",
                                   "pista",
                                   "loteb98");
            lista = srv.doDebug(MODO_SERVLETCOUNT, param);
       */
      ///////////////////

      //if (lista != null && lista.size() > 2 ){
      // fallaba al elegir solo una semana como intervalo
      if (lista != null && lista.size() > 0) {
        vCasos = (Vector) lista;

        // ponemos las etiquetas del report
        fijarEtiqueas();
        data = (DataDemSem) lista.firstElement();

        num_reg = (String) data.get(DataDemSem.COUNT);

        //demora_total =(String) ((DataDemSem)lista.firstElement()).get(DataDemSem.MED_DEMORA);

        this.setTotalRegistros(num_reg); //Integer.toString(lista.size()));
        this.setRegistrosMostrados(Integer.toString(lista.size() - 1));
        ////# System_Out.println("Lista "+ num_reg + lista.getState());

        // establece las matrices de datos
        dataHandler.RegisterTable(vCasos, m_table, m_column, null);
        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        modoOperacion = modoNORMAL;
        Inicializar();
        return true;

      }
      else {
        vCasos.setSize(0);
        //msgBox = new CMessage(this.app, CMessage.msgAVISO, res.getString("msg18.Text") + lista);
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg18.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return false;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();

      modoOperacion = modoNORMAL;
      Inicializar();
      return false;
    }

  }

  public void MostrarGrafica() {
    TemplateManager tm = erw.GetTemplateManager();

    if (lista != null && lista.getState() == CLista.listaLLENA) {

      // oculta una de las secciones
      if (tm.IsVisible("RP_FTR")) {
        tm.SetVisible("PG_HDR", true);
        tm.SetVisible("DTL_00", true);
        tm.SetVisible("RP_FTR", false);
      }
      else {
        tm.SetVisible("PG_HDR", false);
        tm.SetVisible("DTL_00", false);
        tm.SetVisible("RP_FTR", true);
      }

      // repinta el grafico
      erwClient.refreshReport(true);

    }
  }

  public void setListaParam(CLista a_param) {
    param = a_param;
  }

  public CLista getListaParam() {
    return param;
  }

} //___________________________________ END_CLASS
