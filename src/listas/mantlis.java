package listas;

import java.util.ResourceBundle;

import capp.CApp;
import comun.constantes;

//import javax.swing.UIManager;
public class mantlis
    extends CApp {

//  final String strSERVLET = "servlet/SrvMantlis";
  final String strSERVLET = constantes.strSERVLET_MANT_LIS;

  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  String sEtCodigo = "";
  String sEtLista = "";

  public mantlis() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    ResourceBundle res = ResourceBundle.getBundle("listas.Res" + this.getIdioma());
    sEtCodigo = res.getString("msg1.Text");
    sEtLista = res.getString("msg2.Text");
    setTitulo(res.getString("msg7.Text"));

    // Constructor con el USU
    VerPanel("", new CMantenimientoListas(this, true));

  }
}
