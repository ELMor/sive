package listas;

import java.util.ResourceBundle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
//import javax.swing.*;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CDialog;
import capp.CMessage;
import catalogo.DataCat;

public class DialogLista
    extends CDialog {

  Panel pnl = new Panel();
  XYLayout xYLayout = new XYLayout();
  CCampoCodigo txtCod = new CCampoCodigo();
  Label lblCod = new Label();
  TextField txtDes = new TextField();
  Label lblDes = new Label();
  TextField txtDesL = new TextField();
  Label lblDesL = new Label();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  //Recursos Strings
  ResourceBundle res;
  protected PanelLista pnlLista;

  DialogLista_actionListener actionListener = new DialogLista_actionListener(this);

  public boolean bAceptar;
  protected boolean bModificar = false;

  public DialogLista(CApp app, PanelLista p, boolean b) {
    super(app);
    try {
      res = ResourceBundle.getBundle("listas.Res" + this.app.getIdioma());
      pnlLista = p;
      bModificar = b;
      jbInit();
      pack();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // init
  void jbInit() throws Exception {

    xYLayout.setHeight(142);
    txtCod.setBackground(new Color(255, 255, 150));
    xYLayout.setWidth(309);
    setSize(309, 142);
    setTitle(res.getString("msg3.Text"));

    lblCod.setText(res.getString("lblCod.Text"));
    txtDes.setBackground(new Color(255, 255, 150));
    txtDes.addActionListener(new DialogLista_txtDes_actionAdapter(this));
    lblDes.setText(res.getString("lblDes.Text"));
    txtDesL.addActionListener(new DialogLista_txtDesL_actionAdapter(this));
    lblDesL.setText(res.getString("lblDesL.Text"));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));

    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");

    pnl.setLayout(xYLayout);
    pnl.add(txtCod, new XYConstraints(130, 11, 85, 21));
    pnl.add(lblCod, new XYConstraints(7, 12, 80, 20));
    pnl.add(txtDes, new XYConstraints(130, 41, 167, 21));
    pnl.add(lblDes, new XYConstraints(7, 42, 80, 20));
    pnl.add(btnAceptar, new XYConstraints(129, 108, -1, -1));
    pnl.add(btnCancelar, new XYConstraints(219, 108, -1, -1));
    pnl.add(txtDesL, new XYConstraints(130, 72, 167, 21));
    pnl.add(lblDesL, new XYConstraints(7, 73, 114, 20));
    add(pnl, BorderLayout.CENTER);

    btnAceptar.setImage(pnlLista.imgs.getImage(3));
    btnCancelar.setImage(pnlLista.imgs.getImage(4));

    // idioma local
    if (app.getIdiomaLocal().length() == 0) {
      lblDesL.setVisible(false);
      txtDesL.setVisible(false);
    }
    else {
      lblDesL.setText(res.getString("lblDesL.Text") + app.getIdiomaLocal() +
                      "):");
    }

    if (bModificar) {
      txtCod.setEnabled(false);

    }
    btnCancelar.addActionListener(actionListener);
    btnAceptar.addActionListener(actionListener);
  }

  void btn_actionListener(ActionEvent e) {
    boolean bDispose = true;
    String sMsg = "";
    CMessage msgBox;

    try {

      // aceptar
      if (e.getActionCommand().equals("aceptar")) {

        // datos m�nimos
        if ( (txtCod.getText().length() == 0) ||
            (txtDes.getText().length() == 0)) {
          bDispose = false;
          sMsg = res.getString("msg4.Text");
        }

        // c�digo no repetido
        if (bDispose && !bModificar) {
          //System_out.println("Antes ver Lista valores  ");
          //System_out.println("Lista valores  tam"+  pnlLista.listaValores.size() + " Bien");
          for (int j = 0; j < pnlLista.listaValores.size(); j++) {
            if ( ( (DataCat) pnlLista.listaValores.elementAt(j)).getCod().
                equals(txtCod.getText())) {
              bDispose = false;
              sMsg = res.getString("msg5.Text");
              break;
            }
          }
        }

        // datos con tama�o permitido
        if (bDispose) {
          if (txtCod.getText().length() > 10) {
            bDispose = false;
            sMsg = res.getString("msg6.Text");
            txtCod.selectAll();
          }

          if (txtDes.getText().length() > 30) {
            bDispose = false;
            sMsg = res.getString("msg6.Text");
            txtDes.selectAll();
          }

          if (txtDesL.getText().length() > 30) {
            bDispose = false;
            sMsg = res.getString("msg6.Text");
            txtDesL.selectAll();
          }
        }

        // mensaje de aviso
        if (!bDispose) {
          msgBox = new CMessage(this.app, CMessage.msgAVISO, sMsg);
          msgBox.show();
          msgBox = null;
        }
        else {
          bAceptar = true;
        }
      }

      if (bDispose) {
        dispose();

      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
    }

  }

  String getCod() {
    return txtCod.getText();
  }

  String getDes() {
    return txtDes.getText();
  }

  String getDesL() {
    return txtDesL.getText();
  }

  void setCod(String cadena) {
    txtCod.setText(cadena);
  }

  void setDesL(String cadena) {
    txtDesL.setText(cadena);
  }

  void setDes(String cadena) {
    txtDes.setText(cadena);
  }

  void txtDes_actionPerformed(ActionEvent e) {

    boolean bDispose = true;
    String sMsg = "";
    CMessage msgBox;
    // datos m�nimos
    if ( (txtCod.getText().length() == 0) ||
        (txtDes.getText().length() == 0)) {
      bDispose = false;
      sMsg = res.getString("msg4.Text");
    }

    // c�digo no repetido
    if (bDispose && !bModificar) {
      for (int j = 0; j < pnlLista.listaValores.size(); j++) {
        if ( ( (DataCat) pnlLista.listaValores.elementAt(j)).getCod().equals(
            txtCod.getText())) {
          bDispose = false;
          sMsg = res.getString("msg5.Text");
          break;
        }
      }
    }

    // datos con tama�o permitido
    if (bDispose) {
      if (txtCod.getText().length() > 10) {
        bDispose = false;
        sMsg = res.getString("msg6.Text");
        txtCod.selectAll();
      }

      if (txtDes.getText().length() > 30) {
        bDispose = false;
        sMsg = res.getString("msg6.Text");
        txtDes.selectAll();
      }

      if (txtDesL.getText().length() > 30) {
        bDispose = false;
        sMsg = res.getString("msg6.Text");
        txtDesL.selectAll();
      }
    }

    // mensaje de aviso
    if (!bDispose) {
      msgBox = new CMessage(this.app, CMessage.msgAVISO, sMsg);
      msgBox.show();
      msgBox = null;
    }
    else {
      bAceptar = true;
    }

    if (bDispose) {
      dispose();
    }
  }

  void txtDesL_actionPerformed(ActionEvent e) {

  }
}

// escuchador de los eventos de los botones
class DialogLista_actionListener
    implements ActionListener {
  DialogLista adaptee;

  DialogLista_actionListener(DialogLista adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btn_actionListener(e);
  }
}

class DialogLista_txtDes_actionAdapter
    implements java.awt.event.ActionListener {
  DialogLista adaptee;

  DialogLista_txtDes_actionAdapter(DialogLista adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.txtDes_actionPerformed(e);
  }
}

class DialogLista_txtDesL_actionAdapter
    implements java.awt.event.ActionListener {
  DialogLista adaptee;

  DialogLista_txtDesL_actionAdapter(DialogLista adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.txtDesL_actionPerformed(e);
  }
}
