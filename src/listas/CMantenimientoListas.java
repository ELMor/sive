package listas;

import capp.CApp;
import capp.CListaMantenimiento;
import comun.constantes;
import sapp.StubSrvBD;

public class CMantenimientoListas
    extends CListaMantenimiento {

  public CMantenimientoListas(mantlis a) {
    super( (CApp) a,
          2,
          "100\n462",
          a.sEtCodigo + "\n" + a.sEtLista,
          new StubSrvBD(),
          a.strSERVLET,
          a.servletSELECCION_X_CODIGO,
          a.servletSELECCION_X_DESCRIPCION);

//    ConfigModo(true, false, true);
    ConfigModo(true, true, true);
  }

  public CMantenimientoListas(mantlis a, boolean USU) {
    super( (CApp) a,
          2,
          "100\n462",
          a.sEtCodigo + "\n" + a.sEtLista,
          new StubSrvBD(),
          a.strSERVLET,
          a.servletSELECCION_X_CODIGO,
          a.servletSELECCION_X_DESCRIPCION,
          constantes.keyBtnAnadir,
          constantes.keyBtnModificar,
          constantes.keyBtnBorrar,
          constantes.keyBtnAux
          );

//    ConfigModo(true, false, true);
    ConfigModo(true, true, true);
    this.setBorde(false);
  }

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {

    modoOperacion = modoESPERA;
    Inicializar();
    PanelLista panel = new PanelLista(this.app,
                                      PanelLista.modoALTA,
                                      null);
    panel.show();

    if ( (panel.bAceptar) && (panel.valido)) {
      lista.addElement(panel.datLista);
      RellenaTabla();
    }

    panel = null;

    modoOperacion = modoNORMAL;
    Inicializar();

  }

  // modifica l�nea de la lista
  public void btnModificar_actionPerformed(int i) {
    modoOperacion = modoESPERA;
    Inicializar();
    PanelLista panel = new PanelLista(this.app,
                                      PanelLista.modoMODIFICACION,
                                      (DataMantlis) lista.elementAt(i));
    panel.show();

    if (panel.bAceptar) {
      lista.removeElementAt(i);
      lista.insertElementAt(panel.datLista, i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();

  }

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed(int i) {

    modoOperacion = modoESPERA;
    Inicializar();
    PanelLista panel = new PanelLista(this.app,
                                      PanelLista.modoBAJA,
                                      (DataMantlis) lista.elementAt(i));
    panel.show();

    if (panel.bAceptar) {
      lista.removeElementAt(i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // rellena los par�metros para enviar al servlet
  public Object setComponente(String s) {
    return new DataMantlis(s);
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(Object o) {
    DataMantlis lis = (DataMantlis) o;

    return lis.getCdlista() + "&" + lis.getDslista();
  }

}