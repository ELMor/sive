package listas;

import java.net.URL;
import java.util.Enumeration;
import java.util.ResourceBundle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import capp.CTabla;
import catalogo.DataCat;
import comun.constantes;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import sapp.StubSrvBD;

public class PanelLista
    extends CDialog {
  public boolean valido = false; //Publica para poder insertar lista desde paq preguntas
  ResourceBundle res = ResourceBundle.getBundle("listas.Res" +
                                                this.app.getIdioma());
  //Modos de operaci�n de la ventana
  public static final int modoALTA = 0;
  public static final int modoBAJA = 1;
  public static final int modoCONSULTA = 3;
  public static final int modoESPERA = 2;
  public static final int modoMODIFICACION = 4;

  //Modos de operaci�n del Servlet
//  final String strSERVLET = "servlet/SrvMantlis";
  final String strSERVLET = constantes.strSERVLET_MANT_LIS;

  final int servletALTA = 0;
  final int servletMODIFICACION = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletOBTENER_VALORES = 10;
  final int servletLISTA_SELECCIONADA = 11;

  //constantes del panel
  //Constantes del panel
  final String imgNAME[] = {
      "images/alta2.gif",
      "images/modificacion2.gif",
      "images/baja2.gif",
      "images/aceptar.gif",
      "images/cancelar.gif"};

  //par�metros
  protected int modoOperacion = modoALTA;
  CLista listaValores = null;
  protected StubSrvBD stubCliente = new StubSrvBD();
  public boolean bAceptar = false;

  // escuchadores de eventos
  PanelLista_tabla_actionAdapter actionAdapter = new
      PanelLista_tabla_actionAdapter(this);
  PanelListabtnActionListener actionListener = new PanelListabtnActionListener(this);

  BorderLayout borderLayout = new BorderLayout();
  StatusBar statusBar = new StatusBar();
  ButtonControl btnAddVal = new ButtonControl();
  TextField descLista = new TextField();
  Label lblLista = new Label();
  Label lblDeslLisL = new Label();
  CCampoCodigo lista = new CCampoCodigo();
  ButtonControl btnModVal = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  Label lblDescLis = new Label();
  ButtonControl btnDelVal = new ButtonControl();
  XYLayout xYLayout = new XYLayout();
  CTabla tabla = new CTabla();
  TextField descLocal = new TextField();
  Panel pnl = new Panel();
  ButtonControl btnAceptar = new ButtonControl();

  // contenedor de imagenes
  public CCargadorImagen imgs = null;
  public DataMantlis datLista;

  // constructor
  public PanelLista(CApp a,
                    int modo,
                    DataMantlis lis) {
    super(a);
    try {
      modoOperacion = modo;
      datLista = lis;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // iniciador
  void jbInit() throws Exception {

    this.setLayout(borderLayout);

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnAddVal.setImage(imgs.getImage(0));
    btnModVal.setImage(imgs.getImage(1));
    btnDelVal.setImage(imgs.getImage(2));
    btnAceptar.setImage(imgs.getImage(3));
    btnCancelar.setImage(imgs.getImage(4));

    xYLayout.setWidth(450);
    xYLayout.setHeight(400);
    pnl.setLayout(xYLayout);

    lblLista.setText(res.getString("lblLista.Text"));
    lblDeslLisL.setText(res.getString("lblDeslLisL.Text"));
    lblDescLis.setText(res.getString("lblDescLis.Text"));

    lista.setBackground(new Color(255, 255, 150));
    descLista.setBackground(new Color(255, 255, 150));
    descLista.addActionListener(new PanelLista_descLista_actionAdapter(this));

    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "60\n310"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tabla.ColumnButtonsStrings")), '\n'));
    tabla.setNumColumns(2);
    descLocal.addActionListener(new PanelLista_descLocal_actionAdapter(this));

    btnAceptar.setActionCommand("aceptar");
    btnDelVal.setActionCommand("delvalor");
    btnCancelar.setActionCommand("cancelar");
    btnModVal.setActionCommand("modvalor");
    statusBar.setBevelOuter(BevelPanel.LOWERED);
    statusBar.setBevelInner(BevelPanel.LOWERED);
    btnAddVal.setActionCommand("advalor");
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    btnAceptar.addActionListener(actionListener);
    tabla.addActionListener(actionAdapter);
    btnDelVal.addActionListener(actionListener);
    btnCancelar.addActionListener(actionListener);
    btnModVal.addActionListener(actionListener);
    btnAddVal.addActionListener(actionListener);

    pnl.add(lista, new XYConstraints(104, 7, 86, -1));
    pnl.add(lblLista, new XYConstraints(5, 7, 95, -1));
    pnl.add(descLista, new XYConstraints(104, 37, 292, -1));
    pnl.add(lblDescLis, new XYConstraints(5, 37, 100, -1));
    pnl.add(descLocal, new XYConstraints(189, 68, 208, -1));
    pnl.add(lblDeslLisL, new XYConstraints(5, 68, -1, -1));
    pnl.add(tabla, new XYConstraints(5, 114, 389, 163));
    pnl.add(btnAddVal, new XYConstraints(7, 288, -1, -1));
    pnl.add(btnModVal, new XYConstraints(40, 288, -1, -1));
    pnl.add(btnDelVal, new XYConstraints(73, 288, -1, -1));
    pnl.add(btnAceptar, new XYConstraints(186, 314, 100, 26));
    pnl.add(btnCancelar, new XYConstraints(294, 314, 100, 26));

    this.setSize(new Dimension(411, 400));
    this.add(statusBar, BorderLayout.SOUTH);
    this.add(pnl, BorderLayout.CENTER);

    // idioma local
    if (app.getIdiomaLocal().length() == 0) {
      lblDeslLisL.setVisible(false);
      descLocal.setVisible(false);
    }
    else {
      lblDeslLisL.setText(res.getString("lblDeslLisL.Text") +
                          app.getIdiomaLocal() + "):");
    }

    // modo baja/consulta
    if (modoOperacion == modoBAJA) {
      lista.setText(datLista.getCdlista());
      descLista.setText(datLista.getDslista());
      descLocal.setText(datLista.getDsllista());

      // obtiene lalos valores de la lista
      ObtenerValores(datLista.getCdlista());

      // pinta las l�neas
      escribirLineas();

      // consulta si la lista est� seleccionada
      String sStatus = res.getString("msg8.Text");

      CLista param = new CLista();
      param.addElement(new DataMantlis(datLista.getCdlista()));
      stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
      param = (CLista) stubCliente.doPost(servletLISTA_SELECCIONADA, param);

      // rellena los datos
      modoOperacion = modoCONSULTA;

      if (param.size() > 0) {
        sStatus = sStatus + (String) param.firstElement();

        if ( ( (String) param.firstElement()).equals("N")) {
          modoOperacion = modoBAJA;

        }
      }
      else {
        sStatus = sStatus + "S";
      }

      statusBar.setText(sStatus);
    }

    // modo modificar/consulta
    if (modoOperacion == modoMODIFICACION) {
      lista.setText(datLista.getCdlista());
      descLista.setText(datLista.getDslista());
      descLocal.setText(datLista.getDsllista());

      // obtiene lalos valores de la lista
      ObtenerValores(datLista.getCdlista());

      // pinta las l�neas
      escribirLineas();

      // consulta si la lista est� seleccionada
      String sStatus = res.getString("msg8.Text");

      CLista param = new CLista();
      param.addElement(new DataMantlis(datLista.getCdlista()));
      stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
      param = (CLista) stubCliente.doPost(servletLISTA_SELECCIONADA, param);

      // rellena los datos
      modoOperacion = modoCONSULTA;

      if (param.size() > 0) {
        sStatus = sStatus + (String) param.firstElement();

        if ( ( (String) param.firstElement()).equals("N")) {
          modoOperacion = modoMODIFICACION;

        }
      }
      else {
        sStatus = sStatus + "S";
      }

      statusBar.setText(sStatus);
    }

    Inicializar();
  }

  // modos de operaci�n
  public void Inicializar() {
    switch (modoOperacion) {

      case modoALTA:
        setTitle(res.getString("msg10.Text"));
        btnAddVal.setEnabled(true);
        btnModVal.setEnabled(true);
        btnDelVal.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        tabla.setEnabled(true);
        lista.setEnabled(true);
        descLista.setEnabled(true);
        descLocal.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        listaValores = new CLista();

        tabla.addActionListener(actionAdapter);
        break;

      case modoMODIFICACION:
        setTitle(res.getString("msg15.Text"));
        btnAddVal.setEnabled(true);
        btnModVal.setEnabled(true);
        btnDelVal.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        tabla.setEnabled(true);
        lista.setEnabled(false);
        descLista.setEnabled(true);
        descLocal.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        tabla.addActionListener(actionAdapter);
        break;

      case modoBAJA:
        setTitle(res.getString("msg11.Text"));
        btnAddVal.setEnabled(false);
        btnModVal.setEnabled(false);
        btnDelVal.setEnabled(false);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        tabla.setEnabled(true);
        lista.setEnabled(false);
        descLista.setEnabled(false);
        descLocal.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        tabla.removeActionListener(actionAdapter);
        break;

      case modoESPERA:
        tabla.setEnabled(false);
        btnAddVal.setEnabled(false);
        btnModVal.setEnabled(false);
        btnDelVal.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        lista.setEnabled(false);
        descLista.setEnabled(false);
        descLocal.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoCONSULTA:
        setTitle(res.getString("msg12.Text"));
        tabla.removeActionListener(actionAdapter);
        tabla.setEnabled(true);
        btnAddVal.setEnabled(false);
        btnModVal.setEnabled(false);
        btnDelVal.setEnabled(false);
        btnAceptar.setVisible(false);
        btnCancelar.setLabel(res.getString("btnCancelar.Label"));
        btnCancelar.setEnabled(true);
        lista.setEnabled(false);
        descLista.setEnabled(false);
        descLocal.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
  }

  // a�adir un valor
  void btnAddVal_actionPerformed(ActionEvent e) {

    DialogLista dial;

    dial = new DialogLista(app, this, false);
    dial.show();

    // a�ade el valor
    if (dial.bAceptar) {
      listaValores.addElement(new DataCat(dial.getCod(),
                                          dial.getDes(),
                                          dial.getDesL()));
      // repinta la tabla
      escribirLineas();
    }

    dial = null;
    btnAddVal.requestFocus();
  }

  // modifica un valor
  void btnModVal_actionPerformed() {
    DialogLista dial;
    int indice = tabla.getSelectedIndex();

    if (indice != BWTEnum.NOTFOUND) {

      DataCat datValor = (DataCat) listaValores.elementAt(indice);

      dial = new DialogLista(app, this, true);

      // graba el valor a modificar
      dial.setCod(datValor.getCod());
      dial.setDes(datValor.getDes());
      dial.setDesL(datValor.getDesL());
      dial.show();

      // actualiza los datos en la lista
      if (dial.bAceptar) {
        listaValores.removeElementAt(indice);
        listaValores.insertElementAt(new DataCat(dial.getCod(),
                                                 dial.getDes(),
                                                 dial.getDesL()), indice);

        // repinta la tabla
        escribirLineas();
      }

      dial = null;
    }
  }

  // borra un valor
  void btnDelVal_actionPerformed(ActionEvent e) {
    if (tabla.getSelectedItem() != null) {
      CMessage dialogo = new CMessage(this.app, 2, res.getString("msg13.Text"));
      dialogo.show();

      if (dialogo.getResponse() == true) {
        listaValores.removeElementAt(tabla.getSelectedIndex());
        escribirLineas();
      }
    }
  }

  // graba la lista en BD
  void A�adir() {
    CMessage msgBox = null;
    CLista data = null;
    DataMantlis lis = null;
    String miDato = "";

    if (isDataValid()) {
      try {

        // modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        // par�metros
        data = new CLista();
        switch (this.app.getCA().length()) {
          case 0:
            miDato = "_00";
            break;
          case 1:
            miDato = "_0" + this.app.getCA();
            break;
          case 2:
            miDato = "_" + this.app.getCA();
            break;
        }
        lis = new DataMantlis(lista.getText().toUpperCase() + miDato,
                              descLista.getText(),
                              descLocal.getText());
        lis.setValores(listaValores);
        data.addElement(lis);
        data.trimToSize();
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
        stubCliente.doPost(servletALTA, data);
        datLista = lis;
        dispose();

      }
      catch (Exception excepc) {
        excepc.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
        msgBox.show();
      }
      modoOperacion = modoALTA;
      Inicializar();
    }
  }

  // graba la lista en BD
  void Modificar() {
    CMessage msgBox = null;
    CLista data = null;
    DataMantlis lis = null;
    String miDato = "";

    if (isDataValid()) {
      try {

        // modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        // par�metros
        data = new CLista();

        lis = new DataMantlis(lista.getText(),
                              descLista.getText(),
                              descLocal.getText());
        lis.setValores(listaValores);
        data.addElement(lis);
        data.trimToSize();

        stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
        stubCliente.doPost(servletMODIFICACION, data);

        /*
                           // debug
                            SrvMantlis srv = new SrvMantlis();
                            // par�metros jdbc
             srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                                   "sive_desa",
                                   "sive_desa");
                            srv.doDebug(servletMODIFICACION, data);
         */
        datLista = lis;
        dispose();

      }
      catch (Exception excepc) {
        excepc.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
        msgBox.show();
      }
      modoOperacion = modoMODIFICACION;
      Inicializar();
    }
  }

  // borra la lista en la BD
  void Borrar() {
    CMessage msgBox = null;
    CLista data = null;
    DataMantlis lis = null;

    msgBox = new CMessage(app, CMessage.msgPREGUNTA, res.getString("msg14.Text"));
    msgBox.show();

    // confirma el borrado
    if (msgBox.getResponse()) {

      try {

        // modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        // par�metros
        data = new CLista();
        lis = new DataMantlis(lista.getText());
        data.addElement(lis);
        data.trimToSize();
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
        stubCliente.doPost(servletBAJA, data);
        dispose();

      }
      catch (Exception excepc) {
        excepc.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
        msgBox.show();
      }
      modoOperacion = modoALTA;
      Inicializar();
    }
  }

  // limpia la pantalla
  void vaciarPantalla() {
    lista.setText("");
    descLista.setText("");
    descLocal.setText("");
    if (tabla.countItems() > 0) {
      tabla.deleteItems(0, tabla.countItems() - 1);
    }
  }

  // rellena la tabla
  void escribirLineas() {
    DataCat datValor;
    String datLineaEscribir = new String(""); ;
    int tam;

    //Borramos la tabla que hab�a
    tam = tabla.countItems();
    if (tam > 0) {
      tabla.deleteItems(0, tam - 1);

    }
    Enumeration enum = listaValores.elements();

    while (enum.hasMoreElements()) {
      //Recogemos los datos de una l�nea
      datValor = (DataCat) (enum.nextElement());

      if (app.getIdioma() == 0) {
        datLineaEscribir = datValor.getCod() + "&" +
            datValor.getDes();
      }
      else {
        datLineaEscribir = datValor.getCod() + "&" +
            datValor.getDesL();

        //A�adimos la l�nea a la tabla
      }
      tabla.addItem(datLineaEscribir, '&'); //'&' es el car�cter separador de datos (cada datos va a una columnna)
    }
  }

  // obtenci�n de los valores
  public void ObtenerValores(String sCod) {
    CLista data = null;
    CMessage msgBox = null;

    try {
      // prepara los par�metros
      data = new CLista();

      data.addElement(new DataMantlis(sCod));
      stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
      listaValores = (CLista) stubCliente.doPost(servletOBTENER_VALORES, data);
      data = null;
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
    }
  }

  // comprueba datos m�nimos
  public boolean isDataValid() {
    boolean bIndicador = true;
    CMessage msgBox;
    String sMsg = "";

    // datos m�nimos
    if ( (lista.getText().length() == 0) ||
        (descLista.getText().length() == 0) ||
        (tabla.countItems() == 0)) {
      bIndicador = false;
      sMsg = res.getString("msg16.Text");
    }

    if (bIndicador) {
      // datos con tama�o permitido

      //En modo modoALTA el c�digo no puede exceder de 5 (luego se a�ade sufijo)
      if (modoOperacion == modoALTA) {
        if (lista.getText().length() > 5) {
          bIndicador = false;
          sMsg = res.getString("msg17.Text");
          lista.selectAll();
        }
      }
      //En modo modoMODIFICACION el c�digo no puede exceder de 8
      if (modoOperacion == modoMODIFICACION) {
        if (lista.getText().length() > 8) {
          bIndicador = false;
          sMsg = res.getString("msg17.Text");
          lista.selectAll();
        }
      }

      if (descLista.getText().length() > 40) {
        bIndicador = false;
        sMsg = res.getString("msg17.Text");
        descLista.selectAll();
      }

      if (descLocal.getText().length() > 40) {
        bIndicador = false;
        sMsg = res.getString("msg17.Text");
        descLocal.selectAll();
      }
    }

    // mensaje de aviso
    if (!bIndicador) {
      msgBox = new CMessage(this.app, CMessage.msgAVISO, sMsg);
      msgBox.show();
      msgBox = null;
    }
    valido = bIndicador;
    return bIndicador;
  }

  void descLista_actionPerformed(ActionEvent e) {
    A�adir();
  }

  void descLocal_actionPerformed(ActionEvent e) {
    A�adir();
  }
} // fin de la clase

    /******************************************************************************/

// action listener de evento en botones
class PanelListabtnActionListener
    implements ActionListener, Runnable {
  PanelLista adaptee = null;
  ActionEvent e = null;

  public PanelListabtnActionListener(PanelLista adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    if (e.getActionCommand().equals("advalor")) { // a�adir valor
      adaptee.btnAddVal_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("modvalor")) { // modificar valor
      adaptee.btnModVal_actionPerformed();
    }
    else if (e.getActionCommand().equals("delvalor")) { // borrar valor
      adaptee.btnDelVal_actionPerformed(e);
    }
    else if (e.getActionCommand() == "aceptar") { // aceptar
      adaptee.bAceptar = true;

      // realiza la operaci�n
      switch (adaptee.modoOperacion) {
        case PanelLista.modoALTA:
          adaptee.A�adir();
          break;
        case PanelLista.modoMODIFICACION:
          adaptee.Modificar();
          break;
        case PanelLista.modoBAJA:
          adaptee.Borrar();
          break;
      }

    }
    else if (e.getActionCommand() == "cancelar") { // cancelar
      adaptee.bAceptar = false;
      adaptee.dispose();
    }
  }
}

// escuchador de los click en la tabla
class PanelLista_tabla_actionAdapter
    implements jclass.bwt.JCActionListener {
  PanelLista adaptee;

  PanelLista_tabla_actionAdapter(PanelLista adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    adaptee.btnModVal_actionPerformed();
  }
}

class PanelLista_descLista_actionAdapter
    implements java.awt.event.ActionListener {
  PanelLista adaptee;

  PanelLista_descLista_actionAdapter(PanelLista adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.descLista_actionPerformed(e);
  }
}

class PanelLista_descLocal_actionAdapter
    implements java.awt.event.ActionListener {
  PanelLista adaptee;

  PanelLista_descLocal_actionAdapter(PanelLista adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.descLocal_actionPerformed(e);
  }
}
