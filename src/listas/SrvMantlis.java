package listas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import catalogo.DataCat;
import sapp.DBServlet;

public class SrvMantlis
    extends DBServlet {

  // modos de operaci�n
  final int servletALTA = 0;
  final int servletMODIFICACION = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletOBTENER_VALORES = 10;
  final int servletLISTA_SELECCIONADA = 11;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    // objetos de datos
    CLista data = new CLista();
    CLista lVal = null;
    DataCat dVal = null;
    DataMantlis dMantlis = null;
    DataMantlis datLinea = null;
    int iValor = 1;

    //Para las descripciones de listas
    String sDes = "";
    String sDesL = "";
    //Para las descripciones de valores de las listas
    String sDesValor = "";
    String sDesValorL = "";

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    dMantlis = (DataMantlis) param.firstElement();

    // modos de operaci�n
    try {
      switch (opmode) {

        // alta de lista de valores
        case servletALTA:

          // prepara la query
          query =
              "insert into SIVE_LISTAS (CD_LISTA, DS_LISTA, DSL_LISTA) values (?, ?, ?)";
          st = con.prepareStatement(query);
          // codigo
          st.setString(1, dMantlis.getCdlista().trim().toUpperCase());
          // descripci�n
          st.setString(2, dMantlis.getDslista().trim());
          // descripci�n local
          if (dMantlis.getDsllista().trim().length() > 0) {
            st.setString(3, dMantlis.getDsllista().trim());
          }
          else {
            st.setNull(3, java.sql.Types.VARCHAR);
            // lanza la query
          }
          st.executeUpdate();
          st.close();

          //Ahora habr� que a�adir los valores de las listas
          lVal = dMantlis.getValores();

          query = "insert into SIVE_LISTA_VALORES (CD_LISTA, CD_LISTAP, DS_LISTAP, DSL_LISTAP) values (?, ?, ?, ?)";
          st = con.prepareStatement(query);
          for (int j = 0; j < lVal.size(); j++) {
            // prepara la query
            // codigo lista
            st.setString(1, dMantlis.getCdlista().trim());
            // valor
            dVal = (DataCat) lVal.elementAt(j);
            // c�digo valor
            st.setString(2, dVal.getCod().trim());
            // descripci�n
            st.setString(3, dVal.getDes().trim());
            // descripci�n local
            if (dVal.getDesL().trim().length() > 0) {
              st.setString(4, dVal.getDesL().trim());
            }
            else {
              st.setNull(4, java.sql.Types.VARCHAR);
              // lanza la query
            }
            st.executeUpdate();
          }
          st.close();

          break;

          // modificacion de lista de valores
          //Lo que se hace es borrar valores que ya hab�a e insertar los nuevos
        case servletMODIFICACION:

          // elimina los valores
          query = "DELETE FROM SIVE_LISTA_VALORES WHERE CD_LISTA = ?";
          st = con.prepareStatement(query);
          st.setString(1, dMantlis.getCdlista().trim());
          st.executeUpdate();
          st.close();

          // prepara la query
          query =
              "update SIVE_LISTAS SET DS_LISTA = ? , DSL_LISTA = ?  WHERE CD_LISTA = ?";
          st = con.prepareStatement(query);
          // descripci�n
          st.setString(1, dMantlis.getDslista().trim());
          // descripci�n local
          if (dMantlis.getDsllista().trim().length() > 0) {
            st.setString(2, dMantlis.getDsllista().trim());
          }
          else {
            st.setNull(2, java.sql.Types.VARCHAR);
            // codigo
          }
          st.setString(3, dMantlis.getCdlista().trim().toUpperCase());

          // lanza la query
          st.executeUpdate();
          st.close();

          //Ahora habr� que a�adir los valores de las listas
          lVal = dMantlis.getValores();

          query = "insert into SIVE_LISTA_VALORES (CD_LISTA, CD_LISTAP, DS_LISTAP, DSL_LISTAP) values (?, ?, ?, ?)";
          st = con.prepareStatement(query);
          for (int j = 0; j < lVal.size(); j++) {
            // prepara la query
            // codigo lista
            st.setString(1, dMantlis.getCdlista().trim());
            // valor
            dVal = (DataCat) lVal.elementAt(j);
            // c�digo valor
            st.setString(2, dVal.getCod().trim());
            // descripci�n
            st.setString(3, dVal.getDes().trim());
            // descripci�n local
            if (dVal.getDesL().trim().length() > 0) {
              st.setString(4, dVal.getDesL().trim());
            }
            else {
              st.setNull(4, java.sql.Types.VARCHAR);
              // lanza la query
            }
            st.executeUpdate();
          }
          st.close();

          break;

          // Baja de lista de valores
        case servletBAJA:

          // elimina los valores
          query = "DELETE FROM SIVE_LISTA_VALORES WHERE CD_LISTA = ?";
          st = con.prepareStatement(query);
          st.setString(1, dMantlis.getCdlista().trim());
          st.executeUpdate();
          st.close();

          // elimina la lista
          query = "DELETE FROM SIVE_LISTAS WHERE CD_LISTA = ?";
          st = con.prepareStatement(query);
          st.setString(1, dMantlis.getCdlista().trim());
          st.executeUpdate();
          st.close();

          break;

          // b�squeda de listas de valores
        case servletSELECCION_X_CODIGO:
        case servletSELECCION_X_DESCRIPCION:

          // prepara la query
          // ARG: upper (7/5/02)
          if (param.getFilter().length() > 0) {
            if (opmode == servletSELECCION_X_CODIGO) {
              query = "select CD_LISTA, DS_LISTA, DSL_LISTA from SIVE_LISTAS where CD_LISTA like ? and CD_LISTA > ? order by CD_LISTA";
            }
            else {
              query = "select CD_LISTA, DS_LISTA, DSL_LISTA from SIVE_LISTAS where upper(DS_LISTA) like upper(?) and upper (DS_LISTA) > upper(?) order by DS_LISTA";

            }
          }
          else {

            if (opmode == servletSELECCION_X_CODIGO) {
              query = "select CD_LISTA, DS_LISTA, DSL_LISTA from SIVE_LISTAS where CD_LISTA like ? order by CD_LISTA";
            }
            else {
              query = "select CD_LISTA, DS_LISTA, DSL_LISTA from SIVE_LISTAS where upper(DS_LISTA) like upper(?) order by DS_LISTA";
            }
          }

          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);

          // filtro
          if (opmode == servletSELECCION_X_CODIGO) {
            st.setString(1, dMantlis.getCdlista().trim() + "%");
          }
          else {
            st.setString(1, "%" + dMantlis.getCdlista().trim() + "%");

            // paginaci�n
          }
          if (param.getFilter().length() > 0) {
            st.setString(2, param.getFilter().trim());
          }
          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {

            //____________________

            sDes = rs.getString("DS_LISTA");
            sDesL = rs.getString("DSL_LISTA");

            // obtiene la descripcion principal en funci�n del idioma si el cliente no es un mantenimiento
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (param.getMantenimiento() == false)) {
              if (sDesL != null) {
                sDes = sDesL;
              }
            }

            // crea el objeto de la lista
            datLinea = new DataMantlis(rs.getString("CD_LISTA"),
                                       sDes,
                                       sDesL);

            //________________________

            // control de tama�o
            if (iValor > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (opmode == servletSELECCION_X_CODIGO) {
                data.setFilter( ( (DataMantlis) data.lastElement()).getCdlista());
              }
              else {
                data.setFilter( ( (DataMantlis) data.lastElement()).getDslista());
              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // A�ade un nodo
            data.addElement(datLinea);

            iValor++;

          }
          rs.close();
          st.close();
          break;

          // escoger un item
        case servletOBTENER_X_CODIGO:
        case servletOBTENER_X_DESCRIPCION:

          // prepara la query
          // prepara la query
          if (opmode == servletOBTENER_X_CODIGO) { //**** like y no = para poder elegir indicando la CA o no en el c�digo
            query =
                "select CD_LISTA, DS_LISTA, DSL_LISTA from SIVE_LISTAS where CD_LISTA like ?";
          }
          else {
            query =
                "select CD_LISTA, DS_LISTA, DSL_LISTA from SIVE_LISTAS where DS_LISTA = ?";

            // lanza la query
          }
          st = con.prepareStatement(query);
          if (opmode == servletOBTENER_X_CODIGO) {
            st.setString(1, dMantlis.getCdlista().trim() + "%");
          }
          else {
            st.setString(1, dMantlis.getCdlista().trim());
          }
          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {

            //____________________

            sDes = rs.getString("DS_LISTA");
            sDesL = rs.getString("DSL_LISTA");

            // obtiene la descripcion principal en funci�n del idioma si el cliente no es un mantenimiento
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (param.getMantenimiento() == false)) {
              if (sDesL != null) {
                sDes = sDesL;
              }
            }

            // crea el objeto de la lista
            datLinea = new DataMantlis(rs.getString("CD_LISTA"),
                                       sDes,
                                       sDesL);

            //________________________

            // A�ade un nodo
            data.addElement(datLinea);
          }
          rs.close();
          st.close();
          break;

        case servletOBTENER_VALORES:

          //Ahora localizamos los valores
          query = "select CD_LISTA, CD_LISTAP, DS_LISTAP, DSL_LISTAP from SIVE_LISTA_VALORES where CD_LISTA = ?";
          st = con.prepareStatement(query);

          st.setString(1, dMantlis.getCdlista());
          rs = st.executeQuery();

          while (rs.next()) {

            sDesValor = rs.getString("DS_LISTAP");
            sDesValorL = rs.getString("DSL_LISTAP");

            // obtiene la descripcion principal en funci�n del idioma si el cliente no es un mantenimiento
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
                (param.getMantenimiento() == false)) {
              if (sDesValor != null) {
                sDesValor = sDesValorL;
              }
            }

            //Aqu� metemos los valores por bucle
            data.addElement(new DataCat(rs.getString("CD_LISTAP"),
                                        sDesValor,
                                        sDesValorL));
          }

          rs.close();
          st.close();

          break;

          // pregunta seleccionada
        case servletLISTA_SELECCIONADA:
          String sIndicador = "N";
          query = "select COUNT(*) from SIVE_PREGUNTA where CD_LISTA = ?";
          st = con.prepareStatement(query);
          st.setString(1, dMantlis.getCdlista().trim());
          rs = st.executeQuery();

          if (rs.next()) {
            if (rs.getInt(1) > 0) {
              sIndicador = "S";

            }
          }
          rs.close();
          st.close();

          data.addElement(sIndicador);

          break;

      }
      // rollback
    }
    catch (Exception e) {
      con.rollback();
      throw e;
    }

    // cierra la conexion y acaba el procedimiento doWork
    con.commit();
    closeConnection(con);

    if (data != null) {
      data.trimToSize();
    }
    return data;
  }
}
