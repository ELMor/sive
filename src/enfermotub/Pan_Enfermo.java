/*
  Ha sido modificado con respecto a la versi�n original
  en el sentido de que ahora permite efectuar la selecci�n de
  enfermos y/o contactos de casos pertenencientes al registro de
  Tuberculosis.
 */

package enfermotub;

import java.net.URL;
//import nivel1.*;
//import notdata.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;

import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import comun.CFechaSimple;
import comun.Common;
import comun.DataEnfermo;
import comun.DataMunicipioEDO;
import comun.DataNivel1;
import comun.DataZBS;
import comun.DataZBS2;
import comun.Fechas;
import comun.constantes;
//import zbs.*;
import sapp.StubSrvBD;
import suca.panelsuca;
import suca.zonificacionSanitaria;

public class Pan_Enfermo
    extends CPanel
    implements zonificacionSanitaria {

  // Para conocer la aplicaci�n desde la que se ejecuta
  private final String APP_TUBERCULOSIS = constantes.APP_TUBERCULOSIS;

  private String sCodAplicacion = null;

  //ResourceBundle res;  09/02/2000

  //modos de operaci�n de la ventana
  public static final int modoINICIO = 0; // solo puede apretar boton b�squeda

  public static final int modoMODIFICAR = 1; // modifica datos del enfermo
  public static final int modoSELECCION = 2;
  public static final int modoOBTENER = 4;
  public static final int modoESPERA = 5; // para cuano se va a por datos
  public static final int modoPAIS = 6; // no pinchar en CA y PROV
  public static final int modoNOPAIS = 7; // pinchar en CA
  public static final int modoCA = 8;
  public static final int modoNOCA = 9;
  public static final int modoPROVINCIA = 10;
  public static final int modoNOPROVINCIA = 11;
  public static final int modoFEC = 12; // fecha escrita
  public static final int modoNOFEC = 13; // fecha calculada

  //servlets para la busqueda de niveles
  final String strSERVLETNivel1 = "servlet/SrvNivel1Com";
  final String strSERVLETNivel2 = "servlet/SrvZBS2Com";
  final String strSERVLETZona = "servlet/SrvMunCom";

  //  final String strSERVLETZonif = "servlet/SrvMunicipio";
  final String strSERVLET_MUNICIPIO_CONT = "servlet/SrvMunicipioContCom";

//Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  protected String codEnfermo = null;

  /** copia de la edad antigua para saber si ha cambiado */
  protected String strEdadBk = null;

  // Listas generales  de datos fijos que se obtienen al principio
  /** Lista que aporta los c�digo de identificaci�n */
  protected CLista listaId = null;
  /** Lista que contiene las provincias */
  protected CLista listaSexo = null;
  /** Lista que contiene las provincias */
  protected CLista listaMBaja = null;
  /** es la lista que contiene los datos del enfermo */
  protected CLista listaEnfermo = null;
  /** es la lista que contiene las otras direcciones */
  protected CLista listaOtrasDirecciones = null;

  /** para la comunicacion con el servlet */
  public StubSrvBD stubCliente = new StubSrvBD();

  /** codigo del enfermo modificado */
  protected String strCodigoBK = "";

  /** para la traducci�n de las cadenas a otro idioma */
  //  protected ResourceBundle res = ResourceBundle.getBundle("enfermo.Res");

  /** indica si tiene permiso para ver los nombres o las siglas*/
  protected boolean bPermiso = false;
  // sincronizaci�n
  protected boolean sinBloquear = true;

  final int servletSELECCION_MUNICIPIO_X_CODIGO = 9;
  public boolean btramero = false;

  // par�metros
  public int modoAnterior = 0;
  protected int modo = 0;

  //Indica si se han cambiado datos en otras direcciones desde �ltima vez que se
  //grabaron todos los datos en b. datos (al pulsar aceptar de este panel)
  boolean bCambioOtrasDirecciones = false;

  // objeto que sirve de sincronizaci�n
  Object sincro = new Object();

  //COMPONENTES VISUALES
  XYLayout xYLayout1 = new XYLayout();
  Label lblCodigo = new Label();
  CCampoCodigo txtCodigo = new CCampoCodigo();
  ButtonControl btnCodigo = new ButtonControl();
  Label lblApellido1 = new Label();
  TextField txtApellido1 = new TextField();
  TextField txtApellido2 = new TextField();
  TextField txtNombre = new TextField();
  Label lblFecNacimiento = new Label();
  //Cambio de TextField por fechas.CFecha ARS 26-05-01
  // El CFechaSimple estaba ya comentado
  //---
//  TextField txtFecNacimiento = new TextField();
  fechas.CFecha txtFecNacimiento = new fechas.CFecha("N");
  //CFechaSimple txtFecNacimiento = new CFechaSimple("N");
  Label lblEdad = new Label();
  TextField txtEdad = new TextField();
  Choice chcEdad = new Choice();
  Label lblIdentificacion = new Label();
  Choice chcIdentificacion = new Choice();
  CampoNIF txtIdentificacion = new CampoNIF();
  Label lblSexo = new Label();
  Choice chcSexo = new Choice();
  TextField txtNivel1L = new TextField();
  ButtonControl btnNivel1 = new ButtonControl();
  TextField txtNivel2L = new TextField();
  ButtonControl btnNivel2 = new ButtonControl();
  TextField txtZBSL = new TextField();
  ButtonControl btnZBS = new ButtonControl();
  Checkbox chkbRevisao = new Checkbox();
  Label lblMBaja = new Label();
  Choice chcMBaja = new Choice();
  Label lblFecha = new Label();
  //Cambio de TextField por fechas.CFecha ARS 26-05-01
  //---
//  TextField txtFecha = new TextField();
  fechas.CFecha txtFecha = new fechas.CFecha("N");
  ButtonControl btnODirecciones = new ButtonControl();
  ButtonControl btnActualizar = new ButtonControl();
  //ButtonControl btnCancelar = new ButtonControl();
  Label lblObservacion = new Label();
  TextField txtObservacion = new TextField();
  Label lblTelefono = new Label();
  TextField txtTelefono = new TextField();
  Label chcFcalc = new Label();
  TextField txtNivel1 = new TextField();
  TextField txtNivel2 = new TextField();
  TextField txtZBS = new TextField();
  GroupBox pnlIdentificacion = new GroupBox();
  GroupBox pnlLocalizacion = new GroupBox();
  Label lblNivel1 = new Label();
  Label lblNivel2 = new Label();
  Label lblZBS = new Label();

  // SUCA
  panelsuca panSuca = null;

  // Para seleccionar lo que se debe buscar
  Checkbox chkEnfermos = new Checkbox();
  Checkbox chkContactos = new Checkbox();

  //______________________________________________ GESTOR de EVENTOS
  Pan_EnfermoBtnActionListener btnActionListener = new
      Pan_EnfermoBtnActionListener(this);
  Pan_EnfermoTextAdapter textAdapter = new Pan_EnfermoTextAdapter(this);
  PanBusEnfermoChoiceItemListener chItemListener = new
      PanBusEnfermoChoiceItemListener(this);
  PanBusEnfermoTextFocusListener txtFocusListener = new
      PanBusEnfermoTextFocusListener(this);

  // constructor
  public Pan_Enfermo(CApp a) {
    try {
      setApp(a);
      //res = ResourceBundle.getBundle("enfermo.Res" + a.getIdioma()); PDP 09/02/2000

      sCodAplicacion = a.getParametro("COD_APLICACION");

      // lanzamos un thread que trae los datos del choice
      LeerChoices lcho = new LeerChoices(this);
      Thread th = new Thread(lcho);
      th.start();

      // tramero
      boolean tramero = true;

      if (app.getIT_TRAMERO().equals("N")) {
        tramero = false;

        // visibilidad de datos sensibles
      }
      int modo = panelsuca.modoINICIO;

      if (app.getIT_FG_ENFERMO().equals("N")) {
        modo = panelsuca.modoCONFIDENCIAL;

      }
      panSuca = new panelsuca(app, tramero, modo, false, this);

      btramero = panSuca.getModoTramero();

      //// System_out.println("TRAMERO en enfermo " + btramero);

      jbInit();

      /// a�adimos el de edad,  que se pone a pelo
      /// a�adimos campos nulos
      chcEdad.add("a�os");
      chcEdad.add("meses");
      chcIdentificacion.add(" ");
      // Cris chcPais.add("  ");
      chcSexo.add("  ");
      chcMBaja.add("   ");
      // Cris chcCA.add("   ");
      // Cris chcProvincia.add(" ");

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Para que compile con la interfaz de suca.
  public void setZonificacionSanitaria(String a, String b) {}

  /** ete m�todo sirve para mantener una sincronizaci�n en los eventos */
  public synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modo;
      modo = modoESPERA;
      //Inicializar();
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  /** este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    if (modo == modoESPERA) {
      modo = modoAnterior;
    }
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  public synchronized void setPermiso(boolean bool) {
    bPermiso = bool;
  }

  public synchronized boolean getPermiso() {
    return bPermiso;
  }

  // inicializa el aspecto del panel
  public void jbInit() throws Exception {

    setBorde(false);

    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        Common.imgLUPA,
        Common.imgACEPTAR,
        Common.imgCANCELAR};

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    xYLayout1.setHeight(550);
    xYLayout1.setWidth(628);

    this.setLayout(xYLayout1);

    // ponemos la imagen de la lupa
    btnCodigo.setImage(imgs.getImage(0));

    chkEnfermos.setLabel("Enfermos");
    chkContactos.setLabel("Contactos");
    chkEnfermos.setState(true);
    chkContactos.setState(true);

    if (!sCodAplicacion.equals(APP_TUBERCULOSIS)) {
      chkEnfermos.setVisible(false);
      chkContactos.setVisible(false);
      chkContactos.setState(false);
    }

    btnNivel1.setImage(imgs.getImage(0));
    txtNivel2L.setEditable(false);
    btnNivel2.setImage(imgs.getImage(0));
    txtZBSL.setEditable(false);
    btnZBS.setImage(imgs.getImage(0));
    btnODirecciones.setImage(imgs.getImage(0));
    btnActualizar.setImage(imgs.getImage(1));
    //btnCancelar.setImage(imgs.getImage(2));

    //lblCodigo.setText(res.getString("lblCodigo.Text"));
    lblCodigo.setText("Codigo:");
    //lblApellido1.setText(res.getString("lblApellido1B.Text"));
    lblApellido1.setText("Apellidos  y nombre:");
    //lblFecNacimiento.setText(res.getString("lblFecNacimiento.Text"));
    lblFecNacimiento.setText("F. Nacimiento:");
    //lblEdad.setText(res.getString("lblEdad.Text");
    lblEdad.setText("Edad");
    //lblIdentificacion.setText(res.getString("lblIdentificacion.Text"));
    lblIdentificacion.setText("Identificaci�n");
    //lblSexo.setText(res.getString("lblSexo.Text"));
    lblSexo.setText("Sexo");
    //chkbRevisao.setLabel(res.getString("chkbRevisao.Label"));
    chkbRevisao.setLabel("Enfermo revisado");
    //lblMBaja.setText(res.getString("lblMBaja.Text"));
    lblMBaja.setText("Motivo de baja:");
    //lblFecha.setText(res.getString("lblFecha.Text"));
    lblFecha.setText("Fecha");
    //btnODirecciones.setLabel(res.getString("btnODirecciones.Label"));
    btnODirecciones.setLabel("Otras direcciones");
    //btnActualizar.setLabel(res.getString("btnActualizar.Label"));
    btnActualizar.setLabel("Aceptar");
    //btnCancelar.setLabel("Cancelar");
    //lblObservacion.setText(res.getString("lblObservacion.Text"));
    lblObservacion.setText("Observaciones");
    txtObservacion.setText("");
    //lblTelefono.setText(res.getString("lblTelefono.Text"));
    lblTelefono.setText("Tlfno");
    chcFcalc.setText("");
    lblNivel1.setText(app.getNivel1());
    lblNivel2.setText(app.getNivel2());
    //lblZBS.setText(res.getString("lblZBS.Text"));
    lblZBS.setText("ZBS");

    /// fijamos nombre a los botones
    btnCodigo.setActionCommand("codigo");
    btnNivel1.setActionCommand("nivel1");
    btnNivel2.setActionCommand("nivel2");
    btnZBS.setActionCommand("zbs");
    btnODirecciones.setActionCommand("otrasDirecciones");
    btnActualizar.setActionCommand("actualizar");
    //btnCancelar.setActionCommand("cerrar");

    String si = getApp().getIT_FG_ENFERMO();

    if (si != null) {
      if (si.equals("N")) {
        setPermiso(false);
      }
      else {
        setPermiso(true);
      }
    }

    // a�adimos el nombre de los choices
    chcMBaja.setName("mBaja");
    chcSexo.setName("sexo");
    chcEdad.setName("edad");
    chcIdentificacion.setName("identifica");

    // a�adimos el nombre de la cjaa de texto
    txtCodigo.setName("codigo");
    txtEdad.setName("edad");
    txtFecNacimiento.setName("fecNacimiento");
    txtFecha.setName("fecBaja");
    txtNivel1.setName("nivel1");
    txtNivel2.setName("nivel2");
    txtZBS.setName("zbs");

    // ponemosen amarillo las claves primarias
    txtCodigo.setBackground(Common.amarillo);
    txtApellido1.setBackground(Common.amarillo);
    txtNivel1L.setEditable(false);
    txtNivel2L.setEditable(false);
    txtZBSL.setEditable(false);

    //pnlIdentificacion.setLabel(res.getString("pnlIdentificacion.Label"));
    pnlIdentificacion.setLabel("Datos de identificacion");

    //pnlLocalizacion.setLabel(res.getString("pnlLocalizacion.Label"));
    pnlLocalizacion.setLabel("Datos de localizaci�n");

    /// a�adimoslosc componentes
    this.add(lblCodigo, new XYConstraints(15, 6, -1, -1));
    this.add(txtCodigo, new XYConstraints(78, 6, 86, -1));
    this.add(btnCodigo, new XYConstraints(173, 6, -1, -1));

    this.add(chkEnfermos, new XYConstraints(220, 7, -1, -1));
    this.add(chkContactos, new XYConstraints(310, 7, -1, -1));

    this.add(lblApellido1, new XYConstraints(17, 49, 117, -1));
    this.add(txtApellido1, new XYConstraints(144, 49, 145, -1));
    this.add(txtApellido2, new XYConstraints(300, 49, 150, -1));
    this.add(txtNombre, new XYConstraints(460, 49, 145, -1));
    this.add(lblIdentificacion, new XYConstraints(17, 82, -1, -1));
    this.add(chcIdentificacion, new XYConstraints(144, 82, 96, 23));
    this.add(txtIdentificacion, new XYConstraints(245, 82, 90, -1));
    this.add(lblSexo, new XYConstraints(345, 83, 36, -1));
    this.add(chcSexo, new XYConstraints(382, 82, 85, -1));
    this.add(lblTelefono, new XYConstraints(480, 82, 35, -1));
    this.add(txtTelefono, new XYConstraints(516, 82, 91, -1));
    this.add(lblFecNacimiento, new XYConstraints(17, 114, 87, -1));
    this.add(txtFecNacimiento, new XYConstraints(144, 114, 106, -1));
    this.add(chcFcalc, new XYConstraints(262, 114, 76, 21));
    this.add(lblEdad, new XYConstraints(370, 114, 37, -1));
    this.add(txtEdad, new XYConstraints(420, 114, 41, -1));
    this.add(chcEdad, new XYConstraints(475, 114, 75, -1));

    //suca
    this.add(panSuca, new XYConstraints(19, 173, 596, 116));

    this.add(lblNivel1, new XYConstraints(17, 298, 92, 19));
    this.add(txtNivel1, new XYConstraints(116, 298, 66, 22));
    this.add(btnNivel1, new XYConstraints(193, 298, -1, -1));
    this.add(txtNivel1L, new XYConstraints(230, 298, 216, -1));
    this.add(lblNivel2, new XYConstraints(17, 325, 92, 21));
    this.add(txtNivel2, new XYConstraints(116, 325, 66, 21));
    this.add(btnNivel2, new XYConstraints(192, 325, -1, -1));
    this.add(txtNivel2L, new XYConstraints(230, 325, 216, -1));
    this.add(lblZBS, new XYConstraints(17, 355, 92, 21));
    this.add(txtZBS, new XYConstraints(116, 355, 66, -1));
    this.add(btnZBS, new XYConstraints(192, 355, -1, -1));
    this.add(txtZBSL, new XYConstraints(230, 355, 217, -1));
    this.add(lblMBaja, new XYConstraints(17, 400, 88, -1));
    this.add(chcMBaja, new XYConstraints(116, 400, 146, -1));
    this.add(chkbRevisao, new XYConstraints(305, 400, -1, -1));
    this.add(lblFecha, new XYConstraints(459, 399, -1, -1));
    this.add(txtFecha, new XYConstraints(520, 400, 88, -1));
    this.add(lblObservacion, new XYConstraints(17, 430, -1, -1));
    this.add(txtObservacion, new XYConstraints(116, 430, 492, -1));

    this.add(btnActualizar, new XYConstraints(455, 465, -1, -1));
    //this.add(btnCancelar, new XYConstraints(525, 465, -1, -1));
    this.add(btnODirecciones, new XYConstraints(320, 465, -1, -1));
    this.add(pnlIdentificacion, new XYConstraints(2, 35, 625, 115));
    this.add(pnlLocalizacion, new XYConstraints(3, 155, 625, 239));
    // gesti�n de eventos de botones
    btnCodigo.addActionListener(btnActionListener);

    btnNivel1.addActionListener(btnActionListener);
    btnNivel2.addActionListener(btnActionListener);
    btnZBS.addActionListener(btnActionListener);
    btnODirecciones.addActionListener(btnActionListener);
    btnActualizar.addActionListener(btnActionListener);
    //btnCancelar.addActionListener(btnActionListener);

    // gestion de evenetos de los Choices
    chcMBaja.addItemListener(chItemListener);
    chcSexo.addItemListener(chItemListener);
    chcEdad.addItemListener(chItemListener);
    chcIdentificacion.addItemListener(chItemListener);

    // gesti�n de eventos de las cajas de texto
    //txtCodigo.addActionListener(txtFocusListener);
    //txtEdad.addActionListener(txtFocusListener);
    //txtFecNacimiento.addActionListener(txtFocusListener);
    //txtMunicipio.addActionListener(txtFocusListener);
    //txtNivel1.addActionListener(txtFocusListener);
    //txtNivel2.addActionListener(txtFocusListener);
    //txtZBS.addActionListener(txtFocusListener);

    txtCodigo.addKeyListener(textAdapter);
    txtEdad.addKeyListener(textAdapter);
    txtFecNacimiento.addKeyListener(textAdapter);
    txtFecha.addKeyListener(textAdapter);
    txtNivel1.addKeyListener(textAdapter);
    txtNivel2.addKeyListener(textAdapter);
    txtZBS.addKeyListener(textAdapter);

    txtCodigo.addFocusListener(txtFocusListener);
    txtEdad.addFocusListener(txtFocusListener);
    txtFecNacimiento.addFocusListener(txtFocusListener);
    txtFecha.addFocusListener(txtFocusListener);
    txtNivel1.addFocusListener(txtFocusListener);
    txtNivel2.addFocusListener(txtFocusListener);
    txtZBS.addFocusListener(txtFocusListener);

    // se inicializan los botones a false o true
    this.modo = modoINICIO;
    Inicializar();

  }

  public void Inicializar() {
    switch (modo) {
      case modoINICIO:

        panSuca.setModoDeshabilitado();

        txtCodigo.setEnabled(true);
        btnCodigo.setEnabled(true);

        chkEnfermos.setEnabled(true);
        chkContactos.setEnabled(true);

        txtApellido1.setEnabled(false);
        txtApellido2.setEnabled(false);
        txtNombre.setEnabled(false);
        txtFecNacimiento.setEnabled(false);
        txtEdad.setEnabled(false);
        chcEdad.setEnabled(false);
        chcIdentificacion.setEnabled(false);
        txtIdentificacion.setEnabled(false);
        chcSexo.setEnabled(false);
        txtNivel1L.setEnabled(false);
        btnNivel1.setEnabled(false);
        txtNivel2L.setEnabled(false);
        btnNivel2.setEnabled(false);
        txtZBSL.setEnabled(false);
        btnZBS.setEnabled(false);
        chkbRevisao.setEnabled(false);
        chcMBaja.setEnabled(false);
        txtFecha.setEnabled(false);
        btnODirecciones.setEnabled(false);
        btnActualizar.setEnabled(false);
        //btnCancelar.setEnabled(false);
        txtObservacion.setEnabled(false);
        txtTelefono.setEnabled(false);
        txtNivel1.setEnabled(false);
        txtNivel2.setEnabled(false);
        txtZBS.setEnabled(false);
        break;
      case modoPAIS:
      case modoCA:
      case modoPROVINCIA:
      case modoNOPAIS:
      case modoNOCA:
      case modoNOPROVINCIA:
      case modoFEC:
      case modoNOFEC:
      case modoMODIFICAR:

        panSuca.setModoNormal();

        if (panSuca.getCD_PAIS().equalsIgnoreCase("ESP") ||
            panSuca.getCD_PAIS().equals("1")) {
          txtNivel1.setEnabled(true);
          btnNivel1.setEnabled(true);
        }
        if (txtNivel1.getText().trim().length() > 0) {
          txtNivel2.setEnabled(true);
          btnNivel2.setEnabled(true);
        }
        if (txtNivel2.getText().trim().length() > 0) {
          txtZBS.setEnabled(true);
          btnZBS.setEnabled(true);
        }

        txtEdad.setEnabled(true);
        chcEdad.setEnabled(true);
        txtCodigo.setEnabled(true);
        btnCodigo.setEnabled(true);
        chkEnfermos.setEnabled(true);
        chkContactos.setEnabled(true);

        if (bPermiso) {
          txtApellido2.setEnabled(true);
          txtNombre.setEnabled(true);
          txtApellido1.setEnabled(true);
          txtNombre.setVisible(true);
          txtApellido2.setVisible(true);
        }
        else {
          txtApellido2.setEnabled(false);
          txtNombre.setEnabled(false);
          txtApellido1.setEnabled(false);
          txtNombre.setVisible(false);
          txtApellido2.setVisible(false);
        }
        txtFecNacimiento.setEnabled(true);
        txtFecha.setEnabled(true);
        chcEdad.setEnabled(true);
        chcIdentificacion.setEnabled(true);
        txtIdentificacion.setEnabled(true);
        chcSexo.setEnabled(true);
        chkbRevisao.setEnabled(true);
        chcMBaja.setEnabled(true);
        btnODirecciones.setEnabled(true);
        btnActualizar.setEnabled(true);
        //btnCancelar.setEnabled(true);
        txtObservacion.setEnabled(true);
        txtTelefono.setEnabled(true);
        break;
      case modoESPERA:

        //poner panel suca a espera
        panSuca.setModoEspera();
        panSuca.doLayout();

        txtCodigo.setEnabled(false);
        btnCodigo.setEnabled(false);
        chkEnfermos.setEnabled(false);
        chkContactos.setEnabled(false);

        txtApellido1.setEnabled(false);
        txtApellido2.setEnabled(false);
        txtNombre.setEnabled(false);
        txtFecNacimiento.setEnabled(false);
        txtEdad.setEnabled(false);
        chcEdad.setEnabled(false);
        chcIdentificacion.setEnabled(false);
        txtIdentificacion.setEnabled(false);
        chcSexo.setEnabled(false);
        txtNivel1L.setEnabled(false);
        btnNivel1.setEnabled(false);
        txtNivel2L.setEnabled(false);
        btnNivel2.setEnabled(false);
        txtZBSL.setEnabled(false);
        btnZBS.setEnabled(false);
        chkbRevisao.setEnabled(false);
        chcMBaja.setEnabled(false);
        txtFecha.setEnabled(false);
        btnODirecciones.setEnabled(false);
        btnActualizar.setEnabled(false);
        //btnCancelar.setEnabled(false);
        txtObservacion.setEnabled(false);
        txtTelefono.setEnabled(false);
        txtNivel1.setEnabled(false);
        txtNivel2.setEnabled(false);
        txtZBS.setEnabled(false);
        break;
    }
  }

  // Implementacion del metodo cambiaModoTramero
  // del interfaz zonificacionSanitaria
  public void cambiaModoTramero(boolean b) {
    btramero = b;
  }

  // Implementacion del metodo ponerEnEspera
  // del interfaz zonificacionSanitaria
  public int ponerEnEspera() {
    int anterior = modo;
    modo = modoESPERA;
    Inicializar();
    return anterior;
  }

  // Implementacion del metodo ponerModo
  // del interfaz zonificacionSanitaria
  public void ponerModo(int mod) {
    modo = mod;
    Inicializar();
  }

  public void vialInformado() {}

  // Implementacion del metodo setZonificacionSanitaria
  // del interfaz zonificacionSanitaria
  public void setZonificacionSanitaria(String mun) {
    CMessage msgBox;
    DataMunicipioEDO data;

    if (mun.trim().equals("")) {
      txtNivel1.setText("");
      txtNivel1L.setText("");
      txtNivel2.setText("");
      txtNivel2L.setText("");
      txtZBS.setText("");
      txtZBSL.setText("");
    }
    else {

      try {

        CLista lista = new CLista();
        data = new DataMunicipioEDO(mun, "");
        lista.addElement(data);
        lista.setIdioma(app.getIdioma());
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_MUNICIPIO_CONT));
        lista = (CLista) stubCliente.doPost(0, lista);

        if (lista.size() > 0) {
          DataMunicipioEDO dataMun;
          dataMun = (DataMunicipioEDO) lista.elementAt(0);

          //NIVELES *********
          txtNivel1.setText(dataMun.getCodNivel1());
          txtNivel1L.setText(dataMun.getDesNivel1());
          txtNivel2.setText(dataMun.getCodNivel2());
          txtNivel2L.setText(dataMun.getDesNivel2());
          txtZBS.setText(dataMun.getCodZBS());
          txtZBSL.setText(dataMun.getDesZBS());
        }
        else {
          txtNivel1.setText("");
          txtNivel1L.setText("");
          txtNivel2.setText("");
          txtNivel2L.setText("");
          txtZBS.setText("");
          txtZBSL.setText("");
        }

      }
      catch (Exception e) { //acceso a base de datos mal
        e.printStackTrace();
        msgBox = new CMessage(app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      } //fin del try
    }
  }

  // funciones que devuelven los objetos globales

  public CLista getListataId() {
    synchronized (sincro) {
      return listaId;
    }
  }

  public void setListaId(CLista a_data) {
    synchronized (sincro) {
      listaId = a_data;
    }
  }

  public CLista getListaPaises() {
    synchronized (sincro) {
      return panSuca.getListaPaises();
    }
  }

  public CLista getListaCA() {
    synchronized (sincro) {
      return panSuca.getListaCA();
    }
  }

  public CLista getListaSexo() {
    synchronized (sincro) {
      return listaSexo;
    }
  }

  public void setListaSexo(CLista a_lista) {
    synchronized (sincro) {
      listaSexo = a_lista;
    }
  }

  public CLista getListaMBaja() {
    synchronized (sincro) {
      return listaMBaja;
    }
  }

  public void setListaMBaja(CLista a_lista) {
    synchronized (sincro) {
      listaMBaja = a_lista;
    }
  }

  public CLista getListaEnfermo() {
    synchronized (sincro) {
      return listaEnfermo;
    }
  }

  public void setListaEnfermo(CLista a_lista) {
    synchronized (sincro) {
      listaEnfermo = a_lista;
    }
  }

  /**
   *  esta funci�n es la misma que en cApp.Ctitulo
   *  es la que realiza la funci�n del aspa del titulo del applet
   */
  void btnCerrar_actionPerformed(ActionEvent e) {
    // cerramos todas las listas para que se libere la memoria
    /*listaId = null;
         listaPaises = null;
         listaCA = null;
         listaProvincias =null;
         listaSexo = null ;
         listaMBaja = null;
         listaEnfermo= null;
         listaOtrasDirecciones= null;*/
    ( (ApPruEnfermo) app).adios();
    //try {
    //  app.getAppletContext().showDocument( new URL(app.getCodeBase(), "default.html"),"_self");
    //} catch (Exception ex) {}
  }

  protected void isDataValidOtrasDir(CLista a_lista) {
    CMessage msgBox;
    Hashtable hash = null;
    int indice;
    String campo = null;
    String dato = null;
    boolean campos_obligatorios = true;
    Hashtable parameter = null;
    Hashtable datbien = null;

    if (listaOtrasDirecciones != null && listaOtrasDirecciones.size() > 0) {
      datbien = (Hashtable) (listaOtrasDirecciones.firstElement());
    }
    else {
      datbien = (Hashtable) (listaEnfermo.firstElement());
    }

    if (a_lista != null && a_lista.size() > 0) {
      parameter = (Hashtable) (a_lista.firstElement());
    }
    else {
      return;
    }

    dato = (String) datbien.get("DS_DIREC2");
    if (dato != null && dato.length() > 0 && dato.length() <= 40) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_DIREC2", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_DIREC2", "");
    }
    dato = (String) datbien.get("DS_NUM2");
    if (dato != null && dato.length() > 0 && dato.length() <= 5) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_NUM2", dato);
    }
    else { // el campo no cumplelos reqisitos
      parameter.put("DS_NUM2", "");
    }
    dato = (String) datbien.get("DS_PISO2");
    if (dato != null && dato.length() > 0 && dato.length() <= 5) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_PISO2", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_PISO2", "");
    }
    dato = (String) datbien.get("CD_POST2");
    if (dato != null && dato.length() > 0 && dato.length() <= 5) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_POST2", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_POST2", "");
    }
    dato = (String) datbien.get("CD_MUNI2");
    if (dato != null && dato.length() > 0 && dato.length() <= 3) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_MUNI2", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_MUNI2", "");
    }
    dato = (String) datbien.get("CD_POST3");
    if (dato != null && dato.length() > 0 && dato.length() <= 5) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_POST3", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_POST3", "");
    }
    dato = (String) datbien.get("DS_PISO3");
    if (dato != null && dato.length() > 0 && dato.length() <= 5) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_PISO3", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_PISO3", "");
    }
    dato = (String) datbien.get("DS_NUM3");
    if (dato != null && dato.length() > 0 && dato.length() <= 5) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_NUM3", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_NUM3", "");
    }
    dato = (String) datbien.get("CD_MUNI3");
    if (dato != null && dato.length() > 0 && dato.length() <= 3) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_MUNI3", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_MUNI3", "");
    }
    dato = (String) datbien.get("DS_DIREC3");
    if (dato != null && dato.length() > 0 && dato.length() <= 40) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_DIREC3", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_DIREC3", "");
    }
    dato = (String) datbien.get("DS_TELEF3");
    if (dato != null && dato.length() > 0 && dato.length() <= 14) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_TELEF3", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_TELEF3", "");
    }

    dato = (String) datbien.get("DS_TELEF2");
    if (dato != null && dato.length() > 0 && dato.length() <= 14) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_TELEF2", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_TELEF2", "");
    }
    dato = (String) datbien.get("DS_OBSERV2");
    if (dato != null && dato.length() > 0 && dato.length() <= 40) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_OBSERV2", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_OBSERV2", "");
    }
    dato = (String) datbien.get("DS_OBSERV3");
    if (dato != null && dato.length() > 0 && dato.length() <= 40) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_OBSERV3", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_OBSERV3", "");
    }

    dato = (String) datbien.get("CD_PROV2");
    if (dato != null && dato.length() > 0 && dato.length() <= 40) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_PROV2", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_PROV2", "");
    }
    dato = (String) datbien.get("CD_PROV3");
    if (dato != null && dato.length() > 0 && dato.length() <= 40) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_PROV3", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_PROV3", "");
    }

  }

  /**
   *  comprueba que los datos de la pantalla sean v�lidos
   *  comprueba que esten informados los campos obligatorios
   *  y que los campos sean de la longitud adecuada
   *  rellena la lista con esos datos
   *  tambi�n compruena si ha cambiado alguno
   */
  protected boolean isDataValidViejo(CLista a_lista) {
    CMessage msgBox;
    boolean fallaCamposObligatorios = false;
    boolean fechaErronea = false;
    Hashtable hash = null;
    int indice;
    String campo = null;
    String dato = null;
    Hashtable parameter = (Hashtable) (a_lista.firstElement());
    DataEnfermo datoInicial = (DataEnfermo) (listaEnfermo.firstElement());
    boolean cambio = false; // true

    // si ha cambiado otras direcciones
    //cambiamos los datos de otras direcciones
    if (listaOtrasDirecciones != null && listaOtrasDirecciones.size() > 0) {
      cambio = true;
    }
    // rellenamos los datos de otras direcciones
    isDataValidOtrasDir(a_lista);

    // metemos los datos de la fecha de actualizacion y CD_OPE
    dato = app.getLogin();
    if (dato != null && dato.length() > 0 && dato.length() <= 8) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_OPE", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      fallaCamposObligatorios = true;
      campo = "CD_OPE";
    }

    //no pasa nada porque luego en el servlet le pongo la
    //fecha buena
    ( (DataEnfermo) parameter).setFechaActual(Fechas.date2String(new Date()));

    // comprueba que esten informados los campos obligatorios
    dato = txtCodigo.getText();
    if (dato != null && dato.length() > 0 && dato.length() <= 6) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_ENFERMO", dato);
    }
    else { // el campo no cumplelos requisitos
      fallaCamposObligatorios = true;
      campo = "Codigo";
    }

    if (bPermiso) {
      // si no tiene permiso de ver el nombre no escribo
      StringBuffer inicial = new StringBuffer();
      dato = txtApellido1.getText();
      if (dato != null && dato.length() > 0 && dato.length() <= 20) {
        cambio = cambio ||
            Common.ha_cambiado(dato, (String) datoInicial.get("DS_APE1"));
        if (dato.length() > 1) {
          inicial.append(dato.substring(0, 1));
          // el campo es valido y lo a�adimos como parametro
        }
        parameter.put("DS_APE1", dato);
        parameter.put("DS_FONOAPE1", Common.traduccionFonetica(dato));
      }
      else { // el campo no cumplelos requisitos
        fallaCamposObligatorios = true;
        campo = "Apellido1";
      }
      dato = txtApellido2.getText();
      if (dato != null && dato.length() > 0 && dato.length() <= 30) {
        cambio = cambio ||
            Common.ha_cambiado(dato, (String) datoInicial.get("DS_APE2"));
        if (dato.length() > 1) {
          inicial.append(dato.substring(0, 1));
          // el campo es valido y lo a�adimos como parametro
        }
        parameter.put("DS_APE2", dato);
        parameter.put("DS_FONOAPE2", Common.traduccionFonetica(dato));
      }
      else { // el campo no cumplelos requisitos
        parameter.put("DS_APE2", "");
        parameter.put("DS_FONOAPE2", "");
      }
      dato = txtNombre.getText();
      if (dato != null && dato.length() > 0 && dato.length() <= 30) {
        // el campo es valido y lo a�adimos como parametro
        cambio = cambio ||
            Common.ha_cambiado(dato, (String) datoInicial.get("DS_NOMBRE"));
        if (dato.length() > 1) {
          inicial.append(dato.substring(0, 1));
        }
        parameter.put("DS_NOMBRE", dato);
        parameter.put("DS_FONONOMBRE", Common.traduccionFonetica(dato));
      }
      else { // el campo no cumplelos requisitos
        parameter.put("DS_NOMBRE", "");
        parameter.put("DS_FONONOMBRE", "");
      }
      if (inicial.length() > 0) {
        parameter.put("SIGLAS", inicial.toString());
      }
    }
    else {
      parameter.put("DS_APE1", datoInicial.get("DS_APE1"));
      parameter.put("DS_FONOAPE1", datoInicial.get("DS_FONOAPE1"));
    }

    dato = txtFecha.getText();
    if (dato != null && dato.trim().length() > 0 && dato.trim().length() <= 10) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.getFechaBaja());
      // el campo es valido y lo a�adimos como parametro
      ( (DataEnfermo) parameter).setFechaBaja(dato);
    }
    else { // el campo no cumplelos requisitos
      ( (DataEnfermo) parameter).setFechaBaja(null);
    }

    CFechaSimple copi_txtFecNacimiento = new CFechaSimple("N");
    // comprobamos la fecha de nacimiento
    copi_txtFecNacimiento.setText(txtFecNacimiento.getText());
    copi_txtFecNacimiento.ValidarFecha();
    dato = copi_txtFecNacimiento.getFecha();
    txtFecNacimiento.setText(dato);
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.getFechaNacimiento());
    if (txtFecNacimiento.getText().trim().length() == 0) {
      // el usuario no ha escrito ninguna fecha
      ( (DataEnfermo) parameter).setFechaNacimiento(null);
    }
    else {
      ( (DataEnfermo) parameter).setFechaNacimiento(txtFecNacimiento.getText());
    }

    dato = panSuca.getDS_DIREC();
    if (dato != null && dato.length() > 0 && dato.length() <= 50) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DS_DIREC"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_DIREC", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_DIREC", "");
    }
    dato = panSuca.getDS_NUM();
    if (dato != null && dato.length() > 0 && dato.length() <= 5) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DS_NUM"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_NUM", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_NUM", "");
    }
//    // System_out.println("N�=" + dato);
    dato = panSuca.getDS_PISO();
    if (dato != null && dato.length() > 0 && dato.length() <= 4) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DS_PISO"));
      parameter.put("DS_PISO", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_PISO", "");
    }
    dato = panSuca.getCD_POSTAL();
    if (dato != null && dato.length() > 0 && dato.length() <= 6) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_POSTAL"));
      parameter.put("CD_POSTAL", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_POSTAL", "");
    }
    dato = panSuca.getCD_MUN();
    if (dato != null && dato.length() > 0 && dato.length() <= 3) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_MUN"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_MUN", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_MUN", "");
    }

    dato = txtNivel1.getText();
    if (dato != null && dato.length() > 0 && dato.length() <= 2) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_NIVEL_1"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_NIVEL_1", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_NIVEL_1", "");
    }
    dato = txtNivel2.getText();
    if (dato != null && dato.length() > 0 && dato.length() <= 2) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_NIVEL_2"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_NIVEL_2", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_NIVEL_2", "");
    }
    dato = txtZBS.getText();
    if (dato != null && dato.length() > 0 && dato.length() <= 2) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_ZBS"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_ZBS", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_ZBS", "");
    }

    dato = txtObservacion.getText();
    if (dato != null && dato.length() > 0 && dato.length() <= 50) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DS_OBSERV"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_OBSERV", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_OBSERV", "");
    }
    dato = txtTelefono.getText();
    if (dato != null && dato.length() > 0 && dato.length() <= 14) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DS_TELEF"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_TELEF", dato);
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_TELEF", "");
    }

    dato = chcFcalc.getText(); // si tiene algun valor es calculada
    if (dato != null && dato.length() > 0 && dato.charAt(0) == 'c') {
      dato = "S";
      // el campo es valido y lo a�adimos como parametro
      parameter.put("IT_CALC", "S");
    }
    else {
      dato = "N";
      parameter.put("IT_CALC", "N");
    }
    if (chkbRevisao.getState()) {
      // el campo es valido y lo a�adimos como parametro
      dato = "S";
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("IT_REVISADO"));
      parameter.put("IT_REVISADO", "S");
    }
    else {
      dato = "N";
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("IT_REVISADO"));
      parameter.put("IT_REVISADO", "N");
    }

    indice = chcMBaja.getSelectedIndex();
    indice--;
    if (indice >= 0 && indice < listaMBaja.size()) {
      hash = (Hashtable) listaMBaja.elementAt(indice);
      dato = (String) hash.get("CD_MOTBAJA");
      if (dato != null && dato.length() > 0) {
        // el campo es valido y lo a�adimos como parametro
        cambio = cambio ||
            Common.ha_cambiado(dato, (String) datoInicial.get("CD_MOTBAJA"));
        parameter.put("CD_MOTBAJA", dato.toUpperCase());
      }
      else { // el campo no cumplelos requisitos
        parameter.put("CD_MOTBAJA", "");
      }
    }
    else {
      parameter.put("CD_MOTBAJA", "");
      String antMotBaja = (String) datoInicial.get("CD_MOTBAJA");
      if (antMotBaja != null && antMotBaja.trim().length() > 0) {
        cambio = true;
      }
    }

    indice = chcSexo.getSelectedIndex();
    indice--;
    if (indice >= 0 && indice < listaSexo.size()) {
      String antSexo = (String) datoInicial.get("CD_SEXO");
      hash = (Hashtable) listaSexo.elementAt(indice);
      dato = (String) hash.get("CD_SEXO");
      if (dato != null && dato.length() > 0) {
        cambio = cambio ||
            Common.ha_cambiado(dato, (String) datoInicial.get("CD_SEXO"));
        parameter.put("CD_SEXO", dato.toUpperCase());
      }
      else { // el campo no cumplelos requisitos
        parameter.put("CD_SEXO", "");
      }
    }
    else {
      parameter.put("CD_SEXO", "");
      String antSexo = (String) datoInicial.get("CD_SEXO");
      if (antSexo != null && antSexo.trim().length() > 0) {
        cambio = true;
      }
    }

    dato = panSuca.getCD_PAIS();
    if (dato != null && dato.length() > 0 && dato.length() <= 3) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_PAIS"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_PAIS", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_PAIS", "");
    }

    dato = panSuca.getCD_PROV();
    if (dato != null && dato.length() > 0 && dato.length() <= 2) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_PROV"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_PROV", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_PROV", "");
    }

    //para cdvial, cdtvia, dscalnum, cdtnum que se han a�adido
    //a enfermo
    dato = panSuca.getCDVIAL();
    if (dato != null && dato.length() > 0 && dato.length() <= 5) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CDVIAL"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CDVIAL", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CDVIAL", "");
    }

    dato = panSuca.getCDTVIA();
    if (dato != null && dato.length() > 0 && dato.length() <= 5) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CDTVIA"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CDTVIA", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CDTVIA", "");
    }

    dato = panSuca.getCDTNUM();
    if (dato != null && dato.length() > 0 && dato.length() <= 1) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CDTNUM"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CDTNUM", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CDTNUM", "");
    }

    dato = panSuca.getDSCALNUM();
    if (dato != null && dato.length() > 0 && dato.length() <= 6) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DSCALNUM"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DSCALNUM", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DSCALNUM", "");
    }

    dato = txtIdentificacion.getText();
    indice = chcIdentificacion.getSelectedIndex();
    indice--;
    if (dato != null && dato.length() > 0 && dato.length() <= 20 && indice >= 0) {
      // el campo es valido y lo a�adimos como parametro
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DS_NDOC"));
      parameter.put("DS_NDOC", dato);
      // metemos el TIPO DOC
      if (indice >= 0 && indice < listaId.size()) {
        hash = (Hashtable) listaId.elementAt(indice);
        dato = (String) hash.get("CD_TDOC");
        if (dato != null && dato.length() > 0) {
          cambio = cambio ||
              Common.ha_cambiado(dato, (String) datoInicial.get("CD_TDOC"));
          parameter.put("CD_TDOC", dato);
        }
        else { // el campo no cumplelos requisitos
          fallaCamposObligatorios = true;
          //campo = res.getString("msg10.Text");
          campo = "Tipo Documento";
        }
      }
    }
    else if (indice < 0 && dato.trim().length() > 0) {
      fallaCamposObligatorios = true;
      //campo = res.getString("msg10.Text");
      campo = "Tipo Documento";
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DS_NDOC", "");
      parameter.put("CD_TDOC", "");
      String antSexo = (String) datoInicial.get("CD_TDOC");
      if (antSexo != null && antSexo.trim().length() > 0) {
        cambio = true;
      }
    }

//_________ Cepillator

    // comprobamos los de otras direcciones

    if (fallaCamposObligatorios) {
      //msgBox = new CMessage(app,CMessage.msgAVISO, res.getString("msg40.Text") + campo);
      msgBox = new CMessage(app, CMessage.msgAVISO,
                            "Faltan campo obligatorio:" + campo);
      msgBox.show();
      msgBox = null;
    }

    if (fechaErronea) {
      //msgBox = new CMessage(app,CMessage.msgAVISO, res.getString("msg41.Text")+ campo);
      msgBox = new CMessage(app, CMessage.msgAVISO,
                            "Fecha mal escrita:" + campo);
      msgBox.show();
      msgBox = null;
    }

    //  true si ha ido bien
    if (cambio == false) {
      // si los datos no han cambiado
      return false;
    }
    else {
      return!fallaCamposObligatorios && !fechaErronea;
    }
  }

  ///_____________________________________________________________________
  /**
   *  comprueba que los datos de la pantalla sean v�lidos
   *  comprueba que esten informados los campos obligatorios
   *  y que los campos sean de la longitud adecuada
   *  rellena la lista con esos datos
   *  tambi�n compruena si ha cambiado alguno
   */
  protected boolean isDataValid(CLista a_lista) {
    CMessage msgBox;
    boolean fallaCamposObligatorios = false;
    boolean fechaErronea = false;
    boolean fallaTamano = false; //Long excesiva campos  //LRG
    Hashtable hash = null;
    int indice;
    String campo = null;
    String dato = null;
    Hashtable parameter = (Hashtable) (a_lista.firstElement());
    DataEnfermo datoInicial = (DataEnfermo) (listaEnfermo.firstElement());
    boolean cambio = false;
    String desDato = null;
    String antDato = null;
    // si ha cambiado otras direcciones
    //cambiamos los datos de otras direcciones
//    if (listaOtrasDirecciones != null && listaOtrasDirecciones.size() > 0){
    if (bCambioOtrasDirecciones) {
      cambio = true;
    }

    // rellenamos los datos de otras direcciones
    isDataValidOtrasDir(a_lista);

    /*
       //________ TRAZA_______________________________________________
       // System_out.println("___________________________________________");
       // System_out.println("A_LISTA Tras  isDataValidOtrasDir() :");
       if (a_lista!=null) {
        for (int cont=0; cont< a_lista.size(); cont++) {
         // System_out.println("    Elemento " + cont);
          Object elemLista= a_lista.elementAt(cont);
          if  ( (elemLista.getClass())== (new DataEnfermo(" ").getClass()) )  {
            Hashtable laHash= (Hashtable) elemLista;
            Enumeration enum= laHash.keys();
            while (enum.hasMoreElements()) {
              String clave= (String)(enum.nextElement());
              Object valor=null;
              valor=laHash.get(clave);
              if (valor.getClass() == (new String()).getClass()){
                 // System_out.println("        Clave  "+clave+"                 " + (String)valor);
              }
              else {
                 // System_out.println("        Clave  "+clave+"   No es un String  ");
              }
           }//While
          } //if
        } //for
       }
     */

    //________ FIN TRAZA_______________________________________________

    //Comprobaci�n de datos obligatorios

    // metemos los datos de la fecha de actualizacion y CD_OPE
    dato = app.getLogin();
    if (dato != null && dato.length() > 0 && dato.length() <= 8) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_OPE", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      fallaCamposObligatorios = true;
      campo = "CD_OPE";
    }

    //no pasa nada porque luego en el servlet le pongo la
    //fecha buena
    ( (DataEnfermo) parameter).setFechaActual(Fechas.date2String(new Date()));

    // comprueba que esten informados los campos obligatorios
    dato = txtCodigo.getText();
    if (dato != null && dato.length() > 0 && dato.length() <= 6) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_ENFERMO", dato);
    }
    else { // el campo no cumplelos requisitos
      //Si falta dato
      if (dato == null || dato.length() == 0) { //LRG
        fallaCamposObligatorios = true;
        campo = "Codigo";
      }
      //Si long excesiva y est�n obligatorios
      else if (!fallaCamposObligatorios) { //LRG
        fallaTamano = true;
        txtCodigo.selectAll();
      }
    }

    dato = txtIdentificacion.getText();
    indice = chcIdentificacion.getSelectedIndex();
    indice--;
    //Si se ha elegido tipo doc y se ha rellenado num doc con longitud adecuada
    if (dato != null && dato.length() > 0 && dato.length() <= 20 && indice >= 0 &&
        indice < listaId.size()) {
      // Los campos son validos y lo a�adimos como parametro
      //Se a�ade numero doc
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DS_NDOC"));
      parameter.put("DS_NDOC", dato);
      //Se a�ade tipo doc
      hash = (Hashtable) listaId.elementAt(indice);
      dato = (String) hash.get("CD_TDOC");
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_TDOC"));
      parameter.put("CD_TDOC", dato);
    }
    //Si se ha elegido n�mero pero no tipo doc
    // o al rev�s hay tipo doc pero no hay n�mero
    else if ( (indice < 0 && dato.trim().length() > 0) ||
             (indice >= 0 && indice < listaId.size() && (dato.length() == 0))) {
      fallaCamposObligatorios = true;
      campo = "Tipo Documento";
    }
    //Si no hay tipo doc ni num doc se ponen campos vac�os y se indica si ha habido cambio
    else if (indice < 0 && dato.length() == 0) {
      parameter.put("DS_NDOC", "");
      parameter.put("CD_TDOC", "");
      //Si antes hab�a tipo doc es que ha habido cambio
      antDato = (String) datoInicial.get("CD_TDOC");
      if (antDato != null && antDato.trim().length() > 0) {
        cambio = true;
      }
    }
    //Si est�n los dos datos pero long n�mero es excesiva
    else if (indice >= 0 && indice < listaId.size() && dato.length() > 20) {
      //Si est�n obligatorios
      if (!fallaCamposObligatorios) { //LRG
        fallaTamano = true;
        txtIdentificacion.selectAll();
      }
    }

    if (bPermiso) {
      // si no tiene permiso de ver el nombre no escribo
      StringBuffer inicial = new StringBuffer();
      dato = txtApellido1.getText();
      if (dato != null && dato.length() > 0 && dato.length() <= 20) {
        cambio = cambio ||
            Common.ha_cambiado(dato, (String) datoInicial.get("DS_APE1"));
        if (dato.length() > 1) {
          inicial.append(dato.substring(0, 1));
          // el campo es valido y lo a�adimos como parametro
        }
        parameter.put("DS_APE1", dato);
        parameter.put("DS_FONOAPE1", Common.traduccionFonetica(dato));
      }
      else { // el campo no cumplelos requisitos

        //Si falta dato , error
        if (dato == null || dato.length() == 0) { //LRG
          fallaCamposObligatorios = true;
          campo = "Apellido1";
        }
        //Si long excesiva y est�n obligatorios
        else if (!fallaCamposObligatorios) { //LRG
          fallaTamano = true;
          txtApellido1.selectAll();
        }
      }

      dato = txtApellido2.getText();
      if (dato != null && dato.length() > 0 && dato.length() <= 20) { //LRG Retocado long
        cambio = cambio ||
            Common.ha_cambiado(dato, (String) datoInicial.get("DS_APE2"));
        if (dato.length() > 1) {
          inicial.append(dato.substring(0, 1));
          // el campo es valido y lo a�adimos como parametro
        }
        parameter.put("DS_APE2", dato);
        parameter.put("DS_FONOAPE2", Common.traduccionFonetica(dato));
      }
      else { // el campo no cumplelos requisitos

        //Si falta dato
        if (dato == null || dato.length() == 0) { //LRG
          //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
          if ( (datoInicial.containsKey("DS_APE2")) &&
              (! ( ( (String) datoInicial.get("DS_APE2")).equals("")))) {
            cambio = true;
          }
          parameter.put("DS_APE2", "");
          parameter.put("DS_FONOAPE2", "");
        }
        //Si long excesiva y est�n obligatorios
        else if (!fallaCamposObligatorios) { //LRG
          fallaTamano = true;
          txtApellido2.selectAll();
        }

      }

      dato = txtNombre.getText();
      if (dato != null && dato.length() > 0 && dato.length() <= 20) {
        // el campo es valido y lo a�adimos como parametro
        cambio = cambio ||
            Common.ha_cambiado(dato, (String) datoInicial.get("DS_NOMBRE"));
        if (dato.length() > 1) {
          inicial.append(dato.substring(0, 1));
        }
        parameter.put("DS_NOMBRE", dato);
        parameter.put("DS_FONONOMBRE", Common.traduccionFonetica(dato));
      }
      else { // el campo no cumplelos requisitos
        //Si falta dato
        if (dato == null || dato.length() == 0) { //LRG
          //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
          if ( (datoInicial.containsKey("DS_NOMBRE")) &&
              (! ( ( (String) datoInicial.get("DS_NOMBRE")).equals("")))) {
            cambio = true;
          }
          parameter.put("DS_NOMBRE", "");
          parameter.put("DS_FONONOMBRE", "");
        }
        //Si long excesiva y est�n obligatorios
        else if (!fallaCamposObligatorios) { //LRG
          fallaTamano = true;
          txtNombre.selectAll();
        }
      }
      if (inicial.length() > 0) {
        parameter.put("SIGLAS", inicial.toString());
      }
    }
    //Si no tiene permiso de ver el nombre se deja el dato inicial
    else {
      parameter.put("DS_APE1", datoInicial.get("DS_APE1"));
      parameter.put("DS_FONOAPE1", datoInicial.get("DS_FONOAPE1"));
    }

    dato = txtFecha.getText();
    if (dato != null && dato.trim().length() > 0 && dato.trim().length() <= 10) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.getFechaBaja());
      // el campo es valido y lo a�adimos como parametro
      ( (DataEnfermo) parameter).setFechaBaja(dato);
    }
    else { // el campo no cumplelos requisitos
      ( (DataEnfermo) parameter).setFechaBaja(null);
    }

    CFechaSimple copi_txtFecNacimiento = new CFechaSimple("N");
    // comprobamos la fecha de nacimiento
    copi_txtFecNacimiento.setText(txtFecNacimiento.getText());
    copi_txtFecNacimiento.ValidarFecha();
    dato = copi_txtFecNacimiento.getFecha();
    txtFecNacimiento.setText(dato);
    cambio = cambio ||
        Common.ha_cambiado(dato, (String) datoInicial.getFechaNacimiento());
    if (txtFecNacimiento.getText().trim().length() == 0) {
      // el usuario no ha escrito ninguna fecha
      ( (DataEnfermo) parameter).setFechaNacimiento(null);
    }
    else {
      ( (DataEnfermo) parameter).setFechaNacimiento(txtFecNacimiento.getText());
    }

    dato = panSuca.getDS_DIREC();
    if (dato != null && dato.length() > 0 && dato.length() <= 50) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DS_DIREC"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_DIREC", dato);
    }
    else { // el campo no cumplelos requisitos
      //Si falta dato
      if (dato == null || dato.length() == 0) { //LRG
        //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
        if ( (datoInicial.containsKey("DS_DIREC")) &&
            (! ( ( (String) datoInicial.get("DS_DIREC")).equals("")))) {
          cambio = true;
        }
        parameter.put("DS_DIREC", "");
      }
      //Si long excesiva y est�n obligatorios
      else if (!fallaCamposObligatorios) { //LRG
        fallaTamano = true;
        panSuca.hacerSelectAll("DS_DIREC");
      }

    }

    dato = panSuca.getDS_NUM();
    if (dato != null && dato.length() > 0 && dato.length() <= 5) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DS_NUM"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_NUM", dato);
    }
    else { // el campo no cumplelos requisitos
      //Si falta dato
      if (dato == null || dato.length() == 0) { //LRG
        //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
        if ( (datoInicial.containsKey("DS_NUM")) &&
            (! ( ( (String) datoInicial.get("DS_NUM")).equals("")))) {
          cambio = true;
        }
        parameter.put("DS_NUM", "");
      }
      //Si long excesiva y est�n obligatorios
      else if (!fallaCamposObligatorios) { //LRG
        fallaTamano = true;
        panSuca.hacerSelectAll("DS_NUM");
      }

    }
//    // System_out.println("N�=" + dato);
    dato = panSuca.getDS_PISO();
    if (dato != null && dato.length() > 0 && dato.length() <= 4) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DS_PISO"));
      parameter.put("DS_PISO", dato);
    }
    else { // el campo no cumplelos requisitos
      //Si falta dato
      if (dato == null || dato.length() == 0) { //LRG
        //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
        if ( (datoInicial.containsKey("DS_PISO")) &&
            (! ( ( (String) datoInicial.get("DS_PISO")).equals("")))) {
          cambio = true;
        }
        parameter.put("DS_PISO", "");
      }
      //Si long excesiva y est�n obligatorios
      else if (!fallaCamposObligatorios) { //LRG
        fallaTamano = true;
        panSuca.hacerSelectAll("DS_PISO");
      }
    }

    dato = panSuca.getCD_POSTAL();
    if (dato != null && dato.length() > 0 && dato.length() <= 6) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_POSTAL"));
      parameter.put("CD_POSTAL", dato);
    }
    else { // el campo no cumplelos requisitos
      //Si falta dato
      if (dato == null || dato.length() == 0) { //LRG
        //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
        if ( (datoInicial.containsKey("CD_POSTAL")) &&
            (! ( ( (String) datoInicial.get("CD_POSTAL")).equals("")))) {
          cambio = true;
        }
        parameter.put("CD_POSTAL", "");
      }
      //Si long excesiva y est�n obligatorios
      else if (!fallaCamposObligatorios) { //LRG
        fallaTamano = true;
        panSuca.hacerSelectAll("CD_POSTAL");
      }
    }

    dato = panSuca.getCD_MUN();
    desDato = panSuca.getDS_MUN();
    //Si hay dato de municipio (es decir, si se ha tra�do descripci�n)
    if (desDato != null && desDato.length() > 0) {
      // el campo es valido y lo a�adimos como parametro
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_MUN"));
      parameter.put("CD_MUN", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
      if ( (datoInicial.containsKey("CD_MUN")) &&
          (! ( ( (String) datoInicial.get("CD_MUN")).equals("")))) {
        cambio = true;
      }
      parameter.put("CD_MUN", "");
    }

    dato = txtNivel1.getText();
    desDato = txtNivel1L.getText();
    //Si hay dato de nivel 1 (es decir, si se ha tra�do descripci�n)
    if (desDato != null && desDato.length() > 0) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_NIVEL_1"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_NIVEL_1", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
      if ( (datoInicial.containsKey("CD_NIVEL_1")) &&
          (! ( ( (String) datoInicial.get("CD_NIVEL_1")).equals("")))) {
        cambio = true;
      }
      parameter.put("CD_NIVEL_1", "");
    }

    dato = txtNivel2.getText();
    desDato = txtNivel2L.getText();
    //Si hay dato de nivel 2 (es decir, si se ha tra�do descripci�n)
    if (desDato != null && desDato.length() > 0) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_NIVEL_2"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_NIVEL_2", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
      if ( (datoInicial.containsKey("CD_NIVEL_2")) &&
          (! ( ( (String) datoInicial.get("CD_NIVEL_2")).equals("")))) {
        cambio = true;

      }
      parameter.put("CD_NIVEL_2", "");
    }

    dato = txtZBS.getText();
    desDato = txtZBSL.getText();
    //Si hay dato de ZBS (es decir, si se ha tra�do descripci�n)
    if (desDato != null && desDato.length() > 0) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_ZBS"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_ZBS", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
      if ( (datoInicial.containsKey("CD_ZBS")) &&
          (! ( ( (String) datoInicial.get("CD_ZBS")).equals("")))) {
        cambio = true;
      }
      parameter.put("CD_ZBS", "");
    }

    dato = txtObservacion.getText();
    if (dato != null && dato.length() > 0 && dato.length() <= 50) { //Cambiada long LRG
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DS_OBSERV"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_OBSERV", dato);
    }
    else { // el campo no cumplelos requisitos
      //Si falta dato
      if (dato == null || dato.length() == 0) {
        //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
        if ( (datoInicial.containsKey("DS_OBSERV")) &&
            (! ( ( (String) datoInicial.get("DS_OBSERV")).equals("")))) {
          cambio = true;
        }
        parameter.put("DS_OBSERV", "");
      }
      //Si long excesiva y est�n obligatorios
      else if (!fallaCamposObligatorios) {
        fallaTamano = true;
        txtObservacion.selectAll();
      }

    }

    dato = txtTelefono.getText();
    if (dato != null && dato.length() > 0 && dato.length() <= 14) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DS_TELEF"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DS_TELEF", dato);
    }
    else { // el campo no cumplelos requisitos
      //Si falta dato
      if (dato == null || dato.length() == 0) {
        //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
        if ( (datoInicial.containsKey("DS_TELEF")) &&
            (! ( ( (String) datoInicial.get("DS_TELEF")).equals("")))) {
          cambio = true;
        }
        parameter.put("DS_TELEF", "");
      }
      //Si long excesiva y est�n obligatorios
      else if (!fallaCamposObligatorios) {
        fallaTamano = true;
        txtTelefono.selectAll();
      }
    }

    dato = chcFcalc.getText(); // si tiene algun valor es calculada
    if (dato != null && dato.length() > 0 && dato.charAt(0) == 'c') {
      dato = "S";
      // el campo es valido y lo a�adimos como parametro
      parameter.put("IT_CALC", "S");
    }
    else {
      dato = "N";
      parameter.put("IT_CALC", "N");
    }

    if (chkbRevisao.getState()) {
      // el campo es valido y lo a�adimos como parametro
      dato = "S";
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("IT_REVISADO"));
      parameter.put("IT_REVISADO", "S");
    }
    else {
      dato = "N";
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("IT_REVISADO"));
      parameter.put("IT_REVISADO", "N");
    }

    indice = chcMBaja.getSelectedIndex();
    indice--;
    if (indice >= 0 && indice < listaMBaja.size()) {
      hash = (Hashtable) listaMBaja.elementAt(indice);
      dato = (String) hash.get("CD_MOTBAJA");
      if (dato != null && dato.length() > 0) {
        // el campo es valido y lo a�adimos como parametro
        cambio = cambio ||
            Common.ha_cambiado(dato, (String) datoInicial.get("CD_MOTBAJA"));
        parameter.put("CD_MOTBAJA", dato.toUpperCase());
      }
      else { // el campo no cumplelos requisitos
        parameter.put("CD_MOTBAJA", "");
      }
    }
    else {
      parameter.put("CD_MOTBAJA", "");
      String antMotBaja = (String) datoInicial.get("CD_MOTBAJA");
      if (antMotBaja != null && antMotBaja.trim().length() > 0) {
        cambio = true;
      }
    }

    indice = chcSexo.getSelectedIndex();
    indice--;
    if (indice >= 0 && indice < listaSexo.size()) {
      String antSexo = (String) datoInicial.get("CD_SEXO");
      hash = (Hashtable) listaSexo.elementAt(indice);
      dato = (String) hash.get("CD_SEXO");
      if (dato != null && dato.length() > 0) {
        cambio = cambio ||
            Common.ha_cambiado(dato, (String) datoInicial.get("CD_SEXO"));
        parameter.put("CD_SEXO", dato.toUpperCase());
      }
      else { // el campo no cumplelos requisitos
        parameter.put("CD_SEXO", "");
      }
    }
    else {
      parameter.put("CD_SEXO", "");
      String antSexo = (String) datoInicial.get("CD_SEXO");
      if (antSexo != null && antSexo.trim().length() > 0) {
        cambio = true;
      }
    }

    dato = panSuca.getCD_PAIS();
    if (dato != null && dato.length() > 0 && dato.length() <= 3) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_PAIS"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_PAIS", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_PAIS", "");
      //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
      if ( (datoInicial.containsKey("CD_PAIS")) &&
          (! ( ( (String) datoInicial.get("CD_PAIS")).equals("")))) {
        cambio = true;
      }
    }

    dato = panSuca.getCD_PROV();
    if (dato != null && dato.length() > 0 && dato.length() <= 2) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CD_PROV"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_PROV", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CD_PROV", "");
      //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
      if ( (datoInicial.containsKey("CD_PROV")) &&
          (! ( ( (String) datoInicial.get("CD_PROV")).equals("")))) {
        cambio = true;

      }
    }

    //para cdvial, cdtvia, dscalnum, cdtnum que se han a�adido
    //a enfermo
    dato = panSuca.getCDVIAL();
    if (dato != null && dato.length() > 0 && dato.length() <= 5) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CDVIAL"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CDVIAL", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CDVIAL", "");
      //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
      if ( (datoInicial.containsKey("CDVIAL")) &&
          (! ( ( (String) datoInicial.get("CDVIAL")).equals("")))) {
        cambio = true;
      }
    }

    dato = panSuca.getCDTVIA();
    if (dato != null && dato.length() > 0 && dato.length() <= 5) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CDTVIA"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CDTVIA", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CDTVIA", "");
      //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
      if ( (datoInicial.containsKey("CDTVIA")) &&
          (! ( ( (String) datoInicial.get("CDTVIA")).equals("")))) {
        cambio = true;
      }
    }

    dato = panSuca.getCDTNUM();
    if (dato != null && dato.length() > 0 && dato.length() <= 1) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("CDTNUM"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CDTNUM", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("CDTNUM", "");
      //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
      if ( (datoInicial.containsKey("CDTNUM")) &&
          (! ( ( (String) datoInicial.get("CDTNUM")).equals("")))) {
        cambio = true;

      }
    }

    dato = panSuca.getDSCALNUM();
    if (dato != null && dato.length() > 0 && dato.length() <= 6) {
      cambio = cambio ||
          Common.ha_cambiado(dato, (String) datoInicial.get("DSCALNUM"));
      // el campo es valido y lo a�adimos como parametro
      parameter.put("DSCALNUM", dato.toUpperCase());
    }
    else { // el campo no cumplelos requisitos
      parameter.put("DSCALNUM", "");
      //Ahora dato vac�o. Por tanto , si antes hab�a dato, se apunta cambio
      if ( (datoInicial.containsKey("DSCALNUM")) &&
          (! ( ( (String) datoInicial.get("DSCALNUM")).equals("")))) {
        cambio = true;

      }
    }

//_________ Cepillator

    // comprobamos los de otras direcciones

    if (fallaCamposObligatorios) {
      msgBox = new CMessage(app, CMessage.msgAVISO,
                            "Faltan campo obligatorio:" + campo);
      msgBox.show();
      msgBox = null;
    }
    else if (fechaErronea) {
      msgBox = new CMessage(app, CMessage.msgAVISO,
                            "Fecha mal escrita:" + campo);
      msgBox.show();
      msgBox = null;
    }
    else if (fallaTamano) {
      msgBox = new CMessage(app, CMessage.msgAVISO,
                            "Longitud excesiva de los campos");
      msgBox.show();
      msgBox = null;
    }

    //  true si ha ido bien
    if (cambio == false) {
      // si los datos no han cambiado
      return false;
    }
    else {
      return (!fallaCamposObligatorios && !fechaErronea && !fallaTamano);
    }

  }

  // M�todos privados
  private String metaModoActual() {
    String s = null;

    boolean bEnfermos = chkEnfermos.getState();
    boolean bContactos = chkContactos.getState();

    if (bEnfermos && bContactos) {
      s = constantes.sEnfermosContactos;
    }
    else if (bEnfermos) {
      s = constantes.sSoloEnfermos;
    }
    else if (bContactos) {
      s = constantes.sSoloContactos;
    }

    return s;
  }

  private String restaTiempo(int iTiempo, boolean is_meses, Date a_date) {

    SimpleDateFormat formato3 = new SimpleDateFormat("dd/MM/yyyy",
        new Locale("es", "ES"));
    String strDate = "";
    try {
      strDate = formato3.format(a_date);
    }
    catch (Exception e) {
      e.printStackTrace(); // // System_out.println("Excepcion en el formato3 raro");
    }
    int anios = Integer.parseInt(strDate.substring(6));
    int meses = Integer.parseInt(strDate.substring(3, 5));
    if (is_meses) {
      meses -= iTiempo;
      if (meses < 0) {
        meses += 12;
        anios--;
      }
    }
    else {
      anios -= iTiempo;
    }
    String sMes = (new Integer(meses)).toString();
    if (sMes.length() < 2) {
      sMes = "0" + sMes;

    }
    return "15/" + sMes + "/" + (new Integer(anios)).toString();
  }

  public void txtFocusEdad() {
    // calculamos la fecha de Nacimiento restando la edad aldia de hoy
    String strNuevaFecha = txtEdad.getText();

    // si no ha cambiado la edad nos salimos sin hacer nada
    if (strEdadBk.equals(strNuevaFecha)) {
      return;
    }

    Date fec = new Date();

    int indice = chcEdad.getSelectedIndex();
    int valor = Integer.parseInt(txtEdad.getText());
    if (indice == 1) { // pone meses
      strNuevaFecha = restaTiempo(valor, true, new Date());
    }
    else if (indice == 0) { // pone a�os
      strNuevaFecha = restaTiempo(valor, false, new Date());
    }

    //strNuevaFecha = Fechas.date2String(fresult);
    txtFecNacimiento.setText(strNuevaFecha);
    //chcFcalc.setText(res.getString("chcFcalc.Text"));
    chcFcalc.setText("calculada");
  }

  public void txtFocusFecNacimiento() {
    int anios;
    CFechaSimple copi_txtFecNacimiento = new CFechaSimple("N");
    // calculamos la edad restando la fecha al dia
    copi_txtFecNacimiento.setText(txtFecNacimiento.getText());
    copi_txtFecNacimiento.ValidarFecha();
    if (copi_txtFecNacimiento.getValid().equals("N")) {
      //////////txtFecNacimiento.setText("");
      //CMessage msgBox = new CMessage(app, CMessage.msgERROR, res.getString("msg11.Text"));
      CMessage msgBox = new CMessage(app, CMessage.msgERROR,
                                     "Formato Fecha dd/mm/aaaa");
      msgBox.show();
      msgBox = null;
    }
    else {
      txtFecNacimiento.setText(copi_txtFecNacimiento.getFecha());
      //chcFcalc.setText(res.getString("chcFcalc.Text"));
      chcFcalc.setText("calculada");
      java.util.Date d = Fechas.string2Date(txtFecNacimiento.getText());
      //calculamos la edad tambi�n
      if (Fechas.edadTipo(d)) {
        anios = Fechas.edadAnios(d);
        chcEdad.select(0);
      }
      else {
        anios = Fechas.edadMeses(d);
        chcEdad.select(1);
      }
      strEdadBk = Integer.toString(anios);
      txtEdad.setText(strEdadBk);
    }
    copi_txtFecNacimiento = null;
  }

  public void txtFocusFecBaja() {
    int anios;
    CFechaSimple copi_txtFecBaja = new CFechaSimple("N");
    // calculamos la edad restando la fecha al dia
    copi_txtFecBaja.setText(txtFecNacimiento.getText());
    copi_txtFecBaja.ValidarFecha();
    if (copi_txtFecBaja.getValid().equals("N")) {
      //////////txtFecNacimiento.setText("");
      //CMessage msgBox = new CMessage(app, CMessage.msgERROR, res.getString("msg11.Text"));
      CMessage msgBox = new CMessage(app, CMessage.msgERROR,
                                     "Formato Fecha dd/mm/aaaa");
      msgBox.show();
      msgBox = null;
    }
    else {
      txtFecha.setText(copi_txtFecBaja.getFecha());
    }
    copi_txtFecBaja = null;
  }

  public void txtFocusNivel1() {
    // datos de envio
    DataNivel1 nivel1;
    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    // gestion de datos
    if (txtNivel1.getText().length() > 0) {
      param = new CLista();
      param.addElement(new DataNivel1(txtNivel1.getText()));
      strServlet = strSERVLETNivel1;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    // busca el item
    if (param != null) {

      try {
        param.setIdioma(app.getIdioma());
        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {
          nivel1 = (DataNivel1) param.firstElement();
          txtNivel1.setText(nivel1.getCod());
          txtNivel1L.setText(nivel1.getDes());
        }
        // no hay datos
        else {
          //msg = new CMessage(this.app, CMessage.msgAVISO, res.getString("msg12.Text"));
          msg = new CMessage(this.app, CMessage.msgAVISO, "No hay datos.");
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
  }

  public void txtFocusNivel2() {
    // datos de envio
    DataZBS2 nivel2;
    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    if (txtNivel2.getText().length() > 0) {
      param = new CLista();
      param.addElement(new DataZBS2(txtNivel1.getText(), txtNivel2.getText(),
                                    "", ""));
      strServlet = strSERVLETNivel2;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    // busca el item
    if (param != null) {

      try {
        param.setIdioma(app.getIdioma());
        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {
          nivel2 = (DataZBS2) param.firstElement();
          txtNivel2.setText(nivel2.getNiv2());
          txtNivel2L.setText(nivel2.getDes());
        }
        // no hay datos
        else {
          //msg = new CMessage(this.app, CMessage.msgAVISO, res.getString("msg12.Text"));
          msg = new CMessage(this.app, CMessage.msgAVISO, "No hay datos.");
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
  }

  public void txtFocusZBS() {
    // datos de envio
    DataZBS zbs;
    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    if (txtZBS.getText().length() > 0) {
      param = new CLista();
      param.addElement(new DataZBS(txtZBS.getText(), "", "", txtNivel1.getText(),
                                   txtNivel2.getText()));
      strServlet = strSERVLETZona;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    // busca el item
    if (param != null) {

      try {
        param.setIdioma(app.getIdioma());
        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {
          zbs = (DataZBS) param.firstElement();
          txtZBS.setText(zbs.getCod());
          txtZBSL.setText(zbs.getDes());

        }

        // no hay datos
        else {
          //msg = new CMessage(this.app, CMessage.msgAVISO, res.getString("msg12.Text"));
          msg = new CMessage(this.app, CMessage.msgAVISO, "No hay datos.");
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
  }

  // limpia la pantalla
  void vaciarPantalla() {
    //txtCodigo.setText("");
    txtApellido1.setText("");
    txtApellido2.setText("");
    txtNombre.setText("");
    txtFecNacimiento.setText("");
    strEdadBk = "";
    txtEdad.setText(strEdadBk);
    chcEdad.select(0);
    chcIdentificacion.select(0);
    txtIdentificacion.setText("");
    chcSexo.select(0);
    txtTelefono.setText("");
    txtNivel1.setText("");
    txtNivel2.setText("");
    txtNivel1L.setText("");
    txtNivel2L.setText("");
    txtZBS.setText("");
    txtZBSL.setText("");
    chkbRevisao.setState(false);
    chcMBaja.select(0);
    txtFecha.setText("");
    txtObservacion.setText("");
    //panSuca.setLimpiar();
    listaEnfermo = null;
    listaOtrasDirecciones = null;
  }

  /**
       *   esta funcion rellena toda la pantalla con los datos de la Lista de Enfermo
   */
  public void rellenaPantalla() {

    Hashtable hash = null;
    String dato = null;

    if (listaEnfermo != null) {
      hash = (Hashtable) listaEnfermo.firstElement();
    }

    if (hash == null) {
      return;
    }

//      // System_out.println("Cuando he vuelto de recoger los datos FC_ULTACT " + (String) hash.get("FC_ULTACT"));

    // ponemos el modo de modificar
    modo = modoMODIFICAR;
    // pintamos los choices
    synchronized (sincro) {
      if (listaId != null && chcIdentificacion.getItemCount() == 1) {
        Common.writeChoice(app, chcIdentificacion, listaId, false, "DS_TIPODOC");
      }
      if (listaSexo != null && chcSexo.getItemCount() == 1) {
        Common.writeChoice(app, chcSexo, listaSexo, false, "DS_SEXO");
      }
      if (listaMBaja != null && chcMBaja.getItemCount() == 1) {
        Common.writeChoice(app, chcMBaja, listaMBaja, false, "DS_MOTBAJA");
      }
    } // synchro

    dato = (hash.get("CD_ENFERMO")).toString();
    if (dato != null) {
      txtCodigo.setText(dato);
    }
    else {
      txtCodigo.setText("");
    }

    if (bPermiso) {
      dato = (String) hash.get("DS_APE1");
      if (dato != null) {
        txtApellido1.setText(dato);
      }
      else {
        txtApellido1.setText("");
      }
      dato = (String) hash.get("DS_APE2");
      if (dato != null) {
        txtApellido2.setText(dato);
      }
      else {
        txtApellido2.setText("");
      }
      dato = (String) hash.get("DS_NOMBRE");
      if (dato != null) {
        txtNombre.setText(dato);
      }
      else {
        txtNombre.setText("");
      }
    }
    else {
      //lblApellido1.setText(res.getString("lblApellido1.Text"));
      lblApellido1.setText("Apellido 1:");
      dato = (String) hash.get("SIGLAS");
      if (dato != null) {
        txtApellido1.setText(dato);
      }
      else {
        txtApellido1.setText("");
      }
      txtNombre.setText("");
      txtApellido2.setText("");
    }

    dato = (String) hash.get("IT_REVISADO");
    if (dato != null && dato.equals("S")) {
      chkbRevisao.setState(true);
    }
    else {
      chkbRevisao.setState(false);
    }

    dato = (String) hash.get("DS_TELEF");
    if (dato != null) {
      txtTelefono.setText(dato);
    }
    else {
      txtTelefono.setText("");
    }

    dato = (String) hash.get("DS_OBSERV");
    if (dato != null) {
      txtObservacion.setText(dato);
    }
    else {
      txtObservacion.setText("");
    }

    dato = (String) hash.get("CD_NIVEL_1");
    if (dato != null) {
      txtNivel1.setText(dato);
    }
    else {
      txtNivel1.setText("");
    }

    dato = (String) hash.get("CD_NIVEL_2");
    if (dato != null) {
      txtNivel2.setText(dato);
    }
    else {
      txtNivel2.setText("");
    }

    dato = (String) hash.get("CD_ZBS");
    if (dato != null) {
      txtZBS.setText(dato);
    }
    else {
      txtZBS.setText("");
    }

    dato = (String) hash.get("DS_NIVEL_1");
    if (dato != null) {
      txtNivel1L.setText(dato);
    }
    else {
      txtNivel1L.setText("");
    }

    dato = (String) hash.get("DS_NIVEL_2");
    if (dato != null) {
      txtNivel2L.setText(dato);
    }
    else {
      txtNivel2L.setText("");
    }

    dato = (String) hash.get("DS_ZBS");
    if (dato != null) {
      txtZBSL.setText(dato);
    }
    else {
      txtZBSL.setText("");
    }

    dato = (String) hash.get("CD_PAIS");
    if (dato != null) {
      panSuca.setCD_PAIS(dato);
    }
    else {
      panSuca.setCD_PAIS("");
    }

    dato = (String) hash.get("CD_CA");
    if (dato != null) {
      panSuca.setCD_CA(dato);
    }
    else {
      panSuca.setCD_CA("");
    }

    dato = (String) hash.get("CD_PROV");
    if (dato != null) {
      panSuca.setCD_PROV(dato);
    }
    else {
      panSuca.setCD_PROV("");
    }

    dato = (String) hash.get("CD_MUN");
    if (dato != null) {
      panSuca.setCD_MUN(dato);
    }
    else {
      panSuca.setCD_MUN("");
    }

    dato = (String) hash.get("DS_MUN");
    if (dato != null) {
      panSuca.setDS_MUN(dato);
    }
    else {
      panSuca.setDS_MUN("");
    }

    dato = (String) hash.get("CDVIAL");
    if (dato != null) {
      panSuca.setCDVIAL(dato);
    }
    else {
      panSuca.setCDVIAL("");
    }

    dato = (String) hash.get("DS_DIREC");
    if (dato != null) {
      panSuca.setDS_DIREC(dato);
    }
    else {
      panSuca.setDS_DIREC("");
    }

    dato = (String) hash.get("DS_NUM");
    if (dato != null) {
      panSuca.setDS_NUM(dato);
    }
    else {
      panSuca.setDS_NUM("");
    }

    dato = (String) hash.get("DS_PISO");
    if (dato != null) {
      panSuca.setDS_PISO(dato);
    }
    else {
      panSuca.setDS_PISO("");
    }

    dato = (String) hash.get("DSCALNUM");
    if (dato != null) {
      panSuca.setDSCALNUM(dato);
    }
    else {
      panSuca.setDSCALNUM("");
    }

    dato = (String) hash.get("CD_POSTAL");
    if (dato != null) {
      panSuca.setCD_POSTAL(dato);
    }
    else {
      panSuca.setCD_POSTAL("");
    }

    dato = (String) hash.get("CDTVIA");
    if (dato != null) {
      panSuca.setCDTVIA(dato);
    }
    else {
      panSuca.setCDTVIA("");
    }

    dato = (String) hash.get("CDTNUM");
    if (dato != null) {
      panSuca.setCDTNUM(dato);
    }
    else {
      panSuca.setCDTNUM("");
    }

    // E Date dd = (Date)hash.get("FC_BAJA");
    Date dd; // E
    dato = (String) hash.get("FC_BAJA"); // E Fechas.date2String(dd);
    if (dato != null) {
      txtFecha.setText(dato);
    }
    else {
      txtFecha.setText("");
    }

    //dd = (Date)hash.get("FC_NAC");
    //dato =  Fechas.date2String(dd);
    dato = (String) hash.get("FC_NAC");
    int anios = 0;
    if (dato != null) {
      dd = Fechas.string2Date(dato);
      txtFecNacimiento.setText(dato);
      //indica si la fecha es calculada o no
      dato = (String) hash.get("IT_CALC");
      if (dato != null && dato.charAt(0) == 'S') {
        //chcFcalc.setText(res.getString("chcFcalc.Text"));
        chcFcalc.setText("calculada");
      }
      else {
        //chcFcalc.setText(res.getString("chcFcalc.Text"));
        chcFcalc.setText("calculada");
      }
      //calculamos la edad tambi�n
      if (Fechas.edadTipo(dd)) {
        anios = Fechas.edadAnios(dd);
        chcEdad.select(0);
      }
      else {
        anios = Fechas.edadMeses(dd);
        chcEdad.select(1);
      }
      strEdadBk = Integer.toString(anios);
      txtEdad.setText(strEdadBk);

    }
    else {
      ///////////////txtFecNacimiento.setText("");
    }

    dato = (String) hash.get("DS_NDOC");
    if (dato != null) {
      txtIdentificacion.setText(dato);
    }
    else {
      txtIdentificacion.setText("");
    }

    // ponemos los choices
    int indice = Common.buscaDato(listaId, "CD_TDOC",
                                  (String) hash.get("CD_TDOC"));
    indice++;
    if (indice >= 0) {
      chcIdentificacion.select(indice);
    }

    indice = Common.buscaDato(listaMBaja, "CD_MOTBAJA",
                              (String) hash.get("CD_MOTBAJA"));
    indice++;
    if (indice >= 0) {
      chcMBaja.select(indice);
    }

    indice = Common.buscaDato(listaSexo, "CD_SEXO", (String) hash.get("CD_SEXO"));
    indice++;
    if (indice >= 0) {
      chcSexo.select(indice);
    }

    // lo siguiente, �como se hace en el panelsuca?
    Inicializar();
    this.doLayout();
  }

  public void txtFocusCodigo() {
    if (metaModoActual() == null) {
      Common.ShowWarning(this.app, "Debe seleccionar al menos una opci�n");
      txtCodigo.setText("");
      return;
    }
    CLista data = new CLista();
    String campo = txtCodigo.getText();
    if (campo != null && campo.trim().length() > 0 &&
        !strCodigoBK.equals(campo.trim())) {
      Inicializar();
      strCodigoBK = campo;

      DataEnfermo denfer = new DataEnfermo("CD_ENFERMO");
      denfer.put("CD_ENFERMO", campo);
      data.addElement(denfer);
      data.addElement(metaModoActual()); // �mbito de la selecci�n

      /*
                 try {
                   SrvEnfermo srv = new SrvEnfermo();
           listaEnfermo = srv.doPrueba(SrvEnfermo.modoDATOSENFERMO, data);
                 } catch (Exception e) {
                 }
       */

      if (btramero) {
        listaEnfermo = Common.traerDatos(app, stubCliente,
                                         constantes.strSERVLET_ENFERMO,
                                         constantes.modoDATOSENFERMOTRAMERO,
                                         data);
      }
      else {
        listaEnfermo = Common.traerDatos(app, stubCliente,
                                         constantes.strSERVLET_ENFERMO,
                                         constantes.modoDATOSENFERMO, data);

      }
      listaOtrasDirecciones = null;
      if (listaEnfermo != null && listaEnfermo.size() > 0) {
        rellenaPantalla();
      }
      else {
        //CMessage msgBox = new CMessage(app,CMessage.msgAVISO, res.getString("msg13.Text"));
        CMessage msgBox = new CMessage(app, CMessage.msgAVISO, "No hay datos");
        msgBox.show();
        msgBox = null;
      }
    }
    else {
      txtCodigo.setText(strCodigoBK);
    }
  }

  // inicia la prueba de busenfermo  buscar al enfermo
  void btnPrueba_actionPerformed(ActionEvent e) {
    CMessage msgBox;
    DialBusEnfermo dial;
    CLista data = new CLista();
    data.setIdioma(app.getIdioma());
    DataEnfermo dEnfer = null;

    if (sCodAplicacion.equals(APP_TUBERCULOSIS)) {
      if (metaModoActual() == null) {
        Common.ShowWarning(this.app, "Debe seleccionar al menos una opci�n");
        txtCodigo.setText("");
        return;
      }
    }

    try {
      codEnfermo = null;

      dial = new DialBusEnfermo(this.app, this, metaModoActual(),
                                sCodAplicacion.equals(APP_TUBERCULOSIS));

      dial.show();
      CLista lista = dial.getListaDatosEnfermo();
      dial = null;

      //miramos si hay que rellenar los choices
      // se piden los dactos y rellenamos la tabla
      if (lista != null) {
        listaEnfermo = lista;
        listaOtrasDirecciones = null;
        rellenaPantalla();
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
      dial = null;
    }

    // libera recursos
  }

  // inicia la prueba de busenfermo,  buscar otras direcciones
  void btnOtrasDir_actionPerformed(ActionEvent e) {
    CMessage msgBox;
    // creamos las pantallas otras direcciones
    DialOtrasDirecciones dialOtrasDir = null;

    try {
      dialOtrasDir = new DialOtrasDirecciones(this.app, this);
      if (listaOtrasDirecciones != null && listaOtrasDirecciones.size() > 0) {
        dialOtrasDir.setData(listaOtrasDirecciones); // le pasamos los datos de la CLista
      }
      else {
        dialOtrasDir.setData(listaEnfermo); // le pasamos los datos de la CLista
      }
      // ponemos a modo espera
      dialOtrasDir.show();
      // nos devuelve los datos concretos d eotras direcciones
      //listaOtrasDirecciones = dialOtrasDir.getData();
      listaOtrasDirecciones = dialOtrasDir.getData();
      dialOtrasDir = null;

    }
    catch (Exception ex) {
      ex.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
      dialOtrasDir = null;
    }
  }

  // graba en la base de datos los datos escritos
  void btnActualiza_actionPerformed(ActionEvent e) {
    // rellenamos los datos de los textFiled en listaEnfermo
    // comprobamos que los datos esten bien puestos
    CLista result = null;
    CLista listaNuevosDatos = new CLista();

    listaNuevosDatos.addElement(new DataEnfermo("CD_ENFERMO"));

    // lo ponemos a modo espera
    // mandamos que sobrescriba los valores de listaEnfermo
    if (isDataValid(listaNuevosDatos)) {
      // a�adimos los datos anteriores
      listaNuevosDatos.addElement(datosAnteriores( (DataEnfermo) listaEnfermo.
                                                  firstElement()));
      // mandamos al servlet la listaEnfermo

      result = Common.traerDatos(app, stubCliente,
                                 constantes.strSERVLET_ENFERMO,
                                 SrvEnfermo.modoMODIFICADATOS, listaNuevosDatos);

      if (result != null) {
        //CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, res.getString("msg16.Text"));
        CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                       "Se han modificado los datos");
        msgBox.show();
        msgBox = null;
        // cierra el applet y deja la p�gina b�sica
        listaNuevosDatos.removeElementAt(1);
        // por si acaso, dejamos los datos antiguos
        listaEnfermo = listaNuevosDatos;
        String fec = (String) ( (Hashtable) result.firstElement()).get(
            "FC_ULTACT");
        ( (DataEnfermo) listaEnfermo.firstElement()).put("FC_ULTACT", fec);

        listaOtrasDirecciones = null;
        /// mandamos cerra<r el applet
        btnCerrar_actionPerformed(null);
      }
    }
  }

  protected DataEnfermo datosAnteriores(DataEnfermo a_data) {
    DataEnfermo parameter = new DataEnfermo("CD_ENFERMO");
    String dato = app.getLogin();
    Object o;
    if (dato != null && dato.length() > 0 && dato.length() <= 6) {
      // el campo es valido y lo a�adimos como parametro
      parameter.put("CD_OPE", "'" + dato.toUpperCase() + "'");
    }

    o = a_data.get("CD_ENFERMO");
    if (o != null) {
      parameter.put("CD_ENFERMO", o);
    }
    o = a_data.get("DS_APE1");
    if (o != null) {
      parameter.put("DS_APE1", o);
    }
    else {
      parameter.put("DS_APE1", "*");
    }
    o = a_data.get("DS_FONOAPE1");
    if (o != null) {
      parameter.put("DS_FONOAPE1", o);
    }
    o = a_data.get("DS_APE2");
    if (o != null) {
      parameter.put("DS_APE2", o);
    }
    o = a_data.get("DS_FONOAPE2");
    if (o != null) {
      parameter.put("DS_FONOAPE2", o);
    }
    o = a_data.get("DS_NOMBRE");
    if (o != null) {
      parameter.put("DS_NOMBRE", o);
    }
    o = a_data.get("DS_FONONOMBRE");
    if (o != null) {
      parameter.put("DS_FONONOMBRE", o);
    }
    o = a_data.get("SIGLAS");
    if (o != null) {
      parameter.put("SIGLAS", o);
    }
    ( (DataEnfermo) parameter).setFechaBaja(a_data.getFechaBaja());
    ( (DataEnfermo) parameter).setFechaNacimiento(a_data.getFechaNacimiento());
    //((DataEnfermo)parameter).setFechaActual(a_data.getFechaActual());

    o = a_data.get("FC_ULTACT");
//    // System_out.println("Cuando cojo los datos anteriores FC_ULTACT " + o.toString());
    if (o != null) {
      parameter.put("FC_ULTACT", o);

    }
    o = a_data.get("DS_DIREC");
    if (o != null) {
      parameter.put("DS_DIREC", o);
    }
    o = a_data.get("DS_NUM");
    if (o != null) {
      parameter.put("DS_NUM", o);
    }
    o = a_data.get("DS_PISO");
    if (o != null) {
      parameter.put("DS_PISO", o);
    }
    o = a_data.get("CD_POSTAL");
    if (o != null) {
      parameter.put("CD_POSTAL", o);
    }
    o = a_data.get("CD_MUN");
    if (o != null) {
      parameter.put("CD_MUN", o);
    }
    o = a_data.get("CD_PROV");
    if (o != null) {
      parameter.put("CD_PROV", o);
    }
    o = a_data.get("CD_NIVEL_1");
    if (o != null) {
      parameter.put("CD_NIVEL_1", o);
    }
    o = a_data.get("CD_NIVEL_2");
    if (o != null) {
      parameter.put("CD_NIVEL_2", o);
    }
    o = a_data.get("CD_ZBS");
    if (o != null) {
      parameter.put("CD_ZBS", o);
    }
    o = a_data.get("DS_OBSERV");
    if (o != null) {
      parameter.put("DS_OBSERV", o);
    }
    o = a_data.get("DS_TELEF");
    if (o != null) {
      parameter.put("DS_TELEF", o);
    }
    o = a_data.get("IT_CALC");
    if (o != null) {
      parameter.put("IT_CALC", o);
    }
    o = a_data.get("IT_REVISADO");
    if (o != null) {
      parameter.put("IT_REVISADO", o);
    }
    o = a_data.get("CD_MOTBAJA");
    if (o != null) {
      parameter.put("CD_MOTBAJA", o);
    }
    o = a_data.get("CD_SEXO");
    if (o != null) {
      parameter.put("CD_SEXO", o);
    }
    o = a_data.get("CD_PAIS");
    if (o != null) {
      parameter.put("CD_PAIS", o);

    }
    o = a_data.get("DS_NDOC");
    if (o != null) {
      parameter.put("DS_NDOC", o);
    }
    o = a_data.get("CD_TDOC");
    if (o != null) {
      parameter.put("CD_TDOC", o);
    }
    o = a_data.get("CD_OPE");
    if (o != null) {
      parameter.put("CD_OPE", o);
    }
    o = a_data.get("CD_PROV2");
    if (o != null) {
      parameter.put("CD_PROV2", o);
    }
    o = a_data.get("CD_MUNI2");
    if (o != null) {
      parameter.put("CD_MUNI2", o);
    }
    o = a_data.get("CD_POST2");
    if (o != null) {
      parameter.put("CD_POST2", o);
    }
    o = a_data.get("DS_DIREC2");
    if (o != null) {
      parameter.put("DS_DIREC2", o);
    }
    o = a_data.get("DS_NUM2");
    if (o != null) {
      parameter.put("DS_NUM2", o);
    }
    o = a_data.get("DS_PISO2");
    if (o != null) {
      parameter.put("DS_PISO2", o);
    }
    o = a_data.get("DS_OBSERV2");
    if (o != null) {
      parameter.put("DS_OBSERV2", o);
    }
    o = a_data.get("CD_PROV3");
    if (o != null) {
      parameter.put("CD_PROV3", o);
    }
    o = a_data.get("CD_MUNI3");
    if (o != null) {
      parameter.put("CD_MUNI3", o);
    }
    o = a_data.get("CD_POST3");
    if (o != null) {
      parameter.put("CD_POST3", o);
    }
    o = a_data.get("DS_DIREC3");
    if (o != null) {
      parameter.put("DS_DIREC3", o);
    }
    o = a_data.get("DS_NUM3");
    if (o != null) {
      parameter.put("DS_NUM3", o);
    }
    o = a_data.get("DS_PISO3");
    if (o != null) {
      parameter.put("DS_PISO3", o);
    }
    o = a_data.get("DS_TELEF3");
    if (o != null) {
      parameter.put("DS_TELEF3", o);
    }
    o = a_data.get("DS_TELEF2");
    if (o != null) {
      parameter.put("DS_TELEF2", o);
    }
    o = a_data.get("DS_OBSERV3");
    if (o != null) {
      parameter.put("DS_OBSERV3", o);

    }
    o = a_data.get("CDTVIA");
    if (o != null) {
      parameter.put("CDTVIA", o);
    }
    o = a_data.get("DSCALNUM");
    if (o != null) {
      parameter.put("DSCALNUM", o);
    }
    o = a_data.get("CDTNUM");
    if (o != null) {
      parameter.put("CDTNUM", o);
    }
    o = a_data.get("CDVIAL");
    if (o != null) {
      parameter.put("CDVIAL", o);

      // E Se tienen en cuenta IT_ENFERMO e IT_TRAMERO
    }
    o = a_data.get("IT_ENFERMO");
    if (o != null) {
      parameter.put("IT_ENFERMO", o);
    }
    o = a_data.get("IT_CONTACTO");
    if (o != null) {
      parameter.put("IT_CONTACTO", o);
      //
    }
    return parameter;
  }

// prueba listener de evento  de botones
  class Pan_EnfermoBtnActionListener
      implements ActionListener, Runnable {
    Pan_Enfermo adaptee = null;
    ActionEvent e = null;

    public Pan_EnfermoBtnActionListener(Pan_Enfermo adaptee) {
      this.adaptee = adaptee;
    }

    // evento
    public void actionPerformed(ActionEvent e) {
      if (adaptee.bloquea()) {
        this.e = e;
        Thread th = new Thread(this);
        th.start();
      }
    }

    // hilo de ejecuci�n para servir el evento
    public void run() {
      String name = e.getActionCommand();
      CLista lalista = null;
      String campo = null;

      Inicializar();
      if (name.equals("codigo")) { // muestra la pantalla de b�squeda
        adaptee.btnPrueba_actionPerformed(e);
      }
      else if (name.equals("otrasDirecciones")) { // actualizar las direcciones
        adaptee.btnOtrasDir_actionPerformed(e);
      }
      else if (name.equals("actualizar")) { // actualizar todos los datos
        adaptee.btnActualiza_actionPerformed(e);
      }
      else if (name.equals("nivel1")) {

        DataNivel1 data = null;
        CMessage mensaje = null;
        CApp app = adaptee.getApp();

        try {
          CListaBuscarNivel1 lista = new CListaBuscarNivel1(app,
              //res.getString("msg8.Text")+adaptee.getApp().getNivel1(),
              "Se han modificado los datos" + adaptee.getApp().getNivel1(),
              adaptee.stubCliente,
              strSERVLETNivel1,
              servletOBTENER_X_CODIGO,
              servletOBTENER_X_DESCRIPCION,
              servletSELECCION_X_CODIGO,
              servletSELECCION_X_DESCRIPCION);
          lista.show();
          data = (DataNivel1) lista.getComponente();

        }
        catch (Exception excepc) {
          excepc.printStackTrace();
          mensaje = new CMessage(adaptee.getApp(), CMessage.msgERROR,
                                 excepc.getMessage());
          mensaje.show();
        }

        if (data != null) {
          txtNivel1.setText(data.getCod());
          ///************  problema del idioma
          txtNivel1L.setText(data.getDes());
          txtNivel2.setText("");
          txtNivel2L.setText("");
          txtZBS.setText("");
          txtZBSL.setText("");
        }

      }
      else if (name.equals("nivel2")) {
        CApp ap = adaptee.getApp();
        DataZBS2 data = null;
        CMessage msgBox = null;

        try {

          CListaZBS2 lista = new CListaZBS2(adaptee,
                                            //res.getString("msg8.Text")+ap.getNivel2(),
                                            "Se han modificado los datos" +
                                            ap.getNivel2(),
                                            adaptee.stubCliente,
                                            strSERVLETNivel2,
                                            servletOBTENER_NIV2_X_CODIGO,
                                            servletOBTENER_NIV2_X_DESCRIPCION,
                                            servletSELECCION_NIV2_X_CODIGO,
                                            servletSELECCION_NIV2_X_DESCRIPCION);
          lista.show();
          data = (DataZBS2) lista.getComponente();
        }
        catch (Exception er) {
          er.printStackTrace();
          msgBox = new CMessage(adaptee.getApp(), CMessage.msgERROR,
                                er.getMessage());
          msgBox.show();
          msgBox = null;
        }

        if (data != null) {
          txtNivel2.setText(data.getNiv2());
          ///************  problema del idioma
          txtNivel2L.setText(data.getDes());
          txtZBS.setText("");
          txtZBSL.setText("");
        }
      }
      else if (name.equals("zbs")) {

        DataZBS data = null;
        CMessage msgBox = null;

        try {
          CListaZona lista = new CListaZona(adaptee,
                                            //res.getString("msg9.Text"),
                                            "Selecci�n de Zona b�sica de salud",
                                            adaptee.stubCliente,
                                            strSERVLETZona,
                                            servletOBTENER_NIV2_X_CODIGO,
                                            servletOBTENER_NIV2_X_DESCRIPCION,
                                            servletSELECCION_NIV2_X_CODIGO,
                                            servletSELECCION_NIV2_X_DESCRIPCION);
          lista.show();
          data = (DataZBS) lista.getComponente();

        }
        catch (Exception er) {
          er.printStackTrace();
          msgBox = new CMessage(adaptee.getApp(), CMessage.msgERROR,
                                er.getMessage());
          msgBox.show();
          msgBox = null;
        }

        if (data != null) {
          txtZBS.setText(data.getCod());
          txtZBSL.setText(data.getDes());
        }
      }
      adaptee.desbloquea();
    }

  } //__________________________________________________ END_CLASS

// escuchador de los cambios en los itemListener  de los choices
  /**
   *  hay tres CListas que representan paises,comunidades y provincias
   *  cuando cambiamos el pais, se rellena la lista de comunidades
   *  y se vacia la lista de Provincias
   */
  class PanBusEnfermoTextFocusListener
      implements java.awt.event.FocusListener, java.awt.event.ActionListener,
      Runnable {
    Pan_Enfermo adaptee;
    String name2 = null;
    protected StubSrvBD stub = new StubSrvBD();

    PanBusEnfermoTextFocusListener(Pan_Enfermo adaptee) {
      this.adaptee = adaptee;
    }

    public void focusGained(FocusEvent e) {
    }

    public void focusLost(FocusEvent e) {
      if (adaptee.bloquea()) {
        name2 = ( (Component) e.getSource()).getName();
        // lanzamos el thread que tratar� el evento
        new Thread(this).start();
      }
    }

    public void actionPerformed(ActionEvent e) {
      if (adaptee.bloquea()) {
        name2 = ( (Component) e.getSource()).getName();
//         // System_out.println("EVENTO " + name2);
        // lanzamos el thread que tratar� el evento
        new Thread(this).start();
      }
    }

    public void run() {
      CLista lalista = null;
      String campo = null;
      CLista result = null, data = new CLista();
      DataEnfermo denfer = null;
      try {

        Inicializar();
        if (name2.equals("codigo")) { // muestra la pantalla de b�squeda
          adaptee.txtFocusCodigo();
          // Cris }else if (name2.equals("municipio")){
          // Cris       adaptee.txtFocusMunicipio();
        }
        else if (name2.equals("edad")) {
          adaptee.txtFocusEdad();
        }
        else if (name2.equals("fecNacimiento")) {
          adaptee.txtFocusFecNacimiento();
        }
        else if (name2.equals("nivel1")) {
          adaptee.txtFocusNivel1();
        }
        else if (name2.equals("nivel2")) {
          adaptee.txtFocusNivel2();
        }
        else if (name2.equals("zbs")) {
          adaptee.txtFocusZBS();
        }
        else if (name2.equals("fecBaja")) {
          adaptee.txtFocusFecBaja();
        }

      }
      catch (Exception exc) {
        exc.printStackTrace(); // // System_out.println("focusLis " + exc.toString());
      }
      adaptee.desbloquea();
    }

  } //_______________________________________________ END_CLASS

// escuchador de los cambios en los itemListener  de los choices
  /**
   *  hay tres CListas que representan paises,comunidades y provincias
   *  cuando cambiamos el pais, se rellena la lista de comunidades
   *  y se vacia la lista de Provincias
   */
  class PanBusEnfermoChoiceItemListener
      implements java.awt.event.ItemListener, Runnable {
    Pan_Enfermo adaptee;
    ItemEvent e = null;

    PanBusEnfermoChoiceItemListener(Pan_Enfermo adaptee) {
      this.adaptee = adaptee;
    }

    // evento
    public void itemStateChanged(ItemEvent e) {
      if (adaptee.bloquea()) {
        this.e = e;
        // lanzamos el thread que tratar� el evento
        //new Thread(this).start();
        run1();
      }
    }

    public void run() {
      CLista data = new CLista();
      CLista appList = null;
      int indice;
      Inicializar();
      adaptee.desbloquea();
    }

    // hilo de ejecuci�n para servir el evento
    public void run1() {
      // quitamos los valores de los textfield de descripcion
      String name2 = ( (Component) e.getSource()).getName();
      CLista data = new CLista();
      CLista appList = null;
      int indice;
      String elpais = "no";

      if (name2.equals("mBaja")) {
        indice = chcMBaja.getSelectedIndex();
        if (indice == 0) {
          // se borra la fecha de baja
          txtFecha.setText("");
        }
        else {
          // se a�ade la fecha actual
          Date d = new Date();
          // falta parsear la fecha
          txtFecha.setText(Fechas.date2String(d));
        }
      }
      else if (name2.equals("edad")) {
        strEdadBk = "";
        adaptee.txtFocusEdad();
      }
      else if (name2.equals("identifica")) {
        if (chcIdentificacion.getSelectedIndex() == 0) {
          txtIdentificacion.setText("");
        }
      }
      else {
        // para la checkbox del sexo
      }
      adaptee.desbloquea();

    }

  } //________________________________________________ END CLASS BusEnfermoTableAdapter

  /**
   *  Esta clase recoge el evento de modificar los valores del TextField
       *  cuando cambia el valor simplemente borra el TextField descriptivo asociado
   */
  class Pan_EnfermoTextAdapter
      extends java.awt.event.KeyAdapter {
    Pan_Enfermo adaptee = null;
    KeyEvent e = null;

    Pan_EnfermoTextAdapter(Pan_Enfermo adaptee) {
      this.adaptee = adaptee;
    }

    public void keyPressed(KeyEvent e) {
      if (adaptee.bloquea()) {
        this.e = e;
        // lanzamos el thread que tratar� el evento
        // new Thread(this).start();
        run();
      }
    }

    public void run() {
      // quitamos los valores de los textfield de descripcion
      String name2 = ( (Component) e.getSource()).getName();
      CLista data = new CLista();
      adaptee.bloquea();

      /// buscamos que bot�n es el que se ha pinchado y lo ejecutamos
      if (name2.equals("codigo")) {
        vaciarPantalla();
        modo = Pan_Enfermo.modoINICIO;
      }
      else if (name2.equals("nivel1")) {
        txtNivel1L.setText("");
        // borramos tambi�n el nivel2
        txtNivel2.setText("");
        txtNivel2L.setText("");
        txtZBS.setText("");
        txtZBSL.setText("");
      }
      else if (name2.equals("nivel2")) {
        txtNivel2L.setText("");
        // borramos tambi�n el nivel2
        txtZBS.setText("");
        txtZBSL.setText("");
      }
      else if (name2.equals("zbs")) {
        txtZBSL.setText("");
      }
      else if (name2.equals("edad")) {
        ///chcFcalc.setText("");
        ////////txtFecNacimiento.setText("");
        modo = modoNOFEC;
      }
      else if (name2.equals("fecNacimiento")) {
        txtEdad.setText("");
        chcFcalc.setText("");
        modo = modoFEC;
      }

      adaptee.desbloquea();
    }
  } //_________________________________________________ END CLASS BusEnfermoTextAdapter

} //__________________________________________________ END_CLASE PRINCIPAL

//listas de valores

class CListaBuscarNivel1
    extends CListaValores {

  public CListaBuscarNivel1(CApp a,
                            String title,
                            StubSrvBD stub,
                            String servlet,
                            int obtener_x_codigo,
                            int obtener_x_descricpcion,
                            int seleccion_x_codigo,
                            int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

// lista de valores
class CListaZBS2
    extends CListaValores {

  protected Pan_Enfermo panel;

  public CListaZBS2(Pan_Enfermo p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtNivel1.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

// lista de valores
class CListaZona
    extends CListaValores {

  protected Pan_Enfermo panel;

  public CListaZona(Pan_Enfermo p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS(s, "", "", panel.txtNivel1.getText(),
                       panel.txtNivel2.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS) o).getDes());
  }
}

/**
 *  esta clase se lanza como un thread para traer los datos
 */
class LeerChoices
    implements Runnable {
  protected Pan_Enfermo adaptee = null;
  /** para la comunicacion con el servlet */
  public StubSrvBD stubCliente = new StubSrvBD();

  /**
   *  se inicia la lista no puede ser null
   */
  public LeerChoices(Pan_Enfermo app) {
    adaptee = app;
  }

  public void run() {
    try {
      CLista parametros = null;
      CLista result = null;
      CLista laLista;

      parametros = new CLista();
      DataEnfermo d = new DataEnfermo("CD_USUARIO");
      d.setFiltro(adaptee.getApp().getLogin());
      parametros.addElement(d);
      laLista = Common.traerDatos(adaptee.getApp(), stubCliente,
                                  constantes.strSERVLET_ENFERMO,
                                  SrvEnfermo.modoDATOSCHOICES, parametros);

      //datoParcial
      /*result = (CLista) laLista.elementAt(0);
                 if ( result != null && result.size() > 0 ){
          DataEnfermo p = (DataEnfermo) result.firstElement();
          String si = (String) p.get("IT_FG_ENFERMO");
          if( si != null && si.equals("N") ){
             adaptee.setPermiso (true);
          }
                 }else{
          adaptee.setPermiso (false);
                 } */

      //para el documento
      result = (CLista) laLista.elementAt(1);
      adaptee.setListaId(result);

      //para el sexo
      result = (CLista) laLista.elementAt(2);
      adaptee.setListaSexo(result);

      //result = (CLista) laLista.elementAt(3);
      //adaptee.setListaPaises(result);

      //result = (CLista) laLista.elementAt(4);
      //adaptee.setListaCA(result);

      result = (CLista) laLista.elementAt(3);
      adaptee.setListaMBaja(result);

      stubCliente = null;
//// System_out.println("ya est�");
    }
    catch (Exception exc) {
      exc.printStackTrace(); //// System_out.println("Error : LeerChoices " + exc.toString());
    }
  }

} // ENd Class
