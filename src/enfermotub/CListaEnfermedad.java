package enfermotub;

import java.util.Enumeration;

import capp.CApp;
import capp.CListaValores;
import comun.DataEnfermo;
import sapp.StubSrvBD;

/**
 *  esta clase activa el servlet, y se enlaza con la clase CListaValores
 *  responsable de mostrar los valores y que el usuario seleccione los
 *  los valores pedidos
 */
public class CListaEnfermedad
    extends CListaValores {
  /** nombredel campo calve */
  String codigo = null;
  /** nombre del campo descripci�n */
  String descripcion = null;
  //Hash para guardar par�metros usuario (filtros adicionales)
  //No es el que se env�a
  public DataEnfermo hash = null;
  //________________________________________________________

  public CListaEnfermedad(CApp a,
                          String title,
                          StubSrvBD stub,
                          String servlet,
                          int obtener_x_codigo,
                          int obtener_x_descripcion,
                          int seleccion_x_codigo,
                          int seleccion_x_descripcion,
                          String a_codigo,
                          String a_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    codigo = a_codigo;
    descripcion = a_descripcion;

    //Por defecto campo filtro es el de c�digo. Luego usuario puedde cambiarlo
    hash = new DataEnfermo(a_codigo);

    //Nota Aqu� no se puede hacer llamada
    //al m�todo de b�squeda inicial  btnSearch_actionPerformed();
    //Antes de esto se deben fijar par�metros
  }

  //________________________________________________________

  /**
   *  a�ade un parametro a la lista
   *  por si el servlet necesita m�s de un parametro
   * M�todo debe ser llamado antes de hacer el show de la CListaEnfermedad
   */

  public void setParameter(String campo, String dato) {
    if (campo != null && dato != null && dato.trim().length() > 0) {
      hash.put(campo, dato);
    }
    else {
      hash.put(campo, codigo);
    }
  }

  //________________________________________________________

  /**
   *  Hace la primera b�squeda a b. datos llamando a m�todo de padre
   * M�todo debe ser llamado tras hacer los setParameter
   * y antes de hacer el show de la CListaEnfermedad
   */

  public void buscarInicial() {
    btnSearch_actionPerformed();
  }

  //________________________________________________________

  //M�todo llamado por clase padre cuando udusario pulsa buscar
  public Object setComponente(String miFiltro) {

    //_____________ LRIVERA___
    // Crea nuevo elemento de petici�n
    //y almacena el campo que sirve como filtro
    //Ese campo depende de la opci�n en check de lista valores
    DataEnfermo hashEnvio = (DataEnfermo) (hash.clone());

    if (optSearch1.getState()) {
      hashEnvio.setCampoFiltro(codigo);
    }
    else {
      hashEnvio.setCampoFiltro(descripcion);

      //Nota: Necesario tener, adem�s del filtro, el campo filtro
      //Ya que en servlet SrvGeneral, al guardar el �ltimo de una trama
      //se hace como:
      //     o =  ((DataEnfermo)v.lastElement()).get(campo_filtro);
      //          v.setFilter(o.toString());
      //Por tanto, si filtramos por descripci�n debemos reccoger
      // como filtro el get("DS_....")

      //Filtro: el String que meta el usuario en caja de texto
    }
    if (miFiltro != null) {
      hashEnvio.setFiltro(miFiltro);
    }
    //____Comprob claves____
    for (Enumeration enumer = hashEnvio.keys(); enumer.hasMoreElements(); ) {
      String miclave = (String) enumer.nextElement();
      // System_out.println("En doWork******Clave " + miclave + " con valor " + (String)( hashEnvio  .get(miclave) ) );
    }
    return hashEnvio;
  }

  //________________________________________________________

  // Recibe un DataEnfermo y obtiene su c�digo
  public String getCodigo(Object o) {
    return (String) ( (DataEnfermo) o).get(codigo);
  }

  //________________________________________________________

  // Recibe un DataEnfermo y obtiene su descripci�n
  public String getDescripcion(Object o) {
    return (String) ( (DataEnfermo) o).get(descripcion);
  }
} //____________________________________ END_CLASE}
