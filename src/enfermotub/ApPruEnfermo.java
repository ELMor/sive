package enfermotub;

import java.net.URL;
import java.util.ResourceBundle;

import capp.CApp;
import capp.CLista;
import capp.CMessage;
import catalogo.Catalogo;
//import listas.*;
import comun.DataEnfermo;
import comun.constantes;
import sapp.StubSrvBD;

public class ApPruEnfermo
    extends CApp {

  //constantes del panel
  public static final String imgLUPA = "images/Magnify.gif";
  ResourceBundle res;
  public static final String imgALTA = "images/alta.gif";
  public static final String imgMODIFICACION = "images/modificacion.gif";
  public static final String imgBORRAR = "images/baja.gif";
  public static final String strSERVLET_CAT = "servlet/SrvCat";
  public static final String strSERVLET_ENFERMO = "servlet/SrvEnfermo";
  public static final String strSERVLET_NIV2 = "servlet/SrvZBS2";
//  public static final String strSERVLET_ZBS = "servlet/SrvZBS";

  public static final int servletSELECCION_NIV2_X_CODIGO = 7;
  public static final int servletOBTENER_NIV2_X_CODIGO = 8;
  public static final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  public static final int servletOBTENER_NIV2_X_DESCRIPCION = 10;

  //modos de operaci�n de la ventana
  final int modoINICIO = 0;
  final int modoMODIFICAR = 1;
  final int modoSELECCION = 2;
  final int modoOBTENER = 4;
  final int modoESPERA = 5;

  // estos n�meros le indican a su respectivo servlet como actuar
  public static final int modoLIKE_NIV1 = 2 + Catalogo.catNIVEL1; // para el nivel1
  public static final int modoLIKE_NIV2 = 7; //para nivel2 el servlet
  public static final int modoBUSCAENFERMO = 0;
  public static final int modoDATOSENFERMO = 10;
  public static final int modoMUNICIPIO = 120;
  public static final int modoENFERMEDAD = 130;

  public ApPruEnfermo() {}

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("enfermotub.Res" + this.getIdioma());

    if (this.getParametro("COD_APLICACION").equals(constantes.APP_TUBERCULOSIS)) {
      setTitulo("Mantenimiento de Enfermos/Contactos");
    }
    else {
      setTitulo("Mantenimiento de Enfermos");
    }

    CApp a = (CApp)this;

    VerPanel("", new Pan_Enfermo(a));
  }

  /** borramos el panel y nos vamos   */
  public void adios() {
    try {
      getAppletContext().showDocument(new URL(getCodeBase(), "default.html"),
                                      "_self");
    }
    catch (Exception ex) {}

  }

  public void stop() {
  }

} //__________________________________________________ END_CLASS

/**
 *  esta clase se lanza como un thread para traer los datos
 */
class LeerDatos
    implements Runnable {
  CLista m_lista = null;
  String m_strServlet = null;
  int m_iCat = 0;
  ApPruEnfermo m_app = null;

  /**
   *  se inicia la lista no puede ser null
   */
  public LeerDatos(ApPruEnfermo app, CLista a_lista, String a_strServlet,
                   int a_iCat) {
    m_lista = a_lista;
    m_strServlet = a_strServlet;
    m_iCat = a_iCat;
    m_app = app;
  }

  public void run() {
    try {
      traer_datos();
    }
    catch (Exception ex) {
      ex.printStackTrace();
      CMessage msgBox = new CMessage(m_app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
    }

  }

  // hilo de ejecuci�n para ir trayendo datos
  public void traer_datos() throws Exception {
    CLista data = null;
    CLista result_lista = null;

    data = new CLista();
    DataEnfermo dEnfer = new DataEnfermo("h");
    data.addElement(dEnfer);
    ////# // System_out.println("Hemosmetido un DataEnfermo");

    // invoca al servlet
    ////# // System_out.println ( "Pedimos datos " +m_strServlet + m_iCat);
    StubSrvBD stub = new StubSrvBD(new URL(m_strServlet));
    result_lista = (CLista) stub.doPost(m_iCat, data);

    // insertamos los elementos en la lista
    for (int j = 0; j < result_lista.size(); j++) {
      m_lista.addElement(result_lista.elementAt(j));
    }
  }

} //______________________________________________  END_CLASE
