package enfermotub;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.ServletException;

import capp.CApp;
import capp.CLista;
import comun.Data;
import comun.DataEnfermo;
import comun.Fechas;
import comun.constantes;
// $$$ modo debug
import jdbcpool.JDCConnectionDriver;
import sapp.DBServlet;

/**
 *  Esta clase est� en el servidor recibe una CLista
 *  recoge de ella el nombre de la clase de datos y los par�metros
 *  ejecuta la sentencia SQL
 *  rellena una CLISTA con objetos de datos
 *  y finaliza la conexi�n
 */
public class SrvEnfermo
    extends SrvGeneral {

  /** hashtable que contiene los datos */
  Hashtable parameter = null;

  // Estados que corresponden a cada sentencia SQL
  // son los modos de operaci�n
  /** con los datos se buscan los enfermos que cumplan esas condiciones */
  public static final int modoBUSCAENFERMO = 0;
  /** se buscan todos los datos de un enfermo en concreto */
  private static final int modoDATOSENFERMO = constantes.modoDATOSENFERMO;

  /** seactualizan los datos del enfermo */
  public static final int modoMODIFICADATOS = 20;
  /** seactualizan los datos de las comunidades auton�micas */
  public static final int modoCCAA = 60;
  public static final int modoCCAA_TRAMERO = 65;
  /** seactualizan los datos de las comunidades auton�micas */
  public static final int modoPAISES = 70;
  /** seactualizan los datos de las comunidades auton�micas */
  public static final int modoPROVINCIAS = 80;
  /** seactualizan los datos de las comunidades auton�micas */
  public static final int modoTDOC = 90;
  /** seactualizan los datos de las comunidades auton�micas */
  public static final int modoSEXO = 100;
  /** seactualizan los datos de las comunidades auton�micas */
  public static final int modoMBAJA = 110;
  /** seactualizan los datos de las comunidades auton�micas */
  public static final int modoMUNICIPIO = 120;
  /** seactualizan los datos de las comunidades auton�micas */
  public static final int modoENFERMEDAD = 130;
  /** seactualizan los datos de las comunidades auton�micas */
//  public static final int modoZBS=140;
  /** seactualizan los datos de las comunidades auton�micas */
  public static final int modoNivel1 = 150;
  /** seactualizan los datos de las comunidades auton�micas */
  public static final int modoNivel2 = 160;
  /** seactualizan los datos de las comunidades auton�micas */
  public static final int modoE_NOTIF = 170;
  public static final int modoDENFER = 180;
  public static final int modoDENFERDATOS = 185;
  public static final int modoCOPIA = 190;
  public static final int modoPERMISO = 200;
  /**  das un municipio y recibes la descripci�n de los niveles de este */
  public static final int modoNIVELES_MUN = 210;

  //Trae tres listas con todos tipos doc. ,sexos, y motivos baja
  //( Para los  choices del panel enfermo que no son de localizaci�n geogr�fica)
  public static final int modoDATOSCHOICES = 300;

  public static final int modoBUSCAENFERMOTRAMERO = 310;

  /** se buscan todos los datos de un enfermo en concreto */
  private static final int modoDATOSENFERMOTRAMERO = constantes.
      modoDATOSENFERMOTRAMERO;

  // Para gestionar el borrado de enfermos de tuberculosis.
  public static final int modoCIERRA_CASO_TUBERCULOSIS = 400;

  /** campos de la base de datos */
  public static final String CD_ENFERMO = "CD_ENFERMO";
  public static final String CD_PROV = "CD_PROV";
  public static final String CD_PAIS = "CD_PAIS";
  public static final String CD_MUN = "CD_MUN";
  public static final String CD_TDOC = "CD_TDOC";
  public static final String DS_APE1 = "DS_APE1";
  public static final String DS_APE2 = "DS_APE2";
  public static final String DS_NOMBRE = "DS_NOMBRE";
  public static final String DS_FONOAPE1 = "DS_FONOAPE1";
  public static final String DS_FONOAPE2 = "DS_FONOAPE2";
  public static final String DS_FONONOMBRE = "DS_FONONOMBRE";

  public static final String DS_DATOS_ENFERMO = "SELECT CD_ENFERMO, CD_PAIS, CD_PROV, CD_MUN, CD_TDOC, DS_APE1 ,DS_APE2, DS_NOMBRE, DS_FONOAPE1 , DS_FONOAPE2, DS_FONONOMBRE, IT_CALC, FC_NAC , CD_SEXO, CD_POSTAL, DS_DIREC, DS_NUM, DS_PISO , DS_TELEF, CD_NIVEL_1, CD_NIVEL_2, CD_ZBS, CD_OPE, IT_REVISADO, CD_MOTBAJA, FC_BAJA, DS_OBSERV, CD_PROV2, CD_MUNI2, CD_POST2, DS_DIREC2, DS_NUM2, DS_PISO2, DS_OBSERV2, CD_PROV3, CD_MUNI3, CD_POST3, DS_DIREC3, DS_NUM3, DS_PISO3, DS_TELEF3, DS_OBSERV3, DS_NDOC, SIGLAS, DS_TELEF2, FC_ULTACT, CDTVIA,  DSCALNUM, CDTNUM, CDVIAL, IT_ENFERMO, IT_CONTACTO ";

  // Variables a�adidas para gestionar las situaciones Enfermo, Contacto y
  // Enfermo/Contacto.
  private static final String sTextoSoloEnfermos = " IT_ENFERMO = 'S' ";
  private static final String sTextoSoloContactos = " IT_CONTACTO = 'S' ";
  private static final String sTextoEnfermosContactos = "";

  private String sMetaModo = constantes.sEnfermosContactos;
  private String sTextoMetaModo = sTextoEnfermosContactos;
  // Se supone, mientras no se diga lo contrario, que
  // se van a recuperar todos los elementos.
  private int iOrdinalMetaModo = 0;

  // Variables a�adidas para gestionar la baja de un enfermo de tuberculosis.
  private boolean bBajaDefuncion = false;

  //Para modo ENFERMEDAD
  String query = "";
  String sDes = "";
  String sDesL = "";

  SimpleDateFormat formUltAct = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
      new Locale("es", "ES"));

  //para M� Luisa
  // se modifica en el modo modoMODIFICADATOS
  String fc_ultAct = "";

  public void SrvEnfermo() {
  }

  /** devuelve la clase de datos propia de este server */
  public Object getNewData() {
    return new DataEnfermo("");
  }

  //________________________________________________________________________

  protected CLista doWork(int opmode, CLista param) throws Exception {
    String descrip = null;
    Hashtable hash = null;
    CLista result = null;
    this.param = param;
    //Para contar los registros que se llevan introducidos
    int numRegistros = 1;
    sTextoMetaModo = ""; // La iniciamos, porque parece que falla (ARS 14-05-01)

    if (param == null) {
      // no nos han pasado datos iniciales
      return null;
    }
    DataEnfermo dat = (DataEnfermo) param.firstElement();

    // El n�mero de elementos de param es funci�n del modo de llamada.
    // El valor de sMetaModo, si procede, est� en el �ltimo.

    if (opmode == modoMODIFICADATOS) {
      iOrdinalMetaModo = 2;
    }
    else {
      iOrdinalMetaModo = 1;
    }
    if (param.size() > iOrdinalMetaModo) {
      sMetaModo = (String) param.elementAt(iOrdinalMetaModo);
      if (sMetaModo.equals(constantes.sSoloEnfermos)) {
        sTextoMetaModo = sTextoSoloEnfermos;
      }
      else if (sMetaModo.equals(constantes.sSoloContactos)) {
        sTextoMetaModo = sTextoSoloContactos;
      }
      else {
        sTextoMetaModo = sTextoEnfermosContactos;
      }
    }
//// System_out.println(param+"---"+sTextoMetaModo);
    // establece la conexi�n con la base de datos
    if (breal) {
      con = openConnection();
    }

    try {
      ///establecemos las propiedades indicadas
      //________________________________________________________________________
      if (opmode == modoMODIFICADATOS) {
        con.setAutoCommit(false);
      }
      else {
        con.setAutoCommit(true);
      }

      // borramos los errores
      strError = null;

      //________________________________________________________________________

      if (opmode == modoDATOSENFERMO) {
        result = realiza_SQL(opmode, param);
        if (result != null && result.size() > 0) {
          hash = (Hashtable) result.firstElement();
          // ahora le a�adimos las descripciones
          String strMun = (String) hash.get("CD_MUN");
          if (strMun != null) {
            descrip = traeDescripcion(
                "SELECT DS_MUN FROM  SIVE_MUNICIPIO WHERE ", "CD_MUN", "DS_MUN",
                strMun, "CD_PROV", (String) hash.get("CD_PROV"));
            if (descrip != null) {
              hash.put("DS_MUN", descrip);
            }
          }
          strMun = (String) hash.get("CD_MUNI2");
          if (strMun != null) {
            descrip = traeDescripcion(
                "SELECT DS_MUN FROM  SIVE_MUNICIPIO WHERE ", "CD_MUN", "DS_MUN",
                strMun, "CD_PROV", (String) hash.get("CD_PROV2"));
            if (descrip != null) {
              hash.put("DS_MUNI2", descrip);
            }
          }
          strMun = (String) hash.get("CD_MUNI3");
          if (strMun != null) {
            descrip = traeDescripcion(
                "SELECT DS_MUN FROM  SIVE_MUNICIPIO WHERE ", "CD_MUN", "DS_MUN",
                strMun, "CD_PROV", (String) hash.get("CD_PROV3"));
            if (descrip != null) {
              hash.put("DS_MUNI3", descrip);
            }
          }

          strMun = (String) hash.get("CD_SEXO");
          if (strMun != null) {
            //    descrip = traeDescripcion("SELECT DS_SEXO FROM  SIVE_SEXO WHERE " , "CD_SEXO", "DS_SEXO", strMun);
            descrip = traeDescripcionEnIdioma(
                "SELECT DS_SEXO,DSL_SEXO FROM  SIVE_SEXO WHERE ", "CD_SEXO",
                "DS_SEXO", "DSL_SEXO", strMun, param.getIdioma());
            if (descrip != null) {
              hash.put("DS_SEXO", descrip);

            }
          }

          strMun = (String) hash.get("CD_TDOC");
          if (strMun != null) {
            descrip = traeDescripcion(
                "SELECT DS_TIPODOC FROM SIVE_T_DOC WHERE ", "CD_TDOC",
                "DS_TIPODOC", strMun);
            if (descrip != null) {
              hash.put("DS_TIPODOC", descrip);

            }
          }

          //Se traen descripciones en el idioma oportuno
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_NIVEL_1,DSL_NIVEL_1 FROM  SIVE_NIVEL1_S WHERE ",
              "CD_NIVEL_1", "DS_NIVEL_1", "DSL_NIVEL_1", hash.get("CD_NIVEL_1"),
              param.getIdioma());
          if (descrip != null) {
            hash.put("DS_NIVEL_1", descrip);
          }
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_NIVEL_2,DSL_NIVEL_2 FROM  SIVE_NIVEL2_S WHERE ",
              "CD_NIVEL_2", "DS_NIVEL_2", "DSL_NIVEL_2", hash.get("CD_NIVEL_2"),
              param.getIdioma());
          if (descrip != null) {
            hash.put("DS_NIVEL_2", descrip);
          }
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_ZBS,DSL_ZBS FROM  SIVE_ZONA_BASICA WHERE ", "CD_ZBS",
              "DS_ZBS", "DSL_ZBS", hash.get("CD_ZBS"), param.getIdioma());
          if (descrip != null) {
            hash.put("DS_ZBS", descrip);

            //   descrip = traeDescripcion("SELECT DS_SEXO FROM  SIVE_SEXO WHERE " , "CD_SEXO", "DS_SEXO", hash.get("CD_SEXO"));
          }
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_SEXO,DSL_SEXO FROM  SIVE_SEXO WHERE ", "CD_SEXO",
              "DS_SEXO", "DSL_SEXO", strMun, param.getIdioma());
          if (descrip != null) {
            hash.put("DS_SEXO", descrip);

            /// provincia
          }
          StringBuffer codCA = new StringBuffer();
          descrip = traeDescripcionCA(
              "SELECT CD_CA, DS_PROV FROM SIVE_PROVINCIA WHERE ", "CD_PROV",
              "DS_PROV", hash.get("CD_PROV"), "CD_CA", codCA);
          if (codCA.length() > 0) {
            hash.put("CD_CA", codCA.toString());
          }
          if (descrip != null) {
            hash.put("DS_PROV", descrip);
          }
          descrip = traeDescripcion("SELECT DS_CA FROM SIVE_COM_AUT WHERE ",
                                    "CD_CA", "DS_CA", codCA.toString());
          if (descrip != null) {
            hash.put("DS_CA", descrip);

            /// provincia 2
          }
          codCA = new StringBuffer();
          descrip = traeDescripcionCA(
              "SELECT CD_CA, DS_PROV FROM SIVE_PROVINCIA WHERE ", "CD_PROV",
              "DS_PROV", hash.get("CD_PROV2"), "CD_CA", codCA);
          if (codCA.length() > 0) {
            hash.put("CD_CA2", codCA.toString());
          }
          if (descrip != null) {
            hash.put("DS_PROV2", descrip);
          }
          descrip = traeDescripcion("SELECT DS_CA FROM SIVE_COM_AUT WHERE ",
                                    "CD_CA", "DS_CA", codCA.toString());
          if (descrip != null) {
            hash.put("DS_CA2", descrip);
            /// provincia 3
          }
          codCA = new StringBuffer();
          descrip = traeDescripcionCA(
              "SELECT CD_CA, DS_PROV FROM SIVE_PROVINCIA WHERE ", "CD_PROV",
              "DS_PROV", hash.get("CD_PROV3"), "CD_CA", codCA);
          if (codCA.length() > 0) {
            hash.put("CD_CA3", codCA.toString());
          }
          if (descrip != null) {
            hash.put("DS_PROV3", descrip);
          }
          descrip = traeDescripcion("SELECT DS_CA FROM SIVE_COM_AUT WHERE ",
                                    "CD_CA", "DS_CA", codCA.toString());
          if (descrip != null) {
            hash.put("DS_CA3", descrip); //LRivera**** Pon�a un 2 (cambiado)
          }
        }
        //________________________________________________________________________

      }
      else if (opmode == modoDATOSENFERMOTRAMERO) {

        result = realiza_SQL(opmode, param);

        if (result != null && result.size() > 0) {

          hash = (Hashtable) result.firstElement();
          // ahora le a�adimos las descripcion
          String strMun = (String) hash.get("CD_MUN");
          if (strMun != null) {
            descrip = traeDescripcion(
                "SELECT DSMUNI FROM  SUCA_MUNICIPIO WHERE ", "CDMUNI", "DSMUNI",
                strMun, "CDPROV", (String) hash.get("CD_PROV"));
            if (descrip != null) {
              hash.put("DS_MUN", descrip);
            }
          }
          strMun = (String) hash.get("CD_MUNI2");
          if (strMun != null) {
            descrip = traeDescripcion(
                "SELECT DSMUNI FROM  SUCA_MUNICIPIO WHERE ", "CDMUNI", "DSMUNI",
                strMun, "CDPROV", (String) hash.get("CD_PROV2"));
            if (descrip != null) {
              hash.put("DS_MUNI2", descrip);
            }
          }
          strMun = (String) hash.get("CD_MUNI3");
          if (strMun != null) {
            descrip = traeDescripcion(
                "SELECT DSMUNI FROM  SUCA_MUNICIPIO WHERE ", "CDMUNI", "DSMUNI",
                strMun, "CDPROV", (String) hash.get("CD_PROV3"));
            if (descrip != null) {
              hash.put("DS_MUNI3", descrip);
            }
          }

          strMun = (String) hash.get("CD_SEXO");
          if (strMun != null) {
            //    descrip = traeDescripcion("SELECT DS_SEXO FROM  SIVE_SEXO WHERE " , "CD_SEXO", "DS_SEXO", strMun);
            descrip = traeDescripcionEnIdioma(
                "SELECT DS_SEXO,DSL_SEXO FROM  SIVE_SEXO WHERE ", "CD_SEXO",
                "DS_SEXO", "DSL_SEXO", strMun, param.getIdioma());
            if (descrip != null) {
              hash.put("DS_SEXO", descrip);
            }
          }

          strMun = (String) hash.get("CD_TDOC");
          if (strMun != null) {
            descrip = traeDescripcion(
                "SELECT DS_TIPODOC FROM SIVE_T_DOC WHERE ", "CD_TDOC",
                "DS_TIPODOC", strMun);
            if (descrip != null) {
              hash.put("DS_TIPODOC", descrip);
            }
          }
          //Se traen descripciones en el idioma oportuno
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_NIVEL_1,DSL_NIVEL_1 FROM  SIVE_NIVEL1_S WHERE ",
              "CD_NIVEL_1", "DS_NIVEL_1", "DSL_NIVEL_1", hash.get("CD_NIVEL_1"),
              param.getIdioma());
          if (descrip != null) {
            hash.put("DS_NIVEL_1", descrip);
          }
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_NIVEL_2,DSL_NIVEL_2 FROM  SIVE_NIVEL2_S WHERE ",
              "CD_NIVEL_2", "DS_NIVEL_2", "DSL_NIVEL_2", hash.get("CD_NIVEL_2"),
              param.getIdioma());
          if (descrip != null) {
            hash.put("DS_NIVEL_2", descrip);
          }
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_ZBS,DSL_ZBS FROM  SIVE_ZONA_BASICA WHERE ", "CD_ZBS",
              "DS_ZBS", "DSL_ZBS", hash.get("CD_ZBS"), param.getIdioma());
          if (descrip != null) {
            hash.put("DS_ZBS", descrip);

//              descrip = traeDescripcion("SELECT DS_SEXO FROM  SIVE_SEXO WHERE " , "CD_SEXO", "DS_SEXO", hash.get("CD_SEXO"));
          }
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_SEXO,DSL_SEXO FROM  SIVE_SEXO WHERE ", "CD_SEXO",
              "DS_SEXO", "DSL_SEXO", strMun, param.getIdioma());
          if (descrip != null) {
            hash.put("DS_SEXO", descrip);

            /// provincia
          }
          StringBuffer codCA = new StringBuffer();
          descrip = traeDescripcionCA(
              "SELECT CDCOMU, DSPROV FROM SUCA_PROVINCIA WHERE ", "CDPROV",
              "DSPROV", hash.get("CD_PROV"), "CDCOMU", codCA);
          if (codCA.length() > 0) {
            hash.put("CD_CA", codCA.toString());
          }
          if (descrip != null) {
            hash.put("DS_PROV", descrip);

          }
          descrip = traeDescripcion("SELECT DSCOMU FROM SUCA_AUTONOMIAS WHERE ",
                                    "CDCOMU", "DSCOMU", codCA.toString());
          if (descrip != null) {
            hash.put("DS_CA", descrip);
            /// provincia 2
          }
          codCA = new StringBuffer();
          descrip = traeDescripcionCA(
              "SELECT CDCOMU, DSPROV FROM SUCA_PROVINCIA WHERE ", "CDPROV",
              "DSPROV", hash.get("CD_PROV2"), "CDCOMU", codCA);
          if (codCA.length() > 0) {
            hash.put("CD_CA2", codCA.toString());
          }
          if (descrip != null) {
            hash.put("DS_PROV2", descrip);
          }
          descrip = traeDescripcion("SELECT DSCOMU FROM SUCA_AUTONOMIAS WHERE ",
                                    "CDCOMU", "DSCOMU", codCA.toString());
          if (descrip != null) {
            hash.put("DS_CA2", descrip);
            /// provincia 3
          }
          codCA = new StringBuffer();
          descrip = traeDescripcionCA(
              "SELECT CDCOMU, DSPROV FROM SUCA_PROVINCIA WHERE ", "CDPROV",
              "DSPROV", hash.get("CD_PROV3"), "CDCOMU", codCA);
          if (codCA.length() > 0) {
            hash.put("CD_CA3", codCA.toString());
          }
          if (descrip != null) {
            hash.put("DS_PROV3", descrip);
          }
          descrip = traeDescripcion("SELECT DSCOMU FROM SUCA_AUTONOMIAS WHERE ",
                                    "CDCOMU", "DSCOMU", codCA.toString());
          if (descrip != null) {
            hash.put("DS_CA2", descrip);

          }
        }

        //________________________________________________________________________

      }
      else if (opmode == modoMODIFICADATOS) {

        result = realiza_SQL(opmode, param); // UPDATE
        //grabamos los datos anteriores
        CLista da = realiza_SQL(modoCOPIA, param); // INSERT

        if (bBajaDefuncion) {
          da = null;
          da = realiza_SQL(modoCIERRA_CASO_TUBERCULOSIS, param); // UPDATE
          // y s�lo en caso de defunci�n.
        }

        if (da == null) {
          con.rollback();
        }
        else {
          con.commit();
        }
        //Devuelvo la fc_ultact
        hash = new Hashtable();
        hash.put("FC_ULTACT", fc_ultAct);
        result.addElement(hash);
        //________________________________________________________________________

      }
      else if (opmode == modoBUSCAENFERMO) {
        //  nos traemos todos los enfermos que tienen municipio
        result = realiza_SQL(opmode, param);

        //Si nos han pedido enfermos de una enfermedad
        if (dat.get("b.CD_ENFCIE") != null) {
          String strCD_ENFCIE = (String) dat.get("b.CD_ENFCIE");
//             String strDS_PROCESO = traeDescripcion("SELECT DS_PROCESO FROM  SIVE_PROCESOS WHERE " , "CD_ENFCIE", "DS_PROCESO", dat.get("b.CD_ENFCIE"));
          String strDS_PROCESO = traeDescripcionEnIdioma(
              "SELECT DS_PROCESO,DSL_PROCESO FROM  SIVE_PROCESOS WHERE ",
              "CD_ENFCIE", "DS_PROCESO", "DSL_PROCESO", dat.get("b.CD_ENFCIE"),
              param.getIdioma());
          if (strCD_ENFCIE == null) {
            strCD_ENFCIE = " ";
          }
          // Buscamos la descripci�n de municipio y rellenamos enfermedad y municipio en cada enfermo
          for (Enumeration e = result.elements(); e.hasMoreElements(); ) {
            hash = (Hashtable) e.nextElement();
            descrip = traeDescripcion(
                "SELECT DS_MUN FROM  SIVE_MUNICIPIO WHERE ", "CD_MUN", "DS_MUN",
                hash.get("CD_MUN"), "CD_PROV", (String) hash.get("CD_PROV"));
            if (descrip != null) {
              hash.put("DS_MUN", descrip);
            }
            hash.put("CD_ENFCIE", strCD_ENFCIE);
            hash.put("DS_PROCESO", strDS_PROCESO);
          }
        }

        else {
          // rellenamos los enfermos con todas sus enfermedades
          result = rellenarEnfermedades(result);
          for (Enumeration e = result.elements(); e.hasMoreElements(); ) {
            hash = (Hashtable) e.nextElement();
            descrip = traeDescripcion(
                "SELECT DS_MUN FROM  SIVE_MUNICIPIO WHERE ", "CD_MUN", "DS_MUN",
                hash.get("CD_MUN"), "CD_PROV", (String) hash.get("CD_PROV"));
            if (descrip != null) {
              hash.put("DS_MUN", descrip);
            }
            descrip = traeDescripcionEnIdioma(
                "SELECT DS_PROCESO,DSL_PROCESO FROM  SIVE_PROCESOS WHERE ",
                "CD_ENFCIE", "DS_PROCESO", "DSL_PROCESO", hash.get("CD_ENFCIE"),
                param.getIdioma());
            if (descrip != null) {
              hash.put("DS_PROCESO", descrip);
            }
          }

        } //else

        //________________________________________________________________________

      }
      else if (opmode == modoBUSCAENFERMOTRAMERO) {

        //  nos traemos todos los enfermos que tienen municpio
        result = realiza_SQL(opmode, param);

        //Si nos han pedido enfermos de una enfermedad
        if (dat.get("b.CD_ENFCIE") != null) {
          String strCD_ENFCIE = (String) dat.get("b.CD_ENFCIE");
//             String strDS_PROCESO = traeDescripcion("SELECT DS_PROCESO FROM  SIVE_PROCESOS WHERE " , "CD_ENFCIE", "DS_PROCESO", dat.get("b.CD_ENFCIE"));
          String strDS_PROCESO = traeDescripcionEnIdioma(
              "SELECT DS_PROCESO,DSL_PROCESO FROM  SIVE_PROCESOS WHERE ",
              "CD_ENFCIE", "DS_PROCESO", "DSL_PROCESO", dat.get("b.CD_ENFCIE"),
              param.getIdioma());
          if (strCD_ENFCIE == null) {
            strCD_ENFCIE = " ";
          }
          // rellenamosla descripci�n de municipio
          for (Enumeration e = result.elements(); e.hasMoreElements(); ) {
            hash = (Hashtable) e.nextElement();
            descrip = traeDescripcion(
                "SELECT DSMUNI FROM  SUCA_MUNICIPIO WHERE ", "CDMUNI", "DSMUNI",
                hash.get("CD_MUN"), "CDPROV", (String) hash.get("CD_PROV"));
            if (descrip != null) {
              hash.put("DS_MUN", descrip);
            }
            hash.put("CD_ENFCIE", strCD_ENFCIE);
            hash.put("DS_PROCESO", strDS_PROCESO);
          }
        }
        else {
          // rellenamos los enfermos con todas sus enfermedades
          result = rellenarEnfermedades(result);
          for (Enumeration e = result.elements(); e.hasMoreElements(); ) {
            hash = (Hashtable) e.nextElement();
            descrip = traeDescripcion(
                "SELECT DSMUNI FROM  SUCA_MUNICIPIO WHERE ", "CDMUNI", "DSMUNI",
                hash.get("CD_MUN"), "CDPROV", (String) hash.get("CD_PROV"));
            if (descrip != null) {
              hash.put("DS_MUN", descrip);
              //    descrip = traeDescripcion("SELECT DS_PROCESO FROM  SIVE_PROCESOS WHERE " , "CD_ENFCIE", "DS_PROCESO", hash.get("CD_ENFCIE"));
            }
            descrip = traeDescripcionEnIdioma(
                "SELECT DS_PROCESO,DSL_PROCESO FROM  SIVE_PROCESOS WHERE ",
                "CD_ENFCIE", "DS_PROCESO", "DSL_PROCESO", hash.get("CD_ENFCIE"),
                param.getIdioma());
            if (descrip != null) {
              hash.put("DS_PROCESO", descrip);
            }
          }

        }
        //________________________________________________________________________

      }
      else if (opmode == modoDENFERDATOS) {
        //  nos traemos todos los enfermos que tienen municpio
        result = realiza_SQL(modoDENFER, param);
        String strMun = null;
        //  rellenamos la lista con la descripci�n de los municipios
        for (Enumeration e = result.elements(); e.hasMoreElements(); ) {
          hash = (Hashtable) e.nextElement();
          strMun = (String) hash.get("CD_MUN");
          if (strMun != null) {
            descrip = traeDescripcion(
                "SELECT DS_MUN FROM  SIVE_MUNICIPIO WHERE ", "CD_MUN", "DS_MUN",
                strMun, "CD_PROV", (String) hash.get("CD_PROV"));
            if (descrip != null) {
              hash.put("DS_MUN", descrip);
            }
          }
          strMun = (String) hash.get("CD_MUNI2");
          if (strMun != null) {
            descrip = traeDescripcion(
                "SELECT DS_MUN FROM  SIVE_MUNICIPIO WHERE ", "CD_MUN", "DS_MUN",
                strMun, "CD_PROV", (String) hash.get("CD_PROV2"));
            if (descrip != null) {
              hash.put("DS_MUNI2", descrip);
            }
          }
          strMun = (String) hash.get("CD_MUNI3");
          if (strMun != null) {
            descrip = traeDescripcion(
                "SELECT DS_MUN FROM  SIVE_MUNICIPIO WHERE ", "CD_MUN", "DS_MUN",
                strMun, "CD_PROV", (String) hash.get("CD_PROV3"));
            if (descrip != null) {
              hash.put("DS_MUNI3", descrip);
            }
          }
          strMun = (String) hash.get("CD_SEXO");
          if (strMun != null) {
            //   descrip = traeDescripcion("SELECT DS_SEXO FROM  SIVE_SEXO WHERE " , "CD_SEXO", "DS_SEXO", strMun);
            descrip = traeDescripcionEnIdioma(
                "SELECT DS_SEXO,DSL_SEXO FROM  SIVE_SEXO WHERE ", "CD_SEXO",
                "DS_SEXO", "DSL_SEXO", strMun, param.getIdioma());
            if (descrip != null) {
              hash.put("DS_SEXO", descrip);

            }
          }

          strMun = (String) hash.get("CD_TDOC");
          if (strMun != null) {
            descrip = traeDescripcion(
                "SELECT DS_TIPODOC FROM SIVE_T_DOC WHERE ", "CD_TDOC",
                "DS_TIPODOC", strMun);
            if (descrip != null) {
              hash.put("DS_TIPODOC", descrip);
            }
          }

          //Se traen descripciones en el idioma oportuno
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_NIVEL_1,DSL_NIVEL_1 FROM  SIVE_NIVEL1_S WHERE ",
              "CD_NIVEL_1", "DS_NIVEL_1", "DSL_NIVEL_1", hash.get("CD_NIVEL_1"),
              param.getIdioma());
          if (descrip != null) {
            hash.put("DS_NIVEL_1", descrip);
          }
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_NIVEL_2,DSL_NIVEL_2 FROM  SIVE_NIVEL2_S WHERE ",
              "CD_NIVEL_2", "DS_NIVEL_2", "DSL_NIVEL_2", hash.get("CD_NIVEL_2"),
              param.getIdioma());
          if (descrip != null) {
            hash.put("DS_NIVEL_2", descrip);
          }
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_ZBS,DSL_ZBS FROM  SIVE_ZONA_BASICA WHERE ", "CD_ZBS",
              "DS_ZBS", "DSL_ZBS", hash.get("CD_ZBS"), param.getIdioma());
          if (descrip != null) {
            hash.put("DS_ZBS", descrip);

            /// provincia
          }
          StringBuffer codCA = new StringBuffer();
          descrip = traeDescripcionCA(
              "SELECT CD_CA, DS_PROV FROM SIVE_PROVINCIA WHERE ", "CD_PROV",
              "DS_PROV", hash.get("CD_PROV"), "CD_CA", codCA);
          if (codCA.length() > 0) {
            hash.put("CD_CA", codCA.toString());
          }
          if (descrip != null) {
            hash.put("DS_PROV", descrip);
          }
          descrip = traeDescripcion("SELECT DS_CA FROM SIVE_COM_AUT WHERE ",
                                    "CD_CA", "DS_CA", codCA.toString());
          if (descrip != null) {
            hash.put("DS_CA", descrip);
            /// provincia 2
          }
          codCA = new StringBuffer();
          descrip = traeDescripcionCA(
              "SELECT CD_CA, DS_PROV FROM SIVE_PROVINCIA WHERE ", "CD_PROV",
              "DS_PROV", hash.get("CD_PROV2"), "CD_CA", codCA);
          if (codCA.length() > 0) {
            hash.put("CD_CA2", codCA.toString());
          }
          if (descrip != null) {
            hash.put("DS_PROV2", descrip);
          }
          descrip = traeDescripcion("SELECT DS_CA FROM SIVE_COM_AUT WHERE ",
                                    "CD_CA", "DS_CA", codCA.toString());
          if (descrip != null) {
            hash.put("DS_CA2", descrip);
            /// provincia 3
          }
          codCA = new StringBuffer();
          descrip = traeDescripcionCA(
              "SELECT CD_CA, DS_PROV FROM SIVE_PROVINCIA WHERE ", "CD_PROV",
              "DS_PROV", hash.get("CD_PROV3"), "CD_CA", codCA);
          if (codCA.length() > 0) {
            hash.put("CD_CA3", codCA.toString());
          }
          if (descrip != null) {
            hash.put("DS_PROV3", descrip);
          }
          descrip = traeDescripcion("SELECT DS_CA FROM SIVE_COM_AUT WHERE ",
                                    "CD_CA", "DS_CA", codCA.toString());
          if (descrip != null) {
            hash.put("DS_CA3", descrip);
          }
        } // orf
      }

      //________________________________________________________________________

      else if (opmode == modoDATOSCHOICES) {
        /// traemos todos los choices
        CLista datoParcial;
        result = new CLista();
        CLista parametros = new CLista();
        DataEnfermo d = new DataEnfermo("CD_ENFERMO");
        parametros.addElement(d);

        datoParcial = null; // E realiza_SQL(modoPERMISO, param);
        result.addElement(datoParcial);
        result.addElement(getDataEnfermo("CD_TDOC", "DS_TIPODOC", "DSL_TIPODOC",
                                         "SIVE_T_DOC"));
        result.addElement(getDataEnfermo("CD_SEXO", "DS_SEXO", "DSL_SEXO",
                                         "SIVE_SEXO"));
//       result.addElement(getDataEnfermo("CD_PAIS", "DS_PAIS", "SIVE_PAISES"));
//       result.addElement(getDataEnfermo("CD_CA", "DS_CA", "DSL_CA", "SIVE_COM_AUT"));
        result.addElement(getDataEnfermo("CD_MOTBAJA", "DS_MOTBAJA",
                                         "DSL_MOTBAJA", "SIVE_MOTIVO_BAJA"));
      }
      //________________________________________________________________________

      else if (opmode == modoNIVELES_MUN) {
        if (param != null && param.size() > 0) {
          DataEnfermo denfermo = (DataEnfermo) param.firstElement();

          //Se traen descripciones en el idioma oportuno
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_NIVEL_1,DSL_NIVEL_1 FROM  SIVE_NIVEL1_S WHERE ",
              "CD_NIVEL_1", "DS_NIVEL_1", "DSL_NIVEL_1",
              denfermo.get("CD_NIVEL_1"), param.getIdioma());
          if (descrip != null) {
            denfermo.put("DS_NIVEL_1", descrip);
          }
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_NIVEL_2,DSL_NIVEL_2 FROM  SIVE_NIVEL2_S WHERE ",
              "CD_NIVEL_2", "DS_NIVEL_2", "DSL_NIVEL_2",
              denfermo.get("CD_NIVEL_2"), param.getIdioma());
          if (descrip != null) {
            denfermo.put("DS_NIVEL_2", descrip);
          }
          descrip = traeDescripcionEnIdioma(
              "SELECT DS_ZBS,DSL_ZBS FROM  SIVE_ZONA_BASICA WHERE ", "CD_ZBS",
              "DS_ZBS", "DSL_ZBS", denfermo.get("CD_ZBS"), param.getIdioma());
          if (descrip != null) {
            denfermo.put("DS_ZBS", descrip);

          }
          result = param;
        }
      }
      //________________________________________________________________________

      else if ( (opmode == modoENFERMEDAD + SrvGeneral.servletOBTENER_X_CODIGO) ||
               (opmode ==
                modoENFERMEDAD + SrvGeneral.servletOBTENER_X_DESCRIPCION)) {

        // prepara la query
        if (opmode == modoENFERMEDAD + SrvGeneral.servletOBTENER_X_CODIGO) {
          query = "select a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO from SIVE_ENFEREDO a, SIVE_PROCESOS b where a.CD_ENFCIE = b.CD_ENFCIE and a.CD_TVIGI in ('I','A') and a.CD_ENFCIE = ?";
        }
        else {
          query = "select a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO from SIVE_ENFEREDO a, SIVE_PROCESOS b where a.CD_ENFCIE = b.CD_ENFCIE and a.CD_TVIGI in ('I','A') and DS_PROCESO = ?";

        }
        result = new CLista();

        // lanza la query
        st = con.prepareStatement(query);

        //En el filtro puede ir el c�d o la desc
        //dat es el elemnto de lista de  petici�n
        st.setString(1, ( (String) (dat.getFiltro())).trim());
        rs = st.executeQuery();

        // extrae el registro encontrado
        while (rs.next()) {

          sDes = rs.getString("DS_PROCESO");
          sDesL = rs.getString("DSL_PROCESO");

          // obtiene la descripcion principal en funci�n del idioma si el cliente no es un mantenimiento
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)) {
            if (sDesL != null) {
              sDes = sDesL;
            }
          }
          // a�ade un nodo
          DataEnfermo elemento = new DataEnfermo(" ");
          elemento.put("CD_ENFCIE", rs.getString("CD_ENFCIE"));
          elemento.put("DS_PROCESO", sDes);

          result.addElement(elemento);

        } //while

        rs.close();
        st.close();

        if (result != null) {
          result.trimToSize();
        }

      }

      else if ( (opmode ==
                 modoENFERMEDAD + SrvGeneral.servletSELECCION_X_CODIGO) ||
               (opmode ==
                modoENFERMEDAD + SrvGeneral.servletSELECCION_X_DESCRIPCION)) {

        // peticion de trama
        if (param.getFilter().length() > 0) {

          // prepara la query
          if (opmode == modoENFERMEDAD + SrvGeneral.servletSELECCION_X_CODIGO) {
            query = "select a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO from SIVE_ENFEREDO a, SIVE_PROCESOS b where a.CD_ENFCIE = b.CD_ENFCIE and a.CD_TVIGI in ('I','A') and a.CD_ENFCIE like ?  and a.CD_ENFCIE > ? order by CD_ENFCIE";
          }
          else {
            query = "select a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO from SIVE_ENFEREDO a, SIVE_PROCESOS b where a.CD_ENFCIE = b.CD_ENFCIE and a.CD_TVIGI in ('I','A') and DS_PROCESO like ?  and DS_PROCESO > ? order by DS_PROCESO";
          }
        }
        else {
          // prepara la query
          if (opmode == modoENFERMEDAD + SrvGeneral.servletSELECCION_X_CODIGO) {
            query = "select a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO from SIVE_ENFEREDO a, SIVE_PROCESOS b where a.CD_ENFCIE = b.CD_ENFCIE and a.CD_TVIGI in ('I','A') and a.CD_ENFCIE like ? order by CD_ENFCIE";
          }
          else {
            query = "select a.CD_ENFCIE, b.DS_PROCESO, b.DSL_PROCESO from SIVE_ENFEREDO a, SIVE_PROCESOS b where a.CD_ENFCIE = b.CD_ENFCIE and a.CD_TVIGI in ('I','A') and DS_PROCESO like ? order by DS_PROCESO";
          }
        }

        result = new CLista();

        // lanza la query
        st = con.prepareStatement(query);

        //En el filtro puede ir el c�d o la desc
        //dat es el elemnto de lista de  petici�n
        st.setString(1, ( (String) (dat.getFiltro())).trim() + "%");
        // paginaci�n. Viene en la CLista el �ltimo cod o des
        if (param.getFilter().length() > 0) {
          st.setString(2, param.getFilter().trim());
        }

        rs = st.executeQuery();

        // extrae el registro encontrado
        while (rs.next()) {

          // control de tama�o
          if (numRegistros > DBServlet.maxSIZE) {
            result.setState(CLista.listaINCOMPLETA);
            if (opmode == modoENFERMEDAD + SrvGeneral.servletSELECCION_X_CODIGO) {
              result.setFilter( (String) ( ( (DataEnfermo) result.lastElement()).
                                          get("CD_ENFCIE")));
            }
            else {
              result.setFilter( (String) ( ( (DataEnfermo) result.lastElement()).
                                          get("DS_PROCESO")));
            }
            break;
          }

          // control de estado
          if (result.getState() == CLista.listaVACIA) {
            result.setState(CLista.listaLLENA);
          }

          sDes = rs.getString("DS_PROCESO");
          sDesL = rs.getString("DSL_PROCESO");

          // obtiene la descripcion principal en funci�n del idioma si el cliente no es un mantenimiento
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)) {
            if (sDesL != null) {
              sDes = sDesL;
            }
          }
          // a�ade un nodo
          DataEnfermo elemento = new DataEnfermo(" ");
          elemento.put("CD_ENFCIE", rs.getString("CD_ENFCIE"));
          elemento.put("DS_PROCESO", sDes);

          result.addElement(elemento);

          numRegistros++;

        } //while

        rs.close();
        st.close();

        if (result != null) {
          result.trimToSize();
        }

      }

      else {
        //En los casos m�s sencillos se usan m�todos del padre SRvGeneral
        //realiza_sQL (en SrvGeneral) llama a prepareSQLSentencia ( aqu�, en SrvEnfrmo)
        //prepareSQLSentencia,implementado en cada hijo, fija la sentencia SQL y como se deben leer
        //los par�metros
        //Para ello, prepareSQLSentencia se puede apoyar, si es necesario, en los m�todos del SrvGeneral
        //  ponFiltro(...  ) para fijar parte de la sent SQL  y  prepareFiltro(...   ) ,
        // para ajustar los par�metros. Estos m�todos son llamados con par�metros que indican si
        // hay where o no, si se usa like etc.
        //Finalmente, el padre recoge los datos

        result = realiza_SQL(opmode, param);
      }

    }
    catch (Exception exc) {
      result = null;
      strError = exc.toString();

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    if (strError != null) {
      throw new ServletException(strError);
    }

    return result;
  }

  //_______________________________________________________________
  //_______________________________________________________________

  /* public String obtieneMAX(){
     String result = "0";
     //nos traemos todas las descripciones de Municipios
     String query = "select MAX(NM_MOVENF) from SIVE_MOV_ENFERMO ";
     try {
         // lanza la query
         st = con.prepareStatement(query);
         rs = st.executeQuery();
         if (rs.next()) {
              int d = rs.getInt(1);
              d++;
              result =  (new Integer(d)).toString();
              trazaLog("Contador de movimientos: " + String.valueOf(d));   // E
         }
         rs.close();
         st.close();
         rs = null;
         st = null;
         return result;
     }catch (Exception exc){
        return "1";
     }
   }
   */
  //_______________________________________________________________
  /**
   *  devuelve la sentencia SQL seg�n el modo indicado
   *  @param a_i_modo  indica el modo en que est� el di�logo
   *  @param a_data es el tipo de dato que tiene los par�metros
   *  @param resultSQL es la sentencia SQL a ejecutar
   *  @return devuelve el n�mero de registros que se deben de traer
   */
  public PreparedStatement prepareSQLSentencia(int a_imodo, Object a_data,
                                               StringBuffer resultSQL,
                                               StringBuffer numEltos,
                                               Connection con) {

    int iNumReg = DBServlet.maxSIZE;

    try {
      switch (a_imodo) {

        //Casos en los que se trama (se traen como m�ximo DBServlet.maxSIZE registros)
        //iNumReg tiene ese valor
        case modoBUSCAENFERMO:
          st = getSQLBuscaEnfermo(resultSQL);
          break;
        case modoDATOSENFERMO:
          st = getSQLDatosEnfermo(resultSQL);
          break;
        case modoBUSCAENFERMOTRAMERO:
          st = getSQLBuscaEnfermo(resultSQL);
          break;
        case modoDATOSENFERMOTRAMERO:
          st = getSQLDatosEnfermo(resultSQL);
          break;
        case modoMODIFICADATOS:
          Vector vec = getSQLModificaEnfermo(resultSQL);
          st = (PreparedStatement) vec.elementAt(0);
          fc_ultAct = (String) vec.elementAt(1);
          iNumReg = 0;
          break;
        case modoCOPIA:
          st = getSQLInsertEnfermo(resultSQL);
          iNumReg = 0;
          break;
        case modoCIERRA_CASO_TUBERCULOSIS:
          st = getSQLCierraCasoTBC(resultSQL);
          iNumReg = 0;
          break;
        case modoDENFER:

          // trae todos los datos del enfermo. poniendo los campos en el filtrado
          Data parameter = (Data) (param.firstElement());
          resultSQL.append(DS_DATOS_ENFERMO + " FROM SIVE_ENFERMO " +
                           ponFiltro("CD_ENFERMO", true, true, "CD_ENFERMO", false));
          resultSQL.append(" AND FC_BAJA IS NULL ");
          resultSQL.append(sTextoMetaModo.equals("") ? "" :
                           " AND " + sTextoMetaModo);
          st = con.prepareStatement(resultSQL.toString());
          prepareFiltro("CD_ENFERMO", true, true, "CD_ENFERMO", true);
          break;
          //Equipos notificadores
        case modoE_NOTIF:
        case modoE_NOTIF + SrvGeneral.servletOBTENER_X_CODIGO:

          // ejemplo que trae todas las ZBS
          resultSQL.append("SELECT CD_E_NOTIF, DS_E_NOTIF FROM SIVE_E_NOTIF " +
                           ponFiltro("CD_E_NOTIF", false, true, "CD_E_NOTIF", true));
          st = con.prepareStatement(resultSQL.toString());
          prepareFiltro("CD_E_NOTIF", false, true, "CD_E_NOTIF", true);
          break;
        case modoE_NOTIF + SrvGeneral.servletOBTENER_X_DESCRIPCION: // = DESC

          // ejemplo que trae todas las ZBS
          resultSQL.append("SELECT CD_E_NOTIF, DS_E_NOTIF FROM SIVE_E_NOTIF " +
                           ponFiltro("DS_E_NOTIF", false, true, "DS_E_NOTIF", true));
          st = con.prepareStatement(resultSQL.toString());
          prepareFiltro("DS_E_NOTIF", false, true, "CD_E_NOTIF", true);
          break;
        case modoE_NOTIF + SrvGeneral.servletSELECCION_X_CODIGO: // LIKE COD

          // ejemplo que trae todas las ZBS
          resultSQL.append("SELECT CD_E_NOTIF, DS_E_NOTIF FROM SIVE_E_NOTIF " +
                           ponFiltro("CD_E_NOTIF", true, true, "CD_E_NOTIF", true));
          st = con.prepareStatement(resultSQL.toString());
          prepareFiltro("CD_E_NOTIF", true, true, "CD_E_NOTIF", true);
          break;
        case modoE_NOTIF + SrvGeneral.servletSELECCION_X_DESCRIPCION: // LIKE DESC

          // ejemplo que trae todas las ZBS
          resultSQL.append("SELECT CD_E_NOTIF, DS_E_NOTIF FROM SIVE_E_NOTIF " +
                           ponFiltro("DS_E_NOTIF", true, true, "DS_E_NOTIF", true));
          st = con.prepareStatement(resultSQL.toString());
          prepareFiltro("DS_E_NOTIF", true, true, "CD_E_NOTIF", true);
          break;

          //Municipios
        case modoMUNICIPIO:
        case modoMUNICIPIO + SrvGeneral.servletOBTENER_X_CODIGO:

          // se debe filtrar por pais, CA, prov
          resultSQL.append(
              "SELECT CD_MUN, DS_MUN, CD_NIVEL_1, CD_NIVEL_2, CD_ZBS  FROM SIVE_MUNICIPIO " +
              ponFiltro("CD_MUN", false, true, "CD_MUN", true));
          st = con.prepareStatement(resultSQL.toString());
          prepareFiltro("CD_MUN", false, true, "CD_MUN", true);
          break;
        case modoMUNICIPIO + SrvGeneral.servletOBTENER_X_DESCRIPCION:

//                  resultSQL.append( "SELECT CD_MUN, DS_MUN , CD_NIVEL_1, CD_NIVEL_2, CD_ZBS FROM SIVE_MUNICIPIO "  + ponFiltro("DS_MUN", false, true, "CD_MUN", true) );
          resultSQL.append(
              "SELECT CD_MUN, DS_MUN , CD_NIVEL_1, CD_NIVEL_2, CD_ZBS FROM SIVE_MUNICIPIO " +
              ponFiltro("DS_MUN", false, true, "DS_MUN", true));
          st = con.prepareStatement(resultSQL.toString());
//                  prepareFiltro("DS_MUN", false, true, "CD_MUN", true);
          prepareFiltro("DS_MUN", false, true, "DS_MUN", true);
          break;
        case modoMUNICIPIO + SrvGeneral.servletSELECCION_X_CODIGO:
          resultSQL.append(
              "SELECT CD_MUN, DS_MUN , CD_NIVEL_1, CD_NIVEL_2, CD_ZBS FROM SIVE_MUNICIPIO " +
              ponFiltro("CD_MUN", true, true, "CD_MUN", true));
          st = con.prepareStatement(resultSQL.toString());
          prepareFiltro("CD_MUN", true, true, "CD_MUN", true);
          break;
        case modoMUNICIPIO + SrvGeneral.servletSELECCION_X_DESCRIPCION:

//                resultSQL.append( "SELECT CD_MUN, DS_MUN , CD_NIVEL_1, CD_NIVEL_2, CD_ZBS FROM SIVE_MUNICIPIO "  + ponFiltro("DS_MUN", true, true,"CD_MUN", true) );
          resultSQL.append(
              "SELECT CD_MUN, DS_MUN , CD_NIVEL_1, CD_NIVEL_2, CD_ZBS FROM SIVE_MUNICIPIO " +
              ponFiltro("DS_MUN", true, true, "DS_MUN", true));
          st = con.prepareStatement(resultSQL.toString());
//                prepareFiltro("DS_MUN", true, true,"CD_MUN", true);
          prepareFiltro("DS_MUN", true, true, "DS_MUN", true);
          break;

          //________________________________________________________________
          //Modos en los que se trama (no hya m�ximo de registros)
          //iNumReg tiene valor 9999. Se traen todos

        case modoPAISES:
          resultSQL.append("SELECT CD_PAIS, DS_PAIS FROM SIVE_PAISES "
                           //+ ponFiltro("CD_PAIS", true, true, "DS_PAIS", true) );
                           + ponFiltro("DS_PAIS", true, true, "DS_PAIS", true));
          st = con.prepareStatement(resultSQL.toString());
          //prepareFiltro("CD_PAIS", true, true, "DS_PAIS", true);
          prepareFiltro("DS_PAIS", true, true, "DS_PAIS", true);
          iNumReg = 9999;
          break;
        case modoCCAA:
          resultSQL.append("SELECT CD_CA, DS_CA, DSL_CA FROM SIVE_COM_AUT "
                           //   + ponFiltro("CD_CA", true, true, "DS_CA", true) );
                           + ponFiltro("DS_CA", true, true, "DS_CA", true));
          st = con.prepareStatement(resultSQL.toString());
//                  prepareFiltro("CD_CA", true, true, "DS_CA", true);
          prepareFiltro("DS_CA", true, true, "DS_CA", true);
          iNumReg = 9999;
          break;
        case modoCCAA_TRAMERO:
          resultSQL.append(
              "SELECT CDCOMU CD_CA, DSCOMU DS_CA FROM SUCA_AUTONOMIAS "
              + ponFiltro("DSCOMU", true, true, "DSCOMU", true));

          st = con.prepareStatement(resultSQL.toString());

          prepareFiltro("DSCOMU", true, true, "DSCOMU", true);
          iNumReg = 9999;
          break;
          //Recoge las provincias . En cliente se a�ade filtro CA
        case modoPROVINCIAS:
          resultSQL.append(
              "SELECT CD_PROV, DS_PROV, DSL_PROV FROM SIVE_PROVINCIA "
              //  + ponFiltro("CD_PROV",true,  true, "DS_PROV", true) );
              + ponFiltro("DS_PROV", true, true, "DS_PROV", true));
          st = con.prepareStatement(resultSQL.toString());
//                  prepareFiltro("CD_PROV",true,  true, "DS_PROV", true);
          prepareFiltro("DS_PROV", true, true, "DS_PROV", true);
          iNumReg = 9999;
          break;
        case modoTDOC:
          resultSQL.append(
              "SELECT CD_TDOC, DS_TIPODOC, DSL_TIPODOC FROM SIVE_T_DOC "
              //  + ponFiltro("CD_TDOC", true, true, "CD_TDOC", true) );
              + ponFiltro("DS_TIPODOC", true, true, "DS_TIPODOC", true));
          st = con.prepareStatement(resultSQL.toString());
//                  prepareFiltro("CD_TDOC", true, true, "CD_TDOC", true);
          prepareFiltro("DS_TIPODOC", true, true, "DS_TIPODOC", true);
          iNumReg = 9999;
          break;
        case modoSEXO:
          resultSQL.append("SELECT CD_SEXO, DS_SEXO,DSL_SEXO FROM SIVE_SEXO "
                           //  + ponFiltro("DS_SEXO", true, true, "CD_SEXO", true) );
                           + ponFiltro("DS_SEXO", true, true, "DS_SEXO", true));
          st = con.prepareStatement(resultSQL.toString());
//                prepareFiltro("DS_SEXO", true, true, "CD_SEXO", true);
          prepareFiltro("DS_SEXO", true, true, "DS_SEXO", true);
          iNumReg = 9999;
          break;
        case modoMBAJA:
          resultSQL.append(
              "SELECT CD_MOTBAJA, DS_MOTBAJA,DSL_MOTBAJA FROM SIVE_MOTIVO_BAJA "
//                    + ponFiltro("DS_MOTBAJA", true, true, "CD_MOTBAJA",true) );
              + ponFiltro("DS_MOTBAJA", true, true, "DS_MOTBAJA", true));
          st = con.prepareStatement(resultSQL.toString());
//                  prepareFiltro("DS_MOTBAJA", true, true, "CD_MOTBAJA",true);
          prepareFiltro("DS_MOTBAJA", true, true, "DS_MOTBAJA", true);
          iNumReg = 9999;
          break;
        case modoNivel1:
          resultSQL.append(
              "SELECT CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1 FROM SIVE_NIVEL1_S "
              //     + ponFiltro("CD_NIVEL_1",true,  true,"CD_NIVEL_1", true) );
              + ponFiltro("DS_NIVEL_1", true, true, "DS_NIVEL_1", true));
          st = con.prepareStatement(resultSQL.toString());
//                  prepareFiltro("CD_NIVEL_1",true,  true,"CD_NIVEL_1", true);
          prepareFiltro("DS_NIVEL_1", true, true, "DS_NIVEL_1", true);
          iNumReg = 9999;
          break;
        case modoNivel2:
          resultSQL.append(
              "SELECT CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2, CD_NIVEL_1 FROM SIVE_NIVEL2_S "
              + ponFiltro("DS_NIVEL_2", true, true, "DS_NIVEL_2", true));
          st = con.prepareStatement(resultSQL.toString());
//                  prepareFiltro("CD_NIVEL_2",true,  true,"CD_NIVEL_2", true);
          prepareFiltro("DS_NIVEL_2", true, true, "DS_NIVEL_2", true);
          iNumReg = 9999;
          break;
          /*          case modoPERMISO:  // LIKE DESC
                            iNumReg = 9999;
                            resultSQL.append( "SELECT IT_FG_ENFERMO FROM SIVE_USUARIO " + ponFiltro("CD_USUARIO",false,  true,"CD_USUARIO", true) );
                            st = con.prepareStatement(resultSQL.toString());
               prepareFiltro("CD_USUARIO",false,  true, "CD_USUARIO", true);
                            break;
        */
      } //Fin prepare

    }
    catch (Exception exc) {
      strError = exc.toString();
    }

    numEltos.setLength(0);
    numEltos.append(Integer.toString(iNumReg));

    return st;
  }

  public boolean prepararTratamientoIdiomas(int a_imodo, Hashtable hashIdiomas) {

    boolean bTratIdiomas = false;
    hashIdiomas.clear();

    switch (a_imodo) {

      case modoBUSCAENFERMO:
        break;
      case modoDATOSENFERMO:
        break;
      case modoBUSCAENFERMOTRAMERO:
        break;
      case modoDATOSENFERMOTRAMERO:
        break;
      case modoMODIFICADATOS:
        break;
      case modoCOPIA:
        break;
      case modoDENFER:
        break;

        //Equipos notificadores
      case modoE_NOTIF:
      case modoE_NOTIF + SrvGeneral.servletOBTENER_X_CODIGO:
        break;
      case modoE_NOTIF + SrvGeneral.servletOBTENER_X_DESCRIPCION: // = DESC
        break;
      case modoE_NOTIF + SrvGeneral.servletSELECCION_X_CODIGO: // LIKE COD
        break;
      case modoE_NOTIF + SrvGeneral.servletSELECCION_X_DESCRIPCION: // LIKE DESC
        break;

        //Municipios
      case modoMUNICIPIO:
      case modoMUNICIPIO + SrvGeneral.servletOBTENER_X_CODIGO:
        break;
      case modoMUNICIPIO + SrvGeneral.servletOBTENER_X_DESCRIPCION:
        break;
      case modoMUNICIPIO + SrvGeneral.servletSELECCION_X_CODIGO:
        break;
      case modoMUNICIPIO + SrvGeneral.servletSELECCION_X_DESCRIPCION:
        break;
      case modoPAISES:
        break;
      case modoCCAA:
        bTratIdiomas = true;
        hashIdiomas.put("DS_CA", "DSL_CA");
        break;
      case modoCCAA_TRAMERO:
        break;
      case modoPROVINCIAS:
        bTratIdiomas = true;
        hashIdiomas.put("DS_PROV", "DSL_PROV");
        break;
      case modoTDOC:
        bTratIdiomas = true;
        hashIdiomas.put("DS_TIPODOC", "DSL_TIPODOC");

        break;
      case modoSEXO:
        bTratIdiomas = true;
        hashIdiomas.put("DS_SEXO", "DSL_SEXO");
        break;
      case modoMBAJA:
        bTratIdiomas = true;
        hashIdiomas.put("DS_MOTBAJA", "DSL_MOTBAJA");
        break;
      case modoNivel1:
        bTratIdiomas = true;
        hashIdiomas.put("DS_NIVEL_1", "DSL_NIVEL_1");
        break;
      case modoNivel2:
        bTratIdiomas = true;
        hashIdiomas.put("DS_NIVEL_2", "DSL_NIVEL_2");
        break;
      case modoPERMISO: // LIKE DESC
        break;
    }
    return bTratIdiomas;
  }

  //_______________________________________________________________

  //fija los par�metros de los datos
  public void fija_parametros(String a_campo) {
  }

  //_______________________________________________________________
  ///// Funciones que crean la sentencia SQL

  public PreparedStatement getSQLBuscaEnfermo(StringBuffer result) throws
      Exception {
    String clave = null;
    Object o = null;
    String elfiltro = null;
    boolean add_and = false;
    DataEnfermo parameter = (DataEnfermo) (param.firstElement());
    int iNumParam;
    StringBuffer strParam = new StringBuffer();

    if (!parameter.getMostrarBajas()) {
      result.append(" a.FC_BAJA IS NULL ");
      add_and = true;
    }
    else {
      add_and = false;
    }

    /// recorremos la Hashtable a�adiendo los componentes
    for (Enumeration e = parameter.keys(); e.hasMoreElements(); ) {
      clave = (String) e.nextElement();

      if (clave != null && ! (clave.equals("b.CD_ENFCIE"))) {
        /// quitamos el campo enfermedad porque pertenece a otra tabla
        if (add_and) {
          result.append(" AND ");
        }
        else {
          add_and = true;
        }
        result.append(clave + " LIKE  ? ");
      }
    } ///orf

    /// ponemos el filtro de CD_ENFCIE
    String strCD_ENFCIE = (String) parameter.get("b.CD_ENFCIE");
    if (strCD_ENFCIE != null) {
      if (add_and) {
        result.append(" AND ");
      }
      else {
        add_and = true;
      }
      result.append(
          " a.CD_ENFERMO IN ( SELECT CD_ENFERMO FROM SIVE_EDOIND WHERE CD_ENFCIE = ? " +
          " UNION " +
          " SELECT CD_ENFERMO FROM SIVE_CASOCONTAC ) ");
    }

    // ponemos el filtro y el ordenamiento
    elfiltro = param.getFilter();
    if (elfiltro != null && elfiltro.trim().length() > 0) {
      if (add_and) {
        result.append(" AND ");
      }
      else {
        add_and = true;
      }
      result.append(" CD_ENFERMO > ? ");
    }

    // ordenaci�n
    result.append(" order by a.CD_ENFERMO");

    // a�adimos
    if (add_and) {
      result.insert(0, "SELECT DISTINCT a.CD_ENFERMO, a.DS_APE1, a.DS_APE2, a.DS_NOMBRE, a.FC_NAC, a.CD_PROV, a.CD_MUN, a.SIGLAS, " +
                    " IT_ENFERMO, IT_CONTACTO " +
                    " FROM SIVE_ENFERMO a WHERE " +
                    (sTextoMetaModo.equals("") ? "" : sTextoMetaModo + " AND "));
    }
    else {
      result.insert(0, "SELECT DISTINCT a.CD_ENFERMO, a.DS_APE1, a.DS_APE2, a.DS_NOMBRE, a.FC_NAC, a.CD_PROV, a.CD_MUN, a.SIGLAS, " +
                    " IT_ENFERMO, IT_CONTACTO " +
                    " FROM SIVE_ENFERMO a " +
                    (sTextoMetaModo.equals("") ? "" :
                     " WHERE " + sTextoMetaModo));
    }

    String ss = result.toString();

    st = con.prepareStatement(result.toString());

    iNumParam = 1; // contador de los par�metros

    // ponemos los par�metros
    /// recorremos la Hashtable a�adiendo los componentes
    for (Enumeration e = parameter.keys(); e.hasMoreElements(); ) {
      clave = (String) e.nextElement();
      o = parameter.get(clave);

      if (clave != null && ! (clave.equals("b.CD_ENFCIE"))) {
        if (o != null) {
          try {
            String str = (String) o;
            if (str.charAt(0) == '\'') {
              // le quitamos las comillas
              o = str.substring(1, str.length() - 1);
            }
            else if (str.trim().length() == 0) { //ponemos NULL
              o = null;
            }
          }
          catch (Exception exc) {}
          if (o != null) {
            //st.setObject(iNumParam++, o);
            if (o.getClass() == (new String()).getClass()) {
              st.setString(iNumParam++, (String) o);
            }
            else if (o.getClass() == (new StringBuffer()).getClass()) {
              java.util.Date d = Fechas.string2Date(o.toString());
              st.setDate(iNumParam++, new java.sql.Date(d.getTime()));
            }
            else {
              st.setObject(iNumParam++, o);
            }
          }
          else {
            st.setNull(iNumParam++, java.sql.Types.VARCHAR);
          }

        }
        else { // o == null
          st.setNull(iNumParam++, java.sql.Types.VARCHAR);
        }
      }
    } //orf
    // ponemos la enfermedad
    strCD_ENFCIE = (String) parameter.get("b.CD_ENFCIE");
    if (strCD_ENFCIE != null) {
      st.setString(iNumParam++, strCD_ENFCIE);
    }

    // ponemos el filtro y el ordenamiento
    elfiltro = (String) param.getFilter();
    if (elfiltro.length() > 0) {
      st.setObject(iNumParam++, elfiltro);
    }

    return st;
  }

  //_______________________________________________________________

  public PreparedStatement getSQLDatosEnfermo(StringBuffer result) throws
      Exception {
    String codEnfer = null;

    String sInSelect = null;

    String sCodEnfeEdo = null;

    Hashtable parameter = (Hashtable) (param.firstElement());

    result.append(DS_DATOS_ENFERMO);
    //result.append(" FROM SIVE_ENFERMO WHERE CD_ENFERMO =  ");

    sCodEnfeEdo = cogeCodigoEnfermedad();

    result.append(" FROM SIVE_ENFERMO WHERE ( CD_ENFERMO IN ");
    result.append(" (SELECT CD_ENFERMO FROM SIVE_EDOIND WHERE CD_ENFCIE = '");

    if ( (sCodEnfeEdo.length() > 0) && (sCodEnfeEdo != null)) {
      result.append(sCodEnfeEdo);
      result.append("') OR CD_ENFERMO IN (SELECT CD_ENFERMO ");
      result.append("FROM SIVE_CASOCONTAC) ) AND CD_ENFERMO = ");
    }
    else {
      // Si no existe en c�digo de enfermedad.
      result.setLength(0);
    }

    codEnfer = (String) parameter.get("CD_ENFERMO");

    if ( (result.length() > 0) && (codEnfer != null)) {
      result.append(codEnfer);
      result.append(sTextoMetaModo.equals("") ? "" : " AND " + sTextoMetaModo); // E **
    }
    else {
      //  la sentencia que se le pasa es erronea FALtA codigo
      result.setLength(0);
    }
    st = con.prepareStatement(result.toString());

    return st;
  }

  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    //java.sql.Timestamp TSFec = new java.sql.Timestamp(dFecha.getTime());
    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);
    return TSFec;
  }

  private String cogeCodigoEnfermedad() {

    String strConsultaEnfermedad =
        "select CD_ENFCIE from SIVE_ENFEREDO where CD_ENFERE='8'";
    String cod_enf_cie = "";

    try {
      st = con.prepareStatement(strConsultaEnfermedad);
      rs = st.executeQuery();
      while (rs.next()) {
        cod_enf_cie = rs.getString(1);
      }
      rs.close();
      rs = null;
      st.close();
      st = null;
    }
    catch (SQLException ex) {
      trazaLog("Fallo por: " + ex);
    }

    return cod_enf_cie;
  }

  //_______________________________________________________________

  public Vector /*PreparedStatement*/ getSQLModificaEnfermo(StringBuffer result) throws
      Exception {
    // Devuelve un vector: la PreparedStatement y
    //                     la fcUltact como string

    Vector resultado = new Vector();

    String clave = null;
    Object o = null;
    String elfiltro = null;
    Object codEnfer = null;
    int iNumParam = 1;

    Hashtable parameter = (Hashtable) (param.firstElement());

    result.append("UPDATE SIVE_ENFERMO SET ");
    boolean add_and = false;

    codEnfer = parameter.get("CD_ENFERMO");
    if (codEnfer == null) {
      resultado.addElement(st);
      return resultado;
    }

    // modificamos todos los valores que viene en la lista
    for (Enumeration e = parameter.keys(); e.hasMoreElements(); ) {
      clave = (String) e.nextElement();
      if (clave != null) { ///&& !(o.equals("CD_ENFERMEDAD")) ) {
        if (add_and) {
          result.append(" , ");
        }
        else {
          add_and = true;
        }
        /// quitamos el campo enfermedad porque pertenece a otra tabla
        result.append(clave + " = ? ");
      }
    }

    result.append(" WHERE CD_ENFERMO = ? ");

    st = con.prepareStatement(result.toString());
    String sFecha = "";

    // modificamos todos los valores que vienen en la lista
    for (Enumeration e = parameter.keys(); e.hasMoreElements(); ) {
      clave = (String) e.nextElement();

      // En este punto podemos conocer si se va a dar de baja el
      // registro.
      if (clave.equals("CD_MOTBAJA")) {
        o = parameter.get("CD_MOTBAJA");
        if ( ( (String) o).equals(constantes.sMotivoDefuncion)) {
          bBajaDefuncion = true;
        }
      }

      if (clave.equals("FC_ULTACT")) {
        java.util.Date fdate = new java.util.Date();
        sFecha = formUltAct.format(fdate); //string

        //partirla!!
        st.setTimestamp(iNumParam++, cadena_a_timestamp(sFecha));
      }
      else {
        o = parameter.get(clave);

        if (clave != null) {
          if (o != null) {
            try {
              String str = (String) o;
              if (str.charAt(0) == '\'') {
                // le quitamos las comillas
                o = str.substring(1, str.length() - 1);
              }
              else if (str.trim().length() == 0) { //ponemos NULL
                o = null;
              }
            }
            catch (Exception exc) {}
            if (o != null) {
              //st.setObject(iNumParam++, o);
              if (o.getClass() == (new String()).getClass()) {
                st.setString(iNumParam++, (String) o);
              }
              else if (o.getClass() == (new StringBuffer()).getClass()) {
                java.util.Date d = Fechas.string2Date(o.toString());
                st.setDate(iNumParam++, new java.sql.Date(d.getTime()));
              }
              else {
                st.setObject(iNumParam++, o);
              }

            }
            else {
              st.setNull(iNumParam++, java.sql.Types.VARCHAR);
            }

          }
          else { // o == null
            st.setNull(iNumParam++, java.sql.Types.VARCHAR);
          }
        }
      }
    } //orf

    st.setObject(iNumParam++, codEnfer.toString());

    resultado.addElement(st);
    resultado.addElement(sFecha);

    return resultado;
  }

  //_______________________________________________________________

  public PreparedStatement getSQLCierraCasoTBC(StringBuffer result) throws
      Exception {
    String sCodigoEnfermo = null;
    String sFechaActualizacion = null;

    Hashtable parameter = (Hashtable) (param.firstElement());

    result.append(
        "UPDATE SIVE_REGISTROTBC SET FC_SALRTBC = ?, CD_MOTSALRTBC = ? " +
        "WHERE NM_EDO IN (SELECT NM_EDO FROM SIVE_EDOIND WHERE CD_ENFERMO = ?) ");

    sCodigoEnfermo = (String) parameter.get("CD_ENFERMO");

    java.util.Date fdate = new java.util.Date();
    sFechaActualizacion = formUltAct.format(fdate); //string
    // sFechaActualizacion = (String) parameter.get("FC_ULTACT");

    st = con.prepareStatement(result.toString());

    st.setTimestamp(1, cadena_a_timestamp(sFechaActualizacion));

    st.setString(2, constantes.sMotivoDefuncionTBC);

    st.setString(3, sCodigoEnfermo);

    return st;
  }

  //_________________________________________________________________

  public PreparedStatement getSQLInsertEnfermo(StringBuffer result) throws
      Exception {
    String codEnfer = null, clave = null;
    Object o = null;
    int iNumParam = 1;

    Hashtable parameter = (Hashtable) (param.elementAt(1));

    // E
    PreparedStatement stat = null;
    ResultSet rset = null;

    String maximo = String.valueOf(sapp.Funciones.SecGeneral(con, stat, rset,
        "NM_MOVENF")); // E

    // E

    result.append("INSERT INTO SIVE_MOV_ENFERMO ( NM_MOVENF ");
    boolean add_and = true;

    // modificamos todos los valores que viene en la lista
    for (Enumeration e = parameter.keys(); e.hasMoreElements(); ) {
      clave = (String) e.nextElement();
      if (clave != null && (clave.trim().length() > 0)) {
        if (add_and) {
          result.append(" , ");
        }
        else {
          add_and = true;
        }
        /// quitamos el campo enfermedad porque pertenece a otra tabla
        result.append(clave);
      }
    }

    result.append(" ) VALUES ( " + maximo);

    // modificamos todos los valores que viene en la lista
    for (Enumeration e = parameter.keys(); e.hasMoreElements(); ) {
      clave = (String) e.nextElement();

      if (clave != null) { //   && (o.trim().length() > 0) ) {    ///&& !(o.equals("CD_ENFERMEDAD")) ) {
        /// quitamos el campo enfermedad porque pertenece a otra tabla
        result.append(" , ? ");
      }
    }
    result.append(" ) ");

    st = con.prepareStatement(result.toString());

    for (Enumeration e = parameter.keys(); e.hasMoreElements(); ) {
      clave = (String) e.nextElement();

      if (clave.equals("FC_ULTACT")) {
        //java.util.Date fdate = new java.util.Date();
        //sFecha = formUltAct.format(fdate);//string
        //String sFecha = (String)parameter.get(clave);

        // no se porque pero a veces esta fecha la quiere como un StringBuffer
        // y otras como un string
        o = parameter.get(clave);
        if (o.getClass() == (new String()).getClass()) {

          String sFecha = (String) o;

          //partirla!!
          st.setTimestamp(iNumParam++, cadena_a_timestamp(sFecha));
        }
        else if (o.getClass() == (new StringBuffer()).getClass()) {

          StringBuffer sFecha = (StringBuffer) o;

          //partirla!!
          st.setTimestamp(iNumParam++, cadena_a_timestamp(sFecha.toString()));
        }
      }
      else {

        o = parameter.get(clave);

        if (clave != null) {
          if (o != null) {
            try {
              String str = (String) o;
              if (str.charAt(0) == '\'') {
                // le quitamos las comillas
                o = str.substring(1, str.length() - 1);
              }
              else if (str.trim().length() == 0) { //ponemos NULL
                o = null;
              }
            }
            catch (Exception exc) {}
            if (o != null) {
              //st.setObject(iNumParam++, o);
              if (o.getClass() == (new String()).getClass()) {
                st.setString(iNumParam++, (String) o);
              }
              else if (o.getClass() == (new StringBuffer()).getClass()) {
                java.util.Date d = Fechas.string2Date(o.toString());
                st.setDate(iNumParam++, new java.sql.Date(d.getTime()));
              }
              else {
                st.setObject(iNumParam++, o);
              }
            }
            else {
              st.setNull(iNumParam++, java.sql.Types.VARCHAR);
            }

          }
          else { // o == null
            st.setNull(iNumParam++, java.sql.Types.VARCHAR);
          }
        }
      }
    } //orf

    return st;
  }

  //_______________________________________________________________

  /** de la lista de enfermos pone a cada enfermo su enfermedad, si tiene varias duplica el registro */
  public CLista rellenarEnfermedades(CLista a_enfermos) {
    CLista result = new CLista();
    CLista enfermedades = null;
    DataEnfermo denf = null;
    DataEnfermo copia = null;
    String strENFERMEDAD = null;
    boolean is_add = false;

    // preparamos la sentencia
    try {

      st = con.prepareStatement(
          " SELECT DISTINCT CD_ENFCIE FROM SIVE_EDOIND  WHERE CD_ENFERMO = ? ");

      for (int i = 0; i < a_enfermos.size(); i++) {
        // para cada enfermos buscamos sus enfermedade
        denf = (DataEnfermo) a_enfermos.elementAt(i);
        st.setObject(1, denf.get("CD_ENFERMO"));
        rs = st.executeQuery();
        is_add = false;
        while (rs.next()) {
          strENFERMEDAD = rs.getString(1);
          copia = (DataEnfermo) denf.clone();
          copia.put("CD_ENFCIE", strENFERMEDAD);
          result.addElement(copia);
          is_add = true;
        }
        if (!is_add) {
          // si no seh a�adido se mete uno vacio
          result.addElement(denf);
        }

        rs.close();
        rs = null;
      } // orf

      st.close();
      st = null;
    }
    catch (Exception exc) {
      result = null;
    }

    result.setState(a_enfermos.getState());
    result.setFilter(a_enfermos.getFilter());
    return result;
  }

  //_______________________________________________________________

  protected CLista getDataEnfermo(String a_clave, String a_descrip,
                                  String a_descripL, String a_tabla) throws
      Exception {
    DataEnfermo d = null;
    String clave, dato, datoL;
    CLista result = new CLista();

    if (a_clave == null || a_descrip == null || a_descripL == null || a_tabla == null) {
      return null; // no se pueden hallar los datos
    }

    st = con.prepareStatement("SELECT " + a_clave + " , " + a_descrip + " , " +
                              a_descripL + " FROM " + a_tabla + " ORDER BY " +
                              a_descrip);
    rs = st.executeQuery();
    while (rs.next()) {
      d = new DataEnfermo(a_clave);
      clave = rs.getString(1);
      if (clave != null) {
        d.put(a_clave, clave);
      }
      dato = rs.getString(2);
      if (dato != null) {
        d.put(a_descrip, dato);
      }
      datoL = rs.getString(3);
      if (datoL != null) {
        d.put(a_descripL, datoL);

        //_________ Idiomas L Rivera____________
        //Si idioma no es el local y hay desc local se mete la desc local en la desc
        if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (datoL != null)) {
          d.put(a_descrip, datoL);
        }
      }

      result.addElement(d);
    }
    rs.close();
    st.close();
    rs = null;
    st = null;

    return result;
  }

  //_______________________________________________________________

  protected CLista getDataEnfermo(String a_clave, String a_descrip,
                                  String a_tabla) throws Exception {
    DataEnfermo d = null;
    String clave, dato;
    CLista result = new CLista();

    if (a_clave == null || a_descrip == null || a_tabla == null) {
      return null; // no se pueden hallar los datos
    }

    st = con.prepareStatement("SELECT " + a_clave + " , " + a_descrip +
                              " FROM " + a_tabla + " ORDER BY " + a_descrip);
    rs = st.executeQuery();
    while (rs.next()) {
      d = new DataEnfermo(a_clave);
      clave = rs.getString(1);
      if (clave != null) {
        d.put(a_clave, clave);
      }
      dato = rs.getString(2);
      if (dato != null) {
        d.put(a_descrip, dato);
      }

      result.addElement(d);
    }
    rs.close();
    st.close();
    rs = null;
    st = null;

    return result;
  }

  //_______________________________________________________________
  /**
   *  esta funci�n ejecuta la sentencia select con el filtro dado
   *  y devuelve el dato que le han pedido
       * @param a_strSQL es la sen tencia SQL SELECT DS_MUN FROM SIVE_MUNICIPIO WHERE
       * @param a_code es la cadena que describe el campo por el que se filtra  CD_MUN
   * @param a_des es el campo que lleva la descripci�n DS_MUN
   * @param a_filter es el campo que lleva el dato para filtrar
   */
  public String traeDescripcionCA(String a_strSQL, String a_code, String a_des,
                                  Object a_filter, String campo2,
                                  StringBuffer a_CA) {

    //traeDescripcionCA("SELECT CDCOMU, DSPROV FROM SUCA_PROVINCIA WHERE ", "CDPROV", "DSPROV", hash.get("CD_PROV"), "CDCOMU",codCA);

    String result = null;
    String otroDato = null;
    // si no hay filtro  no traemos nada
    if (a_filter == null) {
      return result;
    }

    try {
      st = con.prepareStatement(a_strSQL + a_code + " = ? ");
      st.setObject(1, a_filter);
      rs = st.executeQuery();
      if (rs.next()) {
        result = rs.getString(a_des);
        otroDato = rs.getString(campo2);
      }
      rs.close();
      rs = null;
      st.close();
      st = null;
    }
    catch (Exception exc) {
      result = " ";
    }

    a_CA.append(otroDato);
    return result;

  }

  /** prueba del servlet */

  public CLista doPrueba(int operacion, CLista parametros) throws Exception {
    jdcdriver = new JDCConnectionDriver("oracle.jdbc.driver.OracleDriver",
        "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
        "sive_desa",
        "sive_desa");
    Connection con = null;
    con = openConnection();
    return doWork(operacion, parametros);
  }

  //_______________________________________________________________

  /** prueba del servlet */
  static public void main(String[] args) {
    try {

      /*    CLista data = new CLista();
           SrvEnfermo srv = new SrvEnfermo();
           CLista dd = srv.doPrueba(modoDENFER, data);
           DataEnfermo d = new DataEnfermo("CD_ENFERMO");*/

      CLista data = new CLista();
      SrvEnfermo srv = new SrvEnfermo();
      DataEnfermo d = new DataEnfermo("CD_ENFERMO");

      d.put("CD_NIVEL_1", "1");
      d.put("CD_NIVEL_2", "1");
      d.put("CD_ZBS", "1");
      d.setFiltro("");
      //data.addElement(d);
      data.addElement(d); // par�metro anterior

//     DataEnfermo denfer = new DataEnfermo ("CD_ENFERMO");
//     denfer.put("CD_ENFERMO", "670");
//     data.addElement(denfer);
//     data.addElement(constantes.sSoloEnfermos);

      CLista dd;
//     dd = srv.doPrueba(SrvEnfermo.modoDATOSENFERMO, data);
//   dd = srv.doPrueba(SrvEnfermo.modoDENFERDATOS, data);
//   dd = srv.doPrueba(SrvEnfermo.modoBUSCAENFERMO, data);
//   dd = srv.doPrueba(SrvEnfermo.modoNIVELES_MUN, data);
      dd = srv.doPrueba(SrvEnfermo.modoDENFER, data);

      /*     String ss = "Lista: " + dd;
           Object o = null;
           if (dd != null && dd.size() > 0) {
              d = (DataEnfermo) dd.firstElement();
               o = d.get("DS_MUN");
               o = d.get("DS_CA");
               o = d.get("DS_TIPODOC");
               o = d.get("DS_SEXO");
               o = d.get("CD_ENFERMO");
               o = d.get("DS_NIVEL_1");
               o = d.get("DS_NIVEL_2");
               o = d.get("DS_ZBS");
               o = d.get("CD_ENFERMO");
               o = d.get("FC_BAJA");
// System_out.println(d.get("DS_MUN")+";"+d.get("DS_CA")+";"+d.get("DS_TIPODOC"));
// System_out.println(d.get("DS_SEXO")+";"+d.get("CD_ENFERMO")+";"+d.get("DS_NIVEL_1"));
// System_out.println(d.get("DS_NIVEL_2")+";"+d.get("DS_ZBS")+";"+d.get("FC_BAJA"));
// System_out.println(d.get("CD_OPE")+";"+d.get("FC_ULTACT"));
           }else{
              ;
           }
       */
      int i = 33;
    }
    catch (Exception exc) {
      String dd = exc.toString();
    }
  }
} //________________________________________________ END_CLASS
