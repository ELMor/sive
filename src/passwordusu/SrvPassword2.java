package passwordusu;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sapp2.DBServlet;
import sapp2.Data;
import sapp2.Lista;

public class SrvPassword2
    extends DBServlet {

  public SrvPassword2() {}

  protected Lista doWork(int opmode, Lista param) throws Exception {

    // control de error
    boolean bError = false;
    String sMsg = null;

    // Objetos JDBC
    PreparedStatement st = null;
    ResultSet rs = null;

    // buffers
    Lista snapShot = null;
    Lista vResultado = new Lista();
    Data dataEntrada = null;

    boolean encontrado = false;

    final String sSEL_PASS = "SELECT COD_APLICACION, " +
        "COD_USUARIO, PASSWORD " +
        "FROM V_APLIC_USU_GRU  " +
        "WHERE COD_APLICACION = ? " +
        "AND COD_USUARIO= ? " +
        "AND PASSWORD= ? ";

    final String sUPD_PASS = "UPDATE GAT_T002 " +
        "set PASSWORD=? " +
        "where COD_USUARIO=?";

    try {
      con.setAutoCommit(false);

      dataEntrada = (Data) param.elementAt(0);

      // Preparacion de la sentencia SQL
      st = con.prepareStatement(sSEL_PASS);

      st.setString(1, dataEntrada.getString("COD_APLICACION"));
      st.setString(2, dataEntrada.getString("COD_USUARIO"));
      st.setString(3, dataEntrada.getString("PASSWORD"));

      // Ejecucion de la query, obteniendo un ResultSet
      rs = st.executeQuery();

      //Si existe alg�n registro realizo la modificaci�n
      if (rs.next()) {
        encontrado = true;
      }
      // cierre
      st.close();
      st = null;
      con.commit();

      if (encontrado == true) {
        st = con.prepareStatement(sUPD_PASS);

        st.setString(1, dataEntrada.getString("PASSWORD_NUEVA"));
        st.setString(2, dataEntrada.getString("COD_USUARIO"));

        st.executeUpdate();

        // cierre
        st.close();
        st = null;
        con.commit();

      }
      else {
        String codApli = dataEntrada.getString("COD_APLICACION");
        String codUsu = dataEntrada.getString("COD_USUARIO");
        String codPass = dataEntrada.getString("PASSWORD");
        sMsg = "El usuario " + codUsu + " no existe en la aplicaci�n " +
            codApli + " con la password introducida";
        bError = true;
      }
    }
    catch (SQLException e1) {
      // traza el error
      trazaLog(e1);
      bError = true;
      sMsg = "Error en alg�n registro";
    }
    catch (Exception e2) {
      trazaLog(e2);
      bError = true;
      sMsg = "Error inesperado";
    }

    // devuelve el error recogido
    if (bError) {
      throw new Exception(sMsg);
    }
    return vResultado;
  } //end do_Work

} //end class
