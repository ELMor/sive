package notlimpiar;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

import capp.CLista;
import sapp.DBServlet;

public class SrvNotBorrar
    extends DBServlet {

  final int servletBORRAR_NO_RS = 0;
  //la entrada no debe llavar nada (param: login, perfil)
  //salida: si bien:  String con la notifedo borradas
  //                  String con la notif_sem borradas
  //        si mal: null

  final int servletSELECT_SI_RS = 1;
  //la entrada no debe llavar nada (param: login, perfil)
  //salida: si bien:  listaSalida con DataNotificaciones
  //        si mal: null

  final int servletBORRAR_SI_RS = 2;
  //la entrada no debe llavar nada (param: login)
  //salida: si bien:  listaSalida con DataNotificaciones
  //        si mal: null

  String fechaR = new String();
  String fechaN = new String();
  String ano = new String();
  int ind = 0;
  String mes = new String();
  String dia = new String();

  public SimpleDateFormat Format_Horas = new SimpleDateFormat(
      "dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));
  public java.util.Date dFecha = null;
  public java.sql.Date sqlFec = null;
  public SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
  public java.sql.Timestamp fecRecu = null;
  public String sfecRecu = "";

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 0;

    // objetos de datos

    // datos de param
    DataNotificaciones dataen = null;

    DataNotificaciones dataEntrada = null;
    DataNotificaciones dataClave = null;
    CLista listaSalida = new CLista();

    Vector listaNM_EDO = null; // Variables utilizadas para conocer
    Vector listaCD_E_NOTIF = null; // los permisos del usuario sobre los datos
    // que se pretende borrar.

    int iPerfil = 0;
    String sLogin = null;
    String CD_E_NOTIF_EQ = "";
    String cd_e_notif = "";

    CLista listaNotificaciones = new CLista(); //todas con it_ressem = 'N' o 'S'
    CLista listaBorrar_Edonum = new CLista(); //de esas, las que no tienen numericas
    CLista listaBorrar = new CLista(); //de esas, las que no tienen tp notif_edoi
    CLista listaNotifSem = new CLista(); //todas sin cobertura

    boolean bAlmenosUno = false;

    //querys
    String sQuery = "";

    //fechas
    //fecha actual-----------------------------------------------------------
    //para devolver (sFecha_Actual), para insertar (st.setTimestamp(9, TSFec))
    java.util.Date dFecha_Actual = new java.util.Date();
    String sFecha_Actual = Format_Horas.format(dFecha_Actual); //string
    //partirla!!
    int sdd = (new Integer(sFecha_Actual.substring(0, 2))).intValue();
    int smm = (new Integer(sFecha_Actual.substring(3, 5))).intValue();
    int syyyy = (new Integer(sFecha_Actual.substring(6, 10))).intValue();
    int shh = (new Integer(sFecha_Actual.substring(11, 13))).intValue();
    int smi = (new Integer(sFecha_Actual.substring(14, 16))).intValue();
    int sss = (new Integer(sFecha_Actual.substring(17, 19))).intValue();

    java.sql.Timestamp TSFec = new java.sql.Timestamp(syyyy - 1900, smm - 1,
        sdd, shh, smi, sss, 0);
    //st.setTimestamp(9, TSFec);
    // --------------------------------------------------------------------

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {

      if (opmode != servletBORRAR_SI_RS) {
        //seleccionamos la lista de notifedos con it_ressem a 'N'
        // o a 'S'

        ////////////////////////////////////////
        // modificacion jlt 26/10/2001
        // optimizamos la consulta
        /////////////////////////////////////////////////

        // recuperamos los valores
        dataen = (DataNotificaciones) param.firstElement();

        // parametros temporales
        int iparam = 1;

        sQuery = "select a.cd_e_notif, a.cd_anoepi,a.cd_semepi, "
            + " a.fc_recep,a.fc_fecnotif, a.NM_NNOTIFR,b.NM_NNOTIFT, "
            + " b.NM_NTOTREAL, c.CD_CENTRO, c.CD_NIVEL_1, c.CD_NIVEL_2, "
            + " c.CD_ZBS, a.CD_OPE, a.FC_ULTACT "
            + "	from sive_notifedo a, sive_notif_sem b, sive_e_notif c "
            + " where a.it_ressem = ? and "
            + " a.CD_E_NOTIF = b.CD_E_NOTIF and "
            + " a.CD_ANOEPI = b.CD_ANOEPI and "
            + " a.CD_SEMEPI = b.CD_SEMEPI	and "
            + " a.CD_E_NOTIF = c.CD_E_NOTIF and "
            + " not exists (select cd_e_notif from sive_edonum "
            + "	   	   	  where cd_e_notif = a.CD_E_NOTIF and "
            + "       		  		cd_anoepi = a.CD_ANOEPI and "
            + "				          cd_semepi = a.CD_SEMEPI and "
            + "         				fc_recep = a.FC_RECEP and "
            + "         				fc_fecnotif = a.FC_FECNOTIF) and "

            + " not exists (select cd_e_notif from sive_notif_edoi "
            + "	   	   	  where cd_e_notif = a.CD_E_NOTIF and "
            + "		  		        cd_anoepi = a.CD_ANOEPI and "
            + "         				cd_semepi = a.CD_SEMEPI and "
            + "         				fc_recep = a.FC_RECEP and "
            + "				          fc_fecnotif = a.FC_FECNOTIF) ";

        //////////////////////////  a�adimos filtros

        if (!dataen.getCD_NIVEL_1().equals("") &&
            dataen.getCD_NIVEL_1() != null) {
          sQuery = sQuery + " and c.cd_nivel_1 = ? ";

        }
        if (!dataen.getCD_ANOEPI().equals("") &&
            dataen.getCD_ANOEPI() != null) {
          sQuery = sQuery + " and a.cd_anoepi = ? ";

        }
        if (!dataen.getCD_SEMEPI().equals("") &&
            dataen.getCD_SEMEPI() != null) {
          sQuery = sQuery + " and a.cd_semepi = ? ";

        }
        if (!dataen.getCD_E_NOTIF().equals("") &&
            dataen.getCD_E_NOTIF() != null) {
          sQuery = sQuery + " and a.cd_e_notif = ? ";

          //////////////////////////

        }
        if (opmode == servletSELECT_SI_RS) {
          sQuery = sQuery + "order by a.fc_ultact";
          st = con.prepareStatement(sQuery);
          //st.setString(1, "S");
          st.setString(iparam, "S");
          iparam++;
        }
        else {
          st = con.prepareStatement(sQuery);
          //st.setString(1, "N");
          st.setString(iparam, "N");
          iparam++;
        }

        /////////////////////////// a�adimos los valores

        if (!dataen.getCD_NIVEL_1().equals("") &&
            dataen.getCD_NIVEL_1() != null) {
          st.setString(iparam, dataen.getCD_NIVEL_1());
          iparam++;
        }

        if (!dataen.getCD_ANOEPI().equals("") &&
            dataen.getCD_ANOEPI() != null) {
          st.setString(iparam, dataen.getCD_ANOEPI());
          iparam++;
        }

        if (!dataen.getCD_SEMEPI().equals("") &&
            dataen.getCD_SEMEPI() != null) {
          st.setString(iparam, dataen.getCD_SEMEPI());
          iparam++;
        }

        if (!dataen.getCD_E_NOTIF().equals("") &&
            dataen.getCD_E_NOTIF() != null) {
          st.setString(iparam, dataen.getCD_E_NOTIF());
          iparam++;
        }

        ///////////////////////////

        rs = st.executeQuery();

        while (rs.next()) {
          fecRecu = rs.getTimestamp("FC_ULTACT");
          sfecRecu = timestamp_a_cadena(fecRecu);
          dataClave = new DataNotificaciones(
              rs.getString("CD_E_NOTIF"),
              rs.getString("CD_NIVEL_1"),
              rs.getString("CD_NIVEL_2"),
              rs.getString("CD_ZBS"),
              rs.getString("CD_ANOEPI"),
              rs.getString("CD_SEMEPI"),
              rs.getString("CD_CENTRO"),
              rs.getString("NM_NNOTIFT"),
              rs.getString("NM_NTOTREAL"),
              rs.getString("NM_NNOTIFR"),
              formater.format(rs.getDate("FC_RECEP")),
              formater.format(rs.getDate("FC_FECNOTIF")),
              rs.getString("CD_OPE"), sfecRecu);

          listaNotificaciones.addElement(dataClave);
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        ////////////////////////////////////

        //hasta aqui, las notificaciones que puedo borrar
        //control de autorizaciones para cada notificacion
        //copia -----------------------------------
        CLista listaAlmacen = new CLista();
        for (i = 0; i < listaNotificaciones.size(); i++) {
          dataEntrada = (DataNotificaciones) listaNotificaciones.elementAt(i);
          listaAlmacen.addElement(dataEntrada);
        } //----------------------------------------

        iPerfil = param.getPerfil();
        sLogin = param.getLogin();

        // comentamos todo esta c�digo, pues la selecci�n de las
        // notificaciones seg�n el perfil se hace en el panel de
        // selecci�n,si es perfil 2 podr� seleccionar todas las
        // �reas, si es perfil 3 s�lo podr� seleccionar aquellas
        // que pueda ver,y los equipos est�n condicionados al
        // �rea seleccionada
        /*
             for (i=0; i < listaAlmacen.size(); i++){
          dataEntrada = (DataNotificaciones) listaAlmacen.elementAt(i);
          cd_e_notif = dataEntrada.getCD_E_NOTIF().trim();
          //0: Selecciono seg�n el perfil.
          switch (iPerfil) {
            case 1:
            case 5:
              break;//no deben entrar
            case 2:
              break;
            case 3:
            case 4:
              listaCD_E_NOTIF = new Vector();
              listaCD_E_NOTIF.addElement(cd_e_notif + ",");
              if (iPerfil == 3) {
                if (!getPermisoPerfil_3 (con, sLogin, listaCD_E_NOTIF))
                        listaNotificaciones.removeElementAt(i);
              }
              else if (iPerfil == 4) {
                if (!getPermisoPerfil_4 (con, sLogin, listaCD_E_NOTIF))
                       listaNotificaciones.removeElementAt(i);
              }
              break;
          }//switch parcial
             }//for
         */

      } //fin del if

      //hasta aqui tengo la listaBorrar ----
      switch (opmode) {
        case servletBORRAR_SI_RS:

          //listaBorrar(= param)
          DataNotificaciones data = null;
          for (i = 0; i < param.size(); i++) {
            data = (DataNotificaciones) param.elementAt(i);
          }

          //realizamos el calculo
          String sTotal = data.getNM_NTOTREAL();
          if (sTotal == null) {
            sTotal = "0";
          }
          if (sTotal == "") {
            sTotal = "0";
          }
          String sReal = data.getNM_NNOTIFR();
          if (sReal == null) {
            sReal = "0";
          }
          if (sReal == "") {
            sReal = "0";

          }
          int iTotal = (new Integer(sTotal)).intValue();
          int iReal = (new Integer(sReal)).intValue();
          int iDefinitivo = (new Integer(iTotal - iReal)).intValue();

          if (iReal != 0) {

            sQuery = "update SIVE_NOTIF_SEM "
                + " set NM_NTOTREAL = ? , CD_OPE = ? , FC_ULTACT = ? "
                + " WHERE (CD_E_NOTIF = ? AND CD_ANOEPI = ? "
                + " AND CD_SEMEPI = ? )";

            st = con.prepareStatement(sQuery);

            st.setInt(1, iDefinitivo);
            st.setString(2, param.getLogin());
            st.setTimestamp(3, TSFec);

            st.setString(4, data.getCD_E_NOTIF().trim());
            st.setString(5, data.getCD_ANOEPI().trim());
            st.setString(6, data.getCD_SEMEPI().trim());

            st.executeUpdate();
            st.close();
            st = null;
          }

          fechaR = (String) data.getFC_RECEP().trim();
          fechaN = (String) data.getFC_FECNOTIF().trim();
          ind = fechaR.indexOf("/");
          dia = fechaR.substring(0, ind);
          fechaR = fechaR.substring(ind + 1);
          ind = 0;
          ind = fechaR.indexOf("/");
          mes = fechaR.substring(0, ind);
          ano = fechaR.substring(ind + 1);
          fechaR = ano + mes + dia;

          ind = 0;
          ano = new String();
          mes = new String();
          dia = new String();

          ind = fechaN.indexOf("/");
          dia = fechaN.substring(0, ind);
          fechaN = fechaN.substring(ind + 1);
          ind = 0;
          ind = fechaN.indexOf("/");
          mes = fechaN.substring(0, ind);
          ano = fechaN.substring(ind + 1);
          fechaN = ano + mes + dia;

          /*      sQuery = "DELETE from SIVE_NOTIFEDO "
                          + " WHERE (CD_E_NOTIF = ? AND CD_ANOEPI = ? "
                          + " AND CD_SEMEPI = ? AND FC_RECEP = ? "
                          + " AND FC_FECNOTIF = ? )";
           */

          sQuery = "DELETE from SIVE_NOTIFEDO "
              + " WHERE (CD_E_NOTIF = ? AND CD_ANOEPI = ? "
              + " AND CD_SEMEPI = ? AND TO_CHAR(FC_RECEP, 'YYYYMMDD') = ? "
              + " AND TO_CHAR(FC_FECNOTIF, 'YYYYMMDD') = ? )";

          st = con.prepareStatement(sQuery);

          st.setString(1, data.getCD_E_NOTIF().trim());
          st.setString(2, data.getCD_ANOEPI().trim());
          st.setString(3, data.getCD_SEMEPI().trim());

          /*      dFecha = formater.parse(data.getFC_RECEP().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(4, sqlFec);
                dFecha = formater.parse(data.getFC_FECNOTIF().trim());
                sqlFec = new java.sql.Date(dFecha.getTime());
                st.setDate(5, sqlFec);
           */

          st.setString(4, fechaR);
          st.setString(5, fechaN);

          st.executeUpdate();
          st.close();
          st = null;

          listaSalida = new CLista();
          listaSalida.addElement(data);

          break;
        case servletSELECT_SI_RS:

          //listaBorrar : lista de posibles a borrar
          listaSalida = new CLista();
          listaSalida = listaNotificaciones;

          /*
                 for (i=0; i < listaBorrar.size(); i++){
             dataEntrada = (DataNotificaciones) listaBorrar.elementAt(i);
             DataNotificaciones dataSalida = new DataNotificaciones(
                    dataEntrada.getCD_E_NOTIF(), "", "", "",
                    dataEntrada.getCD_ANOEPI(), dataEntrada.getCD_SEMEPI(),
                     "", "", "", "",
                    dataEntrada.getFC_RECEP() , dataEntrada.getFC_FECNOTIF(),
                    dataEntrada.getCD_OPE_NOTIFEDO(),
                    dataEntrada.getFC_ULTACT_NOTIFEDO());
             //vamos incluyendo lo que falta
                 //NOTIFEDO!!!!!
                 //!!!!!!!!!!!!!
              sQuery = "SELECT NM_NNOTIFR "+
                " FROM SIVE_NOTIFEDO WHERE "+
                " CD_E_NOTIF = ? AND CD_ANOEPI = ? "+
                " AND CD_SEMEPI = ? AND "+
                " FC_RECEP = ? AND FC_FECNOTIF = ? ";
            st = con.prepareStatement(sQuery);
            st.setString(1, dataEntrada.getCD_E_NOTIF().trim());
            st.setString(2, dataEntrada.getCD_ANOEPI().trim());
            st.setString(3, dataEntrada.getCD_SEMEPI().trim());
            dFecha = formater.parse(dataEntrada.getFC_RECEP().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(4, sqlFec);
            dFecha = formater.parse(dataEntrada.getFC_FECNOTIF().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(5, sqlFec);
            rs = st.executeQuery();
            while (rs.next()) {
               dataSalida.NM_NNOTIFR = rs.getString("NM_NNOTIFR"); //si nulo, null
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
                 //NOTIF_SEM!!!!!
                 //!!!!!!!!!!!!!
            sQuery = "SELECT NM_NNOTIFT, NM_NTOTREAL "+
                " FROM SIVE_NOTIF_SEM WHERE "+
                " CD_E_NOTIF = ? AND CD_ANOEPI = ? "+
                " AND CD_SEMEPI = ? ";
            st = con.prepareStatement(sQuery);
            st.setString(1, dataEntrada.getCD_E_NOTIF().trim());
            st.setString(2, dataEntrada.getCD_ANOEPI().trim());
            st.setString(3, dataEntrada.getCD_SEMEPI().trim());
            rs = st.executeQuery();
            while (rs.next()) {
               dataSalida.NM_NNOTIFT = rs.getString("NM_NNOTIFT"); //si nulo, null
               dataSalida.NM_NTOTREAL = rs.getString("NM_NTOTREAL"); //si nulo, null
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
                 //EQUIPO!!!!!!!
                 //!!!!!!!!!!!!!
                 sQuery = "SELECT CD_CENTRO, CD_NIVEL_1, CD_NIVEL_2, CD_ZBS "+
                " FROM SIVE_E_NOTIF WHERE "+
                " CD_E_NOTIF = ? ";
            st = con.prepareStatement(sQuery);
            st.setString(1, dataEntrada.getCD_E_NOTIF().trim());
            rs = st.executeQuery();
            while (rs.next()) {
                 dataSalida.CD_NIVEL_1 = rs.getString("CD_NIVEL_1");
                 dataSalida.CD_NIVEL_2 = rs.getString("CD_NIVEL_2");
                 dataSalida.CD_ZBS = rs.getString("CD_ZBS");
                 dataSalida.CD_CENTRO =  rs.getString("CD_CENTRO");
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            listaSalida.addElement(dataSalida);
                 } //for
           */
          break;
        case servletBORRAR_NO_RS:

          //borrar notifedo
          String notifedo = new Integer(listaBorrar.size()).toString();

          listaSalida.addElement(notifedo); //las notifedo que borro
          for (i = 0; i < listaBorrar.size(); i++) {

            dataEntrada = (DataNotificaciones) listaBorrar.elementAt(i);

            sQuery = "DELETE from SIVE_NOTIFEDO "
                + " WHERE (CD_E_NOTIF = ? AND CD_ANOEPI = ? "
                + " AND CD_SEMEPI = ? AND FC_RECEP = ? "
                + " AND FC_FECNOTIF = ? )";

            st = con.prepareStatement(sQuery);

            st.setString(1, dataEntrada.getCD_E_NOTIF().trim());
            st.setString(2, dataEntrada.getCD_ANOEPI().trim());
            st.setString(3, dataEntrada.getCD_SEMEPI().trim());

            dFecha = formater.parse(dataEntrada.getFC_RECEP().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(4, sqlFec);
            dFecha = formater.parse(dataEntrada.getFC_FECNOTIF().trim());
            sqlFec = new java.sql.Date(dFecha.getTime());
            st.setDate(5, sqlFec);

            st.executeUpdate();
            st.close();
            st = null;

          } //for

          //ESTUDIO LOS NOTIF_SEM: borrar todos los notif_sem
          //sin notifedo cuyo equipo No proceda cobertura
          listaBorrar = new CLista();

          sQuery = "SELECT a.CD_E_NOTIF, a.CD_ANOEPI, a.CD_SEMEPI " +
              " FROM SIVE_NOTIFEDO a, SIVE_E_NOTIF b, SIVE_C_NOTIF c " +
              " WHERE a.CD_E_NOTIF = b.CD_E_NOTIF AND " +
              " b.CD_CENTRO =  c.CD_CENTRO AND c.IT_COBERTURA = ?";

          st = con.prepareStatement(sQuery);
          st.setString(1, "N");
          rs = st.executeQuery();

          while (rs.next()) {

            dataClave = new DataNotificaciones(
                rs.getString("CD_E_NOTIF"), null, null, null,
                rs.getString("CD_ANOEPI"),
                rs.getString("CD_SEMEPI"), null, null, null,
                null, null, null, null, null);

            listaNotifSem.addElement(dataClave);
          }

          rs.close();
          rs = null;
          st.close();
          st = null;

          //notifedo
          bAlmenosUno = false;
          for (i = 0; i < listaNotifSem.size(); i++) {

            dataClave = (DataNotificaciones) listaNotifSem.elementAt(i);

            sQuery = "SELECT FC_RECEP " +
                " FROM SIVE_NOTIFEDO WHERE " +
                " CD_E_NOTIF = ? AND CD_ANOEPI = ? " +
                " AND CD_SEMEPI = ? ";

            st = con.prepareStatement(sQuery);
            st.setString(1, dataClave.getCD_E_NOTIF().trim());
            st.setString(2, dataClave.getCD_ANOEPI().trim());
            st.setString(3, dataClave.getCD_SEMEPI().trim());

            rs = st.executeQuery();

            while (rs.next()) {
              bAlmenosUno = true;
              break;
            }

            rs.close();
            rs = null;
            st.close();
            st = null;

            if (!bAlmenosUno) { //No hay ninguno
              listaBorrar.addElement(dataClave);
            }
            bAlmenosUno = false;

          } //for

          String notifsem = new Integer(listaBorrar.size()).toString();
          listaSalida.addElement(notifsem); //las notif_sem que borro

          for (i = 0; i < listaBorrar.size(); i++) {

            dataEntrada = (DataNotificaciones) listaBorrar.elementAt(i);

            sQuery = "DELETE from SIVE_NOTIF_SEM "
                + " WHERE (CD_E_NOTIF = ? AND CD_ANOEPI = ? "
                + " AND CD_SEMEPI = ? )";

            st = con.prepareStatement(sQuery);

            st.setString(1, dataEntrada.getCD_E_NOTIF().trim());
            st.setString(2, dataEntrada.getCD_ANOEPI().trim());
            st.setString(3, dataEntrada.getCD_SEMEPI().trim());

            st.executeUpdate();
            st.close();
            st = null;

          } //for

          break; //ppal

          //////////////

          /////////////

      } //fin switch

      //valida la transacci�n
      con.commit();

    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return listaSalida;

  } //fin doWork

//*********** Perfil 3
   private boolean getPermisoPerfil_3(Connection con, String strOPE,
                                      Vector listaCD_E_NOTIF) {

     PreparedStatement stAut = null;
     String queryAut = null;
     ResultSet rsAut = null;

     StringTokenizer stkEquipos = null;
     String strInEquipos = null;
     Vector listaClaves = null;
     int j = 0;
     boolean bPermisos = true;

     for (int i = 0; i < listaCD_E_NOTIF.size() && bPermisos; i++) {
       stkEquipos = new StringTokenizer( (String) listaCD_E_NOTIF.elementAt(i),
                                        ",");
       strInEquipos = new String();
       listaClaves = new Vector();

       while (stkEquipos.hasMoreTokens()) {
         listaClaves.addElement(stkEquipos.nextToken());
       }

       for (j = 0; j < listaClaves.size() - 1; j++) {
         strInEquipos = strInEquipos + "?,";
       }
       strInEquipos = strInEquipos + "?";

       queryAut = new String(" select CD_NIVEL_1 from SIVE_NIVEL1_S where " +
           " CD_NIVEL_1 in ( select CD_NIVEL_1 from SIVE_E_NOTIF where " +
           " CD_E_NOTIF in (" + strInEquipos + "))" +
           " and " +
           //" CD_NIVEL_1 not in ( select CD_NIVEL_1 from SIVE_AUTORIZACIONES where " +
           " CD_NIVEL_1 not in ( select CD_NIVEL_1 from SIVE_AUTORIZACIONES_PISTA where " +
           " CD_USUARIO = ? )");

       try {
         stAut = con.prepareStatement(queryAut);
         for (j = 0; j < listaClaves.size(); j++) {
           stAut.setString(j + 1, (String) listaClaves.elementAt(j));
         }
         stAut.setString(j + 1, strOPE);

         rsAut = stAut.executeQuery();

         if (rsAut.next()) {
           bPermisos = false;
         }

         rsAut.close();
         rsAut = null;

         stAut.close();
         stAut = null;

       }
       catch (Exception e) {}

       if (!bPermisos) {
         break;
       }
     }

     return bPermisos;
   }

//********* Perfil 4
   private boolean getPermisoPerfil_4(Connection con, String strOPE,
                                      Vector listaCD_E_NOTIF) {

     PreparedStatement stAut = null;
     String queryAut = null;
     ResultSet rsAut = null;

     StringTokenizer stkEquipos = null;
     String strInEquipos = null;

     String strCD_NIVEL_1_AUX = null;
     String strCD_NIVEL_2_AUX = null;

     Vector listaClaves = null;
     Vector listaParesNOTIF = null;
     Vector listaParesAUTORIZACION = null;

     int j = 0;
     boolean bPermisos = true;

     for (int i = 0; i < listaCD_E_NOTIF.size() && bPermisos; i++) {
       listaParesNOTIF = new Vector();
       listaParesAUTORIZACION = new Vector();

       stkEquipos = new StringTokenizer( (String) listaCD_E_NOTIF.elementAt(i),
                                        ",");
       strInEquipos = new String();
       listaClaves = new Vector();

       while (stkEquipos.hasMoreTokens()) {
         listaClaves.addElement(stkEquipos.nextToken());
       }

       for (j = 0; j < listaClaves.size() - 1; j++) {
         strInEquipos = strInEquipos + "?,";
       }
       strInEquipos = strInEquipos + "?";

       queryAut = new String(
           " select CD_NIVEL_1, CD_NIVEL_2 from SIVE_E_NOTIF where " +
           " CD_E_NOTIF in (" + strInEquipos + ")");

       try {
         stAut = con.prepareStatement(queryAut);
         for (j = 0; j < listaClaves.size(); j++) {
           stAut.setString(j + 1, (String) listaClaves.elementAt(j));
         }

         rsAut = stAut.executeQuery();

         while (rsAut.next()) {
           listaParesNOTIF.addElement(rsAut.getString("CD_NIVEL_1") + "-" +
                                      rsAut.getString("CD_NIVEL_2"));
         }

         rsAut.close();
         rsAut = null;

         stAut.close();
         stAut = null;

         //queryAut = new String(" select CD_NIVEL_1, CD_NIVEL_2 from SIVE_AUTORIZACIONES where " +
         //           " CD_USUARIO = ? ");
         queryAut = new String(
             " select CD_NIVEL_1, CD_NIVEL_2 from SIVE_AUTORIZACIONES_PISTA where " +
             " CD_USUARIO = ? ");

         stAut = con.prepareStatement(queryAut);
         stAut.setString(1, strOPE);

         rsAut = stAut.executeQuery();

         while (rsAut.next()) {
           strCD_NIVEL_1_AUX = rsAut.getString("CD_NIVEL_1");
           strCD_NIVEL_2_AUX = rsAut.getString("CD_NIVEL_2");
           if (strCD_NIVEL_1_AUX != null && strCD_NIVEL_2_AUX != null) {
             listaParesAUTORIZACION.addElement(strCD_NIVEL_1_AUX + "-" +
                                               strCD_NIVEL_2_AUX);
           }
         }

         rsAut.close();
         rsAut = null;

         stAut.close();
         stAut = null;

         for (j = 0; j < listaParesNOTIF.size() && bPermisos; j++) {
           if (!listaParesAUTORIZACION.contains(listaParesNOTIF.elementAt(j))) {
             bPermisos = false;
           }
         }

       }
       catch (Exception e) {
         bPermisos = false;
       }

     }

     return bPermisos;
   }

} //fin de la clase
