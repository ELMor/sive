package notlimpiar;

import java.util.ResourceBundle;

import capp.CApp;

public class notcomenzar
    extends CApp {

  ResourceBundle res;

  public void init() {
    super.init();

  }

  public void start() {
    res = ResourceBundle.getBundle("notlimpiar.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    PanelLimpiar miPanel = new PanelLimpiar(a);
    VerPanel("", miPanel);
  }

}