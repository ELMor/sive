/*
 * Clase: SrvRespTuber
 * Paquete: infproto
 * Hereda: SrvDebugGeneral
 * Autor: Emilio Postigo Riancho
 * Fecha Inicio: 04/01/2000
 * Descripci�n: Servlet encargado de efectuar el mantenimiento de
               protocolos en Tuberculosis y de gestionar los conflictos
               generados por las diferentes respuestas de varios notificadores.
 *
 * Modificacion 19/06/2000 JLT
 * Modificacion realizada para conseguir que cuando un notificador modifique su
     * protocolo, las preguntas nuevas que conteste o las que modifique (y solo esas)
 * se reflejen en el protocolo general (sive_respedo), y todas las respuestas en el
 * protocolo particular (sive_movrespedo)
 * en el caso de un notificador nuevo, se funciona como antes
 */

package infproto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;

import capp.CLista;
import comun.Fechas;
import comun.SrvDebugGeneral;
import comun.constantes;
import tuberculosis.datos.notiftub.DatNotifTub;

public class SrvRespTuber
    extends SrvDebugGeneral {

  // Modos de funcionamiento del servlet
  private final int modoALTA = constantes.modoALTA;
  private final int modoALTA_SB = constantes.modoALTA_SB;
  private final int modoMODIFICACION = constantes.modoMODIFICACION;
  private final int modoMODIFICACION_SB = constantes.modoMODIFICACION_SB;

  // modificacion para actualizar prot
  private final int modoACTUALIZARPROT = constantes.modoACTUALIZARPROT;

  // modificacion para vaciar protocolo
  private final int modoVACIARPROT = constantes.modoVACIARPROT;

  // objetos JDBC
  Connection conexion = null;

  // Para almacenar datos temporalmente
  private Vector vClavesPreguntas = null;

  private Vector vparamovrespedo = null;
//  private Vector vparamovrespedonotif = null;

  // valores del equipo notificador completo
  DatNotifTub dtNotificador = null;

  // modificacion momentanea
  private int modoOperacion;

  // Estados del protocolo
  private Vector vEstadoInicial = null;
  private Vector vEstadoFinal = null;

  java.sql.Date dFechaAsignacion = null;

  // modificacion 19/06/2000
  Vector vdrtRespedo = null;
  Vector vdrtMovrespedo = null;

  Vector Vaux2 = null;
  Vector Vaux = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {
    // Lista de valores de salida
    CLista clRetorno = null;

    // Valores de entrada
    CLista cListaEntrada = null;

    // Para saber si se bloquea o no
    boolean bBloqueo = true;

    // modificacion
    modoOperacion = opmode;

    if (super.con != null) {
      conexion = super.con; // Se est� en modo debug
    }
    else {
      conexion = openConnection(); // Modo real
    }

    conexion.setAutoCommit(false);

    dFechaAsignacion = new java.sql.Date(new java.util.Date().getTime());
    cListaEntrada = param;

    try {
      switch (opmode) {
        case modoALTA_SB:
          bBloqueo = false;
        case modoALTA:
          cListaEntrada = updateCaso(cListaEntrada, bBloqueo);
          clRetorno = insertDatos(cListaEntrada);
          break;
        case modoMODIFICACION_SB:
          bBloqueo = false;
        case modoMODIFICACION:
          cListaEntrada = updateCaso(cListaEntrada, bBloqueo);
          clRetorno = updateDatos(cListaEntrada);
          break;

          // modificacion para actualizar prot
        case modoACTUALIZARPROT:
          cListaEntrada = updateCaso(cListaEntrada, bBloqueo);
          clRetorno = updateActProt(cListaEntrada);
          break;

          // modificacion para vaciar protocolo
        case modoVACIARPROT:
          cListaEntrada = updateCaso(cListaEntrada, bBloqueo);
          clRetorno = updateVaciarProt(cListaEntrada);
          break;

      }
      conexion.commit();
    }
    catch (Exception e) {
      conexion.rollback();
      // System_out.println(e.getMessage());
      throw e;
    }
    finally {
      closeConnection(conexion);
    }

    return clRetorno;
  }

  // m�todos privados

  // Bloqueos. Para actualizar SIVE_EDOIND
  private CLista updateCaso(CLista p, boolean bloqueo) throws Exception {
    java.sql.Timestamp ts = new java.sql.Timestamp(new java.util.Date().getTime());

    DatNotifTub dnt = null;

    PreparedStatement st = null;
    String sQuery = null;
    String sQueyBloqueo = null;

    CLista cLista = p;

    dnt = (DatNotifTub) ( (Hashtable) cLista.elementAt(1)).get(constantes.
        DATOSNOTIFICADOR);

    sQuery = " update SIVE_EDOIND set CD_OPE = ?, FC_ULTACT = ? " +
        " where " +
        " NM_EDO = ? ";

    if (bloqueo) {
      sQueyBloqueo = " and CD_OPE = ? and FC_ULTACT = ? ";
    }
    else {
      sQueyBloqueo = "";
    }

    st = conexion.prepareStatement(sQuery + sQueyBloqueo);

    // Datos
    st.setString(1, cLista.getLogin());
    st.setTimestamp(2, ts);

    // Clave
    st.setInt(3, new Integer(dnt.getNM_EDO()).intValue());

    // Bloqueo
    if (bloqueo) {
      st.setString(4, dnt.getCD_OPE_EDOIND());
      st.setTimestamp(5, Fechas.string2Timestamp(dnt.getFC_ULTACT_EDOIND()));
    }

    int iActualizados = st.executeUpdate();

    if (bloqueo) {
      if (iActualizados == 0) {
        throw new Exception(constantes.PRE_BLOQUEO + constantes.SIVE_EDOIND);
      }
    }

    dnt.setCD_OPE_EDOIND(cLista.getLogin());
    dnt.setFC_ULTACT_EDOIND(Fechas.timestamp2String(ts));

    Hashtable h = new Hashtable();
    h.put(constantes.DATOSNOTIFICADOR, dnt);

    cLista.setElementAt(h, 1);

    return cLista;
  }

  // Para rellenar conflictos con respuesta en blanco
  private void insertMovimientosBlancos(DatNotifTub dnt) throws Exception {
    DataConflictoTuber dct = null;

    for (int i = 0; i < vEstadoFinal.size(); i++) {
      dct = (DataConflictoTuber) vEstadoFinal.elementAt(i);
      if (dct.getDS_RESPUESTA().trim().equals("")) {
        PreparedStatement st = null;
        String sQuery = null;

        sQuery = " insert into SIVE_MOVRESPEDO " +
            " (NM_EDO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, FC_RECEP, FC_FECNOTIF, CD_FUENTE, " +
            "  CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, DS_RESPUESTA, IT_CONFLICTO, FC_ASIGN) " +
            " values " +
            " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

        st = conexion.prepareStatement(sQuery);

        // Notificador
        st.setInt(1, new Integer(dnt.getNM_EDO()).intValue());
        st.setString(2, dnt.getCD_E_NOTIF_EDOI());
        st.setString(3, dnt.getCD_ANOEPI_EDOI());
        st.setString(4, dnt.getCD_SEMEPI());
        st.setDate(5,
                   new java.sql.Date(Fechas.string2Date(dnt.getFC_RECEP_EDOI()).getTime()));
        st.setDate(6,
                   new java.sql.Date(Fechas.string2Date(dnt.getFC_NOTIF()).getTime()));
        st.setString(7, dnt.getCD_FUENTE());

        // Datos de movimiento en s�
        st.setString(8, dct.getCD_TSIVE());
        st.setString(9, dct.getCD_MODELO());
        st.setInt(10, dct.getNM_LIN());
        st.setString(11, dct.getCD_PREGUNTA());
        st.setString(12, " ");
        st.setString(13, "S");
        st.setDate(14, new java.sql.Date(new java.util.Date().getTime()));

        st.executeUpdate();

      }
    }
  }

  /*
    // Para establecer los conflictos
       private void estableceConflictos(int iCaso, String equipo) throws Exception {
      DataConflictoTuber dctActual = null;
      DataConflictoTuber dctInicial = null;
      for (int i = 0; i < vEstadoFinal.size(); i++) {
        dctActual = (DataConflictoTuber) vEstadoFinal.elementAt(i);
        dctInicial = estaDctEnVector(dctActual, vEstadoInicial);
        if (dctInicial != null) {
          // La pregunta ya estaba
          if ( !dctInicial.getEquiposPoseedores().contains(equipo) ) {
            // Nuestro equipo no era su poseedor
       if ( !dctInicial.getDS_RESPUESTA().equals(dctActual.getDS_RESPUESTA()) ) {
              // Las respuestas son distintas
              updateDatosMovimiento(dctActual, "S");
            } else {
              updateDatosMovimiento(dctActual, dctInicial.getIT_CONFLICTO());
            }
          } else {
            updateDatosMovimiento(dctActual, dctInicial.getIT_CONFLICTO());
          }
        }
      }
      // Resuelve los conflictos posibles
      resuelveConflictos(iCaso);
    }
   */

  // Para establecer los conflictos
  private void estableceConflictos(int iCaso, DatNotifTub dtNotificador) throws
      Exception {

    DataConflictoTuber dctActual = null;
    DataConflictoTuber dctInicial = null;

    for (int i = 0; i < vEstadoFinal.size(); i++) {
      dctActual = (DataConflictoTuber) vEstadoFinal.elementAt(i);
      dctInicial = estaDctEnVector(dctActual, vEstadoInicial);
      if (dctInicial != null) {
        // La pregunta ya estaba
        if (!dctInicial.getEquiposPoseedores().contains(dtNotificador.getNM_EDO()) ||
            !dctInicial.getEquiposPoseedores().contains(dtNotificador.
            getCD_E_NOTIF_EDOI()) ||
            !dctInicial.getEquiposPoseedores().contains(dtNotificador.
            getCD_ANOEPI_EDOI()) ||
            !dctInicial.getEquiposPoseedores().contains(dtNotificador.
            getCD_SEMEPI()) ||
            !dctInicial.getEquiposPoseedores().contains(dtNotificador.
            getFC_NOTIF()) ||
            !dctInicial.getEquiposPoseedores().contains(dtNotificador.
            getFC_RECEP_EDOI()) ||
            !dctInicial.getEquiposPoseedores().contains(dtNotificador.
            getCD_FUENTE())) {
          // Nuestro equipo no era su poseedor
          if (!dctInicial.getDS_RESPUESTA().equals(dctActual.getDS_RESPUESTA())) {
            // Las respuestas son distintas
            updateDatosMovimiento(dctActual, "S");
          }
          else {
            updateDatosMovimiento(dctActual, dctInicial.getIT_CONFLICTO());
          }
        }
        else {
          updateDatosMovimiento(dctActual, dctInicial.getIT_CONFLICTO());
        }

      }
    }
    // Resuelve los conflictos posibles
    resuelveConflictos(iCaso);
  }

  // establece conflictos y adem�s compara preguntas actuales e iniciales,
  // aquellas que estaban al principio y no al final, se borran de movrespedo
  private void estableceConflictosaux(int iCaso, DatNotifTub dtNotificador) throws
      Exception {

    DataConflictoTuber dctActual = null;
    DataConflictoTuber dctInicial = null;

    for (int i = 0; i < vEstadoFinal.size(); i++) {
      dctActual = (DataConflictoTuber) vEstadoFinal.elementAt(i);
      dctInicial = estaDctEnVectoraux(dctActual, vEstadoInicial);
      if (dctInicial != null) {
        // La pregunta ya estaba
        if (!dctInicial.getEquiposPoseedores().contains(dtNotificador.getNM_EDO()) ||
            !dctInicial.getEquiposPoseedores().contains(dtNotificador.
            getCD_E_NOTIF_EDOI()) ||
            !dctInicial.getEquiposPoseedores().contains(dtNotificador.
            getCD_ANOEPI_EDOI()) ||
            !dctInicial.getEquiposPoseedores().contains(dtNotificador.
            getCD_SEMEPI()) ||
            !dctInicial.getEquiposPoseedores().contains(dtNotificador.
            getFC_NOTIF()) ||
            !dctInicial.getEquiposPoseedores().contains(dtNotificador.
            getFC_RECEP_EDOI()) ||
            !dctInicial.getEquiposPoseedores().contains(dtNotificador.
            getCD_FUENTE())) {
          // Nuestro equipo no era su poseedor
          if (!dctInicial.getDS_RESPUESTA().equals(dctActual.getDS_RESPUESTA())) {
            // Las respuestas son distintas
            updateDatosMovimiento(dctActual, "S");
          }
          else {
            updateDatosMovimiento(dctActual, dctInicial.getIT_CONFLICTO());
          }
        }
        else {
          updateDatosMovimiento(dctActual, dctInicial.getIT_CONFLICTO());
        }

      }
    }
    // Resuelve los conflictos posibles
    resuelveConflictos(iCaso);
  }

  private void arreglarmovrespedo(int iCaso, Vector vi, CLista p) throws
      Exception {

    DataConflictoTuber dtinicial = null;
    boolean bBorrar = false;
    Vector vtmp = null;
    vtmp = (Vector) p.elementAt(0);
    DataRespTuber dtfinal = null;
    dtfinal = (DataRespTuber) vtmp.elementAt(0);

    // modificacion
    DataConflictoTuber dctmov = new DataConflictoTuber();
    vparamovrespedo = new Vector();
    //vparamovrespedonotif = new Vector();

    for (int i = 0; i < vi.size(); i++) {
      dtinicial = (DataConflictoTuber) vi.elementAt(i);
      for (int j = 0; j < vtmp.size(); j++) {
        dtfinal = (DataRespTuber) vtmp.elementAt(j);
        if (dtinicial.getNM_EDO() == dtfinal.getNM_EDO() &&
            dtinicial.getCD_TSIVE().equals( (String) dtfinal.getCD_TSIVE()) &&
            dtinicial.getCD_PREGUNTA().equals( (String) dtfinal.getCD_PREGUNTA())) {
          bBorrar = false;
          break;
        }
        else {
          bBorrar = true;
        }
      } // 2 for

      if (bBorrar) {
        /////////////////////
        // Para cada pregunta del protocolo se recuperan sus poseedores
        PreparedStatement st = null;
        ResultSet rs = null;
        String sQuery = null;
        //Hashtable hnotif = null;
        //hnotif = new Hashtable();
        trazaLog("entro en if borrar");

        sQuery = " select CD_E_NOTIF, NM_EDO, CD_ANOEPI, CD_SEMEPI, " +
            " FC_RECEP, FC_FECNOTIF, CD_FUENTE, " +
            " IT_CONFLICTO, CD_PREGUNTA, DS_RESPUESTA , CD_TSIVE, " +
            " CD_MODELO, NM_LIN from SIVE_MOVRESPEDO " +
            " where " +
            " NM_EDO = ? and CD_TSIVE = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ?  ";

        st = conexion.prepareStatement(sQuery);

        //for (int i = 0; i < vEstado.size(); i++) {
        //dct = (DataConflictoTuber) vEstado.elementAt(i);

        st.setInt(1, iCaso);
        st.setString(2, dtinicial.getCD_TSIVE());
        st.setString(3, dtinicial.getCD_MODELO());
        st.setInt(4, dtinicial.getNM_LIN());
        st.setString(5, dtinicial.getCD_PREGUNTA());

        rs = st.executeQuery();

        while (rs.next()) {
          trazaLog("entro en while");

          Hashtable hnotif = null;
          hnotif = new Hashtable();
          String sFrecep = null;
          String sFnotif = null;
          sFrecep = (String) Fechas.date2String(rs.getDate("FC_RECEP"));
          sFnotif = (String) Fechas.date2String(rs.getDate("FC_FECNOTIF"));

          //dctmov.addEquipoPoseedor(rs.getString("CD_E_NOTIF"));
          hnotif.put("CD_E_NOTIF", rs.getString("CD_E_NOTIF"));
          //dctmov.addEquipoPoseedor(rs.getString("NM_EDO"));
          hnotif.put("NM_EDO", rs.getString("NM_EDO"));
          //dctmov.addEquipoPoseedor(rs.getString("CD_ANOEPI"));
          hnotif.put("CD_ANOEPI", rs.getString("CD_ANOEPI"));
          trazaLog("paso anoepi");
          //dctmov.addEquipoPoseedor(rs.getString("CD_SEMEPI"));
          hnotif.put("CD_SEMEPI", rs.getString("CD_SEMEPI"));
          //dctmov.addEquipoPoseedor(sFrecep);
          trazaLog("sFrecep" + sFrecep);
          hnotif.put("FC_RECEP", sFrecep);
          //dctmov.addEquipoPoseedor(sFnotif);
          hnotif.put("FC_FECNOTIF", sFnotif);
          trazaLog("paso fechas");
          //dctmov.addEquipoPoseedor(rs.getString("CD_FUENTE"));
          hnotif.put("CD_FUENTE", rs.getString("CD_FUENTE"));
          hnotif.put("IT_CONFLICTO", rs.getString("IT_CONFLICTO"));

          //hnotif.put("CD_PREGUNTA", dtinicial.getCD_PREGUNTA());
          hnotif.put("CD_PREGUNTA", rs.getString("CD_PREGUNTA"));
          hnotif.put("DS_RESPUESTA", rs.getString("DS_RESPUESTA"));
          hnotif.put("CD_TSIVE", rs.getString("CD_TSIVE"));
          hnotif.put("CD_MODELO", rs.getString("CD_MODELO"));
          hnotif.put("NM_LIN", new Integer(rs.getInt("NM_LIN")).toString());
          //hnotif.put("DS_RESPUESTA", (String) dtinicial.getDS_RESPUESTA());
          trazaLog("paso respuestas");

          vparamovrespedo.addElement(hnotif);
          trazaLog("a�ado el elmento al vector");
        }

        /////////////////////
        deletemov(dtinicial);
        //vparamovrespedo.addElement(dtinicial);

      }
      else {
        updatemov(dtfinal);
      }

    } // 1 for

  }

  // Para saber si un DataConflictoTuber est� en un vector
  // de los que empleamos
  private DataConflictoTuber estaDctEnVector(DataConflictoTuber dct, Vector v) {
    DataConflictoTuber dctTemp = null;

    for (int i = 0; i < v.size() && dctTemp == null; i++) {
      if (dct.equalsPregunta( (DataConflictoTuber) v.elementAt(i))) {
        dctTemp = (DataConflictoTuber) v.elementAt(i);
      }
    }

    return dctTemp;
  }

  private DataConflictoTuber estaDctEnVectoraux(DataConflictoTuber dct,
                                                Vector v) {
    DataConflictoTuber dctTemp = null;

    for (int i = 0; i < v.size() && dctTemp == null; i++) {
      if (dct.equalsPreguntaaux( (DataConflictoTuber) v.elementAt(i))) {
        dctTemp = (DataConflictoTuber) v.elementAt(i);
      }
    }

    return dctTemp;
  }

  // Resoluci�n autom�tica de conflictos
  private void resuelveConflictos(int iCaso) throws Exception {
    PreparedStatement st = null;
    PreparedStatement stCorrectas = null;

    ResultSet rs = null;
    ResultSet rsCorrectas = null;

    String sQuery = null;
    String sQueryCorrectas = null;

    Vector vPreguntas = null;
    DataConflictoTuber dct = null;

    vPreguntas = new Vector();

    // Se recuperan las claves de las preguntas del caso
    sQuery = " select DISTINCT re.CD_TSIVE, re.CD_MODELO, re.NM_LIN, re.CD_PREGUNTA, re.DS_RESPUESTA " +
        " from SIVE_RESP_EDO re, SIVE_MOVRESPEDO mre where re.NM_EDO = ? and mre.IT_CONFLICTO = ? and " +
        " re.CD_TSIVE = mre.CD_TSIVE and re.CD_MODELO = mre.CD_MODELO and " +
        " re.NM_LIN = mre.NM_LIN and re.CD_PREGUNTA = mre.CD_PREGUNTA ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, iCaso);
    st.setString(2, "S");

    rs = st.executeQuery();

    while (rs.next()) {
      dct = new DataConflictoTuber(iCaso,
                                   rs.getString("CD_TSIVE"),
                                   rs.getString("CD_MODELO"),
                                   rs.getInt("NM_LIN"),
                                   rs.getString("CD_PREGUNTA"),
                                   rs.getString("DS_RESPUESTA"));

      vPreguntas.addElement(dct);
    }

    if (vPreguntas.size() > 0) {
      // Para cada pregunta del protocolo se recupera el n� de respuestas
      // existentes en SIVE_MOVRESPEDO para la misma
      sQuery = " select Count(*) NM_TOTAL_RESPUESTAS from SIVE_MOVRESPEDO " +
          " where " +
          " NM_EDO = ? and CD_TSIVE = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ? ";

      st = conexion.prepareStatement(sQuery);

      sQueryCorrectas =
          " select Count(*) NM_TOTAL_RESPUESTAS from SIVE_MOVRESPEDO " +
          " where " +
          " NM_EDO = ? and CD_TSIVE = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ? and " +
          " DS_RESPUESTA = ? ";

      stCorrectas = conexion.prepareStatement(sQueryCorrectas);

      for (int i = 0; i < vPreguntas.size(); i++) {
        dct = (DataConflictoTuber) vPreguntas.elementAt(i);

        st.setInt(1, iCaso);
        st.setString(2, dct.getCD_TSIVE());
        st.setString(3, dct.getCD_MODELO());
        st.setInt(4, dct.getNM_LIN());
        st.setString(5, dct.getCD_PREGUNTA());

        rs = st.executeQuery();

        stCorrectas.setInt(1, iCaso);
        stCorrectas.setString(2, dct.getCD_TSIVE());
        stCorrectas.setString(3, dct.getCD_MODELO());
        stCorrectas.setInt(4, dct.getNM_LIN());
        stCorrectas.setString(5, dct.getCD_PREGUNTA());
        stCorrectas.setString(6, dct.getDS_RESPUESTA());

        rsCorrectas = stCorrectas.executeQuery();

        if (rs.next() && rsCorrectas.next()) {
          if (rs.getInt("NM_TOTAL_RESPUESTAS") ==
              rsCorrectas.getInt("NM_TOTAL_RESPUESTAS")) {
            // No hay conflicto --> se elimina
            updateDatosMovimiento(dct, "N");
          }
        }
      }
    }

  }

  // Recopilaci�n de datos de estado del protocolo
  private Vector cargaEstado(int iCaso) throws Exception {
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    // variables para guardar con formato String las
    // fechas de recepcion y notificacion
    String sFrecep = null;
    String sFnotif = null;

    Vector vEstado = null;
    DataConflictoTuber dct = null;

    vEstado = new Vector();

    // Se recuperan las claves de las preguntas del caso
    sQuery = " select CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, DS_RESPUESTA " +
        " from SIVE_RESP_EDO where NM_EDO = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, iCaso);

    rs = st.executeQuery();

    while (rs.next()) {
      dct = new DataConflictoTuber(iCaso,
                                   rs.getString("CD_TSIVE"),
                                   rs.getString("CD_MODELO"),
                                   rs.getInt("NM_LIN"),
                                   rs.getString("CD_PREGUNTA"),
                                   rs.getString("DS_RESPUESTA"));

      vEstado.addElement(dct);

    }

    // Para cada pregunta del protocolo se recuperan sus poseedores
//    sQuery = " select CD_E_NOTIF, " + // y el resto de la clave...
    sQuery = " select CD_E_NOTIF, NM_EDO, CD_ANOEPI, CD_SEMEPI, " +
        " FC_RECEP, FC_FECNOTIF, CD_FUENTE, " +
        " IT_CONFLICTO from SIVE_MOVRESPEDO " +
        " where " +
        " NM_EDO = ? and CD_TSIVE = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ? and DS_RESPUESTA = ? ";

    st = conexion.prepareStatement(sQuery);

    for (int i = 0; i < vEstado.size(); i++) {
      dct = (DataConflictoTuber) vEstado.elementAt(i);

      st.setInt(1, iCaso);
      st.setString(2, dct.getCD_TSIVE());
      st.setString(3, dct.getCD_MODELO());
      st.setInt(4, dct.getNM_LIN());
      st.setString(5, dct.getCD_PREGUNTA());
      st.setString(6, dct.getDS_RESPUESTA());

      rs = st.executeQuery();

      while (rs.next()) {

        sFrecep = null;
        sFnotif = null;
        sFrecep = (String) Fechas.date2String(rs.getDate("FC_RECEP"));
        sFnotif = (String) Fechas.date2String(rs.getDate("FC_FECNOTIF"));

        dct.addEquipoPoseedor(rs.getString("CD_E_NOTIF"));
        dct.addEquipoPoseedor(rs.getString("NM_EDO"));
        dct.addEquipoPoseedor(rs.getString("CD_ANOEPI"));
        dct.addEquipoPoseedor(rs.getString("CD_SEMEPI"));
//        dct.addEquipoPoseedor(rs.getString("FC_RECEP"));
        dct.addEquipoPoseedor(sFrecep);
//        dct.addEquipoPoseedor(rs.getString("FC_FECNOTIF"));
        dct.addEquipoPoseedor(sFnotif);
        dct.addEquipoPoseedor(rs.getString("CD_FUENTE"));
        dct.setIT_CONFLICTO(rs.getString("IT_CONFLICTO"));
      }

    }

    return vEstado;
  }

  // Recopilaci�n de datos de estado del protocolo
  private Vector cargaRespedo(int iCaso) throws Exception {
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;
    Vector vEstado = null;
    DataRespTuber drtInicial = null;

    vEstado = new Vector();

    // Se recuperan las claves de las preguntas del caso
    sQuery = " select CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, DS_RESPUESTA " +
        " from SIVE_RESP_EDO where NM_EDO = ? " +
        " order by CD_MODELO ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, iCaso);

    rs = st.executeQuery();

    while (rs.next()) {
      drtInicial = new DataRespTuber(iCaso,
                                     rs.getString("CD_MODELO"),
                                     rs.getInt("NM_LIN"),
                                     rs.getString("CD_PREGUNTA"),
                                     rs.getString("DS_RESPUESTA"),
                                     " ", " ");

      vEstado.addElement(drtInicial);

    }

    return vEstado;

  }

  private Vector cargaMovrespedo(int iCaso, DatNotifTub dtNotificador) throws
      Exception {
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;
    Vector vEstado = null;

    DataRespTuber drt = null;

    vEstado = new Vector();

    // Se recuperan las claves de las preguntas de los movimientos
    // del notificador
    sQuery = " select CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA , DS_RESPUESTA from SIVE_MOVRESPEDO where NM_EDO = ? and CD_E_NOTIF = ? and "
        + " CD_ANOEPI = ? and CD_SEMEPI = ? and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? "
        + " order by CD_MODELO ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, iCaso);
    st.setString(2, dtNotificador.getCD_E_NOTIF_EDOI());
    st.setString(3, dtNotificador.getCD_ANOEPI_EDOI());
    st.setString(4, dtNotificador.getCD_SEMEPI());
    st.setDate(5,
               new java.sql.Date(Fechas.string2Date(dtNotificador.getFC_RECEP_EDOI()).
                                 getTime()));
    st.setDate(6,
               new java.sql.Date(Fechas.string2Date(dtNotificador.getFC_NOTIF()).
                                 getTime()));
    st.setString(7, dtNotificador.getCD_FUENTE());

    rs = st.executeQuery();

    while (rs.next()) {
      drt = new DataRespTuber();
      drt.setCD_TSIVE(rs.getString("CD_TSIVE"));
      drt.setNM_EDO(iCaso);
      drt.setCD_MODELO(rs.getString("CD_MODELO"));
      drt.setNM_LINEA(rs.getInt("NM_LIN"));
      drt.setCD_PREGUNTA(rs.getString("CD_PREGUNTA"));
      drt.setDS_RESPUESTA(rs.getString("DS_RESPUESTA"));

      vEstado.addElement(drt);
    }

    return vEstado;

  }

  // Borrado de los movimientos asociados a un caso EDOIND y
  // un equipo notificador
  private void deleteDatosMovimientos(int iCaso, DatNotifTub dtNotificador) throws
      Exception {
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    DataRespTuber drt = null;

    vClavesPreguntas = new Vector();

    // Se recuperan las claves de las preguntas de los movimientos
    // del notificador
    sQuery = " select CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA from SIVE_MOVRESPEDO where NM_EDO = ? and CD_E_NOTIF = ? and "
        + " CD_ANOEPI = ? and CD_SEMEPI = ? and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, iCaso);

    st.setString(2, dtNotificador.getCD_E_NOTIF_EDOI());
    st.setString(3, dtNotificador.getCD_ANOEPI_EDOI());
    st.setString(4, dtNotificador.getCD_SEMEPI());
    st.setDate(5,
               new java.sql.Date(Fechas.string2Date(dtNotificador.getFC_RECEP_EDOI()).
                                 getTime()));
    st.setDate(6,
               new java.sql.Date(Fechas.string2Date(dtNotificador.getFC_NOTIF()).
                                 getTime()));
    st.setString(7, dtNotificador.getCD_FUENTE());

    rs = st.executeQuery();

    while (rs.next()) {
      drt = new DataRespTuber();
      drt.setCD_TSIVE(rs.getString("CD_TSIVE"));
      drt.setNM_EDO(iCaso);
      drt.setCD_MODELO(rs.getString("CD_MODELO"));
      drt.setNM_LINEA(rs.getInt("NM_LIN"));
      drt.setCD_PREGUNTA(rs.getString("CD_PREGUNTA"));

      vClavesPreguntas.addElement(drt);
    }

    // Se borran todos los movimientos del caso/equipo
    sQuery =
        " delete from SIVE_MOVRESPEDO where NM_EDO = ? and CD_E_NOTIF = ? and "
        + " CD_ANOEPI = ? and CD_SEMEPI = ? and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, iCaso);
    st.setString(2, dtNotificador.getCD_E_NOTIF_EDOI());
    st.setString(3, dtNotificador.getCD_ANOEPI_EDOI());
    st.setString(4, dtNotificador.getCD_SEMEPI());
    st.setDate(5,
               new java.sql.Date(Fechas.string2Date(dtNotificador.getFC_RECEP_EDOI()).
                                 getTime()));
    st.setDate(6,
               new java.sql.Date(Fechas.string2Date(dtNotificador.getFC_NOTIF()).
                                 getTime()));
    st.setString(7, dtNotificador.getCD_FUENTE());

    try {
      int c = st.executeUpdate();
    }
    catch (java.sql.SQLException e) {
    }

    // Se borran aquellas respuestas del protocolo que sea posible
    sQuery = " delete from SIVE_RESP_EDO where " +
        " CD_TSIVE = ? and NM_EDO = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ? ";

    for (int i = 0; i < vClavesPreguntas.size(); i++) {
      st = conexion.prepareStatement(sQuery);
      drt = (DataRespTuber) vClavesPreguntas.elementAt(i);

      st.setString(1, drt.getCD_TSIVE());
      st.setInt(2, drt.getNM_EDO());
      st.setString(3, drt.getCD_MODELO());
      st.setInt(4, drt.getNM_LINEA());
      st.setString(5, drt.getCD_PREGUNTA());

      try {
        int con = st.executeUpdate();
      }
      catch (java.sql.SQLException e) {

      }
    }

    st = null;
  }

  private void arreglarrespedoAUX(int iCaso, CLista p) throws Exception {
    PreparedStatement st = null;
    PreparedStatement st2 = null;
    ResultSet rs = null;
    ResultSet rs2 = null;
    String sQuery = null;
    String sQuery2 = null;

    Vector vp = null;
    DataRespTuber dr = null;

    vp = (Vector) p.elementAt(0);
    dr = (DataRespTuber) vp.elementAt(0);

    // Se borran aquellas respuestas del protocolo que sea posible
    /*    sQuery = " delete from SIVE_RESP_EDO where " +
                 "  NM_EDO = ? and CD_MODELO <> ? and " +
                 " CD_MODELO not in (select CD_MODELO from SIVE_MODELO " +
                 " where CD_TSIVE = ? and IT_OK = ? ) "; */

    sQuery = " delete from SIVE_RESP_EDO where " +
        "  NM_EDO = ? and CD_PREGUNTA NOT IN (SELECT CD_PREGUNTA " +
        " from  SIVE_LINEA_ITEM where CD_MODELO = ?) ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, iCaso);
    st.setString(2, dr.getCD_MODELO());

    /*    st.setInt(1, iCaso );
        st.setString(2, dr.getCD_MODELO());
        st.setString(3, dr.getCD_TSIVE());
        st.setString(4, "S"); */

    int l = st.executeUpdate();
    trazaLog("arreglarespedo l = " + l);

    sQuery2 = " select CD_PREGUNTA FROM SIVE_RESP_EDO WHERE " +
        " NM_EDO = ? AND CD_MODELO <> ? ";

    st2 = conexion.prepareStatement(sQuery2);
    st2.setInt(1, iCaso);
    st2.setString(2, dr.getCD_MODELO());

    rs = st2.executeQuery();

    Vector vpregunta = null;
    vpregunta = new Vector();

    while (rs.next()) {

      String spregunta = (String) rs.getString("CD_PREGUNTA");
      trazaLog("spregunta " + spregunta);
      vpregunta.addElement(spregunta);

    } // while

    for (int i = 0; i < vpregunta.size(); i++) {

      String spregunta2 = (String) vpregunta.elementAt(i);

      sQuery2 =
          " select NM_LIN from SIVE_LINEA_ITEM where CD_PREGUNTA = ? AND " +
          " CD_MODELO = ? ";

      st2 = conexion.prepareStatement(sQuery2);

      st2.setString(1, spregunta2);
      st2.setString(2, dr.getCD_MODELO());

      rs2 = st2.executeQuery();

      int slinea = 0;
      if (rs2.next()) {
        slinea = rs2.getInt("NM_LIN");
        trazaLog("slinea " + slinea);
      }

      sQuery2 = " update SIVE_RESP_EDO set CD_MODELO = ? , NM_LIN = ?  where " +
          "  NM_EDO = ? and CD_PREGUNTA = ? ";

      st2 = conexion.prepareStatement(sQuery2);

      st2.setString(1, dr.getCD_MODELO());
      st2.setInt(2, slinea);
      st2.setInt(3, iCaso);
      st2.setString(4, spregunta2);

      int x = st2.executeUpdate();
      trazaLog("el update x = " + x);

    } // for

  }

  ///////////
  private void arreglarrespedo(int iCaso, CLista p, DatNotifTub dtNotificador) throws
      Exception {
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    Vector vp = null;
    DataRespTuber dr = null;

    vp = (Vector) p.elementAt(0);
    dr = (DataRespTuber) vp.elementAt(0);

    // Se borran aquellas respuestas del protocolo que sea posible
    sQuery = " delete from SIVE_RESP_EDO where " +
        "  NM_EDO = ? and CD_MODELO <> ? and " +
        " CD_MODELO not in (select CD_MODELO from SIVE_MODELO " +
        " where CD_TSIVE = ? and IT_OK = ? ) ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, iCaso);
    st.setString(2, dr.getCD_MODELO());
    st.setString(3, dr.getCD_TSIVE());
    st.setString(4, "S");

    int l = st.executeUpdate();

    ///////////////
    // posiblemente sea conveniente sacar todo el data para luego obtener
    // la informaci�n necesaria para insertar en las tablas
    sQuery = " select CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, DS_RESPUESTA " +
        " from SIVE_RESP_EDO where NM_EDO = ? and CD_MODELO = ? and CD_TSIVE = ? " +
        " order by CD_MODELO ";

    st = conexion.prepareStatement(sQuery);

    Vector vresp = null;
    vresp = new Vector();
    DataRespTuber dtresp = new DataRespTuber();

    st.setInt(1, iCaso);
    st.setString(2, dr.getCD_MODELO());
    st.setString(3, dr.getCD_TSIVE());

    rs = st.executeQuery();

    while (rs.next()) {
      dtresp = new DataRespTuber(iCaso,
                                 rs.getString("CD_MODELO"),
                                 rs.getInt("NM_LIN"),
                                 rs.getString("CD_PREGUNTA"),
                                 rs.getString("DS_RESPUESTA"),
                                 " ", " ");

      vresp.addElement(dtresp);

    }

    trazaLog("paso el primer select");

    sQuery = " select CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA " +
        " from SIVE_LINEA_ITEM where CD_MODELO = ? and CD_TSIVE = ? ";

    st = conexion.prepareStatement(sQuery);

    Vector vlineaitem = null;
    vlineaitem = new Vector();
    DataRespTuber dtlineaitem = new DataRespTuber();

    st.setString(1, dr.getCD_MODELO());
    st.setString(2, dr.getCD_TSIVE());

    rs = st.executeQuery();

    while (rs.next()) {
      dtlineaitem = new DataRespTuber(iCaso,
                                      rs.getString("CD_MODELO"),
                                      rs.getInt("NM_LIN"),
                                      rs.getString("CD_PREGUNTA"),
                                      " ",
                                      " ", " ");

      vlineaitem.addElement(dtlineaitem);

    }

    DataRespTuber d = new DataRespTuber();
    Vector vrespant = null;
    vrespant = new Vector();
    for (int i = 0; i < vdrtRespedo.size(); i++) {
      d = (DataRespTuber) vdrtRespedo.elementAt(i);
      String srespedo = (String) d.getCD_PREGUNTA();
      vrespant.addElement(srespedo);
      trazaLog("srespedo = " + srespedo);
    }

    DataRespTuber d1 = null;
    d1 = new DataRespTuber(); // datos de vlineaitem
    DataRespTuber d2 = null;
    d2 = new DataRespTuber(); // datos de respedo despues
    DataRespTuber d3 = null;
    d3 = new DataRespTuber(); // datos de respedo antes
    DataConflictoTuber dparamovrespedo = null;
    dparamovrespedo = new DataConflictoTuber();
    Hashtable hnotif = null;
    hnotif = new Hashtable();

    boolean bseguir = false;

    // se hacen las comparaciones
    //comparo d1 con d2, si alguna pregunta no coincide, se compara con
    // d3, y si hay coincidencia de pregunta, la inserto en respedo
    for (int i = 0; i < vlineaitem.size(); i++) {
      bseguir = true;
      d1 = (DataRespTuber) vlineaitem.elementAt(i);
      trazaLog("en el 1 for");

      for (int j = 0; j < vresp.size(); j++) {
        d2 = (DataRespTuber) vresp.elementAt(j);
        if ( ( (String) d1.getCD_PREGUNTA()).equals( (String) d2.getCD_PREGUNTA())) {
          //if( (spregunta1).equals( spregunta2 )) {
          bseguir = false;
          break;
        }
        else {
          bseguir = true;
        }
        trazaLog("en el 2 for");
      } // 2 for

      if (bseguir) {

        for (int k = 0; k < vdrtRespedo.size(); k++) {
          trazaLog("en el 3 for");
          d3 = (DataRespTuber) vdrtRespedo.elementAt(k);
          //if( (spregunta1).equals( spregunta3 )) {
          if ( ( (String) d1.getCD_PREGUNTA()).equals( (String) d3.
              getCD_PREGUNTA())) {
            // insertar en respedo con el modelo y linea
            // de vlineaitem pero con la respuesta de vrespant
            // y comparar el dato a insertar con el dato borrado
            // de movrespedo previamente, por si es necesario insertarlo
            // de nuevo en esa tabla(primero insertar el padre
            // y luego el hijo)

            sQuery = " insert into SIVE_RESP_EDO " +
                " (CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, NM_EDO, DS_RESPUESTA) " +
                " values " +
                " (?, ?, ?, ?, ?, ?) ";

            st = conexion.prepareStatement(sQuery);

            st.setString(1, d1.getCD_TSIVE());
            st.setString(2, d1.getCD_MODELO());
            st.setInt(3, d1.getNM_LINEA());
            st.setString(4, d1.getCD_PREGUNTA());
            st.setInt(5, d3.getNM_EDO());
            st.setString(6, d3.getDS_RESPUESTA());

            int g = st.executeUpdate();
            trazaLog("g =" + g);

            // para movrespedo
            for (int m = 0; m < vparamovrespedo.size(); m++) {
              trazaLog("m = " + m);
              //dparamovrespedo = (DataConflictoTuber) vparamovrespedo.elementAt(m);
              hnotif = (Hashtable) vparamovrespedo.elementAt(m);
              if ( ( (String) d3.getCD_PREGUNTA()).equals( (String) hnotif.get(
                  "CD_PREGUNTA"))) {

                trazaLog("estoy en el if del for 4");
                //Vector vequipos = (Vector) dparamovrespedo.getEquiposPoseedores();
                //for(int n = 0;n<vequipos.size(); n++) {
                sQuery = " insert into SIVE_MOVRESPEDO " +
                    " (NM_EDO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, FC_RECEP, FC_FECNOTIF, CD_FUENTE, " +
                    "  CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, DS_RESPUESTA, IT_CONFLICTO, FC_ASIGN) " +
                    " values " +
                    " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

                st = conexion.prepareStatement(sQuery);

                // Notificador
                st.setInt(1,
                          new Integer( (String) hnotif.get("NM_EDO")).intValue());
                st.setString(2, (String) hnotif.get("CD_E_NOTIF"));
                st.setString(3, (String) hnotif.get("CD_ANOEPI"));
                st.setString(4, (String) hnotif.get("CD_SEMEPI"));
                String sfecharecep = (String) hnotif.get("FC_RECEP");
                trazaLog("sfecharecep" + sfecharecep);
                st.setDate(5,
                           new java.sql.Date(Fechas.string2Date( (String) hnotif.
                    get("FC_RECEP")).getTime()));
                st.setDate(6,
                           new java.sql.Date(Fechas.string2Date( (String) hnotif.
                    get("FC_FECNOTIF")).getTime()));
                st.setString(7, (String) hnotif.get("CD_FUENTE"));

                // Datos de movimiento en s�
                st.setString(8, d1.getCD_TSIVE());
                st.setString(9, d1.getCD_MODELO());
                st.setInt(10, d1.getNM_LINEA());
                String spregunta = (String) hnotif.get("CD_PREGUNTA");
                trazaLog("spregunta" + spregunta);
                st.setString(11, (String) hnotif.get("CD_PREGUNTA"));
                st.setString(12, (String) hnotif.get("DS_RESPUESTA"));
                st.setString(13, (String) hnotif.get("IT_CONFLICTO"));
                st.setDate(14, new java.sql.Date(new java.util.Date().getTime()));

                int h = st.executeUpdate();
                trazaLog("h =" + h);
                //} // for 5

              } //else {
              //break;
              //  trazaLog("no coincide d1 con d3");
              // }

            } // for 4

            break;

          }
        } // for 3

      } // if seguir

    } // for 1

  } // fin metodo

  private void arreglarmovrespedo2(int iCaso, CLista p) throws Exception {
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    Vector vp = null;
    DataRespTuber dr = null;

    vp = (Vector) p.elementAt(0);
    dr = (DataRespTuber) vp.elementAt(0);

    // Se borran aquellas respuestas del protocolo que no pertenezcan
    // a un modelo operativo (para cualquier tipo de usuario)
    sQuery = " delete from SIVE_MOVRESPEDO where " +
        "  NM_EDO = ?  and " +
        " CD_MODELO not in (select CD_MODELO from SIVE_MODELO " +
        " where CD_TSIVE = ? and IT_OK = ? ) ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, iCaso);
    st.setString(2, dr.getCD_TSIVE());
    st.setString(3, "S");

    int m = st.executeUpdate();
  }

  private void arreglarrespedo2(int iCaso, CLista p) throws Exception {
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    Vector vp = null;
    DataRespTuber dr = null;

    vp = (Vector) p.elementAt(0);
    dr = (DataRespTuber) vp.elementAt(0);

    // Se borran aquellas respuestas del protocolo que sea posible
    sQuery = null;
    sQuery = " delete from SIVE_RESP_EDO where " +
        "  NM_EDO = ? and " +
        " CD_MODELO not in (select CD_MODELO from SIVE_MODELO " +
        " where CD_TSIVE = ? and IT_OK = ? ) ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, iCaso);
//    st.setString(2, dr.getCD_MODELO());
    st.setString(2, dr.getCD_TSIVE());
    st.setString(3, "S");

    int b = st.executeUpdate();

  }

  // Inserci�n de la respuesta a una pregunta
  private void insertrespedo(DataRespTuber drt) throws Exception {
    PreparedStatement st = null;
    String sQuery = null;

    sQuery = " insert into SIVE_RESP_EDO " +
        " (CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, NM_EDO, DS_RESPUESTA) " +
        " values " +
        " (?, ?, ?, ?, ?, ?) ";

    st = conexion.prepareStatement(sQuery);

    st.setString(1, drt.getCD_TSIVE());
    st.setString(2, drt.getCD_MODELO());
    st.setInt(3, drt.getNM_LINEA());
    st.setString(4, drt.getCD_PREGUNTA());
    st.setInt(5, drt.getNM_EDO());
    st.setString(6, drt.getDS_RESPUESTA());

    st.executeUpdate();

  }

  private void updaterespedo(DataRespTuber data) throws Exception {
    PreparedStatement st = null;
    ResultSet rs = null;
    String sQuery = null;

    // Se borran aquellas respuestas del protocolo que sea posible
    sQuery = " update  SIVE_RESP_EDO set DS_RESPUESTA = ? where " +
        //"  CD_TSIVE = ? and CD_MODELO = ? and " +
        //" NM_LIN = ? and CD_PREGUNTA = ? and NM_EDO = ? ";
        "  CD_TSIVE = ? and  " +
        "  CD_PREGUNTA = ? and NM_EDO = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setString(1, data.getDS_RESPUESTA());
    st.setString(2, data.getCD_TSIVE());
    //st.setString(3, data.getCD_MODELO());
    //st.setInt(4, data.getNM_LINEA());
    st.setString(3, data.getCD_PREGUNTA());
    st.setInt(4, data.getNM_EDO());

    st.executeUpdate();

  }

  private void deletemov(DataConflictoTuber dtinicial) throws Exception {
    PreparedStatement st = null;
    String sQuery = null;

    // Se borran todos los movimientos del caso/equipo
    sQuery =
        " delete from SIVE_MOVRESPEDO where NM_EDO = ? and CD_PREGUNTA = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, dtinicial.getNM_EDO());
    st.setString(2, dtinicial.getCD_PREGUNTA());

    try {
      st.executeUpdate();
    }
    catch (java.sql.SQLException e) {
    }

    st = null;
  }

  private void updatemov(DataRespTuber dtinicial) throws Exception {
    PreparedStatement st = null;
    String sQuery = null;

    // Se modifican todos los movimientos del caso/equipo
    sQuery = " update SIVE_MOVRESPEDO set CD_MODELO = ? , NM_LIN = ? "
        + " where NM_EDO = ? and CD_PREGUNTA = ? and "
        + " CD_MODELO not in (select CD_MODELO from SIVE_MODELO "
        + " where CD_TSIVE = ? and IT_OK = ? ) ";

    st = conexion.prepareStatement(sQuery);

    st.setString(1, dtinicial.getCD_MODELO());
    st.setInt(2, dtinicial.getNM_LINEA());
    st.setInt(3, dtinicial.getNM_EDO());
    st.setString(4, dtinicial.getCD_PREGUNTA());
    st.setString(5, dtinicial.getCD_TSIVE());
    st.setString(6, "S");

    try {
      st.executeUpdate();
    }
    catch (java.sql.SQLException e) {
    }

    st = null;
  }

  // Actualizaci�n de un protocolo existente
  private CLista updateDatos(CLista p) throws Exception {

    int iCaso = ( (DataRespTuber) ( (Vector) p.firstElement()).firstElement()).
        getNM_EDO();
    // contiene toda la clave de sive_notif_edoi
    dtNotificador = (DatNotifTub) ( (Hashtable) p.elementAt(1)).get(constantes.
        DATOSNOTIFICADOR);

    // Aqu� se recogen las respuestas antiguas
    vEstadoInicial = cargaEstado(iCaso);

    // calculo el protocolo de respedo y movrespedo
    vdrtRespedo = cargaRespedo(iCaso);

    vdrtMovrespedo = cargaMovrespedo(iCaso, dtNotificador);

    // Se borran los datos antiguos
    // (respuestas antiguas del protocolo y movimientos del
    // equipo actual
    deleteDatosMovimientos(iCaso, dtNotificador);

    // Se insertan los datos actuales
    insertDatos(p);

    // Aqu� se recogen las preguntas actuales
    vEstadoFinal = cargaEstado(iCaso);

    // Se insertan los movimientos de aquellas preguntas que se han
    // quedado en blanco en SIVE_RESP_EDO
    insertMovimientosBlancos( (DatNotifTub) ( (Hashtable) p.elementAt(1)).get(
        constantes.DATOSNOTIFICADOR));

    // Se comparan las respuestas antiguas con las
    // actuales y se establecen conflictos
    estableceConflictos(iCaso, dtNotificador);

    return p;
  }

  /* modificaci�n para actualizar protocolo
   en la tabla movrespedo se borrar�n aquellos registros cuyas
   preguntas no existan en el modelo actual, y para aquellas preguntas
   que permanezcan se modificara su modelo con el actual
   */

  private CLista updateActProt(CLista p) throws Exception {

    int iCaso = ( (DataRespTuber) ( (Vector) p.firstElement()).firstElement()).
        getNM_EDO();

    // contiene toda la clave de sive_notif_edoi
    dtNotificador = (DatNotifTub) ( (Hashtable) p.elementAt(1)).get(constantes.
        DATOSNOTIFICADOR);

    // Aqu� se recogen los registro de resp_edo del modelo antiguo
    vEstadoInicial = cargaEstado(iCaso);

    // calculo el protocolo de respedo y movrespedo
    vdrtRespedo = cargaRespedo(iCaso);

    vdrtMovrespedo = cargaMovrespedo(iCaso, dtNotificador);

    // Se borran los datos antiguos
    // (respuestas antiguas del protocolo y movimientos
    deleteDatosMovimientos(iCaso, dtNotificador);

    // Se insertan los datos actuales
    insertDatos(p);

    arreglarmovrespedo(iCaso, vEstadoInicial, p);

    arreglarmovrespedo2(iCaso, p); // para evitar que se graben
    // preguntas de modelos no operativos

    //arreglarrespedoAUX(iCaso, p);
    arreglarrespedo(iCaso, p, dtNotificador);

    arreglarrespedo2(iCaso, p); // para evitar que se graben
    // preguntas de modelos no operativos

    // Aqu� se recogen las preguntas actuales
    vEstadoFinal = cargaEstado(iCaso);

    // Se insertan los movimientos de aquellas preguntas que se han
    // quedado en blanco en SIVE_RESP_EDO
    insertMovimientosBlancos( (DatNotifTub) ( (Hashtable) p.elementAt(1)).get(
        constantes.DATOSNOTIFICADOR));

    // Se comparan las respuestas antiguas con las
    // actuales y se establecen conflictos
    estableceConflictosaux(iCaso, dtNotificador);

    return p;
  }

  /////////////////////////

  private CLista updateVaciarProt(CLista p) throws Exception {
    PreparedStatement st = null;
    String sQuery = null;

    String sCaso = null;
    sCaso = (String) ( (DataResp) ( (Vector) p.firstElement()).firstElement()).
        getCaso();
    int iCaso = (new Integer(sCaso).intValue());

    // contiene toda la clave de sive_notif_edoi
    dtNotificador = (DatNotifTub) ( (Hashtable) p.elementAt(1)).get(constantes.
        DATOSNOTIFICADOR);

    // Se borran  todos los movimientos del caso/equipo
    sQuery =
        " delete from SIVE_MOVRESPEDO where NM_EDO = ? and CD_E_NOTIF = ? and "
        + " CD_ANOEPI = ? and CD_SEMEPI = ? and FC_RECEP = ? and FC_FECNOTIF = ? and CD_FUENTE = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setInt(1, iCaso);
    st.setString(2, dtNotificador.getCD_E_NOTIF_EDOI());
    st.setString(3, dtNotificador.getCD_ANOEPI_EDOI());
    st.setString(4, dtNotificador.getCD_SEMEPI());
    st.setDate(5,
               new java.sql.Date(Fechas.string2Date(dtNotificador.getFC_RECEP_EDOI()).
                                 getTime()));
    st.setDate(6,
               new java.sql.Date(Fechas.string2Date(dtNotificador.getFC_NOTIF()).
                                 getTime()));
    st.setString(7, dtNotificador.getCD_FUENTE());

    try {
      int c = st.executeUpdate();
    }
    catch (java.sql.SQLException e) {
    }

    st = null;

    return p;
  }

  ////////////////////////

  // Pone en blanco todas las respuestas actualmente cargadas
  private void limpiarProtocolo(int iCaso) throws Exception {
    PreparedStatement st = null;
    String sQuery = null;

    sQuery = " update SIVE_RESP_EDO set DS_RESPUESTA = ? where NM_EDO = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setString(1, " ");
    st.setInt(2, iCaso);

    st.executeUpdate();

    st = null;
  }

  private ListaOrdenable ComparaRespuestas(Vector v1, Vector v2, Vector v3) throws
      Exception {

    DataRespTuber drtv1 = null;
    DataRespTuber drtv2 = null;
    DataRespTuber drtv3 = null;

    // este metodo sirve para generar un vector aux
    // que contenga las mismas preguntas que vRespuestas pero con
    // las respuestas adecuadas para grabar en sive_respedo

    // vector v1 respuestas de movrespedo
    // vector v2 respuestas que vengan del dialogo
    // vector v3 respuesas de respedo

    Vaux = new Vector();

    boolean bseguir;

    for (int i = 0; i < v2.size(); i++) {
      bseguir = true;
      drtv2 = (DataRespTuber) v2.elementAt(i);

      for (int j = 0; j < v1.size() && bseguir; j++) {
        drtv1 = (DataRespTuber) v1.elementAt(j);
        if ( ( (String) drtv2.getCD_PREGUNTA()).equals( (String) drtv1.
            getCD_PREGUNTA())) {
          if (! ( (String) drtv2.getDS_RESPUESTA()).equals( (String) drtv1.
              getDS_RESPUESTA())) {

            Vaux.addElement(drtv2);
            break; // vRespuestas
          }
          else {
            bseguir = false;
            for (int k = 0; k < v3.size(); k++) {
              drtv3 = (DataRespTuber) v3.elementAt(k);
              if ( ( (String) drtv2.getCD_PREGUNTA()).equals( (String) drtv3.
                  getCD_PREGUNTA())) {
                Vaux.addElement(drtv3); // vdrtRespedo
                break;
              }
            } // for 3
          }

        }

      } // for 2

    } // for 1

    // para actualizar el vector con las nuevas salidas
    if (modoOperacion == modoACTUALIZARPROT) {
      Vaux = recomponerVaux(Vaux, v2);
    }
    else {

      // if(v1.size() <= v2.size()) {          // caso que queda despues de actualizar

      Vaux = recomponerVaux2(Vaux, v2); // protocolo, el notificador "siguiente"
      // tiene las preguntas nuevas vacias,
      // }                                    // con esto metodo se arregla
    }

    ListaOrdenable vlista = new ListaOrdenable();

    // transformo Vaux en ListaOrdenable
    for (int i = 0; i < Vaux.size(); i++) {
      vlista.addElement( (DataRespTuber) Vaux.elementAt(i));
    }

    return vlista;
  }

  private Vector recomponerVaux(Vector v4, Vector v5) throws Exception {
    DataRespTuber drtv4 = null;
    DataRespTuber drtv5 = null;
    DataRespTuber drtv5clone = null;
    boolean btodas = false;

    for (int i = 0; i < v5.size(); i++) {
      btodas = false;
      drtv5 = (DataRespTuber) v5.elementAt(i);
      for (int j = 0; j < v4.size(); j++) {
        drtv4 = (DataRespTuber) v4.elementAt(j);
        // para que el modelo coincida, a�ado
        // la pregunta de vrespuesta pero con la
        // respuesta de vrespuestaaux, clonando el data
        if ( ( (String) drtv4.getCD_PREGUNTA()).equals( (String) drtv5.
            getCD_PREGUNTA())) {
          drtv5clone = (DataRespTuber) (drtv5.clone());
          drtv5clone.setDS_RESPUESTA( (String) drtv4.getDS_RESPUESTA());
          v4.setElementAt(drtv5clone, j);
          btodas = true;
          break;
        }
      }

      if (!btodas) {
        v4.addElement(drtv5);
      }

    }

    return v4;
  }

  private Vector recomponerVaux2(Vector v4, Vector v5) throws Exception {
    DataRespTuber drtv4 = null;
    DataRespTuber drtv5 = null;
    boolean btodas = false;

    for (int i = 0; i < v5.size(); i++) {
      btodas = false;
      drtv5 = (DataRespTuber) v5.elementAt(i);
      for (int j = 0; j < v4.size(); j++) {
        drtv4 = (DataRespTuber) v4.elementAt(j);
        if ( ( (String) drtv4.getCD_PREGUNTA()).equals( (String) drtv5.
            getCD_PREGUNTA())) {
          btodas = true;
          break;
        }
      }

      if (!btodas) {
        v4.addElement(drtv5);
      }

    }

    return v4;
  }

  private Vector recomponerVaux3(Vector v4, Vector v5) throws Exception {
    DataRespTuber drtv4 = null;
    DataRespTuber drtv5 = null;
    boolean btodas = false;

    for (int i = 0; i < v5.size(); i++) {
      btodas = false;
      drtv5 = (DataRespTuber) v5.elementAt(i);
      for (int j = 0; j < v4.size(); j++) {
        drtv4 = (DataRespTuber) v4.elementAt(j);
        if ( ( (String) drtv4.getCD_PREGUNTA()).equals( (String) drtv5.
            getCD_PREGUNTA())) {
          btodas = true;
          break;
        }
      }

      if (!btodas) {
        v4.addElement(drtv5);

        try {
          insertrespedo(drtv5);
        }
        catch (java.sql.SQLException e) {
          updaterespedo(drtv5);
        }

      }

    }

    return v4;
  }

  // Inserci�n de un movimiento
  private void insertDatosMovimiento(DataRespTuber drt, DatNotifTub dnt) throws
      Exception {
    PreparedStatement st = null;
    String sQuery = null;

    sQuery = " insert into SIVE_MOVRESPEDO " +
        " (NM_EDO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, FC_RECEP, FC_FECNOTIF, CD_FUENTE, " +
        "  CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, DS_RESPUESTA, IT_CONFLICTO, FC_ASIGN) " +
        " values " +
        " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

    st = conexion.prepareStatement(sQuery);

    // Notificador
    st.setInt(1, new Integer(dnt.getNM_EDO()).intValue());
    st.setString(2, dnt.getCD_E_NOTIF_EDOI());
    st.setString(3, dnt.getCD_ANOEPI_EDOI());
    st.setString(4, dnt.getCD_SEMEPI());
    st.setDate(5,
               new java.sql.Date(Fechas.string2Date(dnt.getFC_RECEP_EDOI()).getTime()));
    st.setDate(6,
               new java.sql.Date(Fechas.string2Date(dnt.getFC_NOTIF()).getTime()));
    st.setString(7, dnt.getCD_FUENTE());

    // Datos de movimiento en s�
    st.setString(8, drt.getCD_TSIVE());
    st.setString(9, drt.getCD_MODELO());
    st.setInt(10, drt.getNM_LINEA());
    st.setString(11, drt.getCD_PREGUNTA());
    st.setString(12, drt.getDS_RESPUESTA());
    st.setString(13, "N");
    st.setDate(14, dFechaAsignacion);

    st.executeUpdate();
  }

  private void insertDatosMovimientosaux(Hashtable dctant) throws Exception {
    PreparedStatement st = null;
    String sQuery = null;

    // datos antiguos
    sQuery = " insert into SIVE_MOVRESPEDO " +
        " (NM_EDO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, FC_RECEP, FC_FECNOTIF, CD_FUENTE, " +
        "  CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, DS_RESPUESTA, IT_CONFLICTO, FC_ASIGN) " +
        " values " +
        " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

    st = conexion.prepareStatement(sQuery);

    // Notificador
    st.setInt(1, new Integer( (String) dctant.get("NM_EDO")).intValue());
    st.setString(2, (String) dctant.get("CD_E_NOTIF"));
    st.setString(3, (String) dctant.get("CD_ANOEPI")); ;
    st.setString(4, (String) dctant.get("CD_SEMEPI"));
    st.setDate(5,
               new java.sql.Date(Fechas.string2Date( (String) dctant.get("FC_RECEP")).
                                 getTime()));
    st.setDate(6,
               new java.sql.Date(Fechas.string2Date( (String) dctant.get("FC_FECNOTIF")).
                                 getTime()));
    st.setString(7, (String) dctant.get("CD_FUENTE"));

    // Datos de movimiento en s�
    st.setString(8, (String) dctant.get("CD_TSIVE"));
    st.setString(9, (String) dctant.get("CD_MODELO"));
    st.setInt(10, new Integer( (String) dctant.get("NM_LIN")).intValue());
    st.setString(11, (String) dctant.get("CD_PREGUNTA"));
    st.setString(12, (String) dctant.get("DS_RESPUESTA"));
    st.setString(13, (String) dctant.get("IT_CONFLICTO"));
    st.setDate(14,
               new java.sql.Date(Fechas.string2Date( (String) dctant.get("FC_ASIGN")).
                                 getTime()));

    try {
      int n = st.executeUpdate();
    }
    catch (java.sql.SQLException e) {
    }
  }

  // Asignaci�n de conflicto a los movimientos de una pregunta
  private void updateDatosMovimiento(DataConflictoTuber dct, String sConflicto) throws
      Exception {
    PreparedStatement st = null;
    String sQuery = null;

    sQuery = " update SIVE_MOVRESPEDO " +
        " set IT_CONFLICTO = ?, FC_ASIGN = ? " +
        " where NM_EDO = ? and CD_TSIVE = ? and CD_MODELO = ? and " +
        " NM_LIN = ? and CD_PREGUNTA = ? ";

    st = conexion.prepareStatement(sQuery);

    // Datos
    st.setString(1, sConflicto);
    st.setDate(2, dFechaAsignacion);

    // Clave
    st.setInt(3, dct.getNM_EDO());
    st.setString(4, dct.getCD_TSIVE());
    st.setString(5, dct.getCD_MODELO());
    st.setInt(6, dct.getNM_LIN());
    st.setString(7, dct.getCD_PREGUNTA());

    st.executeUpdate();
  }

  // Inserci�n de la respuesta a una pregunta
  private void insertDatosPreguntaProtocolo(DataRespTuber drt) throws Exception {
    PreparedStatement st = null;
    String sQuery = null;

    sQuery = " insert into SIVE_RESP_EDO " +
        " (CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, NM_EDO, DS_RESPUESTA) " +
        " values " +
        " (?, ?, ?, ?, ?, ?) ";

    st = conexion.prepareStatement(sQuery);

    st.setString(1, drt.getCD_TSIVE());
    st.setString(2, drt.getCD_MODELO());
    st.setInt(3, drt.getNM_LINEA());
    st.setString(4, drt.getCD_PREGUNTA());
    st.setInt(5, drt.getNM_EDO());
    st.setString(6, drt.getDS_RESPUESTA());

    st.executeUpdate();
  }

  // Modificaci�n de un movimiento ya existente
  private void updateDatosPreguntaProtocolo(DataRespTuber drt) throws Exception {
    PreparedStatement st = null;
    String sQuery = null;

    sQuery = " update SIVE_RESP_EDO " +
        " set DS_RESPUESTA = ? " +
        " where " +
        " CD_TSIVE = ? and CD_MODELO = ? and NM_LIN = ? and CD_PREGUNTA = ? and " +
        " NM_EDO = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setString(1, drt.getDS_RESPUESTA());
    st.setString(2, drt.getCD_TSIVE());
    st.setString(3, drt.getCD_MODELO());
    st.setInt(4, drt.getNM_LINEA());
    st.setString(5, drt.getCD_PREGUNTA());
    st.setInt(6, drt.getNM_EDO());

    st.executeUpdate();
  }

  private void updateDatosPreguntaProtocoloaux(DataRespTuber drt) throws
      Exception {
    PreparedStatement st = null;
    String sQuery = null;

    sQuery = " update SIVE_RESP_EDO " +
        " set DS_RESPUESTA = ? , CD_MODELO = ? , NM_LIN = ? " +
        " where " +
        " CD_TSIVE = ? and CD_PREGUNTA = ? and " +
        " NM_EDO = ? ";

    st = conexion.prepareStatement(sQuery);

    st.setString(1, drt.getDS_RESPUESTA());
    st.setString(2, drt.getCD_MODELO());
    st.setInt(3, drt.getNM_LINEA());
    st.setString(4, drt.getCD_TSIVE());
    st.setString(5, drt.getCD_PREGUNTA());
    st.setInt(6, drt.getNM_EDO());

    try {
      int num = st.executeUpdate();
    }
    catch (Exception e) {
      // System_out.println(e.getMessage());
      throw e;
    }
  }

  // Inserci�n de un nuevo protocolo (SIVE_RESP_EDO + SIVE_MOVRESPEDO)
  private CLista insertDatos(CLista p) throws Exception {

    DataRespTuber dr = null;
    DataRespTuber drRespuestas = null; // modificacion 19/06/2000

    // se utiliza ListaOrdenable para luego poder ordenarlas
    // por c�digo de pregunta
    //Vector vRespuestas = null;
    ListaOrdenable vRespuestas = null;
    // modificacion momentanea
    //Vector vRespuestasAux = null;
    ListaOrdenable vRespuestasAux = null;

    DatNotifTub dnt = null;

    PreparedStatement st = null;
    String sQuery = null;

    // ordeno vRespuestas y vRespuestaAux
    Vector vResp = (Vector) p.elementAt(0);
    vRespuestas = new ListaOrdenable();

    for (int i = 0; i < vResp.size(); i++) {
      vRespuestas.addElement( (DataRespTuber) vResp.elementAt(i));
    }

    //vRespuestas = (ListaOrdenable) p.elementAt(0);
    dnt = (DatNotifTub) ( (Hashtable) p.elementAt(1)).get(constantes.
        DATOSNOTIFICADOR);

    // modificacion momentanea
    // me genero un nuevo vRespuestas que saldra de la comparacion
    // de respuestas entre vRespuestas (ultimas respuestas introducidas)
    // y vClavesPreguntas (las de movrespedo para el notificador protagonista)
    // grabare en respedo solo aquellas respuestas que hayan cambiado o sean nuevas

    ///////////////////////////////////////////////////

    if (modoOperacion == modoALTA || modoOperacion == modoALTA_SB) {
      vRespuestasAux = vRespuestas;
    }
    else {

      //vdrtRespedo = cargaRespedo(iCaso);
      if (vdrtMovrespedo.size() > 0) { // caso de notificador con respuestas
        vRespuestasAux = (ListaOrdenable) ComparaRespuestas(vdrtMovrespedo,
            vRespuestas, vdrtRespedo);
        // modificacion momentanea
        if (vRespuestasAux.size() > 0) {
          // ordenar vectores
          vRespuestasAux.ordenaAsc();
          vRespuestas.ordenaAsc();
        }
        else {
          vRespuestasAux = vRespuestas;
        }

      }
      else {
        vRespuestasAux = vRespuestas;

      }
    }

    if (vRespuestasAux.size() > 0) {
      // Se limpia el protocolo actualmente cargado
      limpiarProtocolo( ( (DataRespTuber) vRespuestas.elementAt(0)).getNM_EDO());

      for (int i = 0; i < vRespuestasAux.size(); i++) {
        dr = (DataRespTuber) vRespuestasAux.elementAt(i); // a respedo
        drRespuestas = (DataRespTuber) vRespuestas.elementAt(i); // a movrespedo

        // SIVE_RESPEDO
        try {
          insertDatosPreguntaProtocolo(dr);
        }
        catch (java.sql.SQLException e) {
          updateDatosPreguntaProtocolo(dr);
        }

        // SIVE_MOVRESPEDO
        insertDatosMovimiento(drRespuestas, dnt); // modificacion drRespuestas

      }
    }

    // caso en que el protocolo del notificador tiene alg�na pregunta
    // sin contestar,o todas (despues de una actualizaci�n del protocolo
    // de otro notificador),  que el protocolo en respedo esa pregunta lleve
    // la respuesta que tuviera, a menos que sea un alta de caso
    // , en cuyo caso se graba tal cual lo que venga
    if (modoOperacion != modoALTA && modoOperacion != modoALTA_SB) {
      //if(vRespuestasAux.size() < vdrtRespedo.size()
      //    || vdrtMovrespedo.size() == 0) {
      Vaux2 = recomponerVaux3(vRespuestasAux, vdrtRespedo);
      //}
    }

    return p;
  }

  /*
    private CLista insertDatosaux(CLista p) throws Exception {
      DataRespTuber dr = null;
      DataRespTuber drRespuestas = null; // modificacion momentanea
      Vector vRespuestas = null;
      // modificacion momentanea
      Vector vRespuestasAux = null;
      DatNotifTub dnt = null;
      PreparedStatement st = null;
      String sQuery = null;
      vRespuestas = (Vector) p.elementAt(0);
      dnt = (DatNotifTub) ((Hashtable) p.elementAt(1)).get(constantes.DATOSNOTIFICADOR);
      // modificacion momentanea
      // me genero un nuevo vRespuestas que saldra de la comparacion
      // de respuestas entre vRespuestas (ultimas respuestas introducidas)
      // y vClavesPreguntas (las de movrespedo para el notificador protagonista)
      // grabare en respedo solo aquellas respuestas que hayan cambiado o sean nuevas
      ///////////////////////////////////////////////////
      //vdrtRespedo = cargaRespedo(iCaso);
      //vRespuestasAux = ComparaRespuestas(vdrtMovrespedo, vRespuestas, vdrtRespedo);
      if (vRespuestas.size() > 0) {
        // Se limpia el protocolo actualmente cargado
       limpiarProtocolo( ((DataRespTuber) vRespuestas.elementAt(0)).getNM_EDO() );
        for (int i = 0; i < vRespuestas.size(); i++) {
          dr = (DataRespTuber) vRespuestas.elementAt(i);
          //if(i < vRespuestas.size()) {
              //drRespuestas = (DataRespTuber) vRespuestas.elementAt(i);
          //}else {
           //   drRespuestas = null;
          //}
          // SIVE_RESPEDO
          try {
            insertDatosPreguntaProtocolo(dr);
          } catch (java.sql.SQLException e) {
            updateDatosPreguntaProtocolo(dr);
          }
          // SIVE_MOVRESPEDO
          // modificacion
          //if(drRespuestas != null)
            insertDatosMovimiento(dr, dnt);
        }
      }
      return p;
    }
   */

  // Para modo debug
  protected CLista doDebug() throws Exception {

    return new CLista();
  }

  /** prueba del servlet */
  public static void main(String[] args) {

  }

}
