package infproto;

import java.util.Vector;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.ScrollPane;

import COM.objectspace.jgl.Pair;
import com.borland.jbcl.control.TreeControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import com.borland.jbcl.model.BasicViewManager;
import com.borland.jbcl.model.GraphLocation;
import com.borland.jbcl.model.GraphSelectionEvent;
import com.borland.jbcl.model.SingleGraphSelection;
import com.borland.dx.text.Alignment;
import com.borland.jbcl.view.CompositeItemPainter;
import com.borland.jbcl.view.FocusableItemPainter;
import com.borland.jbcl.view.ImageItemPainter;
import com.borland.jbcl.view.SelectableItemPainter;
import com.borland.jbcl.view.TextItemPainter;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CPanel;
import comun.constantes;
import tuberculosis.cliente.diatuber.DialogTub;
// modificacion 19/06/2000
import tuberculosis.cliente.pantubnotif.PanTablaNotif;

public class PanelTuber
    extends CPanel {

  //ancho del arbol
  public int iAncho = 0;

  //Para el panel PRINCIPAL
  TreeControl arbolContenido = new TreeControl();

  protected Vector vArbol = new Vector(); //descripciones Enfermedad
  protected Vector vCNE = new Vector();
  protected Vector vCA = new Vector();
  protected Vector vN1 = new Vector();
  protected Vector vN2 = new Vector();
  protected Vector vReferencia = new Vector();

  ScrollPane panelScroll = new ScrollPane();
  ScrollPane panelScroll2 = new ScrollPane();

  public Panel_Informe_Completo pnl;
  public Panel pnl2 = new Panel();

  protected DataProtocolo datosproto = null;
  protected CCargadorImagen imgs = null;

  final String imgNAME[] = {
      "images/view.gif",
      "images/view2.gif"};

  XYLayout xYLayout1 = new XYLayout();
  XYLayout xYLayout2 = new XYLayout();

  DialogTub dialogo = null;

  // modificacion 19/06/2000
  PanTablaNotif panTablanotif = null;

  final int modoESPERA = constantes.modoESPERA;
  final int modoMODIFICACION = constantes.modoMODIFICACION;

  //public PanelTuber(CApp a, DataProtocolo data,
  //                DialogTub dlg) {

  public PanelTuber(CApp a, DataProtocolo data,
                    DialogTub dlg, PanTablaNotif pan) {
    try {

      app = a;
      datosproto = data;
      dialogo = dlg;

      panTablanotif = pan;

      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();

    }
  }

  void jbInit() throws Exception {
    // Sin relieve
    this.setBorde(false);

    // carga las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    // �rbol no editable
    arbolContenido.setEditInPlace(false);
    // estilo del nodo raiz
    arbolContenido.setStyle(com.borland.jbcl.view.TreeView.STYLE_PLUSES);
    // inicialmente el �rbol est� cerrado
    arbolContenido.setExpandByDefault(true);

    arbolContenido.setBackground(new Color(255, 255, 255));

    // gestor de eventos
    arbolContenido.addSelectionListener(new PanelTuber_selectionAdapter(this));

    // formulario EDO
    //  modificacion 19/06/2000 necesito recuperar informacion
    // de pantablanotif (si es alta de notificador o modificacion)
    //pnl = new Panel_Informe_Completo(app, datosproto, this, dialogo);
    pnl = new Panel_Informe_Completo(app, datosproto, this, dialogo,
                                     panTablanotif);

    Pintar_Arbol(pnl);

    //mlm
    //panelEDO
    this.setSize(new Dimension(740, 170 + 80));
    xYLayout1.setHeight(345 + 80);
    xYLayout1.setWidth(740); //el panel grande
    this.setLayout(xYLayout1);

    pnl2.setLayout(xYLayout2);
    xYLayout2.setHeight(345 + 80);
    xYLayout2.setWidth(150); //mas largo
    pnl2.setLayout(xYLayout2);
    pnl2.add(arbolContenido, new XYConstraints(0, 0, -1, 345 + 80)); //ponia 345
    pnl2.doLayout();
    pnl2.repaint();

    panelScroll.add(pnl); //panel_Informe
//      panelScroll.doLayout();

    panelScroll2.add(pnl2); //arbol
//      panelScroll2.doLayout();

//      this.doLayout();

    this.add(panelScroll2, new XYConstraints(0, 0, 150, 250 + 80)); //arbol//ponia 315
    this.add(panelScroll, new XYConstraints(150, 0, 580, 250 + 80)); //ponia 315

    this.xYLayout2.setWidth(iAncho + 40);
    this.xYLayout2.setHeight(pnl.iTam);
    this.pnl2.setLayout(xYLayout2);
    this.pnl2.doLayout();
    this.doLayout();
    this.doLayout();

  }

  public void Pintar_Arbol(Panel_Informe_Completo p) {
    vArbol = new Vector(); //descripciones Enfermedad
    vCNE = new Vector();
    vCA = new Vector();
    vN1 = new Vector();
    vN2 = new Vector();
    vReferencia = new Vector();

    //Cargamos los vectores con los datos
    Vector vEnfermedad = new Vector();
    Vector vDescripcion = new Vector();
    Vector vtmpCNE = new Vector();
    Vector vtmpCA = new Vector();
    Vector vtmpN1 = new Vector();
    Vector vtmpN2 = new Vector();

    vEnfermedad = p.getEnfermedad();
    vDescripcion = p.getDescripciones();
    vtmpCNE = p.getCNE();
    vtmpCA = p.getCA();
    vtmpN1 = p.getN1();
    vtmpN2 = p.getN2();

    Nodo pto;

    String maslargo = "";

    //vArbol
    pto = (Nodo) vEnfermedad.elementAt(0);
    vArbol.addElement(new Nodo(pto.getDescripcion(), pto.getY()));
    maslargo = pto.getDescripcion() + "__";

    //resto de vArbol
    for (int idesc = 0; idesc < vDescripcion.size(); idesc++) {
      pto = (Nodo) vDescripcion.elementAt(idesc);
      vArbol.addElement(new Nodo(pto.getDescripcion(), pto.getY()));
      if (pto.getDescripcion().length() > maslargo.length()) {
        maslargo = pto.getDescripcion() + "__" + "__";
      }
    }
    vEnfermedad = null;
    vDescripcion = null;

    //vCNE
    for (int icne = 0; icne < vtmpCNE.size(); icne++) {
      pto = (Nodo) vtmpCNE.elementAt(icne);
      vCNE.addElement(new Nodo(pto.getDescripcion(), pto.getY()));
      if (pto.getDescripcion().length() > maslargo.length()) {
        maslargo = pto.getDescripcion() + "__" + "__" + "__";
      }
    }
    vtmpCNE = null;

    //vCA
    for (int ica = 0; ica < vtmpCA.size(); ica++) {
      pto = (Nodo) vtmpCA.elementAt(ica);
      vCA.addElement(new Nodo(pto.getDescripcion(), pto.getY()));
      if (pto.getDescripcion().length() > maslargo.length()) {
        maslargo = pto.getDescripcion() + "__" + "__" + "__";
      }
    }
    vtmpCA = null;

    //vN1
    for (int in1 = 0; in1 < vtmpN1.size(); in1++) {
      pto = (Nodo) vtmpN1.elementAt(in1);
      vN1.addElement(new Nodo(pto.getDescripcion(), pto.getY()));
      if (pto.getDescripcion().length() > maslargo.length()) {
        maslargo = pto.getDescripcion() + "__" + "__" + "__";
      }
    }
    vtmpN1 = null;

    //vN2
    for (int in2 = 0; in2 < vtmpN2.size(); in2++) {
      pto = (Nodo) vtmpN2.elementAt(in2);
      vN2.addElement(new Nodo(pto.getDescripcion(), pto.getY()));
      if (pto.getDescripcion().length() > maslargo.length()) {
        maslargo = pto.getDescripcion() + "__" + "__" + "__";
      }
    }
    vtmpN2 = null;

    // inicializa el �rbol
    Inicializar();

    //ver cuantos pixeles ocupa el layout
    Graphics g = app.getGraphics();
    g.setFont(new Font("Dialog", 1, 12));
    iAncho = g.getFontMetrics().stringWidth(maslargo);

    // modificacion 27/06/000
    //p.doLayout();

  }

  // inicializa el �rbol
  public void Inicializar() {
    GraphLocation root;
    GraphLocation cne;
    GraphLocation ca;
    GraphLocation n1;
    GraphLocation n2;
    GraphLocation gl;

    Nodo nodo;
    int j;

    // establece el visor imagen+texto
    arbolContenido.setViewManager(new BasicViewManager
                                  (new FocusableItemPainter
                                   (new SelectableItemPainter
                                    (new CompositeItemPainter
                                     (new ImageItemPainter(this, Alignment.LEFT),
                                      new TextItemPainter())))));

    // modificacion 28/06/2000
    //arbolContenido.doLayout();

    j = 0;
    // nodo raiz
    nodo = (Nodo) vArbol.elementAt(j);
    root = arbolContenido.setRoot(new Pair(null, nodo.getDescripcion()));

    vReferencia.addElement(new Nodo2(root, nodo.getY()));

    //paso a los siguientes

    if (vCNE.size() > 0) {
      j++;
      nodo = (Nodo) vArbol.elementAt(j);
      cne = arbolContenido.addChild(root,
                                    new Pair(imgs.getImage(0), nodo.getDescripcion()));
      vReferencia.addElement(new Nodo2(cne, nodo.getY()));
      // partes del cne
      for (int i = 0; i < vCNE.size(); i++) {
        nodo = (Nodo) vCNE.elementAt(i);
        gl = arbolContenido.addChild(cne,
                                     new Pair(imgs.getImage(1), nodo.getDescripcion()));
        vReferencia.addElement(new Nodo2(gl, nodo.getY()));
      }
    }

    // nodo CA
    if (vCA.size() > 0) {
      j++;
      nodo = (Nodo) vArbol.elementAt(j);
      ca = arbolContenido.addChild(root,
                                   new Pair(imgs.getImage(0), nodo.getDescripcion()));
      vReferencia.addElement(new Nodo2(ca, nodo.getY()));
      // partes del ca
      for (int i = 0; i < vCA.size(); i++) {
        nodo = (Nodo) vCA.elementAt(i);
        gl = arbolContenido.addChild(ca,
                                     new Pair(imgs.getImage(1), nodo.getDescripcion()));
        vReferencia.addElement(new Nodo2(gl, nodo.getY()));
      }
    }

    // nodo n1
    if (vN1.size() > 0) {
      j++;
      nodo = (Nodo) vArbol.elementAt(j);
      n1 = arbolContenido.addChild(root,
                                   new Pair(imgs.getImage(0), nodo.getDescripcion()));
      vReferencia.addElement(new Nodo2(n1, nodo.getY()));
      // partes del n1
      for (int i = 0; i < vN1.size(); i++) {
        nodo = (Nodo) vN1.elementAt(i);
        gl = arbolContenido.addChild(n1,
                                     new Pair(imgs.getImage(1), nodo.getDescripcion()));
        vReferencia.addElement(new Nodo2(gl, nodo.getY()));
      }
    }

    if (vN2.size() > 0) {
      j++;
      nodo = (Nodo) vArbol.elementAt(j);
      n2 = arbolContenido.addChild(root,
                                   new Pair(imgs.getImage(0), nodo.getDescripcion()));
      vReferencia.addElement(new Nodo2(n2, nodo.getY()));
      // partes del n2
      for (int i = 0; i < vN2.size(); i++) {
        nodo = (Nodo) vN2.elementAt(i);
        gl = arbolContenido.addChild(n2,
                                     new Pair(imgs.getImage(1), nodo.getDescripcion()));
        vReferencia.addElement(new Nodo2(gl, nodo.getY()));
      }
    } //fin if n2

  }

  // mueve el panel de scroll a la selecci�n indicada
  void arbolContenido_selectionChanged(GraphSelectionEvent e) {
    GraphLocation[] selections;
    Nodo2 nodo = null;

    //QQ: Cambios a partir de Confprot:
    /*
         if ( e.getSelection().getCount() > 0 ) {
      selections = e.getSelection().getAll();
      adaptee.nodoSeleccionado = selections[0];
         } else {
      adaptee.nodoSeleccionado = null;
      adaptee.arbolModelo.setSelection( new SingleGraphSelection(adaptee.arbolModelo.getRoot()));
         }
     */
    // copia el nodo seleccionado
    if (arbolContenido.getSelection().getCount() > 0) {
      //QQ: Eliminado: selections = arbolContenido.getSelection().getAll();
      selections = e.getSelection().getAll();
      for (int i = 0; i < vReferencia.size(); i++) {
        nodo = (Nodo2) vReferencia.elementAt(i);
        if (selections[0] == nodo.getGraphLocation()) {
          panelScroll.setScrollPosition(0, nodo.getY());
          panelScroll.doLayout();
          break;
        }
      } //for
    } //if
    else { //Nuevo:
      arbolContenido.setSelection(new SingleGraphSelection(arbolContenido.
          getRoot()));
    }
  } //void

  public void recuperarProtocolo() {

    dialogo.bGrabarProt = true;

    dialogo.Inicializar(modoESPERA);

    this.validate();
    this.setBorde(false);

    pnl = null;
    pnl = new Panel_Informe_Completo(app, datosproto, this, dialogo,
                                     panTablanotif);

    pnl.doLayout();
    pnl.repaint();

    Pintar_Arbol(pnl);

    panelScroll.add(pnl); //panel_Informe

    panelScroll.doLayout();
    panelScroll.repaint();
    panelScroll.validate();

    //this.add(panelScroll, new XYConstraints(150, 0, 580, 250 + 80)); //ponia 315

    // sin this
    validate();
    doLayout();
    repaint();

    // para mejorar el pintado
    try {
      Thread.sleep(500);
    }
    catch (Exception e) {
    }

    dialogo.Inicializar(modoMODIFICACION);

  }

  // metodo llamado desde el evento pulsar pesta�a protocolo
  // para recuperar para cada notificador su protocolo
  public void recuperarProtocolo2() {

    try {

      // variable para controlar que cuando se grabe un protocolo y
      // se pulse grabar, si estamos en el panel del protocolo, se
      // grabe dicho protocolo
      dialogo.bGrabarProt = true;

      // modo espera
      dialogo.Inicializar(modoESPERA);

      this.validate();

      this.setBorde(false);

      // carga las im�genes
//      imgs = new CCargadorImagen(app, imgNAME);
      imgs.CargaImagenes();

      arbolContenido.setEditInPlace(false);
      // estilo del nodo raiz
      arbolContenido.setStyle(com.borland.jbcl.view.TreeView.STYLE_PLUSES);
      // inicialmente el �rbol est� cerrado
      arbolContenido.setExpandByDefault(true);

      arbolContenido.setBackground(new Color(255, 255, 255));

      // gestor de eventos
      arbolContenido.addSelectionListener(new PanelTuber_selectionAdapter(this));

      pnl = null;
      pnl = new Panel_Informe_Completo(app, datosproto, this, dialogo,
                                       panTablanotif);

      Pintar_Arbol(pnl);

      //pnl2.add(arbolContenido, new XYConstraints(0, 0, -1, 345 + 80));//ponia 345
      //mlm
      //panelEDO
      this.setSize(new Dimension(740, 170 + 80));
      xYLayout1.setHeight(345 + 80);
      xYLayout1.setWidth(740); //el panel grande
      this.setLayout(xYLayout1);

      pnl2.setLayout(xYLayout2);
      xYLayout2.setHeight(345 + 80);
      xYLayout2.setWidth(150); //mas largo
      pnl2.setLayout(xYLayout2);

//      pnl2.add(arbolContenido, new XYConstraints(0, 0, -1, 345 + 80));//ponia 345
      /*
            pnl2.doLayout();
            pnl2.repaint();
            panelScroll2.doLayout();
            panelScroll2.repaint();
            pnl.doLayout();
            pnl.repaint();
            panelScroll.add(pnl); //panel_Informe
            panelScroll.doLayout();
            panelScroll.repaint();
            this.doLayout();
            this.repaint();
            // con esto no funciona
//      this.add(panelScroll2, new XYConstraints(0, 0, 150, 250 + 80)); //arbol//ponia 315
//      this.add(panelScroll, new XYConstraints(150, 0, 580, 250 + 80)); //ponia 315
       */

///////////////////////

      pnl.doLayout();
      pnl.repaint();

      //panelScroll = null;
      //panelScroll = new ScrollPane();

      panelScroll.validate();

      panelScroll.doLayout();
      panelScroll.repaint();
      panelScroll.add(pnl); //panel_Informe

      panelScroll2.validate();

      //panelScroll2.add(pnl2);//arbol
      panelScroll2.doLayout();
      panelScroll2.repaint();

      pnl2.doLayout();
      pnl2.repaint();

/////////////////////

      this.xYLayout2.setWidth(iAncho + 40);
      this.xYLayout2.setHeight(pnl.iTam);
      this.pnl2.setLayout(xYLayout2);
      this.pnl2.doLayout();
      this.doLayout();
      this.doLayout();
      this.repaint();

      this.validate();

      // modificacion 27/06/2000
      try {
        Thread.sleep(500);
      }
      catch (Exception e) {
      }

      // modo modificacion
      dialogo.Inicializar(modoMODIFICACION);

    }
    catch (Exception ex) {
      ex.printStackTrace();

    }

  }

} // end class

class PanelTuber_selectionAdapter
    extends com.borland.jbcl.model.GraphSelectionAdapter {
  PanelTuber adaptee;

  public PanelTuber_selectionAdapter(PanelTuber adaptee) {
    this.adaptee = adaptee;
  }

  public void selectionChanged(GraphSelectionEvent e) {
    adaptee.arbolContenido_selectionChanged(e);
  }
}
