package infproto;

import com.borland.jbcl.model.GraphLocation;

// referencias del arbol
class Nodo2 {
  protected int y;
  protected GraphLocation g;

  public Nodo2(GraphLocation g, int y) {
    this.y = y;
    this.g = g;
  }

  GraphLocation getGraphLocation() {
    return g;
  }

  int getY() {
    return y;
  }
}
