package infproto;

import com.borland.jbcl.control.ButtonControl;

public class CButtonControl
    extends ButtonControl {

  //datos de entrada
  public String CodModelo;
  public String Nivel;
  public boolean bListenerCargado = false;

  //constructor
  public CButtonControl(String CODMODELO,
                        String NIVEL) {
    super();

    //parametros de entrada
    CodModelo = CODMODELO;
    Nivel = NIVEL;

    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  } //end Constructor

//jbInit*****************************************
  private void jbInit() throws Exception {

    this.setVisible(false);

  } //fin jbnit

//-------------------------------------------------------------------------

  public void setCodModelo(String codMod) {
    CodModelo = codMod;
  }

  public void setNivel(String nivel) {
    Nivel = nivel;
  }

  public String getCodModelo() {
    return (CodModelo);
  }

  public String getNivel() {
    return (Nivel);
  }

} //endclass
