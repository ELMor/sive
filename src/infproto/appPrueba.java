package infproto;

import java.util.Hashtable;

import capp.CApp;
import comun.constantes;

public class appPrueba
    extends CApp {

  int modoESPERA = constantes.modoESPERA;
  int modoALTA = constantes.modoALTA;
  int modoMODIFICACION = constantes.modoMODIFICACION;
  int modoBAJA = constantes.modoBAJA;
  int modoCONSULTA = constantes.modoCONSULTA;

  public void init() {
    super.init();
    setTitulo("");
  }

  public void start() {
    CApp a = this;

    Dialogprueba diaPanelPrueba = new Dialogprueba(a, new Hashtable(),
        modoMODIFICACION);

    // Pruebas
    diaPanelPrueba.show();

    System.exit(0);

    // fin Pruebas

    //VerPanel("", panel);
  }

}
