package infproto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import sapp.DBServlet;

/* Select, insert y updates sobre la tabla SIVE_RESP_BROTES */
public class SrvRespBrotes
    extends DBServlet {

  // modos de operaci�n
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  /*final int servletSELECT = 2;*/

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    DataResp dataEntrada = null;
    CLista listaSalida = new CLista();
    DataResp dataSalida = null;

    // campos
    String Caso = "";
    String CodModelo = "";
    String Num = "";
    String CodPregunta = "";
    String Des = ""; //V
    String valor = ""; //Varon

    final String sALTA_RESP = "insert into SIVE_RESP_BROTES "
        + "(CD_TSIVE, CD_MODELO, NM_LIN, CD_PREGUNTA, "
        + " CD_ANO, NM_ALERBRO, DS_RESPUESTA) "
        + "values (?, ?, ?, ?, ?, ?, ?)";

    final String sBORRAR = "DELETE FROM SIVE_RESP_BROTES "
        + "WHERE (CD_ANO = ? AND NM_ALERBRO = ?) ";

    /*final String sSELECT_00 ="select "
          + " CD_MODELO, NM_LIN, CD_PREGUNTA, "
          + " DS_RESPUESTA "
          + "from SIVE_RESP_BROTES "
          + "where (CD_ANO = ? AND NM_ALERBRO = ?) order by CD_MODELO, NM_LIN";
        final String sSELECT_01 ="select "
          + " a.DS_LISTAP, a.DSL_LISTAP "
          + "from SIVE_LISTA_VALORES a, SIVE_PREGUNTA b where "
          + "(b.CD_PREGUNTA = ? and a.CD_LISTAP = ? "
          + " and a.CD_LISTA = b.CD_LISTA)" ; */

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {

      // modos de operaci�n
      switch (opmode) {

        //ALTA******************************************
        case servletALTA:

          //va bien: listaSalida.size() = 0
          //va mal: listaSalida = null y throws ex

          //recorremos la lista de entrada
          for (i = 0; i < param.size(); i++) {
            dataEntrada = (DataResp) param.elementAt(i);

            st = con.prepareStatement(sALTA_RESP);

            //todos OBLIGATORIOS *************************
            st.setString(1, "B");
            st.setString(2, dataEntrada.getCodModelo().trim());

            Integer NumLinAlta = new Integer(dataEntrada.getNumLinea().trim());
            st.setInt(3, NumLinAlta.intValue());

            st.setString(4, dataEntrada.getCodPregunta().trim());

            String sAno = dataEntrada.getCaso().trim().substring(0, 4);
            Integer iNM = new Integer(dataEntrada.getCaso().trim().substring(4));
            st.setString(5, sAno);
            st.setInt(6, iNM.intValue());

            st.setString(7, dataEntrada.getDesPregunta().trim());
            st.executeUpdate();
            st.close();
            st = null;

            sAno = null;
            NumLinAlta = null;
            iNM = null;

          } //fin for

          break;

          //MODIFICAR******************************************
        case servletMODIFICAR:

          //va bien: listaSalida.size() = 0
          //va mal: listaSalida = null y throws ex

          //cargamos con lo que ha llegado en param (Lista)
          //(un registro) de momento
          dataEntrada = (DataResp) param.firstElement();
          //borro e inserto, para evitar que registros vacios queden
          //llenos y que no entren otros nuevos informados
          st = con.prepareStatement(sBORRAR);

          String sANO = dataEntrada.getCaso().trim().substring(0, 4);
          Integer iNMModificar = new Integer(dataEntrada.getCaso().trim().
                                             substring(4));
          st.setString(1, sANO);
          st.setInt(2, iNMModificar.intValue());

          st.executeUpdate();
          st.close();
          st = null;

          iNMModificar = null;
          sANO = null;

          //modificando pero la lista viene con num caso,  + codmodelo=""
          //ie, han vaciado el protocolo, hay solo que borrar
          if (param.size() == 1
              && dataEntrada.getCodModelo().trim().equals("")) {
            //no debo hacer nada mas!!!

          }
          else {
            //recorremos la lista de entrada
            for (i = 0; i < param.size(); i++) {
              dataEntrada = (DataResp) param.elementAt(i);

              st = con.prepareStatement(sALTA_RESP);

              //todos OBLIGATORIOS *************************
              st.setString(1, "B");
              st.setString(2, dataEntrada.getCodModelo().trim());
              Integer NumLinRecursivo = new Integer(dataEntrada.getNumLinea().
                  trim());
              st.setInt(3, NumLinRecursivo.intValue());
              st.setString(4, dataEntrada.getCodPregunta().trim());
              String sAnoRecursivo = dataEntrada.getCaso().trim().substring(0,
                  4);
              Integer iNMRecursivo = new Integer(dataEntrada.getCaso().trim().
                                                 substring(4));
              st.setString(5, sAnoRecursivo);
              st.setInt(6, iNMRecursivo.intValue());
              st.setString(7, dataEntrada.getDesPregunta().trim());

              st.executeUpdate();
              st.close();
              st = null;
              NumLinRecursivo = null;
              sAnoRecursivo = null;
              iNMRecursivo = null;
            } //fin for
          }

          break;

          /*      //SELECT***********************************************
                case servletSELECT:
                //va bien: listaSalida.size() = 0 , listaSalida.size > 0
                //va mal: listaSalida = null y throws ex
                  //cargamos con lo que ha llegado en param (Lista)
               dataEntrada = (DataResp) param.firstElement();//REGISTRO UNICO
              if ( !dataEntrada.getCaso().trim().equals("")){
                  // lanza la query
                  st = con.prepareStatement(sSELECT_00);
               Integer iCasoSelect = new Integer(dataEntrada.getCaso().trim());
                  st.setInt(1, iCasoSelect.intValue());
                  rs = st.executeQuery();
                // extrae los registros encontrados
                while(rs.next()){
                  CodModelo = rs.getString("CD_MODELO");
                  Integer NumLinWhile = new Integer (rs.getInt("NM_LIN"));
                  Num = NumLinWhile.toString();
                  CodPregunta = rs.getString("CD_PREGUNTA");
                  Des = rs.getString("DS_RESPUESTA");
                    // a�ade un nodo
                    dataSalida = new DataResp (dataEntrada.getCaso().trim(),
                                                CodModelo, Num,
                                                CodPregunta, Des, null);
                    listaSalida.addElement(dataSalida);
                  NumLinWhile = null;
                }//fin while
                    rs.close();
                    rs=null;
                    st.close();
                    st=null;
                    iCasoSelect = null;
                  //RECOMPONEMOS LA PARTE QUE NOS FALTA EN LA listaSalida (valor)
                 for (i=0; i< listaSalida.size(); i++){
                    dataSalida = (DataResp) listaSalida.elementAt(i);
                    CodPregunta  = dataSalida.getCodPregunta();
                    Des = dataSalida.getDesPregunta();
                    Caso = dataSalida.getCaso();
                    Num = dataSalida.getNumLinea();
                    CodModelo = dataSalida.getCodModelo();
                    // lanza la query
                    st = con.prepareStatement(sSELECT_01);
                    st.setString(1, CodPregunta); //cd_pregunta
                    st.setString(2, Des); //cd_listap
                    rs = st.executeQuery();
                    //solo saldra un registro, o (ninguno: no se modifica)
                    while(rs.next()){
                      //modifico el dataSalida
                      listaSalida.removeElementAt(i);
                        String des = rs.getString("DS_LISTAP") ;
                        String desL = rs.getString("DSL_LISTAP");
                        if ((param.getIdioma() != CApp.idiomaPORDEFECTO)
                        && (desL != null))
                           valor = desL;
                        else
                           valor = des;
                      listaSalida.insertElementAt
                          (new DataResp(Caso, CodModelo, Num, CodPregunta,
                          Des, valor ),i);
                    }
                 }
                }//fin de if de caso informado
                 //----------------------------------------------------
                  break;
           */

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;

      throw ex;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;
  } //fin doWork

} //fin de la clase
