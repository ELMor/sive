/*
 * Creado: 01/02/2000 / Emilio Postigo Riancho
 */

package infproto;

import java.util.Vector;

public class DataConflictoTuber {

  private int NmEdo;
  private String CdTsive = null;
  private String CdModelo = null;
  private int NmLinea;
  private String CdPregunta = null;

  private String DsRespuesta = null;
  private Vector vCdEq = null;
  private String ItConflicto = null;

  public DataConflictoTuber() {

  }

  public DataConflictoTuber(int nmedo,
                            String cdtsive,
                            String cdmodelo,
                            int nmlinea,
                            String cdpregunta,
                            String dsrespuesta,
                            Vector cdeq,
                            String itconflicto) {

    NmEdo = nmedo;
    CdTsive = cdtsive;
    CdModelo = cdmodelo;
    NmLinea = nmlinea;
    CdPregunta = cdpregunta;

    DsRespuesta = dsrespuesta;
    vCdEq = cdeq;
    ItConflicto = itconflicto;

  }

  public DataConflictoTuber(int nmedo,
                            String cdtsive,
                            String cdmodelo,
                            int nmlinea,
                            String cdpregunta,
                            String dsrespuesta) {

    NmEdo = nmedo;
    CdTsive = cdtsive;
    CdModelo = cdmodelo;
    NmLinea = nmlinea;
    CdPregunta = cdpregunta;

    DsRespuesta = dsrespuesta;
//    vCdEq = new Vector();
//    ItConflicto = "";

  }

  // M�todos de lectura
  public int getNM_EDO() {
    return NmEdo;
  }

  public String getCD_TSIVE() {
    return CdTsive;
  }

  public String getCD_MODELO() {
    return CdModelo;
  }

  public int getNM_LIN() {
    return NmLinea;
  }

  public String getCD_PREGUNTA() {
    return CdPregunta;
  }

  public String getDS_RESPUESTA() {
    String s = "";
    if (DsRespuesta != null) {
      s = DsRespuesta;
    }
    return s;
  }

  public String getIT_CONFLICTO() {
    return ItConflicto;
  }

  //
  public boolean equals(DataConflictoTuber dct) {
    boolean bValor = false;

    bValor = (NmEdo == dct.NmEdo) &&
        (CdTsive.equals(dct.CdTsive)) &&
        (CdModelo.equals(dct.CdModelo)) &&
        (NmLinea == dct.NmLinea) &&
        (CdPregunta.equals(dct.CdPregunta)) &&
        (vCdEq.equals(dct.vCdEq)) &&
        (ItConflicto.equals(dct.ItConflicto));

    return bValor;
  }

  public boolean equalsPregunta(DataConflictoTuber dct) {
    boolean bValor = false;

    bValor = (NmEdo == dct.NmEdo) &&
        (CdTsive.equals(dct.CdTsive)) &&
        (CdModelo.equals(dct.CdModelo)) &&
        (NmLinea == dct.NmLinea) &&
        (CdPregunta.equals(dct.CdPregunta));

    return bValor;
  }

  // modificacion para actualizar prot
  public boolean equalsPreguntaaux(DataConflictoTuber dct) {
    boolean bValor = false;

    bValor = (NmEdo == dct.NmEdo) &&
        (CdTsive.equals(dct.CdTsive)) &&
        (CdPregunta.equals(dct.CdPregunta));

    return bValor;
  }

  public Vector getEquiposPoseedores() {
    return (vCdEq == null ? new Vector() : vCdEq);
  }

  // Escritura
  public void setIT_CONFLICTO(String itconflicto) {
    ItConflicto = itconflicto;
  }

  public void addEquipoPoseedor(String cdeq) {
    if (vCdEq == null) {
      vCdEq = new Vector();
      vCdEq.addElement(cdeq);
    }
    else {
      if (!vCdEq.contains(cdeq)) {
        vCdEq.addElement(cdeq);
      }
    }
  }

}
