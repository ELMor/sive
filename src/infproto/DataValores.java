package infproto;

import java.io.Serializable;

//soporta los valores de las choices
public class DataValores
    implements Serializable {

  protected String sCod = "";
  protected String sDes = "";

  public DataValores(String Cod,
                     String Des) {

    sCod = Cod;
    sDes = Des;

  }

  public String getCod() {
    return sCod;
  }

  public String getDes() {
    return sDes;
  }

}
