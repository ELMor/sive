//Title:        Consulta de alarmas automaticas de una semana epidemilogica.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Se presentara la relacion de enfermedades
//notificadas en la semana elegida con el numero
//de casos y los indicadoes superados con su umbral.

package ConsAlaAuto;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class panelInfAlaAuto
    extends EPanel {

  panelConsAlaAuto pan;
  ResourceBundle res;

  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  final String strLOGO_CCAA = "images/ccaa.gif";

  final String strSERVLET = "servlet/SrvConsAlaAuto";

  final int erwCASOS_EDO_AGRU = 1;
  final int erwCASOS_EDO_AGRU_NO_EQ_NI_CEN = 2;
  final int erwCASOS_EDO_AGRU_EQ = 3;
  final int erwCASOS_EDO_AGRU_CEN = 4;
  final int erwCASOS_EDO_AGRU_EQ_Y_CEN = 5;

  // estructuras de datos
  protected Vector vResultado;
  protected CLista lista;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  public parConsAlaAuto parCons;

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  CApp app = null;
  // JRM: No s� que narices hace aqui una cosa asi
  //ImageControl imageControl1 = new ImageControl();

  public panelInfAlaAuto(CApp a, panelConsAlaAuto miPanel) {
    super(a);
    pan = miPanel;
    app = a;
    res = ResourceBundle.getBundle("ConsAlaAuto.Res" + a.getIdioma());

    try {
      stub = new StubSrvBD();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + "erw/Consulta77.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);
    // JRM: No s� porque hay que a�adir un imageControl �?
    //pan.add(imageControl1, null);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;

    //E //# System_Out.println(lista.getState());
    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {

        PrepararInforme();

        // obtiene los datos del servidor
        parCons.numPagina++;

        //E //# System_Out.println("C7: Mas datos pg=" + parCons.numPagina );

        param.addElement(parCons);
        param.trimToSize();
        stub.setUrl(new URL(app.getURL() + strSERVLET));
        lista = (CLista) stub.doPost(parCons.criterio, param);
        if (lista != null) {
          v = (Vector) lista.elementAt(0);
          if (lista.getState() == CLista.listaINCOMPLETA) {
            parCons.ultEnf = (String) lista.elementAt(1);
            parCons.ultInd = (String) lista.elementAt(2);
          }
          for (int j = 0; j < v.size(); j++) {
            vResultado.addElement(v.elementAt(j));

            // control de registros
          }
          if (vResultado.size() == 0) {
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                  res.getString("msg7.Text"));
            msgBox.show();
            msgBox = null;
            this.setLimpiarStatus();
          }
          else {
            if (lista.getState() == CLista.listaINCOMPLETA) {
              this.setTotalRegistros(res.getString("this.TotalRegistros"));
            }
            else {
              this.setTotalRegistros(res.getString("this1.TotalRegistros"));
            }
            this.setRegistrosMostrados( (new Integer(vResultado.size() / 3)).
                                       toString());
            // repintado
            erwClient.refreshReport(true);
          }
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg7.Text"));
          msgBox.show();
          msgBox = null;
          this.setLimpiarStatus();
        }

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // informe comleto
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  public boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    CLista param = new CLista();

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      PrepararInforme();

      //E //# System_Out.println ("Vuelvo de preparar informe");

      // obtiene los datos del servidor
      parCons.numPagina = 0;
      parCons.bInformeCompleto = conTodos;
      param.addElement(parCons);
      param.trimToSize();
      stub.setUrl(new URL(app.getURL() + strSERVLET));

      lista = (CLista) stub.doPost(parCons.criterio, param);

      /*      SrvConsAlaAuto srv = new SrvConsAlaAuto();
            srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@10.160.5.149:1521:ORCL",
                                 "pista",
                                 "loteb98");
            lista = srv.doDebug(parCons.criterio, param);
       */

      if (lista != null) {
        vResultado = (Vector) lista.elementAt(0);
        if (lista.getState() == CLista.listaINCOMPLETA) {
          parCons.ultEnf = (String) lista.elementAt(1);
          parCons.ultInd = (String) lista.elementAt(2);
        }
        // control de registros

        if (vResultado.size() == 0) {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg7.Text"));
          msgBox.show();
          msgBox = null;
          this.setLimpiarStatus();

          modoOperacion = modoNORMAL;
          Inicializar();
          return false;
        }
        else {
          if (lista.getState() == CLista.listaINCOMPLETA) {
            this.setTotalRegistros(res.getString("this.TotalRegistros"));
          }
          else {
            this.setTotalRegistros(res.getString("this1.TotalRegistros"));
          }
          this.setRegistrosMostrados( (new Integer(vResultado.size() / 3)).
                                     toString());
          // establece las matrices de datos
          Vector retval = new Vector();
          retval.addElement("DS_ENFERMEDAD = DS_ENFERMEDAD");
          retval.addElement("NM_CASOS = NM_CASOS");
          retval.addElement("DS_INDICADORES = DS_INDICADORES");
          dataHandler.RegisterTable(vResultado, "SIVE_C7", retval, null);
          erwClient.setInputProperties(erw.GetTemplateManager(),
                                       erw.getDATReader(true));

          // repintado
          //AIC
          this.pack();
          erwClient.prv_setActivePage(0);
          //erwClient.refreshReport(true);

          modoOperacion = modoNORMAL;
          Inicializar();
          return true;
        }
      }
      else {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg8.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return false;
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();

      modoOperacion = modoNORMAL;
      Inicializar();
      return false;
    }

  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;
    String sEtiqueta[] = {
        "CRITERIO1",
        "CRITERIO2",
    };
    int iEtiqueta = 0;

    // plantilla
    TemplateManager tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
    tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

    // carga los citerios
//    tm.SetLabel("LAB005", "A�o: " + parCons.anoEpi );
//    tm.SetLabel("LAB007", "Semana: " + parCons.semEpi );

    tm.SetLabel(sEtiqueta[iEtiqueta],
                res.getString("msg9.Text") + parCons.anoEpi
                + res.getString("msg10.Text") + parCons.semEpi);
    iEtiqueta++;

    if (!parCons.area.equals(new String())) {
//      tm.SetLabel("LAB008", res.getString("msg11.Text") + parCons.area );
      tm.SetLabel(sEtiqueta[iEtiqueta],
                  app.getNivel1() + ": " + parCons.area + " " +
                  pan.txtDesArea.getText());

      if (!parCons.distrito.equals(new String())) {
//      tm.SetLabel("LAB009", "Distrito: " + parCons.distrito );
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    tm.GetLabel(sEtiqueta[iEtiqueta]) + ", " + app.getNivel2() +
                    ":" + " " + parCons.distrito + " " + pan.txtDesDist.getText());
      }
      iEtiqueta++;
    }

    // oculta criterios no informados
    for (int i = iEtiqueta; i < 2; i++) {
      tm.SetLabel(sEtiqueta[i], "");
    }

  }

  public void MostrarGrafica() {}

}
