//Title:        Consulta de alarmas automaticas de una semana epidemilogica.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Se presentara la relacion de enfermedades
//notificadas en la semana elegida con el numero
//de casos y los indicadoes superados con su umbral.

package ConsAlaAuto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import capp.CLista;
import sapp.DBServlet;

public class SrvConsAlaAuto
    extends DBServlet {

  // modos
  final int ALA_AUTO_SEM = 1;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    String where = null;
    ResultSet rs = null;

    Vector enfermedades = new Vector();
    Vector indicadores = new Vector();
    Vector resultado = new Vector();
    // control
    int i;
    int iEstado;
    String sCodigos = null;

    // parametros de consulta
    parConsAlaAuto parCons;

    // buffers
    String sCod;
    int iNum;

    // objetos de datos
    CLista data = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    parCons = (parConsAlaAuto) param.firstElement();

    // modos de operaci�n
    switch (opmode) {

      // consulta sobre resumen edo
      case ALA_AUTO_SEM:

        query = "select r.cd_enfcie, sum(r.nm_casos), ds_proceso "
            + " from sive_resumen_edos r, sive_procesos p"
            + " where r.cd_semepi = ? and "
            + " r.cd_anoepi = ? and "
            + " r.cd_enfcie in (select cd_enfcie from sive_enferedo where it_baja='N' and (cd_tvigi='A' or cd_tvigi='I')) and "
            + " r.cd_enfcie = p.cd_enfcie";

        if (!parCons.area.equals("")) {
          query = query + " and r.cd_nivel_1 = ? ";
        }
        if (!parCons.distrito.equals("")) {
          query = query + " and r.cd_nivel_2 = ? ";
        }

        query = query + " group by r.cd_enfcie, ds_proceso ";
        query = query + " order by r.cd_enfcie ";

        int par = 1;
        st = con.prepareStatement(query);
        st.setString(par, parCons.semEpi);
        par++;
        st.setString(par, parCons.anoEpi);
        par++;
        if (!parCons.area.equals("")) {
          st.setString(par, parCons.area);
          par++;
        }
        if (!parCons.distrito.equals("")) {
          st.setString(par, parCons.distrito);
          par++;
        }
        rs = st.executeQuery();

        //Cuando pidan mas datos (para ir hasta la pagina correcta)
        //  - pasar las enfermedades hasta la ultima procesada
        //    en la pag. anterior
        //  - pasar los indicadores hasta el ultimo procesado
        //    en la pag. anterior

        String enfCasos[] = new String[3];
        boolean pasarEnf = parCons.numPagina > 0;
        boolean pasarInd = parCons.numPagina > 0;
        while (pasarEnf) {

          if (rs.next()) {
            String enfe = rs.getString(1);
            if (enfe.compareTo(parCons.ultEnf) == 0) {
              enfCasos = new String[3];
              enfCasos[0] = enfe;
              enfCasos[1] = rs.getString(2);
              enfCasos[2] = rs.getString(3);

              // porque puede que no se comprobaran todos
              // los indicadores.
              enfermedades.addElement(enfCasos);
              pasarEnf = false;
            }
            else {
            }
          }
        }

        // Se toman el resto de las enfermedades
        while (rs.next()) {
          enfCasos = new String[3];
          enfCasos[0] = rs.getString(1);
          enfCasos[1] = rs.getString(2);
          enfCasos[2] = rs.getString(3);
          enfermedades.addElement(enfCasos);
          String aux[] = new String[3];
          aux = (String[]) enfermedades.lastElement();

        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        // Se tiene el codigo de las enfermedades pero es necesaria
        // su descripcion
        /*
                String edos = "(";
                for (int s=0; s<enfermedades.size(); s++){
                    String aux[] = new String[3];
                    aux = (String[]) enfermedades.elementAt(s);
                    edos += "'" + aux[0] + "',";
                }
                edos = edos.substring(0, edos.lastIndexOf(',')) + ")";
                query = " select cd_enfcie, ds_proceso from sive_procesos "
             + " where  cd_enfcie in (select cd_enfcie from sive_enferedo where it_baja='N')"
                      //" + edos
                      + " order by cd_enfcie ";
                //# System_Out.println("SrvConsAlaAuto: " + query);
                st = con.prepareStatement(query);
                //# System_Out.println("SrvConsAlaAuto: st = con.prepareStatement(query);" );
                rs = st.executeQuery();
                //# System_Out.println("SrvConsAlaAuto:rs = st.executeQuery();");
                int indEnf = 0;
                while (rs.next()) {
                  //# System_Out.println("SrvConsAla: entro while");
                  String reg[] = new String[3];
                  reg = (String[]) enfermedades.elementAt(indEnf);
                  String nuevoReg[] = new String[3];
                  nuevoReg[0] = reg[0];
                  nuevoReg[1] = reg[1];
                  nuevoReg[2] = rs.getString(2);
                  enfermedades.insertElementAt(nuevoReg, indEnf);
                  reg = (String[]) enfermedades.elementAt(indEnf);
                  //# System_Out.println("SrvConsAla: correspondencia "
                                    + reg[0] + " "
                                    + reg[1] + " "
                                    + reg[2] );
                  indEnf++;
                }
                rs.close();
                rs = null;
                st.close();
                st = null;
                //# System_Out.println("SrvConsAlaAuto: ya tengo las descripciones ");
         */
        // Se obtienen los indicadores y sus valores
        query = " select cd_indalar, nm_valor from sive_alarma "
            + " where ((cd_indalar like (?)) or (cd_indalar like (?)))"
            + " and cd_semepi = ? and "
            + " cd_anoepi = ? ";

        // Vector resultado
        // ( enfer1, enfer2, ... , enferN )
        // Vector enf
        // ( cd_enfcie, nm_casos, (ind1,%1), ... , (indS, %S) )

        int lineas = 1;
        iEstado = CLista.listaVACIA;

        String ultE = new String();
        String ultI = new String();
        enfCasos = new String[3];
        String indi[] = new String[2];
        Integer casos = null;
        Vector enf = null;
        for (int j = 0; j < enfermedades.size(); j++) {
          // Control del tama�o
          if ( (lineas > DBServlet.maxSIZE) && (! (parCons.bInformeCompleto))) {
            iEstado = CLista.listaINCOMPLETA;
            //E //# System_Out.println("SrvConsAlaAuto: ultEnf " + ultE
            //E                                 + " ultInd " + ultI);
            ultE = enfCasos[0];
            ultI = indi[0];
            break;
          }
          // control de estado
          if (iEstado == CLista.listaVACIA) {
            iEstado = CLista.listaLLENA;

          }
          enf = new Vector();
          enfCasos = (String[]) enfermedades.elementAt(j);
          enf.addElement(enfCasos[0]); // codigo
          enf.addElement(enfCasos[2]); // Descripcion
          enf.addElement(enfCasos[1]);
          casos = new Integer(enfCasos[1]);

          if (casos.intValue() > 0) { //E

            st = con.prepareStatement(query);

            //cuidado con traer indicadores de otras areas
            //y distritos

            //primero los de CC
            if (enfCasos[0].length() == 5) {

              //st.setString(1, "CC%_" + enfCasos[0] + "%");
              st.setString(1, "CC%_" + enfCasos[0] + "___");
            }
            else {

              //st.setString(1, "CC%" + enfCasos[0] + "%");
              st.setString(1, "CC%" + enfCasos[0] + "___");

              //despues los de area y distrito si procede
            }
            String area = "";
            String dist = "";
            if (!parCons.area.equals("")) {
              if (parCons.area.trim().length() == 1) {
                area = "_" + parCons.area.trim();
              }
              else {
                area = parCons.area.trim();
              }
              if (!parCons.distrito.equals("")) {
                if (parCons.distrito.trim().length() == 1) {
                  dist = "_" + parCons.distrito.trim();
                }
                else {
                  dist = parCons.distrito.trim();
                  // area y distrito
                }
                if (enfCasos[0].length() == 5) {
                  st.setString(2, area + dist + "%_" + enfCasos[0] + "___");
                }
                else {
                  st.setString(2, area + dist + "%" + enfCasos[0] + "___");
                }
              }
              else { //area, no distrito
                if (enfCasos[0].length() == 5) {
                  st.setString(2, area + "%_" + enfCasos[0] + "___");
                }
                else {
                  st.setString(2, area + "%" + enfCasos[0] + "___");
                }
              }
            }
            else { //no area, no distrito
              if (enfCasos[0].length() == 5) {
                st.setString(2, "%_" + enfCasos[0] + "___");
              }
              else {
                st.setString(2, "%" + enfCasos[0] + "___");
              }
            }

            st.setString(3, parCons.semEpi);
            st.setString(4, parCons.anoEpi);
            //E //# System_Out.println("SrvConsAlaAuto: " + query);
            rs = st.executeQuery();

            while (pasarInd) {
              if (rs.next()) {
                String indic = rs.getString(1);
                if (indic.compareTo(parCons.ultInd) == 0) {
                  // Cuando se llega al ultimo que se proceso se para
                  // para tomar el siguiente y hacer sobre el los
                  // calculos
                  //E //# System_Out.println("SrvConsAlaAuto: ultimo indicador " + indic);
                  pasarInd = false;
                }
              }
            }
            Double umbral = null;
            while (rs.next()) {
              indi = new String[2];
              indi[0] = rs.getString(1);
              indi[1] = rs.getString(2);
              umbral = new Double(indi[1]);
              if (!indi[0].endsWith("003")) {
                umbral = new Double(umbral.doubleValue() * 1.25);

//E            if (casos.doubleValue()>umbral.doubleValue()) {
              }
              Vector indTpc = new Vector();
              indTpc.addElement(indi[0]);
              if (umbral.doubleValue() > (double) 0) {
                Double tpc = new Double( (casos.doubleValue() /
                                          umbral.doubleValue()) * (double) 100);
                //(casos.doubleValue()-valor.doubleValue())*100/casos.doubleValue());
                indTpc.addElement(tpc.toString());
              }
              else {
                indTpc.addElement("?");
              }
              enf.addElement(indTpc);
              //lineas++;
              //lineas = lineas +3;
              lineas++;
//E            }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            if (enf.size() > 2) { //Tiene indicadores
              resultado.addElement(enf);
            }
          } //E fin if
        }

        if (resultado.size() > 0) {
          data = new CLista();
          resultado = completarDescripciones(resultado, con, st, rs);
          resultado = convertirResultado(resultado);
          data.addElement(resultado);
          if (iEstado == CLista.listaINCOMPLETA) {
            data.addElement(ultE);
            data.addElement(ultI);
          }
          data.setState(iEstado);
          data.trimToSize();
        }

        break;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return data;
  }

  Vector buscarEnfInd(Vector v, String e, int j) {
    Vector res = new Vector();
    for (int i = 0 + j; i < v.size(); i++) {
      res = (Vector) v.elementAt(i);
      if (e.equals( (String) res.firstElement())) {
        return res;
      }
    }
    return new Vector();

  }

  Vector completarDescripciones(Vector v, Connection con,
                                PreparedStatement st, ResultSet rs) throws
      Exception {
    String par = "";
    // Vector resultado
    // ( enfer1, enfer2, ... , enferN )
    // Vector enf
    // ( cd_enfcie, ds_enfermedad, nm_casos, (ind1,%1), ... , (indS, %S) )
    Vector enf = new Vector();
    Vector ind = new Vector();
    for (int i = 0; i < v.size(); i++) {
      enf = (Vector) v.elementAt(i);
      if (enf.size() > 2) { //Tiene indicadores     LSR
        for (int j = 3; j < enf.size(); j++) {
          ind = (Vector) enf.elementAt(j);
          if (ind.size() > 0) {
            par = par + "'" + (String) ind.firstElement() + "',";
          }
        }
      }
    }

    Vector resultado = new Vector();
    Vector enfRes = new Vector();

    //quito la ultima coma
    if (par.length() > 0) {
      par = par.substring(0, par.length() - 1);
      String query = "select cd_enfcie, cd_indalar, ds_indalar "
          + " from sive_indicador "
          + " where cd_indalar in ( " + par + " ) ";

      //AIC
      //System_out.println("SrvConsAlaAuto : " + query);

      st = con.prepareStatement(query);
      //st.setString(1, par);
      rs = st.executeQuery();

      // Vector resultado que debe devolver
      // ( enfer1, enfer2, ... , enferN )
      // Vector enf
      // ( cd_enfcie, ds_enfermedad, nm_casos, (ind1, desind1,%1), ... , (indS, desindS,%S) )
      String enfer = "";
      String cd_ind = "";
      String ds_ind = "";
      String antEnf = "";
      enf = new Vector();
      ind = new Vector();

      while (rs.next()) {
        enfer = rs.getString(1);
        //AIC
        //System_out.println("SrvConsAlaAuto : " + enfer);

        if (antEnf.equals("")) {

          enf = buscarEnfInd(v, enfer, 0);
          //AIC el if, antes no hab�a comprobaci�n.
          if (enf != null && enf.size() != 0) {
            antEnf = enfer; //primera vez
            enfRes = new Vector();
            enfRes.addElement( (String) enf.elementAt(0));
            enfRes.addElement( (String) enf.elementAt(1));
            enfRes.addElement( (String) enf.elementAt(2));
          }
        }
        else if (!antEnf.equals(enfer)) {
          antEnf = enfer; //cuando se cambia de enfermedad
          enf = buscarEnfInd(v, enfer, 0);
          if (enf != null && enf.size() != 0) {
            resultado.addElement(enfRes);
            enfRes = new Vector();
            enfRes.addElement( (String) enf.elementAt(0));
            enfRes.addElement( (String) enf.elementAt(1));
            enfRes.addElement( (String) enf.elementAt(2));
          }
        }
        if (enf != null && enf.size() != 0) {
          cd_ind = rs.getString(2);
          ds_ind = rs.getString(3);
          ind = buscarEnfInd(enf, cd_ind, 3);
          ind.insertElementAt(ds_ind, 1);
          enfRes.addElement(ind);
        }
      }
      rs.close();
      rs = null;
      st.close();
      st = null;

      resultado.addElement(enfRes);

      if (resultado != null) {
        for (int i = 0; i < resultado.size(); i++) {
          enf = (Vector) resultado.elementAt(i);
          if (enf != null) {
            for (int j = 3; j < enf.size(); j++) {
              ind = (Vector) enf.elementAt(j);
            }
          }
        }
      }
    } // if

    // System_out.println("JRM");
    return resultado;

  }

  Vector convertirResultado(Vector v) {
    Vector res = new Vector();
    Vector enfe = new Vector();
    String ds_enf = new String();
    String casos = new String();
    String inds = new String();
    Vector ind = new Vector();
    // Vector resultado
    // ( enfer1, enfer2, ... , enferN )
    // Vector enf
    // ( cd_enfcie, ds_enfermedad,nm_casos, (ind1,dsind1,%1), ... , (indS,dsindS %S) )
    for (int j = 0; j < v.size(); j++) {

      enfe = (Vector) v.elementAt(j);
      ds_enf = (String) enfe.elementAt(1);
      casos = (String) enfe.elementAt(2);
      for (int x = 3; x < enfe.size(); x++) {
        inds = new String();
        ind = (Vector) enfe.elementAt(x);
        inds = inds + (String) ind.elementAt(0) + "-" +
            (String) ind.elementAt(1);
        String valor = (String) ind.elementAt(2);
        if (!valor.equals("")) {
          if (valor.length() > valor.indexOf('.') + 2) {
            valor = valor.substring(0, valor.indexOf('.') + 2);
          }
          if (valor.indexOf('.') != -1) {
            valor = valor.replace('.', ',');
          }
          inds = inds + ": " + valor;
        }
        if (x == 3) {
          res.addElement(new DataConsAlaAuto(ds_enf, casos, inds));
        }
        else {
          res.addElement(new DataConsAlaAuto(" ", " ", inds));
        }
      }
    }
    return res;
  }

}
