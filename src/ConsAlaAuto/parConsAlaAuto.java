//Title:        Consulta de alarmas automaticas de una semana epidemilogica.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Se presentara la relacion de enfermedades
//notificadas en la semana elegida con el numero
//de casos y los indicadoes superados con su umbral.

package ConsAlaAuto;

import java.io.Serializable;

public class parConsAlaAuto
    implements Serializable {

  public String anoEpi = new String();
  public String semEpi = new String();
  public String area = "";
  public String distrito = "";
  public int criterio = 0;

  // Para las paginas del informe
  public int numPagina = 0;
  public String ultEnf = new String();
  public String ultInd = new String();
  public boolean bInformeCompleto = false;

  public parConsAlaAuto() {
  }

  public parConsAlaAuto(String ano, String s, String ar, String d) {
    anoEpi = ano;
    semEpi = s;
    area = ar;
    distrito = d;
  }
}
