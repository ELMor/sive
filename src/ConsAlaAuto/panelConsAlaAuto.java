//Title:        Consulta de alarmas automaticas de una semana epidemilogica.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Se presentara la relacion de enfermedades
//notificadas en la semana elegida con el numero
//de casos y los indicadoes superados con su umbral.

package ConsAlaAuto;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
//import eqNot.*;
import utilidades.PanFechas;
import zbs.DataZBS2;

public class panelConsAlaAuto
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  //Modos de operacion del servlet
  final int ALA_AUTO_SEM = 1;

  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/vaciar.gif",
      "images/grafico.gif"};

  protected CCargadorImagen imgs = null;

  protected StubSrvBD stubCliente = new StubSrvBD();

  public parConsAlaAuto param;
  protected panelInfAlaAuto informe = null;

  protected int modoOperacion = modoNORMAL;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  XYLayout xYLayout = new XYLayout();

  Label lblFecha = new Label();
  Label lblArea = new Label();
  CCampoCodigo txtCodArea = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarArea = new ButtonControl();
  TextField txtDesArea = new TextField();
  PanFechas panelFecha;
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  focusAdapter txtFocusAdapter = new focusAdapter(this);

  txt_keyAdapter txtKeyAdapter = new txt_keyAdapter(this);

  actionListener btnActionListener = new actionListener(this);
  CheckboxGroup checkboxAgrupar = new CheckboxGroup();
  Label lblDist = new Label();
  CCampoCodigo txtCodDist = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarDist = new ButtonControl();
  TextField txtDesDist = new TextField();

  public panelConsAlaAuto(CApp a) {
    try {
      setApp(a);
      res = ResourceBundle.getBundle("ConsAlaAuto.Res" + a.getIdioma());
      informe = new panelInfAlaAuto(a, this);
      panelFecha = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      jbInit();
      this.txtCodArea.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesArea.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtCodDist.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesDist.setText(this.app.getDS_NIVEL2_DEFECTO());
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(301);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));
    lblDist.setText(res.getString("lblDist.Text"));
    txtCodDist.addKeyListener(txtKeyAdapter);
    txtCodDist.addFocusListener(txtFocusAdapter);
    txtCodDist.setName("txtCodDist");
    txtCodDist.setBackground(Color.white);
    btnCtrlBuscarDist.setActionCommand("buscarDist");
    txtDesDist.setEditable(false);
    txtDesDist.setEnabled(false); /*E*/

    // gestores de eventos

    btnCtrlBuscarArea.addActionListener(btnActionListener);
    btnCtrlBuscarDist.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    txtCodArea.addKeyListener(txtKeyAdapter);

    txtCodArea.addFocusListener(txtFocusAdapter);

    //btnCtrlBuscarEnfer.setLabel("btnCtrlBuscarPro1");
    btnCtrlBuscarArea.setActionCommand("buscarArea");
    txtDesArea.setEditable(false);
    txtDesArea.setEnabled(false); /*E*/
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    txtCodArea.setName("txtCodArea");
    txtCodArea.setBackground(Color.white);

    lblArea.setText(res.getString("lblArea.Text"));
    lblFecha.setText(res.getString("lblFecha.Text"));

    xYLayout.setWidth(596);

    this.setLayout(xYLayout);

    this.add(lblFecha, new XYConstraints(26, 42, 51, -1));
    this.add(panelFecha, new XYConstraints(40, 42, -1, -1));
    this.add(lblArea, new XYConstraints(26, 82, 101, -1));
    this.add(txtCodArea, new XYConstraints(143, 82, 77, -1));
    this.add(btnCtrlBuscarArea, new XYConstraints(225, 82, -1, -1));
    this.add(txtDesArea, new XYConstraints(254, 82, 287, -1));
    this.add(lblDist, new XYConstraints(26, 122, 101, -1));
    this.add(txtCodDist, new XYConstraints(143, 122, 77, -1));
    this.add(btnCtrlBuscarDist, new XYConstraints(225, 122, -1, -1));
    this.add(txtDesDist, new XYConstraints(254, 122, 287, -1));
    this.add(btnLimpiar, new XYConstraints(389, 162, -1, -1));
    this.add(btnInforme, new XYConstraints(475, 162, -1, -1));

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnCtrlBuscarArea.setImage(imgs.getImage(0));
    btnCtrlBuscarDist.setImage(imgs.getImage(0));
    btnLimpiar.setImage(imgs.getImage(1));
    btnInforme.setImage(imgs.getImage(2));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (panelFecha.txtAno.getText().length() == 0)) {
      bDatosCompletos = false;
    }
    if ( (panelFecha.txtCodSem.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtCodArea.setEnabled(true);
        //txtDesArea.setEnabled(true); /*E*/
        btnCtrlBuscarArea.setEnabled(true);
        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        btnCtrlBuscarArea.setEnabled(true);
        // control area
        if (!txtDesArea.getText().equals("")) {
          btnCtrlBuscarDist.setEnabled(true);
          txtCodDist.setEnabled(true);
          //txtDesDist.setEnabled(true); /*E*/
        }
        else {
          btnCtrlBuscarDist.setEnabled(false);
          txtCodDist.setEnabled(false);
          //txtDesDist.setEnabled(false); /*E*/
          txtCodDist.setText("");
          //txtDesDist.setText(""); /*E*/
        }

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCodArea.setEnabled(false);
        btnCtrlBuscarArea.setEnabled(false);
        txtCodDist.setEnabled(false);
        btnCtrlBuscarDist.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //Busca los niveles1 (area de salud)
  void btnCtrlbuscarArea_actionPerformed(ActionEvent evt) {
    DataNivel1 data = null;
    CMessage mensaje = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaNivel1 lista = new CListaNivel1(app,
                                            res.getString("msg3.Text") +
                                            this.app.getNivel1(),
                                            stubCliente,
                                            strSERVLETNivel1,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }

    if (data != null) {
      txtCodArea.removeKeyListener(txtKeyAdapter);
      txtCodArea.setText(data.getCod());
      txtDesArea.setText(data.getDes());
      txtCodDist.setText("");
      txtDesDist.setText("");
      txtCodArea.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //Busca los niveles2 (distritos)
  //busca el distrito (Niv2)
  void btnCtrlbuscarDist_actionPerformed(ActionEvent evt) {

    DataZBS2 data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg4.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLETNivel2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodDist.removeKeyListener(txtKeyAdapter);
      txtCodDist.setText(data.getNiv2());
      txtDesDist.setText(data.getDes());
      txtCodDist.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    txtCodArea.setText("");
    txtDesArea.setText("");
    txtCodDist.setText("");
    txtDesDist.setText("");

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    param = new parConsAlaAuto();
    CLista par = null;
    CLista resul = null;

    if (isDataValid()) {
      if (!txtDesArea.getText().equals("")) {
        param.area = txtCodArea.getText();
      }

      if (!txtDesDist.getText().equals("")) {
        param.distrito = txtCodDist.getText();
      }

      param.anoEpi = panelFecha.txtAno.getText();
      if (panelFecha.txtCodSem.getText().length() == 1) {
        param.semEpi = "0" + panelFecha.txtCodSem.getText();
      }
      else {
        param.semEpi = panelFecha.txtCodSem.getText();

      }
      param.criterio = ALA_AUTO_SEM;
      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();
      informe.setEnabled(true);
      informe.parCons = param;
      //E //# System_Out.println("Voy a generar el informe");
      boolean hay = informe.GenerarInforme();
      if (hay) {
        informe.show();

      }
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg5.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    // datos de envio
    DataNivel1 nivel1;
    DataZBS2 nivel2;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtCodArea")) &&
        (txtCodArea.getText().length() > 0)) {
      //E //# System_Out.println("Pierde el foco el textArea");
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataNivel1(txtCodArea.getText()));
      strServlet = strSERVLETNivel1;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodDist")) &&
             (txtCodDist.getText().length() > 0)) {
      //E //# System_Out.println("Pierde el foco el textDist");
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS2(txtCodArea.getText(), txtCodDist.getText(),
                                    "", ""));
      strServlet = strSERVLETNivel2;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }

    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtCodArea")) {
            //E //# System_Out.println("realiza el cast y rellena los datos del area");
            nivel1 = (DataNivel1) param.firstElement();
            txtCodArea.removeKeyListener(txtKeyAdapter);
            txtCodArea.setText(nivel1.getCod());
            txtDesArea.setText(nivel1.getDes());
            txtCodArea.addKeyListener(txtKeyAdapter);
          }
          else if (txt.getName().equals("txtCodDist")) {
            //E //# System_Out.println("realiza el cast y rellena los datos del dist");
            nivel2 = (DataZBS2) param.firstElement();
            txtCodDist.removeKeyListener(txtKeyAdapter);
            txtCodDist.setText(nivel2.getNiv2());
            txtDesDist.setText(nivel2.getDes());
            txtCodDist.addKeyListener(txtKeyAdapter);
          }
        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg6.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  void imprimeVector(Vector v) {
    Vector aux = new Vector();
    //E //# System_Out.println("( ");
    for (int s = 0; s < v.size(); s++) {
      try {
        aux = (Vector) v.elementAt(s);
        imprimeVector(aux);
        ////E //# System_Out.println("");
      }
      catch (ClassCastException ex) {
        //E // System_out_print(" " + (String) v.elementAt(s) + " ");
      }
    }
    //E //# // System_out_println(") ");
  }

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtCodArea")) &&
        (txtDesArea.getText().length() > 0)) {
      txtDesArea.setText("");
      txtCodDist.setText("");
      txtDesDist.setText("");

    }
    else if ( (txt.getName().equals("txtCodDist")) &&
             (txtDesDist.getText().length() > 0)) {
      txtDesDist.setText("");
    }
    Inicializar();
  }
} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  panelConsAlaAuto adaptee = null;
  ActionEvent e = null;

  public actionListener(panelConsAlaAuto adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarArea")) {
      adaptee.btnCtrlbuscarArea_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarDist")) {
      adaptee.btnCtrlbuscarDist_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  panelConsAlaAuto adaptee;
  FocusEvent event;

  focusAdapter(panelConsAlaAuto adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

////////////////////// Clases para listas

// lista de valores
class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

class CListaZBS2
    extends CListaValores {

  protected panelConsAlaAuto panel;

  public CListaZBS2(panelConsAlaAuto p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtCodArea.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  panelConsAlaAuto adaptee;

  txt_keyAdapter(panelConsAlaAuto adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}
/*
 import java.awt.*;
 import capp.*;
 import sapp.*;
 import com.borland.jbcl.control.*;
 import com.borland.jbcl.layout.*;
 import java.net.*;
 import java.awt.event.*;
 import nivel1.*;
 import zbs.*;
 import eqNot.*;
 import infcobper.*;
 public class panelConsAlaAuto extends CPanel {
  // im�genes
  final String imgLUPA= "images/Magnify.gif";
  final String imgLIMPIAR = "images/vaciar.gif";
  final String imgGENERAR = "images/grafico.gif";
  // Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  final int modoESPERA = 1;
  protected int modoOperacion = modoNORMAL;
  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  // constantes del panel
  protected String areaActual = "";
  protected String distritoActual = "";
  final String strSERVLETNiv1="servlet/SrvNivel1";
  final String strSERVLETzbs="servlet/SrvZBS2";
  final String strSERVLETsemana="servlet/SrvInfCobSem";
  // stub's
  protected StubSrvBD stubZBS=null;
  protected StubSrvBD stubZBS2=null;
  protected StubSrvBD stubSemana=null;
  final int erwCASOS_EQUIPO = 1;
  final int erwCASOS_CENTRO = 2;
  protected Pan_InfCobSem informe;
    // controles de la ventana
  GroupBox groupBox1 = new GroupBox();
  XYLayout xYLayout1 = new XYLayout();
  Label label1 = new Label();
  XYLayout xYLayout2 = new XYLayout();
  Label label2 = new Label();
  TextField txtAnyo = new TextField();
  TextField txtSemana = new TextField();
  Label label3 = new Label();
  TextField txtArea = new TextField();
  ButtonControl btnArea = new ButtonControl();
  CheckboxGroup opciones = new CheckboxGroup();
  Checkbox chkEquipo = new Checkbox();
  Checkbox chkCentro = new Checkbox();
  Label label4 = new Label();
  TextField txtAreaDesc = new TextField();
  TextField txtMun = new TextField();
  TextField txtMunDesc = new TextField();
  ButtonControl btnMun = new ButtonControl();
  Label label5 = new Label();
  ButtonControl btnObtener = new ButtonControl();
  ButtonControl btnLimpiar = new ButtonControl();
  public panelConsAlaAuto(CApp a, Pan_InfCobSem pi) {
    try  {
      setApp(a);
      informe = pi;
      jbInit();
      informe.setEnabled(false);
      opciones.setSelectedCheckbox(chkCentro);
      stubZBS= new StubSrvBD(new URL(this.app.getURL() + strSERVLETNiv1));
      stubZBS2= new StubSrvBD(new URL(this.app.getURL() + strSERVLETzbs));
      stubSemana= new StubSrvBD(new URL(this.app.getURL() + strSERVLETsemana));
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  void jbInit() throws Exception {
    label2.setForeground(Color.black);
    label2.setFont(new Font("Dialog", 0, 12));
    label1.setForeground(Color.black);
    label1.setFont(new Font("Dialog", 0, 12));
    label1.setText("A�o:");
    label2.setText("Semana:");
    txtAnyo.setBackground(new Color(255, 255, 150));
    txtAnyo.setForeground(Color.black);
    txtAnyo.setFont(new Font("Dialog", 0, 12));
    txtSemana.setBackground(new Color(255, 255, 150));
    txtSemana.setForeground(Color.black);
    txtSemana.setFont(new Font("Dialog", 0, 12));
    label3.setText("Area:");
    txtArea.setBackground(new Color(255, 255, 150));
    txtArea.addKeyListener(new panelConsAlaAuto_txtArea_keyAdapter(this));
    btnArea.setLabel("");
    btnArea.setImageURL(new URL(app.getCodeBase(),imgLUPA));
     btnArea.addActionListener(new panelConsAlaAuto_btnArea_actionAdapter(this));
    chkEquipo.setLabel("Equipo");
    chkEquipo.setCheckboxGroup(opciones);
    chkCentro.setLabel("Centro");
    chkCentro.setCheckboxGroup(opciones);
    label4.setFont(new Font("Dialog", 1, 12));
    label4.setText("Agrupar por:");
    txtAreaDesc.setEditable(false);
    txtMun.addKeyListener(new panelConsAlaAuto_txtMun_keyAdapter(this));
    txtMunDesc.setEditable(false);
    btnMun.setLabel("");
    btnMun.setImageURL(new URL(app.getCodeBase(),imgLUPA));
    btnMun.addActionListener(new panelConsAlaAuto_btnMun_actionAdapter(this));
    label5.setText("Distrito:");
    btnObtener.setLabel("Obtener");
    btnObtener.setImageURL(new URL(app.getCodeBase(),this.imgGENERAR));
    btnObtener.addActionListener(new panelConsAlaAuto_btnObtener_actionAdapter(this));
    btnLimpiar.setLabel("Limpiar");
    btnLimpiar.setImageURL(new URL(app.getCodeBase(),this.imgLIMPIAR));
    btnLimpiar.addActionListener(new panelConsAlaAuto_btnLimpiar_actionAdapter(this));
    txtMun.addKeyListener(new panelConsAlaAuto_txtMun_keyAdapter(this));
    this.setLayout(xYLayout1);
    //opciones.CheckboxGroup();
    groupBox1.setForeground(new Color(155, 0, 0));
    groupBox1.setLayout(xYLayout2);
    groupBox1.setFont(new Font("Dialog", 3, 12));
    groupBox1.setLabel("Cobertura de una semana epidemiol�gica");
    this.add(label1, new XYConstraints(28, 37, -1, -1));
    this.add(label2, new XYConstraints(178, 37, -1, -1));
    this.add(txtAnyo, new XYConstraints(70, 37, 56, -1));
    this.add(txtSemana, new XYConstraints(246, 37, 29, -1));
    this.add(label3, new XYConstraints(28, 69, 38, -1));
    this.add(txtArea, new XYConstraints(70, 69, 56, -1));
    this.add(chkEquipo, new XYConstraints(208, 140, -1, -1));
    this.add(chkCentro, new XYConstraints(132, 140, -1, -1));
    //this.add(opciones, new XYConstraints(0, 0, 30, 30));
    this.add(label4, new XYConstraints(28, 140, -1, -1));
    this.add(btnArea, new XYConstraints(130, 69, -1, -1));
    this.add(txtAreaDesc, new XYConstraints(169, 69, 106, -1));
    this.add(txtMun, new XYConstraints(70, 101, 56, -1));
    this.add(txtMunDesc, new XYConstraints(169, 101, 106, -1));
    this.add(btnMun, new XYConstraints(130, 101, -1, -1));
    this.add(label5, new XYConstraints(28, 101, 41, -1));
    this.add(btnObtener, new XYConstraints(193, 222, -1, -1));
    this.add(btnLimpiar, new XYConstraints(110, 222, -1, -1));
    this.add(groupBox1, new XYConstraints(7, 12, 385, 280));
  }
  public void Inicializar(){
    switch(modoOperacion){
      case modoNORMAL:
        if (txtAreaDesc.getText().compareTo("") != 0){
          btnMun.setEnabled(true);
        }else{
          btnMun.setEnabled(false);
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        btnArea.setEnabled(true);
        btnLimpiar.setEnabled(true);
        btnObtener.setEnabled(true);
      break;
      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        btnMun.setEnabled(false);
        btnArea.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnObtener.setEnabled(false);
      break;
    }
    this.doLayout();
  }
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;
    boolean b = false;
    // comprueba que esten informados los campos obligatorios
    if ((txtAnyo.getText().length() > 0)&&(txtSemana.getText().length() > 0)&&
    (areaActual.length() > 0)) {
      b = true;
      if (txtAnyo.getText().length() > 4){
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtAnyo.selectAll();
      }
      if (txtSemana.getText().length() > 2){
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtSemana.selectAll();
      }
    // advertencia de requerir datos
    } else {
      msg = "Faltan campos obligatorios.";
    }
    if (!b){
      msgBox = new CMessage(app,CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }
    return b;
  }
  void btnArea_actionPerformed(ActionEvent e) {
    DataNivel1 data;
    int modo = modoOperacion;
    CMessage mensaje = null;
    CLista lr;
    CLista datos;
    modoOperacion=modoESPERA;
    Inicializar();
    try{
    CListaNivel1 lista = new CListaNivel1(app,
                                          "Selecci�n de Area",
                                          stubZBS,
                                          strSERVLETNiv1,
                                          servletOBTENER_X_CODIGO,
                                          servletOBTENER_X_DESCRIPCION,
                                          servletSELECCION_X_CODIGO,
                                          servletSELECCION_X_DESCRIPCION);
    lista.show();
    data = (DataNivel1) lista.getComponente();
    modoOperacion = modo;
    if (data != null) {
      txtArea.setText(data.getCod());
      txtAreaDesc.setText(data.getDes());
      areaActual = data.getCod();
      btnMun.setEnabled(true);
    }
    }catch (Exception excepc){
      excepc.printStackTrace();
      mensaje=new CMessage(this.app,CMessage.msgERROR,excepc.getMessage());
      mensaje.show();
      modoOperacion=modo;
    }
    Inicializar();
  }
  void txtArea_keyPressed(KeyEvent e) {
    areaActual = "";
    txtAreaDesc.setText("");
    btnMun.setEnabled(false);
    distritoActual = "";
    txtMun.setText("");
    txtMunDesc.setText("");
  }
  void btnMun_actionPerformed(ActionEvent e) {
    DataZBS2 data;
    CMessage msgBox;
    int modo = modoOperacion;
    try{
    modoOperacion = modoESPERA;
    Inicializar();
    CListaZBS2 lista = new CListaZBS2(this,
                                      "Selecci�n de "+app.getNivel2(),
                                      stubZBS2,
                                      strSERVLETzbs,
                                      servletOBTENER_NIV2_X_CODIGO,
                                      servletOBTENER_NIV2_X_DESCRIPCION,
                                      servletSELECCION_NIV2_X_CODIGO,
                                      servletSELECCION_NIV2_X_DESCRIPCION);
    lista.show();
    data = (DataZBS2) lista.getComponente();
    modoOperacion = modo;
    if (data != null) {
      txtMun.setText(data.getNiv2());
      txtMunDesc.setText(data.getDes());
      btnMun.setEnabled(true);
      modoOperacion = modo;
    }
  }catch (Exception er){
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
      modoOperacion = modo;
  }
  Inicializar();
  }
  void txtMun_keyPressed(KeyEvent e) {
    distritoActual = "";
    txtMunDesc.setText("");
  }
  void btnLimpiar_actionPerformed(ActionEvent e) {
    modoOperacion = modoESPERA;
    Inicializar();
    txtAnyo.setText("");
    txtArea.setText("");
    txtAreaDesc.setText("");
    txtMun.setText("");
    txtMunDesc.setText("");
    txtSemana.setText("");
    areaActual = "";
    distritoActual = "";
    btnMun.setEnabled(false);
    modoOperacion = modoNORMAL;
    Inicializar();
  }
  void btnObtener_actionPerformed(ActionEvent e) {
    CMessage msgBox;
    informe.paramC1 = new ParamC11_12();
    if (isDataValid()) {
      if ((distritoActual.compareTo("") != 0)&&(distritoActual != null)){
        informe.paramC1.sEN = distritoActual;
      }else{
        informe.paramC1.sEN = "";
      }
      informe.paramC1.sAnoI = txtAnyo.getText();
      //informe.paramC1.sAnoF = txtAnyoHasta.getText();
      informe.paramC1.sSemanaI = txtSemana.getText();
      //informe.paramC1.sSemanaF = txtSemanaHasta.getText();
      informe.paramC1.sCN = areaActual;
      if(chkCentro.getState())
        informe.paramC1.sGrupo = "2";
      else
        informe.paramC1.sGrupo = "1";
      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();
      informe.setEnabled(true);
      informe.GenerarInforme();
      modoOperacion = modoNORMAL;
      Inicializar();
    } else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA, "Faltan campos obligatorios.");
      msgBox.show();
      msgBox = null;
    }
  }
 }
 class panelConsAlaAuto_btnArea_actionAdapter implements java.awt.event.ActionListener, Runnable{
  panelConsAlaAuto adaptee;
  ActionEvent e;
  panelConsAlaAuto_btnArea_actionAdapter(panelConsAlaAuto adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }
  public void run(){
    adaptee.btnArea_actionPerformed(e);
  }
 }
// lista de valores
 class CListaNivel1 extends CListaValores {
  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
  }
  public Object setComponente(String s) {
    return new DataNivel1(s);
  }
  public String getCodigo(Object o) {
    return ( ((DataNivel1)o).getCod() );
  }
  public String getDescripcion(Object o) {
    return ( ((DataNivel1)o).getDes() );
  }
 }
 class panelConsAlaAuto_txtArea_keyAdapter extends java.awt.event.KeyAdapter {
  panelConsAlaAuto adaptee;
  panelConsAlaAuto_txtArea_keyAdapter(panelConsAlaAuto adaptee) {
    this.adaptee = adaptee;
  }
  public void keyPressed(KeyEvent e) {
    adaptee.txtArea_keyPressed(e);
  }
 }
 class panelConsAlaAuto_btnMun_actionAdapter implements java.awt.event.ActionListener {
  panelConsAlaAuto adaptee;
  panelConsAlaAuto_btnMun_actionAdapter(panelConsAlaAuto adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.btnMun_actionPerformed(e);
  }
 }
 class panelConsAlaAuto_txtMun_keyAdapter extends java.awt.event.KeyAdapter {
  panelConsAlaAuto adaptee;
  panelConsAlaAuto_txtMun_keyAdapter(panelConsAlaAuto adaptee) {
    this.adaptee = adaptee;
  }
  public void keyPressed(KeyEvent e) {
    adaptee.txtMun_keyPressed(e);
  }
 }
  // lista de valores
 class CListaZBS2 extends CListaValores {
  protected panelConsAlaAuto panel;
  public CListaZBS2(panelConsAlaAuto p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super( p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    panel = p;
  }
  public Object setComponente(String s) {
    return new DataZBS2(panel.txtArea.getText(),s,"","");
  }
  public String getCodigo(Object o) {
    return ( ((DataZBS2)o).getNiv2() );
  }
  public String getDescripcion(Object o) {
    return ( ((DataZBS2)o).getDes() );
  }
 }
 class panelConsAlaAuto_btnLimpiar_actionAdapter implements java.awt.event.ActionListener {
  panelConsAlaAuto adaptee;
  panelConsAlaAuto_btnLimpiar_actionAdapter(panelConsAlaAuto adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    adaptee.btnLimpiar_actionPerformed(e);
  }
 }
 class panelConsAlaAuto_btnObtener_actionAdapter implements java.awt.event.ActionListener, Runnable{
  panelConsAlaAuto adaptee;
  ActionEvent e;
  panelConsAlaAuto_btnObtener_actionAdapter(panelConsAlaAuto adaptee) {
    this.adaptee = adaptee;
  }
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }
  public void run(){
    adaptee.btnObtener_actionPerformed(e);
  }
 */
