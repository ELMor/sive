package eapp2;

import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.PrintJob;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;

public class ControlPanel2
    extends Panel {
  XYLayout xYLayout = new XYLayout();
  ResourceBundle res;
  ButtonControl btnImprimir = new ButtonControl();
  ButtonControl btnSalir = new ButtonControl();
  GPanel gpanel = null;
  ControlPanel2_actionAdapter actionAdapter = new ControlPanel2_actionAdapter(this);
  Checkbox chLeyenda = new Checkbox();
  Checkbox chGrid = new Checkbox();

  public ControlPanel2(GPanel p, int tipo) {
    try {
      gpanel = p;
      jbInit(tipo);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit(int tipo) throws Exception {
    xYLayout.setHeight(26);
    btnImprimir.setLabel("Imprimir");
    btnSalir.setLabel("Salir");
    xYLayout.setWidth(800);
    this.setLayout(xYLayout);

    // im�genes
    Image imgImprimir = gpanel.getApp().getImage(gpanel.getApp().getCodeBase(),
                                                 "images/imprimir.gif");
    Image imgSalir = gpanel.getApp().getImage(gpanel.getApp().getCodeBase(),
                                              "images/salir.gif");
    chLeyenda.setLabel("Mostrar leyenda");
    chLeyenda.addItemListener(new ControlPanel2_chLeyenda_itemAdapter(this));
    chGrid.setLabel("Mostrar grid");
    chGrid.addItemListener(new ControlPanel2_chGrid_itemAdapter(this));
    btnSalir.addActionListener(actionAdapter);
    btnImprimir.addActionListener(actionAdapter);
    btnImprimir.setActionCommand("imprimir");
    btnImprimir.setImage(imgImprimir);
    btnSalir.setActionCommand("salir");
    btnSalir.setImage(imgSalir);

    this.add(chLeyenda, new XYConstraints(14, 0, 138, 24));
    this.add(chGrid, new XYConstraints(178, 0, 182, 24));
    this.add(btnImprimir, new XYConstraints(631, 0, 80, -1));
    this.add(btnSalir, new XYConstraints(715, 0, 80, -1));

    switch (tipo) {
      case PanelChart.CURVA_EPIDEMICA:
        break;
      case PanelChart.BARRAS:
        chGrid.setVisible(false);
        break;
      case PanelChart.BARRAS_INVERTIDAS:
        break;
      case PanelChart.BARRAS_INVERTIDAS_LOG:
        break;
    }
  }

  void btnSalir_actionPerformed(ActionEvent e) {
    if (gpanel != null) {
      gpanel.dispose();
    }
  }

  void btnImprimir_actionPerformed(ActionEvent e) {
    if (gpanel != null) {
      PrintJob pjob = this.getToolkit().getPrintJob(gpanel.getApp().getFrame(),
          "Imprimiendo ...", null);
      if (pjob != null) {
        Graphics g = pjob.getGraphics();
        if (g != null) {
          gpanel.chart.printAll(g);
        }
        pjob.end();
      }
    }
  }

  void chLeyenda_itemStateChanged(ItemEvent e) {
    gpanel.chart.setLeyenda(chLeyenda.getState());
  }

  void chGrid_itemStateChanged(ItemEvent e) {
    gpanel.chart.setGrids(chGrid.getState());
  }

}

class ControlPanel2_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  ControlPanel2 adaptee;
  ActionEvent e;

  ControlPanel2_actionAdapter(ControlPanel2 adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.gpanel.getApp().setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
    if (e.getActionCommand().equals("salir")) {
      adaptee.btnSalir_actionPerformed(e);
    }
    else {
      adaptee.btnImprimir_actionPerformed(e);
    }
    adaptee.gpanel.getApp().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }
}

class ControlPanel2_chLeyenda_itemAdapter
    implements java.awt.event.ItemListener {
  ControlPanel2 adaptee;

  ControlPanel2_chLeyenda_itemAdapter(ControlPanel2 adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chLeyenda_itemStateChanged(e);
  }
}

class ControlPanel2_chGrid_itemAdapter
    implements java.awt.event.ItemListener {
  ControlPanel2 adaptee;

  ControlPanel2_chGrid_itemAdapter(ControlPanel2 adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chGrid_itemStateChanged(e);
  }
}
