package eapp2;

import java.awt.Dimension;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp2.CApp;
import capp2.CDialog;

public class GPanel
    extends CDialog {
  ControlPanel2 ctrlPanel = null;
  public PanelChart chart = null; // E
  XYLayout xyLayout = new XYLayout();

  public GPanel(CApp a, int tipo) {
    super(a);
    try {
      chart = new PanelChart(a, tipo);
      ctrlPanel = new ControlPanel2(this, tipo);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(800, 550));
    this.setTitle("Gr�fica");
    xyLayout.setHeight(550);
    xyLayout.setWidth(800);
    this.setLayout(xyLayout);
    this.add(ctrlPanel, new XYConstraints(0, 0, 800, 26));
    this.add(chart, new XYConstraints(0, 26, 800, 524));
  }
}
