package carga;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Vector;

import capp.CLista;
import sapp.DBServlet;

public class SrvCarga
    extends DBServlet {

  // modos de operaci�n
  final int servletALTA = 0;
  final int servletALTA_BAJA_CATALOGOS = 1;

  final String sBAJA_CATALOGOS =
      "delete from SIVE_CATALOGOS where CD_TABLA = ?";
  final String sALTA_CATALOGOS = "insert into SIVE_CATALOGOS (CD_TABLA,CD_VERSION,FC_ULTACT,CD_OPE,IT_OK,DS_FICHERO) values (?,?,?,?,?,?)";

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    Vector elVector = new Vector();
    Vector lineaVector = null;

    // objetos de datos
    CLista data = new CLista();

    String nomFichero = "";
    String codVersion = "";
    String codOperador = "";
    String OK = "";

    Object[][] registros;
    String tabla;
    String[] campos;
    int numCampos = 0;
    int numCamposClave = 0;
    int numRegistros = 0;
    int pos = 0;

    // contadores de operacion
    int iNumAltas = 0;
    int iNumModificaciones = 0;
    int iNumErrores = 0;

    // contador de registros
    int i;

    // vector con los registros para insertar
    Vector vUpdate = new Vector();

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    switch (opmode) { //A�adido ****************************

//______________________ Codigo altas en tabla (Jesus) ________________________

      case servletALTA: //****************

        // procesa los registros
        for (int x = 0; x < param.size(); x++) {
          //# System_Out.println("->Leemos el vector");
          lineaVector = (Vector) param.elementAt(x);

          //# System_Out.println("->Leemos el nombre de la tabla");
          tabla = (String) lineaVector.firstElement();
          //# System_Out.println(tabla);
          //# System_Out.println("->Leemos el n�mero de campos");
          numCampos = Integer.parseInt( (String) lineaVector.elementAt(1));
          //# System_Out.println((String)lineaVector.elementAt(1));
          //# System_Out.println("->Leemos los campos");
          campos = (String[]) lineaVector.elementAt(2);
          //# System_Out.println("->Leemos el n�mero de registros");
          numRegistros = Integer.parseInt( (String) lineaVector.elementAt(3));
          //# System_Out.println((String)lineaVector.elementAt(3));
          //# System_Out.println("->Leemos los registros");
          registros = (Object[][]) lineaVector.elementAt(4);
          //# System_Out.println("->Leemos el n�mero de campos clave");
          numCamposClave = Integer.parseInt( (String) lineaVector.elementAt(5));
          //# System_Out.println((String)lineaVector.elementAt(5));
          lineaVector = null;

          // modos de operaci�n
          switch (opmode) {

            case servletALTA:

              // proceso global
              try {

                // prepara los inserts
                query = "insert into " + tabla + " (";
                for (int j = 0; j < numCampos; j++) {
                  if (j == 0) {
                    query += campos[j];
                  }
                  else {
                    query += ", " + campos[j];
                  }
                }

                query += ") values (";

                for (int j = 0; j < numCampos; j++) {
                  if (j == 0) {
                    query += "?";
                  }
                  else {
                    query += ", ?";
                  }
                }

                query += ")";

                // procesa todos los registros para insertar
                vUpdate = new Vector(); // vector con los registros para actualizar
                for (i = 0; i < numRegistros; i++) {
                  try {
                    st = null;

                    st = con.prepareStatement(query);
                    //# System_Out.println("->"+query);
                    //# System_Out.println("2");
                    // a�adimos los campos
                    pos = 1;
                    for (int j = 0; j < numCampos; j++) {
                      //# System_Out.println("3");
                      // codigo
                      //# System_Out.println("4");
                      //# System_Out.println("->1 Campo: "+campos[j]+"="+registros[j][i]+" Pos:"+Integer.toString(pos));

                      if (registros[j][i] == null) {
                        try {
                          //# System_Out.println("varchar1");
                          st.setNull(pos, java.sql.Types.VARCHAR);
                          //# System_Out.println("varchar2");
                        }
                        catch (Exception talos) {
                          //# System_Out.println("integer1");
                          st.setInt(pos, 0);
                          //# System_Out.println("integer2");
                        }
                      }
                      else {
                        //# System_Out.println("6");
                        st.setObject(pos, registros[j][i]);
                      }
                      pos++;
                    }
                    //# System_Out.println("7 y pos");
                    //# System_Out.println(pos);
                    st.executeUpdate();
                    //# System_Out.println("8");
                    // aumenta el contador
                    iNumAltas++;

                  }
                  catch (Exception er) { // el registro existe
                    er.printStackTrace();
                    vUpdate.addElement(new Integer(i)); // guarda su codigo
                    //# System_Out.println("1"+tabla);
                    //# System_Out.println(pos);
                  }
                  st.close();
                  st = null;
                } // fin del proceso de inserci�n
                // procesa los registros para actualizar
                if (vUpdate.size() > 0) {
                  ////# System_Out.println("11");

                  ////# System_Out.println("12");
                  // prepara la query
                  query = "update " + tabla + " set ";
                  for (int j = numCamposClave; j < numCampos; j++) {
                    if (j == numCamposClave) {
                      query += campos[j] + "=?";
                    }
                    else {
                      query += ", " + campos[j] + "=?";
                    }
                  }

                  for (int j = 0; j < numCamposClave; j++) {
                    if (j == 0) {
                      query += " where " + campos[j] + "=?";
                    }
                    else {
                      query += " and " + campos[j] + "=?";
                    }
                  }

                  //# System_Out.println(query);
                  // recorre los elementos para actualizar
                  for (int f = 0; f < vUpdate.size(); f++) {
                    st = con.prepareStatement(query);

                    try {

                      i = ( (Integer) vUpdate.elementAt(f)).intValue();
                      //# System_Out.println(vUpdate.elementAt(f));
                      pos = 1;
                      // a�adimos los campos
                      for (int j = numCamposClave; j < numCampos; j++) {
                        // codigo
                        if (registros[j][i] == null) {
                          try {
                            st.setNull(pos, java.sql.Types.VARCHAR);
                          }
                          catch (Exception talos) {
                            st.setInt(pos, 0);
                          }
                        }
                        else {
                          st.setObject(pos, registros[j][i]);
                        }
                        pos++;
                      }
                      // a�adimos los campos
                      for (int j = 0; j < numCamposClave; j++) {
                        if (registros[j][i] == null) {
                          try {
                            st.setNull(pos, java.sql.Types.VARCHAR);
                          }
                          catch (Exception talos) {
                            st.setInt(pos, 0);
                          }
                        }
                        else {
                          st.setObject(pos, registros[j][i]);
                        }
                        pos++;
                      }
                      st.executeUpdate();
                      iNumModificaciones++;

                    }
                    catch (Exception exu) {
                      exu.printStackTrace();
                      iNumErrores++; // se ha producido un error
                      //# System_Out.println(tabla);
                    }
                    st.close();
                    st = null;
                  }
                } // fin del proceso de modificiones
                ////# System_Out.println("->32");
                con.commit();

              }
              catch (Exception erf) { // error general
                ////# System_Out.println("->33");
                erf.printStackTrace();
                con.rollback();
              }
              break;

          } //Cierra switch interno

        } //Cierra for

        // cierra la conexion y acaba el procedimiento doWork
        closeConnection(con);
        ////# System_Out.println("->34");
        data.addElement(new Integer(iNumAltas));
        ////# System_Out.println("->35");
        data.addElement(new Integer(iNumModificaciones));
        ////# System_Out.println("->36");
        data.addElement(new Integer(iNumErrores));
        ////# System_Out.println("->37");

        break; //Del caso servletALTA (switc  externo)

        //__________________________  Fin Codigo Jesus________________________________

      case servletALTA_BAJA_CATALOGOS:

        try {

          //Recoge datos de fichero y version
          tabla = ( (DataCarga) (param.elementAt(0))).sCodTab;

          codVersion = ( (DataCarga) (param.elementAt(0))).sCodVer;
          codOperador = ( (DataCarga) (param.elementAt(0))).sCodOpe;
          OK = ( (DataCarga) (param.elementAt(0))).sItOk;
          nomFichero = ( (DataCarga) (param.elementAt(0))).sNomFic;

          //___________ Borra elemento de tabla CATALOGOS_____________
          st = con.prepareStatement(sBAJA_CATALOGOS);

          st.setString(1, tabla);
          st.executeUpdate();
          st.close();
          st = null;

          //___________ Inserta elemento en tabla CATALOGOS _____________

          //Alta en tabla catalogos solo si hay nombre de fichero y version
          if ( (! (codVersion.equals(""))) && (! (nomFichero.equals("")))) {

            st = con.prepareStatement(sALTA_CATALOGOS);

            st.setString(1, tabla);
            st.setString(2, codVersion);
            st.setDate(3, new java.sql.Date(new java.util.Date().getTime()));
            st.setString(4, codOperador);
            st.setString(5, OK);
            st.setString(6, nomFichero);

            st.executeUpdate();
            st.close();
            st = null;

          }

          con.commit();

        }
        catch (Exception erf) { // error general
          erf.printStackTrace();
          con.rollback();
          throw erf;
        }

        // cierra la conexion y acaba el procedimiento doWork
        closeConnection(con);

        //___________________________________________________________
        break;

    } //Cierra switch  externo

    data.trimToSize();
    return data;
  } //doWork

}
