package carga;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Choice;
import java.awt.Cursor;
import java.awt.FileDialog;
import java.awt.Label;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CFileName;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import sapp.StubSrvBD;

public class DialCarga
    extends CPanel {
  // n�mero m�ximo de registros a transmitir
  static public int maxReg = 50;
  ResourceBundle res;

  // modos de operaci�n de la ventana
  final static int modoALTA = 0;
  final static int modoESPERA = 1;

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  // vector l�neas
  Vector vectorLin = new Vector();
  int longitudDatos = 0;

  // im�genes
  final String imgNAME[] = {
      "images/salvar.gif",
      "images/salvar.gif"};

  final String strSERVLET = "servlet/SrvCarga";

  // modos de operaci�n del servlet
  final int servletALTA = 0;
  final int servletALTA_BAJA_CATALOGOS = 1;

  boolean bHayCabecera = false; //Indica si hay cabecera correcta
  //Valdr� true si cabecera tiene fomato adecutado y en ella se indic fichero y versi�n
  String codVersion = "";
  String nomFichero = "";

  // par�metros
  protected int modoOperacion = modoALTA;
  protected StubSrvBD stubCliente = null;

  // lista de resultados del servlet
  CLista lResultado = null;

  // controles
  XYLayout xYLayout = new XYLayout();
  ButtonControl btnCargar = new ButtonControl();
  String[] arrayNombres = null;
//  String[][] arrayRegistros = null;

  Object[][] arrayRegistros = null;

  Label lblTabla = new Label();
  Choice chTabla = new Choice();
  CFileName fichero = null;
  BevelPanel bevelPanel1 = new BevelPanel();
  Label lblEstado = new Label();

  // constructor
  public DialCarga(CApp a) {

    try {
      this.app = a;
      res = ResourceBundle.getBundle("carga.Res" + app.getIdioma());
      fichero = new CFileName(this.app, FileDialog.LOAD);
      jbInit();
      chTabla.addItem(res.getString("msg2.Text"));
      chTabla.addItem(res.getString("msg3.Text"));
      chTabla.addItem(res.getString("msg4.Text"));
      chTabla.addItem(res.getString("msg5.Text"));
      chTabla.addItem(res.getString("msg6.Text"));
      chTabla.addItem(res.getString("msg7.Text"));
      chTabla.addItem(res.getString("msg8.Text"));
      chTabla.addItem(res.getString("msg9.Text"));
      chTabla.addItem(res.getString("msg10.Text"));
      chTabla.addItem(res.getString("msg11.Text"));
      chTabla.addItem(res.getString("msg12.Text"));
      chTabla.addItem(res.getString("msg13.Text"));
      chTabla.addItem(res.getString("msg14.Text"));
      chTabla.addItem(res.getString("msg15.Text"));
      chTabla.addItem(res.getString("msg16.Text"));
      chTabla.addItem(res.getString("msg17.Text"));
      chTabla.addItem(res.getString("msg20.Text"));
      chTabla.addItem(res.getString("msg21.Text"));

      chTabla.addItem(res.getString("msg22.Text"));
      chTabla.addItem(res.getString("msg23.Text"));
      chTabla.addItem(res.getString("msg24.Text"));
      chTabla.addItem(res.getString("msg25.Text"));
      chTabla.addItem(res.getString("msg26.Text"));
      chTabla.addItem(res.getString("msg27.Text"));

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    xYLayout.setHeight(216);
    xYLayout.setWidth(470);
    setSize(196, 470);
    setLayout(xYLayout);

    btnCargar.setLabel(res.getString("btnCargar.Label"));
    btnCargar.setActionCommand("generar");

    this.add(btnCargar, new XYConstraints(365, 140, 80, 26));
    this.add(lblTabla, new XYConstraints(14, 21, 52, -1));
    this.add(chTabla, new XYConstraints(75, 21, 214, -1));
    this.add(fichero, new XYConstraints(14, 62, -1, -1));
    this.add(bevelPanel1, new XYConstraints(4, 182, 464, 30));
    bevelPanel1.add(lblEstado, new XYConstraints(7, 1, 386, -1));

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    btnCargar.addActionListener(new DialCarga_btnCargar_actionAdapter(this));
    bevelPanel1.setBevelInner(BevelPanel.LOWERED);
    lblTabla.setText(res.getString("lblTabla.Text"));
    imgs.CargaImagenes();

    btnCargar.setImage(imgs.getImage(1));

    // establece el modo de operaci�n
    Inicializar();
    this.doLayout();

  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:

        // modo alta
        btnCargar.setEnabled(true);
        fichero.setEnabled(true);
        bevelPanel1.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:

        // modo espera
        btnCargar.setEnabled(false);
        fichero.setEnabled(false);
        bevelPanel1.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;

    boolean b = false;
    /*
         // comprueba que esten informados los campos obligatorios
         if ( (txtCod.getText().length() > 0) && (txtDes.getText().length() > 0) ) {
      b = true;
      // comprueba la longitud m�xima de los campos
      if (txtCod.getText().length() > 6) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtCod.selectAll();
      }
      if (txtDes.getText().length() > 40) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtDes.selectAll();
      }
      if (txtDesL.getText().length() > 40) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtDesL.selectAll();
      }
      if (txtDesI.getText().length() > 40) {
        b = false;
        msg = "Se ha superado el tama�o de los campos.";
        txtDesI.selectAll();
      }
         // advertencia de requerir datos
         } else {
      msg = "Faltan campos obligatorios.";
         }
         if (!b) {
      msgBox = new CMessage(app,CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
         }
     */
    return b;
  }

  void btnCargar_actionPerformed(ActionEvent e) {
    CMessage msgBox;
    String cadena;
    Vector vectorTrozos = new Vector();
    int partes = 0;
    int contador = 0;
    int inicio = 0;
    int posicion = 0;
    boolean fin = false;
    CLista laLista;
    int caracteres = 52;
    int numCampos = 0;
    int numRegistros = 0;
    int tablaNum = 1;
    String NombreTabla = "";
    Vector vectorDatos = new Vector();
    int numCamposClave = 0;
    int numLineasBloque = 0;
    // contadores de operacion
    int iNumAltas = 0;
    int iNumModificaciones = 0;
    int iNumErrores = 0;

    modoOperacion = modoESPERA;
    Inicializar();
    tablaNum = chTabla.getSelectedIndex();

    boolean bTodoOk = true;

    switch (tablaNum) {
      case 0: //tabla pa�ses
        caracteres = 43;
        break;

      case 1: //tabla CCAA
        caracteres = 52;
        break;

      case 2: //tabla prov
        caracteres = 24;
        break;

      case 3: //tabla municipio
        caracteres = 61;
        break;

      case 4: //tabla nivel1
        caracteres = 32;
        break;

      case 5: //tabla nivel2
        caracteres = 34;
        break;

      case 6: //tabla zbs
        caracteres = 36;
        break;

      case 7: //tabla enfcie
        caracteres = 46;
        break;

      case 8: //tabla enfedo
        caracteres = 43;
        break;

      case 9: //tabla tsive
      case 10: //tabla tlinea
      case 11: //tabla tpregunta
      case 12: //tabla tvigi
        caracteres = 31;
        break;

      case 13: //tabla motbaja
        caracteres = 42;
        break;

      case 14: //tabla nivasis
        caracteres = 16;
        break;

      case 15: //tabla sexo
        caracteres = 13;
        break;

      case 16: //tabla poblaci�n por zonas de salud  SIVE_POBLACION_NS
        caracteres = 22;
        break;

      case 17: //tabla poblaci�n por zonas geogr�ficas  SIVE_POBLACION_NG
        caracteres = 21;
        break;

        // Se a�ade el n�mero de caracteres que se deben leer
        // LARG
      case 18: //tabla grupos de brote
        caracteres = 26;
        break;
      case 19: //tabla tipos de notificador
        caracteres = 26;
        break;

      case 20: //tabla mecanismos de transmision
        caracteres = 44;
        break;

      case 21: //tabla tipos de colectivo
        caracteres = 42;
        break;

      case 22: //tabla situaci�n alerta
        caracteres = 41;
        break;

      case 23: //tipos de brotes
        caracteres = 43;
        break;
    }
    if (cargaVector(caracteres)) {
      // calculamos el n�mero de ciclos
      numRegistros = vectorLin.size();
      partes = numRegistros / maxReg;

      // s�lo una trama
      if ( (partes == 0) && (numRegistros > 0)) {
        partes++;
      }

      switch (tablaNum) {
        case 0: //tabla pa�ses
          NombreTabla = "SIVE_PAISES";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_PAIS";
          arrayNombres[1] = "DS_PAIS";
          break;

        case 1: //tabla CCAA
          NombreTabla = "SIVE_COM_AUT";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_CA";
          arrayNombres[1] = "DS_CA";
          break;

        case 2: //tabla PROVINCIAS
          NombreTabla = "SIVE_PROVINCIA";
          numCampos = 3;
          numCamposClave = 2;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_PROV";
          arrayNombres[1] = "CD_CA";
          arrayNombres[2] = "DS_PROV";
          break;

        case 3: //tabla MUNICIPIO
          NombreTabla = "SIVE_MUNICIPIO";
          numCampos = 6;
          numCamposClave = 2;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_PROV";
          arrayNombres[1] = "CD_MUN";
          arrayNombres[2] = "CD_NIVEL_1";
          arrayNombres[3] = "CD_NIVEL_2";
          arrayNombres[4] = "CD_ZBS";
          arrayNombres[5] = "DS_MUN";
          break;

        case 4: //tabla NIVEL1
          NombreTabla = "SIVE_NIVEL1_S";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_NIVEL_1";
          arrayNombres[1] = "DS_NIVEL_1";
          break;

        case 5: //tabla NIVEL2
          NombreTabla = "SIVE_NIVEL2_S";
          numCampos = 3;
          numCamposClave = 2;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_NIVEL_1";
          arrayNombres[1] = "CD_NIVEL_2";
          arrayNombres[2] = "DS_NIVEL_2";
          break;

        case 6: //tabla ZBS
          NombreTabla = "SIVE_ZONA_BASICA";
          numCampos = 4;
          numCamposClave = 3;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_NIVEL_1";
          arrayNombres[1] = "CD_NIVEL_2";
          arrayNombres[2] = "CD_ZBS";
          arrayNombres[3] = "DS_ZBS";
          break;

        case 7: //tabla ENFCIE
          NombreTabla = "SIVE_ENFER_CIE";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_ENFCIE";
          arrayNombres[1] = "DS_ENFERCIE";
          break;

        case 8: //tabla EDOCNE
          NombreTabla = "SIVE_EDO_CNE";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_ENFEREDO";
          arrayNombres[1] = "DS_ENFEREDO";
          break;

        case 9: //tabla TSIVE
          NombreTabla = "SIVE_TSIVE";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_TSIVE";
          arrayNombres[1] = "DS_TSIVE";
          break;

        case 10: //tabla  TLINEA
          NombreTabla = "SIVE_TLINEA";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_TLINEA";
          arrayNombres[1] = "DS_TLINEA";
          break;

        case 11: //tabla PREGUNTA
          NombreTabla = "SIVE_TPREGUNTA";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_TPREG";
          arrayNombres[1] = "DS_TPREG";
          break;

        case 12: //tabla TVIGILANCIA
          NombreTabla = "SIVE_T_VIGILANCIA";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_TVIGI";
          arrayNombres[1] = "DS_TVIGI";
          break;

        case 13: //tabla MOTIVI_BAJA
          NombreTabla = "SIVE_MOTIVO_BAJA";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_MOTBAJA";
          arrayNombres[1] = "DS_MOTBAJA";
          break;

        case 14: //tabla NIVEL ASISTENCIAL
          NombreTabla = "SIVE_NIV_ASIST";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_NIVASIS";
          arrayNombres[1] = "DS_NIVASIS";
          break;

        case 15: //tabla SEXO
          NombreTabla = "SIVE_SEXO";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_SEXO";
          arrayNombres[1] = "DS_SEXO";
          break;

        case 16: //tabla SIVE_POBLACION_NS
          NombreTabla = "SIVE_POBLACION_NS";
          numCampos = 7;
          numCamposClave = 6;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_NIVEL_1";
          arrayNombres[1] = "CD_NIVEL_2";
          arrayNombres[2] = "CD_ZBS";
          arrayNombres[3] = "CD_ANOEPI";
          arrayNombres[4] = "CD_SEXO";
          arrayNombres[5] = "NM_EDAD";
          arrayNombres[6] = "NM_POBLACION";
          break;

        case 17: //tabla SIVE_POBLACION_NG
          NombreTabla = "SIVE_POBLACION_NG";
          numCampos = 6;
          numCamposClave = 5;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_PROV";
          arrayNombres[1] = "CD_MUN";
          arrayNombres[2] = "CD_ANOEPI";
          arrayNombres[3] = "CD_SEXO";
          arrayNombres[4] = "NM_EDAD";
          arrayNombres[5] = "NM_POBLACION";
          break;

        case 18: //tabla grupos de brote
          NombreTabla = "SIVE_GRUPO_BROTE";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_GRUPO";
          arrayNombres[1] = "DS_GRUPO";
          break;

        case 19: //tabla tipos de notificador
          NombreTabla = "SIVE_T_NOTIFICADOR";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_TNOTIF";
          arrayNombres[1] = "DS_TNOTIF";
          break;

        case 20: //tabla mecanismos de transmision
          NombreTabla = "SIVE_MEC_TRANS";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_TRANSMIS";
          arrayNombres[1] = "DS_TRANSMIS";
          break;

        case 21: //tabla tipos de colectivo
          NombreTabla = "SIVE_TIPOCOL";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_TIPOCOL";
          arrayNombres[1] = "DS_TIPOCOL";
          break;

        case 22: //tabla situaci�n alerta
          NombreTabla = "SIVE_T_SIT_ALERTA";
          numCampos = 2;
          numCamposClave = 1;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_SITALERBRO";
          arrayNombres[1] = "DS_SITALERBRO";
          break;

        case 23: //tipos de brotes
          NombreTabla = "SIVE_TIPO_BROTE";
          numCampos = 3;
          numCamposClave = 2;
          arrayNombres = new String[numCampos];
          arrayNombres[0] = "CD_GRUPO";
          arrayNombres[1] = "CD_TBROTE";
          arrayNombres[2] = "DS_TBROTE";
          break;

      }

      for (int i = 0; i < partes; i++) {

        fin = false;
        contador = 0;
        inicio = i * maxReg;

        if (i == partes - 1) {
          numLineasBloque = numRegistros - (partes - 1) * maxReg;
        }
        else {
          numLineasBloque = maxReg;
        }
//        arrayRegistros = new String[numCampos][numLineasBloque];

        arrayRegistros = new Object[numCampos][numLineasBloque];

        for (contador = 0; contador < numLineasBloque; contador++) {
          /*        while (!fin){
                    if (contador == maxReg){
                      fin = true;
                    }else if(((i*maxReg)+contador) < numRegistros){ //leemos
           */
          cadena = (String) vectorLin.elementAt( (i * maxReg) + contador);

          switch (tablaNum) {
            case 0: //tabla PAIS
              arrayRegistros[0][contador] = cadena.substring(0, 3).trim();
              arrayRegistros[1][contador] = cadena.substring(3, 43).trim();
              break;

            case 1: //tabla CCAA
              arrayRegistros[0][contador] = cadena.substring(0, 2).trim();
              arrayRegistros[1][contador] = cadena.substring(2, 52).trim();
              break;

            case 2: //tabla PROVINCIA
              arrayRegistros[0][contador] = cadena.substring(0, 2).trim();
              arrayRegistros[1][contador] = cadena.substring(2, 4).trim();
              arrayRegistros[2][contador] = cadena.substring(4, 24).trim();
              break;

            case 3: //tabla MUNICIPIO
              arrayRegistros[0][contador] = cadena.substring(0, 2).trim();
              arrayRegistros[1][contador] = cadena.substring(2, 5).trim();
              arrayRegistros[2][contador] = cadena.substring(5, 7).trim();
              arrayRegistros[3][contador] = cadena.substring(7, 9).trim();
              arrayRegistros[4][contador] = cadena.substring(9, 11).trim();
              arrayRegistros[5][contador] = cadena.substring(11, 61).trim();
              break;

            case 4: //tabla NIVEL1
              arrayRegistros[0][contador] = cadena.substring(0, 2).trim();
              arrayRegistros[1][contador] = cadena.substring(2, 32).trim();
              break;

            case 5: //tabla NIVEL2
              arrayRegistros[0][contador] = cadena.substring(0, 2).trim();
              arrayRegistros[1][contador] = cadena.substring(2, 4).trim();
              arrayRegistros[2][contador] = cadena.substring(4, 34).trim();
              break;

            case 6: //tabla ZBS
              arrayRegistros[0][contador] = cadena.substring(0, 2).trim();
              arrayRegistros[1][contador] = cadena.substring(2, 4).trim();
              arrayRegistros[2][contador] = cadena.substring(4, 6).trim();
              arrayRegistros[3][contador] = cadena.substring(6, 36).trim();
              break;

            case 7: //tabla ENFCIE
              arrayRegistros[0][contador] = cadena.substring(0, 6).trim();
              arrayRegistros[1][contador] = cadena.substring(6, 46).trim();
              break;

            case 8: //tabla ENFEDO
              arrayRegistros[0][contador] = cadena.substring(0, 3).trim();
              arrayRegistros[1][contador] = cadena.substring(3, 43).trim();
              break;

            case 9: //tabla TSIVE
            case 10: //tabla TLINEA
            case 11: //tabla TPREGUNTA
            case 12: //tabla TVIGI
              arrayRegistros[0][contador] = cadena.substring(0, 1).trim();
              arrayRegistros[1][contador] = cadena.substring(1, 31).trim();
              break;

            case 13: //tabla MOTIVO BAJA
              arrayRegistros[0][contador] = cadena.substring(0, 2).trim();
              arrayRegistros[1][contador] = cadena.substring(2, 42).trim();
              break;

            case 14: //tabla NIVASIS
            case 15: //tabla SEXO
              arrayRegistros[0][contador] = cadena.substring(0, 1).trim();
              arrayRegistros[1][contador] = cadena.substring(1, 13).trim();
              break;
            case 16: //tabla SIVE_POBLACION_NS
              arrayRegistros[0][contador] = cadena.substring(0, 1).trim();
              arrayRegistros[1][contador] = cadena.substring(1, 26).trim();
//                arrayRegistros[2][contador] = cadena.substring(4,6).trim();
//                arrayRegistros[3][contador] = cadena.substring(6,10).trim();
//                arrayRegistros[4][contador] = cadena.substring(10,11).trim();
//                arrayRegistros[5][contador] = new Integer(  Integer.parseInt(cadena.substring(11,14).trim()) );
//                arrayRegistros[6][contador] = new Integer(  Integer.parseInt(cadena.substring(14,22).trim()) );
              break;
            case 17: //tabla SIVE_POBLACION_NG
              arrayRegistros[0][contador] = cadena.substring(0, 2).trim();
              arrayRegistros[1][contador] = cadena.substring(2, 5).trim();
              arrayRegistros[2][contador] = cadena.substring(5, 9).trim();
              arrayRegistros[3][contador] = cadena.substring(9, 10).trim();
              arrayRegistros[4][contador] = new Integer(Integer.parseInt( (
                  cadena.substring(10, 13).trim())));
              arrayRegistros[5][contador] = new Integer(Integer.parseInt( (
                  cadena.substring(13, 21).trim())));
              break;

            case 18: //tabla G_BROTES
              arrayRegistros[0][contador] = cadena.substring(0, 1).trim();
              arrayRegistros[1][contador] = cadena.substring(1, 16).trim();
              break;

            case 19: //tabla T_NOTIF
              arrayRegistros[0][contador] = cadena.substring(0, 1).trim();
              arrayRegistros[1][contador] = cadena.substring(1, 26).trim();
              break;
          }
          lblEstado.setText(res.getString("lblEstado.Text")
                            + Integer.toString( (i * maxReg) + contador + 1) +
                            res.getString("msg22.Aviso")
                            + Integer.toString(numRegistros));
          lblEstado.doLayout();

          /*            contador ++;
                    }else{
                      fin = true;
                    }*/
        } //while !fin
        // llamamos al servlet
        try {
          stubCliente = new StubSrvBD(new URL(this.app.getURL() + strSERVLET));
          laLista = new CLista();
          vectorDatos = new Vector();
          // a�adimos el c�digo de la tabla
          vectorDatos.addElement(NombreTabla);
          // a�adimos el n�mero de campos
          vectorDatos.addElement(Integer.toString(numCampos));
          // a�adimos los nombres de los campos
          vectorDatos.addElement(arrayNombres);
          // a�adimos el n�mero de registros
          vectorDatos.addElement(Integer.toString(numLineasBloque));
          // a�adimos los elementos a insertar
          vectorDatos.addElement(arrayRegistros);
          // a�adimos el n�mero de registros en el PK
          vectorDatos.addElement(Integer.toString(numCamposClave));

          // a�adimos el vector a la lista
          laLista.addElement(vectorDatos);

          // llamada al servlet
          lResultado = (CLista) stubCliente.doPost(servletALTA, laLista);

          iNumAltas += ( (Integer) lResultado.elementAt(0)).intValue();
          iNumModificaciones += ( (Integer) lResultado.elementAt(1)).intValue();
          iNumErrores += ( (Integer) lResultado.elementAt(2)).intValue();

        }
        catch (Exception er) {
          msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
          msgBox.show();
          bTodoOk = false;
          msgBox = null;
        }
      } //for principal

      //_______________ Se modifica la tabla catalogos del Servidor____________

      //Si hay algun bloque de datos
      if (numRegistros > 0) {

        try {

          laLista = new CLista();

          //Para indicar si ha habido errores o no
          String OK = "";
          if ( (bTodoOk = true) && (iNumErrores == 0)) {
            OK = "S";
          }
          else {
            OK = "N";

            // a�adimos la estructura de petici�n a la lista
          }
          laLista.addElement(new DataCarga(NombreTabla, codVersion,
                                           app.getLogin(), OK, nomFichero));

          // llamada al servlet
          lResultado = (CLista) stubCliente.doPost(servletALTA_BAJA_CATALOGOS,
              laLista);

        }
        catch (Exception er) {
          msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
          msgBox.show();
          msgBox = null;
        }

      }

      // mensaje con el resultado del proceso
      lblEstado.setText(res.getString("lblEstado.Text1")
                        + Integer.toString(numRegistros)
                        + res.getString("msg23.Aviso")
                        + Integer.toString(iNumAltas)
                        + res.getString("msg24.Aviso")
                        + Integer.toString(iNumModificaciones)
                        + res.getString("msg25.Aviso")
                        + Integer.toString(iNumErrores) + " )");

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgERROR,
                            res.getString("msg26.Aviso"));
      msgBox.show();
      msgBox = null;
    }
    modoOperacion = modoALTA;
    Inicializar();
  }

  public boolean cargaVector(int caracteres) {
    CMessage msgBox;

    File miFichero;
    FileReader fStream;
    String sLinea;
    LineNumberReader lector;
    boolean elBoolean = true;

    try {
      miFichero = new File(this.fichero.txtFile.getText());
      fStream = new FileReader(miFichero);
      lector = new LineNumberReader(fStream);

      vectorLin = new Vector();
      //Extraee cabecera y ajusta  var global bHayCabecera
      bHayCabecera = extraerCabeceraDeFichero(lector);

      //Si no hay cabecera se regenera el lector para comenzar la lectura de nuevo
      if (bHayCabecera == false) {
        nomFichero = "";
        codVersion = "";
        fStream = new FileReader(miFichero);
        lector = new LineNumberReader(fStream);
      }

      while ( (sLinea = lector.readLine()) != null) {
        vectorLin.addElement(sLinea);
        if (sLinea.length() != caracteres) {
          elBoolean = false;
        }
      }

      fStream.close();
    }
    catch (IOException ioEx) {
      ioEx.printStackTrace();
      elBoolean = false;
      msgBox = new CMessage(this.app, CMessage.msgERROR, ioEx.getMessage());
      msgBox.show();
      msgBox = null;
    }
    return elBoolean;
  }

  //Si hay cabecera con formato adecuado que indica nombre fich y versi�n devuelve true
  // y pone nombre fich en var global
  //Si fornato cab. no adecuado o no hay nombre fich o versi�n devuelve false
  boolean extraerCabeceraDeFichero(LineNumberReader lect) {
    CMessage msgBox = null;
    String sLinea1 = "";
    String sLinea2 = "";
    String sLinea3 = "";
    String sLinea4 = "";

    try {
      //Lee la linea con String [cabecera]
      if ( (sLinea1 = lect.readLine()) != null) {
        if (!sLinea1.equals("[cabecera]")) {
          return false;
        }
      }
      else {
        return false;
      }

      //Lee la linea que indica fichero
      if ( (sLinea2 = lect.readLine()) != null) {
        //Comprueba empiece con el String  "fichero="
        if (!sLinea2.startsWith("fichero=")) {
          return false;
        }
        else {
          //Extrae el nombre de fichero y comprueba
          nomFichero = sLinea2.substring(8);
          if (nomFichero.equals("")) {
            return false;
          }
        }
      }
      else {
        return false;
      }

      //Lee la linea que indica version
      if ( (sLinea3 = lect.readLine()) != null) {
        //Comprueba empiece con el String  "fichero="
        if (!sLinea3.startsWith("version=")) {
          return false;
        }
        else {
          //Extrae la version y comprueba
          codVersion = sLinea3.substring(8);
          if (codVersion.equals("")) {
            return false;
          }
        }
      }
      else {
        return false;
      }

      //Lee la linea con String [datos]
      if ( (sLinea4 = lect.readLine()) != null) {
        if (!sLinea4.equals("[datos]")) {
          return false;
        }
      }
      else {
        return false;
      }

      //Devuelve true si todo ha ido bien
      return true;

    }
    catch (IOException ioEx) {
      ioEx.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ioEx.getMessage());
      msgBox.show();
      msgBox = null;
      return false;
    }

  }

} //CLASE

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class DialCarga_btnCargar_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialCarga adaptee;
  ActionEvent e;

  DialCarga_btnCargar_actionAdapter(DialCarga adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnCargar_actionPerformed(e);
  }
}
