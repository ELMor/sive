
package carga;

import java.net.URL;
import java.util.ResourceBundle;

import capp.CApp;
import sapp.StubSrvBD;

public class Carga
    extends CApp {

  final int servletSELECCION_X_CODIGO = 5;
  ResourceBundle res;
  final int servletSELECCION_X_DESCRIPCION = 6;

  //PanEstadoCarga panelEstado;

  StubSrvBD stubCliente = null;
  final String strSERVLET = "servlet/SrvCarga";

  public void init() {
    super.init();

  }

  public void start() {
    try {
      CApp a = (CApp)this;
      res = ResourceBundle.getBundle("carga.Res" + a.getIdioma());
      setTitulo(res.getString("msg1.Text"));
      //panelEstado = new PanEstadoCarga();
      //panelEstado.setBorde(true);
      stubCliente = new StubSrvBD(new URL(a.getURL() + strSERVLET));
      //a.addPanelSuperior(new DialCarga(a));
      VerPanel("", new DialCarga(a));
      //addPanelInferior(panelEstado);

    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

}
