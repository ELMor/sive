package pannivelesedo;

import capp.CApp;

public class AppletNiveles
    extends CApp
    implements usaPanelNiveles {
  public void init() {
    super.init();
  }

  public void cambioNivelAntesInformado(int niv) {
    // System_out.println("cambioNivelAntesInformado " + niv);
  }

  public int ponerEnEspera() {
    // System_out.println("ponerEnEspera ");
    return 0;
  }

  public void ponerModo(int modo) {
    // System_out.println("ponerModo " + modo);
  }

  public void start() {
    setTitulo("panelNiveles");
    CApp a = (CApp)this;
    VerPanel("PanelNiveles",
             new panelNiveles(this, this, panelNiveles.MODO_INICIO,
                              panelNiveles.TIPO_NIVEL_3, panelNiveles.LINEAS_1, true),
             false);
    VerPanel("panelNiveles");
  }
}