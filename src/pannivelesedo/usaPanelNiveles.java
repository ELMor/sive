
package pannivelesedo;

public interface usaPanelNiveles {

  public void cambioNivelAntesInformado(int nivel);

  //public void cambioNivelAntesNoInformado(String cdmun);

  public int ponerEnEspera();

  public void ponerModo(int modo);

}
