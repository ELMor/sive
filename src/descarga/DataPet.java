
package descarga;

import java.io.Serializable;

public class DataPet
    implements Serializable {

  //N�meros de tablas
  public final static int tablaPAISES = 0;
  public final static int tablaCCAA = 1;
  public final static int tablaPROVINCIA = 2;
  public final static int tablaMUNICIPIO = 3;

  public final static int tablaNIVEL1_S = 4;
  public final static int tablaNIVEL2_S = 5;
  public final static int tablaZONA_BASICA = 6;

  public final static int tablaENFER_CIE = 7;
  public final static int tablaEDO_CNE = 8;
  public final static int tablaTSIVE = 9;
  public final static int tablaTLINEA = 10;
  public final static int tablaTPREGUNTA = 11;
  public final static int tablaT_VIGILANCIA = 12;
  public final static int tablaMOTIVO_BAJA = 13;
  public final static int tablaNIV_ASIST = 14;
  public final static int tablaSEXO = 15;

  //public final static int tablaTASISTENCIA = 16;
  //public final static int tablaESPECIALIDAD = 17;

  public final static int tablaPOBLACION_NS = 16;
  public final static int tablaPOBLACION_NG = 17;

  public final static int tablaGRUPOBROTE = 18;
  public final static int tablaTNOTIFICADOR = 19;
  public final static int tablaMTRANSMISION = 20;
  public final static int tablaTCOLECTIVO = 21;
  public final static int tablaSALERTA = 22;
  public final static int tablaTBROTE = 23;

  protected int iNumTabla;
  public DataPet(int numTabla) {
    iNumTabla = numTabla;
  }

  public int getNumTabla() {
    return iNumTabla;
  }

}