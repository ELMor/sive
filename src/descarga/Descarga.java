package descarga;

import java.util.ResourceBundle;

import capp.CApp;

public class Descarga
    extends CApp {

  ResourceBundle res;

  public Descarga() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("descarga.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    // abrimos el panel con el modo oportuno
    VerPanel("", new PanelDescarga(a));
  }

}
