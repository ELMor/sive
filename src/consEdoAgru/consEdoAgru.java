//Title:        Consulta 7.2.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina
//Company:      NorSistemas
//Description:  Consulta de casos EDO de un periodo agrupados segun criterio de usuario.

package consEdoAgru;

import java.util.ResourceBundle;

import capp.CApp;

public class consEdoAgru
    extends CApp {

  public PanelConsEdoAgru parametros;
  ResourceBundle res;

  public consEdoAgru() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("consEdoAgru.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    parametros = new PanelConsEdoAgru(a);
    VerPanel("", parametros);
  }
}
