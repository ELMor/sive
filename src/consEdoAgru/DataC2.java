package consEdoAgru;

import java.io.Serializable;

public class DataC2
    implements Serializable {

  public String DESDE; //anos[0] + "    " + sems[0];
  public String HASTA;
  //AIC
  //public int CASOS;
  public String CASOS;
  public float TASAS;

  public DataC2(String d, String h, int c, float t) {
    DESDE = d;
    HASTA = h;
    //AIC
    //CASOS = c;
    CASOS = String.valueOf(c);
    //CASOS = formatearEntero(c);
    TASAS = t;
  }

  public DataC2(String d, String h, int c) {
    DESDE = d;
    HASTA = h;
    //AIC
    //CASOS = c;
    CASOS = String.valueOf(c);
    //CASOS = formatearEntero(c);
  }

  //AIC
  //formatea el entero para darle formato a los  superiores a 1.000
  /*  public static String formatearEntero(int entero)
    {
      String resultado = "";
      String resto;
      while((entero / 1000) > 0)
      {
         int restoInt = entero % 1000;
         if(restoInt == 0)
         {
           resto = "000";
         }
         else
         {
           resto = String.valueOf(entero % 1000);
           for(int i = 0; i < 3 - resto.length(); i++)
           {
             resto = resto + "0";
           }
         }
         resultado = "." + resto + resultado;
         entero = (int)entero / 1000;
      }
      resultado = String.valueOf(entero) + resultado;
      return(resultado);
    } */

  //formatea el entero para darle formato a los  superiores a 1.000
  public static String formatearEntero(int entero) {
    String resultado = "";
    String resto;
    String resto2;
    while ( (entero / 1000) > 0) {
      int restoInt = entero % 1000;
      if (restoInt == 0) {
        resto = "000";
      }
      else {
        resto = String.valueOf(entero % 1000);
        resto2 = String.valueOf(entero % 1000);
        for (int i = 0; i < 3 - resto2.length(); i++) {
          //resto = resto + "0";
          resto = "0" + resto;
        }
      }

      resultado = "." + resto + resultado;
      entero = (int) entero / 1000;
    }

    resultado = String.valueOf(entero) + resultado;

    return (resultado.trim());

  }

  //AIC
  //m�todo para asignar el valor al campo CASOS;
  public void setCasos(int i) {
    CASOS = formatearEntero(i);
  }
}