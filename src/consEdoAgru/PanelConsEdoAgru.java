package consEdoAgru;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

// E Por DataEnferedo
import alarmas.DataEnferedo;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.LabelControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo2.DataCat2;
import cn.DataCN;
import eapp.DataGraf;
import eapp.GPanel;
import eapp.PanelChart;
import eqNot.DataEqNot;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
import utilidades.PanFechas;
import vCasIn.DataMun;
import zbs.DataZBS;
import zbs.DataZBS2;

public class PanelConsEdoAgru
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  //rutas imagenes
  final String imgLUPA = "images/Magnify.gif";
  final String imgLIMPIAR = "images/vaciar.gif";
  final String imgGENERAR = "images/grafico.gif";

  protected StubSrvBD stubCliente = new StubSrvBD();

  public ParamC2 paramC2;

  protected int modoOperacion = modoNORMAL;

  protected PanelInfEdoAgru informe = null;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETMun = "servlet/SrvMun";
  final String strSERVLETProv = "servlet/SrvCat2";
  final String strSERVLETEquipo = "servlet/SrvEqNotCen";
  final String strSERVLETCentro = "servlet/SrvCN";
  final String strSERVLETEnf = "servlet/SrvEnferedo";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  XYLayout xYLayout = new XYLayout();

  Label lblHasta = new Label();
  Label lblDesde = new Label();
  Label lblProvincia = new Label();
  CCampoCodigo txtCodPro = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarPro = new ButtonControl();
  TextField txtDesPro = new TextField();
  Label lblMunicipio = new Label();
  CCampoCodigo txtCodMun = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarMun = new ButtonControl();
  TextField txtDesMun = new TextField();
  Label lblArea = new Label();
  CCampoCodigo txtCodAre = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarAre = new ButtonControl();
  TextField txtDesAre = new TextField();
  Label lblDistrito = new Label();
  CCampoCodigo txtCodDis = new CCampoCodigo(); /*E*/
  TextField txtDesDis = new TextField();
  Label lblZonaBasica = new Label();
  CCampoCodigo txtCodZBS = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarDis = new ButtonControl();
  ButtonControl btnCtrlBuscarZBS = new ButtonControl();
  TextField txtDesZBS = new TextField();
  Label lblCenNot = new Label();
  CCampoCodigo txtCodCenNot = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarCenNot = new ButtonControl();
  TextField txtDesCenNot = new TextField();
  Label lblEquNot = new Label();
  CCampoCodigo txtCodEquNot = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarEquNot = new ButtonControl();
  TextField txtDesEquNot = new TextField();
  PanFechas fechasDesde;
  PanFechas fechasHasta;
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  focusAdapter txtFocusAdapter = new focusAdapter(this);
  txt_keyAdapter txtKeyAdapter = new txt_keyAdapter(this);
  actionListener btnActionListener = new actionListener(this);
  Label lblAgrupacion = new Label();
  TextField txtAgrup = new TextField();
  Label lblEnfermedad = new Label();
  CCampoCodigo txtEnfer = new CCampoCodigo(); /*E*/
  TextField txtDesEnfer = new TextField();
  ButtonControl btnCtrlBuscarEnfer = new ButtonControl();
  LabelControl labelControl1 = new LabelControl();
  LabelControl labelControl2 = new LabelControl();
  Checkbox checkCasos = new Checkbox();
  Checkbox checkTasas = new Checkbox();
  CheckboxGroup checkG = new CheckboxGroup();
  ButtonControl btnGrafico = new ButtonControl();

  public PanelConsEdoAgru(CApp a) {
    try {

      setApp(a);
      res = ResourceBundle.getBundle("consEdoAgru.Res" + a.getIdioma());
      informe = new PanelInfEdoAgru(a);
      fechasDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      fechasHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      jbInit();
      lblArea.setText(this.app.getNivel1() + ":");
      lblDistrito.setText(this.app.getNivel2() + ":");
      this.txtCodAre.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesAre.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtCodDis.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesDis.setText(this.app.getDS_NIVEL2_DEFECTO());
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(538);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));

    // gestores de eventos
    btnCtrlBuscarEnfer.addActionListener(btnActionListener);
    btnCtrlBuscarPro.addActionListener(btnActionListener);
    btnCtrlBuscarMun.addActionListener(btnActionListener);
    btnCtrlBuscarAre.addActionListener(btnActionListener);
    btnCtrlBuscarDis.addActionListener(btnActionListener);
    btnCtrlBuscarZBS.addActionListener(btnActionListener);
    btnCtrlBuscarCenNot.addActionListener(btnActionListener);
    btnCtrlBuscarEquNot.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);
    btnGrafico.addActionListener(btnActionListener);

    txtEnfer.addKeyListener(txtKeyAdapter);
    txtAgrup.addKeyListener(txtKeyAdapter);
    txtCodPro.addKeyListener(txtKeyAdapter);
    txtCodMun.addKeyListener(txtKeyAdapter);
    txtCodAre.addKeyListener(txtKeyAdapter);
    txtCodDis.addKeyListener(txtKeyAdapter);
    txtCodZBS.addKeyListener(txtKeyAdapter);
    txtCodCenNot.addKeyListener(txtKeyAdapter);
    txtCodEquNot.addKeyListener(txtKeyAdapter);

    txtEnfer.addFocusListener(txtFocusAdapter);
    txtCodPro.addFocusListener(txtFocusAdapter);
    txtCodMun.addFocusListener(txtFocusAdapter);
    txtCodAre.addFocusListener(txtFocusAdapter);
    txtCodDis.addFocusListener(txtFocusAdapter);
    txtCodZBS.addFocusListener(txtFocusAdapter);
    txtCodCenNot.addFocusListener(txtFocusAdapter);
    txtCodEquNot.addFocusListener(txtFocusAdapter);

    btnCtrlBuscarEnfer.setActionCommand("buscarEnfer");
    labelControl1.setText(res.getString("labelControl1.Text"));
    labelControl2.setText(res.getString("labelControl2.Text") + " " +
                          res.getString("msg2.Text"));

    checkCasos.setLabel(res.getString("checkCasos.Label"));
    checkTasas.setLabel(res.getString("checkTasas.Label"));
    checkTasas.setCheckboxGroup(checkG);
    btnGrafico.setLabel(res.getString("btnGrafico.Label"));
    checkCasos.setCheckboxGroup(checkG);
    checkCasos.setState(true);
    checkTasas.setState(false);

    //btnCtrlBuscarEnfer.setLabel("btnCtrlBuscarPro1");
    txtDesEnfer.setEditable(false);
    txtDesEnfer.setEnabled(false); /*E*/
    btnCtrlBuscarPro.setActionCommand("buscarPro");
    txtDesPro.setEditable(false);
    txtDesPro.setEnabled(false); /*E*/
    btnCtrlBuscarMun.setActionCommand("buscarMun");
    txtDesMun.setEditable(false);
    txtDesMun.setEnabled(false); /*E*/
    btnCtrlBuscarAre.setActionCommand("buscarAre");
    txtDesAre.setEditable(false);
    txtDesAre.setEnabled(false); /*E*/
    btnCtrlBuscarDis.setActionCommand("buscarDis");
    btnCtrlBuscarZBS.setActionCommand("buscarZBS");
    txtDesZBS.setEditable(false);
    txtDesZBS.setEnabled(false); /*E*/
    btnCtrlBuscarCenNot.setActionCommand("buscarCenNot");
    txtDesCenNot.setEditable(false);
    txtDesCenNot.setEnabled(false); /*E*/
    btnCtrlBuscarEquNot.setActionCommand("buscarEquNot");
    txtDesEquNot.setEditable(false);
    txtDesEquNot.setEnabled(false); /*E*/
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");
    btnGrafico.setActionCommand("grafico");

    txtAgrup.setName("txtAgrup");
    txtEnfer.setName("txtEnfer");
    txtCodPro.setName("txtCodPro");
    txtCodMun.setName("txtCodMun");
    txtCodAre.setName("txtCodAre");
    txtCodDis.setName("txtCodDis");
    txtDesDis.setEditable(false);
    txtDesDis.setEnabled(false); /*E*/
    txtCodZBS.setName("txtCodZBS");
    txtCodCenNot.setName("txtCodCenNot");
    txtCodEquNot.setName("txtCodEquNot");

    lblAgrupacion.setText(res.getString("lblAgrupacion.Text"));
    lblEnfermedad.setText(res.getString("lblEnfermedad.Text"));
    lblHasta.setText(res.getString("lblHasta.Text"));
    lblProvincia.setText(res.getString("lblProvincia.Text"));
    lblMunicipio.setText(res.getString("lblMunicipio.Text"));
    lblZonaBasica.setText(res.getString("lblZonaBasica.Text"));
    lblCenNot.setText(res.getString("lblCenNot.Text"));
    lblEquNot.setText(res.getString("lblEquNot.Text"));
    lblArea.setText(res.getString("lblArea.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));

    txtAgrup.setBackground(new Color(255, 255, 150));
    txtEnfer.setBackground(new Color(255, 255, 150));
    xYLayout.setWidth(596);

    this.setLayout(xYLayout);

    this.add(lblDesde, new XYConstraints(26, 5, 51, -1));
    this.add(fechasDesde, new XYConstraints(40, 5, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 35, 51, -1));
    this.add(fechasHasta, new XYConstraints(40, 35, -1, -1));
    this.add(lblAgrupacion, new XYConstraints(26, 65, 75, -1));
    this.add(txtAgrup, new XYConstraints(143, 65, 77, -1));
    this.add(checkCasos, new XYConstraints(254, 65, -1, -1));
    this.add(checkTasas, new XYConstraints(330, 65, -1, -1));

    this.add(lblEnfermedad, new XYConstraints(26, 95, 75, -1));
    this.add(txtEnfer, new XYConstraints(143, 95, 77, -1));
    this.add(btnCtrlBuscarEnfer, new XYConstraints(225, 95, -1, -1));
    this.add(txtDesEnfer, new XYConstraints(254, 95, 287, -1));

    this.add(lblProvincia, new XYConstraints(26, 125, 64, -1));
    this.add(txtCodPro, new XYConstraints(143, 125, 77, -1));
    this.add(btnCtrlBuscarPro, new XYConstraints(225, 125, -1, -1));
    this.add(txtDesPro, new XYConstraints(254, 125, 287, -1));
    this.add(lblMunicipio, new XYConstraints(26, 155, 66, -1));
    this.add(txtCodMun, new XYConstraints(143, 155, 77, -1));
    this.add(btnCtrlBuscarMun, new XYConstraints(225, 155, -1, -1));
    this.add(txtDesMun, new XYConstraints(254, 155, 287, -1));
    this.add(lblArea, new XYConstraints(26, 185, -1, -1));
    this.add(txtCodAre, new XYConstraints(143, 185, 77, -1));
    this.add(btnCtrlBuscarAre, new XYConstraints(225, 185, -1, -1));
    this.add(txtDesAre, new XYConstraints(254, 185, 287, -1));
    this.add(lblDistrito, new XYConstraints(26, 215, 52, -1));
    this.add(txtCodDis, new XYConstraints(143, 215, 77, -1));
    this.add(btnCtrlBuscarDis, new XYConstraints(225, 215, -1, -1));
    this.add(txtDesDis, new XYConstraints(254, 215, 287, -1));
    this.add(lblZonaBasica, new XYConstraints(26, 245, 77, -1));
    this.add(txtCodZBS, new XYConstraints(143, 245, 77, -1));
    this.add(btnCtrlBuscarZBS, new XYConstraints(225, 245, -1, -1));
    this.add(txtDesZBS, new XYConstraints(254, 245, 287, -1));
    this.add(lblCenNot, new XYConstraints(26, 275, 101, -1));
    this.add(txtCodCenNot, new XYConstraints(143, 275, 77, -1));
    this.add(btnCtrlBuscarCenNot, new XYConstraints(225, 275, -1, -1));
    this.add(txtDesCenNot, new XYConstraints(254, 275, 287, -1));
    this.add(lblEquNot, new XYConstraints(26, 305, 110, -1));
    this.add(txtCodEquNot, new XYConstraints(143, 305, 77, -1));
    this.add(btnCtrlBuscarEquNot, new XYConstraints(225, 305, -1, -1));
    this.add(txtDesEquNot, new XYConstraints(254, 305, 287, -1));
    this.add(btnLimpiar, new XYConstraints(324, 335, -1, -1));
    this.add(btnInforme, new XYConstraints(409, 335, -1, -1));
    this.add(labelControl1, new XYConstraints(24, 335, -1, -1));
    this.add(labelControl2, new XYConstraints(24, 359, 508, -1));
    this.add(btnGrafico, new XYConstraints(494, 335, -1, -1));

    btnCtrlBuscarEnfer.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarPro.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarMun.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarAre.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarDis.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarZBS.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarCenNot.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarEquNot.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnLimpiar.setImageURL(new URL(app.getCodeBase(), imgLIMPIAR));
    btnInforme.setImageURL(new URL(app.getCodeBase(), imgGENERAR));
    btnGrafico.setImageURL(new URL(app.getCodeBase(), imgGENERAR));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (fechasDesde.txtAno.getText().length() == 0) ||
        (fechasHasta.txtAno.getText().length() == 0) ||
        (fechasDesde.txtCodSem.getText().length() == 0) ||
        (fechasHasta.txtCodSem.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    // Comprobacion de la agrupacion
    if (txtAgrup.getText().length() == 0) {
      bDatosCompletos = false;
    }
    else {
      try {
        Integer i = new Integer(txtAgrup.getText().trim());
      }
      catch (NumberFormatException e) {
        bDatosCompletos = false;
      }
    }

    // Comprobacion de la descripcion de la enfermedad
    if (txtDesEnfer.getText().length() == 0) {
      bDatosCompletos = false;

    }
    if (!checkTasas.getState() && !checkCasos.getState()) {
      bDatosCompletos = false;

    }
    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtAgrup.setEnabled(true);
        txtEnfer.setEnabled(true);
        txtCodPro.setEnabled(true);
        txtCodAre.setEnabled(true);
        txtCodCenNot.setEnabled(true);

        btnCtrlBuscarEnfer.setEnabled(true);

        btnCtrlBuscarPro.setEnabled(true);
        // control municipio
        if (!txtDesPro.getText().equals("")) {
          btnCtrlBuscarMun.setEnabled(true);
          txtCodMun.setEnabled(true);
        }
        else {
          btnCtrlBuscarMun.setEnabled(false);
          txtCodMun.setEnabled(false);
        }

        btnCtrlBuscarAre.setEnabled(true);
        // control area
        if (!txtDesAre.getText().equals("")) {
          btnCtrlBuscarDis.setEnabled(true);
          txtCodDis.setEnabled(true);
        }
        else {
          btnCtrlBuscarDis.setEnabled(false);
          txtCodDis.setEnabled(false);
        }

        // control distrito
        if (!txtDesDis.getText().equals("")) {
          btnCtrlBuscarZBS.setEnabled(true);
          txtCodZBS.setEnabled(true);
        }
        else {
          btnCtrlBuscarZBS.setEnabled(false);
          txtCodZBS.setEnabled(false);
        }

        // control eq
        if (!txtDesCenNot.getText().equals("")) {
          btnCtrlBuscarEquNot.setEnabled(true);
          txtCodEquNot.setEnabled(true);
        }
        else {
          txtCodEquNot.setEnabled(false);
          btnCtrlBuscarEquNot.setEnabled(false);
        }

        btnCtrlBuscarCenNot.setEnabled(true);
        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);
        btnGrafico.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtAgrup.setEnabled(false);
        txtEnfer.setEnabled(false);
        txtCodPro.setEnabled(false);
        txtCodMun.setEnabled(false);
        txtCodAre.setEnabled(false);
        txtCodDis.setEnabled(false);
        txtCodZBS.setEnabled(false);
        txtCodCenNot.setEnabled(false);
        txtCodEquNot.setEnabled(false);
        btnCtrlBuscarEnfer.setEnabled(false);
        btnCtrlBuscarPro.setEnabled(false);
        btnCtrlBuscarMun.setEnabled(false);
        btnCtrlBuscarAre.setEnabled(false);
        btnCtrlBuscarDis.setEnabled(false);
        btnCtrlBuscarZBS.setEnabled(false);
        btnCtrlBuscarCenNot.setEnabled(false);
        btnCtrlBuscarEquNot.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        btnGrafico.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //busca la enfermedad
  void btnCtrlBuscarEnfer_actionPerformed(ActionEvent evt) {
    DataEnferedo datosPantalla = null;
    CMessage msgBox = null;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      CListaEnfEDO lista = new CListaEnfEDO(app,
                                            res.getString("msg3.Text"),
                                            stubCliente,
                                            strSERVLETEnf,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      datosPantalla = (DataEnferedo) lista.getComponente();

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (datosPantalla != null) {
      txtEnfer.removeKeyListener(txtKeyAdapter);
      txtEnfer.setText(datosPantalla.getCod());
      txtDesEnfer.setText(datosPantalla.getDes());
      txtEnfer.addKeyListener(txtKeyAdapter);
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca la provincia
  void btnCtrlbuscarPro_actionPerformed(ActionEvent evt) {
    DataCat2 data = null;
    DataCat2 datProv = null;
    CMessage msgBox = null;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      catalogo2.CListaCat2 lista = new catalogo2.CListaCat2(app,
          res.getString("msg4.Text"),
          stubCliente,
          strSERVLETProv,
          servletOBTENER_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA,
          servletOBTENER_X_DESCRIPCION + catalogo2.Catalogo2.catPROVINCIA,
          servletSELECCION_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA,
          servletSELECCION_X_DESCRIPCION + catalogo2.Catalogo2.catPROVINCIA);

      lista.show();
      datProv = (DataCat2) lista.getComponente();
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (datProv != null) {
      txtCodPro.removeKeyListener(txtKeyAdapter);
      txtCodPro.setText(datProv.getCod());
      txtDesPro.setText(datProv.getDes());
      txtCodMun.setText("");
      txtDesMun.setText("");
      txtCodPro.addKeyListener(txtKeyAdapter);
      checkCasos.setState(true);
      checkTasas.setState(false);
      checkCasos.setEnabled(false);
      checkTasas.setEnabled(false);
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca el municipio
  void btnCtrlbuscarMun_actionPerformed(ActionEvent evt) {
    DataMun data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaMun lista = new CListaMun(this,
                                      res.getString("msg5.Text"),
                                      stubCliente,
                                      strSERVLETMun,
                                      servletOBTENER_X_CODIGO,
                                      servletOBTENER_X_DESCRIPCION,
                                      servletSELECCION_X_CODIGO,
                                      servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataMun) lista.getComponente();

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodMun.removeKeyListener(txtKeyAdapter);
      txtCodMun.setText(data.getMunicipio());
      txtDesMun.setText(data.getDescMun());

      txtCodMun.addKeyListener(txtKeyAdapter);
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //Busca los niveles1 (area de salud)
  void btnCtrlbuscarAre_actionPerformed(ActionEvent evt) {
    DataNivel1 data = null;
    CMessage mensaje = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaNivel1 lista = new CListaNivel1(app,
                                            res.getString("msg6.Text") +
                                            this.app.getNivel1(),
                                            stubCliente,
                                            strSERVLETNivel1,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }

    if (data != null) {
      txtCodAre.removeKeyListener(txtKeyAdapter);
      txtCodAre.setText(data.getCod());
      txtDesAre.setText(data.getDes());
      txtCodDis.setText("");
      txtDesDis.setText("");
      txtCodZBS.setText("");
      txtDesZBS.setText("");
      txtCodAre.addKeyListener(txtKeyAdapter);

      checkCasos.setEnabled(! (txtDesCenNot.getText().length() > 0)
                            );
      checkTasas.setEnabled(! (txtDesCenNot.getText().length() > 0)
                            );

    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca el distrito (Niv2)
  void btnCtrlbuscarDis_actionPerformed(ActionEvent evt) {

    DataZBS2 data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg6.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLETNivel2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodDis.removeKeyListener(txtKeyAdapter);
      txtCodDis.setText(data.getNiv2());
      txtDesDis.setText(data.getDes());
      txtCodZBS.setText("");
      txtDesZBS.setText("");

      txtCodDis.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnCtrlbuscarZBS_actionPerformed(ActionEvent evt) {
    DataZBS data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZona lista = new CListaZona(this,
                                        res.getString("msg7.Text"),
                                        stubCliente,
                                        strSERVLETZona,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS) lista.getComponente();

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    if (data != null) {
      txtCodZBS.removeKeyListener(txtKeyAdapter);
      txtCodZBS.setText(data.getCod());
      txtDesZBS.setText(data.getDes());
      txtCodZBS.addKeyListener(txtKeyAdapter);
      checkCasos.setState(true);
      checkTasas.setState(false);
      checkCasos.setEnabled(false);
      checkTasas.setEnabled(false);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnCtrlbuscarCenNot_actionPerformed(ActionEvent evt) {
    DataCN data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaCN lista = new CListaCN(this.app,
                                    res.getString("msg8.Text"),
                                    stubCliente,
                                    strSERVLETCentro,
                                    servletOBTENER_X_CODIGO,
                                    servletOBTENER_X_DESCRIPCION,
                                    servletSELECCION_X_CODIGO,
                                    servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataCN) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodCenNot.removeKeyListener(txtKeyAdapter);
      txtCodCenNot.setText(data.getCodCentro());
      txtDesCenNot.setText(data.getCentroDesc());
      txtCodEquNot.setText("");
      txtDesEquNot.setText("");
      txtCodCenNot.addKeyListener(txtKeyAdapter);
      checkCasos.setState(true);
      checkTasas.setState(false);
      checkCasos.setEnabled(false);
      checkTasas.setEnabled(false);
    }
    modoOperacion = modoNORMAL;
    Inicializar();

  }

  void btnCtrlbuscarEquNot_actionPerformed(ActionEvent evt) {

    DataEqNot data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaEqNot lista = new CListaEqNot(this,
                                          res.getString("msg9.Text"),
                                          stubCliente,
                                          strSERVLETEquipo,
                                          servletOBTENER_X_CODIGO,
                                          servletOBTENER_X_DESCRIPCION,
                                          servletSELECCION_X_CODIGO,
                                          servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataEqNot) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodEquNot.removeKeyListener(txtKeyAdapter);
      txtCodEquNot.setText(data.getEquipo());
      txtDesEquNot.setText(data.getDesEquipo());
      txtCodEquNot.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    txtAgrup.setText("");
    txtEnfer.setText("");
    txtDesEnfer.setText("");
    txtCodAre.setText("");
    txtDesAre.setText("");
    txtCodDis.setText("");
    txtDesDis.setText("");
    txtCodZBS.setText("");
    txtDesZBS.setText("");
    txtCodPro.setText("");
    txtDesPro.setText("");
    txtCodMun.setText("");
    txtDesMun.setText("");
    txtCodEquNot.setText("");
    txtDesEquNot.setText("");
    txtCodCenNot.setText("");
    txtDesCenNot.setText("");
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    paramC2 = new ParamC2();

    boolean eq = false;
    boolean cen = false;
    boolean tasas = false;

    if (isDataValid()) {
      if (!txtAgrup.getText().equals("")) {
        Integer i = new Integer(txtAgrup.getText());
        paramC2.agrupacion = i.intValue();
      }
      if (!txtDesEnfer.getText().equals("")) {
        paramC2.enfermedad = txtEnfer.getText();
        paramC2.enfermedadDesc = txtDesEnfer.getText();
      }
      if (!txtDesPro.getText().equals("")) {
        paramC2.provincia = txtCodPro.getText();
        paramC2.provinciaDesc = txtDesPro.getText();
      }
      if (!txtDesMun.getText().equals("")) {
        paramC2.municipio = txtCodMun.getText();
        paramC2.municipioDesc = txtDesMun.getText();
      }
      if (!txtDesAre.getText().equals("")) {
        paramC2.area = txtCodAre.getText();
        paramC2.areaDesc = txtDesAre.getText();
        tasas = true;
      }
      if (!txtDesDis.getText().equals("")) {
        paramC2.distrito = txtCodDis.getText();
        paramC2.distritoDesc = txtDesDis.getText();
      }
      if (!txtDesZBS.getText().equals("")) {
        paramC2.zbs = txtCodZBS.getText();
        paramC2.zbsDesc = txtDesZBS.getText();
      }
      if (!txtDesCenNot.getText().equals("")) {
        paramC2.cenNotif = txtCodCenNot.getText();
        paramC2.cenNotifDesc = txtDesCenNot.getText();
        cen = true;
      }
      if (!txtDesEquNot.getText().equals("")) {
        //eq = false;
        eq = true;
        paramC2.eqNotif = txtCodEquNot.getText();
        paramC2.eqNotifDesc = txtDesEquNot.getText();
      }

      if (txtDesPro.getText().equals("") &&
          txtDesAre.getText().equals("")) {
        tasas = true;

      }
      paramC2.anoDesde = fechasDesde.txtAno.getText();
      paramC2.anoHasta = fechasHasta.txtAno.getText();
      if (fechasDesde.txtCodSem.getText().length() == 1) {
        paramC2.semDesde = "0" + fechasDesde.txtCodSem.getText();
      }
      else {
        paramC2.semDesde = fechasDesde.txtCodSem.getText();
      }
      if (fechasHasta.txtCodSem.getText().length() == 1) {
        paramC2.semHasta = "0" + fechasHasta.txtCodSem.getText();
      }
      else {
        paramC2.semHasta = fechasHasta.txtCodSem.getText();

        // Compruebo si se calculan las tasas:
        // si: todo vacio, area, area y distrito
        // no: provincia, provincia y municipio, centro, centro y equipo

      }
      if ( (txtDesPro.getText().equals("") && txtDesCenNot.getText().equals("") &&
            txtDesZBS.getText().equals(""))
          || (!txtDesAre.getText().equals(""))
          || (!txtDesAre.getText().equals("") && !txtDesDis.getText().equals(""))) {
        paramC2.tasas = checkTasas.getState();
        //checkG.getSelectedCheckbox().equals(checkTasas);
      }
      else {
        paramC2.tasas = false;

        // Modo de operacion para el servlet
      }
      if ( (eq) && (!cen)) {
        paramC2.criterio = ParamC2.erwCASOS_EDO_AGRU_EQ;
      }
      if ( (eq) && (cen)) {
        paramC2.criterio = ParamC2.erwCASOS_EDO_AGRU_EQ_Y_CEN;
      }
      if ( (!eq) && (cen)) {
        paramC2.criterio = ParamC2.erwCASOS_EDO_AGRU_CEN;
      }
      if ( (!eq) && (!cen)) {
        paramC2.criterio = ParamC2.erwCASOS_EDO_AGRU_NO_EQ_NI_CEN;

        // habilita el panel
      }
      modoOperacion = modoESPERA;
      Inicializar();

      //if (informe == null)
      //  informe = new PanelInfEdoAgru(getApp());

      informe.setEnabled(true);
      informe.paramC2 = paramC2;
      boolean hayInforme = informe.GenerarInforme();
      if (hayInforme) {
        ( (CDialog) informe).show();
      }

      //informe.mostrar();
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg10.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  void btnGrafico_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    paramC2 = new ParamC2();

    // criterio
    String c1 = "";
    String c2 = "";
    String c3 = "";

    // indicadores de control
    boolean eq = false;
    boolean cen = false;
    boolean mostrar = true;

    if (isDataValid()) {
      paramC2.anoDesde = fechasDesde.txtAno.getText();
      paramC2.anoHasta = fechasHasta.txtAno.getText();
      if (fechasDesde.txtCodSem.getText().length() == 1) {
        paramC2.semDesde = "0" + fechasDesde.txtCodSem.getText();
      }
      else {
        paramC2.semDesde = fechasDesde.txtCodSem.getText();
      }
      if (fechasHasta.txtCodSem.getText().length() == 1) {
        paramC2.semHasta = "0" + fechasHasta.txtCodSem.getText();
      }
      else {
        paramC2.semHasta = fechasHasta.txtCodSem.getText();
      }
      if (!txtDesEnfer.getText().equals("")) {
        paramC2.enfermedad = txtEnfer.getText();
        paramC2.enfermedadDesc = txtDesEnfer.getText();
      }
      if (!txtAgrup.getText().equals("")) {
        Integer i = new Integer(txtAgrup.getText());
        paramC2.agrupacion = i.intValue();
      }

      // Compruebo si se calculan las tasas:
      // si: todo vacio, area, area y distrito
      // no: provincia, provincia y municipio, centro, centro y equipo
      if ( (txtDesPro.getText().equals("") && txtDesCenNot.getText().equals("") &&
            txtDesZBS.getText().equals(""))
          || (!txtDesAre.getText().equals(""))
          || (!txtDesAre.getText().equals("") && !txtDesDis.getText().equals(""))) {
        paramC2.tasas = checkTasas.getState();
      }
      else {
        paramC2.tasas = false;

        // etiqueta c1: periodo temporal, agrupacion y empresa
      }
      c1 = res.getString("msg11.Text") + paramC2.anoDesde + " " +
          res.getString("msg12.Text") + paramC2.semDesde + ", "
          + res.getString("msg13.Text") + paramC2.anoHasta + " " +
          res.getString("msg14.Text") + paramC2.semHasta + " "
          + res.getString("msg15.Text") + Integer.toString(paramC2.agrupacion) +
          " ";

      if (paramC2.agrupacion == 1) {
        c1 = c1 + res.getString("msg16.Text");
      }
      else {
        c1 = c1 + res.getString("msg17.Text");

      }
      c1 = c1 + res.getString("msg18.Text") + paramC2.enfermedad + " " +
          paramC2.enfermedadDesc;

      // etiqueta c2: provincia, municipio
      if (!txtDesPro.getText().equals("")) {
        paramC2.provincia = txtCodPro.getText();
        paramC2.provinciaDesc = txtDesPro.getText();
        c2 = res.getString("msg19.Text") + paramC2.provincia + " " +
            paramC2.provinciaDesc;
      }
      if (!txtDesMun.getText().equals("")) {
        paramC2.municipio = txtCodMun.getText();
        paramC2.municipioDesc = txtDesMun.getText();
        c2 = c2 + res.getString("msg20.Text") + paramC2.municipio + " " +
            paramC2.municipioDesc;
      }

      // etiqueta c3: area, distrito, zbs
      if (!txtDesAre.getText().equals("")) {
        paramC2.area = txtCodAre.getText();
        paramC2.areaDesc = txtDesAre.getText();
        c3 = res.getString("msg21.Text") + paramC2.area + " " +
            paramC2.areaDesc;
      }
      if (!txtDesDis.getText().equals("")) {
        paramC2.distrito = txtCodDis.getText();
        paramC2.distritoDesc = txtDesDis.getText();
        c3 = c3 + res.getString("msg22.Text") + paramC2.distrito + " " +
            paramC2.distritoDesc;
      }
      if (!txtDesZBS.getText().equals("")) {
        paramC2.zbs = txtCodZBS.getText();
        paramC2.zbsDesc = txtDesZBS.getText();
        c3 = c3 + res.getString("msg23.Text") + paramC2.zbs + " " +
            paramC2.zbsDesc;
      }

      // c2 centro
      if (!txtDesCenNot.getText().equals("")) {
        paramC2.cenNotif = txtCodCenNot.getText();
        paramC2.cenNotifDesc = txtDesCenNot.getText();
        c2 = res.getString("msg24.Text") + paramC2.cenNotif + " " +
            paramC2.cenNotifDesc;
        c3 = "";
        cen = true;
      }
      // c3 equipo
      if (!txtDesEquNot.getText().equals("")) {
        eq = true;
        paramC2.eqNotif = txtCodEquNot.getText();
        paramC2.eqNotifDesc = txtDesEquNot.getText();
        c3 = res.getString("msg25.Text") + paramC2.eqNotif + " " +
            paramC2.eqNotifDesc;
      }

      // Modo de operacion para el servlet
      if ( (eq) && (!cen)) {
        paramC2.criterio = ParamC2.erwCASOS_EDO_AGRU_EQ;
      }
      if ( (eq) && (cen)) {
        paramC2.criterio = ParamC2.erwCASOS_EDO_AGRU_EQ_Y_CEN;
      }
      if ( (!eq) && (cen)) {
        paramC2.criterio = ParamC2.erwCASOS_EDO_AGRU_CEN;
      }
      if ( (!eq) && (!cen)) {
        paramC2.criterio = ParamC2.erwCASOS_EDO_AGRU_NO_EQ_NI_CEN;

        // habilita el panel
      }
      modoOperacion = modoESPERA;
      Inicializar();

      CLista param = new CLista();

      try {

        // obtiene los datos del servidor
        paramC2.numPagina = 0;
        paramC2.bInformeCompleto = true;
        param.addElement(paramC2);
        param.trimToSize();

        param.setLogin(app.getLogin());
        param.setLortad(app.getParameter("LORTAD"));
        informe.stub.setUrl(new URL(app.getURL() + informe.strSERVLET));
        CLista lista = (CLista) informe.stub.doPost(paramC2.criterio, param);

        /*
                 SrvConsEdoAgru srv = new SrvConsEdoAgru();
                 srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                             "sive_desa",
                             "sive_desa");
                 CLista lista = srv.doDebug(paramC2.criterio, param);
         */

        Vector vAgrupaciones = (Vector) lista.elementAt(0);
        Vector vTotalLineas = (Vector) lista.elementAt(2);
        boolean tasas = ( (Boolean) lista.elementAt(3)).booleanValue();

        // control de registros
        if (vTotalLineas.size() == 0) {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg26.Text"));
          msgBox.show();
          msgBox = null;
          mostrar = false;
        }
        else {

          if (paramC2.tasas) {
            if (!tasas) {
              //No se han calculado las tasas y no se mostrara el informe
              msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                                    res.getString("msg27.Text"));
              msgBox.show();
              msgBox = null;
              mostrar = false;
            }
          }
        }

        // muestra la gr�fica
        if (mostrar) {
          // datos de la gr�fica
          double rawData[][] = null;
          String seriesX[] = null;
          String desde, hasta;

          rawData = new double[2][vAgrupaciones.size() + 1];
          //seriesX = new String[ vAgrupaciones.size()];
          //AIC
          seriesX = new String[vAgrupaciones.size() + 1];
          seriesX[0] = " ";
          rawData[0][0] = 0;
          rawData[1][0] = 0.0;

          for (int i = 0; i < vAgrupaciones.size(); i++) {
            //rawData[0][i] = i;
            //AIC
            rawData[0][i + 1] = i + 1;
            if (paramC2.tasas) {
              rawData[1][i + 1] = ( (DataC2) vAgrupaciones.elementAt(i)).TASAS;
            }
            else {
              //AIC
              //rawData[1][i + 1] = ((DataC2)vAgrupaciones.elementAt(i)).CASOS;
              rawData[1][i +
                  1] = Integer.parseInt( ( (DataC2) vAgrupaciones.elementAt(i)).
                                        CASOS);
            }
            if (paramC2.agrupacion == 1) {
              desde = ( (DataC2) vAgrupaciones.elementAt(i)).DESDE;
              //seriesX[i] = desde.substring(desde.length()-2,desde.length());
              //AIC
              seriesX[i + 1] = desde.substring(desde.length() - 2, desde.length());
            }
            else {
              desde = ( (DataC2) vAgrupaciones.elementAt(i)).DESDE;
              hasta = ( (DataC2) vAgrupaciones.elementAt(i)).HASTA;
              //seriesX[i] = desde.substring(desde.length()-2,desde.length()) +  "-" +
              //             hasta.substring(hasta.length()-2,hasta.length());
              //AIC
              seriesX[i + 1] = desde.substring(desde.length() - 2, desde.length()) +
                  " " +
                  hasta.substring(hasta.length() - 2, hasta.length());
            }
          }

          // leyenda
          String leyenda[] = new String[1];
          if (paramC2.tasas) {
            leyenda[0] = res.getString("msg28.Text") + "   ";
          }
          else {
            leyenda[0] = res.getString("checkCasos.Label") + " ";
          }

          DataGraf data = new DataGraf(res.getString("msg29.Text"), rawData,
                                       leyenda, seriesX);

          GPanel panel = new GPanel(this.getApp(), PanelChart.BARRAS);
          panel.chart.setDatosLabels(data, seriesX);
          panel.chart.setCriterios(c3, c2, c1);
          panel.chart.setTitulo(res.getString("this.chart.Titulo") + " ");
          panel.chart.setTituloEje(res.getString("this.chart.TituloEje"),
                                   leyenda[0]);
          //panel.chart.setis
          panel.show();
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg10.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    // datos de envio
    DataNivel1 nivel1;
    DataZBS2 nivel2;
    DataZBS zbs;
    DataMun mun;
    DataEqNot eqnot;
    DataCN cnnot;
    DataCat2 prov;
    DataEnferedo enf;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtEnfer")) && (txtEnfer.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataEnferedo(txtEnfer.getText()));
      strServlet = strSERVLETEnf;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodAre")) &&
             (txtCodAre.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataNivel1(txtCodAre.getText()));
      strServlet = strSERVLETNivel1;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodDis")) &&
             (txtCodDis.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS2(txtCodAre.getText(), txtCodDis.getText(),
                                    "", ""));
      strServlet = strSERVLETNivel2;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodZBS")) &&
             (txtCodZBS.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS(txtCodZBS.getText(), "", "",
                                   txtCodAre.getText(), txtCodDis.getText()));
      strServlet = strSERVLETZona;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodPro")) &&
             (txtCodPro.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataCat2(txtCodPro.getText()));
      strServlet = strSERVLETProv;
      modoServlet = servletOBTENER_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA;

    }
    else if ( (txt.getName().equals("txtCodMun")) &&
             (txtCodMun.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataMun(txtCodPro.getText(), txtCodMun.getText(), "",
                                   "", "", ""));

      strServlet = strSERVLETMun;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodEquNot")) &&
             (txtCodEquNot.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataEqNot(txtCodEquNot.getText(), "",
                                     txtCodCenNot.getText(), "", "", "", "", "",
                                     "", "",
                                     "", 0, "", "", false));
      strServlet = strSERVLETEquipo;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodCenNot")) &&
             (txtCodCenNot.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataCN(txtCodCenNot.getText(), "", "", "", "", "",
                                  "",
                                  "", "", "", "", "", "", "", "", "", ""));
      strServlet = strSERVLETCentro;
      modoServlet = servletSELECCION_X_CODIGO;

    }

    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtEnfer")) {
            enf = (DataEnferedo) param.firstElement();
            txtEnfer.removeKeyListener(txtKeyAdapter);
            txtEnfer.setText(enf.getCod());
            txtDesEnfer.setText(enf.getDes());
            txtEnfer.addKeyListener(txtKeyAdapter);

          }
          else if (txt.getName().equals("txtCodAre")) {
            nivel1 = (DataNivel1) param.firstElement();
            txtCodAre.removeKeyListener(txtKeyAdapter);
            txtCodAre.setText(nivel1.getCod());
            txtDesAre.setText(nivel1.getDes());
            txtCodAre.addKeyListener(txtKeyAdapter);
            //checkCasos.setEnabled(true);
            //checkTasas.setEnabled(true);
            checkCasos.setEnabled(! (txtDesCenNot.getText().length() > 0)
                                  );
            checkTasas.setEnabled(! (txtDesCenNot.getText().length() > 0)
                                  );

          }
          else if (txt.getName().equals("txtCodDis")) {
            nivel2 = (DataZBS2) param.firstElement();
            txtCodDis.removeKeyListener(txtKeyAdapter);
            txtCodDis.setText(nivel2.getNiv2());
            txtDesDis.setText(nivel2.getDes());
            txtCodDis.addKeyListener(txtKeyAdapter);
          }
          else if (txt.getName().equals("txtCodZBS")) {
            zbs = (DataZBS) param.firstElement();
            txtCodZBS.removeKeyListener(txtKeyAdapter);
            txtCodZBS.setText(zbs.getCod());
            txtDesZBS.setText(zbs.getDes());
            txtCodZBS.addKeyListener(txtKeyAdapter);
            checkCasos.setState(true);
            checkTasas.setState(false);
            checkCasos.setEnabled(false);
            checkTasas.setEnabled(false);

          }
          else if (txt.getName().equals("txtCodPro")) {
            prov = (DataCat2) param.firstElement();
            txtCodPro.removeKeyListener(txtKeyAdapter);
            txtCodPro.setText(prov.getCod());
            txtDesPro.setText(prov.getDes());
            txtCodPro.addKeyListener(txtKeyAdapter);
            checkCasos.setState(true);
            checkTasas.setState(false);
            checkCasos.setEnabled(false);
            checkTasas.setEnabled(false);

          }
          else if (txt.getName().equals("txtCodMun")) {
            mun = (DataMun) param.firstElement();
            txtCodMun.removeKeyListener(txtKeyAdapter);
            txtCodMun.setText(mun.getMunicipio());
            txtDesMun.setText(mun.getDescMun());
            txtCodMun.addKeyListener(txtKeyAdapter);
          }
          else if (txt.getName().equals("txtCodEquNot")) {
            eqnot = (DataEqNot) param.firstElement();
            txtCodEquNot.removeKeyListener(txtKeyAdapter);
            txtCodEquNot.setText(eqnot.getEquipo());
            txtDesEquNot.setText(eqnot.getDesEquipo());
            txtCodEquNot.addKeyListener(txtKeyAdapter);
          }
          else if (txt.getName().equals("txtCodCenNot")) {
            cnnot = (DataCN) param.firstElement();
            txtCodCenNot.removeKeyListener(txtKeyAdapter);
            txtCodCenNot.setText(cnnot.getCodCentro());
            txtDesCenNot.setText(cnnot.getCentroDesc());
            txtCodCenNot.addKeyListener(txtKeyAdapter);
            checkCasos.setState(true);
            checkTasas.setState(false);
            checkCasos.setEnabled(false);
            checkTasas.setEnabled(false);

          }
        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg30.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtEnfer")) &&
        (txtDesEnfer.getText().length() > 0)) {
//      txtEnfer.setText("");
      txtDesEnfer.setText("");
    }
    else if ( (txt.getName().equals("txtCodAre")) &&
             (txtDesAre.getText().length() > 0)) {
//      txtCodAre.setText("");
      txtCodDis.setText("");
      txtCodZBS.setText("");
      txtDesAre.setText("");
      txtDesDis.setText("");
      txtDesZBS.setText("");
      checkCasos.setEnabled(! (txtDesCenNot.getText().length() > 0)
                            );

      checkTasas.setEnabled(! (txtDesCenNot.getText().length() > 0)
                            );

    }
    else if ( (txt.getName().equals("txtCodDis")) &&
             (txtDesDis.getText().length() > 0)) {
//      txtCodDis.setText("");
      txtCodZBS.setText("");
      txtDesDis.setText("");
      txtDesZBS.setText("");

    }
    else if ( (txt.getName().equals("txtCodZBS")) &&
             (txtDesZBS.getText().length() > 0)) {
//      txtCodZBS.setText("");
      txtDesZBS.setText("");
      checkCasos.setState( ( (txtDesPro.getText().length() > 0) ||
                            (txtDesCenNot.getText().length() > 0)));
      checkTasas.setState(! ( (txtDesPro.getText().length() > 0) ||
                             (txtDesCenNot.getText().length() > 0)));
      checkCasos.setEnabled(! ( (txtDesPro.getText().length() > 0) ||
                               (txtDesCenNot.getText().length() > 0)));
      checkTasas.setEnabled(! ( (txtDesPro.getText().length() > 0) ||
                               (txtDesCenNot.getText().length() > 0)));
    }
    else if ( (txt.getName().equals("txtCodPro")) &&
             (txtDesPro.getText().length() > 0)) {
//      txtCodPro.setText("");
      txtCodMun.setText("");
      txtDesPro.setText("");
      txtDesMun.setText("");
      checkCasos.setState( ( (txtDesCenNot.getText().length() > 0) ||
                            (txtDesZBS.getText().length() > 0)));
      checkTasas.setState(! ( (txtDesCenNot.getText().length() > 0) ||
                             (txtDesZBS.getText().length() > 0)));
      checkCasos.setEnabled(! ( (txtDesCenNot.getText().length() > 0) ||
                               (txtDesZBS.getText().length() > 0)));
      checkTasas.setEnabled(! ( (txtDesCenNot.getText().length() > 0) ||
                               (txtDesZBS.getText().length() > 0)));
    }
    else if ( (txt.getName().equals("txtCodMun")) &&
             (txtDesMun.getText().length() > 0)) {
//      txtCodMun.setText("");
      txtDesMun.setText("");
    }
    else if ( (txt.getName().equals("txtCodEquNot")) &&
             (txtDesEquNot.getText().length() > 0)) {
//      txtCodEquNot.setText("");
      txtDesEquNot.setText("");
    }
    else if ( (txt.getName().equals("txtCodCenNot")) &&
             (txtDesCenNot.getText().length() > 0)) {
//      txtCodCenNot.setText("");
      txtCodEquNot.setText("");
      txtDesCenNot.setText("");
      txtDesEquNot.setText("");
      checkCasos.setState( ( (txtDesPro.getText().length() > 0) ||
                            (txtDesZBS.getText().length() > 0)));
      checkTasas.setState(! ( (txtDesPro.getText().length() > 0) ||
                             (txtDesZBS.getText().length() > 0)));
      checkCasos.setEnabled(! ( (txtDesPro.getText().length() > 0) ||
                               (txtDesZBS.getText().length() > 0)));
      checkTasas.setEnabled(! ( (txtDesPro.getText().length() > 0) ||
                               (txtDesZBS.getText().length() > 0)));
    }

    Inicializar();
  }
} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  PanelConsEdoAgru adaptee = null;
  ActionEvent e = null;

  public actionListener(PanelConsEdoAgru adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarEnfer")) {
      adaptee.btnCtrlBuscarEnfer_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarPro")) {
      adaptee.btnCtrlbuscarPro_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarMun")) {
      adaptee.btnCtrlbuscarMun_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarAre")) {
      adaptee.btnCtrlbuscarAre_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarDis")) {
      adaptee.btnCtrlbuscarDis_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarZBS")) {
      adaptee.btnCtrlbuscarZBS_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarCenNot")) {
      adaptee.btnCtrlbuscarCenNot_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarEquNot")) {
      adaptee.btnCtrlbuscarEquNot_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("grafico")) {
      adaptee.btnGrafico_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanelConsEdoAgru adaptee;
  FocusEvent event;

  focusAdapter(PanelConsEdoAgru adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

////////////////////// Clases para listas

class CListaEnfEDO
    extends CListaValores {

  public CListaEnfEDO(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataEnferedo(s);

  }

  public String getCodigo(Object o) {
    return ( ( (DataEnferedo) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEnferedo) o).getDes());
  }
}

// lista de valores
class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

// lista de valores
class CListaZBS2
    extends CListaValores {

  protected PanelConsEdoAgru panel;

  public CListaZBS2(PanelConsEdoAgru p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtCodAre.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

// lista de valores
class CListaZona
    extends CListaValores {

  protected PanelConsEdoAgru panel;

  public CListaZona(PanelConsEdoAgru p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS(s, "", "", panel.txtCodAre.getText(),
                       panel.txtCodDis.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS) o).getDes());
  }
}

class CListaMun
    extends CListaValores {

  protected PanelConsEdoAgru panel;

  public CListaMun(PanelConsEdoAgru p,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataMun(panel.txtCodPro.getText(), s, "", "", "", "");
//    return new DataMunicipioEDO(s,panel.txtCodPro.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataMun) o).getMunicipio());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataMun) o).getDescMun());
  }
}

// lista de valores
class CListaEqNot
    extends CListaValores {
  protected PanelConsEdoAgru panel;

  public CListaEqNot(PanelConsEdoAgru pnl,
                     String title,
                     StubSrvBD stub,
                     String servlet,
                     int obtener_x_codigo,
                     int obtener_x_descricpcion,
                     int seleccion_x_codigo,
                     int seleccion_x_descripcion) {
    super(pnl.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = pnl;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataEqNot(s, "", panel.txtCodCenNot.getText(), "", "", "", "",
                         "", "", "",
                         "", 0, this.app.getLogin(), "", false);
  }

  public String getCodigo(Object o) {
    return ( ( (DataEqNot) o).getEquipo());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEqNot) o).getDesEquipo());
  }
}

// lista de valores
class CListaCN
    extends CListaValores {

  public CListaCN(CApp a,
                  String title,
                  StubSrvBD stub,
                  String servlet,
                  int obtener_x_codigo,
                  int obtener_x_descricpcion,
                  int seleccion_x_codigo,
                  int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataCN(s, "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                      "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataCN) o).getCodCentro());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCN) o).getCentroDesc());
  }
}

class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  PanelConsEdoAgru adaptee;

  txt_keyAdapter(PanelConsEdoAgru adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}
