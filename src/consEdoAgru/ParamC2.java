package consEdoAgru;

import java.io.Serializable;

public class ParamC2
    implements Serializable {

  static final int erwCASOS_EDO_AGRU = 1;
  static final int erwCASOS_EDO_AGRU_NO_EQ_NI_CEN = 2;
  static final int erwCASOS_EDO_AGRU_EQ = 3;
  static final int erwCASOS_EDO_AGRU_CEN = 4;
  static final int erwCASOS_EDO_AGRU_EQ_Y_CEN = 5;

  public String anoDesde = new String();
  public String semDesde = new String();
  public String anoHasta = new String();
  public String semHasta = new String();
  public int agrupacion = 0;
  public String enfermedad = new String();
  public String enfermedadDesc = new String();
  public String eqNotif = new String();
  public String eqNotifDesc = new String();
  public String cenNotif = new String();
  public String cenNotifDesc = new String();
  public String zbs = new String();
  public String zbsDesc = new String();
  public String area = new String();
  public String areaDesc = new String();
  public String distrito = new String();
  public String distritoDesc = new String();
  public String provincia = new String();
  public String provinciaDesc = new String();
  public String municipio = new String();
  public String municipioDesc = new String();
  public boolean tasas = false;
  public int numPagina = 0;
  public boolean bInformeCompleto = false;

  public int criterio = 0;

  public ParamC2() {
  }

  public ParamC2(String anoD, String semD, String anoH, String semH,
                 int agru, String enf, boolean t) {
    anoDesde = anoD;
    semDesde = semD;
    anoHasta = anoH;
    semHasta = semH;
    agrupacion = agru;
    enfermedad = enf;
    tasas = t;
  }
}
