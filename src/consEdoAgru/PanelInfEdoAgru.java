package consEdoAgru;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

//import graf.*;

public class PanelInfEdoAgru
    extends EPanel {
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 1;

  final String strLOGO_CCAA = "images/ccaa.gif";

  final String strSERVLET = "servlet/SrvConsEdoAgru";

  final int erwCASOS_EDO_AGRU = 1;
  final int erwCASOS_EDO_AGRU_NO_EQ_NI_CEN = 2;
  final int erwCASOS_EDO_AGRU_EQ = 3;
  final int erwCASOS_EDO_AGRU_CEN = 4;
  final int erwCASOS_EDO_AGRU_EQ_Y_CEN = 5;

  // estructuras de datos
  protected Vector vTotalCasos;
  protected Vector vTotalLineas;
  protected Vector vAgrupaciones;
  protected boolean tasas = false;

  protected CLista lista;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  public ParamC2 paramC2;

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  public PanelInfEdoAgru(CApp a) {
    super(a);
    res = ResourceBundle.getBundle("consEdoAgru.Res" + a.getIdioma());

    try {
      stub = new StubSrvBD();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(getCApp().getCodeBase().toString() + "erw/Consulta72.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;

    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {

        PrepararInforme();

        // obtiene los datos del servidor
        paramC2.numPagina++;

        param.addElement(paramC2);
        param.trimToSize();
        stub.setUrl(new URL(app.getURL() + strSERVLET));

        param.setLogin(app.getLogin());
        param.setLortad(app.getParameter("LORTAD"));
        lista = (CLista) stub.doPost(paramC2.criterio, param);
        v = (Vector) lista.elementAt(0);
        vTotalCasos = (Vector) lista.elementAt(1);
        vTotalLineas = (Vector) lista.elementAt(2);

        for (int j = 0; j < v.size(); j++) {
          vAgrupaciones.addElement(v.elementAt(j));

          // control de registros
        }
        this.setTotalRegistros(calcularTotalRegistros(paramC2));
        this.setRegistrosMostrados( (new Integer(vAgrupaciones.size())).
                                   toString());
        //AIC
        //this .setTotal(vTotalCasos.elementAt(0).toString());
        this.setTotal(DataC2.formatearEntero(Integer.parseInt(vTotalCasos.
            elementAt(0).toString())));
        // repintado
        erwClient.refreshReport(true);

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // informe comleto
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  public boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO, "");
    CLista param = new CLista();

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      PrepararInforme();

      // obtiene los datos del servidor
      paramC2.numPagina = 0;
      paramC2.bInformeCompleto = conTodos;
      param.addElement(paramC2);
      param.trimToSize();
      stub.setUrl(new URL(app.getURL() + strSERVLET));
      param.setLogin(app.getLogin());
      param.setLortad(app.getParameter("LORTAD"));

      lista = (CLista) stub.doPost(paramC2.criterio, param);

      /*      SrvConsEdoAgru srv = new SrvConsEdoAgru();
            srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@10.160.5.149:1521:ORCL",
                                 "pista",
                                 "loteb98");
            lista = srv.doDebug(paramC2.criterio, param);
       */

      vAgrupaciones = (Vector) lista.elementAt(0);
      vTotalCasos = (Vector) lista.elementAt(1);
      vTotalLineas = (Vector) lista.elementAt(2);
      tasas = ( (Boolean) lista.elementAt(3)).booleanValue();

      // control de registros
      if (vTotalLineas.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg31.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return false;
      }
      else {

        if (paramC2.tasas) {
          if (!tasas) {
            //No se han calculado las tasas y no se mostrara el informe
            msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                                  res.getString("msg32.Text"));
            msgBox.show();
            msgBox = null;
            return false;
          }
        }

        for (int j = 0; j < vAgrupaciones.size(); j++) {
          ( (DataC2) vAgrupaciones.elementAt(j)).CASOS = DataC2.formatearEntero(
              Integer.parseInt( ( (DataC2) vAgrupaciones.elementAt(j)).CASOS));
        }

        Integer total = new Integer(calcularTotalRegistros(paramC2));
        Integer mostrados = (new Integer(vAgrupaciones.size()));
        if (mostrados.intValue() > total.intValue()) {
          this.setTotalRegistros(mostrados.toString());
        }
        else {
          this.setTotalRegistros(total.toString());
        }
        this.setRegistrosMostrados( (new Integer(vAgrupaciones.size())).
                                   toString());
        //AIC
        //this .setTotal(vTotalCasos.elementAt(0).toString());
        this.setTotal(DataC2.formatearEntero(Integer.parseInt(vTotalCasos.
            elementAt(0).toString())));
        // establece las matrices de datos
        Vector retval = new Vector();
        retval.addElement("DESDE = DESDE");
        retval.addElement("HASTA = HASTA");
        retval.addElement("CASOS = CASOS");
        retval.addElement("TASAS = TASAS");
        dataHandler.RegisterTable(vAgrupaciones, "SIVE_C2", retval, null);
        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        modoOperacion = modoNORMAL;
        Inicializar();
        return true;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();
      modoOperacion = modoNORMAL;
      Inicializar();
      return false;
    }

  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;
    int iEtiqueta = 0;
    /*String sEtiqueta[] = {"LAB010",
                           "LAB011",
                           "LAB012",
                           "LAB013",
                           "LAB014"};*/
    String sEtiqueta[] = {
        "CRITERIO1",
        "CRITERIO2",
        "CRITERIO3",
        "CRITERIO4",
        "CRITERIO5"};

    // plantilla
    TemplateManager tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
    tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

    for (int i = 0; i < 5; i++) {
      tm.SetLabel(sEtiqueta[i], "");

      // carga los citerios
//    tm.SetLabel("LAB005", "Desde a�o: " + paramC2.anoDesde + "  Semana: " + paramC2.semDesde);
//    tm.SetLabel("LAB007", " Hasta a�o: " + paramC2.anoHasta + "  Semana: " + paramC2.semHasta);
//    tm.SetLabel("LAB008", "Agrupaci�n: " + paramC2.agrupacion);
//    tm.SetLabel("LAB009", "Enfermedad: " + paramC2.enfermedad);

      // carga los citerios
      /*tm.SetLabel("CRITERIO", EPanel.PERIODO + paramC2.anoDesde + EPanel.SEPARADOR_ANO_SEM + paramC2.semDesde
           + EPanel.HASTA + paramC2.anoHasta + EPanel.SEPARADOR_ANO_SEM + paramC2.semHasta
           + EPanel.AGRUPACION + paramC2.agrupacion + EPanel.ENFERMEDAD + paramC2.enfermedad
        );*/
      // carga los citerios

    }
    tm.SetLabel(sEtiqueta[iEtiqueta],
                res.getString("msg33.Text") + paramC2.anoDesde + " " +
                res.getString("msg34.Text") + paramC2.semDesde + " , "
                + res.getString("msg35.Text") + paramC2.anoHasta + " " +
                res.getString("msg36.Text") + paramC2.semHasta
                + res.getString("msg37.Text") +
                Integer.toString(paramC2.agrupacion) + " ");

    if (paramC2.agrupacion == 1) {
      tm.SetLabel(sEtiqueta[iEtiqueta],
                  tm.GetLabel(sEtiqueta[iEtiqueta]) + " " +
                  res.getString("msg38.Text"));
    }
    else {
      tm.SetLabel(sEtiqueta[iEtiqueta],
                  tm.GetLabel(sEtiqueta[iEtiqueta]) +
                  res.getString("msg39.Text"));
    }
    iEtiqueta++;

    tm.SetLabel(sEtiqueta[iEtiqueta],
                res.getString("msg40.Text") + paramC2.enfermedad + " "
                + paramC2.enfermedadDesc);
    iEtiqueta++;

    if (!paramC2.cenNotif.equals(new String()) ||
        !paramC2.eqNotif.equals(new String())) {

      // cn
      if (!paramC2.cenNotif.equals(new String())) {
//        tm.SetLabel(sEtiqueta[iEtiqueta], res.getString("msg41.Text") + paramC2.cenNotif);
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    res.getString("msg42.Text") + paramC2.cenNotif + " "
                    + paramC2.cenNotifDesc + " ");

      }

      // en
      if (!paramC2.eqNotif.equals(new String())) {
//        tm.SetLabel(sEtiqueta[iEtiqueta], "Equipo Notificador: " + paramC2.eqNotif);
        tm.SetLabel(sEtiqueta[iEtiqueta], tm.GetLabel(sEtiqueta[iEtiqueta])
                    + "   " + res.getString("msg43.Text") + paramC2.eqNotif +
                    " " + paramC2.eqNotifDesc);

      }
      iEtiqueta++;
    }
    else {
      // nivel1
      if (!paramC2.area.equals(new String())) {
//        tm.SetLabel(sEtiqueta[iEtiqueta],  app.getNivel1() + ": " + paramC2.area);
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    app.getNivel1() + ": " + paramC2.area + " "
                    + " " + paramC2.areaDesc + " ");

      }

      // nivel2
      if (!paramC2.distrito.equals(new String())) {
//        tm.SetLabel(sEtiqueta[iEtiqueta], app.getNivel2() + ": " + paramC2.distrito);
        tm.SetLabel(sEtiqueta[iEtiqueta], tm.GetLabel(sEtiqueta[iEtiqueta])
                    + app.getNivel2() + ": " + paramC2.distrito + " " +
                    paramC2.distritoDesc + " ");

      }

      // zbs
      if (!paramC2.zbs.equals(new String())) {
//        tm.SetLabel(sEtiqueta[iEtiqueta], "Zona B�sica de salud: " + paramC2.zbs);
        tm.SetLabel(sEtiqueta[iEtiqueta], tm.GetLabel(sEtiqueta[iEtiqueta])
                    + res.getString("msg44.Text") + paramC2.zbs + " " +
                    paramC2.zbsDesc);

      }
      if (!tm.GetLabel(sEtiqueta[iEtiqueta]).equals(new String())) {
        iEtiqueta++;
        // prov
      }
      if (!paramC2.provincia.equals(new String())) {
//        tm.SetLabel(sEtiqueta[iEtiqueta], "Provincia: " + paramC2.provincia);
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    res.getString("msg45.Text") + paramC2.provincia + " "
                    + paramC2.provinciaDesc + " ");

      }

      //mun
      if (!paramC2.municipio.equals(new String())) {
//        tm.SetLabel(sEtiqueta[iEtiqueta], "Municipio: " + paramC2.municipio);
        tm.SetLabel(sEtiqueta[iEtiqueta], tm.GetLabel(sEtiqueta[iEtiqueta])
                    + res.getString("msg46.Text") + paramC2.municipio + " " +
                    paramC2.municipioDesc);

      }
      if (!tm.GetLabel(sEtiqueta[iEtiqueta]).equals(new String())) {
        iEtiqueta++;
      }
    }

    if (paramC2.tasas) {
      tm.SetLabel("LAB001", res.getString("msg47.Text"));
      tm.SetVisible("FIE004", true);
      tm.SetVisible("FIE001", false);

      tm.SetVisible("CHA001", true);
      tm.SetBounds("CHA001", tm.GetBounds("CHA000"));
      tm.SetVisible("CHA000", false);
    }
    else {
      tm.SetLabel("LAB001", res.getString("msg48.Text"));
      tm.SetVisible("FIE004", false);
      tm.SetVisible("FIE001", true);

      tm.SetVisible("CHA000", true);
      tm.SetVisible("CHA001", false);
    }

  }

  private void setTotal(String total) {
    TemplateManager tm = erw.GetTemplateManager();
    tm.SetLabel("LAB019", total);
  }

  private String calcularTotalRegistros(ParamC2 par) {
    Integer aD = new Integer(par.anoDesde);
    Integer aH = new Integer(par.anoHasta);
    Integer sD = new Integer(par.semDesde);
    Integer sH = new Integer(par.semHasta);

    int tot = 52 - sD.intValue() + 1;
    tot += sH.intValue();
    tot += (aH.intValue() - aD.intValue() - 1) * 52;

    //Double dTot = new Double(tot/par.agrupacion);
    Double aux = new Double( (double) tot / (double) par.agrupacion);
    String sAux = aux.toString();
    sAux = sAux.substring(sAux.indexOf('.'));
    Double otroAux = new Double(sAux);
    if (otroAux.equals(new Double(.0))) {
      return (new Integer(aux.intValue())).toString();
    }
    else
    if (Math.max(otroAux.doubleValue(), 0.5) == 0.5) { //(otroAux<= new Double("0.5"))
      return (new Integer(aux.intValue() + 1)).toString();
    }
    else {
      return (new Integer(aux.intValue())).toString();
    }
    /*
         Double dTot = new Double(Math.ceil(tot/par.agrupacion));
         return (new Integer(dTot.intValue())).toString();
     */
  }

  public void MostrarGrafica() {
    TemplateManager tm = erw.GetTemplateManager();

    if (lista.getState() == CLista.listaLLENA) {
      try {
        PrepararInforme();
      }
      catch (Exception e) {
//E        //# // System_out.println("Exception preparando el informe");
        e.printStackTrace();

      }
      // oculta una de las secciones
      if (tm.IsVisible("PG_HDR")) {
        // No hay grafica
//E        //# // System_out.println("paso al MODO GRAFICA");
        tm.SetVisible("PG_HDR", false);
        tm.SetVisible("GH_00", false);
        tm.SetVisible("DTL_00", false);
        tm.SetVisible("GF_00", false); // para la pagina en blanco
        tm.SetVisible("PG_FTR", false);
        tm.SetVisible("CHA000", true);
      }
      else {
        // esta la grafica y se pasa al otro modo
//E        //# // System_out.println("paso al MODO INFORME");
        tm.SetVisible("PG_HDR", true);
        tm.SetVisible("GH_00", true);
        tm.SetVisible("DTL_00", true);
        tm.SetVisible("GF_00", true); // para la pagina en blanco
        tm.SetVisible("PG_FTR", true);
        tm.SetVisible("CHA000", false);
      }

      // repinta el grafico
      erwClient.refreshReport(true);

    }
  }
}
