//Title:        Consulta 7.2.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina
//Company:      NorSistemas
//Description:  Consulta de casos EDO de un periodo agrupados segun criterio de usuario.

package consEdoAgru;

import java.util.Vector;

public class DatosEdoAgrupados {

  String sDesde;
  String sHasta;
  long lCasos;
  float fTasas;

  // Para la consulta
  String sEnfermedad = new String();
  String sEquipo = new String();
  String sCentro = new String();
  String sZBS = new String();
  String sDistrito = new String();
  String sArea = new String();
  String sMunicipio = new String();

  int iAgruSem = 1;

  public DatosEdoAgrupados() {
  }

  public DatosEdoAgrupados(String d, String h, int c) {
    sDesde = d;
    sHasta = h;
    lCasos = c;
  }

  public static Vector getFieldVector() {
    String[] fields = {
        "desde  = getDesde",
        "hasta = getHasta",
        "casos = getCasos",
        "tasas = getTasas",
    };

    Vector retval = new Vector();

    for (int i = 0; i < fields.length; i++) {
      retval.addElement(fields[i]);

    }
    return retval;
  }

  public String getDesde() {
    return sDesde;
  }

  public String getHasta() {
    return sHasta;
  }

  public long getCasos() {
    return lCasos;
  }

  public float getTasas() {
    return fTasas;
  }

  public String getEnfermedad() {
    return sEnfermedad;
  }

  public String getEquipo() {
    return sEquipo;
  }

  public String getCentro() {
    return sCentro;
  }

  public String getZBS() {
    return sZBS;
  }

  public String getDistrito() {
    return sDistrito;
  }

  public String getArea() {
    return sArea;
  }

  public String getMunicipio() {
    return sMunicipio;
  }

  public int getAgruSem() {
    return iAgruSem;
  }

  public void setEnfermedad(String e) {
    sEnfermedad = e;
  }

  public void setEquipo(String e) {
    sEquipo = e;
  }

  public void setCentro(String c) {
    sCentro = c;
  }

  public void setZBS(String z) {
    sZBS = z;
  }

  public void setDistrito(String d) {
    sDistrito = d;
  }

  public void setArea(String a) {
    sArea = a;
  }

  public void setMunicipio(String m) {
    sMunicipio = m;
  }

  public void setAgruSem(int a) {
    iAgruSem = a;
  }

}