//Title:        Seccion 9.3.6.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Distribucion de los casos de un area segun nivel asistencial y residencia

package distcasnivres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import Registro.RegistroConsultas;
import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvCasNivRes
    extends DBServlet {

  // modos
  final int NIV_ASIS_RES = 2;

  final int DIVISION = 200;

  //Para contener codigos
  String codsEnf = new String();
  String codsEqNot = new String();
  String nmEdos = new String();

  Vector enfermedades = new Vector(); // guia
  Vector enfCtrlPag = new Vector();
  Vector res = new Vector(); //Vector residentes
  Vector noRes = new Vector(); //Vector no residentes
  Vector procesos = new Vector();
  Vector resultado = new Vector();

  //Para recoger descripciones procesos
  String sDesPro = "";
  String sDesProL = "";

  // control
  int i;
  int iEstado;
  String ultE = new String();
  String ant_enf = new String();
  String cd_enf = new String();
  String n1 = new String();
  String n2 = new String();
  Long aux = new Long(0);
  int ind = 0;

  // parametros de consulta
  parConsCasNivRes parCons;

  // ARG:
  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

//___________________________________________________________________

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    //E //# // System_out.println("SrvCasNivRes: entro en el doWork ");

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    String where = null;
    ResultSet rs = null;

    enfermedades = new Vector(70); // guia
    enfCtrlPag = new Vector();
    res = new Vector();
    noRes = new Vector();
    procesos = new Vector(70);
    resultado = new Vector();

    // objetos de datos
    CLista data = null;

    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */

    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    con.setAutoCommit(true);

    //E //# // System_out.println("SrvCasNivRes: antes (parConsCasNivRes) param.firstElement()");
    parCons = (parConsCasNivRes) param.firstElement();
    //E //# // System_out.println("SrvCasNivRes: antes del switch");
    // modos de operaci�n
    switch (opmode) {

      //_________________________________________________________________

      // consulta sobre resumen edo
      case NIV_ASIS_RES:

        // obtengo las enfermedades
        //Y forma un String con todos los c�digos de enfermedades
        //separados por comas

        //Si estamos tramando y es una trama distinta de la primera
        if (parCons.numPagina > 0) { //***********
          query = "select e.cd_enfcie,  p.ds_proceso, p.dsl_proceso "
              + " from sive_enferedo e, sive_procesos p"
              + " where e.it_baja='N' and e.cd_tvigi='I' and "
              + " e.cd_enfcie = p.cd_enfcie"
              + " and e.cd_enfcie > ? "
              //+ " order by e.cd_enfcie";
              + " order by p.ds_proceso, e.cd_enfcie";
        }
        //Si no se trama o trama es la primera  (LRG)
        else {
          query = "select e.cd_enfcie,  p.ds_proceso, p.dsl_proceso "
              + " from sive_enferedo e, sive_procesos p"
              + " where e.it_baja='N' and e.cd_tvigi='I' and "
              + " e.cd_enfcie = p.cd_enfcie"
              //+ " order by e.cd_enfcie";
              + " order by p.ds_proceso, e.cd_enfcie";

        }

        st = con.prepareStatement(query);

        if (parCons.numPagina > 0) { //***********
          st.setString(1, parCons.ultEnf);
        }

        rs = st.executeQuery();
        codsEnf = "( ";

        // JRM: Cuando se hace un select in de mas de 254 elementos
        // falla la consulta, tenemos que dividir para que funcione
        // correctamente.
        int numEnf = 0;
        boolean bAnnadirExp = false;

        String enf = new String();
        boolean hayComa = false;
        boolean hayEnf = false;

        while (rs.next()) {
          hayEnf = true;
          hayComa = false;
          enf = rs.getString(1);
          if (bAnnadirExp) {
            codsEnf = codsEnf + " or cd_enfcie in (";
            bAnnadirExp = false;
          }
          codsEnf = codsEnf + "'" + enf + "'";
          numEnf++;
          // Partimos
          if (numEnf == DIVISION) {
            codsEnf = codsEnf + ") ";
            bAnnadirExp = true;
            numEnf = 0;
          }
          else {
            codsEnf = codsEnf + ", ";
            hayComa = true;
          }

          enfermedades.addElement(enf);
          //__________ Idiomas L Rivera_____

          sDesPro = rs.getString(2);

          // obtiene la descripcion auxiliar en funci�n del idioma
          if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
            sDesProL = rs.getString(3);
            if (sDesProL != null) {
              sDesPro = sDesProL;
            }
          }
          //_________________

          procesos.insertElementAt(sDesPro,
                                   enfermedades.indexOf(enf));
          //E //# // System_out.println("SrvCasNivRes: enfermedad " +  enf);
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        if (hayEnf) { //Si hay enfermedades
          if (hayComa) {
            codsEnf = codsEnf.substring(0, codsEnf.lastIndexOf(',')) + " )";
            //else codsEnf = codsEnf + " )";
            ////# // System_out.println("SrvCasNivRes: enfermedades " + codsEnf);

            //_________________________________________________________________

            // Para ATENCION PRIMARIA ///////////////////////////////

            // obtengo los equipos notificadores
          }
          query = " select e.cd_e_notif "
              + " from sive_c_notif c, sive_e_notif e "
              + " where c.cd_centro = e.cd_centro and "
              + " c.cd_nivasis = 'P' ";

          if (!parCons.area.equals("")) {
            query = query + " and e.cd_nivel_1 = ? ";
          }
          if (!parCons.distrito.equals("")) {
            query = query + " and e.cd_nivel_2 = ? ";
          }

          st = con.prepareStatement(query);
          int par = 1;

          //E //# // System_out.println("SrvCasNivRes: en AP despues de prepareStatement");
          if (!parCons.area.equals("")) {
            st.setString(par, parCons.area);
            par++;
          }
          if (!parCons.distrito.equals("")) {
            st.setString(par, parCons.distrito);
            par++;
          }
          //E //# // System_out.println("SrvCasNivRes: despues de preparar parametros");
          rs = st.executeQuery();

          //JRM: Los equipos notificadores tambi�n pueden producir el
          // error de un n�mero m�ximo en un select in (,,,,,)
          codsEqNot = "( ";
          int numEq = 0;
          bAnnadirExp = false;
          hayComa = false;
          boolean hayEq = false;
          while (rs.next()) {
            hayEq = true;
            hayComa = false;
            if (bAnnadirExp) {
              codsEqNot = codsEqNot + " or cd_e_notif in (";
              bAnnadirExp = false;
            }
            codsEqNot = codsEqNot + "'" + rs.getString(1) + "'";
            numEq++;
            // Partimos
            if (numEq == DIVISION) {
              codsEqNot = codsEqNot + ") ";
              bAnnadirExp = true;
              numEq = 0;
            }
            else {
              codsEqNot = codsEqNot + ", ";
              hayComa = true;
            }
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //_________________________________________________________________

          //E //# // System_out.println("SrvCasNivRes: despues while ");
          if (hayEq) { //hay equipos notificadores
            if (hayComa) {
              codsEqNot = codsEqNot.substring(0, codsEqNot.lastIndexOf(',')) +
                  " )";
              //else codsEqNot = codsEqNot + ")";
              //E //# // System_out.println("SrvCasNivRes: equipos " + codsEqNot);

              //obtengo los nm_edos
            }
            query = " select nm_edo from sive_notif_edoi "
                + " where it_primero = 'S' and cd_fuente = 'E' and "
                + " cd_e_notif in " + codsEqNot;
            if (parCons.anoDesde.equals(parCons.anoHasta)) {
              query = query +
                  " and cd_anoepi = ? and cd_semepi >= ? and cd_semepi <= ? ";
            }
            else {
              query = query + " and ((cd_anoepi = ? and cd_semepi >= ? ) or "
                  + " (cd_anoepi < ? and cd_anoepi > ? ) or "
                  + " (cd_anoepi = ? and cd_semepi <= ? ))";
            }
            st = con.prepareStatement(query);
            par = 1;
            if (parCons.anoDesde.equals(parCons.anoHasta)) {
              st.setString(par, parCons.anoDesde);
              par++;
              st.setString(par, parCons.semDesde);
              par++;
              st.setString(par, parCons.semHasta);
              par++;
            }
            else {
              st.setString(par, parCons.anoDesde);
              par++;
              st.setString(par, parCons.semDesde);
              par++;
              st.setString(par, parCons.anoDesde);
              par++;
              st.setString(par, parCons.anoHasta);
              par++;
              st.setString(par, parCons.anoHasta);
              par++;
              st.setString(par, parCons.semHasta);
              par++;
            }
            rs = st.executeQuery();
            // JRM: Partimos
            nmEdos = "( ";
            int numEdos = 0;
            bAnnadirExp = false;
            hayComa = false;
            boolean hayEdos = false;
            while (rs.next()) {
              hayEdos = true;
              hayComa = false;
              if (bAnnadirExp) {
                nmEdos = nmEdos + " or nm_edo in (";
                bAnnadirExp = false;
              }
              nmEdos = nmEdos + rs.getString(1);
              numEdos++;
              // Partimos
              if (numEdos == DIVISION) {
                nmEdos = nmEdos + ") ";
                bAnnadirExp = true;
                numEdos = 0;
              }
              else {
                nmEdos = nmEdos + ", ";
                hayComa = true;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;
            //_________________________________________________________________

            //Se realiza la �ltima consulta para obtener datos referentes
            // a casos de atenci�n primaria
            //La informaci�n que se recoja de esta consulta ya se ir� ordenando
            //e introduciendo en vectores que representan la info que ver� usuario

            if (hayEdos) { //hay nmEdos
              if (hayComa) {
                nmEdos = nmEdos.substring(0, nmEdos.lastIndexOf(',')) + " )";
                //else   nmEdos = nmEdos + " )";
                ////# // System_out.println("SrvCasNivRes: edos " + nmEdos);

                //obtengo las areas y distritos de los casos
                /*query = " select cd_enfcie, cd_nivel_1, cd_nivel_2 "
                  + " from sive_edoind "
                  + " where nm_edo in " + nmEdos
                  + " and cd_enfcie in " + codsEnf;
                               if (parCons.anoDesde.equals(parCons.anoHasta)) {
                     query = query + " and cd_anoepi = ? and cd_semepi >= ? and cd_semepi <= ? ";
                               }
                               else {
                     query = query + " and ((cd_anoepi = ? and cd_semepi >= ? ) or "
                               + " (cd_anoepi < ? and cd_anoepi > ? ) or "
                               + " (cd_anoepi = ? and cd_semepi <= ? ))";
                               }
                               query = query + " order by cd_enfcie ";*/
                // modificacion jlt para ordenar por descripci�n de enfermedad

              }
              query = " select a.cd_enfcie, a.cd_nivel_1, a.cd_nivel_2 "
                  + " from sive_edoind a, sive_procesos b  "
                  + " where a.cd_enfcie = b.cd_enfcie and "
                  + " a.nm_edo in " + nmEdos
                  + " and a.cd_enfcie in " + codsEnf;

              if (parCons.anoDesde.equals(parCons.anoHasta)) {
                query = query +
                    " and a.cd_anoepi = ? and a.cd_semepi >= ? and a.cd_semepi <= ? ";
              }
              else {
                query = query +
                    " and ((a.cd_anoepi = ? and a.cd_semepi >= ? ) or "
                    + " (a.cd_anoepi < ? and a.cd_anoepi > ? ) or "
                    + " (a.cd_anoepi = ? and a.cd_semepi <= ? ))";
              }
              query = query + " order by b.ds_proceso, a.cd_enfcie ";
              ////# // System_out.println("SrvCasNivRes: " + query);
              st = con.prepareStatement(query);
              par = 1;
              if (parCons.anoDesde.equals(parCons.anoHasta)) {
                st.setString(par, parCons.anoDesde);
                par++;
                st.setString(par, parCons.semDesde);
                par++;
                st.setString(par, parCons.semHasta);
                par++;
                // ARG: Se a�aden parametros
                registroConsultas.insertarParametro(parCons.anoDesde);
                registroConsultas.insertarParametro(parCons.semDesde);
                registroConsultas.insertarParametro(parCons.semHasta);
              }
              else {
                st.setString(par, parCons.anoDesde);
                par++;
                st.setString(par, parCons.semDesde);
                par++;
                st.setString(par, parCons.anoDesde);
                par++;
                st.setString(par, parCons.anoHasta);
                par++;
                st.setString(par, parCons.anoHasta);
                par++;
                st.setString(par, parCons.semHasta);
                par++;
                // ARG: Se a�aden parametros
                registroConsultas.insertarParametro(parCons.anoDesde);
                registroConsultas.insertarParametro(parCons.semDesde);
                registroConsultas.insertarParametro(parCons.anoDesde);
                registroConsultas.insertarParametro(parCons.anoHasta);
                registroConsultas.insertarParametro(parCons.anoHasta);
                registroConsultas.insertarParametro(parCons.semHasta);
              }
              rs = st.executeQuery();

              // ARG: Registro LORTAD
              registroConsultas.registrar("SIVE_EDOIND", query, "CD_ENFERMO",
                                          "", "SrvCasNivRes", true);

              //_________________________________________________________________

              //Se crean las estructuras para recibir informaci�n de la consulta

              //Vector residentes  (LRG)
              //Representa a una columna de la informaci�n que ver� el usuario
              res = new Vector(enfermedades.size());
              //Vector no residentes
              //Representa a una columna de la informaci�n que ver� el usuario
              noRes = new Vector(enfermedades.size());

              //Se inicializan todos los valores de los vectores de residentes
              // y no reisdentes a cero casos
              for (int s = 0; s < enfermedades.size(); s++) {
                res.addElement(new Long(0));
                noRes.addElement(new Long(0));
              }
              //E //# // System_out.println("SrvCasNivRes: he inicializado res y noREs");
              n1 = new String();
              n2 = new String();
              cd_enf = new String();
              aux = new Long(0);
              ind = 0;

              enfCtrlPag = new Vector();

              int lineas = 1;
              iEstado = CLista.listaVACIA;
              //E //# // System_out.println("SrvCasNivRes: despues de las variables y antes del while");

              //_________________________________________________________________

              while (rs.next()) {
                //   select cd_enfcie, cd_nivel_1, cd_nivel_2
                ant_enf = cd_enf;
                cd_enf = rs.getString(1);
                //E //# // System_out.println("SrvCasNivRes: en el while " + cd_enf);
                n1 = rs.getString(2);
                n2 = rs.getString(3);

                if ( (lineas > DBServlet.maxSIZE) &&
                    (! (parCons.bInformeCompleto))) {
                  iEstado = CLista.listaINCOMPLETA;
                  ultE = ant_enf;
                  //E //# // System_out.println("SrvCasNivRes: ultEnf " + ultE );
                  break;
                }
                // control de estado
                if (iEstado == CLista.listaVACIA) {
                  iEstado = CLista.listaLLENA;

                  // para tener las enfermedades que se procesan
                  // en cada pagina para AE y OT
                  // res y noRes siguen los mismos indices

                  // Residente si coinciden o nulos!!!!!!

                }
                if (!enfCtrlPag.contains(cd_enf)) {
                  enfCtrlPag.addElement(cd_enf);
                  lineas++;
                }

                //_________________________________________________________________
                // Compruebo la residencia

                //Si area del caso y equipo coinciden o caso no tine area (LRG)
                if (parCons.area.equals(n1) || n1 == null) {

                  //Si usuario ha indicado distrito
                  if (!parCons.distrito.equals("")) {

                    //Si distrito del caso y equipo coinciden o caso no tine distrito (LRG)
                    if (parCons.distrito.equals(n2) || n2 == null) {
                      //residente
                      //E //# // System_out.println("SrvCasNivRes: residente");
                      ind = enfCtrlPag.indexOf(cd_enf);
                      //E //# // System_out.println("SrvCasNivRes: residente ind " + ind);
                      aux = (Long) res.elementAt(ind);
                      aux = new Long(aux.longValue() + 1);
                      res.setElementAt(aux, ind);
                    }
                    //Si distrito del caso y equipo no coinciden
                    else {
                      //no residente
                      ind = enfCtrlPag.indexOf(cd_enf);
                      //E //# // System_out.println("SrvCasNivRes: no residente ind " + ind);
                      aux = (Long) noRes.elementAt(ind);
                      //E //# // System_out.println("SrvCasNivRes: no residente aux " + aux);
                      aux = new Long(aux.longValue() + 1);
                      noRes.setElementAt(aux, ind);
                    }
                  }

                  //Si usuario no ha indicado distrito
                  else { //residente
                    //E //# // System_out.println("SrvCasNivRes: residente");
                    ind = enfCtrlPag.indexOf(cd_enf);
                    //E //# // System_out.println("SrvCasNivRes: residente ind " + ind);
                    aux = (Long) res.elementAt(ind);
                    aux = new Long(aux.longValue() + 1);
                    res.setElementAt(aux, ind);
                  }
                }
                //Si area del caso y equipo no coinciden
                else { //no residente

                  ind = enfCtrlPag.indexOf(cd_enf);
                  //E //# // System_out.println("SrvCasNivRes: no residente ind " + ind);
                  aux = (Long) noRes.elementAt(ind);
                  //E //# // System_out.println("SrvCasNivRes: no residente aux " + aux);
                  aux = new Long(aux.longValue() + 1);
                  noRes.setElementAt(aux, ind);
                }

                //_________________________________________________________________

              } //Fin while de recogida de datos

              //E //# // System_out.println("SrvCasNivRes: despues del while y antes de rellenar resultado");
              rs.close();
              rs = null;
              st.close();
              st = null;

              // enfCtrlPag es la guia para el control de pagina del informe
              // para las siguiente consultas de atencion especializada y otros

              for (int j = 0; j < enfCtrlPag.size(); j++) {
                String auxDs = (String) procesos.elementAt(enfermedades.indexOf( (
                    String) enfCtrlPag.elementAt(j)));
                Long auxR = (Long) res.elementAt(j);
                Long auxN = (Long) noRes.elementAt(j);
                //E //# // System_out.println("SrvCasNivRes: AP para resultado " + auxDs + " "
                //                                             + auxR + " "
                //                                             + auxN + " ");
                DataC936 dato = new DataC936(auxDs,
                                             (Long) res.elementAt(j),
                                             (Long) noRes.elementAt(j));
                resultado.insertElementAt(dato, j);
                //E //# // System_out.println("SrvCasNivRes: inserto en resultado " + j);
              }
              //E //# // System_out.println("SrvCasNivRes: despues de rellenar resultado");
            } //nmEdos
            else {
              rellenarConCerosAP();
            } //else edos
          } //equipos
          else {
            rellenarConCerosAP();
          }

          //___________________________________________________________________
          //___________________________________________________________________

          // Para ATENCION ESPECIALIZADA ///////////////////////////////////////

          // obtengo los equipos notificadores
          query = " select e.cd_e_notif "
              + " from sive_c_notif c, sive_e_notif e "
              + " where c.cd_centro = e.cd_centro and "
              + " c.cd_nivasis = 'E' ";
          if (!parCons.area.equals("")) {
            query = query + " and e.cd_nivel_1 = ? ";
          }
          if (!parCons.distrito.equals("")) {
            query = query + " and e.cd_nivel_2 = ? ";
          }
          ////# // System_out.println("SrvCasNivRes: " + query);
          st = con.prepareStatement(query);
          par = 1;

          if (!parCons.area.equals("")) {
            st.setString(par, parCons.area);
            par++;
          }
          if (!parCons.distrito.equals("")) {
            st.setString(par, parCons.distrito);
            par++;
          }
          rs = st.executeQuery();
          // JRM: dividimos select in
          codsEqNot = "( ";
          hayComa = false;
          numEq = 0;
          bAnnadirExp = false;
          hayEq = false;
          while (rs.next()) {
            hayEq = true;
            hayComa = false;
            if (bAnnadirExp) {
              codsEqNot = codsEqNot + " or cd_e_notif in (";
              bAnnadirExp = false;
            }
            codsEqNot = codsEqNot + "'" + rs.getString(1) + "'";
            numEq++;
            //Paritmos
            if (numEq == DIVISION) {
              codsEqNot = codsEqNot + ") ";
              bAnnadirExp = true;
              numEq = 0;
            }
            else {
              codsEqNot = codsEqNot + ", ";
              hayComa = true;
            }
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          if (hayEq) { // hay equipos
            if (hayComa) {
              codsEqNot = codsEqNot.substring(0, codsEqNot.lastIndexOf(',')) +
                  " )";
              //else codsEqNot = codsEqNot + " ) ";
              ////# // System_out.println("SrvCasNivRes: codsEq" + codsEqNot);

              //obtengo los nm_edos
            }
            query = " select nm_edo from sive_notif_edoi "
                + " where it_primero = 'S' and cd_fuente = 'E' and "
                + " cd_e_notif in " + codsEqNot;
            if (parCons.anoDesde.equals(parCons.anoHasta)) {
              query = query +
                  " and cd_anoepi = ? and cd_semepi >= ? and cd_semepi <= ? ";
            }
            else {
              query = query + " and ((cd_anoepi = ? and cd_semepi >= ? ) or "
                  + " (cd_anoepi < ? and cd_anoepi > ? ) or "
                  + " (cd_anoepi = ? and cd_semepi <= ? ))";
            }
            ////# // System_out.println("SrvCasNivRes: " + query);
            st = con.prepareStatement(query);
            par = 1;
            if (parCons.anoDesde.equals(parCons.anoHasta)) {
              st.setString(par, parCons.anoDesde);
              par++;
              st.setString(par, parCons.semDesde);
              par++;
              st.setString(par, parCons.semHasta);
              par++;
            }
            else {
              st.setString(par, parCons.anoDesde);
              par++;
              st.setString(par, parCons.semDesde);
              par++;
              st.setString(par, parCons.anoDesde);
              par++;
              st.setString(par, parCons.anoHasta);
              par++;
              st.setString(par, parCons.anoHasta);
              par++;
              st.setString(par, parCons.semHasta);
              par++;
            }
            rs = st.executeQuery();
            nmEdos = "( ";
            hayComa = false;
            boolean hayEdos = false;
            int numEdos = 0;
            bAnnadirExp = false;

            while (rs.next()) {
              hayEdos = true;
              hayComa = false;
              if (bAnnadirExp) {
                nmEdos = nmEdos + " or nm_edo in (";
                bAnnadirExp = false;
              }
              nmEdos = nmEdos + rs.getString(1);
              numEdos++;
              // Partimos
              if (numEdos == DIVISION) {
                nmEdos = nmEdos + ") ";
                bAnnadirExp = true;
                numEdos = 0;
              }
              else {
                nmEdos = nmEdos + ", ";
                hayComa = true;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

            if (hayEdos) { // hay edos
              if (hayComa) {
                nmEdos = nmEdos.substring(0, nmEdos.lastIndexOf(',')) + " )";
                //else       nmEdos = nmEdos + " ) ";
                ////# // System_out.println("SrvCasNivRes: edos" + nmEdos);

                //obtengo las areas y distritos de los casos
                /*query = " select cd_enfcie, cd_nivel_1, cd_nivel_2 "
                  + " from sive_edoind "
                  + " where nm_edo in " + nmEdos
                  + " and cd_enfcie in " + codsEnf ;
                               if (parCons.anoDesde.equals(parCons.anoHasta)) {
                     query = query + " and cd_anoepi = ? and cd_semepi >= ? and cd_semepi <= ? ";
                               }
                               else {
                     query = query + " and ((cd_anoepi = ? and cd_semepi >= ? ) or "
                               + " (cd_anoepi < ? and cd_anoepi > ? ) or "
                               + " (cd_anoepi = ? and cd_semepi <= ? ))";
                               }
                               query = query + " order by cd_enfcie ";*/

                // modificacion jlt para ordernar por descripci�n de enfermedad
              }
              query = " select a.cd_enfcie, a.cd_nivel_1, a.cd_nivel_2 "
                  + " from sive_edoind  a, sive_procesos b "
                  + " where a.cd_enfcie = b.cd_enfcie and "
                  + " a.nm_edo in " + nmEdos
                  + " and a.cd_enfcie in " + codsEnf;

              if (parCons.anoDesde.equals(parCons.anoHasta)) {
                query = query +
                    " and a.cd_anoepi = ? and a.cd_semepi >= ? and a.cd_semepi <= ? ";
              }
              else {
                query = query +
                    " and ((a.cd_anoepi = ? and a.cd_semepi >= ? ) or "
                    + " (a.cd_anoepi < ? and a.cd_anoepi > ? ) or "
                    + " (a.cd_anoepi = ? and a.cd_semepi <= ? ))";
              }
              query = query + " order by b.ds_proceso, a.cd_enfcie ";
              ////# // System_out.println("SrvCasNivRes: " + query);
              st = con.prepareStatement(query);
              par = 1;
              if (parCons.anoDesde.equals(parCons.anoHasta)) {
                st.setString(par, parCons.anoDesde);
                par++;
                st.setString(par, parCons.semDesde);
                par++;
                st.setString(par, parCons.semHasta);
                par++;
                // ARG: Se a�aden parametros
                registroConsultas.insertarParametro(parCons.anoDesde);
                registroConsultas.insertarParametro(parCons.semDesde);
                registroConsultas.insertarParametro(parCons.semHasta);
              }
              else {
                st.setString(par, parCons.anoDesde);
                par++;
                st.setString(par, parCons.semDesde);
                par++;
                st.setString(par, parCons.anoDesde);
                par++;
                st.setString(par, parCons.anoHasta);
                par++;
                st.setString(par, parCons.anoHasta);
                par++;
                st.setString(par, parCons.semHasta);
                par++;
                // ARG: Se a�aden parametros
                registroConsultas.insertarParametro(parCons.anoDesde);
                registroConsultas.insertarParametro(parCons.semDesde);
                registroConsultas.insertarParametro(parCons.anoDesde);
                registroConsultas.insertarParametro(parCons.anoHasta);
                registroConsultas.insertarParametro(parCons.anoHasta);
                registroConsultas.insertarParametro(parCons.semHasta);
              }
              rs = st.executeQuery();

              // ARG: Registro LORTAD
              registroConsultas.registrar("SIVE_EDOIND", query, "CD_ENFERMO",
                                          "", "SrvCasNivRes", true);

              res = new Vector(enfermedades.size());
              noRes = new Vector(enfermedades.size());
              for (int s = 0; s < enfermedades.size(); s++) {
                res.addElement(new Long(0));
                noRes.addElement(new Long(0));
              }

              n1 = new String();
              n2 = new String();
              cd_enf = new String();
              aux = new Long(0);
              ind = 0;

              while (rs.next()) {
                // select cd_enfcie, cd_nivel_1, cd_nivel_2
                cd_enf = rs.getString(1);

                if (enfCtrlPag.contains(cd_enf)) {
                  //esta enfermedad esta en la pagina
                  n1 = rs.getString(2);
                  n2 = rs.getString(3);
                  // Compruebo la residencia
                  if (parCons.area.equals(n1) ||
                      n1 == null) {
                    if (!parCons.distrito.equals("")) {
                      if (parCons.distrito.equals(n2) ||
                          n2 == null) {
                        //residente
                        ind = enfCtrlPag.indexOf(cd_enf);
                        aux = (Long) res.elementAt(ind);
                        aux = new Long(aux.longValue() + 1);
                        res.setElementAt(aux, ind);
                      }
                      else {
                        //no residente
                        ind = enfCtrlPag.indexOf(cd_enf);
                        aux = (Long) noRes.elementAt(ind);
                        aux = new Long(aux.longValue() + 1);
                        noRes.setElementAt(aux, ind);
                      }
                    }
                    else { //residente
                      ind = enfCtrlPag.indexOf(cd_enf);
                      aux = (Long) res.elementAt(ind);
                      aux = new Long(aux.longValue() + 1);
                      res.setElementAt(aux, ind);
                    }
                  }
                  else { //no residente
                    ind = enfCtrlPag.indexOf(cd_enf);
                    aux = (Long) noRes.elementAt(ind);
                    aux = new Long(aux.longValue() + 1);
                    noRes.setElementAt(aux, ind);
                  }
                }

              } //Fin While
              rs.close();
              rs = null;
              st.close();
              st = null;

              // enfCtrlPag es la guia para el control de pagina del informe
              // para las siguiente consultas de atencion especializada y otros

              for (int j = 0; j < enfCtrlPag.size(); j++) {
                DataC936 dato = (DataC936) resultado.elementAt(j);
                dato.NM_AE_RES = (Long) res.elementAt(j);
                dato.NM_AE_NORES = (Long) noRes.elementAt(j);
                //E //# // System_out.println("SrvCasNivRes: AE " + dato.NM_AE_RES + " "
                //                               + dato.NM_AE_NORES );
                resultado.setElementAt(dato, j);
                //E //# // System_out.println("SrvCasNivRes: inserto en resultado " + j);
              }
            } // hay nmEdos
            else {
              rellenarConCerosAE();
            }
          } // hay equipos
          else {
            rellenarConCerosAE();
          }

          //_________________________________________________________________________
          //_________________________________________________________________________

          // Para OTROS NOTIFICADORES

          // obtengo los equipos notificadores
          query = " select e.cd_e_notif "
              + " from sive_c_notif c, sive_e_notif e "
              + " where c.cd_centro = e.cd_centro and "
              + " c.cd_nivasis = 'O' ";

          if (!parCons.area.equals("")) {
            query = query + " and e.cd_nivel_1 = ? ";
          }
          if (!parCons.distrito.equals("")) {
            query = query + " and e.cd_nivel_2 = ? ";
          }
          ////# // System_out.println("SrvCasNivRes: " + query);
          st = con.prepareStatement(query);
          par = 1;

          if (!parCons.area.equals("")) {
            st.setString(par, parCons.area);
            par++;
          }
          if (!parCons.distrito.equals("")) {
            st.setString(par, parCons.distrito);
            par++;
          }
          rs = st.executeQuery();
          //JRM: Dividimos select in (,,,,)
          codsEqNot = "( ";
          hayComa = false;
          numEq = 0;
          bAnnadirExp = false;
          hayEq = false;
          while (rs.next()) {
            hayEq = true;
            hayComa = false;
            if (bAnnadirExp) {
              codsEqNot = codsEqNot + " or cd_e_notif in (";
              bAnnadirExp = false;
            }
            codsEqNot = codsEqNot + "'" + rs.getString(1) + "'";
            numEq++;
            //Paritmos
            if (numEq == DIVISION) {
              codsEqNot = codsEqNot + ") ";
              bAnnadirExp = true;
              numEq = 0;
            }
            else {
              codsEqNot = codsEqNot + ", ";
              hayComa = true;
            }
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          if (hayEq) { //hay equipos
            if (hayComa) {
              codsEqNot = codsEqNot.substring(0, codsEqNot.lastIndexOf(',')) +
                  " )";
              //else   codsEqNot = codsEqNot + ")";
              ////# // System_out.println("SrvCasNivRes: codsEq " + codsEqNot);

              //obtengo los nm_edos
            }
            query = " select nm_edo from sive_notif_edoi "
                + " where it_primero = 'S' and cd_fuente = 'E' and "
                + " cd_e_notif in " + codsEqNot;
            if (parCons.anoDesde.equals(parCons.anoHasta)) {
              query = query +
                  " and cd_anoepi = ? and cd_semepi >= ? and cd_semepi <= ? ";
            }
            else {
              query = query + " and ((cd_anoepi = ? and cd_semepi >= ? ) or "
                  + " (cd_anoepi < ? and cd_anoepi > ? ) or "
                  + " (cd_anoepi = ? and cd_semepi <= ? ))";
            }
            ////# // System_out.println("SrvCasNivRes: " + query);
            st = con.prepareStatement(query);
            par = 1;
            if (parCons.anoDesde.equals(parCons.anoHasta)) {
              st.setString(par, parCons.anoDesde);
              par++;
              st.setString(par, parCons.semDesde);
              par++;
              st.setString(par, parCons.semHasta);
              par++;
            }
            else {
              st.setString(par, parCons.anoDesde);
              par++;
              st.setString(par, parCons.semDesde);
              par++;
              st.setString(par, parCons.anoDesde);
              par++;
              st.setString(par, parCons.anoHasta);
              par++;
              st.setString(par, parCons.anoHasta);
              par++;
              st.setString(par, parCons.semHasta);
              par++;
            }
            rs = st.executeQuery();
            nmEdos = "( ";
            hayComa = false;
            int numEdos = 0;
            bAnnadirExp = false;
            boolean hayEdos = false;
            while (rs.next()) {
              hayEdos = true;
              hayComa = false;
              if (bAnnadirExp) {
                nmEdos = nmEdos + " or nm_edo in (";
                bAnnadirExp = false;
              }
              nmEdos = nmEdos + rs.getString(1);
              numEdos++;
              // Partimos
              if (numEdos == DIVISION) {
                nmEdos = nmEdos + ") ";
                bAnnadirExp = true;
                numEdos = 0;
              }
              else {
                nmEdos = nmEdos + ", ";
                hayComa = true;
              }
            }
            rs.close();
            rs = null;
            st.close();
            st = null;

            if (hayEdos) { // hay nmEdos
              if (hayComa) {
                nmEdos = nmEdos.substring(0, nmEdos.lastIndexOf(',')) + " )";
                //else  nmEdos = nmEdos + " )";
                ////# // System_out.println("SrvCasNivRes: edos " + nmEdos);

                //obtengo las areas y distritos de los casos
                /*query = " select cd_enfcie, cd_nivel_1, cd_nivel_2 "
                    + " from sive_edoind "
                    + " where nm_edo in " + nmEdos
                    + " and cd_enfcie in " + codsEnf;
                               if (parCons.anoDesde.equals(parCons.anoHasta)) {
                     query = query + " and cd_anoepi = ? and cd_semepi >= ? and cd_semepi <= ? ";
                               }
                               else {
                     query = query + " and ((cd_anoepi = ? and cd_semepi >= ? ) or "
                               + " (cd_anoepi < ? and cd_anoepi > ? ) or "
                               + " (cd_anoepi = ? and cd_semepi <= ? ))";
                               }
                               query = query + " order by cd_enfcie ";*/

              }
              query = " select a.cd_enfcie, a.cd_nivel_1, a.cd_nivel_2 "
                  + " from sive_edoind a, sive_procesos b"
                  + " where a.cd_enfcie = b.cd_enfcie and "
                  + " a.nm_edo in " + nmEdos
                  + " and a.cd_enfcie in " + codsEnf;

              if (parCons.anoDesde.equals(parCons.anoHasta)) {
                query = query +
                    " and a.cd_anoepi = ? and a.cd_semepi >= ? and a.cd_semepi <= ? ";
              }
              else {
                query = query +
                    " and ((a.cd_anoepi = ? and a.cd_semepi >= ? ) or "
                    + " (a.cd_anoepi < ? and a.cd_anoepi > ? ) or "
                    + " (a.cd_anoepi = ? and a.cd_semepi <= ? ))";
              }
              query = query + " order by cd_enfcie ";
              ////# // System_out.println("SrvCasNivRes: " + query);
              st = con.prepareStatement(query);
              par = 1;
              if (parCons.anoDesde.equals(parCons.anoHasta)) {
                st.setString(par, parCons.anoDesde);
                par++;
                st.setString(par, parCons.semDesde);
                par++;
                st.setString(par, parCons.semHasta);
                par++;
                // ARG: Se a�aden parametros
                registroConsultas.insertarParametro(parCons.anoDesde);
                registroConsultas.insertarParametro(parCons.semDesde);
                registroConsultas.insertarParametro(parCons.semHasta);
              }
              else {
                st.setString(par, parCons.anoDesde);
                par++;
                st.setString(par, parCons.semDesde);
                par++;
                st.setString(par, parCons.anoDesde);
                par++;
                st.setString(par, parCons.anoHasta);
                par++;
                st.setString(par, parCons.anoHasta);
                par++;
                st.setString(par, parCons.semHasta);
                par++;
                // ARG: Se a�aden parametros
                registroConsultas.insertarParametro(parCons.anoDesde);
                registroConsultas.insertarParametro(parCons.semDesde);
                registroConsultas.insertarParametro(parCons.anoDesde);
                registroConsultas.insertarParametro(parCons.anoHasta);
                registroConsultas.insertarParametro(parCons.anoHasta);
                registroConsultas.insertarParametro(parCons.semHasta);
              }
              rs = st.executeQuery();

              // ARG: Registro LORTAD
              registroConsultas.registrar("SIVE_EDOIND", query, "CD_ENFERMO",
                                          "", "SrvCasNivRes", true);

              res = new Vector(enfermedades.size()); //residentes
              noRes = new Vector(enfermedades.size()); //No residentes
              for (int s = 0; s < enfermedades.size(); s++) {
                res.addElement(new Long(0));
                noRes.addElement(new Long(0));
              }

              n1 = new String();
              n2 = new String();
              cd_enf = new String();
              aux = new Long(0);
              ind = 0;

              while (rs.next()) {
                // select cd_enfcie, cd_nivel_1, cd_nivel_2
                cd_enf = rs.getString(1);

                if (enfCtrlPag.contains(cd_enf)) {
                  //esta enfermedad esta en la pagina
                  n1 = rs.getString(2);
                  n2 = rs.getString(3);
                  // Compruebo la residencia
                  if (parCons.area.equals(n1) ||
                      n1 == null) {
                    if (!parCons.distrito.equals("")) {
                      if (parCons.distrito.equals(n2) ||
                          n2 == null) {
                        //residente
                        ind = enfCtrlPag.indexOf(cd_enf);
                        aux = (Long) res.elementAt(ind);
                        aux = new Long(aux.longValue() + 1);
                        res.setElementAt(aux, ind);
                      }
                      else {
                        //no residente
                        ind = enfCtrlPag.indexOf(cd_enf);
                        aux = (Long) noRes.elementAt(ind);
                        aux = new Long(aux.longValue() + 1);
                        noRes.setElementAt(aux, ind);
                      }
                    }
                    else { //residente
                      ind = enfCtrlPag.indexOf(cd_enf);
                      aux = (Long) res.elementAt(ind);
                      aux = new Long(aux.longValue() + 1);
                      res.setElementAt(aux, ind);
                    }
                  }
                  else { //no residente
                    ind = enfCtrlPag.indexOf(cd_enf);
                    aux = (Long) noRes.elementAt(ind);
                    aux = new Long(aux.longValue() + 1);
                    noRes.setElementAt(aux, ind);
                  }
                }

              } //Fin while
              rs.close();
              rs = null;
              st.close();
              st = null;

              // enfCtrlPag es la guia para el control de pagina del informe
              // para las siguiente consultas de atencion especializada y otros

              for (int j = 0; j < enfCtrlPag.size(); j++) {
                DataC936 dato = (DataC936) resultado.elementAt(j);
                dato.NM_OT_RES = (Long) res.elementAt(j);
                dato.NM_OT_NORES = (Long) noRes.elementAt(j);
                //E //# // System_out.println("SrvCasNivRes: OT " + dato.NM_AE_RES + " "
                //                              + dato.NM_AE_NORES );
                resultado.setElementAt(dato, j);
                //E //# // System_out.println("SrvCasNivRes: inserto en resultado " + j);
              }
            } //hay edos
            else {
              rellenarConCerosOT();
            }
          } //hay equipos
          else {
            rellenarConCerosOT();
          }
        } //hayEnfer
        else {
          data = null;
        }

        if (resultado.size() > 0) {
          data = new CLista();
          data.addElement(resultado);
          if (iEstado == CLista.listaINCOMPLETA) {
            data.addElement(ultE);
          }
          data.setState(iEstado);
          data.trimToSize();
        }

        break;

    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return data;
  }

  void rellenarConCerosAP() {
    //Cojo las enfermedades que caben en una pagina y
    //relleno los campos de atencion primaria con ceros
    //E //# // System_out.println ("SrvCasNivRes: en rellenarConCerosAP");
    int lineas = 1;
    iEstado = CLista.listaVACIA;
    cd_enf = new String();
    for (int j = 0; j < enfermedades.size(); j++) {
      ant_enf = cd_enf;
      cd_enf = (String) enfermedades.elementAt(j);
      if ( (parCons.numPagina > 0) &&
          (cd_enf.compareTo(parCons.ultEnf) <= 0)) {
        // se pasan las enfermedades con un codigo menor o igual a la
        // ultima que aparece en la pagina anterior
      }
      else {
        if ( (lineas > DBServlet.maxSIZE) && (! (parCons.bInformeCompleto))) {
          iEstado = CLista.listaINCOMPLETA;
          ultE = ant_enf;
          //E //# // System_out.println("SrvCasNivRes: ultEnf " + ultE );
          break;
        }
        // control de estado
        if (iEstado == CLista.listaVACIA) {
          iEstado = CLista.listaLLENA;

          //enfCtrlPag.insertElementAt(cd_enf,j);
        }
        enfCtrlPag.addElement(cd_enf);

        Long auxR = new Long(0);
        Long auxN = new Long(0);
        String auxDs = (String) procesos.elementAt(j);
        //E //# // System_out.println("SrvCasNivRes: AP para resultado " + auxDs + " "
        //                                                    + auxR + " "
        //                                                    + auxN + " ");
        DataC936 dato = new DataC936(auxDs, new Long(0), new Long(0));
        resultado.insertElementAt(dato, j);
        //E //# // System_out.println("SrvCasNivRes: inserto en resultado " + j);
        lineas++;
      }
      //E //# // System_out.println("SrvCasNivRes: despues de rellenar resultado de ceros en AP");
    }
  } //rellenarConCerosAP

  void rellenarConCerosAE() {
    // Cojo las enfermedades que caben en una pagina (seleccionadas
    // en atencion primaria) y relleno los campos de atencion
    // especializada con ceros

    for (int j = 0; j < enfCtrlPag.size(); j++) {
      DataC936 dato = (DataC936) resultado.elementAt(j);
      dato.NM_AE_RES = new Long(0);
      dato.NM_AE_NORES = new Long(0);
      //E //# // System_out.println("SrvCasNivRes: AE " + dato.NM_AE_RES + " "
      //                                       + dato.NM_AE_NORES );
      resultado.setElementAt(dato, j);
      //E //# // System_out.println("SrvCasNivRes: inserto en resultado " + j);
    }
    //E //# // System_out.println("SrvCasNivRes: despues de rellenar resultado de ceros en AE");
  } //rellenarConCerosAE

  void rellenarConCerosOT() {
    // Cojo las enfermedades que caben en una pagina (seleccionadas
    // en atencion primaria) y relleno los campos de otros
    // notificadores con ceros
    for (int j = 0; j < enfCtrlPag.size(); j++) {
      DataC936 dato = (DataC936) resultado.elementAt(j);
      dato.NM_OT_RES = new Long(0);
      dato.NM_OT_NORES = new Long(0);
      //E //# // System_out.println("SrvCasNivRes: OT " + dato.NM_OT_RES + " "
      //                                       + dato.NM_OT_NORES );
      resultado.setElementAt(dato, j);
      //E //# // System_out.println("SrvCasNivRes: inserto en resultado " + j);
    }
    //E //# // System_out.println("SrvCasNivRes: despues de rellenar resultado de ceros en OT");
  } //rellenarConCerosAE

}
