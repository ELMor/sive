//Title:        Seccion 9.3.6.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Distribucion de los casos de un area segun nivel asistencial y residencia

package distcasnivres;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class panInfCasNivRes
    extends EPanel
    implements WindowListener {
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 1;

  final String strLOGO_CCAA = "images/ccaa.gif";

  final String strSERVLET = "servlet/SrvCasNivRes";

  final int NIV_ASIS_RES = 2;

  //ventana del panel
  protected CDialog dialogo;

  // estructuras de datos
  protected Vector vResultado;
  protected CLista lista;

  // modo de operación
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  public parConsCasNivRes parCons;

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;
  CApp ap = null;

  public panInfCasNivRes(CApp a) {
    super(a);
    ap = a;

    try {
      res = ResourceBundle.getBundle("distcasnivres.Res" + a.getIdioma());
      stub = new StubSrvBD();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(ap.getCodeBase().toString() + "erw/Dist936.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;

    this.setTitle(res.getString("this.Title"));

    this.addWindowListener(this);

    this.setSize(800, 500);
    this.setResizable(false);

  }

  public void windowOpened(WindowEvent e) {
    //E //# // System_out.println("windowOpened");
  }

  public void windowClosing(WindowEvent e) {
    //E //# // System_out.println("windowClosing");
    this.dispose();
  }

  public void windowClosed(WindowEvent e) {
    //E //# // System_out.println("windowClosed");
  }

  public void windowIconified(WindowEvent e) {
    //E //# // System_out.println("windowIconified");
  }

  public void windowDeiconified(WindowEvent e) {
    //E //# // System_out.println("windowDeiconified");
  }

  public void windowActivated(WindowEvent e) {
    //E //# // System_out.println("windowActivated");
  }

  public void windowDeactivated(WindowEvent e) {
    //E //# // System_out.println("windowDeactivated");
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;

    //E //# // System_out.println(lista.getState());
    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {

        PrepararInforme();

        // obtiene los datos del servidor
        parCons.numPagina++;

        //E //# // System_out.println("C7: Mas datos pg=" + parCons.numPagina );

        param.addElement(parCons);
        param.setIdioma(app.getIdioma());
        param.trimToSize();
        stub.setUrl(new URL(ap.getURL() + strSERVLET));

        // ARG: SE introduce el login
        param.setLogin(app.getLogin());

        // ARG: Se introduce la lortad
        param.setLortad(app.getParameter("LORTAD"));

        lista = (CLista) stub.doPost(parCons.criterio, param);

        /*
                 SrvCasNivRes srv = new SrvCasNivRes();
                 srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                             "dba_edo",
                             "manager");
                 lista = srv.doDebug(parCons.criterio, param);
         */

        if (lista != null) {
          v = (Vector) lista.elementAt(0);
          if (lista.getState() == CLista.listaINCOMPLETA) {
            parCons.ultEnf = (String) lista.elementAt(1);
          }
          for (int j = 0; j < v.size(); j++) {
            vResultado.addElement(v.elementAt(j));

            // control de registros
          }
          if (vResultado.size() == 0) {
            msgBox = new CMessage(this.ap, CMessage.msgADVERTENCIA,
                                  res.getString("msg7.Text"));
            msgBox.show();
            msgBox = null;
            this.setLimpiarStatus();
          }
          else {
            if (lista.getState() == CLista.listaINCOMPLETA) {
              this.setTotalRegistros(res.getString("this.TotalRegistros"));
            }
            else {
              this.setTotalRegistros(res.getString("this1.TotalRegistros"));
            }
            this.setRegistrosMostrados( (new Integer(vResultado.size())).
                                       toString());
            // repintado
            erwClient.refreshReport(true);
          }
        }
        else {
          msgBox = new CMessage(this.ap, CMessage.msgADVERTENCIA,
                                res.getString("msg7.Text"));
          msgBox.show();
          msgBox = null;
          this.setLimpiarStatus();
        }

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.ap, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // informe comleto
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  public boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    CLista param = new CLista();

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      PrepararInforme();

      //E //# // System_out.println ("Vuelvo de preparar informe");

      // obtiene los datos del servidor
      parCons.numPagina = 0;
      parCons.bInformeCompleto = conTodos;
      //E //# // System_out.println("En panel informe:");
      parCons.imprimir();
      param.addElement(parCons);
      param.setIdioma(app.getIdioma());
      param.trimToSize();
      stub.setUrl(new URL(ap.getURL() + strSERVLET));
      //E //# // System_out.println ("Voy al servlet de la primera pagina");

      // ARG: SE introduce el login
      param.setLogin(app.getLogin());

      // ARG: Se introduce la lortad
      param.setLortad(app.getParameter("LORTAD"));

      lista = (CLista) stub.doPost(parCons.criterio, param);

      /*        SrvCasNivRes srv = new SrvCasNivRes();
              srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
           "jdbc:oracle:thin:@10.160.5.149:1521:ORCL",
                                     "pista",
                                     "loteb98");
              lista = srv.doDebug(parCons.criterio, param);
       */

      //E //# // System_out.println ("Vuelvo del servlet de la primera pagina");
      if (lista != null) {
        vResultado = (Vector) lista.elementAt(0);
        if (lista.getState() == CLista.listaINCOMPLETA) {
          parCons.ultEnf = (String) lista.elementAt(1);
        }
        // control de registros

        if (vResultado.size() == 0) {
          msgBox = new CMessage(this.ap, CMessage.msgADVERTENCIA,
                                res.getString("msg7.Text"));
          msgBox.show();
          msgBox = null;
          this.setLimpiarStatus();

          modoOperacion = modoNORMAL;
          Inicializar();
          return false;
        }
        else {
          if (lista.getState() == CLista.listaINCOMPLETA) {
            this.setTotalRegistros(res.getString("this.TotalRegistros"));
          }
          else {
            this.setTotalRegistros(res.getString("this1.TotalRegistros"));
          }
          this.setRegistrosMostrados( (new Integer(vResultado.size())).toString());
          // establece las matrices de datos
          Vector retval = new Vector();
          retval.addElement("DS_ENFERMEDAD = DS_ENFERMEDAD");
          retval.addElement("NM_AP_RES = NM_AP_RES");
          retval.addElement("NM_AP_NORES = NM_AP_NORES");
          retval.addElement("NM_AE_RES = NM_AE_RES");
          retval.addElement("NM_AE_NORES = NM_AE_NORES");
          retval.addElement("NM_OT_RES = NM_OT_RES");
          retval.addElement("NM_OT_NORES = NM_OT_NORES");
          dataHandler.RegisterTable(vResultado, "SIVE_C936", retval, null);
          erwClient.setInputProperties(erw.GetTemplateManager(),
                                       erw.getDATReader(true));

          // repintado
          //AIC
          this.pack();
          erwClient.prv_setActivePage(0);
          //erwClient.refreshReport(true);

          modoOperacion = modoNORMAL;
          Inicializar();
          return true;
        }
      }
      else {
        msgBox = new CMessage(this.ap, CMessage.msgADVERTENCIA,
                              res.getString("msg7.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return false;

      }
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.ap, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();

      modoOperacion = modoNORMAL;
      Inicializar();
      return false;
    }

  }

  public void refrescar() {
    // repintado
    erwClient.refreshReport(true);
  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;

    // plantilla
    TemplateManager tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000", ap.getCodeBase().toString() + strLOGO_CCAA);
    tm.SetImageURL("IMA001", ap.getCodeBase().toString() + strLOGO_CCAA);

    // carga los citerios

    tm.SetLabel("CRITERIO1",
                res.getString("msg8.Text") + parCons.anoDesde +
                res.getString("msg9.Text") + parCons.semDesde
                + res.getString("msg10.Text") + parCons.anoHasta +
                res.getString("msg11.Text") + parCons.semHasta);

    if (!parCons.nombreArea.equals("")) {
      tm.SetLabel("CRITERIO2",
                  res.getString("msg12.Text") + parCons.area + " " +
                  parCons.nombreArea);
      if (!parCons.nombreDistrito.equals("")) {
        tm.SetLabel("CRITERIO2",
                    tm.GetLabel("CRITERIO2") + res.getString("msg13.Text") +
                    parCons.nombreDistrito);
      }
    }
    else {
      tm.SetLabel("CRITERIO2", "");
    }
  }

  public void MostrarGrafica() {}

}
