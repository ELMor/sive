//Title:        Seccion 9.3.6.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Distribucion de los casos de un area segun nivel asistencial y residencia

package distcasnivres;

//import eqNot.*;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
import utilidades.PanFechas;
import zbs.DataZBS2;

public class panConsCasNivRes
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 1;

  //Modos de operacion del servlet
  final int NIV_ASIS_RES = 2;

  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/vaciar.gif",
      "images/grafico.gif"};

  protected CCargadorImagen imgs = null;

  protected StubSrvBD stubCliente = new StubSrvBD();

  public parConsCasNivRes param;
  protected panInfCasNivRes informe = null;
  protected CDialog dialogo = null;

  protected int modoOperacion = modoNORMAL;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  XYLayout xYLayout = new XYLayout();

  Label lblDesde = new Label();
  Label lblArea = new Label();
  CCampoCodigo txtCodArea = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarArea = new ButtonControl();
  TextField txtDesArea = new TextField();
  PanFechas panelDesde;
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  focusAdapter txtFocusAdapter = new focusAdapter(this);
  txt_keyAdapter txtTextAdapter = new txt_keyAdapter(this);
  //textAdapter txtTextAdapter = new textAdapter(this);
  actionListener btnActionListener = new actionListener(this);
  CheckboxGroup checkboxAgrupar = new CheckboxGroup();
  Label lblDist = new Label();
  CCampoCodigo txtCodDist = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarDist = new ButtonControl();
  TextField txtDesDist = new TextField();
  Label lblHasta = new Label();
  PanFechas panelHasta;

  public panConsCasNivRes(CApp a) {
    try {
      setApp(a);
      res = ResourceBundle.getBundle("distcasnivres.Res" + a.getIdioma());
      panelDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      panelHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      informe = new panInfCasNivRes(a);
      jbInit();
      this.txtCodArea.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesArea.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtCodDist.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesDist.setText(this.app.getDS_NIVEL2_DEFECTO());
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(301);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));
    lblDist.setText(res.getString("lblDist.Text"));
    //txtCodDist.addTextListener(txtTextAdapter);
    txtCodDist.addKeyListener(txtTextAdapter);
    txtCodDist.addFocusListener(txtFocusAdapter);
    txtCodDist.setName("txtCodDist");
    txtCodDist.setBackground(Color.white);
    btnCtrlBuscarDist.setActionCommand("buscarDist");
    txtDesDist.setEditable(false);
    txtDesDist.setEnabled(false); /*E*/

    // gestores de eventos

    btnCtrlBuscarArea.addActionListener(btnActionListener);
    btnCtrlBuscarDist.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    //txtCodArea.addTextListener(txtTextAdapter);
    txtCodArea.addKeyListener(txtTextAdapter);
    txtCodArea.addFocusListener(txtFocusAdapter);

    //btnCtrlBuscarEnfer.setLabel("btnCtrlBuscarPro1");
    btnCtrlBuscarArea.setActionCommand("buscarArea");
    txtDesArea.setEditable(false);
    txtDesArea.setEnabled(false); /*E*/
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    txtCodArea.setName("txtCodArea");
    txtCodArea.setBackground(new Color(255, 255, 150));

    lblArea.setText(res.getString("lblArea.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));

    xYLayout.setWidth(596);

    this.setLayout(xYLayout);

    this.add(lblDesde, new XYConstraints(26, 42, 51, -1));
    this.add(panelDesde, new XYConstraints(40, 42, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 82, 51, -1));
    this.add(panelHasta, new XYConstraints(40, 82, -1, -1));
    this.add(lblArea, new XYConstraints(26, 122, 101, -1));
    this.add(txtCodArea, new XYConstraints(143, 122, 77, -1));
    this.add(btnCtrlBuscarArea, new XYConstraints(225, 122, -1, -1));
    this.add(txtDesArea, new XYConstraints(254, 122, 287, -1));
    this.add(lblDist, new XYConstraints(26, 162, 101, -1));
    this.add(txtCodDist, new XYConstraints(143, 162, 77, -1));
    this.add(btnCtrlBuscarDist, new XYConstraints(225, 162, -1, -1));
    this.add(txtDesDist, new XYConstraints(254, 162, 287, -1));
    this.add(btnLimpiar, new XYConstraints(389, 202, -1, -1));
    this.add(btnInforme, new XYConstraints(475, 202, -1, -1));

    imgs = new CCargadorImagen(app, imgNAME);
    lblHasta.setText(res.getString("lblHasta.Text"));
    imgs.CargaImagenes();
    btnCtrlBuscarArea.setImage(imgs.getImage(0));
    btnCtrlBuscarDist.setImage(imgs.getImage(0));
    btnLimpiar.setImage(imgs.getImage(1));
    btnInforme.setImage(imgs.getImage(2));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (panelDesde.txtAno.getText().length() == 0)) {
      bDatosCompletos = false;
    }
    if ( (panelDesde.txtCodSem.getText().length() == 0)) {
      bDatosCompletos = false;
    }
    if ( (panelHasta.txtAno.getText().length() == 0)) {
      bDatosCompletos = false;
    }
    if ( (panelHasta.txtCodSem.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    if ( (txtDesArea.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtCodArea.setEnabled(true);
        //txtDesArea.setEnabled(true); /*E*/
        btnCtrlBuscarArea.setEnabled(true);
        //txtCodDist.setEnabled(true);
        //txtDesDist.setEnabled(true);
        //btnCtrlBuscarDist.setEnabled(true);
        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        btnCtrlBuscarArea.setEnabled(true);
        // control area
        if (!txtDesArea.getText().equals("")) {
          btnCtrlBuscarDist.setEnabled(true);
          txtCodDist.setEnabled(true);
          //txtDesDist.setEnabled(true); /*E*/
        }
        else {
          btnCtrlBuscarDist.setEnabled(false);
          txtCodDist.setEnabled(false);
          //txtDesDist.setEnabled(false); /*E*/
          txtCodDist.setText("");
          txtDesDist.setText("");
        }

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCodArea.setEnabled(false);
        btnCtrlBuscarArea.setEnabled(false);
        txtCodDist.setEnabled(false);
        btnCtrlBuscarDist.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //Busca los niveles1 (area de salud)
  void btnCtrlbuscarArea_actionPerformed(ActionEvent evt) {
    DataNivel1 data = null;
    CMessage mensaje = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaNivel1 lista = new CListaNivel1(app,
                                            res.getString("msg3.Text") +
                                            this.app.getNivel1(),
                                            stubCliente,
                                            strSERVLETNivel1,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }

    if (data != null) {
      txtCodArea.removeKeyListener(txtTextAdapter);
      txtCodArea.setText(data.getCod());
      txtDesArea.setText(data.getDes());
      txtCodDist.setText("");
      txtDesDist.setText("");
      txtCodArea.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //Busca los niveles2 (distritos)
  //busca el distrito (Niv2)
  void btnCtrlbuscarDist_actionPerformed(ActionEvent evt) {

    DataZBS2 data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg3.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLETNivel2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodDist.removeKeyListener(txtTextAdapter);
      txtCodDist.setText(data.getNiv2());
      txtDesDist.setText(data.getDes());
      txtCodDist.addKeyListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    txtCodArea.setText("");
    txtDesArea.setText("");
    txtCodDist.setText("");
    txtDesDist.setText("");

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    param = new parConsCasNivRes();
    CLista par = null;
    CLista resul = null;

    if (isDataValid()) {
      if (!txtDesArea.getText().equals("")) {
        param.area = txtCodArea.getText();
        param.nombreArea = txtDesArea.getText();
      }

      if (!txtDesDist.getText().equals("")) {
        param.distrito = txtCodDist.getText();
        param.nombreDistrito = txtDesDist.getText();
      }

      param.anoDesde = panelDesde.txtAno.getText();
      if (panelDesde.txtCodSem.getText().length() == 1) {
        param.semDesde = "0" + panelDesde.txtCodSem.getText();
      }
      else {
        param.semDesde = panelDesde.txtCodSem.getText();

      }
      param.anoHasta = panelHasta.txtAno.getText();
      if (panelHasta.txtCodSem.getText().length() == 1) {
        param.semHasta = "0" + panelHasta.txtCodSem.getText();
      }
      else {
        param.semHasta = panelHasta.txtCodSem.getText();

      }
      param.criterio = NIV_ASIS_RES;
      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();

      informe.setEnabled(true);

      informe.parCons = param;

      boolean hay = informe.GenerarInforme();
      if (hay) {
        informe.show();

      }

      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg4.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    // datos de envio
    DataNivel1 nivel1;
    DataZBS2 nivel2;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtCodArea")) &&
        (txtCodArea.getText().length() > 0)) {

      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataNivel1(txtCodArea.getText()));
      strServlet = strSERVLETNivel1;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodDist")) &&
             (txtCodDist.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS2(txtCodArea.getText(), txtCodDist.getText(),
                                    "", ""));
      strServlet = strSERVLETNivel2;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }

    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtCodArea")) {

            nivel1 = (DataNivel1) param.firstElement();
            txtCodArea.removeKeyListener(txtTextAdapter);
            txtCodArea.setText(nivel1.getCod());
            txtDesArea.setText(nivel1.getDes());
            txtCodArea.addKeyListener(txtTextAdapter);
          }
          else if (txt.getName().equals("txtCodDist")) {

            nivel2 = (DataZBS2) param.firstElement();
            txtCodDist.removeKeyListener(txtTextAdapter);
            txtCodDist.setText(nivel2.getNiv2());
            txtDesDist.setText(nivel2.getDes());
            txtCodDist.addKeyListener(txtTextAdapter);
          }
        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg6.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  void imprimeVector(Vector v) {
    Vector aux = new Vector();

    for (int s = 0; s < v.size(); s++) {
      try {
        aux = (Vector) v.elementAt(s);
        imprimeVector(aux);

      }
      catch (ClassCastException ex) {

      }
    }

  }

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtCodArea")) &&
        (txtDesArea.getText().length() > 0)) {
      //txtCodAre.setText("");
      txtCodDist.setText("");
      txtDesArea.setText("");
      txtDesDist.setText("");
    }
    else if ( (txt.getName().equals("txtCodDist")) &&
             (txtDesDist.getText().length() > 0)) {
      //txtCodDis.setText("");
      txtDesDist.setText("");
    }
    Inicializar();
  }

} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  panConsCasNivRes adaptee = null;
  ActionEvent e = null;

  public actionListener(panConsCasNivRes adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarArea")) {
      adaptee.btnCtrlbuscarArea_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarDist")) {
      adaptee.btnCtrlbuscarDist_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  panConsCasNivRes adaptee;
  FocusEvent event;

  focusAdapter(panConsCasNivRes adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

// cambio en una caja de codigos
class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  panConsCasNivRes adaptee;

  txt_keyAdapter(panConsCasNivRes adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}

////////////////////// Clases para listas

// lista de valores
class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

class CListaZBS2
    extends CListaValores {

  protected panConsCasNivRes panel;

  public CListaZBS2(panConsCasNivRes p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtCodArea.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}
