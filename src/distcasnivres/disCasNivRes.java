//Title:        Seccion 9.3.6.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Distribucion de los casos de un area segun nivel asistencial y residencia

package distcasnivres;

import java.util.ResourceBundle;

import capp.CApp;

public class disCasNivRes
    extends CApp {

  ResourceBundle res;

  public disCasNivRes() {
  }

  public void init() {
    super.init();
  }

  public void start() {

    res = ResourceBundle.getBundle("distcasnivres.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;

    panConsCasNivRes panelCons = new panConsCasNivRes(a);
    VerPanel("", panelCons);

  }

}
