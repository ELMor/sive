//Title:        Seccion 9.3.6.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Distribucion de los casos de un area segun nivel asistencial y residencia

package distcasnivres;

import java.io.Serializable;

public class parConsCasNivRes
    implements Serializable {

  public String anoDesde = new String();
  public String anoHasta = new String();
  public String semDesde = new String();
  public String semHasta = new String();
  public String area = "";
  public String nombreArea = ""; //E
  public String distrito = "";
  public String nombreDistrito = ""; //E
  public int criterio = 0;

  // Para las paginas del informe
  public int numPagina = 0;
  public String ultEnf = new String();
  public boolean bInformeCompleto = false;

  public parConsCasNivRes() {
  }

  public parConsCasNivRes(String aD, String aH,
                          String sD, String sH,
                          String ar, String d) {
    anoDesde = aD;
    anoHasta = aH;
    semDesde = sD;
    semHasta = sH;
    area = ar;
    distrito = d;
  }

  public void imprimir() {
    //E //# // System_out.println("parConsCasNivRes: " + anoDesde + " "
    //                                        + semDesde + " "
    //                                        + anoHasta + " "
    //                                        + semHasta + " "
    //                                        + area + " "
    //                                        + distrito + " "
    //                                        + criterio);
  }
}
