//Title:        Seccion 9.3.6.
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Distribucion de los casos de un area segun nivel asistencial y residencia

package distcasnivres;

import java.io.Serializable;

public class DataC936
    implements Serializable {

  /*
    DS_ENFERMEDAD
   NM_AP_RES
   NM_AP_NORES
   NM_AE_RES
   NM_AE_NORES
   NM_OT_RES
   NM_OT_NORES
   */

  public String DS_ENFERMEDAD = new String();
  public Long NM_AP_RES = null;
  public Long NM_AP_NORES = null;
  public Long NM_AE_RES = null;
  public Long NM_AE_NORES = null;
  public Long NM_OT_RES = null;
  public Long NM_OT_NORES = null;

  public DataC936() {
  }

  public DataC936(String ds, Long apR, Long apN,
                  Long aeR, Long aeN,
                  Long otR, Long otN) {
    DS_ENFERMEDAD = ds;
    NM_AP_RES = apR;
    NM_AP_NORES = apN;
    NM_AE_RES = aeR;
    NM_AE_NORES = aeN;
    NM_OT_RES = otR;
    NM_OT_NORES = otN;
  }

  public DataC936(String ds, Long apR, Long apN) {
    DS_ENFERMEDAD = ds;
    NM_AP_RES = apR;
    NM_AP_NORES = apN;
  }

}
