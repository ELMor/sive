package comun;

import java.io.Serializable;

public class DataEqNot
    implements Serializable {
  protected String CodEqNot = "";
  protected String DS_EqNot = "";
  protected String CodCN = "";
  protected String DesCN = "";
  protected String CodNiv1 = "";
  protected String CodNiv2 = "";
  protected String CodSemEpi = "";
  protected String CodAnoEpi = "";
  protected String CodZBS = "";
  protected String DS_RespEquip = "";
  protected String telef = "";
  protected String fax = "";
  protected int nm_notift = 0;
  protected String CodOpe = "";
  protected String it_baja = "";

  public boolean bInformeCompleto = false;

  protected boolean Cobertura = false;

  public DataEqNot(String EquNot) {
    CodEqNot = EquNot;
  }

  public DataEqNot(String EquNot, String dseqnot, String codCen, String niv1,
                   String niv2,
                   String sem, String anyo, String zona, String responsable,
                   String tlfno, String nfax,
                   int notift, String loginOp, String itBaja, boolean cobertura) {

    CodEqNot = EquNot;
    DS_EqNot = dseqnot;
    CodCN = codCen;
    CodNiv1 = niv1;
    CodNiv2 = niv2;
    CodSemEpi = sem;
    CodAnoEpi = anyo;
    CodZBS = zona;
    DS_RespEquip = responsable;
    telef = tlfno;
    fax = nfax;
    nm_notift = notift;
    CodOpe = loginOp;
    it_baja = itBaja;
    Cobertura = cobertura;
  }

  public String getEquipo() {
    return CodEqNot;
  }

  public String getDesEquipo() {
    return DS_EqNot;
  }

  public String getCentro() {
    return CodCN;
  }

  public String getDesCentro() {
    return DesCN;
  }

  public void setDesCentro(String a_desCentro) {
    DesCN = a_desCentro;
  }

  public void setCobertura(boolean c) {
    Cobertura = c;
  }

  public String getNiv1() {
    return CodNiv1;
  }

  public String getNiv2() {
    return CodNiv2;
  }

  public String getSemana() {
    return CodSemEpi;
  }

  public String getAnyo() {
    return CodAnoEpi;
  }

  public String getZBS() {
    return CodZBS;
  }

  public String getResponsable() {
    return DS_RespEquip;
  }

  public String getTelefono() {
    return telef;
  }

  public String getFax() {
    return fax;
  }

  public int getNotift() {
    return nm_notift;
  }

  public String getCodOpe() {
    return CodOpe;
  }

  public String getBaja() {
    return it_baja;
  }

  public boolean estaEnCobertura() {
    return Cobertura;
  }
}
