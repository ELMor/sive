//viene del paquete de notadic de EDO
package comun;

import java.io.Serializable;

public class datosPreg
    implements Serializable {
  public int linea = 0;
  public String pregunta = "";
  public String respuesta = "";
  public String modelo = "";
  public String ValorLista = null;
  public String nivel = "";
  public String it_ok = "";
  public String ca = "";
  public String n1 = "";
  public String n2 = "";

  public datosPreg(int l, String p, String r, String mod,
                   String Nivel, String Activo,
                   String CA, String N1, String N2) {
    linea = l;
    pregunta = p;
    respuesta = r;
    modelo = mod;
    nivel = Nivel;
    it_ok = Activo;
    ca = CA;
    n1 = N1;
    n2 = N2;
  }

  public datosPreg(int l, String p, String r,
                   String mod, String valor,
                   String Nivel, String Activo,
                   String CA, String N1, String N2) {
    linea = l;
    pregunta = p;
    respuesta = r;
    modelo = mod;
    ValorLista = valor;
    nivel = Nivel;
    it_ok = Activo;
    ca = CA;
    n1 = N1;
    n2 = N2;
  }

}
