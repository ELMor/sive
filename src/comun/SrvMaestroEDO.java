// Modificado el 29/11/1999 para recuperar, en el modo 4
// (OBTENER_X_CODIGO), el campo CD_ZBS del equipo notificador.

package comun;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import capp.CLista;
import sapp.DBServlet;

//import notdata.*;

public class SrvMaestroEDO
    extends DBServlet {

  // modos de operaci�n del servlet
  //final int servletOBTENER_FLAGS_USU = 3;

  final int servletOBTENER_EQUIPO_X_CODIGO = 4;
  final int servletOBTENER_EQUIPO_X_DESCRIPCION = 5;
  final int servletSELECCION_EQUIPO_X_CODIGO = 6;
  final int servletSELECCION_EQUIPO_X_DESCRIPCION = 7;

  final int servletOBTENER_EQUIPO = 9;

  public SimpleDateFormat Format_Horas = new SimpleDateFormat(
      "dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));

  // par�metros
  DataEntradaEDO dataEDO = null;
  DataEntradaEDO dataLinea = null;

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // Configuraci�n
    String sQuery = "", sFiltro = "";
    String sfc_ultact = "";
    java.util.Date dFecha;
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int iValor = 1;

    // objetos de datos
    CLista data = new CLista();
    DataEntradaEDO ent = null;
    DataNotifEDO notif = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {

      switch (opmode) {

        case servletOBTENER_EQUIPO:
          ent = (DataEntradaEDO) param.firstElement();
          sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
              " CD_CENTRO, CD_NIVEL_2, NM_NOTIFT,  " +
              " CD_OPE, FC_ULTACT from SIVE_E_NOTIF " +
              " where CD_E_NOTIF = ?";

          st = con.prepareStatement(sQuery);

          st.setString(1, ent.getCod().trim());

          break;

        case servletOBTENER_EQUIPO_X_CODIGO:
        case servletOBTENER_EQUIPO_X_DESCRIPCION:

          ent = (DataEntradaEDO) param.firstElement();

          // el nivel1 siempre va a venir relleno, porque incluso
          // al usuario de CA se le obliga a introducir nivel1
          if ( (ent.getNivel2().equals("")) ||
              (ent.getNivel2() == null)) {
            sFiltro = "";
          }
          else {
            sFiltro = " and CD_NIVEL_2 = ?";

            // query
          }
          if (opmode == servletOBTENER_EQUIPO_X_CODIGO) {

            sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                " CD_CENTRO, CD_NIVEL_2, CD_ZBS, NM_NOTIFT,  " +
                " CD_OPE, FC_ULTACT from SIVE_E_NOTIF " +
                " where CD_E_NOTIF = ?";

          }
          else {

            sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                " CD_CENTRO, CD_NIVEL_2, CD_ZBS, NM_NOTIFT,  " +
                " CD_OPE, FC_ULTACT from SIVE_E_NOTIF " +
                " where DS_E_NOTIF = ?";

          }
          if (sFiltro.length() > 0) {
            sQuery = sQuery + sFiltro;

          }
          sQuery = sQuery +
              " and CD_NIVEL_1 = ? and IT_BAJA = ? order by CD_E_NOTIF";

          //# System_Out.println("GUI sQuery "+sQuery);

          st = con.prepareStatement(sQuery);

          // filtro normal
          st.setString(iValor, ent.getCod().trim());
          iValor++;

          // filtro de nivel2
          if (sFiltro.length() > 0) {
            st.setString(iValor, ent.getNivel2().trim());
            iValor++;
          }

          st.setString(iValor, ent.getNivel1().trim());
          iValor++;

          st.setString(iValor, "N");
          iValor++;

          break;

          //TRAMAR ****
        case servletSELECCION_EQUIPO_X_CODIGO:
        case servletSELECCION_EQUIPO_X_DESCRIPCION:

          ent = (DataEntradaEDO) param.firstElement();

          // el nivel1 siempre va a venir relleno, porque incluso
          // al usuario de CA se le obliga a introducir nivel1
          if ( (ent.getNivel2().equals("")) || (ent.getNivel2() == null)) {
            sFiltro = "";
          }
          else {
            sFiltro = " and CD_NIVEL_2 = ?";

            // query

            //tramar
          }
          if (param.getFilter().length() > 0) {

            if (opmode == servletSELECCION_EQUIPO_X_CODIGO) {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, CD_ZBS, NM_NOTIFT,  " +
                  " CD_OPE, FC_ULTACT from SIVE_E_NOTIF " +
                  " where CD_E_NOTIF like ? and CD_E_NOTIF > ? ";

            }
            else {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, CD_ZBS, NM_NOTIFT,  " +
                  " CD_OPE, FC_ULTACT from SIVE_E_NOTIF " +
                  " where DS_E_NOTIF like ? and DS_E_NOTIF > ? ";
            }
          }
          else {
            if (opmode == servletSELECCION_EQUIPO_X_CODIGO) {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, CD_ZBS, NM_NOTIFT,  " +
                  " CD_OPE, FC_ULTACT from SIVE_E_NOTIF " +
                  " where CD_E_NOTIF like ? ";

            }
            else {
              sQuery = "select CD_E_NOTIF, DS_E_NOTIF, " +
                  " CD_CENTRO, CD_NIVEL_2, CD_ZBS, NM_NOTIFT,  " +
                  " CD_OPE, FC_ULTACT from SIVE_E_NOTIF " +
                  " where DS_E_NOTIF like ? ";

            }

          }

          if (sFiltro.length() > 0) {
            sQuery = sQuery + sFiltro;

          }
          sQuery = sQuery + " and CD_NIVEL_1 = ? and IT_BAJA = ? ";

          //order by
          if (opmode == servletSELECCION_EQUIPO_X_CODIGO) {
            sQuery = sQuery + " order by CD_E_NOTIF";
          }
          else {
            sQuery = sQuery + " order by DS_E_NOTIF";

            //pregunta
          }
          st = con.prepareStatement(sQuery);

          // filtro normal
          st.setString(iValor, ent.getCod().trim() + "%");
          iValor++;

          //trama
          if (param.getFilter().length() > 0) {
            st.setString(iValor, param.getFilter().trim());
            iValor++;
          }

          // filtro de nivel2
          if (sFiltro.length() > 0) {
            st.setString(iValor, ent.getNivel2().trim());
            iValor++;
          }

          st.setString(iValor, ent.getNivel1().trim());
          iValor++;

          st.setString(iValor, "N");
          iValor++;

          break;

      }

//resto de la querys para equipos***************************
      if ( (opmode == servletSELECCION_EQUIPO_X_CODIGO) ||
          (opmode == servletSELECCION_EQUIPO_X_DESCRIPCION) ||
          (opmode == servletOBTENER_EQUIPO_X_CODIGO) ||
          (opmode == servletOBTENER_EQUIPO_X_DESCRIPCION) ||
          (opmode == servletOBTENER_EQUIPO)) {

        rs = st.executeQuery();

        java.sql.Timestamp fecRecu = null;
        String sfecRecu = "";
        while (rs.next()) {
          //# System_Out.println("GUI dentro de while");
          //tramar ---
          if ( (opmode == servletSELECCION_EQUIPO_X_CODIGO) ||
              (opmode == servletSELECCION_EQUIPO_X_DESCRIPCION)) {

            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);

              if (opmode == servletSELECCION_EQUIPO_X_CODIGO) {
                data.setFilter( ( (DataEntradaEDO) data.lastElement()).getCod());
              }
              else {
                data.setFilter( ( (DataEntradaEDO) data.lastElement()).getDes());

              }
              break;
            }

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
          } //tramar ------

          //NM_NOTIFT puede ser nulo, ptt lo recuperamos con getString
          //NOTA: NM_NOTIFT ocupa el lugar de cod_nivel_1 en la estructura de datos!!!
          //NOTA: 28/04 -> lo sacamos de notif_sem

          /*sqlFec = rs.getDate("FC_ULTACT");
                   sfc_ultact = formater.format(sqlFec);*/
          fecRecu = rs.getTimestamp("FC_ULTACT");
          sfecRecu = timestamp_a_cadena(fecRecu);

          dataEDO = new DataEntradaEDO(
              rs.getString("CD_E_NOTIF"),
              rs.getString("DS_E_NOTIF"),
              "", //rs.getString ("NM_NOTIFT"), //en nivel 1 va nm_notift
              rs.getString("CD_NIVEL_2"),
              rs.getString("CD_CENTRO"),
              "", //ds_centro
              "", //it_cobertura
              null, //nm_ntotreal
              rs.getString("CD_OPE"), //datos del equipo
              sfecRecu);

          // Modificaci�n
          if (opmode == servletOBTENER_EQUIPO_X_CODIGO ||
              opmode == servletSELECCION_EQUIPO_X_CODIGO) {
            dataEDO.setZBS(rs.getString("CD_ZBS"));
            dataEDO.setNM_NOTIFT(rs.getString("NM_NOTIFT"));
          }

          // a�ade un nodo
          data.addElement(dataEDO);
          i++;
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        for (int j = 0; j < data.size(); j++) {
          dataLinea = (DataEntradaEDO) data.elementAt(j);

          //CENTRO***************************************************************
          sQuery = "select CD_CENTRO, DS_CENTRO, IT_COBERTURA "
              + " from SIVE_C_NOTIF where CD_CENTRO = ?";

          st = con.prepareStatement(sQuery);
          st.setString(1, dataLinea.getCodCentro());
          rs = st.executeQuery();

          while (rs.next()) {

            // obtiene el resto de campos para cada elemento de data
            dataLinea.sCodCentro = rs.getString("CD_CENTRO");
            dataLinea.sDesCentro = rs.getString("DS_CENTRO");
            dataLinea.sDato7 = rs.getString("IT_COBERTURA");

          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //# System_Out.println("GUI detras de centro");

          if (opmode != servletOBTENER_EQUIPO) {
            sQuery = "select NM_NTOTREAL, NM_NNOTIFT " +
                " from SIVE_NOTIF_SEM " +
                " where (CD_E_NOTIF = ? and " +
                " CD_ANOEPI = ? and " +
                " CD_SEMEPI = ? )";

            st = con.prepareStatement(sQuery);
            st.setString(1, dataLinea.getCod());
            st.setString(2, ent.getAno());
            st.setString(3, ent.getSem());

            rs = st.executeQuery();

            dataLinea.sDato8 = null;
            while (rs.next()) {
              dataLinea.sDato8 = rs.getString("NM_NTOTREAL"); //con getString, puede ser null
              dataLinea.sNivel1 = rs.getString("NM_NNOTIFT"); //con getString, puede ser null
            }
            //si la clave no esta en notif_sem, devolvemos nulo en este campo
            rs.close();
            rs = null;
            st.close();
            st = null;

          } //fin ifpegote

        } //for

      } //fin del if de equipos

      //# System_Out.println("GUI detras de saltar pegote");
      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      data = null;
      throw ex;

    }

    // cierra la conexi�n y acaba el procedimiento doWork
    closeConnection(con);
    data.trimToSize();

    return data;
  }

}