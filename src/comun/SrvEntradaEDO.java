//viene del paquete notservlets de EDO
package comun;

//
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvEntradaEDO
    extends DBServlet {

  // modos de operaci�n del servlet
  final int servletOBTENER_NIV1_X_CODIGO = 3;
  final int servletOBTENER_NIV1_X_DESCRIPCION = 4;
  final int servletSELECCION_NIV1_X_CODIGO = 5;
  final int servletSELECCION_NIV1_X_DESCRIPCION = 6;

  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;

  final int servletSELECCION_ZBS_X_CODIGO = 11;
  final int servletOBTENER_ZBS_X_CODIGO = 12;
  final int servletSELECCION_ZBS_X_DESCRIPCION = 13;
  final int servletOBTENER_ZBS_X_DESCRIPCION = 14;

  final int servletOBTENER_NIV1_X_CODIGO_TODAS_AUTORIZ = 19;
  final int servletOBTENER_NIV1_X_DESCRIPCION_TODAS_AUTORIZ = 20;
  final int servletSELECCION_NIV1_X_CODIGO_TODAS_AUTORIZ = 21;
  final int servletSELECCION_NIV1_X_DESCRIPCION_TODAS_AUTORIZ = 22;
  final int servletSELECCION_NIV2_X_CODIGO_TODAS_AUTORIZ = 23;
  final int servletOBTENER_NIV2_X_CODIGO_TODAS_AUTORIZ = 24;
  final int servletSELECCION_NIV2_X_DESCRIPCION_TODAS_AUTORIZ = 25;
  final int servletOBTENER_NIV2_X_DESCRIPCION_TODAS_AUTORIZ = 26;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int iValor = 1;

    // objetos de datos
    CLista data = new CLista();
    DataEntradaEDO nivel = null;

    // Configuraci�n
    String sCampoCod = "CODIGO", sCampoDes = "DESC", sCampoDesL = "DESC_LOCAL";
    String sQuery = "", sAutorizacion1 = "", sAutorizacion2 = "";
    String sCod, sDes, sDesL;

    nivel = (DataEntradaEDO) param.firstElement();

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    /*mlm:
      lo que habia:
      sAutorizacion1 = " and CD_NIVEL_1 IN (select CD_NIVEL_1 from SIVE_AUTORIZACIONES where CD_USUARIO = ?)";
      sAutorizacion2 = " and CD_NIVEL_2 IN (select CD_NIVEL_2 from SIVE_AUTORIZACIONES where CD_USUARIO = ? and CD_NIVEL_1 = ?)";
      nuevo:
      1,1,2
      2,3,1
      para sAutorizaciones2 debe salir solo 2,3 si nivel_1 era 1 */
    Vector vN1 = (Vector) param.getVN1();
    Vector vN2 = (Vector) param.getVN2();
    String n1 = "";
    String n1Fijo = "";
    int k = 0;
    // 05/04/2000 (JMT) introducci�n de las comillas para el perfecto funcionamiento de la query
    for (k = 0; k < vN1.size(); k++) {
      n1 = n1 + ",'" + (String) vN1.elementAt(k) + "'";
    }
    String n2 = "";
    // 05/04/2000 (JMT) introducci�n de las comillas para el perfecto funcionamiento de la query
    for (k = 0; k < vN2.size(); k++) {
      if (nivel.getNivel1() != null) {
        n1Fijo = nivel.getNivel1().trim();
        //quitar lo que sobra-si el correspondiente 1 es igual al fijo, ok)
      }
      if ( ( (String) vN1.elementAt(k)).equals(n1Fijo)) {
        n2 = n2 + ",'" + (String) vN2.elementAt(k) + "'";
      }
    }
    //quitamos las comas primeras
    if (!n1.trim().equals("")) {
      n1 = n1.substring(1);
    }
    if (!n2.trim().equals("")) {
      n2 = n2.substring(1);
      //------------------------------------------------

      // autorizaciones (s�lo para x_codigo y x_descripcion)
      // autorizaciones (s�lo para x_codigo y x_descripcion)
    }
    switch (opmode) {

      case servletOBTENER_NIV1_X_CODIGO:
      case servletOBTENER_NIV1_X_DESCRIPCION:
      case servletSELECCION_NIV1_X_CODIGO:
      case servletSELECCION_NIV1_X_DESCRIPCION:
      case servletSELECCION_NIV2_X_CODIGO:
      case servletOBTENER_NIV2_X_CODIGO:
      case servletSELECCION_NIV2_X_DESCRIPCION:
      case servletOBTENER_NIV2_X_DESCRIPCION:

        switch (param.getPerfil()) {
          case CApp.perfilCA:
            sAutorizacion1 = "";
            sAutorizacion2 = "";
            break;

          case CApp.perfilNIVEL1:
            sAutorizacion1 = " and CD_NIVEL_1 IN ( " + n1 + " )";
            sAutorizacion2 = "";
            break;

          case CApp.perfilNIVEL2:
            sAutorizacion1 = " and CD_NIVEL_1 IN ( " + n1 + " )";
            sAutorizacion2 = " and CD_NIVEL_2 IN ( " + n2 + " )";
            break;
        }

        break;
    }

    // modos de operaci�n
    switch (opmode) {

      case servletOBTENER_NIV1_X_CODIGO:
      case servletOBTENER_NIV1_X_DESCRIPCION:
      case servletOBTENER_NIV1_X_CODIGO_TODAS_AUTORIZ:
      case servletOBTENER_NIV1_X_DESCRIPCION_TODAS_AUTORIZ:

        // configuracion
        sCampoCod = "CD_NIVEL_1";
        sCampoDes = "DS_NIVEL_1";
        sCampoDesL = "DSL_NIVEL_1";

        // query
        if ( (opmode == servletOBTENER_NIV1_X_CODIGO) ||
            (opmode == servletOBTENER_NIV1_X_CODIGO_TODAS_AUTORIZ)) {
          sQuery = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where CD_NIVEL_1 = ?";
        }
        else {
          sQuery = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where DS_NIVEL_1 = ?";

        }
        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_NIVEL_1 > ?";

          // autorizaciones (s�lo podr�n tener x_c�digo y x_descripci�n)
        }
        if (sAutorizacion1.length() > 0) {
          sQuery = sQuery + sAutorizacion1;

        }
        sQuery = sQuery + " order by CD_NIVEL_1";

        st = con.prepareStatement(sQuery);

        // filtro
        st.setString(iValor, nivel.getCod().trim());
        iValor++;

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }

        /*mlm if (sAutorizacion1.length() > 0)
          st.setString(iValor, param.getLogin().trim());*/

        break;

      case servletSELECCION_NIV1_X_CODIGO:
      case servletSELECCION_NIV1_X_DESCRIPCION:
      case servletSELECCION_NIV1_X_CODIGO_TODAS_AUTORIZ:
      case servletSELECCION_NIV1_X_DESCRIPCION_TODAS_AUTORIZ:

        // configuracion
        sCampoCod = "CD_NIVEL_1";
        sCampoDes = "DS_NIVEL_1";
        sCampoDesL = "DSL_NIVEL_1";

        // query
        // ARG: upper (15/5/02)
        if ( (opmode == servletSELECCION_NIV1_X_CODIGO) ||
            (opmode == servletSELECCION_NIV1_X_CODIGO_TODAS_AUTORIZ)) {
          sQuery = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where CD_NIVEL_1 like ?";
        }
        else {
          sQuery = "select CD_NIVEL_1, DS_NIVEL_1, DSL_NIVEL_1 from SIVE_NIVEL1_S where upper(DS_NIVEL_1) like upper(?)";

        }
        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_NIVEL_1 > ?";

          // autorizaciones (s�lo podr�n tener x_c�digo y x_descripci�n)
        }
        if (sAutorizacion1.length() > 0) {
          sQuery = sQuery + sAutorizacion1;

        }
        sQuery = sQuery + " order by CD_NIVEL_1";

        st = con.prepareStatement(sQuery);

        // filtro
        if ( (opmode == servletSELECCION_NIV1_X_CODIGO) ||
            (opmode == servletSELECCION_NIV1_X_CODIGO_TODAS_AUTORIZ)) {
          st.setString(iValor, nivel.getCod().trim() + "%");
        }
        else {
          st.setString(iValor, "%" + nivel.getCod().trim() + "%");

        }
        iValor++;

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }

        /* mlm if (sAutorizacion1.length() > 0)
          st.setString(iValor, param.getLogin().trim()); */

        break;

      case servletOBTENER_NIV2_X_CODIGO:
      case servletOBTENER_NIV2_X_DESCRIPCION:
      case servletOBTENER_NIV2_X_CODIGO_TODAS_AUTORIZ:
      case servletOBTENER_NIV2_X_DESCRIPCION_TODAS_AUTORIZ:

        // configuracion
        sCampoCod = "CD_NIVEL_2";
        sCampoDes = "DS_NIVEL_2";
        sCampoDesL = "DSL_NIVEL_2";

        // query
        if ( (opmode == servletOBTENER_NIV2_X_CODIGO) ||
            (opmode == servletOBTENER_NIV2_X_CODIGO_TODAS_AUTORIZ)) {
          sQuery = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_2 = ? and CD_NIVEL_1 = ?";
        }
        else {
          sQuery = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where DS_NIVEL_2 = ? and CD_NIVEL_1 = ?";

        }
        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_NIVEL_2 > ?";

          // autorizaciones (s�lo podr�n tener x_c�digo y x_descripci�n)
        }
        if (sAutorizacion2.length() > 0) {
          sQuery = sQuery + sAutorizacion2;

        }
        sQuery = sQuery + " order by CD_NIVEL_2";

        st = con.prepareStatement(sQuery);

        // filtro
        st.setString(iValor, nivel.getCod().trim());
        iValor++;

        st.setString(iValor, nivel.getNivel1().trim());
        iValor++;

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }

        /* mlm if (sAutorizacion2.length() > 0) {
          st.setString(iValor, param.getLogin().trim());
          st.setString(iValor+1, nivel.getNivel1().trim());
                 }*/

        break;

      case servletSELECCION_NIV2_X_CODIGO:
      case servletSELECCION_NIV2_X_DESCRIPCION:
      case servletSELECCION_NIV2_X_CODIGO_TODAS_AUTORIZ:
      case servletSELECCION_NIV2_X_DESCRIPCION_TODAS_AUTORIZ:

        // configuraci�n
        sCampoCod = "CD_NIVEL_2";
        sCampoDes = "DS_NIVEL_2";
        sCampoDesL = "DSL_NIVEL_2";

        // query
        // ARG: upper (15/5/02)
        if ( (opmode == servletSELECCION_NIV2_X_CODIGO) ||
            (opmode == servletSELECCION_NIV2_X_CODIGO_TODAS_AUTORIZ)) {
          sQuery = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_2 like ?";
        }
        else {
          sQuery = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where upper(DS_NIVEL_2) like upper(?)";

        }
        sQuery = sQuery + " and CD_NIVEL_1 = ?";
        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_NIVEL_2 > ?";

          // autorizaciones (s�lo podr�n tener x_c�digo y x_descripci�n)
        }
        if (sAutorizacion2.length() > 0) {
          sQuery = sQuery + sAutorizacion2;

        }
        sQuery = sQuery + " order by CD_NIVEL_2";

        st = con.prepareStatement(sQuery);

        // filtro
        if ( (opmode == servletSELECCION_NIV2_X_CODIGO) ||
            (opmode == servletSELECCION_NIV2_X_CODIGO_TODAS_AUTORIZ)) {
          st.setString(iValor, nivel.getCod().trim() + "%");
        }
        else {
          st.setString(iValor, "%" + nivel.getCod().trim() + "%");

        }
        iValor++;

        st.setString(iValor, nivel.getNivel1().trim());
        iValor++;

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }

        /*mlm if (sAutorizacion2.length() > 0) {
          st.setString(iValor, param.getLogin().trim());
          st.setString(iValor+1, nivel.getNivel1().trim());
                 } */

        break;

      case servletOBTENER_ZBS_X_CODIGO:
      case servletOBTENER_ZBS_X_DESCRIPCION:

        // configuraci�n
        sCampoCod = "CD_ZBS";
        sCampoDes = "DS_ZBS";
        sCampoDesL = "DSL_ZBS";

        // query
        if (opmode == servletOBTENER_ZBS_X_CODIGO) {
          sQuery = "select CD_ZBS, DS_ZBS, DSL_ZBS from SIVE_ZONA_BASICA where CD_ZBS = ? and CD_NIVEL_1 = ? and CD_NIVEL_2 = ?";
        }
        else {
          sQuery = "select CD_ZBS, DS_ZBS, DSL_ZBS from SIVE_ZONA_BASICA where DS_ZBS = ? and CD_NIVEL_1 = ? and CD_NIVEL_2 = ?";

        }
        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_ZBS > ?";

          // autorizaciones
          //if (sAutorizacion2.length() > 0)
          //  sQuery = sQuery + sAutorizacion2;

        }
        sQuery = sQuery + " order by CD_ZBS";

        st = con.prepareStatement(sQuery);

        // filtro
        st.setString(iValor, nivel.getCod().trim());
        iValor++;

        st.setString(iValor, nivel.getNivel1().trim());
        iValor++;

        st.setString(iValor, nivel.getNivel2().trim());
        iValor++;

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }

        break;

      case servletSELECCION_ZBS_X_CODIGO:
      case servletSELECCION_ZBS_X_DESCRIPCION:

        // configuraci�n
        sCampoCod = "CD_ZBS";
        sCampoDes = "DS_ZBS";
        sCampoDesL = "DSL_ZBS";

        // query
        // ARG: upper (15/5/02)
        if (opmode == servletSELECCION_ZBS_X_CODIGO) {
          sQuery =
              "select CD_ZBS, DS_ZBS, DSL_ZBS from SIVE_ZONA_BASICA where CD_ZBS like ?";
        }
        else {
          sQuery = "select CD_ZBS, DS_ZBS, DSL_ZBS from SIVE_ZONA_BASICA where upper(DS_ZBS) like upper(?)";

        }
        sQuery = sQuery + " and CD_NIVEL_1 = ? and CD_NIVEL_2 = ?";
        if (param.getFilter().length() > 0) {
          sQuery = sQuery + " and CD_ZBS > ?";

        }
        sQuery = sQuery + " order by CD_ZBS";
        st = con.prepareStatement(sQuery);

        // filtro
        if (opmode == servletSELECCION_ZBS_X_CODIGO) {
          st.setString(iValor, nivel.getCod().trim() + "%");
        }
        else {
          st.setString(iValor, "%" + nivel.getCod().trim() + "%");

        }
        iValor++;

        st.setString(iValor, nivel.getNivel1().trim());
        iValor++;

        st.setString(iValor, nivel.getNivel2().trim());
        iValor++;

        if (param.getFilter().length() > 0) {
          st.setString(iValor, param.getFilter());
          iValor++;
        }

        break;
    }

    rs = st.executeQuery();

    // extrae la p�gina requerida
    while (rs.next()) {

      // control de tama�o
      if (i > DBServlet.maxSIZE) {
        data.setState(CLista.listaINCOMPLETA);
        data.setFilter( ( (DataEntradaEDO) data.lastElement()).getCod());
        break;
      }

      // control de estado
      if (data.getState() == CLista.listaVACIA) {
        data.setState(CLista.listaLLENA);
      }

      // obtiene los campos
      sCod = rs.getString(sCampoCod);
      sDes = rs.getString(sCampoDes);

      // si aplica campo de descripci�n local, lo recogemos
      if (!sCampoDesL.equals("")) {

        sDesL = rs.getString(sCampoDesL);

        // obtiene la descripcion auxiliar en funci�n del idioma
        if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesL != null)) {
          sDes = sDesL;
        }
      }

      // a�ade un nodo
      data.addElement(new DataEntradaEDO(sCod, sDes));

      i++;
    }
    rs.close();
    st.close();

    // cierra la conexi�n y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }

    return data;
  }

  /*
     public CLista doPrueba(int opmode, CLista param) throws Exception {
      breal = false;
      // establece la conexi�n con la base de datos
//      Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
//      con  = DriverManager.getConnection("jdbc:odbc:thin:@194.140.66.208:1521:ORCL", "sive_desa", "sive_desa");
//
      jdcdriver = new JDCConnectionDriver ("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
      con = openConnection();
      return doWork(opmode, param);
     }
   */
}