package comun;

import java.awt.Color;
import java.awt.TextField;
import java.awt.event.KeyEvent;
import java.awt.event.TextEvent;

/*Si se pasa como obligatorio, se pintara amarilo*/
public class CHora
    extends TextField {

  //datos de entrada
  public String sOblig;

  protected String sValidaSN = "N";
  protected String sFechaAlmacen = ""; //si fechaOK, almacena la fecha
  protected String sStringAntes = "";
  protected String sStringDespues = "";

  //constructor
  public CHora(String sOBLIGATORIO) {
    super();

    //parametros de entrada
    sOblig = sOBLIGATORIO;

    //Color
    if (sOblig.equals("S")) {
      setBackground(new Color(255, 255, 150));

    }
    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  } //end Constructor

//jbInit*****************************************
  private void jbInit() throws Exception {

    //ESCRIBIR
    this.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyPressed(KeyEvent e) {
        txt_keyPressed(e);
      }
    });

    //CHANGE
    this.addTextListener(new java.awt.event.TextListener() {
      public void textValueChanged(TextEvent e) {
        txt_textValueChanged(e);
      }
    });

  } //fin jbnit

//-------------------------------------------------------------------------
  protected boolean ChequearHora(String sCad) {

    String sCadena = "";
    sCadena = sCad;
    boolean b = true;
    int x1 = 0;
    String aa, bb = "";
    String hh = "";
    String mm = "";

    try {

      if (sCadena.equals("")) {
        b = false;
      }

      if (!sCadena.equals("")) {

        //SEPARAMOS LOS DATOS
        x1 = sCadena.indexOf(":", 0);

        if (x1 == -1) {
          b = false; //no se encontraron las barras
        }
        else {
          aa = sCadena.substring(0, x1);
          bb = sCadena.substring(x1 + 1, sCadena.length());

          if (aa.equals("") || bb.equals("")) {
            b = false; //algun bloque falta
          }
          else {

            hh = aa;
            mm = bb;

            //COMPROBAR QUE SON NUMEROS y su rango.
            if ( (ChequearEntero(hh, 0, 23, 2) != true) ||
                (ChequearEntero(mm, 0, 59, 2) != true)) {
              b = false;
            }

            //desechamos largos . FORMATEAMOS A DOS CIFRAS, y
            if (mm.length() > 2
                || hh.length() > 2) {
              b = false;
            }
            if (mm.length() == 1) {
              mm = "0" + mm;
            }
            if (hh.length() == 1) {
              hh = "0" + hh;
            }

          } //fin else aa, bb, cc
        } //fin del else (no barras)
      } //fin de scad

      //FIN DE FUNCION
      if (!b) { //si mal
        sFechaAlmacen = "";
      }
      else {
        sFechaAlmacen = hh + ":" + mm;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      b = false;
      sFechaAlmacen = "";
    }
    return (b);
  } //fin funcion--------------------------------------

  /* Salida:  -1  Esta borrando  o es igual
              i   El lugar donde esta a�adiendo (0,...)
      Ej: XBC --> XB0C : 2*/
  protected int CalcularIndice() {
    //lee las cadenas y descubre la diferencia
    int iReturn = -2;
    String sA, sD = "";

    //borrando
    if (sStringAntes.length() >= sStringDespues.length()) {
      iReturn = -1;
    }
    else {
      //intro del primero
      if (sStringAntes.length() == 0) {
        iReturn = 0;
      }
      else {
        for (int i = 0; i < sStringAntes.length(); i++) {
          sA = sStringAntes.substring(i, i + 1);
          sD = sStringDespues.substring(i, i + 1);
          if (sA.compareTo(sD) != 0) { //no son iguales
            iReturn = i;
            break;
          }
          if (iReturn == -2) { //a�ade al final
            iReturn = sStringAntes.length();
          }
        }
      }
    }
    return (iReturn);
  }

  /* Borra caracter (indice, indice +1)*/
  protected void BorrarCaracter(int indice) {

    String sDelante = getText().substring(0, indice);
    String sDetras = getText().substring(indice + 1, getText().length());

    setText(sDelante + sDetras);
    select(indice, indice);
  }

//Entradas: 	Cadena a chequear, Limite inferior permitido, Limite superior, uFlag(1,2)
//Return:	uFlag=1. False, cadena no numerica. True, vacio y cadena numerica
//		    uFlag=2. False, cadena no numerica y vacio. True, cadena numerica
//*************************************************************************************
   protected boolean ChequearEntero(String sDat, int uMin, int uMax, int uFlag) {
     String cChar = "";
     String sString = sDat;
     int uTipo = uFlag;
     boolean b = true;

     //si chequeamos un string que admita vacio.
     if (uTipo == 1) {
       if (sString == "") {
         b = true;
       }
     }
     //si chequeamos un string que no admita vacio.
     if (uTipo == 2) {
       if (sString == "") {
         b = false;
       }
     }
     //comprobar es un numero
     for (int i = 0; i < sString.length(); i++) {
       cChar = sString.substring(i, i + 1);
       try {
         Integer IChar = new Integer(cChar);
       }
       catch (Exception e) { // si no es un numero
         b = false;
       }

     } //fin del for

     //comprobar longitud
     if (b) {
       Integer Imin = new Integer(sString);
       Integer Imax = new Integer(sString);

       if (Imin.intValue() < uMin || uMax < Imax.intValue()) {
         b = false;
       }
     }

     return (b);
   } //fin de ChequearEntero

  protected boolean Valid_Dato_Intermedio(int indice, String s) {

    boolean b = true;
    int Len = getText().length() - 1; //sin el dato nuevo

    //BLOQUE 1
    if (indice < 2) {

      if (!ChequearEntero(s, 0, 9, 1)) { //No N
        b = false;
        //bloque inacabado y longantes=2 (XXX)
      }
      if ( (getText().indexOf(":", 0) == -1) && (Len == 2) && b) {
        b = false;
        //bloque inacabado y longantes<2 no se dara!! (X)-->a�ado, no modifico

        //bloque acabado y longbloqueantes = 2 (XXX/)
      }
      if ( (getText().indexOf(":", 0) != -1) &&
          (getText().substring(0, getText().indexOf(":", 0)).length() == 3) &&
          b) {
        b = false;

        //bloque acabado y longbloqueantes < 2 (XX/) true
      }
    }

    //SEPARADORES
    if (indice == 2 && b) {

      if (!s.equals(":")) {
        b = false;
      }
      else {
        b = true;
      }
    }

    //BLOQUE 2. Existe 1 barra y el indice >
    if (getText().indexOf(":", 0) != -1
        && indice > getText().indexOf(":", 0) && b) { //existe :

      if (!ChequearEntero(s, 0, 9, 1)) { //No N
        b = false;

      }
      int x1 = getText().indexOf(":", 0);
      if (getText().substring(x1 + 1, getText().length()).length() == 3) {
        b = false;
      }
    } //fin bloque 3

    return (b);
  }

  protected void txt_keyPressed(KeyEvent e) {
    //capturo la fecha de antes
    sStringAntes = getText();

  } //fin Pressed

  protected void txt_textValueChanged(TextEvent e) {

    String s = "";

    //capturo la fecha de despues
    sStringDespues = getText();

    //capturo el indice
    int ind = CalcularIndice();

    //a�adiendo
    if ( (ind != -1) && (ind == getText().length() - 1)) {
      //capturo el caracter que se ha introducido
      s = getText().substring(ind, ind + 1);

      if ( (ind == 2) && !s.equals(":")) {
        BorrarCaracter(ind);

      }
      if (ind != 2 && ind < 4) {
        if (!ChequearEntero(s, 0, 9, 1)) { //No N
          BorrarCaracter(ind);
        }
      }
      if (ind > 4) {
        BorrarCaracter(ind);
      }
    } //--------------------------

    //incluyendo en medio
    if ( (ind != -1) && (ind < getText().length() - 1)) {
      s = getText().substring(ind, ind + 1);
      if (!Valid_Dato_Intermedio(ind, s)) {
        BorrarCaracter(ind);
      }
    } //--------------------------
  } //fin change

//Chequea la fecha que hay en la caja
  public void ValidarFecha() {

    //OK
    if (ChequearHora(getText())) {
      sValidaSN = "S";

      //KO
    }
    else {
      sValidaSN = "N";
    }
  }

//Proporciona si el valor es valido
  public String getValid() {
    return (sValidaSN);
  }

//Proporciona la fecha
  public String getFecha() {
    return (sFechaAlmacen);
  }

} //endclass
