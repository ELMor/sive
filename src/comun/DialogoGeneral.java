/*
  Creado el 30/12/99 para gestionar con comodidad
  las clases
  utilizadas por la clase Panel_Informe_Completo
  del paquete infproto
 */

package comun;

import java.util.Hashtable;

import java.awt.ScrollPane;

import capp.CApp;
import capp.CDialog;
import capp.CPanel;
import sapp.StubSrvBD;

public class DialogoGeneral
    extends CDialog {

  public int modoOperacion = constantes.modoALTA;
  public void Inicializar() {};
  public void Inicializar(int i) {};
  public Hashtable getDatosBloqueo() {
    return new Hashtable();
  };

  public PanelGeneral pProtocolo;

  public StubSrvBD stubCliente;

  public DialogoGeneral(CApp a) {
    super(a);
  }

  public class PanelGeneral
      extends CPanel {
    public void Inicializar() {};
    public ScrollPane panelScroll = new ScrollPane();
    public ScrollPane panelScroll2 = new ScrollPane();
  }

}
