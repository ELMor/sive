//viene del paquete de enfermo de EDO
package comun;

public class constantes {

  // Color de fondo para los componentes obligatorios
  public static final java.awt.Color CampoObligatorio = new java.awt.Color(255,
      255, 208);
  public static final java.awt.Color CampoTextoObligatorio = new java.awt.Color(
      255, 255, 150);
//(255, 255, 150);

  // Modos de uso de panel suca
  public static final int servletSELECT_NORMAL = 1;
  public static final int servletSELECT_SUCA = 2;

  //Constantes de claves de los botones para habilitarlos seg�n el USU
  //DEBEN TENER EL MISMO VALOR EN LA BASE DE DATOS

  public static final String keyBtnAnadir = "20001";
  public static final String keyBtnModificar = "20002";
  public static final String keyBtnBorrar = "20003";
  public static final String keyBtnAux = "20004";

  // Modos de usu de SUCA
  public static final int modoDATOSENFERMOTRAMERO = 320;
  public static final int modoDATOSENFERMO = 10;

  // Identificadores de applets de cat�logos de tuberculosis
  public static final int appletCATALOGO_FUENTES = 3010;
  public static final int appletCATALOGO_MOTIVOS_SALIDA = 3020;
  public static final int appletCATALOGO_MOTIVOS_INICIO = 3030;
  public static final int appletCATALOGO_MUESTRAS = 3040;
  public static final int appletCATALOGO_TECNICAS = 3050;
  public static final int appletCATALOGO_VALORES_MUESTRAS = 3060;
  public static final int appletCATALOGO_MICOBACTERIAS = 3070;
  public static final int appletCATALOGO_ESTUDIOS_RESISTENCIAS = 3080;

  // Identificadores de cat�logos de tuberculosis
  public static final int CATALOGO_FUENTES = 3010;
  public static final int CATALOGO_MOTIVOS_SALIDA = 3020;
  public static final int CATALOGO_MOTIVOS_INICIO = 3030;
  public static final int CATALOGO_MUESTRAS = 3040;
  public static final int CATALOGO_TECNICAS = 3050;
  public static final int CATALOGO_VALORES_MUESTRAS = 3060;
  public static final int CATALOGO_MICOBACTERIAS = 3070;
  public static final int CATALOGO_ESTUDIOS_RESISTENCIAS = 3080;

  // Mensaje en caso de bloqueo
  public static final String DATOS_BLOQUEADOS =
      "Los datos han sido modificados. �Desea sobrescribirlos?";

  // Prefijo para excepciones de bloqueo
  public static final String PRE_BLOQUEO = "EXCEP_BLOQUEO_";

  // Prefijo para excepciones de 3 o m�s factores principales
  public static final String MAS3PRALES = "MAS_3_PRINCIPALES_";

  // Nombres de las tablas que pueden ser bloqueadas
  public static final String SIVE_ENFERMO = "SIVE_ENFERMO";
  public static final String SIVE_NOTIFEDO = "SIVE_NOTIFEDO";
  public static final String SIVE_NOTIF_EDOI = "SIVE_NOTIF_EDOI";
  public static final String SIVE_E_NOTIF = "SIVE_E_NOTIF";
  public static final String SIVE_NOTIF_SEM = "SIVE_NOTIF_SEM";
  public static final String SIVE_EDOIND = "SIVE_EDOIND";
  public static final String SIVE_REGISTROTBC = "SIVE_REGISTROTBC";

  // Nombres de las tablas que pueden ser bloqueadas: BROTES
  public static final String SIVE_BROTES = "SIVE_BROTES";

  // modos del dialogo de otros
  public static final int modoNORMAL = 34;
  public static final int modoVISUAL = 6; //consulta

  // NOMBRES DE LAS COLUMNAS DE LA TABLA SIVE_SEC_GENERAL
  public static final String COLUMNA_EDO = "NM_EDO";
  public static final String COLUMNA_ENFERMO = "NM_ENFERMO";
  public static final String COLUMNA_TRATRTBC = "NM_TRATRTBC";
  public static final String COLUMNA_MOVENF = "NM_MOVENF";

  // Constante para flag IT_REPE
  public static final String NO_REPE = "NO_REPE_";

  // Nombres de tablas que llevan un flag IT_REPE: BROTES
  public static final String SIVE_BROTES_MED = "SIVE_BROTES_MED";
  public static final String SIVE_BROTES_FACCONT = "SIVE_BROTES_FACCONT";
  public static final String SIVE_AGCAUSAL_BROTE = "SIVE_AGCAUSAL_BROTE";

  // C�DIGO DE FUENTE DE NOTIFICACI�N EDO
  public static final String FUENTE_EDO = "E";

  // C�DIGO DE CD_T_SIVE PARA CONTACTOS DE TUBERCULOSIS
  public static final String CD_T_SIVE_CONTACTOS = "T";

  // C�DIGO DE CD_T_SIVE PARA EDO Y TUBERCULOSIS
  public static final String CD_T_SIVE_EDO = "E";

  // C�DIGO TUBERCULOSIS
  //public static final String TUBERCULOSIS = "011";
  //modificado el 16/02/2001 el c�digo de tuberculosis no es fijo a 011

  // C�DIGO APLICACI�N TUBERCULOSIS
  public static final String APP_TUBERCULOSIS = "RTBC";

  // Modos est�ndar de llamada a los servlets
  public static final int sOBTENER_X_CODIGO = 4;
  public static final int sOBTENER_X_DESCRIPCION = 5;
  public static final int sSELECCION_X_CODIGO = 6;
  public static final int sSELECCION_X_DESCRIPCION = 7;
  // Meta-Modos de llamada al servlet SrvEnfermo
  public static final String sSoloEnfermos = "1";
  public static final String sSoloContactos = "2";
  public static final String sEnfermosContactos = "3";
  // Motivo de baja = defunci�n.
  public static final String sMotivoDefuncion = "01";
  public static final String sMotivoDefuncionTBC = "01";
  public static final String sMotivoTratamiento = "01";

  //cierre automatico
  public static final String sMotivoCierreAutomatico = "02";

  //constantes del panel
  public static final String imgLUPA = "images/Magnify.gif";
  public static final String imgALTA = "images/alta.gif";
  public static final String imgMODIFICACION = "images/modificacion.gif";
  public static final String imgBORRAR = "images/baja.gif";
  public static final String imgCANCELAR = "images/cancelar.gif";
  public static final String imgBUSCAR = "images/search1.gif";
  public static final String imgACEPTAR = "images/aceptar.gif";
  public static final String imgLIMPIAR = "images/vaciar.gif"; //limpiar
  public static final String imgALTA2 = "images/alta2.gif";
  public static final String imgBAJA2 = "images/baja2.gif";
  public static final String imgMODIFICAR2 = "images/modificacion2.gif";
  public static final String imgPRIMERO = "images/primero.gif";
  public static final String imgANTERIOR = "images/anterior.gif";
  public static final String imgSIGUIENTE = "images/siguiente.gif";
  public static final String imgULTIMO = "images/ultimo.gif";
  public static final String imgACTUALIZAR = "images/actualizar.gif";
  public static final String imgREFRESCAR = "images/refrescar.gif";
  public static final String imgGENERAR = "images/grafico.gif";
  public static final String imgSALIR = "images/salir.gif";

  //SERVLETS DE TUBERCULOSIS_____________________________________________________________________

  public static final String strSERVLET_TUBNOTIF = "servlet/SrvTubNotif";
  public static final String strSERVLET_NOTIFTUB = "servlet/SrvNotifTub";
  public static final String strSERVLET_CONFLICTOS = "servlet/SrvConflictos";
  public static final String strSERVLET_RES_CONFLICTOS =
      "servlet/SrvResConflictos";
  public static final String strSERVLET_CASOSTUB = "servlet/SrvMntCasos";
  public static final String strSERVLET_MODPREGTUB = "servlet/SrvModPregTub";
  public static final String strSERVLET_SELUSUTUB = "servlet/SrvSelUsuTub";
  public static final String strSERVLET_SQLTUB = "servlet/SrvSQLTub";
  public static final String strSERVLET_CONVFECTUB = "servlet/SrvConvFecTub";
  public static final String strSERVLET_AUTORIZACIONES = "servlet/SrvTubAut";
  public static final String strSERVLET_MENU = "servlet/SrvTubApp";
  public static final String strSERVLET_DIA_MUES = "servlet/SrvDiaMues";
  public static final String strSERVLET_CONCASTUB = "servlet/SrvConCasTub";

  public static final String strSERVLET_DIAREGTUB = "servlet/SrvDiaMntRegTub";
  public static final String strSERVLET_EQ_PRIMARIA = "servlet/SrvEqPrimaria";
  public static final String strSERVLET_DIATRAT = "servlet/SrvDiaTrat";
  public static final String strSERVLET_AUTOCIERRE = "servlet/SrvAutoCierre";
  public static final String strSERVLET_CONTUB = "servlet/SrvMntCont";
  public static final String strSERVLET_CATALOGOS_CASOSTUB =
      "servlet/SrvCatCasoTub";
  public static final String strSERVLET_REGTUBS = "servlet/SrvMntRegTub";
  public static final String strSERVLET_CATALOGOS = "servlet/SrvCatRegTub";
  public static final String strSERVLET_MUES = "servlet/SrvCasMues";
  public static final String strSERVLET_RESIS = "servlet/SrvCasResis"; // E 27/01/2000
  public static final String strSERVLET_TOTMUES = "servlet/SrvSelRes";
  public static final String strSERVLET_TOTTRAT = "servlet/SrvSelTrat";
  public static final String strSERVLET_TRATS = "servlet/SrvCasTrat";
  public static final String strSERVLET_PROTICM = "servlet/SrvProtocoloIcm";

  //SErvlets repasar donde est�n
  public static final String strSERVLET_MUNICIPIO = "servlet/SrvMunicipio";
  public static final String strSERVLET_GENERAL = "servlet/SrvGeneral";
  public static final String strSERVLET_GEN = "servlet/SrvGen";
  public static final String strSERVLET_CN = "servlet/SrvCNIcm";
  public static final String strSERVLET_CAT2 = "servlet/SrvCat2";
  public static final String strSERVLET_ENFVIGI = "servlet/SrvEnfVigi";

  // Servlets paquete enfermotub (CAMBIADA LA URL)
  public static final String strSERVLET_ENFERMO = "servlet/SrvEnfermoTub";

  //SERVLETS DE PAQUETES COMPARTIDOS___________________________________________________

  // Servlets paquete comun (HAY QUE CAMBIAR LA URL)
  public static final String strSERVLET_ENF = "servlet/SrvEnfermedadCom";
  public static final String strSERVLET_MAESTRO = "servlet/SrvMaestroEDOCom";
  public static final String strSERVLET_NIV = "servlet/SrvEntradaEDOCom";
  public static final String strSERVLET_NIV2 = "servlet/SrvZBS2Com";
  public static final String strSERVLET_NIVEL1 = "servlet/SrvNivel1Com";
  public static final String strSERVLET_MUN = "servlet/SrvMunCom";
  public static final String strSERVLET_MUNICIPIO_CONT =
      "servlet/SrvMunicipioContCom";

  // Servlets para gesti�n de Protocolos (infproto)
  public static final String strSERVLET_RESP_EDO = "servlet/SrvRespEDOIcm";
  public static final String strSERVLET_RESP_BROTES =
      "servlet/SrvRespBrotesIcm";
  public static final String strSERVLET_RESP_TUBER = "servlet/SrvRespTuber";
  public static final String strSERVLET_RESP_TUBER_CON =
      "servlet/SrvRespTuberCon";
  public static final String strSERVLET_SEL_PROT = "servlet/SrvSelProt";
  public static final String strSERVLET_ACTUALIZAR_PROT =
      "servlet/SrvActualizarProtIcm";

  // Servlets paquete confprot
  public static final String strSERVLET_MOD = "servlet/SrvGesMod";
  public static final String strSERVLET_MODELO = "servlet/SrvLineasModelo";
  public static final String strSERVLET_DE_MODELO = "servlet/SrvModelo";
  public static final String strSERVLET_DESCARGA = "servlet/SrvDesMod";

  //Servlets de paquete alarmas
  public static final String strSERVLET_IND = "servlet/SrvInd";
  public static final String strSERVLET_ALAR = "servlet/SrvAlar";
  public static final String strSERVLET_INDANO = "servlet/SrvIndAno";
  //Paquete cargamodelo
  public static final String strSERVLET_CARGA = "servlet/SrvCarga"; //????
  public static final String strSERVLET_CARGA_MOD = "servlet/SrvCargaMod";
  //Paquete catalogo
  public static final String strSERVLET_CAT = "servlet/SrvCat";
  //Servlet paquete enfcentinela (para busqueda enfermedades centinelas)
  public static final String strSERVLET_ENF_CENTI = "servlet/SrvEnfCenti";
  //Listas
  public static final String strSERVLET_MANT_LIS = "servlet/SrvMantlis";
  //Paquete preguntas
  public static final String strSERVLET_MANT_PREGUNTAS =
      "servlet/SrvMantPreguntas";
  //paquete enfcie
  public static final String strSERVLET_ENF_CIE = "servlet/SrvEnfCie";
  //Paquete fechas
  public static final String strSERVLET_CONV_FEC = "servlet/SrvConvFec";
  //Paquete suca
  public static final String strSERVLET_SUCA = "servlet/srvsuca";
  //Paquete sapp2
  public static final String strSERVLET_QUERY_TOOL = "servlet/SrvQueryTool";

  public static final int modoESPERA = 2;
  public static final int modoALTA = 3;
  public static final int modoMODIFICACION = 4;
  public static final int modoBAJA = 5;
  public static final int modoCONSULTA = 6;
  public static final int modoSOLODECLARANTES = 7;
  public static final int modoDESHABILITADO = 8;
  public static final int modoLEERDATOS = 9;
  public static final int modoALTA_SB = 10; // Sin Bloqueo
  public static final int modoMODIFICACION_SB = 11; // Sin Bloqueo
  public static final int modoBAJA_SB = 12; // Sin Bloqueo
  public static final int modoSELECT = 14; //select
  public static final int modoBORRARUNO = 15;
  public static final int modoBORRARMASIVO = 16;
  public static final int modoBORRARUNO_SB = 17;
  public static final int modoBORRARMASIVO_SB = 18;
  public static final int modoBUSCAR = 20;
  public static final int modoLIMPIAR_FECHAS = 21; // S�LO SE USA EN RTBC
  public static final int modoACTUALIZARPROT = 22;
  public static final int modoVACIARPROT = 23;

  //mant de rt y de casos de tub
  public static final int modoMANTENIMIENTO = 0;
  public static final int modoCERRAR = 1;
  public static final int modoDESHACERCIERRE = 2;
  public static final int modoCASOSTUB = 3;
  public static final int modoALTADESDECASO = 4;

  // para generar cobertura
  public static final int modoGENERACOBERTURA = 11;

  // Para el componente panniveles
  // modos de funcionaniento de 'panniveles'
  public static final int TIPO_NIVEL_1 = 1; // �rea
  public static final int TIPO_NIVEL_2 = 2; // �rea + Distrito
  public static final int TIPO_NIVEL_3 = 3; // �rea + Distrito + Descripci�n del distrito
  public static final int TIPO_NIVEL_4 = 4; // �rea + Distrito + ZBS

  // l�neas utilizadas en 'panniveles' para presentar los controles
  public static final int LINEAS_1 = 1;
  public static final int LINEAS_2 = 2;
  public static final int LINEAS_3 = 3;

  //No puede darse el caso de TIPO_NIVEL_4 y COLUMNAS_2
  public static final int COLUMNAS_2 = 12; //una linea con dos columnas
  // Fin de panniveles

  //listas catalogo para Casos de Tuberculosis
  public static final String DATOS = "DATOS"; //CLista
  public static final String CLASIFICACIONES = "CLASIFICACIONES"; //CLista
  public static final String PAISES = "PAISES"; //CLista
  public static final String CA = "CA"; //CLista
  public static final String TRAMEROS = "TRAMEROS"; //CLista //cdtvia
  public static final String NUMEROS = "NUMEROS"; //CLista  //cdnum
  public static final String CALIFICADORES = "CALIFICADORES"; //CLista //cal
  public static final String SEXO = "SEXO"; //CLista
  public static final String TDOC = "TDOC"; //CLista
  public static final String FUENTESNOTIF = "FUENTESNOTIF"; //CLista
  public static final String MOTIVOS_TRATAMIENTO = "MOTIVOS_TRATAMIENTO"; //CLista
  public static final String ANO_ALTA_DESDE_RT = "ANO_ALTA_DESDE_RT"; //String : Heredar el a�o del RT desde el alta del caso (panSup)
  public static final String TIPO_TECNICALAB = "TIPO_TECNICALAB"; //CLista
  public static final String MUESTRA_LAB = "MUESTRA_LAB"; //CLista
  public static final String VALOR_MUESTRA = "VALOR_MUESTRA"; //CLista
  public static final String TMICOBACTERIA = "TMICOBACTERIA"; //CLista
  public static final String ESTUDIORESIS = "ESTUDIORESIS"; //CLista
  public static final String DATOSNOTIFICADOR = "DATOSNOTIFICADOR"; //DatNotifTub
  public static final String VECTORNOTIFICADORES = "VECTOR_NOTIFICADORES"; // Vector
  public static final String VECTORNUEVOSNOTIFICADORES =
      "VECTOR_NUEVOS_NOTIFICADORES"; // Vector
  public static final String CASOEDO = "CASOEDO"; // String
  public static final String DATOSBLOQUEO = "DATOSBLOQUEO"; // Hashtable
  // constructor
  public constantes() {
  }
}
