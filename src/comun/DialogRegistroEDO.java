//parecido a DialogCasoEDO de notduplic
package comun;

import java.net.URL;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import capp.CTabla;
import jclass.bwt.BWTEnum;
import sapp.StubSrvBD;

public class DialogRegistroEDO
    extends CDialog {

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  //confidencial
  protected boolean bConfidencial = false;

  // im�genes
  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

  Panel panel1 = new Panel();
  XYLayout xYLayout1 = new XYLayout();

  BorderLayout borderLayout1 = new BorderLayout();

  // Servlet
  final int servletSELECCION_DIALOGO_EDO = 21;
  final String strSERVLET_CasoEDO = "servlet/SrvDlgBuscarRegistro";
  StubSrvBD stubCliente;
  String cadena = "";
  protected String sDesEnfermedad_Entrada = "";

  // Datos de salida
  public DataRegistroEDO datos = null;

  // Datos de salida
  public DataRegistroEDO Resultados = null;
  CLista listaSalida = new CLista();
  CLista listaParametros = new CLista();
  TextField txtFecNac = new TextField();
  TextField txtNombre = new TextField();
  TextField txtEnfermedad = new TextField();
  Label label1 = new Label();
  Label label2 = new Label();
  Label label3 = new Label();
  Label label4 = new Label();
  TextField txtEnfermo = new TextField();
  ButtonControl btnSeleccionar = new ButtonControl();
  ButtonControl btnSalir = new ButtonControl();
  //QQ: Adaptar CTabla
  CTabla tabla = new CTabla();

  public DialogRegistroEDO(CApp a, DataRegistroEDO valores) {
    super(a);

    txtEnfermedad.setText(valores.sDesEnfermedad);
    sDesEnfermedad_Entrada = valores.sDesEnfermedad;
    txtEnfermo.setText(valores.sCodEnfermo);
    txtFecNac.setText(valores.sFechaNac);

    //mlm confidencial
    bConfidencial = valores.bConfidencial;
    if (bConfidencial) { //true=normal
      txtNombre.setText(valores.sApe1 + " " + valores.sApe2 + ", " +
                        valores.sNombre);
    }
    else {
      txtNombre.setText(valores.sSiglas);
      //-----------------------------------

      //QQ: Inicializar con todos los campos
    }
    Resultados = new DataRegistroEDO("", "", "", "", "", 0, "", "",
                                     false, false, valores.bConfidencial, "",
                                     "", "", "", "", "",
                                     "", "", "", "", "", "", "", "");
    // ahora implementamos el proceso del Servlet

    try {
      jbInit();
      pack();

      stubCliente = new StubSrvBD(new URL(this.app.getURL() +
                                          strSERVLET_CasoEDO));

      // par�metros
      //QQ: Inicializar con todos los campos
      datos = new DataRegistroEDO(valores.sCodEnfermo,
                                  "",
                                  "",
                                  "", "",
                                  0,
                                  "",
                                  valores.sCodEnfermedad,
                                  false, false, valores.bConfidencial,
                                  "", "", "", "", "", "", "", "", "", "", "",
                                  "", "", "");

      listaParametros = new CLista();
      listaParametros.addElement(datos);

      listaSalida = (CLista) stubCliente.doPost(servletSELECCION_DIALOGO_EDO,
                                                listaParametros);

      Resultados.bHayDatos = false;
      if (listaSalida.size() != 0) { // hay datos
        Resultados.bHayDatos = true;

        // se recuperan los datos de cabecera por si acaso
        // no se han pasado como par�metros
        // QQ: Atenci�n, ya no inclu�mos datos del enfermo
        datos = (DataRegistroEDO) listaSalida.firstElement();
        //txtEnfermedad.setText(sDesEnfermedad_Entrada);
        //txtEnfermo.setText(datos.sCodEnfermo);
        //txtFecNac.setText(datos.sFechaNac);
        //txtNombre.setText(datos.sApe1+" "+datos.sApe2+", "+datos.sNombre);

        tabla.clear();
        for (int r = 0; r < listaSalida.size(); r++) {
          datos = (DataRegistroEDO) listaSalida.elementAt(r);
          cadena = "";
          cadena = Integer.toString(datos.iCodCaso) + "&";

          // mlm: datos del rt en vez de Clasificaci�n
          cadena += datos.sCD_ARTBC + "/" + datos.sCD_NRTBC;
          if (datos.sFC_SALRTBC.equals("")) {
            cadena += "-" + "&";
          }
          else {
            cadena += "-" + datos.sFC_SALRTBC + "&";
            //------------------------------------------

            // fecha notificaci�n
          }
          cadena += datos.sFechaNotif + "&";
          // fecha inicio de s�ntomas
          if (datos.sFechaIni == null) {
            cadena += " " + "&";
          }
          else {
            cadena += datos.sFechaIni + "&";

            // a�adimos la l�nea de texto a la tabla

            //QQ: Nuevas columnas del primer notificador
          }
          if (datos.sDesCentro == null) {
            cadena += " " + "&";
          }
          else {
            cadena += datos.sDesCentro + "&";

          }
          if (datos.sDesEquipo == null) {
            cadena += " " + "&";
          }
          else {
            cadena += datos.sDesEquipo + "&";

          }
          if (datos.sCodN1 == null) {
            cadena += "  -";
          }
          else {
            cadena += datos.sCodN1 + "-";
          }
          if (datos.sCodN2 == null) {
            cadena += " " + "&";
          }
          else {
            cadena += datos.sCodN2 + "&";

          }
          tabla.addItem(cadena, '&');
        }
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void Inicializar() {
  }

  void jbInit() throws Exception {

    xYLayout1.setHeight(350);
    xYLayout1.setWidth(750);
    panel1.setLayout(xYLayout1);
    this.setLayout(borderLayout1);
    this.setSize(new Dimension(750, 350));

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnSeleccionar.setImage(imgs.getImage(0));
    btnSalir.setImage(imgs.getImage(1));

    this.setTitle("Registros de Tuberculosis para un enfermo");

    //    xYLayout1.setHeight(350);
    this.setSize(new Dimension(750, 350));
    //    xYLayout1.setWidth(450);
    txtFecNac.setEditable(false);
    txtNombre.setEditable(false);
    txtEnfermedad.setBackground(new Color(250, 250, 150));
    txtEnfermedad.setForeground(Color.black);
    txtEnfermedad.setFont(new Font("Dialog", 0, 12));
    txtEnfermedad.setEditable(false);
    if (bConfidencial) { //true=normal
      label1.setText("Apellidos y Nombre:");
    }
    else {
      label1.setText("Apellidos y Nombre:");
    }
    label2.setForeground(Color.black);
    label2.setFont(new Font("Dialog", 0, 12));
    label2.setText("Fecha nacimiento:");
    label3.setForeground(Color.black);
    label3.setFont(new Font("Dialog", 0, 12));
    label3.setText("Enfermo:");
    label4.setForeground(Color.black);
    label4.setFont(new Font("Dialog", 0, 12));
    label4.setText("Enfermedad:");
    txtEnfermo.setBackground(new Color(255, 255, 150));
    txtEnfermo.setForeground(Color.black);
    txtEnfermo.setFont(new Font("Dialog", 0, 12));
    txtEnfermo.setEditable(false);
    btnSeleccionar.setFont(new Font("Dialog", 0, 12));
    btnSeleccionar.setLabel("Seleccionar");
    btnSalir.addActionListener(new DialogRegistroEDO_btnSalir_actionAdapter(this));
    btnSalir.setLabel("Salir");

    tabla.setNumColumns(7);
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "40\n120\n80\n80\n150\n140\n80"), '\n')); //mlm : quito la clasificacion y pongo RT y fecha salidaRT
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(
        "Expte.\nReg.-F.Salida\nF. Notif.\nF. I. Sintomas\nCentro\nEquipo\nZonif."),
        '\n'));

    btnSalir.setFont(new Font("Dialog", 0, 12));
    btnSeleccionar.addActionListener(new
        DialogRegistroEDO_btnSeleccionar_actionAdapter(this));
    label1.setFont(new Font("Dialog", 0, 12));
    label1.setForeground(Color.black);
    txtNombre.setFont(new Font("Dialog", 0, 12));
    txtNombre.setBackground(new Color(255, 255, 150));
    txtNombre.setForeground(Color.black);
    txtFecNac.setFont(new Font("Dialog", 0, 12));
    txtFecNac.setForeground(Color.black);
//    panel1.setLayout(xYLayout1);
//    this.add(panel1);

    this.add(panel1, BorderLayout.CENTER);

    panel1.add(label3, new XYConstraints(20, 15, 80, -1));
    panel1.add(txtEnfermo, new XYConstraints(135, 15, 70, -1));
    panel1.add(label1, new XYConstraints(20, 45, 116, -1));
    panel1.add(txtNombre, new XYConstraints(135, 45, 246, -1));
    panel1.add(label2, new XYConstraints(20, 75, 103, -1));
    panel1.add(txtFecNac, new XYConstraints(135, 75, 81, -1));
    panel1.add(label4, new XYConstraints(20, 105, -1, -1));
    panel1.add(txtEnfermedad, new XYConstraints(135, 105, 246, -1));
    panel1.add(tabla, new XYConstraints(20, 145, 710, 150));
    panel1.add(btnSeleccionar, new XYConstraints(550, 307, -1, -1));
    panel1.add(btnSalir, new XYConstraints(660, 307, -1, -1));
  }

  public boolean hayResultados() {
    return Resultados.bHayDatos;
  }

  void btnSeleccionar_actionPerformed(ActionEvent e) {
    CMessage mensaje = null;

    if (tabla.getSelectedIndex() != BWTEnum.NOTFOUND) {
      // se obtiene la l�nea seleccionada
      Resultados = (DataRegistroEDO) listaSalida.elementAt(tabla.
          getSelectedIndex());

      //mlm: si el RT seleccionado esta cerrado, se indica y no se deja seleccionar
      if (!Resultados.sFC_SALRTBC.equals("")) {
        Common.ShowWarning(this.app,
            "El Registro est� cerrado. Debe abrirlo antes de Seleccionarlo.");
        return;
      }
      //--------------------------------------------------------------------------

      // hay resultados
      Resultados.bHayDatos = true;
      // Se ha aceptado
      Resultados.bEsOK = true;
      // se cierra el di�logo
      dispose();
    }
    else { // no se ha seleccionado nada
      mensaje = new CMessage(this.app, CMessage.msgERROR,
                             "No ha seleccionado a ning\u00FAn enfermo.");
      mensaje.show();
      mensaje = null;
    }
  }

  void btnSalir_actionPerformed(ActionEvent e) {
    Resultados.bEsOK = false;
    dispose();
  }

}

class DialogRegistroEDO_btnSeleccionar_actionAdapter
    implements java.awt.event.ActionListener {
  DialogRegistroEDO adaptee;

  DialogRegistroEDO_btnSeleccionar_actionAdapter(DialogRegistroEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnSeleccionar_actionPerformed(e);
  }
}

class DialogRegistroEDO_btnSalir_actionAdapter
    implements java.awt.event.ActionListener {
  DialogRegistroEDO adaptee;

  DialogRegistroEDO_btnSalir_actionAdapter(DialogRegistroEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnSalir_actionPerformed(e);
  }
}
