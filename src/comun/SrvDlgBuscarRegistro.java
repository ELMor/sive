//viene de notservlet: SrvEDOIndiv
//NOTA: solo usamos modo=servletSELECCION_DIALOGO_EDO
package comun;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvDlgBuscarRegistro
    extends DBServlet {

  // modos de operaci�n del servlet
  final int servletSELECCION_DIALOGO_EDO = 21;

  // objetos de datos
  CLista listaSalida = null;
  CLista listaEntrada = new CLista();
  DataRegistroEDO dataDialogoEDO = new DataRegistroEDO();
  DataRegistroEDO dataExpediente = new DataRegistroEDO();

  // par�metros
  String sQuery = ""; // Query para mostrar descripci�n de diagn�stico
  String sQueryNot = ""; //Query para mostrar en la lista el primer declarante
  java.util.Date dFecha;
  java.sql.Date sqlFec;
  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

  // objetos JDBC
  Connection con = null;
  PreparedStatement st = null;
  ResultSet rs = null;
  int i = 0;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);
    listaSalida = new CLista();

    //datos
    String sCodEquipo = "";
    String sAnno = "";
    String sSemana = "";
    String sFechaRecep = "";
    String sFechaNotif = "";
    String sExpediente = "";
    String sCodEnfermedad = "";
    String sDesEnfermedad = "";
    String sCodEnfermo = "";
    String sIniciales = "";
    String sFechaNacto = "";

    String FC_NAC = "", FC_INISNT = "";
    java.util.Date dFC_NAC;
    java.util.Date dFC_INISNT;

    String sDesCla = "";
    String sDesClaL = "";

    try {
      switch (opmode) {

        case servletSELECCION_DIALOGO_EDO:

          //QQ:
          // recogemos los datos de entrada
          dataDialogoEDO = (DataRegistroEDO) param.firstElement();

          String sCod_Enfermedad = dataDialogoEDO.sCodEnfermedad;
          // String sDes_Enfermedad= dataDialogoEDO.sDesEnfermedad;

          // Construimos la query.
          // Comprobar igualaci�n de it_primero.

          sQuery = "select a.NM_EDO, a.CD_CLASIFDIAG," +
              " a.FC_FECNOTIF, a.FC_INISNT, b.CD_E_NOTIF" +
              " from SIVE_EDOIND a, SIVE_NOTIF_EDOI b" +
              " where a.CD_ENFERMO = ? and a.CD_ENFCIE = ?" +
              " and b.IT_PRIMERO=? and a.NM_EDO = b.NM_EDO";

          st = con.prepareStatement(sQuery);
          st.setInt(1,
                    (new Integer(dataDialogoEDO.sCodEnfermo.trim())).intValue());
          st.setString(2, dataDialogoEDO.sCodEnfermedad.trim());
          st.setString(3, "S");
          rs = st.executeQuery();

          while (rs.next()) {
            // Inicio de s�ntomas
            dFC_INISNT = rs.getDate("FC_INISNT");
            if (dFC_INISNT == null) {
              FC_INISNT = null;
            }
            else {
              FC_INISNT = formater.format(dFC_INISNT);

            }

            listaSalida.addElement(new DataRegistroEDO(dataDialogoEDO.
                sCodEnfermo.trim(),
                "", //Antes DS_APE1
                "", //Antes DS_APE2
                "", //Antes DS_NOMBRE
                "",
                rs.getInt("NM_EDO"),
                "", "", true, false, false,
                "", //Antes, fecha nacimiento
                formater.format(rs.getDate("FC_FECNOTIF")),
                rs.getString("CD_CLASIFDIAG"),
                null,
                FC_INISNT,
                "", "", //Codigo, desc centro
                rs.getString("CD_E_NOTIF"), "", //Codigo, desc equipo
                "", "", "", "", "")); //Area, Distrito
            //Ojo, a�adido cd_e_notif.

          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //QUERY DEL JOIN ie DS_CLASIFDIAG
          sQuery = "select "
              + " DS_CLASIFDIAG "
              + " FROM  SIVE_CLASIF_EDO"
              + "  WHERE CD_CLASIFDIAG = ?";

          //Query del primer notificador:
          sQueryNot = " select "
              + "a.DS_CENTRO, b.DS_E_NOTIF, b.CD_NIVEL_1,b.CD_NIVEL_2"
              + " FROM sive_c_notif a, sive_e_notif b"
              + " WHERE b.CD_E_NOTIF=? "
              + " and a.cd_centro=b.cd_centro";

          for (i = 0; i < listaSalida.size(); i++) {
            dataExpediente = (DataRegistroEDO) listaSalida.elementAt(i);
            // valores de enfermedad
            // dataExpediente.sCodEnfermedad=sCod_Enfermedad;
            // dataExpediente.sDesEnfermedad=sDes_Enfermedad;

            //clasif
            if (dataExpediente.sCodClasif != null) {
              st = con.prepareStatement(sQuery);
              st.setString(1, dataExpediente.sCodClasif);
              rs = st.executeQuery();
              while (rs.next()) {

                //_______________________

                sDesCla = rs.getString("DS_CLASIFDIAG");

                // obtiene la descripcion auxiliar en funci�n del idioma
              }
              if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
                sDesClaL = rs.getString("DSL_CLASIFDIAG");
                if (sDesClaL != null) {
                  sDesCla = sDesClaL;
                }
              }
              //_______________________
              //         dataExpediente.sDesClasif = rs.getString("DS_CLASIFDIAG");

              dataExpediente.sDesClasif = sDesCla;

              rs.close();
              rs = null;
              st.close();
              st = null;
            }

            //Primer declarante:
            //Ojo, meter en la estructura sCodEquipo
            if (dataExpediente.sCodEquipo != null) {
              st = con.prepareStatement(sQueryNot);
              st.setString(1, dataExpediente.sCodEquipo);
              rs = st.executeQuery();
              int iAuxPrimeros = 0;
              while (rs.next()) {
                dataExpediente.sDesCentro = rs.getString("DS_CENTRO");
                dataExpediente.sDesEquipo = rs.getString("DS_E_NOTIF");
                dataExpediente.sCodN1 = rs.getString("CD_NIVEL_1");
                dataExpediente.sCodN2 = rs.getString("CD_NIVEL_2");
                iAuxPrimeros++;
              }
              if (iAuxPrimeros > 1) { // Por si hubieran aparecido varios primer-declarante
                dataExpediente.sDesCentro = "Err: Varios declarantes";
                dataExpediente.sDesEquipo = "  como declarante 1�.";
                dataExpediente.sCodN1 = "-";
                dataExpediente.sCodN2 = "-";
              }
            }

            //MLM: para cada expediente, sacamos su REGISTRO DE TUBER
            sQuery = " select "
                + "CD_ARTBC,CD_NRTBC,FC_SALRTBC "
                + " FROM SIVE_REGISTROTBC "
                + " WHERE NM_EDO =? ";
            st = con.prepareStatement(sQuery);
            st.setInt(1, dataExpediente.iCodCaso);

            rs = st.executeQuery();
            String sfecha = "";
            java.util.Date dfecha = null;
            while (rs.next()) {
              dataExpediente.sCD_ARTBC = rs.getString("CD_ARTBC");
              dataExpediente.sCD_NRTBC = rs.getString("CD_NRTBC");
              dfecha = rs.getDate("FC_SALRTBC");
              if (dfecha == null) {
                sfecha = "";
              }
              else {
                sfecha = formater.format(dfecha);

              }
              dataExpediente.sFC_SALRTBC = sfecha;

            }
            //------------------------------------------------------
          } //fin for

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (listaSalida != null) {
      listaSalida.trimToSize();
    }
    return listaSalida;
  } //fin doWork
} //fin clase
