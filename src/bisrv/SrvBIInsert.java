package bisrv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;

import bidata.DataAlerta;
import bidata.DataBrote;
import capp.CLista;
import sapp.DBServlet;

public class SrvBIInsert
    extends DBServlet {

  final int servletINSERT = 0;

  public SimpleDateFormat Format_Horas = new SimpleDateFormat(
      "dd/MM/yyyy HH:mm:ss", new Locale("es", "ES"));
  public java.sql.Timestamp fecRecu = null;
  public String sfecRecu = "";

  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    //java.sql.Timestamp TSFec = new java.sql.Timestamp(dFecha.getTime());
    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);
    //# System_Out.println("ts : " + TSFec.toString());

    return TSFec;
  }

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  private String fecha_de_fecha(String f) {
    //dd/mm/yyyy hh:mm:ss
    f = f.trim();
    return f.substring(0, f.indexOf(' ', 0));
  }

  private String hora_de_fecha(String f) {
    //dd/mm/yyyy hh:mm:ss
    f = f.trim();
    return f.substring(f.indexOf(' ', 0) + 1);
  }

  private String fecha_mas_hora(String f, String h) {
    f = f.trim();
    h = h.trim();
    if (h.equals("")) {
      h = "00:00";
      // System_out.println("SrvBIInsert: fecha " + f);
      // System_out.println("SrvBIInsert: hora " + h);
      // System_out.println("SrvBIInsert: fecha_mas_hora " + f + " " + h.substring(0, h.lastIndexOf(':')));
      //return f + " " + h.substring(0, h.lastIndexOf(':'));
    }
    return f + " " + h + ":00";
  }

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = "";
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    DataAlerta dataEntAlerta = null;
    DataBrote dataEntBrote = null;
    CLista listaSalida = new CLista();
    DataAlerta dataSalAlerta = new DataAlerta();
    DataBrote dataSalBrote = new DataBrote();

    //querys
    String sQuery = "";

    //fechas
    java.util.Date dFecha;
    java.sql.Date sqlFec;
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    //STRING
    String desMunicipio = null;
    String codCadef = null;
    String desNivel1 = null;
    String desNivel2 = null;
    String desZB = null;
    String desNivel1_normal = null;
    String desNivel2_normal = null;
    String desequipo = null;
    String codcentro = null;
    String descentro = null;
    int cdAler = 0;

    try {

      switch (opmode) {

        //con SUCA cambia la seleccion de descripciones de pais, ca, prov y mun

        case servletINSERT:

          //entrada: (DataAlerta, DataBrote)
          //salida:
          //    si bien: (DataAlerta, DataBrote)
          //              con los ope y las fultact cambiadas
          //    si mal:  excepcion y null

          dataEntAlerta = (DataAlerta) param.firstElement();
          dataEntBrote = (DataBrote) param.elementAt(1);

          //1. Modifico la alerta
          sQuery = Pregunta_Modif_Alerta_Brotes();
          st = con.prepareStatement(sQuery);

          st.setString(1, dataEntAlerta.getCD_SITALERBRO().trim());
          dFecha = formater.parse(dataEntAlerta.getFC_ALERBRO().trim());
          sqlFec = new java.sql.Date(dFecha.getTime());
          st.setDate(2, sqlFec);

          st.setString(3, param.getLogin().trim());
          // duda dataEntAlerta.getCD_OPE().trim());

          dFecha = new java.util.Date();
          String sFecha = Format_Horas.format(dFecha); //string
          //partirla!!     *************
          String sfecRecuAlerta = sFecha;
          st.setTimestamp(4, cadena_a_timestamp(sFecha));
          st.setString(5, dataEntAlerta.getCD_ANO().trim());
          st.setInt(6,
                    (new Integer(dataEntAlerta.getNM_ALERBRO().trim())).intValue());

          st.executeUpdate();

          st.close();
          st = null;

          //2. Inserto el brote

          //Atencion: el brote puede estar en situacion 2
          //con una investigacion... no lo puedo borrar y despues
          //insertar porque para ICM habra muchas entidades dependientes

          //Entonces compruebo si existe:
          //    en caso afirmativo hago un update
          //    en caso negatico hago un insert
          String resultado = "";
          String queryComp = " select * from SIVE_BROTES "
              + " where CD_ANO = ? and NM_ALERBRO = ? ";
          st = con.prepareStatement(queryComp);
          st.setString(1, dataEntBrote.getCD_ANO().trim());
          st.setString(2, dataEntBrote.getNM_ALERBRO().trim());

          rs = st.executeQuery();
          if (rs.next()) {
            //resultado existe
            resultado = "S";
          }
          else {
            //resultado no existe
            resultado = "N";
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          if (resultado.equals("S")) {
            sQuery = Pregunta_Update_Brote();
          }
          else if (resultado.equals("N")) {
            sQuery = Pregunta_Insert_Brote();

          }
          st = con.prepareStatement(sQuery);

          // System_out.println("SrvBIInsert: " + sQuery.substring(0, 10));

          if (dataEntBrote.getFC_FSINPRIMC_F().trim().length() > 0) {
            if (dataEntBrote.getFC_FSINPRIMC_H().trim().length() > 0) {
              st.setTimestamp(1,
                              cadena_a_timestamp(fecha_mas_hora(dataEntBrote.getFC_FSINPRIMC_F().
                  trim(),
                  dataEntBrote.getFC_FSINPRIMC_H().trim())));
            }
            else {
              st.setTimestamp(1,
                              cadena_a_timestamp(fecha_mas_hora(dataEntBrote.getFC_FSINPRIMC_F().
                  trim(),
                  "00:00:00")));
            }
          }
          else {
            st.setNull(1, java.sql.Types.DATE);

          }
          if (dataEntBrote.getFC_ISINPRIMC_F().trim().length() > 0) {
            if (dataEntBrote.getFC_ISINPRIMC_H().trim().length() > 0) {
              st.setTimestamp(2,
                              cadena_a_timestamp(fecha_mas_hora(dataEntBrote.getFC_ISINPRIMC_F().
                  trim(),
                  dataEntBrote.getFC_ISINPRIMC_H().trim())));
            }
            else {
              st.setTimestamp(2,
                              cadena_a_timestamp(fecha_mas_hora(dataEntBrote.getFC_ISINPRIMC_F().
                  trim(),
                  "00:00:00")));
            }
          }
          else {
            st.setNull(2, java.sql.Types.DATE);

          }
          if (dataEntBrote.getNM_PERINMIN().trim().length() > 0) {
            st.setInt(3,
                      (new Integer(dataEntBrote.getNM_PERINMIN().trim())).intValue());
          }
          else {
            st.setNull(3, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getNM_PERINMAX().trim().length() > 0) {
            st.setInt(4,
                      (new Integer(dataEntBrote.getNM_PERINMAX().trim())).intValue());
          }
          else {
            st.setNull(4, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getNM_PERINMED().trim().length() > 0) {
            st.setInt(5,
                      (new Integer(dataEntBrote.getNM_PERINMED().trim())).intValue());
          }
          else {
            st.setNull(5, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getNM_ENFERMOS().trim().length() > 0) {
            st.setInt(6,
                      (new Integer(dataEntBrote.getNM_ENFERMOS().trim())).intValue());
          }
          else {
            st.setNull(6, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getFC_EXPOSICION_F().trim().length() > 0) {
            if (dataEntBrote.getFC_EXPOSICION_H().trim().length() > 0) {
              st.setTimestamp(7,
                              cadena_a_timestamp(fecha_mas_hora(dataEntBrote.getFC_EXPOSICION_F().
                  trim(),
                  dataEntBrote.getFC_EXPOSICION_H().trim())));
            }
            else {
              st.setTimestamp(7,
                              cadena_a_timestamp(fecha_mas_hora(dataEntBrote.getFC_EXPOSICION_F().
                  trim(),
                  "00:00:00")));
            }
          }
          else {
            st.setNull(7, java.sql.Types.DATE);

          }
          if (dataEntBrote.getNM_INGHOSP().trim().length() > 0) {
            st.setInt(8,
                      (new Integer(dataEntBrote.getNM_INGHOSP().trim())).intValue());
          }
          else {
            st.setNull(8, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getNM_DEFUNCION().trim().length() > 0) {
            st.setInt(9,
                      (new Integer(dataEntBrote.getNM_DEFUNCION().trim())).intValue());
          }
          else {
            st.setNull(9, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getNM_EXPUESTOS().trim().length() > 0) {
            st.setInt(10,
                      (new Integer(dataEntBrote.getNM_EXPUESTOS().trim())).intValue());
          }
          else {
            st.setNull(10, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getCD_TRANSMIS().trim().length() > 0) {
            st.setString(11, dataEntBrote.getCD_TRANSMIS().trim());
          }
          else {
            st.setNull(11, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getNM_NVACENF().trim().length() > 0) {
            st.setInt(12,
                      (new Integer(dataEntBrote.getNM_NVACENF().trim())).intValue());
          }
          else {
            st.setNull(12, java.sql.Types.INTEGER);

          }
          st.setString(13, param.getLogin().trim());
          // duda dataEntBrote.getCD_OPE().trim());

          dFecha = new java.util.Date();
          sFecha = Format_Horas.format(dFecha); //string
          //partirla!!     *************
          String sfecRecuBrote = sFecha;
          st.setTimestamp(14, cadena_a_timestamp(sFecha));

          if (dataEntBrote.getNM_DCUACMIN().trim().length() > 0) {
            st.setInt(15,
                      (new Integer(dataEntBrote.getNM_DCUACMIN().trim())).intValue());
          }
          else {
            st.setNull(15, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getNM_DCUACMED().trim().length() > 0) {
            st.setInt(16,
                      (new Integer(dataEntBrote.getNM_DCUACMED().trim())).intValue());
          }
          else {
            st.setNull(16, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getNM_DCUACMAX().trim().length() > 0) {
            st.setInt(17,
                      (new Integer(dataEntBrote.getNM_DCUACMAX().trim())).intValue());
          }
          else {
            st.setNull(17, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getNM_NVACNENF().trim().length() > 0) {
            st.setInt(18,
                      (new Integer(dataEntBrote.getNM_NVACNENF().trim())).intValue());
          }
          else {
            st.setNull(18, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getNM_VACNENF().trim().length() > 0) {
            st.setInt(19,
                      (new Integer(dataEntBrote.getNM_VACNENF().trim())).intValue());
          }
          else {
            st.setNull(19, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getNM_VACENF().trim().length() > 0) {
            st.setInt(20,
                      (new Integer(dataEntBrote.getNM_VACENF().trim())).intValue());
          }
          else {
            st.setNull(20, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getDS_OBSERV().trim().length() > 0) {
            st.setString(21, dataEntBrote.getDS_OBSERV().trim());
          }
          else {
            st.setNull(21, java.sql.Types.VARCHAR);

          }
          st.setString(22, dataEntBrote.getCD_TIPOCOL().trim());
          // System_out.println("SrvBIInsert: CD_TIPOCOL " + dataEntBrote.getCD_TIPOCOL().trim());

          if (dataEntBrote.getDS_NOMCOL().trim().length() > 0) {
            st.setString(23, dataEntBrote.getDS_NOMCOL().trim());
          }
          else {
            st.setNull(23, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getDS_DIRCOL().trim().length() > 0) {
            st.setString(24, dataEntBrote.getDS_DIRCOL().trim());
          }
          else {
            st.setNull(24, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getCD_POSTALCOL().trim().length() > 0) {
            st.setString(25, dataEntBrote.getCD_POSTALCOL().trim());
          }
          else {
            st.setNull(25, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getCD_PROVCOL().trim().length() > 0) {
            st.setString(26, dataEntBrote.getCD_PROVCOL().trim());
          }
          else {
            st.setNull(26, java.sql.Types.VARCHAR);

          }

          st.setString(27, dataEntBrote.getDS_BROTE().trim());
          // System_out.println("SrvBIInsert: DS_BROTE " + dataEntBrote.getDS_BROTE().trim());

          st.setString(28, dataEntBrote.getCD_GRUPO().trim());
          // System_out.println("SrvBIInsert: CD_GRUPO " + dataEntBrote.getCD_GRUPO().trim());

          if (dataEntBrote.getFC_FECHAHORA_H().trim().length() > 0) {
            st.setTimestamp(29,
                            cadena_a_timestamp(fecha_mas_hora(dataEntBrote.getFC_FECHAHORA_F().
                trim(),
                dataEntBrote.getFC_FECHAHORA_H().trim())));
          }
          else {
            st.setTimestamp(29,
                            cadena_a_timestamp(fecha_mas_hora(dataEntBrote.getFC_FECHAHORA_F().
                trim(),
                "00:00:00")));

          }
          if (dataEntBrote.getNM_MANIPUL().trim().length() > 0) {
            st.setInt(30,
                      (new Integer(dataEntBrote.getNM_MANIPUL().trim())).intValue());
          }
          else {
            st.setNull(30, java.sql.Types.INTEGER);

          }
          if (dataEntBrote.getCD_ZBS_LE().trim().length() > 0) {
            st.setString(31, dataEntBrote.getCD_ZBS_LE().trim());
          }
          else {
            st.setNull(31, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getCD_NIVEL_2_LE().trim().length() > 0) {
            st.setString(32, dataEntBrote.getCD_NIVEL_2_LE().trim());
          }
          else {
            st.setNull(32, java.sql.Types.VARCHAR);

          }
          st.setString(33, dataEntBrote.getCD_TNOTIF().trim());
          // System_out.println("SrvBIInsert: CD_TNOTIF " + dataEntBrote.getCD_TNOTIF().trim());

          if (dataEntBrote.getIT_RESCALC().trim().length() > 0) {
            st.setString(34, dataEntBrote.getIT_RESCALC().trim());
          }
          else {
            st.setNull(34, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getCD_MUNCOL().trim().length() > 0) {
            st.setString(35, dataEntBrote.getCD_MUNCOL().trim());
          }
          else {
            st.setNull(35, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getCD_NIVEL_1_LCA().trim().length() > 0) {
            st.setString(36, dataEntBrote.getCD_NIVEL_1_LCA().trim());
          }
          else {
            st.setNull(36, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getDS_TELCOL().trim().length() > 0) {
            st.setString(37, dataEntBrote.getDS_TELCOL().trim());
          }
          else {
            st.setNull(37, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getCD_NIVEL_1_LE().trim().length() > 0) {
            st.setString(38, dataEntBrote.getCD_NIVEL_1_LE().trim());
          }
          else {
            st.setNull(38, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getCD_NIVEL_2_LCA().trim().length() > 0) {
            st.setString(39, dataEntBrote.getCD_NIVEL_2_LCA().trim());
          }
          else {
            st.setNull(39, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getCD_ZBS_LCA().trim().length() > 0) {
            st.setString(40, dataEntBrote.getCD_ZBS_LCA().trim());
          }
          else {
            st.setNull(40, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getIT_PERIN().trim().length() > 0) {
            st.setString(41, dataEntBrote.getIT_PERIN().trim());
          }
          else {
            st.setNull(41, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getIT_DCUAC().trim().length() > 0) {
            st.setString(42, dataEntBrote.getIT_DCUAC().trim());
          }
          else {
            st.setNull(42, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getDS_NMCALLE().trim().length() > 0) {
            st.setString(43, dataEntBrote.getDS_NMCALLE().trim());
          }
          else {
            st.setNull(43, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getCD_TBROTE().trim().length() > 0) {
            st.setString(44, dataEntBrote.getCD_TBROTE().trim());
          }
          else {
            st.setNull(44, java.sql.Types.VARCHAR);

          }
          if (dataEntBrote.getDS_PISOCOL().trim().length() > 0) {
            st.setString(45, dataEntBrote.getDS_PISOCOL().trim());
          }
          else {
            st.setNull(45, java.sql.Types.VARCHAR);

          }

          st.setInt(46,
                    (new Integer(dataEntBrote.getNM_ALERBRO().trim())).intValue());
          // System_out.println("SrvBIInsert: NM_ALERBRO " + dataEntBrote.getNM_ALERBRO().trim());

          st.setString(47, dataEntBrote.getCD_ANO().trim());
          // System_out.println("SrvBIInsert: CD_ANO " + dataEntBrote.getCD_ANO().trim());

          st.executeUpdate();

          st.close();
          st = null;

          //3. Se da de alta el investigador
          //No para PISTA, si para ICM
          /*
                 sQuery = Pregunta_Insert_Investigador();
                 st = con.prepareStatement(sQuery);
                 //FC_ALTA,  NM_ALERBRO, CD_ANO, CD_USUARIO
                 st.setString(1, dataEntAlerta.getCD_SITALERBRO().trim());
                 dFecha = formater.parse(dataEntAlerta.getFC_ALERBRO().trim());
                 sqlFec = new java.sql.Date(dFecha.getTime());
                 st.setDate(2, sqlFec);
                 st.setString(3, param.getLogin().trim());
                 // duda dataEntAlerta.getCD_OPE().trim());
                 dFecha = new java.util.Date();
                 String sFecha = Format_Horas.format(dFecha);//string
                 //partirla!!     *************
                 String sfecRecuAlerta = sFecha;
                 st.setTimestamp(4, cadena_a_timestamp(sFecha));
                 st.setString(5, dataEntAlerta.getCD_ANO().trim());
               st.setInt(6,(new Integer(dataEntAlerta.getNM_ALERBRO().trim())).intValue() );
                 st.executeUpdate();
                 st.close();
                 st = null;
           */

          //PREPARAR LA LISTASALIDA con DataAlerta y DataBrote
          dataSalAlerta = dataEntAlerta;
          dataSalBrote = dataEntBrote;
          dataSalAlerta.insert("CD_OPE", param.getLogin().trim());
          dataSalAlerta.insert("FC_ULTACT", sfecRecuAlerta);
          dataSalBrote.insert("CD_OPE", param.getLogin().trim());
          dataSalBrote.insert("FC_ULTACT", sfecRecuBrote);

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    listaSalida.addElement(dataSalAlerta);
    listaSalida.addElement(dataSalBrote);

    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;
  } //fin doWork

  public String Pregunta_Modif_Alerta_Brotes() {

    return " update SIVE_ALERTA_BROTES "
        + " set CD_SITALERBRO = ? "
        + " , FC_ALERBRO = ? , CD_OPE = ?, FC_ULTACT = ? "
        + " where CD_ANO = ? and NM_ALERBRO = ? ";
  }

  public String Pregunta_Insert_Brote() {

    return ("insert into SIVE_BROTES " //
            + " ( FC_FSINPRIMC, FC_ISINPRIMC, "
            + " NM_PERINMIN, NM_PERINMAX, NM_PERINMED, "
            + " NM_ENFERMOS, FC_EXPOSICION, NM_INGHOSP, "
            + " NM_DEFUNCION, NM_EXPUESTOS, "
            + " CD_TRANSMIS, NM_NVACENF, "
            + " CD_OPE, FC_ULTACT, "
            + " NM_DCUACMIN, NM_DCUACMED, NM_DCUACMAX, "
            + " NM_NVACNENF, NM_VACNENF, NM_VACENF, "
            + " DS_OBSERV, CD_TIPOCOL, DS_NOMCOL, DS_DIRCOL, "
            + " CD_POSTALCOL, CD_PROVCOL, "
            + " DS_BROTE, CD_GRUPO, FC_FECHAHORA, NM_MANIPUL, "
            + " CD_ZBS_LE, CD_NIVEL_2_LE, CD_TNOTIF, IT_RESCALC, "
            + " CD_MUNCOL, CD_NIVEL_1_LCA, DS_TELCOL, CD_NIVEL_1_LE, "
            + " CD_NIVEL_2_LCA, CD_ZBS_LCA, "
            +
        " IT_PERIN, IT_DCUAC, DS_NMCALLE, CD_TBROTE, DS_PISOCOL, NM_ALERBRO, CD_ANO) "
            + "values ( ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,  ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,  ?,?,?,?,?, ?,?,?,?,?, ?,?)");
  }

  public String Pregunta_Update_Brote() {

    return (" update SIVE_BROTES "
            + " set FC_FSINPRIMC = ? , FC_ISINPRIMC = ? , "
            + " NM_PERINMIN = ? , NM_PERINMAX = ? , NM_PERINMED = ? , "
            + " NM_ENFERMOS = ? , FC_EXPOSICION = ? , NM_INGHOSP = ? , "
            + " NM_DEFUNCION = ? , NM_EXPUESTOS = ? , " //10
            + " CD_TRANSMIS = ? , NM_NVACENF = ? , "
            + " CD_OPE = ? , FC_ULTACT = ? , "
            + " NM_DCUACMIN = ? , NM_DCUACMED = ? , NM_DCUACMAX = ? , "
            + " NM_NVACNENF = ? , NM_VACNENF = ? , NM_VACENF = ? , " //20
            +
        " DS_OBSERV = ? , CD_TIPOCOL = ? , DS_NOMCOL = ? , DS_DIRCOL = ? , "
            + " CD_POSTALCOL = ? , CD_PROVCOL = ? , "
            +
        " DS_BROTE = ? , CD_GRUPO = ? , FC_FECHAHORA = ? , NM_MANIPUL = ? , " //30
            +
        " CD_ZBS_LE = ? , CD_NIVEL_2_LE = ? , CD_TNOTIF = ? , IT_RESCALC = ? , "
            +
        " CD_MUNCOL = ? , CD_NIVEL_1_LCA = ? , DS_TELCOL = ? , CD_NIVEL_1_LE = ? , "
            + " CD_NIVEL_2_LCA = ? , CD_ZBS_LCA = ? , " //40
            +
        " IT_PERIN = ? , IT_DCUAC = ? , DS_NMCALLE = ? , CD_TBROTE = ?, DS_PISOCOL = ? " //45
            + " where NM_ALERBRO = ? and CD_ANO = ? "); //47

  }

  public String Pregunta_Insert_Investigador() {

    return ("insert into SIVE_INVESTIGADOR_BROTE " //
            + " ( FC_ALTA,  NM_ALERBRO, CD_ANO, CD_USUARIO ) "
            + "values ( ?,?,?,? )");
  }

} //fin de la clase
