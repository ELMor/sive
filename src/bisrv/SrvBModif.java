//Title:        Notificacion datos adicionales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Notificacion de enfermedades EDO de declaracion numerica
//que necesitan datos adicionales.

package bisrv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Vector;

import bidata.DataAlerta;
import capp.CLista;
import sapp.DBServlet;

public class SrvBModif
    extends DBServlet {

  final int servletCOMP_MOD = 3; // modificacion con comprobacion
  final int servletCOMP_BORR = 4; // borrado con comprobacion
  final int servletMODIFICACION = 5; // modificacion
  final int servletBORRADO = 6; //borrado

  final String sQuery = "SELECT CD_NIVEL_1, CD_NIVEL_2, "
      + " CD_CA , IT_OK FROM SIVE_MODELO WHERE "
      + " CD_TSIVE = ? AND CD_MODELO = ?";

  final String sALTA_CAB = "insert into SIVE_EDO_DADIC "
      + "(CD_ENFCIE, SEQ_CODIGO, CD_E_NOTIF, CD_ANOEPI, CD_SEMEPI, "
      + " FC_FECNOTIF, FC_RECEP, CD_OPE, FC_ULTACT) "
      + " values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

  final String sALTA_RESP = "insert into SIVE_RESP_ADIC "
      + "(CD_ENFCIE, CD_E_NOTIF, CD_ANOEPI, CD_TSIVE, CD_SEMEPI, "
      + " FC_FECNOTIF, FC_RECEP, CD_MODELO, SEQ_CODIGO, "
      + " NM_LIN, CD_PREGUNTA, DS_RESPUESTA) "
      + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

  final String sBORRAR_RESP = "DELETE FROM SIVE_RESP_ADIC "
      + " WHERE ( CD_ENFCIE = ? and CD_E_NOTIF = ? and "
      + " CD_ANOEPI = ? and CD_TSIVE = ? and CD_SEMEPI = ? "
      + " and FC_FECNOTIF = ? and FC_RECEP = ? and "
      + " SEQ_CODIGO = ? )";

  final String sBORRAR_RESP_TODOS = "DELETE FROM SIVE_RESP_ADIC "
      + " WHERE ( CD_ENFCIE = ? and CD_E_NOTIF = ? and "
      + " CD_ANOEPI = ? and CD_TSIVE = ? and CD_SEMEPI = ? "
      + " and FC_FECNOTIF = ? and FC_RECEP = ? )";

  final String sBORRAR_CAB = "DELETE FROM SIVE_EDO_DADIC "
      + " WHERE ( CD_ENFCIE = ? and SEQ_CODIGO = ? and "
      + " CD_E_NOTIF = ? and  CD_ANOEPI = ? and CD_SEMEPI = ? "
      + " and FC_FECNOTIF = ? and FC_RECEP = ? )";

  final String sSELECT_VALORES = " select "
      + " a.DS_LISTAP, a.DSL_LISTAP "
      + "from SIVE_LISTA_VALORES a, SIVE_PREGUNTA b where "
      + "(b.CD_PREGUNTA = ? and a.CD_LISTAp = ? "
      + " and a.CD_LISTA = b.CD_LISTA)";

  //fechas
  java.util.Date dFecha;
  java.sql.Date sqlFec;
  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
  SimpleDateFormat formUltAct = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
      new Locale("es", "ES"));
  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    //fechas
    dFecha = new java.util.Date();
    sqlFec = new java.sql.Date(dFecha.getTime());
    formater = new SimpleDateFormat("dd/MM/yyyy");
    formUltAct = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",
                                      new Locale("es", "ES"));

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;

    int iEstado;

    int j = 0;

    // objetos de datos
    CLista data = new CLista();
    Vector listaPartesAct = new Vector();
    DataAlerta alerta = null;

    // objetos de datos
    CLista listaSalida = new CLista();

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    try {
      // modos de operaci�n
      switch (opmode) {

        //Consulta de los partes de una declaracion
        //MODIFICAR******************************************
        case servletCOMP_MOD:

          //va bien: listaSalida = (true, DataAlerta) o
          //         listaSalida = (true, mensaje pregunta) o
          //va mal: listaSalida = null y exc

          //param(0): DataAlerta con la alerta que se desea modificar

          alerta = (DataAlerta) param.elementAt(0);

          // System_out.println("SrvGui ano alerta " + alerta.getCD_ANO () );
          // System_out.println("SrvGui nm alerta " + alerta.getNM_ALERBRO () );
          // System_out.println("SrvGui login " + param.getLogin() );

          // Comprobar si la alerta que se esta modificando
          // ha sido borrado o modificado
          int aux = -1;

          // System_out.println("SrvGui: antes de comprobarModBor ");
          aux = comprobarModBor(alerta, con, st, rs, param.getLogin());
          if (aux == 0) {
            // System_out.println("SrvGui: ha sido modificado o borrado ");
            //se lanza el select con la clave para ver si la alerta
            //ha sido modificado o borrado
            String resMod = modificado_o_borrado(alerta, con, st, rs);
            if (resMod.equals("M")) {
              // System_out.println("SrvGui: ha sido modificado  ");
              //resultado modificado
              listaSalida.addElement(new Boolean(true));
              listaSalida.addElement("Mientras se alteraba el contenido de la alerta ha sido modificada por otro usuario. �Desea sobreescribir sus cambios?");
            }
            else if (resMod.equals("B")) {
              // System_out.println("SrvGui: ha sido borrado ");
              //resultado borrado
              String mens = "Mientras se alteraba el contenido del parte ha sido borrado por otro usuario. �Desea guardar los cambios?";
              listaSalida.addElement(new Boolean(true));
              listaSalida.addElement(mens);
            }
          } // la alerta habia sido modificado o borrado
          else {
            // System_out.println("SrvGui: NO ha sido modificado o borrado ");
            //MODIFICACION

            // System_out.println("SrvGui: antes de modificacionAlerta");
            alerta = modificacionAlerta(alerta, con, st, rs, param.getLogin());
            // System_out.println("SrvGui: despues de modificacionAlerta");

            listaSalida.addElement(new Boolean(true));
            listaSalida.addElement(alerta);
          } //la alerta no habia sido modificado ni borrado

          break;

        case servletCOMP_BORR:

          //param(0): DataAlerta que se va a borrar

          //va bien: listaSalida = (true) o
          //         listaSalida = (true, mensaje pregunta)
          //va mal: listaSalida = null y exc

          alerta = (DataAlerta) param.elementAt(0);

          // Comprobar si la alerta que se va a borrar ha sido borrado
          // o modificado

          aux = -1;
          aux = comprobarModBor(alerta, con, st, rs, param.getLogin());
          if (aux == 0) {
            //no se ha actualizado ninguna tabla de alerta
            //se lanza el select con la clave para ver si la alerta
            //ha sido modificado o borrado

            String resMod = modificado_o_borrado(alerta, con, st, rs);
            if (resMod.equals("M")) {
              //resultado modificado
              listaSalida.addElement(new Boolean(true));
              listaSalida.addElement("La alerta ha sido modificada por otro usuario. �Desea borrarla descartando as� la �ltima modificaci�n?");
            }
            else if (resMod.equals("B")) {
              //resultado: ya habia sido borrado
              listaSalida.addElement(new Boolean(true));
            }
          } // la alerta habia sido modificado o borrado
          else {
            //BORRADO

            //la alerta_brotes se ha modificado en la comprobacion
            borradoAlerta(alerta, con, st, rs);
            listaSalida.addElement(new Boolean(true));

          }
          break;

        case servletBORRADO:

          //param(0): alerta que se va a borrar

          //va bien: listaSalida.size()=0
          //va mal: listaSalida = null y exc

          alerta = (DataAlerta) param.elementAt(0);
          //BORRADO
          borradoAlerta(alerta, con, st, rs);

          break;

        case servletMODIFICACION:

          //param(0): alerta que se va a modificar

          //va bien: listaSalida = (alerta)
          //va mal: listaSalida = null y exc

          alerta = (DataAlerta) param.elementAt(0);
          //MODIFICACION
          //se actualiza sive_alerta_brotes

          //cuando se modifica se borra y se inserta (por si acaso
          //el registro habia sido borrado)
          alerta = borradoInsercionAlerta(alerta, con, st, rs, param.getLogin());

          listaSalida.addElement(alerta);

          break;

      } // fin del switch para los modos de consulta

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;

      throw ex;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    if (listaSalida != null) {
      listaSalida.trimToSize();

    }
    return listaSalida;

  }

  private String fecha_mas_hora(String f, String h) {
    f = f.trim();
    h = h.trim();
    // System_out.println("SRVBINSERT: fecha " + f);
    // System_out.println("SRVBINSERT: hora " + h);
    // System_out.println("SRVBINSERT: fecha_mas_hora " +f + " " + h + ":00");
    //return f + " " + h.substring(0, h.lastIndexOf(':'));
    return f + " " + h + ":00";
  }

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    //java.sql.Timestamp TSFec = new java.sql.Timestamp(dFecha.getTime());
    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);

    return TSFec;
  }

  private DataAlerta modificacionAlerta(DataAlerta alerta,
                                        Connection con,
                                        PreparedStatement st,
                                        ResultSet rs,
                                        String login) throws Exception {

    String queryMod = " update SIVE_ALERTA_ADIC "
        + " set CD_MINPROV = ? , CD_MINMUN = ? "
        + " , CD_TNOTIF = ? , DS_NOTIFINST = ? "
        + " , CD_NOTIFPROV = ? , DS_NOTIFICADOR = ? "
        + " , DS_NOTIFDIREC = ? , CD_NOTIFMUN = ? "
        + " , CD_NOTIFPOSTAL = ? , DS_NNMCALLE = ? "
        + " , DS_NOTIFTELEF = ? , DS_MINFPER = ? "
        + " , DS_MINFDIREC = ? , CD_MINFPOSTAL = ? "
        + " , DS_MINFTELEF = ? , DS_MNMCALLE = ? "
        + " , DS_OBSERV = ? , DS_ALISOSP = ? "
        + " , DS_SINTOMAS = ? , DS_NOTIFPISO = ? , DS_MINFPISO = ? "
        + " where CD_ANO = ? and NM_ALERBRO = ? ";
    st = con.prepareStatement(queryMod);

    // System_out.println("srvGuichi: alerta.getCD_MINPROV() " + alerta.getCD_MINPROV());
    if (alerta.getCD_MINPROV().trim().length() > 0) {
      st.setString(1, alerta.getCD_MINPROV().trim());
    }
    else {
      st.setNull(1, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getCD_MINMUN() " + alerta.getCD_MINMUN());
    }
    if (alerta.getCD_MINMUN().trim().length() > 0) {
      st.setString(2, alerta.getCD_MINMUN().trim());
    }
    else {
      st.setNull(2, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getCD_TNOTIF() " + alerta.getCD_TNOTIF());
    }
    st.setString(3, alerta.getCD_TNOTIF().trim());
    // System_out.println("srvGuichi: alerta.getDS_NOTIFINST() " + alerta.getDS_NOTIFINST());
    if (alerta.getDS_NOTIFINST().trim().length() > 0) {
      st.setString(4, alerta.getDS_NOTIFINST().trim());
    }
    else {
      st.setNull(4, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getCD_NOTIFPROV() " + alerta.getCD_NOTIFPROV());
    }
    if (alerta.getCD_NOTIFPROV().trim().length() > 0) {
      st.setString(5, alerta.getCD_NOTIFPROV().trim());
    }
    else {
      st.setNull(5, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_NOTIFICADOR() " + alerta.getDS_NOTIFICADOR());
    }
    if (alerta.getDS_NOTIFICADOR().trim().length() > 0) {
      st.setString(6, alerta.getDS_NOTIFICADOR().trim());
    }
    else {
      st.setNull(6, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_NOTIFDIREC() " + alerta.getDS_NOTIFDIREC());
    }
    if (alerta.getDS_NOTIFDIREC().trim().length() > 0) {
      st.setString(7, alerta.getDS_NOTIFDIREC().trim());
    }
    else {
      st.setNull(7, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getCD_NOTIFMUN() " + alerta.getCD_NOTIFMUN());
    }
    if (alerta.getCD_NOTIFMUN().trim().length() > 0) {
      st.setString(8, alerta.getCD_NOTIFMUN().trim());
    }
    else {
      st.setNull(8, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getCD_NOTIFPOSTAL() " + alerta.getCD_NOTIFPOSTAL());
    }
    if (alerta.getCD_NOTIFPOSTAL().trim().length() > 0) {
      st.setString(9, alerta.getCD_NOTIFPOSTAL().trim());
    }
    else {
      st.setNull(9, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_NNMCALLE() " + alerta.getDS_NNMCALLE());
    }
    if (alerta.getDS_NNMCALLE().trim().length() > 0) {
      st.setString(10, alerta.getDS_NNMCALLE().trim());
    }
    else {
      st.setNull(10, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_NOTIFTELEF() " + alerta.getDS_NOTIFTELEF());
    }
    if (alerta.getDS_NOTIFTELEF().trim().length() > 0) {
      st.setString(11, alerta.getDS_NOTIFTELEF().trim());
    }
    else {
      st.setNull(11, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_MINFPER() " + alerta.getDS_MINFPER());
    }
    if (alerta.getDS_MINFPER().trim().length() > 0) {
      st.setString(12, alerta.getDS_MINFPER().trim());
    }
    else {
      st.setNull(12, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_MINFDIREC() " + alerta.getDS_MINFDIREC());
    }
    if (alerta.getDS_MINFDIREC().trim().length() > 0) {
      st.setString(13, alerta.getDS_MINFDIREC().trim());
    }
    else {
      st.setNull(13, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getCD_MINFPOSTAL() " + alerta.getCD_MINFPOSTAL());
    }
    if (alerta.getCD_MINFPOSTAL().trim().length() > 0) {
      st.setString(14, alerta.getCD_MINFPOSTAL().trim());
    }
    else {
      st.setNull(14, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_MINFTELEF() " + alerta.getDS_MINFTELEF());
    }
    if (alerta.getDS_MINFTELEF().trim().length() > 0) {
      st.setString(15, alerta.getDS_MINFTELEF().trim());
    }
    else {
      st.setNull(15, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_MNMCALLE() " + alerta.getDS_MNMCALLE());
    }
    if (alerta.getDS_MNMCALLE().trim().length() > 0) {
      st.setString(16, alerta.getDS_MNMCALLE().trim());
    }
    else {
      st.setNull(16, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_OBSERV() " + alerta.getDS_OBSERV());
    }
    if (alerta.getDS_OBSERV().trim().length() > 0) {
      st.setString(17, alerta.getDS_OBSERV().trim());
    }
    else {
      st.setNull(17, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_ALISOSP() " + alerta.getDS_ALISOSP());
    }
    if (alerta.getDS_ALISOSP().trim().length() > 0) {
      st.setString(18, alerta.getDS_ALISOSP().trim());
    }
    else {
      st.setNull(18, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_SINTOMAS() " + alerta.getDS_SINTOMAS());
    }
    if (alerta.getDS_SINTOMAS().trim().length() > 0) {
      st.setString(19, alerta.getDS_SINTOMAS().trim());
    }
    else {
      st.setNull(19, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_NOTIFPISO() " + alerta.getDS_NOTIFPISO());
    }
    if (alerta.getDS_NOTIFPISO().trim().length() > 0) {
      st.setString(20, alerta.getDS_NOTIFPISO().trim());
    }
    else {
      st.setNull(20, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_MINFPISO() " + alerta.getDS_MINFPISO());
    }
    if (alerta.getDS_MINFPISO().trim().length() > 0) {
      st.setString(21, alerta.getDS_MINFPISO().trim());
    }
    else {
      st.setNull(21, java.sql.Types.VARCHAR);

    }
    st.setString(22, alerta.getCD_ANO().trim());
    st.setInt(23, new Integer(alerta.getNM_ALERBRO().trim()).intValue());

    st.executeUpdate();

    st.close();
    st = null;

    // System_out.println("srvGuichi: despues de ADIC ");

    queryMod = " update SIVE_ALERTA_COLEC "
        + " set CD_PROV = ? , CD_MUN = ? "
        + " , DS_NOMCOL = ? , DS_DIRCOL = ? "
        + " , DS_NMCALLE = ? , CD_POSTALCOL = ? "
        + " , DS_TELCOL = ? , DS_PISOCOL = ? "
        + " where CD_ANO = ? and NM_ALERBRO = ? ";
    st = con.prepareStatement(queryMod);

    // System_out.println("srvGuichi: alerta.getCD_PROV() " + alerta.getCD_PROV());
    if (alerta.getCD_PROV().trim().length() > 0) {
      st.setString(1, alerta.getCD_PROV().trim());
    }
    else {
      st.setNull(1, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getCD_MUN() " + alerta.getCD_MUN());
    }
    if (alerta.getCD_MUN().trim().length() > 0) {
      st.setString(2, alerta.getCD_MUN().trim());
    }
    else {
      st.setNull(2, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getDS_NOMCOL() " + alerta.getDS_NOMCOL());
    }
    if (alerta.getDS_NOMCOL().trim().length() > 0) {
      st.setString(3, alerta.getDS_NOMCOL().trim());
    }
    else {
      st.setNull(3, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getDS_DIRCOL() " + alerta.getDS_DIRCOL());
    }
    if (alerta.getDS_DIRCOL().trim().length() > 0) {
      st.setString(4, alerta.getDS_DIRCOL().trim());
    }
    else {
      st.setNull(4, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getDS_NMCALLE() " + alerta.getDS_NMCALLE());
    }
    if (alerta.getDS_NMCALLE().trim().length() > 0) {
      st.setString(5, alerta.getDS_NMCALLE().trim());
    }
    else {
      st.setNull(5, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getCD_POSTALCOL() " + alerta.getCD_POSTALCOL());
    }
    if (alerta.getCD_POSTALCOL().trim().length() > 0) {
      st.setString(6, alerta.getCD_POSTALCOL().trim());
    }
    else {
      st.setNull(6, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getDS_TELCOL() " + alerta.getDS_TELCOL());
    }
    if (alerta.getDS_TELCOL().trim().length() > 0) {
      st.setString(7, alerta.getDS_TELCOL().trim());
    }
    else {
      st.setNull(7, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getDS_PISOCOL() " + alerta.getDS_PISOCOL());
    }
    if (alerta.getDS_PISOCOL().trim().length() > 0) {
      st.setString(8, alerta.getDS_PISOCOL().trim());
    }
    else {
      st.setNull(8, java.sql.Types.VARCHAR);

    }
    st.setString(9, alerta.getCD_ANO().trim());
    st.setInt(10, new Integer(alerta.getNM_ALERBRO().trim()).intValue());

    st.executeUpdate();

    st.close();
    st = null;

    String queryComp = " update SIVE_ALERTA_BROTES "
        + " set CD_OPE = ?, FC_ULTACT = ? "
        + " , CD_LEXPROV = ? , CD_LEXMUN = ? "
        + " , FC_FECHAHORA = ? , CD_NIVEL_1_EX = ? "
        + " , CD_NIVEL_2_EX = ? , CD_ZBS_EX = ? "
        + " , CD_NIVEL_1 = ? "
        + " , CD_NIVEL_2 = ? , CD_E_NOTIF = ? "
        + " , DS_ALERTA = ? , CD_GRUPO = ? "
        + " , NM_EXPUESTOS = ? "
        + " , NM_ENFERMOS = ? , NM_INGHOSP = ? "
        + " , FC_EXPOSICION = ? , FC_INISINTOMAS = ? "
        + " , DS_LEXP = ? "
        + " , DS_LEXPDIREC = ? , CD_LEXPPOST = ? "
        + " , DS_LNMCALLE = ? , DS_LEXPTELEF = ? "
        + " , CD_SITALERBRO = ? "
        + " , FC_ALERBRO = ? , IT_VALIDADA = ? "
        + " , FC_FECVALID = ? , DS_LEXPPISO = ? "
        + " where CD_ANO = ? and NM_ALERBRO = ? ";

    st = con.prepareStatement(queryComp);

    //operador actual
    st.setString(1, login);
    // System_out.println("SrvGui: login " +login);
    //la fecha de ultima actualizacion no es un date
    //es un timestamp, fecha actual
    dFecha = new java.util.Date();
    String sFecha = formUltAct.format(dFecha); //string
    // System_out.println("SrvGui: sFecha " +sFecha);
    java.sql.Timestamp ts = cadena_a_timestamp(sFecha);
    st.setTimestamp(2, ts);

    if (alerta.getCD_LEXPROV().trim().length() > 0) {
      st.setString(3, alerta.getCD_LEXPROV().trim());
    }
    else {
      st.setNull(3, java.sql.Types.VARCHAR);
    }
    if (alerta.getCD_LEXMUN().trim().length() > 0) {
      st.setString(4, alerta.getCD_LEXMUN().trim());
    }
    else {
      st.setNull(4, java.sql.Types.VARCHAR);
    }
    st.setTimestamp(5,
                    cadena_a_timestamp(fecha_mas_hora(alerta.getFC_FECHAHORA_F().
        trim(),
        alerta.getFC_FECHAHORA_H().trim())));
    st.setString(6, alerta.getCD_NIVEL_1_EX().trim());
    if (alerta.getCD_NIVEL_2_EX().trim().length() > 0) {
      st.setString(7, alerta.getCD_NIVEL_2_EX().trim());
    }
    else {
      st.setNull(7, java.sql.Types.VARCHAR);
    }
    if (alerta.getCD_ZBS_EX().trim().length() > 0) {
      st.setString(8, alerta.getCD_ZBS_EX().trim());
    }
    else {
      st.setNull(8, java.sql.Types.VARCHAR);
    }
    st.setString(9, alerta.getCD_NIVEL_1().trim());
    if (alerta.getCD_NIVEL_2().trim().length() > 0) {
      st.setString(10, alerta.getCD_NIVEL_2().trim());
    }
    else {
      st.setNull(10, java.sql.Types.VARCHAR);
    }
    if (alerta.getCD_E_NOTIF().trim().length() > 0) {
      st.setString(11, alerta.getCD_E_NOTIF().trim());
    }
    else {
      st.setNull(11, java.sql.Types.VARCHAR);
    }
    st.setString(12, alerta.getDS_ALERTA().trim());
    st.setString(13, alerta.getCD_GRUPO().trim());

    // System_out.println("SrvGui: hasta el 15");

    if (alerta.getNM_EXPUESTOS().trim().length() > 0) {
      st.setInt(14, (new Integer(alerta.getNM_EXPUESTOS().trim())).intValue());
    }
    else {
      st.setNull(14, java.sql.Types.INTEGER);
    }
    if (alerta.getNM_ENFERMOS().trim().length() > 0) {
      st.setInt(15, (new Integer(alerta.getNM_ENFERMOS().trim())).intValue());
    }
    else {
      st.setNull(15, java.sql.Types.INTEGER);
    }
    if (alerta.getNM_INGHOSP().trim().length() > 0) {
      st.setInt(16, (new Integer(alerta.getNM_INGHOSP().trim())).intValue());
    }
    else {
      st.setNull(16, java.sql.Types.INTEGER);
    }
    if (alerta.getFC_EXPOSICION_F().trim().length() > 0) {
      if (alerta.getFC_EXPOSICION_H().trim().length() > 0) {
        st.setTimestamp(17,
                        cadena_a_timestamp(fecha_mas_hora(alerta.getFC_EXPOSICION_F().
            trim(),
            alerta.getFC_EXPOSICION_H().trim())));
      }
      else {
        st.setTimestamp(17,
                        cadena_a_timestamp(fecha_mas_hora(alerta.getFC_EXPOSICION_F().
            trim(),
            "00:00:00")));
      }
    }
    else {
      st.setNull(17, java.sql.Types.DATE);
    }
    if (alerta.getFC_INISINTOMAS_F().trim().length() > 0) {
      if (alerta.getFC_INISINTOMAS_H().trim().length() > 0) {
        st.setTimestamp(18,
                        cadena_a_timestamp(fecha_mas_hora(alerta.getFC_INISINTOMAS_F().
            trim(),
            alerta.getFC_INISINTOMAS_H().trim())));
      }
      else {
        st.setTimestamp(18,
                        cadena_a_timestamp(fecha_mas_hora(alerta.getFC_INISINTOMAS_F().
            trim(),
            "00:00:00")));
      }
    }
    else {
      st.setNull(18, java.sql.Types.DATE);
    }
    if (alerta.getDS_LEXP().trim().length() > 0) {
      st.setString(19, alerta.getDS_LEXP().trim());
    }
    else {
      st.setNull(19, java.sql.Types.VARCHAR);
    }
    if (alerta.getDS_LEXPDIREC().trim().length() > 0) {
      st.setString(20, alerta.getDS_LEXPDIREC().trim());
    }
    else {
      st.setNull(20, java.sql.Types.VARCHAR);
    }
    if (alerta.getCD_LEXPPOST().trim().length() > 0) {
      st.setString(21, alerta.getCD_LEXPPOST().trim());
    }
    else {
      st.setNull(21, java.sql.Types.VARCHAR);
    }
    if (alerta.getDS_LNMCALLE().trim().length() > 0) {
      st.setString(22, alerta.getDS_LNMCALLE().trim());
    }
    else {
      st.setNull(22, java.sql.Types.VARCHAR);
    }
    if (alerta.getDS_LEXPTELEF().trim().length() > 0) {
      st.setString(23, alerta.getDS_LEXPTELEF().trim());
    }
    else {
      st.setNull(23, java.sql.Types.VARCHAR);
    }
    st.setString(24, alerta.getCD_SITALERBRO().trim());
    dFecha = formater.parse(alerta.getFC_ALERBRO().trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(25, sqlFec);
    if (alerta.getIT_VALIDADA().trim().length() > 0) {
      st.setString(26, alerta.getIT_VALIDADA().trim());
    }
    else {
      st.setNull(26, java.sql.Types.VARCHAR);
    }
    if (alerta.getFC_FECVALID().trim().length() > 0) {
      dFecha = formater.parse(alerta.getFC_FECVALID().trim());
      sqlFec = new java.sql.Date(dFecha.getTime());
      st.setDate(27, sqlFec);
    }
    else {
      st.setNull(27, java.sql.Types.DATE);
    }
    if (alerta.getDS_LEXPPISO().trim().length() > 0) {
      st.setString(28, alerta.getDS_LEXPPISO().trim());
    }
    else {
      st.setNull(28, java.sql.Types.VARCHAR);

    }

    st.setString(29, alerta.getCD_ANO().trim());
    // System_out.println("SrvGui: alerta.getCD_ANO() " + alerta.getCD_ANO());
    st.setInt(30, new Integer(alerta.getNM_ALERBRO().trim()).intValue());
    // System_out.println("SrvGui: alerta.getNM_ALERBRO() " + alerta.getNM_ALERBRO());
    st.executeUpdate();
    st.close();
    st = null;

    alerta.remove("CD_OPE");
    alerta.insert("CD_OPE", login);
    alerta.remove("FC_ULTACT");
    alerta.insert("FC_ULTACT", sFecha);
    // System_out.println("SrvGui:se ha hecho la modificacion ");

    return alerta;
  }

  private DataAlerta borradoInsercionAlerta(DataAlerta alerta,
                                            Connection con,
                                            PreparedStatement st,
                                            ResultSet rs,
                                            String login) throws Exception {

    borradoAlerta(alerta, con, st, rs);
    String queryMod = "insert into SIVE_ALERTA_ADIC " //23
        + "(CD_ANO, NM_ALERBRO, "
        + " CD_MINPROV, CD_MINMUN,  "
        + " CD_TNOTIF, DS_NOTIFINST, "
        + " CD_NOTIFPROV, DS_NOTIFICADOR, DS_NOTIFDIREC, "
        + " CD_NOTIFMUN, CD_NOTIFPOSTAL, DS_NNMCALLE, "
        + " DS_NOTIFTELEF, DS_MINFPER, DS_MINFDIREC, CD_MINFPOSTAL,  "
        + " DS_MINFTELEF,  DS_MNMCALLE, DS_OBSERV, "
        + " DS_ALISOSP,  DS_SINTOMAS, "
        + " DS_NOTIFPISO, DS_MINFPISO "
        + " ) "
        + "values ( ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,? )";
    st = con.prepareStatement(queryMod);

    // System_out.println("srvGuichi: alerta.getCD_MINPROV() " + alerta.getCD_MINPROV());
    st.setString(1, alerta.getCD_ANO().trim());
    st.setInt(2, new Integer(alerta.getNM_ALERBRO().trim()).intValue());

    if (alerta.getCD_MINPROV().trim().length() > 0) {
      st.setString(3, alerta.getCD_MINPROV().trim());
    }
    else {
      st.setNull(3, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getCD_MINMUN() " + alerta.getCD_MINMUN());
    }
    if (alerta.getCD_MINMUN().trim().length() > 0) {
      st.setString(4, alerta.getCD_MINMUN().trim());
    }
    else {
      st.setNull(4, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getCD_TNOTIF() " + alerta.getCD_TNOTIF());
    }
    st.setString(5, alerta.getCD_TNOTIF().trim());
    // System_out.println("srvGuichi: alerta.getDS_NOTIFINST() " + alerta.getDS_NOTIFINST());
    if (alerta.getDS_NOTIFINST().trim().length() > 0) {
      st.setString(6, alerta.getDS_NOTIFINST().trim());
    }
    else {
      st.setNull(6, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getCD_NOTIFPROV() " + alerta.getCD_NOTIFPROV());
    }
    if (alerta.getCD_NOTIFPROV().trim().length() > 0) {
      st.setString(7, alerta.getCD_NOTIFPROV().trim());
    }
    else {
      st.setNull(7, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_NOTIFICADOR() " + alerta.getDS_NOTIFICADOR());
    }
    if (alerta.getDS_NOTIFICADOR().trim().length() > 0) {
      st.setString(8, alerta.getDS_NOTIFICADOR().trim());
    }
    else {
      st.setNull(8, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_NOTIFDIREC() " + alerta.getDS_NOTIFDIREC());
    }
    if (alerta.getDS_NOTIFDIREC().trim().length() > 0) {
      st.setString(9, alerta.getDS_NOTIFDIREC().trim());
    }
    else {
      st.setNull(9, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getCD_NOTIFMUN() " + alerta.getCD_NOTIFMUN());
    }
    if (alerta.getCD_NOTIFMUN().trim().length() > 0) {
      st.setString(10, alerta.getCD_NOTIFMUN().trim());
    }
    else {
      st.setNull(10, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getCD_NOTIFPOSTAL() " + alerta.getCD_NOTIFPOSTAL());
    }
    if (alerta.getCD_NOTIFPOSTAL().trim().length() > 0) {
      st.setString(11, alerta.getCD_NOTIFPOSTAL().trim());
    }
    else {
      st.setNull(11, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_NNMCALLE() " + alerta.getDS_NNMCALLE());
    }
    if (alerta.getDS_NNMCALLE().trim().length() > 0) {
      st.setString(12, alerta.getDS_NNMCALLE().trim());
    }
    else {
      st.setNull(12, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_NOTIFTELEF() " + alerta.getDS_NOTIFTELEF());
    }
    if (alerta.getDS_NOTIFTELEF().trim().length() > 0) {
      st.setString(13, alerta.getDS_NOTIFTELEF().trim());
    }
    else {
      st.setNull(13, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_MINFPER() " + alerta.getDS_MINFPER());
    }
    if (alerta.getDS_MINFPER().trim().length() > 0) {
      st.setString(14, alerta.getDS_MINFPER().trim());
    }
    else {
      st.setNull(14, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_MINFDIREC() " + alerta.getDS_MINFDIREC());
    }
    if (alerta.getDS_MINFDIREC().trim().length() > 0) {
      st.setString(15, alerta.getDS_MINFDIREC().trim());
    }
    else {
      st.setNull(15, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getCD_MINFPOSTAL() " + alerta.getCD_MINFPOSTAL());
    }
    if (alerta.getCD_MINFPOSTAL().trim().length() > 0) {
      st.setString(16, alerta.getCD_MINFPOSTAL().trim());
    }
    else {
      st.setNull(16, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_MINFTELEF() " + alerta.getDS_MINFTELEF());
    }
    if (alerta.getDS_MINFTELEF().trim().length() > 0) {
      st.setString(17, alerta.getDS_MINFTELEF().trim());
    }
    else {
      st.setNull(17, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_MNMCALLE() " + alerta.getDS_MNMCALLE());
    }
    if (alerta.getDS_MNMCALLE().trim().length() > 0) {
      st.setString(18, alerta.getDS_MNMCALLE().trim());
    }
    else {
      st.setNull(18, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_OBSERV() " + alerta.getDS_OBSERV());
    }
    if (alerta.getDS_OBSERV().trim().length() > 0) {
      st.setString(19, alerta.getDS_OBSERV().trim());
    }
    else {
      st.setNull(19, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_ALISOSP() " + alerta.getDS_ALISOSP());
    }
    if (alerta.getDS_ALISOSP().trim().length() > 0) {
      st.setString(20, alerta.getDS_ALISOSP().trim());
    }
    else {
      st.setNull(20, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_SINTOMAS() " + alerta.getDS_SINTOMAS());
    }
    if (alerta.getDS_SINTOMAS().trim().length() > 0) {
      st.setString(21, alerta.getDS_SINTOMAS().trim());
    }
    else {
      st.setNull(21, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_NOTIFPISO() " + alerta.getDS_NOTIFPISO());
    }
    if (alerta.getDS_NOTIFPISO().trim().length() > 0) {
      st.setString(22, alerta.getDS_NOTIFPISO().trim());
    }
    else {
      st.setNull(22, java.sql.Types.VARCHAR);
      // System_out.println("srvGuichi: alerta.getDS_MINFPISO() " + alerta.getDS_MINFPISO());
    }
    if (alerta.getDS_MINFPISO().trim().length() > 0) {
      st.setString(23, alerta.getDS_MINFPISO().trim());
    }
    else {
      st.setNull(23, java.sql.Types.VARCHAR);

    }
    st.executeUpdate();

    st.close();
    st = null;

    // System_out.println("srvGuichi: despues de ADIC ");

    queryMod = "insert into SIVE_ALERTA_COLEC " //10
        + "( CD_ANO, NM_ALERBRO, "
        + " CD_PROV, CD_MUN,  "
        + " DS_NOMCOL, DS_DIRCOL, "
        + " DS_NMCALLE, CD_POSTALCOL, DS_TELCOL, "
        + " DS_PISOCOL  )"
        + " values ( ?,?,?,?,?, ?,?,?,?,?)";
    st = con.prepareStatement(queryMod);

    // System_out.println("srvGuichi: alerta.getCD_PROV() " + alerta.getCD_PROV());
    st.setString(1, alerta.getCD_ANO().trim());
    st.setInt(2, new Integer(alerta.getNM_ALERBRO().trim()).intValue());

    if (alerta.getCD_PROV().trim().length() > 0) {
      st.setString(3, alerta.getCD_PROV().trim());
    }
    else {
      st.setNull(3, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getCD_MUN() " + alerta.getCD_MUN());
    }
    if (alerta.getCD_MUN().trim().length() > 0) {
      st.setString(4, alerta.getCD_MUN().trim());
    }
    else {
      st.setNull(4, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getDS_NOMCOL() " + alerta.getDS_NOMCOL());
    }
    if (alerta.getDS_NOMCOL().trim().length() > 0) {
      st.setString(5, alerta.getDS_NOMCOL().trim());
    }
    else {
      st.setNull(5, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getDS_DIRCOL() " + alerta.getDS_DIRCOL());
    }
    if (alerta.getDS_DIRCOL().trim().length() > 0) {
      st.setString(6, alerta.getDS_DIRCOL().trim());
    }
    else {
      st.setNull(6, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getDS_NMCALLE() " + alerta.getDS_NMCALLE());
    }
    if (alerta.getDS_NMCALLE().trim().length() > 0) {
      st.setString(7, alerta.getDS_NMCALLE().trim());
    }
    else {
      st.setNull(7, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getCD_POSTALCOL() " + alerta.getCD_POSTALCOL());
    }
    if (alerta.getCD_POSTALCOL().trim().length() > 0) {
      st.setString(8, alerta.getCD_POSTALCOL().trim());
    }
    else {
      st.setNull(8, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getDS_TELCOL() " + alerta.getDS_TELCOL());
    }
    if (alerta.getDS_TELCOL().trim().length() > 0) {
      st.setString(9, alerta.getDS_TELCOL().trim());
    }
    else {
      st.setNull(9, java.sql.Types.VARCHAR);

      // System_out.println("srvGuichi: alerta.getDS_PISOCOL() " + alerta.getDS_PISOCOL());
    }
    if (alerta.getDS_PISOCOL().trim().length() > 0) {
      st.setString(10, alerta.getDS_PISOCOL().trim());
    }
    else {
      st.setNull(10, java.sql.Types.VARCHAR);

    }
    st.executeUpdate();

    st.close();
    st = null;

    String queryComp = "insert into SIVE_ALERTA_BROTES " //30
        + " ( CD_OPE, FC_ULTACT,  "
        + " CD_LEXPROV, CD_LEXMUN,  "
        + " FC_FECHAHORA, CD_NIVEL_1_EX, CD_NIVEL_2_EX,  CD_ZBS_EX,"
        + " CD_NIVEL_1, CD_NIVEL_2, CD_E_NOTIF, "
        + " DS_ALERTA, CD_GRUPO, NM_EXPUESTOS, NM_ENFERMOS, NM_INGHOSP, "
        + " FC_EXPOSICION, FC_INISINTOMAS, DS_LEXP,  "
        + " DS_LEXPDIREC,  CD_LEXPPOST, DS_LNMCALLE, "
        + " DS_LEXPTELEF,  CD_SITALERBRO, FC_ALERBRO, "
        + " IT_VALIDADA,  FC_FECVALID,  "
        + "  DS_LEXPPISO, CD_ANO,  NM_ALERBRO ) "
        +
        "values ( ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,  ?,?,?,?,?, ?,?,?,?,?)";

    st = con.prepareStatement(queryComp);

    //operador actual
    st.setString(1, login);
    // System_out.println("SrvGui: login " +login);
    //la fecha de ultima actualizacion no es un date
    //es un timestamp, fecha actual
    dFecha = new java.util.Date();
    String sFecha = formUltAct.format(dFecha); //string
    // System_out.println("SrvGui: sFecha " +sFecha);
    java.sql.Timestamp ts = cadena_a_timestamp(sFecha);
    st.setTimestamp(2, ts);

    if (alerta.getCD_LEXPROV().trim().length() > 0) {
      st.setString(3, alerta.getCD_LEXPROV().trim());
    }
    else {
      st.setNull(3, java.sql.Types.VARCHAR);
    }
    if (alerta.getCD_LEXMUN().trim().length() > 0) {
      st.setString(4, alerta.getCD_LEXMUN().trim());
    }
    else {
      st.setNull(4, java.sql.Types.VARCHAR);
    }
    st.setTimestamp(5,
                    cadena_a_timestamp(fecha_mas_hora(alerta.getFC_FECHAHORA_F().
        trim(),
        alerta.getFC_FECHAHORA_H().trim())));
    st.setString(6, alerta.getCD_NIVEL_1_EX().trim());
    if (alerta.getCD_NIVEL_2_EX().trim().length() > 0) {
      st.setString(7, alerta.getCD_NIVEL_2_EX().trim());
    }
    else {
      st.setNull(7, java.sql.Types.VARCHAR);
    }
    if (alerta.getCD_ZBS_EX().trim().length() > 0) {
      st.setString(8, alerta.getCD_ZBS_EX().trim());
    }
    else {
      st.setNull(8, java.sql.Types.VARCHAR);
    }
    st.setString(9, alerta.getCD_NIVEL_1().trim());
    if (alerta.getCD_NIVEL_2().trim().length() > 0) {
      st.setString(10, alerta.getCD_NIVEL_2().trim());
    }
    else {
      st.setNull(10, java.sql.Types.VARCHAR);
    }
    if (alerta.getCD_E_NOTIF().trim().length() > 0) {
      st.setString(11, alerta.getCD_E_NOTIF().trim());
    }
    else {
      st.setNull(11, java.sql.Types.VARCHAR);
    }
    st.setString(12, alerta.getDS_ALERTA().trim());
    st.setString(13, alerta.getCD_GRUPO().trim());

    // System_out.println("SrvGui: hasta el 15");

    if (alerta.getNM_EXPUESTOS().trim().length() > 0) {
      st.setInt(14, (new Integer(alerta.getNM_EXPUESTOS().trim())).intValue());
    }
    else {
      st.setNull(14, java.sql.Types.INTEGER);
    }
    if (alerta.getNM_ENFERMOS().trim().length() > 0) {
      st.setInt(15, (new Integer(alerta.getNM_ENFERMOS().trim())).intValue());
    }
    else {
      st.setNull(15, java.sql.Types.INTEGER);
    }
    if (alerta.getNM_INGHOSP().trim().length() > 0) {
      st.setInt(16, (new Integer(alerta.getNM_INGHOSP().trim())).intValue());
    }
    else {
      st.setNull(16, java.sql.Types.INTEGER);
    }
    if (alerta.getFC_EXPOSICION_F().trim().length() > 0) {
      if (alerta.getFC_EXPOSICION_H().trim().length() > 0) {
        st.setTimestamp(17,
                        cadena_a_timestamp(fecha_mas_hora(alerta.getFC_EXPOSICION_F().
            trim(),
            alerta.getFC_EXPOSICION_H().trim())));
      }
      else {
        st.setTimestamp(17,
                        cadena_a_timestamp(fecha_mas_hora(alerta.getFC_EXPOSICION_F().
            trim(),
            "00:00:00")));
      }
    }
    else {
      st.setNull(17, java.sql.Types.DATE);
    }
    if (alerta.getFC_INISINTOMAS_F().trim().length() > 0) {
      if (alerta.getFC_INISINTOMAS_H().trim().length() > 0) {
        st.setTimestamp(18,
                        cadena_a_timestamp(fecha_mas_hora(alerta.getFC_INISINTOMAS_F().
            trim(),
            alerta.getFC_INISINTOMAS_H().trim())));
      }
      else {
        st.setTimestamp(18,
                        cadena_a_timestamp(fecha_mas_hora(alerta.getFC_INISINTOMAS_F().
            trim(),
            "00:00:00")));
      }
    }
    else {
      st.setNull(18, java.sql.Types.DATE);
    }
    if (alerta.getDS_LEXP().trim().length() > 0) {
      st.setString(19, alerta.getDS_LEXP().trim());
    }
    else {
      st.setNull(19, java.sql.Types.VARCHAR);
    }
    if (alerta.getDS_LEXPDIREC().trim().length() > 0) {
      st.setString(20, alerta.getDS_LEXPDIREC().trim());
    }
    else {
      st.setNull(20, java.sql.Types.VARCHAR);
    }
    if (alerta.getCD_LEXPPOST().trim().length() > 0) {
      st.setString(21, alerta.getCD_LEXPPOST().trim());
    }
    else {
      st.setNull(21, java.sql.Types.VARCHAR);
    }
    if (alerta.getDS_LNMCALLE().trim().length() > 0) {
      st.setString(22, alerta.getDS_LNMCALLE().trim());
    }
    else {
      st.setNull(22, java.sql.Types.VARCHAR);
    }
    if (alerta.getDS_LEXPTELEF().trim().length() > 0) {
      st.setString(23, alerta.getDS_LEXPTELEF().trim());
    }
    else {
      st.setNull(23, java.sql.Types.VARCHAR);
    }
    st.setString(24, alerta.getCD_SITALERBRO().trim());
    dFecha = formater.parse(alerta.getFC_ALERBRO().trim());
    sqlFec = new java.sql.Date(dFecha.getTime());
    st.setDate(25, sqlFec);
    if (alerta.getIT_VALIDADA().trim().length() > 0) {
      st.setString(26, alerta.getIT_VALIDADA().trim());
    }
    else {
      st.setNull(26, java.sql.Types.VARCHAR);
    }
    if (alerta.getFC_FECVALID().trim().length() > 0) {
      dFecha = formater.parse(alerta.getFC_FECVALID().trim());
      sqlFec = new java.sql.Date(dFecha.getTime());
      st.setDate(27, sqlFec);
    }
    else {
      st.setNull(27, java.sql.Types.DATE);
    }
    if (alerta.getDS_LEXPPISO().trim().length() > 0) {
      st.setString(28, alerta.getDS_LEXPPISO().trim());
    }
    else {
      st.setNull(28, java.sql.Types.VARCHAR);

    }

    st.setString(29, alerta.getCD_ANO().trim());
    // System_out.println("SrvGui: alerta.getCD_ANO() " + alerta.getCD_ANO());
    st.setInt(30, new Integer(alerta.getNM_ALERBRO().trim()).intValue());
    // System_out.println("SrvGui: alerta.getNM_ALERBRO() " + alerta.getNM_ALERBRO());
    st.executeUpdate();
    st.close();
    st = null;

    alerta.remove("CD_OPE");
    alerta.insert("CD_OPE", login);
    alerta.remove("FC_ULTACT");
    alerta.insert("FC_ULTACT", sFecha);
    // System_out.println("SrvGui:se ha hecho la modificacion ");

    return alerta;
  }

  private void borradoAlerta(DataAlerta alerta,
                             Connection con,
                             PreparedStatement st,
                             ResultSet rs) throws Exception {

    String queryB = "delete from SIVE_ALERTA_ADIC "
        + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
    st = con.prepareStatement(queryB);

    st.setString(1, alerta.getCD_ANO().trim());
    st.setInt(2, new Integer(alerta.getNM_ALERBRO().trim()).intValue());
    st.executeUpdate();
    st.close();
    st = null;

    queryB = "delete from SIVE_ALERTA_COLEC "
        + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
    st = con.prepareStatement(queryB);

    st.setString(1, alerta.getCD_ANO().trim());
    st.setInt(2, new Integer(alerta.getNM_ALERBRO().trim()).intValue());
    st.executeUpdate();
    st.close();
    st = null;

    queryB = "delete from SIVE_ALERTA_BROTES "
        + " where ( CD_ANO = ? and NM_ALERBRO = ? )";
    st = con.prepareStatement(queryB);

    st.setString(1, alerta.getCD_ANO().trim());
    st.setInt(2, new Integer(alerta.getNM_ALERBRO().trim()).intValue());
    st.executeUpdate();
    st.close();
    st = null;

  }

  private int comprobarModBor(DataAlerta alerta,
                              Connection con,
                              PreparedStatement st,
                              ResultSet rs,
                              String login) throws Exception {

    String queryComp = " update SIVE_ALERTA_BROTES "
        + " set CD_OPE = ? , FC_ULTACT = ? "
        + " where ( CD_ANO = ? and NM_ALERBRO = ? "
        + " and CD_OPE = ? and FC_ULTACT = ?  )";

    // System_out.println("SrvGui: entro en comprobarModBor " + queryComp);

    st = con.prepareStatement(queryComp);

    java.sql.Timestamp ts = null;

    //operador actual
    st.setString(1, login);
    // System_out.println("SrvGui: login " +login);

    //la fecha de ultima actualizacion no es un date
    //es un timestamp, fecha actual
    dFecha = new java.util.Date();
    String sFecha = formUltAct.format(dFecha); //string
    // System_out.println("SrvGui: sFecha " +sFecha);
    ts = cadena_a_timestamp(sFecha);
    st.setTimestamp(2, ts);

    st.setString(3, alerta.getCD_ANO().trim());
    // System_out.println("SrvGui: alerta.getCD_ANO() " + alerta.getCD_ANO());

    st.setInt(4, new Integer(alerta.getNM_ALERBRO().trim()).intValue());
    // System_out.println("SrvGui: alerta.getNM_ALERBRO() " + alerta.getNM_ALERBRO());

    st.setString(5, alerta.getCD_OPE().trim());
    // System_out.println("SrvGui: alerta.getCD_OPE() " + alerta.getCD_OPE());

    ts = cadena_a_timestamp(alerta.getFC_ULTACT().trim());
    st.setTimestamp(6, ts);
    // System_out.println("SrvGui: alerta.getFC_ULTACT() " + alerta.getFC_ULTACT());
    // System_out.println("SrvGui: nuevo");
    int resUpdate = 0;
    resUpdate = st.executeUpdate();
    st.close();
    st = null;

    // System_out.println("SrvGui: resUpdate=" + resUpdate);

    return resUpdate;
  }

  private String modificado_o_borrado(DataAlerta alerta,
                                      Connection con,
                                      PreparedStatement st,
                                      ResultSet rs) throws Exception {

    String resultado = "";

    String queryComp = " select * from SIVE_ALERTA_BROTES "
        + " where CD_ANO = ? and NM_ALERBRO = ? ";
    st = con.prepareStatement(queryComp);
    st.setString(1, alerta.getCD_ANO().trim());
    st.setString(2, alerta.getNM_ALERBRO().trim());

    rs = st.executeQuery();
    if (rs.next()) {
      //resultado modificado
      resultado = "M";
    }
    else {
      //resultado borrado
      resultado = "B";
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    return resultado;

  }

}
