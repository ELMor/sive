package fechas;

import capp2.CApp;

public class PrueCFechas
    extends CApp {

  PanPrueCFechas ppf = new PanPrueCFechas(this);

  public PrueCFechas() {
    super();
  }

  public void init() {
    super.init();
    setTitulo("Prueba CFecha");
    VerPanel("PanPrueCFechas", ppf);
  }
}
