//Title:        Your Product Name
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Jesus Garcia
//Company:      Norsistemas, S.A.
//Description:  Your description

package panelnivan;

import java.awt.Button;
import java.awt.event.ActionEvent;

import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CPanel;

public class panelNivAn
    extends CPanel {
  Button button1 = new Button();
  XYLayout xYLayout1 = new XYLayout();

  public panelNivAn(CApp a) {
    try {
      this.setApp(a);
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    setBorde(false);
    button1.setLabel("button1");
    button1.addActionListener(new panelNivAn_button1_actionAdapter(this));
    this.setLayout(xYLayout1);
    this.add(button1, new XYConstraints(7, 12, -1, -1));
  }

  void button1_actionPerformed(ActionEvent e) {
    DlgNivAn dlg = new DlgNivAn(this.app,
                                this.app.getANYO_DEFECTO(),
                                this.app.getCD_NIVEL1_DEFECTO(),
                                this.app.getDS_NIVEL1_DEFECTO(),
                                this.app.getCD_NIVEL2_DEFECTO(),
                                this.app.getDS_NIVEL2_DEFECTO());
    dlg.show();

    if (dlg.esOK()) {
      this.button1.setLabel("OK");
    }
    else {
      this.button1.setLabel("FALSE");
    }

    dlg = null;
  }

  public void Inicializar() {
  }
}

class panelNivAn_button1_actionAdapter
    implements java.awt.event.ActionListener {
  panelNivAn adaptee;

  panelNivAn_button1_actionAdapter(panelNivAn adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.button1_actionPerformed(e);
  }
}
