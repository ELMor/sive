package panelnivan;

import java.util.Calendar;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CMessage;
import fechas.conversorfechas;
import panniveles.panelNiveles;
import panniveles.usaPanelNiveles;

public class DlgNivAn
    extends CDialog
    implements usaPanelNiveles {

  public boolean esAceptado = false;
  boolean bAnyoValid = false;
  int modoOperacion;
  int ultimo;

  final int modoNORMAL = 0;
  final int modoESPERA = 2;

  panelNiveles niveles;
  XYLayout xYLayout1 = new XYLayout();
  Label lblAnyo = new Label();
  TextField txtAnyo = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();

  // Para controlar cambios
  private String sAnyo = null;

  public DlgNivAn(CApp a, String elAnyo, String cdArea, String dsArea,
                  String cdDist, String dsDist) {
    super(a);
    this.app = a;
    this.setTitle("Selecci�n zonificaci�n sanitaria");
    niveles = new panelNiveles(a,
                               this,
                               panelNiveles.MODO_INICIO,
                               panelNiveles.TIPO_NIVEL_2,
                               panelNiveles.LINEAS_3,
                               true,
                               true);
    try {
      if (elAnyo.length() == 0) {
        Calendar cal = Calendar.getInstance();
        ultimo = cal.get(cal.YEAR);
      }
      else {
        ultimo = Integer.parseInt(elAnyo);
      }
      niveles.setCDNivel1(cdArea);
      niveles.setCDNivel2(cdDist);
      niveles.setDSNivel1(dsArea);
      niveles.setDSNivel2(dsDist);
      jbInit();
      pack();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        "images/Aceptar.gif",
        "images/Cancelar.gif"};

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    setSize(450, 300);

    this.setLayout(xYLayout1);
    lblAnyo.setText("A�o");
    btnAceptar.setLabel("Aceptar");
    btnAceptar.addActionListener(new DlgNivAn_btnAceptar_actionAdapter(this));
    btnCancelar.setLabel("Cancelar");
    btnCancelar.addActionListener(new DlgNivAn_btnCancelar_actionAdapter(this));
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));

    this.add(niveles, new XYConstraints( -40, 5, -1, 61));
    this.add(lblAnyo, new XYConstraints(8, 69, -1, -1));
    this.add(txtAnyo, new XYConstraints(63, 69, 66, -1));
    this.add(btnAceptar, new XYConstraints(243, 100, -1, -1));
    this.add(btnCancelar, new XYConstraints(318, 100, -1, -1));

    lblAnyo.setAlignment(java.awt.Label.RIGHT);

    txtAnyo.setText(Integer.toString(ultimo));
    txtAnyo.addFocusListener(new DlgNivAn_txtAnyo_focusAdapter(this));
  }

  void txtAnyo_focusGained() {
    sAnyo = txtAnyo.getText().trim();
  }

  void txtAnyo_focusLost() {
    String sDato = "";
    sDato = txtAnyo.getText().trim();

    if (sDato.equals(sAnyo)) {
      return;
    }

    modoOperacion = modoESPERA;
    Inicializar();

    // Se comprueba que el a�o est� relleno y con formato AAAA
    if (sDato.length() == 0) {
      bAnyoValid = false;
      txtAnyo.setText(Integer.toString(ultimo));
      modoOperacion = modoNORMAL;
      Inicializar();

      return;
    }
    if (sDato.length() != 4) {
      bAnyoValid = false;
      txtAnyo.setText(Integer.toString(ultimo));
      modoOperacion = modoNORMAL;
      Inicializar();

      return;
    }

    // comprueba que el a�o introducido sea correcto
    int iAnno = 0;
    try {
      iAnno = new Integer(sDato).intValue();

      // se comprueba que no sea un entero negativo
      if (iAnno < 0) {
        bAnyoValid = false;
        txtAnyo.setText(Integer.toString(ultimo));
        modoOperacion = modoNORMAL;
        Inicializar();

        return;
      }

    }
    catch (java.lang.NumberFormatException e) {
      bAnyoValid = false;
      txtAnyo.setText(Integer.toString(ultimo));
      modoOperacion = modoNORMAL;
      Inicializar();

      return;
    }

    // se comprueba que el a�o se encuentre dentro de los a�os generados

    conversorfechas conv = new conversorfechas(sDato, app);
    if (!conv.anoValido()) {
      bAnyoValid = false;
      txtAnyo.setText(Integer.toString(ultimo));

      modoOperacion = modoNORMAL;
      Inicializar();

      return;
    }

    // si no ha salido por ninguna de las condiciones anteriores,
    // el a�o es correcto y se pone su boolean a true
    bAnyoValid = true;
    ultimo = Integer.parseInt(txtAnyo.getText());
    if (!esAceptado) { //si no hemos pulsado aceptar
      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  private void ShowWarning(String sMessage) {
    CMessage msgBox;

    msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:

        txtAnyo.setEnabled(true);
        niveles.setModoNormal();
        lblAnyo.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:

        txtAnyo.setEnabled(false);
        niveles.setModoEspera();
        lblAnyo.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
    this.doLayout();
  }

  boolean isDataValid() {
    boolean miValor = true;
    txtAnyo_focusLost();

    if (txtAnyo.getText().trim().equals("")) {
      ShowWarning("Introduzca el a�o.");
      txtAnyo.setText(Integer.toString(ultimo));
      txtAnyo.select(txtAnyo.getText().length(), txtAnyo.getText().length());
      txtAnyo.requestFocus();
      miValor = false;
    }
    if ( (!txtAnyo.getText().trim().equals("")) && (!bAnyoValid)) {
      ShowWarning("Introduzca un a�o v�lido.");
      txtAnyo.setText(Integer.toString(ultimo));
      txtAnyo.select(txtAnyo.getText().length(), txtAnyo.getText().length());
      txtAnyo.requestFocus();
      miValor = false;
    }
    return miValor;
  }

  void btnAceptar_actionPerformed(ActionEvent e) {
    modoOperacion = modoESPERA;
    Inicializar();

    this.esAceptado = this.isDataValid();

    if (esAceptado) {
      this.app.setCD_NIVEL1_DEFECTO(this.niveles.getCDNivel1());
      this.app.setDS_NIVEL1_DEFECTO(this.niveles.getDSNivel1());
      this.app.setCD_NIVEL2_DEFECTO(this.niveles.getCDNivel2());
      this.app.setDS_NIVEL2_DEFECTO(this.niveles.getDSNivel2());
      this.app.setANYO_DEFECTO(this.txtAnyo.getText());
      dispose();
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnCancelar_actionPerformed(ActionEvent e) {
    this.esAceptado = false;
    dispose();
  }

  boolean esOK() {
    return esAceptado;
  }

  public String getCodArea() {
    return this.niveles.getCDNivel1();
  }

  public String getDescArea() {
    return this.niveles.getDSNivel1();
  }

  public String getCodDistrito() {
    return this.niveles.getCDNivel2();
  }

  public String getDescDistrito() {
    return this.niveles.getDSNivel2();
  }

  public String getAnyo() {
    return this.txtAnyo.getText();
  }

  //Para el interfaz usaPanelNiveles
  public void cambioNivelAntesInformado(int nivel) {}

  public int ponerEnEspera() {
    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();
    return modo;
  }

  public void ponerModo(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

}

class DlgNivAn_btnAceptar_actionAdapter
    implements java.awt.event.ActionListener {
  DlgNivAn adaptee;
  ActionEvent e;

  DlgNivAn_btnAceptar_actionAdapter(DlgNivAn adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    adaptee.btnAceptar_actionPerformed(e);
  }
}

class DlgNivAn_btnCancelar_actionAdapter
    implements java.awt.event.ActionListener {
  DlgNivAn adaptee;
  ActionEvent e;

  DlgNivAn_btnCancelar_actionAdapter(DlgNivAn adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    adaptee.btnCancelar_actionPerformed(e);
  }
}

class DlgNivAn_txtAnyo_focusAdapter
    extends java.awt.event.FocusAdapter {
  DlgNivAn adaptee;
  FocusEvent e;

  DlgNivAn_txtAnyo_focusAdapter(DlgNivAn adaptee) {
    this.adaptee = adaptee;
  }

  public void focusGained(FocusEvent e) {
    this.e = e;
    adaptee.txtAnyo_focusGained();
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    adaptee.txtAnyo_focusLost();
  }
}
