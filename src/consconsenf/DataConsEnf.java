package consconsenf;

import java.util.Vector;

import capp.CLista;
import conspacenf.Data;
import enfermo.Fechas;
import sapp.DBServlet;

/** relaci�n de pacientes con un fono y no tratados
 *
 */
public class DataConsEnf
    extends conspacenf.Data {

  /** campos que va a tener */
  public static final int COUNT = 0;
  public static final int DS_FONOAPE1 = 1;
  public static final int DS_FONONOMBRE = 2;
  public static final int CD_ENFERMO = 3;
  public static final int DS_APE1 = 4;
  public static final int DS_APE2 = 5;
  public static final int DS_NOMBRE = 6;
  public static final int FC_NAC = 7;
  public static final int CD_PROV = 8;
  public static final int CD_MUN = 9;
  public static final int DS_DIREC = 10;
  public static final int DS_NUM = 11;
  public static final int SIGLAS = 12;
  public static final int DS_MUN = 13;

  /** parametros que tiene */
  // n�mero de par�metros de esta sentencia
  public static final int P_NUM_C3 = 5;

  // posici�n de los diferentes par�metrso en la select
  public static final int P_PAGINA = 0; // indica la p�gina de datos que se quiere traer
  public static final int P_CD_NIVEL_1 = 1;
  public static final int P_CD_NIVEL_2 = 2;

  /** modos que tiene */
  public static final int modoENFDUPLICADOCOUNT = 1;
  public static final int modoENFDUPLICADO = 2;

  public DataConsEnf(int a_campo_filtro) {
    super(a_campo_filtro);
    // siempre hay que reservar
    arrayDatos = new Object[14];
  }

  public Object getNewData() {
    return new DataConsEnf(campo_filtro);
  }

  /** devuelve el c�digo de un modo e funcionamiento */
  public String getCode(int a_modo) {
    return "NM_EDO";
  }

  /** devuelve la descripci�n de un modo de funcionamiento */
  public String getDes(int a_modo) {
    return null;
  }

  /** indica si ya lleva where la select */
  public boolean getWhere(int a_modo) {
    return true;
  }

  /** esta funci�n devuelve la sentencia a ejecutar */
  public int prepareSQLSentencia(int opmode, StringBuffer strSQL,
                                 CLista ini_param) {
    switch (opmode) {
      case modoENFDUPLICADOCOUNT:
        getSQLEnfermoDuplicado(strSQL, true, ini_param);
        break;
      case modoENFDUPLICADO:
        getSQLEnfermoDuplicado(strSQL, false, ini_param);
        break;
    }

    // indicamos le n�merod e registros que queremos
    return DBServlet.maxSIZE; // por defecto
  }

  public void getSQLEnfermoDuplicado(StringBuffer result, boolean is_count,
                                     CLista parametros) {
    String laclave = null, eldato = null;
    Object codEnfer = null;
    Data parameter = null;
    int inum_param = 0;
    if (parametros != null && parametros.size() > 0) {
      parameter = (Data) parametros.firstElement();
      inum_param = parameter.getNumParam();
    }

    // consulta que trae todos los datos de los enfermos con mismo fono que no est�n revisado y cumple condiciones
    if (is_count) {
      result.append("SELECT COUNT(CD_ENFERMO) ");

    }
    else {
      result.append("SELECT DS_FONOAPE1, DS_FONONOMBRE, CD_ENFERMO, DS_APE1,"
                    +
          "DS_APE2, DS_NOMBRE, FC_NAC, CD_PROV, CD_MUN, DS_DIREC, DS_NUM, SIGLAS");

    }

    result.append(" FROM SIVE_ENFERMO WHERE IT_ENFERMO = 'S' AND IT_CONTACTO = 'N' AND DS_FONOAPE1||DS_FONONOMBRE  IN ");
    if (inum_param > 0) {
      // subconsulta que filtra los fonos de enfermos que cumplan la condicion
      result.append(
          " (SELECT DISTINCT(DS_FONOAPE1||DS_FONONOMBRE) FROM SIVE_ENFERMO WHERE ");
      //COMPROBAR LOS NIVELES 1 Y 2
      result.append(" CD_NIVEL_1 like ?");
      if (inum_param == 2) {
        result.append(" AND CD_NIVEL_2 like ? ");
      }
      result.append(" AND DS_FONOAPE1||DS_FONONOMBRE IN ");
      result.append("(SELECT FONO FROM ");
      result.append("(SELECT DS_FONOAPE1||DS_FONONOMBRE FONO,");
      result.append("COUNT(CD_ENFERMO) CUENTA,");
      result.append("MIN(IT_REVISADO) REVISADO ");
      result.append("FROM SIVE_ENFERMO GROUP BY DS_FONOAPE1||DS_FONONOMBRE)");
      result.append("WHERE CUENTA>1 AND REVISADO='N'))");

    }
    else {
      // subconsulta que trae todos los fonos de enfermos no revisados
      result.append(
          " (SELECT DISTINCT(DS_FONOAPE1||DS_FONONOMBRE) FROM SIVE_ENFERMO WHERE ");
      result.append(" DS_FONOAPE1||DS_FONONOMBRE IN ");
      result.append("(SELECT FONO FROM ");
      result.append("(SELECT DS_FONOAPE1||DS_FONONOMBRE FONO,");
      result.append("COUNT(CD_ENFERMO) CUENTA,");
      result.append("MIN(IT_REVISADO) REVISADO ");
      result.append("FROM SIVE_ENFERMO GROUP BY DS_FONOAPE1||DS_FONONOMBRE)");
      result.append("WHERE CUENTA>1 AND REVISADO='N'))");

    }
    if (!is_count) {
      result.append(" ORDER BY DS_FONOAPE1, DS_FONONOMBRE, FC_NAC");

    }
  }

  public String getENFERMO() {
    StringBuffer result = new StringBuffer();

    if (arrayDatos[DS_NOMBRE] != null) {
      result.append(arrayDatos[DS_NOMBRE]);
    }
    result.append(" ");
    if (arrayDatos[DS_APE1] != null) {
      result.append(arrayDatos[DS_APE1]);
    }
    result.append(" ");
    if (arrayDatos[DS_APE2] != null) {
      result.append(arrayDatos[DS_APE2]);
    }

    return result.toString();
  }

  public String getCD_MUNICIPIO() {
    if (arrayDatos.length > DS_MUN) {
      return (String) arrayDatos[DS_MUN];
    }
    else {
      return " ";
    }
  }

  public String getCD_MUN() {
    if (arrayDatos.length > CD_MUN) {
      return (String) arrayDatos[CD_MUN];
    }
    else {
      return " ";
    }
  }

///////  es solo una prueba
  public String getCD_ENFERMO() {
    if (arrayDatos.length > CD_ENFERMO) {
      Object in = arrayDatos[CD_ENFERMO];
      if (in != null) {
        return in.toString();
      }
      else {
        return " ";
      }

    }
    else {
      return " ";
    }
  }

  public String getDS_NOMBRE() {
    if (arrayDatos.length > DS_NOMBRE) {
      return (String) arrayDatos[DS_NOMBRE];
    }
    else {
      return " ";
    }
  }

  public String getDS_APE1() {
    if (arrayDatos.length > DS_APE1) {
      return (String) arrayDatos[DS_APE1];
    }
    else {
      return " ";
    }
  }

  public String getDS_APE2() {
    if (arrayDatos.length > DS_APE2) {
      return (String) arrayDatos[DS_APE2];
    }
    else {
      return " ";
    }
  }

  public String getSIGLAS() {
    if (arrayDatos.length > SIGLAS) {
      return (String) arrayDatos[SIGLAS];
    }
    else {
      return " ";
    }
  }

  public String getFC_NAC() {
    if (arrayDatos.length > FC_NAC) {
      //AIC Para evitar error en el informe.
      java.util.Date da = (java.util.Date) arrayDatos[FC_NAC];
      return Fechas.date2String(da);
      //return (String) arrayDatos[FC_NAC];
    }
    else {
      return " ";
    }
  }

  public String getDS_NOMBREENTERO() {
    StringBuffer strResult = new StringBuffer();
    Object o = null;

    o = arrayDatos[DS_APE1];
    if (o != null) {
      strResult.append(o.toString());
    }
    o = arrayDatos[DS_APE2];
    if (o != null) {
      strResult.append(" " + o.toString());
    }
    o = arrayDatos[DS_NOMBRE];
    if (o != null) {
      strResult.append(" " + o.toString());
    }

    return strResult.toString();
  }

  public String getDS_FONO() {
    StringBuffer strResult = new StringBuffer();
    Object o = null;

    o = arrayDatos[DS_FONOAPE1];
    if (o != null) {
      strResult.append(o.toString());
    }
    o = arrayDatos[DS_FONONOMBRE];
    if (o != null) {
      strResult.append(o.toString());
    }

    return strResult.toString();
  }

  public String getDS_DIRECCION() {
    StringBuffer strResult = new StringBuffer();
    Object o = null;

    o = arrayDatos[DS_DIREC];
    if (o != null) {
      strResult.append(o.toString());
    }
    o = arrayDatos[DS_NUM];
    if (o != null) {
      strResult.append(" " + o.toString());
    }

    return strResult.toString();
  }

  public Vector getFieldVector(boolean permiso) {
    Vector retval = new Vector();

    retval.addElement("DS_FONO  = getDS_FONO");
    retval.addElement("CD_ENFERMO  = getCD_ENFERMO");

    if (permiso) {
      retval.addElement("DS_NOMBREENTERO  = getDS_NOMBREENTERO");
    }
    else {
      retval.addElement("DS_NOMBREENTERO  = getSIGLAS");
    }

    retval.addElement("FC_NAC  = getFC_NAC");
    retval.addElement("CD_MUN  = getCD_MUNICIPIO");
    retval.addElement("DS_DIRECCION  = getDS_DIRECCION");

    return retval;
  }

} //____________________________________ END_CLASS
