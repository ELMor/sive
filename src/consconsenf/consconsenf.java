package consconsenf;

import java.util.ResourceBundle;

import capp.CApp;
import capp.CLista;
import conspacenf.DataPer;
import enfermo.comun;
import sapp.StubSrvBD;

public class consconsenf
    extends CApp {

  protected StubSrvBD stubCliente = new StubSrvBD();
  ResourceBundle res;

  public consconsenf() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    PnlParam pParam = null;
    CApp a = (CApp)this;
    boolean permiso = true;

    try {

      res = ResourceBundle.getBundle("consconsenf.Res" + this.getIdioma());
      CLista parametros = new CLista();
      CLista result = null;

      DataPer d = new DataPer(DataPer.IT_FG_ENFERMO);
      d.setNumParam(1);
      d.put(1, getLogin()); // ponemos el c�digo del enfermo
      parametros.addElement(d);
      result = comun.traerDatos(this, stubCliente, comun.strSERVLET_GEN,
                                DataPer.modoPERMISO, parametros);

      if (result != null && result.size() > 0) {
        DataPer p = (DataPer) result.firstElement();
        permiso = (p.getPERMISO());
        ////# System_Out.println("fijamos " +p.getPERMISO());
      }
      else {
        permiso = false;
      }

    }
    catch (Exception exc) {
      //E //# System_Out.println("Error : LeerChoices " + exc.toString());
      permiso = false;
    }

    ////# System_Out.println("Iniciamos applet consconsenf.ApMantu");
    setTitulo(res.getString("msg1.Text"));
    pParam = new PnlParam(a, permiso);
    VerPanel(res.getString("msg2.Text"), pParam, false);
    VerPanel(res.getString("msg3.Text"));

  }

  public void stop() {
  }

} //__________________________________________________ END_CLASS
