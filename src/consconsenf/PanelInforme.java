
package consconsenf;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class PanelInforme
    extends EPanel {

  PnlParam pan;
  ResourceBundle res;

  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  final String strSERVLET = "servlet/SrvConsEnf";

  final int MODO_SERVLET = DataConsEnf.modoENFDUPLICADO;
  final int MODO_SERVLETCOUNT = DataConsEnf.modoENFDUPLICADOCOUNT;

  /** guarda el n�mero de registros */
  protected String num_reg = "0";

  // estructuras de datos
  protected Vector vCasos = null;

  /** esta es la lista que recoge los datos */
  protected CLista lista;

  /** esta es la lista que contiene los par�metros */
  protected CLista param = null;

  /** n�mero de p�gina por el que se va */
  int num_pag = 0;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // vector y nombre de la tabla y fichero
  String m_file = null;
  String m_table = null;
  Vector m_column = null;

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  CApp app = null;

  public PanelInforme(StubSrvBD s, CApp a, String a_file, String a_nombreTabla,
                      Vector a_column, PnlParam miPanel) {
    super(a);
    app = a;
    res = ResourceBundle.getBundle("consconsenf.Res" + a.getIdioma());
    pan = miPanel;
    m_file = a_file;
    m_table = a_nombreTabla;
    m_column = a_column;
    vCasos = new Vector();

    try {
      stub = s;
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(getCApp().getCodeBase().toString() + m_file);

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;

    // incrementamos el n�mero de p�gina
    num_pag++;

    ////# System_Out.println("Mas Datos" + lista);
    if (lista != null && lista.getState() == CLista.listaINCOMPLETA) {
      ////# System_Out.println ( "Lista Estado " +lista.getState());
      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        // obtiene los datos del servidor
        ( (DataConsEnf) param.firstElement()).put(DataConsEnf.P_PAGINA,
                                                  Integer.toString(num_pag));

        // ARG: Se introduce el login
        param.setLogin(app.getLogin());

        // ARG: Se introduce la lortad
        param.setLortad(app.getParameter("LORTAD"));

        stub.setUrl(new URL(getCApp().getURL() + strSERVLET));
        lista = (CLista) stub.doPost(MODO_SERVLET, param);

        // ponemos las etiquetas del report
        fijarEtiqueas();

        if (lista != null) {
          for (int j = 0; j < lista.size(); j++) {
            vCasos.addElement(lista.elementAt(j));
          }

          // control de registros
          this.setTotalRegistros(num_reg);
          this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

          // repintado
          ////# System_Out.println("Se refresca ");
          erwClient.refreshReport(true);
        }
        else {
          this.setTotalRegistros("0");
          this.setRegistrosMostrados("0");
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(getCApp(), CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  protected void fijarEtiqueas() {
    String dato = null;
    StringBuffer linea = null;
    int iEtiqueta = 0;

    String sEtiqueta[] = {
        "CRITERIO"};

    // cambiamos las etiquetas
    DataConsEnf datac3 = (DataConsEnf) param.firstElement();
    TemplateManager tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000",
                   getCApp().getCodeBase().toString() + "images/ccaa.gif");
    tm.SetImageURL("IMA001",
                   getCApp().getCodeBase().toString() + "images/ccaa.gif");

    linea = new StringBuffer();

    /*
        dato  = (String) datac3.get(DataConsEnf.P_CD_NIVEL_1);
        if (dato != null){
            linea.append(EPanel.AREA);
            linea.append(dato);
        }
        //tm.SetLabel("LAB008", linea.toString()); // NIVEL1
        dato  = (String) datac3.get(DataConsEnf.P_CD_NIVEL_2);
        if (dato != null){
            linea.append(EPanel.DISTRITO);
            linea.append(dato);
        }
        //tm.SetLabel("LAB009", linea.toString()); // NIVEL2
        tm.SetLabel("CRITERIO", linea.toString()); // CRITERIO
     */

    //nivel1
    dato = (String) datac3.get(DataConsEnf.P_CD_NIVEL_1);
    if (dato != null) {
      tm.SetLabel(sEtiqueta[iEtiqueta],
                  app.getNivel1() + ": " + dato + " " + pan.txtDesAre.getText());
      dato = (String) datac3.get(DataConsEnf.P_CD_NIVEL_2);
      //nivel2
      if (dato != null) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    tm.GetLabel(sEtiqueta[iEtiqueta]) + " " + app.getNivel2() +
                    ": " + dato + " " + pan.txtDesDis.getText());
      }
      iEtiqueta++;
    }

    // oculta criterios no informados
    for (int i = iEtiqueta; i < 1; i++) {
      tm.SetLabel(sEtiqueta[i], "");
    }

  }

  // informe comleto
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  public boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    String dato = null;

    // lo ponemo al inico de la p�gina y  borramos los datos que hab�a
    num_pag = 0;
    vCasos.setSize(0);

    ////# System_Out.println("GenerarInforme " );
    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      // obtiene los datos del servidor
      DataConsEnf data = (DataConsEnf) param.firstElement();
      data.bInformeCompleto = conTodos;
      data.put(DataConsEnf.P_PAGINA, Integer.toString(num_pag));

      // ARG: Se introduce el login
      param.setLogin(app.getLogin());

      // ARG: Se introduce la lortad
      param.setLortad(app.getParameter("LORTAD"));

      param.trimToSize();
      stub.setUrl(new URL(getCApp().getURL() + strSERVLET));

      lista = (CLista) stub.doPost(MODO_SERVLETCOUNT, param);
      /*
             SrvConsEnf servlet = new SrvConsEnf();
             servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                 "jdbc:oracle:thin:@192.168.1.11:1521:ORA1",
                                 "dba_edo",
                                 "manager");
             lista = (CLista) servlet.doDebug(MODO_SERVLETCOUNT, param);
       */

      if (lista != null && lista.size() > 0) {
        vCasos = (Vector) lista;
        // ponemos las etiquetas del report
        fijarEtiqueas();

        //AIC
        /*DataConsEnf valor;
                  for(int i = 0; i < vCasos.size(); i++)
                  {
          valor = (DataConsEnf)vCasos.elementAt(i);
          System_out.println("enfermo " + valor.getCD_ENFERMO());
          System_out.println("fecha " + valor.getFC_NAC());
          System_out.println("Direcci�n " + valor.getDS_DIRECCION());
                  }*/

        num_reg = (String) ( (DataConsEnf) lista.firstElement()).get(
            DataConsEnf.COUNT);

        this.setTotalRegistros(num_reg);
        this.setRegistrosMostrados(Integer.toString(lista.size()));
        ////# System_Out.println("Lista "+ num_reg + lista.getState());

        // establece las matrices de datos
        dataHandler.RegisterTable(vCasos, m_table, m_column, null);
        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        modoOperacion = modoNORMAL;
        Inicializar();
        return true;

      }
      else {
        vCasos.setSize(0);
        msgBox = new CMessage(getCApp(), CMessage.msgAVISO,
                              res.getString("msg4.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return false;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(getCApp(), CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();

      modoOperacion = modoNORMAL;
      Inicializar();
      return false;

    }

  }

  public void MostrarGrafica() {
    TemplateManager tm = erw.GetTemplateManager();

    if (lista != null && lista.getState() == CLista.listaLLENA) {

      // oculta una de las secciones
      if (tm.IsVisible("RP_FTR")) {
        tm.SetVisible("PG_HDR", true);
        tm.SetVisible("DTL_00", true);
        tm.SetVisible("RP_FTR", false);
      }
      else {
        tm.SetVisible("PG_HDR", false);
        tm.SetVisible("DTL_00", false);
        tm.SetVisible("RP_FTR", true);
      }

      // repinta el grafico
      erwClient.refreshReport(true);

    }
  }

  public void setListaParam(CLista a_param) {
    param = a_param;
  }

  public CLista getListaParam() {
    return param;
  }

} //___________________________________ END_CLASS
