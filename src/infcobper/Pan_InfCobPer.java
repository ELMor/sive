package infcobper;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import eqNot.DataEqNot;
import sapp.StubSrvBD;

public class Pan_InfCobPer
    extends EPanel {
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 1;

  final String strSERVLET = "servlet/SrvInfCobPer";
  final String strLOGO_CCAA = "images/ccaa.gif";

  final int erwCASOS_EDO = 1;
  public ParamC11_12 paramC1 = null;

  // estructuras de datos
  protected Vector vAgrupaciones;
  protected Vector vTotales;
  protected CLista lista;

  // modo de operación
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // Para pedir mas paginas
  protected Vector ultSemana = new Vector();

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;
  Integer regMostrados = new Integer(0);

  CApp app = null;

  public Pan_InfCobPer(CApp a) {
    super(a);
    app = a;
    res = ResourceBundle.getBundle("infcobper.Res" + a.getIdioma());

    try {
      stub = new StubSrvBD();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + "erw/ERWCobPer.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // todo el informe
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;

    //#// System_out.println(lista.getState());
    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        PrepararInforme();

        // obtiene los datos del servidor
        paramC1.iPagina++;

        // CAmbio el ano y la semana desde para que no recupere el
        // resultSetEntero
        String a = ultSemana.elementAt(0).toString();
        String s = ultSemana.elementAt(1).toString();

        // Obtengo una semana antes de comprobar si ha terminado
        // la trama, por tanto la que devuelve el servlet es
        // la siguiente
        /*
                 Integer iS = new Integer(s);
                 iS = new Integer (iS.intValue() + 1);
                 if (iS.intValue()>52) {
          Integer iA = new Integer(a);
          iA = new Integer (iA.intValue() + 1);
          a = iA.toString();
                 }
                 if (iS.intValue()<10) {
          s = "0" + iS.toString();
                 }
                 else
          s = iS.toString();
         */

        paramC1.sAnoI = a;
        paramC1.sSemanaI = s;

        param.addElement(paramC1);
        param.setIdioma(app.getIdioma());
        param.trimToSize();
        stub.setUrl(new URL(app.getURL() + strSERVLET));
        lista = (CLista) stub.doPost(erwCASOS_EDO, param);

        v = (Vector) lista.elementAt(0);
        //vTotales = (Vector) lista.elementAt(1);
        ultSemana = (Vector) lista.elementAt(2);

        for (int j = 0; j < v.size(); j++) {
          vAgrupaciones.addElement(v.elementAt(j));

          // control de registros
          //this. setTotalRegistros(vTotales.elementAt(3).toString());
        }
        this.setTotalRegistros(regMostrados.toString());
        regMostrados = new Integer(vAgrupaciones.size());
        this.setRegistrosMostrados(regMostrados.toString());
        /*if (ultSemana.size()==0)
          this .setTotalRegistros(res.getString("this.TotalRegistros") );
                 else
          this .setTotalRegistros(res.getString("this1.TotalRegistros")
                                 + ultSemana.elementAt(0).toString()
                                 + " " + ultSemana.elementAt(1).toString());
         */
        this.setTotal(vTotales.elementAt(0).toString(),
                      vTotales.elementAt(1).toString(),
                      vTotales.elementAt(2).toString());

        // repintado
        erwClient.refreshReport(true);

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  protected boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    CLista param = new CLista();
    DataEqNot datosPar = null;

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    vTotales = new Vector();

    try {
      PrepararInforme();

      paramC1.iPagina = 0;
      paramC1.bInformeCompleto = conTodos;
      param.addElement(paramC1);
      //param.addElement(datosPar);
      param.setIdioma(app.getIdioma());
      param.trimToSize();
      stub.setUrl(new URL(app.getURL() + strSERVLET));
      lista = (CLista) stub.doPost(erwCASOS_EDO, param);

      /*     SrvInfCobPer srv = new SrvInfCobPer();
           srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                   "jdbc:oracle:thin:@10.160.12.54:1521:ORCL",
                                   "pista",
                                   "loteb98");
           lista = srv.doDebug(erwCASOS_EDO, param);
       */

      vAgrupaciones = (Vector) lista.elementAt(0);
      vTotales = (Vector) lista.elementAt(1);
      ultSemana = (Vector) lista.elementAt(2);

      // control de registros
      Integer aux = (Integer) vTotales.elementAt(3);
      if (vAgrupaciones.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg5.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return false;
      }
      else {

        regMostrados = new Integer(vAgrupaciones.size());
        this.setRegistrosMostrados(regMostrados.toString());

        this.setTotalRegistros(regMostrados.toString());
        String semMostrada = ( (DataInfCobPer) vAgrupaciones.elementAt(
            vAgrupaciones.size() - 1)).DS_NOMBRE;

        this.setTotal(vTotales.elementAt(0).toString(),
                      vTotales.elementAt(1).toString(),
                      vTotales.elementAt(2).toString());
        // establece las matrices de datos
        Vector retval = new Vector();
        retval.addElement("DS_NOMBRE = DS_NOMBRE");
        retval.addElement("NM_NTOTREAL = NM_NTOTREAL");
        retval.addElement("NM_NNOTIFT = NM_NNOTIFT");
        // jlt
        //retval.addElement("NM_COBERTURAI = NM_COBERTURAI");
        //retval.addElement("NM_COBERTURAR = NM_COBERTURAR");
        retval.addElement("NM_COBERTURAI = NM_COBERTURAIS");
        retval.addElement("NM_COBERTURAR = NM_COBERTURARS");
        dataHandler.RegisterTable(vAgrupaciones, "C_11_12", retval, null);
        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));
        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        modoOperacion = modoNORMAL;
        Inicializar();
        return true;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();

      modoOperacion = modoNORMAL;
      Inicializar();
      return false;
    }

  }

  // este informe no tiene grafica
  public void MostrarGrafica() {
  }

  private void setTotal(String teo, String real, String cob) {
    TemplateManager tm = erw.GetTemplateManager();
    tm.SetLabel("LAB014", teo);
    tm.SetLabel("LAB015", real);
    if (cob.length() >= cob.indexOf('.') + 3) {
      cob = cob.substring(0, cob.indexOf('.') + 3);
      // modificacion jlt cambiamos . por ,
    }
    cob = cob.substring(0, cob.indexOf('.')) + "," +
        cob.substring(cob.indexOf('.') + 1, cob.length());
    tm.SetLabel("LAB016", cob);
  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;
    int iEtiqueta = 0;
    String textoVar;

    // plantilla
    TemplateManager tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
    tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

    // carga los citerios
    tm.SetLabel("CRITERIO1",
                res.getString("msg7.Text") + paramC1.sAnoI +
                EPanel.SEPARADOR_ANO_SEM + paramC1.sSemanaI
                + res.getString("msg8.Text") + " " + paramC1.sAnoF +
                EPanel.SEPARADOR_ANO_SEM + paramC1.sSemanaF
                + res.getString("msg9.Text") + paramC1.iGrupo);
    if (paramC1.iGrupo == 1) {
      tm.SetLabel("CRITERIO1",
                  tm.GetLabel("CRITERIO1") + " " + res.getString("msg10.Text"));
    }
    else {
      tm.SetLabel("CRITERIO1",
                  tm.GetLabel("CRITERIO1") + " " + res.getString("msg11.Text"));

    }
    tm.SetLabel("CRITERIO2",
                res.getString("msg12.Text") + paramC1.sCN + "  " +
                paramC1.sCNdesc);

    if (paramC1.sEN.compareTo("") != 0) {

      tm.SetLabel("CRITERIO3",
                  res.getString("msg13.Text") + paramC1.sEN + "  " +
                  paramC1.sENdesc);
      textoVar = EPanel.EQUIPO + paramC1.sEN;
    }
    else {
      tm.SetLabel("CRITERIO3", "");
      textoVar = "";
    }

  }
}
