package infcobper;

import java.util.ResourceBundle;

import capp.CApp;

public class CasosCobPer
    extends CApp {
  public Pan_CobPer elPanel;
  ResourceBundle res;
  public CasosCobPer() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("infcobper.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    elPanel = new Pan_CobPer(a);
    VerPanel("", elPanel);
  }
}