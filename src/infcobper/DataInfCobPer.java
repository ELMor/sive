
package infcobper;

import java.io.Serializable;

public class DataInfCobPer
    implements Serializable {

  public String DS_NOMBRE = new String();
  public int NM_NTOTREAL = 0;
  public int NM_NNOTIFT = 0;
  public int NM_COBERTURAI = 0;
  public int NM_COBERTURAR = 0;
  public boolean bInformeCompleto = false;
  ////////
  public String NM_COBERTURAIS = new String();
  public String NM_COBERTURARS = new String();
  ////////

  public DataInfCobPer(String d, int real, int teor, int cobi, int cobr) {
    DS_NOMBRE = d;
    NM_NTOTREAL = real;
    NM_NNOTIFT = teor;
    NM_COBERTURAI = cobi;
    NM_COBERTURAR = cobr;
  }

  //// modificación para poder pasar parte decimal con ceros
  public DataInfCobPer(String d, int real, int teor, String cobiS, String cobrS) {
    DS_NOMBRE = d;
    NM_NTOTREAL = real;
    NM_NNOTIFT = teor;
    NM_COBERTURAIS = cobiS;
    NM_COBERTURARS = cobrS;
  }
  /////
}
