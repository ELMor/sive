package infcobper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import eqNot.DataEqNot;
import eqNot.DataNotifSem;
import sapp.DBServlet;
import zbs.DataZBS;

public class SrvEqNotCen
    extends DBServlet {

  public static final String name = "servlet/SrvEqNotCen";

  //Modos de operaci�n del Servlet
  public static final int servletOBTENER_X_CODIGO = 3;
  public static final int servletOBTENER_X_DESCRIPCION = 4;
  public static final int servletSELECCION_X_CODIGO = 5;
  public static final int servletSELECCION_X_DESCRIPCION = 6;

  String anoHasta = null;
  String semHasta = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    CLista data = new CLista();
    DataEqNot datos = null;
    DataZBS datos2 = null;
    DataEqNot datos3 = null;
    DataNotifSem datosSemana = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {
      // modos de operaci�n
      switch (opmode) {

        // b�squeda
        case servletSELECCION_X_CODIGO:
        case servletSELECCION_X_DESCRIPCION:

          datos = (DataEqNot) param.firstElement();
          // prepara la query
          if (param.getFilter().length() > 0) {
            if (opmode == servletSELECCION_X_CODIGO) {
              query = "select * from sive_e_notif "
                  +
                  " where CD_E_NOTIF like ? and CD_E_NOTIF > ? and CD_CENTRO = ? "
                  + " order by CD_E_NOTIF";
            }
            else {
              query = "select * from sive_e_notif "
                  + " where upper(DS_E_NOTIF) like upper(?) and upper(DS_E_NOTIF) > upper(?) and CD_CENTRO = ? "
                  + " order by DS_E_NOTIF";
            }
          }
          else {
            if (opmode == servletSELECCION_X_CODIGO) {
              query = "select * from sive_e_notif "
                  + " where CD_E_NOTIF like ? and CD_CENTRO = ? "
                  + " order by CD_E_NOTIF";
            }
            else {
              query = "select * from sive_e_notif "
                  + " where upper(DS_E_NOTIF) like upper(?) and CD_CENTRO = ? "
                  + " order by DS_E_NOTIF";
            }
          }

          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);
          // filtro
          if (opmode == servletSELECCION_X_CODIGO) {
            st.setString(1, datos.getEquipo().trim() + "%");
          }
          else {
            st.setString(1, "%" + datos.getEquipo().trim() + "%");
            // paginaci�n
          }
          if (param.getFilter().length() > 0) {
            st.setString(2, param.getFilter().trim());
            st.setString(3, datos.getCentro());
          }
          else {
            st.setString(2, datos.getCentro());
          }
          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (opmode == servletSELECCION_X_CODIGO) {
                data.setFilter( ( (DataEqNot) data.lastElement()).getEquipo());
              }
              else {
                data.setFilter( ( (DataEqNot) data.lastElement()).getDesEquipo());

              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // a�ade un nodo
            data.addElement(new DataEqNot(rs.getString("CD_E_NOTIF"),
                                          rs.getString("DS_E_NOTIF"),
                                          rs.getString("CD_CENTRO"),
                                          rs.getString("CD_NIVEL_1"),
                                          rs.getString("CD_NIVEL_2"),
                                          rs.getString("CD_SEMEPI"),
                                          rs.getString("CD_ANOEPI"),
                                          rs.getString("CD_ZBS"),
                                          rs.getString("DS_RESPEQUIP"),
                                          rs.getString("DS_TELEF"),
                                          rs.getString("DS_FAX"),
                                          rs.getInt("NM_NOTIFT"),
                                          rs.getString("CD_OPE"),
                                          rs.getString("IT_BAJA"), false));
            i++;
          }

          rs.close();
          st.close();

          break;

          // obtener
        case servletOBTENER_X_CODIGO:
        case servletOBTENER_X_DESCRIPCION:

          datos = (DataEqNot) param.firstElement();
          // prepara la query

          if (opmode == servletOBTENER_X_CODIGO) {
            query = "select * from sive_e_notif "
                + " where CD_E_NOTIF = ? and CD_CENTRO = ? "
                + " order by CD_E_NOTIF";
          }
          else {
            query = "select * from sive_e_notif "
                + " where DS_E_NOTIF = ? and CD_CENTRO = ? "
                + " order by CD_E_NOTIF";

            // prepara la lista de resultados
          }
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);
          // filtro
          st.setString(1, datos.getEquipo().trim());
          st.setString(2, datos.getCentro().trim());

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              data.setFilter( ( (DataEqNot) data.lastElement()).getEquipo());
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // a�ade un nodo
            data.addElement(new DataEqNot(rs.getString("CD_E_NOTIF"),
                                          rs.getString("DS_E_NOTIF"),
                                          rs.getString("CD_CENTRO"),
                                          rs.getString("CD_NIVEL_1"),
                                          rs.getString("CD_NIVEL_2"),
                                          rs.getString("CD_SEMEPI"),
                                          rs.getString("CD_ANOEPI"),
                                          rs.getString("CD_ZBS"),
                                          rs.getString("DS_RESPEQUIP"),
                                          rs.getString("DS_TELEF"),
                                          rs.getString("DS_FAX"),
                                          rs.getInt("NM_NOTIFT"),
                                          rs.getString("CD_OPE"),
                                          rs.getString("IT_BAJA"), false));
            i++;
          }

          rs.close();
          st.close();

          break;
      } // switch
      con.commit();
    }
    catch (Exception er) {
      con.rollback();
      throw er;
    }
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    data.trimToSize();
    return data;
  }
}