package infcobper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import capp.CLista;
import infcobsem.DataInfCobSem;
import sapp.DBServlet;

public class SrvInfCobPer
    extends DBServlet {

  // informes
  final int erwCASOS_EDO = 1;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;

    ResultSet rs = null;

    // control
    int i = 1;
    int posTotal = 1;
    int posRelativa = 1;
    int paso = 1;
    String totCod = null;
    int totTeorico = 0;
    int totReal = 0;
    int totCoberturai = 0;
    int totCoberturar = 0;
    int todoTeorico = 0;
    int todoReal = 0;
    int todoCoberturai = 0;
    int todoCoberturar = 0;
    Float parteReal = new Float(0);
    Float todoParteReal = new Float(0);
    int todoLineas = 0;
    String sCodigos = null;
    DataInfCobSem Auxiliar = null;
    Vector guiaSemana = new Vector();
    // jlt
    Float aux2 = new Float(0);

    // buffers
    String sCod;
    int iNum;
    ParamC11_12 dat = null;
    DataInfCobSem datoAuxi = null;

    // objetos de datos
    CLista data = new CLista();
    Vector registros = new Vector();
    Vector registrosAux = new Vector();
    Vector totales = new Vector();
    Vector ultSem = new Vector();

    ////#// System_out.println("SrvInfCobPer: entro en el DOPOST" );

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    dat = (ParamC11_12) param.firstElement();
    // modos de operaci�n
    switch (opmode) {

      // l�neas del modelo
      case erwCASOS_EDO:

        // Primero calculamos los totales
        todoReal = 0;
        todoCoberturai = 0;
        todoCoberturar = 0;
        todoTeorico = 0;

        String queryT = new String();
        String periodo = new String();

        if (dat.sEN.compareTo("") == 0) {
          // s�lo centro
          queryT = "select sum(NM_NTOTREAL),sum(NM_NNOTIFT), count(*) "
              + " from SIVE_NOTIF_SEM "
              +
              " where CD_E_NOTIF in (select CD_E_NOTIF from SIVE_E_NOTIF where CD_CENTRO=?) ";
        }
        else {
          queryT = "select sum(NM_NTOTREAL),sum(NM_NNOTIFT), count(*) "
              + " from SIVE_NOTIF_SEM "
              + " where (CD_E_NOTIF=?) ";
        }

        // a�adimos rango de b�squeda

        if (dat.sAnoI.equals(dat.sAnoF)) {
          periodo = " and CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ";
        }
        else {
          periodo = " and ((CD_ANOEPI = ? and CD_SEMEPI>= ? )or "
              + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
              + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))";

        }
        queryT = queryT + periodo;

        ////#// System_out.println("SrvInfCobPer: consulta totales " + queryT);

        // lanza la query
        st = con.prepareStatement(queryT);
        // centro o equipo
        if (dat.sEN.compareTo("") == 0) {
          st.setString(1, dat.sCN);
        }
        else {
          st.setString(1, dat.sEN);
        }

        if (dat.sAnoI.equals(dat.sAnoF)) {
          st.setString(2, dat.sSemanaI);
          st.setString(3, dat.sSemanaF);
          st.setString(4, dat.sAnoF);
        }
        else {
          // a�o desde
          st.setString(2, dat.sAnoI);
          st.setString(4, dat.sAnoI);
          // a�o hasta
          st.setString(5, dat.sAnoF);
          st.setString(6, dat.sAnoF);
          // semana desde
          st.setString(3, dat.sSemanaI);
          // semana hasta
          st.setString(7, dat.sSemanaF);
        }

        /*//#// System_out.println("INFORME:env�o fechas..." + dat.sAnoI + "  "
                           + dat.sSemanaI + "  " + dat.sAnoF + "  "
                           + dat.sSemanaF  );
        */
       todoReal = 0;
        todoTeorico = 0;
        todoLineas = 0;
        todoCoberturai = 0;
        todoCoberturar = 0;

        rs = st.executeQuery();
        ////#// System_out.println("INFORME:hecho...");

        // extrae la p�gina requerida
        if (rs.next()) {
          todoReal = rs.getInt(1);
          todoTeorico = rs.getInt(2);
          todoLineas = rs.getInt(3);
        } //while que recorre el ResultSet

        rs.close();
        rs = null;
        st.close();
        st = null;

        if (todoTeorico > 0) {
          Float aux = new Float( (float) todoReal / (float) todoTeorico);
          ////#// System_out.println("aux.floatValue() " +  aux.floatValue());

          // jlt
          aux2 = new Float(aux.floatValue() * (float) 100);

          aux = new Float(aux.floatValue() * (float) 100);
          todoCoberturai = aux.intValue();

          todoParteReal = new Float( (aux.floatValue() -
                                      ( (float) todoCoberturai)) * (float) 100);
          //todoParteReal = new Float(aux.floatValue()*(float)100);
          todoCoberturar = todoParteReal.intValue();
        }
        else {
          todoCoberturai = 0;
          todoCoberturar = 0;
        }

        Integer iTot = new Integer(todoTeorico);
        totales.addElement(iTot);

        iTot = new Integer(todoReal);
        totales.addElement(iTot);

        Float fTot = new Float(0);

        /////////////  modificacion jlt
        String a = aux2.toString();

        String e = a.substring(0, a.indexOf('.'));
        String d = a.substring(a.indexOf('.') + 1, a.length());

        if (d.length() >= 3) {
          String d1 = d.substring(0, 1);
          String d2 = d.substring(1, 2);
          String d3 = d.substring(2, 3);
          Integer i3 = new Integer(d3);
          Integer i2 = new Integer(d2);
          Integer i1 = new Integer(d1);
          int suma = 0;
          if (i3.intValue() >= 5) {
            suma = i2.intValue() + 1;
            d = d1 + (new Integer(suma)).toString();

            if (i2.intValue() == 9) {
              suma = i1.intValue() + 1;
              d = (new Integer(suma)).toString() + (new Integer(0)).toString();
            }

          }
          else {
            d = d1 + d2;
          }

          fTot = new Float(e + '.' + d);
          //////////////
        }
        else {

          fTot = new Float(todoCoberturai +
                           ( (float) todoCoberturar / (float) 100));
        }

        totales.addElement(fTot);

        iTot = new Integer(todoLineas / dat.iGrupo);
        totales.addElement(iTot);

        /*//#// System_out.println("SrvInfCobPer: totales "
                          + todoTeorico + "  "
                          + todoReal + "   "
                          + todoCobertura + "  "
                          + iTot );
                         + totales.elementAt(0).toString() + "  "
                         + totales.elementAt(1).toString() + "  "
                         + totales.elementAt(2).toString() + "  "
                         + totales.elementAt(3).toString() );*/

        String queryV = new String();

        // Ahora buscamos los valores de la consulta
        // peticion de trama
        if (dat.sEN.compareTo("") == 0) {
          // s�lo centro
          queryV =
              "select CD_ANOEPI, CD_SEMEPI, sum(NM_NTOTREAL),sum(NM_NNOTIFT) "
              + " from SIVE_NOTIF_SEM "
              +
              " where CD_E_NOTIF in(select CD_E_NOTIF from SIVE_E_NOTIF where CD_CENTRO=?) ";
        }
        else {
          queryV =
              "select CD_ANOEPI, CD_SEMEPI, sum(NM_NTOTREAL),sum(NM_NNOTIFT) "
              + " from SIVE_NOTIF_SEM "
              + " where (CD_E_NOTIF=?) ";
        }

        // a�adimos rango de b�squeda
        //queryV = queryV + " and ((CD_ANOEPI=? and CD_SEMEPI>=?) or (CD_ANOEPI>? and CD_ANOEPI<?) or (CD_ANOEPI=? and CD_SEMEPI<=?)) " ;
        queryV = queryV + periodo;
        queryV = queryV +
            " group by CD_ANOEPI, CD_SEMEPI  order by CD_ANOEPI, CD_SEMEPI ";

        ////#// System_out.println(" Consulta : " + queryV);

        // lanza la query
        st = con.prepareStatement(queryV);
        // centro o equipo
        if (dat.sEN.compareTo("") == 0) {
          st.setString(1, dat.sCN);
        }
        else {
          st.setString(1, dat.sEN);
        }

        if (dat.sAnoI.equals(dat.sAnoF)) {
          st.setString(2, dat.sSemanaI);
          st.setString(3, dat.sSemanaF);
          st.setString(4, dat.sAnoF);
        }
        else {
          // a�o desde
          st.setString(2, dat.sAnoI);
          st.setString(4, dat.sAnoI);
          // a�o hasta
          st.setString(5, dat.sAnoF);
          st.setString(6, dat.sAnoF);
          // semana desde
          st.setString(3, dat.sSemanaI);
          // semana hasta
          st.setString(7, dat.sSemanaF);
        }
        rs = st.executeQuery();

        int iAgru = 0;
        int iEstado;

        // Agrupacion y paginacion
        /*
           registros por pagina = agrupacion * lineas por pagina
           primer registro de la pagina P = P*agru*lineas pagina
         */

        guiaSemana = obtenerGuiaSemana(con, dat);

        iAgru = 0;
        i = 1;
        iEstado = CLista.listaVACIA;

        // Voy a la pagina que solicita
        int iSem = dat.iPagina * DBServlet.maxSIZE * dat.iGrupo;

        ////#// System_out.println("SrvInfCobPer: iSem=" + iSem + "guiaSem.size:" + guiaSemana.size());
        iSem = 0;
        // Se cambia la fecha de inicio de la consulta
        boolean resultSetSituado = true;

        // Veo si hay que situar el resultSet en la nueva pagina
        /*
                     if (iSem!=0)
          resultSetSituado = false;
                     else
          resultSetSituado = true;
         */
        boolean leer = true;
        String sSem = new String();
        String sAno = new String();
        int iReal = 0;
        int iTeor = 0;

        String guia[] = new String[2];

        String anos[] = new String[dat.iGrupo];
        String sems[] = new String[dat.iGrupo];
        int reales[] = new int[dat.iGrupo];
        int teoricos[] = new int[dat.iGrupo];
        boolean hay[] = new boolean[dat.iGrupo];

        ultSem = new Vector();

        while (iSem < guiaSemana.size()) {

          // obtengo una nueva semana
          guia = (String[]) guiaSemana.elementAt(iSem);

          // control de tama�o
          if ( (i > DBServlet.maxSIZE) && (! (dat.bInformeCompleto))) {
            data.setState(CLista.listaINCOMPLETA);
            //data.setFilter( ((DataC1)registros.lastElement()).DS_PROCESO );
            rs.close();
            rs = null;
            st.close();
            st = null;

            ultSem = new Vector();
            ultSem.addElement(guia[0]);
            ultSem.addElement(guia[1]);
            ////#// System_out.println("SrvInfCobPer: ultSemana " + guia[0] + "  " + guia[1] + "   " + iSem);
            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);

            // Veo si debo tomar otro registro del resultSet
          }
          if (leer) {
            if (resultSetSituado) {
              if (rs.next()) {
                sAno = rs.getString("CD_ANOEPI");
                sSem = rs.getString("CD_SEMEPI");
                iReal = rs.getInt(3);
                iTeor = rs.getInt(4);
                ////#// System_out.println("He leido el siguiente " + sAno +"  "+ sSem +"  "+ iReal);
              }
              else {
                // ya no hay mas registros en el resultado
                ////#// System_out.println("ya no hay mas registros en el resultado");
                sAno = new String();
                sSem = new String();
                iReal = 0;
                iTeor = 0;
              }
            }
            else { // viene de mas datos
              ////#// System_out.println("RESULTSET NO SITUADO");
              Vector temp = new Vector();
              temp = this.situarResultSet(guia, rs);
              sAno = (String) temp.elementAt(0);
              sSem = (String) temp.elementAt(1);
              Integer iTemp = (Integer) temp.elementAt(2);
              iReal = iTemp.intValue();
              iTemp = (Integer) temp.elementAt(3);
              iTeor = iTemp.intValue();
              resultSetSituado = true;
            }
          }

          if (guia[0].equals(sAno) && guia[1].equals(sSem)) {
            ////#// System_out.println("Son iguales");
            anos[iAgru] = sAno;
            sems[iAgru] = sSem;
            reales[iAgru] = iReal;
            teoricos[iAgru] = iTeor;
            hay[iAgru] = true;
            leer = true;
          }
          else {
            anos[iAgru] = guia[0];
            sems[iAgru] = guia[1];
            reales[iAgru] = 0;
            teoricos[iAgru] = 0;
            hay[iAgru] = false;
            leer = false;
          }

          if (iAgru == dat.iGrupo - 1) {
            // Se han obtenido las semanas de una agrupacion y
            // se rellena un dato

            boolean agrupar = false;
            // Se comprueba si en esta agrupacion hay
            // algun dato valido para generar la agrupacion o no
            for (int j = 0; j <= iAgru; j++) {
              agrupar = agrupar || hay[j];
            }
            if (agrupar) {
////#// System_out.println("antes de las semanas de una agrupacion AGRUPAR iAgru=" + iAgru);
              registros.addElement(agruparSemanas(iAgru, anos, sems,
                                                  reales, teoricos));
              // Cuenta del numero de agrupaciones por trama
              i++;
            }
            // Se inicializa para la siguiente agrupacion
            iAgru = 0;
            anos = new String[dat.iGrupo];
            sems = new String[dat.iGrupo];
            reales = new int[dat.iGrupo];
            teoricos = new int[dat.iGrupo];
            hay = new boolean[dat.iGrupo];

          }
          else {
            iAgru++;

            //i++;
          }
          iSem++;

        }
        if (iAgru != 0) {
          // ultima agrupacion que no esta completa

          boolean agrupar = false;
          // Se comprueba si en esta agrupacion hay
          // algun dato valido para generar la agrupacion o no
          for (int j = 0; j <= iAgru - 1; j++) {
            agrupar = agrupar || hay[j];
          }
          if (agrupar) {
            ////#// System_out.println("ultima agrupacion incompleta AGRUPAR iAgru=" + iAgru);
            registros.addElement(agruparSemanas(iAgru - 1, anos,
                                                sems, reales,
                                                teoricos));
          }
        }

        break;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    registros.trimToSize();
    data.addElement(registros);
    totales.trimToSize();
    data.addElement(totales);
    ultSem.trimToSize();
    data.addElement(ultSem);
    data.trimToSize();
    return data;
  }

  private Vector obtenerGuiaSemana(Connection con, ParamC11_12 parCons) throws
      Exception {
    Vector vec = new Vector();
    String sem[];
    String queryGuia = new String();
    PreparedStatement st1;
    if (parCons.sAnoI.equals(parCons.sAnoF)) {
      queryGuia = "select CD_ANOEPI, CD_SEMEPI from SIVE_SEMANA_EPI "
          + " where CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? "
          + " order by CD_ANOEPI, CD_SEMEPI ";
      st1 = con.prepareStatement(queryGuia);
      st1.setString(1, parCons.sSemanaI);
      st1.setString(2, parCons.sSemanaF);
      st1.setString(3, parCons.sAnoI);
    }
    else {
      queryGuia = "select CD_ANOEPI, CD_SEMEPI from SIVE_SEMANA_EPI "
          + " where ((CD_ANOEPI = ? and CD_SEMEPI >= ?) or (CD_ANOEPI > ? and CD_ANOEPI < ?) or (CD_ANOEPI = ? and CD_SEMEPI <= ?)) "
          + " order by CD_ANOEPI, CD_SEMEPI ";
      st1 = con.prepareStatement(queryGuia);
      st1.setString(1, parCons.sAnoI);
      st1.setString(2, parCons.sSemanaI);
      st1.setString(3, parCons.sAnoI);
      st1.setString(4, parCons.sAnoF);
      st1.setString(5, parCons.sAnoF);
      st1.setString(6, parCons.sSemanaF);
    }

    ////#// System_out.println(queryGuia);

    ResultSet rs1 = st1.executeQuery();
    while (rs1.next()) {
      sem = new String[2];
      sem[0] = rs1.getString("CD_ANOEPI");
      sem[1] = rs1.getString("CD_SEMEPI");
      vec.addElement(sem);
    }
    ////#// System_out.println("Obtener semanas : " + vec.size());
    rs1.close();
    rs1 = null;
    st1.close();
    st1 = null;
    return vec;
  }

  private Vector situarResultSet(String[] semana, ResultSet rs1) throws
      Exception {
    Vector reg = new Vector();
    Integer iReal;
    Integer iTeor;
    String semRes;
    String anoRes;
    ////#// System_out.println("Debo SITUAR EL RESULTSET en:" + semana[0] + "  " + semana[1]);

    while (rs1.next()) {

      ////#// System_out.println("Entro en el whike desituarResultSet");

      reg = new Vector();
      reg.addElement(rs1.getString("CD_ANOEPI"));
      anoRes = (String) reg.elementAt(0);
      reg.addElement(rs1.getString("CD_SEMEPI"));
      semRes = (String) reg.elementAt(1);
      iReal = new Integer(rs1.getInt(3));
      reg.addElement(iReal);
      iTeor = new Integer(rs1.getInt(3));
      reg.addElement(iTeor);
      ////#// System_out.println("RESULTSET " + reg.elementAt(0).toString() + "  " + reg.elementAt(1).toString() + "  "  + reg.elementAt(2).toString() );

      if ( (anoRes.compareTo(semana[0]) == 0) &&
          (semRes.compareTo(semana[1]) >= 0)) {
        //(semana[1].compareTo((String)reg.elementAt(1))<=0)) {
        ////#// System_out.println("He SITUADO (ano y sem) el resultSet en " + reg.elementAt(0).toString() + "  " + reg.elementAt(1).toString() + "  "  + reg.elementAt(2).toString() );
        return reg;
      }
      if (anoRes.compareTo(semana[0]) > 0) {
        //(semana[1].compareTo((String)reg.elementAt(1))<=0)) {
        ////#// System_out.println("He SITUADO (ano) el resultSet en " + reg.elementAt(0).toString() + "  " + reg.elementAt(1).toString() + "  "  + reg.elementAt(2).toString() );
        return reg;
      }
    }
    // No ha encontrado nada en el resultSet
    // No se si se puede dar este caso, pero por si acaso
    reg.addElement(new String());
    reg.addElement(new String());
    reg.addElement(new Integer(0));
    reg.addElement(new Integer(0));
    ////#// System_out.println("No ha encontrado nada en el RESULTSET" );
    return reg;
  }

  private DataInfCobPer agruparSemanas(int iAgru, String[] anos,
                                       String[] sems, int[] reales,
                                       int[] teoricos) {
    String desde = anos[0] + "  " + sems[0];
    String hasta = anos[iAgru] + "  " + sems[iAgru];
    int numReales = 0;
    int numTeoricos = 0;
    Integer intAux;
    for (int j = 0; j <= iAgru; j++) {
      intAux = new Integer(reales[j]);
      numReales = numReales + intAux.intValue();
      intAux = new Integer(teoricos[j]);
      numTeoricos = numTeoricos + intAux.intValue();
    }

    int coberturai;
    int coberturar;

    //
    String coberturaiS = new String();
    String coberturarS = new String();
    //
    Float parteReal;

    if (numTeoricos > 0) {
      Float aux = new Float( (float) numReales / (float) numTeoricos);
      Float aux3 = new Float(aux.floatValue() * (float) 100);
      aux = new Float(aux.floatValue() * (float) 100);

      coberturai = aux.intValue();

      /////////////  modificacion jlt
      String a = aux3.toString();
      String e = a.substring(0, a.indexOf('.'));
      String d = a.substring(a.indexOf('.') + 1, a.length());

      if (d.length() >= 3) {
        String d1 = d.substring(0, 1);
        String d2 = d.substring(1, 2);
        String d3 = d.substring(2, 3);
        Integer i3 = new Integer(d3);
        Integer i2 = new Integer(d2);
        Integer i1 = new Integer(d1);
        int suma = 0;
        if (i3.intValue() >= 5) {
          suma = i2.intValue() + 1;
          d = d1 + (new Integer(suma)).toString();

          if (i2.intValue() == 9) {
            suma = i1.intValue() + 1;
            d = (new Integer(suma)).toString() + (new Integer(0)).toString();
          }

        }
        else {
          d = d1 + d2;
        }

        parteReal = new Float(d);

        //////////////
      }
      else {

        parteReal = new Float( (aux.floatValue() - (float) coberturai) *
                              (float) 100);
      }

      coberturar = parteReal.intValue();
      // modificaci�n para poder pasar parte decimal con ceros
      coberturaiS = e;
      coberturarS = d;
      //

    }
    else {
      coberturai = 0;
      coberturar = 0;
    }

    /*//#// System_out.println("Agrupar semanas " + desde + "      " + hasta
                            + " " + numReales
                            + " " + numTeoricos
                            + " " + cobertura);
    */

   //return new DataInfCobPer(desde + "      " + hasta, numReales,
   //                         numTeoricos, coberturai, coberturar);

   // modificaci�n para poder pasar parte decimal con ceros
   return new DataInfCobPer(desde + "      " + hasta, numReales,
                            numTeoricos, coberturaiS, coberturarS);

  }

}
