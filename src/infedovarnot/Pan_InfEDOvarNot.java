package infedovarnot;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class Pan_InfEDOvarNot
    extends EPanel {

  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 1;

  final String strSERVLET = "servlet/SrvInfEDOvarNot";
  final String strLOGO_CCAA = "images/ccaa.gif";

  final int erwCASOS_EDO = 1;
  public Param_C9_3_9 paramC1 = new Param_C9_3_9();
  public DataEDOvarNot datosPar = null;

  // estructuras de datos
  protected Vector vAgrupaciones;
  protected Vector vTotales;
  protected Vector vCasos;
  protected CLista lista;
  public CApp app;

  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // Para pedir mas paginas
  protected Vector ultSemana = new Vector();

  // report
  ERW erw = null;

  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;
  Integer regMostrados = new Integer(0);

  public Pan_InfEDOvarNot(CApp a) {
    super(a);
    app = a;
    res = ResourceBundle.getBundle("infedovarnot.Res" + a.getIdioma());
    try {
      stub = new StubSrvBD();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + "erw/ERWEDOvarNot.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar
  public void mostrar() {

    erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // todo el informe
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;

    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        PrepararInforme();
        param.setFilter(lista.getFilter());
        // obtiene los datos del servidor
        //datosPar.iPagina++;
        param.addElement(datosPar);
        param.setFilter("");
        param.setIdioma(app.getIdioma());
        param.trimToSize();
        stub.setUrl(new URL(app.getURL() + strSERVLET));

        param.setLogin(app.getLogin());
        param.setLortad(app.getParameter("LORTAD"));
        lista = (CLista) stub.doPost(erwCASOS_EDO, param);

        v = (Vector) lista.elementAt(0);
        vTotales = (Vector) lista.elementAt(1);

        for (int j = 0; j < v.size(); j++) {
          vCasos.addElement(v.elementAt(j));

          // control de registros
        }
        Integer tot = (Integer) lista.elementAt(1);
        this.setTotalRegistros(tot.toString());
        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // repintado
        erwClient.refreshReport(true);

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  protected boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    boolean elBoolean = true;
    CLista param = new CLista();

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      PrepararInforme();

      // obtiene los datos del servidor
      paramC1.iPagina = 0;

      // �todos los datos?
      datosPar.bInformeCompleto = conTodos;

      //param.addElement(paramC1);
      param.addElement(datosPar);
      param.setIdioma(app.getIdioma());
      param.trimToSize();

      stub.setUrl(new URL(app.getURL() + strSERVLET));
      lista = new CLista();
      lista.setState(CLista.listaVACIA);

      param.setLogin(app.getLogin());
      param.setLortad(app.getParameter("LORTAD"));
      lista = (CLista) stub.doPost(erwCASOS_EDO, param);

      // control de registros
      if (lista == null) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("ms6.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
        elBoolean = false;
      }
      else {

        vCasos = (Vector) lista.elementAt(0);
        Integer tot = (Integer) lista.elementAt(1);
        this.setTotalRegistros(tot.toString());
        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // establece las matrices de datos
        Vector retval = new Vector();
        retval.addElement("SN1SN2 = SN1SN2");
        retval.addElement("SN1NN2 = SN1NN2");
        retval.addElement("NN1SN2 = NN1SN2");
        retval.addElement("NN1NN2 = NN1NN2");

        dataHandler.RegisterTable(vCasos, "SIVE_C9_3_9", retval, null);
        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));
        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        elBoolean = true;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();
      elBoolean = false;
    }

    modoOperacion = modoNORMAL;
    Inicializar();
    return elBoolean;
  }

  // este informe no tiene grafica
  public void MostrarGrafica() {
  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;
    int iEtiqueta = 0;
    String sLabels[] = {
        "CRITERIO2", "CRITERIO3"};

    TemplateManager tm = null;

    // plantilla
    tm = erw.GetTemplateManager();

    // carga los logos
    tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
    tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

    //E Cargar� el login del usuario
    //tm.SetLabel("LOGIN", app.getLogin());

    // carga los citerios
    tm.SetLabel("CRITERIO1",
                res.getString("msg9.Text") + datosPar.sAnoDesde +
                res.getString("msg10.Text") + datosPar.sSemDesde +
                res.getString("msg11.Text") + datosPar.sAnoHasta +
                res.getString("msg12.Text") + datosPar.sSemHasta);

    iEtiqueta = 0;
    if (!datosPar.sDsEnfermedad.equals("")) {
      tm.SetLabel(sLabels[iEtiqueta],
                  res.getString("msg13.Text") + datosPar.sCdEnfermedad + " " +
                  datosPar.sDsEnfermedad);
      iEtiqueta++;
    }

    if (!datosPar.sDsArea.equals("")) {
      sBuffer = res.getString("msg14.Text") + datosPar.sCdArea + " " +
          datosPar.sDsArea;
      if (!datosPar.sDsDistrito.equals("")) {
        sBuffer = sBuffer + res.getString("msg15.Text") + datosPar.sCdDistrito +
            " " + datosPar.sDsDistrito;
      }
      tm.SetLabel(sLabels[iEtiqueta], sBuffer);
      iEtiqueta++;
    }

    for (int i = iEtiqueta; i < 2; i++) {
      tm.SetLabel(sLabels[i], "");

    }
    tm.SetLabel("LAB012", datosPar.sDsNiv1);
    tm.SetLabel("LAB013", datosPar.sDsNiv2);

  }
}
