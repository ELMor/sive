
package infedovarnot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

//import infcobsem.*;
// LORTAD
import Registro.RegistroConsultas;
import capp.CLista;
import sapp.DBServlet;

public class SrvInfEDOvarNot
    extends DBServlet {

  protected String query = "";
  protected String periodo = "";

  // informes
  final int erwCASOS_EDO = 1;

  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;

    ResultSet rs = null;

    // control
    int i = 1;
    DataEDOvarNot Auxiliar = null;
    Vector totales = new Vector();
    int totalRegistros = 0;

    String tNivel =
        "select NM_EDO from sive_notif_edoi where CD_FUENTE = 'E' and CD_E_NOTIF in("
        + "select CD_E_NOTIF from sive_e_notif where CD_CENTRO in("
        + "select CD_CENTRO from sive_c_notif where CD_NIVASIS=?))";

    String sSN1SN2 = "0";
    String sSN1NN2 = "0";
    String sNN1SN2 = "0";
    String sNN1NN2 = "0";

    // buffers
    String sCod;
    int iNum;
    DataEDOvarNot dat = null;
    Param_C9_3_9 datoAuxi = null;

    // objetos de datos
    CLista data = new CLista();
    Vector registros = new Vector();
    Vector registrosAux = new Vector();
    Vector ultSem = new Vector();

    /****************** APLICACION DE LORTAD ********************************/
    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;
          /****************** APLICACION DE LORTAD ********************************/

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);
    con.setAutoCommit(true);

    dat = (DataEDOvarNot) param.firstElement();
    // modos de operaci�n
    switch (opmode) {

      // l�neas del modelo
      case erwCASOS_EDO:
        try {
          // Nivel 1+ y nivel 2+
          query = "select count(distinct NM_EDO) from sive_edoind "
              + "where NM_EDO in(" + tNivel + ") and NM_EDO in (" + tNivel +
              ")";

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {
            periodo =
                " and CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ";
          }
          else {
            periodo = " and ((CD_ANOEPI = ? and CD_SEMEPI>= ? )or "
                + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
                + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))";

          }
          query += periodo;

          if (dat.sCdArea.compareTo("") != 0) {
            query += " and CD_NIVEL_1='" + dat.sCdArea + "' ";
          }

          if (dat.sCdEnfermedad.compareTo("") != 0) {
            query += " and CD_ENFCIE='" + dat.sCdEnfermedad + "' ";
          }

          if (dat.sCdDistrito.compareTo("") != 0) {
            query += " and CD_NIVEL_2='" + dat.sCdDistrito + "' ";
          }

          ////#// System_out.println(query);
          st = con.prepareStatement(query);

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {
            // nivel asistencial 1
            st.setString(1, dat.sCdNiv1);
            // nivel asistencial 1
            st.setString(2, dat.sCdNiv2);
            // semana desde
            st.setString(3, dat.sSemDesde);
            // semana hasta
            st.setString(4, dat.sSemHasta);
            // a�o
            st.setString(5, dat.sAnoDesde);

            registroConsultas.insertarParametro(dat.sCdNiv1);
            registroConsultas.insertarParametro(dat.sCdNiv2);
            registroConsultas.insertarParametro(dat.sSemDesde);
            registroConsultas.insertarParametro(dat.sSemHasta);
            registroConsultas.insertarParametro(dat.sAnoDesde);
          }
          else { // los a�os inicial y final son distintos
            // a�o inicial
            st.setString(3, dat.sAnoDesde);
            // semana inicial
            st.setString(4, dat.sSemDesde);
            st.setString(5, dat.sAnoDesde);
            // a�o final
            st.setString(6, dat.sAnoHasta);
            st.setString(7, dat.sAnoHasta);
            // semana final
            st.setString(8, dat.sSemHasta);

            registroConsultas.insertarParametro("");
            registroConsultas.insertarParametro("");
            registroConsultas.insertarParametro(dat.sAnoDesde);
            registroConsultas.insertarParametro(dat.sSemDesde);
            registroConsultas.insertarParametro(dat.sAnoDesde);
            registroConsultas.insertarParametro(dat.sAnoHasta);
            registroConsultas.insertarParametro(dat.sAnoHasta);
            registroConsultas.insertarParametro(dat.sSemHasta);
          }

          registroConsultas.registrar("SIVE_EDOIND",
                                      query,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvInfEDOvarNot",
                                      true);

          rs = st.executeQuery();

          if (rs.next()) {
            sSN1SN2 = Integer.toString(rs.getInt(1));
          }
          ////#// System_out.println(sSN1SN2);
          rs.close();
          st.close();
          rs = null;
          st = null;

          // Nivel 1+ y nivel 2-
          query = "select count(distinct NM_EDO) from sive_edoind "
              + "where NM_EDO in(" + tNivel + ") and NM_EDO not in (" + tNivel +
              ")";

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {
            periodo =
                " and CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ";
          }
          else {
            periodo = " and ((CD_ANOEPI = ? and CD_SEMEPI>= ? )or "
                + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
                + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))";

          }
          query += periodo;

          if (dat.sCdArea.compareTo("") != 0) {
            query += " and CD_NIVEL_1='" + dat.sCdArea + "' ";
          }

          if (dat.sCdEnfermedad.compareTo("") != 0) {
            query += " and CD_ENFCIE='" + dat.sCdEnfermedad + "' ";
          }

          ////#// System_out.println(query);
          st = con.prepareStatement(query);

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {
            // nivel asistencial 1
            st.setString(1, dat.sCdNiv1);
            // nivel asistencial 1
            st.setString(2, dat.sCdNiv2);
            // semana desde
            st.setString(3, dat.sSemDesde);
            // semana hasta
            st.setString(4, dat.sSemHasta);
            // a�o
            st.setString(5, dat.sAnoDesde);

            registroConsultas.insertarParametro(dat.sCdNiv1);
            registroConsultas.insertarParametro(dat.sCdNiv2);
            registroConsultas.insertarParametro(dat.sSemDesde);
            registroConsultas.insertarParametro(dat.sSemHasta);
            registroConsultas.insertarParametro(dat.sAnoDesde);
          }
          else { // los a�os inicial y final son distintos

            // a�o inicial
            st.setString(3, dat.sAnoDesde);
            // semana inicial
            st.setString(4, dat.sSemDesde);
            st.setString(5, dat.sAnoDesde);
            // a�o final
            st.setString(6, dat.sAnoHasta);
            st.setString(7, dat.sAnoHasta);
            // semana final
            st.setString(8, dat.sSemHasta);

            registroConsultas.insertarParametro("");
            registroConsultas.insertarParametro("");
            registroConsultas.insertarParametro(dat.sAnoDesde);
            registroConsultas.insertarParametro(dat.sSemDesde);
            registroConsultas.insertarParametro(dat.sAnoDesde);
            registroConsultas.insertarParametro(dat.sAnoHasta);
            registroConsultas.insertarParametro(dat.sAnoHasta);
            registroConsultas.insertarParametro(dat.sSemHasta);
          }
          rs = st.executeQuery();

          registroConsultas.registrar("SIVE_EDOIND",
                                      query,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvInfEDOvarNot",
                                      true);

          if (rs.next()) {
            sSN1NN2 = Integer.toString(rs.getInt(1));
          }
          ////#// System_out.println(sSN1NN2);
          rs.close();
          st.close();
          rs = null;
          st = null;

          // Nivel 1- y nivel 2+
          query = "select count(distinct NM_EDO) from sive_edoind "
              + "where NM_EDO not in(" + tNivel + ") and NM_EDO in (" + tNivel +
              ")";

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {
            periodo =
                " and CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ";
          }
          else {
            periodo = " and ((CD_ANOEPI = ? and CD_SEMEPI>= ? )or "
                + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
                + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))";

          }
          query += periodo;

          if (dat.sCdArea.compareTo("") != 0) {
            query += " and CD_NIVEL_1='" + dat.sCdArea + "' ";
          }

          if (dat.sCdEnfermedad.compareTo("") != 0) {
            query += " and CD_ENFCIE='" + dat.sCdEnfermedad + "' ";
          }

          ////#// System_out.println(query);
          st = con.prepareStatement(query);

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {
            // nivel asistencial 1
            st.setString(1, dat.sCdNiv1);
            // nivel asistencial 1
            st.setString(2, dat.sCdNiv2);
            // semana desde
            st.setString(3, dat.sSemDesde);
            // semana hasta
            st.setString(4, dat.sSemHasta);
            // a�o
            st.setString(5, dat.sAnoDesde);

            registroConsultas.insertarParametro(dat.sCdNiv1);
            registroConsultas.insertarParametro(dat.sCdNiv2);
            registroConsultas.insertarParametro(dat.sSemDesde);
            registroConsultas.insertarParametro(dat.sSemHasta);
            registroConsultas.insertarParametro(dat.sAnoDesde);

          }
          else { // los a�os inicial y final son distintos
            ////#// System_out.println("final-inicial no iguales");
            // a�o inicial
            st.setString(3, dat.sAnoDesde);
            // semana inicial
            st.setString(4, dat.sSemDesde);
            st.setString(5, dat.sAnoDesde);
            // a�o final
            st.setString(6, dat.sAnoHasta);
            st.setString(7, dat.sAnoHasta);
            // semana final
            st.setString(8, dat.sSemHasta);

            registroConsultas.insertarParametro("");
            registroConsultas.insertarParametro("");
            registroConsultas.insertarParametro(dat.sAnoDesde);
            registroConsultas.insertarParametro(dat.sSemDesde);
            registroConsultas.insertarParametro(dat.sAnoDesde);
            registroConsultas.insertarParametro(dat.sAnoHasta);
            registroConsultas.insertarParametro(dat.sAnoHasta);
            registroConsultas.insertarParametro(dat.sSemHasta);
          }

          registroConsultas.registrar("SIVE_EDOIND",
                                      query,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvInfEDOvarNot",
                                      true);

          rs = st.executeQuery();
          ////#// System_out.println("3 lin");
          if (rs.next()) {
            sNN1SN2 = Integer.toString(rs.getInt(1));
          }
          ////#// System_out.println(sNN1SN2);
          rs.close();
          st.close();
          rs = null;
          st = null;

          // Nivel 1+ y nivel 2+
          query = "select count(distinct NM_EDO) from sive_edoind "
              + "where NM_EDO not in(" + tNivel + ") and NM_EDO not in (" +
              tNivel + ")";

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {
            periodo =
                " and CD_SEMEPI >= ? and CD_SEMEPI <= ? and CD_ANOEPI = ? ";
          }
          else {
            periodo = " and ((CD_ANOEPI = ? and CD_SEMEPI>= ? )or "
                + " (CD_ANOEPI > ? and CD_ANOEPI < ? )or "
                + " (CD_ANOEPI = ? and CD_SEMEPI <= ? ))";

          }
          query += periodo;

          if (dat.sCdArea.compareTo("") != 0) {
            query += " and CD_NIVEL_1='" + dat.sCdArea + "' ";
          }

          if (dat.sCdEnfermedad.compareTo("") != 0) {
            query += " and CD_ENFCIE='" + dat.sCdEnfermedad + "' ";
          }

          ////#// System_out.println(query);
          st = con.prepareStatement(query);

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {
            // nivel asistencial 1
            st.setString(1, dat.sCdNiv1);
            // nivel asistencial 1
            st.setString(2, dat.sCdNiv2);
            // semana desde
            st.setString(3, dat.sSemDesde);
            // semana hasta
            st.setString(4, dat.sSemHasta);
            // a�o
            st.setString(5, dat.sAnoDesde);

            registroConsultas.insertarParametro(dat.sCdNiv1);
            registroConsultas.insertarParametro(dat.sCdNiv2);
            registroConsultas.insertarParametro(dat.sSemDesde);
            registroConsultas.insertarParametro(dat.sSemHasta);
            registroConsultas.insertarParametro(dat.sAnoDesde);
          }
          else { // los a�os inicial y final son distintos
            ////#// System_out.println("final-inicial no iguales");
            // a�o inicial
            st.setString(3, dat.sAnoDesde);
            // semana inicial
            st.setString(4, dat.sSemDesde);
            st.setString(5, dat.sAnoDesde);
            // a�o final
            st.setString(6, dat.sAnoHasta);
            st.setString(7, dat.sAnoHasta);
            // semana final
            st.setString(8, dat.sSemHasta);

            registroConsultas.insertarParametro("");
            registroConsultas.insertarParametro("");
            registroConsultas.insertarParametro(dat.sAnoDesde);
            registroConsultas.insertarParametro(dat.sSemDesde);
            registroConsultas.insertarParametro(dat.sAnoDesde);
            registroConsultas.insertarParametro(dat.sAnoHasta);
            registroConsultas.insertarParametro(dat.sAnoHasta);
            registroConsultas.insertarParametro(dat.sSemHasta);
          }

          registroConsultas.registrar("SIVE_EDOIND",
                                      query,
                                      "CD_ENFERMO",
                                      "",
                                      "SrvInfEDOvarNot",
                                      true);

          rs = st.executeQuery();
          ////#// System_out.println("4 lin");
          if (rs.next()) {
            sNN1NN2 = Integer.toString(rs.getInt(1));
          }
          ////#// System_out.println(sNN1NN2);
          rs.close();
          st.close();
          rs = null;
          st = null;

          totales = new Vector();
          datoAuxi = new Param_C9_3_9();
          datoAuxi.NN1NN2 = sNN1NN2;
          datoAuxi.SN1SN2 = sSN1SN2;
          datoAuxi.NN1SN2 = sNN1SN2;
          datoAuxi.SN1NN2 = sSN1NN2;
          totales.addElement(datoAuxi);
        }
        catch (Exception er) {
          er.printStackTrace();

        }
        break;
    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    if (totales.size() > 0) {
      totales.trimToSize();
      data.addElement(totales);
      data.addElement(new Integer(1));
      data.trimToSize();

    }
    else {
      data = null;

    }

    return data;
  }

}
