
package infedovarnot;

import java.util.ResourceBundle;

import capp.CApp;

public class CasosEDOvarNot
    extends CApp {

  ResourceBundle res;

  public CasosEDOvarNot() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("infedovarnot.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;

    VerPanel(res.getString("msg2.Text"), new Pan_EDOvarNot(a), true);
    VerPanel(res.getString("msg2.Text"));
  }

}
