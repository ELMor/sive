package infnoteq;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.TextEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo.DataCat;
import catalogo2.DataCat2;
import cn.DataCN;
import eqNot.DataEqNot;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
import utilidades.PanFechas;
import vCasIn.DataMun;
import zbs.DataZBS;
import zbs.DataZBS2;

public class Pan_NotEq
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  //rutas imagenes
  final String imgLUPA = "images/Magnify.gif";
  final String imgLIMPIAR = "images/vaciar.gif";
  final String imgGENERAR = "images/grafico.gif";

  protected StubSrvBD stubCliente = new StubSrvBD();

  public DataInfNotEq paramC2;

  protected int modoOperacion = modoNORMAL;

  protected Pan_InfNotEq informe;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETMun = "servlet/SrvMun";
  final String strSERVLETProv = "servlet/SrvCat2";
  final String strSERVLETEquipo = "servlet/SrvEqNot";
  final String strSERVLETEnf = "servlet/SrvEnfVigi";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  XYLayout xYLayout = new XYLayout();

  Label lblHasta = new Label();
  Label lblDesde = new Label();
  Label lblEquNot = new Label();
  CCampoCodigo txtCodEquNot = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarEquNot = new ButtonControl();
  TextField txtDesEquNot = new TextField();
  PanFechas fechasDesde;
  PanFechas fechasHasta;
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  textAdapter txtTextAdapter = new textAdapter(this);
  actionListener btnActionListener = new actionListener(this);

  public Pan_NotEq(CApp a) {
    try {

      setApp(a);

      res = ResourceBundle.getBundle("infnoteq.Res" + a.getIdioma());
      informe = new Pan_InfNotEq(a);
      fechasDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      fechasHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      jbInit();
      informe.setEnabled(false);
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(301);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));

    // gestores de eventos
    btnCtrlBuscarEquNot.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    txtCodEquNot.addTextListener(txtTextAdapter);

    //btnCtrlBuscarEnfer.setLabel("btnCtrlBuscarPro1");
    btnCtrlBuscarEquNot.setActionCommand("buscarEquNot");
    txtDesEquNot.setEditable(false);
    txtDesEquNot.setEnabled(false); /*E*/
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    txtCodEquNot.setName("txtCodEquNot");
    txtCodEquNot.setBackground(new Color(255, 255, 150));
    txtCodEquNot.addFocusListener(new Pan_NotEq_txtCodEquNot_focusAdapter(this));
    txtCodEquNot.addKeyListener(new Pan_NotEq_txtCodEquNot_keyAdapter(this));

    lblHasta.setText(res.getString("lblHasta.Text"));
    lblEquNot.setText(res.getString("lblEquNot.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));

    xYLayout.setWidth(596);

    this.setLayout(xYLayout);

    this.add(lblDesde, new XYConstraints(26, 42, 51, -1));
    this.add(fechasDesde, new XYConstraints(40, 42, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 80, 51, -1));
    this.add(fechasHasta, new XYConstraints(40, 80, -1, -1));
    this.add(lblEquNot, new XYConstraints(26, 137, 110, -1));
    this.add(txtCodEquNot, new XYConstraints(143, 137, 77, -1));
    this.add(btnCtrlBuscarEquNot, new XYConstraints(225, 137, -1, -1));
    this.add(txtDesEquNot, new XYConstraints(254, 137, 287, -1));
    this.add(btnLimpiar, new XYConstraints(389, 213, -1, -1));
    this.add(btnInforme, new XYConstraints(475, 213, -1, -1));

    btnCtrlBuscarEquNot.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnLimpiar.setImageURL(new URL(app.getCodeBase(), imgLIMPIAR));
    btnInforme.setImageURL(new URL(app.getCodeBase(), imgGENERAR));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (fechasDesde.txtAno.getText().length() == 0) ||
        (fechasHasta.txtAno.getText().length() == 0) ||
        (fechasDesde.txtCodSem.getText().length() == 0) ||
        (fechasHasta.txtCodSem.getText().length() == 0) ||
        (txtDesEquNot.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtCodEquNot.setEnabled(true);
        btnCtrlBuscarEquNot.setEnabled(true);
        //txtDesEquNot.setEnabled(true); /*E*/
        txtDesEquNot.setEditable(false);

        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCodEquNot.setEnabled(false);
        btnCtrlBuscarEquNot.setEnabled(false);
        //txtDesEquNot.setEnabled(false); /*E*/
        txtDesEquNot.setEditable(false);

        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  void btnCtrlbuscarEquNot_actionPerformed(ActionEvent evt) {

    DataEqNot data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaEqNot lista = new CListaEqNot(this,
                                          res.getString("msg9.Text"),
                                          stubCliente,
                                          strSERVLETEquipo,
                                          servletOBTENER_X_CODIGO,
                                          servletOBTENER_X_DESCRIPCION,
                                          servletSELECCION_X_CODIGO,
                                          servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataEqNot) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodEquNot.removeTextListener(txtTextAdapter);
      txtCodEquNot.setText(data.getEquipo());
      txtDesEquNot.setText(data.getDesEquipo());
      txtCodEquNot.addTextListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    modoOperacion = modoESPERA;
    Inicializar();

    txtCodEquNot.setText("");
    txtDesEquNot.setText("");

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    paramC2 = new DataInfNotEq();

    if (isDataValid()) {

      if (!txtDesEquNot.getText().equals("")) {
        paramC2.sDsEquipo = txtDesEquNot.getText();
        paramC2.sCdEquipo = txtCodEquNot.getText();
      }

      paramC2.sAnoDesde = fechasDesde.txtAno.getText();
      paramC2.sAnoHasta = fechasHasta.txtAno.getText();
      if (fechasDesde.txtCodSem.getText().length() == 1) {
        paramC2.sSemDesde = "0" + fechasDesde.txtCodSem.getText();
      }
      else {
        paramC2.sSemDesde = fechasDesde.txtCodSem.getText();
      }
      if (fechasHasta.txtCodSem.getText().length() == 1) {
        paramC2.sSemHasta = "0" + fechasHasta.txtCodSem.getText();
      }
      else {
        paramC2.sSemHasta = fechasHasta.txtCodSem.getText();

        // habilita el panel
      }
      modoOperacion = modoESPERA;
      Inicializar();
      informe.setEnabled(true);
      informe.datosPar = paramC2;

      if (informe.GenerarInforme()) {
        informe.show();

      }
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg10.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  // cambio en una caja de texto
  void textValueChanged(TextEvent e) {
  }

  void txtCodEquNot_keyPressed(KeyEvent e) {
    txtDesEquNot.setText("");
  }

  void txtCodEquNot_focusLost(FocusEvent e) {
    // datos de envio
    DataNivel1 nivel1;
    DataZBS2 nivel2;
    DataZBS zbs;
    DataMun mun;
    DataEqNot eqnot;
    DataCN cnnot;
    DataCat2 prov;
    DataCat enf;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    param = new CLista();
    param.setIdioma(app.getIdioma());
    param.addElement(new DataEqNot(txtCodEquNot.getText()));
    strServlet = strSERVLETEquipo;
    modoServlet = servletOBTENER_X_CODIGO;

    // busca el item
    if (txtCodEquNot.getText().length() > 0) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {
          // realiza el cast y rellena los datos
          eqnot = (DataEqNot) param.firstElement();
          txtCodEquNot.setText(eqnot.getEquipo());
          txtDesEquNot.setText(eqnot.getDesEquipo());
        }
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg11.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }
} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  Pan_NotEq adaptee = null;
  ActionEvent e = null;

  public actionListener(Pan_NotEq adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarEquNot")) {
      adaptee.btnCtrlbuscarEquNot_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}

// cambio en una caja de codigos
class textAdapter
    implements java.awt.event.TextListener {
  Pan_NotEq adaptee;

  textAdapter(Pan_NotEq adaptee) {
    this.adaptee = adaptee;
  }

  public void textValueChanged(TextEvent e) {
    adaptee.textValueChanged(e);
  }
}

////////////////////// Clases para listas

// lista de valores
class CListaEqNot
    extends CListaValores {
  protected Pan_NotEq panel;

  public CListaEqNot(Pan_NotEq pnl,
                     String title,
                     StubSrvBD stub,
                     String servlet,
                     int obtener_x_codigo,
                     int obtener_x_descricpcion,
                     int seleccion_x_codigo,
                     int seleccion_x_descripcion) {
    super(pnl.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = pnl;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataEqNot(s, "", "", "", "", "", "", "", "", "",
                         "", 0, this.app.getLogin(), "", false);

  }

  public String getCodigo(Object o) {
    return ( ( (DataEqNot) o).getEquipo());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEqNot) o).getDesEquipo());
  }
}

// lista de valores
class CListaCN
    extends CListaValores {

  public CListaCN(CApp a,
                  String title,
                  StubSrvBD stub,
                  String servlet,
                  int obtener_x_codigo,
                  int obtener_x_descricpcion,
                  int seleccion_x_codigo,
                  int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataCN(s, "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                      "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataCN) o).getCodCentro());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCN) o).getCentroDesc());
  }
}

class Pan_NotEq_txtCodEquNot_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_NotEq adaptee;

  Pan_NotEq_txtCodEquNot_keyAdapter(Pan_NotEq adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCodEquNot_keyPressed(e);
  }
}

class Pan_NotEq_txtCodEquNot_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_NotEq adaptee;
  FocusEvent e;

  Pan_NotEq_txtCodEquNot_focusAdapter(Pan_NotEq adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.txtCodEquNot_focusLost(e);
  }
}
