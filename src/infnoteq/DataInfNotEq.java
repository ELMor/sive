
package infnoteq;

import java.io.Serializable;

public class DataInfNotEq
    implements Serializable {

  public String sAnoDesde = "";
  public String sSemDesde = "";
  public String sAnoHasta = "";
  public String sSemHasta = "";
  public String sCdEquipo = "";
  public String sDsEquipo = "";
  public boolean bInformeCompleto = false;

  public DataInfNotEq(String anoDesde, String semDesde, String anoHasta,
                      String semHasta, String cdEquipo, String dsEquipo) {

    sAnoDesde = anoDesde;
    sSemDesde = semDesde;
    sAnoHasta = anoHasta;
    sSemHasta = semHasta;
    sCdEquipo = cdEquipo;
    sDsEquipo = dsEquipo;
  }

  public DataInfNotEq() {
    sAnoDesde = "";
    sSemDesde = "";
    sAnoHasta = "";
    sSemHasta = "";
    sCdEquipo = "";
    sDsEquipo = "";
    bInformeCompleto = false;
  }

}
