
package infnoteq;

import java.io.Serializable;

public class Param_C10
    implements Serializable {
  public String DS_SEM = "";
  public String FC_RECEP = "";
  public String FC_NOTIF = "";
  public String NM_NUM = "";
  public String NM_INDIV = "";
  public String IT_RES = "";
  public String NM_NTEO = "";
  public String NM_NREAL = "";
  public int iPagina = 0;
  public boolean bInformeCompleto = false;

  public Param_C10() {
    DS_SEM = "";
    FC_RECEP = "";
    NM_NUM = "";
    NM_INDIV = "";
    IT_RES = "";
    NM_NTEO = "";
    NM_NREAL = "";
    iPagina = 0;

  }

  public Param_C10(String semana, String fecha, String enfnum, String numindiv,
                   String itRes, String nTeo, String nReal, String fnot) {
    DS_SEM = semana;
    FC_RECEP = fecha;
    NM_NUM = enfnum;
    NM_INDIV = numindiv;
    IT_RES = itRes;
    NM_NTEO = nTeo;
    NM_NREAL = nReal;
    FC_NOTIF = fnot;
    iPagina = 0;
  }

}
