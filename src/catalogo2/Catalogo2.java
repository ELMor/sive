package catalogo2;

import java.util.ResourceBundle;

import capp.CApp;

public class Catalogo2
    extends CApp {

  // catalogos
  public static final int catPROVINCIA = 10;
  ResourceBundle res;
  public static final int catNIVEL2 = 20;
  public static final int catTBROTE = 30;

  // cat�logos
  public int iCatPral;
  public int iCatAux;

  // modos de operaci�n del servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // constantes del panel
  final String strSERVLET = "servlet/SrvCat2";

  // parametros
  int iMaxCod;
  int iMaxDes;
  String sLabel; //Para coulumna del multicolumn
  String sEtiqueta; //Para t�tulo del di�logo

  public void init() {
    super.init();

    // cat�logos
    iCatPral = Integer.parseInt(getParameter("catpral", null));
    iCatAux = Integer.parseInt(getParameter("cataux", null));

    res = ResourceBundle.getBundle("catalogo2.Res" + this.getIdioma());
    switch (iCatPral) {
      case Catalogo2.catPROVINCIA:
        sEtiqueta = res.getString("msg1.Text");
        sLabel = res.getString("msg2.Text");
        break;
      case Catalogo2.catNIVEL2:
        sEtiqueta = res.getString("msg3.Text") + " " + this.getNivel2();
        sLabel = this.getNivel2();
        break;
      case Catalogo2.catTBROTE:
        sEtiqueta = "Mantenimiento de tipos de brote";
        sLabel = "Tipo de brote";
        break;
    }
    setTitulo(sEtiqueta);
    VerPanel("", new CListaMantCat2(this));
  }

  public void start() {
  }
}
