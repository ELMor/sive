package catalogo2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvCat2
    extends DBServlet {

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int j = 1;

    // objetos de datos
    CLista data = null;
    DataCat2 cat2 = null;
    String sCod;
    String sDes;
    String sDesL;
    String sCodAux;
    String sDesAux;
    String sDesLAux;

    // querys
    // nuevo item
    // ARG: upper (8/5/02)
    final String[] asALTA = {
        "insert into SIVE_PROVINCIA (CD_PROV, CD_CA, DS_PROV, DSL_PROV) values (?, ?, ?, ?)",
        "insert into SIVE_NIVEL2_S (CD_NIVEL_2, CD_NIVEL_1, DS_NIVEL_2, DSL_NIVEL_2) values (?, ?, ?, ?)",
        "insert into SIVE_TIPO_BROTE (CD_TBROTE, CD_GRUPO, DS_TBROTE, DSL_TBROTE) values (?, ?, ?, ?)"};

    // actualiza el item
    final String[] asACTUALIZA = {
        "update SIVE_PROVINCIA SET DS_PROV = ?, DSL_PROV = ? where CD_PROV = ? and CD_CA = ?",
        "update SIVE_NIVEL2_S SET DS_NIVEL_2 = ?, DSL_NIVEL_2 = ? where CD_NIVEL_2 = ? and CD_NIVEL_1 = ?",
        "update SIVE_TIPO_BROTE SET DS_TBROTE = ?, DSL_TBROTE = ? where CD_TBROTE = ? and CD_GRUPO = ?"};

    // borra el item
    final String[] asBAJA = {
        "delete from SIVE_PROVINCIA where CD_PROV = ? and CD_CA = ?",
        "delete from SIVE_NIVEL2_S where CD_NIVEL_2 = ? and CD_NIVEL_1 = ?",
        "delete from SIVE_TIPO_BROTE where CD_TBROTE = ? and CD_GRUPO = ?"};

    // obtiene lista de items
    final String[] asLISTADO = {
        "select a.CD_PROV, a.DS_PROV, a.DSL_PROV, a.CD_CA, b.DS_CA, b.DSL_CA from SIVE_PROVINCIA a, SIVE_COM_AUT b where a.CD_CA = b.CD_CA and a.CD_PROV like ? order by a.CD_PROV",
        "select a.CD_NIVEL_2, a.DS_NIVEL_2, a.DSL_NIVEL_2, a.CD_NIVEL_1, b.DS_NIVEL_1, b.DSL_NIVEL_1 from SIVE_NIVEL2_S a, SIVE_NIVEL1_S b where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 like ? order by b.CD_NIVEL_1,a.CD_NIVEL_2",
        "select a.CD_TBROTE, a.DS_TBROTE, a.DSL_TBROTE, a.CD_GRUPO, b.DS_GRUPO, b.DSL_GRUPO from SIVE_TIPO_BROTE a, SIVE_GRUPO_BROTE b where a.CD_GRUPO = b.CD_GRUPO and a.CD_TBROTE like ? order by a.CD_TBROTE"};

    final String[] asLISTADOdes = {
        "select a.CD_PROV, a.DS_PROV, a.DSL_PROV, a.CD_CA, b.DS_CA, b.DSL_CA from SIVE_PROVINCIA a, SIVE_COM_AUT b where a.CD_CA = b.CD_CA and upper(a.DS_PROV) like upper(?) order by a.DS_PROV",
        "select a.CD_NIVEL_2, a.DS_NIVEL_2, a.DSL_NIVEL_2, a.CD_NIVEL_1, b.DS_NIVEL_1, b.DSL_NIVEL_1 from SIVE_NIVEL2_S a, SIVE_NIVEL1_S b where a.CD_NIVEL_1 = b.CD_NIVEL_1 and upper(a.DS_NIVEL_2) like upper(?) order by b.DS_NIVEL_1,a.DS_NIVEL_2",
        "select a.CD_TBROTE, a.DS_TBROTE, a.DSL_TBROTE, a.CD_GRUPO, b.DS_GRUPO, b.DSL_GRUPO from SIVE_TIPO_BROTE a, SIVE_GRUPO_BROTE b where a.CD_GRUPO = b.CD_GRUPO and upper(a.DS_TBROTE) like (?) order by a.DS_TBROTE"};

    // obtiene lista de items desde uno determinado
    final String[] asLISTADO2 = {
        "select a.CD_PROV, a.DS_PROV, a.DSL_PROV, a.CD_CA, b.DS_CA, b.DSL_CA from SIVE_PROVINCIA a, SIVE_COM_AUT b where a.CD_CA = b.CD_CA and a.CD_PROV like ? and a.CD_PROV > ? order by a.CD_PROV",
        "select a.CD_NIVEL_2, a.DS_NIVEL_2, a.DSL_NIVEL_2, a.CD_NIVEL_1, b.DS_NIVEL_1, b.DSL_NIVEL_1 from SIVE_NIVEL2_S a, SIVE_NIVEL1_S b where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 like ? and b.CD_NIVEL_1 > ? order by b.CD_NIVEL_1,a.CD_NIVEL_2",
        "select a.CD_TBROTE, a.DS_TBROTE, a.DSL_TBROTE, a.CD_GRUPO, b.DS_GRUPO, b.DSL_GRUPO from SIVE_TIPO_BROTE a, SIVE_GRUPO_BROTE b where a.CD_GRUPO = b.CD_GRUPO and a.CD_TBROTE like ? and a.CD_TBROTE > ? order by a.CD_TBROTE"};
    /*final String [] asLISTADO2 = {"select a.CD_PROV, a.DS_PROV, a.DSL_PROV, a.CD_CA, b.DS_CA, b.DSL_CA from SIVE_PROVINCIA a, SIVE_COM_AUT b where a.CD_CA = b.CD_CA and a.CD_PROV like ? and a.CD_PROV > ? order by CD_PROV",
                                  "select a.CD_NIVEL_2, a.DS_NIVEL_2, a.DSL_NIVEL_2, a.CD_NIVEL_1, b.DS_NIVEL_1, b.DSL_NIVEL_1 from SIVE_NIVEL2_S a, SIVE_NIVEL1_S b where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 like ? and b.CD_NIVEL_1 > ? order by CD_NIVEL_2",
                                  "select a.CD_TBROTE, a.DS_TBROTE, a.DSL_TBROTE, a.CD_GRUPO, b.DS_GRUPO, b.DSL_GRUPO from SIVE_TIPO_BROTE a, SIVE_GRUPO_BROTE b where a.CD_GRUPO = b.CD_GRUPO and a.CD_TBROTE like ? and a.CD_TBROTE > ? order by CD_TBROTE"};*/

    final String[] asLISTADO2des = {
        "select a.CD_PROV, a.DS_PROV, a.DSL_PROV, a.CD_CA, b.DS_CA, b.DSL_CA from SIVE_PROVINCIA a, SIVE_COM_AUT b where a.CD_CA = b.CD_CA and upper(a.DS_PROV) like upper(?) and upper(a.DS_PROV) > upper(?) order by a.DS_PROV",
        "select a.CD_NIVEL_2, a.DS_NIVEL_2, a.DSL_NIVEL_2, a.CD_NIVEL_1, b.DS_NIVEL_1, b.DSL_NIVEL_1 from SIVE_NIVEL2_S a, SIVE_NIVEL1_S b where a.CD_NIVEL_1 = b.CD_NIVEL_1 and upper(a.DS_NIVEL_2) like upper(?) and upper(b.DS_NIVEL_1) > upper(?) order by b.DS_NIVEL_1,a.DS_NIVEL_2",
        "select a.CD_TBROTE, a.DS_TBROTE, a.DSL_TBROTE, a.CD_GRUPO, b.DS_GRUPO, b.DSL_GRUPO from SIVE_TIPO_BROTE a, SIVE_GRUPO_BROTE b where a.CD_GRUPO = b.CD_GRUPO and upper(a.DS_TBROTE) like upper(?) and upper(a.DS_TBROTE) > upper(?) order by a.DS_TBROTE"};
    /*final String [] asLISTADO2des = {"select a.CD_PROV, a.DS_PROV, a.DSL_PROV, a.CD_CA, b.DS_CA, b.DSL_CA from SIVE_PROVINCIA a, SIVE_COM_AUT b where a.CD_CA = b.CD_CA and a.DS_PROV like ? and a.DS_PROV > ? order by DS_PROV",
                                  "select a.CD_NIVEL_2, a.DS_NIVEL_2, a.DSL_NIVEL_2, a.CD_NIVEL_1, b.DS_NIVEL_1, b.DSL_NIVEL_1 from SIVE_NIVEL2_S a, SIVE_NIVEL1_S b where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.DS_NIVEL_2 like ? and b.DS_NIVEL_1 > ? order by DS_NIVEL_2",
                                  "select a.CD_TBROTE, a.DS_TBROTE, a.DSL_TBROTE, a.CD_GRUPO, b.DS_GRUPO, b.DSL_GRUPO from SIVE_TIPO_BROTE a, SIVE_GRUPO_BROTE b where a.CD_GRUPO = b.CD_GRUPO and a.DS_TBROTE like ? and a.DS_TBROTE > ? order by DS_TBROTE"};*/

    // busca un item
    final String[] asBUSQUEDA = {
        "select a.CD_PROV, a.DS_PROV, a.DSL_PROV, a.CD_CA, b.DS_CA, b.DSL_CA from SIVE_PROVINCIA a, SIVE_COM_AUT b where a.CD_CA = b.CD_CA and a.CD_PROV = ?",
        "select a.CD_NIVEL_2, a.DS_NIVEL_2, a.DSL_NIVEL_2, a.CD_NIVEL_1, b.DS_NIVEL_1, b.DSL_NIVEL_1 from SIVE_NIVEL2_S a, SIVE_NIVEL1_S b where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = ?",
        "select a.CD_TBROTE, a.DS_TBROTE, a.DSL_TBROTE, a.CD_GRUPO, b.DS_GRUPO, b.DSL_GRUPO from SIVE_TIPO_BROTE a, SIVE_GRUPO_BROTE b where a.CD_GRUPO = b.CD_GRUPO and a.CD_TBROTE = ?"};

    // ARG: upper (8/5/02)
    final String[] asBUSQUEDAdes = {
        "select a.CD_PROV, a.DS_PROV, a.DSL_PROV, a.CD_CA, b.DS_CA, b.DSL_CA from SIVE_PROVINCIA a, SIVE_COM_AUT b where a.CD_CA = b.CD_CA and upper(a.DS_PROV) = upper(?)",
        "select a.CD_NIVEL_2, a.DS_NIVEL_2, a.DSL_NIVEL_2, a.CD_NIVEL_1, b.DS_NIVEL_1, b.DSL_NIVEL_1 from SIVE_NIVEL2_S a, SIVE_NIVEL1_S b where a.CD_NIVEL_1 = b.CD_NIVEL_1 and upper(a.DS_NIVEL_2) = upper(?)",
        "select a.CD_TBROTE, a.DS_TBROTE, a.DSL_TBROTE, a.CD_GRUPO, b.DS_GRUPO, b.DSL_GRUPO from SIVE_TIPO_BROTE a, SIVE_GRUPO_BROTE b where a.CD_GRUPO = b.CD_GRUPO and upper(a.DS_TBROTE) = upper(?)"};

    // nombres de los campos
    final String[] asCod = {
        "CD_PROV", "CD_NIVEL_2", "CD_TBROTE"}; //PDP 24/02/2000
    final String[] asDes = {
        "DS_PROV", "DS_NIVEL_2", "DS_TBROTE"};
    final String[] asDesL = {
        "DSL_PROV", "DSL_NIVEL_2", "DSL_TBROTE"};
    final String[] asCodAux = {
        "CD_CA", "CD_NIVEL_1", "CD_GRUPO"};
    final String[] asDesAux = {
        "DS_CA", "DS_NIVEL_1", "DS_GRUPO"};
    final String[] asDesLAux = {
        "DSL_CA", "DSL_NIVEL_1", "DSL_GRUPO"};

    // configuraci�n del servlet
    int iModo = opmode % 10;
    int iCatalogo = opmode - iModo;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    cat2 = (DataCat2) param.firstElement();
    try {
      // modos de operaci�n
      switch (iModo) {

        // alta
        case servletALTA:

          // prepara la query
          st = con.prepareStatement(asALTA[iCatalogo / 10 - 1]);
          // codigo
          st.setString(1, cat2.getCod().trim().toUpperCase());
          // codigo auxiliar
          st.setString(2, cat2.getCodAux().trim());
          // descripci�n
          st.setString(3, cat2.getDes().trim());
          // descripci�n local
          if (cat2.getDesL().trim().length() > 0) {
            st.setString(4, cat2.getDesL().trim());
          }
          else {
            st.setNull(4, java.sql.Types.VARCHAR);

            // lanza la query
          }
          st.executeUpdate();
          st.close();

          break;

          // baja
        case servletBAJA:

          // lanza la query
          st = con.prepareStatement(asBAJA[iCatalogo / 10 - 1]);
          st.setString(1, cat2.getCod().trim());
          st.setString(2, cat2.getCodAux().trim());
          st.executeUpdate();
          st.close();

          break;

          // listado
        case servletSELECCION_X_CODIGO:
        case servletSELECCION_X_DESCRIPCION:

          // prepara la query
          if (param.getFilter().length() > 0) {
            if (iModo == servletSELECCION_X_CODIGO) {
              st = con.prepareStatement(asLISTADO2[iCatalogo / 10 - 1]);
            }
            else {
              st = con.prepareStatement(asLISTADO2des[iCatalogo / 10 - 1]);
            }
          }
          else {
            if (iModo == servletSELECCION_X_CODIGO) { //{
              st = con.prepareStatement(asLISTADO[iCatalogo / 10 - 1]);
            }
            else { //}{
              st = con.prepareStatement(asLISTADOdes[iCatalogo / 10 - 1]);
            }
          }

          // prepara la lista de resultados
          data = new CLista();

          // filtro
          if (iModo == servletSELECCION_X_CODIGO) {
            st.setString(j, cat2.getCod().trim() + "%");
          }
          else {
            st.setString(j, "%" + cat2.getCod().trim() + "%");

            // paginaci�n
          }
          if (param.getFilter().length() > 0) {
            j++;
            st.setString(j, param.getFilter().trim());
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {

            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (iModo == servletSELECCION_X_CODIGO) {
                data.setFilter( ( (DataCat2) data.lastElement()).getCodAux());

              }
              else {
                data.setFilter( ( (DataCat2) data.lastElement()).getDesAux());

              }
              break;
            }

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);

            }

            // obtiene los campos
            sCod = rs.getString(asCod[iCatalogo / 10 - 1]);
            sDes = rs.getString(asDes[iCatalogo / 10 - 1]);
            sDesL = rs.getString(asDesL[iCatalogo / 10 - 1]);
            sCodAux = rs.getString(asCodAux[iCatalogo / 10 - 1]);
            sDesAux = rs.getString(asDesAux[iCatalogo / 10 - 1]);
            sDesLAux = rs.getString(asDesLAux[iCatalogo / 10 - 1]);

            //_________________________

            // obtiene la descripcion principal en funci�n del idioma solo si no estamos en un mantenimiento
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesL != null) &&
                (param.getMantenimiento() == false)) {
              sDes = sDesL;

              // obtiene la descripcion auxiliar en funci�n del idioma
            }
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesLAux != null)) {
              sDesAux = sDesLAux;

              // a�ade un nodo
            }
            data.addElement(new DataCat2(sCod, sDes, sDesL, sCodAux, sDesAux));

            //________________________

            i++;
          }

          rs.close();
          st.close();

          break;

          // modificaci�n
        case servletMODIFICAR:

          // actualiza
          st = con.prepareStatement(asACTUALIZA[iCatalogo / 10 - 1]);

          // descripci�n
          st.setString(1, cat2.getDes().trim());
          // descripci�n local
          if (cat2.getDesL().trim().length() > 0) {
            st.setString(2, cat2.getDesL().trim());
          }
          else {
            st.setNull(2, java.sql.Types.VARCHAR);
            // pral
          }
          st.setString(3, cat2.getCod().trim());
          // aux
          st.setString(4, cat2.getDesAux().trim());

          st.executeUpdate();
          st.close();
          st = null;

          break;

        case servletOBTENER_X_CODIGO:
        case servletOBTENER_X_DESCRIPCION:

          // lanza la query
          if (iModo == servletOBTENER_X_CODIGO) {
            st = con.prepareStatement(asBUSQUEDA[iCatalogo / 10 - 1]);
          }
          else {
            st = con.prepareStatement(asBUSQUEDAdes[iCatalogo / 10 - 1]);

          }
          st.setString(1, cat2.getCod().trim());

          rs = st.executeQuery();

          data = new CLista();

          // extrae el registro encontrado
          while (rs.next()) {
            // obtiene los campos
            sCod = rs.getString(asCod[iCatalogo / 10 - 1]);
            sDes = rs.getString(asDes[iCatalogo / 10 - 1]);
            sDesL = rs.getString(asDesL[iCatalogo / 10 - 1]);
            sCodAux = rs.getString(asCodAux[iCatalogo / 10 - 1]);
            sDesAux = rs.getString(asDesAux[iCatalogo / 10 - 1]);
            sDesLAux = rs.getString(asDesLAux[iCatalogo / 10 - 1]);

            //____________________

            // obtiene la descripcion principal en funci�n del idioma solo si no estamos en un mantenimiento
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesL != null) &&
                (param.getMantenimiento() == false)) {
              sDes = sDesL;

              // obtiene la descripcion auxiliar en funci�n del idioma
            }
            if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesLAux != null)) {
              sDesAux = sDesLAux;

              // a�ade un nodo
            }
            data.addElement(new DataCat2(sCod, sDes, sDesL, sCodAux, sDesAux));
            //____________________

          }
          rs.close();
          st.close();

          break;
      }
      con.commit();
    }
    catch (Exception ef) {
      con.rollback();
      throw ef;
    }
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }

  /*
     //////Modo Debug  pdp 24/02/2000
     public static void main(String args[]) {
    SrvCat2 srv = new SrvCat2();
    //Lista vParam = new Lista();
    //Data filtro = new Data();
    //Data peticion = new Data();
    // par�metros jdbc
    srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                           "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                           "sive_desa",
                           "sive_desa");
    srv.doDebug(4, null);
    srv = null;
     }
   */
}
