
package catalogo2;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import catalogo.CListaCat;
import catalogo.Catalogo;
import catalogo.DataCat;
import sapp.StubSrvBD;

public class DialCat2
    extends CDialog {

  boolean valido = false;
  ResourceBundle res;
  // modos de operaci�n de la ventana
  final static int modoALTA = 0;
  final static int modoMODIFICAR = 1;
  final static int modoBAJA = 2;
  final static int modoESPERA = 5;

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // constantes del panel
  final String strSERVLET = "servlet/SrvCat2";
  final String strSERVLET_AUX = "servlet/SrvCat";

  // par�metros
  protected String sTituloLista = "";
  protected int modoOperacion = modoALTA;
  protected int modoOperacionBk = modoALTA;
  protected CLista listaCat2 = null;
  protected CLista listaAux = null;
  protected StubSrvBD stubCliente = null;
  protected String sCodBk = "";
  public String sCodAuxBk = "";

  public boolean bAceptar = false;

  // cat�logos
  protected int iCatPral;
  protected int iCatAux;

  //Longitud m�xima descripciones
  int maxLongDes;
  int maxLongDesL;

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  // im�genes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/aceptar.gif",
      "images/cancelar.gif"};

  CApp app = null;

  // controles
  Label lbl1 = new Label();
  CCampoCodigo txtCod = new CCampoCodigo();
  Label lbl2 = new Label();
  CCampoCodigo txtCodAux = new CCampoCodigo();
  Label lbl3 = new Label();
  TextField txtDes = new TextField();
  Label lbl4 = new Label();
  TextField txtDesL = new TextField();
  TextField txtDesAux = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnSearchAux = new ButtonControl();

  cat2ActionListener btnActionListener = new cat2ActionListener(this);
  XYLayout xYLayout1 = new XYLayout();
  FlowLayout flowLayout1 = new FlowLayout();

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {

      case modoALTA:

        // modo alta
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnSearchAux.setEnabled(true);
        txtCod.setEnabled(true);
        txtCodAux.setEnabled(true);
        txtDes.setEnabled(true);
        txtDesL.setEnabled(true);
        txtDesAux.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:

        // modo modificaci�n y baja
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnSearchAux.setEnabled(false);
        txtCod.setEnabled(false);
        txtCodAux.setEnabled(false);
        txtDes.setEnabled(true);
        txtDesL.setEnabled(true);
        txtDesAux.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        btnSearchAux.setEnabled(false);
        txtCod.setEnabled(false);
        txtCodAux.setEnabled(false);
        txtDes.setEnabled(false);
        txtDesL.setEnabled(false);
        txtDesAux.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoBAJA:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnSearchAux.setEnabled(false);
        txtCod.setEnabled(false);
        txtCodAux.setEnabled(false);
        txtDes.setEnabled(false);
        txtDesL.setEnabled(false);
        txtDesAux.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

    }
  }

  // contructor
  public DialCat2(CApp a, int modo, int iPral, int iAux, String tit) {

    super(a);
    try {
      app = a;

      res = ResourceBundle.getBundle("catalogo2.Res" + this.app.getIdioma());

      this.setTitle(tit);

      iCatPral = iPral;
      iCatAux = iAux;
      switch (iAux) {
        case Catalogo.catCCAA:
          sTituloLista = res.getString("msg5.Text");
          lbl2.setText(res.getString("lbl2.Text"));
          break;
        case Catalogo.catNIVEL1:
          sTituloLista = res.getString("msg6.Text") + app.getNivel1();
          lbl2.setText(app.getNivel1());
          break;
      }

      listaCat2 = new CLista();
      listaCat2.setState(CLista.listaNOVALIDA);
      listaAux = new CLista();
      listaAux.setState(CLista.listaNOVALIDA);

      modoOperacion = modo;
      stubCliente = new StubSrvBD(new URL(a.getURL() + strSERVLET));
      jbInit();
      btnSearchAux.setFocusAware(false);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // aspecto de la ventana
  private void jbInit() throws Exception {
    setSize(498, 290);
    lbl1.setText(res.getString("lbl1.Text"));
    switch (iCatPral) {
      case Catalogo2.catPROVINCIA:
        lbl1.setText(res.getString("lbl1.Text"));
        maxLongDes = 20;
        maxLongDesL = 20;
        break;
      case Catalogo2.catNIVEL2:
        lbl1.setText(this.app.getNivel2() + ":");
        maxLongDes = 30;
        maxLongDesL = 30;
        break;
        //DSR:
      case Catalogo2.catTBROTE:
        lbl1.setText(res.getString("lbl1.Text"));
        lbl2.setText(res.getString("lbl5.Text"));
        maxLongDes = 40;
        maxLongDesL = 40;
        break;
    }
    lbl3.setText(res.getString("lbl3.Text"));
    txtDes.setBackground(new Color(255, 255, 150));
    lbl4.setText(res.getString("lbl4.Text"));

    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnSearchAux.setImage(imgs.getImage(0));
    btnAceptar.setImage(imgs.getImage(1));
    btnCancelar.setImage(imgs.getImage(2));

    btnSearchAux.setActionCommand("buscarAux");
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");
    txtCod.setName("cod");
    txtCod.setBackground(new Color(255, 255, 150));
    txtCodAux.setName("codAux");
    txtCodAux.setBackground(new Color(255, 255, 150));
    txtCodAux.addFocusListener(new DialCat2_txtCodAux_focusAdapter(this));
    txtCodAux.addKeyListener(new DialCat2_txtCodAux_keyAdapter(this));
    this.setLayout(xYLayout1);
    this.add(lbl1, new XYConstraints(23, 25, -1, -1));
    this.add(txtCod, new XYConstraints(117, 27, 77, -1));
    this.add(lbl2, new XYConstraints(23, 65, -1, -1));
    this.add(txtCodAux, new XYConstraints(117, 67, 77, -1));
    this.add(btnSearchAux, new XYConstraints(200, 66, -1, -1));
    this.add(txtDesAux, new XYConstraints(234, 67, 236, -1));
    this.add(lbl3, new XYConstraints(23, 105, -1, -1));
    this.add(txtDes, new XYConstraints(193, 107, 280, -1));
    this.add(lbl4, new XYConstraints(23, 145, -1, -1));
    this.add(txtDesL, new XYConstraints(193, 147, 280, -1));
    this.add(btnAceptar, new XYConstraints(304, 185, 80, 26));
    this.add(btnCancelar, new XYConstraints(389, 185, 80, 26));

    // establece los escuchadores
    btnSearchAux.addActionListener(btnActionListener);
    btnAceptar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);

    // idioma local no es visible si no tengo
    if (app.getIdiomaLocal().length() == 0) {
      txtDesL.setVisible(false);
      lbl4.setVisible(false);
    }
    else {
      lbl4.setText(res.getString("lbl4.Text") + app.getIdiomaLocal() + "):");
    }

    txtDesAux.setEditable(false);

    // establece el modo de operaci�n
    Inicializar();
  }

  // procesa opci�n a�adir un item
  public void A�adir() {
    CMessage msgBox = null;
    CLista data = null;

    // determina si los datos est�n completos
    if (this.isDataValid()) {

      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        // prepara los par�metros
        data = new CLista();
        data.addElement(new DataCat2(txtCod.getText().toUpperCase(),
                                     txtDes.getText(), txtDesL.getText(),
                                     txtCodAux.getText().toUpperCase(), ""));

        // apunta al servlet principal
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET));
        this.stubCliente.doPost(servletALTA + iCatPral, data);

        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoALTA;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

    }
  }

  // procesa opci�n modificar
  public void Modificar() {
    CMessage msgBox = null;
    CLista data = null;

    if (this.isDataValid()) {

      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        // prepara los par�metros de env�o
        data = new CLista();
        data.addElement(new DataCat2(txtCod.getText(), txtDes.getText(),
                                     txtDesL.getText(), txtCodAux.getText(),
                                     txtCodAux.getText()));

        // apunta al servlet principal
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET));
        this.stubCliente.doPost(servletMODIFICAR + iCatPral, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoMODIFICAR;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
  }

  // procesa opci�n borrar
  public void Borrar() {
    CMessage msgBox = null;
    CLista data = null;

    msgBox = new CMessage(app, CMessage.msgPREGUNTA, res.getString("msg7.Text"));
    msgBox.show();

    // el usuario confirma la operaci�n
    if (msgBox.getResponse()) {

      msgBox = null;

      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        // par�metros de env�o
        data = new CLista();
        data.addElement(new DataCat2(txtCod.getText(), "", "",
                                     txtCodAux.getText(), ""));

        // apunta al servlet principal
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET));
        this.stubCliente.doPost(servletBAJA + iCatPral, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoBAJA;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

    }
    else {
      msgBox = null;
    }
  }

  // procesa opci�n obtener la lista auxiliar
  public void btnSearchAux_actionPerformed(ActionEvent evt) {

    DataCat data;
    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    Inicializar();

    CListaCat lista = new CListaCat(app,
                                    sTituloLista,
                                    stubCliente,
                                    strSERVLET_AUX,
                                    servletOBTENER_X_CODIGO + iCatAux,
                                    servletOBTENER_X_DESCRIPCION + iCatAux,
                                    servletSELECCION_X_CODIGO + iCatAux,
                                    servletSELECCION_X_DESCRIPCION + iCatAux);
    ( (CListaCat) lista).show();
    data = (DataCat) lista.getComponente();

    if (data != null) {
      txtCodAux.setText(data.getCod());
      txtDesAux.setText(data.getDes());
    }
    modoOperacion = modo;
    Inicializar();

  }

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;
    boolean b = false;

    /* System_out.println("txtCod-txtDes-txtDesAux-txtDesL: " +
      txtCod.getText() + "-" +
      txtDes.getText() + "-" +
      txtDesAux.getText() + "-" +
      txtDesL.getText()); */
    // comprueba que esten informados los campos obligatorios
    if ( (txtCod.getText().length() > 0) && (txtDes.getText().length() > 0)
        && (txtDesAux.getText().length() > 0)) {
      b = true;
      if (this.txtCod.getText().length() > 2) {
        b = false;
        msg = res.getString("msg8.Text");
        txtCod.selectAll();
      }
      if (this.txtDes.getText().length() > maxLongDes) {
        b = false;
        msg = res.getString("msg8.Text");
        txtDes.selectAll();
      }
      if (this.txtDesL.getText().length() > maxLongDesL) {
        b = false;
        msg = res.getString("msg8.Text");
        txtDesL.selectAll();
      }

      // advertencia de requerir datos
    }
    else {
      msg = res.getString("msg9.Text");
    }
    if (!b) {
      msgBox = new CMessage(app, CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }
    valido = b;
    return b;
  }

  void txtCodAux_keyPressed(KeyEvent e) {
    txtDesAux.setText("");
  }

  void txtCodAux_focusLost(FocusEvent e) {
    // datos de envio
    DataCat data;
    CLista param = null;
    CMessage msg;
    String sDes1;
    int modo = modoOperacion;

    if (txtCodAux.getText().length() > 0) {

      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();

      try {
        param = new CLista();
        param.setIdioma(app.getIdioma());
        param.addElement(new DataCat(txtCodAux.getText().toUpperCase()));

        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_AUX));
        param = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO + iCatAux,
                                            param);

        // rellena los datos
        if (param.size() > 0) {
          data = (DataCat) param.elementAt(0);

          txtCodAux.setText(data.getCod());
          sDes1 = data.getDes();

//REPASAR********************************************  Ver si interesa comparar DesL con "" o con null
//Caso de idioma No por defecto pero desL a NULL en b. datos , aqu� llegar�a "" y no NULL
//Pues el null se ha pasado a "" en el DataCat2
          /*
               if ((this.app.getIdioma() != CApp.idiomaPORDEFECTO) && (data.getDesL() !=null))
                      sDes1 = data.getDesL();
           */
          txtDesAux.setText(sDes1);

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg10.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
    // modo normal
    modoOperacion = modo;
    Inicializar();

  }
}

// action listener para los botones
class cat2ActionListener
    implements ActionListener, Runnable {
  DialCat2 adaptee = null;
  ActionEvent e = null;

  public cat2ActionListener(DialCat2 adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;

    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    if (e.getActionCommand() == "buscarAux") { // lista auxiliar
      adaptee.btnSearchAux_actionPerformed(e);
    }
    else if (e.getActionCommand() == "aceptar") { // aceptar
      adaptee.bAceptar = true;

      // realiza la operaci�n
      switch (adaptee.modoOperacion) {
        case DialCat2.modoALTA:
          adaptee.A�adir();
          break;
        case DialCat2.modoMODIFICAR:
          adaptee.Modificar();
          break;
        case DialCat2.modoBAJA:
          adaptee.Borrar();
          break;
      }
    }
    else if (e.getActionCommand() == "cancelar") { // cancelar
      adaptee.bAceptar = false;
      adaptee.dispose();
    }
  }
}

class DialCat2_txtCodAux_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialCat2 adaptee;

  DialCat2_txtCodAux_keyAdapter(DialCat2 adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCodAux_keyPressed(e);
  }
}

class DialCat2_txtCodAux_focusAdapter
    extends java.awt.event.FocusAdapter {
  DialCat2 adaptee;

  DialCat2_txtCodAux_focusAdapter(DialCat2 adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.txtCodAux_focusLost(e);
  }
}
