package enfermo;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Choice;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CTabla;
import catalogo.Catalogo;
import catalogo.DataCat;
import jclass.bwt.JCActionEvent;
import sapp.StubSrvBD;
import suca.datasuca;
import suca.panelsuca;
import suca.srvsuca;
//SvrZBS2, DataZBS2. para obtener el nivel2.
import zbs.DataZBS2;

public class DialBusEnfermo
    extends CDialog {

  //para saber que constructor se abre
  public int constructorDIRECTO = 0;
  ResourceBundle res = ResourceBundle.getBundle("enfermo.Res" + app.getIdioma());
  public int constructorINDIRECTO = 1;
  public int constructor = constructorDIRECTO;

//__________________________________________________ MODOS
  //modos de operaci�n de la ventana
  public final int modoINICIO = 0;
  public final int modoESPERA = 1;
  public final int modoNIVEL1 = 4;
  public final int modoNIVEL2 = 5;
  public final int modoPAIS = 6; // no pinchar en CA y PROV
  public final int modoNOPAIS = 7; // pinchar en CA
  public final int modoCA = 8;
  public final int modoNOCA = 9;
  public final int modoPROVINCIA = 10;
  public final int modoNOPROVINCIA = 11;
  public final int modoSELECION = 12; // metiendo datos para seleccionar los enfermos

  /** indica en que estado est� el frame */
  protected int modo = 0;
  protected int modoAnterior = 0;
  protected int miIndice = 0;

  /** esta es la lista que contiene los datos que se muestran en la tabla */
  CLista listaEnfermos = null;

  /** esta lista son los datos del enfermos seleccionado */
  CLista listaDatosEnfermo = null;

  /** esta lista son los datos de b�squeda */
  CLista listaDatosBusqueda = null;

  /** Lista que contiene todos los posibles identificaciones */
  protected CLista listaId = null;
  /** Lista que contiene todos los paises posibles */
  protected CLista listaPaises = null;
  /** Lista que contiene las regiones posibles */
  protected CLista listaCA = null;
  /** Lista que contiene las provincias */
  protected CLista listaProvincias = null;

  /** indica si tiene permiso de ver los nombres */
  protected boolean bPerNom = false;

  //Servlet
  protected final String strSERVLET_SUCA = "servlet/srvsuca";
  protected boolean bTramero = false;

//__________________________________________________ SINCRONIZACION
  /** esta variable se pone a true cuando no se inhiben los eventos
   *  y a false se rechazan todos los eventos
   */
  protected boolean sinBloquear = true;

  public StubSrvBD stubCliente = new StubSrvBD();

//__________________________________________________ COMP GRAFICOS
//  BevelPanel pnl = new BevelPanel();
  XYLayout xYLayout = new XYLayout();
  Choice chId = new Choice();
  CampoNIF txtIDL = new CampoNIF();
  CCampoCodigo txtEnfermedad = new CCampoCodigo();
  ButtonControl btnEnfermedad = new ButtonControl();
  TextField txtEnfermedadL = new TextField();
  Label lblApellido1 = new Label();
  TextField txtApellido1 = new TextField();
  Label lblApellido2 = new Label();
  TextField txtApellido2 = new TextField();
  Label lblNombre = new Label();
  TextField txtNombre = new TextField();
  CheckboxGroup chbgBusFonetica = new CheckboxGroup();
  Checkbox chbBusFonetica = new Checkbox();
  Checkbox chbBusalfabetica = new Checkbox();
  Label lblPais = new Label();
  Choice chPais = new Choice();
  Label lblCA = new Label();
  Choice chCA = new Choice();
  Label lblProvincia = new Label();
  Choice chProvincia = new Choice();
  Label lblMunicipio = new Label();
  TextField txtMunicipioL = new TextField();
  CCampoCodigo txtMunicipio = new CCampoCodigo();
  ButtonControl btnMunicipio = new ButtonControl();
  TextField txtNivelL1 = new TextField();
  ButtonControl btnNivel1 = new ButtonControl();
  Label lblNivel1 = new Label();
  CCampoCodigo txtNivel1 = new CCampoCodigo();
  Label lblNivel2 = new Label();
  CCampoCodigo txtNivel2 = new CCampoCodigo();
  TextField txtNivelL2 = new TextField();
  ButtonControl btnNivel2 = new ButtonControl();
  CTabla tablaEnfermos = new CTabla();
  ButtonControl btnBuscar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnVaciar = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();
  GroupBox pnlEnfermedad = new GroupBox();
  XYLayout xYLayout1 = new XYLayout();
  GroupBox pnlNombreA = new GroupBox();
  XYLayout xYLayout2 = new XYLayout();
  GroupBox pnlZona = new GroupBox();
  XYLayout xYLayout3 = new XYLayout();

  //______________________________________________ GESTOR de EVENTOS
  BusEnfermoBtnActionListener btnActionListener = new
      BusEnfermoBtnActionListener(this);
  BusEnfermoTextAdapter textAdapter = new BusEnfermoTextAdapter(this);
  BusEnfermoTableAdapter tableAdapter = new BusEnfermoTableAdapter(this);
  BusEnfermoChoiceItemListener chItemListener = new
      BusEnfermoChoiceItemListener(this);
  BusEnfermoTextFocusListener txtFocusListener = new
      BusEnfermoTextFocusListener(this);

  Checkbox chkbMostrarBaja = new Checkbox();

  //______________________________________________ CONSTRUCTOR
  // ARG: Primer constructor
  public DialBusEnfermo(CApp app, Pan_Enfermo pan_enfer) {
    super(app);
    setTitle(res.getString("msg4.Text"));
    constructor = constructorDIRECTO;
    try {
      listaEnfermos = new CLista();
      listaEnfermos.setState(CLista.listaNOVALIDA);

      listaCA = pan_enfer.getListaCA();
      while (listaCA == null) { // Paises == null) {
        listaCA = pan_enfer.getListaCA();
        Thread.sleep(50);
      }
      // cogemos del panel los paises
      listaPaises = pan_enfer.getListaPaises();
      bPerNom = pan_enfer.getPermiso();
      bTramero = pan_enfer.btramero;

      jbInit();

      // rellenamos los choices con valores fijos
      comun.writeChoice(app, chId, pan_enfer.listaId, true, "DS_TIPODOC");
      comun.writeChoice(app, chPais, pan_enfer.getListaPaises(), true,
                        "DS_PAIS");
      //comun.writeChoice(app, chPais, pan_enfer.getListaPaises(), true,"DSPAIS");
      comun.writeChoice(app, chCA, pan_enfer.getListaCA(), true, "DS_CA");
      //comun.writeChoice(app, chCA, pan_enfer.getListaCA(), true,"DSCOMU");
      chProvincia.add("  ");
      // seleccionamos a Espa�a como pa�s
      miIndice = comun.buscaDato(listaPaises, "CD_PAIS", "ESP");
      if (miIndice >= 0 && miIndice < listaPaises.size()) {
        chPais.select(miIndice + 1);
        modo = modoPAIS;
        Inicializar();
        // ahora seleccionamos la comunidad aut�noma
        miIndice = comun.buscaDato(listaCA, "CD_CA", this.app.getCA());
        if (miIndice >= 0 && miIndice < listaCA.size()) {
          chCA.select(miIndice + 1);
          //cargarProvincias(miIndice);
          hazCA();
          modo = modoPROVINCIA;
          Inicializar();
        }
      }

      pack();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void hazCA() {
    int indice = chCA.getSelectedIndex();
    indice--;

    //cargar provincias
    CLista appList = null;
    CLista data = new CLista();
    int modoLlamadaServlet;
    boolean bIdiomaPorDefecto = (this.app.getIdioma() ==
                                 getCApp().idiomaPORDEFECTO);
    if (indice >= 0) {
      appList = listaCA;

      String codCA = null;
      if (constructor == constructorDIRECTO) {
        codCA = (String) ( (datasuca) appList.elementAt(indice)).get("CD_CA");
      }
      else {
        Hashtable hashtmp = (Hashtable) appList.elementAt(indice);
        codCA = (String) hashtmp.get("CD_CA");
      }

      listaProvincias = null;

      datasuca dat = new datasuca();
      dat.put("CD_CA", codCA);
      data.addElement(dat);

      // Distintas posibilidades de llamada
      modoLlamadaServlet = (bTramero ? srvsuca.modoPROVINCIAS :
                            srvsuca.modoPROVINCIAS_SIN_TRAMERO);
      if (!bTramero) {
        data.addElement(bIdiomaPorDefecto ? "S" : "N");
      }
      //********************************************************************

       listaProvincias = comun.traerDatos(getCApp(), stubCliente,
                                          strSERVLET_SUCA, modoLlamadaServlet,
                                          data);

      if (chProvincia.getItemCount() > 0) {
        chProvincia.removeAll();

      }
      if (listaProvincias.size() > 0) {
        chProvincia.add(" ");
        comun.writeChoice(getCApp(), chProvincia, listaProvincias, false,
                          "DS_PROV");
        if (listaProvincias.size() == 1) {
          chProvincia.select(1);
          modo = modoPROVINCIA;
        }
        else {
          modo = modoNOPROVINCIA;
        }
      }
    }
    else {
      if (chProvincia.getItemCount() > 0) {
        chProvincia.removeAll();
      }
      chProvincia.add(" ");
      modo = modoNOCA;
    }

    Inicializar();
  }

  // ARG: Segundo constructor
  public DialBusEnfermo(CApp a) {
    super(a);
    setTitle(res.getString("msg4.Text"));
    constructor = constructorINDIRECTO;
    try {
      // cogemos las listas
      CLista parametros = null, result = null;

      /// comprobamos si tieen permiso el usuario
      parametros = new CLista();
      enfermo.DataEnfermo d = new enfermo.DataEnfermo("CD_USUARIO");
      d.setFiltro(app.getLogin());
      parametros.addElement(d);
      result = comun.traerDatos(app, stubCliente, comun.strSERVLET_ENFERMO,
                                SrvEnfermo.modoPERMISO, parametros);

      String si = this.getCApp().getIT_FG_ENFERMO();
      bPerNom = (si != null && si.equals("S"));

      parametros = new CLista();
      parametros.addElement(new enfermo.DataEnfermo("CD_TDOC"));
      listaId = comun.traerDatos(app, stubCliente, comun.strSERVLET_ENFERMO,
                                 SrvEnfermo.modoTDOC, parametros);
      //// System_out.println("listaID "+ listaId);
      parametros = new CLista();
      parametros.addElement(new enfermo.DataEnfermo("CD_PAIS"));
      listaPaises = comun.traerDatos(app, stubCliente, comun.strSERVLET_ENFERMO,
                                     SrvEnfermo.modoPAISES, parametros);
      //// System_out.println("listaPaises "+ listaPaises);
      parametros = new CLista();
      parametros.addElement(new enfermo.DataEnfermo("CD_CA"));
      listaCA = comun.traerDatos(app, stubCliente, comun.strSERVLET_ENFERMO,
                                 SrvEnfermo.modoCCAA, parametros);

      //// System_out.println("listaCA "+ listaCA);

      jbInit();

      // rellenamos los choices con valores fijos
      comun.writeChoice(app, chId, listaId, true, "DS_TIPODOC");
      comun.writeChoice(app, chPais, listaPaises, true, "DS_PAIS");
      comun.writeChoice(app, chCA, listaCA, true, "DS_CA");
      chProvincia.add("  ");

      // seleccionamos a Espa�a como pa�s
      miIndice = comun.buscaDato(listaPaises, "CD_PAIS", "ESP");
      if (miIndice >= 0 && miIndice < listaPaises.size()) {
        chPais.select(miIndice + 1);
        this.modo = modoPAIS;
        Inicializar();
        // ahora seleccionamos la comunidad aut�noma

        miIndice = comun.buscaDato(listaCA, "CD_CA", this.app.getCA());

        if (miIndice >= 0 && miIndice < listaCA.size()) {
          chCA.select(miIndice + 1);
          //cargarProvincias(miIndice);
          hazCA();
          modo = modoPROVINCIA;
          Inicializar();
        }
      }

      pack();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // ARG: Tercer constructor
  public DialBusEnfermo(CApp a, panelsuca panSuca) {
    super(a);
    setTitle(res.getString("msg4.Text"));
    constructor = constructorINDIRECTO;
    try {
      // cogemos las listas
      CLista parametros = null, result = null;

      bTramero = panSuca.getModoTramero();

      /// comprobamos si tieen permiso el usuario
      parametros = new CLista();
      enfermo.DataEnfermo d = new enfermo.DataEnfermo("CD_USUARIO");
      d.setFiltro(app.getLogin());
      parametros.addElement(d);
      result = comun.traerDatos(app, stubCliente, comun.strSERVLET_ENFERMO,
                                SrvEnfermo.modoPERMISO, parametros);

      String si = this.getCApp().getIT_FG_ENFERMO();
      bPerNom = (si != null && si.equals("S"));

      parametros = new CLista();
      parametros.addElement(new enfermo.DataEnfermo("CD_TDOC"));
      listaId = comun.traerDatos(app, stubCliente, comun.strSERVLET_ENFERMO,
                                 SrvEnfermo.modoTDOC, parametros);
      //// System_out.println("listaID "+ listaId);
      parametros = new CLista();
      parametros.addElement(new enfermo.DataEnfermo("CD_PAIS"));
      listaPaises = comun.traerDatos(app, stubCliente, comun.strSERVLET_ENFERMO,
                                     SrvEnfermo.modoPAISES, parametros);
      //// System_out.println("listaPaises "+ listaPaises);
      parametros = new CLista();
      parametros.addElement(new enfermo.DataEnfermo("CD_CA"));
      listaCA = panSuca.getListaCA();

      //// System_out.println("listaCA "+ listaCA);

      jbInit();

      // rellenamos los choices con valores fijos
      comun.writeChoice(app, chId, listaId, true, "DS_TIPODOC");
      comun.writeChoice(app, chPais, listaPaises, true, "DS_PAIS");
      comun.writeChoice(app, chCA, listaCA, true, "DS_CA");
      chProvincia.add("  ");

      // seleccionamos a Espa�a como pa�s
      miIndice = comun.buscaDato(listaPaises, "CD_PAIS", "ESP");
      if (miIndice >= 0 && miIndice < listaPaises.size()) {
        chPais.select(miIndice + 1);
        this.modo = modoPAIS;
        Inicializar();
        // ahora seleccionamos la comunidad aut�noma

        miIndice = comun.buscaDato(listaCA, "CD_CA", this.app.getCA());

        if (miIndice >= 0 && miIndice < listaCA.size()) {
          chCA.select(miIndice + 1);
          //cargarProvincias(miIndice);
          hazCA();
          modo = modoPROVINCIA;
          Inicializar();
        }
      }

      pack();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /** ete m�todo sirve para mantener una sincronizaci�n en los eventos */
  public synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnterior = modo;
      modo = modoESPERA;
      Inicializar();
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  /** este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    if (modo == modoESPERA) {
      modo = modoAnterior;
    }
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  void jbInit() throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        comun.imgLUPA,
        comun.imgBUSCAR,
        comun.imgLIMPIAR,
        comun.imgACEPTAR,
        comun.imgCANCELAR};
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    // fijamos las dimensiones del di�logo
    //setSize(600, 454);

    setSize(636, 525);
    xYLayout.setHeight(525);
    xYLayout.setWidth(636); //447);

    setLayout(xYLayout);

    // ponemos la imagen de la lupa
    btnEnfermedad.setImage(imgs.getImage(0));
    btnMunicipio.setImage(imgs.getImage(0));
    btnNivel1.setImage(imgs.getImage(0));
    btnNivel2.setImage(imgs.getImage(0));
    btnBuscar.setImage(imgs.getImage(1));
    btnVaciar.setImage(imgs.getImage(2));
    btnAceptar.setImage(imgs.getImage(3));
    btnCancelar.setImage(imgs.getImage(4));

    txtIDL.setText("");
    txtEnfermedad.setText("");
    lblApellido1.setText(res.getString("lblApellido1.Text"));
    lblApellido2.setText(res.getString("lblApellido2.Text"));
    lblNombre.setText(res.getString("lblNombre.Text"));
    chbBusFonetica.setLabel(res.getString("chbBusFonetica.Label"));
    chbBusFonetica.setCheckboxGroup(chbgBusFonetica);
    chbBusalfabetica.setLabel(res.getString("chbBusalfabetica.Label"));
    chbBusalfabetica.setCheckboxGroup(chbgBusFonetica);
    lblPais.setText(res.getString("lblPais.Text"));
    lblCA.setText(res.getString("lblCA.Text"));
    lblProvincia.setText(res.getString("lblProvincia.Text"));
    lblMunicipio.setText(res.getString("lblMunicipio.Text"));
    txtMunicipioL.setEditable(false);
    txtNivelL1.setEditable(false);
    lblNivel1.setText(app.getNivel1());
    lblNivel2.setText(app.getNivel2());
    tablaEnfermos.setColumnButtonsStrings(jclass.util.JCUtilConverter.
                                          toStringList(new String(res.getString(
        "tablaEnfermos.ColumnButtonsStrings")), '\n'));
    tablaEnfermos.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new
        String("45\n200\n75\n120\n110"), '\n'));
    tablaEnfermos.setNumColumns(5);

    btnBuscar.setLabel(res.getString("btnBuscar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnVaciar.setLabel(res.getString("btnVaciar.Label"));

    pnlEnfermedad.setLabel(res.getString("pnlEnfermedad.Label"));

    pnlNombreA.setLabel(res.getString("pnlNombreA.Label"));

    pnlZona.setLabel(res.getString("pnlZona.Label"));
    chkbMostrarBaja.setLabel(res.getString("chkbMostrarBaja.Label"));

    // a�adimos el nombrede los botones
    btnBuscar.setActionCommand("btnBuscar");
    btnCancelar.setActionCommand("btnCancelar");
    btnEnfermedad.setActionCommand("btnEnfermedad");
    btnMunicipio.setActionCommand("btnMunicipio");
    btnNivel1.setActionCommand("btnNivel1");
    btnNivel2.setActionCommand("btnNivel2");
    btnVaciar.setActionCommand("btnVaciar");
    btnAceptar.setActionCommand("btnAceptar");

    // a�adimos el nombre de la cjaa de texto
    txtEnfermedad.setName("enfermedad");
    txtMunicipio.setName("municipio");
    txtNivel1.setName("nivel1");
    txtNivel1.addKeyListener(new DialBusEnfermo_txtNivel1_keyAdapter(this));
    txtNivel2.setName("nivel2");
    txtNivelL2.setEditable(false);

    // a�adimos el nombre de los choices
    chPais.setName("pais");
    chCA.setName("ccaa");
    chProvincia.setName("provincia");

    chbBusFonetica.setState(true);

    // a�adimos al panel todos los componentes
    // primero a�adimos las listas y despu�s el resto de componentes
    // panel enfermedad
    this.add(txtEnfermedad, new XYConstraints(10, 18, 53, 20));
    this.add(btnEnfermedad, new XYConstraints(68, 17, -1, -1));
    this.add(txtEnfermedadL, new XYConstraints(101, 17, 153, 20));
    this.add(chId, new XYConstraints(16, 75, 115, 20));
    this.add(txtIDL, new XYConstraints(143, 77, 98, 20));
    this.add(chbBusFonetica, new XYConstraints(30, 109, 84, 24));
    this.add(chbBusalfabetica, new XYConstraints(151, 109, 80, 24));
    this.add(txtApellido1, new XYConstraints(99, 139, 158, 20));
    this.add(txtApellido2, new XYConstraints(99, 168, 158, 20));
    this.add(lblNombre, new XYConstraints(13, 196, 51, 20));
    this.add(txtNombre, new XYConstraints(99, 196, 158, 20));
    this.add(lblApellido2, new XYConstraints(13, 168, 67, 22));
    this.add(lblApellido1, new XYConstraints(14, 139, 63, -1));
    this.add(lblPais, new XYConstraints(286, 21, 31, 20));
    this.add(chPais, new XYConstraints(361, 21, 128, 20));
    this.add(lblCA, new XYConstraints(286, 55, 31, 22));
    this.add(chCA, new XYConstraints(361, 55, 128, 20));
    this.add(lblProvincia, new XYConstraints(286, 91, 55, 20));
    this.add(chProvincia, new XYConstraints(361, 91, 128, 20));
    this.add(lblMunicipio, new XYConstraints(286, 126, 61, 21));
    this.add(txtMunicipio, new XYConstraints(361, 126, 50, 20));
    this.add(txtMunicipioL, new XYConstraints(446, 126, 177, 20));
    this.add(btnMunicipio, new XYConstraints(417, 126, -1, -1));
    this.add(lblNivel1, new XYConstraints(286, 161, 71, 20));
    // panel nombre
    //panel localizacion
    this.add(txtNivel1, new XYConstraints(361, 161, 50, 20));
    this.add(btnNivel1, new XYConstraints(417, 161, -1, -1));
    this.add(txtNivelL1, new XYConstraints(446, 161, 177, 20));
    this.add(lblNivel2, new XYConstraints(286, 195, 74, 20));
    this.add(txtNivel2, new XYConstraints(361, 195, 50, 20));
    this.add(btnNivel2, new XYConstraints(417, 195, -1, -1));
    this.add(txtNivelL2, new XYConstraints(446, 195, 177, 20));
    this.add(tablaEnfermos, new XYConstraints(20, 265, 570, 178));
    this.add(btnBuscar, new XYConstraints(20, 235, 90, 28));
    this.add(btnVaciar, new XYConstraints(115, 235, 90, 28));

    this.add(btnCancelar, new XYConstraints(526, 461, -1, -1));
    this.add(btnAceptar, new XYConstraints(435, 461, -1, -1));
    this.add(chkbMostrarBaja, new XYConstraints(286, 235, 211, 22));
    this.add(pnlEnfermedad, new XYConstraints( -5, 0, 276, 52));
    this.add(pnlNombreA, new XYConstraints( -5, 56, 277, 171));
    this.add(pnlZona, new XYConstraints(270, 0, 364, 228));

    // gesti�n de eventos de botones
    btnEnfermedad.addActionListener(btnActionListener);
    btnMunicipio.addActionListener(btnActionListener);
    btnNivel1.addActionListener(btnActionListener);
    btnNivel2.addActionListener(btnActionListener);
    btnBuscar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);
    btnAceptar.addActionListener(btnActionListener);
    btnVaciar.addActionListener(btnActionListener);

    // gestion de evenetos de los Choices
    chPais.addItemListener(chItemListener);
    chCA.addItemListener(chItemListener);
    chProvincia.addItemListener(chItemListener);

    // modificaci�n de los datos de las cajas
    txtEnfermedad.addActionListener(txtFocusListener);
    txtMunicipio.addActionListener(txtFocusListener);
    txtNivel1.addActionListener(txtFocusListener);
    txtNivel2.addActionListener(txtFocusListener);

    txtEnfermedad.addKeyListener(textAdapter);
    txtMunicipio.addKeyListener(textAdapter);
    txtNivel1.addKeyListener(textAdapter);
    txtNivel2.addKeyListener(textAdapter);

    txtEnfermedad.addFocusListener(txtFocusListener);
    txtMunicipio.addFocusListener(txtFocusListener);
    txtNivel1.addFocusListener(txtFocusListener);
    txtNivel2.addFocusListener(txtFocusListener);

    //gesti�n de eventos de la tabla
    tablaEnfermos.addActionListener(tableAdapter);

    // se inicializan los botones a false o true
    this.modo = modoINICIO;
    Inicializar();

//    pnl.doLayout();
  }

  public void Inicializar() {

    switch (modo) {
      case modoINICIO:
        txtEnfermedad.setEnabled(true);
        txtApellido1.setEnabled(true);
        txtApellido2.setEnabled(true);
        txtNombre.setEnabled(true);
        btnEnfermedad.setEnabled(true);
        txtMunicipio.setEnabled(false);
        txtMunicipioL.setEnabled(false);
        btnMunicipio.setEnabled(false);
        chProvincia.setEnabled(false);
        chCA.setEnabled(false);
        btnNivel1.setEnabled(false);
        btnNivel2.setEnabled(false);
        txtEnfermedadL.setEnabled(false);
        txtNivelL1.setEnabled(false);
        txtNivelL2.setEnabled(false);
        btnAceptar.setEnabled(true);
        btnBuscar.setEnabled(true);
        btnEnfermedad.setEnabled(true);
        btnVaciar.setEnabled(true);
        btnCancelar.setEnabled(true);
        //// System_out.println("modoinicio");
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
      case modoSELECION:

        // hacemos invisibleslas listas
        //Todos habilitados menos los de modificar y borrar y lista valores
        txtEnfermedad.setEnabled(true);
        txtApellido1.setEnabled(true);
        txtApellido2.setEnabled(true);
        txtNombre.setEnabled(true);
        btnEnfermedad.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnBuscar.setEnabled(true);
        btnEnfermedad.setEnabled(true);
        btnVaciar.setEnabled(true);
        btnCancelar.setEnabled(true);
        //// System_out.println("modoseleccion");
        InicializarLocalizacion();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
      case modoESPERA:
        InicializarLocalizacion();
        txtEnfermedad.setEnabled(false);
        txtMunicipioL.setEnabled(false);
        txtNivel1.setEnabled(false);
        txtNivel2.setEnabled(false);
        txtApellido1.setEnabled(false);
        txtApellido2.setEnabled(false);
        txtNombre.setEnabled(false);
        //// System_out.println("modoespera");
        btnMunicipio.setEnabled(false);
        btnNivel1.setEnabled(false);
        btnNivel2.setEnabled(false);

        btnAceptar.setEnabled(false);
        btnBuscar.setEnabled(false);
        btnEnfermedad.setEnabled(false);
        btnVaciar.setEnabled(false);
        btnCancelar.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
      case modoNOPAIS:
        chCA.select(0);
      case modoNOCA:
        chProvincia.select(0);
      case modoNOPROVINCIA:
        txtMunicipio.setText("");
        txtMunicipioL.setText("");
      case modoPROVINCIA:
      case modoCA:
      case modoPAIS:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnBuscar.setEnabled(true);
        btnEnfermedad.setEnabled(true);
        btnVaciar.setEnabled(true);
        InicializarLocalizacion();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }

    doLayout();
  }

  public void InicializarLocalizacion() {
    Hashtable hash;
    boolean bReset = false;

    // pais seleccionado
    if (chPais.getSelectedIndex() > 0) {

      hash = (Hashtable) listaPaises.elementAt(chPais.getSelectedIndex() - 1);

      // Espa�a
      if ( ( (String) hash.get("CD_PAIS")).equals("ESP")) {

        chCA.setEnabled(true);

        txtNivel1.setEnabled(true);
        btnNivel1.setEnabled(true);
        if (this.txtNivelL1.getText().length() > 0) {
          txtNivel2.setEnabled(true);
          btnNivel2.setEnabled(true);
        }

        if (chCA.getSelectedIndex() > 0) {
          chProvincia.setEnabled(true);

          if (chProvincia.getSelectedIndex() > 0) {
            txtMunicipio.setEnabled(true);
            btnMunicipio.setEnabled(true);

            // sin provincia
          }
          else {
            txtMunicipio.setEnabled(false);
            btnMunicipio.setEnabled(false);
          }

          // sin CA
        }
        else {
          chProvincia.setEnabled(false);
          txtMunicipio.setEnabled(false);
          btnMunicipio.setEnabled(false);
        }
        // Otro Pais
      }
      else {
        bReset = true;
      }

      // sin Pais
    }
    else {
      bReset = true;
    }

    if (bReset) {
      txtNivel1.setEnabled(false);
      btnNivel1.setEnabled(false);
      txtNivel2.setEnabled(false);
      btnNivel2.setEnabled(false);
      chCA.select(0);
      chCA.setEnabled(false);
      if (chProvincia.getItemCount() > 1) {
        chProvincia.select(0);
      }
      chProvincia.setEnabled(false);
      txtMunicipio.setEnabled(false);
      btnMunicipio.setEnabled(false);
      txtNivel1.setText("");
      txtNivel2.setText("");
      txtMunicipio.setText("");
    }
    txtEnfermedad.setEnabled(true);
    txtMunicipioL.setEnabled(false);
    txtApellido1.setEnabled(true);
    txtApellido2.setEnabled(true);
    txtNombre.setEnabled(true);
  }

  /**
   *  Esta funci�n devuelve el idioma
   */
  public int getIdioma() {
    return app.getIdioma();
  }

  public CLista getListaDatosEnfermo() {
    return listaDatosEnfermo;
  }

  public void setListaDatosEnfermo(CLista lista) {
    listaDatosEnfermo = lista;
  }

  public void txtFocusEnfermedad() {

    try {

      CLista data = new CLista();
      CLista result;
      String campo = txtEnfermedad.getText();
      Inicializar();
      if (campo.trim().length() > 0 &&
          txtEnfermedadL.getText().trim().length() == 0) {

        enfermo.DataEnfermo dacat = new enfermo.DataEnfermo("CD_ENFCIE");
        dacat.setFiltro(txtEnfermedad.getText());
        //  dacat.put("CD_ENFCIE", txtEnfermedad.getText());
        data.addElement(dacat);
        result = comun.traerDatos(app, stubCliente, comun.strSERVLET_ENFERMO,
                                  SrvEnfermo.modoENFERMEDAD +
                                  SrvGeneral.servletOBTENER_X_CODIGO, data);

        /*
                    DataCat dacat = new DataCat (txtEnfermedad.getText());
                    data.addElement(dacat);
                    result = comun.traerDatos(app, stubCliente, comun.strSERVLET_CAT, Catalogo.catPROCESOS + SrvGeneral.servletOBTENER_X_CODIGO, data);
         */
        if (result != null && result.size() > 0) {

          enfermo.DataEnfermo dato = (enfermo.DataEnfermo) result.firstElement();
          if (dato != null) {
            txtEnfermedad.setText( (String) dato.get("CD_ENFCIE"));
            txtEnfermedadL.setText( (String) dato.get("DS_PROCESO"));
          }
          else {
            CMessage msgBox = new CMessage(app, CMessage.msgAVISO,
                                           res.getString("msg5.Text"));
            msgBox.show();
            msgBox = null;
          }

          /*
                DataCat dato = (DataCat) result.firstElement();
                if (dato != null) {
                    txtEnfermedad.setText( dato.getCod());
                    txtEnfermedadL.setText((String) dato.getDes());
                }else{
                   CMessage msgBox = new CMessage(app,CMessage.msgAVISO, res.getString("msg5.Text"));
                   msgBox.show();
                   msgBox = null;
                }
           */

        }
        else {
          CMessage msgBox = new CMessage(app, CMessage.msgAVISO,
                                         res.getString("msg5.Text"));
          msgBox.show();
          msgBox = null;
        } //else
      }

    }

    catch (Exception ex) {
      ex.printStackTrace();
      CMessage msgBox = new CMessage(app, CMessage.msgAVISO, ex.getMessage());
      msgBox.show();
      msgBox = null;

    }

    //     }

  }

  public void txtFocusNivel1() {
    CLista data = new CLista();
    CLista result;
    String campo = txtNivel1.getText();
    Inicializar();
    if (campo.trim().length() > 0 && txtNivelL1.getText().trim().length() == 0) {
      DataCat dacat = new DataCat(txtNivel1.getText());
      data.addElement(dacat);
      result = comun.traerDatos(app, stubCliente, comun.strSERVLET_CAT,
                                Catalogo.catNIVEL1 +
                                SrvGeneral.servletOBTENER_X_CODIGO, data);

      if (result != null && result.size() > 0) {
        DataCat dato = (DataCat) result.firstElement();
        if (dato != null) {
          txtNivel1.setText(dato.getCod());
          ///************  problema del idioma
          txtNivelL1.setText(dato.getDes());
          txtNivel2.setText("");
          txtNivelL2.setText("");
        }
        else {
          CMessage msgBox = new CMessage(app, CMessage.msgAVISO,
                                         res.getString("msg5.Text"));
          msgBox.show();
          msgBox = null;
        }
      }
      else {
        CMessage msgBox = new CMessage(app, CMessage.msgAVISO,
                                       res.getString("msg5.Text"));
        msgBox.show();
        msgBox = null;
      }
    }
  }

  public void txtFocusNivel2() {
    CLista data = new CLista();
    CLista result;
    String campo = txtNivel2.getText();
    if (campo.trim().length() > 0 && txtNivelL2.getText().trim().length() == 0) {
      Inicializar();
      DataZBS2 dacat = new DataZBS2(txtNivel1.getText(), txtNivel2.getText(),
                                    "", "");
      data.addElement(dacat);
      result = comun.traerDatos(app, stubCliente, comun.strSERVLET_NIV2,
                                ApPruEnfermo.servletOBTENER_NIV2_X_CODIGO, data);

      if (result != null && result.size() > 0) {
        DataZBS2 dato = (DataZBS2) result.firstElement();
        if (dato != null) {
          txtNivel2.setText(dato.getNiv2());
          txtNivelL2.setText(dato.getDes());
        }
        else {
          CMessage msgBox = new CMessage(app, CMessage.msgAVISO,
                                         res.getString("msg5.Text"));
          msgBox.show();
          msgBox = null;
        }
      }
      else {
        CMessage msgBox = new CMessage(app, CMessage.msgAVISO,
                                       res.getString("msg5.Text"));
        msgBox.show();
        msgBox = null;
      }
    }
  }

  public void txtFocusMunicipio() {
    CLista data = new CLista();
    CLista result;
    String campo = txtMunicipio.getText();
    if (campo.trim().length() > 0 &&
        txtMunicipioL.getText().trim().length() == 0) {
      Inicializar();
      enfermo.DataEnfermo dacat = new enfermo.DataEnfermo("CD_MUN");
      dacat.put("CD_MUN", campo);
      data.addElement(dacat);
      result = comun.traerDatos(app, stubCliente, comun.strSERVLET_ENFERMO,
                                SrvEnfermo.modoMUNICIPIO +
                                SrvGeneral.servletOBTENER_X_CODIGO, data);

      if (result != null && result.size() > 0) {
        enfermo.DataEnfermo dato = (enfermo.DataEnfermo) result.firstElement();
        if (dato != null) {
          txtMunicipio.setText( (String) dato.get("CD_MUN"));
          txtMunicipioL.setText( (String) dato.get("DS_MUN"));
        }
        else {
          CMessage msgBox = new CMessage(app, CMessage.msgAVISO,
                                         res.getString("msg5.Text"));
          msgBox.show();
          msgBox = null;
        }
      }
      else {
        CMessage msgBox = new CMessage(app, CMessage.msgAVISO,
                                       res.getString("msg5.Text"));
        msgBox.show();
        msgBox = null;
      }
    }
  }

  public void cargarProvincias(int indexCA) {
    CLista appList = null;
    CLista data = new CLista();
    int modoLlamadaServlet;
    boolean bIdiomaPorDefecto = (app.getIdioma() == app.idiomaPORDEFECTO);
    if (indexCA >= 0) {
      appList = listaCA;
      String codCA = (String) ( (datasuca) appList.elementAt(indexCA)).get(
          "CDCOMU");

      listaProvincias = null;

      datasuca dat = new datasuca();
      dat.put("CD_CA", codCA);
      data.addElement(dat);

      // Distintas posibilidades de llamada
      modoLlamadaServlet = (bTramero ? srvsuca.modoPROVINCIAS :
                            srvsuca.modoPROVINCIAS_SIN_TRAMERO);
      if (!bTramero) {
        data.addElement(bIdiomaPorDefecto ? "" : "N");
      }
      //********************************************************************

       listaProvincias = comun.traerDatos(app, stubCliente, strSERVLET_SUCA,
                                          modoLlamadaServlet, data);

      if (chProvincia.getItemCount() > 0) {
        chProvincia.removeAll();

      }
      if (listaProvincias.size() > 0) {
        chProvincia.add(" ");
        comun.writeChoice(app, chProvincia, listaProvincias, false, "DS_PROV");
        if (listaProvincias.size() == 1) {
          chProvincia.select(1);
          //modoOperacion = modoPROVINCIAS;
        }
        else {
          //modoOperacion = modoNOPROVINCIAS;
        }
      }
    }
  }

  // limpia la pantalla
  void vaciarPantalla() {
    txtEnfermedad.setText("");
    txtEnfermedadL.setText("");
    txtApellido1.setText("");
    txtApellido2.setText("");
    txtNombre.setText("");
    chId.select(0);
    txtMunicipioL.setText("");
    txtMunicipio.setText("");
    txtNivel1.setText("");
    txtNivelL1.setText("");
    txtNivel2.setText("");
    txtNivelL2.setText("");
    txtIDL.setText("");
    chCA.select(0);
    chPais.select(0);
    if (chProvincia.getItemCount() > 0) {
      chProvincia.removeAll();
    }
    chProvincia.addItem("   ");
  }

  public void enfermedad_actionButton() {

    CListaEnfermedad lista = new CListaEnfermedad(app,
                                                  res.getString("msg6.Text"),
                                                  stubCliente,
                                                  comun.strSERVLET_ENFERMO,

                                                  SrvEnfermo.modoENFERMEDAD +
                                                  SrvGeneral.
                                                  servletOBTENER_X_CODIGO,
                                                  SrvEnfermo.modoENFERMEDAD +
                                                  SrvGeneral.
                                                  servletOBTENER_X_DESCRIPCION,
                                                  SrvEnfermo.modoENFERMEDAD +
                                                  SrvGeneral.
                                                  servletSELECCION_X_CODIGO,
                                                  SrvEnfermo.modoENFERMEDAD +
                                                  SrvGeneral.
        servletSELECCION_X_DESCRIPCION,
                                                  "CD_ENFCIE", "DS_PROCESO");
    //JRM2
    lista.buscarInicial();
    lista.show();
    enfermo.DataEnfermo dato = (enfermo.DataEnfermo) lista.getComponente();
    lista = null;
    if (dato != null) {
      String desEnfer = (String) dato.get("DS_PROCESO");
      String codEnfer = (String) dato.get("CD_ENFCIE");
      txtEnfermedadL.setText(desEnfer);
      txtEnfermedad.setText(codEnfer);
    }

    /*
            CListaCat lista = new CListaCat(app,
         res.getString("msg6.Text"), stubCliente,
                                            comun.strSERVLET_CAT,
         Catalogo.catPROCESOS + SrvGeneral.servletOBTENER_X_CODIGO,
         Catalogo.catPROCESOS +SrvGeneral.servletOBTENER_X_DESCRIPCION,
         Catalogo.catPROCESOS +SrvGeneral.servletSELECCION_X_CODIGO,
         Catalogo.catPROCESOS +SrvGeneral.servletSELECCION_X_DESCRIPCION
                                            );
            lista.show();
            DataCat dato = (DataCat)lista.getComponente();
            lista = null;
            if (dato != null) {
               txtEnfermedadL.setText(dato.getCod());
               txtEnfermedad.setText(dato.getDes());
            }
     */
    modo = modoSELECION;
  }

  protected void lanza_busqueda() {
    // rellenamos la hashtable solo con los datos que tengan valores utiles
    enfermo.DataEnfermo dEnfer = new enfermo.DataEnfermo("CD_ENFERMO");
    String filtro = null;
    int indice = 0;
    CLista result = null;

    listaDatosBusqueda = new CLista();
    listaDatosBusqueda.setIdioma(getIdioma());

    //// System_out.println ( "Lanzamos la b�squeda ");
    //dEnfer.setFechaNacimiento("17/04/1970");

    filtro = txtIDL.getText();
    if (filtro != null && filtro.trim().length() > 0) {
      //ARG: el campo que contiene el DNI es DS_NDOC
      dEnfer.put("a.DS_NDOC", filtro);
      if (chId.getSelectedIndex() == 1) {
        filtro = "1";
      }
      else {
        filtro = "2";
      }
      dEnfer.put("a.CD_TDOC", filtro);
      //dEnfer.put("a.CD_ENFERMO", filtro);
    }

    ///   habr�a que comprobar si es fonetica o no
    filtro = txtApellido1.getText();
    if (filtro != null && filtro.trim().length() > 0) {
      if (chbBusFonetica.getState()) {
//// System_out.println(filtro + " -- " + comun.traduccionFonetica(filtro).trim()+"%");
        dEnfer.put(" a.DS_FONOAPE1",
                   comun.traduccionFonetica(filtro).trim() + "%");
      }
      else {
        dEnfer.put("a.DS_APE1", filtro + "%");
      }
    }
    filtro = txtApellido2.getText();
    if (filtro != null && filtro.trim().length() > 0) {
      if (chbBusFonetica.getState()) {
        dEnfer.put(" a.DS_FONOAPE2",
                   "'" + comun.traduccionFonetica(filtro).trim() + "%'");
      }
      else {
        dEnfer.put("a.DS_APE2", filtro + "%");
      }

    }
    filtro = txtNombre.getText();
    if (filtro != null && filtro.trim().length() > 0) {
      if (chbBusFonetica.getState()) {
        dEnfer.put(" a.DS_FONONOMBRE",
                   comun.traduccionFonetica(filtro).trim() + "%");
      }
      else {
        dEnfer.put("a.DS_NOMBRE", filtro + "%");
      }
    }

    // miramos si queremos ver los enfermos de baja
    dEnfer.setMostrarBajas(chkbMostrarBaja.getState());

    filtro = txtNivel1.getText();
    if (filtro != null && filtro.trim().length() > 0) {
      dEnfer.put("a.CD_NIVEL_1", filtro);
    }
    filtro = txtNivel2.getText();
    if (filtro != null && filtro.trim().length() > 0) {
      dEnfer.put("a.CD_NIVEL_2", filtro);
    }
    indice = chPais.getSelectedIndex();
    indice--; // leq uiqtamos uno por le componenete nulo
    if (indice >= 0) {
      filtro = (String) ( (Hashtable) listaPaises.elementAt(indice)).get(
          "CD_PAIS");
      if (filtro != null && filtro.trim().length() > 0) {
        dEnfer.put("a.CD_PAIS", filtro);
      }
    }

    indice = chProvincia.getSelectedIndex();
    indice--; // leq uiqtamos uno por le componenete nulo
    if (indice >= 0) {
      filtro = (String) ( (Hashtable) listaProvincias.elementAt(indice)).get(
          "CD_PROV");
      if (filtro != null && filtro.trim().length() > 0) {
        dEnfer.put("a.CD_PROV", filtro);
      }
    }

    filtro = txtMunicipio.getText(); /// MUNICIPIO
    if (filtro != null && filtro.trim().length() > 0) {
      dEnfer.put("a.CD_MUN", filtro);
    }
    filtro = txtEnfermedad.getText(); /// ENFERMEDAD
    if (filtro != null && filtro.trim().length() > 0) {
      dEnfer.put("b.CD_ENFCIE", filtro);
    }

    listaDatosBusqueda.addElement(dEnfer);

    //// System_out.println("ANTES traimoslalista" + listaEnfermos);

    if (bTramero) {
      listaEnfermos = comun.traerDatos(app, stubCliente,
                                       comun.strSERVLET_ENFERMO,
                                       SrvEnfermo.modoBUSCAENFERMOTRAMERO,
                                       listaDatosBusqueda);
    }
    else {
      listaEnfermos = comun.traerDatos(app, stubCliente,
                                       comun.strSERVLET_ENFERMO,
                                       SrvEnfermo.modoBUSCAENFERMO,
                                       listaDatosBusqueda);

    }
    if (listaEnfermos == null) {
      CMessage msgBox = new CMessage(app, CMessage.msgAVISO,
                                     res.getString("msg5.Text"));
      msgBox.show();
      msgBox = null;
    }
    else {
      escribirTabla(listaEnfermos);
    }
  }

  /**
   *  Lee el c�digo del enfermo seleccionado
   *  llama a la funci�n que rellene la pantalla  del Pan_Enfermo
   *  y se despide
   */
  void tabla_actionPerformed(JCActionEvent e) {
    int indice = tablaEnfermos.getSelectedIndex();
    Hashtable h;
    Object code = null;
    String descrip = null;
    int indLista = -1;

    if (indice < 0) {
      // se ha producido lag�n error y nos vamos
      return;
    }

    if (indice == listaEnfermos.size()) {
      /// se ha pinchado el elemento M�s...
      listaDatosBusqueda.setFilter(listaEnfermos.getFilter());

      listaDatosBusqueda.setState(CLista.listaINCOMPLETA); //LRIV
      CLista result = comun.traerDatos(app, stubCliente,
                                       comun.strSERVLET_ENFERMO,
                                       SrvEnfermo.modoBUSCAENFERMO,
                                       listaDatosBusqueda);
      listaEnfermos.appendData(result);
      escribirTabla(listaEnfermos);
    }
    else {
      /// se buscan los datos del enfermo pinchado y se vuelve
      h = (Hashtable) (listaEnfermos.elementAt(indice));
      code = h.get("CD_ENFERMO");

      CLista data = new CLista();
      data.setIdioma(app.getIdioma());
      enfermo.DataEnfermo dEnfer = null;
      //le indicamos el enfermo seleccionado
      if (code != null) {
        // buscamos todos los datosa del enfermo
        dEnfer = new enfermo.DataEnfermo("CD_ENFERMO");
        dEnfer.put("CD_ENFERMO", "'" + code + "'");
        data.addElement(dEnfer);
        listaDatosEnfermo = comun.traerDatos(app, stubCliente,
                                             comun.strSERVLET_ENFERMO,
                                             ApPruEnfermo.modoDATOSENFERMO,
                                             data);

        // a�adimos las descripciones de ENFERMEDAD, TDOC, SEXO, PAIS, CA
        if (listaDatosEnfermo != null && listaDatosEnfermo.size() > 0) {
          dEnfer = (enfermo.DataEnfermo) listaDatosEnfermo.firstElement();
          descrip = (String) h.get("CD_ENFCIE");
          if (descrip != null) {
            dEnfer.put("CD_ENFCIE", descrip);
          }
          descrip = (String) h.get("DS_PROCESO");
          if (descrip != null) {
            dEnfer.put("DS_PROCESO", descrip);
          }

          indLista = comun.buscaDato(listaId, "CD_TDOC",
                                     (String) dEnfer.get("CD_TDOC"));
          if (indLista >= 0 && indLista < listaId.size()) {
            descrip = (String) ( (Hashtable) listaId.elementAt(indLista)).get(
                "DS_TDOC");
            if (descrip != null) {
              dEnfer.put("DS_TDOC", descrip);
            }
          }
          indLista = comun.buscaDato(listaPaises, "CD_PAIS",
                                     (String) dEnfer.get("CD_PAIS"));
          if (indLista >= 0 && indLista < listaPaises.size()) {
            descrip = (String) ( (Hashtable) listaPaises.elementAt(indLista)).
                get("DS_PAIS");
            if (descrip != null) {
              dEnfer.put("DS_PAIS", descrip);
            }
          }
          indLista = comun.buscaDato(listaCA, "CD_CA",
                                     (String) dEnfer.get("CD_CA"));
          if (indLista >= 0 && indLista < listaCA.size()) {
            descrip = (String) ( (Hashtable) listaCA.elementAt(indLista)).get(
                "DS_CA");
            if (descrip != null) {
              dEnfer.put("DS_CA", descrip);
            }
          }

        }
      }

      // le gravamos los datos pedidos
      dispose();
    }
  }

  // rellena la tabla
  void escribirTabla(CLista lista) {
    enfermo.DataEnfermo datUnaLinea;
    StringBuffer datLineaEscribir = new StringBuffer();
    String dato = null;
    int tam;

    //Borramos la tabla que hab�a
    tam = tablaEnfermos.countItems();
    if (tam > 0) {
      tablaEnfermos.deleteItems(0, tam - 1);
    }

    if (lista == null || (lista.size() == 0)) {
      CMessage msgBox = new CMessage(app, CMessage.msgAVISO,
                                     res.getString("No_hay_datos_"));
      msgBox.show();
      msgBox = null;
      /// no tiene elementos porlo que no hay que pintar nada
      return;
    }

    Enumeration enum = lista.elements();

    while (enum.hasMoreElements()) {
      //Recogemos los datos de una l�nea
      datUnaLinea = (enfermo.DataEnfermo) (enum.nextElement());
      //ponemos a cero el buffer
      datLineaEscribir.setLength(0);

      Object o = (datUnaLinea.get("CD_ENFERMO"));
      if (o != null) {
//          // System_out.println("cd_enfermo " + o.toString());
        datLineaEscribir.append(o.toString());
      }
      datLineaEscribir.append("&");
      if (bPerNom) {
        dato = (String) datUnaLinea.get("DS_APE1");
        if (dato != null) {
          datLineaEscribir.append(dato);
        }
        datLineaEscribir.append(" ");
        dato = (String) datUnaLinea.get("DS_APE2");
        if (dato != null) {
          datLineaEscribir.append(dato);
        }
        datLineaEscribir.append(" ");
        dato = (String) datUnaLinea.get("DS_NOMBRE");
        if (dato != null) {
          datLineaEscribir.append(dato);
        }
      }
      else {
        dato = (String) datUnaLinea.get("SIGLAS");
        if (dato != null) {
          datLineaEscribir.append(dato);
        }
      }
      datLineaEscribir.append("&");
      dato = (String) datUnaLinea.get("FC_NAC");
      if (dato != null) {
        datLineaEscribir.append(dato);
      }
      //Date dd =(Date) datUnaLinea.get("FC_NAC");
      //if ( dd != null){
      //    dato = Fechas.date2String(dd);
      //    if (dato != null){ datLineaEscribir.append(dato);}
      //}
      datLineaEscribir.append("&");
      dato = (String) datUnaLinea.get("DS_MUN");
      if (dato != null) {
        datLineaEscribir.append(dato);
      }
      datLineaEscribir.append("&");
      dato = (String) datUnaLinea.get("DS_PROCESO");
      if (dato != null) {
        datLineaEscribir.append(dato);
      }
      datLineaEscribir.append("&");

      datLineaEscribir.append(" &");

      //A�adimos la l�nea a la tabla
      tablaEnfermos.addItem(datLineaEscribir.toString(), '&'); //'&' es el car�cter separador de datos (cada datos va a una columnna)
    } // elihw

    // miramos si hay que a�adir Mas ...
    if (lista.getState() == CLista.listaINCOMPLETA) {
      //  como la lista est� incompleta ponemos lo de m�s
      tablaEnfermos.addItem(res.getString("msg7.Text"), '&');
    }
  }

  /**
   *  esta funci�n fija los parametros para que se muestren los datos
   */
  public void setData(CLista a_dat) {
  }

  /**
   *  esta funci�n devuelvelos valores calculados
   */
  public CLista getData() {
    return listaDatosEnfermo;
  }

  /**
   *  esta clase recoge los eventos de pinchar en las lupas
   *  cuando se pincha a una lupa se llama a un servlet para pedir los datos
   *  se deshabilitan el resto de los botones lupa
   *  y se manda ejecutar la acci�n correspondiente
   */
// action listener de evento en bot�nes
  class BusEnfermoBtnActionListener
      implements ActionListener, Runnable {
    DialBusEnfermo adaptee = null;
    ActionEvent e = null;

    public BusEnfermoBtnActionListener(DialBusEnfermo adaptee) {
      this.adaptee = adaptee;
    }

    // evento
    public void actionPerformed(ActionEvent e) {
      //if  ( adaptee.bloquea()){
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
      //}
    }

    // hilo de ejecuci�n para servir el evento
    public void run() {
      // deshabilita los escuchadores de cambios de c�digo
      String name = e.getActionCommand();
      String name2 = ( (Component) e.getSource()).getName();
      CLista lalista = null;
      String campo = null;

      int antModo = modo;

      modo = modoESPERA;
      Inicializar();

      /// buscamos que bot�n es el que se ha pinchado y lo ejecutamos
      if (name.equals("btnEnfermedad") || name2.equals("btnEnfermedad")) {
        adaptee.enfermedad_actionButton();
      }
      else if (name.equals("btnMunicipio") || name2.equals("btnMunicipio")) {
        CListaEnfermedad lista = new CListaEnfermedad(adaptee.getCApp(),
            res.getString("msg2.Text"), adaptee.stubCliente,
            comun.strSERVLET_ENFERMO,
            SrvEnfermo.modoMUNICIPIO + SrvGeneral.servletOBTENER_X_CODIGO,
            SrvEnfermo.modoMUNICIPIO + SrvGeneral.servletOBTENER_X_DESCRIPCION,
            SrvEnfermo.modoMUNICIPIO + SrvGeneral.servletSELECCION_X_CODIGO,
            SrvEnfermo.modoMUNICIPIO +
            SrvGeneral.servletSELECCION_X_DESCRIPCION,
                           "CD_MUN", "DS_MUN");
        // a�adimosla provincia para que filtre por provincias
        int indice = chProvincia.getSelectedIndex();
        indice--;
        if (indice >= 0) {
          campo = (String) ( (Hashtable) listaProvincias.elementAt(indice)).get(
              "CD_PROV");
        }
        if (campo != null && (campo.trim().length() > 0)) {
          lista.setParameter("CD_PROV", campo);
        }

        //________ TRAZA
        /*
             for (Enumeration enumer = lista.hash.keys(); enumer.hasMoreElements();) {
                    String clave  = (String) enumer.nextElement();
                    // System_out.println("******Clave " + clave + " con valor " + (String)(lista.hash.get(clave) ) );
                }
         */
        //___________________

        //Hace b�squeda inicial
        lista.buscarInicial();

        lista.show();
        enfermo.DataEnfermo dato = (enfermo.DataEnfermo) lista.getComponente();
        lista = null;
        if (dato != null) {
          txtMunicipioL.setText( (String) dato.get("DS_MUN"));
          // guardamos le valor del c�digo
          txtMunicipio.setText( (String) dato.get("CD_MUN"));
        }
        modo = modoSELECION;
      }
      else if (name.equals("btnNivel1") || name2.equals("btnNivel1")) {
        CApp ap = adaptee.getCApp();
        CListaNivel1 lista = new CListaNivel1(adaptee.getCApp(),
                                              res.getString("msg3.Text") +
                                              ap.getNivel1(),
                                              adaptee.stubCliente,
                                              comun.strSERVLET_CAT,
                                              Catalogo.catNIVEL1 +
                                              SrvGeneral.servletOBTENER_X_CODIGO,
                                              Catalogo.catNIVEL1 +
                                              SrvGeneral.servletOBTENER_X_DESCRIPCION,
                                              Catalogo.catNIVEL1 +
                                              SrvGeneral.servletSELECCION_X_CODIGO,
                                              Catalogo.catNIVEL1 +
                                              SrvGeneral.servletSELECCION_X_DESCRIPCION);
        lista.show();
        DataCat dato = (DataCat) lista.getComponente();
        lista = null;
        if (dato != null) {
          txtNivel1.setText(dato.getCod());
          txtNivelL1.setText(dato.getDes());

        }
        modo = modoSELECION;
      }
      else if (name.equals("btnNivel2") || name2.equals("btnNivel2")) {
        CApp ap = adaptee.getCApp();
        CListaNivel2 lista = new CListaNivel2(ap,
                                              res.getString("msg3.Text") +
                                              ap.getNivel2(),
                                              stubCliente,
                                              comun.strSERVLET_NIV2,
                                              ApPruEnfermo.
                                              servletOBTENER_NIV2_X_CODIGO,
                                              ApPruEnfermo.
                                              servletOBTENER_NIV2_X_DESCRIPCION,
                                              ApPruEnfermo.
                                              servletSELECCION_NIV2_X_CODIGO,
                                              ApPruEnfermo.
            servletSELECCION_NIV2_X_DESCRIPCION,
                                              txtNivel1.getText());
        lista.show();
        DataZBS2 dato = (DataZBS2) lista.getComponente();
        lista = null;
        if (dato != null) {
          txtNivel2.setText(dato.getNiv2());

          txtNivelL2.setText(dato.getDes());
        }
        modo = modoSELECION;
      }
      else if (name.equals("btnBuscar")) {
        //// System_out.println("buscar");
        lanza_busqueda();
        modo = antModo;

      }
      else if (name.equals("btnAceptar")) {
        tabla_actionPerformed(null);
      }
      else if (name.equals("btnCancelar")) {
        //// System_out.println("cancelar");
        /// nos salimos y borramos todo
        // le gravamos los datos pedidos
        listaDatosEnfermo = null;
        dispose();
      }
      else if (name.equals("btnVaciar")) {
        vaciarPantalla();
        modo = modoNOPAIS;
      }

      Inicializar();
      // desbloquea la recepci�n  de eventos
      adaptee.desbloquea();
    }
  }

  class BusEnfermoTextFocusListener
      implements java.awt.event.FocusListener, java.awt.event.ActionListener,
      Runnable {
    DialBusEnfermo adaptee;
    String name2 = null;
    protected StubSrvBD stub = new StubSrvBD();

    BusEnfermoTextFocusListener(DialBusEnfermo adaptee) {
      this.adaptee = adaptee;
    }

    public void focusGained(FocusEvent e) {
    }

    public void focusLost(FocusEvent e) {
      //if  (adaptee.bloquea()){
      name2 = ( (Component) e.getSource()).getName();
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
      //}
    }

    public void actionPerformed(ActionEvent e) {
      //if  (adaptee.bloquea()){
      name2 = ( (Component) e.getSource()).getName();
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
      //}
    }

    public void run() {
      CLista lalista = null;
      String campo = null;
      CLista result = null, data = new CLista();
      enfermo.DataEnfermo denfer = null;
      try {

        if (name2.equals("enfermedad")) { // muestra la pantalla de b�squeda
          adaptee.txtFocusEnfermedad();
        }
        else if (name2.equals("nivel1")) {
          adaptee.txtFocusNivel1();
        }
        else if (name2.equals("nivel2")) {
          adaptee.txtFocusNivel2();
        }
        else if (name2.equals("municipio")) {
          adaptee.txtFocusMunicipio();
        }

      }
      catch (Exception exc) {
        exc.printStackTrace(); //// System_out.println("focusLis " + exc.toString());
      }
      //adaptee.desbloquea();
    }

  } //_______________________________________________ END_CLASS

  /**
   *  Esta clase recoge el evento de modificar los valores del TextField
       *  cuando cambia el valor simplemente borra el TextField descriptivo asociado
   */
  class BusEnfermoTextAdapter
      extends java.awt.event.KeyAdapter {
    DialBusEnfermo adaptee = null;
    KeyEvent e = null;

    BusEnfermoTextAdapter(DialBusEnfermo adaptee) {
      this.adaptee = adaptee;
    }

    public void keyPressed(KeyEvent e) {
//      if  (adaptee.bloquea()){
      this.e = e;
      // lanzamos el thread que tratar� el evento
//         new Thread(this).start();
      run();
//      }
    }

    public void run() {
      // quitamos los valores de los textfield de descripcion
      String name2 = ( (Component) e.getSource()).getName();
      CLista data = new CLista();

      //// System_out.println("XXevento " + name2 + adaptee.modoAnterior);
      /// buscamos que bot�n es el que se ha pinchado y lo ejecutamos
      if (name2.equals("enfermedad")) {
        txtEnfermedadL.setText("");
      }
      else if (name2.equals("nivel1")) {
        txtNivelL1.setText("");
        // borramos tambi�n el nivel2
        txtNivel2.setText("");
        txtNivelL2.setText("");
      }
      else if (name2.equals("nivel2")) {
        txtNivelL2.setText("");
      }
      else if (name2.equals("municipio")) {
        txtMunicipioL.setText("");
      }

      // adaptee.desbloquea();
    }
  } //_________________________________________________ END CLASS BusEnfermoTextAdapter

// escuchador de los click en la tabla
  class BusEnfermoTableAdapter
      implements jclass.bwt.JCActionListener, Runnable {
    DialBusEnfermo adaptee;
    JCActionEvent e;

    BusEnfermoTableAdapter(DialBusEnfermo adaptee) {
      this.adaptee = adaptee;
    }

    public void actionPerformed(JCActionEvent e) {
      //if  ( adaptee.bloquea()){
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
      //}
    }

    // hilo de ejecuci�n para servir el evento
    public void run() {

      modo = modoESPERA;
      Inicializar();

      //// System_out.println("Evento de la tabla " + e.toString());
      adaptee.tabla_actionPerformed(e);
      adaptee.desbloquea();
    }
  } //________________________________________________ END CLASS BusEnfermoTableAdapter

// escuchador de los cambios en los itemListener  de los choices
  /**
   *  hay tres CListas que representan paises,comunidades y provincias
   *  cuando cambiamos el pais, se rellena la lista de comunidades
   *  y se vacia la lista de Provincias
   */
  class BusEnfermoChoiceItemListener
      implements java.awt.event.ItemListener, Runnable {
    DialBusEnfermo adaptee;
    ItemEvent e = null;

    BusEnfermoChoiceItemListener(DialBusEnfermo adaptee) {
      this.adaptee = adaptee;
    }

    // evento
    public void itemStateChanged(ItemEvent e) {
      //if  (adaptee.bloquea()){
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
      //}
    }

    // hilo de ejecuci�n para servir el evento
    public void run() {
      // quitamos los valores de los textfield de descripcion
      String name2 = ( (Component) e.getSource()).getName();
      CLista data = new CLista();
      CLista appList = null;
      int indice;

      modo = modoESPERA;
      Inicializar();

      /// buscamos que bot�n es el que se ha pinchado y lo ejecutamos
      if (name2.equals("pais")) {
        //// System_out.println("metemos la ch pais");
        // recogemos el valor seleccionado y le restamos uno del valor  nulo
        indice = chPais.getSelectedIndex();
        indice--;
        String elpais = chPais.getSelectedItem();
        //borramos el municipio
        txtMunicipioL.setText("");
        txtMunicipio.setText("");

        // traemos los datos de las comunidades
        if (indice >= 0 && elpais.equalsIgnoreCase("ESPA�A")) {
          // esperamos a que haya terminado el thread
          modo = modoPAIS;
        }
        else {
          modo = modoNOPAIS;
        }
      }
      else if (name2.equals("ccaa")) {
        //// System_out.println("metemos la ch ccaa");
        // recogemos el valor seleccionado y lew restamos uno delvalor nulo
        adaptee.hazCA();
        //cargar provincias
      }
      else if (name2.equals("provincia")) {
        //borramos el municipio
        if (listaProvincias.size() > 0) {
          modo = modoPROVINCIA;
        }
        else {
          modo = modoNOPROVINCIA;
        }
      }

      Inicializar();
      // desbloquea la recepci�n  de eventos
      //adaptee.desbloquea();

    }

  } //________________________________________________ END CLASS BusEnfermoTableAdapter

  void txtNivel1_keyPressed(KeyEvent e) {
    txtNivel2.setText("");
    txtNivelL2.setText("");
    txtNivelL1.setText("");
    btnNivel2.setEnabled(false);
  }

} //_________________________________________________  END_CLASS BUSENFERMO

class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descripcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();
  }

  public Object setComponente(String filtro) {
    return new DataCat(filtro);
  }

  public String getCodigo(Object o) {
    return ( (DataCat) o).getCod();
  }

  public String getDescripcion(Object o) {
    return ( (DataCat) o).getDes();
  }
}

// lista de valores
class CListaNivel2
    extends CListaValores {

  protected String codNivel1;

  public CListaNivel2(CApp p,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion,
                      String a_codNivel1) {
    super(p,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    codNivel1 = a_codNivel1;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataZBS2(codNivel1, s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

class DialBusEnfermo_txtNivel1_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialBusEnfermo adaptee;

  DialBusEnfermo_txtNivel1_keyAdapter(DialBusEnfermo adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtNivel1_keyPressed(e);
  }
}
