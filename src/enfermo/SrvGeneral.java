package enfermo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Locale;
import java.util.StringTokenizer;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

/**
 *  Esta clase est� en el servidor recibe una CLista
 *  recoge de ella el nombre de la clase de datos y los par�metros
 *  ejecuta la sentencia SQL
 *  rellena una CLISTA con objetos de datos
 *  y finaliza la conexi�n
 */
public abstract class SrvGeneral
    extends DBServlet {

  // modos de operaci�n
  public static final int servletALTA = 0;
  public static final int servletMODIFICAR = 1;
  public static final int servletBAJA = 2;
  public static final int servletOBTENER_X_CODIGO = 3;
  public static final int servletOBTENER_X_DESCRIPCION = 4;
  public static final int servletSELECCION_X_CODIGO = 5;
  public static final int servletSELECCION_X_DESCRIPCION = 6;

  /** para reenviar los errores */
  String strError = null;

  /** conexi�n que se mantiene cada vez en el servidor */
  Connection con = null;

  /** statement de la sentencia SQL */
  PreparedStatement st = null;

  /** resultados de la query */
  ResultSet rs = null;

  /** sentencia sql que se est� ejecutando */
  StringBuffer strSQL = null;

  /** son los datos de los parametros */
  CLista param = null;

  /**  indica el n�mero de datos que hay que traer */
  int iNumReg = 0;

  /** indic asi es prueba o real */
  public boolean breal = true;

  //_______ L RIVERA _________________
  /*indica si en la consulta actual es necesario tener en cuenta los idiomas*/
  protected boolean bTratarIdiomas = false;
  //Hashtable para guardar parejas de campos en los que hay que tratar idiomas
  //P. ejemplo ("DS_NIVEL_1", "DSL_NIVEL_1")
  protected Hashtable hashIdiomas = new Hashtable();

  //__________________________________

  /// funciones abstractas para que las implementes los hijos
  //________________________________________________________________________
  /** devuelve la sentencia de SQL pedida
   *  @param a_imodo es el n�mero que indica la sentencia SQL
   *  @param a_data es el tipo de datos que tiene los parametros
   *  @param a_result  es la cadena que se va a devolver con la sentencia SQL
   *  @countEltos  devuelve el n�mero de registros que se tienen que leer
   */
//  protected abstract int prepareSQLSentencia  ( int a_imodo, Object a_data, StringBuffer  a_result);
  // a_result es el n�mero de sentencias y adem�s se le pasa la conexi�n
  // devuelve el stratement
  public abstract PreparedStatement prepareSQLSentencia(int a_imodo,
      Object a_data, StringBuffer a_result, StringBuffer countEltos,
      Connection con);

  //L RIVERA
  /* devuelve un booleano que indica si es necesario hacer tratamiento de idiomas o no en la consulta
   * hashIdiomas es la variable hashIdiomas que recoge las parejas de nombres de campos DS_CAMPO y DSL_CAMPO
   **  @param a_imodo es el n�mero que indica la sentencia SQL
   *Si hay que hacer tratamiento de idiomas se llamar� luego en cada registro recogido a la funci�n tratarIdiomas
   */

  public abstract boolean prepararTratamientoIdiomas(int a_imodo,
      Hashtable hashIdiomas);

  //________________________________________________________________________

  protected CLista doWork(int opmode, CLista param) throws Exception {
    CLista result = null;
    this.param = param;

    //// System_out.println("DEnfermo: bienvenido al servlet Enfermo");

    if (param == null) {
      // no nos han pasado datos iniciales
      return null;
    }

    // establece la conexi�n con la base de datos
    if (breal) {
      con = openConnection();
    }

    ///establecemos las propiedades indicadas

    con.setAutoCommit(true);

    result = realiza_SQL(opmode, param);
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return result;
  }

  //________________________________________________________________________

  protected CLista doPrueba(int opmode, CLista param) throws Exception {
    breal = false;
    // establece la conexi�n con la base de datos
    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
    con = DriverManager.getConnection("jdbc:odbc:pista_oracle", "pista",
                                      "loteb98");

    return doWork(opmode, param);
  }

  //________________________________________________________________________

  // funcionalidad del servlet
  /**
   *  sevlet generico recibe los parametros de entrada en la CLista
   *  y devuelve una CLista con los resultados
   */
  protected CLista realiza_SQL(int opmode, CLista a_param) throws Exception {
    // objetos JDBC

    // objetos de datos
    CLista result_data = null;
    /** parametros de entrada */
    CLista param = a_param;
    int iValor = 1;

    // prepara la sentencia con sus par�metros
    strSQL = new StringBuffer();
    StringBuffer numEltos = new StringBuffer();
    //iNumReg = prepareSQLSentencia(opmode, param.firstElement(), strSQL);
    st = prepareSQLSentencia(opmode, param.firstElement(), strSQL, numEltos,
                             con);

    //____________

    //Sie indica si la consulta tiene que tratar idiomas o no
    // y si es necesario, en qu� campos
    bTratarIdiomas = prepararTratamientoIdiomas(opmode, hashIdiomas);

    //_____________

    // System_out.println("DSe lanza sentencia : " + strSQL.toString());
    if (strSQL.length() <= 0) {
      //// System_out.println("Error La sentencia pedida no existe ");
      return null;
    }
    else {
      iNumReg = Integer.parseInt(numEltos.toString());
      // System_out.println("******** inumReg en SrvGeneral  " + iNumReg);
    }

    if (iNumReg > 0) {
      // lo que se ha lanzado es una select
      rs = st.executeQuery();
      result_data = recogerDatos();
      rs.close();
      rs = null;
    }
    else {
      // se est� modificando la base de datos
      st.executeUpdate();
      // siempre se deuvelve una lista aunque sea vacia
      result_data = new CLista();
      //// System_out.println("DEnfermo: executeUpdate");

      //le meto a
    }

    //// System_out.println("DEnfermo: adios");
    // cerramos el cursor
    st.close();
    st = null;

    // System_out.println("SrvGeneral: cierrra el st");

    if (result_data != null) {
      result_data.trimToSize();
    }

    // System_out.println("SrvGeneral:realiza_SQL   devuelve valor");
    return result_data;
  }

  //________________________________________________________________________

  private String timestamp_a_cadena(java.sql.Timestamp ts) {
    // yyyy-mm-dd hh:mm:ss ---->  dd/mm/yyyy hh:mm:ss
    String aux = ts.toString();
    String res = aux.substring(11, 19); // hh:mm:ss
    res = aux.substring(0, 4) + ' ' + res; //a�o    yyyy hh:mm:ss
    res = aux.substring(5, 7) + '/' + res; //mes    mm/yyyy hh:mm:ss
    res = aux.substring(8, 10) + '/' + res; //dia   dd/mm/yyyy hh:mm:ss
    return res;
  }

  //________________________________________________________________________

  public CLista recogerDatos() {
    CLista v = new CLista();
    Hashtable h;
    Object o;
    StringTokenizer parserSQL = null;
    String token;
    int num_datos = 0;
    Object campo_filtro = null;
    int i;
    String fecha;
    Integer cod;

    String fc_ultact = "";

    //// System_out.println("DEnfermo: entramos en recoger datos");
    try {

      // System_out.println("******** inumReg en SrvGeneral recogerDatos()  " + iNumReg);

      //Se recorren todos los registros del ResultSet rs
      // llevando cuenta de cuantos llevamos
      for (i = 1; rs.next(); i++) { //Cambiado i de 0 a 1*****

        // System_out.println("******** i vale  " + i);

        // control de tama�o
        if (i > iNumReg) { //DBServlet.maxSIZE) {
          v.setState(CLista.listaINCOMPLETA);
          campo_filtro = ( (Data) v.lastElement()).getCampoFiltro();
          // System_out.println("Campo filtro es ******"  + campo_filtro);
          if (campo_filtro != null) {
            o = ( (DataEnfermo) v.lastElement()).get(campo_filtro);
            v.setFilter(o.toString());
            // System_out.println("Se mete filtro en vector ******"  + v.getFilter());
          }
          // System_out.println("******** iNumReg en SrvGeneral saliendo de recogerDatos()  " + iNumReg);
          break;
        }

        // de la clase hija nos da los datos del servlet
        // copiamos la clase hija paraque conserve el filtrado
        //Esto har� p.ej un new Data("CD_ENFCIE") siendo CD_ENFCIE el campo de filtrado
        h = (Hashtable) ( (Data) param.firstElement()).getNewData();

        //Se tokeniza la sent. SQL para saber a partir de ella los campos
        //que sea necesario recoger de cada registro del resultSet rs
        parserSQL = new StringTokenizer(strSQL.toString(), ", ");
        token = parserSQL.nextToken();
        // System_out.println("******** Se pilla token ******"  + token);

        //Primer token es el "SELECT" : lo saltamos siempre
        token = parserSQL.nextToken();
        // System_out.println("******** Se pilla token ******"  + token);

        //Entre el segundo token (actual ) hasta encontrar el "FROM"
        //Estar�n los campos buscados
        for (; ! (token.equals("FROM")); ) {

          // Si hay un join quitamos la marca del join
          //para luego poder hacer p.ejemplo getObject("CD_NIVEL_1")
          //en vez de  un getObject("a.CDNIVEL1")
          if (token.charAt(1) == '.') {
            token = token.substring(2);
          }

          else if (token.equals("DISTINCT")) {
            token = parserSQL.nextToken();
            // System_out.println("******** Se a�ade h (registro) ******"  + token);
            continue;
          }

          if (token.equals("FC_ULTACT")) {
            fc_ultact = timestamp_a_cadena(rs.getTimestamp(token));
            if (fc_ultact != null) {

              h.put(token, fc_ultact);
            }
          }

          //En los tokens correspondientes a campos que haya que consultar
          //Se recoge el objeto (puede ser de cualquier clase) de la base de datos
          else {
            o = rs.getObject(token);
            if (o != null) { // si es null no lo metemos

              // convierte las fechas a cadena
              if (o.getClass().getName().equals("java.sql.Timestamp")) {
                fecha = (String) Fechas.date2String( (java.util.Date) o);
                h.put(token, fecha);
              }
              else if (o.getClass().getName().equals("java.sql.Date")) {
                fecha = (String) Fechas.date2String( (java.util.Date) o);
                h.put(token, fecha);
              }
              else if (o.getClass().getName().equals("java.math.BigDecimal")) {
                cod = new Integer( ( (Number) o).intValue());
                h.put(token, cod);
              }
              else {
                h.put(token, o);
                // System_out.println("*Se mete en h token de    "  + token );
                // System_out.println("******** con objeto  "  + o.toString());
              }
            }
          }

          token = parserSQL.nextToken();
          // System_out.println("*Siguiente token  "  + token );
        }
        //Si procede se tratan los idiomas para llevar dsl a ds en este registro
        if (bTratarIdiomas) {
          tratarIdiomas(h);

        }
        v.addElement(h);
        // System_out.println("******** Se a�ade h (registro) ******"  + token);
      } //Fin for de recorrido registros

      // vemos si est� llena la lista
      if (i <= iNumReg) {
        v.setState(CLista.listaLLENA);
      }

      // System_out.println("Saliendo de recoger datos estado lista "+ v.getState() );
      // System_out.println("DEnfermo:salimos bien");
      return v;
    }

    catch (SQLException exc) {
      exc.printStackTrace();
      strError = exc.toString();
      return null;
    }
  }

  //_________________________________________________________________

  /* LRIVERA
    //Se tratan los idiomas para, si es necesario, llevar dsl a ds en este registro
   /*Par entrada: Hashtable con datos del registro de la b.datos tratado
    */
   void tratarIdiomas(Hashtable hashRegistro) {

     String nombreCampoDS;
     String nombreCampoDSL;
     //Valores del esos campos en registro
     String sDesL = "";

     // Si idioma es el local
     if ( (param.getIdioma() != CApp.idiomaPORDEFECTO)) {

       //Se recorren la hash de idiomas para buscar las parejas de nombres de campos de ds y dsl
       for (Enumeration e = hashIdiomas.keys(); e.hasMoreElements(); ) {
         nombreCampoDS = (String) e.nextElement();
         nombreCampoDSL = (String) (hashIdiomas.get(nombreCampoDS));

         //Si se han recogido las dos descripciones
         if ( (hashRegistro.containsKey(nombreCampoDS)) &&
             (hashRegistro.containsKey(nombreCampoDSL))) {

           //Se recoge la desc en idioma
           sDesL = (String) (hashRegistro.get(nombreCampoDSL));
           //Si no es nula se mete en campo desc
           if (sDesL != null) {
             hashRegistro.put(nombreCampoDS, sDesL);
           }
         } //if

       } //for
     } //if
   }

  //________________________________________________________________________
  /**
   *  esta funci�n comprueba si hay un dato de filtrado
   *  si lo hay a�ade la clausula WHERE
   *  @param a_data es el campo de la select por el que se quiere filtrar
   *  @param is_like es true si debes usar un like
   *  @param a_where es un booleano que indica si se a�ade WHERE O AND
   *  @param a_strOrder es el campo por el que se ordena
   *  @param a_order es un booleano que indica si se ordena por el campo
   *
   */

  public String ponFiltro(String a_data, boolean is_like, boolean a_where,
                          String a_strOrder, boolean a_order) {
    //// System_out.println("DEnfermo: Lo que recibimose " + param.firstElement());
    Data parameter = (Data) (param.firstElement());
    //// System_out.println("DEnfermo:2 Lo que recibimose");
    Object o = null;
    String clave = null;
    StringBuffer resultSQL = new StringBuffer();
    boolean hay_parametros = false;

//// System_out.println("DponFiltro K:" + a_data + is_like + a_where + a_order);
    /// a�adimos los parametros que nos meten por defecto
    for (Enumeration e = parameter.keys(); e.hasMoreElements(); ) {
      clave = (String) e.nextElement();
      //// System_out.println("Dparametro " + clave + o);
      if (clave != null) {
        if (hay_parametros) { // si ya hay parametrosa�adimo el AND
          resultSQL.append(" AND ");
        }
        if (clave.equals("FC_NAC")) {
          // rango de busqueda para la fecha de nacimiento
          resultSQL.append(clave + " >= ? AND " + clave + " <= ? ");
        }
        else {
          if (is_like) {
            // ARG: upper (9/5/02)
            resultSQL.append(" upper(" + clave + ") LIKE upper(?) ");
          }
          else {
            resultSQL.append(clave + " =  ? ");
          }
        }
        hay_parametros = true;
      }
    }

    // a�adimos le filtro, se lee del Data
    String filtro = (String) parameter.getFiltro();
    if (filtro != null && filtro.trim().length() > 0) {
      if (hay_parametros) { // si ya hay parametrosa�adimo el AND
        resultSQL.append(" AND ");
      }
      if (is_like) {
        // ARG: upper (9/5/02)
        resultSQL.append(" upper(" + a_data + ") LIKE upper(?) ");
      }
      else {
        resultSQL.append(a_data + " =  ? ");
      }

      hay_parametros = true;
    }

    // a�adimos la paginaci�n
    if (param.getFilter().length() > 0) {
      if (hay_parametros) { // si ya hay parametrosa�adimo el AND
        resultSQL.append(" AND ");
      }
      resultSQL.append(a_data + " >  ? ");
      hay_parametros = true;
    }

    // se a�ade la ordenaci�n
    if (a_order) {
      resultSQL.append(" order by " + a_strOrder);
    }

    // a�adimos la clausula WHERE o el AND
    if (a_where && hay_parametros) {
      return " WHERE " + resultSQL.toString();
    }
    if (hay_parametros) {
      return " AND " + resultSQL.toString();
    }

    return resultSQL.toString();
  }

  //________________________________________________________________________

  private java.util.Date calculaRango(java.util.Date date, int dias) {
    Calendar cal = new GregorianCalendar(new Locale("es", "ES"));
    cal.setTime(date);
    cal.add(cal.DATE, dias);

    return cal.getTime();
  }

  //________________________________________________________________________
  /**
   *  esta funci�n rellena las ? , pone los par�metros
   *  @param a_data es el campo de la select por el que se quiere filtrar
   *  @param is_like es true si debes usar un like
   *  @param a_where es un booleano que indica si se a�ade WHERE O AND
   *  @param a_strOrder es el campo por el que se ordena
   *  @param a_order es un booleano que indica si se ordena por el campo
   */

  public void prepareFiltro(String a_data, boolean is_like, boolean a_where,
                            String a_strOrder, boolean a_order) {
    //// System_out.println("DEnfermo: Lo que recibimose " + param.firstElement());
    Data parameter = (Data) (param.firstElement());
    //// System_out.println("DEnfermo:2 Lo que recibimose");
    Object o = null;
    String clave = null;
    int iNumParam = 1;
    try {
//// System_out.println("DponFiltro K:" + a_data + is_like + a_where + a_order);

      /*
               La asignaci�n de valores a los par�metros tiene tres partes:
               1)  (Se hace en el bucle for)
           Se ven todas las claves que usuario ha metido en la hashTable de petici�n
               Se recuperan los valores con valor = hash.get("CAMPO_CLAVE") en el elmento hash de petici�n;
           Y se insertan como par�metros los valores correspondientes a esa claves en el
               mismo orden***** en que van en la Hash
           (Creo que usuario ha tenido que meter las claves  en orden alfab�tico???????????
               2) Se mete el par�metro del filtro (se recupera con hash.getFilter() en la hash de petici�n
               3) Se mete el par�metro de paguinaci�n (se recupera con param.getFilter() enlista de petici�n)
       */

      for (Enumeration e = parameter.keys(); e.hasMoreElements(); ) {
        clave = (String) e.nextElement();
        o = parameter.get(clave);
        // System_out.println("Dparametro " + clave + o+ "|");

        if (clave != null) {

          //Caso muy particular
          // para el rango de la fecha de nacimiento
          // pongo dos parametros uno para <= y otro para >=
          if (clave.equals("FC_NAC")) {
            if (o != null) {
              java.util.Date d = Fechas.string2Date(o.toString());
              java.util.Date dini = calculaRango(d, -30);
              st.setDate(iNumParam++, new java.sql.Date(dini.getTime()));

              java.util.Date dfin = calculaRango(d, 30);
              st.setDate(iNumParam++, new java.sql.Date(dfin.getTime()));
            }
            else {
              st.setNull(iNumParam++, java.sql.Types.DATE);
              st.setNull(iNumParam++, java.sql.Types.DATE);
            }
          }

          //Resto de claves
          else {

            if (o != null) {
              try {
                String str = (String) o; //???????????????????
                if (str.charAt(0) == '\'') {
                  // le quitamos las comillas
                  o = str.substring(1, str.length() - 1);
                }
                else if (str.trim().length() == 0) { //ponemos NULL
                  o = null;
                }
              }
              catch (Exception exc) {}

              if (o != null) {
                //st.setObject(iNumParam++, o);
                //Si el valor  es un String se pone setString
                if (o.getClass() == (new String()).getClass()) {
                  // System_out.println("*****Parametro de clave " + clave + " con valor  " + o + " en par�metro orden "+ iNumParam );
                  st.setString(iNumParam++, (String) o);
                }
                //Si el valor es un Date se pone setDate
                else if (o.getClass() == (new StringBuffer()).getClass()) {
                  java.util.Date d = Fechas.string2Date(o.toString());
                  st.setDate(iNumParam++, new java.sql.Date(d.getTime()));
                }
                //Si el valor es otro se pone setObject
                else {
                  st.setObject(iNumParam++, o);
                }
              }
              else {
                st.setNull(iNumParam++, java.sql.Types.VARCHAR);
              }

            }
            else { // o == null
              st.setNull(iNumParam++, java.sql.Types.VARCHAR);
            }
          }
        } //if (clave != null){
      } //for recorre la hash de petici�n

      // a�adimos le filtro, se lee del Data (en cada hashtable un filtro)
      String filtro = (String) parameter.getFiltro();
      if (filtro != null && filtro.trim().length() > 0) {
        if (is_like) {
          // System_out.println("Filtro***********" + filtro + "%" + "  en par�metro orden "+ iNumParam );
          st.setString(iNumParam++, "%" + filtro + "%");
        }
        else {
          // System_out.println("Filtro***********" + filtro + "  en par�metro orden "+ iNumParam );
          st.setString(iNumParam++, filtro);
        }
      }

      // a�adimos la paginaci�n,se lee de la CLista
      if (param.getFilter().length() > 0) {
        // System_out.println("Paginaci�n***********" + param.getFilter().trim() + "  en par�metro orden "+ iNumParam );
        st.setString(iNumParam++, param.getFilter().trim());
      }
    }
    catch (Exception exc) {
      // System_out.println("preparedFiltro" + exc.toString());
      strError = exc.toString();
    }
  }

  //_______Filtra con un c�digo. Trae descripci�n en castellano____________________________________________________
  /**
   *  esta funci�n ejecuta la sentencia select con el filtro dado
   *  y devuelve el dato que le han pedido
       * @param a_strSQL es la sen tencia SQL SELECT DS_MUN FROM SIVE_MUNICIPIO WHERE
       * @param a_code es la cadena que describe el campo por el que se filtra  CD_MUN
   * @param a_des es el campo que lleva la descripci�n DS_MUN
   * @param a_filter es el campo que lleva el dato para filtrar
   */
  public String traeDescripcion(String a_strSQL, String a_code, String a_des,
                                Object a_filter) {

    //descrip =  traeDescripcion("SELECT DSCOMU FROM SUCA_AUTONOMIAS WHERE ", "CDCOMU", "DSCOMU", codCA.toString());

    String result = " ";
    // si no hay filtro  no traemos nada
    if (a_filter == null) {
      return result;
    }

    try {
      st = con.prepareStatement(a_strSQL + a_code + " = ? ");
      // System_out.println(" ****TRAE DESC:  " +a_strSQL + a_code + " = ? ");
      st.setObject(1, a_filter);
      // System_out.println(" ****FILTRO: " + a_filter.toString());
      rs = st.executeQuery();
      if (rs.next()) {
        result = rs.getString(a_des);
      }
      rs.close();
      rs = null;
      st.close();
      st = null;
    }
    catch (Exception exc) {
      result = " ";
      exc.printStackTrace();
      //// System_out.println("Error Enfermo: fallamos en " + a_strSQL + a_code + " = ? " + a_filter);
    }

    return result;
  }

  //________________________________________________________________________
  //_______Filtra con un c�digo. Trae descripci�n en castellano o local____________________________________________________
  /**
   *  esta funci�n ejecuta la sentencia select con el filtro dado
   *  y devuelve el dato que le han pedido
       * @param a_strSQL es la sen tencia SQL SELECT DS_MUN FROM SIVE_MUNICIPIO WHERE
       * @param a_code es la cadena que describe el campo por el que se filtra  CD_MUN
   * @param a_des es el campo que lleva la descripci�n DS_MUN
   * @param a_desL es el campo que lleva la descripci�n en idioma
   * @param a_filter es el campo que lleva el dato para filtrar
   * idioma es el entero de la aplicaci�n que indica idioma
   * M�todo parecido al anterior pero recupera DSL y elige si debe devolver DS o DSL
   * seg�n tengamos el idioma por defecto o el anterior
   */
  public String traeDescripcionEnIdioma(String a_strSQL, String a_code,
                                        String a_des, String a_desL,
                                        Object a_filter, int idioma) {
    //Ejemplo uso:
    //descrip =  traeDescripcion("SELECT DS_PROV,DSLPROV FROM SIVE_PROVINCIA WHERE ", "CD_PROV", "DS_PROV", "DSL_PROV",codPro.toString(),1);
    String sDesL = "";
    String result = " ";
    // si no hay filtro  no traemos nada
    if (a_filter == null) {
      return result;
    }

    try {
      st = con.prepareStatement(a_strSQL + a_code + " = ? ");
      st.setObject(1, a_filter);
      rs = st.executeQuery();
      if (rs.next()) {
        result = rs.getString(a_des);
        sDesL = rs.getString(a_desL);
        //Si idioma no es el local y hay desc local se devuelve la desc local
        if ( (idioma != CApp.idiomaPORDEFECTO) && (sDesL != null)) {
          result = sDesL;
        }
      }
      rs.close();
      rs = null;
      st.close();
      st = null;
    }
    catch (Exception exc) {
      result = " ";
      exc.printStackTrace();
      //// System_out.println("Error Enfermo: fallamos en " + a_strSQL + a_code + " = ? " + a_filter);
    }

    return result;
  }

  //_______Filtra con dos c�digos. Trae descripci�n en castellano____________________________________________________

  public String traeDescripcion(String a_strSQL, String a_code, String a_des,
                                Object a_filter, String a_code2,
                                String a_filter2) {
    String result = " ";
    // si no hay filtro  no traemos nada
    if (a_code == null || a_code2 == null) {
      return result;
    }

    try {
      st = con.prepareStatement(a_strSQL + a_code + " = ? AND " + a_code2 +
                                " = ? ");
      if (a_filter != null) {
        st.setObject(1, a_filter);
      }
      else {
        st.setNull(1, java.sql.Types.VARCHAR);
      }

      if (a_filter2 != null) {
        st.setObject(2, a_filter2);
      }
      else {
        st.setNull(2, java.sql.Types.VARCHAR);
      }

      rs = st.executeQuery();
      if (rs.next()) {
        result = rs.getString(a_des);
      }
      rs.close();
      rs = null;
      st.close();
      st = null;
    }
    catch (Exception exc) {
      result = " ";
      exc.printStackTrace();
      //// System_out.println("Error Enfermo: fallamos en " + a_strSQL+ a_code + " = ? AND " + a_code2 + " = ? ");
    }

    return result;
  }

  //_______Filtra con dos c�digos. Trae descripci�n en castellano o local____________________________________________________

  public String traeDescripcionEnIdioma(String a_strSQL, String a_code,
                                        String a_des, String a_desL,
                                        Object a_filter, String a_code2,
                                        String a_filter2, int idioma) {
    String result = " ";
    String sDesL = "";
    // si no hay filtro  no traemos nada
    if (a_code == null || a_code2 == null) {
      return result;
    }

    try {
      st = con.prepareStatement(a_strSQL + a_code + " = ? AND " + a_code2 +
                                " = ? ");
      if (a_filter != null) {
        st.setObject(1, a_filter);
      }
      else {
        st.setNull(1, java.sql.Types.VARCHAR);
      }

      if (a_filter2 != null) {
        st.setObject(2, a_filter2);
      }
      else {
        st.setNull(2, java.sql.Types.VARCHAR);
      }

      rs = st.executeQuery();
      if (rs.next()) {
        result = rs.getString(a_des);
        sDesL = rs.getString(a_desL);
        //Si idioma no es el local y hay desc local se devuelve la desc local
        if ( (idioma != CApp.idiomaPORDEFECTO) && (sDesL != null)) {
          result = sDesL;
        }
      }
      rs.close();
      rs = null;
      st.close();
      st = null;
    }
    catch (Exception exc) {
      result = " ";
      exc.printStackTrace();
      //// System_out.println("Error Enfermo: fallamos en " + a_strSQL+ a_code + " = ? AND " + a_code2 + " = ? ");
    }

    return result;
  }

} //________________________________________________ END_CLASS
