
package enfermo;

import java.awt.TextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class CampoNIF
    extends TextField
    implements KeyListener {

  public CampoNIF() {
    super();
    addKeyListener(this);
  }

  public void keyReleased(KeyEvent e) {

    String cadena = getText().toUpperCase();
    int valorASCII = 0;

    if (cadena.length() > 0) {

      if (e.getKeyChar() == '�') {
        e.setKeyChar('�');
      }
      else if (e.getKeyChar() == '�') {
        e.setKeyChar('�');
      }
      else if (e.getKeyChar() == '�') {
        e.setKeyChar('�');
      }
      else if (e.getKeyChar() == '�') {
        e.setKeyChar('�');
      }
      else if (e.getKeyChar() == '�') {
        e.setKeyChar('�');
      }
      else if (e.getKeyChar() == '�') {
        e.setKeyChar('�');
      }
      else if (e.getKeyChar() == '�') {
        e.setKeyChar('�');
      }
      else if ( (e.getKeyChar() < '0') ||
               (e.getKeyChar() > '9' && e.getKeyChar() < 'A') ||
               (e.getKeyChar() > 'Z' && e.getKeyChar() < 'a') ||
               (e.getKeyChar() > 'z')) {
        // caracteres no permitidos
        cadena = cadena.substring(0, cadena.length() - 1);
      }
    }

    setText(cadena);
    setCaretPosition(cadena.length());
  }

  public void keyPressed(KeyEvent e) {
  }

  public void keyTyped(KeyEvent e) {
  }

} // END CLASS
