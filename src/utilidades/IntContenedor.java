
package utilidades;

import capp.CApp;

public interface IntContenedor {

  //Para poner al contenedor en espera .
  //Devuelve el modo de pantalla anterior
  public int ponerEnEspera();

  //Para poner el contenedor en un modo (normalmente recuperar modo anterior)
  public void ponerModo(int modo);

  public CApp getMiCApp();

}
