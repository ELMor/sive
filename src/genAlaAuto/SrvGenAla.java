//Title:        Generacion de alarmas automaticas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Al inicio del a�o epidemiologico, o por peticion del administrador
//se generaran/actualizaran las alarmas automaticas de cada
//enfermedad a nivel de Area y a nivel de la Comunidad.

package genAlaAuto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Vector;

import capp.CLista;
import capp.CMessage;
import jdbcpool.JDCConnectionDriver;
import sapp.DBServlet;

public class SrvGenAla
    extends DBServlet {

  //modos de consulta al servlet
  final int servletGEN_AL_AUTO = 2;
  final int servletGEN_VEC_ENF = 8;
  final int servletADV_GEN_AL = 9;

  final int tipoCOMUNIDAD = 3;
  final int tipoNIVEL1 = 4;
  final int tipoNIVEL2 = 5;

  final int tipoMEDIA = 6;
  final int tipoNO_MEDIA = 7;

  final String ERROR_NO_DATOS =
      "No existen datos de los a�os anteriores para calcular los estad�sticos";
  final String ERROR_NO_ANO = "No es posible calcular los estad�sticos para ese a�o porque no esta generado en el sistema";

  final String R = "-"; //Car�cter de relleno para c�digos de nivel 1 o nivel 2 que no lleguen a su valor m�ximo

  SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

  // objetos JDBC
  Connection con = null;
  PreparedStatement st = null;
  ResultSet rs = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    CMessage msgBox;
    boolean noHayReg = false;
    boolean noHayAno = false;
    boolean hayAlarmas = false;

    int anteriores = -1;
    int anterioresmediana = -1;

    int iSemepi = -1;

    // objetos de datos
    CLista data = null; //lista de resultado al cliente (lineas, es decir, Strings)
    ParGenAlaAuto parGen = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    parGen = (ParGenAlaAuto) param.firstElement();

    String cd_indalar = new String();
    String cd_izquier = new String();
    String cd_secuen[] = new String[3];
    String cd_nivel1 = new String();
    String cd_nivel2 = new String();
    String ds_indalar = new String();
    String cd_enfcie = new String();
    String it_activo = new String();
    String it_tipo = new String();

    String cd_anoepi = parGen.anoEpi;
    String cd_semepi = new String();
    Double nm_valor = null;
    Double nm_desv = null;

    Vector izquierdas = new Vector();
    Vector secIzq = new Vector();

    Vector registrosSD = new Vector();
    Vector enfermedad = new Vector();

    try {

      // modos de operaci�n
      switch (opmode) {

        //________________________________________________________
        //__________________________________________________________

        case servletADV_GEN_AL:

          String queryCom = "select count(*) from sive_ind_ano "
              + " where cd_anoepi = ? and "
              + " cd_indalar like ? ";
          st = con.prepareStatement(queryCom);
          st.setString(1, parGen.anoEpi);
          switch (parGen.tipo) {

            //Se buscan alarmas de comunidad
            case tipoCOMUNIDAD:
              st.setString(2, "CC" + R + R + "A%");
////# // System_out.println("&& Adv Comunidad");
              break;

              //Se buscan alarmas de Niv1
            case tipoNIVEL1:

              //Si Cod NIV1 ocupa dos carateres va directamente al c�digo de indicador
              if (parGen.sCodNiv1.length() == 2) {
                st.setString(2, parGen.sCodNiv1 + R + R + "A%");
                //Si Cod NIV1 ocupa un carater se a�ade adem�s un subrayado al c�digo de indicador
              }
              else {
                st.setString(2, R + parGen.sCodNiv1 + R + R + "A%");
////# // System_out.println("&& Adv N1"+parGen.sCodNiv1);
              }
              break;

              //Se buscan alarmas de Niv2
            case tipoNIVEL2:
              String codigo = "";
              //Si Cod NIV1 ocupa dos carateres va directamente al c�digo de indicador
              if (parGen.sCodNiv1.length() == 2) {
                codigo = parGen.sCodNiv1;
                //Si Cod NIV1 ocupa un carater se a�ade adem�s un subrayado al c�digo de indicador
              }
              else {
                codigo = R + parGen.sCodNiv1;

                //Si Cod NIV2 ocupa dos carateres va directamente al c�digo de indicador
              }
              if (parGen.sCodNiv2.length() == 2) {
                codigo = codigo + parGen.sCodNiv2;
                //Si Cod NIV2 ocupa un carater se a�ade adem�s un subrayado al c�digo de indicador
              }
              else {
                codigo = codigo + R + parGen.sCodNiv2;
////# // System_out.println("&& Adv N2"+parGen.sCodNiv1+" "+ parGen.sCodNiv2);
              }
              st.setString(2, codigo + "A%");
              break;
          }
          rs = st.executeQuery();
          if (rs.next()) {
            int hay = rs.getInt(1);
            if (hay > 0) {
              hayAlarmas = true;
            }
          }
          st.close();
          st = null;

          data = new CLista();
          data.addElement(new Boolean(hayAlarmas));

          if (data != null) {
            data.trimToSize();

            // cierra la conexion
          }
          closeConnection(con);
          break;

          //__________________________________________________________
          //__________________________________________________________

        case servletGEN_VEC_ENF:

          //Comprobacion de que el a�o se ha generado en sive_semana_epi
          String queryAno = "select count(*) from SIVE_SEMANA_EPI "
              + " where cd_ANOEPI = ? ";
          st = con.prepareStatement(queryAno);
          st.setString(1, parGen.anoEpi);
          rs = st.executeQuery();
          if (rs.next()) {
            int sems = rs.getInt(1);
            if ( (sems != 52) && (sems != 53)) {
              noHayAno = true;
            }
          }
          st.close();
          st = null;

          Integer ano = new Integer(parGen.anoEpi);
          Integer anoI = new Integer(ano.intValue() - 5);
          Integer anoF = new Integer(ano.intValue() - 1);

          if (!noHayAno) {
//# // System_out.println("&&  Hay a�o");
            switch (parGen.tipo) {
              case tipoCOMUNIDAD:
                String queryComSD =
                    "select sum(NM_CASOS), CD_ENFCIE, CD_SEMEPI, CD_ANOEPI "
                    + " from SIVE_RESUMEN_EDOS "
                    + " where CD_ANOEPI <= ? and CD_ANOEPI >= ? and "
                    + " CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where IT_BAJA = 'N' and (CD_TVIGI = 'A' or CD_TVIGI = 'I')) "
                    + " group by CD_ENFCIE, CD_SEMEPI, CD_ANOEPI "
                    + " order by CD_ENFCIE, CD_SEMEPI, CD_ANOEPI";

                registrosSD = calcularVectorOrdenadoCasos(tipoCOMUNIDAD,
                    queryComSD, anoI, anoF, "", "");

                // Compruebo si hay registros para calcular los estadisticos
                // Es suficiente con que haya datos de un ano anterior
                if (registrosSD.size() == 0) {
                  noHayReg = true;
//                  //# // System_out.println("SrvGenAla no hay registros para comunidad");
                }

////# // System_out.println("&& Vect com");
                break;

              case tipoNIVEL1:

                queryComSD =
                    "select sum(NM_CASOS), CD_ENFCIE, CD_SEMEPI, CD_NIVEL_1, CD_ANOEPI  "
                    + " from SIVE_RESUMEN_EDOS "
                    + " where CD_ANOEPI <= ? and CD_ANOEPI >= ? and "
                    + " CD_NIVEL_1 = ? and " //A�adido
                    + " CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where IT_BAJA = 'N' and (CD_TVIGI = 'A' or CD_TVIGI = 'I') ) "
                    + " group by  CD_ENFCIE, CD_SEMEPI, CD_NIVEL_1,CD_ANOEPI " //Cambiado sin N1
                    + " order by CD_ENFCIE, CD_SEMEPI, CD_ANOEPI";

                registrosSD = calcularVectorOrdenadoCasos(tipoNIVEL1,
                    queryComSD, anoI, anoF, parGen.sCodNiv1, "");

                // Compruebo si hay registros para calcular los estadisticos
                // Es suficiente con que haya datos de un ano anterior
                if (registrosSD.size() == 0) {
                  noHayReg = true;
//                  //# // System_out.println("SrvGenAla no hay registros para nivel1");
                }

////# // System_out.println("&& Vect N1"+parGen.sCodNiv1);
                break;

              case tipoNIVEL2:
                queryComSD =
                    "select sum(NM_CASOS), CD_ENFCIE, CD_SEMEPI, CD_NIVEL_1, CD_NIVEL_2 CD_ANOEPI  "
                    + " from SIVE_RESUMEN_EDOS "
                    + " where CD_ANOEPI <= ? and CD_ANOEPI >= ? and "
                    + " CD_NIVEL_1 = ? and " //A�adido
                    + " CD_NIVEL_2 = ? and " //A�adido
                    + " CD_ENFCIE in (select CD_ENFCIE from SIVE_ENFEREDO where IT_BAJA = 'N' and (CD_TVIGI = 'A' or CD_TVIGI = 'I')) "
                    +
                    " group by  CD_ENFCIE, CD_SEMEPI, CD_ANOEPI,CD_NIVEL_1, CD_NIVEL_2 "
                    + " order by CD_ENFCIE, CD_SEMEPI, CD_ANOEPI";

                registrosSD = calcularVectorOrdenadoCasos(tipoNIVEL2,
                    queryComSD, anoI, anoF, parGen.sCodNiv1, parGen.sCodNiv2);
                ////# // System_out.println("SrvGenAla registrosSD calculado para la comunidad" );

                // Compruebo si hay registros para calcular los estadisticos
                // Es suficiente con que haya datos de un ano anterior
                if (registrosSD.size() == 0) {
                  noHayReg = true;
                  //# // System_out.println("&&&&&& No hay reg");
//                  //# // System_out.println("SrvGenAla no hay registros para nivel1");
                }

//                //# // System_out.println("&& Vect N2"+parGen.sCodNiv1+" "+ parGen.sCodNiv2);
                break;
            }
          }
          else { //no hay a�o
//# // System_out.println("&& No hay a�o");
            data = new CLista();
            data.addElement(new Boolean(true));
            data.addElement(new String(ERROR_NO_ANO));
//            //# // System_out.println("SrvGenAla no hay ano generado ");
          }

          if (noHayReg) {
//# // System_out.println("&& No hay registros");
            data = new CLista();
            data.addElement(new Boolean(true));
            data.addElement(new String(ERROR_NO_DATOS));
          }

          if (! (noHayAno) && ! (noHayReg)) {
//# // System_out.println("&&  hay a�o y reg");
            data = new CLista();
            data.addElement(new Boolean(false));
            data.addElement(registrosSD);
          }

          if (data != null) {
            data.trimToSize();

            // cierra la conexion
          }
          closeConnection(con);
////# // System_out.println("&& FIN VEct");
          break;
          //_____________________________

          //__________________________________________________________

        case servletGEN_AL_AUTO:

          String queryB = "";
          String queryI = "";
          String codigo = ""; //Auxiliar
          // NOTAS PARA LA GENERACION AUTOMATICA DE ALARMAS:
          //  - para generar las alarmas de un a�o debe estar generado
          //    antes en sive_ano_epi, y en sive_semana_epi
          //  - para generar las alarmas de un a�o es suficiente con que
          //    haya datos de alguno de los cinco a�os anteriores al que
          //    se esta generando
          //  - en la tabla sive_resumen_edos puede faltar alguna semana
          //    en algun a�o
          //  - los estadisticos se generan con los datos que haya para
          //    una semana de como maximo 5 a�os anteriores, por lo tanto
          //    en un a�o los estadisticos de una semana pueden estar
          //    calculados con 5 a�os anteriores, los de otra semana
          //    con 3, ...
          //  - si para una semana no hay ningun dato de a�os anteriores
          //    los estadisticos son 0
          //  - se genera un indicador para media, otro para mediana y
          //    otro para maximo para un a�o, para cada enfermedad,
          //    ademas de las variaciones de nivel1 y nivel2

          enfermedad = parGen.enfermedad;
          cd_enfcie = (String) enfermedad.elementAt(0);
          ////# // System_out.println("SrvGenAla: enfermedad " + cd_enfcie
          //                   + " tipo " + parGen.tipo);
          ////# // System_out.println("SrvGenAla: Borrado de las alarmas de esa endfermedad en ese a�o");

          int indSem = 0;
          Vector semana = new Vector();
          Vector casos = new Vector();
          Vector casosmediana = new Vector();
          int semanas = 0;
          boolean insertarIndicadores = true;

          Vector semProc = new Vector();

          switch (parGen.tipo) {

            case tipoCOMUNIDAD:

              //Borrado de las alarmas de esa endfermedad en ese a�o
              queryB = " delete from SIVE_ALARMA "
                  + " where CD_ANOEPI = ? and "
                  + " CD_INDALAR like ?  ";
              st = con.prepareStatement(queryB);
              st.setString(1, parGen.anoEpi);
              st.setString(2,
                           calculaIzquier(tipoCOMUNIDAD, cd_enfcie, "", "") + "%");
              st.executeUpdate();
              st.close();
              st = null;

              // Borrado de la tabla ind_ano de esa enfermedad  y en ese a�o
              queryI = " delete from SIVE_IND_ANO "
                  + " where CD_ANOEPI = ? and "
                  + " CD_INDALAR like ?  ";
              st = con.prepareStatement(queryI);
              st.setString(1, parGen.anoEpi);
              st.setString(2,
                           calculaIzquier(tipoCOMUNIDAD, cd_enfcie, "", "") + "%");
              st.executeUpdate();
              st.close();
              st = null;

////# // System_out.println("&& Del Com");
              break;

            case tipoNIVEL1:

              cd_nivel1 = parGen.sCodNiv1;

              //Borrado de las alarmas de esa endfermedad en ese a�o
              queryB = " delete from SIVE_ALARMA "
                  + " where CD_ANOEPI = ? and "
                  + " CD_INDALAR like ?  ";
              st = con.prepareStatement(queryB);
              st.setString(1, parGen.anoEpi);

              st.setString(2,
                           calculaIzquier(tipoNIVEL1, cd_enfcie, cd_nivel1, "") +
                           "%");

              st.executeUpdate();
              st.close();
              st = null;
////# // System_out.println("&& Del N1"+parGen.sCodNiv1);
              break;

            case tipoNIVEL2:

              cd_nivel1 = parGen.sCodNiv1;
              cd_nivel2 = parGen.sCodNiv2;

              //Borrado de las alarmas de esa endfermedad en ese a�o
              queryB = " delete from SIVE_ALARMA "
                  + " where CD_ANOEPI = ? and "
                  + " CD_INDALAR like ?  ";
              st = con.prepareStatement(queryB);
              st.setString(1, parGen.anoEpi);

              /*
                            //Si Cod NIV1 ocupa dos carateres va directamente al c�digo de indicador
                            if (parGen.sCodNiv1.length()==2)
                              codigo =   parGen.sCodNiv1;
                            //Si Cod NIV1 ocupa un carater se a�ade adem�s un subrayado al c�digo de indicador
                            else
                              codigo =  R + parGen.sCodNiv1;
                            //Si Cod NIV2 ocupa dos carateres va directamente al c�digo de indicador
                            if (parGen.sCodNiv2.length()==2)
                              codigo = codigo +  parGen.sCodNiv2;
                            //Si Cod NIV2 ocupa un carater se a�ade adem�s un subrayado al c�digo de indicador
                            else
                              codigo =  codigo + R + parGen.sCodNiv2;
                            if (cd_enfcie.length()==5)
                              st.setString(2, codigo + "A_" + cd_enfcie + "%");
                            else
                              st.setString(2, codigo + "A" + cd_enfcie + "%");
               */
              st.setString(2,
                           calculaIzquier(tipoNIVEL2, cd_enfcie, cd_nivel1, cd_nivel2) +
                           "%");

              st.executeUpdate();
              st.close();
              st = null;
////# // System_out.println("&& Del N2"+parGen.sCodNiv1+" "+ parGen.sCodNiv2);
              break;

          } //Switch

////# // System_out.println("&& FIN DEL ");

          // Empiezo a procesar las semanas de la enfermedad
          semanas = enfermedad.size();
          indSem = 1;

          // Para cada enfermedad se generan tres indicadores
          // para la media, para la mediana y para el max
          cd_izquier = calculaIzquier(parGen.tipo,
                                      cd_enfcie,
                                      cd_nivel1,
                                      cd_nivel2);
          // Media
          cd_secuen[0] = "001";
          cd_indalar = cd_izquier + cd_secuen[0];
          ds_indalar = "Media " + cd_enfcie;
          it_activo = "S";
          it_tipo = "A";
          insertarIndicador(parGen.tipo, con,
                            cd_indalar, ds_indalar, cd_enfcie,
                            it_activo, it_tipo, cd_izquier, cd_secuen[0],
                            cd_nivel1, cd_nivel2);
          insertarIndAno(con, cd_indalar, parGen.anoEpi, new Double(1.25));
          // Mediana
          cd_secuen[1] = "002";
          cd_indalar = cd_izquier + cd_secuen[1];
          ds_indalar = "Mediana " + cd_enfcie;
          it_activo = "S";
          it_tipo = "A";
          insertarIndicador(parGen.tipo, con,
                            cd_indalar, ds_indalar, cd_enfcie,
                            it_activo, it_tipo, cd_izquier, cd_secuen[1],
                            cd_nivel1, cd_nivel2);
          insertarIndAno(con, cd_indalar, parGen.anoEpi, new Double(1.25));
          // Maximo
          cd_secuen[2] = "003";
          cd_indalar = cd_izquier + cd_secuen[2];
          ds_indalar = "Maximo " + cd_enfcie;
          it_activo = "S";
          it_tipo = "A";
          insertarIndicador(parGen.tipo, con,
                            cd_indalar, ds_indalar, cd_enfcie,
                            it_activo, it_tipo, cd_izquier, cd_secuen[2],
                            cd_nivel1, cd_nivel2);
          insertarIndAno(con, cd_indalar, parGen.anoEpi, new Double(1));

          // determina el n� de semanas del a�o que va a generar
//          st = con.prepareStatement("select COUNT(CD_ANOEPI) from SIVE_SEMANA_EPI where CD_ANOEPI = ?");
          st = con.prepareStatement(
              "select COUNT(CD_SEMEPI) from SIVE_SEMANA_EPI where CD_ANOEPI = ?");
          st.setString(1, parGen.anoEpi);
          rs = st.executeQuery();
          if (rs.next()) {
            semanas = rs.getInt(1);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

//          while (indSem<semanas) {
          while (indSem < enfermedad.size()) {

            semana = (Vector) enfermedad.elementAt(indSem);
            indSem++;
            cd_semepi = (String) semana.elementAt(0);

            // no insertamos valores cuando para un a�o
            // con 52 semanas, vengan valores para la
            // semana 53
            iSemepi = (new Integer(cd_semepi)).intValue();
            if (iSemepi > semanas) {
              break;
            }
            semProc.addElement(cd_semepi);
            ////# // System_out.println("SrvGenAla: comunidad " + cd_enfcie + cd_semepi );

            // Se obtienen los registros que corresponden a esa
            // enfermedad y esa semana
            casos = (Vector) semana.elementAt(1);
            anteriores = casos.size();

            // modificacion jlt 23/10/2001
            // los c�lculos se hacen teniendo en cuenta los casos a cero
            // o que no vengan
            casosmediana = (Vector) calcularVector(casos);
            anterioresmediana = casosmediana.size();

            //if (anteriores==0){
            if (anterioresmediana == 0) {
              // si falta una semana pongo los estadisticos a 0
              //continue;
              nm_valor = new Double(0);
              nm_desv = new Double(0);
            }
            else {
              Long sum = new Long(0);
              String auxS = new String();
              Long auxL = new Long(0);
              //for (int s=0; s<anteriores; s++) {
              for (int s = 0; s < anterioresmediana; s++) {
                //auxS = (String) casos.elementAt(s);
                auxS = (String) casosmediana.elementAt(s);
                auxL = new Long(auxS);
                sum = new Long(sum.longValue() + auxL.longValue());
              }
              //Integer iAnt = new Integer(anteriores);
              Integer iAnt = new Integer(anterioresmediana);
              //nm_valor = new Double (sum.doubleValue()/iAnt.doubleValue());
              nm_valor = new Double(sum.doubleValue() / 5);

              // se calcula la desviacion estandar
              double dAux = 0;
              // sumatorio
              //for (int i=0; i<anteriores; i++)
              for (int i = 0; i < anterioresmediana; i++) {

                /*dAux = dAux +
                     Math.pow((new Double((String) casos.elementAt(i))).doubleValue()
                               - nm_valor.doubleValue(), 2);
                                 dAux = dAux/anteriores;*/
                dAux = dAux +
                    Math.pow( (new Double( (String) casosmediana.elementAt(i))).
                             doubleValue()
                             - nm_valor.doubleValue(), 2);
              }
              dAux = dAux / anterioresmediana;
              dAux = Math.sqrt(dAux);
              nm_desv = new Double(dAux);
            }
            insertarAlarma(tipoMEDIA, con,
                           parGen.anoEpi, cd_izquier + cd_secuen[0],
                           cd_semepi, nm_valor, nm_desv);
            //Para la mediana
            //if (anteriores==0){
            if (anterioresmediana == 0) {
              // si falta una semana pongo los estadisticos a 0
              //continue;
              nm_valor = new Double(0);
              nm_desv = new Double(0);

            }
            else {

              //if ((anteriores%2)!=0)
              if ( (anterioresmediana % 2) != 0) {

                // Las restas de 1 son porque el vector empieza en 0
                //nm_valor = new Double((String) casos.elementAt((anteriores+1)/2 -1));
                nm_valor = new Double( (String) casosmediana.elementAt( (
                    anterioresmediana + 1) / 2 - 1));
              }
              else {
                //nm_valor = new Double((String) casos.elementAt(anteriores/2-1));
                //Double aux = new Double((String) casos.elementAt(anteriores/2 +1-1));
                nm_valor = new Double( (String) casosmediana.elementAt(
                    anterioresmediana / 2 - 1));
                Double aux = new Double( (String) casosmediana.elementAt(
                    anterioresmediana / 2 + 1 - 1));
                nm_valor = new Double( (nm_valor.doubleValue() +
                                        aux.doubleValue()) / 2);
              }
            }
            // Inserto la alarma de mediana
            insertarAlarma(tipoNO_MEDIA, con,
                           parGen.anoEpi, cd_izquier + cd_secuen[1],
                           cd_semepi, nm_valor, nm_desv);
            // Para el maximo
            //if (anteriores==0){
            if (anterioresmediana == 0) {
              // si falta una semana pongo los estadisticos a 0
              //continue;
              nm_valor = new Double(0);
            }
            else {
              //nm_valor = calcularMaximo(casos);
              nm_valor = calcularMaximo(casosmediana);
            }
            // Inserto la alarma de maximo
            insertarAlarma(tipoNO_MEDIA, con,
                           parGen.anoEpi, cd_izquier + cd_secuen[2],
                           cd_semepi, nm_valor, nm_desv);
          } //semanas

          // Para las semanas que faltan se meten ceros
          String sSem = new String();
          Double cero = new Double(0);

          //for (int j=1; j<53; j++) {
          for (int j = 1; j <= semanas; j++) {
            Integer iSem = new Integer(j);
            sSem = iSem.toString();
            if (sSem.length() == 1) {
              sSem = '0' + sSem;
            }
            if (!semProc.contains(sSem)) {
              // Inserto la alarma de media
              insertarAlarma(tipoMEDIA, con,
                             parGen.anoEpi, cd_izquier + cd_secuen[0],
                             sSem, cero, cero);
              // Inserto la alarma de mediana
              insertarAlarma(tipoNO_MEDIA, con,
                             parGen.anoEpi, cd_izquier + cd_secuen[1],
                             sSem, cero, cero);
              // Inserto la alarma de maximo
              insertarAlarma(tipoNO_MEDIA, con,
                             parGen.anoEpi, cd_izquier + cd_secuen[2],
                             sSem, cero, cero);
            }
          }

          // valida la transacci�n
          con.commit();

          // cierra la conexion
          closeConnection(con);

          break;
      } //fin switch

      //fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      ////# // System_out.println("srvGenAla: despues de rollback excepcion " + cd_enfcie +
      //                   " " + cd_semepi + ex.getMessage());
      ex.printStackTrace();

      throw ex;
    }

    return data;
  } // doWork

  //__________________________________________________________
  //__________________________________________________________

  String calculaIzquier(int tipo, String cd_enfcie,
                        String cd_nivel1, String cd_nivel2) {
    String cd_izquier = new String();
    switch (tipo) {
      case tipoCOMUNIDAD:
        cd_izquier = "CC" + R + R + "A";
        for (int i = 0; i < 6 - cd_enfcie.length(); i++) {
          cd_izquier += R;
        }
        cd_izquier += cd_enfcie;
        break;
      case tipoNIVEL1:
        if (cd_nivel1.length() == 1) {
          cd_izquier = R + cd_nivel1 + R + R + "A";
        }
        else {
          cd_izquier = cd_nivel1 + R + R + "A";

        }
        for (int i = 0; i < 6 - cd_enfcie.length(); i++) {
          cd_izquier += R;
        }
        cd_izquier += cd_enfcie;
        break;
      case tipoNIVEL2:
        if (cd_nivel1.length() == 1) {
          cd_izquier = R + cd_nivel1;
        }
        else {
          cd_izquier = cd_nivel1;

        }
        if (cd_nivel2.length() == 1) {
          cd_izquier = cd_izquier + R + cd_nivel2 + "A";
        }
        else {
          cd_izquier = cd_izquier + cd_nivel2 + "A";

        }
        for (int i = 0; i < 6 - cd_enfcie.length(); i++) {
          cd_izquier += R;
        }
        cd_izquier += cd_enfcie;
        break;
    }
    return cd_izquier;
  }

  Double calcularMaximo(Vector casos) {
    Long max = new Long(0);
    Long aux = new Long(0);
    for (int i = 0; i < casos.size(); i++) {
      aux = new Long( (String) casos.elementAt(i));
      if (aux.longValue() > max.longValue()) {
        max = aux;
      }
    }
    return new Double(max.toString());
  }

  Vector calcularVector(Vector casos) {

    Vector casos2 = new Vector();
    Vector casos3 = new Vector();
    Double valor = null;
    valor = new Double(0);
    Integer cero = new Integer(0);
    int max = 0;
    int dif = 0;
    casos2 = (Vector) casos.clone();
    for (int i = 0; i < casos.size(); i++) {

      max = calcularMaximo2(casos2);
      casos3.addElement( (String) casos2.elementAt(max));
      casos2.removeElementAt(max);
    }

    dif = 5 - casos3.size();
    for (int j = 0; j < dif; j++) {
      casos3.addElement(cero.toString());
    }

    return casos3;
  }

  int calcularMaximo2(Vector casos) {
    Long max = new Long(0);
    Long aux = new Long(0);
    int imax = 0;
    for (int i = 0; i < casos.size(); i++) {
      aux = new Long( (String) casos.elementAt(i));
      if (aux.longValue() > max.longValue()) {
        max = aux;
        imax = i;
      }
    }
    return imax;

  }

  void imprimirVector(Vector v) {
    Vector aux = new Vector();
//    //# // System_out.println("( ");
    for (int s = 0; s < v.size(); s++) {
      try {
        aux = (Vector) v.elementAt(s);
        imprimirVector(aux);
        ////# // System_out.println("");
      }
      catch (ClassCastException ex) {
//        // System_out.print(" " + (String) v.elementAt(s) + " ");
      }
    }
//    //# // System_out.println(") ");
  }

  String calculaSecuen(String izq) throws Exception {

    String sec = new String();
    String query = "select max(cd_secuen) from sive_indicador "
        + " where cd_izquier = ? ";

    st = con.prepareStatement(query);
    st.setString(1, izq);
    rs = st.executeQuery();
    if (rs.next()) {
      if (rs == null) {
        sec = "001";
      }
      else {
        Integer iSec = new Integer(rs.getInt(1) + 1);
        sec = iSec.toString();
        int lsecuen = sec.length();
        for (int i = 0; i < 3 - lsecuen; i++) {
          sec = "0" + sec;
        }
      }
    }
    return sec;
  }

  Vector calcularVectorOrdenadoCasos(int tipo, String query,
                                     Integer aI, Integer aF,
                                     String cod1, String cod2) throws Exception { //C�digos a�adidos *******
    boolean camEnf = false;
    boolean hayReg = false;

    st = con.prepareStatement(query);
    st.setString(2, aI.toString());
    st.setString(1, aF.toString());
    //A�ade par�metros para buscar el niv 1 y niv2 si es necesario
    switch (tipo) {
      case tipoNIVEL1:
        st.setString(3, cod1);
        break;
      case tipoNIVEL2:
        st.setString(3, cod1);
        st.setString(4, cod2);
        break;
    }

    rs = st.executeQuery();

    Vector regsSD = new Vector(); // {enfer1, ... , enferN} Comunidad
    Vector enfer = new Vector(); // {enfer, sem1, sem2, ... ,sem52}
    Vector sem = new Vector(); // {semana, vector de casos}
    // {semana, n11, ... , n1N}
    Vector numCas = new Vector(); // {caso1,...,cason}

    String antEnf = new String();
    String antSem = new String();

    //NM_CASOS, CD_ENFCIE, CD_SEMEPI, CD_NIVEL_1, CD_NIVEL_2 CD_ANOEPI
    while (rs.next()) {

      hayReg = true;
//# // System_out.println("&&&&& Entra en Hay Reg**********");
      String reg[] = new String[5];
      reg[0] = rs.getString(1); //NM_CASOS
      reg[1] = rs.getString(2); //CD_ENFCIE
////# // System_out.println("&& Enfermedad***********************" + reg[1]);
      reg[2] = rs.getString(3); //CD_SEMEPI

      //Primera enfermedad : Se a�ade un registro de enfermedad
      if (antEnf.equals(new String())) {
        antEnf = reg[1]; //Se guarda dato de enfermedad para ver si lugo ha cambiado la enfermedad
        enfer.addElement(reg[1]);
      }

      //Enfermedades que no son la primera ***********
      //S�lo se a�ade un objetoa al vector enfer si realmente cambia la enfermedad
      else
      if (!antEnf.equals(reg[1])) {
        if (!camEnf) {
          camEnf = true;
          //cambio de enfermedad
          // por tanto tambien hay cambio de semana
        }
        sem.addElement(numCas); //A�ade el �ltimo numcas a vector que contiene semana y sus casos
        enfer.addElement(sem); // a�ado la ultima semana a vector de enfermedad y sus semanas
        regsSD.addElement(enfer); //A�ado �ltima enf a vector que contiemne enfemedades

        enfer = new Vector();
        enfer.addElement(reg[1]); //Se a�ade una enfermedad ultima recogida
        antEnf = reg[1]; //Marca como �ltima enfermdad la recogida ya
        antSem = new String();
        sem = new Vector();
        numCas = new Vector();
      }

      if (antSem.equals(new String())) {
        // Primera semana de la enfermedad
        antSem = reg[2];
        sem = new Vector();
        sem.addElement(reg[2]);
      }
      else // Veo si hay un cambio de semana
      if (!antSem.equals(reg[2])) {
        //cambio de semana
        sem.addElement(numCas);
        enfer.addElement(sem);
        antSem = reg[2];
        sem = new Vector();
        sem.addElement(reg[2]);
        numCas = new Vector();
      }

      // Se a�ade el numero de casos
      numCas.addElement(reg[0]);

    } //While
    rs.close();
    st.close();
    rs = null;
    st = null;

    if (hayReg == true) {
//# // System_out.println("&& A�ade registro ala salir");
      sem.addElement(numCas);
      enfer.addElement(sem); // a�ado la ultima semana
      regsSD.addElement(enfer);
      //# // System_out.println("&& Tam  regsd " + regsSD.size() );
    }
    return regsSD;
  }

  void insertarIndicador(int tipo, Connection con,
                         String cd_ind, String ds_ind, String cd_enf,
                         String it_ac, String it_ti, String cd_iz, String cd_se,
                         String cd_n1, String cd_n2) throws Exception {

    String insert = "insert into SIVE_INDICADOR "
        + "(CD_INDALAR, DS_INDALAR, CD_NIVEL_1, CD_NIVEL_2, "
        + " CD_ENFCIE, IT_ACTIVO, IT_TIPO, CD_IZQUIER, CD_SECUEN) "
        + "values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

    PreparedStatement stIn = null;
    try {
      stIn = con.prepareStatement(insert);
      stIn.setString(1, cd_ind);
      stIn.setString(2, ds_ind);
      switch (tipo) {
        case tipoCOMUNIDAD:
          stIn.setNull(3, java.sql.Types.VARCHAR);
          stIn.setNull(4, java.sql.Types.VARCHAR);
          break;
        case tipoNIVEL1:
          stIn.setString(3, cd_n1);
          stIn.setNull(4, java.sql.Types.VARCHAR);
          break;
        case tipoNIVEL2:
          stIn.setString(3, cd_n1);
          stIn.setString(4, cd_n2);
          break;
      }
      stIn.setString(5, cd_enf);
      stIn.setString(6, it_ac);
      stIn.setString(7, it_ti);
      stIn.setString(8, cd_iz);
      stIn.setString(9, cd_se);

      stIn.executeUpdate();
      stIn.close();
      stIn = null;
    }
    catch (SQLException e) {
      ////# // System_out.println ("SrvGenAla: ya estaba el indicador " + e.getMessage());
      try {
        stIn.close();
        stIn = null;
      }
      catch (Exception ex) {
        throw ex;
      }
    }
    catch (Exception ex) {
      throw ex;
    }
  }

  void insertarIndAno(Connection con,
                      String cd_ind, String cd_ano, Double nm_coef) throws
      Exception {
    // Inserto el indicador
    String insert = "insert into SIVE_IND_ANO "
        + "(CD_INDALAR,CD_ANOEPI, NM_COEF) "
        + "values (?, ?, ?)";
    PreparedStatement stIn = null;
    try {
      stIn = con.prepareStatement(insert);
      stIn.setString(1, cd_ind);
      stIn.setString(2, cd_ano);
      stIn.setDouble(3, nm_coef.doubleValue());
      stIn.executeUpdate();
      stIn.close();
      stIn = null;
    }
    catch (SQLException e) {
      try {
        stIn.close();
        stIn = null;
      }
      catch (Exception ex) {
        throw ex;
      }
      ////# // System_out.println ("SrvGenAla: ya estaba el ind_ano " + e.getMessage());
    }
    catch (Exception ex) {
      throw ex;
    }
  }

  void insertarAlarma(int tipo, Connection con,
                      String cd_ano, String cd_ind, String cd_sem,
                      Double nm_v, Double nm_d) throws Exception {
    String insert = "insert into SIVE_ALARMA "
        + "(CD_ANOEPI, CD_INDALAR, CD_SEMEPI,NM_VALOR, NM_DSTANDAR ) "
        + " values (?, ?, ?, ?, ?)";

    PreparedStatement stIn = con.prepareStatement(insert);
    stIn.setString(1, cd_ano);
    stIn.setString(2, cd_ind);
    stIn.setString(3, cd_sem);
    stIn.setDouble(4, nm_v.doubleValue());
    //stIn.setString(4, nm_v.toString());
    if (tipo == tipoMEDIA) {
      stIn.setDouble(5, nm_d.doubleValue());
    }
    else {
      stIn.setNull(5, java.sql.Types.NUMERIC);
    }
    stIn.executeUpdate();
    stIn.close();
    stIn = null;

  }

  public CLista doPrueba(int operacion, CLista parametros) throws Exception {
    jdcdriver = new JDCConnectionDriver("oracle.jdbc.driver.OracleDriver",
        "jdbc:oracle:thin:@10.160.5.149:1521:ORCL",
        "pista",
        "loteb98");
    Connection con = null;
    con = openConnection();
    return doWork(operacion, parametros);
  }

}
