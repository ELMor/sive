//Title:
//Title:        Generacion de alarmas automaticas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Al inicio del a�o epidemiologico, o por peticion del administrador
//se generaran/actualizaran las alarmas automaticas de cada
//enfermedad a nivel de Area y a nivel de la Comunidad.

package genAlaAuto;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo.DataCat;
import notdata.DataEntradaEDO;
import notutil.CListaNiveles1;
import sapp.StubSrvBD;

public class PanGenAlaAuto
    extends CPanel {

  //ctes del panel

  //modos de consulta al servlet de alarmas
  final int servletGEN_AL_AUTO = 2;
  ResourceBundle res;
  final int servletGEN_VEC_ENF = 8;
  final int servletADV_GEN_AL = 9;

  //Modos op servlet Entrada EDO  (servlet auxiliar)

  final int servletOBTENER_NIV1_X_CODIGO = 3;
  final int servletOBTENER_NIV1_X_DESCRIPCION = 4;
  final int servletSELECCION_NIV1_X_CODIGO = 5;
  final int servletSELECCION_NIV1_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletRESTRICCION_NIVEL2 = 11;

  final int tipoCOMUNIDAD = 3;
  final int tipoNIVEL1 = 4;
  final int tipoNIVEL2 = 5;

  //rutas imagenes
  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/Magnify.gif"};
  //modos de pantalla
  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  //Variables
  int modoOperacion;
  int modoServlet;

  protected CLista lista = new CLista();
  //Servlet de gen alarmas
  final String strSERVLET = "servlet/SrvGenAla";
  //Servlet para �rea y distrito
  final String strSERVLET_NIVELES = "servlet/SrvEntradaEDO";

  protected StubSrvBD stubCliente = new StubSrvBD();

  protected int modoOperacionBk;

  Label lblNivel1 = new Label();
  Label lblNivel2 = new Label();
  TextField txtCodNivel1 = new TextField();
  TextField txtCodNivel2 = new TextField();
  TextField txtDesNivel1 = new TextField();
  TextField txtDesNivel2 = new TextField();
  ButtonControl btnBuscarNivel2 = new ButtonControl();
  ButtonControl btnBuscarNivel1 = new ButtonControl();

  ButtonControl btnGenAla = new ButtonControl();
  //Escuchadores
  PanGenAlaAutoActionListener btnActionListener = new
      PanGenAlaAutoActionListener(this);
  AlmActionListener btnLupasActionListener = new AlmActionListener(this);
  txt_keyAdapter txtKeyAdapter = new txt_keyAdapter(this);
  focusAdapter txtFocusAdapter = new focusAdapter(this);

  protected CCargadorImagen imgs = null;
  Panel panel1 = new Panel();
  Label lblAno = new Label();
  TextField textAno = new TextField();
  BorderLayout borderLayout1 = new BorderLayout();
  XYLayout xYLayout1 = new XYLayout();
  StatusBar statusBar1 = new StatusBar();
  BorderLayout borderLayout3 = new BorderLayout();

  public PanGenAlaAuto(CApp a) {
    try {
      setApp(a);
      //modoServlet = m;

      res = ResourceBundle.getBundle("genAlaAuto.Res" + app.getIdioma());

      jbInit();
      this.txtCodNivel1.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesNivel1.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtCodNivel2.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesNivel2.setText(this.app.getDS_NIVEL2_DEFECTO());
//# // System_out.println("Se cargala clase buena tras JBINIT*********");

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

//    xYLayout1.setHeight(159);
//    xYLayout1.setWidth(400);
    panel1.setLayout(xYLayout1);
    this.setLayout(borderLayout3);
    this.setSize(new Dimension(517, 381));

    lblAno.setText(res.getString("lblAno.Text"));

    btnGenAla.addActionListener(btnActionListener);
    btnGenAla.setLabel(res.getString("btnGenAla.Label"));

    imgs = new CCargadorImagen(app, imgNAME);
    statusBar1.setBevelOuter(BevelPanel.LOWERED);
    statusBar1.setBevelInner(BevelPanel.LOWERED);
    statusBar1.setText("");
    textAno.setBackground(new Color(255, 255, 150));

    imgs.CargaImagenes();
    btnGenAla.setImage(imgs.getImage(0));

    //Nuevo codigo

    btnBuscarNivel2.setImage(imgs.getImage(1));
    btnBuscarNivel1.setImage(imgs.getImage(1));

    lblNivel1.setText(this.app.getNivel1() + ":");
    lblNivel2.setText(this.app.getNivel2() + ":");

    btnBuscarNivel1.setActionCommand("BuscarNivel1"); // Nivel1
    btnBuscarNivel2.setActionCommand("BuscarNivel2"); // Nivel2

    txtCodNivel1.setName("txtCodNivel1");
    txtDesNivel1.setName("txtDesNivel1");
    txtCodNivel2.setName("txtCodNivel2");
    txtDesNivel2.setName("txtDesNivel2");

    this.add(panel1, BorderLayout.CENTER);
    panel1.add(lblAno, new XYConstraints(27, 35, 122, -1));
    panel1.add(textAno, new XYConstraints(158, 35, 60, -1));

    panel1.add(lblNivel1, new XYConstraints(27, 75, 99, -1));
    panel1.add(txtCodNivel1, new XYConstraints(132, 75, 77, -1));
    panel1.add(btnBuscarNivel1, new XYConstraints(213, 75, -1, -1));
    panel1.add(txtDesNivel1, new XYConstraints(242, 75, 210, -1));

    panel1.add(lblNivel2, new XYConstraints(27, 110, 99, -1));
    panel1.add(txtCodNivel2, new XYConstraints(132, 110, 77, -1));
    panel1.add(btnBuscarNivel2, new XYConstraints(213, 110, -1, -1));
    panel1.add(txtDesNivel2, new XYConstraints(242, 110, 210, -1));
    panel1.add(btnGenAla, new XYConstraints(387, 139, -1, -1));

    this.add(statusBar1, BorderLayout.SOUTH);

    //Nuevo c�digo para area, distrito

    // establece los escuchadores
    btnBuscarNivel1.addActionListener(btnLupasActionListener);
    btnBuscarNivel2.addActionListener(btnLupasActionListener);
    txtCodNivel1.addKeyListener(txtKeyAdapter);
    txtCodNivel2.addKeyListener(txtKeyAdapter);
    txtCodNivel1.addFocusListener(txtFocusAdapter);
    txtCodNivel2.addFocusListener(txtFocusAdapter);

    //Controlse en estado fijo
    txtDesNivel1.setEnabled(false);
    txtDesNivel1.setEditable(false);
    txtDesNivel2.setEnabled(false);
    txtDesNivel2.setEditable(false);

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        btnGenAla.setEnabled(true);
        btnBuscarNivel1.setEnabled(true);
        txtCodNivel1.setEnabled(true);
        InicializaNivel2();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnGenAla.setEnabled(false);
        btnBuscarNivel1.setEnabled(false);
        btnBuscarNivel2.setEnabled(false);
        txtCodNivel1.setEnabled(false);
        txtCodNivel2.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
    //Visibilidad seg�n perfil
    switch (app.getPerfil()) {
      //Epid de Area : No ve distrito
      case 3:
        lblNivel2.setVisible(false);
        btnBuscarNivel2.setVisible(false);
        txtCodNivel2.setVisible(false);
        txtDesNivel2.setVisible(false);

        break;
        //Epid. de CA: No ve area, distrito
      case 2:
        lblNivel1.setVisible(false);
        lblNivel2.setVisible(false);
        btnBuscarNivel1.setVisible(false);
        btnBuscarNivel2.setVisible(false);
        txtCodNivel1.setVisible(false);
        txtCodNivel2.setVisible(false);
        txtDesNivel1.setVisible(false);
        txtDesNivel2.setVisible(false);
        break;
    }
    doLayout();
  }

  // actualiza el estado del nivel2, condicionado al nivel1
  protected void InicializaNivel2() {
    if (txtDesNivel1.getText().length() == 0) {
      txtCodNivel2.setText("");
      txtDesNivel2.setText("");
      txtCodNivel2.setEnabled(false);
      btnBuscarNivel2.setEnabled(false);
    }
    else {
      txtCodNivel2.setEnabled(true);
      btnBuscarNivel2.setEnabled(true);
    }
  }

//__________________________________________________________________________

  void btnGenAla_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    CLista data;

    CLista hayAlarmas = new CLista();
    Vector fallosEnf = new Vector();
    int tipo = 2;

    statusBar1.setText("");
    try {

      if (isDataValid() == true) {
        this.modoOperacion = modoESPERA;
        Inicializar();

        switch (app.getPerfil()) {
          case 2:
            tipo = tipoCOMUNIDAD;
            break;
          case 3:
            tipo = tipoNIVEL1;
            break;
          case 4:
            tipo = tipoNIVEL2;
            break;
        }

        // apunta al servlet principal
        stubCliente.setUrl(new URL(app.getURL() + strSERVLET));

        // Para comprobar si existen e indicarlo al usuario
        modoServlet = servletADV_GEN_AL;
        data = new CLista();

        statusBar1.setText(res.getString("statusBar1.Text"));
        data.addElement(new ParGenAlaAuto(textAno.getText().trim(), tipo,
                                          txtCodNivel1.getText().trim(),
                                          txtCodNivel2.getText().trim()));
        //# // System_out.println("Voy a comprobar la existencia de las alarmas");

        hayAlarmas = (CLista) stubCliente.doPost(modoServlet, data);
        // SrvGenAla srv = new SrvGenAla();
        // hayAlarmas = srv.doPrueba(modoServlet, data);
        // srv = null;
        //# // System_out.println (" He vuelto del servlet comprobar la existencia de las alarmas ");
        statusBar1.setText("");
        boolean generar = false;
        if (hayAlarmas != null) {
          Boolean hay = (Boolean) hayAlarmas.firstElement();
          if (hay.booleanValue()) {
            msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                                  res.getString("msg2.Text"));
            msgBox.show();
            generar = msgBox.getResponse();
            msgBox = null;
          }
          else {
            generar = true;
          }
        }

        if (generar) { //ReTOCADO*************************
          generarAlarmas(textAno.getText().trim(), tipo);
        } // generar

      } //if dataValid
      else {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg3.Text"));
        msgBox.show();
        msgBox = null;
      }
      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }
    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

//__________________________________________________________________________

  boolean generarAlarmas(String anoEpi, int tipo) throws Exception {
    CMessage msgBox;
    CLista data;
    CLista lisEnfer = new CLista();
    CLista hayAlarmas = new CLista();
    Vector enfermedades = new Vector();
    Vector enfermedad = new Vector();
    Vector fallosEnf = new Vector();

    String sTipo = new String();
    switch (tipo) {
      case tipoCOMUNIDAD:

        //# // System_out.println ("En generar las alarmas de comunidad");
        sTipo = " comunidad ";
        break;
      case tipoNIVEL1:

        //# // System_out.println ("En generar las alarmas de nivel1");
        sTipo = " area ";
        break;
      case tipoNIVEL2:

        //# // System_out.println ("En generar las alarmas de nivel2");
        sTipo = " distrito ";
        break;
    }

//__________________________________________________________________________

    // Se realiza el proceso por enfermedades
    // Primero se pide al servlet que genere la estructura
    // que contiene todos los datos
    modoServlet = servletGEN_VEC_ENF;
    data = new CLista();
    data.addElement(new ParGenAlaAuto(anoEpi, tipo, txtCodNivel1.getText().trim(),
                                      txtCodNivel2.getText().trim()));

    statusBar1.setText(res.getString("statusBar2.Text"));
    //# // System_out.println("Voy a generar la estructura de datos" + sTipo);
    lisEnfer = (CLista) stubCliente.doPost(modoServlet, data);
//SrvGenAla srv = new SrvGenAla();
//lisEnfer = srv.doPrueba(modoServlet, data);
//srv = null;

    //# // System_out.println (" He vuelto del servlet estructura de datos " + sTipo);
    statusBar1.setText("");

    if (lisEnfer != null) {
      //# // System_out.println (" la estructura de datos no es nula");
      Boolean error = (Boolean) lisEnfer.firstElement();

      //Si hay true en primer elemento de lista es que no hay registros o no hay a�o
      if (error.booleanValue()) {
        String sError = (String) lisEnfer.elementAt(1);
        //sError = res.getString("msg4.Text") + sTipo + sError;
        //# // System_out.println (" ha habido un error " + sError);
        msgBox = new CMessage(this.app, CMessage.msgERROR, sError);
        msgBox.show();
        msgBox = null;
        return false;
      }

      else {
        //# // System_out.println (" no ha habido un error");
        enfermedades = (Vector) lisEnfer.elementAt(1);
        //# // System_out.println ("numero de enfermedades " + enfermedades.size());
        //imprimeVector(enfermedades);

//__________________________________________________________________________

        modoServlet = servletGEN_AL_AUTO;
        int numEnf = enfermedades.size();

//# // System_out.println("Tama�o vector enfermedades ***********" + enfermedades.size() );

        String cd_enf = new String();
        int indEnf = 0;
        while (indEnf < enfermedades.size()) {
          try {
            enfermedad = (Vector) enfermedades.elementAt(indEnf);
            indEnf++;

            cd_enf = (String) enfermedad.elementAt(0);
//# // System_out.println("Enfermedad  " + indEnf + "   " + cd_enf);

            data = new CLista();
            data.addElement(new ParGenAlaAuto(anoEpi, enfermedad, tipo,
                                              txtCodNivel1.getText().trim(),
                                              txtCodNivel2.getText().trim()));
            statusBar1.setText(res.getString("statusBar1.Text") + sTipo
                               + res.getString("msg5.Text") + indEnf +
                               res.getString("msg6.Text")
                               + numEnf + res.getString("msg7.Text"));
            // obtiene la lista
            //# // System_out.println("Voy a generar las alarmas " + sTipo);
            lista = (CLista) stubCliente.doPost(modoServlet, data);

            /*genAlaAuto.SrvGenAla srv = new genAlaAuto.SrvGenAla();
             srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                 "jdbc:oracle:thin:@10.160.12.54:1521:ORCL",
                                         "pista",
                                         "loteb98");
             lista = srv.doDebug(modoServlet, data);
             */

            /*
             SrvGenAla srv1 = new SrvGenAla();
             lista = srv1.doPrueba(modoServlet, data);
             srv1 = null;
             */

            //# // System_out.println (" He vuelto del servlet " + sTipo);
            statusBar1.setText("");
          }
          catch (Exception e) {
            e.printStackTrace();
            fallosEnf.addElement(cd_enf);
          }
        } // bucle while de proceso de las enfermedades

        if (fallosEnf.size() != 0) {
          String sEnfer = new String();
          for (int i = 0; i < fallosEnf.size(); i++) {
            sEnfer = sEnfer + (String) fallosEnf.elementAt(i) + " ";
          }
          sEnfer = res.getString("msg4.Text") + sTipo
              + res.getString("msg8.Text") + sEnfer;
          msgBox = new CMessage(this.app, CMessage.msgERROR, sEnfer);
          msgBox.show();
          msgBox = null;
        }
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg4.Text") + sTipo +
                                res.getString("msg9.Text"));
          msgBox.show();
          msgBox = null;
        }
      } // no error  (else)
    } // data != null

    return true;

  }

  //___________________________________________________________________________

  boolean isDataValid() {

    //Comprueba el a�o
    String aux = textAno.getText().trim();
    if (aux.length() != 4) {
      return false;
    }
    //Si perfil es de N1 o Niv2 debe haber cod1
    if ( (app.getPerfil() == 3) || (app.getPerfil() == 4)) {
      if (txtDesNivel1.equals("")) {
        return false;
      }
    }
    //Si perfil es de Niv2 debe haber cod2
    if ( (app.getPerfil() == 4)) {
      if (txtDesNivel2.equals("")) {
        return false;
      }
    }

    return true;

  }

  /*void imprimeVector(Vector v) {
    Vector aux = new Vector();
    //# // System_out.println("( ");
    for (int s=0; s<v.size(); s++) {
      try {
        aux = (Vector) v.elementAt(s);
        imprimeVector(aux);
        ////# // System_out.println("");
      }
      catch (ClassCastException ex){
        // System_out.print(" " + (String) v.elementAt(s) + " ");
      }
    }
    //# // System_out.println(") ");
     }*/

  // procesa opci�n seleccionar las listas de niveles 1***********
  public void btnBuscarNivel1_actionPerformed(ActionEvent evt) {

    DataEntradaEDO data;
    CMessage msgBox;
    this.modoOperacionBk = this.modoOperacion;

    try {
      txtCodNivel1.setText("");
      txtDesNivel1.setText("");
      txtCodNivel2.setText("");
      txtDesNivel2.setText("");
      // apunta al servlet auxiliar
      stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_NIVELES));
      this.modoOperacion = modoESPERA;
      Inicializar();

      CListaNiveles1 lista = new CListaNiveles1(app,
                                                res.getString("msg10.Text") +
                                                app.getNivel1(),
                                                stubCliente,
                                                strSERVLET_NIVELES,
                                                servletOBTENER_NIV1_X_CODIGO,
          servletOBTENER_NIV1_X_DESCRIPCION,
          servletSELECCION_NIV1_X_CODIGO,
          servletSELECCION_NIV1_X_DESCRIPCION);

      lista.show();
      data = (DataEntradaEDO) lista.getComponente();
      modoOperacion = this.modoOperacionBk;
      if (data != null) {
        txtCodNivel1.setText(data.getCod());
        txtDesNivel1.setText(data.getDes());
        this.modoOperacion = this.modoOperacionBk;
      }

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
      this.modoOperacion = this.modoOperacionBk;
    }
    Inicializar();
  } //fin de nivel1

  // procesa opci�n seleccionar las listas de niveles 1***********
  public void btnBuscarNivel2_actionPerformed(ActionEvent evt) {

    DataEntradaEDO data;
    CMessage msgBox;
    this.modoOperacionBk = this.modoOperacion;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      CListaNiveles2 lista = new CListaNiveles2(this,
                                                res.getString("msg10.Text") +
                                                app.getNivel2(),
                                                stubCliente,
                                                strSERVLET_NIVELES,
                                                servletOBTENER_NIV2_X_CODIGO,
          servletOBTENER_NIV2_X_DESCRIPCION,
          servletSELECCION_NIV2_X_CODIGO,
          servletSELECCION_NIV2_X_DESCRIPCION);

      lista.show();
      data = (DataEntradaEDO) lista.getComponente();
      this.modoOperacion = this.modoOperacionBk;
      if (data != null) {
        txtCodNivel2.setText(data.getCod());
        txtDesNivel2.setText(data.getDes());
        this.modoOperacion = this.modoOperacionBk;
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
      this.modoOperacion = this.modoOperacionBk;
    }
    Inicializar();
  } //fin nivel2

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {
    //Datos de envio
    DataEntradaEDO data;
//    DataEnferedo datEnf;
    CLista param = null;
    CMessage msg;

    int modo = modoOperacion; //Modo regreso de pantalla
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    param = new CLista();
    param.setIdioma(app.getIdioma());
    param.setPerfil(app.getPerfil());
    param.setLogin(app.getLogin());

    // gestion de datos
    if ( (txt.getName().equals("txtCodNivel1")) &&
        (txtCodNivel1.getText().length() > 0)) {
      param.addElement(new DataEntradaEDO(txtCodNivel1.getText().toUpperCase(),
                                          "", ""));
      strServlet = strSERVLET_NIVELES;
      modoServlet = servletOBTENER_NIV1_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodNivel2")) &&
             (txtCodNivel2.getText().length() > 0)) {
      param.addElement(new DataEntradaEDO(txtCodNivel2.getText(), "",
                                          txtCodNivel1.getText()));
      strServlet = strSERVLET_NIVELES;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    // busca el item
    if (param.size() > 0) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtCodNivel1")) {
            data = (DataEntradaEDO) param.firstElement();
            txtCodNivel1.setText(data.getCod());
            txtDesNivel1.setText(data.getDes());

          }
          else if (txt.getName().equals("txtCodNivel2")) {
            data = (DataEntradaEDO) param.firstElement();
            txtCodNivel2.setText(data.getCod());
            txtDesNivel2.setText(data.getDes());

          }

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg11.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modo;
      Inicializar();
    }

  }

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtCodNivel1")) &&
        (txtDesNivel1.getText().length() > 0)) {
      txtCodNivel2.setText("");
      txtDesNivel1.setText("");
      txtDesNivel2.setText("");

    }
    else if ( (txt.getName().equals("txtCodNivel2")) &&
             (txtDesNivel2.getText().length() > 0)) {
      txtDesNivel2.setText("");

    }
    Inicializar();
  }

} //clase

//____________________________ CLASES  DE ESCUCHA___________________

// action listener para los botones
class PanGenAlaAutoActionListener
    implements ActionListener, Runnable {
  PanGenAlaAuto adaptee = null;
  ActionEvent e = null;

  public PanGenAlaAutoActionListener(PanGenAlaAuto adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.btnGenAla_actionPerformed(e);
  }
}

//*****************************************************

// clase para nivel1
 class CListaCat
     extends CListaValores {

   public CListaCat(CApp a,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
     super(a,
           title,
           stub,
           servlet,
           obtener_x_codigo,
           obtener_x_descricpcion,
           seleccion_x_codigo,
           seleccion_x_descripcion);
     btnSearch_actionPerformed();
   }

   public Object setComponente(String s) {
     return new DataCat(s);
   }

   public String getCodigo(Object o) {
     return ( ( (DataCat) o).getCod());
   }

   public String getDescripcion(Object o) {
     return ( ( (DataCat) o).getDes());
   }
 }

class CListaNiveles2
    extends CListaValores {

  protected PanGenAlaAuto panel;

  public CListaNiveles2(PanGenAlaAuto p,
                        String title,
                        StubSrvBD stub,
                        String servlet,
                        int obtener_x_codigo,
                        int obtener_x_descricpcion,
                        int seleccion_x_codigo,
                        int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s, "", panel.txtCodNivel1.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

// ESCUCHADORES******PULSAR BOTONES***************************
// action listener para los botones
class AlmActionListener
    implements ActionListener, Runnable {
  PanGenAlaAuto adaptee = null;
  ActionEvent e = null;

  public AlmActionListener(PanGenAlaAuto adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;

    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("BuscarNivel1")) { // nivel 1
      adaptee.btnBuscarNivel1_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("BuscarNivel2")) { // nivel 2
      adaptee.btnBuscarNivel2_actionPerformed(e);
    }
  } //run
} //CLASE

class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  PanGenAlaAuto adaptee;

  txt_keyAdapter(PanGenAlaAuto adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanGenAlaAuto adaptee;
  FocusEvent event;

  focusAdapter(PanGenAlaAuto adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {

    adaptee.focusLost(event);
  }
}
