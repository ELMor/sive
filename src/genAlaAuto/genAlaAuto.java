//Title:        Generacion de alarmas automaticas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Al inicio del a�o epidemiologico, o por peticion del administrador
//se generaran/actualizaran las alarmas automaticas de cada
//enfermedad a nivel de Area y a nivel de la Comunidad.

package genAlaAuto;

import java.util.ResourceBundle;

import capp.CApp;

public class genAlaAuto
    extends CApp {

  ResourceBundle res;

  //public genAlaAuto() {
  //}

  public void init() {
    super.init();
// System_out.println("super.init en genalauto");

  }

  public void start() {
// System_out.println("start en genalauto");
    res = ResourceBundle.getBundle("genAlaAuto.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    PanGenAlaAuto panelGen = new PanGenAlaAuto(a);
    VerPanel("", panelGen);

    /*
         try {
            File miFichero = new File("c:\\resumen.txt");
            FileWriter fStream = new FileWriter(miFichero);
            String linea = new String();
            for (int i=1994; i< 1999; i++) {
              Integer ano = new Integer(i);
              //linea = "insert into sive_ano_epi values ("
                //      + "'" + ano.toString()+ "', TO_DATE('01/01/01'), TO_DATE('01/01/01'));";
                //fStream.write(linea);
                //fStream.write("\r");
                //fStream.write("\n");
              for (int j=1; j<53; j++) {
                Integer sem = new Integer(j);
                String ssem = sem.toString();
                if (j<10)
                  ssem = "0" + ssem;
         linea = "insert into sive_resumen_edos values ('"
                      + ano.toString()+"',"+ "'0-0-0','" + ssem
                      + "','10','14','10','14','4',1);";
                //linea = ano.toString()+","+ "0-0-0," + ssem
                //      + ",10,14,10,14,4,1,";
                //linea = ano.toString()+"\t"
                //        +"0-0-0"+"\t"
                //        +ssem+"\t"
                //        +"10"+"\t"
                //        +"14"+"\t"
                //        +"10"+"\t"
                //        +"14"+"\t"
                //        +"4"+"\t"
                //        +"1"+"\t";
                fStream.write(linea);
                fStream.write("\r");
                fStream.write("\n");
              }
            }
            fStream.close();
            fStream = null;
            miFichero = null;
          }
          catch (Exception e){}
     */

  }

}
