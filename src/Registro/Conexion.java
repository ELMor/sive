/*
 * Utilizaremos la clase RegistroConsultas
 */

package Registro;

// Para las conexiones a bases de datos.
import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Establece una conexi�n a la base de datos y crea un onjeto RegistroConsultas
 *
 * @author  JRM
 * @verison 04/06/01
 */
public class Conexion {
  /**
   * M�todo principal encargado de generar la conexi�n y el objeto RegistroConsultas
   */
  public static void main(String[] args) {
    // Objeto conexi�n que se utilizar� con posterioridad.
    Connection conexion;
    RegistroConsultas registro;
    try {
      // Creamos el driver.
      Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
      // Creamos la conexi�n a la base de datos
      conexion = DriverManager.getConnection(
          "jdbc:oracle:thin:@192.168.0.13:1521:SIVE",
          "dba_edo", "manager");
      // System_out.println("Conexi�n creada con �xito");

      registro = new RegistroConsultas(conexion, true, "JRM");
      registro.insertarParametro("JRM");
      registro.insertarParametro("ARG");
      registro.registrar("SIVE_EDOIND",
          "Selec * from SIVE_EDOIND where CD_ENFERMO = ? and CD_ENFERMO = ",
          "CD_ENFERMO",
          "",
          "Pruebas",
          true
          );
      registro.borrarSesion();
      conexion.close();
      System.in.read();
    }
    catch (Exception E) {
      // System_out.println("Error al crear la conexi�ns");
    }
  }
} // Conexion
