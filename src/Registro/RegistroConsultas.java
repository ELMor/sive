/*
 * Clases para el registro de acceso a datos seg�n la LORTAD
 */
package Registro;

// Para las conexiones a bases de datos.
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Seg�n el nivel m�s alto de acceso a datos que impone la LORTAD cualquier consulta debe ser registrada para
 * llevar un control del acceso a los datos por parte del usuario.
 * Esta clase se adapta a la LORTAD y al mecanismo de registro de consultas que impone ICM que trabaja sobre
 * pipes.
 * El funcionamiento de esta clase est� pensado para incorporarla a cada uno de los servlets.
 * Cada vez que un servlet haga un acceso a una tabla de datos personales utilizar� el m�todo registrar()
 * para registrar la consutla que se est� realizando al log que implanta ICM
 *
 * @author   JRM
 * @version  04/06/01
 */
public class RegistroConsultas {
  /** Conexion utilizada con la base de datos */
  protected Connection conexion;

  /** Vector de par�metros **/
  private Vector parametros;

  /** Nombre de la tabla de usuario y sesion */
  private final String TABLA_SESSION = "sive_lortad";

  /** Par�metros del procedimiento almacenado */
  String codAplicacion;
  String tablaLogica;
  String operLogico;
  String accesoLogico;
  String tablaFisica;
  String operFisica;
  /** Descripcion de la clave */
  String desClave;
  /** Clave */
  String clave;
  /** Identificacion */
  String ident;
  /** Identifiaci�n Duplicado */
  String identDup;
  /** Proceso */
  String codProceso;
  /** Descripci�n de proceso */
  String desProceso;
  /** Consulta realizada */
  String desSQL;
  /** Usuario a registrar */
  String usuario;

  /** Nos indica si debemos aplicar la LORTAD */
  boolean aplicarLORTAD;

  /** Inicializaci�n de datos */
  public RegistroConsultas(Connection conexion,
                           boolean aplicarLORTAD,
                           String usuario) {
    this.aplicarLORTAD = aplicarLORTAD;
    if (aplicarLORTAD) {
      parametros = new Vector();
      this.conexion = conexion;
      this.usuario = usuario;
      codAplicacion = "SIVE";
      operLogico = "C"; // Consulta.
      accesoLogico = "C"; // Consulta.
      operFisica = "C"; // Consulta.
      ident = "";
      identDup = "";
      codProceso = "";
      desProceso = "";
      // Inserta en la tabla de registro de sesion de usuario
      insertarSesion();
    }
  }

  /**
   *  Este m�todo devolver� la tarjeta sanitaria o el DNI del c�digo de
   *  enfermo que se le pasa como par�metro. Hay que tener en cuenta que
   *  en la aplicaci�n puede que el enfermo no tenga registrada ni la
   *  tarjeta sanitaria ni el DNI (por las razones que sean). En ese caso
   *  devolveremos ""
   *
   * @param C�digo del enfermo del que se quieren obtener datos.
   */
  private String obtenerIdentificacion(String CD_ENFERMO) {
    String identificacion = "";
    String strSelect;
    PreparedStatement st = null;
    ResultSet rset = null; // Para recoger los datos.

    try {
      // JRM (10/07/01): Hay ocasiones en las que el CD_ENFERMO viene entrecomillado.
      if (CD_ENFERMO.startsWith("'")) {
        strSelect = "select DS_NDOC from SIVE_ENFERMO where CD_ENFERMO = " +
            CD_ENFERMO;
        // System_out.println("select DS_NDOC from SIVE_ENFERMO where CD_ENFERMO = " + CD_ENFERMO);
      }
      else {
        strSelect = "select DS_NDOC from SIVE_ENFERMO where CD_ENFERMO = '" +
            CD_ENFERMO + "'";
        // System_out.println("select DS_NDOC from SIVE_ENFERMO where CD_ENFERMO = '" + CD_ENFERMO + "'");
      }

      st = conexion.prepareStatement(strSelect);
      rset = st.executeQuery();

      if (rset.next()) {
        identificacion = rset.getString("DS_NDOC");
      }
      rset.close();
      st.close();
      return identificacion;
    }
    catch (Exception ex) {
      ex.printStackTrace();
      // System_out.println("Fallo en obtenerIdentificacion");
    }
    return identificacion;
  } // obtenerIdentificacion

  /**
   * Este m�todo se necesita por la particularidad de la ejecuci�n de las
   * selects en java. En java se pueden escribir un string con la sentencia
   * y meterle ? para los par�metros. Debemos poder reproducir al sentencia
   * desde el mecanismo de implantaci�n de LORTAD por lo que este m�todo lo
   * que hace es sustituir cada ? por el par�metro adecuado.
   *
   * @param consulta - consulta de origen
   */
  private String transformarConsulta(String consulta) {
    String consultaConParametros = consulta;
    String consultaIz;
    String consultaDe;
    String parametro;
    int pos;
    Enumeration e;
    try {
      e = parametros.elements();
      while (e.hasMoreElements()) {
        parametro = (String) e.nextElement();
        pos = consultaConParametros.indexOf("?");
        consultaIz = consultaConParametros.substring(0, pos);
        consultaDe = consultaConParametros.substring(pos + 1,
            consultaConParametros.length());
        // Cuando es una fecha o un valor null o un caso especial (empieza por blanco)
        // no se a�aden los '
        if ( (parametro.toUpperCase().startsWith("TO_DATE")) ||
            (parametro.toUpperCase().startsWith("NULL")) ||
            (parametro.startsWith(" "))) {

          // Sin ap�strofes
          consultaConParametros = consultaIz + "" + parametro + "" + consultaDe;
        }
        else {
          consultaConParametros = consultaIz + "'" + parametro + "'" +
              consultaDe;
        }
      }
      return consultaConParametros;
    }
    catch (Exception Ex) {
      ; // Nos zampamos la excepci�n y dejamos la consulta como estaba.
    }
    return consulta;
  } // transformarConsulta

  /**
   * M�todo que ejecuta el procedimiento alamcenado.
   * Se actualizar� la llamada al procedimiento seg�n los
   * atributos que sirven de par�metro a este.
   */
  private void crearRegLog() {
    // NOTA: Es importante la sint�xis. Cuando el procdimiento lleve par�metros
    // habr� que poner algo del estilo { call CREAR_REG_LOG (?) } y
    // asignar a ? mediante el m�todo setInt(1,valor) del CallableStatement.

    // Para ejecutar el procedimiento almacenado.
    CallableStatement st;

    try {
      // Procedimiento a ejecutar.
      String procedimiento =
          "{ ? = call SIVE_PAQ_LOG.crear_reg_log (?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
      //String procedimiento = "{ call PRUEBA (?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
      st = conexion.prepareCall(procedimiento);
      // Cargamos todos los par�metros del procedimiento.
      st.registerOutParameter(1, java.sql.Types.NUMERIC);
      st.setString(2, codAplicacion);
      st.setString(3, tablaLogica);
      st.setString(4, operLogico);
      st.setString(5, accesoLogico);
      st.setString(6, tablaFisica);
      st.setString(7, operFisica);
      st.setString(8, desClave);
      st.setString(9, clave);
      st.setString(10, ident);
      st.setString(11, identDup);
      st.setString(12, codProceso);
      st.setString(13, desProceso);
      st.setString(14, tablaFisica);
      st.setString(15, desSQL);
      st.executeQuery();
      st.close();
    }
    catch (SQLException EX) {
      // System_out.println(EX.getMessage());
    }
  }

  /** Registro de la consulta. Llama a un procedimiento almacenado */
  public void registrar(String tabla,
                        String consulta,
                        String desClave,
                        String clave,
                        String desProceso,
                        boolean blimpiarParametros) {
    if (aplicarLORTAD) {
      tablaFisica = tabla;
      tablaLogica = tabla;
      this.desClave = desClave;
      this.clave = clave;
      if (!clave.equals("")) {
        ident = obtenerIdentificacion(clave);
      }
      this.desProceso = desProceso;
      if (parametros.size() != 0) {
        desSQL = transformarConsulta(consulta);
      }
      else {
        desSQL = consulta;
        // Comprobar parametros. Si hay cambio la select.
      }
      crearRegLog();
      if (blimpiarParametros) {
        limpiarVector();
      }
    } // aplicarLORTAD
  } // registrar

  /**
   * Limpiamos los elementos del vector
   */
  private void limpiarVector() {
    parametros.removeAllElements();
  } // limpiarVector

  /**
   * Insertamos un parametro al vector.
   */
  public void insertarParametro(String parametro) {
    if (aplicarLORTAD) {
      String vacio = null;
      if (parametro == null) {
        parametros.addElement(vacio);
      }
      else {
        parametros.addElement(parametro);
      }
    }
  } // insertarParametro

  /**
   * Inserta en la tabla de registro de sesion de usuario.
   */
  public void insertarSesion() {
    if (aplicarLORTAD) {
      // Para ejecutar el procedimiento almacenado.
      CallableStatement st;

      try {
        // Procedimiento a ejecutar.
        String procedimiento = "{ ? = call SIS_FUNC_CONTROL_SESION (?,?) }";
        st = conexion.prepareCall(procedimiento);
        // Cargamos todos los par�metros del procedimiento.
        st.registerOutParameter(1, java.sql.Types.NUMERIC);
        // Le decimos que estamos en modo Alta de usuario.
        st.setString(2, "A");
        // Le decimos cu�l es el usuario que debe almacenar.
        st.registerOutParameter(3, java.sql.Types.VARCHAR);
        st.setString(3, usuario);
        st.executeQuery();
        st.close();
      }
      catch (SQLException EX) {
        // System_out.println(EX.getMessage());
      }
    }
  } // insertarSesion

  /**
   * Borra de la tabla de registro de sesion de usuario.
   */
  public void borrarSesion() {
    if (aplicarLORTAD) {
      // Para ejecutar el procedimiento almacenado.
      CallableStatement st;

      try {
        // Procedimiento a ejecutar.
        String procedimiento = "{ ? = call SIS_FUNC_CONTROL_SESION (?,?) }";
        st = conexion.prepareCall(procedimiento);
        // Cargamos todos los par�metros del procedimiento.
        st.registerOutParameter(1, java.sql.Types.NUMERIC);
        // Le decimos que borre el usuario de la tabla.
        st.setString(2, "B");
        // Le decimos cu�l es el usuario que debe almacenar.
        st.registerOutParameter(3, java.sql.Types.VARCHAR);
        st.setString(3, usuario);
        st.executeQuery();
        st.close();
      }
      catch (SQLException EX) {
        // System_out.println(EX.getMessage());
      }
    }
  } // borrarSesion

} // RegistroConsultas