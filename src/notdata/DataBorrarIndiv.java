package notdata;

import java.io.Serializable;

public class DataBorrarIndiv
    implements Serializable {

  public String NM_EDO = "";
  public String CD_ENFERMO = "";
  public String CD_OPE = "";

  public DataBorrarIndiv(String nm_edo,
                         String cd_enfermo,
                         String cd_ope) {
    NM_EDO = nm_edo;
    CD_ENFERMO = cd_enfermo;
    CD_OPE = cd_ope;
  }

  public String getNM_EDO() {
    return NM_EDO;
  }

  public String getCD_ENFERMO() {
    return CD_ENFERMO;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }
}