package notdata;

import java.io.Serializable;

public class DataEnfCaso
    implements Serializable {

  protected String CD_ENFERMO = "";
  protected String NM_EDO = "";
  protected String FC_ULTACT = "";

  public DataEnfCaso(String cod, String Caso) {
    CD_ENFERMO = cod;
    NM_EDO = Caso;
  }

  public DataEnfCaso(String cod, String Caso, String fc_ultact) {
    CD_ENFERMO = cod;
    NM_EDO = Caso;
    FC_ULTACT = fc_ultact;
  }

  public String getCD_ENFERMO() {
    return CD_ENFERMO;
  }

  public String getNM_EDO() {
    return NM_EDO;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }
}
