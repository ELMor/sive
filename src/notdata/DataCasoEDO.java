package notdata;

import java.io.Serializable;

//Adaptación de la lista de casos duplicados para mostrar el primer notificador

public class DataCasoEDO
    implements Serializable {
  public String sCodEnfermo = "";
  public String sApe1 = "";
  public String sApe2 = "";
  public String sNombre = "";
  public String sSiglas = "";
  public int iCodCaso = 0;
  public String sDesEnfermedad = "";
  public String sCodEnfermedad = "";
  public boolean bHayDatos = false;
  public boolean bEsOK = false;
  public boolean bConfidencial = false;
  public String sFechaNac = "";
  public String sFechaNotif = "";
  public String sCodClasif = "";
  public String sDesClasif = "";
  public String sFechaIni = "";
  //QQ
  public String sCodCentro = "";
  public String sDesCentro = "";
  public String sCodEquipo = "";
  public String sDesEquipo = "";
  public String sCodN1 = "";
  public String sCodN2 = "";

  public DataCasoEDO() {}

  public DataCasoEDO(String CodEnfermo,
                     String Ape1,
                     String Ape2,
                     String Nombre,
                     String Siglas,
                     int CodCaso,
                     String DesEnfermedad,
                     String CodEnfermedad,
                     boolean HayDatos,
                     boolean EsOK, boolean Confidencial,
                     String FechaNac,
                     String FechaNotif,
                     String CodClasif,
                     String DesClasif,
                     String FechaIni,
                     //QQ
                     String CodCentro,
                     String DesCentro,
                     String CodEquipo,
                     String DesEquipo,
                     String CodN1,
                     String CodN2
                     ) {
    sCodEnfermo = CodEnfermo;
    sApe1 = Ape1;
    sApe2 = Ape2;
    sNombre = Nombre;
    sSiglas = Siglas;
    iCodCaso = CodCaso;
    sDesEnfermedad = DesEnfermedad;
    sCodEnfermedad = CodEnfermedad;
    bHayDatos = HayDatos;
    bEsOK = EsOK;
    bConfidencial = Confidencial;
    sFechaNac = FechaNac;
    sFechaNotif = FechaNotif;
    sCodClasif = CodClasif;
    sDesClasif = DesClasif;
    sFechaIni = FechaIni;
    //QQ
    sCodCentro = CodCentro;
    sDesCentro = DesCentro;
    sCodEquipo = CodEquipo;
    sDesEquipo = DesEquipo;
    sCodN1 = CodN1;
    sCodN2 = CodN2;
  }
}
