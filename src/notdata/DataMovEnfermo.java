package notdata;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

//Estructura para sive_mov_enfermo
public class DataMovEnfermo
    implements Serializable {

  public String CD_ENFERMO = null;
  public String CD_PROV = null;
  public String CD_PAIS = null;
  public String CD_MUN = null;
  public String CD_TDOC = null;
//public String CD_VIA   = null;
//public String CD_VIAL = null; //residuo
  public String DS_APE1 = null;
  public String DS_APE2 = null;
  public String DS_NOMBRE = null;
  public String DS_FONOAPE1 = null;
  public String DS_FONOAPE2 = null;
  public String DS_FONONOMBRE = null;
  public String IT_CALC = null;
  public String FC_NAC = null; //15
  public String CD_SEXO = null;
  public String CD_POSTAL = null;
  public String DS_DIREC = null;
  public String DS_NUM = null;
  public String DS_PISO = null;
  public String DS_TELEF = null;
  public String CD_NIVEL_1 = null;
  public String CD_NIVEL_2 = null;
  public String CD_ZBS = null;
  public String CD_OPE = null;
  public String FC_ULTACT = null;
  public String IT_REVISADO = null;
  public String CD_MOTBAJA = null;
  public String FC_BAJA = null;
  public String DS_NDOC = null;
  public String SIGLAS = null; //31

  public String DS_OBSERV = null;
  public String CD_PROV2 = null;
  public String CD_MUNI2 = null;
  public String CD_POST2 = null;
  public String DS_DIREC2 = null;
  public String DS_NUM2 = null;
  public String DS_PISO2 = null;
  public String DS_TELEF2 = null;
  public String DS_OBSERV2 = null;
  public String CD_PROV3 = null;
  public String CD_MUNI3 = null;
  public String CD_POST3 = null;
  public String DS_DIREC3 = null;
  public String DS_NUM3 = null;
  public String DS_PISO3 = null;
  public String DS_TELEF3 = null;
  public String DS_OBSERV3 = null;

// suca
  public String CDVIAL = null;
  public String CDTVIA = null;
  public String CDTNUM = null;
  public String DSCALNUM = null;

  /*//descripciones: enfermo
  public String DS_SEXO = null;
  public String DS_VIAL = null;
  public String DS_VIALITE = null;
  public String DS_TIPODOC = null;
  public String DS_MOTBAJA = null; //36 */

//constructor para enfermo
 public DataMovEnfermo(String cd_enfermo,
                       String cd_prov,
                       String cd_pais, String cd_mun,
                       String cd_tdoc,
                       String ds_ape1, String ds_ape2,
                       String ds_nombre, String ds_fonoape1,
                       String ds_fonoape2, String ds_fononombre,
                       String it_calc, String fc_nac,
                       String cd_sexo,
                       String cd_postal,
                       String ds_direc, String ds_num,
                       String ds_piso, String ds_telef,
                       String cd_nivel_1, String cd_nivel_2,
                       String cd_zbs, String cd_ope,
                       String fc_ultact, String it_revisado,
                       String cd_motbaja,
                       String fc_baja,
                       String ds_ndoc, String siglas,

                       String ds_observ,
                       String cd_prov2,
                       String cd_muni2,
                       String cd_post2,
                       String ds_direc2,
                       String ds_num2,
                       String ds_piso2,
                       String ds_telef2,
                       String ds_observ2,
                       String cd_prov3,
                       String cd_muni3,
                       String cd_post3,
                       String ds_direc3,
                       String ds_num3,
                       String ds_piso3,
                       String ds_telef3,
                       String ds_observ3,
                       String cdvial, String cdtvia, String cdtnum,
                       String dscalnum) {

   CD_ENFERMO = cd_enfermo;
   CD_PROV = cd_prov;
   CD_PAIS = cd_pais;
   CD_MUN = cd_mun;
   CD_TDOC = cd_tdoc;
   //CD_VIA   = cd_via;
   //CD_VIAL = cd_vial;
   DS_APE1 = ds_ape1;
   DS_APE2 = ds_ape2;
   DS_NOMBRE = ds_nombre;
   DS_FONOAPE1 = ds_fonoape1;
   DS_FONOAPE2 = ds_fonoape2;
   DS_FONONOMBRE = ds_fononombre;
   IT_CALC = it_calc;
   FC_NAC = fc_nac;
   CD_SEXO = cd_sexo;
   CD_POSTAL = cd_postal;
   DS_DIREC = ds_direc;
   DS_NUM = ds_num;
   DS_PISO = ds_piso;
   DS_TELEF = ds_telef;
   CD_NIVEL_1 = cd_nivel_1;
   CD_NIVEL_2 = cd_nivel_2;
   CD_ZBS = cd_zbs;
   CD_OPE = cd_ope;
   FC_ULTACT = fc_ultact;
   IT_REVISADO = it_revisado;
   CD_MOTBAJA = cd_motbaja;
   FC_BAJA = fc_baja;
   DS_NDOC = ds_ndoc;
   SIGLAS = siglas;

   DS_OBSERV = ds_observ;
   CD_PROV2 = cd_prov2;
   CD_MUNI2 = cd_muni2;
   CD_POST2 = cd_post2;
   DS_DIREC2 = ds_direc2;
   DS_NUM2 = ds_num2;
   DS_PISO2 = ds_piso2;
   DS_TELEF2 = ds_telef2;
   DS_OBSERV2 = ds_observ2;
   CD_PROV3 = cd_prov3;
   CD_MUNI3 = cd_muni3;
   CD_POST3 = cd_post3;
   DS_DIREC3 = ds_direc3;
   DS_NUM3 = ds_num3;
   DS_PISO3 = ds_piso3;
   DS_TELEF3 = ds_telef3;
   DS_OBSERV3 = ds_observ3;
   CDVIAL = cdvial;
   CDTVIA = cdtvia;
   CDTNUM = cdtnum;
   DSCALNUM = dscalnum;
 } //fin constructor enfermo

  public String getCD_ENFERMO() {
    return CD_ENFERMO;
  }

  public String getCD_PROV() {
    return CD_PROV;
  }

  public String getCD_PAIS() {
    return CD_PAIS;
  }

  public String getCD_MUN() {
    return CD_MUN;
  }

  public String getCD_TDOC() {
    return CD_TDOC;
  }

//public String getCD_VIA   () { return CD_VIA ;}
//public String getCD_VIAL   () { return CD_VIAL ;}
  public String getDS_APE1() {
    return DS_APE1;
  }

  public String getDS_APE2() {
    return DS_APE2;
  }

  public String getDS_NOMBRE() {
    return DS_NOMBRE;
  }

  public String getDS_FONOAPE1() {
    return DS_FONOAPE1;
  }

  public String getDS_FONOAPE2() {
    return DS_FONOAPE2;
  }

  public String getDS_FONONOMBRE() {
    return DS_FONONOMBRE;
  }

  public String getIT_CALC() {
    return IT_CALC;
  }

  public String getFC_NAC() {
    return FC_NAC;
  }

  public String getCD_SEXO() {
    return CD_SEXO;
  }

  public String getCD_POSTAL() {
    return CD_POSTAL;
  }

  public String getDS_DIREC() {
    return DS_DIREC;
  }

  public String getDS_NUM() {
    return DS_NUM;
  }

  public String getDS_PISO() {
    return DS_PISO;
  }

  public String getDS_TELEF() {
    return DS_TELEF;
  }

  public String getCD_NIVEL_1() {
    return CD_NIVEL_1;
  }

  public String getCD_NIVEL_2() {
    return CD_NIVEL_2;
  }

  public String getCD_ZBS() {
    return CD_ZBS;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public String getIT_REVISADO() {
    return IT_REVISADO;
  }

  public String getCD_MOTBAJA() {
    return CD_MOTBAJA;
  }

  public String getFC_BAJA() {
    return FC_BAJA;
  }

  public String getDS_NDOC() {
    return DS_NDOC;
  }

  public String getSIGLAS() {
    return SIGLAS;
  }

  public String getDS_OBSERV() {
    return DS_OBSERV;
  }

  public String getCD_PROV2() {
    return CD_PROV2;
  }

  public String getCD_MUNI2() {
    return CD_MUNI2;
  }

  public String getCD_POST2() {
    return CD_POST2;
  }

  public String getDS_DIREC2() {
    return DS_DIREC2;
  }

  public String getDS_NUM2() {
    return DS_NUM2;
  }

  public String getDS_PISO2() {
    return DS_PISO2;
  }

  public String getDS_TELEF2() {
    return DS_TELEF2;
  }

  public String getDS_OBSERV2() {
    return DS_OBSERV2;
  }

  public String getCD_PROV3() {
    return CD_PROV3;
  }

  public String getCD_MUNI3() {
    return CD_MUNI3;
  }

  public String getCD_POST3() {
    return CD_POST3;
  }

  public String getDS_DIREC3() {
    return DS_DIREC3;
  }

  public String getDS_NUM3() {
    return DS_NUM3;
  }

  public String getDS_PISO3() {
    return DS_PISO3;
  }

  public String getDS_TELEF3() {
    return DS_TELEF3;
  }

  public String getDS_OBSERV3() {
    return DS_OBSERV3;
  }

  public String getCDVIAL() {
    return CDVIAL;
  }

  public String getCDTVIA() {
    return CDTVIA;
  }

  public String getCDTNUM() {
    return CDTNUM;
  }

  public String getDSCALNUM() {
    return DSCALNUM;
  }

  private void writeObject(ObjectOutputStream s) throws IOException {
    s.defaultWriteObject();
  }

  private void readObject(ObjectInputStream s) throws IOException,
      ClassNotFoundException {
    s.defaultReadObject();
  }

} //FIN DE LA CLASE
