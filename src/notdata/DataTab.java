package notdata;

import java.util.Hashtable;

//Limite: 49 si, 50 NO
//Estructura para sive_enfermo, sive_edoind, SIVE_NOTIF_EDOI
public class DataTab
    extends Hashtable {

//CONSTRUCTORES
  public DataTab() {
    super();
  }

//Si es un nulo, no hara NADA, y al hacer el get tendre null
//Si no es nulo, hara un put y al hacer el get obtendre el dato
  public synchronized void insert(Object key, Object value) {
    if ( (key != null) && (value != null)) {
      put(key, value);
    }
  }

//para comprobar si una enfermedad es I o A
  public String getCD_TVIGI() {
    return (String) get("CD_TVIGI");
  }

//nuevo!!! para controlar si tenemos que mover el enfermo a historicos
  public String getIT_MOV_ENFERMO() {
    return (String) get("IT_MOV_ENFERMO");
  }

// m�todos
  public String getCD_ENFERMO() {
    return (String) get("CD_ENFERMO");
  }

  public String getCD_PROV() {
    return (String) get("CD_PROV");
  }

  public String getCD_PAIS() {
    return (String) get("CD_PAIS");
  }

  public String getCD_MUN() {
    return (String) get("CD_MUN");
  }

  public String getCD_TDOC() {
    return (String) get("CD_TDOC");
  }

  //public String getCD_VIA   () { return (String) get("CD_VIA");}
  //public String getCD_VIAL   () { return (String) get("CD_VIAL");}
  public String getCDVIAL() {
    return (String) get("CDVIAL");
  }

  public String getCDTVIA() {
    return (String) get("CDTVIA");
  }

  public String getCDTNUM() {
    return (String) get("CDTNUM");
  }

  public String getDSCALNUM() {
    return (String) get("DSCALNUM");
  }

  public String getDS_APE1() {
    return (String) get("DS_APE1");
  }

  public String getDS_APE2() {
    return (String) get("DS_APE2");
  }

  public String getDS_NOMBRE() {
    return (String) get("DS_NOMBRE");
  }

  public String getDS_FONOAPE1() {
    return (String) get("DS_FONOAPE1");
  }

  public String getDS_FONOAPE2() {
    return (String) get("DS_FONOAPE2");
  }

  public String getDS_FONONOMBRE() {
    return (String) get("DS_FONONOMBRE");
  }

  public String getIT_CALC() {
    return (String) get("IT_CALC");
  }

  public String getFC_NAC() {
    return (String) get("FC_NAC");
  }

  public String getCD_SEXO() {
    return (String) get("CD_SEXO");
  }

  public String getCD_POSTAL() {
    return (String) get("CD_POSTAL");
  }

  public String getDS_DIREC() {
    return (String) get("DS_DIREC");
  }

  public String getDS_NUM() {
    return (String) get("DS_NUM");
  }

  public String getDS_PISO() {
    return (String) get("DS_PISO");
  } //20

  public String getDS_TELEF() {
    return (String) get("DS_TELEF");
  }

  public String getCD_NIVEL_1() {
    return (String) get("CD_NIVEL_1");
  }

  public String getCD_NIVEL_2() {
    return (String) get("CD_NIVEL_2");
  }

  public String getCD_ZBS() {
    return (String) get("CD_ZBS");
  }

  public String getCD_OPE() {
    return (String) get("CD_OPE");
  }

  public String getFC_ULTACT() {
    return (String) get("FC_ULTACT");
  }

  public String getIT_REVISADO() {
    return (String) get("IT_REVISADO");
  }

  public String getCD_MOTBAJA() {
    return (String) get("CD_MOTBAJA");
  }

  public String getFC_BAJA() {
    return (String) get("FC_BAJA");
  }

  public String getDS_NDOC() {
    return (String) get("DS_NDOC");
  }

  public String getSIGLAS() {
    return (String) get("SIGLAS");
  }

  /*public String getDS_VIAL () { return (String) get("DS_VIAL");}
     public String getDS_VIALITE () { return (String) get("DS_VIALITE");} */

  public String getNM_EDO() {
    return (String) get("NM_EDO");
  }

  public String getCD_ANOEPI() {
    return (String) get("CD_ANOEPI");
  }

  public String getCD_SEMEPI() {
    return (String) get("CD_SEMEPI");
  }

  public String getCD_ANOURG() {
    return (String) get("CD_ANOURG");
  }

  public String getNM_ENVIOURGSEM() {
    return (String) get("NM_ENVIOURGSEM");
  }

  public String getCD_CLASIFDIAG() {
    return (String) get("CD_CLASIFDIAG");
  }

  public String getIT_PENVURG() {
    return (String) get("IT_PENVURG");
  }

  public String getCD_PROV_EDOIND() {
    return (String) get("CD_PROV_EDOIND");
  }

  public String getCD_MUN_EDOIND() {
    return (String) get("CD_MUN_EDOIND");
  }

  public String getCD_NIVEL_1_EDOIND() {
    return (String) get("CD_NIVEL_1_EDOIND");
  }

  public String getCD_NIVEL_2_EDOIND() {
    return (String) get("CD_NIVEL_2_EDOIND");
  }

  public String getCD_ZBS_EDOIND() {
    return (String) get("CD_ZBS_EDOIND");
  }

  public String getFC_RECEP() {
    return (String) get("FC_RECEP");
  }

  public String getDS_CALLE() {
    return (String) get("DS_CALLE");
  }

  public String getDS_NMCALLE() {
    return (String) get("DS_NMCALLE");
  }

  public String getDS_PISO_EDOIND() {
    return (String) get("DS_PISO_EDOIND");
  }

  public String getCD_POSTAL_EDOIND() {
    return (String) get("CD_POSTAL_EDOIND");
  }

  public String getCD_OPE_EDOIND() {
    return (String) get("CD_OPE_EDOIND");
  }

  public String getFC_ULTACT_EDOIND() {
    return (String) get("FC_ULTACT_EDOIND");
  }

  public String getCD_ANOOTRC() {
    return (String) get("CD_ANOOTRC");
  }

  public String getNM_ENVOTRC() {
    return (String) get("NM_ENVOTRC");
  } //60

  public String getCD_ENFCIE() {
    return (String) get("CD_ENFCIE");
  }

  // modificacion jlt 14/12/2001
  // recuperamos la descripci�n de la enfermedad seleccionada
  public String getDS_PROCESO() {
    return (String) get("DS_PROCESO");
  }

  public String getFC_FECNOTIF() {
    return (String) get("FC_FECNOTIF");
  }

  public String getIT_DERIVADO() {
    return (String) get("IT_DERIVADO");
  }

  public String getDS_CENTRODER() {
    return (String) get("DS_CENTRODER");
  }

  public String getIT_DIAGCLI() {
    return (String) get("IT_DIAGCLI");
  }

  public String getFC_INISNT() {
    return (String) get("FC_INISNT");
  }

  public String getDS_COLECTIVO() {
    return (String) get("DS_COLECTIVO");
  }

  public String getIT_ASOCIADO() {
    return (String) get("IT_ASOCIADO");
  }

  public String getIT_DIAGMICRO() {
    return (String) get("IT_DIAGMICRO");
  }

  public String getDS_ASOCIADO() {
    return (String) get("DS_ASOCIADO");
  }

  /*public String getCD_VIA_EDOIND () { return (String) get("CD_VIA_EDOIND");}
       public String getCD_VIAL_EDOIND () { return (String) get("CD_VIAL_EDOIND");}*/
  public String getIT_DIAGSERO() {
    return (String) get("IT_DIAGSERO");
  }

  public String getDS_DIAGOTROS() {
    return (String) get("DS_DIAGOTROS");
  }

      /*public String getDS_VIAL_EDOIND () { return  (String) get("DS_VIAL_EDOIND");}
     public String getDS_VIALITE_EDOIND () { return (String) get("DS_VIALITE_EDOIND");}*/

  public String getDS_PROV_EDOIND() {
    return (String) get("DS_PROV_EDOIND");
  }

  public String getDS_MUN_EDOIND() {
    return (String) get("DS_MUN_EDOIND");
  }

  public String getDS_NIVEL_1_EDOIND() {
    return (String) get("DS_NIVEL_1_EDOIND");
  }

  public String getDS_NIVEL_2_EDOIND() {
    return (String) get("DS_NIVEL_2_EDOIND");
  }

  public String getDS_ZBS_EDOIND() {
    return (String) get("DS_ZBS_EDOIND");
  }

  public String getCD_CA_EDOIND() {
    return (String) get("CD_CA_EDOIND");
  }

  public String getCD_E_NOTIF() {
    return (String) get("CD_E_NOTIF");
  }

  public String getDS_DECLARANTE() {
    return (String) get("DS_DECLARANTE");
  }

  public String getIT_PRIMERO() {
    return (String) get("IT_PRIMERO");
  }

  public String getCD_OPE_NOTIF_EDOI() {
    return (String) get("CD_OPE_NOTIF_EDOI");
  }

  public String getFC_ULTACT_NOTIF_EDOI() {
    return (String) get("FC_ULTACT_NOTIF_EDOI");
  }

  public String getCD_ANOEPI_NOTIF_EDOI() {
    return (String) get("CD_ANOEPI_NOTIF_EDOI");
  }

  public String getCD_SEMEPI_NOTIF_EDOI() {
    return (String) get("CD_SEMEPI_NOTIF_EDOI");
  }

  public String getFC_RECEP_NOTIF_EDOI() {
    return (String) get("FC_RECEP_NOTIF_EDOI");
  }

  public String getFC_FECNOTIF_NOTIF_EDOI() {
    return (String) get("FC_FECNOTIF_NOTIF_EDOI");
  }

  public String getCD_NIVEL_1_E_NOTIF() {
    return (String) get("CD_NIVEL_1_E_NOTIF");
  }

  public String getCD_NIVEL_2_E_NOTIF() {
    return (String) get("CD_NIVEL_2_E_NOTIF");
  }

  public String getCD_ZBS_E_NOTIF() {
    return (String) get("CD_ZBS_E_NOTIF");
  }

  public String getCD_CENTRO_E_NOTIF() {
    return (String) get("CD_CENTRO_E_NOTIF");
  }

  public String getDS_CENTRO_E_NOTIF() {
    return (String) get("DS_CENTRO_E_NOTIF");
  }

} //FIN DE LA CLASE
