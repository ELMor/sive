
package notdata;

import java.io.Serializable;

public class DataListaEDONum
    implements Serializable {

  public String sCodEnfer = "", sDesEnfer = "", sCasos = "", sFechaNotif = "";
  public String sVigi = "", sFechaRecep = "";
  public String sCodEquipo = "", sAnno = "", sSemana = "", sCodOpe = "";
  public String sFechaUltAct = "";
  // C�digo de enfermedad anterior (si se ha modificacdo)
  public String sCodEnferAnt = "";
  // La acci�n puede ser servletUPDATE_LISTA_EDO_NUM,
  // servletINSERT_LISTA_EDO_NUM O servletDELETE_LISTA_EDO_NUM
  int iAccion = -1;

  public String sPartes = "";

  public DataListaEDONum() {}

  public DataListaEDONum(String codenfer, String desenfer,
                         String casos, String fechrecep,
                         String vigi) {
    sCodEnfer = codenfer;
    sDesEnfer = desenfer;
    sCasos = casos;
    sFechaRecep = fechrecep;
    sVigi = vigi;

  }

  public DataListaEDONum(String codenfer, String desenfer,
                         String casos, String fechrecep,
                         String vigi, String codequipo,
                         String anno, String semana,
                         String codope, String fechaultact) {

    sCodEnfer = codenfer;
    sDesEnfer = desenfer;
    sCasos = casos;
    sFechaRecep = fechrecep;
    sVigi = vigi;
    sCodEquipo = codequipo;
    sAnno = anno;
    sSemana = semana;
    sCodOpe = codope;
    sFechaUltAct = fechaultact;
  }

  public DataListaEDONum(String codenfer, String desenfer,
                         String casos, String fechrecep,
                         String vigi, String codequipo,
                         String anno, String semana,
                         String codope, String fechaultact,
                         String fechanotif) {

    sCodEnfer = codenfer;
    sDesEnfer = desenfer;
    sCasos = casos;
    sFechaRecep = fechrecep;
    sVigi = vigi;
    sCodEquipo = codequipo;
    sAnno = anno;
    sSemana = semana;
    sCodOpe = codope;
    sFechaUltAct = fechaultact;
    sFechaNotif = fechanotif;
  }

  public DataListaEDONum(String codenfer, String desenfer,
                         String casos, String fechrecep,
                         String vigi, String codequipo,
                         String anno, String semana,
                         String codope, String fechaultact,
                         String fechanotif, String Partes) {

    sCodEnfer = codenfer;
    sDesEnfer = desenfer;
    sCasos = casos;
    sFechaRecep = fechrecep;
    sVigi = vigi;
    sCodEquipo = codequipo;
    sAnno = anno;
    sSemana = semana;
    sCodOpe = codope;
    sFechaUltAct = fechaultact;
    sFechaNotif = fechanotif;
    sPartes = Partes;
  }

  public String getCodEnfer() {
    return sCodEnfer;
  }

  public String getDesEnfer() {
    return sDesEnfer;
  }

  public String getCasos() {
    return sCasos;
  }

  public String getFechaRecep() {
    return sFechaRecep;
  }

  public String getVigi() {
    return sVigi;
  }

  public String getCodEquipo() {
    return sCodEquipo;
  }

  public String getAnno() {
    return sAnno;
  }

  public String getSemana() {
    return sSemana;
  }

  public String getCodOpe() {
    return sCodOpe;
  }

  public String getFechaUltAct() {
    return sFechaUltAct;
  }

  public int getAccion() {
    return iAccion;
  }

  public String getCodEnferAnt() {
    return sCodEnferAnt;
  }

  public String getFechaNotif() {
    return sFechaNotif;
  }

  public String getPartes() {
    return sPartes;
  }
}
