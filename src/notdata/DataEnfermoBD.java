package notdata;

import java.io.Serializable;

//Estructura para guardar los datos del enfermo,
//que llegan de la BD
public class DataEnfermoBD
    implements Serializable {

  protected String CD_ENFERMO = null;
  protected String CD_TDOC = null;
  protected String DS_APE1 = null;
  protected String DS_APE2 = null;
  protected String DS_NOMBRE = null;
  protected String DS_FONOAPE1 = null;
  protected String DS_FONOAPE2 = null;
  protected String DS_FONONOMBRE = null;

  protected String IT_CALC = null;
  protected String FC_NAC = null;
  protected String CD_SEXO = null;
  protected String DS_TELEF = null;
  protected String DS_NDOC = null;
  protected String SIGLAS = null;

  //no se si serviran en un futuro
  //Nota: Desde boton de enfermo, no se recogen!
  protected String CD_OPE = null;
  protected String FC_ULTACT = null;

  protected String CD_NIVEL_1 = null;
  protected String CD_NIVEL_2 = null;
  protected String CD_ZBS = null;

//constructor para enfermo
  public DataEnfermoBD(String cd_enfermo,
                       String cd_tdoc,
                       String ds_ape1, String ds_ape2,
                       String ds_nombre, String ds_fonoape1,
                       String ds_fonoape2, String ds_fononombre,
                       String it_calc, String fc_nac,
                       String cd_sexo,
                       String ds_telef,
                       String cd_nivel_1, String cd_nivel_2,
                       String cd_zbs, String cd_ope,
                       String fc_ultact,
                       String ds_ndoc, String siglas) {

    CD_ENFERMO = cd_enfermo;
    CD_TDOC = cd_tdoc;
    DS_APE1 = ds_ape1;
    DS_APE2 = ds_ape2;
    DS_NOMBRE = ds_nombre;
    DS_FONOAPE1 = ds_fonoape1;
    DS_FONOAPE2 = ds_fonoape2;
    DS_FONONOMBRE = ds_fononombre;
    CD_NIVEL_1 = cd_nivel_1;
    CD_NIVEL_2 = cd_nivel_2;
    CD_ZBS = cd_zbs;
    CD_OPE = cd_ope;
    FC_ULTACT = fc_ultact;
    IT_CALC = it_calc;
    FC_NAC = fc_nac;
    CD_SEXO = cd_sexo;
    CD_SEXO = cd_sexo;
    DS_TELEF = ds_telef;
    DS_NDOC = ds_ndoc;
    SIGLAS = siglas;

  }

  public String getCD_ENFERMO() {
    if (CD_ENFERMO == null) {
      CD_ENFERMO = "";
    }
    return CD_ENFERMO;
  }

  public String getCD_TDOC() {
    if (CD_TDOC == null) {
      CD_TDOC = "";
    }
    return CD_TDOC;
  }

  public String getDS_APE1() {
    if (DS_APE1 == null) {
      DS_APE1 = "";
    }
    return DS_APE1;
  }

  public String getDS_APE2() {
    if (DS_APE2 == null) {
      DS_APE2 = "";
    }
    return DS_APE2;
  }

  public String getDS_NOMBRE() {
    if (DS_NOMBRE == null) {
      DS_NOMBRE = "";
    }
    return DS_NOMBRE;
  }

  public String getDS_FONOAPE1() {
    if (DS_FONOAPE1 == null) {
      DS_FONOAPE1 = "";
    }
    return DS_FONOAPE1;
  }

  public String getDS_FONOAPE2() {
    if (DS_FONOAPE2 == null) {
      DS_FONOAPE2 = "";
    }
    return DS_FONOAPE2;
  }

  public String getDS_FONONOMBRE() {
    if (DS_FONONOMBRE == null) {
      DS_FONONOMBRE = "";
    }
    return DS_FONONOMBRE;
  }

  public String getIT_CALC() {
    if (IT_CALC == null) {
      IT_CALC = "";
    }
    return IT_CALC;
  }

  public String getFC_NAC() {
    if (FC_NAC == null) {
      FC_NAC = "";
    }
    return FC_NAC;
  }

  public String getCD_SEXO() {
    if (CD_SEXO == null) {
      CD_SEXO = "";
    }
    return CD_SEXO;
  }

  public String getDS_TELEF() {
    if (DS_TELEF == null) {
      DS_TELEF = "";
    }
    return DS_TELEF;
  }

  public String getCD_NIVEL_1() {
    if (CD_NIVEL_1 == null) {
      CD_NIVEL_1 = "";
    }
    return CD_NIVEL_1;
  }

  public String getCD_NIVEL_2() {
    if (CD_NIVEL_2 == null) {
      CD_NIVEL_2 = "";
    }
    return CD_NIVEL_2;
  }

  public String getCD_ZBS() {
    if (CD_ZBS == null) {
      CD_ZBS = "";
    }
    return CD_ZBS;
  }

  public String getCD_OPE() {
    if (CD_OPE == null) {
      CD_OPE = "";
    }
    return CD_OPE;
  }

  public String getFC_ULTACT() {
    if (FC_ULTACT == null) {
      FC_ULTACT = "";
    }
    return FC_ULTACT;
  }

  public String getDS_NDOC() {
    if (DS_NDOC == null) {
      DS_NDOC = "";
    }
    return DS_NDOC;
  }

  public String getSIGLAS() {
    if (SIGLAS == null) {
      SIGLAS = "";
    }
    return SIGLAS;
  }

} //fin de la clase  Data
