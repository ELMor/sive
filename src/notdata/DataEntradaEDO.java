package notdata;

import java.io.Serializable;

public class DataEntradaEDO
    implements Serializable {

  public String sCod = "", sDes = "";
  public String sNivel1 = "", sNivel2 = "";
  public String sCodCentro = "", sDesCentro = "";
  public String sDato7 = "", sDato8 = "";
  public String sSem = "";
  public String sAno = "";
  public String CD_OPE = "";
  // ARG: Se a�ade FC_ALTA
  public String FC_ALTA = "";
  public String FC_ULTACT = "";

  public DataEntradaEDO() {

  }

  //mlmora entrada a servlet maestro
  public DataEntradaEDO(String codEquipo, String ano,
                        String sem, String n1, String n2) {
    sCod = codEquipo;
    sAno = ano;
    sSem = sem;
    sNivel1 = n1;
    sNivel2 = n2;
  }

  // ARG: entrada a servlet maestro con FC_ALTA.
  //      Se usara como fecha de notificacion !!!
  public DataEntradaEDO(String codEquipo, String ano, String sem,
                        String n1, String n2, String fc_alta) {
    sCod = codEquipo;
    sAno = ano;
    sSem = sem;
    sNivel1 = n1;
    sNivel2 = n2;
    FC_ALTA = fc_alta;
  }

  //usado en notutil
  public DataEntradaEDO(String cod) {
    sCod = cod;
  }

  //usado en servlet entradaedo
  public DataEntradaEDO(String cod, String des) {
    sCod = cod;
    sDes = des;
  }

  //usado en PanelEntradaEDO
  public DataEntradaEDO(String cod, String des, String nivel1) {

    sCod = cod;
    sDes = des;
    sNivel1 = nivel1;
  }

  //usado en PanelIndivCabecera
  public DataEntradaEDO(String cod, String des,
                        String nivel1, String nivel2) {

    sCod = cod;
    sDes = des;
    sNivel1 = nivel1;
    sNivel2 = nivel2;
  }

  //salida de servletMaestro
  public DataEntradaEDO(String cod, String des,
                        String nivel1, String nivel2,
                        String codcentro, String descentro,
                        String dato7, String dato8,
                        String cd_ope, String fc_ultact) {

    sCod = cod;
    sDes = des;
    sNivel1 = nivel1;
    sNivel2 = nivel2;
    sCodCentro = codcentro;
    sDesCentro = descentro;
    sDato7 = dato7;
    sDato8 = dato8;
    CD_OPE = cd_ope;
    FC_ULTACT = fc_ultact;
  }

  // ARG: salida de servletMaestro, se a�ade la fecha de alta, FC_ALTA
  public DataEntradaEDO(String cod, String des, String nivel1,
                        String nivel2, String codcentro, String descentro,
                        String dato7, String dato8, String cd_ope,
                        String fc_alta, String fc_ultact) {
    sCod = cod;
    sDes = des;
    sNivel1 = nivel1;
    sNivel2 = nivel2;
    sCodCentro = codcentro;
    sDesCentro = descentro;
    sDato7 = dato7;
    sDato8 = dato8;
    CD_OPE = cd_ope;
    FC_ALTA = fc_alta;
    FC_ULTACT = fc_ultact;
  }

  /*
     public DataEntradaEDO(String cod, String des,
                      String nivel1, String nivel2,
                      String codcentro, String descentro) {
       sCod = cod;
       sDes = des;
       sNivel1 = nivel1;
       sNivel2 = nivel2;
       sCodCentro = codcentro;
       sDesCentro = descentro;
     }
     public DataEntradaEDO(String cod, String des,
                      String nivel1, String nivel2,
                      String codcentro, String descentro,
                      String dato7) {
       sCod = cod;
       sDes = des;
       sNivel1 = nivel1;
       sNivel2 = nivel2;
       sCodCentro = codcentro;
       sDesCentro = descentro;
       sDato7 = dato7;
     }  */

  //gets----
  public String getCD_OPE() {
    return CD_OPE;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public String getFC_ALTA() {
    return FC_ALTA;
  }

  public String getCod() {
    return sCod;
  }

  public String getDes() {
    return sDes;
  }

  public String getNivel1() {
    return sNivel1;
  }

  public String getNivel2() {
    return sNivel2;
  }

  public String getCodCentro() {
    return sCodCentro;
  }

  public String getDesCentro() {
    return sDesCentro;
  }

  public String getSem() {
    return sSem;
  }

  public String getAno() {
    return sAno;
  }

  public String getDato7() {
    return sDato7;
  }

  public String getDato8() {
    return sDato8;
  }
}
