package notdata;

import java.io.Serializable;

//Estructura para SrvEDOIndiv
public class DataListaEDOIndiv
    implements Serializable {

  //entrada
  public String sCodEquipo = "";
  public String sAnno = "";
  public String sSemana = "";
  public String sFechaRecep = "";
  public String sFechaNotif = "";

  //salida
  public String sExpediente = "";
  public String sCodEnfermedad = "";
  public String sDesEnfermedad = "";
  public String sCodEnfermo = "";
  public String sIniciales = "";
  public String sFechaNacto = "";

  public DataListaEDOIndiv() {}

  public DataListaEDOIndiv(
      String codequipo,
      String anno,
      String semana,
      String fcrecep,
      String fcnotif,
      String expediente,
      String codenfermedad,
      String desenferemedad,
      String codenfermo,
      String iniciales,
      String fcnacimiento) {

    sCodEquipo = codequipo;
    sAnno = anno;
    sSemana = semana;
    sFechaRecep = fcrecep;
    sFechaNotif = fcnotif;
    sExpediente = expediente;
    sCodEnfermedad = codenfermedad;
    sDesEnfermedad = desenferemedad;
    sCodEnfermo = codenfermo;
    sIniciales = iniciales;
    sFechaNacto = fcnacimiento;
  }

  public String getExpediente() {
    return sExpediente;
  }

  public String getCodEnfermedad() {
    return sCodEnfermedad;
  }

  public String getDesEnfermedad() {
    return sDesEnfermedad;
  }

  public String getCodEnfermo() {
    return sCodEnfermo;
  }

  public String getIniciales() {

    return sIniciales;
  }

  public String getFechaNacto() {
    return sFechaNacto;
  }

  public String getCodEquipo() {
    return sCodEquipo;
  }

  public String getAnno() {
    return sAnno;
  }

  public String getSemana() {
    return sSemana;
  }

  public String getFechaRecep() {
    return sFechaRecep;
  }

  public String getFechaNotif() {
    return sFechaNotif;
  }

}
