package notdata;

import java.io.Serializable;

public class DataIndivEnfermedad
    implements Serializable {

  public String CD_ENFCIE = "";
  public String CD_TVIGI = "";
  public String DS_PROCESO = "";
  public String IT_INDURG = "";

  public DataIndivEnfermedad(String codVigi, String CodEnfermedad,
                             String DesEnfermedad, String it_indurg) {
    CD_ENFCIE = CodEnfermedad;
    CD_TVIGI = codVigi;
    DS_PROCESO = DesEnfermedad;
    IT_INDURG = it_indurg;
  }

  public String getCD_ENFCIE() {
    return CD_ENFCIE;
  }

  public String getCD_TVIGI() {
    return CD_TVIGI;
  }

  public String getDS_PROCESO() {
    return DS_PROCESO;
  }

  public String getIT_INDURG() {
    return IT_INDURG;
  }
}
