package notdata;

import java.io.Serializable;

public class DataBorrarMasivo
    implements Serializable {

//notifedo
  public String CD_E_NOTIF = "";
  public String CD_ANOEPI = "";
  public String CD_SEMEPI = "";
  public String FC_RECEP = "";
  public String FC_FECNOTIF = "";

//notif_sem
  public String NM_NTOTREAL = "";
  public String CD_OPE = "";
  public String FC_ULTACT = "";

//flags
  public String sCOBERTURA = "";
  public String IT_ACTUALIZAR_NOTIF_SEM = "";

  public DataBorrarMasivo(String sCobertura,
                          String it_actualizar_notif_sem,
                          String cd_e_notif, String cd_anoepi, String cd_semepi,
                          String fc_recep, String fc_fecnotif,
                          String nm_ntotreal, String cd_ope, String fc_ultact) {

    sCOBERTURA = sCobertura;
    IT_ACTUALIZAR_NOTIF_SEM = it_actualizar_notif_sem;
    CD_E_NOTIF = cd_e_notif;
    CD_ANOEPI = cd_anoepi;
    CD_SEMEPI = cd_semepi;
    FC_RECEP = fc_recep;
    FC_FECNOTIF = fc_fecnotif;
    NM_NTOTREAL = nm_ntotreal;

    CD_OPE = cd_ope;
    FC_ULTACT = fc_ultact;

  }

  public String getsCOBERTURA() {
    return sCOBERTURA;
  }

  public String getIT_ACTUALIZAR_NOTIF_SEM() {
    return IT_ACTUALIZAR_NOTIF_SEM;
  }

  public String getCD_E_NOTIF() {
    return CD_E_NOTIF;
  }

  public String getCD_ANOEPI() {
    return CD_ANOEPI;
  }

  public String getCD_SEMEPI() {
    return CD_SEMEPI;
  }

  public String getCD_OPE() {
    return CD_OPE;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public String getFC_RECEP() {
    return FC_RECEP;
  }

  public String getFC_FECNOTIF() {
    return FC_FECNOTIF;
  }

  public String getNM_NTOTREAL() {
    return NM_NTOTREAL;
  }
} //fin Class
