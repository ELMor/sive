package notdata;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

//Estructura para sive_enfermo
public class DataEnfermo
    implements Serializable {

  public String CD_ENFERMO = null;
//public String CD_PROV= null;
//public String CD_PAIS = null;
//public String CD_MUN = null;
  public String CD_TDOC = null;
//public String CD_VIA   = null;
//public String CD_VIAL = null; //residuo
  public String DS_APE1 = null;
  public String DS_APE2 = null;
  public String DS_NOMBRE = null;
  public String DS_FONOAPE1 = null;
  public String DS_FONOAPE2 = null;
  public String DS_FONONOMBRE = null;
  public String IT_CALC = null;
  public String FC_NAC = null; //15
  public String CD_SEXO = null;
//public String CD_POSTAL = null;
//public String DS_DIREC = null;
//public String DS_NUM   = null;
//public String DS_PISO   = null;
  public String DS_TELEF = null;
//public String CD_NIVEL_1    = null;
//public String CD_NIVEL_2     = null;
//public String CD_ZBS   = null;
  public String CD_OPE = null;
  public String FC_ULTACT = null;
  public String IT_REVISADO = null;
  public String CD_MOTBAJA = null;
  public String FC_BAJA = null;
  public String DS_NDOC = null;
  public String SIGLAS = null; //31

//descripciones: enfermo

//public String DS_VIAL = null;
//public String DS_VIALITE = null;

//constructor para enfermo
  public DataEnfermo(String cd_enfermo,
                     String cd_tdoc,
                     String ds_ape1, String ds_ape2,
                     String ds_nombre, String ds_fonoape1,
                     String ds_fonoape2, String ds_fononombre,
                     String it_calc, String fc_nac,
                     String cd_sexo, String ds_telef,
                     String cd_ope,
                     String fc_ultact, String it_revisado,
                     String cd_motbaja,
                     String fc_baja,
                     String ds_ndoc, String siglas) {

    CD_ENFERMO = cd_enfermo;
    CD_TDOC = cd_tdoc;
    DS_APE1 = ds_ape1;
    DS_APE2 = ds_ape2;
    DS_NOMBRE = ds_nombre;
    DS_FONOAPE1 = ds_fonoape1;
    DS_FONOAPE2 = ds_fonoape2;
    DS_FONONOMBRE = ds_fononombre;
    IT_CALC = it_calc;
    FC_NAC = fc_nac;
    CD_SEXO = cd_sexo;
    DS_TELEF = ds_telef;
    CD_OPE = cd_ope;
    FC_ULTACT = fc_ultact;
    IT_REVISADO = it_revisado;
    CD_MOTBAJA = cd_motbaja;
    FC_BAJA = fc_baja;
    DS_NDOC = ds_ndoc;
    SIGLAS = siglas;

  } //fin constructor enfermo

  public String getCD_ENFERMO() {
    return CD_ENFERMO;
  }

//public String getCD_PROV () { return CD_PROV ;}
//public String getCD_PAIS () { return CD_PAIS ;}
//public String getCD_MUN () { return CD_MUN ;}
  public String getCD_TDOC() {
    return CD_TDOC;
  }

//public String getCD_VIA   () { return CD_VIA ;}
//public String getCD_VIAL   () { return CD_VIAL ;}
  public String getDS_APE1() {
    return DS_APE1;
  }

  public String getDS_APE2() {
    return DS_APE2;
  }

  public String getDS_NOMBRE() {
    return DS_NOMBRE;
  }

  public String getDS_FONOAPE1() {
    return DS_FONOAPE1;
  }

  public String getDS_FONOAPE2() {
    return DS_FONOAPE2;
  }

  public String getDS_FONONOMBRE() {
    return DS_FONONOMBRE;
  }

  public String getIT_CALC() {
    return IT_CALC;
  }

  public String getFC_NAC() {
    return FC_NAC;
  }

  public String getCD_SEXO() {
    return CD_SEXO;
  }

  public String getDS_TELEF() {
    return DS_TELEF;
  }

//public String getCD_POSTAL () { return CD_POSTAL ;}
//public String getDS_DIREC () { return  DS_DIREC;}
//public String getDS_NUM   () { return  DS_NUM;}
//public String getDS_PISO   () { return  DS_PISO;}
//public String getDS_TELEF    () { return  DS_TELEF;}
//public String getCD_NIVEL_1    () { return  CD_NIVEL_1;}
//public String getCD_NIVEL_2     () { return CD_NIVEL_2 ;}
//public String getCD_ZBS   () { return  CD_ZBS;}
  public String getCD_OPE() {
    return CD_OPE;
  }

  public String getFC_ULTACT() {
    return FC_ULTACT;
  }

  public String getIT_REVISADO() {
    return IT_REVISADO;
  }

  public String getCD_MOTBAJA() {
    return CD_MOTBAJA;
  }

  public String getFC_BAJA() {
    return FC_BAJA;
  }

  public String getDS_NDOC() {
    return DS_NDOC;
  }

  public String getSIGLAS() {
    return SIGLAS;
  }

//public String getDS_VIAL () { return DS_VIAL ;}
//public String getDS_VIALITE () { return DS_VIALITE ;}

  private void writeObject(ObjectOutputStream s) throws IOException {
    s.defaultWriteObject();
  }

  private void readObject(ObjectInputStream s) throws IOException,
      ClassNotFoundException {
    s.defaultReadObject();
  }

} //FIN DE LA CLASE
