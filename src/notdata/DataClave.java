package notdata;

import java.io.Serializable;

public class DataClave
    implements Serializable {

  public String CD_E_NOTIF = "";
  public String CD_ANOEPI = "";
  public String CD_SEMEPI = "";
  public String FC_RECEP = "";
  public String FC_FECNOTIF = "";

  public DataClave(String cd_e_notif,
                   String cd_anoepi,
                   String cd_semepi,
                   String fc_recep,
                   String fc_fecnotif) {
    CD_E_NOTIF = cd_e_notif;
    CD_ANOEPI = cd_anoepi;
    CD_SEMEPI = cd_semepi;
    FC_RECEP = fc_recep;
    FC_FECNOTIF = fc_fecnotif;

  }

  public String getCD_E_NOTIF() {
    return CD_E_NOTIF;
  }

  public String getCD_ANOEPI() {
    return CD_ANOEPI;
  }

  public String getCD_SEMEPI() {
    return CD_SEMEPI;
  }

  public String getFC_RECEP() {
    return FC_RECEP;
  }

  public String getFC_FECNOTIF() {
    return FC_FECNOTIF;
  }
}