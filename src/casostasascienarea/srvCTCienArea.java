//Title:        Casos o Tasas por 100000 habitantes por Area
//Version:
//Copyright:    Copyright (c) 1998
//Author:
//Company:
//Description:  Consultas 9.3.10, 9.3.11 y 9.3.12

package casostasascienarea;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

// Para probar.
//import jdbcpool.*;

public class srvCTCienArea
    extends DBServlet {

  // modos de operaci�n
  final int servletCTCA = 0;
  final int servletCTAREA = 1;
  final int servletCTDIST = 2;

  protected CLista paramEntrada = null;
  Connection con = null;

  // jlt
  Vector nivelescondatos = new Vector();
  Vector nivelescondatosacu = new Vector();

  public static final String CD_PARALISIS = "357.0";

  public static String prov = "28";
  public static String ca = "";

  public static final String pobmun = "15000";
  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int lineas = 0;
    int iEstado = 0;

    //Datos de entrada accesibles desde todo el servlet
    paramEntrada = param;

    // objetos de datos
    CLista data = new CLista();
    Vector salida = new Vector();
    dataCons93 datos = null;
    parCons93 par = null;

    ///////
    Vector niveles2 = new Vector();
    Vector pobla2 = new Vector();
    ////////

    Integer totalReg = new Integer(0);

    String codsEnf = " ( ";

    // Casos de enfermedades con tratamiento especial
    // paralisis flaccida aguda (poblaci�n menor de 15 a�os)
    String strCD_ENFERMEDAD = null;
    final int TOPE_EDAD = 15;
    boolean haytasas = false;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    par = (parCons93) param.firstElement();

    //////////
    CLista listaca = new CLista();
    listaca = (CLista) param.elementAt(1);
    ca = (String) listaca.firstElement();
    /////////

    // jlt
    // obtenemos los niveles con datos para luego tratarlos
    nivelescondatos = (Vector) obtenerNiveles(con, st, rs, opmode, par);
    if (!par.casosTasas) {
      haytasas = true;

      //// calculo el valor de la provincia

//    prov = (String) calcularProv(con,st,rs, par);

      /////////////////////

      // modos de operaci�n
    }
    switch (opmode) {

      // casos o tasas por cien mil habitantes por area
      case servletCTCA:

        /*query = " select sum(a.nm_casos), a.cd_enfcie, a.cd_nivel_1 "
              + " from sive_resumen_edos a, sive_procesos b "
              + " where a.cd_anoepi = ? "
              + " and a.cd_semepi >= ? and a.cd_semepi <= ? "
              + " and a.cd_enfcie = b.cd_enfcie ";
                   for (int i =0;i<par.lista.size(); i++){ //cd_nivel_1 in ()
          if (i==0)
            if (i==par.lista.size()-1)
              query = query + " and ( a.cd_nivel_1 = ? ) ";
            else
              query = query + " and ( a.cd_nivel_1 = ? ";
          else if (i==par.lista.size()-1)
            query = query + " or a.cd_nivel_1 = ? ) ";
          else
            query = query + " or a.cd_nivel_1 = ? ";
                   }
                   if (!par.ultEnf.equals("")){
          query = query + " and a.cd_enfcie > ? "
                      + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 "
                      + " order by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 ";
                   }
                   else
          query = query + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 "
                        + " order by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 ";
                   //System.out.println("SRVCTCIENAREA: " + query);*/

        query = "select tab_1.sum_parcial,tab_1.enf, tab_1.niv1, "
            + " tab_2.sum_total from "
            + " (select sum(a.nm_casos) as sum_parcial, "
            + " a.cd_enfcie as enf, a.cd_nivel_1 as niv1, "
            + " b.ds_proceso as proc "
            + " from sive_resumen_edos a, sive_procesos b "
            + " where a.cd_anoepi = ? "
            + " and a.cd_semepi >= ? and a.cd_semepi <= ? "
            + " and a.cd_enfcie = b.cd_enfcie ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_1 in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( a.cd_nivel_1 = ? ) ";
            }
            else {
              query = query + " and ( a.cd_nivel_1 = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or a.cd_nivel_1 = ? ) ";
          }
          else {
            query = query + " or a.cd_nivel_1 = ? ";
          }
        }

//          if (!par.ultEnf.equals("")){
//            query = query + " and a.cd_enfcie > ? "
        //                       + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 ) tab_1, ";

//          }
//          else
        query = query +
            " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 ) tab_1,";

        // calculo los totales
        /*          query = query + "(select sum(c.nm_casos) as sum_total,c.cd_enfcie as enf "
                                + "from sive_resumen_edos c, sive_procesos d "
                                + "where c.cd_anoepi = ?  "
             + " and c.cd_semepi >= ? and c.cd_semepi <= ? and "
                                + "c.cd_enfcie = d.cd_enfcie ";
         */
        // modificaci�n para no mostrar los casos de otras provincias
        query = query +
            "(select sum(c.nm_casos) as sum_total,c.cd_enfcie as enf "
            + "from sive_resumen_edos c, sive_procesos d "
            + "where c.cd_anoepi = ?  "
            + " and c.cd_semepi >= ? and c.cd_semepi <= ? and "
            + "c.cd_enfcie = d.cd_enfcie  and"
            + " c.cd_prov = " + prov;

//          if (!par.ultEnf.equals("")){
//            query = query + " and c.cd_enfcie > ? "
//                        + " group by d.ds_proceso, c.cd_enfcie ) tab_2, ";

//          }
//          else
        query = query + " group by d.ds_proceso, c.cd_enfcie ) tab_2";

        query = query + " where tab_1.enf = tab_2.enf "
            + " order by tab_1.proc, tab_1.enf, tab_1.niv1 ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.semD);
        st.setString(3, par.semH);

        Hashtable elem = new Hashtable();
        //System.out.println("SRVCTCIENAREA: lista " + par.lista);
        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);
          //System.out.println("SRVCTCIENAREA: elemeto de la lista " + elem.toString());
          st.setString(i + 4, (String) elem.get("CD_NIVEL_1"));
        }

//          if (!par.ultEnf.equals("")){
//            st.setString(par.lista.size()+4, par.ultEnf);
//          }

        st.setString(par.lista.size() + 4, par.ano);
        st.setString(par.lista.size() + 5, par.semD);
        st.setString(par.lista.size() + 6, par.semH);
//          if (!par.ultEnf.equals("")){
//            st.setString(par.lista.size() + 8, par.ultEnf);
//          }

        // lanza la query
        rs = st.executeQuery();
        //System.out.println("SRVCTCIENAREA: despues de  st.executeQuery()");

        lineas = 1;
        iEstado = CLista.listaVACIA;

        String antEnf = "";
        Vector cols = new Vector();
        Vector parNC = new Vector();
        String enf = "";
        codsEnf = " ( ";

        while (rs.next()) {
          //control de tama�o
          // if ((lineas > DBServlet.maxSIZE)&&(!(par.bInformeCompleto))) {
          //   iEstado = CLista.listaINCOMPLETA;
          //   break;
          // }

          // control de estado
          // if (iEstado == CLista.listaVACIA)
          //   iEstado = CLista.listaLLENA;

          //enf = rs.getString("CD_ENFCIE");
          enf = rs.getString(2);

          if (enf.compareTo(par.ultEnf) <= 0) {
            continue;
          }

          codsEnf = codsEnf + "'" + enf + "',";

          if (antEnf.equals("")) { //para la primera
            antEnf = enf;
            //System.out.println("SRVCTCIENAREA: enfermedad " + enf);

          }
          if (!enf.equals(antEnf)) {
            //crear un data y ponerlo en la lista de salida
            //System.out.println("SRVCTCIENAREA: crear un data y ponerlo en la lista de salida " + cols);
            //trazaLog("llamada1 a rellenaActDataCons93");
            salida.addElement(rellenaActDataCons93(antEnf, cols));
            antEnf = enf;
            cols = new Vector();
            parNC = new Vector();
            lineas++;
          }

          //acumular
          parNC = new Vector();
          //parNC.addElement(rs.getString ("cd_nivel_1"));
          parNC.addElement(rs.getString(3));
          parNC.addElement(new Long(rs.getLong(1))); // parcial
          parNC.addElement(new Long(rs.getLong(4))); // total
          cols.addElement(parNC);
          //System.out.println("SRVCTCIENAREA: a�adido un parNC " + parNC.elementAt(0).toString() + " " + parNC.elementAt(1).toString());

        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        //AIC
        //codsEnf = codsEnf.substring(0, codsEnf.length()-1) + " ) ";
        if (codsEnf.length() > 0) {
          int tamanyo = codsEnf.length() - 1;
          codsEnf = codsEnf.substring(0, tamanyo) + " ) ";
        }

//          System.out.println(codsEnf);

        //meter la ultima
        //System.out.println("SRVCTCIENAREA: pongo el ultimo data en la lista de salida ");
        //if ((cols.size()!=0) && (lineas<=DBServlet.maxSIZE))
        if ( (cols.size() != 0)) {

          //trazaLog("llamada2 a rellenaActDataCons93");
          salida.addElement(rellenaActDataCons93(enf, cols));

        }
        if (salida.size() == 0) {
          break;
        }

        ////////////////////////////////////////////
        // CALCULO LOS ACUMULADOS

        // modificacion jlt 04/01/02
        // falta actualizarlo para distrito
        //aunque ellos no lo manejan
        nivelescondatosacu = (Vector) obtenerNivelesacu(con, st, rs, opmode,
            par, codsEnf);
        query = "select tab_1.sum_parcial,tab_1.enf, tab_1.niv1, "
            + " tab_2.sum_total from "
            + " (select sum(a.nm_casos) as sum_parcial, "
            + " a.cd_enfcie as enf, a.cd_nivel_1 as niv1, "
            + " b.ds_proceso as proc "
            + " from sive_resumen_edos a, sive_procesos b "
            + " where a.cd_anoepi = ? "
            + " and a.cd_semepi >= ? and a.cd_semepi <= ? "
            ///////  modificacion jlt 04/01/02
            + " and a.cd_enfcie in " + codsEnf

            + " and a.cd_enfcie = b.cd_enfcie ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_1 in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( a.cd_nivel_1 = ? ) ";
            }
            else {
              query = query + " and ( a.cd_nivel_1 = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or a.cd_nivel_1 = ? ) ";
          }
          else {
            query = query + " or a.cd_nivel_1 = ? ";
          }
        }

//          if (!par.ultEnf.equals("")){
//            query = query + " and a.cd_enfcie > ? "
        //                       + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 ) tab_1, ";

//          }
//          else
        query = query +
            " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 ) tab_1,";

        // calculo los totales
        /*          query = query + "(select sum(c.nm_casos) as sum_total,c.cd_enfcie as enf "
                                + "from sive_resumen_edos c, sive_procesos d "
                                + "where c.cd_anoepi = ?  "
             + " and c.cd_semepi >= ? and c.cd_semepi <= ? and "
                                + "c.cd_enfcie = d.cd_enfcie ";
         */
        // no calculo los casos de otras provincias
        query = query +
            "(select sum(c.nm_casos) as sum_total,c.cd_enfcie as enf "
            + "from sive_resumen_edos c, sive_procesos d "
            + "where c.cd_anoepi = ?  "
            + " and c.cd_semepi >= ? and c.cd_semepi <= ? and "
            + "c.cd_enfcie = d.cd_enfcie "
            + " and c.cd_prov = " + prov;

//          if (!par.ultEnf.equals("")){
//            query = query + " and c.cd_enfcie > ? "
//                        + " group by d.ds_proceso, c.cd_enfcie ) tab_2, ";

//          }
//          else
        query = query + " group by d.ds_proceso, c.cd_enfcie ) tab_2";

        query = query + " where tab_1.enf = tab_2.enf "
            + " order by tab_1.proc, tab_1.enf, tab_1.niv1 ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, "01");
        st.setString(3, par.semH);

        elem = new Hashtable();
        //System.out.println("SRVCTCIENAREA: lista " + par.lista);
        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);
          //System.out.println("SRVCTCIENAREA: elemeto de la lista " + elem.toString());
          st.setString(i + 4, (String) elem.get("CD_NIVEL_1"));
        }

//          if (!par.ultEnf.equals("")){
//            st.setString(par.lista.size()+4, par.ultEnf);
//          }

        st.setString(par.lista.size() + 4, par.ano);
        st.setString(par.lista.size() + 5, "01");
        st.setString(par.lista.size() + 6, par.semH);
//          if (!par.ultEnf.equals("")){
//            st.setString(par.lista.size() + 8, par.ultEnf);
//          }

        // lanza la query
        rs = st.executeQuery();
        //System.out.println("SRVCTCIENAREA: despues de  st.executeQuery()");

        lineas = 1;
        iEstado = CLista.listaVACIA;

        antEnf = "";
        cols = new Vector();
        parNC = new Vector();
        enf = "";

        while (rs.next()) {
          //control de tama�o
          // if ((lineas > DBServlet.maxSIZE)&&(!(par.bInformeCompleto))) {
          //   iEstado = CLista.listaINCOMPLETA;
          //   break;
          // }

          // control de estado
          // if (iEstado == CLista.listaVACIA)
          //   iEstado = CLista.listaLLENA;

          //enf = rs.getString("CD_ENFCIE");
          enf = rs.getString(2);

          // prepara la query para los acumulados
              /*          query = " select sum(a.nm_casos), a.cd_enfcie, a.cd_nivel_1 "
                          + " from sive_resumen_edos a, sive_procesos b "
                          + " where a.cd_anoepi = ? "
                          + " and a.cd_semepi >= ? and a.cd_semepi <= ? "
                          + " and a.cd_enfcie = b.cd_enfcie ";
                    dataCons93 dat = (dataCons93) salida.firstElement();
                    for (int i =0;i<par.lista.size(); i++){
                      if (i==0)
                        if (i==par.lista.size()-1)
                          query = query + " and  a.cd_nivel_1 = ?  ";
                        else
                          query = query + " and ( a.cd_nivel_1 = ? ";
                      else if (i==par.lista.size()-1)
                        query = query + " or a.cd_nivel_1 = ? ) ";
                      else
                        query = query + " or a.cd_nivel_1 = ? ";
                    }
                    if (!par.ultEnf.equals("")){
                      query = query + " and a.cd_enfcie > ? "
               + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 "
               + " order by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 ";
                    }
                    else
               query = query + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 "
               + " order by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 ";
                    //System.out.println("SRVCTCIENAREA: " + query);
                    st = con.prepareStatement(query);
                    // codigo
                    st.setString(1, par.ano);
                    st.setString(2, "01");
                    st.setString(3, par.semH);
// ****** Antiguo
//          for (int i =0;i<dat.COLS.size(); i++){
//            st.setString(i+4, (String)dat.COLS.elementAt(i));
//          }
                    elem = new Hashtable();
                    for (int i =0;i<par.lista.size(); i++){
                      elem = (Hashtable)par.lista.elementAt(i);
                      st.setString(i+4, (String)elem.get("CD_NIVEL_1"));
                    }
// ********* tambi�n antiguo ********
//          if (!par.ultEnf.equals(""))
//            st.setString(dat.COLS.size()+4, par.ultEnf);
                    // lanza la query
                    rs = st.executeQuery();
                    lineas = 1;
                    iEstado = CLista.listaVACIA;
                    antEnf = "";
                    cols = new Vector();
                    parNC = new Vector();
                    enf = "";
                    while (rs.next()) {
                      //control de tama�o
               if ((lineas > DBServlet.maxSIZE)&&(!(par.bInformeCompleto))) {
                        iEstado = CLista.listaINCOMPLETA;
                        break;
                      }
                      // control de estado
                      if (iEstado == CLista.listaVACIA)
                        iEstado = CLista.listaLLENA;
                      enf = rs.getString("CD_ENFCIE");*/

          if (enf.compareTo(par.ultEnf) <= 0) {
            continue;
          }

          if (antEnf.equals("")) { //para la primera
            antEnf = enf;

          }
          if (!enf.equals(antEnf)) {
            //crear un data y ponerlo en la lista de salida

            rellenaAcuDataCons93(antEnf, cols, salida);
            antEnf = enf;
            cols = new Vector();
            lineas++;
          }

          ////////
          // modificacion jlt
          //acumular
          parNC = new Vector();
          //parNC.addElement(rs.getString ("cd_nivel_1"));
          parNC.addElement(rs.getString(3));
          parNC.addElement(new Long(rs.getLong(1)));
          parNC.addElement(new Long(rs.getLong(4)));
          cols.addElement(parNC);

          ////////

        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        //meter la ultima
        //if ((cols.size()!=0) && (lineas<=DBServlet.maxSIZE))
        if ( (cols.size() != 0)) {
          rellenaAcuDataCons93(enf, cols, salida);

          /*
                    ////////////////////////
                    // modificacion jlt 26/12/2001
                    //calculo totales
                    query = " select sum(a.nm_casos), a.cd_enfcie "
                          + " from sive_resumen_edos a, sive_procesos b "
                          + " where a.cd_anoepi = ? "
                          + " and a.cd_semepi >= ? and a.cd_semepi <= ? "
                          + " and a.cd_enfcie = b.cd_enfcie and "
                          + " a.cd_enfcie in " + codsEnf;
                    if (!par.ultEnf.equals("")){
                      query = query + " and a.cd_enfcie > ? "
                                  + " group by b.ds_proceso, a.cd_enfcie "
                                  + " order by b.ds_proceso, a.cd_enfcie ";
                    }
                    else
                      query = query + " group by b.ds_proceso, a.cd_enfcie "
                                    + " order by b.ds_proceso, a.cd_enfcie ";
                    st = con.prepareStatement(query);
                    // codigo
                    st.setString(1, par.ano);
                    st.setString(2, par.semD);
                    st.setString(3, par.semH);
                    if (!par.ultEnf.equals("")){
                      st.setString(4, par.ultEnf);
                    }
                    // lanza la query
                    rs = st.executeQuery();
                    //System.out.println("SRVCTCIENAREA: despues de  st.executeQuery()");
                    lineas = 1;
                    iEstado = CLista.listaVACIA;
                    Vector ctotales = new Vector();
                    while (rs.next()) {
                      //control de tama�o
               if ((lineas > DBServlet.maxSIZE)&&(!(par.bInformeCompleto))) {
                        iEstado = CLista.listaINCOMPLETA;
                        break;
                      }
                      // control de estado
                      if (iEstado == CLista.listaVACIA)
                        iEstado = CLista.listaLLENA;
                      // relleno la hashtable con los totales
                      ctotales.addElement(new Long(rs.getLong (1)));
                    }
                    rs.close();
                    rs = null;
                    st.close();
                    st= null;
                    // relleno el vector de salida
                    for (int j=0;j<salida.size();j++) {
                      dataCons93 dat2 = (dataCons93) salida.elementAt(j);
                      dat2.ACTT = ((Long)ctotales.elementAt(j)).floatValue();
                    }
                    /////////////////////////
                    ////////////////////////
                    // modificacion jlt 26/12/2001
                    //calculo totales para acumulados
                    query = " select sum(a.nm_casos), a.cd_enfcie "
                          + " from sive_resumen_edos a, sive_procesos b "
                          + " where a.cd_anoepi = ? "
                          + " and a.cd_semepi >= '01' and a.cd_semepi <= ? "
                          + " and a.cd_enfcie = b.cd_enfcie and "
                          + " a.cd_enfcie in " + codsEnf;
                    if (!par.ultEnf.equals("")){
                      query = query + " and a.cd_enfcie > ? "
                                  + " group by b.ds_proceso, a.cd_enfcie "
                                  + " order by b.ds_proceso, a.cd_enfcie ";
                    }
                    else
                      query = query + " group by b.ds_proceso, a.cd_enfcie "
                                    + " order by b.ds_proceso, a.cd_enfcie ";
                    st = con.prepareStatement(query);
                    // codigo
                    st.setString(1, par.ano);
//          st.setString(2, par.semD);
                    st.setString(2, par.semH);
                    if (!par.ultEnf.equals("")){
                      st.setString(3, par.ultEnf);
                    }
                    // lanza la query
                    rs = st.executeQuery();
                    //System.out.println("SRVCTCIENAREA: despues de  st.executeQuery()");
                    lineas = 1;
                    iEstado = CLista.listaVACIA;
                    Vector ctotalesacu = new Vector();
                    while (rs.next()) {
                      //control de tama�o
               if ((lineas > DBServlet.maxSIZE)&&(!(par.bInformeCompleto))) {
                        iEstado = CLista.listaINCOMPLETA;
                        break;
                      }
                      // control de estado
                      if (iEstado == CLista.listaVACIA)
                        iEstado = CLista.listaLLENA;
                      // relleno la hashtable con los totales
                      ctotalesacu.addElement(new Long(rs.getLong (1)));
                    }
                    rs.close();
                    rs = null;
                    st.close();
                    st= null;
                    // relleno el vector de salida
                    for (int j=0;j<salida.size();j++) {
                      dataCons93 dat3 = (dataCons93) salida.elementAt(j);
               dat3.ACUT = ((Long)ctotalesacu.elementAt(j)).floatValue();
                    }
                    /////////////////////////
           */

          /**************************************************/
          /****************** POBLACION *********************/
          /**************************************************/
          ///////////////////
          // modificaci�n jlt
          // calculamos la poblaci�n pero solo para los municipios con
          // poblaci�n mayor de 15000 habitantes
        }
        if (par.hab15) {

          query = new String();
          query = "select (tab_pob.sum_pob - tab_res.sum_res) total, tab_pob.niv_pob as nivel_1 from "
              +
              " (select tab_area.cd_nivel_1 as niv_pob ,sum(nm_poblacion) as sum_pob "
              + " from sive_poblacion_ns tab_area "
              + " where tab_area.cd_anoepi in (select max(cd_anoepi) "
              + " from sive_poblacion_ns where cd_anoepi <= ?) ";

          for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_1 in ()
            if (i == 0) {
              if (i == par.lista.size() - 1) {
                query = query + " and ( tab_area.cd_nivel_1 = ? ) ";
              }
              else {
                query = query + " and ( tab_area.cd_nivel_1 = ? ";
              }
            }
            else if (i == par.lista.size() - 1) {
              query = query + " or tab_area.cd_nivel_1 = ? ) ";
            }
            else {
              query = query + " or tab_area.cd_nivel_1 = ? ";
            }
          }

              /*            query = query + " group by tab_area.cd_nivel_1) tab_pob "
                        + " , "
               + "(select tab_mun.cd_nivel_1 as niv_res ,sum(sum_pob) as sum_res "
                        + "from sive_municipio tab_mun, "
                        + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
                      + "from sive_poblacion_ng "
                      + " where cd_anoepi in (select max(cd_anoepi) "
                        + "   from sive_poblacion_ng where cd_anoepi <= ?) "
               + "   group by cd_mun having sum(nm_poblacion) <= " +  pobmun  + " ) tab_sum "
                        + " where tab_mun.cd_mun = tab_sum.cd_mun "
                        + " and tab_mun.cd_nivel_1 != null "
                        + " and tab_mun.cd_prov = " +  prov
                        + " group by tab_mun.cd_nivel_1) tab_res "
                        + " where tab_pob.niv_pob = tab_res.niv_res "
                        + " order by tab_pob.niv_pob ";
           */

          query = query + " group by tab_area.cd_nivel_1) tab_pob "
              + " , "
              +
              "(select tab_mun.cd_nivel_1 as niv_res ,sum(sum_pob) as sum_res "
              + "from sive_municipio tab_mun, "
              + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
              + "from sive_poblacion_ng "
              + " where cd_anoepi in (select max(cd_anoepi) "
              + "   from sive_poblacion_ng where cd_anoepi <= ?) "
              + "   group by cd_mun having sum(nm_poblacion) <= " + pobmun +
              " ) tab_sum "
              + " where tab_mun.cd_mun = tab_sum.cd_mun "
              + " and tab_mun.cd_prov = " + prov
              + " group by tab_mun.cd_nivel_1 "
              //////////
              + " UNION "
              +
              " select distinct cd_nivel_1 as niv_res , 0 as sum_res from sive_nivel1_s "
              + " MINUS "
              +
              " select distinct cd_nivel_1 as niv_res, 0 as sum_res from sive_municipio "
              /////////
              + " ) tab_res "
              + " where tab_pob.niv_pob = tab_res.niv_res "
              + " order by tab_pob.niv_pob ";

          st = con.prepareStatement(query);
          // codigo
          st.setString(1, par.ano);

          elem = null;
          Vector niveles = new Vector();

          Vector pobla = new Vector();

          elem = new Hashtable();
          for (int i = 0; i < par.lista.size(); i++) {
            elem = (Hashtable) par.lista.elementAt(i);
            st.setString(i + 2, (String) elem.get("CD_NIVEL_1"));
            niveles.insertElementAt( (String) elem.get("CD_NIVEL_1"), i);
            pobla.insertElementAt(new Long("-1"), i);
          }

          st.setString(par.lista.size() + 2, par.ano);

          // lanza la query
          rs = st.executeQuery();

          int ind1 = 0;
          String n1 = "";
          while (rs.next()) {
            n1 = rs.getString("nivel_1");
            ind1 = niveles.indexOf(n1);
            pobla.setElementAt(new Long(rs.getLong(1)), ind1);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //rellenarPoblacion(niveles, pobla, salida);
          rellenarPoblacionhab15(con, st, rs, salida, par, opmode, pobla);

        }
        else {

          // modificacion jlt recuperamos la poblacion del a�o m�ximo
          // que sea menor o igual que el seleccionado
          query = " select sum(nm_poblacion), cd_nivel_1 "
              + " from sive_poblacion_ns "
              + " where cd_anoepi in (select max(cd_anoepi) "
              + " from sive_poblacion_ns where cd_anoepi <= ?) ";

          /************ COMENTAMOS ESTO DE LA POBLACI�N **********
                    for (int i =0;i<dat.COLS.size(); i++){
                      if (i==0)
                        if (i==dat.COLS.size()-1)
                          query = query + " and  cd_nivel_1 = ?  ";
                        else
                          query = query + " and ( cd_nivel_1 = ? ";
                      else if (i==dat.COLS.size()-1)
                        query = query + " or cd_nivel_1 = ? ) ";
                      else
                        query = query + " or cd_nivel_1 = ? ";
                    }
               query = query + " group by cd_nivel_1 order by cd_nivel_1 ";
               ************************ FIN DEL C�DIGO ANTIGUO ********* 22-01-00 */
          /****** C�DIGO NUEVO ************/
          for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_1 in ()
            if (i == 0) {
              if (i == par.lista.size() - 1) {
                query = query + " and ( cd_nivel_1 = ? ) ";
              }
              else {
                query = query + " and ( cd_nivel_1 = ? ";
              }
            }
            else if (i == par.lista.size() - 1) {
              query = query + " or cd_nivel_1 = ? ) ";
            }
            else {
              query = query + " or cd_nivel_1 = ? ";
            }
          }
          /*if (!par.ultEnf.equals("")){
            query = query + " and cd_enfcie > ? "
                        + " group by cd_nivel_1 "
                        + " order by cd_nivel_1 ";
                     }
                     else */
          query = query + " group by cd_nivel_1 "
              + " order by cd_nivel_1 ";

          /********************************/
          //System.out.println("SRVCTCIENAREA: " + query);

          st = con.prepareStatement(query);
          // codigo
          st.setString(1, par.ano);

          elem = null;
          Vector niveles = new Vector();

          Vector pobla = new Vector();

          /************ COMENTAMOS ESTO DE LA POBLACI�N **********
                    for (int i =0;i<dat.COLS.size(); i++){
                      st.setString(i+2, (String)dat.COLS.elementAt(i));
               niveles.insertElementAt((String)dat.COLS.elementAt(i), i);
                      pobla.insertElementAt(new Long("-1"), i);
                    }
               ************************ FIN DEL C�DIGO ANTIGUO ********* 22-01-00 */
          /****** C�DIGO NUEVO ************/
          elem = new Hashtable();
          for (int i = 0; i < par.lista.size(); i++) {
            elem = (Hashtable) par.lista.elementAt(i);
            st.setString(i + 2, (String) elem.get("CD_NIVEL_1"));
            niveles.insertElementAt( (String) elem.get("CD_NIVEL_1"), i);
            pobla.insertElementAt(new Long("-1"), i);
          }
          /********************************/

          // lanza la query
          rs = st.executeQuery();

          int ind1 = 0;
          String n1 = "";
          while (rs.next()) {
            n1 = rs.getString("cd_nivel_1");
            ind1 = niveles.indexOf(n1);
            pobla.setElementAt(new Long(rs.getLong(1)), ind1);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //rellenarPoblacion(niveles, pobla, salida);
          rellenarPoblacion(con, st, rs, salida, par, opmode, pobla);

        } // fin del if de poblacion mayor de 15000 hab

        /*// Veamos pobla que tiene....
        for (int pep=0;pep<pobla.size();pep++)
         System.out.println("Poblaci�n nueva:"+pobla.elementAt(pep));
// C�digo para ver datos.... comentable..
        dataCons93 datillo = new dataCons93();
        for (int pepe=0;pepe<salida.size();pepe++) {
         datillo = (dataCons93)salida.elementAt(pepe);
         System.out.println("------------ "+pepe+" ------------");
         for (int p=0;p<datillo.COLS.size();p++)
           System.out.println("COLS "+datillo.COLS.elementAt(p));
         for (int p=0;p<datillo.ACTS.size();p++)
           System.out.println("ACTS "+datillo.ACTS.elementAt(p));
         for (int p=0;p<datillo.ACUS.size();p++)
           System.out.println("ACUS "+datillo.ACUS.elementAt(p));
         for (int p=0;p<datillo.POBS.size();p++)
           System.out.println("POBS "+datillo.POBS.elementAt(p));
        }
// Fin de las l�neas de programa borrables.*/

// *****************************************

       //rellenar los datos de poblacion en el primer data
       //   rellenarPoblacion(niveles, pobla, salida);

       if (!par.casosTasas) {

         calcularTasas(con, st, rs, salida, par, opmode);
       }

        //calcular el total de registros
        query = " select count(distinct cd_enfcie)  "
            + " from sive_resumen_edos "
            + " where cd_anoepi = ? "
            + " and cd_semepi >= ? and cd_semepi <= ? "
            + " and cd_enfcie in (select cd_enfcie "
            + " from sive_procesos) ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_1 in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( cd_nivel_1 = ? ) ";
            }
            else {
              query = query + " and ( cd_nivel_1 = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or cd_nivel_1 = ? ) ";
          }
          else {
            query = query + " or cd_nivel_1 = ? ";
          }
        }

        //System.out.println("SRVCTCIENAREA: " + query);

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.semD);
        st.setString(3, par.semH);

        elem = new Hashtable();
        //System.out.println("SRVCTCIENAREA: lista " + par.lista);
        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);
          //System.out.println("SRVCTCIENAREA: elemeto de la lista " + elem.toString());
          st.setString(i + 4, (String) elem.get("CD_NIVEL_1"));
        }

        // lanza la query
        rs = st.executeQuery();

        if (rs.next()) {
          totalReg = new Integer(rs.getInt(1));
        }
        rs.close();
        rs = null;
        st.close();
        st = null;
        break;
//--------------------------------------------------------------------
//--------------------------------------------------------------------
//--------------------------------------------------------------------
//--------------------------------------------------------------------

        // casos o tasas por cien mil habitantes por distrito
      case servletCTAREA:

        query = "select tab_1.sum_parcial,tab_1.enf, tab_1.niv2, "
            + " tab_2.sum_total from "
            + " (select sum(a.nm_casos) as sum_parcial, "
            + " a.cd_enfcie as enf, a.cd_nivel_2 as niv2, "
            + " b.ds_proceso as proc "
            + " from sive_resumen_edos a, sive_procesos b "
            + " where a.cd_anoepi = ? "
            + " and a.cd_semepi >= ? and a.cd_semepi <= ? "
            + " and a.cd_nivel_1 = ? "
            + " and a.cd_enfcie = b.cd_enfcie ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_1 in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( a.cd_nivel_2 = ? ) ";
            }
            else {
              query = query + " and ( a.cd_nivel_2 = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or a.cd_nivel_2 = ? ) ";
          }
          else {
            query = query + " or a.cd_nivel_2 = ? ";
          }
        }

//          if (!par.ultEnf.equals("")){
//            query = query + " and a.cd_enfcie > ? "
        //                       + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 ) tab_1, ";

//          }
//          else
        query = query +
            " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_2 ) tab_1,";

        // calculo los totales
        query = query +
            "(select sum(c.nm_casos) as sum_total,c.cd_enfcie as enf "
            + "from sive_resumen_edos c, sive_procesos d "
            + "where c.cd_anoepi = ?  "
            + " and c.cd_semepi >= ? and c.cd_semepi <= ? "
            + " and c.cd_nivel_1 = ? "
            + " and c.cd_enfcie = d.cd_enfcie ";

//          if (!par.ultEnf.equals("")){
//            query = query + " and c.cd_enfcie > ? "
//                        + " group by d.ds_proceso, c.cd_enfcie ) tab_2, ";

//          }
//          else
        query = query + " group by d.ds_proceso, c.cd_enfcie ) tab_2";

        query = query + " where tab_1.enf = tab_2.enf "
            + " order by tab_1.proc, tab_1.enf, tab_1.niv2 ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.semD);
        st.setString(3, par.semH);
        st.setString(4, par.area);

        elem = new Hashtable();
        //System.out.println("SRVCTCIENAREA: lista " + par.lista);
        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);
          //System.out.println("SRVCTCIENAREA: elemeto de la lista " + elem.toString());
          st.setString(i + 5, (String) elem.get("CD_NIVEL_2"));
        }

//          if (!par.ultEnf.equals("")){
//            st.setString(par.lista.size()+4, par.ultEnf);
//          }

        st.setString(par.lista.size() + 5, par.ano);
        st.setString(par.lista.size() + 6, par.semD);
        st.setString(par.lista.size() + 7, par.semH);
        st.setString(par.lista.size() + 8, par.area);
//          if (!par.ultEnf.equals("")){
//            st.setString(par.lista.size() + 8, par.ultEnf);
//          }

        // lanza la query
        rs = st.executeQuery();
        //System.out.println("SRVCTCIENAREA: despues de  st.executeQuery()");

        lineas = 1;
        iEstado = CLista.listaVACIA;

        antEnf = "";
        cols = new Vector();
        parNC = new Vector();
        enf = "";
        codsEnf = " ( ";
        while (rs.next()) {
          //control de tama�o
          // if ((lineas > DBServlet.maxSIZE)&&(!(par.bInformeCompleto))) {
          //   iEstado = CLista.listaINCOMPLETA;
          //   break;
          // }

          // control de estado
          // if (iEstado == CLista.listaVACIA)
          //   iEstado = CLista.listaLLENA;

          //enf = rs.getString("CD_ENFCIE");
          enf = rs.getString(2);

          if (enf.compareTo(par.ultEnf) <= 0) {
            continue;
          }

          codsEnf = codsEnf + "'" + enf + "',";

          if (antEnf.equals("")) { //para la primera
            antEnf = enf;
            //System.out.println("SRVCTCIENAREA: enfermedad " + enf);

          }
          if (!enf.equals(antEnf)) {
            //crear un data y ponerlo en la lista de salida
            //System.out.println("SRVCTCIENAREA: crear un data y ponerlo en la lista de salida " + cols);
            //trazaLog("llamada1 a rellenaActDataCons93");
            salida.addElement(rellenaActDataCons93(antEnf, cols));
            antEnf = enf;
            cols = new Vector();
            parNC = new Vector();
            lineas++;
          }

          //acumular
          parNC = new Vector();
          //parNC.addElement(rs.getString ("cd_nivel_1"));
          parNC.addElement(rs.getString(3));
          parNC.addElement(new Long(rs.getLong(1))); // parcial
          parNC.addElement(new Long(rs.getLong(4))); // total
          cols.addElement(parNC);
          //System.out.println("SRVCTCIENAREA: a�adido un parNC " + parNC.elementAt(0).toString() + " " + parNC.elementAt(1).toString());

        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        //AIC
        //codsEnf = codsEnf.substring(0, codsEnf.length()-1) + " ) ";
        if (codsEnf.length() > 0) {
          int tamanyo = codsEnf.length() - 1;
          codsEnf = codsEnf.substring(0, tamanyo) + " ) ";
        }

//          System.out.println(codsEnf);

        //meter la ultima
        //System.out.println("SRVCTCIENAREA: pongo el ultimo data en la lista de salida ");
        //if ((cols.size()!=0) && (lineas<=DBServlet.maxSIZE))
        if ( (cols.size() != 0)) {
          salida.addElement(rellenaActDataCons93(enf, cols));

        }
        if (salida.size() == 0) {
          break;
        }

        ////////////////////////////////////////////
        // CALCULO LOS ACUMULADOS
        nivelescondatosacu = (Vector) obtenerNivelesacu(con, st, rs, opmode,
            par, codsEnf);
        query = "select tab_1.sum_parcial,tab_1.enf, tab_1.niv2, "
            + " tab_2.sum_total from "
            + " (select sum(a.nm_casos) as sum_parcial, "
            + " a.cd_enfcie as enf, a.cd_nivel_2 as niv2, "
            + " b.ds_proceso as proc "
            + " from sive_resumen_edos a, sive_procesos b "
            + " where a.cd_anoepi = ? "
            + " and a.cd_semepi >= ? and a.cd_semepi <= ? "
            + " and a.cd_nivel_1 = ? "
            ///////  modificacion jlt 04/01/02
            + " and a.cd_enfcie in " + codsEnf

            + " and a.cd_enfcie = b.cd_enfcie ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_1 in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( a.cd_nivel_2 = ? ) ";
            }
            else {
              query = query + " and ( a.cd_nivel_2 = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or a.cd_nivel_2 = ? ) ";
          }
          else {
            query = query + " or a.cd_nivel_2 = ? ";
          }
        }

//          if (!par.ultEnf.equals("")){
//            query = query + " and a.cd_enfcie > ? "
        //                       + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 ) tab_1, ";

//          }
//          else
        query = query +
            " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_2 ) tab_1,";

        // calculo los totales
        query = query +
            "(select sum(c.nm_casos) as sum_total,c.cd_enfcie as enf "
            + "from sive_resumen_edos c, sive_procesos d "
            + "where c.cd_anoepi = ?  "
            + " and c.cd_semepi >= ? and c.cd_semepi <= ?  "
            + " and c.cd_nivel_1 = ? "
            + " and c.cd_enfcie = d.cd_enfcie ";

//          if (!par.ultEnf.equals("")){
//            query = query + " and c.cd_enfcie > ? "
//                        + " group by d.ds_proceso, c.cd_enfcie ) tab_2, ";

//          }
//          else
        query = query + " group by d.ds_proceso, c.cd_enfcie ) tab_2";

        query = query + " where tab_1.enf = tab_2.enf "
            + " order by tab_1.proc, tab_1.enf, tab_1.niv2 ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, "01");
        st.setString(3, par.semH);
        st.setString(4, par.area);

        elem = new Hashtable();
        //System.out.println("SRVCTCIENAREA: lista " + par.lista);
        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);
          //System.out.println("SRVCTCIENAREA: elemeto de la lista " + elem.toString());
          st.setString(i + 5, (String) elem.get("CD_NIVEL_2"));
        }

//          if (!par.ultEnf.equals("")){
//            st.setString(par.lista.size()+4, par.ultEnf);
//          }

        st.setString(par.lista.size() + 5, par.ano);
        st.setString(par.lista.size() + 6, "01");
        st.setString(par.lista.size() + 7, par.semH);
        st.setString(par.lista.size() + 8, par.area);
//          if (!par.ultEnf.equals("")){
//            st.setString(par.lista.size() + 8, par.ultEnf);
//          }

        // lanza la query
        rs = st.executeQuery();
        //System.out.println("SRVCTCIENAREA: despues de  st.executeQuery()");

        lineas = 1;
        iEstado = CLista.listaVACIA;

        antEnf = "";
        cols = new Vector();
        parNC = new Vector();
        enf = "";

        while (rs.next()) {
          //control de tama�o
          // if ((lineas > DBServlet.maxSIZE)&&(!(par.bInformeCompleto))) {
          //   iEstado = CLista.listaINCOMPLETA;
          //   break;
          // }

          // control de estado
          // if (iEstado == CLista.listaVACIA)
          //   iEstado = CLista.listaLLENA;

          //enf = rs.getString("CD_ENFCIE");
          enf = rs.getString(2);

          if (enf.compareTo(par.ultEnf) <= 0) {
            continue;
          }

          if (antEnf.equals("")) { //para la primera
            antEnf = enf;

          }
          if (!enf.equals(antEnf)) {
            //crear un data y ponerlo en la lista de salida

            rellenaAcuDataCons93(antEnf, cols, salida);
            antEnf = enf;
            cols = new Vector();
            lineas++;
          }

          ////////
          // modificacion jlt
          //acumular
          parNC = new Vector();
          //parNC.addElement(rs.getString ("cd_nivel_1"));
          parNC.addElement(rs.getString(3));
          parNC.addElement(new Long(rs.getLong(1)));
          parNC.addElement(new Long(rs.getLong(4)));
          cols.addElement(parNC);

          ////////

        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        //meter la ultima
        //if ((cols.size()!=0) && (lineas<=DBServlet.maxSIZE))
        if ( (cols.size() != 0)) {
          rellenaAcuDataCons93(enf, cols, salida);

          /*          // prepara la query para los actuales
               query = " select sum(a.nm_casos), a.cd_enfcie,a.cd_nivel_2 "
                          + " from sive_resumen_edos a, sive_procesos b"
                          + " where a.cd_anoepi = ? "
                          + " and a.cd_semepi >= ? and a.cd_semepi <= ? "
                          + " and a.cd_nivel_1 = ? "
                          + " and a.cd_enfcie = b.cd_enfcie ";
                    for (int i =0;i<par.lista.size(); i++){ //cd_nivel_2 in ()
                      if (i==0)
                        if (i==par.lista.size()-1)
                          query = query + " and ( a.cd_nivel_2 = ? ) ";
                        else
                          query = query + " and ( a.cd_nivel_2 = ? ";
                      else if (i==par.lista.size()-1)
                        query = query + " or a.cd_nivel_2 = ? ) ";
                      else
                        query = query + " or a.cd_nivel_2 = ? ";
                    }
                  if (!par.ultEnf.equals("")){
                      query = query + " and a.cd_enfcie > ? "
               + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_2 "
               + " order by b.ds_proceso, a.cd_enfcie, a.cd_nivel_2 ";
                    }
                    else
                      query = query + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_2 order by b.ds_proceso, a.cd_enfcie, a.cd_nivel_2 ";
                    st = con.prepareStatement(query);
                    // codigo
                    st.setString(1, par.ano);
                    st.setString(2, par.semD);
                    st.setString(3, par.semH);
                    st.setString(4, par.area);
                    elem = new Hashtable();
                    for (int i =0;i<par.lista.size(); i++){
                      elem = (Hashtable)par.lista.elementAt(i);
                      st.setString(i+5, (String)elem.get("CD_NIVEL_2"));
                    }
                    if (!par.ultEnf.equals("")){
                      st.setString(par.lista.size()+5, par.ultEnf);
                    }
                    // lanza la query
                    rs = st.executeQuery();
                    lineas = 1;
                    iEstado = CLista.listaVACIA;
                    antEnf = "";
                    cols = new Vector();
                    parNC = new Vector();
                    enf = "";
                    while (rs.next()) {
                      //control de tama�o
               if ((lineas > DBServlet.maxSIZE)&&(!(par.bInformeCompleto))) {
                        iEstado = CLista.listaINCOMPLETA;
                        break;
                      }
                      // control de estado
                      if (iEstado == CLista.listaVACIA)
                        iEstado = CLista.listaLLENA;
                      enf = rs.getString("CD_ENFCIE");
                      if (enf.compareTo(par.ultEnf)<=0)
                        continue;
                      codsEnf = codsEnf + "'" + enf + "',";
                      if (antEnf.equals("")) //para la primera
                        antEnf = enf;
                      if (!enf.equals(antEnf)){
                        //crear un data y ponerlo en la lista de salida
                        trazaLog("llamada3 a rellenaActDataCons93");
                        salida.addElement(rellenaActDataCons93(antEnf, cols));
                        antEnf = enf;
                        cols = new Vector();
                        lineas++;
                      }
                        //acumular
                        parNC = new Vector();
                        parNC.addElement(rs.getString ("cd_nivel_2"));
                        parNC.addElement(new Long(rs.getLong (1)));
                        cols.addElement(parNC);
                    }
                    rs.close();
                    rs = null;
                    st.close();
                    st= null;
                    //AIC
                    //codsEnf = codsEnf.substring(0, codsEnf.length()-1) + " ) ";
                    if(codsEnf.length() > 0)
                    {
                      int tamanyo = codsEnf.length() - 1;
                      codsEnf = codsEnf.substring(0, tamanyo) + " ) ";
                    }
                    //codsEnf = codsEnf.substring(0, codsEnf.length()) + " ) ";
                    //meter la ultima
                    if ((cols.size()!=0) && (lineas<=DBServlet.maxSIZE))
                      trazaLog("llamada4 a rellenaActDataCons93");
                      salida.addElement(rellenaActDataCons93(enf, cols));
                    if (salida.size()==0)
                      break;
                    // prepara la query para los acumulados
               query = " select sum(a.nm_casos), a.cd_enfcie, a.cd_nivel_2 "
                          + " from sive_resumen_edos a, sive_procesos b"
                          + " where a.cd_anoepi = ? "
                          + " and a.cd_semepi >= ? and a.cd_semepi <= ? "
                          + " and a.cd_nivel_1 = ? "
                          + " and a.cd_enfcie = b.cd_enfcie ";
                    dataCons93 dat = (dataCons93) salida.firstElement();
// CAMBIO Anho Nimo Garc�a. Para que funcione con varios distritos. 22-03-01
// C�digo nuevo 22-03-01
                    for (int i =0;i<par.lista.size(); i++){ //cd_nivel_2 in ()
                      if (i==0)
                        if (i==par.lista.size()-1)
                          query = query + " and ( a.cd_nivel_2 = ? ) ";
                        else
                          query = query + " and ( a.cd_nivel_2 = ? ";
                      else if (i==par.lista.size()-1)
                        query = query + " or a.cd_nivel_2 = ? ) ";
                      else
                        query = query + " or a.cd_nivel_2 = ? ";
                    }
                  if (!par.ultEnf.equals("")){
                      query = query + " and a.cd_enfcie > ? "
               + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_2 "
               + " order by b.ds_proceso, a.cd_enfcie, a.cd_nivel_2 ";
                    }
                    else
               query = query + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_2 "
               + " order by b.ds_proceso, a.cd_enfcie, a.cd_nivel_2 ";
// CAMBIO Anho Nimo Garc�a. Para que funcione con varios distritos. 23-03-01
                    st = con.prepareStatement(query);
                    // codigo
                    st.setString(1, par.ano);
                    st.setString(2, "01");
                    st.setString(3, par.semH);
                    st.setString(4, par.area);
// C�digo nuevo------ 22-03-01
                    for (int i =0;i<par.lista.size(); i++){
                      elem = (Hashtable)par.lista.elementAt(i);
                      st.setString(i+5, (String)elem.get("CD_NIVEL_2"));
                    }
                    if (!par.ultEnf.equals("")){
                      st.setString(par.lista.size()+5, par.ultEnf);
                    }
// Fin de c�digo nuevo
// Cambio Anho Nimo Garcia, 22-03-01
                    // lanza la query
                    rs = st.executeQuery();
                    lineas = 1;
                    iEstado = CLista.listaVACIA;
                    antEnf = "";
                    cols = new Vector();
                    parNC = new Vector();
                    enf = "";
                    while (rs.next()) {
                      //control de tama�o
               if ((lineas > DBServlet.maxSIZE)&&(!(par.bInformeCompleto))) {
                        iEstado = CLista.listaINCOMPLETA;
                        break;
                      }
                      // control de estado
                      if (iEstado == CLista.listaVACIA)
                        iEstado = CLista.listaLLENA;
                      enf = rs.getString("CD_ENFCIE");
                      if (enf.compareTo(par.ultEnf)<=0)
                        continue;
                      if (antEnf.equals("")) //para la primera
                        antEnf = enf;
                      if (!enf.equals(antEnf)){
                        //crear un data y ponerlo en la lista de salida
                        rellenaAcuDataCons93(antEnf, cols, salida);
                        antEnf = enf;
                        cols = new Vector();
                        lineas++;
                      }
                        // modificacion jlt
                        //acumular
                        parNC = new Vector();
                        parNC.addElement(rs.getString ("cd_nivel_2"));
                        parNC.addElement(new Long(rs.getLong (1)));
                        cols.addElement(parNC);
                    }
                    rs.close();
                    rs = null;
                    st.close();
                    st= null;
                    //meter la ultima
                    if ((cols.size()!=0) && (lineas<=DBServlet.maxSIZE))
                      rellenaAcuDataCons93(enf, cols, salida);
           */

          /**************************************************/
          /****************** POBLACION *********************/
          /**************************************************/

        }

        if (par.hab15) {

          query = new String();
          query = "select (tab_pob.sum_pob - tab_res.sum_res) total, tab_pob.niv_pob as nivel_2 from "
              +
              " (select tab_area.cd_nivel_2 as niv_pob ,sum(nm_poblacion) as sum_pob "
              + " from sive_poblacion_ns tab_area "
              + " where tab_area.cd_anoepi in (select max(cd_anoepi) "
              + " from sive_poblacion_ns where cd_anoepi <= ?) and "
              + " tab_area.cd_nivel_1 = ? ";

          for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_2 in ()
            if (i == 0) {
              if (i == par.lista.size() - 1) {
                query = query + " and ( tab_area.cd_nivel_2 = ? ) ";
              }
              else {
                query = query + " and ( tab_area.cd_nivel_2 = ? ";
              }
            }
            else if (i == par.lista.size() - 1) {
              query = query + " or tab_area.cd_nivel_2 = ? ) ";
            }
            else {
              query = query + " or tab_area.cd_nivel_2 = ? ";
            }
          }

              /*            query = query + " group by tab_area.cd_nivel_2) tab_pob "
                        + " , "
               + "(select tab_mun.cd_nivel_2 as niv_res ,sum(sum_pob) as sum_res "
                        + "from sive_municipio tab_mun, "
                        + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
                      + "from sive_poblacion_ng "
                      + " where cd_anoepi in (select max(cd_anoepi) "
                        + "   from sive_poblacion_ng where cd_anoepi <= ?) "
               + "   group by cd_mun having sum(nm_poblacion) <= 15000 ) tab_sum "
                        + " where tab_mun.cd_mun = tab_sum.cd_mun "
                        + " and tab_mun.cd_prov = '28' "
                        + " and tab_mun.cd_nivel_1 = ? "
                        + " group by tab_mun.cd_nivel_2) tab_res "
                        + " where tab_pob.niv_pob = tab_res.niv_res "
                        + " order by tab_pob.niv_pob ";
           */

          query = query + " group by tab_area.cd_nivel_2) tab_pob "
              + " , "
              +
              "(select tab_mun.cd_nivel_2 as niv_res ,sum(sum_pob) as sum_res "
              + "from sive_municipio tab_mun, "
              + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
              + "from sive_poblacion_ng "
              + " where cd_anoepi in (select max(cd_anoepi) "
              + "   from sive_poblacion_ng where cd_anoepi <= ?) "
              +
              "   group by cd_mun having sum(nm_poblacion) <= 15000 ) tab_sum "
              + " where tab_mun.cd_mun = tab_sum.cd_mun "
              //+ " and tab_mun.cd_prov = '28' "
              + " and tab_mun.cd_prov = " + prov
              + " and tab_mun.cd_nivel_1 = ? "
              + " group by tab_mun.cd_nivel_2 "
              ////////////
              + " UNION "
              +
              " select distinct cd_nivel_2 as niv_res , 0 as sum_res from sive_nivel2_s "
              + " where cd_nivel_1 = ? "
              + " MINUS "
              +
              " select distinct cd_nivel_2 as niv_res, 0 as sum_res from sive_municipio "
              + " where cd_nivel_1 = ? "
              ////////////
              + " ) tab_res "
              + " where tab_pob.niv_pob = tab_res.niv_res "
              + " order by tab_pob.niv_pob ";

          st = con.prepareStatement(query);
          // codigo
          st.setString(1, par.ano);

          st.setString(2, par.area);

          elem = null;
          Vector niveles = new Vector();

          Vector pobla = new Vector();

          elem = new Hashtable();
          for (int i = 0; i < par.lista.size(); i++) {
            elem = (Hashtable) par.lista.elementAt(i);
            st.setString(i + 3, (String) elem.get("CD_NIVEL_2"));
            niveles.insertElementAt( (String) elem.get("CD_NIVEL_2"), i);
            pobla.insertElementAt(new Long("-1"), i);
          }

          st.setString(par.lista.size() + 3, par.ano);

          st.setString(par.lista.size() + 4, par.area);
          st.setString(par.lista.size() + 5, par.area);
          st.setString(par.lista.size() + 6, par.area);

          // lanza la query
          rs = st.executeQuery();

          int ind1 = 0;
          String n1 = "";

          while (rs.next()) {
            n1 = rs.getString("nivel_2");
            ind1 = niveles.indexOf(n1);
            pobla.setElementAt(new Long(rs.getLong(1)), ind1);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //rellenarPoblacion(niveles, pobla, salida);
          rellenarPoblacionhab15(con, st, rs, salida, par, opmode, pobla);

        }
        else {

          // modificacion jlt recuperamos la poblacion del a�o m�ximo
          // que sea menor o igual que el seleccionado
          query = " select sum(nm_poblacion), cd_nivel_2 "
              + " from sive_poblacion_ns "
              + " where cd_anoepi in (select max(cd_anoepi)  "
              + " from sive_poblacion_ns where cd_anoepi <= ?) "
              + " and cd_nivel_1 = ? ";

          /**************** Comentado el 23-03-01.
                    for (int i =0;i<dat.COLS.size(); i++){
                      if (i==0)
                        if (i==dat.COLS.size()-1)
                          query = query + " and  cd_nivel_2 = ?  ";
                        else
                          query = query + " and ( cd_nivel_2 = ? ";
                      else if (i==dat.COLS.size()-1)
                        query = query + " or cd_nivel_2 = ? ) ";
                      else
                        query = query + " or cd_nivel_2 = ? ";
                    }
               query = query + " group by cd_nivel_2 order by cd_nivel_2 ";
           ****************/
          /**************** C�digo nuevo ********************/

          for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_2 in ()
            if (i == 0) {
              if (i == par.lista.size() - 1) {
                query = query + " and ( cd_nivel_2 = ? ) ";
              }
              else {
                query = query + " and ( cd_nivel_2 = ? ";
              }
            }
            else if (i == par.lista.size() - 1) {
              query = query + " or cd_nivel_2 = ? ) ";
            }
            else {
              query = query + " or cd_nivel_2 = ? ";
            }
          }

          /*if (!par.ultEnf.equals("")){
            query = query + " and cd_enfcie > ? "
                        + " group by cd_nivel_2 "
                        + " order by cd_nivel_2 ";
                     }
                     else*/
          query = query + " group by cd_nivel_2 "
              + " order by cd_nivel_2 ";

          /********* Fin del c�digo nuevo *******************/
          //System.out.println("SRVCTCIENAREA: " + query);

          st = con.prepareStatement(query);
          // codigo
          st.setString(1, par.ano);
          st.setString(2, par.area);

          elem = null;
          Vector niveles = new Vector();
          Vector pobla = new Vector();
          /************* Comentado el 23-03-01 **************/
          /*          for (int i =0;i<dat.COLS.size(); i++){
                      st.setString(i+3, (String)dat.COLS.elementAt(i));
               niveles.insertElementAt((String)dat.COLS.elementAt(i), i);
                      pobla.insertElementAt(new Long("-1"), i);
                    }*/
          /************* C�digo nuevo **********************/
          for (int i = 0; i < par.lista.size(); i++) {
            elem = (Hashtable) par.lista.elementAt(i);
            st.setString(i + 3, (String) elem.get("CD_NIVEL_2"));
            niveles.insertElementAt( (String) elem.get("CD_NIVEL_2"), i);
            pobla.insertElementAt(new Long("-1"), i);
          }
          /**************Fin c�digo nuevo*********************/

          // lanza la query
          rs = st.executeQuery();

          int ind = 0;
          String n1 = "";
          while (rs.next()) {
            n1 = rs.getString("cd_nivel_2");
            ind = niveles.indexOf(n1);
            pobla.setElementAt(new Long(rs.getLong(1)), ind);

          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //rellenarPoblacion(niveles, pobla, salida);
          rellenarPoblacion(con, st, rs, salida, par, opmode, pobla);

        } // if de la poblacion

        //rellenar los datos de poblacion en el primer data
        //rellenarPoblacion(niveles, pobla, salida);

        /*// C�digo para ver datos.... comentable..
        dataCons93 datillo = new dataCons93();
        for (int pepe=0;pepe<salida.size();pepe++) {
         datillo = (dataCons93)salida.elementAt(pepe);
         System.out.println("------------ "+pepe+" ------------");
         for (int p=0;p<datillo.COLS.size();p++)
           System.out.println("COLS "+datillo.COLS.elementAt(p));
         for (int p=0;p<datillo.ACTS.size();p++)
           System.out.println("ACTS "+datillo.ACTS.elementAt(p));
         for (int p=0;p<datillo.ACUS.size();p++)
           System.out.println("ACUS "+datillo.ACUS.elementAt(p));
         for (int p=0;p<datillo.POBS.size();p++)
           System.out.println("POBS "+datillo.POBS.elementAt(p));
        }
// Fin de las l�neas de programa borrables.*/

       if (!par.casosTasas) {
         calcularTasas(con, st, rs, salida, par, opmode);
       }

        query = " select count(distinct cd_enfcie) "
            + " from sive_resumen_edos "
            + " where cd_anoepi = ? "
            + " and cd_semepi >= ? and cd_semepi <= ? "
            + " and cd_enfcie in (select cd_enfcie "
            + " from sive_procesos) "
            + " and cd_nivel_1 = ? ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_2 in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( cd_nivel_2 = ? ) ";
            }
            else {
              query = query + " and ( cd_nivel_2 = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or cd_nivel_2 = ? ) ";
          }
          else {
            query = query + " or cd_nivel_2 = ? ";
          }
        }

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.semD);
        st.setString(3, par.semH);
        st.setString(4, par.area);

        elem = new Hashtable();
        //System.out.println("SRVCTCIENAREA: lista " + par.lista);
        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);
          //System.out.println("SRVCTCIENAREA: elemeto de la lista " + elem.toString());
          st.setString(i + 5, (String) elem.get("CD_NIVEL_2"));
        }

        // lanza la query
        rs = st.executeQuery();

        if (rs.next()) {
          totalReg = new Integer(rs.getInt(1));
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

//--------------------------------------------------------------------
//--------------------------------------------------------------------
//--------------------------------------------------------------------
//--------------------------------------------------------------------

        // casos o tasas por cien mil habitantes por zbs
      case servletCTDIST:

        query = "select tab_1.sum_parcial,tab_1.enf, tab_1.zbs, "
            + " tab_2.sum_total from "
            + " (select sum(a.nm_casos) as sum_parcial, "
            + " a.cd_enfcie as enf, a.cd_zbs as zbs, "
            + " b.ds_proceso as proc "
            + " from sive_resumen_edos a, sive_procesos b "
            + " where a.cd_anoepi = ? "
            + " and a.cd_semepi >= ? and a.cd_semepi <= ? "
            + " and a.cd_nivel_1 = ? "
            + " and a.cd_nivel_2 = ? "
            + " and a.cd_enfcie = b.cd_enfcie ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_1 in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( a.cd_zbs = ? ) ";
            }
            else {
              query = query + " and ( a.cd_zbs = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or a.cd_zbs = ? ) ";
          }
          else {
            query = query + " or a.cd_zbs = ? ";
          }
        }

//          if (!par.ultEnf.equals("")){
//            query = query + " and a.cd_enfcie > ? "
        //                       + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 ) tab_1, ";

//          }
//          else
        query = query +
            " group by b.ds_proceso, a.cd_enfcie, a.cd_zbs ) tab_1,";

        // calculo los totales
        query = query +
            "(select sum(c.nm_casos) as sum_total,c.cd_enfcie as enf "
            + "from sive_resumen_edos c, sive_procesos d "
            + "where c.cd_anoepi = ?  "
            + " and c.cd_semepi >= ? and c.cd_semepi <= ? "
            + " and c.cd_nivel_1 = ? "
            + " and c.cd_nivel_2 = ? "
            + " and c.cd_enfcie = d.cd_enfcie ";

//          if (!par.ultEnf.equals("")){
//            query = query + " and c.cd_enfcie > ? "
//                        + " group by d.ds_proceso, c.cd_enfcie ) tab_2, ";

//          }
//          else
        query = query + " group by d.ds_proceso, c.cd_enfcie ) tab_2";

        query = query + " where tab_1.enf = tab_2.enf "
            + " order by tab_1.proc, tab_1.enf, tab_1.zbs ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.semD);
        st.setString(3, par.semH);
        st.setString(4, par.area);
        st.setString(5, par.distrito);

        elem = new Hashtable();
        //System.out.println("SRVCTCIENAREA: lista " + par.lista);
        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);
          //System.out.println("SRVCTCIENAREA: elemeto de la lista " + elem.toString());
          st.setString(i + 6, (String) elem.get("CD_ZBS"));
        }

//          if (!par.ultEnf.equals("")){
//            st.setString(par.lista.size()+4, par.ultEnf);
//          }

        st.setString(par.lista.size() + 6, par.ano);
        st.setString(par.lista.size() + 7, par.semD);
        st.setString(par.lista.size() + 8, par.semH);
        st.setString(par.lista.size() + 9, par.area);
        st.setString(par.lista.size() + 10, par.distrito);
//          if (!par.ultEnf.equals("")){
//            st.setString(par.lista.size() + 8, par.ultEnf);
//          }

        // lanza la query
        rs = st.executeQuery();
        //System.out.println("SRVCTCIENAREA: despues de  st.executeQuery()");

        lineas = 1;
        iEstado = CLista.listaVACIA;

        antEnf = "";
        cols = new Vector();
        parNC = new Vector();
        enf = "";
        codsEnf = " ( ";
        while (rs.next()) {
          //control de tama�o
          // if ((lineas > DBServlet.maxSIZE)&&(!(par.bInformeCompleto))) {
          //   iEstado = CLista.listaINCOMPLETA;
          //   break;
          // }

          // control de estado
          // if (iEstado == CLista.listaVACIA)
          //   iEstado = CLista.listaLLENA;

          //enf = rs.getString("CD_ENFCIE");
          enf = rs.getString(2);

          if (enf.compareTo(par.ultEnf) <= 0) {
            continue;
          }

          codsEnf = codsEnf + "'" + enf + "',";

          if (antEnf.equals("")) { //para la primera
            antEnf = enf;
            //System.out.println("SRVCTCIENAREA: enfermedad " + enf);

          }
          if (!enf.equals(antEnf)) {
            //crear un data y ponerlo en la lista de salida
            //System.out.println("SRVCTCIENAREA: crear un data y ponerlo en la lista de salida " + cols);
            //trazaLog("llamada1 a rellenaActDataCons93");
            salida.addElement(rellenaActDataCons93(antEnf, cols));
            antEnf = enf;
            cols = new Vector();
            parNC = new Vector();
            lineas++;
          }

          //acumular
          parNC = new Vector();
          //parNC.addElement(rs.getString ("cd_nivel_1"));
          parNC.addElement(rs.getString(3));
          parNC.addElement(new Long(rs.getLong(1))); // parcial
          parNC.addElement(new Long(rs.getLong(4))); // total
          cols.addElement(parNC);
          //System.out.println("SRVCTCIENAREA: a�adido un parNC " + parNC.elementAt(0).toString() + " " + parNC.elementAt(1).toString());

        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        //AIC
        //codsEnf = codsEnf.substring(0, codsEnf.length()-1) + " ) ";
        if (codsEnf.length() > 0) {
          int tamanyo = codsEnf.length() - 1;
          codsEnf = codsEnf.substring(0, tamanyo) + " ) ";
        }

//          System.out.println(codsEnf);

        //meter la ultima
        //System.out.println("SRVCTCIENAREA: pongo el ultimo data en la lista de salida ");
        //if ((cols.size()!=0) && (lineas<=DBServlet.maxSIZE))
        if ( (cols.size() != 0)) {
          salida.addElement(rellenaActDataCons93(enf, cols));

        }
        if (salida.size() == 0) {
          break;
        }

        ////////////////////////////////////////////
        // CALCULO LOS ACUMULADOS
        nivelescondatosacu = (Vector) obtenerNivelesacu(con, st, rs, opmode,
            par, codsEnf);
        query = "select tab_1.sum_parcial,tab_1.enf, tab_1.zbs, "
            + " tab_2.sum_total from "
            + " (select sum(a.nm_casos) as sum_parcial, "
            + " a.cd_enfcie as enf, a.cd_zbs as zbs, "
            + " b.ds_proceso as proc "
            + " from sive_resumen_edos a, sive_procesos b "
            + " where a.cd_anoepi = ? "
            + " and a.cd_semepi >= ? and a.cd_semepi <= ? "
            + " and a.cd_nivel_1 = ? "
            + " and a.cd_nivel_2 = ? "
            ///////  modificacion jlt 04/01/02
            + " and a.cd_enfcie in " + codsEnf

            + " and a.cd_enfcie = b.cd_enfcie ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_1 in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( a.cd_zbs = ? ) ";
            }
            else {
              query = query + " and ( a.cd_zbs = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or a.cd_zbs = ? ) ";
          }
          else {
            query = query + " or a.cd_zbs = ? ";
          }
        }

//          if (!par.ultEnf.equals("")){
//            query = query + " and a.cd_enfcie > ? "
        //                       + " group by b.ds_proceso, a.cd_enfcie, a.cd_nivel_1 ) tab_1, ";

//          }
//          else
        query = query +
            " group by b.ds_proceso, a.cd_enfcie, a.cd_zbs ) tab_1,";

        // calculo los totales
        query = query +
            "(select sum(c.nm_casos) as sum_total,c.cd_enfcie as enf "
            + "from sive_resumen_edos c, sive_procesos d "
            + "where c.cd_anoepi = ?  "
            + " and c.cd_semepi >= ? and c.cd_semepi <= ?  "
            + " and c.cd_nivel_1 = ? "
            + " and c.cd_nivel_2 = ? "
            + " and c.cd_enfcie = d.cd_enfcie ";

//          if (!par.ultEnf.equals("")){
//            query = query + " and c.cd_enfcie > ? "
//                        + " group by d.ds_proceso, c.cd_enfcie ) tab_2, ";

//          }
//          else
        query = query + " group by d.ds_proceso, c.cd_enfcie ) tab_2";

        query = query + " where tab_1.enf = tab_2.enf "
            + " order by tab_1.proc, tab_1.enf, tab_1.zbs ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, "01");
        st.setString(3, par.semH);
        st.setString(4, par.area);
        st.setString(5, par.distrito);

        elem = new Hashtable();
        //System.out.println("SRVCTCIENAREA: lista " + par.lista);
        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);
          //System.out.println("SRVCTCIENAREA: elemeto de la lista " + elem.toString());
          st.setString(i + 6, (String) elem.get("CD_ZBS"));
        }

//          if (!par.ultEnf.equals("")){
//            st.setString(par.lista.size()+4, par.ultEnf);
//          }

        st.setString(par.lista.size() + 6, par.ano);
        st.setString(par.lista.size() + 7, "01");
        st.setString(par.lista.size() + 8, par.semH);
        st.setString(par.lista.size() + 9, par.area);
        st.setString(par.lista.size() + 10, par.distrito);
//          if (!par.ultEnf.equals("")){
//            st.setString(par.lista.size() + 8, par.ultEnf);
//          }

        // lanza la query
        rs = st.executeQuery();
        //System.out.println("SRVCTCIENAREA: despues de  st.executeQuery()");

        lineas = 1;
        iEstado = CLista.listaVACIA;

        antEnf = "";
        cols = new Vector();
        parNC = new Vector();
        enf = "";

        while (rs.next()) {
          //control de tama�o
          // if ((lineas > DBServlet.maxSIZE)&&(!(par.bInformeCompleto))) {
          //   iEstado = CLista.listaINCOMPLETA;
          //   break;
          // }

          // control de estado
          // if (iEstado == CLista.listaVACIA)
          //   iEstado = CLista.listaLLENA;

          //enf = rs.getString("CD_ENFCIE");
          enf = rs.getString(2);

          if (enf.compareTo(par.ultEnf) <= 0) {
            continue;
          }

          if (antEnf.equals("")) { //para la primera
            antEnf = enf;

          }
          if (!enf.equals(antEnf)) {
            //crear un data y ponerlo en la lista de salida

            rellenaAcuDataCons93(antEnf, cols, salida);
            antEnf = enf;
            cols = new Vector();
            lineas++;
          }

          ////////
          // modificacion jlt
          //acumular
          parNC = new Vector();
          //parNC.addElement(rs.getString ("cd_nivel_1"));
          parNC.addElement(rs.getString(3));
          parNC.addElement(new Long(rs.getLong(1)));
          parNC.addElement(new Long(rs.getLong(4)));
          cols.addElement(parNC);

          ////////

        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        //meter la ultima
        //if ((cols.size()!=0) && (lineas<=DBServlet.maxSIZE))
        if ( (cols.size() != 0)) {
          rellenaAcuDataCons93(enf, cols, salida);

          /*          // prepara la query para los actuales
                    query = " select sum(a.nm_casos), a.cd_enfcie,a.cd_zbs "
                          + " from sive_resumen_edos a, sive_procesos b"
                          + " where a.cd_anoepi = ? "
                          + " and a.cd_semepi >= ? and a.cd_semepi <= ? "
                          + " and a.cd_nivel_1 = ? "
                          + " and a.cd_nivel_2 = ? "
                          + " and a.cd_enfcie = b.cd_enfcie ";
                    for (int i =0;i<par.lista.size(); i++){ //cd_zbs in ()
                      if (i==0)
                        if (i==par.lista.size()-1)
                          query = query + " and ( a.cd_zbs = ? ) ";
                        else
                          query = query + " and ( a.cd_zbs = ? ";
                      else if (i==par.lista.size()-1)
                        query = query + " or a.cd_zbs = ? ) ";
                      else
                        query = query + " or a.cd_zbs = ? ";
                    }
                    if (!par.ultEnf.equals("")){
                      query = query + " and a.cd_enfcie > ? "
               + " group by b.ds_proceso, a.cd_enfcie, a.cd_zbs "
               + " order by b.ds_proceso, a.cd_enfcie, a.cd_zbs ";
                    }
                    else
                      query = query + " group by b.ds_proceso, a.cd_enfcie, a.cd_zbs order by b.ds_proceso, a.cd_enfcie, a.cd_zbs ";
                    st = con.prepareStatement(query);
                    // codigo
                    st.setString(1, par.ano);
                    st.setString(2, par.semD);
                    st.setString(3, par.semH);
                    st.setString(4, par.area);
                    st.setString(5, par.distrito);
                    elem = new Hashtable();
                    for (int i =0;i<par.lista.size(); i++){
                      elem = (Hashtable)par.lista.elementAt(i);
                      st.setString(i+6, (String)elem.get("CD_ZBS"));
                    }
                    if (!par.ultEnf.equals("")){
                      st.setString(par.lista.size()+6, par.ultEnf);
                    }
                    // lanza la query
                    rs = st.executeQuery();
                    lineas = 1;
                    iEstado = CLista.listaVACIA;
                    antEnf = "";
                    cols = new Vector();
                    parNC = new Vector();
                    enf = "";
                    while (rs.next()) {
                      //control de tama�o
               if ((lineas > DBServlet.maxSIZE)&&(!(par.bInformeCompleto))) {
                        iEstado = CLista.listaINCOMPLETA;
                        break;
                      }
                      // control de estado
                      if (iEstado == CLista.listaVACIA)
                        iEstado = CLista.listaLLENA;
                      enf = rs.getString("CD_ENFCIE");
                      if (enf.compareTo(par.ultEnf)<=0)
                        continue;
                      codsEnf = codsEnf + "'" + enf + "',";
                      if (antEnf.equals("")) //para la primera
                        antEnf = enf;
                      if (!enf.equals(antEnf)){
                        //crear un data y ponerlo en la lista de salida
                        trazaLog("llamada5 a rellenaActDataCons93");
                        salida.addElement(rellenaActDataCons93(antEnf, cols));
                        antEnf = enf;
                        cols = new Vector();
                        lineas++;
                      }
                      //acumular
                      parNC = new Vector();
                      parNC.addElement(rs.getString ("cd_zbs"));
                      parNC.addElement(new Long(rs.getLong (1)));
                      cols.addElement(parNC);
                    }
                    rs.close();
                    rs = null;
                    st.close();
                    st= null;
                    //AIC
                    //codsEnf = codsEnf.substring(0, codsEnf.length()-1) + " ) ";
                    if(codsEnf.length() > 0)
                    {
                      int tamanyo = codsEnf.length() - 1;
                      codsEnf = codsEnf.substring(0, tamanyo) + " ) ";
                    }
                    //codsEnf = codsEnf.substring(0, codsEnf.length()) + " ) ";
                    //meter la ultima
                    if ((cols.size()!=0) && (lineas<=DBServlet.maxSIZE))
                      trazaLog("llamada6 a rellenaActDataCons93");
                      salida.addElement(rellenaActDataCons93(enf, cols));
                    if (salida.size()==0)
                      break;
                    // prepara la query para los acumulados
                    query = " select sum(a.nm_casos), a.cd_enfcie, a.cd_zbs "
                          + " from sive_resumen_edos a, sive_procesos b"
                          + " where a.cd_anoepi = ? "
                          + " and a.cd_semepi >= ? and cd_semepi <= ? "
                          + " and a.cd_nivel_1 = ? "
                          + " and a.cd_nivel_2 = ? "
                          + " and a.cd_enfcie = b.cd_enfcie ";
                   dataCons93  dat = (dataCons93) salida.firstElement();
// Esto es lo nuevo que se pone.
                    for (int i =0;i<par.lista.size(); i++){ //cd_zbs in ()
                      if (i==0)
                        if (i==par.lista.size()-1)
                          query = query + " and ( a.cd_zbs = ? ) ";
                        else
                          query = query + " and ( a.cd_zbs = ? ";
                      else if (i==par.lista.size()-1)
                        query = query + " or a.cd_zbs = ? ) ";
                      else
                        query = query + " or a.cd_zbs = ? ";
                    }
                    if (!par.ultEnf.equals("")){
                      query = query + " and a.cd_enfcie > ? "
               + " group by b.ds_proceso, a.cd_enfcie, a.cd_zbs "
               + " order by b.ds_proceso, a.cd_enfcie, a.cd_zbs ";
                    }
                    else
                      query = query + " group by b.ds_proceso, a.cd_enfcie, a.cd_zbs order by b.ds_proceso, a.cd_enfcie, a.cd_zbs ";
// Final de lo nuevo..... proseguimos   22-03-01
                    st = con.prepareStatement(query);
                    // codigo
                    st.setString(1, par.ano);
                    st.setString(2, "01");
                    st.setString(3, par.semH);
                    st.setString(4, par.area);
                    st.setString(5, par.distrito);
                    elem = new Hashtable();
                    for (int i =0;i<par.lista.size(); i++){
                      elem = (Hashtable)par.lista.elementAt(i);
                      st.setString(i+6, (String)elem.get("CD_ZBS"));
                    }
                    if (!par.ultEnf.equals("")){
                      st.setString(par.lista.size()+6, par.ultEnf);
                    }
                    // lanza la query
                    rs = st.executeQuery();
                    lineas = 1;
                    iEstado = CLista.listaVACIA;
                    antEnf = "";
                    cols = new Vector();
                    parNC = new Vector();
                    enf = "";
                    while (rs.next()) {
                      //control de tama�o
               if ((lineas > DBServlet.maxSIZE)&&(!(par.bInformeCompleto))) {
                        iEstado = CLista.listaINCOMPLETA;
                        break;
                      }
                      // control de estado
                      if (iEstado == CLista.listaVACIA)
                        iEstado = CLista.listaLLENA;
                      enf = rs.getString("CD_ENFCIE");
                      if (enf.compareTo(par.ultEnf)<=0)
                        continue;
                      if (antEnf.equals("")) //para la primera
                        antEnf = enf;
                      if (!enf.equals(antEnf)){
                        //crear un data y ponerlo en la lista de salida
                        rellenaAcuDataCons93(antEnf, cols, salida);
                        antEnf = enf;
                        cols = new Vector();
                        lineas++;
                      }
                        // jlt
                        parNC = new Vector();
                        parNC.addElement(rs.getString ("cd_zbs"));
                        parNC.addElement(new Long(rs.getLong (1)));
                        cols.addElement(parNC);
                    }
                    rs.close();
                    rs = null;
                    st.close();
                    st= null;
                    //meter la ultima
                    if ((cols.size()!=0) && (lineas<=DBServlet.maxSIZE))
                      rellenaAcuDataCons93(enf, cols, salida);
           */

          /**************************************************/
          /****************** POBLACION *********************/
          /**************************************************/

        }
        if (par.hab15) {

          query = new String();
          query =
              "select (tab_pob.sum_pob - tab_res.sum_res) total, tab_pob.niv_pob as zbs from "
              +
              " (select tab_area.cd_zbs as niv_pob ,sum(nm_poblacion) as sum_pob "
              + " from sive_poblacion_ns tab_area "
              + " where tab_area.cd_anoepi in (select max(cd_anoepi) "
              + " from sive_poblacion_ns where cd_anoepi <= ?) and "
              + " tab_area.cd_nivel_1 = ? and "
              + " tab_area.cd_nivel_2 = ? ";

          for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_2 in ()
            if (i == 0) {
              if (i == par.lista.size() - 1) {
                query = query + " and ( tab_area.cd_zbs = ? ) ";
              }
              else {
                query = query + " and ( tab_area.cd_zbs = ? ";
              }
            }
            else if (i == par.lista.size() - 1) {
              query = query + " or tab_area.cd_zbs = ? ) ";
            }
            else {
              query = query + " or tab_area.cd_zbs = ? ";
            }
          }

          /*            query = query + " group by tab_area.cd_zbs) tab_pob "
                        + " , "
               + "(select tab_mun.cd_zbs as niv_res ,sum(sum_pob) as sum_res "
                        + "from sive_municipio tab_mun, "
                        + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
                      + "from sive_poblacion_ng "
                      + " where cd_anoepi in (select max(cd_anoepi) "
                        + "   from sive_poblacion_ng where cd_anoepi <= ?) "
               + "   group by cd_mun having sum(nm_poblacion) <= 15000 ) tab_sum "
                        + " where tab_mun.cd_mun = tab_sum.cd_mun "
                        + " and tab_mun.cd_prov = '28' "
                        + " and tab_mun.cd_nivel_1 = ? "
                        + " and tab_mun.cd_nivel_2 = ? "
                        + " and tab_mun.cd_zbs != null "
                        + " group by tab_mun.cd_zbs) tab_res "
                        + " where tab_pob.niv_pob = tab_res.niv_res "
                        + " order by tab_pob.niv_pob ";
           */

          query = query + " group by tab_area.cd_zbs) tab_pob "
              + " , "
              + "(select tab_mun.cd_zbs as niv_res ,sum(sum_pob) as sum_res "
              + "from sive_municipio tab_mun, "
              + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
              + "from sive_poblacion_ng "
              + " where cd_anoepi in (select max(cd_anoepi) "
              + "   from sive_poblacion_ng where cd_anoepi <= ?) "
              +
              "   group by cd_mun having sum(nm_poblacion) <= 15000 ) tab_sum "
              + " where tab_mun.cd_mun = tab_sum.cd_mun "
              + " and tab_mun.cd_prov = " + prov
              + " and tab_mun.cd_nivel_1 = ? "
              + " and tab_mun.cd_nivel_2 = ? "
              + " group by tab_mun.cd_zbs "
              //////////
              + " UNION "
              +
              " select distinct cd_zbs as niv_res , 0 as sum_res from sive_zona_basica "
              + " where cd_nivel_1 = ? and cd_nivel_2 = ? "
              + " MINUS "
              +
              " select distinct cd_zbs as niv_res, 0 as sum_res from sive_municipio "
              + " where cd_nivel_1 = ? and cd_nivel_2 = ? "
              /////////
              + " ) tab_res "
              + " where tab_pob.niv_pob = tab_res.niv_res "
              + " order by tab_pob.niv_pob ";

          st = con.prepareStatement(query);
          // codigo
          st.setString(1, par.ano);

          st.setString(2, par.area);
          st.setString(3, par.distrito);

          elem = null;
          Vector niveles = new Vector();

          Vector pobla = new Vector();

          elem = new Hashtable();
          for (int i = 0; i < par.lista.size(); i++) {
            elem = (Hashtable) par.lista.elementAt(i);
            st.setString(i + 4, (String) elem.get("CD_ZBS"));
            niveles.insertElementAt( (String) elem.get("CD_ZBS"), i);
            pobla.insertElementAt(new Long("-1"), i);
          }

          st.setString(par.lista.size() + 4, par.ano);

          st.setString(par.lista.size() + 5, par.area);
          st.setString(par.lista.size() + 6, par.distrito);

          st.setString(par.lista.size() + 7, par.area);
          st.setString(par.lista.size() + 8, par.distrito);
          st.setString(par.lista.size() + 9, par.area);
          st.setString(par.lista.size() + 10, par.distrito);

          // lanza la query
          rs = st.executeQuery();

          int ind1 = 0;
          String n1 = "";

          while (rs.next()) {
            n1 = rs.getString("zbs");
            ind1 = niveles.indexOf(n1);
            pobla.setElementAt(new Long(rs.getLong(1)), ind1);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //rellenarPoblacion(niveles, pobla, salida);
          rellenarPoblacionhab15(con, st, rs, salida, par, opmode, pobla);

        }
        else {

          // modificacion jlt recuperamos la poblacion del a�o m�ximo
          // que sea menor o igual que el seleccionado
          query = " select sum(nm_poblacion), cd_zbs "
              + " from sive_poblacion_ns "
              + " where cd_anoepi in (select max(cd_anoepi) "
              + " from sive_poblacion_ns where cd_anoepi <= ?) "
              + " and cd_nivel_1 = ? and cd_nivel_2 = ? ";

// ******* C�digo comentado el 23-03-01 ************
          /*          for (int i =0;i<dat.COLS.size(); i++){
                      if (i==0)
                        if (i==dat.COLS.size()-1)
                          query = query + " and  cd_zbs = ?  ";
                        else
                          query = query + " and ( cd_zbs = ? ";
                      else if (i==dat.COLS.size()-1)
                        query = query + " or cd_zbs = ? ) ";
                      else
                        query = query + " or cd_zbs = ? ";
                    }
                    query = query + " group by cd_zbs order by cd_zbs ";*/

          /********** C�digo nuevo como correcci�n 23-03-01 *******/
          for (int i = 0; i < par.lista.size(); i++) { //cd_zbs in ()
            if (i == 0) {
              if (i == par.lista.size() - 1) {
                query = query + " and ( cd_zbs = ? ) ";
              }
              else {
                query = query + " and ( cd_zbs = ? ";
              }
            }
            else if (i == par.lista.size() - 1) {
              query = query + " or cd_zbs = ? ) ";
            }
            else {
              query = query + " or cd_zbs = ? ";
            }
          }

          /*if (!par.ultEnf.equals("")){
            query = query + " and cd_enfcie > ? "
                        + " group by cd_zbs "
                        + " order by cd_zbs ";
                     }
                     else */
          query = query + " group by cd_zbs order by cd_zbs ";

          /************* Fin de correci�n *************/

          //System.out.println("SRVCTCIENAREA: " + query);

          st = con.prepareStatement(query);
          // codigo
          st.setString(1, par.ano);
          st.setString(2, par.area);
          st.setString(3, par.distrito);

          Vector niveles = new Vector();
          Vector pobla = new Vector();
          /************* Otra cosita que se comenta: 23-03-01 *********/
          /*          for (int i =0;i<dat.COLS.size(); i++){
                      st.setString(i+4, (String)dat.COLS.elementAt(i));
               niveles.insertElementAt((String)dat.COLS.elementAt(i), i);
                      pobla.insertElementAt(new Long("-1"), i);
                    }*/
          /************* Fin de la otra cosita comentada **************/
          /************* Codiguito de correcci�n **********************/
          elem = new Hashtable();
          for (int i = 0; i < par.lista.size(); i++) {
            elem = (Hashtable) par.lista.elementAt(i);
            st.setString(i + 4, (String) elem.get("CD_ZBS"));
            niveles.insertElementAt( (String) elem.get("CD_ZBS"), i);
            pobla.insertElementAt(new Long("-1"), i);
          }
          /************************************************************/

          // lanza la query
          rs = st.executeQuery();

          int ind = 0;
          String n1 = "";
          while (rs.next()) {
            n1 = rs.getString("cd_zbs");
            ind = niveles.indexOf(n1);
            pobla.setElementAt(new Long(rs.getLong(1)), ind);
          }
          rs.close();
          rs = null;
          st.close();
          st = null;

          //rellenarPoblacion(niveles, pobla, salida);
          rellenarPoblacion(con, st, rs, salida, par, opmode, pobla);

        } // fin del else  de poblacion

// C�digo para ver datos.... comentable..
        /*dataCons93 datillo = new dataCons93();
         for (int pepe=0;pepe<salida.size();pepe++) {
          datillo = (dataCons93)salida.elementAt(pepe);
          System.out.println("------------ "+pepe+" ------------");
          for (int p=0;p<datillo.COLS.size();p++)
            System.out.println("COLS "+datillo.COLS.elementAt(p));
          for (int p=0;p<datillo.ACTS.size();p++)
            System.out.println("ACTS "+datillo.ACTS.elementAt(p));
          for (int p=0;p<datillo.ACUS.size();p++)
            System.out.println("ACUS "+datillo.ACUS.elementAt(p));
          for (int p=0;p<datillo.POBS.size();p++)
            System.out.println("POBS "+datillo.POBS.elementAt(p));
         }
// Fin de las l�neas de programa borrables.*/

        //rellenar los datos de poblacion en el primer data
        //rellenarPoblacion(niveles, pobla, salida);

        if (!par.casosTasas) {
          calcularTasas(con, st, rs, salida, par, opmode);
        }

        query = " select count(distinct cd_enfcie) "
            + " from sive_resumen_edos "
            + " where cd_anoepi = ? "
            + " and cd_semepi >= ? and cd_semepi <= ? "
            + " and cd_enfcie in (select cd_enfcie "
            + " from sive_procesos) "
            + " and cd_nivel_1 = ? "
            + " and cd_nivel_2 = ? ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_zbs in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( cd_zbs = ? ) ";
            }
            else {
              query = query + " and ( cd_zbs = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or cd_zbs = ? ) ";
          }
          else {
            query = query + " or cd_zbs = ? ";
          }
        }
        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.semD);
        st.setString(3, par.semH);
        st.setString(4, par.area);
        st.setString(5, par.distrito);

        elem = new Hashtable();
        //System.out.println("SRVCTCIENAREA: lista " + par.lista);
        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);
          //System.out.println("SRVCTCIENAREA: elemeto de la lista " + elem.toString());
          st.setString(i + 6, (String) elem.get("CD_ZBS"));
        }

        // lanza la query
        rs = st.executeQuery();

        if (rs.next()) {
          totalReg = new Integer(rs.getInt(1));
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        break;
    }

    //comun para los tres modos

    if (salida.size() != 0) {
      //calcular los notificadores
      calcularNotificadores(con, st, rs, salida, par);

      //adecuar los valores para la estructura del informe
      ponerValoresResto(salida, par, opmode);

      //final calcular los totales de casos o tasas
      //modificacion jlt 17/12/2001
      //  if (par.casosTasas)
      calcularTotalesEnf(salida, par);

      //final poner las descripciones en las enfermedades
      descripEnfermedades(con, st, rs, salida, codsEnf, haytasas);
      //comun para los tres modos

      //aic
      int pos1, pos2;
      pos1 = codsEnf.lastIndexOf(',');
      pos2 = codsEnf.lastIndexOf(')');
      //par.ultEnf = codsEnf.substring(codsEnf.lastIndexOf(','), codsEnf.lastIndexOf(')'));
      if (pos1 >= 0 && pos2 >= 0) {
        par.ultEnf = codsEnf.substring(pos1, pos2);
      }

      par.ultEnf.trim();
    }
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    data.setState(iEstado);
    data.addElement(salida);
    data.addElement(totalReg);
    data.trimToSize();

    return data;
  }

  private void descripEnfermedades(Connection con, PreparedStatement st,
                                   ResultSet rs,
                                   Vector salida, String codsEnf,
                                   boolean haytasas) throws Exception {

    //Para las descripciones de proceso
    String sDesPro = "";
    String sDesProL = "";

    //System.out.println("SRVCTCIENAREA: en descripEnfermedades ");
    Vector cods = new Vector();
    Vector dess = new Vector();

    String query = "";
    query = " select CD_ENFCIE, DS_PROCESO, DSL_PROCESO"
        + " from sive_procesos "
        + " where CD_ENFCIE in " + codsEnf;
    //System.out.println("SRVCTCIENAREA: enfs " + codsEnf);
    st = con.prepareStatement(query);
    // lanza la query
    rs = st.executeQuery();

    int cont = 0;
    while (rs.next()) {

      cods.insertElementAt(rs.getString("CD_ENFCIE"), cont);
      sDesPro = rs.getString("DS_PROCESO");
      if (haytasas) {
        if (rs.getString("CD_ENFCIE").equals(CD_PARALISIS)) {
          sDesPro = "* " + sDesPro;
        }
      }
      sDesProL = rs.getString("DSL_PROCESO");
      // obtiene la descripcion auxiliar en funci�n del idioma
      if (paramEntrada.getIdioma() != CApp.idiomaPORDEFECTO) {
        if (sDesProL != null) {
          sDesPro = sDesProL;
        }
      }

      dess.insertElementAt(sDesPro, cont);
      cont++;
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

    int pos = 0;
    dataCons93 datos = new dataCons93();
    for (int z = 0; z < salida.size(); z++) {
      datos = (dataCons93) salida.elementAt(z);
      pos = cods.indexOf(datos.ENFERMEDAD);
      datos.ENFERMEDAD = (String) dess.elementAt(pos);
      salida.setElementAt(datos, z);
    }
  }

  private String calcularProv(Connection con, PreparedStatement st,
                              ResultSet rs,
                              parCons93 par) throws Exception {

    String prov = "";

    String query = "";
    query = " select CD_PROV "
        + " from sive_provincia "
        + " where CD_CA = ? ";

    st = con.prepareStatement(query);

    st.setString(1, ca);

    // lanza la query
    rs = st.executeQuery();

    while (rs.next()) {

      prov = rs.getString(1);

    }

    rs.close();
    rs = null;
    st.close();
    st = null;

    return prov;
  }

  private void calcularTasas(Connection con, PreparedStatement st,
                             ResultSet rs, Vector salida,
                             parCons93 par, int opmode) throws Exception {

    //AIC
    if (salida == null || salida.size() == 0) {
      return;
    }

//    System.out.println("SrvCTCienArea:  control 1");

    dataCons93 datos = (dataCons93) salida.elementAt(0);
    float casos = (float) 0;
    float casosacu = (float) 0;
    float pobla = (float) 0;
    // modificacion jlt 17/12/2001
    float casostotal = (float) 0;
    float casostotalacu = (float) 0;

    float poblatotal = (float) 0;
    float poblatotalpar = (float) 0;
    float poblatotalhabpar = (float) 0;

    Vector niveles = (Vector) datos.COLS.clone();

    Vector poblas = (Vector) datos.POBS.clone();
    String query = "";

    //System.out.println("SRVCTCIENAREA: en calcularTasas ");

    // modificacion jlt
    //17/12/2001 calculo la poblacion total
    dataCons93 datapob = (dataCons93) salida.elementAt(0);
    poblatotal = (new Long(datapob.POBT)).floatValue();

    for (int j = 0; j < salida.size(); j++) {
      //System.out.println("SRVCTCIENAREA: 1");
      datos = (dataCons93) salida.elementAt(j);
      ////////////////////////////////
      Hashtable elem2 = new Hashtable();
      Vector niveles2 = new Vector();

      ////////////////////////////
      // modificacion jlt
      // dependiendo de los niveles
      switch (opmode) {
        case servletCTCA:
          for (int i = 0; i < par.lista.size(); i++) {
            elem2 = (Hashtable) par.lista.elementAt(i);
            niveles2.insertElementAt( (String) elem2.get("CD_NIVEL_1"), i);
          }
          break;

        case servletCTAREA:
          for (int i = 0; i < par.lista.size(); i++) {
            elem2 = (Hashtable) par.lista.elementAt(i);
            niveles2.insertElementAt( (String) elem2.get("CD_NIVEL_2"), i);
          }
          break;

        case servletCTDIST:
          for (int i = 0; i < par.lista.size(); i++) {
            elem2 = (Hashtable) par.lista.elementAt(i);
            niveles2.insertElementAt( (String) elem2.get("CD_ZBS"), i);
          }
          break;
      }
      /////////////////////

      ////////////////////////////
      // modificaci�n jlt 18/09/01 para calcular bien las tasas de la
      // enfermedad 61 (par�lisis fl�ccida aguda)
      // calculamos las poblaciones de los menores de 15 a�os

      if ( ( (String) datos.ENFERMEDAD).trim().equals(CD_PARALISIS)) {

        switch (opmode) {
          case servletCTCA:

            if (par.hab15) {

              query = new String();
              query = "select (tab_pob.sum_pob - tab_res.sum_res) total, tab_pob.niv_pob as nivel_1 from "
                  +
                  " (select tab_area.cd_nivel_1 as niv_pob ,sum(nm_poblacion) as sum_pob "
                  + " from sive_poblacion_ns tab_area "
                  + " where tab_area.cd_anoepi in (select max(cd_anoepi) "
                  +
                  " from sive_poblacion_ns where cd_anoepi <= ?)  and tab_area.nm_edad < 15 ";

              for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_1 in ()
                if (i == 0) {
                  if (i == par.lista.size() - 1) {
                    query = query + " and ( tab_area.cd_nivel_1 = ? ) ";
                  }
                  else {
                    query = query + " and ( tab_area.cd_nivel_1 = ? ";
                  }
                }
                else if (i == par.lista.size() - 1) {
                  query = query + " or tab_area.cd_nivel_1 = ? ) ";
                }
                else {
                  query = query + " or tab_area.cd_nivel_1 = ? ";
                }
              }

              query = query + " group by tab_area.cd_nivel_1) tab_pob "
                  + " , "
                  +
                  "(select tab_mun.cd_nivel_1 as niv_res ,sum(sum_pob) as sum_res "
                  + "from sive_municipio tab_mun, "
                  + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
                  + "from sive_poblacion_ng "
                  + " where cd_anoepi in (select max(cd_anoepi) "
                  +
                  "   from sive_poblacion_ng where cd_anoepi <= ?) and nm_edad < 15 "
                  +
                  "   group by cd_mun having sum(nm_poblacion) <= 15000 ) tab_sum "
                  + " where tab_mun.cd_mun = tab_sum.cd_mun "
                  + " and tab_mun.cd_prov = " + prov
                  + " group by tab_mun.cd_nivel_1 "
                  //////////
                  + " UNION "
                  +
                  " select distinct cd_nivel_1 as niv_res , 0 as sum_res from sive_nivel1_s "
                  + " MINUS "
                  +
                  " select distinct cd_nivel_1 as niv_res, 0 as sum_res from sive_municipio "
                  /////////
                  + " ) tab_res "
                  + " where tab_pob.niv_pob = tab_res.niv_res "
                  + " order by tab_pob.niv_pob ";

              st = con.prepareStatement(query);
              // codigo
              st.setString(1, par.ano);

              Hashtable elem = new Hashtable();
              for (int i = 0; i < par.lista.size(); i++) {
                elem = (Hashtable) par.lista.elementAt(i);
                st.setString(i + 2, (String) elem.get("CD_NIVEL_1"));
              }

              st.setString(par.lista.size() + 2, par.ano);

              // lanza la query
              rs = st.executeQuery();

              while (rs.next()) {
                datos.POBS.addElement(new Long(rs.getLong(1)));
              }
              rs.close();
              rs = null;
              st.close();
              st = null;

            }
            else {

              query = " select sum(nm_poblacion), cd_nivel_1 "
                  + " from sive_poblacion_ns "
                  + " where cd_anoepi in (select max(cd_anoepi) "
                  + " from sive_poblacion_ns where cd_anoepi <= ?) ";

              for (int i = 0; i < par.lista.size(); i++) {
                if (i == 0) {
                  if (i == par.lista.size() - 1) {
                    query = query + " and ( cd_nivel_1 = ? ) ";
                  }
                  else {
                    query = query + " and ( cd_nivel_1 = ? ";
                  }
                }
                else if (i == par.lista.size() - 1) {
                  query = query + " or cd_nivel_1 = ? ) ";
                }
                else {
                  query = query + " or cd_nivel_1 = ? ";
                }
              }
              query = query + " and nm_edad < ? ";
              query = query + " group by cd_nivel_1 order by cd_nivel_1 ";

              st = con.prepareStatement(query);
              // codigo
              st.setString(1, par.ano);
              Hashtable elem = null;
              for (int i = 0; i < par.lista.size(); i++) {
                elem = (Hashtable) par.lista.elementAt(i);
                st.setString(i + 2, (String) elem.get("CD_NIVEL_1"));
              }

              st.setInt(par.lista.size() + 2, 15);
              // lanza la query
              rs = st.executeQuery();

              //t ind = 0;

              //poblas = new Vector();
              //niveles = new Vector();

              while (rs.next()) {

                datos.POBS.addElement(new Long(rs.getLong(1)));
                //poblas.addElement(new Long(rs.getLong(1)));
                //niveles.addElement(rs.getString("cd_nivel_1"));
                //poblas.setElementAt(new Long(rs.getLong(1)), ind);
                //niveles.setElementAt(rs.getString("cd_nivel_1"), ind);

                //d++;
              }
              rs.close();
              rs = null;
              st.close();
              st = null;

            } // del if de poblacion

            break;
          case servletCTAREA:

            if (par.hab15) {

              query = new String();
              query = "select (tab_pob.sum_pob - tab_res.sum_res) total, tab_pob.niv_pob as nivel_2 from "
                  +
                  " (select tab_area.cd_nivel_2 as niv_pob ,sum(nm_poblacion) as sum_pob "
                  + " from sive_poblacion_ns tab_area "
                  + " where tab_area.cd_anoepi in (select max(cd_anoepi) "
                  +
                  " from sive_poblacion_ns where cd_anoepi <= ?) and tab_area.nm_edad < 15 and "
                  + " tab_area.cd_nivel_1 = ? ";

              for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_2 in ()
                if (i == 0) {
                  if (i == par.lista.size() - 1) {
                    query = query + " and ( tab_area.cd_nivel_2 = ? ) ";
                  }
                  else {
                    query = query + " and ( tab_area.cd_nivel_2 = ? ";
                  }
                }
                else if (i == par.lista.size() - 1) {
                  query = query + " or tab_area.cd_nivel_2 = ? ) ";
                }
                else {
                  query = query + " or tab_area.cd_nivel_2 = ? ";
                }
              }

              query = query + " group by tab_area.cd_nivel_2) tab_pob "
                  + " , "
                  +
                  "(select tab_mun.cd_nivel_2 as niv_res ,sum(sum_pob) as sum_res "
                  + "from sive_municipio tab_mun, "
                  + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
                  + "from sive_poblacion_ng "
                  + " where cd_anoepi in (select max(cd_anoepi) "
                  +
                  "   from sive_poblacion_ng where cd_anoepi <= ? ) and nm_edad < 15 "
                  +
                  "   group by cd_mun having sum(nm_poblacion) <= 15000 ) tab_sum "
                  + " where tab_mun.cd_mun = tab_sum.cd_mun "
                  + " and tab_mun.cd_prov = " + prov
                  + " and tab_mun.cd_nivel_1 = ? "
                  + " group by tab_mun.cd_nivel_2 "
                  ////////////
                  + " UNION "
                  +
                  " select distinct cd_nivel_2 as niv_res , 0 as sum_res from sive_nivel2_s "
                  + " where cd_nivel_1 = ? "
                  + " MINUS "
                  +
                  " select distinct cd_nivel_2 as niv_res, 0 as sum_res from sive_municipio "
                  + " where cd_nivel_1 = ? "
                  ////////////
                  + " ) tab_res "
                  + " where tab_pob.niv_pob = tab_res.niv_res "
                  + " order by tab_pob.niv_pob ";

              st = con.prepareStatement(query);
              // codigo
              st.setString(1, par.ano);

              st.setString(2, par.area);

              Hashtable elem = new Hashtable();
              for (int i = 0; i < par.lista.size(); i++) {
                elem = (Hashtable) par.lista.elementAt(i);
                st.setString(i + 3, (String) elem.get("CD_NIVEL_2"));
              }

              st.setString(par.lista.size() + 3, par.ano);

              st.setString(par.lista.size() + 4, par.area);

              st.setString(par.lista.size() + 5, par.area);
              st.setString(par.lista.size() + 6, par.area);

              // lanza la query
              rs = st.executeQuery();

              while (rs.next()) {
                datos.POBS.addElement(new Long(rs.getLong(1)));
              }
              rs.close();
              rs = null;
              st.close();
              st = null;

            }
            else {

              query = " select sum(nm_poblacion), cd_nivel_2 "
                  + " from sive_poblacion_ns "
                  + " where cd_anoepi in (select max(cd_anoepi)  "
                  + " from sive_poblacion_ns where cd_anoepi <= ?) "
                  + " and cd_nivel_1 = ? ";
              for (int i = 0; i < par.lista.size(); i++) {
                if (i == 0) {
                  if (i == par.lista.size() - 1) {
                    query = query + " and ( cd_nivel_2 = ? ) ";
                  }
                  else {
                    query = query + " and ( cd_nivel_2 = ? ";
                  }
                }
                else if (i == par.lista.size() - 1) {
                  query = query + " or cd_nivel_2 = ? ) ";
                }
                else {
                  query = query + " or cd_nivel_2 = ? ";
                }
              }

              query = query + " and nm_edad < ? ";
              query = query + " group by cd_nivel_2 order by cd_nivel_2 ";

              //System.out.println("SRVCTCIENAREA: " + query);

              st = con.prepareStatement(query);
              // codigo
              st.setString(1, par.ano);
              st.setString(2, par.area);

              Hashtable elem = null;
              for (int i = 0; i < par.lista.size(); i++) {
//            System.out.print("falla2....");
                elem = (Hashtable) par.lista.elementAt(i);
//            System.out.println("aqu�");
                st.setString(i + 3, (String) elem.get("CD_NIVEL_2"));
              }

              st.setInt(par.lista.size() + 3, 15);

              // lanza la query
              rs = st.executeQuery();

              //veles = new Vector();
              //blas = new Vector();
              //d = 0;
              while (rs.next()) {
                datos.POBS.addElement(new Long(rs.getLong(1)));
                //poblas.insertElementAt(new Long(rs.getLong(1)), ind);
                //niveles.insertElementAt(rs.getString("cd_nivel_2"), ind);
                //ind++;
              }
              rs.close();
              rs = null;
              st.close();
              st = null;

            } // del if de poblacion

            break;
          case servletCTDIST:

            if (par.hab15) {

              query = new String();
              query =
                  "select (tab_pob.sum_pob - tab_res.sum_res) total, tab_pob.niv_pob as zbs from "
                  +
                  " (select tab_area.cd_zbs as niv_pob ,sum(nm_poblacion) as sum_pob "
                  + " from sive_poblacion_ns tab_area "
                  + " where tab_area.cd_anoepi in (select max(cd_anoepi) "
                  +
                  " from sive_poblacion_ns where cd_anoepi <= ?) and tab_area.nm_edad < 15 and "
                  + " tab_area.cd_nivel_1 = ? and "
                  + " tab_area.cd_nivel_2 = ? ";

              for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_2 in ()
                if (i == 0) {
                  if (i == par.lista.size() - 1) {
                    query = query + " and ( tab_area.cd_zbs = ? ) ";
                  }
                  else {
                    query = query + " and ( tab_area.cd_zbs = ? ";
                  }
                }
                else if (i == par.lista.size() - 1) {
                  query = query + " or tab_area.cd_zbs = ? ) ";
                }
                else {
                  query = query + " or tab_area.cd_zbs = ? ";
                }
              }

              query = query + " group by tab_area.cd_zbs) tab_pob "
                  + " , "
                  +
                  "(select tab_mun.cd_zbs as niv_res ,sum(sum_pob) as sum_res "
                  + "from sive_municipio tab_mun, "
                  + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
                  + "from sive_poblacion_ng "
                  + " where cd_anoepi in (select max(cd_anoepi) "
                  +
                  "   from sive_poblacion_ng where cd_anoepi <= ?) and nm_edad < 15 "
                  +
                  "   group by cd_mun having sum(nm_poblacion) <= 15000 ) tab_sum "
                  + " where tab_mun.cd_mun = tab_sum.cd_mun "
                  + " and tab_mun.cd_prov = " + prov
                  + " and tab_mun.cd_nivel_1 = ? "
                  + " and tab_mun.cd_nivel_2 = ? "
                  + " group by tab_mun.cd_zbs "
                  //////////
                  + " UNION "
                  +
                  " select distinct cd_zbs as niv_res , 0 as sum_res from sive_zona_basica "
                  + " where cd_nivel_1 = ? and cd_nivel_2 = ? "
                  + " MINUS "
                  +
                  " select distinct cd_zbs as niv_res, 0 as sum_res from sive_municipio "
                  + " where cd_nivel_1 = ? and cd_nivel_2 = ? "
                  /////////
                  + " ) tab_res "
                  + " where tab_pob.niv_pob = tab_res.niv_res "
                  + " order by tab_pob.niv_pob ";

              st = con.prepareStatement(query);
              // codigo
              st.setString(1, par.ano);

              st.setString(2, par.area);
              st.setString(3, par.distrito);

              Hashtable elem = new Hashtable();
              for (int i = 0; i < par.lista.size(); i++) {
                elem = (Hashtable) par.lista.elementAt(i);
                st.setString(i + 4, (String) elem.get("CD_ZBS"));
              }

              st.setString(par.lista.size() + 4, par.ano);

              st.setString(par.lista.size() + 5, par.area);
              st.setString(par.lista.size() + 6, par.distrito);

              st.setString(par.lista.size() + 7, par.area);
              st.setString(par.lista.size() + 8, par.distrito);

              st.setString(par.lista.size() + 9, par.area);
              st.setString(par.lista.size() + 10, par.distrito);

              // lanza la query
              rs = st.executeQuery();

              int ind1 = 0;
              String n1 = "";
              while (rs.next()) {
                datos.POBS.addElement(new Long(rs.getLong(1)));
              }
              rs.close();
              rs = null;
              st.close();
              st = null;

            }
            else {

              query = " select sum(nm_poblacion), cd_zbs "
                  + " from sive_poblacion_ns "
                  + " where cd_anoepi in (select max(cd_anoepi) "
                  + " from sive_poblacion_ns where cd_anoepi <= ?) "
                  + " and cd_nivel_1 = ? "
                  + " and cd_nivel_2 = ? ";
              for (int i = 0; i < par.lista.size(); i++) {
                if (i == 0) {
                  if (i == par.lista.size() - 1) {
                    query = query + " and ( cd_zbs = ? ) ";
                  }
                  else {
                    query = query + " and ( cd_zbs = ? ";
                  }
                }
                else if (i == par.lista.size() - 1) {
                  query = query + " or cd_zbs = ? ) ";
                }
                else {
                  query = query + " or cd_zbs = ? ";
                }
              }
              query = query + " and nm_edad < ? ";
              query = query + " group by cd_zbs order by cd_zbs ";

              st = con.prepareStatement(query);
              // codigo
              st.setString(1, par.ano);
              st.setString(2, par.area);
              st.setString(3, par.distrito);

              Hashtable elem = null;
              for (int i = 0; i < par.lista.size(); i++) {

                elem = (Hashtable) par.lista.elementAt(i);

                st.setString(i + 4, (String) elem.get("CD_ZBS"));
              }
              st.setInt(par.lista.size() + 4, 15);
              // lanza la query
              rs = st.executeQuery();

              while (rs.next()) {
                datos.POBS.addElement(new Long(rs.getLong(1)));

              }
              rs.close();
              rs = null;
              st.close();
              st = null;

            } // del if de poblacion

            break;

        } // del switch

        // calculo la poblacion total
        // modificacion jlt 28/12/2001
        if (par.hab15) {
          poblatotalpar = (float) rellenarPoblacionhab15par(con, st, rs, par,
              opmode);
        }
        else {
          poblatotalpar = (float) rellenarPoblacionpara(con, st, rs, par,
              opmode);
          ////////////////////////

        }

      } // if de la enfermedad

      ////////////////////////////

      for (int s = 0; s < datos.COLS.size(); s++) {

        int indi = 0; // para los casos
        int indi2 = 0; // para las poblaciones
        // modificacion jlt, para obtener bien las tasas
        // para cada nivel
        //indi = datos.COLS.indexOf((String)niveles.elementAt(s));
        //indi = niveles2.indexOf((String) datos.COLS.elementAt(s));

        //indi = nivelescondatos.indexOf((String) datos.COLS.elementAt(s));
        // modificacion jlt 04/01/02
        indi = nivelescondatosacu.indexOf( (String) datos.COLS.elementAt(s));
        indi2 = niveles2.indexOf( (String) datos.COLS.elementAt(s));

        if (indi != -1) {
//              System.out.println("enferme" + datos.ENFERMEDAD);
          casos = ( (Long) datos.ACTS.elementAt(indi)).floatValue();
          casosacu = ( (Long) datos.ACUS.elementAt(indi)).floatValue();
          //casos = ((Long)datos.ACTS.elementAt(s)).floatValue();
          // modificacion jlt 18/09/01
          if ( ( (String) datos.ENFERMEDAD).trim().equals(CD_PARALISIS)) {
            pobla = ( (Long) datos.POBS.elementAt(indi2)).floatValue();
          }
          else {
            pobla = ( (Long) poblas.elementAt(indi2)).floatValue();
          }

          //}
          // modificacion jlt
          // 17/12/2001
          datos.ACTS.setElementAt(new Float(casos * (float) 100000 / pobla),
                                  indi);
          //casos = ((Long)datos.ACUS.elementAt(indi)).floatValue();
          datos.ACUS.setElementAt(new Float(casosacu * (float) 100000 / pobla),
                                  indi);

          //casostotal = casostotal + casos;
          casostotal = datos.ACTT;
          //casostotalacu = casostotalacu + casosacu;
          casostotalacu = datos.ACUT;

          //datos.ACTS.setElementAt(new Float( casos * (float)100000 / pobla),s);
          //casos = ((Long)datos.ACUS.elementAt(s)).floatValue();
          //datos.ACUS.setElementAt(new Float( casos * (float)100000 / pobla),s);
        }

      }

      // calculamos los totales como n� total de casos entre total
      // de poblaci�n

      //datos.ACTT = (new Float( casostotal * (float)100000 / poblatotal)).floatValue();
      datos.ACTT = (new Float(casostotal * (float) 100000 / poblatotal)).
          floatValue();
      datos.ACUT = (new Float(casostotalacu * (float) 100000 / poblatotal)).
          floatValue();
      if ( ( (String) datos.ENFERMEDAD).trim().equals(CD_PARALISIS)) {
        datos.ACTT = (new Float(casostotal * (float) 100000 / poblatotalpar)).
            floatValue();
        datos.ACUT = (new Float(casostotalacu * (float) 100000 / poblatotalpar)).
            floatValue();

      }
      casostotal = 0;
      casostotalacu = 0;

      salida.setElementAt(datos, j);
    }

  }

  private void ponerValoresEnf(dataCons93 datos, parCons93 par) {
    // casos o tasas actuales
    //System.out.println("SRVCTCIENAREA: en ponerValoresEnf ");

    int i = 0;
    if (par.casosTasas) {
      if (i < datos.ACTS.size()) {
        datos.ACT1 = ( (Long) datos.ACTS.elementAt(i)).floatValue();
        i++;
        if (i < datos.ACTS.size()) {
          datos.ACT2 = ( (Long) datos.ACTS.elementAt(i)).floatValue();
          i++;
          if (i < datos.ACTS.size()) {
            datos.ACT3 = ( (Long) datos.ACTS.elementAt(i)).floatValue();
            i++;
            if (i < datos.ACTS.size()) {
              datos.ACT4 = ( (Long) datos.ACTS.elementAt(i)).floatValue();
              i++;
              if (i < datos.ACTS.size()) {
                datos.ACT5 = ( (Long) datos.ACTS.elementAt(i)).floatValue();
                i++;
              }
            }
          }
        }
      }
      i = 0;
      if (i < datos.ACUS.size()) {
        datos.ACU1 = ( (Long) datos.ACUS.elementAt(i)).floatValue();
        i++;
        if (i < datos.ACUS.size()) {
          datos.ACU2 = ( (Long) datos.ACUS.elementAt(i)).floatValue();
          i++;
          if (i < datos.ACUS.size()) {
            datos.ACU3 = ( (Long) datos.ACUS.elementAt(i)).floatValue();
            i++;
            if (i < datos.ACUS.size()) {
              datos.ACU4 = ( (Long) datos.ACUS.elementAt(i)).floatValue();
              i++;
              if (i < datos.ACUS.size()) {
                datos.ACU5 = ( (Long) datos.ACUS.elementAt(i)).floatValue();
                i++;
              }
            }
          }
        }
      }
    }
    else { //tasas
      if (i < datos.ACTS.size()) {
        // JRM:Pueden venir a null los datos, no entiendo la raz�n pero asi es.
        datos.ACT1 = 0;
        try {
          datos.ACT1 = ( (Float) datos.ACTS.elementAt(i)).floatValue();
        }
        catch (Exception E) {
          ;
        }
        i++;

        if (i < datos.ACTS.size()) {
          datos.ACT2 = 0;
          try {
            datos.ACT2 = ( (Float) datos.ACTS.elementAt(i)).floatValue();
          }
          catch (Exception E) {
            ;
          }
          i++;
          if (i < datos.ACTS.size()) {
            datos.ACT3 = 0;
            try {
              datos.ACT3 = ( (Float) datos.ACTS.elementAt(i)).floatValue();
            }
            catch (Exception E) {
              ;
            }
            i++;

            if (i < datos.ACTS.size()) {
              datos.ACT4 = 0;
              try {
                datos.ACT4 = ( (Float) datos.ACTS.elementAt(i)).floatValue();
              }
              catch (Exception E) {
                ;
              }
              i++;

              if (i < datos.ACTS.size()) {
                datos.ACT5 = 0;
                try {
                  datos.ACT5 = ( (Float) datos.ACTS.elementAt(i)).floatValue();
                }
                catch (Exception E) {
                  ;
                }
                i++;
              }
            }
          }
        }
      }
      i = 0;
      if (i < datos.ACUS.size()) {
        datos.ACU1 = 0;
        try {
          datos.ACU1 = ( (Float) datos.ACUS.elementAt(i)).floatValue();
        }
        catch (Exception E) {
          ;
        }
        i++;

        if (i < datos.ACUS.size()) {
          datos.ACU2 = 0;
          try {
            datos.ACU2 = ( (Float) datos.ACUS.elementAt(i)).floatValue();
          }
          catch (Exception E) {
            ;
          }
          i++;

          if (i < datos.ACUS.size()) {
            datos.ACU3 = 0;
            try {
              datos.ACU3 = ( (Float) datos.ACUS.elementAt(i)).floatValue();
            }
            catch (Exception E) {
              ;
            }
            i++;
            if (i < datos.ACUS.size()) {
              datos.ACU4 = 0;
              try {
                datos.ACU4 = ( (Float) datos.ACUS.elementAt(i)).floatValue();
              }
              catch (Exception E) {
                ;
              }
              i++;
              if (i < datos.ACUS.size()) {
                datos.ACU5 = 0;
                try {
                  datos.ACU5 = ( (Float) datos.ACUS.elementAt(i)).floatValue();
                }
                catch (Exception E) {
                  ;
                }
                i++;
              }
            }
          }
        }
      }

    }

  }

  private void ponerValoresResto(Vector salida, parCons93 par, int opmode) {

    // modificacion jlt
    // me recorro todas las enfermedades para rellenar todo
    for (int j = 0; j < salida.size(); j++) {
      //dataCons93 datos = (dataCons93) salida.elementAt(0);
      dataCons93 datos = (dataCons93) salida.elementAt(j);

      int i = 0;
      if (i < datos.COLS.size()) {
        ////////
        //if(datos.ENFERMEDAD.equals("RTUBER"))
        // datos.COL2 = (String)datos.COLS.elementAt(i);
        ///////
        //if (datos.POBS.elementAt(i)!=null){
        datos.COL1 = (String) datos.COLS.elementAt(i);
        i++;
        if (i < datos.COLS.size()) {
          //if (datos.POBS.elementAt(i)!=null){
          datos.COL2 = (String) datos.COLS.elementAt(i);
          i++;
          if (i < datos.COLS.size()) {
            //if (datos.POBS.elementAt(i)!=null){
            datos.COL3 = (String) datos.COLS.elementAt(i);
            i++;
            if (i < datos.COLS.size()) {
              //if (datos.POBS.elementAt(i)!=null){
              datos.COL4 = (String) datos.COLS.elementAt(i);
              i++;
              if (i < datos.COLS.size()) {
                //if (datos.POBS.elementAt(i)!=null){
                datos.COL5 = (String) datos.COLS.elementAt(i);
                i++;
              }
            }
          }
        }
      }

      // poblacion
      i = 0;
      if (i < datos.POBS.size()) {
        //if (datos.POBS.elementAt(i)!=null){
        datos.POB1 = ( (Long) datos.POBS.elementAt(i)).longValue();
        i++;
        if (i < datos.POBS.size()) {
          //if (datos.POBS.elementAt(i)!=null){
          datos.POB2 = ( (Long) datos.POBS.elementAt(i)).longValue();
          i++;
          if (i < datos.POBS.size()) {
            //if (datos.POBS.elementAt(i)!=null){
            datos.POB3 = ( (Long) datos.POBS.elementAt(i)).longValue();
            i++;
            if (i < datos.POBS.size()) {
              //if (datos.POBS.elementAt(i)!=null){
              datos.POB4 = ( (Long) datos.POBS.elementAt(i)).longValue();
              i++;
              if (i < datos.POBS.size()) {
                //if (datos.POBS.elementAt(i)!=null){
                datos.POB5 = ( (Long) datos.POBS.elementAt(i)).longValue();
                i++;
              }
            }
          }
        }
      }

      //notificadores reales actuales
      i = 0;
      if (i < datos.NOTRACTS.size()) {
        //if (datos.NOTRACTS.elementAt(i)!=null){
        datos.NOTRACT1 = ( (Integer) datos.NOTRACTS.elementAt(i)).intValue();
        i++;
        if (i < datos.NOTRACTS.size()) {
          //if (datos.NOTRACTS.elementAt(i)!=null){
          datos.NOTRACT2 = ( (Integer) datos.NOTRACTS.elementAt(i)).intValue();
          i++;
          if (i < datos.NOTRACTS.size()) {
            //if (datos.NOTRACTS.elementAt(i)!=null){
            datos.NOTRACT3 = ( (Integer) datos.NOTRACTS.elementAt(i)).intValue();
            i++;
            if (i < datos.NOTRACTS.size()) {
              //if (datos.NOTRACTS.elementAt(i)!=null){
              datos.NOTRACT4 = ( (Integer) datos.NOTRACTS.elementAt(i)).
                  intValue();
              i++;
              if (i < datos.NOTRACTS.size()) {
                //if (datos.NOTRACTS.elementAt(i)!=null){
                datos.NOTRACT5 = ( (Integer) datos.NOTRACTS.elementAt(i)).
                    intValue();
                i++;
              }
            }
          }
        }
      }
      //notificadores reales acumulados
      i = 0;
      if (i < datos.NOTRACUS.size()) {
        //if (datos.NOTRACUS.elementAt(i)!=null){
        datos.NOTRACU1 = ( (Integer) datos.NOTRACUS.elementAt(i)).intValue();
        i++;
        if (i < datos.NOTRACUS.size()) {
          //if (datos.NOTRACUS.elementAt(i)!=null){
          datos.NOTRACU2 = ( (Integer) datos.NOTRACUS.elementAt(i)).intValue();
          i++;
          if (i < datos.NOTRACUS.size()) {
            //if (datos.NOTRACUS.elementAt(i)!=null){
            datos.NOTRACU3 = ( (Integer) datos.NOTRACUS.elementAt(i)).intValue();
            i++;
            if (i < datos.NOTRACUS.size()) {
              //if (datos.NOTRACUS.elementAt(i)!=null){
              datos.NOTRACU4 = ( (Integer) datos.NOTRACUS.elementAt(i)).
                  intValue();
              i++;
              if (i < datos.NOTRACUS.size()) {
                //if (datos.NOTRACUS.elementAt(i)!=null){
                datos.NOTRACU5 = ( (Integer) datos.NOTRACUS.elementAt(i)).
                    intValue();
                i++;
              }
            }
          }
        }
      }

      //notificadores teoricos actuales
      i = 0;
      if (i < datos.NOTTACTS.size()) {
        //if (datos.NOTTACTS.elementAt(i)!=null){
        datos.NOTTACT1 = ( (Integer) datos.NOTTACTS.elementAt(i)).intValue();
        i++;
        if (i < datos.NOTTACTS.size()) {
          //if (datos.NOTTACTS.elementAt(i)!=null){
          datos.NOTTACT2 = ( (Integer) datos.NOTTACTS.elementAt(i)).intValue();
          i++;
          if (i < datos.NOTTACTS.size()) {
            //if (datos.NOTTACTS.elementAt(i)!=null){
            datos.NOTTACT3 = ( (Integer) datos.NOTTACTS.elementAt(i)).intValue();
            i++;
            if (i < datos.NOTTACTS.size()) {
              //if (datos.NOTTACTS.elementAt(i)!=null){
              datos.NOTTACT4 = ( (Integer) datos.NOTTACTS.elementAt(i)).
                  intValue();
              i++;
              if (i < datos.NOTTACTS.size()) {
                //if (datos.NOTTACTS.elementAt(i)!=null){
                datos.NOTTACT5 = ( (Integer) datos.NOTTACTS.elementAt(i)).
                    intValue();
                i++;
              }
            }
          }
        }
      }
      //notificadores teoricos acumulados
      i = 0;
      if (i < datos.NOTTACUS.size()) {
        //if (datos.NOTTACUS.elementAt(i)!=null){
        datos.NOTTACU1 = ( (Integer) datos.NOTTACUS.elementAt(i)).intValue();
        i++;
        if (i < datos.NOTTACUS.size()) {
          //if (datos.NOTTACUS.elementAt(i)!=null){
          datos.NOTTACU2 = ( (Integer) datos.NOTTACUS.elementAt(i)).intValue();
          i++;
          if (i < datos.NOTTACUS.size()) {
            //if (datos.NOTTACUS.elementAt(i)!=null){
            datos.NOTTACU3 = ( (Integer) datos.NOTTACUS.elementAt(i)).intValue();
            i++;
            if (i < datos.NOTTACUS.size()) {
              //if (datos.NOTTACUS.elementAt(i)!=null){
              datos.NOTTACU4 = ( (Integer) datos.NOTTACUS.elementAt(i)).
                  intValue();
              i++;
              if (i < datos.NOTTACUS.size()) {
                //if (datos.NOTTACUS.elementAt(i)!=null){
                datos.NOTTACU5 = ( (Integer) datos.NOTTACUS.elementAt(i)).
                    intValue();
                i++;
              }
            }
          }
        }
      }

      //coberturas actuales
      i = 0;
      if (i < datos.COBACTS.size()) {
        //if (datos.COBACTS.elementAt(i)!=null){
        datos.COBACT1 = ( (Float) datos.COBACTS.elementAt(i)).floatValue();
        i++;
        if (i < datos.COBACTS.size()) {
          //if (datos.COBACTS.elementAt(i)!=null){
          datos.COBACT2 = ( (Float) datos.COBACTS.elementAt(i)).floatValue();
          i++;
          if (i < datos.COBACTS.size()) {
            //if (datos.COBACTS.elementAt(i)!=null){
            datos.COBACT3 = ( (Float) datos.COBACTS.elementAt(i)).floatValue();
            i++;
            if (i < datos.COBACTS.size()) {
              //if (datos.COBACTS.elementAt(i)!=null){
              datos.COBACT4 = ( (Float) datos.COBACTS.elementAt(i)).floatValue();
              i++;
              if (i < datos.COBACTS.size()) {
                //if (datos.COBACTS.elementAt(i)!=null){
                datos.COBACT5 = ( (Float) datos.COBACTS.elementAt(i)).
                    floatValue();
                i++;
              }
            }
          }
        }
      }
      //coberturas acumulados
      i = 0;
      if (i < datos.COBACUS.size()) {
        //if (datos.COBACUS.elementAt(i)!=null){
        datos.COBACU1 = ( (Float) datos.COBACUS.elementAt(i)).floatValue();
        i++;
        if (i < datos.COBACUS.size()) {
          //if (datos.COBACUS.elementAt(i)!=null){
          datos.COBACU2 = ( (Float) datos.COBACUS.elementAt(i)).floatValue();
          i++;
          if (i < datos.COBACUS.size()) {
            //if (datos.COBACUS.elementAt(i)!=null){
            datos.COBACU3 = ( (Float) datos.COBACUS.elementAt(i)).floatValue();
            i++;
            if (i < datos.COBACUS.size()) {
              //if (datos.COBACUS.elementAt(i)!=null){
              datos.COBACU4 = ( (Float) datos.COBACUS.elementAt(i)).floatValue();
              i++;
              if (i < datos.COBACUS.size()) {
                //if (datos.COBACUS.elementAt(i)!=null){
                datos.COBACU5 = ( (Float) datos.COBACUS.elementAt(i)).
                    floatValue();
                i++;
              }
            }
          }
        }
      }

    } // enf for

  } // end class

//    private void ponerValoresResto(Vector salida, parCons93 par, int opmode){
//    dataCons93 datos = (dataCons93) salida.elementAt(0);

  //sirve para areas, distritos y zbs

  //rellenar con las descripciones

  //System.out.println("SRVCTCIENAREA: en ponerValoresResto ");

  /*
       Hashtable hash = new Hashtable();
       int ind =0;
       String cd = "";
       for (int i=0; i<par.lista.size(); i++){
    hash = (Hashtable)par.lista.elementAt(i);
    switch (opmode) {
      case servletCTCA:
        cd = (String)hash.get("CD_NIVEL_1");
        if (cd!=null){
          ind = datos.COLS.indexOf(cd);
          if ((String)hash.get("DS_NIVEL_1") != null){
            cd = (String)hash.get("DS_NIVEL_1");
          }
          else if((String)hash.get("DSL_NIVEL_1") != null){
            cd = (String)hash.get("DSL_NIVEL_1");
          }
          else {
            cd = (String)hash.get("CD_NIVEL_1");
          }
        }
        break;
      case servletCTAREA:
        cd = (String)hash.get("CD_NIVEL_2");
        if (cd!=null){
          ind = datos.COLS.indexOf(cd);
          if ((String)hash.get("DS_NIVEL_2") != null){
            cd = (String)hash.get("DS_NIVEL_2");
          }
          else if((String)hash.get("DSL_NIVEL_2") != null){
            cd = (String)hash.get("DSL_NIVEL_2");
          }
          else {
            cd = (String)hash.get("CD_NIVEL_2");
          }
        }
        break;
      case servletCTDIST:
        cd = (String)hash.get("CD_ZBS");
        if (cd!=null){
          ind = datos.COLS.indexOf(cd);
          if ((String)hash.get("DS_ZBS") != null){
            cd = (String)hash.get("DS_ZBS");
          }
          else if((String)hash.get("DSL_ZBS") != null){
            cd = (String)hash.get("DSL_ZBS");
          }
          else {
            cd = (String)hash.get("CD_ZBS");
          }
        }
        break;
    }
    switch (ind){
      case 0:
        datos.COL1 = cd;
        break;
      case 1:
        datos.COL2 = cd;
        break;
      case 2:
        datos.COL3 = cd;
        break;
      case 3:
        datos.COL4 = cd;
        break;
      case 4:
        datos.COL5 = cd;
        break;
    }
       }
   */

  /* jlt
       int i=0;
       if (i<datos.COLS.size()){
       //if (datos.POBS.elementAt(i)!=null){
    datos.COL1 = (String)datos.COLS.elementAt(i);
    i++;
    if (i<datos.COLS.size()){
    //if (datos.POBS.elementAt(i)!=null){
      datos.COL2 = (String)datos.COLS.elementAt(i);
      i++;
      if (i<datos.COLS.size()){
      //if (datos.POBS.elementAt(i)!=null){
        datos.COL3 = (String)datos.COLS.elementAt(i);
        i++;
        if (i<datos.COLS.size()){
        //if (datos.POBS.elementAt(i)!=null){
          datos.COL4 = (String)datos.COLS.elementAt(i);
          i++;
          if (i<datos.COLS.size()){
          //if (datos.POBS.elementAt(i)!=null){
            datos.COL5 = (String)datos.COLS.elementAt(i);
            i++;
          }
        }
      }
    }
       }
       // poblacion
       i=0;
       if (i<datos.POBS.size()){
       //if (datos.POBS.elementAt(i)!=null){
    datos.POB1 = ((Long)datos.POBS.elementAt(i)).longValue();
    i++;
    if (i<datos.POBS.size()){
    //if (datos.POBS.elementAt(i)!=null){
      datos.POB2 = ((Long)datos.POBS.elementAt(i)).longValue();
      i++;
      if (i<datos.POBS.size()){
      //if (datos.POBS.elementAt(i)!=null){
        datos.POB3 = ((Long)datos.POBS.elementAt(i)).longValue();
        i++;
        if (i<datos.POBS.size()){
        //if (datos.POBS.elementAt(i)!=null){
          datos.POB4 = ((Long)datos.POBS.elementAt(i)).longValue();
          i++;
          if (i<datos.POBS.size()){
          //if (datos.POBS.elementAt(i)!=null){
            datos.POB5 = ((Long)datos.POBS.elementAt(i)).longValue();
            i++;
          }
        }
      }
    }
       }
       //notificadores reales actuales
       i=0;
       if (i<datos.NOTRACTS.size()){
       //if (datos.NOTRACTS.elementAt(i)!=null){
    datos.NOTRACT1 = ((Integer)datos.NOTRACTS.elementAt(i)).intValue();
    i++;
    if (i<datos.NOTRACTS.size()){
    //if (datos.NOTRACTS.elementAt(i)!=null){
      datos.NOTRACT2 = ((Integer)datos.NOTRACTS.elementAt(i)).intValue();
      i++;
      if (i<datos.NOTRACTS.size()){
      //if (datos.NOTRACTS.elementAt(i)!=null){
        datos.NOTRACT3 = ((Integer)datos.NOTRACTS.elementAt(i)).intValue();
        i++;
        if (i<datos.NOTRACTS.size()){
        //if (datos.NOTRACTS.elementAt(i)!=null){
          datos.NOTRACT4 = ((Integer)datos.NOTRACTS.elementAt(i)).intValue();
          i++;
          if (i<datos.NOTRACTS.size()){
          //if (datos.NOTRACTS.elementAt(i)!=null){
            datos.NOTRACT5 = ((Integer)datos.NOTRACTS.elementAt(i)).intValue();
            i++;
          }
        }
      }
    }
       }
       //notificadores reales acumulados
       i=0;
       if (i<datos.NOTRACUS.size()){
       //if (datos.NOTRACUS.elementAt(i)!=null){
    datos.NOTRACU1 = ((Integer)datos.NOTRACUS.elementAt(i)).intValue();
    i++;
    if (i<datos.NOTRACUS.size()){
    //if (datos.NOTRACUS.elementAt(i)!=null){
      datos.NOTRACU2 = ((Integer)datos.NOTRACUS.elementAt(i)).intValue();
      i++;
      if (i<datos.NOTRACUS.size()){
      //if (datos.NOTRACUS.elementAt(i)!=null){
        datos.NOTRACU3 = ((Integer)datos.NOTRACUS.elementAt(i)).intValue();
        i++;
        if (i<datos.NOTRACUS.size()){
        //if (datos.NOTRACUS.elementAt(i)!=null){
          datos.NOTRACU4 = ((Integer)datos.NOTRACUS.elementAt(i)).intValue();
          i++;
          if (i<datos.NOTRACUS.size()){
          //if (datos.NOTRACUS.elementAt(i)!=null){
            datos.NOTRACU5 = ((Integer)datos.NOTRACUS.elementAt(i)).intValue();
            i++;
          }
        }
      }
    }
       }
       //notificadores teoricos actuales
       i=0;
       if (i<datos.NOTTACTS.size()){
       //if (datos.NOTTACTS.elementAt(i)!=null){
    datos.NOTTACT1 = ((Integer)datos.NOTTACTS.elementAt(i)).intValue();
    i++;
    if (i<datos.NOTTACTS.size()){
    //if (datos.NOTTACTS.elementAt(i)!=null){
      datos.NOTTACT2 = ((Integer)datos.NOTTACTS.elementAt(i)).intValue();
      i++;
      if (i<datos.NOTTACTS.size()){
      //if (datos.NOTTACTS.elementAt(i)!=null){
        datos.NOTTACT3 = ((Integer)datos.NOTTACTS.elementAt(i)).intValue();
        i++;
        if (i<datos.NOTTACTS.size()){
        //if (datos.NOTTACTS.elementAt(i)!=null){
          datos.NOTTACT4 = ((Integer)datos.NOTTACTS.elementAt(i)).intValue();
          i++;
          if (i<datos.NOTTACTS.size()){
          //if (datos.NOTTACTS.elementAt(i)!=null){
            datos.NOTTACT5 = ((Integer)datos.NOTTACTS.elementAt(i)).intValue();
            i++;
          }
        }
      }
    }
       }
       //notificadores teoricos acumulados
       i=0;
       if (i<datos.NOTTACUS.size()){
       //if (datos.NOTTACUS.elementAt(i)!=null){
    datos.NOTTACU1 = ((Integer)datos.NOTTACUS.elementAt(i)).intValue();
    i++;
    if (i<datos.NOTTACUS.size()){
    //if (datos.NOTTACUS.elementAt(i)!=null){
      datos.NOTTACU2 = ((Integer)datos.NOTTACUS.elementAt(i)).intValue();
      i++;
      if (i<datos.NOTTACUS.size()){
      //if (datos.NOTTACUS.elementAt(i)!=null){
        datos.NOTTACU3 = ((Integer)datos.NOTTACUS.elementAt(i)).intValue();
        i++;
        if (i<datos.NOTTACUS.size()){
        //if (datos.NOTTACUS.elementAt(i)!=null){
          datos.NOTTACU4 = ((Integer)datos.NOTTACUS.elementAt(i)).intValue();
          i++;
          if (i<datos.NOTTACUS.size()){
          //if (datos.NOTTACUS.elementAt(i)!=null){
            datos.NOTTACU5 = ((Integer)datos.NOTTACUS.elementAt(i)).intValue();
            i++;
          }
        }
      }
    }
       }
       //coberturas actuales
       i=0;
       if (i<datos.COBACTS.size()){
       //if (datos.COBACTS.elementAt(i)!=null){
    datos.COBACT1 = ((Float)datos.COBACTS.elementAt(i)).floatValue();
    i++;
    if (i<datos.COBACTS.size()){
    //if (datos.COBACTS.elementAt(i)!=null){
      datos.COBACT2 = ((Float)datos.COBACTS.elementAt(i)).floatValue();
      i++;
      if (i<datos.COBACTS.size()){
      //if (datos.COBACTS.elementAt(i)!=null){
        datos.COBACT3 = ((Float)datos.COBACTS.elementAt(i)).floatValue();
        i++;
        if (i<datos.COBACTS.size()){
        //if (datos.COBACTS.elementAt(i)!=null){
          datos.COBACT4 = ((Float)datos.COBACTS.elementAt(i)).floatValue();
          i++;
          if (i<datos.COBACTS.size()){
          //if (datos.COBACTS.elementAt(i)!=null){
            datos.COBACT5 = ((Float)datos.COBACTS.elementAt(i)).floatValue();
            i++;
          }
        }
      }
    }
       }
       //coberturas acumulados
       i=0;
       if (i<datos.COBACUS.size()){
       //if (datos.COBACUS.elementAt(i)!=null){
    datos.COBACU1 = ((Float)datos.COBACUS.elementAt(i)).floatValue();
    i++;
    if (i<datos.COBACUS.size()){
    //if (datos.COBACUS.elementAt(i)!=null){
      datos.COBACU2 = ((Float)datos.COBACUS.elementAt(i)).floatValue();
      i++;
      if (i<datos.COBACUS.size()){
      //if (datos.COBACUS.elementAt(i)!=null){
        datos.COBACU3 = ((Float)datos.COBACUS.elementAt(i)).floatValue();
        i++;
        if (i<datos.COBACUS.size()){
        //if (datos.COBACUS.elementAt(i)!=null){
          datos.COBACU4 = ((Float)datos.COBACUS.elementAt(i)).floatValue();
          i++;
          if (i<datos.COBACUS.size()){
          //if (datos.COBACUS.elementAt(i)!=null){
            datos.COBACU5 = ((Float)datos.COBACUS.elementAt(i)).floatValue();
            i++;
          }
        }
      }
    }
       }
     }
   */

  private void calcularTotalesEnf(Vector salida, parCons93 par) {
    //sirve para area, distrito y zbs
    dataCons93 datos = new dataCons93();
    //System.out.println("SRVCTCIENAREA: en calcularTotalesEnf ");

    for (int j = 0; j < salida.size(); j++) {
      datos = (dataCons93) salida.elementAt(j);
      ponerValoresEnf(datos, par);

      //solo si se han calculado casos, si son tasas
      // el calculo de los totales ya se ha hecho

      // modificacion jlt 26/12/2001
      // ya he calculado previamente los totales
      //   if(par.casosTasas) {
      //     datos.ACTT = datos.ACT1 + datos.ACT2 + datos.ACT3 + datos.ACT4 + datos.ACT5;
      //     datos.ACUT = datos.ACU1 + datos.ACU2 + datos.ACU3 + datos.ACU4 + datos.ACU5;
      //  }

    }
  }

  /*  private void calcularNotificadores(Connection con, PreparedStatement st, ResultSet rs,
                                       Vector salida, parCons93 par)
      throws Exception{
      // sirve para areas, distritos y niveles
      dataCons93 datos = (dataCons93) salida.elementAt(0);
      String query= "";
      String zonificacion = "";
      Hashtable elem2 = new Hashtable();
      Vector niveles2 = new Vector();
      // modificacion jlt
      // nos recorremos todas los niveles seleccionados
      for (int i =0;i<par.lista.size(); i++){
        elem2 = (Hashtable)par.lista.elementAt(i);
        niveles2.insertElementAt((String)elem2.get("CD_NIVEL_2"), i);
      }
      //////////////////
      //System.out.println("SRVCTCIENAREA: en calcularNotificadores ");
      if (!par.area.equals("")){
        if (!par.distrito.equals("")){
          //ZBS
          zonificacion = " cd_nivel_1 = ? and cd_nivel_2 = ? and cd_zbs = ? ";
        }
        else{
          //DISTRITO
          zonificacion = " cd_nivel_1 = ? and cd_nivel_2 = ? ";
        }
      }
      else {
        //AREA
        zonificacion = " cd_nivel_1 = ? ";
      }
      //para los actuales
      query = " select sum(nm_ntotreal), sum(nm_nnotift) "
            + " from sive_notif_sem "
            + " where cd_anoepi = ? "
            + " and cd_semepi >= ? and cd_semepi <= ?"
            + " and cd_e_notif in ( "
            + " select cd_e_notif from  sive_e_notif where " + zonificacion + " and cd_centro in ( "
            + " select cd_centro from sive_c_notif where it_cobertura = ? ))";
      int real = 0;
      int reales = 0;
      int teor = 0;
      int teors = 0;
      float cob = (float) 0.0;
      float cobs = (float) 0.0;
      //System.out.println("SRVCTCIENAREA: " + query);
      int i = 0;
      //while (datos.COLS.elementAt(i)!= null){
      while (i<datos.COLS.size()){
        st = con.prepareStatement(query);
        int param = 1;
        st.setString(param, par.ano); param++;
        st.setString(param, par.semD); param++;
        st.setString(param, par.semH); param++;
        if (!par.area.equals("")){
          if (!par.distrito.equals("")){
            //ZBS
            st.setString(param, par.area); param++;
            st.setString(param, par.distrito); param++;
            st.setString(param, (String)datos.COLS.elementAt(i)); param++;
          }
          else{
            //DISTRITO
            st.setString(param, par.area); param++;
            st.setString(param, (String)datos.COLS.elementAt(i)); param++;
          }
        }
        else {
          //AREA
          st.setString(param, (String)datos.COLS.elementAt(i)); param++;
        }
        st.setString(param, "S"); param++;
        // lanza la query
        rs = st.executeQuery();
        if (rs.next()) {
          real = rs.getInt(1);
          reales = reales + real;
          teor = rs.getInt(2);
          teors = teors + teor;
          cob = (float)real*(float)100/(float)teor;
          cobs = cobs + cob;
          datos.NOTRACTS.insertElementAt(new Integer(real),i);
          datos.NOTTACTS.insertElementAt(new Integer(teor),i);
          datos.COBACTS.insertElementAt(new Float(cob),i);
        }
        rs.close();
        rs = null;
        st.close();
        st= null;
        i++;
      }
      datos.NOTRACTT = reales;
      datos.NOTTACTT = teors;
      datos.COBACTT = cobs;
      //para los acumulados
      query = " select sum(nm_ntotreal), sum(nm_nnotift) "
            + " from sive_notif_sem "
            + " where cd_anoepi = ? "
            + " and cd_semepi >= ? and cd_semepi <= ? "
            + " and cd_e_notif in ( "
            + " select cd_e_notif from  sive_e_notif where " + zonificacion + " and cd_centro in ( "
            + " select cd_centro from sive_c_notif where it_cobertura = ? ))";
      i = 0;
      real = 0;
      reales = 0;
      teor = 0;
      teors = 0;
      cob = (float) 0.0;
      cobs = (float) 0.0;
      //System.out.println("SRVCTCIENAREA: " + query);
      //while (datos.COLS.elementAt(i)!= null){
      while (i<datos.COLS.size()){
        st = con.prepareStatement(query);
        int param = 1;
        st.setString(param, par.ano); param++;
        st.setString(param, "01"); param++;
        st.setString(param, par.semH); param++;
        if (!par.area.equals("")){
          if (!par.distrito.equals("")){
            //ZBS
            st.setString(param, par.area); param++;
            st.setString(param, par.distrito); param++;
            st.setString(param, (String)datos.COLS.elementAt(i)); param++;
          }
          else{
            //DISTRITO
            st.setString(param, par.area); param++;
            st.setString(param, (String)datos.COLS.elementAt(i)); param++;
          }
        }
        else {
          //AREA
          st.setString(param, (String)datos.COLS.elementAt(i)); param++;
        }
        st.setString(param, "S"); param++;
        // lanza la query
        rs = st.executeQuery();
        while (rs.next()) {
          real = rs.getInt(1);
          reales = reales + real;
          teor = rs.getInt(2);
          teors = teors + teor;
          cob = (float)real*(float)100/(float)teor;
          cobs = cobs + cob;
          datos.NOTRACUS.insertElementAt(new Integer(real),i);
          datos.NOTTACUS.insertElementAt(new Integer(teor),i);
          datos.COBACUS.insertElementAt(new Float(cob),i);
        }
        rs.close();
        rs = null;
        st.close();
        st= null;
        i++;
      }
      datos.NOTRACUT = reales;
      datos.NOTTACUT = teors;
      datos.COBACUT = cobs;
    } */

  // modificacion jlt
  private void calcularNotificadores(Connection con, PreparedStatement st,
                                     ResultSet rs,
                                     Vector salida, parCons93 par) throws
      Exception {
    // sirve para areas, distritos y niveles

    dataCons93 datos = (dataCons93) salida.elementAt(0);
    String query = "";
    String zonificacion = "";

    String queryTOT = "";
    String zonificacionTOT = "";

    Hashtable elem2 = new Hashtable();
    Vector niveles2 = new Vector();

    // modificacion jlt
    // dependiendo de los niveles

    if (!par.area.equals("")) {
      if (!par.distrito.equals("")) {
        //ZBS
        for (int i = 0; i < par.lista.size(); i++) {
          elem2 = (Hashtable) par.lista.elementAt(i);
          niveles2.insertElementAt( (String) elem2.get("CD_ZBS"), i);
        }
      }
      else {
        //DISTRITO
        for (int i = 0; i < par.lista.size(); i++) {
          elem2 = (Hashtable) par.lista.elementAt(i);
          niveles2.insertElementAt( (String) elem2.get("CD_NIVEL_2"), i);
        }
      }
    }
    else {
      //AREA
      for (int i = 0; i < par.lista.size(); i++) {
        elem2 = (Hashtable) par.lista.elementAt(i);
        niveles2.insertElementAt( (String) elem2.get("CD_NIVEL_1"), i);
      }
    }

    // se introduce la variable zonificaciontot para calcular los totales
    // de notificadores y cobertura del �mbito general (toda la comunidad, todo
    // el �rea, o todo el distrito -- igual que se hace con los casos y con
    // la poblacion --)
    if (!par.area.equals("")) {
      if (!par.distrito.equals("")) {
        //ZBS
        zonificacion = " cd_nivel_1 = ? and cd_nivel_2 = ? and cd_zbs = ? ";
        zonificacionTOT = " cd_nivel_1 = ? and cd_nivel_2 = ? and ";
      }
      else {
        //DISTRITO
        zonificacion = " cd_nivel_1 = ? and cd_nivel_2 = ? ";
        zonificacionTOT = " cd_nivel_1 = ? and ";
      }
    }
    else {
      //AREA
      zonificacion = " cd_nivel_1 = ? ";
      zonificacionTOT = " ";
    }

    //para los actuales
    query = " select sum(nm_ntotreal), sum(nm_nnotift) "
        + " from sive_notif_sem "
        + " where cd_anoepi = ? "
        + " and cd_semepi >= ? and cd_semepi <= ?"
        + " and cd_e_notif in ( "
        + " select cd_e_notif from  sive_e_notif where " + zonificacion +
        " and cd_centro in ( "
        + " select cd_centro from sive_c_notif where it_cobertura = ? ))";

    //para los TOTALES
    queryTOT = " select sum(nm_ntotreal), sum(nm_nnotift) "
        + " from sive_notif_sem "
        + " where cd_anoepi = ? "
        + " and cd_semepi >= ? and cd_semepi <= ?"
        + " and cd_e_notif in ( "
        + " select cd_e_notif from  sive_e_notif where " + zonificacionTOT +
        "  cd_centro in ( "
        + " select cd_centro from sive_c_notif where it_cobertura = ? ))";

    int real = 0;
    int reales = 0;
    int teor = 0;
    int teors = 0;
    float cob = (float) 0.0;
    float cobs = (float) 0.0;

    int i = 0;

    // modificacion jlt para recuperar los notificadores
    // de cada nivel
    while (i < niveles2.size()) {
      st = con.prepareStatement(query);

      int param = 1;
      st.setString(param, par.ano);
      param++;
      st.setString(param, par.semD);
      param++;
      st.setString(param, par.semH);
      param++;

      if (!par.area.equals("")) {
        if (!par.distrito.equals("")) {
          //ZBS
          st.setString(param, par.area);
          param++;
          st.setString(param, par.distrito);
          param++;
          //st.setString(param, (String)datos.COLS.elementAt(i)); param++;
          // modificacion jlt
          st.setString(param, (String) niveles2.elementAt(i));
          param++;
        }
        else {
          //DISTRITO
          st.setString(param, par.area);
          param++;
          // modificacion jlt
          st.setString(param, (String) niveles2.elementAt(i));
          param++;
        }
      }
      else {
        //AREA
        //st.setString(param, (String)datos.COLS.elementAt(i)); param++;
        // modificacion jlt
        st.setString(param, (String) niveles2.elementAt(i));
        param++;

      }
      st.setString(param, "S");
      param++;

      // lanza la query
      rs = st.executeQuery();

      // si no hay registros, en la posici�n i no habr� nada
      // esto servir� para en cliente separarlos bien por
      // niveles

      if (rs.next()) {
        real = rs.getInt(1);
        teor = rs.getInt(2);
        cob = (float) real * (float) 100 / (float) teor;
        // jlt para que cobs no adquiera valores 'malos'
        //if(teor != 0)
        //  cobs = cobs + cob;

        datos.NOTRACTS.insertElementAt(new Integer(real), i);
        datos.NOTTACTS.insertElementAt(new Integer(teor), i);
        datos.COBACTS.insertElementAt(new Float(cob), i);
      }

      rs.close();
      rs = null;
      st.close();
      st = null;

      i++;
    }

    /// se calculan los totales
    st = con.prepareStatement(queryTOT);

    int param = 1;
    st.setString(param, par.ano);
    param++;
    st.setString(param, par.semD);
    param++;
    st.setString(param, par.semH);
    param++;

    if (!par.area.equals("")) {
      if (!par.distrito.equals("")) {
        //ZBS
        st.setString(param, par.area);
        param++;
        st.setString(param, par.distrito);
        param++;
      }
      else {
        //DISTRITO
        st.setString(param, par.area);
        param++;

      }
    }
    else {
      //AREA

    }

    st.setString(param, "S");
    param++;

    // lanza la query
    rs = st.executeQuery();

    if (rs.next()) {
      reales = rs.getInt(1);
      teors = rs.getInt(2);
      cobs = (float) reales * (float) 100 / (float) teors;
    }

    rs.close();
    rs = null;
    st.close();
    st = null;

    datos.NOTRACTT = reales;
    datos.NOTTACTT = teors;
    datos.COBACTT = cobs;

    //para los acumulados
    // aqu� tambi�n se introduce el c�lculo de los totales
    query = " select sum(nm_ntotreal), sum(nm_nnotift) "
        + " from sive_notif_sem "
        + " where cd_anoepi = ? "
        + " and cd_semepi >= ? and cd_semepi <= ? "
        + " and cd_e_notif in ( "
        + " select cd_e_notif from  sive_e_notif where " + zonificacion +
        " and cd_centro in ( "
        + " select cd_centro from sive_c_notif where it_cobertura = ? ))";

    queryTOT = " select sum(nm_ntotreal), sum(nm_nnotift) "
        + " from sive_notif_sem "
        + " where cd_anoepi = ? "
        + " and cd_semepi >= ? and cd_semepi <= ? "
        + " and cd_e_notif in ( "
        + " select cd_e_notif from  sive_e_notif where " + zonificacionTOT +
        " cd_centro in ( "
        + " select cd_centro from sive_c_notif where it_cobertura = ? ))";

    i = 0;
    real = 0;
    reales = 0;
    teor = 0;
    teors = 0;
    cob = (float) 0.0;
    cobs = (float) 0.0;

    while (i < niveles2.size()) {
      st = con.prepareStatement(query);

      int paramACU = 1;
      st.setString(paramACU, par.ano);
      paramACU++;
      st.setString(paramACU, "01");
      paramACU++;
      st.setString(paramACU, par.semH);
      paramACU++;

      if (!par.area.equals("")) {
        if (!par.distrito.equals("")) {
          //ZBS
          st.setString(paramACU, par.area);
          paramACU++;
          st.setString(paramACU, par.distrito);
          paramACU++;
          st.setString(paramACU, (String) niveles2.elementAt(i));
          paramACU++;
        }
        else {
          //DISTRITO
          st.setString(paramACU, par.area);
          paramACU++;
          st.setString(paramACU, (String) niveles2.elementAt(i));
          paramACU++;
        }
      }
      else {
        //AREA
        st.setString(paramACU, (String) niveles2.elementAt(i));
        paramACU++;
      }
      st.setString(paramACU, "S");
      paramACU++;

      // lanza la query
      rs = st.executeQuery();

      while (rs.next()) {
        real = rs.getInt(1);
        teor = rs.getInt(2);
        cob = (float) real * (float) 100 / (float) teor;
        //if(teor != 0)
        //  cobs = cobs + cob;

        datos.NOTRACUS.insertElementAt(new Integer(real), i);
        datos.NOTTACUS.insertElementAt(new Integer(teor), i);
        datos.COBACUS.insertElementAt(new Float(cob), i);
      }
      rs.close();
      rs = null;
      st.close();
      st = null;

      i++;
    }

    // se calculan los totales
    st = con.prepareStatement(queryTOT);

    int paramACU = 1;
    st.setString(paramACU, par.ano);
    paramACU++;
    st.setString(paramACU, par.semD);
    paramACU++;
    st.setString(paramACU, par.semH);
    paramACU++;

    if (!par.area.equals("")) {
      if (!par.distrito.equals("")) {
        //ZBS
        st.setString(paramACU, par.area);
        paramACU++;
        st.setString(paramACU, par.distrito);
        paramACU++;
      }
      else {
        //DISTRITO
        st.setString(paramACU, par.area);
        paramACU++;

      }
    }
    else {
      //AREA

    }

    st.setString(paramACU, "S");
    paramACU++;

    // lanza la query
    rs = st.executeQuery();

    if (rs.next()) {
      reales = rs.getInt(1);
      teors = rs.getInt(2);
      cobs = (float) reales * (float) 100 / (float) teors;
    }

    rs.close();
    rs = null;
    st.close();
    st = null;

    datos.NOTRACUT = reales;
    datos.NOTTACUT = teors;
    datos.COBACUT = cobs;

  }

  /*
     private void rellenarPoblacion(Vector nivs, Vector pobs, Vector salida){
     // sirve para areas, distritos y zbs
    dataCons93 datos = (dataCons93) salida.elementAt(0);
    int indice = 0;
    long pobla = 0;
    //System.out.println("SRVCTCIENAREA: en rellenarporblacion niveles " + nivs + " poblacion " + pobs + " cols " + datos.COLS);
    int i = 0;
    //while (nivs.elementAt(i)!= null){
    datos.POBS = new Vector();
// *****  Comentado el 23-03-01 para intentar arreglar las poblaciones
// ***** 23-03-01 ****** Nuevo c�digo.... *********
    while (i<pobs.size()) {
      if (((Long)pobs.elementAt(i)).longValue()!=-1)
        pobla = pobla + ((Long)pobs.elementAt(i)).longValue();
      datos.POBS.addElement((Long)pobs.elementAt(i));
      i++;
    }
    datos.POBT = pobla;
     }
   */

//  private void rellenarPoblacion(Vector nivs, Vector pobs, Vector salida){
// calculamos el total de poblacion para cada nivel para
//la columna de total del informe
  private void rellenarPoblacion(Connection con, PreparedStatement st,
                                 ResultSet rs,
                                 Vector salida, parCons93 par, int opmode,
                                 Vector pobs) throws Exception {

    int i = 0;
    dataCons93 datos = (dataCons93) salida.elementAt(0);
    String query = "";

    // primero rellenamos la poblaci�n para cada caso
    while (i < pobs.size()) {

      if ( ( (Long) pobs.elementAt(i)).longValue() != -1) {

        datos.POBS.addElement( (Long) pobs.elementAt(i));
      }
      i++;
    }

    // ahora calculamos el total
    query = " select sum(nm_poblacion) "
        + " from sive_poblacion_ns "
        + " where cd_anoepi in (select max(cd_anoepi) "
        + " from sive_poblacion_ns where cd_anoepi <= ?) ";

    switch (opmode) {

      // casos o tasas por cien mil habitantes por area
      case servletCTAREA:

        query = query + " and cd_nivel_1 = ? ";

        break;

      case servletCTDIST:

        query = query + " and cd_nivel_1 = ? and cd_nivel_2 = ? ";

        break;

    }

    st = con.prepareStatement(query);
    // codigo
    st.setString(1, par.ano);

    switch (opmode) {

      // casos o tasas por cien mil habitantes por area
      case servletCTAREA:

        st.setString(2, par.area);

        break;

      case servletCTDIST:

        st.setString(2, par.area);
        st.setString(3, par.distrito);

        break;

    }

    // lanza la query
    rs = st.executeQuery();

    while (rs.next()) {

      //pobla.setElementAt(new Long(rs.getLong(1)), ind1);
      datos.POBT = rs.getLong(1);
    }
    rs.close();
    rs = null;
    st.close();
    st = null;

//    datos.POBT = pobla;

  }

  // calculamos el total de poblacion para cada nivel para
//la columna de total del informe
  private void rellenarPoblacionhab15(Connection con, PreparedStatement st,
                                      ResultSet rs,
                                      Vector salida, parCons93 par, int opmode,
                                      Vector pobs) throws Exception {

    int i = 0;
    dataCons93 datos = (dataCons93) salida.elementAt(0);
    String query = "";

    // primero rellenamos la poblaci�n para cada caso
    while (i < pobs.size()) {

      if ( ( (Long) pobs.elementAt(i)).longValue() != -1) {

        datos.POBS.addElement( (Long) pobs.elementAt(i));
      }
      i++;
    }

    switch (opmode) {

      // casos o tasas por cien mil habitantes por area
      case servletCTCA:

        query = new String();
        query = "select (tab_pob.sum_pob - tab_res.sum_res) total from "
            + " (select sum(nm_poblacion) as sum_pob "
            + " from sive_poblacion_ns tab_area "
            + " where tab_area.cd_anoepi in (select max(cd_anoepi) "
            + " from sive_poblacion_ns where cd_anoepi <= ?) ) tab_pob "
            + " , "
            + "(select sum(sum_pob) as sum_res "
            + "from sive_municipio tab_mun, "
            + "(select cd_mun ,nvl(sum(nm_poblacion),0) as sum_pob "
            + "from sive_poblacion_ng "
            + " where cd_anoepi in (select max(cd_anoepi) "
            + "   from sive_poblacion_ng where cd_anoepi <= ?) "
            + "   group by cd_mun having sum(nm_poblacion) <= " + pobmun +
            " ) tab_sum "
            + " where tab_mun.cd_mun = tab_sum.cd_mun "
            + " and tab_mun.cd_prov = " + prov
            + " ) tab_res ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.ano);

        // lanza la query
        rs = st.executeQuery();

        while (rs.next()) {

          //pobla.setElementAt(new Long(rs.getLong(1)), ind1);
          datos.POBT = rs.getLong(1);
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

      case servletCTAREA:

        query = new String();
        query = "select (tab_pob.sum_pob - tab_res.sum_res) total from "
            + " (select sum(nm_poblacion) as sum_pob "
            + " from sive_poblacion_ns tab_area "
            + " where tab_area.cd_anoepi in (select max(cd_anoepi) "
            + " from sive_poblacion_ns where cd_anoepi <= ?) and "
            + " tab_area.cd_nivel_1 = ? ) tab_pob"
            + " , "
            + "(select nvl(sum(sum_pob),0) as sum_res "
            + "from sive_municipio tab_mun, "
            + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
            + "from sive_poblacion_ng "
            + " where cd_anoepi in (select max(cd_anoepi) "
            + "   from sive_poblacion_ng where cd_anoepi <= ?) "
            + "   group by cd_mun having sum(nm_poblacion) <= 15000 ) tab_sum "
            + " where tab_mun.cd_mun = tab_sum.cd_mun "
            + " and tab_mun.cd_prov = " + prov
            + " and tab_mun.cd_nivel_1 = ? "
            + " ) tab_res ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);

        st.setString(2, par.area);

        st.setString(3, par.ano);

        st.setString(4, par.area);

        // lanza la query
        rs = st.executeQuery();

        while (rs.next()) {

          //pobla.setElementAt(new Long(rs.getLong(1)), ind1);
          datos.POBT = rs.getLong(1);
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

      case servletCTDIST:

        query = new String();
        query = "select (tab_pob.sum_pob - tab_res.sum_res) total from "
            + " (select sum(nm_poblacion) as sum_pob "
            + " from sive_poblacion_ns tab_area "
            + " where tab_area.cd_anoepi in (select max(cd_anoepi) "
            + " from sive_poblacion_ns where cd_anoepi <= ?) and "
            + " tab_area.cd_nivel_1 = ? and "
            + " tab_area.cd_nivel_2 = ? ) tab_pob "
            + " , "
            + "(select nvl(sum(sum_pob),0) as sum_res "
            + "from sive_municipio tab_mun, "
            + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
            + "from sive_poblacion_ng "
            + " where cd_anoepi in (select max(cd_anoepi) "
            + "   from sive_poblacion_ng where cd_anoepi <= ?) "
            + "   group by cd_mun having sum(nm_poblacion) <= 15000 ) tab_sum "
            + " where tab_mun.cd_mun = tab_sum.cd_mun "
            + " and tab_mun.cd_prov = " + prov
            + " and tab_mun.cd_nivel_1 = ? "
            + " and tab_mun.cd_nivel_2 = ? ) tab_res ";

        st = con.prepareStatement(query);

        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.area);
        st.setString(3, par.distrito);
        st.setString(4, par.ano);
        st.setString(5, par.area);
        st.setString(6, par.distrito);

        // lanza la query
        rs = st.executeQuery();

        while (rs.next()) {

          //pobla.setElementAt(new Long(rs.getLong(1)), ind1);
          datos.POBT = rs.getLong(1);
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

    }

  }

  // calculamos el total de poblacion para cada nivel para
//la columna de total del informe
// para poblacion < 15000 habitantes y enfermedad paralisis
  private float rellenarPoblacionhab15par(Connection con, PreparedStatement st,
                                          ResultSet rs,
                                          parCons93 par, int opmode) throws
      Exception {

    String query = "";
    float poblatotalhabpara = (float) 0;

    switch (opmode) {

      // casos o tasas por cien mil habitantes por area
      case servletCTCA:

        query = new String();
        query = "select (tab_pob.sum_pob - tab_res.sum_res) total from "
            + " (select sum(nm_poblacion) as sum_pob "
            + " from sive_poblacion_ns tab_area "
            + " where tab_area.cd_anoepi in (select max(cd_anoepi) "
            +
            " from sive_poblacion_ns where cd_anoepi <= ?) and nm_edad < 15 ) tab_pob "
            + " , "
            + "(select nvl(sum(sum_pob),0) as sum_res "
            + "from sive_municipio tab_mun, "
            + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
            + "from sive_poblacion_ng "
            + " where cd_anoepi in (select max(cd_anoepi) "
            + "   from sive_poblacion_ng where cd_anoepi <= ?) "
            + "   group by cd_mun having sum(nm_poblacion) <= " + pobmun +
            " ) tab_sum "
            + " where tab_mun.cd_mun = tab_sum.cd_mun "
            + " and tab_mun.cd_prov = " + prov
            + " ) tab_res ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.ano);

        // lanza la query
        rs = st.executeQuery();

        while (rs.next()) {

          //pobla.setElementAt(new Long(rs.getLong(1)), ind1);
          poblatotalhabpara = rs.getLong(1);
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

      case servletCTAREA:

        query = new String();
        query = "select (tab_pob.sum_pob - tab_res.sum_res) total from "
            + " (select sum(nm_poblacion) as sum_pob "
            + " from sive_poblacion_ns tab_area "
            + " where tab_area.cd_anoepi in (select max(cd_anoepi) "
            +
            " from sive_poblacion_ns where cd_anoepi <= ?) and nm_edad < 15 and "
            + " tab_area.cd_nivel_1 = ? ) tab_pob"
            + " , "
            + "(select nvl(sum(sum_pob),0) as sum_res "
            + "from sive_municipio tab_mun, "
            + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
            + "from sive_poblacion_ng "
            + " where cd_anoepi in (select max(cd_anoepi) "
            + "   from sive_poblacion_ng where cd_anoepi <= ?) "
            + "   group by cd_mun having sum(nm_poblacion) <= 15000 ) tab_sum "
            + " where tab_mun.cd_mun = tab_sum.cd_mun "
            + " and tab_mun.cd_prov = " + prov
            + " and tab_mun.cd_nivel_1 = ? "
            + " ) tab_res ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);

        st.setString(2, par.area);

        st.setString(3, par.ano);

        st.setString(4, par.area);

        // lanza la query
        rs = st.executeQuery();

        while (rs.next()) {

          //pobla.setElementAt(new Long(rs.getLong(1)), ind1);
          poblatotalhabpara = rs.getLong(1);
        }
        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

      case servletCTDIST:

        query = new String();
        query = "select (tab_pob.sum_pob - tab_res.sum_res) total from "
            + " (select sum(nm_poblacion) as sum_pob "
            + " from sive_poblacion_ns tab_area "
            + " where tab_area.cd_anoepi in (select max(cd_anoepi) "
            +
            " from sive_poblacion_ns where cd_anoepi <= ?) and nm_edad < 15 and "
            + " tab_area.cd_nivel_1 = ? and "
            + " tab_area.cd_nivel_2 = ? ) tab_pob "
            + " , "
            + "(select nvl(sum(sum_pob),0) as sum_res "
            + "from sive_municipio tab_mun, "
            + "(select cd_mun ,sum(nm_poblacion) as sum_pob "
            + "from sive_poblacion_ng "
            + " where cd_anoepi in (select max(cd_anoepi) "
            + "   from sive_poblacion_ng where cd_anoepi <= ?) "
            + "   group by cd_mun having sum(nm_poblacion) <= 15000 ) tab_sum "
            + " where tab_mun.cd_mun = tab_sum.cd_mun "
            + " and tab_mun.cd_prov = " + prov
            + " and tab_mun.cd_nivel_1 = ? "
            + " and tab_mun.cd_nivel_2 = ? ) tab_res ";

        st = con.prepareStatement(query);

        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.area);
        st.setString(3, par.distrito);
        st.setString(4, par.ano);
        st.setString(5, par.area);
        st.setString(6, par.distrito);

        // lanza la query
        rs = st.executeQuery();

        while (rs.next()) {

          //pobla.setElementAt(new Long(rs.getLong(1)), ind1);
          poblatotalhabpara = rs.getLong(1);
        }

        rs.close();
        rs = null;
        st.close();
        st = null;

        break;

    }

    return poblatotalhabpara;

  }

  // calculamos el total de poblacion para cada nivel para
//la columna de total del informe
// para la poblacion menor de 15 a�os
  private float rellenarPoblacionpara(Connection con, PreparedStatement st,
                                      ResultSet rs,
                                      parCons93 par, int opmode) throws
      Exception {

    String query = "";
    float poblatotalpara = (float) 0;

    // ahora calculamos el total
    query = " select sum(nm_poblacion) "
        + " from sive_poblacion_ns "
        + " where cd_anoepi in (select max(cd_anoepi) "
        + " from sive_poblacion_ns where cd_anoepi <= ?) "
        + " and nm_edad < 15 ";

    switch (opmode) {

      // casos o tasas por cien mil habitantes por area
      case servletCTAREA:

        query = query + " and cd_nivel_1 = ? ";

        break;

      case servletCTDIST:

        query = query + " and cd_nivel_1 = ? and cd_nivel_2 = ? ";

        break;

    }

    st = con.prepareStatement(query);
    // codigo
    st.setString(1, par.ano);

    switch (opmode) {

      case servletCTAREA:

        st.setString(2, par.area);

        break;

      case servletCTDIST:

        st.setString(2, par.area);
        st.setString(3, par.distrito);

        break;

    }

    // lanza la query
    rs = st.executeQuery();

    while (rs.next()) {

      poblatotalpara = (new Long(rs.getLong(1))).floatValue();
    }

    rs.close();
    rs = null;
    st.close();
    st = null;

    return poblatotalpara;

  }

  /*  private dataCons93 rellenaActDataCons93(String enf, Vector cols){
      Vector parNC = new Vector();
      dataCons93 datos = new dataCons93();
      datos.ENFERMEDAD = enf;
      int i = 0;
      //System.out.println("SRVCTCIENAREA: rellenaActDataCons93 cols " + cols.size());
      //while (cols.elementAt(i)!= null){
      //AIC
      datos.ACTS = new Vector();
      while (i<cols.size()){
        parNC = (Vector) cols.elementAt(i);
        datos.COLS.insertElementAt((String)parNC.elementAt(0), i);
        //AIC
        datos.ACTS.addElement((Long)parNC.elementAt(1));
        //datos.ACTS.insertElementAt((Long)parNC.elementAt(1), i);
        i++;
      }
      return datos;
    }*/

  // modificacion jlt  m
  private dataCons93 rellenaActDataCons93(String enf, Vector cols) {
    Vector parNC = new Vector();
    dataCons93 datos = new dataCons93();
    datos.ENFERMEDAD = enf;

    //trazaLog("entro en rellenaActDataCons93");
    //trazaLog("enfermedad" + enf);

    int i = 0;
    //System.out.println("SRVCTCIENAREA: rellenaActDataCons93 cols " + cols.size());

    //while (cols.elementAt(i)!= null){
    //AIC
    // jlt
    datos.ACTS = new Vector();

    int ind = 0;

    // relleno el datos.ACTS con valores vac�os, luego ya
    // insertar�
    for (int j = 0; j < 5; j++) {
      datos.ACTS.addElement(new Long(0));

    }

    while (i < cols.size()) {
      parNC = (Vector) cols.elementAt(i);
      //jlt
      ind = nivelescondatos.indexOf( (String) parNC.elementAt(0));

      //trazaLog("valor de ind" + (new Integer(ind)).toString());
      //trazaLog("valor de i" + (new Integer(i)).toString());

//      datos.COLS.insertElementAt((String)parNC.elementAt(0), i);
// modificacion jlt 04/01/02
//      datos.COLS.insertElementAt((String)parNC.elementAt(0), i);
      //AIC
      //datos.ACTS.addElement((Long)parNC.elementAt(1));
      datos.ACTS.insertElementAt( (Long) parNC.elementAt(1), ind);

      // modificacion jlt
      datos.ACTT = ( (Long) parNC.elementAt(2)).floatValue();

      i++;
    }
    return datos;
  }

  private void rellenaAcuDataCons93(String enf, Vector cols, Vector salida) {

    //System.out.println("SRVCTCIENAREA: rellenaAcuDataCons93 cols " + cols);

    Vector parNC = new Vector();
    dataCons93 datos = new dataCons93();
    int j = 0;
    for (j = 0; j < salida.size(); j++) {
      datos = (dataCons93) salida.elementAt(j);
      if (datos.ENFERMEDAD.equals(enf)) {
        //System.out.println("SRVCTCIENAREA: encontrada enf  " + enf);
        break;
      }
    }

    int i = 0;
    //while (cols.elementAt(i)!= null){
    datos.ACUS = new Vector();

    int ind = 0;

    // relleno el datos.ACUS con valores vac�os, luego ya
    // insertar�
    for (int k = 0; k < 5; k++) {
      datos.ACUS.addElement(new Long(0));

    }
    while (i < cols.size()) {
      //AIC

      //jlt
      //Vector tmp = new Vector();
      //tmp = (Vector) cols.elementAt(i);
      parNC = (Vector) cols.elementAt(i);
      //ind = nivelescondatos.indexOf((String)tmp.elementAt(0));

      // modificacion jlt 04/01/02
//      ind = nivelescondatos.indexOf((String)parNC.elementAt(0));
      ind = nivelescondatosacu.indexOf( (String) parNC.elementAt(0));

//datos.ACUS.addElement((Long)cols.elementAt(i));
      // modificacion jlt
      //datos.ACUS.insertElementAt((Long)cols.elementAt(i), ind);

      ///////modificacion 04/01/2002
      datos.COLS.insertElementAt( (String) parNC.elementAt(0), i);
      datos.ACUS.insertElementAt( (Long) parNC.elementAt(1), ind);
      datos.ACUT = ( (Long) parNC.elementAt(2)).floatValue();
      i++;
    }
    //AIC
    if (j < salida.size()) {
      salida.setElementAt(datos, j); ;
    }
  }

  /*
    public CLista doPrueba(int opmode, CLista param) throws Exception {
        jdcdriver = new JDCConnectionDriver ("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                             "sive_desa",
                             "sive_desa");
        con = openConnection();
        return doWork(opmode, param);
    }
   */

  private Vector obtenerNiveles(Connection con, PreparedStatement st,
                                ResultSet rs,
                                int opmode, parCons93 par) throws Exception {

    String query = new String();
    Vector niv = new Vector();
    String nivel = new String();

    switch (opmode) {

      // casos o tasas por cien mil habitantes por area
      case servletCTCA:

        // prepara la query para los actuales
        query = " select distinct cd_nivel_1 "
            + " from sive_resumen_edos "
            + " where cd_anoepi = ? "
            + " and cd_semepi >= ? and cd_semepi <= ? ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_1 in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( cd_nivel_1 = ? ) ";
            }
            else {
              query = query + " and ( cd_nivel_1 = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or cd_nivel_1 = ? ) ";
          }
          else {
            query = query + " or cd_nivel_1 = ? ";
          }
        }

        query = query + " order by cd_nivel_1 ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.semD);
        // modificacion jlt 04/01/02
//          st.setString(2, "01");
        st.setString(3, par.semH);

        Hashtable elem = new Hashtable();
        //System.out.println("SRVCTCIENAREA: lista " + par.lista);
        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);
          //System.out.println("SRVCTCIENAREA: elemeto de la lista " + elem.toString());
          st.setString(i + 4, (String) elem.get("CD_NIVEL_1"));
        }

        // lanza la query
        rs = st.executeQuery();

        while (rs.next()) {
          //control de tama�o
          nivel = rs.getString("CD_NIVEL_1");
          niv.addElement(nivel);

        }
        rs.close();
        rs = null;
        st.close();
        st = null;
        break;

      case servletCTAREA:

        // prepara la query para los actuales
        query = " select distinct cd_nivel_2"
            + " from sive_resumen_edos "
            + " where cd_anoepi = ? "
            + " and cd_semepi >= ? and cd_semepi <= ? "
            + " and cd_nivel_1 = ? ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_2 in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( cd_nivel_2 = ? ) ";
            }
            else {
              query = query + " and ( cd_nivel_2 = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or cd_nivel_2 = ? ) ";
          }
          else {
            query = query + " or cd_nivel_2 = ? ";
          }
        }

        query = query + " order by cd_nivel_2 ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.semD);
        st.setString(3, par.semH);
        st.setString(4, par.area);

        elem = new Hashtable();

        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);

          st.setString(i + 5, (String) elem.get("CD_NIVEL_2"));
        }

        // lanza la query
        rs = st.executeQuery();

        while (rs.next()) {
          //control de tama�o
          nivel = rs.getString("CD_NIVEL_2");
          niv.addElement(nivel);

        }

        rs.close();
        rs = null;
        st.close();
        st = null;
        break;

      case servletCTDIST:

        // prepara la query para los actuales
        query = " select distinct cd_zbs "
            + " from sive_resumen_edos "
            + " where cd_anoepi = ? "
            + " and cd_semepi >= ? and cd_semepi <= ? "
            + " and cd_nivel_1 = ? "
            + " and cd_nivel_2 = ? ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_zbs in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( cd_zbs = ? ) ";
            }
            else {
              query = query + " and ( cd_zbs = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or cd_zbs = ? ) ";
          }
          else {
            query = query + " or cd_zbs = ? ";
          }
        }
        query = query + " order by cd_zbs ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.semD);
        st.setString(3, par.semH);
        st.setString(4, par.area);
        st.setString(5, par.distrito);

        elem = new Hashtable();

        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);

          st.setString(i + 6, (String) elem.get("CD_ZBS"));
        }

        // lanza la query
        rs = st.executeQuery();

        while (rs.next()) {
          //control de tama�o
          nivel = rs.getString("CD_ZBS");
          niv.addElement(nivel);

        }
        rs.close();
        rs = null;
        st.close();
        st = null;
        break;

    } // end switch

    return niv;

  } //end obtener niveles

  private Vector obtenerNivelesacu(Connection con, PreparedStatement st,
                                   ResultSet rs,
                                   int opmode, parCons93 par, String codsEnf) throws
      Exception {

    String query = new String();
    Vector niv = new Vector();
    String nivel = new String();

    switch (opmode) {

      // casos o tasas por cien mil habitantes por area
      case servletCTCA:

        // prepara la query para los actuales
        query = " select distinct cd_nivel_1 "
            + " from sive_resumen_edos "
            + " where cd_anoepi = ? "
            + " and cd_semepi >= ? and cd_semepi <= ? "
            // modificacion jlt 04/01/02
            + " and cd_enfcie in " + codsEnf;

        for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_1 in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( cd_nivel_1 = ? ) ";
            }
            else {
              query = query + " and ( cd_nivel_1 = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or cd_nivel_1 = ? ) ";
          }
          else {
            query = query + " or cd_nivel_1 = ? ";
          }
        }

        query = query + " order by cd_nivel_1 ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
//          st.setString(2, par.semD);
        // modificacion jlt 04/01/02
        st.setString(2, "01");
        st.setString(3, par.semH);

        Hashtable elem = new Hashtable();
        //System.out.println("SRVCTCIENAREA: lista " + par.lista);
        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);
          //System.out.println("SRVCTCIENAREA: elemeto de la lista " + elem.toString());
          st.setString(i + 4, (String) elem.get("CD_NIVEL_1"));
        }

        // lanza la query
        rs = st.executeQuery();

        while (rs.next()) {
          //control de tama�o
          nivel = rs.getString("CD_NIVEL_1");
          niv.addElement(nivel);

        }
        rs.close();
        rs = null;
        st.close();
        st = null;
        break;

      case servletCTAREA:

        // prepara la query para los actuales
        query = " select distinct cd_nivel_2"
            + " from sive_resumen_edos "
            + " where cd_anoepi = ? "
            + " and cd_semepi >= ? and cd_semepi <= ? "
            // modificacion jlt 04/01/02
            + " and cd_enfcie in " + codsEnf

            + " and cd_nivel_1 = ? ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_nivel_2 in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( cd_nivel_2 = ? ) ";
            }
            else {
              query = query + " and ( cd_nivel_2 = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or cd_nivel_2 = ? ) ";
          }
          else {
            query = query + " or cd_nivel_2 = ? ";
          }
        }

        query = query + " order by cd_nivel_2 ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        //st.setString(2, par.semD);
        // modificacion jlt 04/01/02
        st.setString(2, "01");
        st.setString(3, par.semH);
        st.setString(4, par.area);

        elem = new Hashtable();

        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);

          st.setString(i + 5, (String) elem.get("CD_NIVEL_2"));
        }

        // lanza la query
        rs = st.executeQuery();

        while (rs.next()) {
          //control de tama�o
          nivel = rs.getString("CD_NIVEL_2");
          niv.addElement(nivel);

        }

        rs.close();
        rs = null;
        st.close();
        st = null;
        break;

      case servletCTDIST:

        // prepara la query para los actuales
        query = " select distinct cd_zbs "
            + " from sive_resumen_edos "
            + " where cd_anoepi = ? "
            + " and cd_semepi >= ? and cd_semepi <= ? "
            + " and cd_nivel_1 = ? "
            + " and cd_nivel_2 = ? ";

        for (int i = 0; i < par.lista.size(); i++) { //cd_zbs in ()
          if (i == 0) {
            if (i == par.lista.size() - 1) {
              query = query + " and ( cd_zbs = ? ) ";
            }
            else {
              query = query + " and ( cd_zbs = ? ";
            }
          }
          else if (i == par.lista.size() - 1) {
            query = query + " or cd_zbs = ? ) ";
          }
          else {
            query = query + " or cd_zbs = ? ";
          }
        }
        query = query + " order by cd_zbs ";

        st = con.prepareStatement(query);
        // codigo
        st.setString(1, par.ano);
        st.setString(2, par.semD);
        st.setString(3, par.semH);
        st.setString(4, par.area);
        st.setString(5, par.distrito);

        elem = new Hashtable();

        for (int i = 0; i < par.lista.size(); i++) {
          elem = (Hashtable) par.lista.elementAt(i);

          st.setString(i + 6, (String) elem.get("CD_ZBS"));
        }

        // lanza la query
        rs = st.executeQuery();

        while (rs.next()) {
          //control de tama�o
          nivel = rs.getString("CD_ZBS");
          niv.addElement(nivel);

        }
        rs.close();
        rs = null;
        st.close();
        st = null;
        break;

    } // end switch

    return niv;

  } //end obtener niveles

} // end class
