//Title:        Casos o Tasas por 100000 habitantes por Area
//Version:
//Copyright:    Copyright (c) 1998
//Author:
//Company:
//Description:  Consultas 9.3.10, 9.3.11 y 9.3.12

package casostasascienarea;

import java.io.Serializable;
import java.util.Vector;

public class dataCons93
    implements Serializable {

  //para el primer informe
  public String codigo = "";
  public String descrip = "";

  //para el segundo informe

  //habra varios datas con la parte de poblacion vacia (para
  //las lineas de enfermedades) y el ultimo data tendra valores
  //para las lineas de poblacion

  public String ENFERMEDAD = "";

  //se rellena con el numero de columnas que ha elegido el usuario
  public int columnas = 0;

  public String COL1 = "";
  public String COL2 = "";
  public String COL3 = "";
  public String COL4 = "";
  public String COL5 = "";
  public String COLT = "T";

  public Vector COLS = null;

  public float ACT1 = (float) 0.0;
  public float ACT2 = (float) 0.0;
  public float ACT3 = (float) 0.0;
  public float ACT4 = (float) 0.0;
  public float ACT5 = (float) 0.0;
  public float ACTT = (float) 0.0;

  public Vector ACTS = null;

  public float ACU1 = (float) 0.0;
  public float ACU2 = (float) 0.0;
  public float ACU3 = (float) 0.0;
  public float ACU4 = (float) 0.0;
  public float ACU5 = (float) 0.0;
  public float ACUT = (float) 0.0;

  public Vector ACUS = null;

  public long POB1 = 0;
  public long POB2 = 0;
  public long POB3 = 0;
  public long POB4 = 0;
  public long POB5 = 0;
  public long POBT = 0;

  public Vector POBS = null;

  public int NOTTACT1 = 0;
  public int NOTTACT2 = 0;
  public int NOTTACT3 = 0;
  public int NOTTACT4 = 0;
  public int NOTTACT5 = 0;
  public int NOTTACTT = 0;

  public Vector NOTTACTS = null;

  public int NOTTACU1 = 0;
  public int NOTTACU2 = 0;
  public int NOTTACU3 = 0;
  public int NOTTACU4 = 0;
  public int NOTTACU5 = 0;
  public int NOTTACUT = 0;

  public Vector NOTTACUS = null;

  public int NOTRACT1 = 0;
  public int NOTRACT2 = 0;
  public int NOTRACT3 = 0;
  public int NOTRACT4 = 0;
  public int NOTRACT5 = 0;
  public int NOTRACTT = 0;

  public Vector NOTRACTS = null;

  public int NOTRACU1 = 0;
  public int NOTRACU2 = 0;
  public int NOTRACU3 = 0;
  public int NOTRACU4 = 0;
  public int NOTRACU5 = 0;
  public int NOTRACUT = 0;

  public Vector NOTRACUS = null;

  public float COBACT1 = (float) 0.0;
  public float COBACT2 = (float) 0.0;
  public float COBACT3 = (float) 0.0;
  public float COBACT4 = (float) 0.0;
  public float COBACT5 = (float) 0.0;
  public float COBACTT = (float) 0.0;

  public Vector COBACTS = null;

  public float COBACU1 = (float) 0.0;
  public float COBACU2 = (float) 0.0;
  public float COBACU3 = (float) 0.0;
  public float COBACU4 = (float) 0.0;
  public float COBACU5 = (float) 0.0;
  public float COBACUT = (float) 0.0;

  public Vector COBACUS = null;

  public dataCons93() {
    COLS = new Vector();
    ACTS = new Vector();
    ACUS = new Vector();
    POBS = new Vector();
    NOTTACTS = new Vector();
    NOTTACUS = new Vector();
    NOTRACTS = new Vector();
    NOTRACUS = new Vector();
    COBACTS = new Vector();
    COBACUS = new Vector();

  }
}