//Title:        Casos o Tasas por 100000 habitantes por Area
//Version:
//Copyright:    Copyright (c) 1998
//Author:
//Company:
//Description:  Consultas 9.3.10, 9.3.11 y 9.3.12

package casostasascienarea;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaADZE;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import notutil.UtilEDO;
import sapp.StubSrvBD;
import utilidades.PanDosFechasUnAno;
import vCasIn.DataMun;
import zbs.DataZBS;

public class panelConsulta
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  //rutas imagenes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/vaciar.gif",
      "images/grafico.gif"};

  protected CCargadorImagen imgs = null;

  protected StubSrvBD stubCliente = new StubSrvBD();

  public parCons93 parCons;

  protected int modoOperacion = modoNORMAL;

  protected panelInforme informe = null;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";
//  final String strSERVLETZona = "servlet/SrvMun";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  // modos de la lista
  final int AREA = 12;
  final int DISTRITO = 13;
  final int ZBS = 14;
  final int ENFERMEDAD = 15;

  //columnas de los informes
  final int COL_AREA = 5;
  final int COL_DISTRITO = 5;
  final int COL_ZBS = 5;
  final int COL_ENFERMEDAD = 13;

  XYLayout xYLayout = new XYLayout();

  Label lblArea = new Label();
  CCampoCodigo txtCodAre = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarAre = new ButtonControl();
  TextField txtDesAre = new TextField();
  Label lblDistrito = new Label();
  CCampoCodigo txtCodDis = new CCampoCodigo(); /*E*/
  TextField txtDesDis = new TextField();
  ButtonControl btnCtrlBuscarDis = new ButtonControl();
  PanDosFechasUnAno panFechas;

  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  focusAdapter txtFocusAdapter = new focusAdapter(this);
  txt_keyAdapter txtKeyAdapter = new txt_keyAdapter(this);
  actionListener btnActionListener = new actionListener(this);

  Checkbox checkCasos = new Checkbox();
  Checkbox checkTasas = new Checkbox();
  CheckboxGroup checkG = new CheckboxGroup();

  Checkbox habitantes15 = new Checkbox();

  public panelConsulta(CApp a) {
    try {

      setApp(a);
      res = ResourceBundle.getBundle("casostasascienarea.Res" + app.getIdioma());
      informe = new panelInforme(a);
      panFechas = new PanDosFechasUnAno(this,
                                        PanDosFechasUnAno.modoINICIO_SEMANA, false);
      jbInit();
      lblArea.setText(this.app.getNivel1() + ":");
      lblDistrito.setText(this.app.getNivel2() + ":");
      this.txtCodAre.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesAre.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtCodDis.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesDis.setText(this.app.getDS_NIVEL2_DEFECTO());
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(538);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));

    // gestores de eventos
    btnCtrlBuscarAre.addActionListener(btnActionListener);
    btnCtrlBuscarDis.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    txtCodAre.addKeyListener(txtKeyAdapter);
    txtCodDis.addKeyListener(txtKeyAdapter);

    txtCodAre.addFocusListener(txtFocusAdapter);
    txtCodDis.addFocusListener(txtFocusAdapter);

    checkCasos.setLabel(res.getString("checkCasos.Label"));
    checkTasas.setLabel(res.getString("checkTasas.Label"));
    checkTasas.setCheckboxGroup(checkG);
    checkCasos.setCheckboxGroup(checkG);
    checkCasos.setState(true);
    checkTasas.setState(false);

    btnCtrlBuscarAre.setActionCommand("buscarAre");
    txtDesAre.setEditable(false);
    txtDesAre.setEnabled(false); /*E*/
    btnCtrlBuscarDis.setActionCommand("buscarDis");
    txtDesDis.setEditable(false);
    txtDesDis.setEnabled(false); /*E*/

    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    txtCodAre.setName("txtCodAre");
    txtCodDis.setName("txtCodDis");

    lblArea.setText(res.getString("lblArea.Text"));

    xYLayout.setWidth(596);
    this.setLayout(xYLayout);

    this.add(panFechas, new XYConstraints(25, 25, -1, -1));
    /*this.add(checkCasos, new XYConstraints(25, 100, 75, -1));
         this.add(checkTasas, new XYConstraints(143, 100, 77, -1));
         this.add(lblArea, new XYConstraints(25, 130, -1, -1));
         this.add(txtCodAre, new XYConstraints(143, 130, 77, -1));
         this.add(btnCtrlBuscarAre, new XYConstraints(225, 130, -1, -1));
         this.add(txtDesAre, new XYConstraints(254, 130, 287, -1));
         this.add(lblDistrito, new XYConstraints(26, 160, 52, -1));
         this.add(txtCodDis, new XYConstraints(143, 160, 77, -1));
         this.add(btnCtrlBuscarDis, new XYConstraints(225, 160, -1, -1));
         this.add(txtDesDis, new XYConstraints(254, 160, 287, -1));
     */
    this.add(lblArea, new XYConstraints(25, 100, -1, -1));
    this.add(txtCodAre, new XYConstraints(143, 100, 77, -1));
    this.add(btnCtrlBuscarAre, new XYConstraints(225, 100, -1, -1));
    this.add(txtDesAre, new XYConstraints(254, 100, 287, -1));
    this.add(lblDistrito, new XYConstraints(26, 130, -1, -1));
    this.add(txtCodDis, new XYConstraints(143, 130, 77, -1));
    this.add(btnCtrlBuscarDis, new XYConstraints(225, 130, -1, -1));
    this.add(txtDesDis, new XYConstraints(254, 130, 287, -1));
    this.add(checkCasos, new XYConstraints(25, 160, -1, -1));
    this.add(checkTasas, new XYConstraints(143, 160, -1, -1));

    this.add(btnLimpiar, new XYConstraints(390, 250, -1, -1));
    this.add(btnInforme, new XYConstraints(475, 250, -1, -1));
    this.add(habitantes15, new XYConstraints(25, 200, -1, -1));

    imgs = new CCargadorImagen(app, imgNAME);
    habitantes15.setLabel(res.getString("habitantes15.Label"));
    imgs.CargaImagenes();
    btnCtrlBuscarAre.setImage(imgs.getImage(0));
    btnCtrlBuscarDis.setImage(imgs.getImage(0));
    btnLimpiar.setImage(imgs.getImage(1));
    btnInforme.setImage(imgs.getImage(2));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    CMessage msgBox; //LRG
    UtilEDO util = new UtilEDO(); //Para comparar fechas
    String msg = "";
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (panFechas.txtAno.getText().length() == 0) ||
        (panFechas.txtCodSemDesde.getText().length() == 0) ||
        (panFechas.txtCodSemHasta.getText().length() == 0)) {
      bDatosCompletos = false;
      msg = res.getString("msg3.Text");
    }

    else if (!checkTasas.getState() && !checkCasos.getState()) {
      bDatosCompletos = false;
      msg = res.getString("msg3.Text");
    }
    //LRG
    else if (util.fecha1MayorqueFecha2(panFechas.txtFecSemDesde.getText(),
                                       panFechas.txtFecSemHasta.getText())) {
      bDatosCompletos = false;
      msg = "Fecha 'Desde' no debe ser posterior a fecha 'Hasta'";
    }

    if (bDatosCompletos == false) {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA, msg);
      msgBox.show();
      msgBox = null;
    }

    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtCodAre.setEnabled(true);
        btnCtrlBuscarAre.setEnabled(true);
        // control area
        if (!txtDesAre.getText().equals("")) {
          btnCtrlBuscarDis.setEnabled(true);
          txtCodDis.setEnabled(true);
          //txtDesDis.setEnabled(true); /*E*/
        }
        else {
          btnCtrlBuscarDis.setEnabled(false);
          txtCodDis.setEnabled(false);
          //txtDesDis.setEnabled(false); /*E*/
        }

        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCodAre.setEnabled(false);
        txtCodDis.setEnabled(false);
        btnCtrlBuscarAre.setEnabled(false);
        btnCtrlBuscarDis.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //Busca los niveles1 (area de salud)
  void btnCtrlbuscarAre_actionPerformed(ActionEvent evt) {
    nivel1.DataNivel1 data = null;
    CMessage mensaje = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaNivel1 lista = new CListaNivel1(app,
                                            res.getString("msg2.Text") +
                                            this.app.getNivel1(),
                                            stubCliente,
                                            strSERVLETNivel1,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (nivel1.DataNivel1) lista.getComponente();

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }

    if (data != null) {
      txtCodAre.removeKeyListener(txtKeyAdapter);
      txtCodAre.setText(data.getCod());
      txtDesAre.setText(data.getDes());
      txtCodDis.setText("");
      txtDesDis.setText("");
      txtCodAre.addKeyListener(txtKeyAdapter);

    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca el distrito (Niv2)
  void btnCtrlbuscarDis_actionPerformed(ActionEvent evt) {

    zbs.DataZBS2 data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg2.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLETNivel2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (zbs.DataZBS2) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodDis.removeKeyListener(txtKeyAdapter);
      txtCodDis.setText(data.getNiv2());
      txtDesDis.setText(data.getDes());
      txtCodDis.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    txtCodAre.setText("");
    txtDesAre.setText("");
    txtCodDis.setText("");
    txtDesDis.setText("");
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    parCons = new parCons93();

    if (isDataValid()) {
      if (!txtDesAre.getText().equals("")) {
        parCons.area = txtCodAre.getText();
        parCons.areaDesc = txtDesAre.getText();
      }
      if (!txtDesDis.getText().equals("")) {
        parCons.distrito = txtCodDis.getText();
        parCons.distritoDesc = txtDesDis.getText();
      }

      parCons.ano = panFechas.txtAno.getText();
      if (panFechas.txtCodSemDesde.getText().length() == 1) {
        parCons.semD = "0" + panFechas.txtCodSemDesde.getText();
      }
      else {
        parCons.semD = panFechas.txtCodSemDesde.getText();
      }
      if (panFechas.txtCodSemHasta.getText().length() == 1) {
        parCons.semH = "0" + panFechas.txtCodSemHasta.getText();
      }
      else {
        parCons.semH = panFechas.txtCodSemHasta.getText();

      }
      parCons.casosTasas = checkCasos.getState();
      parCons.hab15 = habitantes15.getState();

      parCons.ultEnf = "";

      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();

      // Aparece un dialogo con una lista para que seleccione
      // las columnas que se van a mostrar en el informe:
      // areas (si CA), distritos (si area) o zbs (si distrito)

      CListaADZE diaElegir = null;
      if (parCons.area.equals("")) {
        diaElegir = new CListaADZE(app, AREA, COL_AREA, new CLista());
      }
      else if (parCons.distrito.equals("")) {
        diaElegir = new CListaADZE(app, parCons.area, COL_DISTRITO, new CLista());
      }
      else {
        diaElegir = new CListaADZE(app, parCons.area, parCons.distrito, COL_ZBS,
                                   new CLista());

      }
      diaElegir.show();

      if (diaElegir.resultado()) {
        parCons.lista = diaElegir.listaRes();

        //System.out.println("llamo al informe");
        informe.setEnabled(true);
        informe.parCons = parCons;
        boolean hayInforme = informe.GenerarInforme();
        if (hayInforme) {
          ( (CDialog) informe).show();
        }
      }
      //informe.mostrar();
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    //NOta LRG : Si hay mensaje por no validaci�n datos se saca en isDataValid

    /*
        else {
          msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA, res.getString("msg3.Text"));
          msgBox.show();
          msgBox = null;
        }
     */
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    // datos de envio
    nivel1.DataNivel1 nivel1;
    zbs.DataZBS2 nivel2;
    DataZBS zbs;
    DataMun mun;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtCodAre")) &&
        (txtCodAre.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new nivel1.DataNivel1(txtCodAre.getText()));
      strServlet = strSERVLETNivel1;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodDis")) &&
             (txtCodDis.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new zbs.DataZBS2(txtCodAre.getText(), txtCodDis.getText(),
                                        "", ""));
      strServlet = strSERVLETNivel2;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtCodAre")) {
            nivel1 = (nivel1.DataNivel1) param.firstElement();
            txtCodAre.removeKeyListener(txtKeyAdapter);
            txtCodAre.setText(nivel1.getCod());
            txtDesAre.setText(nivel1.getDes());
            txtCodAre.addKeyListener(txtKeyAdapter);

          }
          else if (txt.getName().equals("txtCodDis")) {
            nivel2 = (zbs.DataZBS2) param.firstElement();
            txtCodDis.removeKeyListener(txtKeyAdapter);
            txtCodDis.setText(nivel2.getNiv2());
            txtDesDis.setText(nivel2.getDes());
            txtCodDis.addKeyListener(txtKeyAdapter);
          }
        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg4.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtCodAre")) &&
        (txtDesAre.getText().length() > 0)) {
//      txtCodAre.setText("");
      txtCodDis.setText("");
      txtDesAre.setText("");
      txtDesDis.setText("");

    }
    else if ( (txt.getName().equals("txtCodDis")) &&
             (txtDesDis.getText().length() > 0)) {
//      txtCodDis.setText("");
      txtDesDis.setText("");

    }

    Inicializar();
  }
} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  panelConsulta adaptee = null;
  ActionEvent e = null;

  public actionListener(panelConsulta adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarAre")) {
      adaptee.btnCtrlbuscarAre_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarDis")) {
      adaptee.btnCtrlbuscarDis_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  panelConsulta adaptee;
  FocusEvent event;

  focusAdapter(panelConsulta adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

////////////////////// Clases para listas

// lista de valores
class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new nivel1.DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (nivel1.DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (nivel1.DataNivel1) o).getDes());
  }
}

// lista de valores
class CListaZBS2
    extends CListaValores {

  protected panelConsulta panel;

  public CListaZBS2(panelConsulta p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new zbs.DataZBS2(panel.txtCodAre.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (zbs.DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (zbs.DataZBS2) o).getDes());
  }
}

// lista de valores
class CListaZona
    extends CListaValores {

  protected panelConsulta panel;

  public CListaZona(panelConsulta p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS(s, "", "", panel.txtCodAre.getText(),
                       panel.txtCodDis.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS) o).getDes());
  }
}

class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  panelConsulta adaptee;

  txt_keyAdapter(panelConsulta adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}
