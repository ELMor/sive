//Title:        Casos o Tasas por 100000 habitantes por Area
//Version:
//Copyright:    Copyright (c) 1998
//Author:
//Company:
//Description:  Consultas 9.3.10, 9.3.11 y 9.3.12

package casostasascienarea;

import java.io.Serializable;

import capp.CLista;

public class parCons93
    implements Serializable {

  public String ano = "";
  public String semD = "";
  public String semH = "";

  public String area = "";
  public String areaDesc = "";
  public String distrito = "";
  public String distritoDesc = "";

  public boolean casosTasas = true;
  public boolean hab15 = false;

  public CLista lista = null;

  public int numPagina = 0;
  public boolean bInformeCompleto = false;
  public String ultEnf = "";

  public parCons93() {}

  public parCons93(String a, String sD, String sH,
                   String are, String dis) {
    ano = a;
    semD = sD;
    semH = sH;

    area = are;
    distrito = dis;

    lista = new CLista();
  }
}