package infprotoedo;

import java.io.Serializable;

//Estructura para la consulta a SIVE_RESP_EDO

public class DataRespCompleta
    implements Serializable {
  protected String Caso = "";
  protected String CodModelo = "";
  protected String Num = "";
  protected String CodPregunta = "";
  protected String Nivel = "";
  protected String ca = "";
  protected String n1 = "";
  protected String n2 = "";
  protected String IT_OK = "";
  protected String Des = ""; //V
  protected String ValorLista = ""; //varon
  protected String TipoPregunta = ""; // C, N, F, B, L

  public DataRespCompleta(String caso,
                          String codM, String num,
                          String pregunta, String nivel,
                          String CA, String N1, String N2,
                          String activo,
                          String des,
                          String valorlista,
                          String tipoPreg) {

    Caso = caso;
    CodModelo = codM;
    Num = num;
    CodPregunta = pregunta;
    Nivel = nivel;
    ca = CA;
    n1 = N1;
    n2 = N2;
    IT_OK = activo;
    Des = des;
    ValorLista = valorlista;
    TipoPregunta = tipoPreg;

  } //fin construct

  public String getCA() {
    return ca;
  }

  public String getN1() {
    return n1;
  }

  public String getN2() {
    return n2;
  }

  public String getIT_OK() {
    return IT_OK;
  }

  public String getNivel() {
    return Nivel;
  }

  public String getCodModelo() {
    return CodModelo;
  }

  public String getCaso() {
    return Caso;
  }

  public String getNumLinea() {
    return Num;
  }

  public String getCodPregunta() {
    return CodPregunta;
  }

  public String getDesPregunta() {
    return Des;
  }

  public String getValorLista() {
    return ValorLista;
  }

  public String getTipoPregunta() {
    return TipoPregunta;
  }

}
