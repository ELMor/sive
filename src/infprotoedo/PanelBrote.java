package infprotoedo;

import java.util.Vector;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.ScrollPane;

import COM.objectspace.jgl.Pair;
import bidial.DialogInvesInfor;
import com.borland.jbcl.control.TreeControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import com.borland.jbcl.model.BasicViewManager;
import com.borland.jbcl.model.GraphLocation;
import com.borland.jbcl.model.GraphSelectionEvent;
import com.borland.jbcl.model.SingleGraphSelection;
import com.borland.dx.text.Alignment;
import com.borland.jbcl.view.CompositeItemPainter;
import com.borland.jbcl.view.FocusableItemPainter;
import com.borland.jbcl.view.ImageItemPainter;
import com.borland.jbcl.view.SelectableItemPainter;
import com.borland.jbcl.view.TextItemPainter;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CPanel;

public class PanelBrote
    extends CPanel {

  //ancho del arbol
  public int iAncho = 0;

  //Para el panel PRINCIPAL
  TreeControl arbolContenido = new TreeControl();

  protected Vector vArbol = new Vector(); //descripciones Enfermedad
  protected Vector vCNE = new Vector();
  protected Vector vCA = new Vector();
  protected Vector vN1 = new Vector();
  protected Vector vN2 = new Vector();
  protected Vector vReferencia = new Vector();

  ScrollPane panelScroll = new ScrollPane();
  ScrollPane panelScroll2 = new ScrollPane();

  public Panel_Informe_Completo pnl;
  Panel pnl2 = new Panel();

  protected DataProtocolo datosproto = null;
  protected CCargadorImagen imgs = null;

  final String imgNAME[] = {
      "images/view.gif",
      "images/view2.gif"};

  XYLayout xYLayout1 = new XYLayout();
  XYLayout xYLayout2 = new XYLayout();

  DialogInvesInfor dialogo = null;

  public PanelBrote(CApp a, DataProtocolo data,
                    DialogInvesInfor dlg) {
    try {

      app = a;
      datosproto = data;
      dialogo = dlg;

      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();

    }
  }

  void jbInit() throws Exception {
    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    // �rbol no editable
    arbolContenido.setEditInPlace(false);
    // estilo del nodo raiz
    arbolContenido.setStyle(com.borland.jbcl.view.TreeView.STYLE_PLUSES);
    // inicialmente el �rbol est� cerrado
    arbolContenido.setExpandByDefault(true);

    arbolContenido.setBackground(new Color(255, 255, 255));

    // gestor de eventos
    arbolContenido.addSelectionListener(new PanelBrote_selectionAdapter(this));

    // formulario EDO
    pnl = new Panel_Informe_Completo(app, datosproto, this, dialogo);

    Pintar_Arbol(pnl);

    //mlm
    //PanelBrote
    this.setSize(new Dimension(740, 170));
    xYLayout1.setHeight(345);
    xYLayout1.setWidth(740); //el panel grande
    this.setLayout(xYLayout1);

    pnl2.setLayout(xYLayout2);
    xYLayout2.setHeight(345);
    xYLayout2.setWidth(150); //mas largo
    pnl2.setLayout(xYLayout2);
    pnl2.add(arbolContenido, new XYConstraints(0, 0, -1, 345)); //ponia 345

    panelScroll.add(pnl); //panel_Informe
    panelScroll2.add(pnl2); //arbol

    this.add(panelScroll2, new XYConstraints(0, 0, 150, 250)); //arbol//ponia 315
    this.add(panelScroll, new XYConstraints(150, 0, 580, 250)); //ponia 315

    this.xYLayout2.setWidth(iAncho + 40);
    this.xYLayout2.setHeight(pnl.iTam);
    this.pnl2.setLayout(xYLayout2);
    this.pnl2.doLayout();
    this.doLayout();
    this.doLayout();

  }

  public void Pintar_Arbol(Panel_Informe_Completo p) {
    vArbol = new Vector(); //descripciones Enfermedad
    vCNE = new Vector();
    vCA = new Vector();
    vN1 = new Vector();
    vN2 = new Vector();
    vReferencia = new Vector();

    //Cargamos los vectores con los datos
    Vector vEnfermedad = new Vector();
    Vector vDescripcion = new Vector();
    Vector vtmpCNE = new Vector();
    Vector vtmpCA = new Vector();
    Vector vtmpN1 = new Vector();
    Vector vtmpN2 = new Vector();

    vEnfermedad = p.getEnfermedad();
    vDescripcion = p.getDescripciones();
    vtmpCNE = p.getCNE();
    vtmpCA = p.getCA();
    vtmpN1 = p.getN1();
    vtmpN2 = p.getN2();

    Nodo pto;

    String maslargo = "";

    //vArbol
    pto = (Nodo) vEnfermedad.elementAt(0);
    vArbol.addElement(new Nodo(pto.getDescripcion(), pto.getY()));
    maslargo = pto.getDescripcion() + "__";

    //resto de vArbol
    for (int idesc = 0; idesc < vDescripcion.size(); idesc++) {
      pto = (Nodo) vDescripcion.elementAt(idesc);
      vArbol.addElement(new Nodo(pto.getDescripcion(), pto.getY()));
      if (pto.getDescripcion().length() > maslargo.length()) {
        maslargo = pto.getDescripcion() + "__" + "__";
      }
    }
    vEnfermedad = null;
    vDescripcion = null;

    //vCNE
    for (int icne = 0; icne < vtmpCNE.size(); icne++) {
      pto = (Nodo) vtmpCNE.elementAt(icne);
      vCNE.addElement(new Nodo(pto.getDescripcion(), pto.getY()));
      if (pto.getDescripcion().length() > maslargo.length()) {
        maslargo = pto.getDescripcion() + "__" + "__" + "__";
      }
    }
    vtmpCNE = null;

    //vCA
    for (int ica = 0; ica < vtmpCA.size(); ica++) {
      pto = (Nodo) vtmpCA.elementAt(ica);
      vCA.addElement(new Nodo(pto.getDescripcion(), pto.getY()));
      if (pto.getDescripcion().length() > maslargo.length()) {
        maslargo = pto.getDescripcion() + "__" + "__" + "__";
      }
    }
    vtmpCA = null;

    //vN1
    for (int in1 = 0; in1 < vtmpN1.size(); in1++) {
      pto = (Nodo) vtmpN1.elementAt(in1);
      vN1.addElement(new Nodo(pto.getDescripcion(), pto.getY()));
      if (pto.getDescripcion().length() > maslargo.length()) {
        maslargo = pto.getDescripcion() + "__" + "__" + "__";
      }
    }
    vtmpN1 = null;

    //vN2
    for (int in2 = 0; in2 < vtmpN2.size(); in2++) {
      pto = (Nodo) vtmpN2.elementAt(in2);
      vN2.addElement(new Nodo(pto.getDescripcion(), pto.getY()));
      if (pto.getDescripcion().length() > maslargo.length()) {
        maslargo = pto.getDescripcion() + "__" + "__" + "__";
      }
    }
    vtmpN2 = null;

    // inicializa el �rbol
    Inicializar();

    //ver cuantos pixeles ocupa el layout
    Graphics g = app.getGraphics();
    g.setFont(new Font("Dialog", 1, 12));
    iAncho = g.getFontMetrics().stringWidth(maslargo);
  }

  // inicializa el �rbol
  public void Inicializar() {
    GraphLocation root;
    GraphLocation cne;
    GraphLocation ca;
    GraphLocation n1;
    GraphLocation n2;
    GraphLocation gl;

    Nodo nodo;
    int j;

    // establece el visor imagen+texto
    arbolContenido.setViewManager(new BasicViewManager
                                  (new FocusableItemPainter
                                   (new SelectableItemPainter
                                    (new CompositeItemPainter
                                     (new ImageItemPainter(this, Alignment.LEFT),
                                      new TextItemPainter())))));

    j = 0;
    // nodo raiz
    nodo = (Nodo) vArbol.elementAt(j);
    root = arbolContenido.setRoot(new Pair(null, nodo.getDescripcion()));
    vReferencia.addElement(new Nodo2(root, nodo.getY()));

    //paso a los siguientes

    if (vCNE.size() > 0) {
      j++;
      nodo = (Nodo) vArbol.elementAt(j);
      cne = arbolContenido.addChild(root,
                                    new Pair(imgs.getImage(0), nodo.getDescripcion()));
      vReferencia.addElement(new Nodo2(cne, nodo.getY()));
      // partes del cne
      for (int i = 0; i < vCNE.size(); i++) {
        nodo = (Nodo) vCNE.elementAt(i);
        gl = arbolContenido.addChild(cne,
                                     new Pair(imgs.getImage(1), nodo.getDescripcion()));
        vReferencia.addElement(new Nodo2(gl, nodo.getY()));
      }
    }

    // nodo CA
    if (vCA.size() > 0) {
      j++;
      nodo = (Nodo) vArbol.elementAt(j);
      ca = arbolContenido.addChild(root,
                                   new Pair(imgs.getImage(0), nodo.getDescripcion()));
      vReferencia.addElement(new Nodo2(ca, nodo.getY()));
      // partes del ca
      for (int i = 0; i < vCA.size(); i++) {
        nodo = (Nodo) vCA.elementAt(i);
        gl = arbolContenido.addChild(ca,
                                     new Pair(imgs.getImage(1), nodo.getDescripcion()));
        vReferencia.addElement(new Nodo2(gl, nodo.getY()));
      }
    }

    // nodo n1
    if (vN1.size() > 0) {
      j++;
      nodo = (Nodo) vArbol.elementAt(j);
      n1 = arbolContenido.addChild(root,
                                   new Pair(imgs.getImage(0), nodo.getDescripcion()));
      vReferencia.addElement(new Nodo2(n1, nodo.getY()));
      // partes del n1
      for (int i = 0; i < vN1.size(); i++) {
        nodo = (Nodo) vN1.elementAt(i);
        gl = arbolContenido.addChild(n1,
                                     new Pair(imgs.getImage(1), nodo.getDescripcion()));
        vReferencia.addElement(new Nodo2(gl, nodo.getY()));
      }
    }

    if (vN2.size() > 0) {
      j++;
      nodo = (Nodo) vArbol.elementAt(j);
      n2 = arbolContenido.addChild(root,
                                   new Pair(imgs.getImage(0), nodo.getDescripcion()));
      vReferencia.addElement(new Nodo2(n2, nodo.getY()));
      // partes del n2
      for (int i = 0; i < vN2.size(); i++) {
        nodo = (Nodo) vN2.elementAt(i);
        gl = arbolContenido.addChild(n2,
                                     new Pair(imgs.getImage(1), nodo.getDescripcion()));
        vReferencia.addElement(new Nodo2(gl, nodo.getY()));
      }
    } //fin if n2

    doLayout();
  }

  // mueve el panel de scroll a la selecci�n indicada
  void arbolContenido_selectionChanged(GraphSelectionEvent e) {
    GraphLocation[] selections;
    Nodo2 nodo = null;

    //QQ: Cambios a partir de Confprot:
    // copia el nodo seleccionado
    if (arbolContenido.getSelection().getCount() > 0) {
      //QQ: Eliminado: selections = arbolContenido.getSelection().getAll();
      selections = e.getSelection().getAll();
      for (int i = 0; i < vReferencia.size(); i++) {
        nodo = (Nodo2) vReferencia.elementAt(i);
        if (selections[0] == nodo.getGraphLocation()) {
          panelScroll.setScrollPosition(0, nodo.getY());
          break;
        }
      } //for
    } //if
    else { //Nuevo:
      arbolContenido.setSelection(new SingleGraphSelection(arbolContenido.
          getRoot()));
    }
  } //void
}

class PanelBrote_selectionAdapter
    extends com.borland.jbcl.model.GraphSelectionAdapter {
  PanelBrote adaptee;

  public PanelBrote_selectionAdapter(PanelBrote adaptee) {
    this.adaptee = adaptee;
  }

  public void selectionChanged(GraphSelectionEvent e) {
    adaptee.arbolContenido_selectionChanged(e);
  }
}
