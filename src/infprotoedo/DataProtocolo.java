package infprotoedo;

import java.io.Serializable;

public class DataProtocolo
    implements Serializable {

  public String sCodigo = "", sDescripcion = "";
  public String sNivel1 = "", sNivel2 = "", sCa = "";
  public String NumCaso = "";

  public DataProtocolo() {
  }

  public DataProtocolo(String codigo, String descripcion,
                       String nivel1, String nivel2, String ca,
                       String numcaso) {
    sCodigo = codigo;
    sDescripcion = descripcion;
    sNivel1 = nivel1;
    sNivel2 = nivel2;
    sCa = ca;
    NumCaso = numcaso;
  }

  public String getCod() {
    return sCodigo;
  }

  public String getDes() {
    return sDescripcion;
  }

  public String getNivel1() {
    return sNivel1;
  }

  public String getNivel2() {
    return sNivel2;
  }

  public String getCa() {
    return sCa;
  }

  public String getCaso() {
    return NumCaso;
  }
}
