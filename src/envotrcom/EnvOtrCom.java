package envotrcom;

import java.util.ResourceBundle;

import capp.CApp;

public class EnvOtrCom
    extends CApp {

  ResourceBundle res;

  public EnvOtrCom() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("envotrcom.Res" + this.getIdioma());
    setTitulo(res.getString("msg13.Text"));
    CApp a = (CApp)this;
    // abrimos el panel con el modo oportuno
    VerPanel("", new PanEnvOtrCom(a));
  }

}
