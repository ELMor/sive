package envotrcom;

//import eqNot.*;
import java.net.URL;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import mantdatos.DataMantDatos;
import sapp.StubSrvBD;

public class DialCarta
    extends EPanel {

  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 1;

  //Servlet y modos consulta para recuperar datos de Servicio de Epidemiologia
  final String strSERVLETParam = "servlet/SrvMantDatos";
  final String strLOGO_CCAA = "images/ccaa.gif";
  final int servletOBTENER_X_CODIGO = 3;

  //Tipo de carta que debe resultar como salida de este informe
  //Posibles valores
  public final static int CARTA_ENV_OTR_COM = 1;
  public final static int CARTA_ENV_URG = 2;
  int tipoDeCarta = 0;
  String sAnoEnvio = "";
  String sNumEnvio = "";

  /*
    // estructuras de datos
    protected Vector vAgrupaciones;
    protected Vector vTotales;
    protected Vector vCasos;
    protected CLista lista;
   */
  // modo de operaci�n
  protected int modoOperacion;

  // conexion con el servlet
  protected StubSrvBD stub;

  // Para pedir mas paginas
//  protected Vector ultSemana = new Vector();

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  CApp app = null;
//  Integer regMostrados = new Integer(0);

  public DialCarta(CApp a, int tipoCarta, String anoEnvio, String numEnvio) {
    super(a);
    app = a;
    res = ResourceBundle.getBundle("envotrcom.Res" + app.getIdioma());
    tipoDeCarta = tipoCarta;
    sAnoEnvio = anoEnvio;
    sNumEnvio = numEnvio;
    try {

      stub = new StubSrvBD();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + "erw/ERWCartaCAM.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }

    obtenerDatosPrimeraPagina();
  }

  //mostrar
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    ( (CDialog)this).show();
    LlamadaInforme(false);
  }

  // todo el informe
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // siguiente trama
  public void MasDatos() {

    /*
        CMessage msgBox;
        CLista param = new CLista();
        Vector v;
        ////#// System_out.println(lista.getState());
        if (lista.getState() == CLista.listaINCOMPLETA) {
          modoOperacion = modoESPERA;
          Inicializar();
          this.setGenerandoInforme();
          try {
            PrepararInforme();
            param.setFilter(lista.getFilter());
            // obtiene los datos del servidor
            //datosPar.iPagina++;
            param.addElement(datosPar);
            param.setFilter("");
            param.trimToSize();
            stub.setUrl(new URL(app.getURL() + strSERVLET));
            lista = (CLista) stub.doPost(erwCASOS_EDO, param);
            v = (Vector) lista.elementAt(0);
            vTotales = (Vector) lista.elementAt(1);
            for (int j=0; j<v.size(); j++)
              vCasos.addElement(v.elementAt(j));
            // control de registros
            Integer tot = (Integer) lista.elementAt(1);
            this.setTotalRegistros(tot.toString());
         this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());
            // repintado
            erwClient.refreshReport(true);
          } catch (Exception e) {
            e.printStackTrace();
            msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
            msgBox.show();
            msgBox = null;
            this.setLimpiarStatus();
          }
          modoOperacion = modoNORMAL;
          Inicializar();
        }
     */
  }

// Rellena los datos de las solapas
  void obtenerDatosPrimeraPagina() {
    /*
        CMessage msgBox = null;
        try {
          parametros = new CLista();
          // par�metros que se le pasan a DataEnfCaso: CD_ENFERMO, NM_CASO
//      DataEnfCaso dataEnf = new DataEnfCaso("", (String)hashNotifEDO.get("NM_EDO"));
          DataEnfCaso dataEnf = new DataEnfCaso("", numCasoEdo);
          parametros.addElement(dataEnf);
          if (app.getIT_TRAMERO().equals("S")  )
            result = Comunicador.Communicate(this.getCApp(),
                                stub,
                                servletSELECT_EDO_INVIDIDUALIZADA_SUCA,
                                strSERVLET_INDIV_SELECT,
                                parametros);
          else
            result = Comunicador.Communicate(this.getCApp(),
                                stub,
                                servletSELECT_EDO_INVIDIDUALIZADA,
                                strSERVLET_INDIV_SELECT,
                                parametros);
          // comprueba que hay datos
          if (result.size() == 0) {
         msgBox = new CMessage(this.app, CMessage.msgAVISO, "No se encontraron datos ");
            msgBox.show();
            msgBox = null;
          }
          else {
            //tengo los datos de la Indiv
            dataTab = (DataTab)result.elementAt(0);
          }
        } catch (Exception e) {
         msgBox = new CMessage(this.app,  CMessage.msgERROR, e.getMessage());
            e.printStackTrace();
            msgBox.show();
            msgBox = null;
        }
     */
  } //fin de obtenerDatosPrimeraPagina

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  protected boolean LlamadaInforme(boolean conTodos) {

    CMessage msgBox;
    CLista param = new CLista();
    boolean elBoolean = true;
    int indice = 0; //Recorrido vector preguntas
    //Para hacer listado
    Vector vLineas = new Vector(); //vector de lineas que ir� al informe
    boolean bHayLineas = true;

    // plantilla
    TemplateManager tm = null;

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {

      //Obtiene datos de la primera p�gina del caso EDO
      PrepararInforme();
      /*
            //Si ha habido error al ir a por lineas no se saca informe
            if (llegaListaPresYResp == false) {
              //Nota: ya se ha sacado mensaje
              return false;
            }
       */

      //_______________________________________________________________________

      /*
              if (bHayLineas == true) {
                // establece las matrices de datos
                Vector retval = new Vector();
                retval.addElement("BLOQUE = BLOQUE");
                retval.addElement("SECCION = SECCION");
                retval.addElement("PREGUNTA = PREGUNTA");
                retval.addElement("RESPUESTA = RESPUESTA");
              dataHandler.RegisterTable(vLineas, "sive_911", retval, null);
              }
              //No hay lineas
              else {
                tm = erw.GetTemplateManager();
                //Hace invisible lineas de deatalle
                tm.SetVisible(tm.GetDetailZone("SEC_00" ),false);
                Object[] [] grupos = tm.GetGroups("SEC_00");
                //Hace invisible cab. de primera agrupaci�n (bloques)
                tm.SetVisible(grupos[0][0],false);
                //Hace invisible cab. de segunda agrupaci�n (secciones)
                tm.SetVisible(grupos[1][0],false);
               //Se ponen dos lineas cualquiera para que funcione
           vLineas.addElement(new DataPregYResp("seccion","bloque"," "," "));
           vLineas.addElement(new DataPregYResp("seccion","bloque2"," "," "));
                Vector retval = new Vector();
                retval.addElement("BLOQUE = BLOQUE");
                retval.addElement("SECCION = SECCION");
                retval.addElement("PREGUNTA = PREGUNTA");
                retval.addElement("RESPUESTA = RESPUESTA");
              dataHandler.RegisterTable(vLineas, "sive_911", retval, null);
              }
       */

      erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

      // repintado
      //AIC
      this.pack();
      erwClient.prv_setActivePage(0);
      //erwClient.refreshReport(true);

      elBoolean = true;

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();
      elBoolean = false;
    }

    modoOperacion = modoNORMAL;
    Inicializar();
    return elBoolean;

  }

  // este informe no tiene grafica
  public void MostrarGrafica() {
  }

  protected void PrepararInforme() throws Exception {
    CMessage msgBox = null;
    DataMantDatos dataParamAuxi = null;
    URL u;
    CLista data = new CLista();
    CLista dataAuxi = new CLista();
    // plantilla
    TemplateManager tm = erw.GetTemplateManager();

//___________________________________________________

    try {

      stub = new StubSrvBD(new URL(app.getURL() + strSERVLETParam));
      CLista listaResultadoParam = new CLista(); // Va a ser una CList (hereda de Vector) en la que cada elem. tiene dos Strings
      listaResultadoParam = new CLista();
      listaResultadoParam.setState(CLista.listaNOVALIDA);

      data.setLogin(this.app.getLogin());
      // Primero se lee el registro la tabla
      data.addElement(new DataMantDatos("", "", "", "", "", "", "", "", "", "",
                                        ""));
      dataAuxi = (CLista) stub.doPost(servletOBTENER_X_CODIGO, data);
      // Ahora escribimos los valores de los componentes
      dataParamAuxi = (DataMantDatos) dataAuxi.firstElement();
      // Comprobamos que no est� vac�a
      if (dataParamAuxi.getDireccion().compareTo("") != 0) {

        // carga los logos
        tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
        tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

        //Pone etiquetas en plantilla
        tm.SetLabel("LABANONUMENV", sAnoEnvio + "/" + sNumEnvio);
        tm.SetLabel("LABCON", dataParamAuxi.getCodConsejero());
        tm.SetLabel("LABSER", dataParamAuxi.getServicio());
        tm.SetLabel("LABCAL", dataParamAuxi.getDireccion());
        tm.SetLabel("LABCODPOS", dataParamAuxi.getCP());
        tm.SetLabel("LABPOB", dataParamAuxi.getPoblacion());

        //Fecha de hoy
        java.util.Date dFecHoy = new java.util.Date();
        Calendar cal = new GregorianCalendar();
        int mes = 0;
        cal.setTime(dFecHoy);
        tm.SetLabel("LABDIA", Integer.toString(cal.get(Calendar.DAY_OF_MONTH)));
        mes = cal.get(Calendar.MONTH);
        switch (mes) {
          case (Calendar.JANUARY):
            tm.SetLabel("LABMES", res.getString("msg1.Text"));
            break;
          case (Calendar.FEBRUARY):
            tm.SetLabel("LABMES", res.getString("msg2.Text"));
            break;
          case (Calendar.MARCH):
            tm.SetLabel("LABMES", res.getString("msg3.Text"));
            break;
          case (Calendar.APRIL):
            tm.SetLabel("LABMES", res.getString("msg4.Text"));
            break;
          case (Calendar.MAY):
            tm.SetLabel("LABMES", res.getString("msg5.Text"));
            break;
          case (Calendar.JUNE):
            tm.SetLabel("LABMES", res.getString("msg6.Text"));
            break;
          case (Calendar.JULY):
            tm.SetLabel("LABMES", res.getString("msg7.Text"));
            break;
          case (Calendar.AUGUST):
            tm.SetLabel("LABMES", res.getString("msg8.Text"));
            break;
          case (Calendar.SEPTEMBER):
            tm.SetLabel("LABMES", res.getString("msg9.Text"));
            break;
          case (Calendar.OCTOBER):
            tm.SetLabel("LABMES", res.getString("msg10.Text"));
            break;
          case (Calendar.NOVEMBER):
            tm.SetLabel("LABMES", res.getString("msg11.Text"));
            break;
          case (Calendar.DECEMBER):
            tm.SetLabel("LABMES", res.getString("msg12.Text"));
            break;
        }

        tm.SetLabel("LABANO", Integer.toString(cal.get(Calendar.YEAR)));

        //Oculta etiquetas seg�n tipo de carta
        if (tipoDeCarta == DialCarta.CARTA_ENV_OTR_COM) {
          tm.SetLabel("LABTEXTOURG1", "");
          tm.SetLabel("LABTEXTOURG2", "");
        }
        else {
          tm.SetLabel("LABTEXTOOTRCOM1", "");
          tm.SetLabel("LABTEXTOOTRCOM2", "");
        }

        //Etiquetas firma
        tm.SetLabel("LABNOM", dataParamAuxi.getJefeServ());
        tm.SetLabel("LABSER2", dataParamAuxi.getServicio());
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

  }
} //CLASE
