
package envotrcom;

//Clase para representar todos los env�os urgentes de un a�o
//Se usa tanto para petici�n como para respuesta

import java.io.Serializable;
import java.util.Vector;

public class DataAnoEnvOtrCom
    implements Serializable {

  protected String sCodAnoOtrCom = ""; //C�digo del a�o
  protected boolean bHayAno = false; //Dice si existe el a�o epidemiol�gico o no
  protected Vector vEnvios = new Vector(); //Vector de env�os (fecha y n�mero)

  //Datos para generac��n fichero
//Usadas para generar ficheros
  protected String sSep = "|"; //Caracter separador entre datos de una l�nea
  protected String sComAut;
  protected String sTSive;

  //Constructor para petici�n
  public DataAnoEnvOtrCom(String codAnoOtrCom) {
    sCodAnoOtrCom = codAnoOtrCom;
  }

  public DataAnoEnvOtrCom(String codAnoOtrCom, boolean hayAno, Vector envios) {
    sCodAnoOtrCom = codAnoOtrCom;
    bHayAno = hayAno;
    vEnvios = envios;
  }

  public String getCodAnoOtrCom() {
    return sCodAnoOtrCom;
  }

  public boolean getHayAno() {
    return bHayAno;
  }

  public Vector getEnvios() {
    return vEnvios;
  }

//Para indicar com aut y TSive cuando haya que generar fichero

  public String getSep() {
    return sSep;
  }

  public void setTSive(String tSive) {
    sTSive = tSive;
  }

  public void setComAut(String comAut) {
    sComAut = comAut;
  }

  public String getTSive() {
    return sTSive;
  }

  public String getComAut() {
    return sComAut;
  }

}
