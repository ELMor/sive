
package envotrcom;

import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Choice;
import java.awt.Cursor;
import java.awt.Event;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import capp.CTabla;
import envressem.DataNotInd;
import envressem.NumyFecEnv;
import fechas.CFecha;
import infedoind.DialInfEdoInd;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import jclass.bwt.JCItemEvent;
import jclass.bwt.JCItemListener;
import sapp.StubSrvBD;

public class PanEnvOtrCom
    extends CPanel {

  //a�o epidemiol�gico
  public String sAnoBk = "";
  ResourceBundle res;
  //�ltimo a�o epid.
  public String sUltAnoBk = "";

  //modos de consulta al servlet
  final int servletOBTENER_ULTIMO_ANO_Y_ENVIOS = 1;
  final int servletOBTENER_UN_ANO_Y_ENVIOS = 2;
  final int servletSELECCION_EDOIND_OTRCOM = 3;

  final int servletGRABAR_ENVIO = 4;

  //rutas imagenes
  final String imgNAME[] = {
      "images/grafico.gif",
      "images/primero.gif",
      "images/anterior.gif",
      "images/siguiente.gif",
      "images/ultimo.gif",
      "images/grafico.gif",
      "images/salvar.gif"};

  // indica la fila seleccionada en la tabla
  public int m_itemSelecc = -1;

  //modos de pantalla
  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  //Variables
  int modoOperacion;
  int modoServlet;

  // lista
  public CLista lista = new CLista();

  //Vector que guarda parejas fecha,n�mero de los envios de un a�o
  Vector vEnvios;

  String strNuevo = "";

  protected CLista listaFich = new CLista();
  protected StubSrvBD stubCliente = new StubSrvBD();
  protected StubSrvBD stubCliente2 = new StubSrvBD();

  XYLayout xYLayout1 = new XYLayout();
  XYLayout xYLayoutPrueba = new XYLayout();
  ButtonControl btnGrabarEnv = new ButtonControl();
  ButtonControl btnCarta = new ButtonControl();

  ButtonControl btnListado = new ButtonControl();
  CTabla tabla = new CTabla();
  String sAnoActual = "";
  PanEnvOtrComActionListener btnActionListener = new PanEnvOtrComActionListener(this);
  PanEnvOtrCombtnNormalActionListener btnNormalActionListener = new
      PanEnvOtrCombtnNormalActionListener(this);
  PanEnvOtrCom_tabla_actionAdapter tablaActionListener = new
      PanEnvOtrCom_tabla_actionAdapter(this);
  PanEnvOtrComItemListener multlstItemListener = new PanEnvOtrComItemListener(this);

  protected CCargadorImagen imgs = null;

  StatusBar statusBar = new StatusBar();
  Panel pnl = new Panel();
  Label lblAno = new Label();
  Choice choiceEnv = new Choice();
  Label lblFecha = new Label();

  // ARG: Se en vez de TextField usamos CFecha
  //TextField txtFecEnv = new TextField();
  CFecha txtFecEnv = new CFecha("S");

  Label lblEnvio = new Label();
  TextField txtAno = new TextField();

  ButtonControl btnPrimero = new ButtonControl();
  ButtonControl btnAnterior = new ButtonControl();
  ButtonControl btnSiguiente = new ButtonControl();
  ButtonControl btnUltimo = new ButtonControl();

  public PanEnvOtrCom(CApp a) {
    try {
      setApp(a);
      res = ResourceBundle.getBundle("envotrcom.Res" + app.getIdioma());
      strNuevo = res.getString("msg14.Text");
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    //   this.setSize(new Dimension(569, 400)); //????????????
    // System_out.println("Empieza el jbInit********");
    xYLayoutPrueba.setHeight(362); //???????????    362
    xYLayoutPrueba.setWidth(660); //?????????????  678
    setLayout(xYLayoutPrueba); //????????????

    //    xYLayout1.setHeight(260);  //??????????????
//    xYLayout1.setWidth(450);   //????????????

    lblEnvio.setText(res.getString("lblEnvio.Text"));
    lblFecha.setText(res.getString("lblFecha.Text"));
    lblAno.setText(res.getString("lblAno.Text"));
    btnGrabarEnv.setLabel(res.getString("btnGrabarEnv.Label"));
    btnCarta.setLabel(res.getString("btnCarta.Label"));
    btnListado.setLabel(res.getString("btnListado.Label"));

    btnGrabarEnv.setActionCommand("GrabarEnv");
    btnListado.setActionCommand("GenListado");
    btnCarta.setActionCommand("GenCarta");
    btnPrimero.setActionCommand("primero");
    btnAnterior.setActionCommand("anterior");
    btnSiguiente.setActionCommand("siguiente");
    btnUltimo.setActionCommand("ultimo");

//    pnl.setLayout(xYLayout1);
//   this.add(pnl, new XYConstraints(13, 5, 627, 337));
//    this.add(panFile, new XYConstraints(22, 229, -1, -1));

    this.add(tabla, new XYConstraints(12, 87, 580, 182));
    this.add(btnPrimero, new XYConstraints(459, 276, -1, -1));
    this.add(btnAnterior, new XYConstraints(494, 276, -1, -1));
    this.add(btnSiguiente, new XYConstraints(529, 276, -1, -1));
    this.add(btnUltimo, new XYConstraints(564, 276, -1, -1));
    this.add(lblAno, new XYConstraints(19, 32, 31, -1));
    this.add(choiceEnv, new XYConstraints(248, 32, 99, -1));
    this.add(lblFecha, new XYConstraints(356, 32, 92, -1));
    this.add(txtFecEnv, new XYConstraints(455, 32, 96, -1));
    this.add(lblEnvio, new XYConstraints(197, 32, 40, -1));
    this.add(txtAno, new XYConstraints(71, 32, 80, -1));
    this.add(btnGrabarEnv, new XYConstraints(248, 320, -1, 26));
    this.add(btnCarta, new XYConstraints(358, 320, -1, 26));
    this.add(btnListado, new XYConstraints(468, 320, -1, 26));

    // configuraci�n de la tabla
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(res.
        getString("tabla.ColumnButtonsStrings"), '\n'));
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(
        "100\n100\n150\n112\n100", '\n'));
    tabla.setNumColumns(5);

    //CAraga imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnListado.setImage(imgs.getImage(0));
    btnPrimero.setImage(imgs.getImage(1));
    btnAnterior.setImage(imgs.getImage(2));
    btnSiguiente.setImage(imgs.getImage(3));
    btnUltimo.setImage(imgs.getImage(4));
    btnCarta.setImage(imgs.getImage(5));
    btnGrabarEnv.setImage(imgs.getImage(6));

    //Escuchadores
    choiceEnv.addItemListener(new PanEnvOtrCom_choiceEnv_itemAdapter(this));
    txtAno.addFocusListener(new PanEnvOtrCom_txtAno_focusAdapter(this));

    btnGrabarEnv.addActionListener(btnActionListener);
    btnListado.addActionListener(btnActionListener);
    btnCarta.addActionListener(btnActionListener);

    tabla.addActionListener(tablaActionListener);
    tabla.addItemListener(multlstItemListener);
    btnPrimero.addActionListener(btnNormalActionListener);
    btnAnterior.addActionListener(btnNormalActionListener);
    btnSiguiente.addActionListener(btnNormalActionListener);
    btnUltimo.addActionListener(btnNormalActionListener);

//    add(pnl, BorderLayout.CENTER);//?????????
//    add(statusBar, BorderLayout.SOUTH); //???????????

    //Trae el �ltimo a�o y sus env�os de la b. datos
    traerUltimoAno();

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // limpia la pantalla
  void vaciarPantalla() {
    if (tabla.countItems() > 0) {
      tabla.deleteItems(0, tabla.countItems() - 1);
    }
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {

      case modoNORMAL:

        //Inicializaci�n botones depende de elecci�n en choice
        //Si no hay elecci�n, botones desactivados
        if (choiceEnv.getSelectedItem().equals("")) {
          btnGrabarEnv.setEnabled(false);
          btnCarta.setEnabled(false);
          btnListado.setEnabled(false);
          btnAnterior.setEnabled(false);
          btnPrimero.setEnabled(false);
          btnSiguiente.setEnabled(false);
          btnUltimo.setEnabled(false);

        }
        //Si es envio nuevo, bot�n de carta desactivado
        // (Antes hay que grabar el env�o)
        else if (choiceEnv.getSelectedItem().equals(strNuevo)) {
          btnGrabarEnv.setEnabled(true);
          btnCarta.setEnabled(false);
          //Resto de botones activos solo si tabla tiene elementos
          if (tabla.countItems() > 0) {
            btnListado.setEnabled(true);
            btnAnterior.setEnabled(true);
            btnPrimero.setEnabled(true);
            btnSiguiente.setEnabled(true);
            btnUltimo.setEnabled(true);
          }
          else {
            btnListado.setEnabled(false);
            btnAnterior.setEnabled(false);
            btnPrimero.setEnabled(false);
            btnSiguiente.setEnabled(false);
            btnUltimo.setEnabled(false);
          }
        }
        //Si es env�o antiguo, bot�n de grabar env�o desactivado
        else {
          btnGrabarEnv.setEnabled(false);
          btnCarta.setEnabled(true);
          //Resto de botones activos solo si tabla tiene elementos
          if (tabla.countItems() > 0) {
            btnListado.setEnabled(true);
            btnAnterior.setEnabled(true);
            btnPrimero.setEnabled(true);
            btnSiguiente.setEnabled(true);
            btnUltimo.setEnabled(true);
          }
          else {
            btnListado.setEnabled(false);
            btnAnterior.setEnabled(false);
            btnPrimero.setEnabled(false);
            btnSiguiente.setEnabled(false);
            btnUltimo.setEnabled(false);
          }
        }
        txtAno.setEnabled(true);
        tabla.setEnabled(true);
        txtFecEnv.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnGrabarEnv.setEnabled(false);
        btnCarta.setEnabled(false);
        btnListado.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnPrimero.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        txtAno.setEnabled(false);
        tabla.setEnabled(false);
        txtFecEnv.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
    doLayout();
  }

  void btnGrabarEnv_actionPerformed(ActionEvent evt) {
    DataAnoEnvOtrCom datAnoEnvOtrCom;
    CLista data;
    CLista data2;
    int j = 0;
    String sLinea;
    File miFichero = null;
    FileWriter fStream = null;
    CMessage msgBox;
    CLista resEdoInd = null;
    File miFicheroCab = null;
    FileWriter fStreamCab = null;

    String sCodComAutDefinitivo = "";
    boolean bHayDatos = false;
    NumyFecEnv envio;
    Vector vEnv = new Vector(); //Vector de env�os uso para petici�n de num env�o al Servlet

    CLista listaErrores = null;
    CLista listaCatalogos = new CLista();
//  DialErrores dlg = null;

    try {

      // ARG: Se comprueba que la fecha sea valida
      txtFecEnv.ValidarFecha();
      if ( (isDataValid() == true) && (txtFecEnv.getValid().equals("S"))) {

        this.modoOperacion = modoESPERA;
        Inicializar();

        //____________ Generaci�n fichero de casos urgentes______________________________

        data2 = null;
        data2 = new CLista();
        //Se preparan los datos de petici�n
        if (choiceEnv.getSelectedItem().equals(strNuevo)) {
          envio = new NumyFecEnv( -1);
          //# // System_out.println("El nuevo******************************");
        }
        else {
          envio = new NumyFecEnv(Integer.parseInt(choiceEnv.getSelectedItem()));
        }
        vEnv.addElement(envio);

        data2.addElement(new DataAnoEnvOtrCom(txtAno.getText().trim(), true,
                                              vEnv));
        ( (DataAnoEnvOtrCom) (data2.firstElement())).setComAut(app.getCA());
        ( (DataAnoEnvOtrCom) (data2.firstElement())).setTSive(app.getTSive());

        //# // System_out.println("Antes de apuntar******************************");
        // apunta al servlet principal
        stubCliente2.setUrl(new URL(app.getURL() + "servlet/SrvEnvOtrCom"));

        data2.setLogin(app.getLogin());
        data2.setLortad(app.getParameter("LORTAD"));

        // obtiene la lista de resultados
        resEdoInd = (CLista) stubCliente2.doPost(servletGRABAR_ENVIO, data2);
        /*
                  SrvEnvOtrCom srv = new SrvEnvOtrCom();
                  srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                               "jdbc:oracle:thin:@192.168.0.13:1521:SIVE",
                               "dba_edo",
                               "manager");
                  resEdoInd = srv.doDebug(servletGRABAR_ENVIO, data2);
                  srv = null;
         */

        //Se ajusta la pantalla de cliente a los cambios ya produciidos en b. datos

        //Fecha de hoy para el envio
        java.util.Date dFecHoy = new java.util.Date();
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
        txtFecEnv.setText(formater.format(dFecHoy));
        //A�ade nuevo env�o a lista de env�os. Se a�ade el primero (va en orden descend)
        vEnvios.insertElementAt(new NumyFecEnv( (vEnvios.size()) + 1,
                                               formater.format(dFecHoy)), 0);

        choiceEnv.removeAll();
        //A�ade el caso de NO SELECCION (e� seleccionado por defecto)
        choiceEnv.addItem("");
        //A�ade la opci�n nuevo envio
        choiceEnv.addItem(strNuevo);

        //Reestructura el choice
        for (int cont = 0; cont < vEnvios.size(); cont++) {
          NumyFecEnv elem = (NumyFecEnv) (vEnvios.elementAt(cont));
          choiceEnv.addItem(Integer.toString(elem.getNumEnv()));
        }
        //Selecciona el que se acaba de a�adir a lista de envios
        choiceEnv.select(2);

      } //if  is Data Valid

      // error al procesar
    }
    catch (Exception e) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }

    this.modoOperacion = modoNORMAL;
    Inicializar();

  }

  void btnListado_actionPerformed(ActionEvent evt) {

    DialInfEdoInd dlgInforme = null;
    int indice = tabla.getSelectedIndex();
    CMessage msgBox = null;

    try {

      this.modoOperacion = modoESPERA;
      Inicializar();

      //Si hay fila elegida
      if (indice != BWTEnum.NOTFOUND) {
        dlgInforme = new DialInfEdoInd(app,
                                       Integer.toString( ( (DataNotInd) (lista.
            elementAt(indice))).getNumEdo()));
        dlgInforme.setEnabled(true);
        if (dlgInforme.GenerarInforme()) {
          dlgInforme.show();
        }
        setCurrent(indice);
      }

      // error al procesar
    }
    catch (Exception e) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }
    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

//___________________ CARTA_________
  void btnCarta_actionPerformed(ActionEvent evt) {

    DialCarta dlgCarta = null;
    CMessage msgBox = null;

    try {

      this.modoOperacion = modoESPERA;
      Inicializar();

      //Si hay env�o elegido
      if (isDataValidParaCarta()) {
        //Si se ha elegido nuevo se pone el sigte numero de secuencia
        if (choiceEnv.getSelectedItem().equals(strNuevo)) {
          dlgCarta = new DialCarta(app, DialCarta.CARTA_ENV_OTR_COM, sAnoBk,
                                   Integer.toString( (vEnvios.size()) + 1));
        }
        else {
          dlgCarta = new DialCarta(app, DialCarta.CARTA_ENV_OTR_COM, sAnoBk,
                                   choiceEnv.getSelectedItem());
        }
        dlgCarta.setEnabled(true);
        if (dlgCarta.GenerarInforme()) {
          dlgCarta.show();
        }
      }

      // error al procesar
    }
    catch (Exception e) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }
    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

//__________________________________

  boolean isDataValidParaCarta() {
    //Comprobaciones a�o en cualquier caso
    if (
        //Comprueba que los �ltimos datos "v�lidos" (las copias de seguridad) no son vac�os
        (!sAnoBk.equals("")) &&
        //y comprueba que no se han cambiado los datos en cajas de texto respecto a datos a�o v�lidos
        (txtAno.getText().trim().equals(sAnoBk)) &&
        //y que se ha seleccionado alg�n env�o en choice
        (! (choiceEnv.getSelectedItem().equals("")))
        ) {
      return true;
    }
    else {
      return false;
    }
  }

  boolean isDataValid() {

    //Comprobaciones a�o en cualquier caso
    if (
        //Comprueba que los �ltimos datos "v�lidos" (las copias de seguridad) no son vac�os
        (!sAnoBk.equals("")) &&
        //y comprueba que no se han cambiado los datos en cajas de texto respecto a datos a�o v�lidos
        (txtAno.getText().trim().equals(sAnoBk)) &&
        //y que se ha seleccionado alg�n env�o en choice
        (! (choiceEnv.getSelectedItem().equals("")))

        ) {
      return true;
    }
    else {
      return false;
    }
  }

  void traerUltimoAno() {

    CMessage msgBox = null;
    CLista data = null;
    Object componente = null;

    // procesa
    try {

      modoOperacion = modoESPERA;
      Inicializar();

      // obtiene y pinta la lista nueva
      data = new CLista();

      // idioma
      data.setIdioma(app.getIdioma());
      // perfil
      data.setPerfil(app.getPerfil());
      // login
      data.setLogin(app.getLogin());

      data.addElement(new DataAnoEnvOtrCom(""));

      stubCliente.setUrl(new URL(app.getURL() + "servlet/SrvEnvOtrCom"));

      data.setLogin(app.getLogin());
      data.setLortad(app.getParameter("LORTAD"));

      lista = (CLista) stubCliente.doPost(servletOBTENER_ULTIMO_ANO_Y_ENVIOS,
                                          data);
      data = null;

      // comprueba que hay datos
      if (lista.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg15.Text"));
        msgBox.show();
        msgBox = null;
        //Recupera el a�o que hab�a
        txtAno.setText(sAnoBk);
      }
      else {

        // vacia la lista de envios y la tabla
        tabla.clear();
        choiceEnv.removeAll();

        DataAnoEnvOtrCom datAnoEnvOtrCom = (DataAnoEnvOtrCom) (lista.
            firstElement());

        //A�ade el caso de NO SELECCION (e� seleccionado por defecto)
        choiceEnv.addItem("");
        //A�ade la opci�n nuevo envio
        choiceEnv.addItem(strNuevo);
        //A�ade los env�os efectuados en ese a�o
        if (datAnoEnvOtrCom.getHayAno() == true) {
          //Guarda el a�o
          txtAno.setText(datAnoEnvOtrCom.getCodAnoOtrCom());

          vEnvios = datAnoEnvOtrCom.getEnvios();
          for (int j = 0; j < vEnvios.size(); j++) {
            NumyFecEnv elem = (NumyFecEnv) (vEnvios.elementAt(j));
            choiceEnv.addItem(Integer.toString(elem.getNumEnv()));
          }
          //Guarda el datoo del a�o para posterior posible recuperaci�n
          sAnoBk = txtAno.getText();
          sUltAnoBk = txtAno.getText();
        }
        //Si no hay a�o epid.
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg16.Text"));
          msgBox.show();
          msgBox = null;
          //Recupera el a�o que hab�a
          txtAno.setText(sAnoBk);

        }

      }

      // error al procesar
    }
    catch (Exception ex) {
      ex.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
    }
    modoOperacion = modoNORMAL;
    Inicializar();

  }

  void txtAno_focusLost(FocusEvent e) {

    CMessage msgBox = null;
    CLista data = null;
    Object componente = null;

    // procesa
    try {

      txtAno.setText(txtAno.getText().trim());

      //Si el a�o no ha cambiado no hace nada
      if (txtAno.getText().equals(sAnoBk)) {
        return;
      }

      //chequea el a�o.Debe ser entero entre 0 y 9999
      //Si no es ocrrecto recupera el valor antiguo y ya est�
      if (ChequearEntero(txtAno.getText(), 0, 9999, 2) == false) {
        txtAno.setText(sAnoBk);
        return;
      }

      modoOperacion = modoESPERA;
      Inicializar();

      // obtiene y pinta la lista nueva
      data = new CLista();

      // idioma
      data.setIdioma(app.getIdioma());
      // perfil
      data.setPerfil(app.getPerfil());
      // login
      data.setLogin(app.getLogin());

      data.addElement(new DataAnoEnvOtrCom(txtAno.getText().trim()));

      stubCliente.setUrl(new URL(app.getURL() + "servlet/SrvEnvOtrCom"));
      data.setLogin(app.getLogin());
      data.setLortad(app.getParameter("LORTAD"));
      lista = (CLista) stubCliente.doPost(servletOBTENER_UN_ANO_Y_ENVIOS, data);
      data = null;

      // comprueba que hay datos
      if (lista.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg15.Text"));
        msgBox.show();
        msgBox = null;
        //Recupera el a�o que hab�a
        txtAno.setText(sAnoBk);
      }
      else {

        // vacia la lista de envios y la tabla
        tabla.clear();
        choiceEnv.removeAll();

        DataAnoEnvOtrCom datAnoEnvOtrCom = (DataAnoEnvOtrCom) (lista.
            firstElement());

        //A�ade el caso de NO SELECCION (e� seleccionado por defecto)
        choiceEnv.addItem("");

        //A�ade la opci�n nuevo envio s�lo para el �ltimo a�o
        if (txtAno.getText().equals(sUltAnoBk)) {
          choiceEnv.addItem(strNuevo);

          //A�ade los env�os efectuados en ese a�o
        }
        if (datAnoEnvOtrCom.getHayAno() == true) {
          vEnvios = datAnoEnvOtrCom.getEnvios();
          for (int j = 0; j < vEnvios.size(); j++) {
            NumyFecEnv elem = (NumyFecEnv) (vEnvios.elementAt(j));
            choiceEnv.addItem(Integer.toString(elem.getNumEnv()));
          }
          //Guarda el datoo del a�o para posterior posible recuperaci�n
          sAnoBk = txtAno.getText();

        }
        //Si no hay a�o epid.
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg16.Text"));
          msgBox.show();
          msgBox = null;
          //Recupera el a�o que hab�a
          txtAno.setText(sAnoBk);

        }

        /*
                // verifica que sea la �ltima trama
                if (lista.getState() == CLista.listaINCOMPLETA)
                  tabla.addItem(res.getString("msg17.Text"));
         */
      }

      // error al procesar
    }
    catch (Exception ex) {
      ex.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
    }
    modoOperacion = modoNORMAL;
    Inicializar();

  }

  public void setCurrent(int i) {
    Event ev = null;

    // System_out.println( tabla.countItems() );

    if (i >= tabla.countItems()) {
      i = tabla.countItems() - 1;

    }
    if (i > 0) {
      tabla.select(i);
      m_itemSelecc = i;
    }
  }

//Funci�n �til para validar a�o
//Entradas: 	Cadena a chequear, Limite inferior permitido, Limite superior, uFlag(1,2)
//Return:	uFlag=1. False, cadena no numerica. True, vacio y cadena numerica
//		    uFlag=2. False, cadena no numerica y vacio. True, cadena numerica
//*************************************************************************************
   boolean ChequearEntero(String sDat, int uMin, int uMax, int uFlag) {
     String cChar = "";
     String sString = sDat;
     int uTipo = uFlag;
     boolean b = true;

     //si chequeamos un string que admita vacio.
     if (uTipo == 1) {
       if (sString.equals("")) {
         b = true;
       }
     }
     //si chequeamos un string que no admita vacio.
     if (uTipo == 2) {
       if (sString.equals("")) {
         b = false;
       }
     }
     //comprobar es un numero
     for (int i = 0; i < sString.length(); i++) {
       cChar = sString.substring(i, i + 1);
       try {
         Integer IChar = new Integer(cChar);
       }
       catch (Exception e) { // si no es un numero
         b = false;
       }

     } //fin del for

     //comprobar valor
     if (b) {
       Integer Imin = new Integer(sString);
       Integer Imax = new Integer(sString);

       if (Imin.intValue() < uMin || uMax < Imax.intValue()) {
         b = false;
       }
     }

//# // System_out.println("Devuelve b **********" + b);
     return (b);
   } //fin de ChequearEntero

  void choiceEnv_itemStateChanged(ItemEvent e) {

    CMessage msgBox = null;
    CLista data = null;
    Object componente = null;
    NumyFecEnv envio;
    Vector vEnv = new Vector(); // Vetor env�os para uso local. Petici�n de un env�o al Servlet

    /*
        //No hace nada si no hay a�o o no hay env�o seleccionado
        if  ( (choiceEnv.getSelectedItem().equals("") ) ||  (txtAno.getText().equals("")) )
          return;
     */

    //No hace nada si no hay a�o  seleccionado
    if ( (txtAno.getText().equals(""))) {
      return;
    }

    //Si no  selecciona env�o resetea campo de fecha env�o y tabla y se sale
    if (choiceEnv.getSelectedItem().equals("")) {
      txtFecEnv.setText("");
      // vacia la tabla
      tabla.clear();
      return;
    }

    // procesa
    try {

      //Si  hay env�o nuevo seleccionado resetea campo de fecha env�o
      if (choiceEnv.getSelectedItem().equals(strNuevo)) {
        txtFecEnv.setText("");
      }
      //Cambia campo de fecha env�o
      else {
        //Si es el �ltimo a�o busca la fecha en vector teniendo en cuenta que choice tiene "" y res.getString("msg14.Text")
        if (txtAno.getText().equals(sUltAnoBk)) {
          NumyFecEnv elemento = (NumyFecEnv) (vEnvios.elementAt(choiceEnv.
              getSelectedIndex() - 2));
          txtFecEnv.setText(elemento.getFecEnv());
        }

        //Si no es el �ltimo a�o busca la fecha en vector teniemdo en cuenta que choice tiene ""
        else {
          NumyFecEnv elemento = (NumyFecEnv) (vEnvios.elementAt(choiceEnv.
              getSelectedIndex() - 1));
          txtFecEnv.setText(elemento.getFecEnv());
        }
      }

      // vacia la tabla
      tabla.clear();

      modoOperacion = modoESPERA;
      Inicializar();

      // obtiene y pinta la lista nueva
      data = new CLista();

      // idioma
      data.setIdioma(app.getIdioma());
      // perfil
      data.setPerfil(app.getPerfil());
      // login
      data.setLogin(app.getLogin());

      //Se preparan los datos de petici�n
      if (choiceEnv.getSelectedItem().equals(strNuevo)) {
        envio = new NumyFecEnv( -1);
      }
      else {
        envio = new NumyFecEnv(Integer.parseInt(choiceEnv.getSelectedItem()));
      }
      vEnv.addElement(envio);
      data.addElement(new DataAnoEnvOtrCom(txtAno.getText().trim(), true, vEnv));
      ( (DataAnoEnvOtrCom) (data.firstElement())).setComAut(app.getCA());
      ( (DataAnoEnvOtrCom) (data.firstElement())).setTSive(app.getTSive());

      //Envio al servlet
      stubCliente.setUrl(new URL(app.getURL() + "servlet/SrvEnvOtrCom"));
      data.setLogin(app.getLogin());
      data.setLortad(app.getParameter("LORTAD"));

      lista = (CLista) stubCliente.doPost(servletSELECCION_EDOIND_OTRCOM, data);
      /*
             SrvEnvOtrCom srv = new SrvEnvOtrCom();
             srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@192.168.0.13:1521:SIVE",
                             "dba_edo",
                             "manager");
             lista = srv.doDebug(servletSELECCION_EDOIND_OTRCOM, data);
       */
      data = null;

      // comprueba que hay datos
      if (lista.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg15.Text"));
        msgBox.show();
        msgBox = null;
      }
      else {
        // escribe las l�neas en la tabla
        for (int j = 0; j < lista.size(); j++) {
          componente = lista.elementAt(j);
          tabla.addItem(setLinea(componente), '&');
        }
      }

      // error al procesar
    }
    catch (Exception ex) {
      ex.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, ex.getMessage());
      msgBox.show();
      msgBox = null;
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // formatea el componente para ser insertado en una fila de la tabla
  public String setLinea(Object o) {
    DataNotInd dat = (DataNotInd) o;
    String sLinea = dat.getCodEnfEdo() + '&' + //******
        dat.getSiglas() + '&' + //********
        dat.getCodAno() + "  " + dat.getCodSem() + '&' +
        dat.getFecRec() + '&' +
        dat.getDesComAut();
    return sLinea;
  }

  // doble click en la tabla
  public void tabla_actionPerformed() {

    DialInfEdoInd dlgInforme = null;
    int indice = tabla.getSelectedIndex();
    CMessage msgBox = null;

    try {

      this.modoOperacion = modoESPERA;
      Inicializar();

      //Si hay fila elegida
      if (indice != BWTEnum.NOTFOUND) {
        dlgInforme = new DialInfEdoInd(app,
                                       Integer.toString( ( (DataNotInd) (lista.
            elementAt(indice))).getNumEdo()));
        dlgInforme.setEnabled(true);
        if (dlgInforme.GenerarInforme()) {
          dlgInforme.show();
        }
        setCurrent(indice);
      }

      // error al procesar
    }
    catch (Exception e) {
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }
    this.modoOperacion = modoNORMAL;
    Inicializar();

  }

  void tabla_itemStateChanged(JCItemEvent evt) {

    m_itemSelecc = tabla.getSelectedIndex();

  }

} //clase

// action listener de evento en botones normales( No implementa Runnable)
class PanEnvOtrCombtnNormalActionListener
    implements ActionListener {
  PanEnvOtrCom adaptee = null;
  ActionEvent e = null;

  public PanEnvOtrCombtnNormalActionListener(PanEnvOtrCom adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;

    if (e.getActionCommand() == "primero") {
      if (adaptee.tabla.countItems() > 0) {
        adaptee.m_itemSelecc = 0;
        adaptee.tabla.select(adaptee.m_itemSelecc);
        adaptee.tabla.setTopRow(adaptee.m_itemSelecc);
      }
    }
    else if (e.getActionCommand() == "anterior") {
      if (adaptee.m_itemSelecc > 0) {
        adaptee.m_itemSelecc--;
        adaptee.tabla.select(adaptee.m_itemSelecc);
        if (adaptee.m_itemSelecc - adaptee.tabla.getTopRow() <= 0) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() - 1);
        }
      }
    }
    else if (e.getActionCommand() == "siguiente") {
      int ultimo = adaptee.tabla.countItems() - 1;
      if (adaptee.m_itemSelecc < ultimo) {
        adaptee.m_itemSelecc++;
        adaptee.tabla.select(adaptee.m_itemSelecc);
        if (adaptee.m_itemSelecc - adaptee.tabla.getTopRow() >= 8) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() + 1);
        }
      }
    }
    else if (e.getActionCommand() == "ultimo") {
      int ultimo = adaptee.tabla.countItems() - 1;
      if (ultimo >= 0) {
        adaptee.m_itemSelecc = ultimo;
        adaptee.tabla.select(adaptee.m_itemSelecc);
        if (adaptee.tabla.countItems() >= 8) {
          adaptee.tabla.setTopRow(adaptee.tabla.countItems() - 8);
        }
        else {
          adaptee.tabla.setTopRow(0);
        }
      }
    }
    else { //A�adir, Mod, borrar o auxiliar
//      adaptee.btn_actionPermormed(e);
    }
  }
}

// action listener para los botones
class PanEnvOtrComActionListener
    implements ActionListener, Runnable {
  PanEnvOtrCom adaptee = null;
  ActionEvent e = null;

  public PanEnvOtrComActionListener(PanEnvOtrCom adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    if (e.getActionCommand().equals("GrabarEnv")) {
      adaptee.btnGrabarEnv_actionPerformed(e);
    }
    if (e.getActionCommand().equals("GenListado")) {
      adaptee.btnListado_actionPerformed(e);
    }
    if (e.getActionCommand().equals("GenCarta")) {
      adaptee.btnCarta_actionPerformed(e);

    }
  }
}

class PanEnvOtrCom_txtAno_focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanEnvOtrCom adaptee;
  FocusEvent e;

  PanEnvOtrCom_txtAno_focusAdapter(PanEnvOtrCom adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.txtAno_focusLost(e);
  }
}

class PanEnvOtrCom_choiceEnv_itemAdapter
    implements java.awt.event.ItemListener, Runnable {
  PanEnvOtrCom adaptee;
  ItemEvent e;

  PanEnvOtrCom_choiceEnv_itemAdapter(PanEnvOtrCom adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    adaptee.choiceEnv_itemStateChanged(e);
  }
}

// escuchador de los doble click en la tabla
class PanEnvOtrCom_tabla_actionAdapter
    implements jclass.bwt.JCActionListener {
  PanEnvOtrCom adaptee;

  PanEnvOtrCom_tabla_actionAdapter(PanEnvOtrCom adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    GestionTabla th = new GestionTabla(adaptee);
    th.run();
  }
}

// hilo de ejecuci�n para la gesti�n del doble click en la tabla
class GestionTabla
    implements Runnable {
  PanEnvOtrCom adaptee;

  public GestionTabla(PanEnvOtrCom l) {
    adaptee = l;
    Thread th = new Thread();
    th.run();
  }

  // obtiene la nueva trama de la lista
  public void run() {
    adaptee.tabla_actionPerformed();
  }
}

//Gesti�n de los click
//Tambi�n implementa Runnable por si usuario pulsa m�s..
class PanEnvOtrComItemListener
    implements JCItemListener, Runnable {
  PanEnvOtrCom adaptee = null;
  JCItemEvent e = null;

  public PanEnvOtrComItemListener(PanEnvOtrCom adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(JCItemEvent e) {
    if (e.getStateChange() == JCItemEvent.SELECTED) { //evento seleccion (no deseleccion)
      this.e = e;
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.tabla_itemStateChanged(e);
  }
}
