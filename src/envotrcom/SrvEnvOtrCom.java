package envotrcom;

//Servlet usado para env�os  a otras comunidades
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Vector;

// LORTAD
import Registro.RegistroConsultas;
import capp.CApp;
import capp.CLista;
import enfermo.Fechas;
import envressem.DataNotInd;
import envressem.NumyFecEnv;
import sapp.DBServlet;

public class SrvEnvOtrCom
    extends DBServlet {

  final int servletOBTENER_ULTIMO_ANO_Y_ENVIOS = 1;
  final int servletOBTENER_UN_ANO_Y_ENVIOS = 2;
  final int servletSELECCION_EDOIND_OTRCOM = 3;

  final int servletGRABAR_ENVIO = 4;
//  final int servletGENFICH_EDOIND_OTRCOM = 4;

  public final static int NO_HAY_EDAD = 0;
  public final static int EDAD_EN_MESES = 1;
  public final static int EDAD_EN_A�OS = 2;

  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;
    int k; //Para �ndice b�squeda columnas

    /****************** APLICACION DE LORTAD ********************************/
    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;
          /****************** APLICACION DE LORTAD ********************************/

      // objetos de datos
      //SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
    }
    CLista data = null; //lista de resultado al cliente (lineas, es decir, Strings)
    DataAnoEnvOtrCom datAnoOtrCom = null;
    String sLinea = null;
    Enumeration enum; //Para recorrer listas

    //Par�metros recibidos para generar fichero
    String sSep = "|"; //Caracter separador entre datos de una l�nea
    String sAno = "";
    String sSem = "";

    //_____________________ Querys y par�metors nuevos usados en generacion y envio RES SEMANAL_______________________________
    String sCodPro = "";
    String sCodMun = "";

    //_____________________ Querys y par�metors a�adidos usados en envios (urgente)_______________________________

    final String sBUSQUEDA_ULTIMO_ANO =
        "select MAX(CD_ANOEPI) as MAXANOEPI from SIVE_SEMANA_EPI ";
    final String sBUSQUEDA_UN_ANO =
        "select * from SIVE_SEMANA_EPI where CD_ANOEPI = ? ";
    final String sLISTADO_ENVIOS_UN_ANO = "select NM_ENVOTRC, FC_ENVOTRC from SIVE_ENV_OTRCOM where CD_ANOOTRC = ? order by NM_ENVOTRC desc";
    //Seleccion edos a otras comunidades no enviados a�n
    final String sLISTADO_LOS_EDOS_OTRCOM_NUEVO =
        " select * from SIVE_EDOIND where " +
        " CD_PROV not in (select CD_PROV from SIVE_PROVINCIA where CD_CA = ? )"
        + " and NM_ENVOTRC is null order by FC_RECEP DESC";
    //Seleccion edos urgentes ya enviados antes
    final String sLISTADO_LOS_EDOS_OTRCOM_YA_ENVIADO =
        " select * from SIVE_EDOIND where " +
        " CD_PROV not in (select CD_PROV from SIVE_PROVINCIA where CD_CA = ? )"
        + " and NM_ENVOTRC = ? and CD_ANOOTRC = ?  order by FC_RECEP DESC";
    final String sBUSQUEDA_COMAUT = "select b.CD_CA, b.DS_CA,b.DSL_CA from SIVE_PROVINCIA a, SIVE_COM_AUT b where a.CD_CA = b.CD_CA and a.CD_PROV = ?";
    final String sBUSQUEDA_ENFEREDO =
        "select CD_ENFERE from SIVE_ENFEREDO where CD_ENFCIE = ?";
    final String sLISTADO_ENFERMO =
        "select SIGLAS, FC_NAC, CD_SEXO from SIVE_ENFERMO where CD_ENFERMO = ? ";

    final String sOBTENER_NUM_ENVIO =
        "select MAX(NM_ENVOTRC) MAXNM from SIVE_ENV_OTRCOM where CD_ANOOTRC =  ?";
    //Para actualizar el num de envio
    final String sMODIFICACION_LOS_EDOS_OTRCOM =
        "UPDATE SIVE_EDOIND SET CD_ANOOTRC = ?, NM_ENVOTRC =? where "
        + " NM_EDO = ?";
    //Para crear un env�o en tabla de env�os
    final String sALTA_ENVOTRCOM = "INSERT INTO SIVE_ENV_OTRCOM ( CD_ANOOTRC, NM_ENVOTRC, FC_ENVOTRC ) VALUES (?,?,?)";

    //Envios de un a�o (tablas SIVE_ANOEPI y SIVE_ENV_OTRCOM)
    boolean bHayAno = false;
    int iNumEnv; //N�mero d3e envio
    java.sql.Date dFecEnv = null; //Fecha env�o
    String sFecEnv = ""; //Fecha envio
    Vector vEnvios = null;

    //Variables para envio ind. urgente
    int cuentaNoNulos = 0;
    int iMaxEnv = 0;

    String sComAut = ""; //Com Aut�noma del applet (donde se ejecuta)
    String sTSive = "";

    String sCodComAutEdo = ""; //Comm aut�noma a la que pertenece la prov del caso EDO
    String sDesComAutEdo = "";
    String sDesLComAutEdo = "";

    DataNotInd datoNotInd = null;
    int iNumEdo;
    String sTipCas = "";
    int iCodEnfermo;
    String sCodEnfCie = "";
    java.sql.Date dFecNot = null;
    java.sql.Date dFecRec = null; //Fecha de recepci�n
    String sFecRec = "";

    String sSig = ""; //*****
    String sCodEnfEdo = ""; //***************

    java.sql.Date dFecNac = null;
    String sCodSex = "";
    // Para c�lculo de edad del enfermo
    java.util.Date laFecha; //Fecha de la que extraeremos a�os , meses
    String fechaNac = "";
    String fechaNot = "";
    int anyoNac = 0;
    int anyoNot = 0;
    int mesNac = 0;
    int mesNot = 0;
    int anyos = 0;
    int meses = 0;
    Calendar cal = new GregorianCalendar();
    String laEdad = "";

    String sCodMod = "";
    int iNumLin = 0;
    String sDesRes = "";

    //___________________________________________________________________

    // establece la conexi�n con la base de datos
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    con.setAutoCommit(true);

    //Extrae par�metros de consulta
    datAnoOtrCom = (DataAnoEnvOtrCom) param.firstElement();

    // prepara la lista de resultados
    data = new CLista();

    // modos de operaci�n
    switch (opmode) {

      case servletOBTENER_ULTIMO_ANO_Y_ENVIOS:

        //______________________  Obtener �ltimo a�o ___________________________

        // prepara la query
        st = con.prepareStatement(sBUSQUEDA_ULTIMO_ANO);

        rs = st.executeQuery();

        // extrae la p�gina requerida  Exactamente como en el select normalmente
        while (rs.next()) {

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          //A�o m�ximo. Siempre distinto de null si tabla a�os no vac�a, pues todos los a�os son distintos de null
          //sAno= rs.getString("MAX(CD_ANOEPI)");
          sAno = rs.getString("MAXANOEPI");
          if (sAno == null) {
            bHayAno = false;
          }
          else {
            bHayAno = true;
          }
        }
        rs.close();
        st.close();
        rs = null;
        st = null;

//        data.addElement(new DataConvFec(datConvFec.getAno(),vFec, bHayFec ));

        //______________________  Obtener sus env�os ___________________________

        //Si se ha recogido max(ano) , es decir, siempre que haya alg�n a�o en b. datos
        if (bHayAno == true) {

          // prepara la query
          st = con.prepareStatement(sLISTADO_ENVIOS_UN_ANO);

          // filtro
          st.setString(1, sAno);

          rs = st.executeQuery();

          // obtiene los campos
          vEnvios = new Vector();

          // extrae la p�gina requerida  Exactamente como en el select normalmente
          while (rs.next()) {

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            //N�mero d3e envio
            iNumEnv = rs.getInt("NM_ENVOTRC");
            //Fecha
            dFecEnv = rs.getDate("FC_ENVOTRC");
            sFecEnv = Fechas.date2String(dFecEnv);
            vEnvios.addElement(new NumyFecEnv(iNumEnv, sFecEnv));
          } //while

          rs.close();
          st.close();
          rs = null;
          st = null;

          // a�ade un nodo
          data.addElement(new DataAnoEnvOtrCom(sAno, true, vEnvios));

        } // if hay a�o

        else {
          // a�ade un nodo indicando que no hay a�o
          data.addElement(new DataAnoEnvOtrCom(sAno, false, vEnvios));
        }

        break;

      case servletOBTENER_UN_ANO_Y_ENVIOS:

        //______________________  Obtener un a�o ___________________________

        //Se recogen par�metros
        sAno = datAnoOtrCom.getCodAnoOtrCom().trim();

        // prepara la query
        st = con.prepareStatement(sBUSQUEDA_UN_ANO);

        st.setString(1, datAnoOtrCom.getCodAnoOtrCom().trim());

        rs = st.executeQuery();

        // extrae la p�gina requerida  Exactamente como en el select normalmente
        while (rs.next()) {

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          //A�o m�ximo. Siempre distinto de null si tabla a�os no vac�a, pues todos los a�os son distintos de null
          sAno = rs.getString("CD_ANOEPI");
          bHayAno = true;
        }
        rs.close();
        st.close();
        rs = null;
        st = null;

        //______________________  Obtener sus env�os ___________________________

        //Si se ha recogido max(ano) , es decir, siempre que haya alg�n a�o en b. datos
        if (bHayAno == true) {

          // prepara la query
          st = con.prepareStatement(sLISTADO_ENVIOS_UN_ANO);

          // filtro
          st.setString(1, sAno);

          rs = st.executeQuery();

          // obtiene los campos
          vEnvios = new Vector();

          // extrae la p�gina requerida  Exactamente como en el select normalmente
          while (rs.next()) {

            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            //N�mero d3e envio
            iNumEnv = rs.getInt("NM_ENVOTRC");
            //Fecha
            dFecEnv = rs.getDate("FC_ENVOTRC");
            sFecEnv = Fechas.date2String(dFecEnv);

            vEnvios.addElement(new NumyFecEnv(iNumEnv, sFecEnv));
          }

          rs.close();
          st.close();
          rs = null;
          st = null;

          // a�ade un nodo
          data.addElement(new DataAnoEnvOtrCom(sAno, true, vEnvios));
        } // if hay a�o

        else {
          // a�ade un nodo indicando que no hay a�o
          data.addElement(new DataAnoEnvOtrCom(sAno, false, vEnvios));
        }
        break;

        //======================== Modo envio edos para tabla ===================================

      case servletSELECCION_EDOIND_OTRCOM:
        try {

          //______________________  Consulta casos edo ind urgentes___________________________

          //Caso de nuevo env�o
          if ( ( (NumyFecEnv) (datAnoOtrCom.getEnvios().firstElement())).
              getNumEnv() == -1) {

            st = con.prepareStatement(sLISTADO_LOS_EDOS_OTRCOM_NUEVO);
            registroConsultas.insertarParametro(datAnoOtrCom.getComAut().trim());
            st.setString(1, datAnoOtrCom.getComAut().trim());
            registroConsultas.registrar("SIVE_EDOIND",
                                        sLISTADO_LOS_EDOS_OTRCOM_NUEVO,
                                        "C�d. Enf",
                                        "",
                                        "SrvEnvOtrCom",
                                        true);
          }
          //Caso de env�o yq hecho anteriormente
          else {

            st = con.prepareStatement(sLISTADO_LOS_EDOS_OTRCOM_YA_ENVIADO);
            registroConsultas.insertarParametro(datAnoOtrCom.getComAut().trim());
            st.setString(1, datAnoOtrCom.getComAut().trim());
            registroConsultas.insertarParametro(
                String.valueOf( ( (NumyFecEnv)
                                 (datAnoOtrCom.getEnvios().firstElement())).
                               getNumEnv())
                );
            st.setInt(2,
                      ( (NumyFecEnv) (datAnoOtrCom.getEnvios().firstElement())).getNumEnv());
            registroConsultas.insertarParametro(datAnoOtrCom.getCodAnoOtrCom().
                                                trim());
            st.setString(3, datAnoOtrCom.getCodAnoOtrCom().trim());
            registroConsultas.registrar("SIVE_EDOIND",
                                        sLISTADO_LOS_EDOS_OTRCOM_YA_ENVIADO,
                                        "C�d. Enf",
                                        "",
                                        "SrvEnvOtrCom",
                                        true);

          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {

            // obtiene los campos

            iNumEdo = rs.getInt("NM_EDO");
            sAno = rs.getString("CD_ANOEPI");
            sSem = rs.getString("CD_SEMEPI");
            sTipCas = rs.getString("CD_CLASIFDIAG");
            if (sTipCas == null) {
              sTipCas = "";

            }
            sCodPro = rs.getString("CD_PROV");
            if (sCodPro == null) {
              sCodPro = "";
            }
            sCodMun = rs.getString("CD_MUN");
            if (sCodMun == null) {
              sCodMun = "";
            }
            iCodEnfermo = rs.getInt("CD_ENFERMO");
            dFecRec = rs.getDate("FC_RECEP");
            sFecRec = Fechas.date2String(dFecRec);
            sCodEnfCie = rs.getString("CD_ENFCIE");
            dFecNot = rs.getDate("FC_FECNOTIF");

            // a�ade un nodo
            data.addElement(new DataNotInd(iNumEdo, sCodPro, sCodMun,
                                           iCodEnfermo, sCodEnfCie, dFecNot,
                                           sTipCas, sAno, sSem));
            ( (DataNotInd) (data.lastElement())).setFecRec(sFecRec);
          }
          rs.close();
          st.close();
          rs = null;
          st = null;

// // System_out.println("%%OBTENIDO ENFEDOIND *********************");

          //Bucle para recoger datos de ENFEDO y SIGLAS
          for (int cont = 0; cont < data.size(); cont++) {
            datoNotInd = (DataNotInd) (data.elementAt(cont));

            //_______________ Busca ENFEDO______________________________

            // prepara la query
            st = con.prepareStatement(sBUSQUEDA_ENFEREDO);
            registroConsultas.insertarParametro(datoNotInd.getCodEnfCie());
            // filtro
            st.setString(1, datoNotInd.getCodEnfCie());
            registroConsultas.registrar("SIVE_ENFERMO",
                                        sBUSQUEDA_ENFEREDO,
                                        "C�d. Enf",
                                        datoNotInd.getCodEnfCie(),
                                        "SrvEnvOtrCom",
                                        true);
            rs = st.executeQuery();

            // extrae la p�gina requerida  Exactamente como en el select normalmente
            while (rs.next()) {

              //N�mero d3e envio
              sCodEnfEdo = rs.getString("CD_ENFERE");
            }
            ( (DataNotInd) (data.elementAt(cont))).setCodEnfEdo(sCodEnfEdo);

            rs.close();
            st.close();
            rs = null;
            st = null;

// // System_out.println("%%OBTENIDO ENFEDO *********************");
            //_______________ Busca Siglas______________________________
            st = con.prepareStatement(sLISTADO_ENFERMO);
            registroConsultas.insertarParametro(String.valueOf(datoNotInd.
                getCodEnfermo()));
            st.setInt(1, datoNotInd.getCodEnfermo());
            registroConsultas.registrar("SIVE_ENFERMO",
                                        sLISTADO_ENFERMO,
                                        "C�d. Enf",
                                        String.valueOf(datoNotInd.getCodEnfermo()),
                                        "SrvEnvOtrCom",
                                        true);

            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {
              // obtiene los campos
              sSig = rs.getString("SIGLAS");
              dFecNac = rs.getDate("FC_NAC");
              sCodSex = rs.getString("CD_SEXO");
              if (sCodSex == null) {
                sCodSex = "";
              }
            }

            datoNotInd.setSiglas(sSig);

            //C�lculo de la edad y relleno de datos en elemento

            rs.close();
            st.close();
            st = null;
            rs = null;
// // System_out.println("%%OBTIENE SIGLAAS ENFERMO*********************");

            //_______________ Busca Com Aut�noma______________________________

            st = con.prepareStatement(sBUSQUEDA_COMAUT);
//// System_out.println("%%Prep Com AUt*********************");
            st.setString(1, datoNotInd.getCodPro());
//// System_out.println("%%Set codPro********************* " + datoNotInd.getCodPro());
            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {
              // obtiene los campos
              sCodComAutEdo = rs.getString("CD_CA");
//// System_out.println("%%Obtiene sCodComAutEdo********************* " + sCodComAutEdo);
              sDesComAutEdo = rs.getString("DS_CA");
//// System_out.println("%%Obtiene sDesComAutEdo********************* " + sDesComAutEdo);
              sDesLComAutEdo = rs.getString("DSL_CA");
//// System_out.println("%%Obtiene sDesLComAutEdo********************* " );

              // obtiene la descripcion auxiliar en funci�n del idioma
              if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
                if (sDesLComAutEdo != null) {
                  sDesComAutEdo = sDesLComAutEdo;
                }
              }
            }
            datoNotInd.setCodComAut(sCodComAutEdo);
            datoNotInd.setDesComAut(sDesComAutEdo);

//// System_out.println("%%Los mete********************* ");
            rs.close();
            st.close();
            st = null;
            rs = null;
// // System_out.println("%%OBTIENE COM AUT*********************");

          } //For
// // System_out.println("%%OBTENIDAS SIGLAS Y ENFEDO *********************");

// // System_out.println("%%data  *********************" + data.toString());

        }
        catch (Exception ex) {
          throw ex;
        }
        break;

        //======================== Modo ENV�O DEL FICHero ===================================
        // listado

      case servletGRABAR_ENVIO:

// // System_out.println("%%Entra en  gen fichero *********************");

        //prepara la lista auxiliar
        CLista dataAux = new CLista();

        //Recogida Par�mettros enviados para el envio semanal y el urgente
        sSep = datAnoOtrCom.getSep();
        sComAut = datAnoOtrCom.getComAut();
        sTSive = datAnoOtrCom.getTSive();

// // System_out.println("%%Recogidos paramentros*********************");

        try {

          //Se va a modificar la b. datos al final
          con.setAutoCommit(false);

          //______________________  Consulta casos edo ind urgentes___________________________

          //Caso de nuevo env�o
          if ( ( (NumyFecEnv) (datAnoOtrCom.getEnvios().firstElement())).
              getNumEnv() == -1) {
            st = con.prepareStatement(sLISTADO_LOS_EDOS_OTRCOM_NUEVO);
            registroConsultas.insertarParametro(datAnoOtrCom.getComAut().trim());
            st.setString(1, datAnoOtrCom.getComAut().trim());
            registroConsultas.registrar("SIVE_EDOIND",
                                        sLISTADO_LOS_EDOS_OTRCOM_NUEVO,
                                        "C�d. Enf",
                                        "",
                                        "SrvEnvOtrCom",
                                        true);
          }
          //Caso de env�o ya hecho anteriormente
          else {
            // prepara la query
            st = con.prepareStatement(sLISTADO_LOS_EDOS_OTRCOM_YA_ENVIADO);

            registroConsultas.insertarParametro(datAnoOtrCom.getComAut().trim());
            st.setString(1, datAnoOtrCom.getComAut().trim());

            registroConsultas.insertarParametro(String.valueOf( ( (NumyFecEnv) (
                datAnoOtrCom.getEnvios().firstElement())).getNumEnv()));
            st.setInt(2,
                      ( (NumyFecEnv) (datAnoOtrCom.getEnvios().firstElement())).getNumEnv());

            registroConsultas.insertarParametro(datAnoOtrCom.getCodAnoOtrCom().
                                                trim());
            st.setString(3, datAnoOtrCom.getCodAnoOtrCom().trim());
            registroConsultas.registrar("SIVE_EDOIND",
                                        sLISTADO_LOS_EDOS_OTRCOM_YA_ENVIADO,
                                        "C�d. Enf",
                                        "",
                                        "SrvEnvOtrCom",
                                        true);

          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {

            // obtiene los campos

            //Tanto Strings obligatorios como no obligatorios se obtienen igual
            //En cliente se convertir�n los Strings nulos a cadenas""

            iNumEdo = rs.getInt("NM_EDO");
            sAno = rs.getString("CD_ANOEPI");
            sSem = rs.getString("CD_SEMEPI");
            sTipCas = rs.getString("CD_CLASIFDIAG");
            if (sTipCas == null) {
              sTipCas = "";

            }
            sCodPro = rs.getString("CD_PROV");
            if (sCodPro == null) {
              sCodPro = "";
            }
            sCodMun = rs.getString("CD_MUN");
            if (sCodMun == null) {
              sCodMun = "";
            }
            iCodEnfermo = rs.getInt("CD_ENFERMO");
            dFecRec = rs.getDate("FC_RECEP"); //*******************
            sFecRec = Fechas.date2String(dFecRec);

            sCodEnfCie = rs.getString("CD_ENFCIE");
            dFecNot = rs.getDate("FC_FECNOTIF");
            // a�ade un nodo
            dataAux.addElement(new DataNotInd(iNumEdo, sCodPro, sCodMun,
                                              iCodEnfermo, sCodEnfCie, dFecNot,
                                              sTipCas, sAno, sSem));
            ( (DataNotInd) (dataAux.lastElement())).setFecRec(sFecRec);
          }
          rs.close();
          st.close();
          rs = null;
          st = null;
// // System_out.println("%%OBTIENE EDOS OTRCOM *********************");

          //Caso de nuevo env�o
          if ( ( (NumyFecEnv) (datAnoOtrCom.getEnvios().firstElement())).
              getNumEnv() == -1) {

            //______  Consulta en  SIVE_EDOIND para ver el MAX(NM:ENVIOOTRCOMSEM___________________________

            st = con.prepareStatement(sOBTENER_NUM_ENVIO);

            st.setString(1, datAnoOtrCom.getCodAnoOtrCom().trim());
            rs = st.executeQuery();

            // extrae la p�gina requerida
            while (rs.next()) {
              // obtiene los campos
              //iMaxEnv = rs.getInt("MAX(NM_ENVOTRC)");
              iMaxEnv = rs.getInt("MAXNM");
              //# // System_out.println("%%Cog Max************" + iMaxEnv);
            }

            rs.close();
            st.close();
            st = null;
            rs = null;
// // System_out.println("%% OBTIENE MAX *********************");

            //______  Alta en tabla ENVOTRCOM ___________________________

            // lanza la query
            st = con.prepareStatement(sALTA_ENVOTRCOM);
//# // System_out.println("%%prep alta envurg*********************");
            //A�o al que corresponde el evvio
            st.setString(1, datAnoOtrCom.getCodAnoOtrCom().trim());
            //Valor de num envio que se actualiza
            int iEntrada = iMaxEnv + 1;
            st.setInt(2, iEntrada);
            java.util.Date fecHoy = new java.util.Date();
            // codigo buscado
            st.setDate(3, new java.sql.Date(fecHoy.getTime()));

//# // System_out.println("%% Pone MaxEnv gui*********************");
            st.executeUpdate();

            st.close();
            st = null;

//  // System_out.println("%%  Alta Envio");
          }

          enum = dataAux.elements();
          while (enum.hasMoreElements()) { //Recorremos lista
            DataNotInd datNotInd = (DataNotInd) (enum.nextElement());
            //# // System_out.println("%%extrae elem de lista  *********************");

            //Caso de nuevo env�o
            if ( ( (NumyFecEnv) (datAnoOtrCom.getEnvios().firstElement())).
                getNumEnv() == -1) {

              //______________________  Actualiza secuencia de envio en tabla EDOIND para los urgentes ___________________________

              // lanza la query
              st = con.prepareStatement(sMODIFICACION_LOS_EDOS_OTRCOM);
////# // System_out.println("Mod urg *********************");
              //A�o env�o
              st.setString(1, datAnoOtrCom.getCodAnoOtrCom().trim());
              //Valor de num envio que se actualiza
              st.setInt(2, iMaxEnv + 1);
              // codigo buscado
              st.setInt(3, datNotInd.getNumEdo());
////# // System_out.println("Pone MaxEnv *********************"+ iMaxEnv + 1);
              st.executeUpdate();
              st.close();
              st = null;

// // System_out.println("%%MODIFICA EDOIND*********************");
            } // if era nuevo envio

          } //while recorrido de vector

          if (opmode == servletGRABAR_ENVIO) {

            // valida la transacci�n de una semana
            con.commit();
          }
        }
        catch (Exception ex) {
          if (opmode == servletGRABAR_ENVIO) {
            con.rollback();
          }
          throw ex;
        }

        break;

    } //fin switch

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }
}
