package notentrada;

import java.util.Enumeration;

import capp.CLista;
import notdata.DataEntradaEDO;

/**
 * Equipos notificadores de una selacci�n de �rea, distrito y a�o.
 * Se implement� como opci�n para Castilla y Le�n.
 * @Author JRM
 * @version 23/06/2000
 */
public class ENotificadores {
  /**
   * Indica si un notificador de la lista ha sido seleccionado
   */
  final public String SELECCIONADO = "SI";

  /**
   * Lista que contiene los datos de todos los notificadores.
   * Lista es en definitiva un vector donde cada elemento es un objeto de la
       * clase DataEntradaEDO. Se utilizar� para ir recorriendo y permitir actualizar
   * los equipos notificadores en los controles oportunos.
   */
  private CLista lista = null;

  /**
   * Asignamos la lista y el primer notificador asignado
   * @param lista con todos los notificadores.
   * @CodNotif c�digo del primer notificador seleccionado en la lista.
   */
  public ENotificadores(CLista lista, String codNotif) {
    this.lista = lista;
    Enumeration e;
    DataEntradaEDO datos;

    e = lista.elements();
    //Marcamos al codNotif como seleccionado.
    marcarSeleccionado(codNotif);
  }

  /**
   * Recorre en la lista b�scando a CodNotif y m�rcandolo como seleccionado
   * para que no vuelva a ser elegido.
   * TRUCO: Utilizaremos el campo CD_OPE del DataEntradaEDO para marcar si ha
   * sido seleccionado o no. Un valor SI indicara que ha sido seleccionado.
   */
  public void marcarSeleccionado(String CodNotif) {
    DataEntradaEDO datos;
    Enumeration e = lista.elements();
    boolean encontrado = false;

    while ( (e.hasMoreElements()) && (!encontrado)) {
      datos = (DataEntradaEDO) e.nextElement();

      // Marcamos como seleccionados todos los que est�n por debajo.
      // ARS 04-06-01
      /*      if (datos.sCod.equals(CodNotif))
            {
              datos.CD_OPE = SELECCIONADO;
              encontrado = true;
            }*/
      datos.CD_OPE = SELECCIONADO;
      if (datos.sCod.equals(CodNotif)) {
        encontrado = true;
      }
    }
  }

  /**
   * Nos devuelve el siguiente equipo notificador que no �ste ya visitado
   * @result cadena vacia si no encuentra al siguiente equipo notificador
   *         o en su defecto el equipo notificador que toque.
   */
  String siguiente() {
    String CodEquipo = "";
    DataEntradaEDO datos;
    Enumeration e = lista.elements();
    boolean encontrado = false;
    boolean noHayB = true;
    String descripcion;
    int longitudDes;
    // JRM2 Modifica para que se salte el siguiente si este est� de Baja

    while ( (e.hasMoreElements()) && (!encontrado)) {
      noHayB = true;
      datos = (DataEntradaEDO) e.nextElement(); // Pasa al siguiente elemento
      longitudDes = datos.getDes().length();

      if (longitudDes > 3) {
        if (datos.getDes().charAt(longitudDes - 1) == ')' &&
            datos.getDes().charAt(longitudDes - 2) == 'B' &&
            datos.getDes().charAt(longitudDes - 3) == '(') {
//          // System_out.println ("Efectivamente esta de baja, pasamos de �l");
          noHayB = false;
        } //Fin de si esta de BAJA
      }

      if (!datos.CD_OPE.equals(SELECCIONADO) && (noHayB)) {
        datos.CD_OPE = SELECCIONADO;
        CodEquipo = datos.sCod;
        encontrado = true;
      }
    }
    return CodEquipo;
  } // siguiente
} // ENotificadores
