/*
 * Autor        Fecha           Acci�n
 *  JRM         22/06/2000      A�ade gesti�n pArDiAn.
 */
package notentrada;

import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CPanel;
import capp.CPanelButton;
import capp.CTabla;
import jclass.bwt.JCActionEvent;
import jclass.bwt.JCItemEvent;
import jclass.bwt.JCItemListener;
import notdata.DataListaNotifEDO;

public abstract class PanelListaEDO
    extends CPanel {

  // contenedor de im�genes
  protected CCargadorImagen imgs = null;
  ResourceBundle res;

  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/alta2.gif",
      "images/modificacion2.gif",
      "images/baja2.gif"};

  //Constantes del panel
  final String imgNAME_DiaSem[] = {
      "images/dia.gif",
      "images/semana.gif"};

  final String imgLABEL[] = {
      "", ""};

  // modos de operaci�n del panel
  final int modoNORMAL = 0;
  final int modoINICIO = 1;
  final int modoESPERA = 2;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;

  //valores Prim de la validacion
  public String IT_VALIDADA_PRIM = "";
  public String FC_FECVALID_PRIM = "";

  // booleans para habilitar botones seg�n flags de usuario
  boolean bAlta = false,
      bBaja = false,
      bMod = false;

  //estado boton partes
  boolean btnPartes_Enabled = false;

  // controles
  XYLayout xYLayout = new XYLayout();
  ButtonControl btnAlta = new ButtonControl();
  ButtonControl btnModificacion = new ButtonControl();
  ButtonControl btnBaja = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnPartes = new ButtonControl();

  //TextControl textControl1 = new TextControl();
  CTabla tblSemana = new CTabla();
  CTabla tblDia = new CTabla();

  Checkbox chkValidar = new Checkbox();
  Label lblValidar = new Label();
  Label lblEtiquetaValidar = new Label();

  PanelListaEDOactionAdapter actionAdapter = new PanelListaEDOactionAdapter(this);

  DiaSemana btnCtrl;

  // par�metros
  protected int modoOperacion = modoINICIO;

  /**
   * Panel de �rea, distrito y a�o
   */
  PanelArDiAn pArDiAn;

  /**
   * Establemos cual es el panel �rea, distrito y a�o.
   * @param panel ya creado
   */
  public void setPanelArDiAn(PanelArDiAn pArDiAn) {
    this.pArDiAn = pArDiAn;
  }

  public PanelListaEDO(CApp a) {
    try {

      setApp(a);
      res = ResourceBundle.getBundle("notentrada.Res" + a.getIdioma());
      btnCtrl = new DiaSemana(this, imgNAME_DiaSem, imgLABEL);
      jbInit();
      setBorde(false);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {

    // carga las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));
    btnAlta.setImage(imgs.getImage(2));
    btnModificacion.setImage(imgs.getImage(3));
    btnBaja.setImage(imgs.getImage(4));

    // controles
    this.setSize(new Dimension(553, 300));
    xYLayout.setWidth(664);
    xYLayout.setHeight(200);
    btnAlta.addActionListener(actionAdapter);
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    chkValidar.setLabel(res.getString("chkValidar.Label"));
    lblValidar.setText("");
    lblEtiquetaValidar.setText(res.getString("lblEtiquetaValidar.Text"));

    this.setLayout(xYLayout);
    this.add(btnAlta, new XYConstraints(100, 115, -1, -1));
    this.add(btnModificacion, new XYConstraints(130, 115, -1, -1));
    this.add(btnBaja, new XYConstraints(160, 115, -1, -1));
    this.add(btnAceptar, new XYConstraints(350, 115, 80, -1));
    this.add(btnCancelar, new XYConstraints(440, 115, 80, -1));
    this.add(btnCtrl, new XYConstraints(236, 115, -1, -1));

    this.add(chkValidar, new XYConstraints(104, 155, 128, 21));
    this.add(lblEtiquetaValidar, new XYConstraints(254, 154, 100, 21));
    this.add(lblValidar, new XYConstraints(354, 154, 70, 21));

    btnAlta.setActionCommand("alta");
    btnModificacion.setActionCommand("modificacion");
    btnBaja.setActionCommand("baja");
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");

    btnAlta.addActionListener(actionAdapter);
    btnModificacion.addActionListener(actionAdapter);
    btnBaja.addActionListener(actionAdapter);

    tblDia.addActionListener(new tbl_actionAdapter(this));
    //TablaItemListener tablaItemListener = new TablaItemListener(this);
    tblDia.addItemListener(new TablaItemListener(this));

    //validacion
    chkValidar.setVisible(false);
    lblValidar.setVisible(false);
    lblEtiquetaValidar.setVisible(false);

    chkValidar.addItemListener( (ItemListener) app);
    btnAceptar.addActionListener(actionAdapter);
    /*btnCancelar.addActionListener(actionAdapter);*/
    btnCancelar.addActionListener( (ActionListener) app);
    /*btnAceptar.addActionListener((ActionListener)app);*/
  }

  protected void InitPanel() {
    tblSemana.clear();
    tblDia.clear();
    GetFlagsUser();

  }

  // m�todo que recoge los flas de usuario
  public void GetFlagsUser() {

    //leemos del applet
    //permisos
    if (this.getApp().getIT_AUTALTA().equals("S")) {
      bAlta = true;
    }
    else {
      bAlta = false;
    }
    if (this.getApp().getIT_AUTBAJA().equals("S")) {
      bBaja = true;
    }
    else {
      bBaja = false;
    }
    if (this.getApp().getIT_AUTMOD().equals("S")) {
      bMod = true;
    }
    else {
      bMod = false;

    }

  } //fin getFlag

  //se supone que ha ido bien, el alta - modif - baja
  public void Actualizar_valores_validar(int modo) {
    int estado = modo;
    String it_validada = "";
    if (chkValidar.getState()) {
      it_validada = "S";
    }
    else {
      it_validada = "N";

      //baja ----
    }
    if (estado == modoBAJA) {
      IT_VALIDADA_PRIM = "";
      FC_FECVALID_PRIM = "";
    }
    //alta ----
    if (estado == modoALTA) {
      if (it_validada.equals("N")) {
        IT_VALIDADA_PRIM = "N";
        FC_FECVALID_PRIM = "";
      }
      else {
        IT_VALIDADA_PRIM = "S";
        FC_FECVALID_PRIM = this.app.getFC_ACTUAL();
      }
    } //alta ----
    //modificacion ----
    if (estado == modoMODIFICACION &&
        !IT_VALIDADA_PRIM.equals(it_validada)) {

      //S->N
      if (IT_VALIDADA_PRIM.equals("S") &&
          it_validada.equals("N")) {

        IT_VALIDADA_PRIM = "N";
        FC_FECVALID_PRIM = "";
      }
      //N->S
      if (IT_VALIDADA_PRIM.equals("N") &&
          it_validada.equals("S")) {

        IT_VALIDADA_PRIM = "S";
        FC_FECVALID_PRIM = this.app.getFC_ACTUAL();
      }
    }
  }

  public void Cargar_Validar(DataListaNotifEDO datos) {
    //cargar
    if (datos.getIT_VALIDADA().equals("S")) {
      chkValidar.setState(true);
    }
    else {
      chkValidar.setState(false);

    }

    lblValidar.setText(datos.getFC_FECVALID());

    //estado inicial para que cuando el que valide pase de S->N a N->S, grabe la fecha actual
    IT_VALIDADA_PRIM = datos.getIT_VALIDADA();
    FC_FECVALID_PRIM = datos.getFC_FECVALID();
  }

  public void Pintar_Validar() {
    //visualizar
    if (this.app.getIT_MODULO_3().equals("N")) {
      chkValidar.setVisible(false);
      lblValidar.setVisible(false);
      lblEtiquetaValidar.setVisible(false);
    }
    else if (this.app.getIT_MODULO_3().equals("S")) {
      if (this.app.getPerfil() == 5) { //visible, pero no enable
        chkValidar.setVisible(false);
        chkValidar.setVisible(true);
        lblValidar.setVisible(true);
        lblEtiquetaValidar.setVisible(true);
        chkValidar.setEnabled(false);
      }
      else if (this.app.getPerfil() == 2 ||
               this.app.getPerfil() == 3 ||
               this.app.getPerfil() == 4) {
        if (this.app.getIT_FG_VALIDAR().equals("N")) { //visible, pero no enable
          chkValidar.setVisible(false);
          chkValidar.setVisible(true);
          lblValidar.setVisible(true);
          lblEtiquetaValidar.setVisible(true);
          chkValidar.setEnabled(false);

        }
        else {
          chkValidar.setVisible(false);
          chkValidar.setVisible(true);
          lblValidar.setVisible(true);
          lblEtiquetaValidar.setVisible(true);
          chkValidar.setEnabled(true);
        }

      }
      else {
        chkValidar.setVisible(false);
        lblValidar.setVisible(false);
        lblEtiquetaValidar.setVisible(false);
      }
    }
    //parches:
    if (modoOperacion == modoBAJA ||
        modoOperacion == modoCONSULTA) {
      chkValidar.setEnabled(false);
    }

  } //fin validar

  public void Inicializar() {

    switch (modoOperacion) {
      /* case modoNORMAL:
         //# // System_out.println("plista modoNORMAL");
         enableControls(true);
           Permisos();
         setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
         break; */

      case modoINICIO:

        InitPanel();
        btnDia_actionPerformed(); // selecci�n por d�a
        enableControls(false);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        enableControls(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA:

        btnAlta.setEnabled(true);
        btnModificacion.setEnabled(true);
        btnBaja.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnCtrl.setEnabled(true);
        tblSemana.setEnabled(true);
        tblDia.setEnabled(true);
        tblDia.setVisible(true);
        tblSemana.setVisible(false);
        btnCancelar.setEnabled(true);
        btnAceptar.setLabel(res.getString("btnAceptar.Label"));
        Pintar_Validar();
        Permisos();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICACION:

        enableControls(true);
        btnAceptar.setLabel(res.getString("btnAceptar.Label"));
        Pintar_Validar();
        Permisos();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoCONSULTA: //como modif, pero con solo +- y cancelar

        enableControls(true);
        btnAlta.setEnabled(false);
        btnBaja.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnAceptar.setLabel(res.getString("btnAceptar.Label"));
        Pintar_Validar();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:

        btnAlta.setEnabled(false);
        btnModificacion.setEnabled(false);
        btnBaja.setEnabled(false);
        btnAceptar.setEnabled(true);
        tblSemana.setEnabled(true);
        tblDia.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnAceptar.setLabel(res.getString("btnAceptar.Label"));
        Pintar_Validar();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

    }

    this.doLayout();
  }

  public void Permisos() {
    // habilitar los botones btnAlta, btnBaja, btnModificacion
    // y btnNotif en funci�n de los flags de usuario
    btnAlta.setEnabled(bAlta);
    btnBaja.setEnabled(bBaja);
    btnModificacion.setEnabled(bMod);

  }

  public void EnableButtons(boolean bAlta, boolean bBaja, boolean bMod,
                            boolean bAceptar) {
    //AQUI CONTROL DE PERMISOS!!!!!!!
    btnAlta.setEnabled(bAlta);
    btnBaja.setEnabled(bBaja);
    btnModificacion.setEnabled(bMod);
    btnAceptar.setEnabled(bAceptar);
  }

  public void enableControls(boolean en) {
    chkValidar.setEnabled(en);
    tblSemana.setEnabled(en);
    tblDia.setEnabled(en);
    btnAlta.setEnabled(en);
    btnModificacion.setEnabled(en);
    btnBaja.setEnabled(en);
    btnAceptar.setEnabled(en);
    btnCancelar.setEnabled(en);
    btnCtrl.setEnabled(en);
    doLayout();
  }

  public void prueba(int AcaboDe) {
    if (AcaboDe == 1) { //seleccionado
      chkValidar.setState(true);
    }
    else {
      chkValidar.setState(false);

    }

    if (IT_VALIDADA_PRIM.equals("S")) {
      if (chkValidar.getState()) {
        lblValidar.setText(FC_FECVALID_PRIM);

      }
      else {
        lblValidar.setText("");
      }
    }
    else if (IT_VALIDADA_PRIM.equals("N")) {
      if (chkValidar.getState()) {
        lblValidar.setText(this.app.getFC_ACTUAL());
      }
      else {
        lblValidar.setText("");
      }
    }

  }

  protected abstract void btnAlta_actionPerformed();

  protected abstract void btnModificacion_actionPerformed();

  protected abstract void btnBaja_actionPerformed();

  protected abstract void btnAceptar_actionPerformed();

  protected abstract void btnCancelar_actionPerformed();

  protected abstract void btnPartes_actionPerformed();

  protected abstract void tbl_actionPerformed(JCActionEvent e);

  protected abstract void tbl_item(JCItemEvent e);

  public void btnSemana_actionPerformed() {

    tblSemana.setVisible(true);
    tblDia.setVisible(false);
    btnAlta.setEnabled(false);
    btnModificacion.setEnabled(false);
    btnBaja.setEnabled(false);
    btnAceptar.setEnabled(false);

    //cargar estado de boton Partes
    if (btnPartes.isEnabled()) {
      btnPartes_Enabled = true;
    }
    else {
      btnPartes_Enabled = false;

      //deshabilitar partes
    }
    btnPartes.setEnabled(false);
    validate();
  }

  public void btnDia_actionPerformed() {

    tblDia.setVisible(true);
    tblSemana.setVisible(false);
    btnAlta.setEnabled(true);
    btnModificacion.setEnabled(true);
    btnBaja.setEnabled(true);
    btnAceptar.setEnabled(true);
    //recargar estado del boton partes
    if (btnPartes_Enabled) {
      btnPartes.setEnabled(true);
    }
    else {
      btnPartes.setEnabled(false);

    }

    if (modoOperacion == modoCONSULTA) {
      Inicializar();

    }
    validate();
  }

  public String getFC_FECVALID() {
    String fc = "";
    fc = lblValidar.getText();
    return (fc);
  }

  public String getIT_VALIDADA() {
    String it_validada = "";
    if (chkValidar.getState()) {
      it_validada = "S";
    }
    else {
      it_validada = "N";

    }
    return (it_validada);
  }

}

class PanelListaEDOactionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelListaEDO adaptee;
  ActionEvent evt;

  PanelListaEDOactionAdapter(PanelListaEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {

    //revisa que se haya ejecutado el focus lost del otro panel
    NotificacionEDO apEDO = (NotificacionEDO) adaptee.getApp();
    apEDO.pMaestroEDO.CfechaRecep_focusLost(null);
    apEDO.pMaestroEDO.txtNotifTeor_focusLost();
    apEDO.pMaestroEDO.txtNotifReales_focusLost();

    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {

    if (evt.getActionCommand().equals("alta")) {
      adaptee.btnAlta_actionPerformed();
    }
    else if (evt.getActionCommand().equals("modificacion")) {
      adaptee.btnModificacion_actionPerformed();
    }
    else if (evt.getActionCommand().equals("baja")) {
      adaptee.btnBaja_actionPerformed();
    }
    else if (evt.getActionCommand().equals("aceptar")) {
      adaptee.btnAceptar_actionPerformed();
      /*else if (evt.getActionCommand().equals("cancelar"))
        adaptee.btnCancelar_actionPerformed(); */
    }
    else if (evt.getActionCommand().equals("partes")) {
      adaptee.btnPartes_actionPerformed();

    }
  }
}

//Click
class TablaItemListener
    implements JCItemListener {
  PanelListaEDO adaptee;

  TablaItemListener(PanelListaEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(JCItemEvent e) {
    if (e.getStateChange() == JCItemEvent.SELECTED) {
      adaptee.tbl_item(e);
    }
  }
}

// listener de los doble click en la tabla
class tbl_actionAdapter
    implements jclass.bwt.JCActionListener {
  PanelListaEDO adaptee;

  tbl_actionAdapter(PanelListaEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    adaptee.tbl_actionPerformed(e);
  }
}

class DiaSemana
    extends CPanelButton {

  protected PanelListaEDO panel;

  public DiaSemana(PanelListaEDO p, String img[], String lbl[]) {
    super(p.getApp(),
          img,
          lbl);
    panel = p;
  }

  public void btnNormalActionPerformed(String s) {
    if (s.equals("0")) {
      panel.btnDia_actionPerformed();
    }
    else {
      panel.btnSemana_actionPerformed();
    }
  }
}
