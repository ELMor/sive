/*
 * Autor              Fecha                 Accion
 *  JRM               21/06/2000            Hace que el choice de enfermedades
 *                                          cada vez que se elija una de sus
 *                                          opciones se vaya a la caja de texto
 *                                          de num�ricas.
 */
package notentrada;

import java.util.ResourceBundle;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import notdata.DataIndivEnfermedad;
import notdata.DataListaEDONum;
import notutil.Comunicador;
import sapp.StubSrvBD;

public class DialogListaEDONum
    extends CDialog {

  // contenedor de im�genes
  protected CCargadorImagen imgs = null;
  ResourceBundle res = ResourceBundle.getBundle("notentrada.Res" +
                                                getCApp().getIdioma());

  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

  // modos de operaci�n del dialog
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoCONSULTA = 6;

  // modos de operaci�n del servlet
  final int servletSELECCION_LISTA_ENFER = 9;

  // constantes del dialog
  final String strSERVLET_EDONUM = "servlet/SrvEDONum";
  final String strSERVLET_ENFERMEDAD = "servlet/SrvEnfermedad";
  final String sVigi = "N";

  // par�metros
  CLista lista, listaRet;
  StubSrvBD stubCliente;
  int iOut = -1; // Modo de salida del dialog (Aceptar=0, Cancelar=-1)
  int iModo = modoALTA;
  String sCodEnfer = "", sDesEnfer = "", sCasos = "", sFechaNotif = "";
  String sFechaRecep = "";
  String sPartes = "";

  // controles
  XYLayout xYLayout = new XYLayout();
  //Panel panel1 = new Panel();
  Label label1 = new Label();
  Label label2 = new Label();
  Label label3 = new Label();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  TextField txtCasos = new TextField();
  Choice choEnfermedad = new Choice();

  // listeners
  DialogListaEDONumactionAdapter actionAdapter = new
      DialogListaEDONumactionAdapter(this);

  // constructor
  public DialogListaEDONum(CApp a, String title, StubSrvBD stub, String servlet,
                           int modo, DataListaEDONum data) {
    super(a);
    try {
      jbInit();
      pack();
      stubCliente = stub;
      iModo = modo;
      Inicializar(data);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnAceptar.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));

    this.setTitle(res.getString("DialogListaEDONum.Title"));

    xYLayout.setHeight(150);
    xYLayout.setWidth(425);
    setSize(423, 175);
    setLayout(xYLayout);
    label1.setText(res.getString("label1.Text"));
    label2.setText(res.getString("label2B.Text"));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    txtCasos.setBackground(new Color(255, 255, 150));
    choEnfermedad.setBackground(new Color(255, 255, 150));
    add(label1, new XYConstraints(19, 24, 79, -1));
    add(choEnfermedad, new XYConstraints(100, 24, 302, -1));
    add(label2, new XYConstraints(19, 70, 51, -1));
    add(txtCasos, new XYConstraints(100, 70, 50, -1));
    this.add(btnAceptar, new XYConstraints(250, 108, -1, -1));
    add(btnCancelar, new XYConstraints(324, 108, -1, -1));

    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");
    btnAceptar.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);

    // JRM: Para controlar el cambio de item
    choEnfermedad.addItemListener(new EscuchadorChoice(this));

  }

  void Inicializar(DataListaEDONum data) {

    lista = new CLista();

    sCodEnfer = data.getCodEnfer();
    sDesEnfer = data.getDesEnfer();
    sCasos = data.getCasos();
    sPartes = data.getPartes();

    switch (iModo) {

      // en modo alta se debe mostrar el choice con todas
      // las enfermedades num�ricas
      case modoALTA:
        RellenaLista();
        break;

        // en modo modificaci�n se han el choice relleno, pero
        // con la enfermedad que se recoge, seleccionada
      case modoMODIFICACION:
        RellenaLista();
        choEnfermedad.select(sDesEnfer);
        // no se permite modificar la enfermedad
        choEnfermedad.setEnabled(false);
        txtCasos.setText(sCasos);
        break;

      case modoCONSULTA:
        RellenaLista();
        choEnfermedad.select(sDesEnfer);
        // no se permite modificar la enfermedad ni casos
        choEnfermedad.setEnabled(false);
        txtCasos.setText(sCasos);
        txtCasos.setEnabled(false);
        btnAceptar.setEnabled(false);
        break;
    }
  }

  void RellenaLista() {
    CLista data = null;
    Object componente;

    try {

      data = new CLista();
      DataIndivEnfermedad dataEnf = new DataIndivEnfermedad(sVigi, "", "", "");
      data.addElement(dataEnf);

      lista = Comunicador.Communicate(this.getCApp(),
                                      stubCliente,
                                      servletSELECCION_LISTA_ENFER,
                                      strSERVLET_ENFERMEDAD,
                                      data);

      // comprueba que hay datos
      if (lista.size() == 0) {
        CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                       res.getString("msg5.Text"));
        msgBox.show();
        msgBox = null;
      }
      else {
        for (int i = 0; i < lista.size(); i++) {
          componente = lista.elementAt(i);
          choEnfermedad.addItem( ( (DataIndivEnfermedad) componente).
                                getDS_PROCESO());
        }
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //aceptar ------------------------------------
  void btnAceptar_actionPerformed() {
    if (isDataValid()) {
      iOut = 0;
      listaRet = new CLista();

      int iSel = choEnfermedad.getSelectedIndex();

      if (iSel == -1) {
        iSel = 0;
      }
      Object componente = lista.elementAt(iSel);
      sCodEnfer = ( (DataIndivEnfermedad) componente).getCD_ENFCIE();
      sDesEnfer = ( (DataIndivEnfermedad) componente).getDS_PROCESO();
      String sVigilancia = ( (DataIndivEnfermedad) componente).getCD_TVIGI();

      listaRet.addElement(new DataListaEDONum(sCodEnfer,
                                              sDesEnfer,
                                              txtCasos.getText(),
                                              sFechaNotif,
                                              sVigilancia));
      dispose();
    }
  } //fin de aceptar  -----

  void btnCancelar_actionPerformed() {
    iOut = -1;
    dispose();
  }

  // se comprueba que, antes de dar alta o modificaci�n,
  // todos los datos est�n rellenos y son correctos
  boolean isDataValid() {

    // se comprueba que el campo de casos est� relleno
    String casos = txtCasos.getText().trim();
    if (casos.length() == 0) {
      ShowWarning(res.getString("msg6.Text"));
      return false;
    }

    // se comprueba que el contenido de casos es num�rico
    try {
      Integer I = new Integer(casos);
      int i = I.intValue();
      if (i < 0) {
        ShowWarning(res.getString("msg7.Text"));
        txtCasos.setText("");
        return false;
      }
      if (i > 9999999) {
        ShowWarning(res.getString("msg8.Text"));
        txtCasos.setText("");
        return false;
      }
      //pegote de partes
      if (!sPartes.trim().equals("")) {
        Integer ICasos = new Integer(txtCasos.getText().trim());
        int iCasos = ICasos.intValue();
        Integer IPartes = new Integer(sPartes);
        int iPartes = IPartes.intValue();

        if (iCasos < iPartes) {
          String msg = res.getString("msg9.Text") + sPartes + ")";
          ShowWarning(msg);
          txtCasos.setText("");
          return false;
        }
      }
      //---------------

    }
    catch (Exception e) {
      ShowWarning(res.getString("msg10.Text"));
      txtCasos.setText("");
      return false;
    }

    // se comprueba que el campo de fecha de notificaci�n est� relleno
    //if (txtFechaNotif.getText().trim().length() == 0) {
    //  ShowWarning("Introduzca campo fecha de notificaci�n");
    //  return false;
    //}

    // si todo est� bien, retorna true
    return true;
  }

  public Object getComponente() {
    Object o = null;

    if (iOut != -1) {
      o = listaRet.elementAt(0);

    }
    return o;
  }

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  private void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

  /**
   * Cada vez que cambia un item en el choice de enfermedades se pasa el
   * foco a la caja de texto de n�mero de casos.
   */
  void choEnfermedad_itemStateChanged() {
    txtCasos.requestFocus();
  }
}

class DialogListaEDONumactionAdapter
    implements java.awt.event.ActionListener {
  DialogListaEDONum adaptee;

  DialogListaEDONumactionAdapter(DialogListaEDONum adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("aceptar")) {
      adaptee.btnAceptar_actionPerformed();
    }
    else if (e.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed();
    }
  }
}

/**
 * Eschuchador para el cambio de opci�n en los choices.
 * @author   JRM
 * @version  21/06/2000
 */

class EscuchadorChoice
    implements java.awt.event.ItemListener {
  /**
   * Dialogo en el que est�n los choices
   */
  DialogListaEDONum Dialogo;

  /**
   * Registro del di�logo para llamar al m�todo correcto.
   */
  EscuchadorChoice(DialogListaEDONum Dialogo) {
    this.Dialogo = Dialogo;
  }

  /**
   * Cambio de item
   */
  public void itemStateChanged(ItemEvent e) {
    Dialogo.choEnfermedad_itemStateChanged();
  }
} // EscuchadorChoice
