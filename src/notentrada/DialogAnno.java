package notentrada;

import java.util.Calendar;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import notdata.DataListaEDONum;

public class DialogAnno
    extends CDialog {
  // contenedor de im�genes
  protected CCargadorImagen imgs = null;
  ResourceBundle res = ResourceBundle.getBundle("notentrada.Res" +
                                                getCApp().getIdioma());

  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

  // par�metros
  CLista lista, listaRet;
  int iOut = -1; // Modo de salida del dialog (Aceptar=0, Cancelar=-1)

  // controles
  XYLayout xYLayout = new XYLayout();
  Label label2 = new Label();
  ButtonControl btnAceptar = new ButtonControl();
  /*ButtonControl btnCancelar = new ButtonControl();*/
  TextField txtAnno = new TextField();

  // listeners
  DialogAdapter actionAdapter = new DialogAdapter(this);

  // constructor
  public DialogAnno(CApp a, String title) {
    super(a);
    try {
      jbInit();
      pack();
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnAceptar.setImage(imgs.getImage(0));
    /*btnCancelar.setImage(imgs.getImage(1));*/

    this.setTitle(res.getString("this.Title"));

    xYLayout.setHeight(122);
    xYLayout.setWidth(250);
    setSize(255, 155);
    setLayout(xYLayout);
    label2.setSize(new Dimension(400, 228));
    label2.setText(res.getString("label2.Text"));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    /*btnCancelar.setLabel("Cancelar"); */
    txtAnno.setBackground(new Color(255, 255, 150)); //CAMBIAR

    this.add(label2, new XYConstraints(68, 40, -1, -1));
    this.add(txtAnno, new XYConstraints(122, 40, 50, -1));
    this.add(btnAceptar, new XYConstraints(90, 80, -1, -1));
    /*this.add(btnCancelar, new XYConstraints(324, 108, -1, -1));*/

    btnAceptar.setActionCommand("aceptar");
    /*btnCancelar.setActionCommand("cancelar");*/
    btnAceptar.addActionListener(actionAdapter);
    /*btnCancelar.addActionListener(actionAdapter);*/

  }

  void Inicializar() {
    //pinta a�o actual
    Calendar cal = Calendar.getInstance();
    int iYearAct = cal.get(cal.YEAR);
    txtAnno.setText(new Integer(iYearAct).toString());
  }

  //aceptar ------------------------------------
  void btnAceptar_actionPerformed() {
    if (isDataValid()) {
      iOut = 0;
      listaRet = new CLista();

      listaRet.addElement(new DataListaEDONum("", "", "", "", "", "",
                                              txtAnno.getText(),
                                              "", "", "", ""));
      dispose();
    }
  } //fin de aceptar  -----

  /*void btnCancelar_actionPerformed() {
    iOut = -1;
    dispose();
     } */

  // se comprueba que, antes de dar alta o modificaci�n,
  // todos los datos est�n rellenos y son correctos
  boolean isDataValid() {

    // se comprueba que el campo de casos est� relleno
    String casos = txtAnno.getText().trim();
    if (casos.length() == 0) {
      ShowWarning(res.getString("msg1.Text"));
      return false;
    }

    // se comprueba que el contenido de casos es num�rico
    try {
      Integer I = new Integer(casos);
      int i = I.intValue();
      if (i < 0) {
        ShowWarning(res.getString("msg2.Text"));
        txtAnno.setText("");
        return false;
      }
      if (casos.length() > 4) {
        ShowWarning(res.getString("msg3.Text"));
        txtAnno.setText("");
        return false;
      }

    }
    catch (Exception e) {
      ShowWarning(res.getString("msg4.Text"));
      txtAnno.setText("");
      return false;
    }

    // si todo est� bien, retorna true
    return true;
  }

  public Object getComponente() {
    Object o = null;

    if (iOut != -1) {
      o = listaRet.elementAt(0);

    }
    return o;
  }

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  private void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }
}

class DialogAdapter
    implements java.awt.event.ActionListener {
  DialogAnno adaptee;

  DialogAdapter(DialogAnno adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("aceptar")) {
      adaptee.btnAceptar_actionPerformed();
      /*else if  ( e.getActionCommand().equals("cancelar") )
        adaptee.btnCancelar_actionPerformed(); */
    }
  }
}
