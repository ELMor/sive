package notentrada;

import java.net.URL;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import fechas.conversorfechas;
import notdata.DataEntradaEDO;
import notutil.CListaNiveles1;
import notutil.Comunicador;
import sapp.StubSrvBD;

public class PanelEntradaEDO
    extends CPanel {

  // contenedor de im�genes
  protected CCargadorImagen imgs = null;
  ResourceBundle res;

  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/aceptar.gif"};

  //variable control
  //PARA NO DISPARAR 2 VECES LOSTFOCUS!!!!!!!
  boolean bFoco1 = false;
  boolean bFoco2 = false;
  boolean bFocoA = false;

  boolean bAceptarPulsado = false;

  // modos de operaci�n del panel
  final int modoNORMAL = 0;
  final int modoESPERA = 2;

  // modos de operaci�n del servlet
  final int servletOBTENER_NIV1_X_CODIGO = 3;
  final int servletOBTENER_NIV1_X_DESCRIPCION = 4;
  final int servletSELECCION_NIV1_X_CODIGO = 5;
  final int servletSELECCION_NIV1_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletRESTRICCION_NIVEL2 = 11;

  // constantes del panel
  final String strSERVLET = "servlet/SrvEntradaEDO";

  // par�metros
  protected int modoOperacion = modoESPERA;
  protected CLista lista = null;
  protected StubSrvBD stubCliente = null;
  private boolean bNivel2Obligatorio = false;
  private boolean bNivel1SelDePopup = false;
  private boolean bNivel2SelDePopup = false;
  private String sCodNivel1 = "", sDesNivel1 = "", sCodNivel2 = "",
      sDesNivel2 = "", sAnno = "";
  private int iYearAct = 0;
  NotificacionEDO applet;
  // hashtable NotifEDO
  Hashtable hashNotifEDO = null;
  // variables booleanas que indican si el contenido de los campos
  // respectivos es correcto
  protected boolean bNivel1Valid = false;
  protected boolean bNivel2Valid = false;
  protected boolean bAnnoValid = false;

  // dialog de mensaje
  CMessage msgBox = null;
  boolean bMsg = false;

  // controles
  XYLayout xYLayout = new XYLayout();
  Label lblNivel1 = new Label();
  TextField txtNivel1 = new TextField();
  ButtonControl btnNivel1 = new ButtonControl();
  TextField txtDesNivel1 = new TextField();
  Label label1 = new Label();
  TextField txtNivel2 = new TextField();
  ButtonControl btnNivel2 = new ButtonControl();
  TextField txtDesNivel2 = new TextField();
  Label label2 = new Label();
  TextField txtAnno = new TextField();
  ButtonControl btnAceptar = new ButtonControl();

  // listeners
  EntradaEDOtextAdapter textAdapter = new EntradaEDOtextAdapter(this);
  EntradaEDOactionAdapter actionAdapter = new EntradaEDOactionAdapter(this);
  EntradaEDOfocusAdapter focusAdapter = new EntradaEDOfocusAdapter(this);

  //public PanelEntradaEDO(CApp a) {
  public PanelEntradaEDO(NotificacionEDO a, Hashtable hashNotifEDO) {
    try {
      setApp( (CApp) a);
      res = ResourceBundle.getBundle("notentrada.Res" + a.getIdioma());
      this.hashNotifEDO = hashNotifEDO;
      jbInit();
      applet = a;
      InitPanel();
      /**********   INICIAMOS LOS CAMPOS DE TEXTO   ********/
      this.txtNivel1.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesNivel1.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtNivel2.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesNivel2.setText(this.app.getDS_NIVEL2_DEFECTO());
      /************/
      RestriccionNivel2();
      stubCliente = new StubSrvBD(new URL(app.getURL() + strSERVLET));

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {

    // carga las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnNivel1.setImage(imgs.getImage(0));
    btnNivel2.setImage(imgs.getImage(0));
    btnAceptar.setImage(imgs.getImage(1));

    this.setLayout(xYLayout);
    xYLayout.setHeight(400);
    xYLayout.setWidth(680);
    this.setBackground(Color.lightGray);

    lblNivel1.setText(this.app.getNivel1() + ":");
    label1.setText(this.app.getNivel2() + ":");

    //lblNivel1.setText("Area sanitaria:");
    txtNivel1.setBackground(new Color(255, 255, 150));
    //label1.setText("Distrito sanitario:");
    label2.setText(res.getString("label2.Text"));
    txtAnno.setBackground(new Color(255, 255, 150));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));

    txtDesNivel1.setEditable(false);
    txtDesNivel2.setEditable(false);

    //btnNivel1.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    //btnNivel2.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    //btnAceptar.setImageURL(new URL(app.getCodeBase(), imgACEPTAR));

    this.add(lblNivel1, new XYConstraints(52, 95, 109, -1));
    this.add(txtNivel1, new XYConstraints(162, 95, 50, -1));
    this.add(btnNivel1, new XYConstraints(230, 94, -1, -1));
    this.add(txtDesNivel1, new XYConstraints(271, 95, 330, -1));
    this.add(label1, new XYConstraints(52, 127, 105, -1));
    this.add(txtNivel2, new XYConstraints(162, 127, 50, -1));
    this.add(btnNivel2, new XYConstraints(230, 127, -1, -1));
    this.add(txtDesNivel2, new XYConstraints(271, 127, 330, -1));
    this.add(label2, new XYConstraints(52, 159, 50, -1));
    this.add(txtAnno, new XYConstraints(162, 159, 50, -1));
    this.add(btnAceptar, new XYConstraints(518, 200, 83, 24));

    btnNivel1.setActionCommand("nivel1");
    btnNivel2.setActionCommand("nivel2");
    btnAceptar.setActionCommand("aceptar");
    txtNivel1.setName("nivel1");
    txtNivel2.setName("nivel2");

    btnNivel1.addActionListener(actionAdapter);
    txtNivel1.addKeyListener(textAdapter);
    btnNivel2.addActionListener(actionAdapter);
    txtNivel2.addKeyListener(textAdapter);
    btnAceptar.addActionListener(actionAdapter);

    txtNivel1.setName("nivel1");
    txtNivel2.setName("nivel2");
    txtAnno.setName("anno");

    txtNivel1.addFocusListener(focusAdapter);
    txtNivel2.addFocusListener(focusAdapter);
    txtAnno.addFocusListener(focusAdapter);
  }

  private void InitPanel() {
    Calendar cal = Calendar.getInstance();
    iYearAct = cal.get(cal.YEAR);
    txtAnno.setText(new Integer(iYearAct).toString());
    // modificacion 29/05/01
    // recoger el a�o del applet
    if (! (this.app.getANYO_DEFECTO().equals("")) &&
        ! (this.app.getANYO_DEFECTO() == null)) {
      txtAnno.setText( (String)this.app.getANYO_DEFECTO());
    }

    bNivel1Valid = false;
    bNivel2Valid = false;
    bAnnoValid = false;

    txtNivel1.setText("");
    txtNivel1.select(txtNivel1.getText().length(), txtNivel1.getText().length());
    txtNivel1.requestFocus();

    btnNivel2.setEnabled(false);
    txtNivel2.setEnabled(false); //*

    //SECRETO: inicializar todo a false primero, y luego activar/o no
    btnNivel1.setEnabled(false);
    btnAceptar.setEnabled(false);
  }

  protected void Normal_Botones_N1_Aceptar() {
    btnNivel1.setEnabled(true);
    btnAceptar.setEnabled(true);
  }

  protected void Espera_Botones_N1_Aceptar() {
    btnNivel1.setEnabled(false);
    btnAceptar.setEnabled(false);
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:

        txtNivel1.setEnabled(true);
        txtNivel2.setEnabled(true);
        txtAnno.setEnabled(true);
        Normal_Botones_N1_Aceptar();
        InicializaNivel2();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:

        txtNivel1.setEnabled(false);
        txtNivel2.setEnabled(false);
        txtAnno.setEnabled(false);
        Espera_Botones_N1_Aceptar();
        btnNivel2.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  void RestriccionNivel2() {

    //modoOperacion = modoESPERA;
    //Inicializar();

    //Perfiles: 4, 5 OBLIGATORIO NIVEL 2
    switch (app.getPerfil()) {
      case 4:
      case 5:
        txtNivel2.setBackground(new Color(255, 255, 150));
        bNivel2Obligatorio = true;
        break;
      case 1:
      case 2:
      case 3:
        bNivel2Obligatorio = false;
        break;
    }

    modoOperacion = modoNORMAL;
    Inicializar();

  }

  // comprueba que las variables booleanas correspondiente a cada
  // campos se encuentren a true (ya validadas en los focuslost).
  // Si est�n a false, puede que no hayan perdido el foco, as� que
  // se les da otra oportunidad
  protected boolean isDataValid() {

    //si sale algun mensaje, no hace falta seguir
    txtNivel1_focusLost();
    txtNivel2_focusLost();
    txtAnno_focusLost();

    //NIVEL1*********************
    if (txtNivel1.getText().trim().equals("")) {
      ShowWarning(res.getString("msg13.Text") + this.app.getNivel1() + "");

      txtNivel1.select(txtNivel1.getText().length(), txtNivel1.getText().length());
      txtNivel1.requestFocus();
      return false;
    }
    else { //si va relleno, se ha hecho lostfocus
      if (!bNivel1Valid) {
        ShowWarning(res.getString("msg13.Text") + this.app.getNivel1() +
                    res.getString("msg14.Text"));
        txtNivel1.setText("");
        txtNivel1.select(txtNivel1.getText().length(),
                         txtNivel1.getText().length());
        txtNivel1.requestFocus();
        return false;
      }
    } //fin de NIVEL 1********************

    //NIVEL2**********************
    //si esta vacio y es obligatorio, mal
    if (txtNivel2.getText().trim().equals("") &&
        bNivel2Obligatorio) {
      ShowWarning(res.getString("msg13.Text") + this.app.getNivel2() + "");
      txtNivel2.setText("");
      txtNivel2.select(txtNivel2.getText().length(), txtNivel2.getText().length());
      txtNivel2.requestFocus();
      return false;
    }
    //si va relleno, se ha hecho el lostfocus
    if (!txtNivel2.getText().trim().equals("")) {
      if (!bNivel2Valid) {
        ShowWarning(res.getString("msg13.Text") + this.app.getNivel2() +
                    res.getString("msg15.Text"));
        txtNivel2.setText("");
        txtNivel2.select(txtNivel2.getText().length(),
                         txtNivel2.getText().length());
        txtNivel2.requestFocus();
        return false;
      }
    }

    //A�O**************************
    if (txtAnno.getText().trim().equals("")) {
      ShowWarning(res.getString("msg16.Text"));
      txtAnno.setText("");
      txtAnno.select(txtAnno.getText().length(), txtAnno.getText().length());
      txtAnno.requestFocus();
      return false;
    }
    if ( (!txtAnno.getText().trim().equals("")) && (!bAnnoValid)) {
      ShowWarning(res.getString("msg17.Text"));
      txtAnno.setText("");
      txtAnno.select(txtAnno.getText().length(), txtAnno.getText().length());
      txtAnno.requestFocus();
      return false;
    }

    return true;
  }

  // Actualiza datos en la hashtable NotifEDO
  private void ActDatosHash() {
    hashNotifEDO.put("CD_NIVEL_1", txtNivel1.getText().trim());
    hashNotifEDO.put("DS_NIVEL_1", txtDesNivel1.getText().trim());
    if (txtNivel2.getText().trim().length() != 0) {
      hashNotifEDO.put("CD_NIVEL_2", txtNivel2.getText().trim());
      hashNotifEDO.put("DS_NIVEL_2", txtDesNivel2.getText().trim());
    }
    hashNotifEDO.put("CD_ANOEPI", txtAnno.getText().trim());
  }

  public CLista CompruebaNivel(int iMode, String sCod, String sNivel1) throws
      Exception {

    CLista lista = null, data = null;
    data = new CLista();
    data.addElement(new DataEntradaEDO(sCod, "", sNivel1));

    lista = Comunicador.Communicate(getApp(),
                                    stubCliente,
                                    iMode,
                                    strSERVLET,
                                    data);

    lista.trimToSize();
    return lista;
  }

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  private void ShowWarning(String sMessage) {
    if (msgBox != null) {
      msgBox = null; //lo matamos y abrimos otro

    }
    msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

  // m�todo que se ejecuta en respuesta al focuslost en txtNivel1
  void txtNivel1_focusLost() {

    if (bFoco1) {
      return;
    }

    modoOperacion = modoESPERA;
    Inicializar();

    if (bNivel1Valid) {
      modoOperacion = modoNORMAL;
      Inicializar();
      return;
    }

    if (bNivel1SelDePopup) {
      modoOperacion = modoNORMAL;
      Inicializar();
      return;
    }

    if (txtNivel1.getText().trim().equals("")) {
      bFoco1 = true;
      //ShowWarning("Introduzca " + this.app.getNivel1() +"");
      bFoco1 = false;
      bNivel1Valid = false;
      bNivel2Valid = false;
      modoOperacion = modoNORMAL;
      Inicializar();

      return;
    }
    // Comprueba que datos introducidos son v�lidos si no se han
    // seleccionado de los popup
    CLista data = null;
    Object componente;

    if (!bNivel1SelDePopup) {

      try {
        data = CompruebaNivel(
            servletOBTENER_NIV1_X_CODIGO,
            txtNivel1.getText().trim(), "");

        if (data.size() == 0) {
          bFoco1 = true;
          ShowWarning(res.getString("msg18.Text"));
          bFoco1 = false;
          txtNivel1.setText("");
          bNivel1Valid = false;
          bNivel2Valid = false;

          modoOperacion = modoNORMAL;
          Inicializar();

          return;
        }

      }
      catch (Exception e) { //acceso a base de datos mal
        e.printStackTrace();
        bFoco1 = true;
        msgBox = new CMessage(app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        bFoco1 = false;
        bNivel1Valid = false;
        bNivel2Valid = false;

        //reseteo
        txtNivel1.setText("");
        txtNivel1.select(txtNivel1.getText().length(),
                         txtNivel1.getText().length());
        txtNivel1.requestFocus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return;
      } //fin del try

      //si todo fue bien
      componente = data.elementAt(0);
      sCodNivel1 = ( (DataEntradaEDO) componente).getCod();
      sDesNivel1 = ( (DataEntradaEDO) componente).getDes();
      txtDesNivel1.setText(sDesNivel1);
      bNivel1SelDePopup = true;

    } //fin del if (dato validado)

    // si no ha salido por ninguna de las condiciones anteriores,
    // el nivel1 el v�lido, y se pone su boolean a true
    bNivel1Valid = true;
    if (!bAceptarPulsado) { //si no hemos pulsado aceptar

      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  // m�todo que se ejecuta en respuesta al focuslost en txtnivel2
  public void txtNivel2_focusLost() {
    if (bFoco2) {
      return;
    }

    if (bNivel2Valid) {
      return;
    }

    if (bNivel2SelDePopup) {
      return;
    }

    modoOperacion = modoESPERA;
    Inicializar();

    CLista data = null;
    Object componente = null;

    //si es obligatorio y esta vacio
    if (bNivel2Obligatorio
        && txtNivel2.getText().trim().length() == 0) {
      bFoco2 = true;
      //ShowWarning("Introduzca " + this.app.getNivel2() +"");
      bFoco2 = false;
      bNivel2Valid = false;
      modoOperacion = modoNORMAL;
      Inicializar();
      return;
    } //!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    //si lleno - y no se pulso popup
    if (txtNivel2.getText().trim().length() != 0
        && !bNivel2SelDePopup) {

      try {
        data = CompruebaNivel(servletOBTENER_NIV2_X_CODIGO,
                              txtNivel2.getText().trim(),
                              txtNivel1.getText().trim());

        if (data.size() == 0) {
          bFoco2 = true;
          ShowWarning(res.getString("msg18.Text"));
          bFoco2 = false;
          txtNivel2.setText("");
          txtDesNivel2.setText("");
          bNivel2Valid = false;

          modoOperacion = modoNORMAL;
          Inicializar();
          return;
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        bFoco2 = true;
        msgBox = new CMessage(app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        bFoco2 = false;

        bNivel2Valid = false;

        //resetea
        txtNivel2.setText("");
        txtNivel2.select(txtNivel2.getText().length(),
                         txtNivel2.getText().length());
        txtNivel2.requestFocus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return;
      }

      bNivel2SelDePopup = true;

      componente = data.elementAt(0);
      sCodNivel2 = ( (DataEntradaEDO) componente).getCod();
      sDesNivel2 = ( (DataEntradaEDO) componente).getDes();
      txtDesNivel2.setText(sDesNivel2);

    } //fin del if

    // si no ha salido por ninguna de las condiciones anteriores,
    // el nivel2 es correcto, y se pone su boolean a true
    bNivel2Valid = true;

    if (!bAceptarPulsado) { //si no hemos pulsado aceptar
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  // m�todo que se ejecuta en respuesta al focuslost en txtAnno
  void txtAnno_focusLost() {
    if (bFocoA) {
      return;
    }
    modoOperacion = modoESPERA;
    Inicializar();

    String sDato = "";
    sDato = txtAnno.getText().trim();

    // Se comprueba que el a�o est� relleno y con formato AAAA
    if (sDato.length() == 0) {
      bFocoA = true;
      //ShowWarning("Introduzca el campo obligatorio a�o");
      bFocoA = false;
      bAnnoValid = false;

      modoOperacion = modoNORMAL;
      Inicializar();

      return;
    }
    if (sDato.length() != 4) {
      bFocoA = true;
      //ShowWarning("Introduzca el a�o de 4 d�gitos");
      bFocoA = false;
      bAnnoValid = false;

      modoOperacion = modoNORMAL;
      Inicializar();

      return;
    }

    // comprueba que el a�o introducido sea correcto
    int iAnno = 0;
    try {
      iAnno = new Integer(sDato).intValue();

      // se comprueba que no sea un entero negativo
      if (iAnno < 0) {
        bFocoA = true;
        //ShowWarning("No hay datos con el criterio informado.");
        bFocoA = false;
        //txtAnno.setText("");
        bAnnoValid = false;

        modoOperacion = modoNORMAL;
        Inicializar();

        return;
      }

    }
    catch (java.lang.NumberFormatException e) {
      //NOTA:no podemos distinguir este error y conection refused!!!!!!!
      bFocoA = true;
      //ShowWarning("Introduzca un valor num�rico correcto en el campo a�o");
      bFocoA = false;
      //txtAnno.setText("");
      bAnnoValid = false;
      modoOperacion = modoNORMAL;
      Inicializar();

      return;
    }

    // se comprueba que el a�o se encuentre dentro de los a�os generados

    conversorfechas conv = new conversorfechas(sDato, app);
    if (!conv.anoValido()) {
      bFocoA = true;
      //ShowWarning("Introduzca un a�o v�lido");
      bFocoA = false;
      //txtAnno.setText("");
      bAnnoValid = false;

      modoOperacion = modoNORMAL;
      Inicializar();

      return;
    }

    // si no ha salido por ninguna de las condiciones anteriores,
    // el a�o es correcto y se pone su boolean a true
    bAnnoValid = true;
    if (!bAceptarPulsado) { //si no hemos pulsado aceptar
      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // cambio de nivel 1
  void txtNivel1_keyPressed() {
    //if (txtNivel1.getText().equals("")) {
    bNivel1SelDePopup = false;
    bNivel2SelDePopup = false;
    txtDesNivel1.setText("");
    txtNivel2.setText("");
    txtDesNivel2.setText("");
    btnNivel2.setEnabled(false);
    txtNivel2.setEnabled(false); //*
    bNivel1Valid = false;
    bNivel2Valid = false;
    //}
  }

  // actualiza el estado del nivel2, condicionado al nivel1
  protected void InicializaNivel2() {
    if (txtDesNivel1.getText().length() == 0) {
      txtNivel2.setText("");
      txtDesNivel2.setText("");
      btnNivel2.setEnabled(false);
      txtNivel2.setEnabled(false); //*

    }
    else {
      btnNivel2.setEnabled(true);
      txtNivel2.setEnabled(true); //*

    }
  }

  // procesa opci�n seleccionar las listas de niveles 1***********
  public void btnNivel1_actionPerformed() {
    DataEntradaEDO data;

    modoOperacion = modoESPERA;
    Inicializar();

    try {

      txtNivel1.setText("");
      txtDesNivel1.setText("");
      txtNivel2.setText("");
      txtDesNivel2.setText("");
      CListaNiveles1 lista = new CListaNiveles1(app,
                                                res.getString("msg19.Text"),
                                                stubCliente,
                                                strSERVLET,
                                                servletOBTENER_NIV1_X_CODIGO,
          servletOBTENER_NIV1_X_DESCRIPCION,
          servletSELECCION_NIV1_X_CODIGO,
          servletSELECCION_NIV1_X_DESCRIPCION);
      lista.btnSearch_actionPerformed();
      lista.show();

      data = (DataEntradaEDO) lista.getComponente();

      if (data != null) { //existen datos OK
        txtNivel1.setText(data.getCod());
        txtDesNivel1.setText(data.getDes());
        bNivel1SelDePopup = true;

      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;

    }
    if (bNivel1SelDePopup) {
      bNivel1Valid = true;

    }
    modoOperacion = modoNORMAL;
    Inicializar();
  } //fin de nivel1

  // cambio de nivel 2
  void txtNivel2_keyPressed() {
    //if (txtNivel2.getText().equals("")) {
    bNivel2SelDePopup = false;
    txtDesNivel2.setText("");
    bNivel2Valid = false;
    //}
  }

  // procesa opci�n seleccionar las listas de niveles 2***********
  public void btnNivel2_actionPerformed() {
    // JRM lo comenta por errores de compilaci�n con el panel PanelArDian
    /*
      DataEntradaEDO data;
      modoOperacion = modoESPERA;
      Inicializar();
      try{
        CListaNiveles2 lista = new CListaNiveles2(this,
                                            res.getString("msg19.Text"),
                                            stubCliente,
                                            strSERVLET,
                                            servletOBTENER_NIV2_X_CODIGO,
                                            servletOBTENER_NIV2_X_DESCRIPCION,
                                            servletSELECCION_NIV2_X_CODIGO,
         servletSELECCION_NIV2_X_DESCRIPCION);
        lista.show();
        data = (DataEntradaEDO) lista.getComponente();
      if (data != null) {//hay datos
        txtNivel2.setText(data.getCod());
        txtDesNivel2.setText(data.getDes());
        bNivel2SelDePopup = true;
      }
       }catch (Exception er){
        er.printStackTrace();
        msgBox = new CMessage(app, CMessage.msgERROR, er.getMessage());
        msgBox.show();
        msgBox = null;
       }
       if (bNivel2SelDePopup)
      bNivel2Valid=true;
     modoOperacion = modoNORMAL;
     Inicializar();
     */

  } //fin nivel2

//ACEPTAR***************************
  void btnAceptar_actionPerformed() {

    modoOperacion = modoESPERA;
    Inicializar();

    bAceptarPulsado = true; //en a�o, no activar modoNormal

    //si todo va bien, se graba y abre
    if (isDataValid()) {

      this.ActDatosHash();
      applet.VerNotificaciones();
    }
    else { //si mal, se pasa a normal

      modoOperacion = modoNORMAL;
      Inicializar();
    }

    bAceptarPulsado = false;

  }

//SALIR***************************
  void btnSalir_actionPerformed() {
    try {
      app.getAppletContext().showDocument(new URL(app.getCodeBase(),
                                                  "default.html"), "_self");
    }
    catch (Exception ex) {}
  }
}

// escuchador para las cajas de texto
class EntradaEDOtextAdapter
    extends java.awt.event.KeyAdapter {
  PanelEntradaEDO adaptee;

  EntradaEDOtextAdapter(PanelEntradaEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    if ( ( (TextField) e.getSource()).getName().equals("nivel1")) {
      adaptee.txtNivel1_keyPressed();
    }
    else if ( ( (TextField) e.getSource()).getName().equals("nivel2")) {
      adaptee.txtNivel2_keyPressed();
    }
  }
}

class EntradaEDOactionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelEntradaEDO adaptee;
  ActionEvent evt;

  EntradaEDOactionAdapter(PanelEntradaEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if (evt.getActionCommand().equals("nivel1")) {
      adaptee.btnNivel1_actionPerformed();
    }
    else if (evt.getActionCommand().equals("nivel2")) {
      adaptee.btnNivel2_actionPerformed();
    }
    else if (evt.getActionCommand().equals("aceptar")) {
      adaptee.btnAceptar_actionPerformed();
    }
  }
} //fin class

class EntradaEDOfocusAdapter
    implements java.awt.event.FocusListener, Runnable {
  PanelEntradaEDO adaptee;
  FocusEvent evt;

  EntradaEDOfocusAdapter(PanelEntradaEDO adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void focusGained(FocusEvent e) {

  }

  public void run() {
    if ( ( (TextField) evt.getSource()).getName().equals("nivel1")) {
      adaptee.txtNivel1_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("nivel2")) {
      adaptee.txtNivel2_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("anno")) {
      adaptee.txtAnno_focusLost();
    }
  }

}
// JRM comenta por error de compilacion con PanelArDiAn
/*
 class CListaNiveles2 extends CListaValores {
  protected PanelEntradaEDO panel;
  public CListaNiveles2(PanelEntradaEDO p,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    panel = p;
    btnSearch_actionPerformed(); //E
  }
  public Object setComponente(String s) {
    return new DataEntradaEDO(s,"",panel.txtNivel1.getText());
  }
  public String getCodigo(Object o) {
    return ( ((DataEntradaEDO)o).getCod() );
  }
  public String getDescripcion(Object o) {
    return ( ((DataEntradaEDO)o).getDes() );
  }
 }
 */
