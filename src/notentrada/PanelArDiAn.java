/*
 *  PanelArDiAn es un panel encargado de mostrar una selecci�n de
 *  �rea, distrito y a�o. Esta selecci�n ser� utilizada en la notificaci�n
 *  de casos EDO.
 *  Este panel controla la habilitaci�n del panel maestro, para ello en su
 *  creaci�n este panel recibe el objeto panel maestro para acceder al
 *  m�todo setEnabled de �ste que permitir� habilitar o deshabilitar el
 *  panel maestro de datos.
 *  Los puntos en los que se consultan cuando hay que habilitar o deshabilitar
 *  el panel maestro son desde todos los lost_focus, los click y los keypressed
 *  de �rea, distrito y a�o, adem�s de en la creaci�n de esta clase.
 *  Cuando se habilita el panel se actualizan los datos necesarios del panel
 *  maestro para que este pueda realizar sus c�lculos.
 *  Autor         Fecha           Acci�n
 *    JRM         09/06/2000      Creaci�n
 */
package notentrada;

import java.net.URL;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import fechas.conversorfechas;
import notdata.DataEntradaEDO;
import notutil.CListaNiveles1;
import notutil.Comunicador;
import sapp.StubSrvBD;

/**
 * Panel para la petici�n de un �rea, un distrito y a�o.
 * Se permite mediante la constante CON_ZBS la petici�n de la
 * zona b�sica de salud en la caja de texto del distrito.
 * @author    JRM
 * @version   09/06/2000
 */
public class PanelArDiAn
    extends CPanel {
  /**
   *  Si -false- indica que en la petici�n de distrito no se
   *  requiere la ZBS (Zona B�sica de Salud)
   */

  // final boolean CON_ZBS = true;
  boolean CON_ZBS; // Ya no es una constante

  // Modos de espera del panel
  final int modoNORMAL = 0;
  final int modoESPERA = 2;

  /**
   * Servlet al que le vamos a pedir los datos
   */
  final String strSERVLET = "servlet/SrvEntradaEDO";

  // modos de operaci�n del servlet
  final int servletOBTENER_NIV1_X_CODIGO = 3;
  final int servletOBTENER_NIV1_X_DESCRIPCION = 4;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;

  final int servletSELECCION_NIV1_X_CODIGO = 5;
  final int servletSELECCION_NIV1_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;

  // JRM: Selecci�n de distrito/Zona b�sica de salud
  final int servletSELECCION_NIV2_X_CODIGO_CON_ZBS = 30;
  final int servletOBTENER_NIV2_X_CODIGO_CON_ZBS = 31;
  final int servletSELECCION_NIV2_X_DESCRIPCION_CON_ZBS = 32;
  final int servletOBTENER_NIV2_X_DESCRIPCION_CON_ZBS = 33;

  final int servletRESTRICCION_NIVEL2 = 11;

  /**
   * Hashtable de notificaci�n en EDO.
   */
  Hashtable hashNotifEDO = null;

  /**
   * atributo utilizado para la captura de los textos en el idioma
   * correspondiente del usuario
   */
  ResourceBundle res;

  /**
   * Para conectar con el servlet adecuado
   */
  protected StubSrvBD stubCliente = null;

  /**
   * modo de operaci�n utilizado en el panel. Servir� para
   * habilitar o deshabilitar los controles.
   */
  protected int modoOperacion = modoESPERA;

  /**
   * Ret�cula para colocar los controles
   */
  XYLayout xYLayout = new XYLayout();

  // Controles del panel
  TextField tfArea = new TextField();
  TextField tfDistrito = new TextField();
  TextField tfDesArea = new TextField();
  TextField tfDesDistrito = new TextField();
  TextField tfAnno = new TextField();
  Label lArea = new Label();
  Label lDistrito = new Label();
  Label lAnno = new Label();
  ButtonControl btnArea = new ButtonControl();
  ButtonControl btnDistrito = new ButtonControl();

  /**
   * Contenedor de im�genes
   */
  protected CCargadorImagen imgs = null;

  // Im�genes utilizadas en el panel
  final String imgNAME[] = {
      "images/Magnify.gif"};

  /**
   * Di�logo para mensajes
   */
  CMessage msgBox = null;
  boolean bMsg = false;

  /**
   * Indica si el �rea ha sido seleccionado desde bot�n
   */
  private boolean bAreaSelDePopup = false;
  /**
   * Indica si el distrito ha sido seleccionado desde bot�n
   */
  private boolean bDistritoSelDePopup = false;

  /**
   * El �rea introducido es v�lido
   */
  protected boolean bAreaValid = false;
  /**
   * El distrito introducido es v�lido
   */
  protected boolean bDistritoValid = false;
  /**
   * El a�o introducido es v�lido.
   */
  protected boolean bAnnoValid = false;

  /**
   * Indica si el distrito es obligatorio o no.
   */
  private boolean bDistritoObligatorio = false;

  /**
   * A�o actual
   */
  private int iYearAct = 0;
  /**
   * Auxilicares
   */
  private String sCodNivel1 = "", sDesNivel1 = "", sCodNivel2 = "",
      sDesNivel2 = "", sAnno = "";

  /**
   * para no disparar dos veces los lost_focus
   */
  boolean bFoco1 = false;
  boolean bFoco2 = false;
  boolean bFocoA = false;

  /**
   * Panel maestro, se utilizar� para controlar los habilitar y deshabilitar.
   */
  PanelMaestroEDO panelMaestro;

  /**
   * Perfil del usuario
   */
  int perfil;

  /**
   *  Constructor del panel, carga recursos e inicializaci�n de valores.
   *  params  AppNot: Applet de notificaci�n donde se sit�a el panel.
   *  params  hashNotifEDO: Datos importantes de notificaci�n
   */
  public PanelArDiAn(NotificacionEDO AppNot, Hashtable hashNotifEDO) {
    try {
      // Guardamos la aplicaci�n, recordad que setApp es un m�todo heredado
      // de CPanel.
      setApp( (CApp) AppNot);
      // Nos quedamos con el perfil del usuario
      perfil = AppNot.getPerfil();

      // ARG: Inicializamos el flag CON_ZBS
      if (AppNot.getParameter("ORIGEN").equals("M")) {
        CON_ZBS = false;
      }
      else {
        CON_ZBS = true;

      }
      panelMaestro = ( (NotificacionEDO)this.getApp()).getPanelMaestro();
      res = ResourceBundle.getBundle("notentrada.Res" + AppNot.getIdioma());
      this.hashNotifEDO = hashNotifEDO;
      jbInit();

      // Pintamos los campos que son obligatorios.
      if (perfil != 5) {
        RestriccionNivel2();
      }
      else {
        // establecemos perfil 5
        habilitarPanelPerfil5();
        tfAnno_focusLost();
      }

      stubCliente = new StubSrvBD(new URL(app.getURL() + strSERVLET));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  } // PanelArDiAn

  /**
   *  Inicializaci�n de controles en el panel
   */
  private void jbInit() throws Exception {
    EscuchadorPerdidaFoco escPerdidaFoco = new EscuchadorPerdidaFoco(this);
    EscuchadorBoton escBoton = new EscuchadorBoton(this);
    EscuchadorKeyPress escKeyPress = new EscuchadorKeyPress(this);

    // Inicializaciones sobre el grid layout.
    this.setLayout(xYLayout);
    xYLayout.setHeight(78);
    lAnno.setAlignment(2);
    lAnno.setText("A�o: ");
    xYLayout.setWidth(680);
    this.setBackground(Color.lightGray);

    lArea.setText(this.app.getNivel1() + ":");
    lDistrito.setText(this.app.getNivel2() + ":");
    tfArea.setBackground(new Color(255, 255, 150));
    tfAnno.setBackground(new Color(255, 255, 150));

    // Cargamos las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    tfDesArea.setEditable(false);
    tfDesDistrito.setEditable(false);
    imgs.CargaImagenes();
    btnArea.setImage(imgs.getImage(0));
    btnDistrito.setImage(imgs.getImage(0));

    // Establecemos escuchadores
    btnArea.addActionListener(escBoton);
    btnDistrito.addActionListener(escBoton);
    tfArea.addFocusListener(escPerdidaFoco);
    tfDistrito.addFocusListener(escPerdidaFoco);
    tfAnno.addFocusListener(escPerdidaFoco);
    tfArea.addKeyListener(escKeyPress);
    tfDistrito.addKeyListener(escKeyPress);

    // Para los eventos
    btnArea.setActionCommand("Area");
    btnDistrito.setActionCommand("Distrito");
    tfArea.setName("Area");
    tfDistrito.setName("Distrito");
    tfDistrito.addActionListener(new PanelArDiAn_tfDistrito_actionAdapter(this));
    tfAnno.setName("Anno");

    initPanel();
    // �rea
    this.add(lArea, new XYConstraints(22, 7, 65, -1));
    this.add(tfArea, new XYConstraints(90, 7, 54, -1));
    this.add(btnArea, new XYConstraints(151, 8, 24, 24));
    this.add(tfDesArea, new XYConstraints(188, 9, 292, -1));
    this.add(lDistrito, new XYConstraints(22, 39, 65, -1));

    // Distrito
    this.add(tfDistrito, new XYConstraints(90, 38, 56, -1));
    this.add(btnDistrito, new XYConstraints(151, 41, 24, 24));
    this.add(tfDesDistrito, new XYConstraints(187, 40, 292, -1));

    // Distrito
    this.add(lAnno, new XYConstraints(532, 42, -1, -1));
    this.add(tfAnno, new XYConstraints(586, 41, 57, -1));

    //Deshabilitamos el panel maestro
    habilitarPanelMaestro();

    // Quitamos el borde del panel
    this.setBorde(false);
  } // jbInit()

  /**
   *  Inicializaci�n del panel, etiquetas de texto, etc..
   */
  private void initPanel() {
    Calendar cal = Calendar.getInstance();
    iYearAct = cal.get(cal.YEAR);
    tfAnno.setText(new Integer(iYearAct).toString());
    // modificacion 29/05/01
    // recoger el a�o del applet
    if (! (this.app.getANYO_DEFECTO().equals("")) &&
        ! (this.app.getANYO_DEFECTO() == null)) {
      tfAnno.setText( (String)this.app.getANYO_DEFECTO());
    }

    bAreaValid = false;
    bDistritoValid = false;
    bAnnoValid = true;

    // ARG: Lo comento
    //tfArea.setText("");
    //tfArea.select(tfArea.getText().length(),tfArea.getText().length());
    //tfArea.requestFocus();

    btnDistrito.setEnabled(false);
    tfDistrito.setEnabled(false); //*

    //SECRETO: inicializar todo a false primero, y luego activar/o no
    btnArea.setEnabled(false);

    // Inicializamos seg�n los valores que nos vengan por defecto
    if (!this.app.getCD_NIVEL1_DEFECTO().equals("")) {
      this.tfArea.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.tfDesArea.setText(this.app.getDS_NIVEL1_DEFECTO());
      bAreaValid = true;
      // JRM: Cuando estamos en Castilla y Le�n necesitamos meter Distrito-ZBS
      // todo junto. No rellenamos en este caso la caja de distrito porque no
      // conocemos la ZBS.
      // ARG: Ahora llega el distrito junto con la ZBS
      // Llega el distrito junto con la ZBS
      //if (!getApp().getParameter("ORIGEN").equals("C"))
      //{
      this.tfDistrito.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.tfDesDistrito.setText(this.app.getDS_NIVEL2_DEFECTO());
      bDistritoValid = true;
      //}
      //habilitarPanelMaestro();
    }
  } // initPanel()

  /**
   * Dependiendo del perfil unos valores son obligatorios o no.
   */
  void RestriccionNivel2() {
    //Perfiles: 4, 5 OBLIGATORIO NIVEL 2
    switch (app.getPerfil()) {
      case 4:
      case 5:
        tfDistrito.setBackground(new Color(255, 255, 150));
        bDistritoObligatorio = true;
        break;
      case 1:
      case 2:
      case 3:
        bDistritoObligatorio = false;
        break;
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  } // RestriccionNivel2()

  /**
   * Seg�n el modo de operaci�n se habilitan o deshabilitan controles.
   */
  public void Inicializar() {
    // Cuando el perfil es 5 es como si no existiese panel
    if (perfil != 5) {
      switch (modoOperacion) {
        case modoNORMAL:
          tfArea.setEnabled(true);
          tfDistrito.setEnabled(true);
          tfAnno.setEnabled(true);
          Normal_Botones_N1_Aceptar();
          InicializaNivel2();
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          break;

        case modoESPERA:
          tfArea.setEnabled(false);
          tfDistrito.setEnabled(false);
          tfAnno.setEnabled(false);
          Espera_Botones_N1_Aceptar();
          btnDistrito.setEnabled(false);
          setCursor(new Cursor(Cursor.WAIT_CURSOR));
          break;
      }
    }
  } // Inicializar()

  /**
   *  Hace comprobaciones de niveles
   */
  public CLista CompruebaNivel(int iMode, String sCod, String sNivel1) throws
      Exception {
    CLista lista = null, data = null;
    data = new CLista();

    //JRM: A�adimos la informaci�n de �reas para que funcionen las p�rdidas
    // de foco.
    data.setVN1(getApp().getCD_NIVEL_1_AUTORIZACIONES());
    data.setVN2(getApp().getCD_NIVEL_2_AUTORIZACIONES());

    data.addElement(new DataEntradaEDO(sCod, "", sNivel1));

    lista = Comunicador.Communicate(getApp(),
                                    stubCliente,
                                    iMode,
                                    strSERVLET,
                                    data);

    lista.trimToSize();
    return lista;
  }

  /**
   * Mensajes de aviso
   */
  private void ShowWarning(String sMessage) {
    if (msgBox != null) {
      msgBox = null; //lo matamos y abrimos otro

    }
    msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

  /**
   * Click sobre el bot�n de �reas.
   */
  void Click_Area(ActionEvent accion) {
    DataEntradaEDO data;

    modoOperacion = modoESPERA;
    Inicializar();

    try {

      tfArea.setText("");
      tfDesArea.setText("");
      tfDistrito.setText("");
      tfDesDistrito.setText("");
      // Borramos si se ha seleccionado equipo notificador
      if (!panelMaestro.txtCodEquipo.getText().trim().equals("")) {
        panelMaestro.actEquipoNotificador("");

      }
      CListaNiveles1 lista = new CListaNiveles1(app,
                                                res.getString("msg19.Text"),
                                                stubCliente,
                                                strSERVLET,
                                                servletOBTENER_NIV1_X_CODIGO,
          servletOBTENER_NIV1_X_DESCRIPCION,
          servletSELECCION_NIV1_X_CODIGO,
          servletSELECCION_NIV1_X_DESCRIPCION);

      lista.btnSearch_actionPerformed();
      lista.show();

      data = (DataEntradaEDO) lista.getComponente();

      //existen datos OK
      if (data != null) {
        tfArea.setText(data.getCod());
        tfDesArea.setText(data.getDes());
        bAreaSelDePopup = true;
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    if (bAreaSelDePopup) {
      bAreaValid = true;
      // miramos el panel maestro para deshabilitarlo o habilitarlo
    }
    habilitarPanelMaestro();

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  /**
   * Click sobre el bot�n de distritos.
   */
  void Click_Distrito(ActionEvent accion) {
    CListaNiveles2 lista;
    DataEntradaEDO data;

    modoOperacion = modoESPERA;
    Inicializar();

    // Borramos si se ha seleccionado equipo notificador
    if (!panelMaestro.txtCodEquipo.getText().trim().equals("")) {
      panelMaestro.actEquipoNotificador("");

    }
    try {
      // Petici�n s�lo con distrito.
      if (!CON_ZBS) {
        lista = new CListaNiveles2(this,
                                   res.getString("msg19.Text"),
                                   stubCliente,
                                   strSERVLET,
                                   servletOBTENER_NIV2_X_CODIGO,
                                   servletOBTENER_NIV2_X_DESCRIPCION,
                                   servletSELECCION_NIV2_X_CODIGO,
                                   servletSELECCION_NIV2_X_DESCRIPCION);
        // Petici�n con distrito/zona b�sica de salud
      }
      else {
        lista = new CListaNiveles2(this,
                                   res.getString("msg19.Text"),
                                   stubCliente,
                                   strSERVLET,
                                   servletOBTENER_NIV2_X_CODIGO_CON_ZBS,
                                   servletOBTENER_NIV2_X_DESCRIPCION_CON_ZBS,
                                   servletSELECCION_NIV2_X_CODIGO_CON_ZBS,
                                   servletSELECCION_NIV2_X_DESCRIPCION_CON_ZBS);

      }
      lista.show();
      data = (DataEntradaEDO) lista.getComponente();

      //hay datos
      if (data != null) {
        tfDistrito.setText(data.getCod());
        tfDesDistrito.setText(data.getDes());
        bDistritoSelDePopup = true;
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    if (bDistritoSelDePopup) {
      bDistritoValid = true;
      // miramos el panel maestro para deshabilitarlo o habilitarlo
    }
    habilitarPanelMaestro();
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  /**
   * Habilitar de controles de �rea
   */
  protected void Normal_Botones_N1_Aceptar() {
    btnArea.setEnabled(true);
  }

  /**
   * Deshabilitar de controles de area
   */
  protected void Espera_Botones_N1_Aceptar() {
    btnArea.setEnabled(false);
  }

  /**
   * Inicializa controles de distrito
   */
  protected void InicializaNivel2() {
    if (tfDesArea.getText().length() == 0) {
      tfDistrito.setText("");
      tfDesDistrito.setText("");
      btnDistrito.setEnabled(false);
      tfDistrito.setEnabled(false); //*

    }
    else {
      btnDistrito.setEnabled(true);
      tfDistrito.setEnabled(true); //*
    }
  }

  /**
   * P�rdida de foco en �rea
   */
  void tfArea_focusLost() {
    tfArea.setText(tfArea.getText().trim());

    if (bFoco1) {
      return;
    }

    if (sCodNivel1.equals(tfArea.getText().trim())) {
      if (tfDesArea.getText().trim().equals("")) {
        tfDesArea.setText(sDesNivel1);
      }
      return;
    }

    sCodNivel2 = "";
    tfDistrito.setText("");
    tfDesDistrito.setText("");
    // Borramos si se ha seleccionado equipo notificador
    if (!panelMaestro.txtCodEquipo.getText().trim().equals("")) {
      panelMaestro.actEquipoNotificador("");

    }
    modoOperacion = modoESPERA;
    Inicializar();

    if (bAreaValid) {
      modoOperacion = modoNORMAL;
      Inicializar();
      // miramos el panel maestro para deshabilitarlo o habilitarlo
      habilitarPanelMaestro();
      return;
    }

    if (bAreaSelDePopup) {
      modoOperacion = modoNORMAL;
      Inicializar();
      // miramos el panel maestro para deshabilitarlo o habilitarlo
      habilitarPanelMaestro();
      return;
    }

    if (tfArea.getText().trim().equals("")) {
      bFoco1 = true;
      //ShowWarning("Introduzca " + this.app.getNivel1() +"");
      bFoco1 = false;
      bAreaValid = false;
      bDistritoValid = false;
      modoOperacion = modoNORMAL;
      Inicializar();
      // miramos el panel maestro para deshabilitarlo o habilitarlo
      habilitarPanelMaestro();
      return;
    }
    // Comprueba que datos introducidos son v�lidos si no se han
    // seleccionado de los popup
    CLista data = null;
    Object componente;

    if (!bAreaSelDePopup) {
      try {
        data = CompruebaNivel(
            servletOBTENER_NIV1_X_CODIGO,
            tfArea.getText().trim(), "");

        if (data.size() == 0) {
          bFoco1 = true;
          ShowWarning(res.getString("msg18.Text"));
          bFoco1 = false;
          tfArea.setText("");
          bAreaValid = false;
          bDistritoValid = false;
          modoOperacion = modoNORMAL;
          Inicializar();
          // miramos el panel maestro para deshabilitarlo o habilitarlo
          habilitarPanelMaestro();
          return;
        }
      }
      catch (Exception e) { //acceso a base de datos mal
        e.printStackTrace();
        bFoco1 = true;
        msgBox = new CMessage(app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        bFoco1 = false;
        bAreaValid = false;
        bDistritoValid = false;
        //reseteo
        tfArea.setText("");
        tfArea.select(tfArea.getText().length(),
                      tfArea.getText().length());
        tfArea.requestFocus();

        modoOperacion = modoNORMAL;
        Inicializar();
        // miramos el panel maestro para deshabilitarlo o habilitarlo
        habilitarPanelMaestro();
        return;
      } //fin del try

      //si todo fue bien
      componente = data.elementAt(0);
      sCodNivel1 = ( (DataEntradaEDO) componente).getCod();
      sDesNivel1 = ( (DataEntradaEDO) componente).getDes();
      tfDesArea.setText(sDesNivel1);
      bAreaSelDePopup = true;

    } //fin del if (dato validado)

    // si no ha salido por ninguna de las condiciones anteriores,
    // el nivel1 el v�lido, y se pone su boolean a true
    bAreaValid = true;
    // miramos el panel maestro para deshabilitarlo o habilitarlo
    habilitarPanelMaestro();
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  /**
   * P�rdida de foco en a�o
   */
  void tfAnno_focusLost() {
    if (bFocoA) {
      return;
    }
    modoOperacion = modoESPERA;
    Inicializar();

    String sDato = "";
    sDato = tfAnno.getText().trim();

    // Se comprueba que el a�o est� relleno y con formato AAAA
    if (sDato.length() == 0) {
      bFocoA = true;
      //ShowWarning("Introduzca el campo obligatorio a�o");
      bFocoA = false;
      bAnnoValid = false;

      modoOperacion = modoNORMAL;
      Inicializar();
      // miramos el panel maestro para deshabilitarlo o habilitarlo
      habilitarPanelMaestro();

      return;
    }
    if (sDato.length() != 4) {
      bFocoA = true;
      //ShowWarning("Introduzca el a�o de 4 d�gitos");
      bFocoA = false;
      bAnnoValid = false;

      modoOperacion = modoNORMAL;
      Inicializar();
      // miramos el panel maestro para deshabilitarlo o habilitarlo
      habilitarPanelMaestro();
      return;
    }

    // comprueba que el a�o introducido sea correcto
    int iAnno = 0;
    try {
      iAnno = new Integer(sDato).intValue();

      // se comprueba que no sea un entero negativo
      if (iAnno < 0) {
        bFocoA = true;
        //ShowWarning("No hay datos con el criterio informado.");
        bFocoA = false;
        //txtAnno.setText("");
        bAnnoValid = false;

        modoOperacion = modoNORMAL;
        Inicializar();
        // miramos el panel maestro para deshabilitarlo o habilitarlo
        habilitarPanelMaestro();
        return;
      }

    }
    catch (java.lang.NumberFormatException e) {
      //NOTA:no podemos distinguir este error y conection refused!!!!!!!
      bFocoA = true;
      //ShowWarning("Introduzca un valor num�rico correcto en el campo a�o");
      bFocoA = false;
      //txtAnno.setText("");
      bAnnoValid = false;
      modoOperacion = modoNORMAL;
      Inicializar();
      // miramos el panel maestro para deshabilitarlo o habilitarlo
      habilitarPanelMaestro();
      return;
    }

    // se comprueba que el a�o se encuentre dentro de los a�os generados

    conversorfechas conv = new conversorfechas(sDato, app);
    if (!conv.anoValido()) {
      bFocoA = true;
      //ShowWarning("Introduzca un a�o v�lido");
      bFocoA = false;
      //txtAnno.setText("");
      bAnnoValid = false;

      modoOperacion = modoNORMAL;
      Inicializar();
      // miramos el panel maestro para deshabilitarlo o habilitarlo
      habilitarPanelMaestro();
      return;
    }

    // si no ha salido por ninguna de las condiciones anteriores,
    // el a�o es correcto y se pone su boolean a true
    bAnnoValid = true;
    // miramos el panel maestro para deshabilitarlo o habilitarlo
    habilitarPanelMaestro();

    sAnno = tfAnno.getText().trim();

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  /**
   * P�rdida de foco en distrito
   */
  void tfDistrito_focusLost() {
    String Distrito = tfDistrito.getText();

    // Borramos si se ha seleccionado equipo notificador
    if (!panelMaestro.txtCodEquipo.getText().trim().equals("")) {
      panelMaestro.actEquipoNotificador("");

      // El distrito cuando tiene zona b�sica de salud es de la forma XXYY
      // donde XX es el distrito e YY la zona b�sica de salud
    }
    if ( (CON_ZBS) && (Distrito.length() >= 1) && (Distrito.length() < 4)) {
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            res.getString("SelecDZBS.Text"));
      msgBox.show();
      msgBox = null;
      return;
    }

    if (bFoco2) {
      return;
    }

    if (bDistritoValid) {
      return;
    }

    if (bDistritoSelDePopup) {
      return;
    }

    modoOperacion = modoESPERA;
    Inicializar();

    CLista data = null;
    Object componente = null;

    //si es obligatorio y esta vacio
    if (bDistritoObligatorio &&
        tfDistrito.getText().trim().length() == 0
        ) {
      bFoco2 = true;
      //ShowWarning("Introduzca " + this.app.getNivel2() +"");
      bFoco2 = false;
      bDistritoValid = false;
      modoOperacion = modoNORMAL;
      Inicializar();
      // miramos el panel maestro para deshabilitarlo o habilitarlo
      habilitarPanelMaestro();
      return;
    } //!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    //si lleno - y no se pulso popup
    if (tfDistrito.getText().trim().length() != 0 &&
        !bDistritoSelDePopup) {
      try {
        if (!CON_ZBS) {
          data = CompruebaNivel(servletOBTENER_NIV2_X_CODIGO,
                                tfDistrito.getText().trim(),
                                tfArea.getText().trim());
        }
        else {
          data = CompruebaNivel(servletOBTENER_NIV2_X_CODIGO_CON_ZBS,
                                tfDistrito.getText().trim(),
                                tfArea.getText().trim());

        }

        if (data.size() == 0) {
          bFoco2 = true;
          ShowWarning(res.getString("msg18.Text"));
          bFoco2 = false;
          tfDistrito.setText("");
          tfDesDistrito.setText("");
          sCodNivel2 = "";
          bDistritoValid = false;

          modoOperacion = modoNORMAL;
          Inicializar();
          // miramos el panel maestro para deshabilitarlo o habilitarlo
          habilitarPanelMaestro();
          return;
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        bFoco2 = true;
        msgBox = new CMessage(app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        bFoco2 = false;

        bDistritoValid = false;

        //resetea
        tfDistrito.setText("");
        tfDistrito.select(tfDistrito.getText().length(),
                          tfDistrito.getText().length());
        tfDistrito.requestFocus();

        modoOperacion = modoNORMAL;
        Inicializar();
        // miramos el panel maestro para deshabilitarlo o habilitarlo
        habilitarPanelMaestro();
        return;
      }

      bDistritoSelDePopup = true;

      componente = data.elementAt(0);
      sCodNivel2 = ( (DataEntradaEDO) componente).getCod();
      sDesNivel2 = ( (DataEntradaEDO) componente).getDes();
      tfDesDistrito.setText(sDesNivel2);

    } //fin del if

    // si no ha salido por ninguna de las condiciones anteriores,
    // el nivel2 es correcto, y se pone su boolean a true
    bDistritoValid = true;
    // miramos el panel maestro para deshabilitarlo o habilitarlo
    habilitarPanelMaestro();
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  /**
   * Pulsaci�n de teclado sobre �rea.
   */
  void tfArea_keyPressed() {
    if (sCodNivel1.equals(tfArea.getText().trim())) {
      if (tfDesArea.getText().trim().equals("")) {
        tfDesArea.setText(sDesNivel1);
      }
      return;
    }
    sCodNivel1 = "";
    sCodNivel2 = "";
    bAreaSelDePopup = false;
    bDistritoSelDePopup = false;
    tfDesArea.setText("");
    tfDistrito.setText("");
    tfDesDistrito.setText("");
    btnDistrito.setEnabled(false);
    tfDistrito.setEnabled(false); //*
    bAreaValid = false;
    bDistritoValid = false;
    // miramos el panel maestro para deshabilitarlo o habilitarlo
    habilitarPanelMaestro();
  }

  /**
   * Pulsaci�n de teclado sobre distrito.
   */
  void tfDistrito_keyPressed() {
    bDistritoSelDePopup = false;
    sCodNivel2 = "";
    tfDesDistrito.setText("");
    bDistritoValid = false;
    // miramos el panel maestro para deshabilitarlo o habilitarlo
    habilitarPanelMaestro();
  }

  /**
   * Comprobaci�n de datos correctos
   * @return Devuelve true si el �rea,distrito y a�o son v�lidos
   */
  private boolean datosCorrectos() {
    boolean bDistrito = true;
    if (bDistritoObligatorio) {
      bDistrito = bDistritoValid;
    }
    else {
      bDistrito = true;
    }
    return (bAreaValid) && (bDistrito) && (bAnnoValid);
  }

  /**
   * Habilitamos el control del panel maestro o no.
   */
  private void habilitarPanelMaestro() {
    String CodArea = tfArea.getText();
    String DesArea = tfDesArea.getText();
    String CodDistrito;
    String DesDistrito;

    if ( (!bDistritoObligatorio) && (tfDistrito.getText().trim().length() == 0)) {
      CodDistrito = "";
      DesDistrito = "";
    }
    else {
      CodDistrito = tfDistrito.getText();
      DesDistrito = tfDesDistrito.getText();
    }

    //panelMaestro.setEnabled(datosCorrectos());
    panelMaestro.habilitarDesdePanelArDiAn(datosCorrectos());
    if (panelMaestro.isEnabled()) {
      panelMaestro.ActFechaDesdePanelArDiAn(tfAnno.getText(),
                                            CodArea,
                                            DesArea,
                                            CodDistrito,
                                            DesDistrito);

    }
  }

  /**
   *  Habilita y carga el panel seg�n los datos de un usuario de perfil 5
   */
  private void habilitarPanelPerfil5() {
    // Los datos son correctos.
    bAreaValid = true;
    bDistritoValid = true;
    bAnnoValid = true;

    // Colores de fondo
    tfArea.setBackground(Color.gray);
    tfDistrito.setBackground(Color.gray);

    tfArea.setEnabled(false);
    tfDistrito.setEnabled(false);
    tfDesArea.setEnabled(false);
    tfDesDistrito.setEnabled(false);
    btnArea.setEnabled(false);
    btnDistrito.setEnabled(false);
    tfAnno.setEnabled(true);

    // Escribimos �rea.
    tfArea.setText(getApp().getCD_NIVEL_1_EQ());
    tfDesArea.setText(getApp().getDS_NIVEL_1_EQ());

    // Escribimos distrito y la zona b�sica si se pone
    if (CON_ZBS) {
      tfDistrito.setText(getApp().getCD_NIVEL_2_EQ() + getApp().getCD_ZBS_EQ());
    }
    else {
      tfDistrito.setText(getApp().getCD_NIVEL_2_EQ());

    }
    if (CON_ZBS) {
      tfDesDistrito.setText(getApp().getDS_NIVEL_2_EQ() + " - " +
                            getApp().getDS_ZBS_EQ());
    }
    else {
      tfDesDistrito.setText(getApp().getDS_NIVEL_2_EQ());

      // Adem�s pintamos el equipo notificador que debe ser fijo y deshabilitamos
      // parte de los controles del equipo notificador
    }
    panelMaestro.setCodNivel1(getApp().getCD_NIVEL_1_EQ());
    if (CON_ZBS) {
      panelMaestro.setCodNivel2(getApp().getCD_NIVEL_2_EQ() +
                                getApp().getCD_ZBS_EQ());
    }
    else {
      panelMaestro.setCodNivel2(getApp().getCD_NIVEL_2_EQ());

    }
    panelMaestro.txtCodEquipo.setText(getApp().getCD_E_NOTIF());
    panelMaestro.txtCodEquipo_focusLost();
    panelMaestro.modoOperacion = panelMaestro.modoCABECERA;
    // deshabilitamos la parte de equipo notificador
    //panelMaestro.habilitarEqNot(false);

  }

  /**
   * Habilita o deshabilita todo el panel. Se podr� efectuar dicha operaci�n
       * siempre que el perfil del usuario sea distinto de 5 en cuyo caso permanecer�
   * deshabilitado con los datos de partida que nos diga el applet.
   * @param bOK establece si se quiere habilitar o no el panel.
   */
  void habilitarPanel(boolean bOK) {
    if (perfil != 5) {
      tfArea.setEnabled(bOK);
      tfDistrito.setEnabled(bOK);
      tfDesArea.setEnabled(bOK);
      tfDesDistrito.setEnabled(bOK);
      tfAnno.setEnabled(bOK);
      btnArea.setEnabled(bOK);
      btnDistrito.setEnabled(bOK);
    }

  }

  void tfDistrito_actionPerformed(ActionEvent e) {

  }

} // PanelArDiAn

/**
 * Lista utilizada para la transmision de los distritos
 *
 * @author JRM
 * @version 12/06/2000
 */
class CListaNiveles2
    extends CListaValores {
  /**
   * Panel sobre el que act�a la lista
   */
  protected PanelArDiAn panel;

  /**
   * Constructor, inicializaci�n de valores
   */
  public CListaNiveles2(PanelArDiAn p,
                        String title,
                        StubSrvBD stub,
                        String servlet,
                        int obtener_x_codigo,
                        int obtener_x_descricpcion,
                        int seleccion_x_codigo,
                        int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  /**
   * Crea data estableciendo nivel2
   */
  public Object setComponente(String s) {
    return new DataEntradaEDO(s, "", panel.tfArea.getText());
  }

  /**
   * Devuelve c�digo grabado desde setComponente
   */
  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  /**
   * Devuelve descripci�n grabada desde setComponente
   */
  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

/**
 * Escuchador para la p�rdida de foco de los textfields
 * @author JRM
 * "version 12/06/2000
 */
class EscuchadorPerdidaFoco
    implements java.awt.event.FocusListener, Runnable {
  PanelArDiAn adaptee;
  FocusEvent evento;

  /**
   * Contructor, incializaci�n al panel.
   */
  EscuchadorPerdidaFoco(PanelArDiAn adaptee) {
    this.adaptee = adaptee;
  }

  /**
   * Captura de la p�rdida de foco y ejecuci�n mediante hilo
   */
  public void focusLost(FocusEvent e) {
    evento = e;
    Thread th = new Thread(this);
    th.start();
  }

  /**
   * Captura del foco. No implementado.
   */
  public void focusGained(FocusEvent e) {

  }

  /**
   * Ejecuci�n de la p�rdida del foco
   */
  public void run() {
    if ( ( (TextField) evento.getSource()).getName().equals("Area")) {
      adaptee.tfArea_focusLost();
    }
    else if ( ( (TextField) evento.getSource()).getName().equals("Distrito")) {
      adaptee.tfDistrito_focusLost();
    }
    else if ( ( (TextField) evento.getSource()).getName().equals("Anno")) {
      adaptee.tfAnno_focusLost();
    }
  }

} // EscuchadorPerdidaFoco

/**
 * Escuchador para el botones del panel.
 * @author   JRM
 * @version  12/06/2000
 */
class EscuchadorBoton
    implements java.awt.event.ActionListener, Runnable {
  PanelArDiAn adaptee;
  ActionEvent evento;

  /**
   * Se inicializa el escuchador al panel.
   */
  EscuchadorBoton(PanelArDiAn adaptee) {
    this.adaptee = adaptee;
  }

  /**
   * Localizamos el click sobre el bot�n de �reas
   */
  public void actionPerformed(ActionEvent accion) {
    evento = accion;
    Thread th = new Thread(this);
    th.start();
  }

  /**
   * Hilo para la ejecuci�n de los eventos click.
   */
  public void run() {
    String boton;

    boton = evento.getActionCommand();
    if (boton.equals("Area")) {
      adaptee.Click_Area(evento);
    }
    else if (boton.equals("Distrito")) {
      adaptee.Click_Distrito(evento);
    }
  }

} // EscuchadorBotonArea

/**
 * Para las pulsaciones de teclas sobre los txtfields s�lo �rea y distrito
 */
class EscuchadorKeyPress
    extends java.awt.event.KeyAdapter {
  PanelArDiAn adaptee;

  /**
   * Contructor, inicializaci�n del panel.
   */
  EscuchadorKeyPress(PanelArDiAn adaptee) {
    this.adaptee = adaptee;
  }

  /**
   * Pulsaci�n de teclado sobre el control
   */
  public void keyPressed(KeyEvent e) {
    if ( ( (TextField) e.getSource()).getName().equals("Area")) {
      adaptee.tfArea_keyPressed();
    }
    else if ( ( (TextField) e.getSource()).getName().equals("Distrito")) {
      adaptee.tfDistrito_keyPressed();
    }
  }
} // EscuchadorKeyPress

class PanelArDiAn_tfDistrito_actionAdapter
    implements java.awt.event.ActionListener {
  PanelArDiAn adaptee;

  PanelArDiAn_tfDistrito_actionAdapter(PanelArDiAn adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.tfDistrito_actionPerformed(e);
  }
}
