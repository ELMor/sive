package capp;

import com.borland.jbcl.control.ButtonControl;

/**
 * Bot�n con identificaci�n para la gesti�n de USU.
 * Sobreescribe el m�todo setEnable para encapsular las autorizaciones
 * sobre las acciones de la aplicaci�n.
 * Nota: si no se indica una clave, no se tiene en cuenta USU
 *
 * @autor LSR
 * @version 1.0.1
 */
public class USUButtonControl
    extends ButtonControl {
  private String key = null;
  private CApp app = null;

  public USUButtonControl(String k, CApp a) {
    super();
    key = k;
    app = a;
    if (!app.isAllowed(key)) {
      super.setEnabled(false);
    }
  }

  public void setEnabled(boolean enabled) {
    if (app.isAllowed(key)) {
      super.setEnabled(enabled);
    }
    else {
      super.setEnabled(false);
    }
  }

  // Para ser utilizado S�LO en Tuberculosis, debido
  // a la existencia del modo CONSULTA
  // Cualquier otro uso ser� perseguido por todos
  // medios legales
  public void setEnabledForzado(boolean enabled) {
    super.setEnabled(enabled);
  }

}
