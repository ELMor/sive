package capp;

import java.applet.Applet;
import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;

//import sapp2.Data;

public class CApp
    extends Applet {
  private BorderLayout borderLayout = new BorderLayout();
  private Frame elPadre = null;
  protected CTabPanel tabPanel = new CTabPanel();
  protected CTitulo pnlTitulo = null;
  protected boolean isStandalone = false;

  //Para las autorizaciones en menus y botones (USU)
  private DataUSU usuIDs = null;

  //Para obtener fichero de recursos (el objeto se crea en cada appplet)
  public ResourceBundle res;

  // par�metros de configuraci�n
  protected int iIdioma;
  protected int iPerfil;
  protected String sLogin;
  protected String sCA;
  protected String sURL;
  protected String sREF;
  protected String sNivel1;
  protected String sNivel2;
  protected String sNivel3;
  protected String sTSive;
  protected String sIdiomaLocal;
  protected String sAyuda;
  protected String IT_AUTALTA;
  protected String IT_AUTBAJA;
  protected String IT_AUTMOD;
  protected String IT_FG_ENFERMO;
  protected String IT_FG_VALIDAR; // validacion  sive_usuario
  protected String IT_MODULO_3; // m�dulo-3 - CA
  protected String IT_TRAMERO; // CA
  protected String CD_E_NOTIF; // sive_usuario....
  protected String DS_E_NOTIF; // sive_usuario....
  protected String CD_NIVEL_1_EQ;
  protected String CD_NIVEL_2_EQ;
  protected String CD_ZBS_EQ;
  protected String DS_NIVEL_1_EQ;
  protected String DS_NIVEL_2_EQ;
  protected String DS_ZBS_EQ;
  protected String CD_NIVEL_1_AUTORIZACIONES; //iran separados por comas
  protected String CD_NIVEL_2_AUTORIZACIONES; //iran separados por comas
  protected String FC_ACTUAL;
  protected static String CD_NIVEL1_DEFECTO = "";
  protected static String DS_NIVEL1_DEFECTO = "";
  protected static String CD_NIVEL2_DEFECTO = "";
  protected static String DS_NIVEL2_DEFECTO = "";
  protected static String ANYO_DEFECTO = "";
  protected String FTP_SERVER = "";
  protected String IT_FG_RESOLVER_CONFLICTOS = "";
  protected String DS_EMAIL = ""; //EMail usuario

  // idiomas de la aplicaci�n
  final static public int idiomaPORDEFECTO = 0;

  // perfiles de usuario
  static public final int perfilCA = 2;
  static public final int perfilNIVEL1 = 3;
  static public final int perfilNIVEL2 = 4;

  public void init() {
    boolean bZona = true;
    String sZona = null;
    int i = 1;

    StringTokenizer token = null; //Para las autorizaciones

    elPadre = getParentFrame();
    try {
      // recupera los par�metros

      // idioma
      iIdioma = Integer.parseInt(this.getParameter("IDIOMA", "0"));

      // login
      sLogin = this.getParameter("LOGIN", null);

      // URL del servidor web
      sURL = this.getParameter("URL_SERVLET", null);

      // URL del servidor web
      sREF = this.getParameter("URL_HTML", null);

      // descripci�n del nivel 1 sanitario (ya en el idioma seleccionado)
      sNivel1 = this.getParameter("NIVEL1", null);

      // descripci�n del nivel 2 sanitario (ya en el idioma seleccionado)
      sNivel2 = this.getParameter("NIVEL2", null);

      // descripci�n del nivel 2 sanitario (ya en el idioma seleccionado)
      sNivel3 = this.getParameter("NIVEL3", null);

      // comunidad aut�noma
      sCA = this.getParameter("CA", null);

      // servidor ftp
      FTP_SERVER = this.getParameter("FTP_SERVER", "");

      // perfil
      iPerfil = Integer.parseInt(this.getParameter("PERFIL", "-1"));

      //Tipo Sive
      sTSive = this.getParameter("TSIVE", null);

      // idioma local
      sIdiomaLocal = this.getParameter("IDIOMA_LOCAL", "");

      // ayuda de operaci�n
      sAyuda = this.getParameter("AYUDA", null);

      // flags de usuario
      IT_AUTALTA = this.getParameter("IT_AUTALTA", "N");
      IT_AUTBAJA = this.getParameter("IT_AUTBAJA", "N");
      IT_AUTMOD = this.getParameter("IT_AUTMOD", "N");
      IT_FG_ENFERMO = this.getParameter("IT_FG_ENFERMO", "N");
      IT_FG_VALIDAR = this.getParameter("IT_FG_VALIDAR", "N");

      //ca_parametros
      IT_MODULO_3 = this.getParameter("IT_MODULO_3", "N");
      CD_E_NOTIF = this.getParameter("CD_E_NOTIF", "");
      DS_E_NOTIF = this.getParameter("DS_E_NOTIF", "");
      CD_NIVEL_1_EQ = this.getParameter("CD_NIVEL_1_EQ", "");
      CD_NIVEL_2_EQ = this.getParameter("CD_NIVEL_2_EQ", "");
      CD_ZBS_EQ = this.getParameter("CD_ZBS_EQ", "");
      DS_NIVEL_1_EQ = this.getParameter("DS_NIVEL_1_EQ", "");
      DS_NIVEL_2_EQ = this.getParameter("DS_NIVEL_2_EQ", "");
      DS_ZBS_EQ = this.getParameter("DS_ZBS_EQ", "");

      CD_NIVEL1_DEFECTO = this.getParameter("CD_NIVEL1_DEFECTO", "");
      CD_NIVEL2_DEFECTO = this.getParameter("CD_NIVEL2_DEFECTO", "");
      DS_NIVEL1_DEFECTO = this.getParameter("DS_NIVEL1_DEFECTO", "");
      DS_NIVEL2_DEFECTO = this.getParameter("DS_NIVEL2_DEFECTO", "");

      // Tramero
      IT_TRAMERO = this.getParameter("IT_TRAMERO", "N");

      //vectores de autorizaciones //con "N" si no se carga
      CD_NIVEL_1_AUTORIZACIONES = this.getParameter("CD_NIVEL_1_AUTORIZACIONES",
          "N");
      CD_NIVEL_2_AUTORIZACIONES = this.getParameter("CD_NIVEL_2_AUTORIZACIONES",
          "N");

      //fecha actual
      FC_ACTUAL = this.getParameter("FC_ACTUAL", "18/06/1999");

      // A�o por defecto
      ANYO_DEFECTO = this.getParameter("ANYO_DEFECTO", "");

      // Capacidad de resolver conflictos
      IT_FG_RESOLVER_CONFLICTOS = this.getParameter("IT_FG_RESOLVER_CONFLICTOS",
          "");

      DS_EMAIL = this.getParameter("DS_EMAIL", "");

      jbInit();

      // lee los ids autorizados para el usuario
      usuIDs = new DataUSU();
      token = new StringTokenizer(getParametro("IT_USU") + " ", ",");
      while (token.hasMoreElements()) {
        usuIDs.put(token.nextElement().toString().trim(),
                   token.nextElement().toString().trim());
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  } //end init

  // libera todos los componentes
  public void destroy() {
    Component[] comps = this.getComponents();
    for (int i = 0; i < this.getComponentCount(); i++) {
      comps[i] = null;
    }
  }

  // devuelve el frame
  public Frame getFrame() {
    return elPadre;
  }

  //Component initialization
  private void jbInit() throws Exception {
    this.setLayout(borderLayout);
    add("Center", tabPanel);
  } //end jbInit

  final public synchronized void VerPanel(String nombre, CPanel p) {
    VerPanel(nombre, p, false);
  } //end VerPanel

  final public synchronized void VerPanel(String nombre, CPanel p,
                                          boolean modoTab) {
    p.setApp(this);
    if (!modoTab) { // Solo el panel
      // Guardar panel anterio
      if (tabPanel.getNumSolapas() == 1) {
        // Hay un panel solo y se guarda el actual
        CPanel pant = PanelActual();
        if (pant != null) {
          p.panelAnt = PanelActual();
          p.nombrePanelAnt = NombrePanelActual();
        } //endif
      }
      else {
        // Antes habia solapas y se pone anterior a null
        p.panelAnt = null;
        p.nombrePanelAnt = null;
      } //endif
      tabPanel.BorrarSolapa();
      tabPanel.InsertarPanel(nombre, p);
      tabPanel.validate();
    }
    else { // ver con solapas
      tabPanel.InsertarPanel(nombre, p);
      tabPanel.validate();
    } //endif
  } //end VerPanel

  final public void VerPanel(String nombre) {
    tabPanel.VerPanel(nombre);
  } //end BorrarPanel

  final public void BorrarPanel(String nombre) {
    tabPanel.BorrarSolapa(nombre);
  } //end BorrarPanel

  final public String NombrePanelActual() {
    return tabPanel.getSolapaActual();
  } //end NombrePanelActual

  final public CPanel PanelActual() {
    return tabPanel.getPanelActual();
  } //end NombrePanelActual

  final public void VerPanelAnterior() {
    tabPanel.VerPanelAnterior();
  } //end VerPanelAnterior

  final public void addPanelSuperior(Container panel) {
    add("North", panel);
  } //end addPanelSuperior

  final public void addPanelInferior(Container panel) {
    add("South", panel);
  } //end addPanelInferior

  final public void setTitulo(String s) {
    pnlTitulo = new CTitulo(this, s);
    if (sAyuda != null) {
      pnlTitulo.setPaginaAyuda(sAyuda);
    }
    add("North", pnlTitulo);
  }

  final public void setAyuda(String s) {
    if (pnlTitulo != null) {
      pnlTitulo.setPaginaAyuda(s);
    }
  }

  final public void addPanelLateral(Container panel) {
    add("West", panel);
  } //end addPanelLateral

  protected Frame getParentFrame() {
    if (elPadre == null) {
      Component p = this;
      while ( ( (p = p.getParent()) != null) && ! (p instanceof Frame)) {
        ;
      }
      elPadre = (Frame) p;
    } //endif

    return elPadre;
  } //end getParentFrame

//Par�metros

//Get Applet information

  public String getAppletInfo() {
    return "Applet Information";
  }

  public String getParameter(String key, String def) {
    return isStandalone ? System.getProperty(key, def) :
        (getParameter(key) != null ? getParameter(key) : def);
  }

  // recupera el idioma
  public int getIdioma() {
    return iIdioma;
  }

  // recupera el perfil
  public int getPerfil() {
    return iPerfil;
  }

  // devuelve el login
  public String getLogin() {
    return sLogin;
  }

  // devuelve el login
  public String getREF() {
    return sREF;
  }

  // devuelve el login
  public String getFTP_SERVER() {
    return FTP_SERVER;
  }

  // devuelve la descripci�n del nivel 1
  public String getNivel1() {
    return sNivel1;
  }

  // devuelve la descripci�n del nivel 1
  public String getNivel2() {
    return sNivel2;
  }

  // devuelve la descripci�n del nivel 1
  public String getNivel3() {
    return sNivel3;
  }

  // devuelve la URL de los servlets
  public String getURL() {
    return sURL;
  }

  // devuelve la ca
  public String getCA() {
    return sCA;
  }

  // devuelve tsive
  public String getTSive() {
    return sTSive;
  }

  // devuelve idioma local
  public String getIdiomaLocal() {
    return sIdiomaLocal;
  }

  // devuelve permiso alta
  public String getIT_AUTALTA() {
    return IT_AUTALTA;
  }

  // devuelve permiso mod.
  public String getIT_AUTMOD() {
    return IT_AUTMOD;
  }

  // devuelve permiso baja
  public String getIT_AUTBAJA() {
    return IT_AUTBAJA;
  }

  // devuelve permiso enfermo
  public String getIT_FG_ENFERMO() {
    return IT_FG_ENFERMO;
  }

  // devuelve si tiene permiso para validar
  public String getIT_FG_VALIDAR() {
    return IT_FG_VALIDAR;
  }

  // devuelve si su CA es modulo 3
  public String getIT_MODULO_3() {
    return IT_MODULO_3;
  }

  // devuelve  el cod equipo del perfil 5
  public String getCD_E_NOTIF() {
    return CD_E_NOTIF;
  }

  // devuelve  la des del equipo del perfil 5
  public String getDS_E_NOTIF() {
    return DS_E_NOTIF;
  }

  public String getCD_NIVEL_1_EQ() {
    return CD_NIVEL_1_EQ;
  }

  //Direcci�n EMAIL usuario
  public String getDS_EMAIL() {
    return DS_EMAIL;
  }

  public void setCD_NIVEL1_DEFECTO(String cd_n1) {
    CD_NIVEL1_DEFECTO = cd_n1;
  }

  public void setDS_NIVEL1_DEFECTO(String cd_n1) {
    DS_NIVEL1_DEFECTO = cd_n1;
  }

  public void setCD_NIVEL2_DEFECTO(String cd_n1) {
    CD_NIVEL2_DEFECTO = cd_n1;
  }

  public void setDS_NIVEL2_DEFECTO(String cd_n1) {
    DS_NIVEL2_DEFECTO = cd_n1;
  }

  public void setNivel1(String s) {
    sNivel1 = s;
  }

  public void setNivel2(String s) {
    sNivel2 = s;
  }

  public void setLogin(String s) {
    sLogin = s;
  }

  public void setPerfil(int i) {
    iPerfil = i;
  }

  public void setURL(String s) {
    sURL = s;
  }

  public void setTSive(String s) {
    sTSive = s;
  }

  public void setANYO_DEFECTO(String cd_n1) {
    ANYO_DEFECTO = cd_n1;
  }

  public String getCD_NIVEL1_DEFECTO() {
    return CD_NIVEL1_DEFECTO;
  }

  public String getDS_NIVEL1_DEFECTO() {
    return DS_NIVEL1_DEFECTO;
  }

  public String getCD_NIVEL2_DEFECTO() {
    return CD_NIVEL2_DEFECTO;
  }

  public String getDS_NIVEL2_DEFECTO() {
    return DS_NIVEL2_DEFECTO;
  }

  public String getANYO_DEFECTO() {
    return ANYO_DEFECTO;
  }

  public String getCD_NIVEL_2_EQ() {
    return CD_NIVEL_2_EQ;
  }

  public String getCD_ZBS_EQ() {
    return CD_ZBS_EQ;
  }

  public String getDS_NIVEL_1_EQ() {
    return DS_NIVEL_1_EQ;
  }

  public String getDS_NIVEL_2_EQ() {
    return DS_NIVEL_2_EQ;
  }

  public String getDS_ZBS_EQ() {
    return DS_ZBS_EQ;
  }

  // devuelve el tramero
  public String getIT_TRAMERO() {
    return IT_TRAMERO;
  }

  //autorizaciones
  public Vector getCD_NIVEL_1_AUTORIZACIONES() {
    Vector vNIVEL_1 = new Vector();

    StringTokenizer st = new StringTokenizer(CD_NIVEL_1_AUTORIZACIONES, ",");
    while (st.hasMoreTokens()) {
      vNIVEL_1.addElement(st.nextToken());
    }
    return vNIVEL_1;
  }

  public Vector getCD_NIVEL_2_AUTORIZACIONES() {
    Vector vNIVEL_2 = new Vector();

    StringTokenizer st = new StringTokenizer(CD_NIVEL_2_AUTORIZACIONES, ",");
    while (st.hasMoreTokens()) {
      vNIVEL_2.addElement(st.nextToken());
    }
    return vNIVEL_2;
  }

  public void setCD_NIVEL_1_AUTORIZACIONES(String s) {
    CD_NIVEL_1_AUTORIZACIONES = s;
  }

  public void setCD_NIVEL_2_AUTORIZACIONES(String s) {
    CD_NIVEL_2_AUTORIZACIONES = s;
  }

  // Para construir la cadena de par�metros del applet.
  protected String getStrCD_NIVEL_1_AUTORIZACIONES() {
    return CD_NIVEL_1_AUTORIZACIONES;
  }

  protected String getStrCD_NIVEL_2_AUTORIZACIONES() {
    return CD_NIVEL_2_AUTORIZACIONES;
  }

  //fecha actual
  public String getFC_ACTUAL() {
    return FC_ACTUAL;
  }

  // Capacidad para resolver conflictos
  public String getStrIT_FG_RESOLVER_CONFLICTOS() {
    return IT_FG_RESOLVER_CONFLICTOS;
  }

  public void setStrIT_FG_RESOLVER_CONFLICTOS(String s) {
    IT_FG_RESOLVER_CONFLICTOS = s;
  }

  //____________ AUTORIZACIONES CON EL USU____________________

  /**
   * Indica si la clave existe en las claves autorizadas del usuario
   *
   * @param key id de la acci�n
   * @return indicador
   */
  public boolean isAccesible(String key) {
    boolean accesible = false;
    String name = null;

    for (Enumeration enum = usuIDs.keys(); enum.hasMoreElements(); ) {
      name = (String) enum.nextElement();
      if (key.equals(name)) {
        accesible = true;
        break;
      }
    }

    return accesible;
  }

  /**
   * Devuelve verdadero/falso dependiendo de si el c�digo de la
   * acci�n est� permitido (existe y no est� cerrada) o no lo est�
   *
   * Nota: si no se indica una clave, no se tiene en cuenta USU
   * @param key id de la acci�n
   * @return indicador
   */
  public boolean isAllowed(String key) {
    boolean allowed = false;
    String name = null;

    if (key.length() > 0) {
      for (Enumeration enum = usuIDs.keys(); enum.hasMoreElements(); ) {
        name = (String) enum.nextElement();
        if (key.equals(name)) {
          if (usuIDs.getString(name).length() == 0) {
            allowed = true;
          }
          break;
        }
      }
    }
    else {
      allowed = true;
    }

    return allowed;
  }

  //____________________________________________________________
  //PARA COMPTIBILIDAD CON FORMA LECTURA PARAMETROS EN CLASES NUEVAS
  /**
   * Lee el par�metro requerido. Si no existe devuelve un string vacio
   *
   * @param nombre nombre del par�metro
   * @return par�metros
   */
  public String getParametro(String name) {
    String param = getParameter(name);
    if (param == null) {
      param = "";
    }
    return param;
  }

} //endclass CApp