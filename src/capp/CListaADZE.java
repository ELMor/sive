//Title:        Casos o Tasas por 100000 habitantes por Area
//Version:
//Copyright:    Copyright (c) 1998
//Author:
//Company:
//Description:  Consultas 9.3.10, 9.3.11 y 9.3.12
//Actualizaci�n: 28/02/2001- Se ha a�adido un nuevo modo para obtener una lista
//               de todas las enfermedades EDO.

package capp;

import java.net.URL;
import java.util.Hashtable;
import java.util.Vector;

import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.TextEvent;

import alarmas.DataEnferedo;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
import zbs.DataZBS;
import zbs.DataZBS2;

//import comun.CFechaSimple; import comun.CHora; import comun.CListaNiveles1; import comun.Comunicador; import comun.constantes; import comun.Data; import comun.DataAutoriza; import comun.DataCasIn; import comun.DataEnferedo; import comun.DataEnfermo; import comun.DataEntradaEDO; import comun.DataEqNot; import comun.DataGeneralCDDS; import comun.DataIndivEnfermedad; import comun.DataLongValid; import comun.DataMun; import comun.DataMunicipioEDO; import comun.DataNivel1; import comun.DataNotifEDO; import comun.DataNotifSem; import comun.DataRegistroEDO; import comun.DataValoresEDO; import comun.DataZBS; import comun.DataZBS2; import comun.DatoBase; import comun.datosParte; import comun.datosPreg; import comun.DialogoGeneral; import comun.DialogRegistroEDO; import comun.DialogSelDomi; import comun.DialogSelEnfermo; import comun.Fechas; import comun.IntContenedor; import comun.PanAnoSemFech; import comun.SrvDebugGeneral; import comun.SrvDlgBuscarRegistro; import comun.SrvEnfermedad; import comun.SrvEntradaEDO; import comun.SrvMaestroEDO; import comun.SrvMun; import comun.SrvMunicipioCont; import comun.SrvNivel1; import comun.SrvZBS2; import comun.UtilEDO; import comun.comun;

public class CListaADZE
    extends CDialog {
  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

  protected StubSrvBD stubCliente = new StubSrvBD();

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  final int modoESPERA = 2;

  protected int modoOperacion = modoNORMAL;

  protected boolean resultado = false;
  protected CLista listaRes = null;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETEnf = "servlet/SrvEnfermedad";
  //AIC
  final String srtSERVLETEnfTodas = "servlet/SrvEnferedo";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;
  final int servletSELECCION_ALARMAS_X_CODIGO = 300;

  // modos de la lista
  final int AREA = 12;
  final int DISTRITO = 13;
  final int ZBS = 14;
  final int ENFERMEDAD = 15;
  //AIC
  final int ENFERMEDADTODAS = 16;

  int modoLista = 0;

  String parteTit = "";

  int selecciones = 0;

  CCargadorImagen imgs = null;

  String tSive = new String();

  XYLayout xYLayout = new XYLayout();

  java.awt.List lista = null;
  CLista listaServlet = null;
  CLista listaIni = null;

  ButtonControl btnAgnadir = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  CheckboxGroup checkboxAgrupar = new CheckboxGroup();
  Label lblElegir = new Label();

  focusAdapterPMP txtFocusAdapter = new focusAdapterPMP(this);
  textAdapterADZ txtTextAdapter = new textAdapterADZ(this);
  actionListenerPMP btnActionListener = new actionListenerPMP(this);
  ListaItemListener lstItemListener = new ListaItemListener(this);

  public CListaADZE(CApp a, int ae, int col, CLista lisIni) {
    //para rellenar la lista con areas

    super(a);
    try {

      lista = new java.awt.List();
      listaServlet = new CLista();
      modoLista = AREA;
      parteTit = " �reas ";
      selecciones = col;

      listaIni = new CLista();
      listaIni = lisIni;

      if (ae == AREA) {
        modoLista = AREA;
        parteTit = " �reas ";
      }
      else {
        if (ae == ENFERMEDAD || ae == ENFERMEDADTODAS) {
          modoLista = ae;
          parteTit = " enfermedades ";
        }
      }

      jbInit();

      rellenarLista("", "");

      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public CListaADZE(CApp a, String area, int col, CLista lisIni) {
    //para rellenar la lista con distritos

    super(a);
    try {

      lista = new java.awt.List();
      listaServlet = new CLista();

      modoLista = DISTRITO;
      parteTit = " distritos ";

      listaIni = new CLista();
      listaIni = lisIni;

      selecciones = col;
      jbInit();

      rellenarLista(area, "");

      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public CListaADZE(CApp a, String area, String distrito, int col,
                    CLista lisIni) {
    //para rellenar la lista con zbs

    super(a);
    try {

      lista = new java.awt.List();
      listaServlet = new CLista();

      modoLista = ZBS;
      parteTit = " zonas b�sicas de salud ";
      selecciones = col;

      listaIni = new CLista();
      listaIni = lisIni;

      jbInit();

      rellenarLista(area, distrito);

      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    this.setTitle("Seleccion de " + parteTit);

    imgs = new CCargadorImagen(getCApp(), imgNAME);
    imgs.CargaImagenes();

    btnAgnadir.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));

    //lista
    lista.setName("lista");
    lista.setMultipleMode(true);
    lista.addItemListener(lstItemListener);

    xYLayout.setHeight(301);
    btnAgnadir.setLabel("Aceptar");
    btnCancelar.setLabel("Cancelar");
    // gestores de eventos

    btnAgnadir.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);

    btnAgnadir.setActionCommand("generar");
    btnCancelar.setActionCommand("cancelar");

    lblElegir.setText("Seleccione " + selecciones + parteTit + " como m�ximo:");

    xYLayout.setWidth(375);
    xYLayout.setHeight(250); ;
    this.setLayout(xYLayout);
    this.setSize(new Dimension(375, 250));

    this.add(lblElegir, new XYConstraints(35, 20, 266, -1));
    this.add(lista, new XYConstraints(35, 55, 305, 102));
    this.add(btnAgnadir, new XYConstraints(165, 175, -1, -1));
    this.add(btnCancelar, new XYConstraints(256, 175, -1, -1));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    CMessage msgBox;
    boolean bDatosCompletos = true;

    int sels[] = lista.getSelectedIndexes();
    // System_out.println("num indices seleccionados " + sels.length);
    // System_out.println("posibles seleccionados " + selecciones);
    if (sels.length > selecciones) {
      bDatosCompletos = false;
      msgBox = new CMessage(this.getCApp(), CMessage.msgADVERTENCIA,
                            "Solo se pueden seleccionar " + selecciones +
                            parteTit);
      msgBox.show();
      msgBox = null;
    }
    if (sels.length < 1) {
      bDatosCompletos = false;
      msgBox = new CMessage(this.getCApp(), CMessage.msgADVERTENCIA,
                            "Al menos se debe seleccionar un " +
                            parteTit.substring(0, parteTit.length() - 1));
      msgBox.show();
      msgBox = null;
    }
    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        btnAgnadir.setEnabled(true);
        btnCancelar.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnAgnadir.setEnabled(false);
        btnCancelar.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  void btnCancelar_actionPerformed(ActionEvent evt) {
    this.resultado = false;
    this.dispose();
  }

  /** cambios en la lista  */
  void lista_itemStateChanged(ItemEvent evt) {
  }

  void seleccionInicial(CLista l) {

    Vector vElems = new Vector();
    String elems[] = lista.getItems();
    if (elems.length > 0) {
      for (int v = 0; v < elems.length; v++) {
        vElems.addElement(elems[v]);
      }
      Hashtable elem = new Hashtable();
      String sel = "";
      for (int j = 0; j < l.size(); j++) {
        elem = (Hashtable) l.elementAt(j);
        switch (modoLista) {
          case AREA:

            // Se a�ade la descrpci�n bien sea en  idioma local o poer defecto //???????????????????
            if ( (app.getIdioma() != CApp.idiomaPORDEFECTO) &&
                ( ( (String) elem.get("DSL_NIVEL_1")).length() > 0)) {
              sel = (String) elem.get("DSL_NIVEL_1");
            }
            else {
              sel = (String) elem.get("DS_NIVEL_1");
            }
            break;
          case DISTRITO:
            sel = (String) elem.get("DS_NIVEL_2");
            // Se a�ade la descrpci�n bien sea en  idioma local o poer defecto //???????????????????
            /*
                           if ((  app.getIdioma() != CApp.idiomaPORDEFECTO) && ( n2.getDesL().length() > 0)){
              l.add(n2.getDes);
                           }else{
              l.add( n2.getDes() );
                           }
             */
            break;
          case ZBS:

            // Se a�ade la descrpci�n bien sea en  idioma local o poer defecto //???????????????????
            if ( (app.getIdioma() != CApp.idiomaPORDEFECTO) &&
                ( ( (String) elem.get("DSL_ZBS")).length() > 0)) {
              sel = (String) elem.get("DSL_ZBS");
            }
            else {
              sel = (String) elem.get("DS_ZBS");
            }
            break;
            //AIC
          case ENFERMEDADTODAS:
          case ENFERMEDAD:
            sel = (String) elem.get("DS_PROCESO");
            // Se a�ade la descrpci�n bien sea en  idioma local o poer defecto //???????????????????
            /*
                           if ((  app.getIdioma() != CApp.idiomaPORDEFECTO) && ( enf.getDesL().length() > 0)){
              l.add(enf.getDesL());
                           }else{
              l.add(enf.getDes() );
                           }
             */
            break;
        }
        lista.select(vElems.indexOf(sel));
      }
    }
  }

  void recogerSelecciones() {
    int indices[] = lista.getSelectedIndexes();
    Hashtable elem = new Hashtable();
    DataNivel1 n1 = null;
    DataZBS2 n2 = null;
    DataZBS zbs = null;
    DataEnferedo enf = null;

    listaRes = new CLista();
    // rellenamos los c�digo de esos indices
    for (int j = 0; (j < indices.length); j++) {
      elem = new Hashtable();
      switch (modoLista) {
        case AREA:
          n1 = (DataNivel1) listaServlet.elementAt(indices[j]);
          elem.put("CD_NIVEL_1", n1.getCod());
          // Se a�ade la descrpci�n bien sea en  idioma local o poer defecto //???????????????????
          if ( (app.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (n1.getDesL().length() > 0)) {
            elem.put("DS_NIVEL_1", n1.getDesL());
          }
          else {
            elem.put("DSL_NIVEL_1", n1.getDes());
          }
          break;
        case DISTRITO:
          n2 = (DataZBS2) listaServlet.elementAt(indices[j]);
          elem.put("CD_NIVEL_2", n2.getNiv2());
          // Se a�ade la descrpci�n bien sea en  idioma local o poer defecto //???????????????????
          elem.put("DS_NIVEL_2", n2.getDes());
          /*
                     if ((  app.getIdioma() != CApp.idiomaPORDEFECTO) && ( n2.getDesL().length() > 0)){
            elem.put("DS_NIVEL_2", n2.getDesL());
                     }else{
            elem.put("DSL_NIVEL_2", n2.getDes());
                     }
           */
          break;
        case ZBS:
          zbs = (DataZBS) listaServlet.elementAt(indices[j]);
          elem.put("CD_ZBS", zbs.getCod());
          // Se a�ade la descrpci�n bien sea en  idioma local o poer defecto //???????????????????
          if ( (app.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (zbs.getDesL().length() > 0)) {
            elem.put("DS_ZBS", zbs.getDesL());
          }
          else {
            elem.put("DSL_ZBS", zbs.getDes());
          }
          break;
          //AIC
        case ENFERMEDADTODAS:
        case ENFERMEDAD:
          enf = (DataEnferedo) listaServlet.elementAt(indices[j]);

          elem.put("CD_ENFCIE", enf.getCod());
          elem.put("DS_PROCESO", enf.getDes());
          // Se a�ade la descrpci�n bien sea en  idioma local o poer defecto //???????????????????
          /*
                     if ((  app.getIdioma() != CApp.idiomaPORDEFECTO) && ( enf.getDesL().length() > 0)){
            elem.put("DS_PROCESO", enf.getDesL());
                     }else{
            elem.put("DSL_PROCESO", enf.getDes());
                     }
           */
          break;
      }
      listaRes.addElement(elem);
    }

  }

  void rellenarLista(String area, String distrito) {
    CMessage msgBox = null;

    //Segun los datos del constructor, la lista se rellena
    //con areas, distritos, zbs o enfermedades I A

    switch (modoLista) {
      case AREA:
        buscaAreas();
        break;
      case DISTRITO:
        buscaDistritos(area);
        break;
      case ZBS:
        buscaZBS(area, distrito);
        break;
        //AIC
      case ENFERMEDADTODAS:
        buscaEnfermedadEdo();
        break;

      case ENFERMEDAD:
        buscaEnfermedad();
        break;
    }

    if (listaIni.size() > 0) {
      if (listaIni.size() <= selecciones) {
        seleccionInicial(listaIni);
      }
      else {
        msgBox = new CMessage(this.getCApp(), CMessage.msgADVERTENCIA, "La selecci�n inicial es mayor que el n�mero de elementos que est� permitido seleccionar");
        msgBox.show();
        msgBox = null;
      }
    }

  }

  void buscaAreas() {
    CMessage msgBox = null;
    CLista data = null;
    CLista resul = null;

    DataNivel1 nivel1 = null;

    try {

      // obtiene y pinta la lista nueva
      data = new CLista();

      // idioma
      data.setIdioma(app.getIdioma());

      // perfil
      data.setPerfil(app.getPerfil());

      // login
      data.setLogin(app.getLogin());

      nivel1 = new DataNivel1();

      data.addElement(nivel1);

      stubCliente.setUrl(new URL(app.getURL() + strSERVLETNivel1));
      resul = (CLista) stubCliente.doPost(servletSELECCION_X_CODIGO, data);
      data = null;

      // comprueba que hay datos
      if (resul.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              "No se encontraron datos.");
        msgBox.show();
        msgBox = null;
      }
      else {
        copiaLista(resul, lista);
      }

      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
  }

  void buscaDistritos(String area) {
    CMessage msgBox = null;
    CLista data = null;
    CLista resul = null;

    DataZBS2 nivel2 = null;

    try {

      // obtiene y pinta la lista nueva
      data = new CLista();

      // idioma
      data.setIdioma(app.getIdioma());

      // perfil
      data.setPerfil(app.getPerfil());

      // login
      data.setLogin(app.getLogin());

      nivel2 = new DataZBS2(area, "", "", "");

      data.addElement(nivel2);

      stubCliente.setUrl(new URL(app.getURL() + strSERVLETNivel2));
      resul = (CLista) stubCliente.doPost(servletSELECCION_NIV2_X_CODIGO, data);
      data = null;

      // comprueba que hay datos
      if (resul.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              "No se encontraron datos.");
        msgBox.show();
        msgBox = null;
      }
      else {
        copiaLista(resul, lista);
      }

      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
  }

  void buscaZBS(String area, String distrito) {
    CMessage msgBox = null;
    CLista data = null;
    CLista resul = null;

    DataZBS zbs = null;

    try {

      // obtiene y pinta la lista nueva
      data = new CLista();

      // idioma
      data.setIdioma(app.getIdioma());

      // perfil
      data.setPerfil(app.getPerfil());

      // login
      data.setLogin(app.getLogin());

      zbs = new DataZBS("", "", "", area, distrito);

      data.addElement(zbs);

      stubCliente.setUrl(new URL(app.getURL() + strSERVLETZona));
      resul = (CLista) stubCliente.doPost(servletSELECCION_NIV2_X_CODIGO, data);
      data = null;

      // comprueba que hay datos
      if (resul.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              "No se encontraron datos.");
        msgBox.show();
        msgBox = null;
      }
      else {
        copiaLista(resul, lista);
      }

      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
  }

  void buscaEnfermedad() {
    CMessage msgBox = null;
    CLista data = null;
    CLista resul = null;

    DataEnferedo enf = null;

    try {

      // obtiene y pinta la lista nueva
      data = new CLista();

      // idioma
      data.setIdioma(app.getIdioma());

      // perfil
      data.setPerfil(app.getPerfil());

      // login
      data.setLogin(app.getLogin());

      enf = new DataEnferedo();

      data.addElement(enf);

      stubCliente.setUrl(new URL(app.getURL() + strSERVLETEnf));
      resul = (CLista) stubCliente.doPost(servletSELECCION_ALARMAS_X_CODIGO,
                                          data);
      data = null;

      // comprueba que hay datos
      if (resul.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              "No se encontraron datos.");
        msgBox.show();
        msgBox = null;
      }
      else {
        copiaLista(resul, lista);
      }

      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
  }

//AIC
  void buscaEnfermedadEdo() {
    CMessage msgBox = null;
    CLista data = null;
    CLista resul = null;

    DataEnferedo enf = null;

    try {

      // obtiene y pinta la lista nueva
      data = new CLista();

      // idioma
      data.setIdioma(app.getIdioma());

      // perfil
      data.setPerfil(app.getPerfil());

      // login
      data.setLogin(app.getLogin());

      enf = new DataEnferedo();

      data.addElement(enf);

      stubCliente.setUrl(new URL(app.getURL() + srtSERVLETEnfTodas));
      resul = (CLista) stubCliente.doPost(servletSELECCION_X_CODIGO, data);
      data = null;

      // comprueba que hay datos
      if (resul.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              "No se encontraron datos.");
        msgBox.show();
        msgBox = null;
      }
      else {
        copiaLista(resul, lista);
      }

      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
  }

  // a�ade los datos de CLista  lista al control l
  void copiaLista(CLista lista, java.awt.List l) {
    CMessage msgBox;
    CLista data;

    DataNivel1 n1 = null;
    DataZBS2 n2 = null;
    DataZBS zbs = null;
    DataEnferedo enf = null;

    listaServlet = lista;

    // vacia el contenido
    l.removeAll();

    data = lista;

    // agrega lso items
    if (data.size() > 0) {

      // vuelca la lista, dependiendo de su tipo
      for (int j = 0; j < data.size(); ++j) {
        if (l.getName() == "lista") {

          switch (modoLista) {
            case AREA:
              n1 = (DataNivel1) data.elementAt(j);

              // Se a�ade la descrpci�n bien sea en  idioma local o poer defecto //???????????????????
              if ( (app.getIdioma() != CApp.idiomaPORDEFECTO) &&
                  (n1.getDesL().length() > 0)) {
                l.add(n1.getDesL());
              }
              else {
                l.add(n1.getDes());
              }
              break;
            case DISTRITO:
              n2 = (DataZBS2) data.elementAt(j);
              l.add(n2.getDes());

              // Se a�ade la descrpci�n bien sea en  idioma local o poer defecto //???????????????????
              /*
                             if ((  app.getIdioma() != CApp.idiomaPORDEFECTO) && ( n2.getDesL().length() > 0)){
                l.add(n2.getDes);
                             }else{
                l.add( n2.getDes() );
                             }
               */
              break;
            case ZBS:
              zbs = (DataZBS) data.elementAt(j);

              // Se a�ade la descrpci�n bien sea en  idioma local o poer defecto //???????????????????
              if ( (app.getIdioma() != CApp.idiomaPORDEFECTO) &&
                  (zbs.getDesL().length() > 0)) {
                l.add(zbs.getDesL());
              }
              else {
                l.add(zbs.getDes());
              }
              break;
              //AIC
            case ENFERMEDADTODAS:
            case ENFERMEDAD:
              enf = (DataEnferedo) data.elementAt(j);
              l.add(enf.getDes());
              // Se a�ade la descrpci�n bien sea en  idioma local o poer defecto //???????????????????
              /*
                             if ((  app.getIdioma() != CApp.idiomaPORDEFECTO) && ( enf.getDesL().length() > 0)){
                l.add(enf.getDesL());
                             }else{
                l.add(enf.getDes() );
                             }
               */
              break;
          }

        }
      }

      // opci�n m�s datos
      if (data.getState() == CLista.listaINCOMPLETA) {
        l.add("M�s ...");
      }

      // mensaje de lista vacia
    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            "No se encontraron datos.");
      msgBox.show();
      msgBox = null;
    }
  }

  void btnAgnadir_actionPerformed(ActionEvent evt) {

    CMessage msgBox;

    if (isDataValid()) {
      this.resultado = true;
      recogerSelecciones();
      dispose();
    }
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {
    /*
      CLista param = null;
      CMessage msg;
      String strServlet = null;
      int modoServlet = 0;
      TextField txt = (TextField) e.getSource();
      // gestion de datos
      if ((txt.getName().equals("txtModelo"))
           && (txtModelo.getText().length() > 0) ) {
        //#System_Out.println("Pierde el foco el textModelo");
        param = new CLista();
        DataModPreg aux = new DataModPreg(txtModelo.getText(), "",
                                          "","", tSive);
        aux.modo=MODELO;
        param.addElement(aux);
        strServlet = strSERVLETModYPreg;
        modoServlet = servletOBTENER_X_CODIGO;
      }
      // sera distinto si se ha puesto antes un modelo o no
      // Por lo menos parece que se van a usar servlets distintos
      else if ((txt.getName().equals("txtCodPreg"))
                && (txtCodPreg.getText().length() > 0)) {
             if (!txtModelo.getText().equals("")) {
                // Servlet que devuelva la pregunta dado un modelo
                //#System_Out.println("Pierde el foco el textCodPreg");
                param = new CLista();
                DataModPreg aux = new DataModPreg(txtModelo.getText(),"",
         txtCodPreg.getText(),"", tSive);
                aux.modo=MOD_Y_PREG;
                param.addElement(aux);
                strServlet = strSERVLETModYPreg;
                modoServlet = servletOBTENER_X_CODIGO;
             }
             else {
                // Servlet que devuelva la pregunta sim un modelo concreto
                //#System_Out.println("Pierde el foco el textCodPreg");
                param = new CLista();
                DataModPreg aux = new DataModPreg("", "",
         txtCodPreg.getText(),"", tSive);
                aux.modo=PREGUNTA;
                param.addElement(aux);
                strServlet = strSERVLETModYPreg;
                modoServlet = servletOBTENER_X_CODIGO;
             }
           }
      // busca el item
      if (param != null) {
        try {
          // consulta en modo espera
          modoOperacion = modoESPERA;
          Inicializar();
          stubCliente.setUrl(new URL(getCApp().getURL() + strServlet));
          param = (CLista) stubCliente.doPost(modoServlet, param);
          // rellena los datos
          if (param.size() > 0) {
            // realiza el cast y rellena los datos
            if (txt.getName().equals("txtModelo")) {
              //#System_Out.println("realiza el cast y rellena los datos del modulo");
              dModPreg = (DataModPreg) param.firstElement();
              txtModelo.removeTextListener(txtTextAdapter);
              txtModelo.setText(dModPreg.codMod);
              txtDesMod.setText(dModPreg.desMod);
              txtModelo.addTextListener(txtTextAdapter);
            }
            else if (txt.getName().equals("txtCodPreg")) {
                  //#System_Out.println("realiza el cast y rellena los datos de preg");
                  dModPreg = (DataModPreg) param.firstElement();
                  txtCodPreg.removeTextListener(txtTextAdapter);
                  txtCodPreg.setText(dModPreg.codPreg);
                  txtDesPreg.setText(dModPreg.desPreg);
                  txtCodPreg.addTextListener(txtTextAdapter);
                 }
          }
          // no hay datos
          else {
         msg = new CMessage(this.getCApp(), CMessage.msgAVISO, "No hay datos.");
            msg.show();
            msg = null;
          }
        } catch(Exception ex) {
         msg = new CMessage(this.getCApp(), CMessage.msgERROR, ex.toString());
            msg.show();
            msg = null;
        }
        // consulta en modo normal
        modoOperacion = modoNORMAL;
        Inicializar();
      }
     */
  }

  // cambio en una caja de texto
  void textValueChanged(TextEvent e) {
    /*
         TextField txt = (TextField) e.getSource();
         // gestion de datos
         if ( (txt.getName().equals("txtModelo"))
         && (txtDesMod.getText().length() > 0)) {
      txtModelo.setText("");
      txtDesMod.setText("");
         }
         else if ( (txt.getName().equals("txtCodPreg"))
              && (txtDesPreg.getText().length() > 0)) {
      txtCodPreg.setText("");
      txtDesPreg.setText("");
         }
         Inicializar();
     */
  }

  public boolean resultado() {
    return resultado;
  }

  public CLista listaRes() {
    return listaRes;
  }

} //clase

// action listener para los botones

class actionListenerPMP
    implements ActionListener, Runnable {
  CListaADZE adaptee = null;
  ActionEvent e = null;

  public actionListenerPMP(CListaADZE adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("generar")) {
      adaptee.btnAgnadir_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapterPMP
    extends java.awt.event.FocusAdapter
    implements Runnable {
  CListaADZE adaptee;
  FocusEvent event;

  focusAdapterPMP(CListaADZE adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

// cambio en una caja de codigos
class textAdapterADZ
    implements java.awt.event.TextListener {
  CListaADZE adaptee;

  textAdapterADZ(CListaADZE adaptee) {
    this.adaptee = adaptee;
  }

  public void textValueChanged(TextEvent e) {
    adaptee.textValueChanged(e);
  }
}

////////////////////// Clases para listas

//operaciones sobre la lista
class ListaItemListener
    implements ItemListener, Runnable {
  CListaADZE adaptee = null;
  ItemEvent e = null;

  public ListaItemListener(CListaADZE adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(ItemEvent e) {
    if (e.getStateChange() == ItemEvent.SELECTED) {
      this.e = e;

      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
  }
}
