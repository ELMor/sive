package capp;

import java.applet.Applet;
import java.util.Vector;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;

abstract public class CPanelButton
    extends Panel {

  protected CCargadorImagen imgs = null;
  protected Applet applet = null;
  protected int actual = -1;
  public Vector buttons;
  protected CPanelButtonbtnNormalActionListener actionListener;

  public int getCurrent() {
    return actual;
  }

  public void setCurrent(int i) {
    ButtonControl btn;

    if (actual != -1) {
      btn = (ButtonControl) buttons.elementAt(actual);
      btn.setSelected(false);
    }
    btn = (ButtonControl) buttons.elementAt(i);
    btn.setSelected(true);
    actual = i;
  }

  public void setEnabled(boolean s) {
    for (int i = 0; i < buttons.size(); i++) {
      ( (ButtonControl) buttons.elementAt(i)).setEnabled(s);
    }
    setCurrent(actual);
  }

  public CPanelButton(CApp a, String sImages[], String sLabels[]) {

    try {
      applet = (Applet) a;
      imgs = new CCargadorImagen(a, sImages);
      imgs.CargaImagenes();
      setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
      actionListener = new CPanelButtonbtnNormalActionListener(this);
      buttons = new Vector();

      for (int i = 0; i < sImages.length; i++) {
        actual = 0;
        addImageButton(imgs.getImage(i), sLabels[i], Integer.toString(i));
      }

      if (actual != -1) {
        setCurrent(actual);

      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  protected Component addImageButton(Image image, String label, String command) {
    ButtonControl button = new ButtonControl(image);

    if (label != null) {
      button.setLabel(label);
    }
    else {
      button.setName(command);
    }
    button.setActionCommand(command);
    button.addActionListener(actionListener);
    button.setFocusAware(false);
    button.setImageFirst(true);
    button.setVisible(true);
    buttons.addElement(button);

    return add(button);
  }

  // ejecuta el proceso del bot�n pulsado
  public abstract void btnNormalActionPerformed(String s);

}

// action listener de evento en botones normales( No implementa Runnable)
class CPanelButtonbtnNormalActionListener
    implements ActionListener {
  CPanelButton adaptee = null;

  public CPanelButtonbtnNormalActionListener(CPanelButton adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    adaptee.setCurrent(Integer.parseInt(e.getActionCommand()));
    adaptee.btnNormalActionPerformed(e.getActionCommand());
  }
}
