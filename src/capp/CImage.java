package capp;

import java.net.URL;

import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Panel;
import java.awt.Toolkit;

public class CImage
    extends Panel {
  protected Image image = null;
  protected MediaTracker tracker = null;
  protected int width;
  protected int height;

  public CImage(URL address, int w, int h) {
    super(new FlowLayout());
    width = w;
    height = h;
    image = Toolkit.getDefaultToolkit().getImage(address);
    tracker = new MediaTracker(this);
    tracker.addImage(image, 0);
    try {
      tracker.waitForID(0);
      this.setSize(width, height);
    }
    catch (InterruptedException e) {
      image = null;
      setSize(0, 0);
    }
  }

  public void paint(Graphics g) {
    Graphics g2;

    if (image != null) {
      g2 = g.create();
      g2.clipRect(0, 0, width, height);
      g2.drawImage(image, 0, 0, width, height, this);
      g2.dispose();
    }
  }

  public Image getImage() {
    return (image);
  }

  // actualizaciones
  public void update(Graphics g) {
    paint(g);
  }

}