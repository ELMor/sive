
package capp;

import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Hashtable;

/**
     * Clase b�sica de datos . Necesaria para que gesti�n del USU se pueda codificar
 * de la misma forma en capp.CApp que en clase capp2.CApp
 *
 * @autor LSR
 * @version 1.0
 */
public class DataUSU
    extends Hashtable {

  /**
   * Graba un valor int como un Integer
   *
   * @return valor
   */
  public void put(String key, int value) {
    put(key, new Integer(value));
  }

  /**
   * Devuelve una cadena. Si no esxiste el valor devuelve ""
   *
   * @return valor
   */
  public String getString(String key) {
    String sValue = (String) get(key);
    if (sValue == null) {
      sValue = new String("");
    }
    return sValue;
  }

  /**
   * Devuelve una cadena con el formato nombre=valor&...
   *
   * @return cadena URL
   */
  public String toURL() {
    Enumeration enum = null;
    String url = "";
    String key = null;

    for (enum = keys(); enum.hasMoreElements(); ) {
      key = (String) enum.nextElement();

      if (url.length() > 0) {
        url += "&";
      }

      url += key + "=" + URLEncoder.encode(getString(key));
    }

    return url;
  }

  public Object clone() {
    DataUSU dtResul = new DataUSU();
    Enumeration ek = null;
    Enumeration ev = null;

    for (ek = keys(), ev = elements(); ek.hasMoreElements(); ) {
      dtResul.put( (String) ek.nextElement(), (Object) ev.nextElement());

    }
    return dtResul;
  }

}