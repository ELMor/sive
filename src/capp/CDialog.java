package capp;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;

public class CDialog
    extends Dialog {
  protected CApp app = null;

  public CDialog(CApp a) {
    super(a.getParentFrame(), "", true);

    app = a;

    setLayout(new BorderLayout());
    setResizable(false);

    this.addWindowListener(new java.awt.event.WindowAdapter() {
      public void windowClosed(WindowEvent e) {
        this_windowClosed(e);
      }

      public void windowClosing(WindowEvent e) {
        this_windowClosing(e);
      }
    });
  } //end CDialog

  public void show() {
    if (app != null) {
      app.getParentFrame().setEnabled(false);
      Dimension dimCapp = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension dimCmsg = getSize();
      setLocation( (dimCapp.width - dimCmsg.width) / 2,
                  (dimCapp.height - dimCmsg.height) / 2);
    } //endif
    super.show();
  } //end show

  void this_windowClosed(WindowEvent e) {
    if (app != null) {
      app.getParentFrame().setEnabled(true); // Siempre que se cierra
    } //endif
  } //end this_windowClosed

  void this_windowClosing(WindowEvent e) {
    dispose(); // Se cierra con la cruz
  } //end this_windowClosing

  public CApp getCApp() {
    return app;
  }
} //endclass CDialog
