package capp;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;

public abstract class CPanel
    extends Panel {

  protected CApp app = null;
  protected CPanel panelAnt = null;
  protected String nombrePanelAnt = null;
  private boolean borde = true;
  private Image canvas;

  public abstract void Inicializar();

  public CApp getApp() {
    return app;
  } //end getApp

  public void setApp(CApp a) {
    app = a;
  } //end setApp

  public void setBorde(boolean st) {
    borde = st;
  } //end setBorde

  CPanel getPanelAnterior() {
    return panelAnt;
  } //end getPanelAnterior

  String getNombrePanelAnterior() {
    return nombrePanelAnt;
  } //end getPanelAnterior

  public void update(Graphics g) {
    paint(g);
  } //end update

  public void paint(Graphics g) {
    if (borde) {
      g.setColor(getBackground().brighter());
      g.drawLine(0, 0, getBounds().width - 1, 0);
      g.drawLine(0, 0, 0, getBounds().height - 1);
      g.setColor(getBackground().darker());
      g.drawLine(0, getBounds().height - 1, getBounds().width - 1,
                 getBounds().height - 1);
      g.drawLine(getBounds().width - 1, getBounds().height - 1,
                 getBounds().width - 1, 0);
    } //endif

    super.paint(g);
  } //end paint

} //endclass CPanel
