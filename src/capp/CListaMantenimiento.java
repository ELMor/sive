package capp;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Event;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import jclass.bwt.JCItemEvent;
import jclass.bwt.JCItemListener;
import sapp.StubSrvBD;

//Para autorizaciones
//import capp2.UButtonControl;

abstract public class CListaMantenimiento
    extends CPanel {

  int numCol;
  ResourceBundle res;
  String tamCol;
  String nomCol;

  //Modos del USU (por defecto sin usu)
  int modoUsu = 0;
  final static int SIN_USU = 0;
  final static int CON_USU = 1;
  String keyBtnAnadir = "";
  String keyBtnModificar = "";
  String keyBtnBorrar = "";
  String keyBtnAux = "";

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  final public int modoESPERA = 1;

  // lista
  public CLista lista = new CLista();

  // stub
  public StubSrvBD stubCliente = null;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  // modos de operaci�n del servlet
  public int modoSeleccion_x_codigo;
  public int modoSeleccion_x_descripcion;

  // servlet
  String sUrlServlet;

  // filtro de b�squeda
  protected String sFiltro;

  // modo del servlet
  public int modoServlet;

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  // im�genes
  final String imgNAME[] = {
      "images/refrescar.gif",
      "images/alta2.gif",
      "images/baja2.gif",
      "images/modificacion2.gif",
      "images/primero.gif",
      "images/anterior.gif",
      "images/siguiente.gif",
      "images/ultimo.gif",
      "images/auxiliar.gif"};

  // indica ela fila seleccionada en la tabla */
 public int m_itemSelecc = -1;

  XYLayout xYLayout1 = new XYLayout();
  CCampoCodigo txtCod = new CCampoCodigo();
  TextField txtDes = new TextField();
  Checkbox chkbxCod = new Checkbox();
  Checkbox chkbxDes = new Checkbox();
  CheckboxGroup checkboxGroup1 = new CheckboxGroup();
  ButtonControl btnCtrlCodModelo = new ButtonControl();
  public CTabla tabla = new CTabla();

  ButtonControl btnAnadir = new ButtonControl();
  ButtonControl btnModificar = new ButtonControl();
  ButtonControl btnBorrar = new ButtonControl();
  ButtonControl btnAux = new ButtonControl();

  ButtonControl btnPrimero = new ButtonControl();
  ButtonControl btnAnterior = new ButtonControl();
  ButtonControl btnSiguiente = new ButtonControl();
  ButtonControl btnUltimo = new ButtonControl();

  CListaMantenimientobtnNormalActionListener btnNormalActionListener = new
      CListaMantenimientobtnNormalActionListener(this);
  CListaMantenimientobtnActionListener btnActionListener = new
      CListaMantenimientobtnActionListener(this);
  CListaMantenimiento_tabla_actionAdapter tablaActionListener = new
      CListaMantenimiento_tabla_actionAdapter(this);
  CListaMantenimientoItemListener multlstItemListener = new
      CListaMantenimientoItemListener(this);
  CListaMantenimientoItemAdapter itemAdapter = new
      CListaMantenimientoItemAdapter(this);
  BevelPanel bevelPanel1 = new BevelPanel();

  // constructor
  public CListaMantenimiento(CApp a, int numColumnas, String tamanosColumnas,
                             String nombresColumnas,
                             StubSrvBD stubApplet, String servlet,
                             int seleccion_x_codigo,
                             int seleccion_x_descripcion) {

    try {
      setApp(a);
      res = ResourceBundle.getBundle("capp.Res" + this.app.getIdioma());
      numCol = numColumnas;
      tamCol = tamanosColumnas;
      nomCol = nombresColumnas;
      stubCliente = stubApplet;
      sUrlServlet = servlet;
      modoSeleccion_x_codigo = seleccion_x_codigo;
      modoSeleccion_x_descripcion = seleccion_x_descripcion;

      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //Constructor con usu (Botones se habilitan o no
  // en funci�n de atutorizaciones del usuario
  public CListaMantenimiento(CApp a, int numColumnas, String tamanosColumnas,
                             String nombresColumnas,
                             StubSrvBD stubApplet, String servlet,
                             int seleccion_x_codigo,
                             int seleccion_x_descripcion,
                             String claveBtnAnadir, String claveBtnModificar,
                             String claveBtnBorrar,
                             String claveBtnAux) {

    try {
      setApp(a);
      res = ResourceBundle.getBundle("capp.Res" + this.app.getIdioma());
      numCol = numColumnas;
      tamCol = tamanosColumnas;
      nomCol = nombresColumnas;
      stubCliente = stubApplet;
      sUrlServlet = servlet;
      modoSeleccion_x_codigo = seleccion_x_codigo;
      modoSeleccion_x_descripcion = seleccion_x_descripcion;

      //Para gesti�n del USU
      modoUsu = CON_USU;
      keyBtnAnadir = claveBtnAnadir;
      keyBtnModificar = claveBtnModificar;
      keyBtnBorrar = claveBtnBorrar;
      keyBtnAux = claveBtnAux;

      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicializa el aspecto del panel
  public void jbInit() throws Exception {
    xYLayout1.setWidth(608);
    xYLayout1.setHeight(304);
    this.setLayout(xYLayout1);

    //Gesti�n del USU si es necesario
    if (modoUsu == CON_USU) {
      btnAnadir = new USUButtonControl(keyBtnAnadir, app);
      btnModificar = new USUButtonControl(keyBtnModificar, app);
      btnBorrar = new USUButtonControl(keyBtnBorrar, app);
      btnAux = new USUButtonControl(keyBtnAux, app);
    }

    btnAnadir.setActionCommand("A�adir");
    btnModificar.setActionCommand("Modificar");
    btnBorrar.setActionCommand("Borrar");
    btnCtrlCodModelo.setActionCommand("BuscarCodModelo");
    btnPrimero.setActionCommand("primero");
    btnAnterior.setActionCommand("anterior");
    btnSiguiente.setActionCommand("siguiente");
    btnUltimo.setActionCommand("ultimo");
    btnAux.setActionCommand("Auxiliar");

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnCtrlCodModelo.setImage(imgs.getImage(0));
    btnCtrlCodModelo.setLabel(res.getString("btnCtrlCodModelo.Label"));
    btnAnadir.setImage(imgs.getImage(1));
    btnBorrar.setImage(imgs.getImage(2));
    btnModificar.setImage(imgs.getImage(3));
    btnPrimero.setImage(imgs.getImage(4));
    btnAnterior.setImage(imgs.getImage(5));
    btnSiguiente.setImage(imgs.getImage(6));
    btnUltimo.setImage(imgs.getImage(7));
    btnAux.setImage(imgs.getImage(8));
    bevelPanel1.setBevelOuter(BevelPanel.LOWERED);
    bevelPanel1.setBevelInner(BevelPanel.FLAT);

    chkbxCod.setLabel(res.getString("chkbxCod.Label"));
    chkbxCod.setCheckboxGroup(checkboxGroup1);
    chkbxCod.addItemListener(itemAdapter);
    chkbxDes.setLabel(res.getString("chkbxDes.Label"));
    chkbxDes.setCheckboxGroup(checkboxGroup1);
    chkbxDes.addItemListener(itemAdapter);

    this.add(txtCod, new XYConstraints(9, 6, 275, -1));
    this.add(txtDes, new XYConstraints(9, 6, 275, -1));
    this.add(chkbxCod, new XYConstraints(9, 33, 133, -1));
    this.add(chkbxDes, new XYConstraints(217, 33, 159, -1));
    this.add(btnCtrlCodModelo, new XYConstraints(502, 32, -1, -1));
    this.add(bevelPanel1, new XYConstraints(9, 62, 577, 2));
    this.add(tabla, new XYConstraints(9, 73, 580, 182));
    /*
        this.add(btnAnadir, new XYConstraints(10, 261, -1, -1));
        this.add(btnModificar, new XYConstraints(45, 261, -1, -1));
        this.add(btnBorrar, new XYConstraints(80, 261, -1, -1));
        this.add(btnAux, new XYConstraints(250, 261, -1, -1));
        this.add(btnPrimero, new XYConstraints(459, 261, -1, -1));
        this.add(btnAnterior, new XYConstraints(494, 261, -1, -1));
        this.add(btnSiguiente, new XYConstraints(529, 261, -1, -1));
        this.add(btnUltimo, new XYConstraints(564, 261, -1, -1));
     */

    this.add(btnAnadir, new XYConstraints(10, 261, 26, 26));
    this.add(btnModificar, new XYConstraints(45, 261, 26, 26));
    this.add(btnBorrar, new XYConstraints(80, 261, 26, 26));
    this.add(btnAux, new XYConstraints(250, 261, 26, 26));
    this.add(btnPrimero, new XYConstraints(459, 261, 22, 22));
    this.add(btnAnterior, new XYConstraints(494, 261, 22, 22));
    this.add(btnSiguiente, new XYConstraints(529, 261, 22, 22));
    this.add(btnUltimo, new XYConstraints(564, 261, 22, 22));

    // configuraci�n de la tabla
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(
        nomCol, '\n'));
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(tamCol, '\n'));
    tabla.setNumColumns(numCol);

    chkbxCod.setState(true);
    txtCod.setVisible(true);
    txtDes.setVisible(false);

    tabla.addActionListener(tablaActionListener);
    tabla.addItemListener(multlstItemListener);
    btnCtrlCodModelo.addActionListener(btnActionListener);

    btnAnadir.addActionListener(btnNormalActionListener);
    btnModificar.addActionListener(btnNormalActionListener);
    btnBorrar.addActionListener(btnNormalActionListener);
    btnAux.addActionListener(btnNormalActionListener);
    btnPrimero.addActionListener(btnNormalActionListener);
    btnAnterior.addActionListener(btnNormalActionListener);
    btnSiguiente.addActionListener(btnNormalActionListener);
    btnUltimo.addActionListener(btnNormalActionListener);

    btnAux.setVisible(false);
    btnAux.setEnabled(false);

    Inicializar();
  }

  // activaci�n del bot�n auxiliar
  public void ActivarAuxiliar() {
    btnAux.setVisible(true);
    btnAux.setEnabled(true);
  }

  public void setCurrent(int i) {
    Event ev = null;

    //# System_Out.println( tabla.countItems() );

    if (i >= tabla.countItems()) {
      i = tabla.countItems() - 1;

    }
    if (i > 0) {
      tabla.select(i);
      m_itemSelecc = i;
      //tabla.setAutoSelect(true);
      //tabla.select(i);
      //ev = new Event(tabla,Event.MOUSE_DOWN,tabla);
    }
  }

  public void Inicializar() {
    switch (modoOperacion) {

      case modoNORMAL:

        // modo modificaci�n y baja
        txtCod.setEnabled(true);
        txtDes.setEnabled(true);
        btnCtrlCodModelo.setEnabled(true);
        chkbxCod.setEnabled(true);
        chkbxDes.setEnabled(true);
        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);
        btnAnadir.setEnabled(true);
        btnModificar.setEnabled(true);
        btnBorrar.setEnabled(true);
        btnAux.setEnabled(true);
        tabla.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCod.setEnabled(false);
        txtDes.setEnabled(false);
        btnCtrlCodModelo.setEnabled(false);
        chkbxCod.setEnabled(false);
        chkbxDes.setEnabled(false);
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        btnAnadir.setEnabled(false);
        btnModificar.setEnabled(false);
        btnAux.setEnabled(false);
        btnBorrar.setEnabled(false);
        tabla.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  // limpia la pantalla
  void vaciarPantalla() {
    if (tabla.countItems() > 0) {
      tabla.deleteItems(0, tabla.countItems() - 1);
    }
  }

  // selecciona otro modelo
  public void btnCtrlCodModelo_actionPerformed() {

    CMessage msgBox = null;
    CLista data = null;
    Object componente = null;

    // procesa
    try {

      modoOperacion = modoESPERA;
      Inicializar();

      // vacia la tabla
      tabla.clear();

      // almacena el filtro
      if (chkbxCod.getState()) {
        sFiltro = txtCod.getText();
      }
      else {
        sFiltro = txtDes.getText();

        // obtiene y pinta la lista nueva
      }
      data = new CLista();

      //AUTORIZACIONES del applet MLM 20-10-99
      data.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
      data.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());

      //TSIVE del applet MLM 20-10-99
      data.setTSive(app.getTSive());

      //Enfermedad, si procede:
      if (!this.app.getParametro("CD_TUBERCULOSIS").equals("")) {
        data.setCdEnfermedad(this.app.getParametro("CD_TUBERCULOSIS"));
      }

      // idioma
      data.setIdioma(app.getIdioma());

      // perfil
      data.setPerfil(app.getPerfil());

      // login
      data.setLogin(app.getLogin());

      data.addElement(setComponente(sFiltro));

      // Indica estamos en un mantenimiento
      data.setMantenimiento(true);

      stubCliente.setUrl(new URL(app.getURL() + sUrlServlet));
      // JRM: A�ade el modo de llamada al servlet
      if (this.chkbxCod.getState()) {
        this.modoServlet = modoSeleccion_x_codigo;
      }
      else {
        this.modoServlet = modoSeleccion_x_descripcion;
      }

      lista = (CLista) stubCliente.doPost(modoServlet, data);
      /*
            eqNot.SrvEqNot srv = new eqNot.SrvEqNot();
            srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                   "jdbc:oracle:thin:@192.168.0.13:1521:ICM",
                                   "dba_edo",
                                   "manager");
            lista = srv.doDebug(modoServlet, data);
       */
      data = null;

      // comprueba que hay datos
      if (lista.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg1.Text"));
        msgBox.show();
        msgBox = null;
      }
      else {
        // escribe las l�neas en la tabla
        for (int j = 0; j < lista.size(); j++) {
          componente = lista.elementAt(j);
          tabla.addItem(setLinea(componente), '&');
        }

        // verifica que sea la �ltima trama
        if (lista.getState() == CLista.listaINCOMPLETA) {
          tabla.addItem(res.getString("msg2.Text"));
        }
      }

      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // boton borrar/modificar/auxiliar
  public void btn_actionPermormed(ActionEvent e) {
    int indice = tabla.getSelectedIndex();

    if (e.getActionCommand().equals("A�adir")) { // a�adir
      btnAnadir_actionPerformed();
      //Si hay fila elegida
    }
    else if (indice != BWTEnum.NOTFOUND) {
      // Si no se ha elegido mas...
      if (! ( (lista.getState() == CLista.listaINCOMPLETA) &&
             (indice == (tabla.countItems() - 1)))) {

        if (e.getActionCommand().equals("Modificar")) { // modificar
          btnModificar_actionPerformed(indice);
        }
        else if (e.getActionCommand().equals("Borrar")) { // borrar
          btnBorrar_actionPerformed(indice);
        }
        else if (e.getActionCommand().equals("Auxiliar")) { // boton auxiliar
          btnAuxiliar_actionPerformed(indice);
        }
      }
    }

    if (indice != BWTEnum.NOTFOUND) {
      setCurrent(indice);
    }
  }

  // doble click en la tabla
  public void tabla_actionPerformed() {
    boolean bModificar = btnModificar.isEnabled();
    if (bModificar) {
      CMessage msgBox = null;
      CLista data = null;
      int iLast;
      Object componente;
      int indice = tabla.getSelectedIndex();

      // lista incompleta
      if ( (lista.getState() == CLista.listaINCOMPLETA) &&
          (tabla.getSelectedIndex() == (tabla.countItems() - 1))) {

        // procesa
        try {

          modoOperacion = modoESPERA;
          Inicializar();

          // �ltimo indice
          iLast = tabla.countItems() - 1;

          // obtiene y pinta la lista nueva
          data = new CLista();

          // alamcena los par�metros para recuperar las tramas
          data.setFilter(lista.getFilter());

          // idioma
          data.setIdioma(app.getIdioma());

          // perfil
          data.setPerfil(app.getPerfil());

          // login
          data.setLogin(app.getLogin());

          //AUTORIZACIONES del applet LRG (feb 2000)
          data.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES());
          data.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());

          //TSIVE del applet LRG (feb 2000)
          data.setTSive(app.getTSive());

          //Enfermedad, si procede:
          if (!this.app.getParametro("CD_TUBERCULOSIS").equals("")) {
            data.setCdEnfermedad(this.app.getParametro("CD_TUBERCULOSIS"));
          }

          // estado
          data.setState(CLista.listaINCOMPLETA);

          // Indica estamos en un mantenimiento
          data.setMantenimiento(true);

          data.addElement(setComponente(sFiltro));
          stubCliente.setUrl(new URL(app.getURL() + sUrlServlet));
          lista.appendData( (CLista) stubCliente.doPost(modoServlet, data));
          /*
                eqNot.SrvEqNot srv = new eqNot.SrvEqNot();
                srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
               "jdbc:oracle:thin:@192.168.0.13:1521:ICM",
                                       "dba_edo",
                                       "manager");
                lista = srv.doDebug(modoServlet, data);
           */
          data = null;

          // escribe las l�neas en la tabla
          tabla.deleteItem(iLast);
          for (int j = iLast; j < lista.size(); j++) {
            componente = lista.elementAt(j);
            tabla.addItem(setLinea(componente), '&');
          }

          // verifica que sea la �ltima trama
          if (lista.getState() == CLista.listaINCOMPLETA) {
            tabla.addItem(res.getString("msg2.Text"));

            //Selecci�n de elementos
          }
          m_itemSelecc = -1;

          // error al procesar
        }
        catch (Exception e) {
          e.printStackTrace();
          msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
          msgBox.show();
          msgBox = null;
        }

        modoOperacion = modoNORMAL;
        Inicializar();

        // modo modificar
      }
      else {
        btnModificar_actionPerformed(indice);
      }
      setCurrent(indice);
    }
  }

  void tabla_itemStateChanged(JCItemEvent evt) {

    m_itemSelecc = tabla.getSelectedIndex();

  }

  // pinta la tabla
  public void RellenaTabla() {
    Object componente;

    // escribe las l�neas en la tabla
    tabla.clear();
    for (int j = 0; j < lista.size(); j++) {
      componente = lista.elementAt(j);
      tabla.addItem(setLinea(componente), '&');
    }

    // verifica que sea la �ltima trama
    if (lista.getState() == CLista.listaINCOMPLETA) {
      tabla.addItem(res.getString("msg2.Text"));
    }
  }

  // confecciona los botones de mantenimiento
  public void ConfigModo(boolean bAlta,
                         boolean bModif,
                         boolean bBaja) {
    final int iY = 257;
    final int iX[] = {
        10, 45, 80};
    int i;

    // remueve todos los botones
    this.remove(btnAnadir);
    this.remove(btnModificar);
    this.remove(btnBorrar);
    this.remove(btnAux);
    this.remove(btnPrimero);
    this.remove(btnAnterior);
    this.remove(btnSiguiente);
    this.remove(btnUltimo);
    i = 0;

    // +
    if (bAlta) {
      this.add(btnAnadir, new XYConstraints(iX[i], iY, -1, -1));
      i++;
    }

    // +-
    if (bModif) {
      this.add(btnModificar, new XYConstraints(iX[i], iY, -1, -1));
      i++;
    }

    // -
    if (bBaja) {
      this.add(btnBorrar, new XYConstraints(iX[i], iY, -1, -1));
    }

    // a�ade de nuevo los botones para la gesti�n del foco
    this.add(btnAux, new XYConstraints(250, 261, -1, -1));
    this.add(btnPrimero, new XYConstraints(459, 261, -1, -1));
    this.add(btnAnterior, new XYConstraints(494, 261, -1, -1));
    this.add(btnSiguiente, new XYConstraints(529, 261, -1, -1));
    this.add(btnUltimo, new XYConstraints(564, 261, -1, -1));
  }

  // bot�n auxiliar
  public void btnAuxiliar_actionPerformed(int i) {
    //# System_Out.println(i);
  }

  // a�ade una l�nea
  public abstract void btnAnadir_actionPerformed();

  // modifica l�nea de la lista
  public abstract void btnModificar_actionPerformed(int i);

  // borra l�nea de la lista
  public abstract void btnBorrar_actionPerformed(int i);

  // rellena los par�metros para enviar al servlet
  public abstract Object setComponente(String s);

  // formatea el componente para ser insertado en una fila
  public abstract String setLinea(Object o);

  void chkbx_itemStateChanged(ItemEvent e) {
    if (e.getItem().equals("Buscar por c�digo")) {
      txtCod.setVisible(true);
      txtCod.setText(txtDes.getText().toUpperCase());
      txtDes.setVisible(false);
    }
    else {
      txtCod.setVisible(false);
      txtDes.setVisible(true);
      txtDes.setText(txtCod.getText());
    }
    doLayout();
  }

} //fin clase

    /******************************************************************************/

// action listener de evento en botones normales( No implementa Runnable)
class CListaMantenimientobtnNormalActionListener
    implements ActionListener {
  CListaMantenimiento adaptee = null;
  ActionEvent e = null;

  public CListaMantenimientobtnNormalActionListener(CListaMantenimiento adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;

    if (e.getActionCommand() == "primero") {
      if (adaptee.tabla.countItems() > 0) {
        adaptee.m_itemSelecc = 0;
        adaptee.tabla.select(adaptee.m_itemSelecc);
        adaptee.tabla.setTopRow(adaptee.m_itemSelecc);
      }
    }
    else if (e.getActionCommand() == "anterior") {
      if (adaptee.m_itemSelecc > 0) {
        adaptee.m_itemSelecc--;
        adaptee.tabla.select(adaptee.m_itemSelecc);
        if (adaptee.m_itemSelecc - adaptee.tabla.getTopRow() <= 0) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() - 1);
        }
      }
    }
    else if (e.getActionCommand() == "siguiente") {
      int ultimo = adaptee.tabla.countItems() - 1;
      if (adaptee.m_itemSelecc < ultimo) {
        adaptee.m_itemSelecc++;
        adaptee.tabla.select(adaptee.m_itemSelecc);
        if (adaptee.m_itemSelecc - adaptee.tabla.getTopRow() >= 8) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() + 1);
        }
      }
    }
    else if (e.getActionCommand() == "ultimo") {
      int ultimo = adaptee.tabla.countItems() - 1;
      if (ultimo >= 0) {
        adaptee.m_itemSelecc = ultimo;
        adaptee.tabla.select(adaptee.m_itemSelecc);
        if (adaptee.tabla.countItems() >= 8) {
          adaptee.tabla.setTopRow(adaptee.tabla.countItems() - 8);
        }
        else {
          adaptee.tabla.setTopRow(0);
        }
      }
    }
    else { //A�adir, Mod, borrar o auxiliar
      adaptee.btn_actionPermormed(e);
    }
  }
}

// escuchador de los click en la tabla
class CListaMantenimiento_tabla_actionAdapter
    implements jclass.bwt.JCActionListener {
  CListaMantenimiento adaptee;

  CListaMantenimiento_tabla_actionAdapter(CListaMantenimiento adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    GestionTabla th = new GestionTabla(adaptee);
    th.run();
  }
}

// action listener de evento en botones
class CListaMantenimientobtnActionListener
    implements ActionListener, Runnable {
  CListaMantenimiento adaptee = null;

  public CListaMantenimientobtnActionListener(CListaMantenimiento adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (adaptee.chkbxCod.getState()) {
      adaptee.modoServlet = adaptee.modoSeleccion_x_codigo;
    }
    else {
      adaptee.modoServlet = adaptee.modoSeleccion_x_descripcion;
    }
    adaptee.btnCtrlCodModelo_actionPerformed();
  }
}

//Tambi�n implementa Runnable por si usuario pulsa m�s..
class CListaMantenimientoItemListener
    implements JCItemListener, Runnable {
  CListaMantenimiento adaptee = null;
  JCItemEvent e = null;

  public CListaMantenimientoItemListener(CListaMantenimiento adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(JCItemEvent e) {
    if (e.getStateChange() == JCItemEvent.SELECTED) { //evento seleccion (no deseleccion)
      this.e = e;
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

//    if (((JCMultiColumnList)e.getSource()).getName() == "tabla")
    adaptee.tabla_itemStateChanged(e);
  }
}

// hilo de ejecuci�n para la gesti�n del doble click en la tabla
class GestionTabla
    implements Runnable {
  CListaMantenimiento adaptee;

  public GestionTabla(CListaMantenimiento l) {
    adaptee = l;
    Thread th = new Thread();
    th.run();
  }

  // obtiene la nueva trama de la lista
  public void run() {
    adaptee.tabla_actionPerformed();
  }
}

class CListaMantenimientoItemAdapter
    implements java.awt.event.ItemListener {
  CListaMantenimiento adaptee;

  CListaMantenimientoItemAdapter(CListaMantenimiento adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkbx_itemStateChanged(e);
  }
}
