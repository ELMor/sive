
package capp;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class CMail {

  // sesi�n de mail
  protected Session s;
  protected MimeMessage msg;
  protected Properties props;

  // constructor
  public CMail(String host) {
    try {
      props = new Properties();
      props.put("mail.smtp.host", host);

      Session s = Session.getDefaultInstance(props, null);

      msg = new MimeMessage(s);
      msg.setHeader("X-Mailer", "PISTA");
    }
    catch (Exception e) {
    }
  }

  // constructor
  public CMail() {
    try {

      Session s = Session.getDefaultInstance(System.getProperties(), null);

      msg = new MimeMessage(s);
      msg.setHeader("X-Mailer", "PISTA");
    }
    catch (Exception e) {
    }
  }

  // establece la direcci�n to
  public int setTo(String dir) {
    try {
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(dir, false));
    }
    catch (Exception e) {
      e.printStackTrace();
      return -1;
    }
    return 0;
  }

  // establece la direcci�n from
  public int setFrom(String dir) {
    try {
      msg.setFrom(new InternetAddress(dir));
    }
    catch (Exception e) {
      e.printStackTrace();
      return -1;
    }
    return 0;
  }

  // establece el asunto del correo
  public int setSubject(String asunto) {
    try {
      msg.setSubject(asunto);
    }
    catch (Exception e) {
      e.printStackTrace();
      return -1;
    }
    return 0;
  }

  // establece el cuerpo del correo
  public int setBody(String cuerpo) {
    try {
      msg.setText(cuerpo);
    }
    catch (Exception e) {
      e.printStackTrace();
      return -1;
    }
    return 0;
  }

  // envia el correo
  public int sendMail() {
    try {
      Transport.send(msg);
    }
    catch (Exception e) {
      e.printStackTrace();
      return -1;
    }
    return 0;
  }

  // prueba
  public static void main(String args[]) {
    CMail mail = new CMail("bigfoot.cesatel.es");
    mail.setTo("lsanroman@norsistemas.es");
    mail.setFrom("lsanroman@norsistemas.es");
    mail.setSubject("prueba");
    mail.setBody("esto es una prueba");
    mail.sendMail();
  }
}