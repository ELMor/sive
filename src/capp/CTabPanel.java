package capp;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;

public class CTabPanel
////////////////////////////////////////////////////////////////////////////////
    extends CPanel {
  //private CSolapa solapa;
  //private CPanelSolapa disp;

  // modificacion 19/06/2000
  // para poder extender CSolapa y CTabPanel
  public CSolapa solapa;
  public CPanelSolapa disp;

  public CTabPanel() {
    setBackground(Color.lightGray);
    setLayout(new BorderLayout());

    solapa = new CSolapa();
    solapa.setBackground(Color.lightGray);
    solapa.setVisible(false); // No se muestra hasta que tenga mas de uno
    disp = new CPanelSolapa();
    disp.setBackground(Color.lightGray);
    disp.setLayout(new CardLayout());

    add("North", solapa);
    add("Center", disp);
  } //end CTabPanel

  public void InsertarPanel(String nombre, CPanel panel) {
    solapa.addItem(nombre);
    disp.addItem(nombre, panel);

    //Si es el segundo se muestra la solapa
    if (getNumSolapas() == 2) {
      solapa.setVisible(true);
    } //endif
    disp.setVisible(true);

    VerPanel(nombre);
  } //end InsertarPanel

  // Devuelve el n�mero de solapas
  public int getNumSolapas() {
    return solapa.countTabs();
  } //end getNumSolapas

  // Muestra uno de los paneles
  public void VerPanel(String nombre) {
    solapa.choose(nombre);
    disp.choose(nombre);
  } //end VerPanel

  // Muestra uno de los paneles
  public void VerPanelAnterior() {
    if ( (getNumSolapas() > 1) && (solapa.getCurrentPanelNdx() > 0)) {
      String ant = solapa.label(solapa.getCurrentPanelNdx() - 1);
      solapa.choose(ant);
      disp.choose(ant);
    }
    else if (getNumSolapas() > 1) {

    }
    else {
      CPanel pant = ( (CApp) getParent()).PanelActual();
      if (pant.panelAnt != null) {
        BorrarSolapa();
        InsertarPanel(pant.nombrePanelAnt, pant.panelAnt);
        validate();
      } //endif
    } //endif
  } //end VerPanelAnterior

  public void BorrarSolapa(String c) {
    String s = solapa.label(solapa.getCurrentPanelNdx());

    boolean esta = false;
    int i;
    for (i = 0; i < getNumSolapas() && !esta; i++) {
      esta = solapa.label(i).equals(c);
    } //endfor

    if (esta) {
      solapa.delItem(i - 1);
      disp.delItem(i - 1);

      if (getNumSolapas() == 1) {
        solapa.setVisible(false);
      } //endif

      if (c.equals(s)) {
        s = solapa.label(getNumSolapas() - 1);
      } //endif
      VerPanel(s);
    } //endif
  } //end BorrarSolapa

  // Elimina todas las solapas
  public void BorrarSolapa() {
    solapa.delAll();
    disp.delAll();
  } //end BorrarSolapa

  public String getSolapaActual() {
    return solapa.label(solapa.getCurrentPanelNdx());
  } //end getSolapaActual

  public CPanel getPanelActual() {
    return disp.getTabPanel(solapa.getCurrentPanelNdx());
  } //end getPanelActual

  public void Inicializar() {}

} //endclass CTabPanel
