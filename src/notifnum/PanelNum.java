package notifnum;

import java.util.Hashtable;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import comun.constantes;

public class PanelNum
    extends CPanel {

  final int modoESPERA = constantes.modoESPERA;
  final int modoALTA = 3; //no se dara
  final int modoMODIFICACION = 4;
  final int modoBAJA = constantes.modoBAJA;
  final int modoCONSULTA = constantes.modoCONSULTA;

  protected int modoOperacion;

  final String imgNAME[] = {
      "images/aceptar.gif",
      "images/cancelar.gif"};

  // controles
  XYLayout xYLayout = new XYLayout();

  public Hashtable hash = null;
  public String nCaso = "";

  ButtonControl btnalta = new ButtonControl();
  ButtonControl btnmodif = new ButtonControl();

  actionAdapter actionAdap = new actionAdapter(this);

  public CApp app = null;

  Hashtable hashNotifEDO = new Hashtable(); // sera mejor crear un metodo

  CMessage msgBox = null;

  public PanelNum(CApp a) {

    try {
      this.app = (CApp) a;

      // que recupere los valores
      hashNotifEDO.put("CD_E_NOTIF", "011012");
      hashNotifEDO.put("CD_ANOEPI", "1001");
      hashNotifEDO.put("CD_SEMEPI", "01");
      hashNotifEDO.put("FC_RECEP", "15/02/2001");
      hashNotifEDO.put("FC_FECNOTIF", "06/01/2001");

      jbInit();
      //modoOperacion = modo;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    // carga las im�genes
    CCargadorImagen imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnalta.setLabel("Alta");
    btnalta.setActionCommand("alta");
    btnalta.addActionListener(actionAdap);

    btnmodif.setLabel("Modificaci�n");
    btnmodif.setActionCommand("modif");
    btnmodif.addActionListener(actionAdap);

    // Organizacion del panel
    this.setSize(new Dimension(100, 100));
    xYLayout.setHeight(100);
    xYLayout.setWidth(100);
    this.setLayout(xYLayout);
    setBorde(false);

    this.add(btnalta, new XYConstraints(50, 10, -1, -1));
    this.add(btnmodif, new XYConstraints(50, 60, -1, -1));

  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {

      case modoESPERA:

        btnalta.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA: //no se dar�

        btnalta.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
      case modoMODIFICACION:
      case modoBAJA:
      case modoCONSULTA:

        btnalta.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

    }
  }

  public void btnalta_actionPerformed() {

    int modo = 3;

    DialNum diaNum = new DialNum(this.getApp(), hashNotifEDO, modo);
    if (diaNum.addPanelProt()) {
      diaNum.show();
      CLista listaTmp = new CLista();
      listaTmp = diaNum.devuelveDatos();
      //for (int i = 0;i<listaTmp.size();i++)
      //System_out.println((Hashtable)listaTmp.elementAt(i));
    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            "No se encontraron datos");
      msgBox.show();
      msgBox = null;
    }
  }

  public void btnmodif_actionPerformed() {
    int modo = 4;

    DialNum diaNum = new DialNum(this.getApp(), hashNotifEDO, modo);
    if (diaNum.addPanelProt()) {
      diaNum.show();
      CLista listaTmp = new CLista();
      listaTmp = diaNum.devuelveDatos();
      //for (int i = 0;i<listaTmp.size();i++)
      //System_out.println((Hashtable)listaTmp.elementAt(i));
    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            "No se encontraron datos");
      msgBox.show();
      msgBox = null;
    }
  }

} //clase

class actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelNum adaptee;
  ActionEvent evt;

  actionAdapter(PanelNum adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {

    if (evt.getActionCommand().equals("alta")) {
      adaptee.btnalta_actionPerformed();
    }
    else if (evt.getActionCommand().equals("modif")) {
      adaptee.btnmodif_actionPerformed();
    }
  }
} //fin class
