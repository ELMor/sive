package notifnum;

import java.util.Hashtable;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.Label;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import comun.constantes;

public class DialNum
    extends CDialog {

  // Modo de salida del dialog (Aceptar=0, Cancelar=-1)
  //Aceptar == repetir query al volver
  //Cancelar == nada
  public int iOut = -1;

  final String imgNAME[] = {
      "images/aceptar.gif", "images/cancelar.gif"};

  public int modoOperacion;
  public int modos;

  ScrollPane panelScroll = new ScrollPane();
  XYLayout xYLayout1 = new XYLayout();
  ButtonControl btnSalir = new ButtonControl("Aceptar");
  ButtonControl btnCancelar = new ButtonControl("Cancelar");
  Label lblEnfermedad = new Label("Enfermedad");
  Label lblCasos = new Label("Casos");

  Pan1 pan1 = null;

  // modificacion
  CApp pNum = null;
  CLista listaDevuelta = null;

  // hashtable NotifEDO
  protected Hashtable hashNotifEDO = null; //ver panelMaestroEDO
  //debe llevar informaci�n sobre cd_e_notif, cd_anoepi, cd_semepi,
  // fc_recep(se pone la del d�a), fc_fecnotif

  // constructor
  public DialNum(CApp p, Hashtable tablaDatos, int modo) {

    super(p);

    try {
      hashNotifEDO = tablaDatos;
      pNum = p;
      modoOperacion = modo;
      modos = modo;
      Inicializar(modoOperacion);
      jbInit();

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void jbInit() throws Exception {
    btnSalir.setActionCommand("salir");
    btnCancelar.setActionCommand("cancelar");

    lblEnfermedad.setAlignment(Label.RIGHT);
    lblEnfermedad.setFont(new Font("", Font.BOLD, 14));
    lblCasos.setAlignment(Label.CENTER);
    lblCasos.setFont(new Font("", Font.BOLD, 14));
    // carga las imagenes
    CCargadorImagen imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnSalir.setImage(imgs.getImage(0));
    btnCancelar.setImage(imgs.getImage(1));

    xYLayout1.setWidth(400);
    xYLayout1.setHeight(350);

    this.setLayout(xYLayout1);
    this.setSize(400, 350);

    DialogOPactionAdapter actionAdapter = new DialogOPactionAdapter(this);
    this.setTitle("Enfermedades Num�ricas");
    this.add(lblEnfermedad, new XYConstraints(170, 5, 100, 20));
    this.add(lblCasos, new XYConstraints(278, 5, 70, 20));
    this.add(btnSalir, new XYConstraints(110, 290, -1, -1));
    this.add(btnCancelar, new XYConstraints(210, 290, -1, -1));
    btnSalir.addActionListener(actionAdapter);
    btnCancelar.addActionListener(actionAdapter);
    this.doLayout();
    this.validate();
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar()

  {

    switch (modoOperacion) {

      case constantes.modoALTA:
      case constantes.modoMODIFICACION:
      case constantes.modoBAJA:
      case constantes.modoCONSULTA:
        btnSalir.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case constantes.modoESPERA:
        btnSalir.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();

  }

  public boolean addPanelProt() {
    int modo = modoOperacion;
    boolean estado = false;
    this.Inicializar(constantes.modoESPERA);

    pan1 = null;
    pan1 = new Pan1(this.app, hashNotifEDO, this, modos);
    estado = pan1.hayDatos();

    this.add(pan1, new XYConstraints(4, 25, 400, 250));
    pan1.doLayout();

    this.Inicializar(modo);
    return estado;
  }

  void btnAceptar_actionPerformed() {
    //hay aue recoger cd_ope y fc_ultact
    this.Inicializar(constantes.modoESPERA);
    listaDevuelta = pan1.devuelveDatos();
    this.Inicializar(modoOperacion);
    dispose();
  }

  void btnCancelar_actionPerformed() {
    // INICIAMOS (no inicializamos, que no existe en castellano,
    // tal vez en espanglish ... ) con el relojito
    this.Inicializar(constantes.modoESPERA);
    dispose();
    this.Inicializar(modoOperacion);
  }

  public CLista devuelveDatos() {
    return listaDevuelta;
  }
} //fin clase

class DialogOPactionAdapter
    implements java.awt.event.ActionListener, Runnable {
  DialNum adaptee;
  ActionEvent evt;

  DialogOPactionAdapter(DialNum adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if (evt.getActionCommand().equals("salir")) {
      adaptee.btnAceptar_actionPerformed();
    }
    else if (evt.getActionCommand().equals("cancelar")) {
      adaptee.btnCancelar_actionPerformed();
    }
  }
} //fin class
