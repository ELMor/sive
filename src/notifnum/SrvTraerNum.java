package notifnum;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Hashtable;

import capp.CLista;
import sapp.DBServlet;

//import jdbcpool.*; // $$$ modo debug

public class SrvTraerNum
    extends DBServlet {

  // nuevo
  protected Hashtable henfnum = null;
  protected Hashtable datosEntrada = null;

  protected int modoOperacion = 0;

  protected CLista listaSalida = null; //intermedia
  protected CLista listaDevolver = null;
  protected CLista listaSalidaResp = null; //las respuestas (dataRespCompleta)

  final int servletALTA = 0;
  final int servletMODIF = 1;

  String sQuery = "";

  // ARG: Para usar fechas en las selects
  private java.sql.Timestamp cadena_a_timestamp(String sFecha) {
    int dd = (new Integer(sFecha.substring(0, 2))).intValue();
    int mm = (new Integer(sFecha.substring(3, 5))).intValue();
    int yyyy = (new Integer(sFecha.substring(6, 10))).intValue();
    int hh = (new Integer(sFecha.substring(11, 13))).intValue();
    int mi = (new Integer(sFecha.substring(14, 16))).intValue();
    int ss = (new Integer(sFecha.substring(17, 19))).intValue();

    java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy - 1900, mm - 1, dd,
        hh, mi, ss, 0);
    return TSFec;
  }

  //  private Connection conexion = null;

//  protected CLista doWork(int opmode, CLista param) throws Exception {
  protected CLista doWork(int opmode, CLista param) throws Exception {

    modoOperacion = opmode;

    // objetos de datos

    listaSalida = new CLista();

    java.util.Date d = null;
    SimpleDateFormat sdf = null;

    // ARG: Para usar fechas en las selects
    java.sql.Timestamp fec = null;
    String fecha = "";

    //variable de control para la secuencialidad
    boolean b = true;

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 0;

    //  if (conexion == null) {
    con = openConnection();
    //  } else {
    //    con = conexion;
    //  }

    con.setAutoCommit(false); //aunque son selects

    try {

      // modos de operaci�n
      switch (modoOperacion) {

        //ALTA*****************************************
        case servletALTA:

          datosEntrada = new Hashtable();
          datosEntrada = (Hashtable) param.firstElement();

          /* sQuery ="select A.CD_ENFCIE, B.DS_ENFEREDO, A.CD_TVIGI " +
                   "from SIVE_ENFEREDO A, SIVE_EDO_CNE B " +
                   "where B.CD_ENFEREDO = A.CD_ENFERE " +
                   "and A.CD_TVIGI IN ('A', 'N', 'X') " +
                   "and A.IT_BAJA <> 'S' AND " +
               "not exists (select * from SIVE_EDONUM C where C.CD_ENFCIE = A.CD_ENFCIE " +
               "and C.CD_E_NOTIF = ? and C.CD_ANOEPI = ? and C.CD_SEMEPI = ? " +
                   "and TO_CHAR(C.FC_RECEP, 'YYYYMMDD') = ? " +
                   "and TO_CHAR(C.FC_FECNOTIF,'YYYYMMDD') = ?) " +
                   "order by A.CD_ENFCIE"; */

          // ARG: sin to_char
          sQuery = "select A.CD_ENFCIE, B.DS_PROCESO, A.CD_TVIGI " +
              "from SIVE_ENFEREDO A, SIVE_PROCESOS B " +
              "where B.CD_ENFCIE = A.CD_ENFCIE " +
              "and A.CD_TVIGI IN ('A', 'N', 'X') " +
              "and A.IT_BAJA <> 'S' AND " +
              "not exists (select * from SIVE_EDONUM C where C.CD_ENFCIE = A.CD_ENFCIE " +
              "and C.CD_E_NOTIF = ? and C.CD_ANOEPI = ? and C.CD_SEMEPI = ? " +
              "and C.FC_RECEP = ? " +
              "and C.FC_FECNOTIF = ?) " +
              "order by B.DS_PROCESO";

          st = con.prepareStatement(sQuery);

          sdf = new SimpleDateFormat("dd/MM/yyyy");

          st.setString(1, (String) datosEntrada.get("CD_E_NOTIF"));
          st.setString(2, (String) datosEntrada.get("CD_ANOEPI"));
          st.setString(3, (String) datosEntrada.get("CD_SEMEPI"));

          // ARG: no se pueden usar to_char
          /* // Preparamos la fecha pa que funcione
               String fecha = ((String)datosEntrada.get("FC_RECEP")).trim();
                         StringTokenizer sTok = new StringTokenizer(fecha,"/");
                         String s1,s2,s3;
                         s1 = sTok.nextToken();
                         s2 = sTok.nextToken();
                         s3 = sTok.nextToken();
                         // As� s� que pita
                         st.setString(4, s3+s2+s1);
           */

          /*              d = new java.util.Date ();
               d = sdf.parse(((String)datosEntrada.get("FC_RECEP")).trim());
                        st.setDate(4, new java.sql.Date(d.getTime()));
                        d = null;*/

          // ARG: sin to_char
          fecha = ( (String) datosEntrada.get("FC_RECEP")).trim() + " 00:00:00";
          fec = (java.sql.Timestamp) cadena_a_timestamp(fecha);
          st.setTimestamp(4, fec);

          // ARG: no se pueden usar to_char
          /* // Hacemos lo mismo por segunda vez
               fecha = ((String)datosEntrada.get("FC_FECNOTIF")).trim();
                         sTok = new StringTokenizer(fecha,"/");
                         s1="";
                         s2="";
                         s3="";
                         s1 = sTok.nextToken();
                         s2 = sTok.nextToken();
                         s3 = sTok.nextToken();
                         // Pues as� deber�a pitar
                         st.setString(5, s3+s2+s1);
           */

          /*              d = new java.util.Date ();
               d = sdf.parse(((String)datosEntrada.get("FC_FECNOTIF")).trim());
                        st.setDate(5, new java.sql.Date(d.getTime()));*/

          // ARG: sin to_char
          fecha = ( (String) datosEntrada.get("FC_FECNOTIF")).trim() +
              " 00:00:00";
          fec = (java.sql.Timestamp) cadena_a_timestamp(fecha);
          st.setTimestamp(5, fec);

          rs = st.executeQuery();

          // extrae los registros encontrados
          while (rs.next()) {

            henfnum = new Hashtable();
            henfnum.put("CD_ENFCIE", rs.getString("CD_ENFCIE"));
            henfnum.put("DS_ENFEREDO", rs.getString("DS_PROCESO"));
            henfnum.put("CD_TVIGI", rs.getString("CD_TVIGI"));

            // a�ade un nodo

            //listaSalida.addElement("");
            listaSalida.addElement(henfnum);
            //NumLinWhile = null;
          } //fin while

          rs.close();
          rs = null;
          st.close();
          st = null;

          break;

        case servletMODIF:

          datosEntrada = new Hashtable();
          datosEntrada = (Hashtable) param.firstElement();
          sQuery = null;
          // lanza la query
          /*  sQuery = " select "    // FALTAN LAS FECHAS
            + " C.CD_ENFCIE, A.DS_ENFEREDO, B.NM_CASOS, C.CD_TVIGI "
            + " from SIVE_EDO_CNE A, SIVE_EDONUM B, SIVE_ENFEREDO C "
            + " where A.CD_ENFEREDO in (select C.CD_ENFERE from SIVE_ENFEREDO "
            + " where C.CD_TVIGI IN ('A', 'N', 'X') AND C.IT_BAJA <> 'S' AND "
            + " C.CD_ENFCIE IN (SELECT B.CD_ENFCIE FROM SIVE_EDONUM "
            + " where B.CD_ENFCIE = C.CD_ENFCIE AND B.CD_E_NOTIF = ? and B.CD_ANOEPI = ? and B.CD_SEMEPI = ? "
            + " and TO_CHAR(B.FC_RECEP, 'YYYYMMDD') = ? "
            + " and TO_CHAR(B.FC_FECNOTIF,'YYYYMMDD') = ? )) "; */

          /*
                     sQuery = " select "
              + " C.CD_ENFCIE, A.DS_PROCESO, B.NM_CASOS, C.CD_TVIGI "
              + " from SIVE_PROCESOS A, SIVE_EDONUM B, SIVE_ENFEREDO C "
              + " where A.CD_ENFCIE in (select C.CD_ENFCIE from SIVE_ENFEREDO "
               + " where C.CD_TVIGI IN ('A', 'N', 'X') AND C.IT_BAJA <> 'S' AND "
              + " C.CD_ENFCIE IN (SELECT B.CD_ENFCIE FROM SIVE_EDONUM "
              + " where B.CD_ENFCIE = C.CD_ENFCIE AND B.CD_E_NOTIF = ? and B.CD_ANOEPI = ? and B.CD_SEMEPI = ? "
              + " and TO_CHAR(B.FC_RECEP, 'YYYYMMDD') = ? "
              + " and TO_CHAR(B.FC_FECNOTIF,'YYYYMMDD') = ? )) "
              + "order by A.DS_PROCESO";
           */

          // ARG: sin to_char
          /*sQuery = " select "
              + " C.CD_ENFCIE, A.DS_PROCESO, B.NM_CASOS, C.CD_TVIGI "
              + " from SIVE_PROCESOS A, SIVE_EDONUM B, SIVE_ENFEREDO C "
              + " where A.CD_ENFCIE in (select C.CD_ENFCIE from SIVE_ENFEREDO "
               + " where C.CD_TVIGI IN ('A', 'N', 'X') AND C.IT_BAJA <> 'S' AND "
              + " C.CD_ENFCIE IN (SELECT B.CD_ENFCIE FROM SIVE_EDONUM "
              + " where B.CD_ENFCIE = C.CD_ENFCIE AND B.CD_E_NOTIF = ? and B.CD_ANOEPI = ? and B.CD_SEMEPI = ? "
              + " and B.FC_RECEP = ? "
              + " and B.FC_FECNOTIF = ? )) "
              + "order by A.DS_PROCESO";*/

          sQuery = " select C.CD_ENFCIE, " +
              " A.DS_PROCESO, " +
              " B.NM_CASOS, " +
              " C.CD_TVIGI " +
              " from SIVE_PROCESOS A, " +
              " SIVE_EDONUM B, " +
              " SIVE_ENFEREDO C " +
              " where A.CD_ENFCIE = C.CD_ENFCIE " +
              " and C.CD_TVIGI IN ('A', 'N', 'X') " +
              " and C.IT_BAJA <> 'S' " +
              " and C.CD_ENFCIE = B.CD_ENFCIE " +
              " and B.CD_E_NOTIF = ? " +
              " and B.CD_ANOEPI = ? " +
              " and B.CD_SEMEPI = ? " +
              " and B.FC_RECEP = ? " +
              " and B.FC_FECNOTIF = ? " +
              " order by A.DS_PROCESO ";

          st = con.prepareStatement(sQuery);

          st.setString(1, (String) datosEntrada.get("CD_E_NOTIF"));
          st.setString(2, (String) datosEntrada.get("CD_ANOEPI"));
          st.setString(3, (String) datosEntrada.get("CD_SEMEPI"));

          // ARG: no se pueden usar to_char
          /* // Preparamos la fecha pa que funcione
                         fecha = ((String)datosEntrada.get("FC_RECEP")).trim();
                         sTok = new StringTokenizer(fecha,"/");
                         s1 = "";
                         s2 = "";
                         s3 = "";
                         s1 = sTok.nextToken();
                         s2 = sTok.nextToken();
                         s3 = sTok.nextToken();
                         // As� s� que pita
                         st.setString(4, s3+s2+s1);
           */

          // ARG: sin to_char
          fecha = ( (String) datosEntrada.get("FC_RECEP")).trim() + " 00:00:00";
          fec = (java.sql.Timestamp) cadena_a_timestamp(fecha);
          st.setTimestamp(4, fec);

          // ARG: no se pueden usar to_char
          /* // Hacemos lo mismo por segunda vez
               fecha = ((String)datosEntrada.get("FC_FECNOTIF")).trim();
                         sTok = new StringTokenizer(fecha,"/");
                         s1="";
                         s2="";
                         s3="";
                         s1 = sTok.nextToken();
                         s2 = sTok.nextToken();
                         s3 = sTok.nextToken();
                         // Pues as� deber�a pitar
                         st.setString(5, s3+s2+s1);
           */

          // ARG: sin to_char
          fecha = ( (String) datosEntrada.get("FC_FECNOTIF")).trim() +
              " 00:00:00";
          fec = (java.sql.Timestamp) cadena_a_timestamp(fecha);
          st.setTimestamp(5, fec);

          rs = st.executeQuery();

          // extrae los registros encontrados
          while (rs.next()) {

            henfnum = new Hashtable();
            henfnum.put("CD_ENFCIE", rs.getString("CD_ENFCIE"));
            henfnum.put("DS_ENFEREDO", rs.getString("DS_PROCESO"));
            henfnum.put("CD_TVIGI", rs.getString("CD_TVIGI"));
            henfnum.put("NM_CASOS", rs.getString("NM_CASOS"));

            // a�ade un nodo

            //listaSalida.addElement("");
            listaSalida.addElement(henfnum);
            //NumLinWhile = null;
          } //fin while

          rs.close();
          rs = null;
          st.close();
          st = null;

          break;

      } //fin switch

      // valida la transacci�n
      con.commit();

      // fall� la transacci�n
    }
    catch (Exception ex) {
      con.rollback();
      listaSalida = null;
      throw ex;
    }
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    listaDevolver = new CLista();
    listaDevolver.addElement( (CLista) listaSalida);
    return listaDevolver;

  } //fin doPost

//_______________________________________________________________________

  /** prueba del servlet */
  /*
    public CLista doPrueba(int operacion, CLista parametros) throws Exception {
      jdcdriver = new JDCConnectionDriver ("oracle.jdbc.driver.OracleDriver",
                             "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                             "pista_desa",
                             "pista_desa");
      Connection con = null;
      con = openConnection();
      return doWork(operacion, parametros);
    } */

} //fin de la clase
