//Title:        Notificacion datos adicionales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Notificacion de enfermedades EDO de declaracion numerica
//que necesitan datos adicionales.

package notadic;

//import java.sql.*:
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Cursor;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.LabelControl;
import com.borland.jbcl.control.StatusBar;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import sapp.StubSrvBD;

public class dialogoNotAdic
    extends CDialog {

  //modos de operaci�n de la ventana
  final public int modoNORMAL = 0;
  ResourceBundle res = ResourceBundle.getBundle("notadic.Res" + app.getIdioma());
  final public int modoESPERA = 1;

  //este es el modoNORMAL final public int modoCONSULTA = 2;
  final public int modoVISUAL = 2;

  final int MAS = 3;
  final int MENOS = 4;
  final int MAS_MENOS = 5;
  final int NO = 6;

  int ultBoton = NO;

  // modo de operaci�n
  public int modoOperacion = modoNORMAL;

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  // im�genes
  final String imgNAME[] = {
      "images/refrescar.gif",
      "images/alta2.gif",
      "images/baja2.gif",
      "images/modificacion2.gif",
      "images/primero.gif",
      "images/anterior.gif",
      "images/siguiente.gif",
      "images/ultimo.gif",
      "images/auxiliar.gif",
      "images/aceptar.gif",
      "images/cancelar.gif",
      "images/salir.gif"};

  // servlet
  protected CLista lista = new CLista();
  final String strSERVLET = "servlet/SrvNotAdic";
  protected StubSrvBD stubCliente = new StubSrvBD();

  final int servletPARTES = 2;

  // para los partes
  int totalPartes = 0;
  int parteAct = 1;
  datosParte parte = null;
  int casos = 0;
  String cd_enf = "";
  String ds_enf = "";
  String equipo = "";
  String ano = "";
  String semana = "";
  String fRecep = "";
  String fNotif = "";

  String nivel1 = "";
  String nivel2 = "";
  String Ca = "";
  String ope = "";
  String fUltAct = "";

  public Vector listaPartes = new Vector();

  public panelInfNotAdic panelInf = null;
  public boolean cerrar = false;

  CMessage msgBox;

  ScrollPane panelScroll = new ScrollPane();

  XYLayout xYLayout1 = new XYLayout();
  ButtonControl btnAnadir = new ButtonControl();
  ButtonControl btnModificar = new ButtonControl();
  ButtonControl btnBorrar = new ButtonControl();
  ButtonControl btnPrimero = new ButtonControl();
  ButtonControl btnAnterior = new ButtonControl();
  ButtonControl btnSiguiente = new ButtonControl();
  ButtonControl btnUltimo = new ButtonControl();
  ButtonControl btnSalir = new ButtonControl();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  LabelControl lblEnf = new LabelControl();
  LabelControl lblCasos = new LabelControl();
  LabelControl lblPartes = new LabelControl();

  public StatusBar barra = new StatusBar();

  dialogoNotAdicbtnNormalActionListener btnNormalActionListener = new
      dialogoNotAdicbtnNormalActionListener(this);

  // constructor
  public dialogoNotAdic(CApp a, parNotAdic par) { //, DataProtocolo dat) {

    super(a);
    try {
      //datosproto = dat;

      casos = par.totalCasos;
      cd_enf = par.cd_enf;
      ds_enf = par.ds_enf;

      equipo = par.equipo;
      ano = par.ano;
      semana = par.semana;
      fRecep = par.fRecep;
      fNotif = par.fNotif;

      nivel1 = par.nivel1;
      nivel2 = par.nivel2;
      Ca = par.Ca;
      //ope = par.ope;
      //fUltAct = par.fUltAct;

      modoOperacion = par.modo;

      //prueba(par);

      // Buscar los partes
      recuperarPartes(par);

      //totalPartes = listaPartes.size();

      if (listaPartes.size() > 0) {
        parte = (datosParte) listaPartes.elementAt(0);
        parte.nivel1 = nivel1;
        parte.nivel2 = nivel2;
        parte.Ca = Ca;
        //parte.ope = ope;
        //parte.fUltAct = fUltAct;

        parteAct = 1;

      }
      else {
        parte = new datosParte(par.cd_enf, -1, "E",
                               par.ano, par.semana, par.equipo,
                               par.fNotif, par.fRecep,
                               par.nivel1, par.nivel2, par.Ca,
                               "", "");

        parteAct = 0;

        barra.setText(res.getString("barra.Text"));
      }

      panelInf = new panelInfNotAdic(a, parte, par.totalCasos, this);

      jbInit();

      Inicializar();

      if (listaPartes.size() == 0) {
        parte = new datosParte();

        parteAct = 0;

        barra.setText(res.getString("barra.Text1"));
      }

      // mientras no se pulse el boton de a�adir o el de modificar,
      // los campos del informe deberian estar deshabilitados

      panelInf.deshabilitarCampos(Cursor.DEFAULT_CURSOR);

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicializa el aspecto del panel
  public void jbInit() throws Exception {

    btnAnadir.setActionCommand("a�adir");
    btnModificar.setActionCommand("modificar");
    btnBorrar.setActionCommand("borrar");
    btnPrimero.setActionCommand("primero");
    btnAnterior.setActionCommand("anterior");
    btnSiguiente.setActionCommand("siguiente");
    btnUltimo.setActionCommand("ultimo");
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");
    btnSalir.setActionCommand("salir");

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();
    btnAnadir.setImage(imgs.getImage(1));
    btnBorrar.setImage(imgs.getImage(2));
    btnModificar.setImage(imgs.getImage(3));
    btnPrimero.setImage(imgs.getImage(4));
    btnAnterior.setImage(imgs.getImage(5));
    btnSiguiente.setImage(imgs.getImage(6));
    btnUltimo.setImage(imgs.getImage(7));
    btnAceptar.setImage(imgs.getImage(9));
    btnCancelar.setImage(imgs.getImage(10));
    btnSalir.setImage(imgs.getImage(11));

    btnSalir.setLabel(res.getString("btnSalir.Label"));

    lblEnf.setText(res.getString("lblEnf.Text") + ds_enf);
    lblCasos.setText(res.getString("lblCasos.Text") + casos);
    lblPartes.setText(res.getString("lblPartes.Text") + totalPartes);

    panelScroll.setSize(585, 300); //575
    panelScroll.add(panelInf); //panel informe datos adicionales

    xYLayout1.setWidth(615);
    xYLayout1.setHeight(535);
    this.setLayout(xYLayout1);

    this.setSize(615, 535);

    this.setTitle(res.getString("this.Title"));

    this.add(lblEnf, new XYConstraints(20, 20, -1, -1));
    this.add(lblCasos, new XYConstraints(20, 40, -1, -1));
    this.add(lblPartes, new XYConstraints(20, 60, -1, -1));
    this.add(panelScroll, new XYConstraints(20, 100, 585, 300)); //555

    this.add(btnAnadir, new XYConstraints(65, 420, 24, 24));
    this.add(btnModificar, new XYConstraints(105, 420, 24, 24));
    this.add(btnBorrar, new XYConstraints(145, 420, 24, 24));

    this.add(btnPrimero, new XYConstraints(245, 420, -1, -1));
    this.add(btnAnterior, new XYConstraints(285, 420, -1, -1));
    this.add(btnSiguiente, new XYConstraints(325, 420, -1, -1));
    this.add(btnUltimo, new XYConstraints(365, 420, -1, -1));

    this.add(btnAceptar, new XYConstraints(465, 420, 24, 24));
    this.add(btnCancelar, new XYConstraints(505, 420, 24, 24));
    this.add(barra, new XYConstraints(20, 470, 475, -1));
    this.add(btnSalir, new XYConstraints(505, 470, -1, -1));

    btnPrimero.addActionListener(btnNormalActionListener);
    btnAnterior.addActionListener(btnNormalActionListener);
    btnSiguiente.addActionListener(btnNormalActionListener);
    btnUltimo.addActionListener(btnNormalActionListener);

    btnAnadir.addActionListener(btnNormalActionListener);
    btnModificar.addActionListener(btnNormalActionListener);
    btnBorrar.addActionListener(btnNormalActionListener);

    btnAceptar.addActionListener(btnNormalActionListener);
    btnCancelar.addActionListener(btnNormalActionListener);
    btnSalir.addActionListener(btnNormalActionListener);

  }

  public void Inicializar() {

    // deshabilitar los campos del informe

    switch (modoOperacion) {

      case modoNORMAL:

        // modo modificaci�n y baja
        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);

        btnAnadir.setEnabled(true);
        if (parteAct == 0) {
          btnModificar.setEnabled(false);
          btnBorrar.setEnabled(false);
        }
        else {
          btnModificar.setEnabled(true);
          btnBorrar.setEnabled(true);
        }
        btnCancelar.setEnabled(true);
        btnAceptar.setEnabled(true);
        btnSalir.setEnabled(true);

        panelInf.button1.setEnabled(false);
        panelInf.button2.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        barra.setText(res.getString("barra.Text2") + parteAct);

        break;

      case modoVISUAL:

        // modo en que no se permiten realizar modificaciones
        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);

        btnAnadir.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBorrar.setEnabled(false);

        btnCancelar.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnSalir.setEnabled(false);

        panelInf.button1.setEnabled(false);
        panelInf.button2.setEnabled(false);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:

        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);

        btnAnadir.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBorrar.setEnabled(false);

        btnCancelar.setEnabled(false);
        btnAceptar.setEnabled(false);
        btnSalir.setEnabled(false);

        panelInf.button1.setEnabled(false);
        panelInf.button2.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
    this.doLayout();

  }

  public void recuperarPartes(parNotAdic par) {

    CLista data;

    try {

      data = new CLista();
      data.addElement(par);
      stubCliente.setUrl(new URL(app.getURL() + strSERVLET));
      data = (CLista) stubCliente.doPost(servletPARTES, data);
      if (data != null) {
        listaPartes = (Vector) data.elementAt(0);
      }
      else {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg1.Text"));
        msgBox.show();
        msgBox = null;
      }

      //SimpleDateFormat formUltAct = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
      for (int i = 0; i < listaPartes.size(); i++) {
        datosParte part = (datosParte) listaPartes.elementAt(i);
        part.nivel1 = nivel1;
        part.nivel2 = nivel2;
        part.Ca = Ca;

      }

      totalPartes = listaPartes.size();

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.toString());
      msgBox.show();
      msgBox = null;
    }
  }

  // a�ade un parte
  public void btnAnadir_actionPerformed() {
    //deshabilitar botones
    btnAnadir.setEnabled(false);
    btnModificar.setEnabled(false);
    btnBorrar.setEnabled(false);

    btnPrimero.setEnabled(false);
    btnAnterior.setEnabled(false);
    btnSiguiente.setEnabled(false);
    btnUltimo.setEnabled(false);

    btnCancelar.setEnabled(true);
    btnAceptar.setEnabled(true);
    btnSalir.setEnabled(false);

    //comprobar que el numero de partes sea menor o igual
    //que el numero de casos
    if (totalPartes + 1 > casos) {
      msgBox = new CMessage(this.app, CMessage.msgERROR,
                            res.getString("msg2.Text"));
      msgBox.show();
      msgBox = null;
      btnCancelar_actionPerformed();
    }
    else {
      ultBoton = MAS;

      //habilitar los campos del informe
      panelInf.cambiarEstado(0); //Alta
      panelInf.habilitarCampos(); //habilito
      //panelInf.descargarChoices();//limpio choice
      panelInf.limpiarRespuestas(); //select(0)+ modoCondicionadas
      //se muestra un informe en blanco y se guardan los datos
      barra.setText(res.getString("barra.Text2") + (totalPartes + 1));

      panelInf.button1.setEnabled(true);
      panelInf.button2.setEnabled(false);

    }

  }

  // modifica parte
  public void btnModificar_actionPerformed() {
    // se modifica el parte que se estaba viendo en ese momento

    //deshabilitar botones
    btnAnadir.setEnabled(false);
    btnModificar.setEnabled(false);
    btnBorrar.setEnabled(false);

    btnPrimero.setEnabled(false);
    btnAnterior.setEnabled(false);
    btnSiguiente.setEnabled(false);
    btnUltimo.setEnabled(false);

    btnCancelar.setEnabled(true);
    btnAceptar.setEnabled(true);
    btnSalir.setEnabled(false);

    panelInf.button1.setEnabled(true);
    panelInf.button2.setEnabled(false);

    if (parteAct != 0) {
      ultBoton = MAS_MENOS;
      panelInf.cambiarEstado(1); //Modificacion
      panelInf.habilitarCampos();
      //panelInf.descargarChoices();//limpio choice

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            res.getString("msg3.Text"));
      msgBox.show();
      msgBox = null;

      modoOperacion = modoNORMAL;
      Inicializar();
      panelInf.deshabilitarCampos(Cursor.DEFAULT_CURSOR);
    }
  }

  // borra parte
  public void btnBorrar_actionPerformed() {
    // se borra el parte que se esta visualizando

    //deshabilitar botones
    btnAnadir.setEnabled(false);
    btnModificar.setEnabled(false);
    btnBorrar.setEnabled(false);

    btnPrimero.setEnabled(false);
    btnAnterior.setEnabled(false);
    btnSiguiente.setEnabled(false);
    btnUltimo.setEnabled(false);

    btnCancelar.setEnabled(false);
    btnAceptar.setEnabled(false);
    btnSalir.setEnabled(false);

    panelInf.button1.setEnabled(false);
    panelInf.button2.setEnabled(false);

    boolean borrado = false;

    if (parteAct != 0) {
      msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                            res.getString("msg4.Text") + parteAct + " ?");
      msgBox.show();
      if (msgBox.getResponse()) {
        //borrar
        ultBoton = MENOS;
        barra.setText(res.getString("barra.Text3") + parteAct);
        borrado = panelInf.borrarParte();
        if (borrado) {
          modoOperacion = modoESPERA;
          Inicializar();
          panelInf.deshabilitarCampos(Cursor.WAIT_CURSOR);
          listaPartes.removeElementAt(parteAct - 1);

          totalPartes = totalPartes - 1;

          if (totalPartes == 0) {

            parteAct = 0;

            panelInf.cambiarEstado(0); //Alta
            panelInf.habilitarCampos(); //habilito
            //panelInf.descargarChoices();//limpio choice
            panelInf.limpiarRespuestas(); //select(0)+ modoCondicionadas
          }
          else
          if (parteAct == totalPartes + 1) {
            parteAct = parteAct - 1;
            panelInf.mostrarParte( (datosParte) listaPartes.elementAt(parteAct -
                1));
          }
          else {
            panelInf.mostrarParte( (datosParte) listaPartes.elementAt(parteAct -
                1));
          }

          lblPartes.setText(res.getString("lblPartes.Text") + totalPartes);
          ultBoton = NO;
        }
        else {
          btnCancelar_actionPerformed();
        }
      }
      else {
        //no borrar
        btnCancelar_actionPerformed();
      }
      msgBox = null;
    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            res.getString("msg3.Text"));
      msgBox.show();
      msgBox = null;
    }

    if (parteAct == 0) {
      barra.setText(res.getString("barra.Text1"));
    }

    modoOperacion = modoNORMAL;
    Inicializar();
    panelInf.deshabilitarCampos(Cursor.DEFAULT_CURSOR);

  }

  // muestra el primer parte
  public void btnPrimero_actionPerformed() {
    //Comprobar si hay algun parte
    if (listaPartes.size() > 0) { //(totalPartes > 0){
      //mostrar el primer parte de la lista
      panelInf.cambiarEstado(1); //modificacion
      panelInf.mostrarParte( (datosParte) listaPartes.elementAt(0));
      parteAct = 1;
      barra.setText(res.getString("barra.Text2") + parteAct);
    }
  }

  // muestra el parte anterior al actual
  public void btnAnterior_actionPerformed() {
    //Comprobar si hay algun parte anterior
    if (parteAct - 1 - 1 >= 0) {
      //mostrar el parte anterior de la lista
      panelInf.cambiarEstado(1); //modificacion
      panelInf.mostrarParte( (datosParte) listaPartes.elementAt(parteAct - 1 -
          1));
      parteAct = parteAct - 1;
      barra.setText(res.getString("barra.Text2") + parteAct);
    }
  }

  // muestra el parte siguiente al actual
  public void btnSiguiente_actionPerformed() {
    //Comprobar si hay algun parte
    if (listaPartes.size() > parteAct + 1 - 1) { //(totalPartes>=parteAct+1-1){
      //mostrar el ultimo parte de la lista
      panelInf.cambiarEstado(1); //modificacion
      panelInf.mostrarParte( (datosParte) listaPartes.elementAt(parteAct + 1 -
          1));
      parteAct++;
      barra.setText(res.getString("barra.Text2") + parteAct);
    }
  }

  // muestra el ultimo parte
  public void btnUltimo_actionPerformed() {
    if (listaPartes.size() != parteAct) { //(totalPartes!=parteAct){
      //mostrar el ultimo parte de la lista
      panelInf.cambiarEstado(1); //modificacion
      panelInf.mostrarParte( (datosParte) listaPartes.elementAt(listaPartes.
          size() - 1));
      totalPartes = listaPartes.size();
      parteAct = totalPartes;
      barra.setText(res.getString("barra.Text2") + parteAct);
    }

  }

  public void btnAceptarDespuesdeActPrtocolo_actionPerformed() {
    modoOperacion = modoESPERA;
    Inicializar();
    panelInf.deshabilitarCampos(Cursor.WAIT_CURSOR);

    if (ultBoton == MAS) {
      datosParte auxP = (datosParte) listaPartes.elementAt(listaPartes.size() -
          1);
      listaPartes.removeElementAt(listaPartes.size() - 1);
      panelInf.Actualizar_Todos();
      panelInf.parteValido = auxP;
      datosParte nuevo = panelInf.insertarParte();
      //cris
      if (nuevo != null) {
        nuevo.nivel1 = nivel1;
        nuevo.nivel2 = nivel2;
        nuevo.Ca = Ca;
        listaPartes.addElement(nuevo);
        totalPartes = listaPartes.size();
        lblPartes.setText(res.getString("lblPartes.Text") + totalPartes);
        parteAct = totalPartes; //parteAct++;

        barra.setText(res.getString("barra.Text2") + parteAct);
      }
    }
    else {
      panelInf.Actualizar_Todos();

    }
    modoOperacion = modoNORMAL;
    Inicializar();
    panelInf.deshabilitarCampos(Cursor.DEFAULT_CURSOR);
  }

  // graba el parte actual
  public void btnAceptar_actionPerformed() {
    if (panelInf.bPulsarAceptarOCancelar) {
      btnAceptarDespuesdeActPrtocolo_actionPerformed();
      panelInf.bPulsarAceptarOCancelar = false;
      panelInf.NIVEL = "";
      ultBoton = NO;
      return;
    }

    datosParte nuevo = new datosParte();

    //actualizar el dialogo
    switch (ultBoton) {
      case MAS:
        modoOperacion = modoESPERA;
        Inicializar();

        panelInf.deshabilitarCampos(Cursor.WAIT_CURSOR);
        barra.setText(res.getString("barra.Text4") + (totalPartes + 1));

        //guardar los datos del informe
        boolean b = panelInf.ValidarObligatorios();
        if (b) {
          nuevo = panelInf.insertarParte();
          //cris
          if (nuevo != null) {
            totalPartes++;
            lblPartes.setText(res.getString("lblPartes.Text") + totalPartes);
            parteAct++;
            nuevo.nivel1 = nivel1;
            nuevo.nivel2 = nivel2;
            nuevo.Ca = Ca;
            listaPartes.addElement(nuevo);
          }

          panelInf.button1.setEnabled(false);
          panelInf.button2.setEnabled(true);

          modoOperacion = modoNORMAL;
          Inicializar();
          panelInf.deshabilitarCampos(Cursor.DEFAULT_CURSOR);
          ultBoton = NO;

        }
        else {
          //Porque machacar los datos metidos???? Preguntar a Cris
          //panelInf.mostrarParte((datosParte)listaPartes.elementAt(totalPartes-1));

          //para que este todo como justo antes de dar a aceptar
          modoOperacion = modoNORMAL;
          Inicializar();

          barra.setText(res.getString("barra.Text2") + (parteAct + 1));

          panelInf.habilitarCampos();

          btnAnadir.setEnabled(false);
          btnModificar.setEnabled(false);
          btnBorrar.setEnabled(false);

          btnPrimero.setEnabled(false);
          btnAnterior.setEnabled(false);
          btnSiguiente.setEnabled(false);
          btnUltimo.setEnabled(false);

          btnCancelar.setEnabled(true);
          btnAceptar.setEnabled(true);
          btnSalir.setEnabled(false);

          panelInf.button1.setEnabled(true);
          panelInf.button2.setEnabled(false);

        }

        //no se muy bine donde va
        //panelInf.button1.setEnabled(false);
        //panelInf.button2.setEnabled(true);

        break;
      case MENOS:

        // borrar los datos del informe
        // lo hago con la respuesta al dialogo de confirmacion
        /*
                 barra.setText(res.getString(" barra.Text3") + parteAct);
                 listaPartes.removeElementAt(parteAct-1);
                 totalPartes = totalPartes - 1;
                 if (totalPartes==0)
          parteAct = 0;
                 else
          parteAct = parteAct - 1;
             lblPartes.setText(res.getString("lblPartes.Text") + totalPartes );
                 ultBoton = NO;
         */
        break;

      case MAS_MENOS:

        // modificar los datos del informe
        modoOperacion = modoESPERA;
        Inicializar();

        panelInf.deshabilitarCampos(Cursor.WAIT_CURSOR);

        barra.setText(res.getString("barra.Text5") + parteAct);
        b = panelInf.ValidarObligatorios(); //parteValido se modifica, si ok
        if (b) {
          nuevo = panelInf.modificarParte(panelInf.parteValido);

          //--------- detallado porque no se recupera ----
          Vector listaPartesCopia = new Vector();
          for (int u = 0; u < listaPartes.size(); u++) {
            datosParte cadaparte = (datosParte) listaPartes.elementAt(u);
            listaPartesCopia.addElement(cadaparte);
          }

          listaPartes = new Vector();
          for (int u = 0; u < listaPartesCopia.size(); u++) {
            if (u != parteAct - 1) {
              datosParte cadaparte = (datosParte) listaPartesCopia.elementAt(u);
              listaPartes.addElement(cadaparte);
            }
            else {
              datosParte cadaparte = new datosParte(nuevo.cd_enf,
                  (new Integer(nuevo.secuencial)).intValue(),
                  "E", nuevo.ano, nuevo.semana,
                  nuevo.equipo, nuevo.fNotif, nuevo.fRecep,
                  nuevo.nivel1, nuevo.nivel2, nuevo.Ca,
                  nuevo.ope, nuevo.fUltAct);

              for (int y = 0; y < nuevo.preguntas.size(); y++) {
                datosPreg valores = (datosPreg) nuevo.preguntas.elementAt(y);
                cadaparte.preguntas.addElement(valores);
              }
              listaPartes.addElement(cadaparte);
            } //else
          }
          //----------

          panelInf.parteValido = nuevo;
          panelInf.button1.setEnabled(false);
          panelInf.button2.setEnabled(true);

          modoOperacion = modoNORMAL;
          Inicializar();
          panelInf.deshabilitarCampos(Cursor.DEFAULT_CURSOR);

          ultBoton = NO;
        }
        else {
          //cris
          //para que este todo como justo antes de dar a aceptar
          modoOperacion = modoNORMAL;
          Inicializar();

          panelInf.habilitarCampos();

          btnAnadir.setEnabled(false);
          btnModificar.setEnabled(false);
          btnBorrar.setEnabled(false);

          btnPrimero.setEnabled(false);
          btnAnterior.setEnabled(false);
          btnSiguiente.setEnabled(false);
          btnUltimo.setEnabled(false);

          btnCancelar.setEnabled(true);
          btnAceptar.setEnabled(true);
          btnSalir.setEnabled(false);

          panelInf.button1.setEnabled(true);
          panelInf.button2.setEnabled(false);
        }

        break;
    }
    /*
         modoOperacion = modoNORMAL;
         Inicializar();
         panelInf.deshabilitarCampos(Cursor.DEFAULT_CURSOR);
     */
  }

  public void btnCancelaDespuesdeActPrtocolo_actionPerformed() {
    //reserva y volver a pintar el layout y dejar en el actual
    panelInf.Volver_a_Reserva();
    panelInf.Volver_a_Abrir(); //LAYOUT
    modoOperacion = modoNORMAL;
    Inicializar();
    panelInf.habilitarCampos();
  }

  // ignora las modificaciones en el parte actual
  public void btnCancelar_actionPerformed() {
    if (panelInf.bPulsarAceptarOCancelar) {
      if (panelInf.iEstado == panelInf.ALTA) {

        //quito el que habia a�adido para visualizarlo
        listaPartes.removeElementAt(listaPartes.size() - 1);
      }
      btnCancelaDespuesdeActPrtocolo_actionPerformed();
      panelInf.bPulsarAceptarOCancelar = false;
      panelInf.NIVEL = "";
      ultBoton = NO;
      panelInf.deshabilitarCampos(Cursor.DEFAULT_CURSOR);
      return;
    }

    modoOperacion = modoNORMAL;
    Inicializar();
    panelInf.deshabilitarCampos(Cursor.DEFAULT_CURSOR);
    panelInf.cambiarEstado(1); //modificacion
    if (parteAct != 0) {
      panelInf.mostrarParte( (datosParte) listaPartes.elementAt(parteAct - 1));
    }
  }

  // sale del dialogo
  public void btnSalir_actionPerformed() {
    dispose();
  }

  //recupera para mlm el total de partes
  public int gettotalPartes() {
    return (totalPartes);
  }

  public CLista getListaValores() {
    CLista lista = new CLista(); //lista de Vectores
    for (int i = 0; i < listaPartes.size(); i++) {
      datosParte cadaparte = (datosParte) listaPartes.elementAt(i);
      lista.addElement(cadaparte.preguntas);
    }
    return (lista);
  } //fin de getListaValores

  //numparte: 0, 1, 2, 3 ...
  public void Cambiar_listaPartes(int numparte, Vector Valores) {
    datosParte cadaparte = (datosParte) listaPartes.elementAt(numparte);
    cadaparte.preguntas = Valores;
  } //fin de  Cambiar_listaPartes

  public Vector CadalistaValores(int numparte) {
    datosParte cadaparte = (datosParte) listaPartes.elementAt(numparte);
    return (cadaparte.preguntas);
  } //fin de  CadalistaValores

} //fin clase

    /******************************************************************************/

// action listener de evento en botones normales( No implementa Runnable)
class dialogoNotAdicbtnNormalActionListener
    implements ActionListener {
  dialogoNotAdic adaptee = null;
  ActionEvent e = null;

  public dialogoNotAdicbtnNormalActionListener(dialogoNotAdic adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals("a�adir")) { // a�adir
      adaptee.btnAnadir_actionPerformed();
    }
    else if (e.getActionCommand().equals("modificar")) { // modificar
      adaptee.btnModificar_actionPerformed();
    }
    else if (e.getActionCommand().equals("borrar")) { // borrar
      adaptee.btnBorrar_actionPerformed();
    }
    else if (e.getActionCommand() == "primero") {
      adaptee.btnPrimero_actionPerformed();
    }
    else if (e.getActionCommand() == "anterior") {
      adaptee.btnAnterior_actionPerformed();
    }
    else if (e.getActionCommand() == "siguiente") {
      adaptee.btnSiguiente_actionPerformed();
    }
    else if (e.getActionCommand() == "ultimo") {
      adaptee.btnUltimo_actionPerformed();
    }
    else if (e.getActionCommand() == "aceptar") {
      adaptee.btnAceptar_actionPerformed();
    }
    else if (e.getActionCommand() == "cancelar") {
      adaptee.btnCancelar_actionPerformed();
    }
    else if (e.getActionCommand() == "salir") {
      adaptee.btnSalir_actionPerformed();
    }
  }
}
