//Title:        Notificacion datos adicionales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Notificacion de enfermedades EDO de declaracion numerica
//que necesitan datos adicionales.

package notadic;

import java.util.Vector;

import capp.CApp;
import capp.CLista;
import infprotoedo.DataLayout;

public class appPrueba
    extends CApp {

  public appPrueba() {
  }

  public void init() {
    super.init();
  }

  public void start() {

    setTitulo("prueba para notificaciones con datos adicionales");
    CApp a = (CApp)this;

    parNotAdic par = new parNotAdic(0, "42", "Botulismo",
                                    8, 0, "1999", "25",
                                    "H01B", "26/06/1999", "23/07/1999",
                                    "1", "", "12");

    VerPanel("prueba para notificaciones con datos adicionales");
    dialogoNotAdic dial = new dialogoNotAdic(a, par);
    dial.show();

  }

  //para probar

  public CLista Recomponer_RespEDO(Vector listaResp, CLista listaPlantilla) {
    CLista listaResp2 = new CLista();

    //encontrar preguntas iguales
    for (int i = 0; i < listaResp.size(); i++) {
      datosPreg dataObsoleto = (datosPreg) listaResp.elementAt(i);

      for (int j = 0; j < listaPlantilla.size(); j++) {
        DataLayout dataActivo = (DataLayout) listaPlantilla.elementAt(j);
        if (dataActivo.getCodPregunta() != null) {
          if (dataObsoleto.pregunta.trim().equals(dataActivo.getCodPregunta().
                                                  trim())) {

            Integer Ilinea = new Integer(dataActivo.getNumLinea());
            int linea = Ilinea.intValue();
            datosPreg dataInsertar = new datosPreg(linea,
                dataActivo.getCodPregunta(),
                dataObsoleto.respuesta,
                dataActivo.getCodModelo(),
                dataObsoleto.ValorLista,
                dataObsoleto.nivel,
                "S", dataActivo.getCA(),
                dataActivo.getNIVEL_1(),
                dataActivo.getNIVEL_2());

            listaResp2.addElement(dataInsertar);
          }
        }
      }
    } //for ----------------------

    //Ver si las que estan condicionadas deben aparecer o no en la
    //lista de valores nueva

    CLista listaRespResul = new CLista();
    DataLayout layPreg = null;
    String linCond = "";
    String codCond = "";
    String valorCond = "";
    datosPreg preg = null;
    datosPreg preg1 = null;

    CLista lisCopia = new CLista();
    for (int t = 0; t < listaResp2.size(); t++) {
      lisCopia.addElement( (datosPreg) listaResp2.elementAt(t));
    }

    for (int t = 0; t < listaPlantilla.size(); t++) {
      layPreg = (DataLayout) listaPlantilla.elementAt(t);
      if ( (!layPreg.getTipoPreg().equals("X")) &&
          (!layPreg.getTipoPreg().equals("D"))) {
        if (layPreg.getCondicionada().equals("S")) {
          //busco el valor de la pregunta condicionante
          preg = buscarPregListaValores(layPreg.getCodPreguntaCond(), lisCopia);
          if (preg != null) {
            if (!preg.respuesta.equals(layPreg.getDesPreguntaCond())) {
              // se borra de la lista de valores
              preg1 = buscarPregListaValores(layPreg.getCodPregunta(), lisCopia);
              listaResp2.removeElement(preg1);
            }
          }
          else {
            // se borra de la lista de valores
            preg1 = buscarPregListaValores(layPreg.getCodPregunta(), lisCopia);
            listaResp2.removeElement(preg1);
          }
        }
      }
    }

    return (listaResp2);
  }

  private datosPreg buscarPregListaValores(String cod, CLista lista) {
    datosPreg preg = null;
    for (int i = 0; i < lista.size(); i++) {
      preg = (datosPreg) lista.elementAt(i);
      if (preg.pregunta.equals(cod)) {
        return preg;
      }
    }
    return null;
  }

}
