//Title:        Notificacion datos adicionales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Notificacion de enfermedades EDO de declaracion numerica
//que necesitan datos adicionales.

package notadic;

import java.io.Serializable;
import java.util.Vector;

public class datosParte
    implements Serializable {

  public String cd_enf = "";
  public String equipo = "";
  public String ano = "";
  public String tSive = "";
  public String semana = "";
  public String fNotif = "";
  public String fRecep = "";
  public int secuencial = 0;

  // para el bloqueo
  public String ope = "";
  public String fUltAct = "";

  //info para el protocolo
  public String nivel1 = "";
  public String nivel2 = "";
  public String Ca = "";

  public Vector preguntas = new Vector();

  public datosParte() {}

  public datosParte(String cd, int sec, String ts,
                    String a, String s, String e,
                    String fn, String fr,
                    String n1, String n2, String ca,
                    String op, String fu) {
    cd_enf = cd;
    secuencial = sec;
    ano = a;
    semana = s;
    equipo = e;
    fNotif = fn;
    fRecep = fr;
    //modelo = mod;
    tSive = ts;

    nivel1 = n1;
    nivel2 = n2;
    Ca = ca;

    ope = op;
    fUltAct = fu;
  }

}
