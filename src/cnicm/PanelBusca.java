package cnicm;

import java.net.URL;
//import zbs.*;
//import notentrada.*;
//import notdata.*;
//import enfermo.comun;
import java.util.ResourceBundle;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.TextEvent;

import com.borland.jbcl.control.BevelPanel;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import capp.CTabla;
import catalogo2.CListaCat2;
import catalogo2.DataCat2;
import comun.Common;
import comun.DataMunicipioEDO;
import comun.DataZBS2;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import jclass.bwt.JCItemEvent;
import sapp.StubSrvBD;

public class PanelBusca
    extends CPanel {

  // variables usadas para saber provincia y municipio actuales
  protected String codProvi = "";
  ResourceBundle res;
  protected String codMuni = "";

  //Modos de operaci�n de la ventana
  final int modoALTA = 0;
  final int modoMODIFICAR = 1;
  final int modoESPERA = 2;

  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  final String strSERVLETcn = "servlet/SrvCN";
  final String strSERVLETNiv1 = "servlet/SrvCat2";
  final String strSERVLETzbs2 = "servlet/SrvZBS2";

  // modos de operaci�n del servlet SrvMaestroEDO
  final int servletSELECCION_MUNICIPIO_X_CODIGO = 9;
  final int servletOBTENER_MUNICIPIO_X_CODIGO = 10;
  final int servletSELECCION_MUNICIPIO_X_DESCRIPCION = 11;
  final int servletOBTENER_MUNICIPIO_X_DESCRIPCION = 12;

  //Constantes del panel
  final String strSERVLET_MAESTRO = "servlet/SrvMaestroEDO";
  //Para municipio
  final String strSERVLETMun = "servlet/SrvMunicipio";

  /** es el dato seleccionado */
  protected DataCN datCentroSeleccionado = null;

  XYLayout xYLayout1 = new XYLayout();
  Label lblCentro = new Label();
  Label lblProvincia = new Label();
  Label lblMunicipio = new Label();
  TextField txtCentroDesc = new TextField();
  CCampoCodigo txtCentro = new CCampoCodigo();
  CCampoCodigo txtProvincia = new CCampoCodigo();
  ButtonControl btnProvincia = new ButtonControl();
  CCampoCodigo txtMunicipio = new CCampoCodigo();
  ButtonControl btnMunicipio = new ButtonControl();
  Label lblCentroDesc = new Label();
  TextField txtDesProvincia = new TextField();
  TextField txtDesMunicipio = new TextField();
  ButtonControl btnBuscar = new ButtonControl();
  ButtonControl btnA�adir = null;
  ButtonControl btnModificar = null;
  ButtonControl btnPrimero = null;
  ButtonControl btnAnterior = null;
  ButtonControl btnSiguiente = null;
  ButtonControl btnUltimo = null;
  public CTabla tabla = new CTabla();
  URL u;

  //lista seleccionada
  final int lCentro = 0;
  final int lNivel1 = 1;
  final int lZBS2 = 2;

  // Stub's
  protected StubSrvBD stubCentro = null;
  protected StubSrvBD stubNivel1 = null;
  protected StubSrvBD stubZBS2 = null;
  protected StubSrvBD stubMun = null;

  /** para la sincronizaci�n de eventos */
  protected boolean sinBloquear = true;

  // bot�n pulsado
  public boolean acepto = false;

  // par�metros
  protected int modoOperacion = modoALTA;
  protected int modoAnt = modoALTA;

  // listas de resultados
  public CLista listaCentro = null;

  PanelBusca_btnBuscar_actionAdapter btn_actionAdapter = null;
  PanelBuscaTextFocusListener focusListener = null;
  BevelPanel bevelPanel1 = new BevelPanel();

  public PanelBusca(CApp a) {
    setApp(a);
    try {

      res = ResourceBundle.getBundle("cn.Res" + app.getIdioma());
      listaCentro = new CLista();
      jbInit();
      listaCentro.setState(CLista.listaNOVALIDA);

      u = app.getCodeBase();
      stubCentro = new StubSrvBD(new URL(this.app.getURL() + "servlet/SrvCN"));
      stubNivel1 = new StubSrvBD(new URL(this.app.getURL() +
                                         "servlet/SrvNivel1"));
      stubZBS2 = new StubSrvBD(new URL(this.app.getURL() + "servlet/SrvZBS2"));
      stubMun = new StubSrvBD(new URL(this.app.getURL() + strSERVLETMun));

      btnMunicipio.setEnabled(false);
      txtDesMunicipio.setEditable(false);
      txtDesMunicipio.setEnabled(false);
      txtDesProvincia.setEditable(false);
      txtDesProvincia.setEnabled(false);
      btnMunicipio.setFocusAware(false);
      btnProvincia.setFocusAware(false);
      txtCentro.requestFocus();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /** ete m�todo sirve para mantener una sincronizaci�n en los eventos */
  public synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      modoAnt = modoOperacion;
      modoOperacion = modoESPERA;
      Inicializar();

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  /** este m�todo desbloquea el sistema */
  public synchronized void desbloquea() {
    sinBloquear = true;
    modoOperacion = modoAnt;
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  void jbInit() throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        Common.imgLUPA,
        Common.imgREFRESCAR,
        Common.imgALTA2,
        Common.imgMODIFICAR2,
        Common.imgPRIMERO,
        Common.imgANTERIOR,
        Common.imgSIGUIENTE,
        Common.imgULTIMO};
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    xYLayout1.setHeight(346);
    xYLayout1.setWidth(563);
    this.setLayout(xYLayout1);

    lblCentro.setText(res.getString("lblCentro.Text"));
    lblProvincia.setText(res.getString("lblProvincia.Text"));
    lblMunicipio.setText(res.getString("lblMunicipio.Text"));
    lblCentroDesc.setText(res.getString("lblCentroDesc.Text"));

    txtProvincia.addTextListener(new PanelBusca_txtProvincia_textAdapter(this));
    txtProvincia.addKeyListener(new PanelBusca_txtProvincia_keyAdapter(this));
    txtProvincia.addActionListener(new PanelBusca_txtProvincia_actionAdapter(this));
    txtMunicipio.addKeyListener(new PanelBusca_txtMunicipio_keyAdapter(this));
    txtMunicipio.addActionListener(new PanelBusca_txtMunicipio_actionAdapter(this));

    btnProvincia = new ButtonControl(imgs.getImage(0));
    btnMunicipio = new ButtonControl(imgs.getImage(0));
    btnBuscar = new ButtonControl(imgs.getImage(1));
    btnA�adir = new ButtonControl(imgs.getImage(2));
    btnModificar = new ButtonControl(imgs.getImage(3));
    btnPrimero = new ButtonControl(imgs.getImage(4));
    btnAnterior = new ButtonControl(imgs.getImage(5));
    btnSiguiente = new ButtonControl(imgs.getImage(6));
    btnUltimo = new ButtonControl(imgs.getImage(7));

    btnBuscar.setActionCommand("buscar");
    btnA�adir.setActionCommand("a�adir");
    btnModificar.setActionCommand("modificar");
    btnPrimero.setActionCommand("primero");
    btnAnterior.setActionCommand("anterior");
    btnSiguiente.setActionCommand("siguiente");
    btnUltimo.setActionCommand("ultimo");

    btn_actionAdapter = new PanelBusca_btnBuscar_actionAdapter(this);
    focusListener = new PanelBuscaTextFocusListener(this);
    btnBuscar.setLabel(res.getString("btnBuscar.Label"));

    btnProvincia.addActionListener(new PanelBusca_btnProvincia_actionAdapter(this));
    btnMunicipio.addActionListener(new PanelBusca_btnMunicipio_actionAdapter(this));
    btnBuscar.addActionListener(btn_actionAdapter);
    btnA�adir.addActionListener(btn_actionAdapter);
    btnModificar.addActionListener(btn_actionAdapter);
    btnPrimero.addActionListener(btn_actionAdapter);
    btnAnterior.addActionListener(btn_actionAdapter);
    btnSiguiente.addActionListener(btn_actionAdapter);
    btnUltimo.addActionListener(btn_actionAdapter);

    txtMunicipio.setName("municipio");
    txtProvincia.setName("provincia");
    txtMunicipio.addFocusListener(focusListener);
    txtProvincia.addFocusListener(focusListener);

    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "125\n125\n125\n120"), '\n'));
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tabla.ColumnButtonsStrings")), '\n'));
    tabla.setNumColumns(4);
    bevelPanel1.setBevelOuter(BevelPanel.LOWERED);
    bevelPanel1.setBevelInner(BevelPanel.FLAT);
    tabla.addItemListener(new PanelBusca_tabla_actionAdapter(this));
    tabla.addActionListener(new PanelBusca_tabla_dobleClick(this));

    this.add(lblCentro, new XYConstraints(23, 5, 43, -1));
    this.add(lblProvincia, new XYConstraints(23, 67, 56, -1));
    this.add(lblMunicipio, new XYConstraints(23, 98, 56, -1));
    this.add(txtCentro, new XYConstraints(93, 5, 96, -1));
    this.add(txtCentroDesc, new XYConstraints(93, 36, 286, -1));
    this.add(txtProvincia, new XYConstraints(93, 67, 57, -1));
    this.add(btnProvincia, new XYConstraints(158, 67, 25, 25));
    this.add(txtDesProvincia, new XYConstraints(189, 67, 190, -1));
    this.add(txtMunicipio, new XYConstraints(93, 98, 58, -1));
    this.add(btnMunicipio, new XYConstraints(158, 98, 25, 25));
    this.add(txtDesMunicipio, new XYConstraints(189, 98, 190, -1));
    this.add(btnBuscar, new XYConstraints(465, 98, -1, -1));
    this.add(tabla, new XYConstraints(23, 138, 514, 124));
    this.add(lblCentroDesc, new XYConstraints(23, 36, 73, 26));
    this.add(btnA�adir, new XYConstraints(26, 270, 25, 25));
    this.add(btnModificar, new XYConstraints(64, 270, 25, 25));
    this.add(btnPrimero, new XYConstraints(373, 270, 25, 25));
    this.add(btnAnterior, new XYConstraints(415, 270, 25, 25));
    this.add(btnSiguiente, new XYConstraints(457, 270, 25, 25));
    this.add(btnUltimo, new XYConstraints(499, 270, 25, 25));
    this.add(bevelPanel1, new XYConstraints(24, 128, 512, 4));
    btnMunicipio.setEnabled(false);
    acepto = false;
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

    modoOperacion = modoALTA;
    Inicializar();
  }

  void btnProvincia_actionPerformed(ActionEvent e) {
    DataCat2 data;
    int modo = modoOperacion;
    CMessage msgBox = null;

    try {
      CListaCat2 lista = new CListaCat2(app,
                                        res.getString("msg9.Text"),
                                        stubNivel1,
                                        strSERVLETNiv1,
                                        servletOBTENER_X_CODIGO +
                                        catalogo.Catalogo.catCCAA,
                                        servletOBTENER_X_DESCRIPCION +
                                        catalogo.Catalogo.catCCAA,
                                        servletSELECCION_X_CODIGO +
                                        catalogo.Catalogo.catCCAA,
                                        servletSELECCION_X_DESCRIPCION +
                                        catalogo.Catalogo.catCCAA);
      lista.show();
      data = (DataCat2) lista.getComponente();

      if (data != null) {
        txtProvincia.setText(data.getCod());
        txtDesProvincia.setText(data.getDes());
        codProvi = data.getCod();
        if (txtDesProvincia.getText().length() > 0) {
          btnMunicipio.setEnabled(true);
        }
        else {
          btnMunicipio.setEnabled(false);
        }
        txtMunicipio.setText("");
        txtDesMunicipio.setText("");
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
      modoOperacion = modo;
    }

  }

  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:
        btnProvincia.setEnabled(true);
        btnProvincia.setSelected(false);
        btnProvincia.setEnabled(true); // JMT: ???
        btnBuscar.setEnabled(true);

        /*        if (txtDesProvincia.getText().length() > 0)
                  btnMunicipio.setEnabled(true);
                else
                  btnMunicipio.setEnabled(false);*/

// JMT
        if (txtDesProvincia.getText().length() > 0) {
          btnMunicipio.setEnabled(true);
          txtMunicipio.setEnabled(true);
        }
        else {
          btnMunicipio.setEnabled(false);
          txtMunicipio.setEnabled(false);
        }

// Fin JMT

        btnModificar.setEnabled(true);
        btnA�adir.setEnabled(true);
        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);
        txtCentro.setEditable(true);
        txtCentroDesc.setEditable(true);
        txtCentroDesc.setEnabled(true);
        txtProvincia.setEditable(true);
        txtDesProvincia.setEditable(false);
        txtMunicipio.setEditable(true);
        txtDesMunicipio.setEditable(false);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:
        btnProvincia.setEnabled(true);
        btnProvincia.setSelected(false);

        /*        if (txtDesProvincia.getText().length() > 0)
                  btnMunicipio.setEnabled(true);
                else
                  btnMunicipio.setEnabled(false);*/

// JMT
        if (txtDesProvincia.getText().length() > 0) {
          btnMunicipio.setEnabled(true);
          txtMunicipio.setEnabled(true);
        }
        else {
          btnMunicipio.setEnabled(false);
          txtMunicipio.setEnabled(false);
        }

// Fin JMT

        btnModificar.setEnabled(true);
        btnA�adir.setEnabled(true);
        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);
        btnBuscar.setEnabled(true);

        txtCentro.setEditable(true);
        //btnCentro.setEnabled(true);
        txtCentroDesc.setEditable(false);
        txtProvincia.setEditable(true);
        txtMunicipio.setEditable(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnProvincia.setEnabled(false);
        btnMunicipio.setEnabled(false);
        btnA�adir.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBuscar.setEnabled(false);
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        txtCentro.setEditable(false);
        txtCentroDesc.setEditable(false);
        txtProvincia.setEditable(false);
        txtMunicipio.setEditable(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  // comprueba que se ha seleccionado un dato de la tabla
  /// y selecciona el dato a mandar
  protected boolean isDataValid() {
    CMessage msgBox;

    int index = tabla.getSelectedIndex();
    if (listaCentro != null && listaCentro.size() > index && index >= 0) {
      datCentroSeleccionado = (DataCN) listaCentro.elementAt(index);
      return true;
    }

    return false;
  }

  /** pierde el foco el texto dela provincia */
  public void obtenerProvincia() {
    CLista data, result;
    String strCodProv = txtProvincia.getText();
    String strDesProv = txtDesProvincia.getText();
    // vamos a por la descripci�n de la provincia
    if (strCodProv != null && strCodProv.trim().length() > 0 &&
        (strDesProv == null || strDesProv.trim().length() == 0)) {
      try {
        data = new CLista();
        data.setIdioma(app.getIdioma());
        data.addElement(new DataCat2(strCodProv));
        //data.setState( CLista.listaINCOMPLETA);
        //# System_Out.println("mandamos datos" + strCodProv);
        URL u = new URL(app.getURL() + strSERVLETNiv1);
        stubNivel1.setUrl(u);
        result = (CLista) stubNivel1.doPost(servletOBTENER_X_CODIGO +
                                            catalogo.Catalogo.catCCAA, data);
        //# System_Out.println("recibimos datos" + result);
        if (result != null && result.size() > 0) {
          strDesProv = ( (DataCat2) result.firstElement()).getDes();
        }
      }
      catch (Exception exc) {
        //# System_Out.println("Error leyendo Provincia "+ exc.toString());
      }
      if (strDesProv != null) {
        txtDesProvincia.setText(strDesProv);
      }
      txtMunicipio.setText("");
      txtDesMunicipio.setText("");
      btnMunicipio.setEnabled(true);
    }
  }

  public void obtenerMunicipio() {
    String strCodMun = txtMunicipio.getText();
    String strDesMun = txtDesMunicipio.getText();
    CLista result, data;
    DataMunicipioEDO datmun;
    // vamos a por la descripci�n del municipio
    if (strCodMun != null && strCodMun.trim().length() > 0 &&
        (strDesMun == null || strDesMun.trim().length() == 0)) {
      try {

        data = new CLista();
        data.addElement(new DataMunicipioEDO(txtMunicipio.getText(),
                                             txtProvincia.getText()));
        stubMun.setUrl(new URL(app.getURL() + strSERVLETMun));
        result = (CLista)this.stubMun.doPost(servletOBTENER_MUNICIPIO_X_CODIGO,
                                             data);

        if ( (result != null) &&
            (result.size() > 0)) {
          datmun = (DataMunicipioEDO) result.firstElement();
          codMuni = datmun.getCodMun();
          txtDesMunicipio.setText(datmun.getDesMun());
          txtMunicipio.setText(codMuni);
        }
        else {
          CMessage msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                         res.getString("msg11.Text"));
          msgBox.show();
          msgBox = null;
        }
      }
      catch (Exception exc) {
        //# System_Out.println("Error leyendo Muncipio "+ exc.toString());
      }
    }
  }

  void btnMunicipio_actionPerformed(ActionEvent e) {

    DataMunicipioEDO data;
    CMessage msgBox;

    try {

      CListaMun2 lista = new CListaMun2(this,
                                        res.getString("msg12.Text"),
                                        stubMun,
                                        strSERVLETMun,
                                        servletOBTENER_MUNICIPIO_X_CODIGO,
                                        servletOBTENER_MUNICIPIO_X_DESCRIPCION,
                                        servletSELECCION_MUNICIPIO_X_CODIGO,
          servletSELECCION_MUNICIPIO_X_DESCRIPCION);
      lista.show();
      data = (DataMunicipioEDO) lista.getComponente();
      if (data != null) {
        codMuni = data.getCodMun();
        txtDesMunicipio.setText(data.getDesMun());
        txtMunicipio.setText(codMuni);
        btnMunicipio.setEnabled(true);
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

  }

  void txtMunicipio_actionPerformed(ActionEvent e) {
    CMessage msgBox = null;
    DataZBS2 datmod = null;
    CLista data = null;
    CLista lRes = null;

    lRes = new CLista();
    modoAnt = modoOperacion;
    try {

      // prepara los par�metros de env�o
      data = new CLista();
      data.addElement(new DataZBS2(txtProvincia.getText(), txtMunicipio.getText(),
                                   "", ""));
      data.setFilter("");
      // idioma
      data.setIdioma(this.getApp().getIdioma());

      // apunta al servlet principal
      lRes = (CLista)this.stubZBS2.doPost(servletOBTENER_X_CODIGO, data);

      if (lRes.size() > 0) {
        // el item existe
        datmod = (DataZBS2) lRes.firstElement();
      }
      else {
        // el item no existe
        this.modoOperacion = modoALTA;
        datmod = new DataZBS2(txtProvincia.getText(), txtMunicipio.getText(),
                              "", "");
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg13.Text"));
        msgBox.show();
        msgBox = null;
      }
      // rellena los datos
      txtMunicipio.setText(datmod.getNiv2());

      // error en el proceso
    }
    catch (Exception esr) {
      this.modoOperacion = modoALTA;
      esr.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, esr.getMessage());
      msgBox.show();
      msgBox = null;
    }

  }

  void btnCancelar_actionPerformed(ActionEvent e) {
    acepto = false;
  }

  void txtProvincia_keyPressed(KeyEvent e) {
    txtMunicipio.setText("");
    codProvi = "";
    btnMunicipio.setEnabled(false);
    txtMunicipio.setEnabled(false);
    txtDesMunicipio.setText("");
    txtDesProvincia.setText("");
  }

  public void btnA�adir_actionPerformed(ActionEvent e) {
    Dial_CN dial = new Dial_CN(app, this, res.getString("msg14.Text"),
                               Dial_CN.modoALTA, null);
    dial.show();
    // lo a�adimos al finald ela lista y repintamos la tabla
    if (dial.datoA�adido != null) {
      listaCentro.addElement(dial.datoA�adido);
      writeListaCentro();
    }
    dial = null;
  }

  public void btnModificar_actionPerformed(ActionEvent e) {

    if (isDataValid()) {
      int index = tabla.getSelectedIndex();
      datCentroSeleccionado = (DataCN) listaCentro.elementAt(index);
      Dial_CN dial = new Dial_CN(app, this, res.getString("msg15.Text"),
                                 Dial_CN.modoMODIFICAR, datCentroSeleccionado);
      dial.txtMunicipio.setText(datCentroSeleccionado.getMun());
      dial.txtDesMunicipio.setText(datCentroSeleccionado.getDesMun());
      dial.show();
      if (dial.datoA�adido != null) {
        listaCentro.removeElementAt(index);
        if (dial.datoA�adido.getBaja().equals("N")) {
          listaCentro.insertElementAt(dial.datoA�adido, index);
        }
        writeListaCentro();
      }
      dial = null;
    }
  }

  void txtMunicipio_keyPressed(KeyEvent e) {
    codMuni = "";
    txtDesMunicipio.setText("");
  }

  void btnBuscar_actionPerformed(ActionEvent e) {
    CLista datos = null;
    CMessage mensaje = null;
    String cad = new String("");
    DataCN daton1 = null;

    listaCentro.setState(CLista.listaNOVALIDA);
    try {
      if (this.listaCentro.getState() == CLista.listaNOVALIDA) {
        datos = new CLista();
        datos.setFilter("");
        daton1 = new DataCN(txtCentro.getText().toUpperCase(),
                            txtProvincia.getText().toUpperCase(),
                            txtMunicipio.getText().toUpperCase(),
                            txtCentroDesc.getText(), "", "", "", "", "", "", "",
                            "", "",
                            this.app.getLogin(), "", "", "");
        datos.addElement(daton1);
        listaCentro = (CLista)this.stubCentro.doPost(SrvCN.
            servletSELECCION_X_TODO_CODIGO, datos);
        datos = null;
      }
      writeListaCentro();

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }
  }

  public void writeListaCentro() {
    DataCN data;
    CMessage msgBox;
    String cadLinea = "";

    tabla.clear();
    cadLinea = "";
    if (listaCentro.size() > 0) {

      // vuelca la lista
      for (int j = 0; j < listaCentro.size(); j++) {
        data = (DataCN) listaCentro.elementAt(j);
        cadLinea = data.getCodCentro().trim() + "&" + data.getCentroDesc().trim() +
            "&" +
            data.getDesProv().trim() + "&" + data.getDesMun().trim();
        tabla.addItem(cadLinea, '&');
      }

      // opci�n m�s datos
      if (listaCentro.getState() == CLista.listaINCOMPLETA) {
        cadLinea = res.getString("msg16.Text") + "&" + "" + "&" + "" + "&";
        tabla.addItem(cadLinea, '&'); //'&' es el car�cter separador de datos (cada datos va a una columnna)
      }

      tabla.repaint();

      // mensaje de lista vacia
    }
    else {
      tabla.clear();
      msgBox = new CMessage(this.app, CMessage.msgAVISO,
                            res.getString("msg17.Text"));
      msgBox.show();
      msgBox = null;
      this.modoOperacion = modoALTA;
    }
  }

  void btnAceptar_actionPerformed(ActionEvent e) {
    acepto = true;
  }

  void tabla_actionPerformed(JCItemEvent evt) {
    CMessage msgBox = null;
    DataCN datUsuario = null;
    CLista data = null;

    int tam;
    tam = tabla.countItems();

    //Obtengo el �ndice selccionado de tabla
    int indSel = tabla.getSelectedIndex();

    if (indSel != BWTEnum.NOTFOUND) {

      try {
        // a�ade una p�gina a la lista principal
        if ( (indSel == tam - 1) &&
            (listaCentro.getState() == CLista.listaINCOMPLETA)) {

          tabla.deselect(tabla.getSelectedIndex());

          // prepara los par�metros de llamada al servlet
          data = new CLista();

          // c�digo de filtrado
          data.addElement(new DataCN(txtCentro.getText().toUpperCase(),
                                     codProvi.toUpperCase(),
                                     codMuni.toUpperCase(),
                                     txtCentroDesc.getText(), "", "", "", "",
                                     "", "", "", "", "",
                                     this.app.getLogin(), "", "", ""));
          // �ltimo c�digo en el cliente
          data.setFilter(listaCentro.getFilter());

          // estado incompleto
          data.setState(listaCentro.getState());

          // idioma
          data.setIdioma(app.getIdioma());

          // a�ade la lista
          listaCentro.appendData( (CLista) stubCentro.doPost(
              servletSELECCION_X_CODIGO, data));

          // visualiza los datos
          writeListaCentro();
        }

        // selecciona el item
        else {
          btnModificar.setEnabled(true);
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

    } //if alguno seleccionado
  }

} // END CLASS

class PanelBusca_btnProvincia_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelBusca adaptee;
  ActionEvent e;

  PanelBusca_btnProvincia_actionAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      new Thread(this).start();
    }
  }

  public void run() {
    adaptee.btnProvincia_actionPerformed(e);
    adaptee.desbloquea();
  }
}

class PanelBusca_btnMunicipio_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelBusca adaptee;
  ActionEvent e;

  PanelBusca_btnMunicipio_actionAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      new Thread(this).start();
    }
  }

  public void run() {
    adaptee.btnMunicipio_actionPerformed(e);
    adaptee.desbloquea();
  }
}

class PanelBusca_txtMunicipio_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelBusca adaptee;
  ActionEvent e;

  PanelBusca_txtMunicipio_actionAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      new Thread(this).start();
    }
  }

  public void run() {
    adaptee.obtenerMunicipio();
    adaptee.desbloquea();
  }
}

class PanelBusca_txtProvincia_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelBusca adaptee;
  ActionEvent e;

  PanelBusca_txtProvincia_actionAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      new Thread(this).start();
    }
  }

  public void run() {
    //adaptee.txtProvincia_actionPerformed(e);
    adaptee.obtenerProvincia();
    adaptee.desbloquea();
  }
}

class PanelBusca_txtProvincia_keyAdapter
    extends java.awt.event.KeyAdapter {
  PanelBusca adaptee;

  PanelBusca_txtProvincia_keyAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtProvincia_keyPressed(e);
  }
}

class PanelBusca_txtProvincia_textAdapter
    implements java.awt.event.TextListener {
  PanelBusca adaptee;

  PanelBusca_txtProvincia_textAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void textValueChanged(TextEvent e) {
    adaptee.txtDesProvincia.setText("");
  }
}

class PanelBuscaTextFocusListener
    implements java.awt.event.FocusListener, Runnable {
  PanelBusca adaptee;
  FocusEvent e = null;

  PanelBuscaTextFocusListener(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void focusGained(FocusEvent e) {
  }

  public void focusLost(FocusEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  public void run() {
    String name2 = ( (Component) e.getSource()).getName();

    if (name2.equals("provincia")) {
      adaptee.obtenerProvincia();
    }
    else if (name2.equals("municipio")) {
      adaptee.obtenerMunicipio();
    }
    adaptee.desbloquea();
  }

} //_______________________________________________ END_CLASS

class PanelBusca_txtMunicipio_keyAdapter
    extends java.awt.event.KeyAdapter {
  PanelBusca adaptee;

  PanelBusca_txtMunicipio_keyAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtMunicipio_keyPressed(e);
  }
}

// escuchador de los click en la tabla
class PanelBusca_tabla_dobleClick
    implements jclass.bwt.JCActionListener, Runnable {
  PanelBusca adaptee;

  PanelBusca_tabla_dobleClick(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    if (adaptee.bloquea()) {
      new Thread(this).start();
    }
  }

  public void run() {
    adaptee.btnModificar_actionPerformed(null);
    adaptee.desbloquea();
  }
}

class PanelBusca_btnBuscar_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelBusca adaptee;
  ActionEvent e;

  PanelBusca_btnBuscar_actionAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      new Thread(this).start();
    }
  }

  public void run() {
    int index;

    if (e.getActionCommand() == "buscar") { // lista principal
      adaptee.btnBuscar_actionPerformed(e);
    }
    else if (e.getActionCommand() == "a�adir") {
      adaptee.btnA�adir_actionPerformed(e);
    }
    else if (e.getActionCommand() == "modificar") {
      adaptee.btnModificar_actionPerformed(e);
    }
    else if (e.getActionCommand() == "primero") {
      if (adaptee.tabla.countItems() > 0) {
        adaptee.tabla.select(0);
        adaptee.tabla.setTopRow(0);
      }
    }
    else if (e.getActionCommand() == "anterior") {
      index = adaptee.tabla.getSelectedIndex();
      if (index > 0) {
        index--;
        adaptee.tabla.select(index);
        if (index - adaptee.tabla.getTopRow() <= 0) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() - 1);
        }
      }
      else {
        adaptee.tabla.select(0);
        adaptee.tabla.setTopRow(0);
      }
    }
    else if (e.getActionCommand() == "siguiente") {
      int ultimo = adaptee.tabla.countItems() - 1;
      index = adaptee.tabla.getSelectedIndex();
      if (index < ultimo && index >= 0) {
        index++;
        adaptee.tabla.select(index);
        if (index - adaptee.tabla.getTopRow() >= 4) {
          adaptee.tabla.setTopRow(adaptee.tabla.getTopRow() + 1);
        }
      }
      else {
        adaptee.tabla.select(0);
        adaptee.tabla.setTopRow(0);
      }
    }
    else if (e.getActionCommand() == "ultimo") {
      int ultimo = adaptee.tabla.countItems() - 1;
      if (ultimo >= 0) {
        index = ultimo;
        adaptee.tabla.select(index);
        if (adaptee.tabla.countItems() >= 4) {
          adaptee.tabla.setTopRow(adaptee.tabla.countItems() - 4);
        }
        else {
          adaptee.tabla.setTopRow(0);
        }
      }
    }
    adaptee.desbloquea();
  }
} //   END_CLASS

class PanelBusca_tabla_actionAdapter
    implements jclass.bwt.JCItemListener, Runnable {
  PanelBusca adaptee;
  JCItemEvent e;

  PanelBusca_tabla_actionAdapter(PanelBusca adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(JCItemEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      new Thread(this).start();
    }
  }

  public void run() {
    if (e.getStateChange() == JCItemEvent.SELECTED) { //evento seleccion (no deseleccion)
      adaptee.tabla_actionPerformed(e);
    }
    adaptee.desbloquea();
  }
}

// lista de valores
class CListaMun2
    extends CListaValores {

  protected PanelBusca panel;

  public CListaMun2(PanelBusca p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descripcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataMunicipioEDO(s, panel.txtProvincia.getText().trim());
  }

  public String getCodigo(Object o) {
    return ( ( (DataMunicipioEDO) o).getCodMun());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataMunicipioEDO) o).getDesMun() + " " +
            ( (DataMunicipioEDO) o).getDesProv());
  }
}
