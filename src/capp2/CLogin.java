package capp2;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import cifrado.Cifrar;
import sapp2.Data;
import sapp2.Lista;

/**
 * @autor LSR
 * @version 1.0
 *
 * Ventana de control de acceso
 * Devuelve una estructura Data con las autorizaciones:
 *   (codigo, estado - vacio=abierto, 'X'=cerrado)
 *   ("CD_TUSU", perfil)
 *
 */
public class CLogin
    extends CDialog {

  // controles
  CImage image = null;
  XYLayout xyLyt = new XYLayout();
  Label lblUsuario = new Label();
  TextField txtUID = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  Label lblContraseña = new Label();
  TextField txtPWD = new TextField();
  actionListener btnActionListener = new actionListener(this);

  // autorizaciones
  private Data dtAutorizaciones = null;

  public void Inicializar() {}

  // configura los controles según el modo de operación
  public void Inicializar(int modo) {
    switch (modo) {
      case CInicializar.NORMAL:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtUID.setEnabled(true);
        txtPWD.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case CInicializar.ESPERA:
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        txtUID.setEnabled(false);
        txtPWD.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  public CLogin(CApp a,
                String u,
                String t) {

    super(a);

    try {
      this.setTitle(t);
      this.getApp().getStub().setUrl(u);
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // aspecto de la ventana
  private void jbInit() throws Exception {
    final String imgSEGURIDAD = "images/seguridad.gif";
    final String imgCANCELAR = "images/cancelar.gif";
    final String imgACEPTAR = "images/aceptar.gif";

    // carga las imagenes
    this.getApp().getLibImagenes().put(imgACEPTAR);
    this.getApp().getLibImagenes().put(imgCANCELAR);
    this.getApp().getLibImagenes().CargaImagenes();
    btnAceptar.setImage(this.getApp().getLibImagenes().get(imgACEPTAR));
    btnCancelar.setImage(this.getApp().getLibImagenes().get(imgCANCELAR));

    setSize(290, 134);
    xyLyt.setHeight(134);
    xyLyt.setWidth(290);
    lblUsuario.setText("Usuario");
    txtUID.setBackground(new Color(255, 255, 150));
    btnAceptar.setLabel("Aceptar");
    btnCancelar.setLabel("Cancelar");
    lblContraseña.setText("Contraseña");
    txtPWD.setBackground(new Color(255, 255, 150));
    txtPWD.setEchoChar('*');
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");
    this.setLayout(xyLyt);
    this.add(lblUsuario, new XYConstraints(81, 11, -1, 17));
    this.add(txtUID, new XYConstraints(177, 5, 100, -1));
    this.add(txtPWD, new XYConstraints(177, 39, 100, -1));
    this.add(btnAceptar, new XYConstraints(110, 78, 80, 26));
    this.add(btnCancelar, new XYConstraints(197, 78, 80, 26));
    this.add(lblContraseña, new XYConstraints(81, 45, 74, 17));

    // establece los escuchadores
    btnAceptar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);

    // imagen
    image = new CImage(getApp(), imgSEGURIDAD, 60, 60);
    this.add(image, new XYConstraints(10, 10, 70, 70));

    // establece el modo de operación
    Inicializar(CInicializar.NORMAL);
  }

  // comprueba el login introducido
  protected void verificarLogin() {
    Data dt = null;
    Lista v = null;

    if ( (txtUID.getText().length() > 0) &&
        (txtPWD.getText().length() > 0)) {

      Inicializar(CInicializar.ESPERA);

      try {
        dt = new Data();
        dt.put("COD_USUARIO", txtUID.getText());
        dt.put("PASSWORD", Cifrar.cifraClave(txtPWD.getText()));
        dt.put("COD_APLICACION", getApp().getParametro("COD_APLICACION"));

        v = new Lista();
        v.addElement(dt);

        v = (Lista)this.getApp().getStub().doPost(1, v);

        if (v.size() > 0) {
          dtAutorizaciones = (Data) v.elementAt(0);
          dtAutorizaciones.put("COD_USUARIO", txtUID.getText());
          //para devolver al applet inicial la fecha actual
          dtAutorizaciones.put("FC_ACTUAL", v.getParameter("FC_ACTUAL"));
        }

        dispose();

      }
      catch (Exception e) {
        this.getApp().trazaLog(e);
        this.getApp().showError(e.getMessage());
      }

      Inicializar(CInicializar.NORMAL);
    }
    else {
      this.getApp().showAdvise("Debe informar el usuario y la contraseña");
    }

  }

  public Data getAutorizaciones() {
    return dtAutorizaciones;
  }
}

// action listener
class actionListener
    implements ActionListener, Runnable {
  CLogin adaptee = null;
  ActionEvent e = null;

  public actionListener(CLogin adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    Thread th = new Thread(this);
    th.start();
  }

  // hilo de ejecución
  public void run() {
    if (e.getActionCommand().equals("aceptar")) {
      adaptee.verificarLogin();
    }
    else {
      adaptee.dispose();
    }
  }
}
