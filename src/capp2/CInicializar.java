package capp2;

/**
 * @autor LSR
 * @version 1.0
 *
 * Interfaz que deben soportar los paneles contenedores
 * para que los componentes les habiliten/deshabiliten
 * en los accesos que realicen a los servlets.
 *
 */
public interface CInicializar {
  public static final int NORMAL = 0;
  public static final int ESPERA = 1;

  public void Inicializar(int modo);
}