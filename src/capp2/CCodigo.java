package capp2;

/**
 * @autor LSR
 * @version 1.0
 *
 * Caja de texto que admite cualquier caracter, pero �nicamente permite letras may�sculas.
 * Implementa el interfaz KeyListener para conseguir este comportamiento.
 *
 *
 * LRG Cambio permite accion cursores dcha e izqda y otros eventos
 */
import java.awt.TextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class CCodigo
    extends TextField
    implements KeyListener {
  private int iMaxLng;
  private int iLong;

  public CCodigo() {
    super();
    iMaxLng = -1;
    addKeyListener(this);
  }

  public CCodigo(int iL) {
    super();
    iMaxLng = iL;
    iLong = 0;
    addKeyListener(this);
  }

  public void keyPressed(KeyEvent e) {
    //C�digo de la tecla asociada a este evento (tecla pulsada)
    int iKey = e.getKeyCode();

//    int codEvento= e.getKeyCode(); //LRG

    if (iKey == 8 ||
        iKey == 9 ||
        iKey == 127) { // tabulador
      return;
    }

    //_____ LRG
    //Si se pulsan ciertas teclas de control (entre ellas las de cursores) se admite el evento
    //Si algunos de estos eventos no se admitesen se producir�an efectos extra�os
    else if ( (iKey == KeyEvent.VK_LEFT)
             || (iKey == KeyEvent.VK_RIGHT)
             || (iKey == KeyEvent.VK_UP)
             || (iKey == KeyEvent.VK_DOWN)
             || (iKey == KeyEvent.VK_ENTER) //NU
             || (iKey == KeyEvent.VK_ESCAPE) //NU
             || (iKey == KeyEvent.VK_CONTROL) //NU
             || (iKey == KeyEvent.VK_INSERT)
             || (iKey == KeyEvent.VK_CLEAR)
             || (iKey == KeyEvent.VK_DELETE)
             || (iKey == KeyEvent.VK_HOME)
             || (iKey == KeyEvent.VK_END)
             || (iKey == KeyEvent.VK_PAGE_UP)
             || (iKey == KeyEvent.VK_PAGE_DOWN)
             || (iKey == KeyEvent.VK_SHIFT) //NU
             || (iKey == KeyEvent.VK_F1)
             || (iKey == KeyEvent.VK_F2)
             || (iKey == KeyEvent.VK_F3)
             || (iKey == KeyEvent.VK_F4)
             || (iKey == KeyEvent.VK_F5)
             || (iKey == KeyEvent.VK_F6)
             || (iKey == KeyEvent.VK_F7)
             || (iKey == KeyEvent.VK_F8)
             || (iKey == KeyEvent.VK_F9)
             || (iKey == KeyEvent.VK_F10)
             || (iKey == KeyEvent.VK_F11)
             || (iKey == KeyEvent.VK_F12)
             ) {
      return;
    }
    //__________

    else {
      //Si se ha indicado long. m�xima
      if (iMaxLng != -1) {
        iLong = getText().length();
        //Si longitud supera la permitida por esta caja se consume el evento
        //Por tanto es como si no existiese evento
        if (iLong >= iMaxLng) {
          e.consume();
        }
        //Si aun no se ha superado long permitida
        else {

          //LRG_____________
          //Se guarda la posicion actual del cursor
          int posCursor = getCaretPosition();
          //Se imprime en caja de texto lo que hab�a antes de cursor m�s
          //el caracter tecleado pasado a may�sculas m�s lo que hab�a despu�s del cursor
          setText(getText().substring(0, posCursor) +
                  Character.toUpperCase(e.getKeyChar()) +
                  getText().substring(posCursor, getText().length()));
          //Se mueve el cursor
          setCaretPosition(posCursor + 1);
          //__________________

          /*
               setText(getText() + Character.toUpperCase(e.getKeyChar()) );
                    setCaretPosition(getText().length());
           */
          //Se consume el evento (Ya no debe hacer nada m�s)
          e.consume();
        }
      }
      //Si la caja no tiene restriccion de longitud m�xima
      else {

        //LRG_____________
        //Se guarda la posicion actual del cursor
        int posCursor = getCaretPosition();
        //Se imprime en caja de texto lo que hab�a antes de cursor m�s
        //el caracter tecleado pasado a may�sculas m�s lo que hab�a despu�s del cursor
        setText(getText().substring(0, posCursor) +
                Character.toUpperCase(e.getKeyChar()) +
                getText().substring(posCursor, getText().length()));
        //Se mueve el cursor
        setCaretPosition(posCursor + 1);
        //__________________

        /*
                setText(getText() + Character.toUpperCase(e.getKeyChar()));
                setCaretPosition(getText().length());
         */
        //Se consume el evento (Ya no debe hacer nada m�s)
        e.consume();
      }
    }
  }

  public void keyTyped(KeyEvent e) {
  }

  public void keyReleased(KeyEvent e) {
  }

}
