package capp2;

/**
 * @autor IC
 * @version 1.0
 *
 * Interfaz que soporta el comportamiento del temporizador.
 *
 */
interface CTimerListener {

  public void TimerEnd(CTimer timer);

  public void TimerError(CTimer timer, Exception e);

} //endinterfece TimerListener
