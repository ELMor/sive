package capp2;

/**
 * @autor LSR
 * @version 1.0
 *
 * Caja de texto que admite una longitud m�xima de datos.
 * La longitud m�xima se informa en el constructor.
 *
 */
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class CTexto
    extends java.awt.TextField
    implements KeyListener {
  private int iMaxLng;
  private int iLong;
  private boolean bControl;

  public CTexto(int iS) {
    iMaxLng = iS;
    iLong = 0;

    //{{REGISTER_LISTENERS
    this.addKeyListener(this);
    //}}
  }

  public void keyPressed(KeyEvent event) {
    char c = event.getKeyChar();
    iLong = getText().length();
    if ( (int) c == 8 || (int) c == 9 || (int) c == 127) {
      return;
    }
    else {
      if (iLong >= iMaxLng) {
        event.consume();
      }
    }
  }

  public void keyTyped(KeyEvent e) {
    /*
         iLong = getText().length();
         if(bControl)  // tabulador
        return;
         if(iLong >= iMaxLng)
        e.consume(); // se supera la longitud m�xima
     */
  }

  public void keyReleased(KeyEvent evt) {
  }

}
