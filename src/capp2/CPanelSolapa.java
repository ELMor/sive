package capp2;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Panel;

/**
 * @autor LSR
 * @version 1.0
 *
 * Conjunto de objetos CPanel para se visualizados mediante pesta�as.
 *
 */
public class CPanelSolapa
    extends Panel {
  CardLayout card;

  CPanelSolapa() {
    setLayout(card = new CardLayout());
  }

  // addItem
  // A�ade un elemetno a los posibles mostrables
  void addItem(String n, Component c) {
    add(n, c);
  }

  // choose
  // Muestra el panel concreto.
  void choose(String n) {
    ( (CardLayout) getLayout()).show(this, n);
  }

  public Insets insets() {
    return new Insets(5, 5, 5, 5);
  }

  public void update(Graphics g) {
    setBackground(getParent().getBackground());
    paint(g);
  }

  //M�todo PAINT del panel que engloba todo
  public void paint(Graphics g) {
    g.setColor(getBackground().brighter());
    if ( ( (CTabPanel) getParent()).getNumSolapas() < 2) {
      g.drawLine(0, 0, getBounds().width - 1, 0);
    } //endif
    g.drawLine(0, 0, 0, getBounds().height - 1);
    g.setColor(getBackground().darker());
    g.drawLine(0, getBounds().height - 1, getBounds().width - 1,
               getBounds().height - 1);
    g.drawLine(getBounds().width - 1, getBounds().height - 1,
               getBounds().width - 1, 0);
  }

  // delItem
  // borra el panel i
  public void delItem(int i) {
    if (i >= 0 && i <= getComponentCount() - 1) {
      remove(getComponent(i));
    }
  }

  public void delAll() {
    removeAll();
    setVisible(false);
  }

  // getTabPanel
  // obtiene el panel i
  public CPanel getTabPanel(int i) {
    Component c;

    if (i >= 0 && i <= getComponentCount() - 1) {
      c = getComponent(i);
      if (c instanceof Panel) {
        return (CPanel) c;
      }
      else {
        return null;
      }
    }
    else {
      return null;
    }
  }

}
