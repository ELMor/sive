package capp2;

import java.util.Vector;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import jclass.bwt.JCItemEvent;
import jclass.bwt.JCItemListener;
import sapp2.Data;
import sapp2.Lista;

/**
 * @autor LSR
 * @version 1.0
 *
 * Tabla con botonera para los m�dulos maestro/detalle.
 * Tama�o 715x304
 * Columnas 670
 *
 * Modificaci�n (Correccci�n error) LRG (Rivera) para que no se realice operacion en caso de
 *      tener selecccionado m�s datos.. pero estemos en modo que no sea a�adir
 *
 * Nueva modificaci�n: Creaci�n de un nuevo constructor, para
 *       que justifique los campos a la derecha. ARS. 20-09-2000.
 */
public class CListaMantenimiento
    extends CPanel {

  // lista
  protected Lista vSnapShot = new Lista();

  // indica la fila seleccionada en la tabla
  public int m_itemSelecc = -1;

  // vectores de la botonera
  protected Vector vCnfBotones = null;
  protected Vector vBotones = new Vector();

  // configuraci�n de la tabla
  protected Vector vLabels = null;
  protected int[] iajustes = null;

  // contenedor
  protected CInicializar ciContenedor = null;

  // filtro
  protected CFiltro cfFiltro = null;

  // modo de operaci�n
  protected int modoOperacion = CInicializar.NORMAL;

  //Indica si el tama�o de la lista es el est�ndar o ha sido introducido por
  //el usuario.

  //Datos del tama�o del componente
  protected int Altura = 304;
  protected int Anchura = 715;
  protected static int AnchoBotones = 35;

  XYLayout xYLayout1 = new XYLayout();
  public CTabla tabla = new CTabla();
  ButtonControl btnPrimero = new ButtonControl();
  ButtonControl btnAnterior = new ButtonControl();
  ButtonControl btnSiguiente = new ButtonControl();
  ButtonControl btnUltimo = new ButtonControl();

  btnActionListener actionListener = new btnActionListener(this);
  tblActionAdapter actionAdapter = new tblActionAdapter(this);
  tblItemListener itemListener = new tblItemListener(this);

  //funciones que devuelven las posiciones de los componentes de
  //la lista en funci�n del tama�o del contenedor.

  protected int AnchoTabla() {
    return (Anchura - 20);
  }

  protected int AltoTabla() {
    return (Altura - 68);
  }

  protected int PosXBotones() {
    return (AnchoTabla() - 120);
  }

  protected int PosYBotones() {
    return (AltoTabla() + 8);
  }

  // constructores
  public CListaMantenimiento(CApp a,
                             Vector labels,
                             Vector botones,
                             CInicializar ci,
                             CFiltro cf) {

    try {
      setApp(a);
      vLabels = labels;
      vCnfBotones = botones;
      ciContenedor = ci;
      cfFiltro = cf;
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public CListaMantenimiento(CApp a,
                             Vector labels,
                             Vector botones,
                             CInicializar ci,
                             CFiltro cf,
                             int altura,
                             int anchura) {

    try {
      setApp(a);
      vLabels = labels;

      vCnfBotones = botones;
      ciContenedor = ci;
      cfFiltro = cf;
      Anchura = anchura;
      Altura = altura;
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public CListaMantenimiento(CApp a,
                             Vector labels,
                             Vector botones,
                             int[] ajustes,
                             CInicializar ci,
                             CFiltro cf) {

    try {
      setApp(a);
      vLabels = labels;
      vCnfBotones = botones;
      iajustes = ajustes;
      ciContenedor = ci;
      cfFiltro = cf;
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public CListaMantenimiento(CApp a,
                             Vector labels,
                             Vector botones,
                             int[] ajustes,
                             CInicializar ci,
                             CFiltro cf,
                             int altura,
                             int anchura) {

    try {
      setApp(a);
      vLabels = labels;
      vCnfBotones = botones;
      iajustes = ajustes;
      ciContenedor = ci;
      cfFiltro = cf;
      Anchura = anchura;
      Altura = altura;
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // inicializa el aspecto del panel
  public void jbInit() throws Exception {
    // im�genes
    final String imgPRIMERO = "images/primero.gif";
    final String imgANTERIOR = "images/anterior.gif";
    final String imgSIGUIENTE = "images/siguiente.gif";
    final String imgULTIMO = "images/ultimo.gif";

    CBoton btn = null;
    CColumna lbl = null;
    UButtonControl btnControl = null;
    int j;
    String sBtnStrings = "";
    String sWidths = "";

    xYLayout1.setWidth(Anchura);
    xYLayout1.setHeight(Altura);
    this.setLayout(xYLayout1);

    // botones de control en la tabla
    btnPrimero.setActionCommand("primero");
    this.getApp().getLibImagenes().put(imgPRIMERO);
    new CContextHelp("Primero", btnPrimero);
    btnAnterior.setActionCommand("anterior");
    this.getApp().getLibImagenes().put(imgANTERIOR);
    new CContextHelp("Anterior", btnAnterior);
    btnSiguiente.setActionCommand("siguiente");
    this.getApp().getLibImagenes().put(imgSIGUIENTE);
    new CContextHelp("Siguiente", btnSiguiente);
    btnUltimo.setActionCommand("ultimo");
    this.getApp().getLibImagenes().put(imgULTIMO);
    new CContextHelp("Ultimo", btnUltimo);

    // crea la botonera
    for (j = 0; j < vCnfBotones.size(); j++) {
      // configuracion
      btn = (CBoton) vCnfBotones.elementAt(j);

      // imagen
      this.getApp().getLibImagenes().put(btn.getImage());

      // crea el control
      btnControl = new UButtonControl(btn.getName(), getApp());
      btnControl.setActionCommand( (new Integer(j)).toString());
      new CContextHelp(btn.getToolTip(), btnControl);
      vBotones.addElement(btnControl);

      // a�ade el escuchador
      btnControl.addActionListener(actionListener);
    }

    // carga las imagenes
    this.getApp().getLibImagenes().CargaImagenes();

    // a�ade la tabla

    this.add(tabla, new XYConstraints(9, 2, AnchoTabla(), AltoTabla()));
    /*
        this.add(btnPrimero, new XYConstraints(PosXBotones(),
                                               PosYBotones(), -1, -1));
        this.add(btnAnterior, new XYConstraints(PosXBotones() + AnchoBotones,
                                                PosYBotones(), -1, -1));
         this.add(btnSiguiente, new XYConstraints(PosXBotones() + (2 * AnchoBotones),
                                                 PosYBotones(), -1, -1));
         this.add(btnUltimo, new XYConstraints(PosXBotones() + (3 * AnchoBotones),
                                              PosYBotones(), -1, -1));
     */

    this.add(btnPrimero, new XYConstraints(PosXBotones(),
                                           PosYBotones(), 22, 22));
    this.add(btnAnterior, new XYConstraints(PosXBotones() + AnchoBotones,
                                            PosYBotones(), 22, 22));
    this.add(btnSiguiente, new XYConstraints(PosXBotones() + (2 * AnchoBotones),
                                             PosYBotones(), 22, 22));
    this.add(btnUltimo, new XYConstraints(PosXBotones() + (3 * AnchoBotones),
                                          PosYBotones(), 22, 22));

    // copia las im�genes en los botones
    btnPrimero.setImage(this.getApp().getLibImagenes().get(imgPRIMERO));
    btnAnterior.setImage(this.getApp().getLibImagenes().get(imgANTERIOR));
    btnSiguiente.setImage(this.getApp().getLibImagenes().get(imgSIGUIENTE));
    btnUltimo.setImage(this.getApp().getLibImagenes().get(imgULTIMO));

    for (j = 0; j < vBotones.size(); j++) {
      btnControl = (UButtonControl) vBotones.elementAt(j);
      btn = (CBoton) vCnfBotones.elementAt(j);
      btnControl.setImage(this.getApp().getLibImagenes().get(btn.getImage()));

      // a�ade los botones de control
      btnControl.addActionListener(actionListener);
      this.add(btnControl, new XYConstraints(10 + j * AnchoBotones,
                                             PosYBotones(), 26, 26));
    }

    // a�ade los botones de gu�a

    // configuraci�n de la tabla
    for (j = 0; j < vLabels.size(); j++) {
      lbl = (CColumna) vLabels.elementAt(j);
      sBtnStrings = sBtnStrings + lbl.getLabel() + "\n";
      sWidths = sWidths + lbl.getSize() + "\n";
    }
    tabla.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(
        sBtnStrings, '\n'));
    tabla.setColumnWidths(jclass.util.JCUtilConverter.toIntList(sWidths, '\n'));
    tabla.setNumColumns(vLabels.size());
    // Ponemos el ajuste de los campos
    if (iajustes != null) {
      tabla.setColumnAlignments(iajustes);

      // a�ade los escuchadores
    }
    tabla.addActionListener(actionAdapter);
    tabla.addItemListener(itemListener);
    btnPrimero.addActionListener(actionListener);
    btnAnterior.addActionListener(actionListener);
    btnSiguiente.addActionListener(actionListener);
    btnUltimo.addActionListener(actionListener);

    setBorde(false);

  }

  // establece la fila actual
  public void setCurrent(int i) {

    if (i >= tabla.countItems()) {
      i = tabla.countItems() - 1;

    }
    if (i > 0) {
      tabla.select(i);
      m_itemSelecc = i;
    }
  }

  public void Inicializar() {
    if (this.ciContenedor != null) {
      this.ciContenedor.Inicializar(this.modoOperacion);
    }
  }

  // limpia la pantalla
  public void vaciarPantalla() {
    if (tabla.countItems() > 0) {
      tabla.deleteItems(0, tabla.countItems() - 1);
    }
  }

  // Botonera
  public void btn_actionPermormed(int i) {
    if (this.cfFiltro != null) {
      this.modoOperacion = CInicializar.ESPERA;
      Inicializar();

      // Esto filtra, por si se ha pulsado el bot�n de modificar
      // o borrar, teniendo seleccionado "mas datos"

      //Si no hay ninguna seleccionada o est� el m�s selecionado
      //y estamos en modificar o borrar (i!=0) , no hace nada
      if ( (getSelected() == null) &&
          (i != 0)) {} //(LRG)
      else {
        this.cfFiltro.realizaOperacion(i);
      }

      this.modoOperacion = CInicializar.NORMAL;
      Inicializar();
    }
  }

  // gesti�n de los botones de control
  public void btn_actionPerformed(String actionCommand) {
    CBoton btn = null;

    // ir al primer registro de la tabla
    if (actionCommand.equals("primero")) {
      if (tabla.countItems() > 0) {
        m_itemSelecc = 0;
        tabla.select(m_itemSelecc);
        tabla.setTopRow(m_itemSelecc);
      }

      // anterior
    }
    else if (actionCommand.equals("anterior")) {
      if (m_itemSelecc > 0) {
        m_itemSelecc--;
        tabla.select(m_itemSelecc);
        if (m_itemSelecc - tabla.getTopRow() <= 0) {
          tabla.setTopRow(tabla.getTopRow() - 1);
        }
      }

      // siguiente
    }
    else if (actionCommand.equals("siguiente")) {
      if (m_itemSelecc < tabla.countItems() - 1) {
        m_itemSelecc++;
        tabla.select(m_itemSelecc);
        if (m_itemSelecc - tabla.getTopRow() >= 8) {
          tabla.setTopRow(tabla.getTopRow() + 1);
        }
      }

      // �ltimo
    }
    else if (actionCommand.equals("ultimo")) {
      if (tabla.countItems() - 1 >= 0) {
        m_itemSelecc = tabla.countItems() - 1;
        tabla.select(m_itemSelecc);
        if (tabla.countItems() >= 8) {
          tabla.setTopRow(tabla.countItems() - 8);
        }
        else {
          tabla.setTopRow(0);
        }
      }

      // botonera
    }
    else {
      btn_actionPermormed(Integer.parseInt(actionCommand));
    }
  }

  // devuelve la fila seleccionada
  public Data getSelected() {
    Data dSel = null;
    int indice = tabla.getSelectedIndex();

    // M�s datos
    if ( (vSnapShot.getEstado() == Lista.INCOMPLETA) &&
        (indice == (tabla.countItems() - 1))) {
      return dSel;
    }

    if (indice != BWTEnum.NOTFOUND) {
      dSel = (Data) vSnapShot.elementAt(indice);

    }
    return dSel;
  }

  // devuelve en indice de la fila seleccionada
  public int getSelectedIndex() {
    return tabla.getSelectedIndex();
  }

  // doble click en la tabla
  public void tabla_actionPerformed() {
    int indice = tabla.getSelectedIndex();
    Lista vMasDatos = null;
    int j = 0;
    CBoton btn = null;

    // lista incompleta
    if ( (vSnapShot.getEstado() == Lista.INCOMPLETA) &&
        (tabla.getSelectedIndex() == (tabla.countItems() - 1))) {

      // procesa
      try {
        if (this.cfFiltro != null) {
          this.modoOperacion = CInicializar.ESPERA;
          Inicializar();

          setSiguientePagina(this.cfFiltro.siguientePagina());

          this.modoOperacion = CInicializar.NORMAL;
          Inicializar();
        }

        // error al procesar
      }
      catch (Exception e) {
        this.getApp().trazaLog(e);
        this.getApp().showError(e.toString());
      }

      // modo modificar
    }
    else {
      setCurrent(indice);

      // doble click en el bot�n
      for (j = 0; j < vCnfBotones.size(); j++) {
        // configuracion
        btn = (CBoton) vCnfBotones.elementAt(j);
        /*
                // doble clik
                if (btn.getDblClick())
                  btn_actionPermormed(j);
         */

        // boton asociado al doble clik
        if (btn.getDblClick()) {
          //Si ese bot�n est� habilitado en doble click se ejecuta la misma acci�n del bot�n
          if ( ( (ButtonControl) (vBotones.elementAt(j))).isEnabled()) {
            btn_actionPermormed(j);
          }
        } //if doble click

      }
    }
  }

  // establece el �ndice seleccionado
  public void tabla_itemStateChanged(JCItemEvent evt) {
    m_itemSelecc = tabla.getSelectedIndex();
  }

  // devuelve la lista
  public Lista getLista() {
    return vSnapShot;
  }

  // pinta la primera trama en la tabla
  public void setPrimeraPaginaMuestra(Lista v) {
    String sFila = null;
    Data dt = null;
    CColumna lbl = null;

    vSnapShot = v;

    // escribe las l�neas en la tabla
    tabla.clear();
    if (vSnapShot.size() > 0) {
      for (int j = 0; j < vSnapShot.size(); j++) {
        sFila = "";
        dt = (Data) vSnapShot.elementAt(j);
        for (int i = 0; i < vLabels.size(); i++) {
          lbl = (CColumna) vLabels.elementAt(i);
//          if ((lbl.getField().equals("DS_MUESTRA")) && (dt.getString("IT_ORIGEN").equals("N"))){
          if ( (lbl.getField().equals("ORIGEN")) &&
              (dt.getString("IT_ORIGEN").equals("N"))) {
            sFila = sFila + " " + "&";
          }
          else {
            if ( ( (lbl.getField().equals("DS_RMUESTRA"))
                  || (lbl.getField().equals("NM_MUESBROTE"))
                  || (lbl.getField().equals("NM_POSITBROTE")))
                && (dt.getString("IT_MUESTRA").equals("N"))) {
              sFila = sFila + " " + "&";
            }
            else {
              sFila = sFila + dt.getString(lbl.getField()) + "&";
            }
          } //fin if dsmuestra
        } //fin for
        tabla.addItem(sFila, '&');
      }

      // verifica que sea la �ltima trama
      if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
        tabla.addItem("M�s datos ...");

        //Selecci�n de elementos
      }
      setCurrent(0);
    }
  }

  // pinta la primera trama en la tabla
  public void setPrimeraPagina(Lista v) {
    String sFila = null;
    Data dt = null;
    CColumna lbl = null;

    vSnapShot = v;

    // escribe las l�neas en la tabla
    tabla.clear();
    if (vSnapShot.size() > 0) {
      for (int j = 0; j < vSnapShot.size(); j++) {
        sFila = "";
        dt = (Data) vSnapShot.elementAt(j);

        for (int i = 0; i < vLabels.size(); i++) {
          lbl = (CColumna) vLabels.elementAt(i);
          sFila = sFila + dt.getString(lbl.getField()) + "&";
        }
        tabla.addItem(sFila, '&');
      }

      // verifica que sea la �ltima trama
      if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
        tabla.addItem("M�s datos ...");

        //Selecci�n de elementos
      }
      setCurrent(0);
    }
  }

  // pinta las siguientes tramas
  public void setSiguientePagina(Lista v) {
    String sFila = null;
    Data dt = null;
    CColumna lbl = null;
    int iLast = tabla.countItems() - 1;

    vSnapShot.addElements(v);

    // escribe las l�neas en la tabla
    tabla.deleteItem(iLast);
    if (vSnapShot.size() >= iLast) {
      for (int j = iLast; j < vSnapShot.size(); j++) {
        sFila = "";
        dt = (Data) vSnapShot.elementAt(j);

        for (int i = 0; i < vLabels.size(); i++) {
          lbl = (CColumna) vLabels.elementAt(i);
          sFila = sFila + dt.get(lbl.getField()) + "&";
        }
        tabla.addItem(sFila, '&');
      }

      // verifica que sea la �ltima trama
      if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
        tabla.addItem("M�s datos ...");

        //Selecci�n de elementos
      }
      setCurrent(iLast);
    }
  }

}

    /******************************************************************************/

// action listener de evento en botones
class btnActionListener
    implements ActionListener, Runnable {
  CListaMantenimiento adaptee = null;
  String actionCommand = null;

  public btnActionListener(CListaMantenimiento adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    actionCommand = e.getActionCommand();
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    adaptee.btn_actionPerformed(actionCommand);
  }
}

// navegaci�n por la tabla con los botones de ayuda
class tblItemListener
    implements JCItemListener {
  CListaMantenimiento adaptee = null;

  public tblItemListener(CListaMantenimiento adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(JCItemEvent e) {
    if (e.getStateChange() == JCItemEvent.SELECTED) { //evento seleccion (no deseleccion)
      adaptee.tabla_itemStateChanged(e);
    }
  }
}

// escuchador de los click en la tabla
class tblActionAdapter
    implements jclass.bwt.JCActionListener, Runnable {
  CListaMantenimiento adaptee;

  public tblActionAdapter(CListaMantenimiento adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    new Thread(this).start();
  }

  public void run() {
    adaptee.tabla_actionPerformed();
  }
}
