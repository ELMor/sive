package capp2;

import java.awt.Menu;

/**
 * Men� con identificaci�n para la gesti�n de USU.
 * Sobreescribe el m�todo setEnable para encapsular las autorizaciones
 * sobre las acciones de la aplicaci�n.
 * Nota: si no se indica una clave, no se tiene en cuenta USU
 *
 * @autor LSR
 * @version 1.0
 */

public class UMenu
    extends Menu {
  private String key = null;
  private CApp app = null;

  public UMenu(String label, String k, CApp a) {
    super(label);
    key = k;
    app = a;
    if (!app.isAllowed(key)) {
      super.setEnabled(false);
    }
  }

  public void setEnabled(boolean enabled) {
    if (app.isAllowed(key)) {
      super.setEnabled(enabled);
    }
    else {
      super.setEnabled(false);
    }
  }
}
