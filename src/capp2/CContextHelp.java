package capp2;

/**
 * @autor IC
 * @version 1.0
 *
 * Esta clase a�ade la capacidad a cualquier componente a mostrar
 * una nota cuando se entra con el rat�n.
 *
 * Un ejemplo:
 *   ...
 *    Button btn = new Button();
 *    btn.setLabel("Aceptar");
 *    new CContextHelp("Pulsa para aceptar los datos",btn);
 *   ...
 *
 * Parametros modificables:
 *
 * Color del fondo       -> CContextHelp.set_Background(Color color)
 * Color del nota        -> CContextHelp.set_Foreground(Color color)
 * Tiempo en mostrar nota-> CContextHelp.setDelayShowNote(int mseg)
 * Tiempo ocultar nota   -> CContextHelp.setDelayHideNote(int mseg)
 * Tipo de letra         -> CContextHelp.setFont(Font font)
 *
 */
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class CContextHelp
    extends Panel
    implements MouseListener, CTimerListener {
  // ESTADOS
  static int ST_INIT = 0;
  static int ST_ESPERANDO_HELP = ST_INIT + 1;
  static int ST_MOSTRANDO_HELP = ST_INIT + 2;

  // Valores configurables
  static Color fg = Color.black;
  static Color bg = new Color(255, 255, 192);
  static int timer_esp = 2000; // seg.
  static int timer_help = 5000; // seg.
  static Font font = new Font("Dialog", Font.PLAIN, 12);

  int estado = ST_INIT;
  String mensage;
  Component comp;
  Container parent = null;

  CTimer timer2;
  CTimer timer1;

  public CContextHelp(String s, Component c) {
    mensage = s;
    comp = c;
    /*
         parent = comp.getParent();
       if (parent != null) {
     parent.add(this,0);
         }//endif
         comp.addMouseListener(this);
         setVisible(false);*/
  } //end CContextHelp

  public static void setDelayShowNote(int mseg) {
    timer_esp = mseg;
  } //end setDelayShowNote

  public static void setDelayHideNote(int mseg) {
    timer_help = mseg;
  } //end setDelayHideNote

  public static void set_Background(Color c) {
    bg = c;
  } //end setBackground

  public static void set_Foreground(Color c) {
    fg = c;
  } //end setBackground

  public static void set_Font(Font f) {
    font = f;
  } //end set_Font

  public void paint(Graphics g) {
    g.setColor(bg);
    g.fillRect(0, 0, getSize().width - 1, getSize().height - 1);
    g.setColor(fg);
    g.draw3DRect(0, 0, getSize().width - 1, getSize().height - 1, false);
    g.drawString(mensage, 1, getSize().height - 3);
  } //end paint

  void Mostrar() {
    if (parent == null) {
      parent = comp.getParent();
      parent.add(this, 0);
    } //endif
    if (parent != null) {
      setFont(font);
      FontMetrics fm = getFontMetrics(getFont());
      setBounds(comp.getLocation().x + 10,
                comp.getLocation().y + comp.getSize().height + 3,
                fm.stringWidth(mensage) + 3, fm.getHeight() + 1);
      setVisible(true);
    } //endif
  } //end Mostrar

  void Ocultar() {
    if (parent == null) {
      parent = comp.getParent();
      parent.add(this, 0);
    } //endif
    if (parent != null) {
      setVisible(false);
    } //endif
  } //end Ocultar

  //-----------------------------
  // Captura de eventos de RATON
  //-----------------------------

  public synchronized void mouseEntered(MouseEvent e) {
    if (estado == ST_INIT) {
      timer1 = new CTimer("timer1", timer_esp, this);
      timer1.Start();
      estado = ST_ESPERANDO_HELP;
    } //endif
  } //end mouseEntered

  public void mouseExited(MouseEvent e) {
    if (estado == ST_ESPERANDO_HELP) {
      timer1.End();
      estado = ST_INIT;
    }
    else if (estado == ST_MOSTRANDO_HELP) {
      timer2.End();
      Ocultar();
      estado = ST_INIT;
    } //endif
  } //end mouseExited

  public void mouseReleased(MouseEvent e) {}

  public void mousePressed(MouseEvent e) {}

  public void mouseClicked(MouseEvent e) {}

  //------------------------------
  // Captura eventos de los TIMER
  //------------------------------
  public void TimerEnd(CTimer timer) {
    if ( (estado == ST_ESPERANDO_HELP) && timer.getName().equals("timer1")) {
      Mostrar();
      timer2 = new CTimer("timer2", timer_help, this);
      timer2.Start();
      estado = ST_MOSTRANDO_HELP;
    }
    else if ( (estado == ST_MOSTRANDO_HELP) && timer.getName().equals("timer2")) {
      Ocultar();
    } //endif
  } //end TimerEnd

  public void TimerError(CTimer timer, Exception e) {
    // // System_out_println(timer.getName()+":"+e);
  } //end TimerError

} //endclass CContextHelp
