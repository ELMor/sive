package capp2;

import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.StubSrvBD;

/**
 * @autor LSR
 * @version 1.0
 *
 * Lista de c�digos auxiliar para localizar registros en una tabla.
 *
 */
public class CListaCodigos
    extends Dialog {

  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  // applet
  private CApp app = null;

  // lista de valores
  private Lista vSnapShot = null;

  // configuraci�n
  private QueryTool qtConf = null;
  private String campoCodigo = null;

  // modo de operaci�n
  private int modoOperacion = modoNORMAL;

  // nodo seleccionado
  private int iNodo;

  // objetos
  XYLayout xYLayout = new XYLayout();
  Label lbl = new Label();
  TextField txtCod = new TextField();
  ButtonControl btnSearch1 = new ButtonControl();
  ButtonControl btnSearch2 = new ButtonControl();
  ButtonControl btnOk = new ButtonControl();
  ButtonControl btnCancel = new ButtonControl();
  CTabla tbl = new CTabla();

  // escuchadores
  CListaCodigosActionListener actionListener = new CListaCodigosActionListener(this);
  CListaCodigosActionAdapter actionAdapter = new CListaCodigosActionAdapter(this);

  // constructor
  public CListaCodigos(CApp a,
                       String title,
                       QueryTool qt,
                       String s) {
    super(a.getParentFrame(), "", true);

    // par�metros
    setTitle(title);
    app = a;

    // configuraci�n
    qtConf = qt;
    campoCodigo = s;

    // trae la primera trama
    this.btnSearch_actionPerformed("LIKE");

    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //Component initialization
  private void jbInit() throws Exception {
    //im�genes
    final String imgBUSCAR1 = "images/search1.gif";
    final String imgBUSCAR2 = "images/search2.gif";
    final String imgACEPTAR = "images/aceptar.gif";
    final String imgCANCELAR = "images/cancelar.gif";

    // pinta la lista
    setSize(245, 329);
    xYLayout.setHeight(329);
    xYLayout.setWidth(245);
    setResizable(false);
    lbl.setText("Buscar:");
    btnOk.setLabel("Aceptar");
    btnCancel.setLabel("Cancelar");

    tbl.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(
        "C�digo", '\n'));
    tbl.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String("194"),
        '\n'));
    tbl.setNumColumns(1);

    btnSearch1.setActionCommand("obtener");
    btnSearch2.setActionCommand("seleccionar");

    this.setLayout(xYLayout);
    this.add(lbl, new XYConstraints(7, 7, 47, 20));
    this.add(txtCod, new XYConstraints(57, 7, 104, -1));
    this.add(btnSearch1, new XYConstraints(164, 7, -1, -1));
    this.add(btnSearch2, new XYConstraints(194, 7, -1, -1));
    this.add(tbl, new XYConstraints(7, 60, 212, 180));
    this.add(btnOk, new XYConstraints(56, 253, 80, 26));
    this.add(btnCancel, new XYConstraints(140, 253, 80, 26));

    // carga las imagenes
    app.getLibImagenes().put(imgBUSCAR1);
    app.getLibImagenes().put(imgBUSCAR2);
    app.getLibImagenes().put(imgACEPTAR);
    app.getLibImagenes().put(imgCANCELAR);
    app.getLibImagenes().CargaImagenes();
    btnSearch1.setImage(app.getLibImagenes().get(imgBUSCAR1));
    btnSearch2.setImage(app.getLibImagenes().get(imgBUSCAR2));
    btnOk.setImage(app.getLibImagenes().get(imgACEPTAR));
    btnCancel.setImage(app.getLibImagenes().get(imgCANCELAR));

    new CContextHelp("Buscar", btnSearch1);
    new CContextHelp("Buscar", btnSearch2);
    new CContextHelp("Aceptar", btnOk);
    new CContextHelp("Salir", btnCancel);

    // instala los escuchadores
    //cierre de la ventana
    this.addWindowListener(new java.awt.event.WindowAdapter() {
      public void windowClosed(WindowEvent e) {
        this_windowClosed(e);
      }

      public void windowClosing(WindowEvent e) {
        this_windowClosing(e);
      }
    });

    // bot�n aceptar
    btnOk.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnOk_actionPerformed();
      }
    });

    // bot�n cancelar
    btnCancel.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnCancel_actionPerformed();
      }
    });

    // bot�n busqueda por exactitud
    btnSearch1.addActionListener(actionListener);

    // bot�n busqueda por similitud
    btnSearch2.addActionListener(actionListener);

    // click en la tabla
    tbl.addActionListener(actionAdapter);
  } //end jbInit

  public void show() {
    if (app != null) {
      app.getParentFrame().setEnabled(false);
      Dimension dimCapp = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension dimCmsg = getSize();
      setLocation( (dimCapp.width - dimCmsg.width) / 2,
                  (dimCapp.height - dimCmsg.height) / 2);
    } //endif
    super.show();
  } //end show

  void this_windowClosed(WindowEvent e) {
    if (app != null) {
      app.getParentFrame().setEnabled(true); // Siempre que se cierra
    } //endif
  } //end this_windowClosed

  void this_windowClosing(WindowEvent e) {
    iNodo = -1;
    dispose(); // Se cierra con la cruz
  } //end this_windowClosing

  // salir
  protected void quit() {
    if (tbl.getSelectedIndex() != BWTEnum.NOTFOUND) {
      iNodo = tbl.getSelectedIndex();
      dispose();
    }
  }

  // bot�n aceptar
  void btnOk_actionPerformed() {

    // nodo m�s datos
    if ( (vSnapShot.getEstado() == Lista.INCOMPLETA) &&
        (tbl.getSelectedIndex() == (tbl.countItems() - 1))) {
      iNodo = -1;
      dispose();

      // otro nodo
    }
    else {
      quit();
    }
  }

  // bot�n cancelar
  void btnCancel_actionPerformed() {
    iNodo = -1;
    dispose();
  }

  // bot�n busqueda
  public void btnSearch_actionPerformed(String sOp) {
    Lista vParam = null;
    Data dFila = null;

    // procesa
    try {

      modoOperacion = modoESPERA;
      Inicializar();

      // vacia la tabla
      tbl.clear();

      // actualiza la configuraci�n
      qtConf.putWhereType(campoCodigo, QueryTool.STRING);
      if (sOp.equals("=")) {
        qtConf.putWhereValue(campoCodigo, txtCod.getText());
      }
      else {
        qtConf.putWhereValue(campoCodigo, txtCod.getText() + "%");
      }
      qtConf.putOperator(campoCodigo, sOp);

      // obtiene y pinta la lista nueva
      vParam = new Lista();
      vParam.addElement(qtConf);
      vParam.setTrama(campoCodigo, "");

      app.getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
      vSnapShot = (Lista) app.getStub().doPost(2, vParam);
      vParam = null;

      // comprueba que hay datos
      if (vSnapShot.size() == 0) {
        app.showAdvise("No se encontraron datos con los criterios informados.");
      }
      else {

        // escribe las l�neas en la tabla
        for (int j = 0; j < vSnapShot.size(); j++) {
          dFila = (Data) vSnapShot.elementAt(j);
          tbl.addItem(dFila.get(campoCodigo), '&');
        }

        // verifica que sea la �ltima trama
        if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
          tbl.addItem("M�s datos ...");
        }
      }

      // error al procesar
    }
    catch (Exception e) {
      app.trazaLog(e);
      app.showError(e.getMessage());
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // habilita/deshabilita las opciones seg�n sea necesario
  void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        txtCod.setEnabled(true);
        btnSearch1.setEnabled(true);
        btnSearch2.setEnabled(true);
        tbl.setEnabled(true);
        btnOk.setEnabled(true);
        btnCancel.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCod.setEnabled(false);
        btnSearch1.setEnabled(false);
        btnSearch2.setEnabled(false);
        tbl.setEnabled(false);
        btnOk.setEnabled(false);
        btnCancel.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  // devuelve la l�nea seleccionada
  public Data getSelected() {
    Data dSel = null;

    if (iNodo != -1) {
      dSel = (Data) vSnapShot.elementAt(iNodo);

    }
    return dSel;
  }

  // seleccionar la lista
  void tbl_actionPerformed() {
    int iLast = 0;
    Lista vParam = null;
    Data dFila;

    // lista incompleta -> trae la siguiente trama
    if ( (vSnapShot.getEstado() == Lista.INCOMPLETA) &&
        (tbl.getSelectedIndex() == (tbl.countItems() - 1))) {

      // procesa
      try {

        modoOperacion = modoESPERA;
        Inicializar();

        iLast = tbl.countItems() - 1;

        // obtiene y pinta la lista nueva
        vParam = new Lista();

        // alamcena los par�metros para recuperar las tramas
        vParam.setTrama(vSnapShot.getTrama());
        vParam.addElement(qtConf);

        app.getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
        vSnapShot.addElements( (Lista) app.getStub().doPost(2, vParam));
        vParam = null;

        // escribe las l�neas en la tabla
        tbl.deleteItem(iLast);
        for (int j = iLast; j < vSnapShot.size(); j++) {
          dFila = (Data) vSnapShot.elementAt(j);
          tbl.addItem(dFila.get(campoCodigo), '&');
        }

        // verifica que sea la �ltima trama
        if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
          tbl.addItem("M�s datos ...");

          // error al procesar
        }
      }
      catch (Exception e) {
        app.trazaLog(e);
        app.showError(e.getMessage());
      }

      modoOperacion = modoNORMAL;
      Inicializar();

      // item seleccionado
    }
    else {
      this.quit();
    }
  }
}

// action listener para los botones de lupa
class CListaCodigosActionListener
    implements ActionListener, Runnable {
  CListaCodigos adaptee = null;
  ActionEvent e;

  public CListaCodigosActionListener(CListaCodigos adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    String sOp = null;

    if (e.getActionCommand().equals("obtener")) { // obtener
      sOp = "=";
    }
    else if (e.getActionCommand().equals("seleccionar")) { // seleccionar
      sOp = "LIKE";
    }

    adaptee.btnSearch_actionPerformed(sOp);
  }
}

// escuchador de los click en la tabla
class CListaCodigosActionAdapter
    implements jclass.bwt.JCActionListener, Runnable {
  CListaCodigos adaptee;

  CListaCodigosActionAdapter(CListaCodigos adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    new Thread(this).start();
  }

  public void run() {
    adaptee.tbl_actionPerformed();
  }
}
