package capp2;

import java.applet.Applet;
import java.util.Vector;

import java.awt.Image;
import java.awt.MediaTracker;

public class CCargadorImagen {

  protected Vector vImg = new Vector();
  protected Vector sImg = new Vector();
  protected Applet applet = null;

  public CCargadorImagen(CApp a) {
    applet = (Applet) a;
  }

  public CCargadorImagen(CApp a, String s[]) {
    applet = (Applet) a;

    // rellena las imagenes
    for (int i = 0; i < s.length; i++) {
      sImg.addElement(s[i]);
    }
  }

  public void CargaImagenes() {
    int i;
    Image img;

    // rellena el vector de imagenes
    if (sImg.size() > 0) {

      // Cargar Imagenes
      MediaTracker tracker = new MediaTracker(applet);

      for (i = 0; i < sImg.size(); i++) {
        img = applet.getImage(applet.getCodeBase(), (String) sImg.elementAt(i));
        vImg.addElement(img);
        tracker.addImage(img, i);
      }

      try {
        tracker.waitForAll();
      }
      catch (InterruptedException e) {
        vImg = new Vector();
        //# System_Out.println(e);
      } //endtry
    }
  }

  // lee una imagen
  public Image getImage(int i) {
    Image img = null;

    if (i <= vImg.size()) {
      img = (Image) vImg.elementAt(i);

    }
    return img;
  }

  // prepara una imagen
  public void setImage(String i) {
    sImg.addElement(i);
  }
}
