package capp2;

/**
 * @autor LSR
 * @version 1.0
 *
 * Librer�a de im�genes descargadas desde un applet.
 *
 * Para descargar im�genes, deber� cargarse la cola de peticiones con las referencias
 * relativas de las im�genes desde la URL del applet, y llamar al m�todo de descarga
 * CargarImagenes():
 *
 *	lib.add("imagenes/aceptar.gif");
 *	lib.add("imagenes/cancelar.gif");
 *	lib.CargarImagenes();
 *
 * Si la im�genen ya ha sido descargada, no la vuelve a descargar.
 * Para utilizar im�genes de la librer�a, se har� de la siguiente manera:
 *
 *	btn.setImage( lib.get("imagenes/aceptar.gif") );
 *
 */
import java.applet.Applet;
import java.util.Hashtable;
import java.util.Vector;

import java.awt.Image;
import java.awt.MediaTracker;

public class CLibImagenes {
  protected Vector req = null;
  protected Hashtable lib = null;
  protected Applet applet = null;

  public CLibImagenes(Applet a) {
    applet = a;
    lib = new Hashtable();
    req = new Vector();
  }

  public void CargaImagenes() {
    int i;
    Image img;

    // rellena el vector de imagenes
    if (req.size() > 0) {

      try {
        // Cargar Imagenes
        MediaTracker tracker = new MediaTracker(applet);

        for (i = 0; i < req.size(); i++) {
          img = applet.getImage(applet.getCodeBase(), (String) req.elementAt(i));
          lib.put(req.elementAt(i), img);
          tracker.addImage(img, i);
        }

        req = new Vector();

        tracker.waitForAll();
      }
      catch (InterruptedException e) {
        lib = new Hashtable();
      } //endtry
    }
  }

  // lee una imagen
  public Image get(String file) {
    return (Image) lib.get(file);
  }

  // prepara una imagen
  public void put(String file) {
    if (lib.get(file) == null) {
      req.addElement(file);
    }
  }
}