package capp2;

import java.awt.MenuItem;
import java.awt.event.ActionListener;

/**
 * Men� item con identificaci�n para la gesti�n de USU.
 * Sobreescribe el m�todo setEnable para encapsular las autorizaciones
 * sobre las acciones de la aplicaci�n.
 * Nota: si no se indica una clave, no se tiene en cuenta USU
 *
 * @autor LSR
 * @version 1.0
 */
public class UMenuItem
    extends MenuItem {
  private String key = null;
  private int code = 0;
  private CApp app = null;

  public UMenuItem(String label, String k, int c, CApp a,
                   ActionListener escuchador) {
    super(label);
    key = k;
    code = c;
    app = a;
    if (!app.isAllowed(key)) {
      super.setEnabled(false);
    }

    // el controlador
    addActionListener(escuchador);
  }

  public void setEnabled(boolean enabled) {
    if (app.isAllowed(key)) {
      super.setEnabled(enabled);
    }
    else {
      super.setEnabled(false);
    }
  }

  public int getCode() {
    return code;
  }
}
