package capp2;

/**
 * @autor LSR
 / @version 1.0
 *
 * Tabla.
 *
 */

// Realmente la CTabla la hice yo (Luis Rivera)en capp  pero da igual
// seguro que no me van a buscar ni a mi ni a San Rom�n, que est� jugando al futbol�n (LRG)

import java.awt.Color;
import java.awt.Insets;

import jclass.bwt.JCMultiColumnList;

public class CTabla
    extends JCMultiColumnList {

  public CTabla() {
    super();

    this.getList().setBackground(Color.white);
    this.getList().setHighlightColor(Color.lightGray);
    this.setColumnResizePolicy(jclass.bwt.BWTEnum.RESIZE_NONE);
    this.setScrollbarDisplay(jclass.bwt.BWTEnum.DISPLAY_VERTICAL_ONLY);
    this.setAutoSelect(true);
    this.setColumnLabelSort(false); //No ordena nunca las filas
    this.setInsets(new Insets(0, 0, 0, 0));
  }
}
