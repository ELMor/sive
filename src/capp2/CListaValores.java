package capp2;

/**
 * @autor LSR
 * @version 1.0
 *
 * Lista de valores auxiliar para localizar registros en una tabla.
 *Modificaciones : (LRG) Todo protected
 */
import java.util.Vector;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.WindowEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import sapp2.Data;
import sapp2.Lista;
import sapp2.QueryTool;
import sapp2.StubSrvBD;

public class CListaValores
    extends Dialog {

  protected final int modoNORMAL = 0;
  protected final int modoESPERA = 1;

  // applet
  protected CApp app = null;

  // lista de valores
  protected Lista vSnapShot = null;

  // configuraci�n
  protected QueryTool qtConf = null;
  protected Vector vTabla = null;

  // modo de operaci�n
  protected int modoOperacion = modoNORMAL;

  // nodo seleccionado
  protected int iNodo;

  // objetos
  XYLayout xYLayout = new XYLayout();
  Label lbl = new Label();
  protected CCodigo txtCod = new CCodigo();
  protected TextField txtDes = new TextField();
  protected CheckboxGroup opt = new CheckboxGroup();
  protected Checkbox optSearch1 = new Checkbox();
  protected Checkbox optSearch2 = new Checkbox();
  protected ButtonControl btnSearch1 = new ButtonControl();
  protected ButtonControl btnSearch2 = new ButtonControl();
  protected ButtonControl btnOk = new ButtonControl();
  protected ButtonControl btnCancel = new ButtonControl();
  protected CTabla tbl = new CTabla();

  // escuchadores
  clistavaloresActionListener actionListener = new clistavaloresActionListener(this);
  clistavaloresActionAdapter actionAdapter = new clistavaloresActionAdapter(this);
  CListaValoresItemAdapter itemAdapter = new CListaValoresItemAdapter(this);

  // constructor
  public CListaValores(CApp a,
                       String title,
                       QueryTool qt,
                       Vector v) {
    super(a.getParentFrame(), "", true);

    // par�metros
    setTitle(title);
    app = a;

    // configuraci�n
    qtConf = qt;
    vTabla = v;

    // trae la primera trama
    this.btnSearch_actionPerformed("LIKE");

    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  //Component initialization
  protected void jbInit() throws Exception {
    //im�genes
    final String imgBUSCAR1 = "images/search1.gif";
    final String imgBUSCAR2 = "images/search2.gif";
    final String imgACEPTAR = "images/aceptar.gif";
    final String imgCANCELAR = "images/cancelar.gif";

    // pinta la lista
    setSize(405, 320);
    xYLayout.setHeight(320);
    xYLayout.setWidth(405);
    setResizable(false);
    lbl.setText("Buscar:");
    btnOk.setLabel("Aceptar");
    btnCancel.setLabel("Cancelar");
    optSearch1.setLabel("C�digo");
    optSearch1.setCheckboxGroup(opt);
    optSearch1.addItemListener(itemAdapter);
    optSearch1.setState(true);
    optSearch2.setLabel("Descripci�n");
    optSearch2.setCheckboxGroup(opt);
    optSearch2.setState(false);
    optSearch2.addItemListener(itemAdapter);

    txtCod.setVisible(true);
    txtDes.setVisible(false);

    tbl.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(
        "C�digo\nDescripci�n", '\n'));
    tbl.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "130\n325"), '\n'));
    tbl.setNumColumns(2);

    btnSearch1.setActionCommand("obtener");
    btnSearch2.setActionCommand("seleccionar");

    this.setLayout(xYLayout);
    this.add(lbl, new XYConstraints(7, 7, 47, 20));
    this.add(txtCod, new XYConstraints(57, 7, 270, -1));
    this.add(txtDes, new XYConstraints(57, 7, 270, -1));
    this.add(optSearch1, new XYConstraints(7, 38, 150, 20));
    this.add(optSearch2, new XYConstraints(190, 38, 150, 20));
    this.add(btnSearch1, new XYConstraints(333, 7, -1, -1));
    this.add(btnSearch2, new XYConstraints(365, 7, -1, -1));
    this.add(tbl, new XYConstraints(7, 60, 387, 180));
    this.add(btnOk, new XYConstraints(230, 253, 80, 26));
    this.add(btnCancel, new XYConstraints(314, 253, 80, 26));

    // carga las imagenes
    app.getLibImagenes().put(imgBUSCAR1);
    app.getLibImagenes().put(imgBUSCAR2);
    app.getLibImagenes().put(imgACEPTAR);
    app.getLibImagenes().put(imgCANCELAR);
    app.getLibImagenes().CargaImagenes();
    btnSearch1.setImage(app.getLibImagenes().get(imgBUSCAR1));
    btnSearch2.setImage(app.getLibImagenes().get(imgBUSCAR2));
    btnOk.setImage(app.getLibImagenes().get(imgACEPTAR));
    btnCancel.setImage(app.getLibImagenes().get(imgCANCELAR));

    new CContextHelp("Buscar", btnSearch1);
    new CContextHelp("Buscar", btnSearch2);
    new CContextHelp("Aceptar", btnOk);
    new CContextHelp("Salir", btnCancel);

    // instala los escuchadores
    //cierre de la ventana
    this.addWindowListener(new java.awt.event.WindowAdapter() {
      public void windowClosed(WindowEvent e) {
        this_windowClosed(e);
      }

      public void windowClosing(WindowEvent e) {
        this_windowClosing(e);
      }
    });

    // bot�n aceptar
    btnOk.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnOk_actionPerformed();
      }
    });

    // bot�n cancelar
    btnCancel.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnCancel_actionPerformed();
      }
    });

    // bot�n busqueda por exactitud
    btnSearch1.addActionListener(actionListener);

    // bot�n busqueda por similitud
    btnSearch2.addActionListener(actionListener);

    // click en la tabla
    tbl.addActionListener(actionAdapter);
  } //end jbInit

  public void show() {
    if (app != null) {
      app.getParentFrame().setEnabled(false);
      Dimension dimCapp = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension dimCmsg = getSize();
      setLocation( (dimCapp.width - dimCmsg.width) / 2,
                  (dimCapp.height - dimCmsg.height) / 2);
    } //endif
    super.show();
  } //end show

  void this_windowClosed(WindowEvent e) {
    if (app != null) {
      app.getParentFrame().setEnabled(true); // Siempre que se cierra
    } //endif
  } //end this_windowClosed

  void this_windowClosing(WindowEvent e) {
    iNodo = -1;
    dispose(); // Se cierra con la cruz
  } //end this_windowClosing

  // salir
  protected void quit() {
    if (tbl.getSelectedIndex() != BWTEnum.NOTFOUND) {
      iNodo = tbl.getSelectedIndex();
      dispose();
    }
  }

  // bot�n aceptar
  void btnOk_actionPerformed() {

    // nodo m�s datos
    if ( (vSnapShot.getEstado() == Lista.INCOMPLETA) &&
        (tbl.getSelectedIndex() == (tbl.countItems() - 1))) {
      iNodo = -1;
      dispose();

      // otro nodo
    }
    else {
      quit();
    }
  }

  // bot�n cancelar
  void btnCancel_actionPerformed() {
    iNodo = -1;
    dispose();
  }

  // bot�n busqueda
  public void btnSearch_actionPerformed(String sOp) {
    String sFiltro = null;
    String sCampoFiltro = null;
    String sCampoCodigo = null;
    Lista vParam = null;
    Data dFila = null;

    // procesa
    try {

      modoOperacion = modoESPERA;
      Inicializar();

      // vacia la tabla
      tbl.clear();

      // actualiza la configuraci�n
      // ARG: toUpperCase (15/5/02)
      if (optSearch1.getState()) {
        sFiltro = txtCod.getText().trim().toUpperCase();
        sCampoFiltro = ( (String) vTabla.elementAt(0)).toUpperCase();
        sCampoCodigo = sCampoFiltro.toUpperCase();
        qtConf.putWhereType(sCampoFiltro, QueryTool.STRING);
        qtConf.putOperator(sCampoFiltro, sOp);
        if (sOp.equals("LIKE")) {
          sFiltro = sFiltro + "%";
        }
        qtConf.putWhereValue(sCampoFiltro, sFiltro);
        sFiltro = "%";
        sCampoFiltro = ( (String) vTabla.elementAt(1)).toUpperCase();
        qtConf.putWhereType(sCampoFiltro, QueryTool.STRING);
        qtConf.putOperator(sCampoFiltro, "LIKE");
        qtConf.putWhereValue(sCampoFiltro, sFiltro);
      }
      else {
        sFiltro = txtDes.getText().trim().toUpperCase();
        sCampoFiltro = ( (String) vTabla.elementAt(1)).toUpperCase();
        qtConf.putWhereType(sCampoFiltro, QueryTool.STRING);
        qtConf.putOperator(sCampoFiltro, sOp);
        if (sOp.equals("LIKE")) {
          sFiltro = "%" + sFiltro + "%";
        }
        qtConf.putWhereValue(sCampoFiltro, sFiltro);
        sFiltro = "%";
        sCampoFiltro = ( (String) vTabla.elementAt(0)).toUpperCase();
        sCampoCodigo = sCampoFiltro;
        qtConf.putWhereType(sCampoFiltro, QueryTool.STRING);
        qtConf.putOperator(sCampoFiltro, "LIKE");
        qtConf.putWhereValue(sCampoFiltro, sFiltro);
      }

      // obtiene y pinta la lista nueva
      vParam = new Lista();
      vParam.addElement(qtConf);
      vParam.setTrama(sCampoCodigo, "");

      app.getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);

      vSnapShot = (Lista) app.getStub().doPost(2, vParam);

      /*
            sapp2.SrvQueryTool srv = new sapp2.SrvQueryTool();
            srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                   "jdbc:oracle:thin:@192.168.0.13:1521:ICM",
                                   "dba_edo",
                                   "manager");
            vSnapShot = srv.doDebug(2, vParam);
       */

      vParam = null;

      // comprueba que hay datos
      if (vSnapShot.size() == 0) {
        app.showAdvise("No se encontraron datos con los criterios informados.");
      }
      else {

        // escribe las l�neas en la tabla
        for (int j = 0; j < vSnapShot.size(); j++) {
          dFila = (Data) vSnapShot.elementAt(j);
          tbl.addItem(dFila.get(vTabla.elementAt(0)) + "&" +
                      dFila.get(vTabla.elementAt(1)), '&');
        }

        // verifica que sea la �ltima trama
        if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
          tbl.addItem("M�s datos ...");
        }
      }

      // error al procesar
    }
    catch (Exception e) {
      app.trazaLog(e);
      app.showError(e.getMessage());
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // habilita/deshabilita las opciones seg�n sea necesario
  void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        txtCod.setEnabled(true);
        txtDes.setEnabled(true);
        btnSearch1.setEnabled(true);
        btnSearch2.setEnabled(true);
        optSearch1.setEnabled(true);
        optSearch2.setEnabled(true);
        tbl.setEnabled(true);
        btnOk.setEnabled(true);
        btnCancel.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCod.setEnabled(false);
        txtDes.setEnabled(false);
        btnSearch1.setEnabled(false);
        btnSearch2.setEnabled(false);
        optSearch1.setEnabled(false);
        optSearch2.setEnabled(false);
        tbl.setEnabled(false);
        btnOk.setEnabled(false);
        btnCancel.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  // devuelve la l�nea seleccionada
  public Data getSelected() {
    Data dSel = null;

    if (iNodo != -1) {
      dSel = (Data) vSnapShot.elementAt(iNodo);

    }
    return dSel;
  }

  // seleccionar la lista
  protected void tbl_actionPerformed() {
    int iLast = 0;
    Lista vParam = null;
    Data dFila;

    // lista incompleta -> trae la siguiente trama
    if ( (vSnapShot.getEstado() == Lista.INCOMPLETA) &&
        (tbl.getSelectedIndex() == (tbl.countItems() - 1))) {

      // procesa
      try {

        modoOperacion = modoESPERA;
        Inicializar();

        iLast = tbl.countItems() - 1;

        // obtiene y pinta la lista nueva
        vParam = new Lista();

        // alamcena los par�metros para recuperar las tramas
        vParam.setTrama(vSnapShot.getTrama());
        vParam.addElement(qtConf);

        app.getStub().setUrl(StubSrvBD.SRV_QUERY_TOOL);
        vSnapShot.addElements( (Lista) app.getStub().doPost(2, vParam));
        vParam = null;

        // escribe las l�neas en la tabla
        tbl.deleteItem(iLast);
        for (int j = iLast; j < vSnapShot.size(); j++) {
          dFila = (Data) vSnapShot.elementAt(j);
          tbl.addItem(dFila.get(vTabla.elementAt(0)) + "&" +
                      dFila.get(vTabla.elementAt(1)), '&');
        }

        // verifica que sea la �ltima trama
        if (vSnapShot.getEstado() == Lista.INCOMPLETA) {
          tbl.addItem("M�s datos ...");

          // error al procesar
        }
      }
      catch (Exception e) {
        app.trazaLog(e);
        app.showError(e.getMessage());
      }

      modoOperacion = modoNORMAL;
      Inicializar();

      // item seleccionado
    }
    else {
      this.quit();
    }
  }

  void chkbx_itemStateChanged(ItemEvent e) {
    if (e.getItem().equals("C�digo")) {
      txtCod.setVisible(true);
      txtCod.setText(txtDes.getText().trim().toUpperCase());
      txtDes.setVisible(false);
    }
    else {
      txtCod.setVisible(false);
      txtDes.setVisible(true);
      txtDes.setText(txtCod.getText().trim());
    }
    doLayout();
  }
}

// action listener para los botones de lupa
class clistavaloresActionListener
    implements ActionListener, Runnable {
  CListaValores adaptee = null;
  ActionEvent e;

  public clistavaloresActionListener(CListaValores adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    String sOp = null;

    if (e.getActionCommand().equals("obtener")) { // obtener
      sOp = "=";
    }
    else if (e.getActionCommand().equals("seleccionar")) { // seleccionar
      sOp = "LIKE";
    }

    adaptee.btnSearch_actionPerformed(sOp);
  }
}

// escuchador de los click en la tabla
class clistavaloresActionAdapter
    implements jclass.bwt.JCActionListener, Runnable {
  CListaValores adaptee;

  clistavaloresActionAdapter(CListaValores adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    new Thread(this).start();
  }

  public void run() {
    adaptee.tbl_actionPerformed();
  }
}

// control de cambio de buscar por c�digo/descripci�n
class CListaValoresItemAdapter
    implements java.awt.event.ItemListener {
  CListaValores adaptee;

  CListaValoresItemAdapter(CListaValores adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {
    adaptee.chkbx_itemStateChanged(e);
  }
}
