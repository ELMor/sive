
package infedonoma;

import java.util.ResourceBundle;

import capp.CApp;

public class CasosEDOnoMA
    extends CApp {

  ResourceBundle res;

  public CasosEDOnoMA() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("infedonoma.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    VerPanel(res.getString("msg2.Text"), new Pan_EDOnoMA(a), false);
    VerPanel(res.getString("msg2.Text"));
  }

}