
package infedonoma;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import Registro.RegistroConsultas;
import capp.CLista;
import sapp.DBServlet;

//import infcobsem.*;

public class SrvInfEDOnoMA
    extends DBServlet {

  protected String query = "";
  protected String periodo = "";

  // informes
  final int erwCASOS_EDO = 1;

  // C�digos de municipio desconocido
  final String mun_desconocido = "'888', '999'";

  // ARG:
  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;

    ResultSet rs = null;

    // control
    int i = 1;
    int posTotal = 1;
    int posRelativa = 1;
    int paso = 1;
    String totCod = null;
    int totTeorico = 0;
    int totReal = 0;
    float totCobertura = 0;
    int todoTeorico = 0;
    int todoReal = 0;
    float todoCobertura = 0;
    int todoLineas = 0;
    String sCodigos = null;
    DataEDOnoMA Auxiliar = null;
    Vector totales = new Vector();
    Vector totalesAux = new Vector();
    int totalRegistros = 0;
    String p1 = "";
    String p2 = "";
    String pz = "";
    String p1a = "";
    String p2a = "";
    String pza = "";

    // buffers
    String sCod;
    int iNum;
    DataEDOnoMA dat = null;
    Param_C9_2_1 datoAuxi = null;

    // objetos de datos
    CLista data = new CLista();
    Vector registros = new Vector();
    Vector registrosAux = new Vector();
    Vector ultSem = new Vector();

    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;

      // establece la conexi�n con la base de datos
    }
    con = openConnection();
    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    con.setAutoCommit(true);

    dat = (DataEDOnoMA) param.firstElement();
    // modos de operaci�n
    switch (opmode) {

      // l�neas del modelo
      case erwCASOS_EDO:
        try {
              /*query = "select a.CD_NIVEL_1,a.CD_E_NOTIF,a.CD_CENTRO,a.DS_E_NOTIF,"
               +"b.NM_EDO,c.CD_NIVEL_1,c.CD_NIVEL_2,c.CD_ZBS,c.CD_MUN,c.CD_ENFERMO,"
            +"c.CD_ENFCIE,c.CD_SEMEPI,c.CD_ANOEPI "
            +"from sive_e_notif a, sive_notif_edoi b, sive_edoind c "
            +"where a.CD_E_NOTIF=b.CD_E_NOTIF and b.IT_PRIMERO='S' and b.CD_FUENTE = 'E' and "
            +"c.NM_EDO=b.NM_EDO ";
           */

          // modificacion jlt 30/10/2001
          // recuperamos la descripci�n de la enfermedad

          query = "select a.CD_NIVEL_1,a.CD_E_NOTIF,a.CD_CENTRO,a.DS_E_NOTIF,"
              +
              "b.NM_EDO,c.CD_NIVEL_1,c.CD_NIVEL_2,c.CD_ZBS,c.CD_MUN,c.CD_ENFERMO,"
              + "c.CD_ENFCIE, d.DS_PROCESO, c.CD_SEMEPI,c.CD_ANOEPI "
              +
              "from sive_e_notif a, sive_notif_edoi b, sive_edoind c , sive_procesos d "
              + "where a.CD_E_NOTIF=b.CD_E_NOTIF and c.CD_ENFCIE = d.CD_ENFCIE and b.IT_PRIMERO='S' and b.CD_FUENTE = 'E' and "
              + "c.NM_EDO=b.NM_EDO ";

          if (dat.porMunicipio) {
            // ARG: Codigos de municipio DESCONOCIDO
            query += "and (c.CD_MUN in (" + mun_desconocido + "))";
            //query += "and (c.CD_MUN is null or c.CD_MUN='')";
          }
          else {
            // ARG: Codigo 99 -> DESCONOCIDO
            query += "and (c.CD_NIVEL_1 = '99')";
            //query += "and (c.CD_NIVEL_1 is null or c.CD_NIVEL_1='')";
          }

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {
            periodo =
                " and c.CD_SEMEPI >= ? and c.CD_SEMEPI <= ? and c.CD_ANOEPI = ? ";
          }
          else {
            periodo = " and ((c.CD_ANOEPI = ? and c.CD_SEMEPI>= ? )or "
                + " (c.CD_ANOEPI > ? and c.CD_ANOEPI < ? )or "
                + " (c.CD_ANOEPI = ? and c.CD_SEMEPI <= ? ))";

          }
          query += periodo;

          //Busca por municipio (munic no informado) pero soilo en el area indicada
          if ( (dat.sCdArea.compareTo("") != 0) && (dat.porMunicipio)) {
            query += " and a.CD_NIVEL_1='" + dat.sCdArea + "' ";
          }

          if (dat.sCdEnfermedad.compareTo("") != 0) {
            query += " and c.CD_ENFCIE='" + dat.sCdEnfermedad + "' ";
          }

          //query += "order by a.CD_NIVEL_1, a.CD_E_NOTIF, c.CD_ENFCIE";
          query += "order by a.CD_NIVEL_1, a.CD_E_NOTIF, d.DS_PROCESO";

          st = con.prepareStatement(query);

          if (dat.sAnoDesde.equals(dat.sAnoHasta)) {
            // semana desde
            st.setString(1, dat.sSemDesde);
            // semana hasta
            st.setString(2, dat.sSemHasta);
            // a�o
            st.setString(3, dat.sAnoDesde);

            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(dat.sSemDesde);
            registroConsultas.insertarParametro(dat.sSemHasta);
            registroConsultas.insertarParametro(dat.sAnoDesde);

          }
          else { // los a�os inicial y final son distintos
            // a�o inicial
            st.setString(1, dat.sAnoDesde);
            st.setString(3, dat.sAnoDesde);
            // semana inicial
            st.setString(2, dat.sSemDesde);
            // a�o final
            st.setString(4, dat.sAnoHasta);
            st.setString(5, dat.sAnoHasta);
            // semana final
            st.setString(6, dat.sSemHasta);

            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(dat.sAnoDesde);
            registroConsultas.insertarParametro(dat.sSemDesde);
            registroConsultas.insertarParametro(dat.sAnoDesde);
            registroConsultas.insertarParametro(dat.sAnoHasta);
            registroConsultas.insertarParametro(dat.sAnoHasta);
            registroConsultas.insertarParametro(dat.sSemHasta);

          }
          rs = st.executeQuery();

          // ARG: Registro LORTAD
          registroConsultas.registrar("SIVE_EDOIND", query, "CD_ENFERMO",
                                      "", "SrvInfEDONoMa", true);

          registrosAux = new Vector();

          while (rs.next()) {
            datoAuxi = new Param_C9_2_1();
            datoAuxi.CD_NIVEL_1 = rs.getString(1);
            datoAuxi.CD_NIVEL_2 = ""; //rs.getString("CD_NIVEL_2");
            datoAuxi.DS_SEMANA = rs.getString("CD_ANOEPI") + "-" +
                rs.getString("CD_SEMEPI");
            //datoAuxi.DS_ENFCIE = rs.getString("CD_ENFCIE");

            // modificacion jlt 30/10/2001
            datoAuxi.DS_ENFCIE = rs.getString("DS_PROCESO");

            datoAuxi.CD_ENFERMO = rs.getInt("CD_ENFERMO");
            p1 = rs.getString(6);
            p2 = rs.getString("CD_NIVEL_2");
            pz = rs.getString("CD_ZBS");

            /*
                         if (p1 != null)
                         System_out.println("Niv1 "+p1);
                         if (p2 != null)
                         System_out.println("Niv2 "+p2);
                         if (pz != null)
                         System_out.println("Zbs "+pz);
             */

            if ( (p1 == null) || (p2 == null) || (pz == null) ||
                (p1.equals("")) || (p2.equals("")) || (pz.equals(""))) {
              datoAuxi.DS_ZBS = "";
            }
            else {
              datoAuxi.DS_ZBS = p1 + "-" + p2 + "-" + pz;
              // System_out.println("Zona");
              // System_out.println(datoAuxi.DS_ZBS);
            }
            datoAuxi.NM_EDO = rs.getInt("NM_EDO");
            // System_out.println("NumEdo "+ datoAuxi.NM_EDO);
            datoAuxi.CD_MUN = rs.getString("CD_MUN");
            datoAuxi.CD_C_NOTIF = rs.getString("CD_CENTRO");
            datoAuxi.CD_E_NOTIF = rs.getString("CD_E_NOTIF");
            datoAuxi.DS_E_NOTIF = rs.getString("DS_E_NOTIF");
            registrosAux.addElement(datoAuxi);
          }

          rs.close();
          st.close();
          rs = null;
          st = null;
          // n�mero total de registros
          totalRegistros = registrosAux.size();

          // descripci�n de los equipos
          ////#// System_out.println("EQUIPOS");
          /*
                     if (registrosAux.size() > 0){
            ////#// System_out.println("Descipciones de Equipos:");
            ////#// System_out.println(registros.size());
            registrosAux = new Vector();
            // recorremos el vector
            for (int k = 0;k < registrosAux.size();k++){
              datoAuxi = (Param_C9_2_1) registros.elementAt(k);
               query = "select CD_E_NOTIF,DS_E_NOTIF,CD_CENTRO from sive_e_notif "+
               "where CD_E_NOTIF in (select CD_E_NOTIF from sive_notif_edoi where "+
                "NM_EDO=? and IT_PRIMERO='S')";
              st = con.prepareStatement(query);
              // c�digo edo
              st.setInt(1, datoAuxi.NM_EDO);
              ////#// System_out.println("nm_edo");
              ////#// System_out.println(datoAuxi.NM_EDO);
              rs = st.executeQuery();
              if (rs.next()){
                datoAuxi.CD_C_NOTIF = rs.getString("CD_CENTRO");
                datoAuxi.CD_E_NOTIF = rs.getString("CD_E_NOTIF");
                datoAuxi.DS_E_NOTIF = rs.getString("DS_E_NOTIF");
                ////#// System_out.println("Un dato");
              }
              registrosAux.addElement(datoAuxi);
              rs.close();
              st.close();
              rs = null;
              st = null;
            }
                     }
           */
          // descripci�n de los centros
          registros = new Vector();

          if (registrosAux.size() > 0) {
            //registros = new Vector();
            // recorremos el vector
            for (int k = 0; k < registrosAux.size(); k++) {
              datoAuxi = (Param_C9_2_1) registrosAux.elementAt(k);
              query = "select DS_CENTRO from sive_c_notif " +
                  "where CD_CENTRO=?";
              st = con.prepareStatement(query);
              st.setString(1, datoAuxi.CD_C_NOTIF);
              rs = st.executeQuery();
              if (rs.next()) {
                datoAuxi.DS_C_NOTIF = rs.getString("DS_CENTRO");
              }
              registros.addElement(datoAuxi);
              rs.close();
              st.close();
              rs = null;
              st = null;
            }
          }
          // ahora los datos del enfermo
          registrosAux = new Vector();

          if (registros.size() > 0) {
            // recorremos el vector
            for (int k = 0; k < registros.size(); k++) {
              datoAuxi = (Param_C9_2_1) registros.elementAt(k);
              query = "select DS_APE1,DS_APE2,DS_NOMBRE from sive_enfermo " +
                  "where CD_ENFERMO=?";
              st = con.prepareStatement(query);

              // c�digo enfermo
              st.setInt(1, datoAuxi.CD_ENFERMO);

              // ARG: Se a�aden parametros
              registroConsultas.insertarParametro(String.valueOf(datoAuxi.
                  CD_ENFERMO));

              rs = st.executeQuery();

              // ARG: Registro LORTAD
              registroConsultas.registrar("SIVE_ENFERMO", query, "CD_ENFERMO",
                                          String.valueOf(datoAuxi.CD_ENFERMO),
                                          "SrvInfEDONoMa", true);

              if (rs.next()) {
                p1a = rs.getString("DS_APE1");
                p2a = rs.getString("DS_APE2");
                pza = rs.getString("DS_NOMBRE");
                if (p2a != null) {
                  p1a += " " + p2a;

                }
                if (pza != null) {
                  p1a += ", " + pza;
                }
                datoAuxi.DS_ENFERMO = p1a;
                ////#// System_out.println(p1a);

              }
              registrosAux.addElement(datoAuxi);
              rs.close();
              st.close();
              rs = null;
              st = null;
            }
          }

              /* ARG: Tanto el nombre del municipio como del area se han de consultar
                  siempre, aunque estos sean desconocidos.
           // ahora los datos del municipio/�rea
           registros = new Vector();
           totales = new Vector();
           ////#// System_out.println("MUNICIPIO/AREA");
           if (registrosAux.size() > 0){
             // recorremos el vector
             for (int k = 0;k < registrosAux.size();k++){
               datoAuxi = (Param_C9_2_1) registrosAux.elementAt(k);
               if (dat.porMunicipio){
               if ((datoAuxi.CD_NIVEL_1 != null)&&(datoAuxi.CD_NIVEL_2 != null)&&
                     (datoAuxi.CD_ZBS != null)){
                   query = "select DS_NIVEL_1 from sive_nivel1_s "+
                     "where CD_NIVEL_1=?";
                 }else{
                   query ="";
                   datoAuxi.CD_NIVEL_2 = "";
                   datoAuxi.CD_MUN = "";
                   datoAuxi.DS_MUN = "";
                 }
               }else{
                 if ((datoAuxi.CD_MUN != null)){
                   query = "select DS_MUN from sive_municipio "+
                     "where CD_MUN=?";
                 }else{
                   query = "";
                   datoAuxi.CD_NIVEL_2 = "";
                   datoAuxi.CD_MUN = "";
                   datoAuxi.DS_MUN = "";
                 }
               }
               if (query.compareTo("") !=0){
                 st = con.prepareStatement(query);
               }
               if ((dat.porMunicipio)&&(query.compareTo("") !=0)){
                 // c�digo nivel 1
                 st.setString(1, datoAuxi.CD_NIVEL_1);
               }else if ((query.compareTo("") !=0)){
                 // c�digo municipio
                 st.setString(1, datoAuxi.CD_MUN);
               }
               if ((query.compareTo("") !=0)){
                 rs = st.executeQuery();
                 if (rs.next()){
                   if (dat.porMunicipio){
                     datoAuxi.DS_NIVEL_1 = rs.getString("DS_NIVEL_1");
                   }else{
                     datoAuxi.DS_MUN = rs.getString("DS_MUN");
                     //datoAuxi.DS_ZBS = "";
                   }
                 }
               }
               totales.addElement(datoAuxi);
               if (query.compareTo("") != 0){
                 rs.close();
                 st.close();
                 rs = null;
                 st = null;
               }
             }
           } */

          // ARG: AREA
          totalesAux = new Vector();
          if (registrosAux.size() > 0) {
            // recorremos el vector
            for (int k = 0; k < registrosAux.size(); k++) {
              datoAuxi = (Param_C9_2_1) registrosAux.elementAt(k);
              if ( (datoAuxi.CD_NIVEL_1 != null) && (datoAuxi.CD_NIVEL_2 != null) &&
                  (datoAuxi.CD_ZBS != null)) {
                query = "select DS_NIVEL_1 from sive_nivel1_s " +
                    "where CD_NIVEL_1= ? ";
              }
              else {
                query = "";
                datoAuxi.CD_NIVEL_2 = "";
                datoAuxi.CD_MUN = "";
                datoAuxi.DS_MUN = "";
              }
              if (query.compareTo("") != 0) {
                st = con.prepareStatement(query);
                // c�digo nivel 1
                st.setString(1, datoAuxi.CD_NIVEL_1);

                rs = st.executeQuery();
                if (rs.next()) {
                  datoAuxi.DS_NIVEL_1 = rs.getString("DS_NIVEL_1");
                }
                totalesAux.addElement(datoAuxi);

                rs.close();
                st.close();
                rs = null;
                st = null;
              }
            }
          }

          // ARG: MUNICIPIO
          totales = new Vector();
          if (totalesAux.size() > 0) {
            // recorremos el vector
            for (int k = 0; k < totalesAux.size(); k++) {
              datoAuxi = (Param_C9_2_1) totalesAux.elementAt(k);
              if ( (datoAuxi.CD_MUN != null)) {
                query = "select DS_MUN from sive_municipio " +
                    "where CD_MUN=?";
              }
              else {
                query = "";
                datoAuxi.CD_NIVEL_2 = "";
                datoAuxi.CD_MUN = "";
                datoAuxi.DS_MUN = "";
              }
              if (query.compareTo("") != 0) {
                st = con.prepareStatement(query);
                // c�digo municipio
                st.setString(1, datoAuxi.CD_MUN);

                rs = st.executeQuery();
                if (rs.next()) {
                  datoAuxi.DS_MUN = rs.getString("DS_MUN");
                }
                totales.addElement(datoAuxi);

                rs.close();
                st.close();
                rs = null;
                st = null;
              }
            }
          }

        }
        catch (Exception er) {
          er.printStackTrace();
          throw (er);
        }
        break;
    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);
    if (totales.size() > 0) {
      totales.trimToSize();
      data.addElement(totales);
      data.addElement(new Integer(totalRegistros));
      data.trimToSize();

    }
    else {
      data = null;

    }

    return data;
  }

}
