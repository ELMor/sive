
package infedonoma;

import java.io.Serializable;

public class Param_C9_2_1
    implements Serializable {
  public String CD_NIVEL_1 = "";
  public String DS_NIVEL_1 = "";
  public String DS_ENFCIE = "";
  public String DS_SEMANA = "";
  public String DS_ENFERMO = "";
  public int CD_ENFERMO = 0;
  public int NM_EDO = 0;
  public String CD_PROV = "";
  public String DS_PROV = "";
  public String CD_MUN = "";
  public String DS_MUN = "";
  public String CD_NIVEL_2 = "";
  public String DS_NIVEL_2 = "";
  public String CD_ZBS = "";
  public String DS_ZBS = "";
  public String CD_E_NOTIF = "";
  public String DS_E_NOTIF = "";
  public String CD_C_NOTIF = "";
  public String DS_C_NOTIF = "";

  public int iPagina = 0;
  public boolean bInformeCompleto = false;

  public Param_C9_2_1() {

  }

  /*public Param_C9_2_1(String semana, String fecha, String enfnum, String numindiv,
                       String itRes,String nTeo, String nReal) {
    DS_SEM = semana;
    FC_RECEP = fecha;
    NM_NUM = enfnum;
    NM_INDIV = numindiv;
    IT_RES = itRes;
    NM_NTEO = nTeo;
    NM_NREAL = nReal;
    iPagina = 0;
     }
   */

}
