package infedonoma;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.SystemColor;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo.DataCat;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
import utilidades.PanFechas;

public class Pan_EDOnoMA
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  //rutas imagenes
  final String imgLUPA = "images/Magnify.gif";
  final String imgLIMPIAR = "images/vaciar.gif";
  final String imgGENERAR = "images/grafico.gif";

  protected StubSrvBD stubCliente = new StubSrvBD();

  public DataEDOnoMA paramC2;

  protected int modoOperacion = modoNORMAL;

  protected Pan_InfEDOnoMA informe;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETMun = "servlet/SrvMun";
  final String strSERVLETProv = "servlet/SrvCat2";
  final String strSERVLETEquipo = "servlet/SrvEqNot";
  final String strSERVLETEnfermedad = "servlet/SrvEnfVigi";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  XYLayout xYLayout = new XYLayout();

  Label lblHasta = new Label();
  Label lblDesde = new Label();
  Label lblEnfermedad = new Label();
  CCampoCodigo txtEnfermedad = new CCampoCodigo(); /*E*/
  ButtonControl btnEnfermedad = new ButtonControl();
  TextField txtDesEnfermedad = new TextField();
  PanFechas fechasDesde;
  PanFechas fechasHasta;
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  //focusAdapter txtFocusAdapter = new focusAdapter(this);
  //textAdapter txtTextAdapter = new textAdapter(this);
  //actionListener btnActionListener = new actionListener(this);
  Label lblArea = new Label();
  CCampoCodigo txtArea = new CCampoCodigo(); /*E*/
  ButtonControl btnArea = new ButtonControl();
  TextField txtDesArea = new TextField();
  Label lblCriterio = new Label();
  CheckboxGroup checkboxGroup1 = new CheckboxGroup();
  Checkbox chkMun = new Checkbox();
  Checkbox chkArea = new Checkbox();

  public Pan_EDOnoMA(CApp a) {
    try {

      setApp(a);
      res = ResourceBundle.getBundle("infedonoma.Res" + a.getIdioma());
      informe = new Pan_InfEDOnoMA(a);
      fechasDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      fechasHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      jbInit();
      this.txtArea.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesArea.setText(this.app.getDS_NIVEL1_DEFECTO());
      informe.setEnabled(false);
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(301);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));
    lblArea.setText(res.getString("lblArea.Text"));
    txtArea.addFocusListener(new Pan_EDOnoMA_txtArea_focusAdapter(this));
    txtArea.addKeyListener(new Pan_EDOnoMA_txtArea_keyAdapter(this));

    // gestores de eventos
    /*btnEnfermedad.addActionListener(btnActionListener);
         btnLimpiar.addActionListener(btnActionListener);
         btnInforme.addActionListener(btnActionListener);
         txtEnfermedad.addTextListener(txtTextAdapter);
         txtEnfermedad.addFocusListener(txtFocusAdapter);
     */

    //btnCtrlBuscarEnfer.setLabel("btnCtrlBuscarPro1");
    btnEnfermedad.setActionCommand("buscarEnfermedad");
    txtDesEnfermedad.setEditable(false);
    txtDesEnfermedad.setEnabled(false); /*E*/
    txtDesArea.setEditable(false);
    txtDesArea.setEnabled(false); /*E*/
    lblCriterio.setText(res.getString("lblCriterio.Text"));
    chkMun.setLabel(res.getString("chkMun.Label"));
    chkMun.setCheckboxGroup(checkboxGroup1);
    chkArea.setLabel(res.getString("chkArea.Label"));
    chkArea.setCheckboxGroup(checkboxGroup1);
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    txtEnfermedad.setName("txtCodEquNot");
    txtEnfermedad.setBackground(SystemColor.control);
    txtEnfermedad.addFocusListener(new Pan_EDOnoMA_txtEnfermedad_focusAdapter(this));
    txtEnfermedad.addKeyListener(new Pan_EDOnoMA_txtEnfermedad_keyAdapter(this));
    //btnArea.addActionListener(btnActionListener);
    btnArea.setActionCommand("buscarArea");
    btnArea.setImageURL(new URL(app.getCodeBase(), imgLUPA));

    lblHasta.setText(res.getString("lblHasta.Text"));
    lblEnfermedad.setText(res.getString("lblEnfermedad.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));

    xYLayout.setWidth(596);

    this.setLayout(xYLayout);

    this.add(lblDesde, new XYConstraints(26, 42, 51, -1));
    this.add(fechasDesde, new XYConstraints(40, 42, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 80, 51, -1));
    this.add(fechasHasta, new XYConstraints(40, 80, -1, -1));
    this.add(lblEnfermedad, new XYConstraints(26, 137, 110, -1));
    this.add(txtEnfermedad, new XYConstraints(143, 137, 77, -1));
    this.add(btnEnfermedad, new XYConstraints(225, 137, -1, -1));
    this.add(txtDesEnfermedad, new XYConstraints(254, 137, 287, -1));
    this.add(lblArea, new XYConstraints(26, 171, -1, -1));
    this.add(txtArea, new XYConstraints(143, 171, 77, -1));
    this.add(btnArea, new XYConstraints(225, 171, -1, -1));
    this.add(txtDesArea, new XYConstraints(254, 171, 287, -1));
    this.add(lblCriterio, new XYConstraints(26, 206, 113, -1));
    this.add(chkMun, new XYConstraints(143, 206, -1, -1));
    this.add(chkArea, new XYConstraints(254, 206, -1, -1));
    this.add(btnLimpiar, new XYConstraints(389, 256, -1, -1));
    this.add(btnInforme, new XYConstraints(475, 256, -1, -1));
    //this.add(checkboxGroup1);

    btnArea.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnArea.addActionListener(new Pan_EDOnoMA_btnArea_actionAdapter(this));
    btnEnfermedad.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnEnfermedad.addActionListener(new Pan_EDOnoMA_btnEnfermedad_actionAdapter(this));
    btnLimpiar.setImageURL(new URL(app.getCodeBase(), imgLIMPIAR));
    btnLimpiar.addActionListener(new Pan_EDOnoMA_btnLimpiar_actionAdapter(this));
    btnInforme.setImageURL(new URL(app.getCodeBase(), imgGENERAR));
    btnInforme.addActionListener(new Pan_EDOnoMA_btnInforme_actionAdapter(this));

    chkMun.setState(true);

    this.modoOperacion = modoNORMAL;
    Inicializar();

  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (fechasDesde.txtAno.getText().length() == 0) ||
        (fechasHasta.txtAno.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtEnfermedad.setEnabled(true);
        btnEnfermedad.setEnabled(true);
        //txtDesEnfermedad.setEnabled(true); /*E*/
        txtDesEnfermedad.setEditable(false);

        txtArea.setEnabled(true);
        btnArea.setEnabled(true);
        //txtDesArea.setEnabled(true); /*E*/
        txtDesArea.setEditable(false);

        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtEnfermedad.setEnabled(false);
        btnEnfermedad.setEnabled(false);
        //txtDesEnfermedad.setEnabled(false); /*E*/
        txtDesEnfermedad.setEditable(false);

        txtArea.setEnabled(false);
        btnArea.setEnabled(false);
        //txtDesArea.setEnabled(false); /*E*/
        txtDesArea.setEditable(false);

        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    modoOperacion = modoESPERA;
    Inicializar();

    txtEnfermedad.setText("");
    txtDesEnfermedad.setText("");
    txtArea.setText("");
    txtDesArea.setText("");

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnInforme_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    paramC2 = new DataEDOnoMA();

    if (isDataValid()) {

      modoOperacion = modoESPERA;
      Inicializar();

      if (!txtDesEnfermedad.getText().equals("")) {
        paramC2.sDsEnfermedad = txtDesEnfermedad.getText();
        paramC2.sCdEnfermedad = txtEnfermedad.getText();
      }

      if (!txtDesArea.getText().equals("")) {
        paramC2.sDsArea = txtDesArea.getText();
        paramC2.sCdArea = txtArea.getText();
      }

      paramC2.sAnoDesde = fechasDesde.txtAno.getText();
      paramC2.sAnoHasta = fechasHasta.txtAno.getText();
      if (fechasDesde.txtCodSem.getText().length() == 1) {
        paramC2.sSemDesde = "0" + fechasDesde.txtCodSem.getText();
      }
      else {
        paramC2.sSemDesde = fechasDesde.txtCodSem.getText();
      }
      if (fechasHasta.txtCodSem.getText().length() == 1) {
        paramC2.sSemHasta = "0" + fechasHasta.txtCodSem.getText();
      }
      else {
        paramC2.sSemHasta = fechasHasta.txtCodSem.getText();
      }
      if (chkMun.getState()) {
        paramC2.porMunicipio = true;
      }
      else {
        paramC2.porMunicipio = false;
        // habilita el panel
      }
      modoOperacion = modoESPERA;
      Inicializar();
      informe.setEnabled(true);
      informe.datosPar = paramC2;
      //E //#// System_out.println("1");
      //E //#// System_out.println(paramC2.sAnoDesde);
      //E //#// System_out.println(paramC2.sSemDesde);
      //E //#// System_out.println(paramC2.sAnoHasta);
      //E //#// System_out.println(paramC2.sSemHasta);
      if (informe.GenerarInforme()) {
        informe.show();
      }
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg3.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  void btnEnfermedad_actionPerformed(ActionEvent e) {
    DataCat data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaEnf lista = new CListaEnf(this.app,
                                      res.getString("msg4.Text"),
                                      stubCliente,
                                      strSERVLETEnfermedad,
                                      servletOBTENER_X_CODIGO,
                                      servletOBTENER_X_DESCRIPCION,
                                      servletSELECCION_X_CODIGO,
                                      servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataCat) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtEnfermedad.setText(data.getCod());
      txtDesEnfermedad.setText(data.getDes());
      //txtEnfermedad.addTextListener(txtTextAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnArea_actionPerformed(ActionEvent e) {
    DataNivel1 data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaArea lista = new CListaArea(this.app,
                                        res.getString("msg5.Text"),
                                        stubCliente,
                                        strSERVLETNivel1,
                                        servletOBTENER_X_CODIGO,
                                        servletOBTENER_X_DESCRIPCION,
                                        servletSELECCION_X_CODIGO,
                                        servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtArea.setText(data.getCod());
      txtDesArea.setText(data.getDes());

    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void txtEnfermedad_keyPressed(KeyEvent e) {
    txtDesEnfermedad.setText("");
  }

  void txtArea_keyPressed(KeyEvent e) {
    txtDesArea.setText("");
  }

  void txtEnfermedad_focusLost(FocusEvent e) {
    // datos de envio
    DataCat enfcie;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    // gestion de datos
    param = new CLista();
    param.addElement(new DataCat(txtEnfermedad.getText()));
    strServlet = strSERVLETEnfermedad;
    modoServlet = servletOBTENER_X_CODIGO;

    // busca el item
    if (txtEnfermedad.getText().length() > 0) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          enfcie = (DataCat) param.firstElement();
          txtEnfermedad.setText(enfcie.getCod());
          txtDesEnfermedad.setText(enfcie.getDes());

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg6.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  void txtArea_focusLost(FocusEvent e) {
    // datos de envio
    DataNivel1 nivel1;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    // gestion de datos
    param = new CLista();
    param.addElement(new DataNivel1(txtArea.getText()));
    strServlet = strSERVLETNivel1;
    modoServlet = servletOBTENER_X_CODIGO;

    // busca el item
    if (txtArea.getText().length() > 0) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          nivel1 = (DataNivel1) param.firstElement();
          txtArea.setText(nivel1.getCod());
          txtDesArea.setText(nivel1.getDes());

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg6.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

} //clase

////////////////////// Clases para listas

// lista de valores
class CListaArea
    extends CListaValores {
  protected Pan_EDOnoMA panel;

  public CListaArea(CApp app,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(app,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

// lista de valores
class CListaEnf
    extends CListaValores {

  public CListaEnf(CApp a,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataCat(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataCat) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCat) o).getDes());
  }
}

class Pan_EDOnoMA_btnEnfermedad_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_EDOnoMA adaptee;
  ActionEvent e;

  Pan_EDOnoMA_btnEnfermedad_actionAdapter(Pan_EDOnoMA adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnEnfermedad_actionPerformed(e);
  }
}

class Pan_EDOnoMA_btnArea_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_EDOnoMA adaptee;
  ActionEvent e;

  Pan_EDOnoMA_btnArea_actionAdapter(Pan_EDOnoMA adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnArea_actionPerformed(e);
  }
}

class Pan_EDOnoMA_txtEnfermedad_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_EDOnoMA adaptee;

  Pan_EDOnoMA_txtEnfermedad_keyAdapter(Pan_EDOnoMA adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtEnfermedad_keyPressed(e);
  }
}

class Pan_EDOnoMA_txtArea_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_EDOnoMA adaptee;

  Pan_EDOnoMA_txtArea_keyAdapter(Pan_EDOnoMA adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtArea_keyPressed(e);
  }
}

class Pan_EDOnoMA_txtEnfermedad_focusAdapter
    extends java.awt.event.FocusAdapter {
  Pan_EDOnoMA adaptee;

  Pan_EDOnoMA_txtEnfermedad_focusAdapter(Pan_EDOnoMA adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.txtEnfermedad_focusLost(e);
  }
}

class Pan_EDOnoMA_txtArea_focusAdapter
    extends java.awt.event.FocusAdapter {
  Pan_EDOnoMA adaptee;

  Pan_EDOnoMA_txtArea_focusAdapter(Pan_EDOnoMA adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    adaptee.txtArea_focusLost(e);
  }
}

class Pan_EDOnoMA_btnLimpiar_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_EDOnoMA adaptee;
  ActionEvent e;

  Pan_EDOnoMA_btnLimpiar_actionAdapter(Pan_EDOnoMA adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnLimpiar_actionPerformed(e);
  }
}

class Pan_EDOnoMA_btnInforme_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_EDOnoMA adaptee;
  ActionEvent e;

  Pan_EDOnoMA_btnInforme_actionAdapter(Pan_EDOnoMA adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnInforme_actionPerformed(e);
  }
}
