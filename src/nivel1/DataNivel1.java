package nivel1;

import java.io.Serializable;

public class DataNivel1
    implements Serializable {
  protected String sCod = "";
  protected String sDes = "";
  protected String sDesL = "";
  protected String sEmail1 = "";
  protected String sEmail2 = "";
  protected String sEmail3 = "";

  public DataNivel1() {
  }

  public DataNivel1(String cod) {
    sCod = cod;
  }

  public DataNivel1(String cod, String des, String desL, String email1,
                    String email2, String email3) {
    sCod = cod;
    sDes = des;
    if (sDesL != null) {
      sDesL = desL;

      // buzones de email
    }
    if (email1 != null) {
      sEmail1 = email1;
    }
    if (email2 != null) {
      sEmail2 = email2;
    }
    if (email3 != null) {
      sEmail3 = email3;
    }
  }

  public String getCod() {
    return sCod;
  }

  public String getDes() {
    return sDes;
  }

  public String getDesL() {
    String s;

    s = sDesL;
    if (s == null) {
      s = "";
    }
    return s;
  }

  public String getEMail1() {
    String s;

    s = sEmail1;
    if (s == null) {
      s = "";
    }
    return s;
  }

  public String getEMail2() {
    String s;

    s = sEmail2;
    if (s == null) {
      s = "";
    }
    return s;
  }

  public String getEMail3() {
    String s;

    s = sEmail3;
    if (s == null) {
      s = "";
    }
    return s;
  }
}
