//Title:        Mantenimiento de nivel 1
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Norsistemas S.A.
//Company:      Norsistemas, S.A.
//Description:

package nivel1;

import capp.CApp;
import capp.CListaMantenimiento;
import sapp.StubSrvBD;

public class CListaMantNivel1
    extends CListaMantenimiento {

  protected int iCatalogo;
  protected String sTitulo;

  public CListaMantNivel1(Nivel1 a) {
    super( (CApp) a,
          2,
          "100\n462",
          a.sEtCod + "\n" + a.sEtDes,
          new StubSrvBD(),
          a.strSERVLET,
          a.servletSELECCION_X_CODIGO,
          a.servletSELECCION_X_DESCRIPCION);
  }

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {
    DataNivel1 cat;
    modoOperacion = modoESPERA;
    Inicializar();
    DialNivel1 panel = new DialNivel1(this.app, DialNivel1.modoALTA);
    panel.show();

    if ( (panel.bAceptar) && (panel.valido)) {
      cat = new DataNivel1(panel.txtCod.getText(),
                           panel.txtDes.getText(),
                           panel.txtDesL.getText(),
                           panel.txtBuzon1.getText(),
                           panel.txtBuzon2.getText(),
                           panel.txtBuzon3.getText());
      lista.addElement(cat);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // modifica l�nea de la lista
  public void btnModificar_actionPerformed(int i) {
    DataNivel1 cat;

    modoOperacion = modoESPERA;
    Inicializar();
    DialNivel1 panel = new DialNivel1(this.app, DialNivel1.modoMODIFICAR);

    // rellena los datos
    cat = (DataNivel1) lista.elementAt(i);
    panel.txtCod.setText(cat.getCod());
    panel.txtDes.setText(cat.getDes());
    panel.txtDesL.setText(cat.getDesL());
    panel.txtBuzon1.setText(cat.getEMail1());
    panel.txtBuzon2.setText(cat.getEMail2());
    panel.txtBuzon3.setText(cat.getEMail3());

    panel.show();

    if ( (panel.bAceptar) && (panel.valido)) {
      cat = new DataNivel1(panel.txtCod.getText(),
                           panel.txtDes.getText(),
                           panel.txtDesL.getText(),
                           panel.txtBuzon1.getText(),
                           panel.txtBuzon2.getText(),
                           panel.txtBuzon3.getText());

      lista.removeElementAt(i);
      lista.insertElementAt(cat, i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed(int i) {
    DataNivel1 cat;

    modoOperacion = modoESPERA;
    Inicializar();
    DialNivel1 panel = new DialNivel1(this.app, DialNivel1.modoBAJA);

    // rellena los datos
    cat = (DataNivel1) lista.elementAt(i);
    panel.txtCod.setText(cat.getCod());
    panel.txtDes.setText(cat.getDes());
    panel.txtDesL.setText(cat.getDesL());
    panel.txtBuzon1.setText(cat.getEMail1());
    panel.txtBuzon2.setText(cat.getEMail2());
    panel.txtBuzon3.setText(cat.getEMail3());

    panel.show();

    if ( (panel.bAceptar)) {
      lista.removeElementAt(i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // rellena los par�metros para enviar al servlet
  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(Object o) {
    DataNivel1 cat = (DataNivel1) o;

    return cat.getCod() + "&" + cat.getDes();
  }

}