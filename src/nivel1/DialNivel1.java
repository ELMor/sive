//Title:        Mantenimiento de nivel 1
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Norsistemas S.A.
//Company:      Norsistemas, S.A.
//Description:

package nivel1;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CDialog;
import capp.CLista;
import capp.CMessage;
import sapp.StubSrvBD;

public class DialNivel1
    extends CDialog {

  boolean valido = false;

  //Recursos Strings
  ResourceBundle res;
  //Modos de operaci�n de la ventana
  static final int modoALTA = 0;
  static final int modoMODIFICAR = 1;
  static final int modoESPERA = 2;
  static final int modoBAJA = 3;

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  //Constantes del panel
  final String imgLUPA = "images/Magnify.gif";
  final String strSERVLET = "servlet/SrvNivel1";
  final String imgACEPTAR = "images/aceptar.gif";
  final String imgCANCELAR = "images/cancelar.gif";

  // par�metros
  protected int modoOperacion = modoALTA;
  protected StubSrvBD stubCliente = null;
  public boolean bAceptar = false;

  // controles
  XYLayout xyLyt = new XYLayout();
  Label lbl1 = new Label();
  CCampoCodigo txtCod = new CCampoCodigo();
  Label lbl2 = new Label();
  TextField txtDes = new TextField();
  Label lbl3 = new Label();
  TextField txtDesL = new TextField();
  Label lbl4 = new Label();
  TextField txtBuzon1 = new TextField();
  TextField txtBuzon2 = new TextField();
  TextField txtBuzon3 = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  niv1ActionListener btnActionListener = new niv1ActionListener(this);

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:

        // modo alta
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtCod.setEnabled(true);
        txtDes.setEnabled(true);
        txtDesL.setEnabled(true);
        txtBuzon1.setEnabled(true);
        txtBuzon2.setEnabled(true);
        txtBuzon3.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        break;

      case modoMODIFICAR:

        // modo modificaci�n y baja
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtCod.setEnabled(false);
        txtDes.setEnabled(true);
        txtDesL.setEnabled(true);
        txtBuzon1.setEnabled(true);
        txtBuzon2.setEnabled(true);
        txtBuzon3.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        txtCod.setEnabled(false);
        txtDes.setEnabled(false);
        txtDesL.setEnabled(false);
        txtBuzon1.setEnabled(false);
        txtBuzon2.setEnabled(false);
        txtBuzon3.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoBAJA:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        txtCod.setEnabled(false);
        txtDes.setEnabled(false);
        txtDesL.setEnabled(false);
        txtBuzon1.setEnabled(false);
        txtBuzon2.setEnabled(false);
        txtBuzon3.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
  }

  // contructor
  public DialNivel1(CApp a, int modo) {

    super(a);

    try {
      res = ResourceBundle.getBundle("nivel1.Res" + app.getIdioma());
      this.setTitle(res.getString("this.Title") + app.getNivel1());
      modoOperacion = modo;
      stubCliente = new StubSrvBD(new URL(a.getURL() + strSERVLET));
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // aspecto de la ventana
  private void jbInit() throws Exception {
    xyLyt.setHeight(281);
    xyLyt.setWidth(468);
    setSize(491, 313);
    lbl1.setText(res.getString("lbl1.Text"));
    txtCod.setBackground(new Color(255, 255, 150));
    lbl2.setText(res.getString("lbl2.Text"));
    txtDes.setBackground(new Color(255, 255, 150));
    lbl3.setText(res.getString("lbl3.Text"));
    lbl4.setText(res.getString("lbl4.Text"));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnAceptar.setImageURL(new URL(app.getCodeBase(), imgACEPTAR));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));
    btnCancelar.setImageURL(new URL(app.getCodeBase(), imgCANCELAR));
    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");
    this.setLayout(xyLyt);
    this.add(lbl1, new XYConstraints(4, 5, -1, -1));
    this.add(txtCod, new XYConstraints(98, 6, 77, -1));
    this.add(lbl2, new XYConstraints(4, 44, -1, -1));
    this.add(txtDes, new XYConstraints(174, 46, 280, -1));
    this.add(lbl3, new XYConstraints(4, 84, -1, -1));
    this.add(txtDesL, new XYConstraints(174, 86, 280, -1));
    this.add(lbl4, new XYConstraints(4, 124, -1, -1));
    this.add(txtBuzon1, new XYConstraints(174, 126, 280, -1));
    this.add(txtBuzon2, new XYConstraints(174, 166, 280, -1));
    this.add(txtBuzon3, new XYConstraints(174, 206, 280, -1));
    this.add(btnAceptar, new XYConstraints(290, 242, 80, 26));
    this.add(btnCancelar, new XYConstraints(374, 242, 80, 26));

    // establece los escuchadores
    btnAceptar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);
    txtCod.addActionListener(btnActionListener);

    // idioma local no es visible si no tengo
    if (app.getIdiomaLocal().length() == 0) {
      txtDesL.setVisible(false);
      lbl3.setVisible(false);
    }
    else {
      lbl3.setText(res.getString("lbl3.Text") + app.getIdiomaLocal() + "):");
    }

    // establece el modo de operaci�n
    Inicializar();
  }

  // procesa opci�n a�adir
  public void A�adir() {
    CMessage msgBox = null;
    CLista data = null;

    if (this.isDataValid()) {

      // a�ade la enfermedad
      try {
        this.modoOperacion = modoESPERA;
        Inicializar();

        data = new CLista();
        data.addElement(new DataNivel1(txtCod.getText(), txtDes.getText(),
                                       txtDesL.getText(), txtBuzon1.getText(),
                                       txtBuzon2.getText(), txtBuzon3.getText()));
        this.stubCliente.doPost(modoALTA, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoALTA;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
  }

  // procesa opci�n modificar
  public void Modificar() {
    CMessage msgBox = null;
    CLista data = null;

    if (this.isDataValid()) {

      // modifica la enfermedad
      try {
        this.modoOperacion = modoESPERA;
        Inicializar();

        data = new CLista();
        data.addElement(new DataNivel1(txtCod.getText(), txtDes.getText(),
                                       txtDesL.getText(), txtBuzon1.getText(),
                                       txtBuzon2.getText(), txtBuzon3.getText()));
        this.stubCliente.doPost(modoMODIFICAR, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoMODIFICAR;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
  }

  // procesa opci�n borrar
  public void Borrar() {
    CMessage msgBox = null;
    CLista data = null;

    msgBox = new CMessage(app, CMessage.msgPREGUNTA, res.getString("msg1.Text"));
    msgBox.show();

    if (msgBox.getResponse()) {

      msgBox = null;
      // borra el nivel 1
      try {

        this.modoOperacion = modoESPERA;
        Inicializar();

        data = new CLista();
        data.addElement(new DataNivel1(txtCod.getText()));
        this.stubCliente.doPost(servletBAJA, data);

        // pasa a modo ALTA
        this.modoOperacion = modoALTA;
        txtCod.setText("");
        txtDes.setText("");
        txtDesL.setText("");
        txtBuzon1.setText("");
        txtBuzon2.setText("");
        txtBuzon3.setText("");

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoBAJA;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

    }
    else {
      msgBox = null;
    }
  }

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;

    boolean b = false;

    // comprueba que esten informados los campos obligatorios
    if ( (txtCod.getText().length() > 0) && (txtDes.getText().length() > 0)) {
      b = true;
      if (txtBuzon1.getText().length() > 50) {
        b = false;
        msg = res.getString("msg2.Text");
        txtBuzon1.selectAll();
      }
      if (txtBuzon2.getText().length() > 50) {
        b = false;
        msg = res.getString("msg2.Text");
        txtBuzon2.selectAll();
      }
      if (txtBuzon3.getText().length() > 50) {
        b = false;
        msg = res.getString("msg2.Text");
        txtBuzon3.selectAll();
      }
      if (txtCod.getText().length() > 2) {
        b = false;
        msg = res.getString("msg2.Text");
        txtCod.selectAll();
      }
      if (txtDes.getText().length() > 30) {
        b = false;
        msg = res.getString("msg2.Text");
        txtDes.selectAll();
      }
      if (txtDesL.getText().length() > 30) {
        b = false;
        msg = res.getString("msg2.Text");
        txtDesL.selectAll();
      }
      // advertencia de requerir datos
    }
    else {
      msg = res.getString("msg3.Text");
    }
    if (!b) {
      msgBox = new CMessage(app, CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }
    valido = b;
    return b;
  }
}

// action listener
class niv1ActionListener
    implements ActionListener, Runnable {
  DialNivel1 adaptee = null;
  ActionEvent e = null;

  public niv1ActionListener(DialNivel1 adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    if ( (e.getActionCommand() == "aceptar")) { // aceptar
      adaptee.bAceptar = true;

      // realiza la operaci�n
      int kk = adaptee.modoOperacion;
      if (kk == adaptee.modoALTA) {
        adaptee.A�adir();
      }
      else if (kk == adaptee.modoMODIFICAR) {
        adaptee.Modificar();
      }
      else if (kk == adaptee.modoBAJA) {
        adaptee.Borrar();
      }
    }
    else if (e.getActionCommand() == "cancelar") { // cancelar
      adaptee.bAceptar = false;
      adaptee.dispose();
    }
  }
}
