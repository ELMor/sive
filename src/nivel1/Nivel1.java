
package nivel1;

import java.util.ResourceBundle;

import capp.CApp;

public class Nivel1
    extends CApp {

  // modos de operación del servlet
  final int servletOBTENER_X_CODIGO = 3;

  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // constantes del panel
  final String strSERVLET = "servlet/SrvNivel1";

  ResourceBundle res;

  //Strings para etiquetas del panel
  String sEtCod;
  String sEtDes;

  public void init() {

    super.init();
  }

  public void start() {

    res = ResourceBundle.getBundle("nivel1.Res" + this.getIdioma());
    sEtCod = res.getString("msg4.Text");
    sEtDes = res.getString("msg5.Text");
    setTitulo(res.getString("msg6.Text") + this.getNivel1());
    VerPanel("", new CListaMantNivel1(this));
  }
}
