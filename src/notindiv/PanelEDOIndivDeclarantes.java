package notindiv;

import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import capp.CTabla;
import jclass.bwt.BWTEnum;
import jclass.bwt.JCActionEvent;
import jclass.util.JCVector;
import notdata.DataTab;
import notutil.UtilEDO;

public class PanelEDOIndivDeclarantes
    extends CPanel {

  //modos de botones
  public int iPulsado = 0; //1: alta    2: modif    3:baja
  ResourceBundle res;

  // modos de operaci�n del panel
  final int modoESPERA = 2;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;
  final int modoSOLODECLARANTES = 7;

  // constantes del panel
  final String imgNAME[] = {
      "images/alta2.gif",
      "images/baja2.gif",
      "images/modificacion2.gif",
      "images/declaracion2.gif"};

  //par�metros//
  String itPrimero = "";
  DataTab datatab2 = null;

  protected int modoOperacion = modoALTA;
  // Hashtable de notifEDO
  Hashtable hashNotifEDO = null;
  // Vector con las l�neas con que se rellena la lista
  JCVector jcvItems = new JCVector();
  // Vector auxiliar que contiene las columnas de una l�nea
  JCVector row1 = null;
  // Objecto CLista que contiene los datos de cada l�nea de la tabla
  CLista clistaTbl = new CLista();
  // dialog que contiene a este panel
  DialogListaEDOIndiv dlgIndiv;

  // listeners
  PanelEDOIndivDeclarantesActionAdapter actionAdapter = new
      PanelEDOIndivDeclarantesActionAdapter(this);
  PanelEDOIndivDeclarantesFocusAdapter focusAdapter = new
      PanelEDOIndivDeclarantesFocusAdapter(this);
  PanelEDOIndivDeclarantesTblActionAdapter tblActionAdapter = new
      PanelEDOIndivDeclarantesTblActionAdapter(this);

  // controles
  Label lEquipoNotif = new Label();
  XYLayout xYLayout1 = new XYLayout();
  Label lCentroNotif = new Label();
  TextField txtDeclarante = new TextField();
  Label lDeclarante = new Label();
  CTabla tbl = new CTabla();
  ButtonControl btnAnadir = new ButtonControl();
  ButtonControl btnModif = new ButtonControl();
  ButtonControl btnEliminar = new ButtonControl();
  GroupBox pnlDeclarantes = new GroupBox();

  protected CCargadorImagen imgs = null;
  Label lblEquipoNotif = new Label();
  Label lblCentroNotif = new Label();
  GroupBox pnlEquipo = new GroupBox();

  public PanelEDOIndivDeclarantes(DialogListaEDOIndiv dlg, int modo,
                                  Hashtable hashNotifEDO) {
    try {
      this.setApp(dlg.getCApp());
      res = ResourceBundle.getBundle("notindiv.Res" + dlg.getCApp().getIdioma());
      dlgIndiv = dlg;
      this.hashNotifEDO = hashNotifEDO; //CARGA EN LOCAL; EL DEL DLG
      jbInit();

      modoOperacion = modo;
      Inicializar();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    this.setSize(new Dimension(745, 350));
    lEquipoNotif.setText(res.getString("lEquipoNotif.Text"));
    xYLayout1.setHeight(350);
    xYLayout1.setWidth(755);
    this.setLayout(xYLayout1);
    lCentroNotif.setText(res.getString("lCentroNotif.Text"));
    lDeclarante.setText(res.getString("lDeclarante.Text"));

    //tabla
    tbl.setColumnWidths(jclass.util.JCUtilConverter.toIntList(new String(
        "55\n200\n55\n30\n30\n200\n80\n40"), '\n'));
    tbl.setColumnButtonsStrings(jclass.util.JCUtilConverter.toStringList(new
        String(res.getString("tbl.ColumnButtonsStrings")), '\n'));
    tbl.setNumColumns(8);
    tbl.setScrollbarDisplay(jclass.bwt.BWTEnum.DISPLAY_VERTICAL_ONLY);

    pnlDeclarantes.setLabel(res.getString("pnlDeclarantes.Label"));
    lblEquipoNotif.setFont(new Font("Dialog", 1, 12));
    lblCentroNotif.setFont(new Font("Dialog", 1, 12));
    pnlEquipo.setLabel(res.getString("pnlEquipo.Label"));
    pnlEquipo.setFont(new Font("Dialog", 3, 12));
    pnlEquipo.setForeground(new Color(153, 0, 0));
    pnlDeclarantes.setFont(new Font("Dialog", 3, 12));
    pnlDeclarantes.setForeground(new Color(153, 0, 0));
    txtDeclarante.setBackground(new Color(255, 255, 150));
    this.add(lEquipoNotif, new XYConstraints(54, 42, 112, -1));
    this.add(lCentroNotif, new XYConstraints(54, 75, 111, -1));
    this.add(txtDeclarante, new XYConstraints(143, 144, 331, -1));
    this.add(lDeclarante, new XYConstraints(30, 145, 90, -1));
    this.add(tbl, new XYConstraints(19, 177, 714, 107));

    this.add(btnAnadir, new XYConstraints(19, 287, -1, -1));
    this.add(btnModif, new XYConstraints(49, 287, -1, -1));
    this.add(btnEliminar, new XYConstraints(79, 287, -1, -1));

    this.add(pnlDeclarantes, new XYConstraints(3, 118, 743, 222));
    this.add(lblEquipoNotif, new XYConstraints(167, 42, 217, -1));
    this.add(lblCentroNotif, new XYConstraints(167, 75, 217, -1));
    this.add(pnlEquipo, new XYConstraints(4, 27, 418, 93));

    btnAnadir.setImage(imgs.getImage(0));
    btnEliminar.setImage(imgs.getImage(1));
    btnModif.setImage(imgs.getImage(2));

    btnAnadir.setEnabled(false);
    btnEliminar.setEnabled(false);
    btnModif.setEnabled(false);

    setBorde(false);

    btnAnadir.setActionCommand("anadir");
    btnEliminar.setActionCommand("eliminar");
    btnModif.setActionCommand("modificar");
    btnAnadir.addActionListener(actionAdapter);
    btnEliminar.addActionListener(actionAdapter);
    btnModif.addActionListener(actionAdapter);

    txtDeclarante.setName("declarante");
    txtDeclarante.addFocusListener(focusAdapter);

    tbl.addActionListener(tblActionAdapter);

    RellenaInicio();
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {

      case modoESPERA:

        // se deshabilitan todos los controles
        tbl.setEnabled(false);
        btnAnadir.setEnabled(false);
        btnModif.setEnabled(false);
        btnEliminar.setEnabled(false); //no se deshabilitan
        txtDeclarante.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

        //LO QUE HARE CON LA EDO INDIV**************
      case modoALTA:
      case modoMODIFICACION:
      case modoSOLODECLARANTES:
        tbl.setEnabled(true);
        btnModif.setEnabled(true);
        btnAnadir.setEnabled(true);
        btnEliminar.setEnabled(true);
        txtDeclarante.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
      case modoCONSULTA:
        tbl.setEnabled(true); //CONSULTANDO, pero dejar que vea scroll
        btnModif.setEnabled(false);
        btnAnadir.setEnabled(false);
        btnEliminar.setEnabled(false);
        txtDeclarante.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
    String perfil = (String) hashNotifEDO.get("PERFIL");
    if (perfil.equals("5")) {
      //tiene deshabilitado el boton de menos
      btnEliminar.setEnabled(false);
    }

  }

  public void RellenaInicio() {
    lblCentroNotif.setText( (String) hashNotifEDO.get("DS_C_NOTIF"));
    lblEquipoNotif.setText( (String) hashNotifEDO.get("DS_E_NOTIF"));

  }

  // m�todo que retorna el declarante de la l�nea i de la tabla
  // de declarantes
  public String getDeclarante(int i) {
    if (i < tbl.countItems()) {
      DataTab datatab = (DataTab) clistaTbl.elementAt(i);
      return (datatab.getDS_DECLARANTE());
    }
    return "";
  }

  public String getNivel1EquipoLinea(int i) {
    if (i < tbl.countItems()) {
      DataTab datatab = (DataTab) clistaTbl.elementAt(i);
      return (datatab.getCD_NIVEL_1_E_NOTIF());
    }
    return "";
  }

  public String getNivel2EquipoLinea(int i) {
    if (i < tbl.countItems()) {
      DataTab datatab = (DataTab) clistaTbl.elementAt(i);
      return (datatab.getCD_NIVEL_2_E_NOTIF());
    }
    return "";
  }

  // m�todo que retorna el flag It_Primero de la l�nea i de la
  // tabla de declarantes
  public String getItPrimero(int i) {
    if (i < tbl.countItems()) {
      DataTab datatab = (DataTab) clistaTbl.elementAt(i);
      return (datatab.getIT_PRIMERO());
    }
    return "";
  }

  // m�todo que retorna el equipo notificador de la l�nea i de la
  // tabla de declarantes
  public String getCodEquipo(int i) {
    if (i < tbl.countItems()) {
      DataTab datatab = (DataTab) clistaTbl.elementAt(i);
      return (datatab.getCD_E_NOTIF());
    }
    return "";
  }

  // m�todo que retorna el a�o de notificaci�n de la l�nea i de la
  // tabla de declarantes
  public String getAnno(int i) {
    if (i < tbl.countItems()) {
      DataTab datatab = (DataTab) clistaTbl.elementAt(i);
      return (datatab.getCD_ANOEPI_NOTIF_EDOI());
    }
    return "";
  }

  // m�todo que retorna la semana de notificaci�n de la l�nea i de la
  // tabla de declarantes
  public String getSemana(int i) {
    if (i < tbl.countItems()) {
      DataTab datatab = (DataTab) clistaTbl.elementAt(i);
      return (datatab.getCD_SEMEPI_NOTIF_EDOI());
    }
    return "";
  }

  // m�todo que retorna la fecha de recepci�n de la l�nea i de la
  // tabla de declarantes
  public String getFechaRecep(int i) {
    if (i < tbl.countItems()) {
      DataTab datatab = (DataTab) clistaTbl.elementAt(i);
      return (datatab.getFC_RECEP_NOTIF_EDOI());
    }
    return "";
  }

  // m�todo que retorna la fecha de notificaci�n de la l�nea i de la
  // tabla de declarantes
  public String getFechaNotif(int i) {
    if (i < tbl.countItems()) {
      DataTab datatab = (DataTab) clistaTbl.elementAt(i);
      return (datatab.getFC_FECNOTIF_NOTIF_EDOI());
    }
    return "";
  }

  // m�todo que retorna el operador de la l�nea i de la tabla de
  // declarantes
  public String getCdOpe(int i) {
    if (i < tbl.countItems()) {
      DataTab datatab = (DataTab) clistaTbl.elementAt(i);
      return (datatab.getCD_OPE_NOTIF_EDOI());
    }
    return "";
  }

  // m�todo que retorna la fecha de �ltima actualizaci�n de la
  // l�nea i de la tabla de declarantes
  public String getFcUltAct(int i) {
    if (i < tbl.countItems()) {
      DataTab datatab = (DataTab) clistaTbl.elementAt(i);
      return (datatab.getFC_ULTACT_NOTIF_EDOI());
    }
    return "";
  }

//CUANDO SE RELLENA DE DATOS LA TABLA (MODIFICACION)
  public void RellenaDeclarantes(CLista clista) {

    Object componente = null;

    String sDeclarante = "", sFechaRecep = "", sItPrimero = "";
    String sAnno = "", sSemana = "", sCodOpe = "", sFechaUltAct = "";
    String sFechaNotif = "", sEquipo = "";
    String sCodCentro = "", sDesCentro = "";
    String sN1Eq = "", sN2Eq = "";

    JCVector row1;

    // se inicializan los campos de texto
    txtDeclarante.setText("");

    // como puede existir m�s de un declarante para la notiicaci�n
    // se rellena la tabla de declarantes CON LOS DATOS QUE SE
    // RECUPERAN DE LA SELECT
    for (int i = 0; i < clista.size(); i++) {

      componente = clista.elementAt(i);
      sDeclarante = ( (DataTab) componente).getDS_DECLARANTE();
      sFechaRecep = ( (DataTab) componente).getFC_RECEP_NOTIF_EDOI();
      sItPrimero = ( (DataTab) componente).getIT_PRIMERO();
      sAnno = ( (DataTab) componente).getCD_ANOEPI_NOTIF_EDOI();
      sSemana = ( (DataTab) componente).getCD_SEMEPI_NOTIF_EDOI();
      sCodOpe = ( (DataTab) componente).getCD_OPE_NOTIF_EDOI();
      sFechaUltAct = ( (DataTab) componente).getFC_ULTACT_NOTIF_EDOI();
      sFechaNotif = ( (DataTab) componente).getFC_FECNOTIF_NOTIF_EDOI();
      sEquipo = ( (DataTab) componente).getCD_E_NOTIF();

      sCodCentro = ( (DataTab) componente).getCD_CENTRO_E_NOTIF();
      sDesCentro = ( (DataTab) componente).getDS_CENTRO_E_NOTIF();
      sN1Eq = ( (DataTab) componente).getCD_NIVEL_1_E_NOTIF();
      sN2Eq = ( (DataTab) componente).getCD_NIVEL_2_E_NOTIF();

      //Cuidado: Ope, ultact son los antiguos
      //No se guardan zonas del equipo!!! de momento
      addRow(sDeclarante, sFechaRecep, sItPrimero, sAnno, sSemana,
             sCodOpe, sFechaUltAct, sFechaNotif, sEquipo, sCodCentro,
             sDesCentro, sN1Eq, sN2Eq);

    }

    tbl.setItems(jcvItems);

    /*//BOTON DE BORRAR
        if (tbl.countItems() > 0)
     btnEliminar.setEnabled(true);
        else
     btnEliminar.setEnabled(true); //PODEMOS BORRAR LA ULTIMA!!*/
  } //fin de RellenaDeclarantes **** **** *** * * * * *

//mlm
  /*public boolean autorizaciones_alta(){
      Vector vN1 = (Vector)hashNotifEDO.get("CD_NIVEL_1_AUTORIZACIONES");
      Vector vN2 = (Vector)hashNotifEDO.get("CD_NIVEL_2_AUTORIZACIONES");
      String N1 = "";
      String N2 = "";
       //CASO *******  ******* ******* *******
       N1 = dlgIndiv.pCabecera.txtCodNivel1.getText().trim();
       N2 = dlgIndiv.pCabecera.txtCodNivel2.getText().trim();
       //n1
       if (!N1.equals("")){
         for (int i=0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if ( n1.trim().equals(N1)){
       ShowWarning("El nivel 1 del Caso, no le permite darse como declarante.");
            txtDeclarante.setText("");
            return false;
          }
         }
       }
       //n2
       if (!N2.equals("")){
         for (int i=0; i < vN2.size(); i++) {
          String n2 = (String) vN2.elementAt(i);
          if ( n2.trim().equals(N2)){
       ShowWarning("El nivel 2 del Caso, no le permite darse como declarante.");
            txtDeclarante.setText("");
            return false;
          }
         }
       }
       //FIN CASO *******  ******* ******* *******
       //DECLARANTE *******  ******* ******* *******
       N1 = "";
       N2 = "";
       if (tbl.countItems() == 0)
          return true;
       //busco el primero
       for (int i=0; i < clistaTbl.size(); i++) {
          DataTab data = (DataTab) clistaTbl.elementAt(i);
          if (data.getIT_PRIMERO().equals("S")){
            N1 =  data.getCD_NIVEL_1_E_NOTIF();
            N2 =  data.getCD_NIVEL_2_E_NOTIF();
          }
       }
        //n1
       if (!N1.equals("")){
         for (int i=0; i < vN1.size(); i++) {
          String n1 = (String) vN1.elementAt(i);
          if ( n1.trim().equals(N1)){
            ShowWarning("El nivel 1 del primer declarante, no le permite darse como declarante.");
            txtDeclarante.setText("");
            return false;
          }
         }
       }
       //n2
       if (!N2.equals("")){
         for (int i=0; i < vN2.size(); i++) {
          String n2 = (String) vN2.elementAt(i);
          if ( n2.trim().equals(N2)){
            ShowWarning("El nivel 2 del primer declarante, no le permite darse como declarante.");
            txtDeclarante.setText("");
            return false;
          }
         }
       }
      //*******  ******* ******* *******
        return true;
    } //autorizaciones_ALTA   */

// construye una l�nea de datos y la a�ade al vector jcvItems
   private void addRow(String declarante, String fecharecep,
                       String itprimero, String anno,
                       String semana, String codope,
                       String fechaultact, String fechanotif,
                       String equipo, String sCodCentro, String sDesCentro,
                       String sN1Eq, String sN2Eq) {

     /*row1 = new JCVector();
          row1.addElement(declarante);
          row1.addElement(fecharecep);
          //imagen
          if (itprimero.trim().equals("S"))
       row1.addElement(imgs.getImage(2)); */

     //mlm
     row1 = new JCVector();
     row1.addElement(sCodCentro);
     row1.addElement(sDesCentro);
     row1.addElement(equipo);
     row1.addElement(sN1Eq);
     if (sN2Eq.trim().equals("")) {
       row1.addElement("  ");
     }
     else {
       row1.addElement(sN2Eq);
     }
     row1.addElement(declarante);
     row1.addElement(fecharecep);
     if (itprimero.trim().equals("S")) {
       row1.addElement(imgs.getImage(3));

       //datos
     }
     DataTab datatab = new DataTab();
     datatab.insert("CD_E_NOTIF", equipo);
     datatab.insert("CD_ANOEPI_NOTIF_EDOI", anno);
     datatab.insert("CD_SEMEPI_NOTIF_EDOI", semana);
     datatab.insert("FC_RECEP_NOTIF_EDOI", fecharecep);
     datatab.insert("DS_DECLARANTE", declarante);
     datatab.insert("IT_PRIMERO", itprimero);
     datatab.insert("CD_OPE_NOTIF_EDOI", codope);
     datatab.insert("FC_ULTACT_NOTIF_EDOI", fechaultact);
     datatab.insert("FC_FECNOTIF_NOTIF_EDOI", fechanotif);
     datatab.insert("CD_NIVEL_1_E_NOTIF", sN1Eq);
     datatab.insert("CD_NIVEL_2_E_NOTIF", sN2Eq);
     datatab.insert("CD_CENTRO_E_NOTIF", sCodCentro);
     datatab.insert("DS_CENTRO_E_NOTIF", sDesCentro);

     jcvItems.addElement(row1);
     clistaTbl.addElement(datatab);
   }

// comprobaci�n de los datos antes de pasarlos a la lista
  private boolean isDataValid() {
    CMessage msgBox = null;

    if (tbl.countItems() > 0) {
      return true;
    }
    else {
      return false;
    }
  } //fin isdatavalid ******** ******* ******* ******

  void txtDeclarante_focusLost() {

    if (iPulsado == 2 && txtDeclarante.getText().length() != 0) {

      // control de longitud
      if (txtDeclarante.getText().length() > 40) {
        ShowWarning(res.getString("msg76.Text"));
        txtDeclarante.selectAll();
        return;
      }

      //cambiar el declarante de la i  ****
      Cambiar_Declarante();

      //limpio
      txtDeclarante.setText("");
    }

    /* // si los datos est�n rellenos, habilitar el bot�n de a�adir
     //SIEMPRE SE PUEDE A�ADIR
     if (isDataValid()){
       //btnAnadir.setEnabled(true);
       validate();
     }
     else
       //btnAnadir.setEnabled(false); */
  }

//Borrar Tabla
  protected void BorrarTabla() {
    if (tbl.countItems() > 0) {
      tbl.deleteItems(0, tbl.countItems() - 1);
    }
  }

  public void Cambiar_Declarante() {
    int iSel = tbl.getSelectedIndex();
    int iTam = clistaTbl.size();
    //clistaTbl ------
    for (int indice = 0; indice < iTam; indice++) {
      DataTab datCapturarDatosRegistro = (DataTab) clistaTbl.elementAt(indice);
      if (indice == iSel) {
        datCapturarDatosRegistro.insert("DS_DECLARANTE",
                                        txtDeclarante.getText().trim());
        clistaTbl.removeElementAt(indice);
        clistaTbl.insertElementAt(datCapturarDatosRegistro, indice);
      }
    }
    //clistaTbl ------
    BorrarTabla();
    jcvItems = new JCVector();
    Object componente = null;

    String sDeclarante = "", sFechaRecep = "", sItPrimero = "";
    String sAnno = "", sSemana = "", sCodOpe = "", sFechaUltAct = "";
    String sFechaNotif = "", sEquipo = "";
    String sCodCentro = "", sDesCentro = "";
    String sN1Eq = "", sN2Eq = "";

    JCVector row1;

    // como puede existir m�s de un declarante para la notiicaci�n
    // se rellena la tabla de declarantes CON LOS DATOS QUE SE
    // RECUPERAN DE LA SELECT
    for (int i = 0; i < iTam; i++) {
      componente = clistaTbl.elementAt(i);
      sDeclarante = ( (DataTab) componente).getDS_DECLARANTE();
      sFechaRecep = ( (DataTab) componente).getFC_RECEP_NOTIF_EDOI();
      sItPrimero = ( (DataTab) componente).getIT_PRIMERO();
      sAnno = ( (DataTab) componente).getCD_ANOEPI_NOTIF_EDOI();
      sSemana = ( (DataTab) componente).getCD_SEMEPI_NOTIF_EDOI();
      sCodOpe = ( (DataTab) componente).getCD_OPE_NOTIF_EDOI();
      sFechaUltAct = ( (DataTab) componente).getFC_ULTACT_NOTIF_EDOI();
      sFechaNotif = ( (DataTab) componente).getFC_FECNOTIF_NOTIF_EDOI();
      sEquipo = ( (DataTab) componente).getCD_E_NOTIF();

      sCodCentro = ( (DataTab) componente).getCD_CENTRO_E_NOTIF();
      sDesCentro = ( (DataTab) componente).getDS_CENTRO_E_NOTIF();
      sN1Eq = ( (DataTab) componente).getCD_NIVEL_1_E_NOTIF();
      sN2Eq = ( (DataTab) componente).getCD_NIVEL_2_E_NOTIF();

      //Cuidado: Ope, ultact son los antiguos
      //No se guardan zonas del equipo!!! de momento
      row1 = new JCVector();
      row1.addElement(sCodCentro);
      row1.addElement(sDesCentro);
      row1.addElement(sEquipo);
      row1.addElement(sN1Eq);
      if (sN2Eq.trim().equals("")) {
        row1.addElement("  ");
      }
      else {
        row1.addElement(sN2Eq);
      }
      row1.addElement(sDeclarante);
      row1.addElement(sFechaRecep);
      if (sItPrimero.trim().equals("S")) {
        row1.addElement(imgs.getImage(3));

        //datos
      }
      DataTab datatab = new DataTab();
      datatab.insert("CD_E_NOTIF", sEquipo);
      datatab.insert("CD_ANOEPI_NOTIF_EDOI", sAnno);
      datatab.insert("CD_SEMEPI_NOTIF_EDOI", sSemana);
      datatab.insert("FC_RECEP_NOTIF_EDOI", sFechaRecep);
      datatab.insert("DS_DECLARANTE", sDeclarante);
      datatab.insert("IT_PRIMERO", sItPrimero);
      datatab.insert("CD_OPE_NOTIF_EDOI", sCodOpe);
      datatab.insert("FC_ULTACT_NOTIF_EDOI", sFechaUltAct);
      datatab.insert("FC_FECNOTIF_NOTIF_EDOI", sFechaNotif);
      datatab.insert("CD_NIVEL_1_E_NOTIF", sN1Eq);
      datatab.insert("CD_NIVEL_2_E_NOTIF", sN2Eq);
      datatab.insert("CD_CENTRO_E_NOTIF", sCodCentro);
      datatab.insert("DS_CENTRO_E_NOTIF", sDesCentro);
      jcvItems.addElement(row1);

    }

    tbl.setItems(jcvItems);

  }

  void btnAnadir_actionPerformed() {
    iPulsado = 1;
    String sIt = "";

    // si se da de alta la primera l�nea, con IT_PRIMERO = "S"
    if (tbl.countItems() == 0) {
      sIt = "S";
    }
    else {
      sIt = "N";

      // control de longitud
    }
    if (txtDeclarante.getText().length() > 40) {
      ShowWarning(res.getString("msg76.Text"));
      txtDeclarante.selectAll();
      return;
    }
    // control de vacio
    if (txtDeclarante.getText().length() == 0) {
      ShowWarning(res.getString("msg77.Text"));
      txtDeclarante.selectAll();
      return;
    }

    //autorizaciones:
    if (modoOperacion == modoALTA) {
      //puede darse como declarante siempre
    }
    if (modoOperacion == modoMODIFICACION ||
        modoOperacion == modoBAJA) {
      //puede dar alta siempre que sea <> clave
    }

    // control de duplicidad de declarantes
    DataTab declarante;
    for (int i = 0; i < clistaTbl.size(); i++) {
      declarante = (DataTab) clistaTbl.elementAt(i);
      if ( ( ( (String) declarante.get("CD_ANOEPI_NOTIF_EDOI"))
            .equals( (String) hashNotifEDO.get("CD_ANOEPI"))) &&
          ( ( (String) declarante.get("CD_SEMEPI_NOTIF_EDOI"))
           .equals( (String) hashNotifEDO.get("CD_SEMEPI"))) &&
          ( ( (String) declarante.get("FC_RECEP_NOTIF_EDOI"))
           .equals( (String) hashNotifEDO.get("FC_RECEP"))) &&
          ( ( (String) declarante.get("FC_FECNOTIF_NOTIF_EDOI"))
           .equals( (String) hashNotifEDO.get("FC_FECNOTIF"))) &&
          ( ( (String) declarante.get("CD_E_NOTIF"))
           .equals( (String) hashNotifEDO.get("CD_E_NOTIF")))) {
        ShowWarning(res.getString("msg78.Text"));
        // borramos el declarante JM
        txtDeclarante.setText("");
        return;
      }

    } //fin duplicidad

    addRow(txtDeclarante.getText().trim(),
           (String) hashNotifEDO.get("FC_RECEP"),
           sIt,
           (String) hashNotifEDO.get("CD_ANOEPI"),
           (String) hashNotifEDO.get("CD_SEMEPI"),
           this.getApp().getLogin(),
           UtilEDO.getFechaAct(),
           (String) hashNotifEDO.get("FC_FECNOTIF"),
           (String) hashNotifEDO.get("CD_E_NOTIF"),
           (String) hashNotifEDO.get("CD_C_NOTIF"),
           (String) hashNotifEDO.get("DS_C_NOTIF"),
           (String) hashNotifEDO.get("CD_NIVEL_1"),
           (String) hashNotifEDO.get("CD_NIVEL_2"));

    tbl.setItems(jcvItems);

    // se vuelven a inicializar campos de texto y bot�n A�adir a false
    txtDeclarante.setText("");

  }

  private void ShowWarning(String sMessage) {
    dlgIndiv.ShowWarning(sMessage);
  }

  public boolean Permiso_Sobre_Linea(int index, String perfil) {

    int iLinea = index;
    // modificaci�n
    String Perfil = perfil;

    Vector vN1 = (Vector) hashNotifEDO.get("CD_NIVEL_1_AUTORIZACIONES");
    Vector vN2 = (Vector) hashNotifEDO.get("CD_NIVEL_2_AUTORIZACIONES");
    String N1 = "";
    String N2 = "";

    //los datos de la linea  deben pertenecer a autorizaciones
    DataTab datatab = (DataTab) clistaTbl.elementAt(iLinea);
    N1 = getNivel1EquipoLinea(iLinea);
    N2 = getNivel2EquipoLinea(iLinea);

    //caso-area, distrito  parejas (deben ser de igual tama�o)
    for (int i = 0; i < vN1.size(); i++) {
      String n1 = (String) vN1.elementAt(i);

      // modificaci�n JLT 20/09/2000
      if (Perfil.equals("3")) { // se comprueba s�lo nivel1
        if (n1.trim().equals(N1)) {
          return (true); //perten
        }
      }
      else {
        String n2 = (String) vN2.elementAt(i);

        if (n1.trim().equals(N1)) {
          if (n2.trim().equals(N2)) {
            return (true); //pertenece
          }
        }
      } // if perfil 3
    }
    //------------------
    return (false);
  } //Permisos

  void btnEliminar_actionPerformed() {
    iPulsado = 3;
    DataTab datatab;
    CLista copia;
    int j;
    String sPerfil = (String) hashNotifEDO.get("PERFIL");
    String dtFecha1 = null;
    String dtFecha2 = null;
    int iSel = tbl.getSelectedIndex();
    if (iSel != BWTEnum.NOTFOUND) {

      if (sPerfil.equals("5")) {
        //tiene deshabilitado el boton de menos
      }
      else if (sPerfil.equals("2")) {
        //siempre puede borrar
        // modificacion JLT 20/09/2000
      }
      else if (sPerfil.equals("1")) { // no puede borrar
        ShowWarning(res.getString("msg79.Text"));
        return;
        /////////////////
      }
      else {
        boolean bPermiso = Permiso_Sobre_Linea(iSel, sPerfil);
        if (!bPermiso) {
          ShowWarning(res.getString("msg79.Text"));
          return;
        }
      }

      // se solicita la confirmaci�n de la baja
      CMessage msgBox = new CMessage(this.app, CMessage.msgPREGUNTA,
                                     res.getString("msg80.Text"));
      msgBox.show();

      if (msgBox.getResponse()) {
        // LSR
        // si se borra el primer declarante, se marca otro como
        j = -1;
        datatab = (DataTab) clistaTbl.elementAt(iSel);

        if (datatab.getIT_PRIMERO().equals("S")) {

          // selecciona el m�s antiguo
          for (int i = 0; i < clistaTbl.size(); i++) {
            if (i != iSel) {
              datatab = (DataTab) clistaTbl.elementAt(i);

              // la primera vez toma la fecha
              if (dtFecha1 == null) {

                dtFecha1 = datatab.getFC_RECEP_NOTIF_EDOI().substring(6, 10) +
                    datatab.getFC_RECEP_NOTIF_EDOI().substring(3, 5) +
                    datatab.getFC_RECEP_NOTIF_EDOI().substring(0, 2);
                j = i;
              }

              // graba el m�s antiguo
              dtFecha2 = datatab.getFC_RECEP_NOTIF_EDOI().substring(6, 10) +
                  datatab.getFC_RECEP_NOTIF_EDOI().substring(3, 5) +
                  datatab.getFC_RECEP_NOTIF_EDOI().substring(0, 2);
              if (dtFecha2.compareTo(dtFecha1) < 0) {
                dtFecha1 = dtFecha2;
                j = i;
              }
            }
          }

          // marca otro como el primer declarante
          if (j != -1) {
            datatab2 = (DataTab) clistaTbl.elementAt(j);
            datatab2.insert("IT_PRIMERO", "S");
          }
        }

        //marca el primer declarante
        if (j != -1) {
          clistaTbl.removeElementAt(iSel);
          copia = (CLista) clistaTbl.clone();
          clistaTbl = new CLista();
          jcvItems = new JCVector();
          RellenaDeclarantes(copia);

        }
        else {
          jcvItems.removeElementAt(iSel);
          clistaTbl.removeElementAt(iSel);
          tbl.setItems(jcvItems);
        }
      }
      msgBox = null;
    }
  }

// m�todo que se ejecuta en respuesta a los click en la tabla
  void tbl_actionPerformed() {

    if (modoOperacion == modoBAJA ||
        modoOperacion == modoCONSULTA) {
      return;
    }
    btnModif_actionPerformed();
  } //tbl_actionPerformed

  void btnModif_actionPerformed() {
    iPulsado = 2;
    int iSel = tbl.getSelectedIndex();
    String sPerfil = (String) hashNotifEDO.get("PERFIL");

    if (iSel != BWTEnum.NOTFOUND) {
      if (sPerfil.equals("5")) {
        //NOTA: puede modificar, pero si modifica la linea que corresponde
        //a su epidemiolo, se siente!!! (no puede saberse cual es la suya)
      }
      else if (sPerfil.equals("2")) {
        //siempre puede modificar
      }
      //////// modificacion JLT 20/09/2000
      else if (sPerfil.equals("1")) { // de perfil no puede modificar nada
        ShowWarning(res.getString("msg81.Text"));
        return;
        ////////////////////////////
      }
      else {
        // modificaci�n JLT 20/09/2000
        //boolean bPermiso= Permiso_Sobre_Linea (iSel);
        boolean bPermiso = Permiso_Sobre_Linea(iSel, sPerfil);
        if (!bPermiso) {
          ShowWarning(res.getString("msg81.Text"));
          return;
        }
      }
      txtDeclarante.setText(getDeclarante(iSel));
      txtDeclarante.requestFocus();
      txtDeclarante.select(0, txtDeclarante.getText().length());
    }
  } //btnModif_actionPerformed

} //fin de la clase

// escuchador para la p�rdida del foco
class PanelEDOIndivDeclarantesFocusAdapter
    extends java.awt.event.FocusAdapter {
  PanelEDOIndivDeclarantes adaptee;

  PanelEDOIndivDeclarantesFocusAdapter(PanelEDOIndivDeclarantes adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    if ( ( (TextField) e.getSource()).getName().equals("declarante")) {
      adaptee.txtDeclarante_focusLost();
    }
  }
}

// listener de los click en los click de los botones
class PanelEDOIndivDeclarantesActionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelEDOIndivDeclarantes adaptee;
  ActionEvent evt;

  PanelEDOIndivDeclarantesActionAdapter(PanelEDOIndivDeclarantes adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if (evt.getActionCommand().equals("anadir")) {
      adaptee.btnAnadir_actionPerformed();
    }
    else if (evt.getActionCommand().equals("eliminar")) {
      adaptee.btnEliminar_actionPerformed();
    }
    else if (evt.getActionCommand().equals("modificar")) {
      adaptee.btnModif_actionPerformed();
    }
  }
}

// escuchador de los click en la tabla
class PanelEDOIndivDeclarantesTblActionAdapter
    implements jclass.bwt.JCActionListener, Runnable {
  PanelEDOIndivDeclarantes adaptee;

  PanelEDOIndivDeclarantesTblActionAdapter(PanelEDOIndivDeclarantes adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(JCActionEvent e) {
    new Thread(this).start();
  }

  public void run() {
    adaptee.tbl_actionPerformed();
  }
}
