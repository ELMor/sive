package notindiv;

import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import fechas.CFecha;
import notdata.DataListaEDOIndiv;
import notdata.DataTab;
import notutil.Comunicador;
import notutil.UtilEDO;
import sapp.StubSrvBD;

public class PanelEDOIndivCaso
    extends CPanel {

  // modos de operaci�n del panel
  //final int modoNORMAL = 0;
  final int modoESPERA = 2;
  ResourceBundle res;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;
  final int modoSOLODECLARANTES = 7;

  // modos de operaci�n del servlet SrvEDOIndiv
  final int servletSELECCION_LISTA_CLASIFEDO = 23;

  // constantes del panel
  final String strSERVLET_EDOIND = "servlet/SrvEDOIndiv";

  // par�metros
  // modo de entrada al panel
  //protected int iModo = modoALTA;
  // modo de operaci�n del panel
  protected int modoOperacion = modoALTA;
  // stub
  protected StubSrvBD stubCliente = null;
  // Lista que contiene las enfermedades individualizadas
  protected CLista listaClasificacion = null;
  // listas auxiliares
  protected CLista parametros = null, result = null;
  // dialog que contiene a este panel
  DialogListaEDOIndiv dlgIndiv;
  // hashtable de NotifEDO
  Hashtable hashNotifEDO = null;

  // listeners
  PanelEDOIndivCasochkitemAdapter chkItemAdapter = new
      PanelEDOIndivCasochkitemAdapter(this);

  // controles
  XYLayout xYLayout = new XYLayout();
  TextField txtDgOtros = new TextField();
  Label lFechaIniSint = new Label();
  Label lAsociado = new Label();
  Label lColectivo = new Label();
  Label lDerivado = new Label();
  Label lCentro = new Label();
  Label lDiagClinico = new Label();

  // ARG: en vez de CFechaSimple usamos CFecha
  //TextField txtFechaIniSint = new TextField();
  //CFechaSimple CfechaIniSint = new CFechaSimple("S");
  CFecha CfechaIniSint = new CFecha("N");

  Label lDgOtros = new Label();
  Label lClasificacion = new Label();
  Label lDiagSerol = new Label();
  Label lDiagMicro = new Label();
  Choice choClasificacion = new Choice();
  Checkbox chkAsociadoCaso = new Checkbox();
  Checkbox chkDgClinico = new Checkbox();
  Checkbox chkDgSerologico = new Checkbox();
  Checkbox chkDgMicrobio = new Checkbox();
  Checkbox chkDerivado = new Checkbox();
  TextField txtCentro = new TextField();
  TextField txtColectivo = new TextField();
  TextField txtAsociado = new TextField();
  GroupBox pnl2 = new GroupBox();
  GroupBox pnl1 = new GroupBox();

  // constructor
  public PanelEDOIndivCaso(DialogListaEDOIndiv dlg, int modo,
                           Hashtable hashNotifEDO) {
    try {
      setApp(dlg.getCApp());
      res = ResourceBundle.getBundle("notindiv.Res" + dlg.getCApp().getIdioma());
      dlgIndiv = dlg;
      stubCliente = dlgIndiv.stubCliente;
      this.hashNotifEDO = hashNotifEDO;
      jbInit();
      //RellenaLista(dlgIndiv.getListaPanelCabecera());
      RellenaLista(dlgIndiv.getListaPanelCaso());
      modoOperacion = modo;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(745, 170));
    xYLayout.setHeight(282);
    xYLayout.setWidth(755);
    lFechaIniSint.setText(res.getString("lFechaIniSint.Text"));
    lAsociado.setText(res.getString("lAsociado.Text"));
    lColectivo.setText(res.getString("lColectivo.Text"));
    lDerivado.setText(res.getString("lDerivado.Text"));
    lCentro.setText(res.getString("lCentro.Text"));
    lDiagClinico.setText(res.getString("lDiagClinico.Text"));
    lDgOtros.setText(res.getString("lDgOtros.Text"));
    lClasificacion.setText(res.getString("lClasificacion.Text"));
    lDiagSerol.setText(res.getString("lDiagSerol.Text"));
    lDiagMicro.setText(res.getString("lDiagMicro.Text"));
    CfechaIniSint.setBackground(Color.white);
    pnl2.setLabel(res.getString("pnl2.Label"));
    pnl1.setLabel(res.getString("pnl1.Label"));
    pnl1.setFont(new Font("Dialog", 3, 12));
    pnl1.setForeground(new Color(153, 0, 0));
    pnl1.setFont(new Font("Dialog", 3, 12));
    pnl1.setForeground(new Color(153, 0, 0));
    pnl2.setFont(new Font("Dialog", 3, 12));
    pnl2.setForeground(new Color(153, 0, 0));
    this.setLayout(xYLayout);
    this.add(txtDgOtros, new XYConstraints(465, 168, 166, -1));
    this.add(chkDgMicrobio, new XYConstraints(395, 168, 24, -1));
    this.add(lFechaIniSint, new XYConstraints(60, 47, 128, -1));
    this.add(lAsociado, new XYConstraints(309, 47, 125, -1));
    this.add(lColectivo, new XYConstraints(60, 72, 58, -1));
    this.add(lDerivado, new XYConstraints(60, 97, 67, -1));
    this.add(lCentro, new XYConstraints(154, 97, 49, -1));
    this.add(lDiagClinico, new XYConstraints(60, 168, 123, -1));
    this.add(CfechaIniSint, new XYConstraints(204, 47, 95, -1));
    this.add(chkDgSerologico, new XYConstraints(279, 168, 24, -1));
    this.add(lDgOtros, new XYConstraints(422, 168, 41, -1));
    this.add(lClasificacion, new XYConstraints(60, 193, 82, -1));
    this.add(lDiagSerol, new XYConstraints(210, 168, 67, -1));
    this.add(lDiagMicro, new XYConstraints(308, 168, 86, -1));
    this.add(choClasificacion, new XYConstraints(183, 193, 229, -1));
    this.add(chkAsociadoCaso, new XYConstraints(434, 47, 23, -1));
    this.add(chkDgClinico, new XYConstraints(184, 168, 23, -1));
    this.add(chkDerivado, new XYConstraints(127, 97, 27, -1));
    this.add(txtCentro, new XYConstraints(204, 97, 325, -1));
    this.add(txtColectivo, new XYConstraints(204, 72, 325, -1));
    this.add(txtAsociado, new XYConstraints(465, 47, 166, -1));
    this.add(pnl2, new XYConstraints(27, 155, 630, 80));
    this.add(pnl1, new XYConstraints(27, 33, 628, 100));

    pnl2.setFont(new Font("Dialog", 3, 12));
    pnl2.setForeground(new Color(153, 0, 0));

    txtAsociado.setVisible(false);
    txtCentro.setVisible(false);
    lCentro.setVisible(false);
    setBorde(false);
    CfechaIniSint.requestFocus();

    choClasificacion.setName("clasificacion");
    chkDerivado.setName("derivado");
    chkAsociadoCaso.setName("asociadocaso");
    choClasificacion.addItemListener(chkItemAdapter);
    chkDerivado.addItemListener(chkItemAdapter);
    chkAsociadoCaso.addItemListener(chkItemAdapter);

    CfechaIniSint.addFocusListener(new java.awt.event.FocusAdapter() {
      public void focusLost(FocusEvent e) {
        CfechaIniSint_focusLost();
      }
    });
  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {

      /*case modoNORMAL:
        enableControls(true);
        // se filtran los controles deshabilitados en funci�n del
        // modo de entrada
        //setModoMod();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break; */

      case modoESPERA:

        // se deshabilitan todos los controles
        //enableControls(false);

        // y se deshabilitan el resto de controles
        choClasificacion.setEnabled(false);
        txtDgOtros.setEnabled(false);
        chkDgMicrobio.setEnabled(false);
        CfechaIniSint.setEnabled(false);
        chkDgSerologico.setEnabled(false);
        chkAsociadoCaso.setEnabled(false);
        chkDgClinico.setEnabled(false);
        chkDerivado.setEnabled(false);
        txtCentro.setEnabled(false);
        txtColectivo.setEnabled(false);
        txtAsociado.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA:
      case modoMODIFICACION:
        enableControls(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
      case modoCONSULTA:
      case modoSOLODECLARANTES: // s�lo se pueden mod. declarantes
        enableControls(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
  }

  private void enableControls(boolean en) {
    txtDgOtros.setEnabled(en);
    chkDgMicrobio.setEnabled(en);
    CfechaIniSint.setEnabled(en);
    chkDgSerologico.setEnabled(en);
    choClasificacion.setEnabled(en);
    chkAsociadoCaso.setEnabled(en);
    chkDgClinico.setEnabled(en);
    chkDerivado.setEnabled(en);
    txtCentro.setEnabled(en);
    txtColectivo.setEnabled(en);
    txtAsociado.setEnabled(en);
  }

  // ARG: En vez de CFechaSimple usamos CFecha
  // private void ActCFecha(CFechaSimple CFecha, String sFecha) {
  private void ActCFecha(CFecha CFecha, String sFecha) {
    CFecha.setText(sFecha);
    CFecha.ValidarFecha();
    CFecha.setText(CFecha.getFecha());
  }

  // retorna la fecha de inicio de s�ntomas
  public String getFechaIniSint() {
    // antes de recogerla, se actualiza
    ActCFecha(CfechaIniSint, CfechaIniSint.getText().trim());
    return CfechaIniSint.getText().trim();
  }

  public CLista getListaClasificacion() {
    return listaClasificacion;
  }

  public void setListaClasificacion(CLista list) {
    listaClasificacion = list;
  }

  public void RellenaCaso(DataTab datatab) {

    String sDgOtros = "", sItDiagmicro = "", sItDiagSero = "";
    String sFcInisnt = "", sItAsociado = "", sItDiagCli = "";
    String sItDerivado = "", sDsCentroder = "", sDsColectivo = "";
    String sDsAsociado = "", sCdClasifDiag = "", sDsClasifDiag = "";
    int j;

    if (datatab.getDS_DIAGOTROS() != null) {
      sDgOtros = datatab.getDS_DIAGOTROS();
    }
    txtDgOtros.setText(sDgOtros);

    if (datatab.getIT_DIAGMICRO() != null) {
      if (datatab.getIT_DIAGMICRO().equals("S")) {
        sItDiagmicro = "S";
      }
    }
    if (sItDiagmicro.equals("S")) {
      chkDgMicrobio.setState(true);
    }
    else {
      chkDgMicrobio.setState(false);

    }
    if (datatab.getIT_DIAGSERO() != null) {
      if (datatab.getIT_DIAGSERO().equals("S")) {
        sItDiagSero = "S";
      }
    }
    if (sItDiagSero.equals("S")) {
      chkDgSerologico.setState(true);
    }
    else {
      chkDgSerologico.setState(false);

    }
    if (datatab.getFC_INISNT() != null) {
      sFcInisnt = datatab.getFC_INISNT();
    }
    CfechaIniSint.setText(sFcInisnt);

    if (datatab.getIT_ASOCIADO() != null) {
      if (datatab.getIT_ASOCIADO().equals("S")) {
        sItAsociado = "S";
      }
    }
    if (sItAsociado.equals("S")) {
      chkAsociadoCaso.setState(true);
      txtAsociado.setVisible(true);
      if (datatab.getDS_ASOCIADO() != null) {
        txtAsociado.setText(datatab.getDS_ASOCIADO());
      }
    }
    else {
      chkAsociadoCaso.setState(false);

    }
    if (datatab.getIT_DIAGCLI() != null) {
      if (datatab.getIT_DIAGCLI().equals("S")) {
        sItDiagCli = "S";
      }
    }
    if (sItDiagCli.equals("S")) {
      chkDgClinico.setState(true);
    }
    else {
      chkDgClinico.setState(false);

    }
    if (datatab.getIT_DERIVADO() != null) {
      if (datatab.getIT_DERIVADO().equals("S")) {
        sItDerivado = "S";
      }
    }
    if (sItDerivado.equals("S")) {
      chkDerivado.setState(true);
      txtCentro.setVisible(true);
      lCentro.setVisible(true);
      if (datatab.getDS_CENTRODER() != null) {
        sDsCentroder = datatab.getDS_CENTRODER();
        txtCentro.setText(sDsCentroder);
      }
    }
    else {
      chkDerivado.setState(false);

    }
    if (datatab.getDS_COLECTIVO() != null) {
      sDsColectivo = datatab.getDS_COLECTIVO();
    }
    txtColectivo.setText(sDsColectivo);

    if (datatab.getCD_CLASIFDIAG() != null) {
      sCdClasifDiag = datatab.getCD_CLASIFDIAG();
      //mlm if (datatab.getDS_CLASIFDIAG() != null)
      //mlm sDsClasifDiag = datatab.getDS_CLASIFDIAG();

      //cho de clasificacion **********
    }
    if (!sCdClasifDiag.equals("")) {
      setClasif(sCdClasifDiag);
    }

  } //fin

//CLASIF ****** ***** ***** **** ****
  public void setClasif(String sCod) {
    DataListaEDOIndiv dataI;
    // se comprueba que los datos se encuentren rellenos
    if (!sCod.equals("")) {

      if (getListaClasificacion() != null) {
        for (int i = 0; i < getListaClasificacion().size(); i++) {
          dataI = (DataListaEDOIndiv) getListaClasificacion().elementAt(i);
          if (dataI.getCodEnfermedad().equals(sCod)) {
            choClasificacion.select(i + 1);
            break;
          }
        }
      }
    }
  } //clasif

  // se ejcuta en respuesta al focuslost en CfechaIniSint
  void CfechaIniSint_focusLost() {

    // se valida y fija la fecha introducida
    // ARG: Se cambia para que avise cuando la fecha tiene un formato no valido
    //ActCFecha(CfechaIniSint, CfechaIniSint.getText().trim());

    CfechaIniSint.ValidarFecha();
    if (CfechaIniSint.getValid().equals("S")) {
      CfechaIniSint.setText(CfechaIniSint.getFecha());
    }
    else {
      CMessage msgBox = new CMessage(app, CMessage.msgERROR,
                                     res.getString("msg104.Text"));
      msgBox.show();
      msgBox = null;
      CfechaIniSint.setText(CfechaIniSint.getFecha());
    }

    // se comprueba que no sea mayor que la fecha actual
    // si es mayor, se actualiza a la fecha actual
    if (UtilEDO.fecha1MayorqueFecha2(getFechaIniSint(), UtilEDO.getFechaAct())) {
      ShowWarning(res.getString("msg75.Text"));
      ActCFecha(CfechaIniSint, UtilEDO.getFechaAct());
    }
  }

  void chkAsociadoCaso_itemStateChanged(ItemEvent e) {
    if (chkAsociadoCaso.getState()) {
      txtAsociado.setVisible(true);
      validate();
    }
    else {
      txtAsociado.setVisible(false);
      txtAsociado.setText("");
    }
  }

  void chkDerivado_itemStateChanged(ItemEvent e) {
    if (chkDerivado.getState()) {
      lCentro.setVisible(true);
      txtCentro.setVisible(true);
      validate();
    }
    else {
      lCentro.setVisible(false);
      txtCentro.setVisible(false);
      txtCentro.setText("");
    }
  }

  void RellenaLista(CLista lista) {

    if (lista == null) {

      int modo = modoOperacion;
      modoOperacion = modoESPERA;
      Inicializar();

      // se ejecuta la llamada al servlet correspondiente
      try {
        listaClasificacion = new CLista();
        parametros = new CLista();
        DataListaEDOIndiv dataInd = new DataListaEDOIndiv();
        parametros.addElement(dataInd);

        parametros.setLogin(app.getLogin());
        parametros.setLortad(app.getParameter("LORTAD"));
        result = Comunicador.Communicate(getApp(),
                                         stubCliente,
                                         servletSELECCION_LISTA_CLASIFEDO,
                                         strSERVLET_EDOIND,
                                         parametros);

        setListaClasificacion(result);
        dlgIndiv.setListaPanelCaso(result);

        //modoOperacion = modoNORMAL;
        modoOperacion = modo;
        Inicializar();

      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    else {
      setListaClasificacion(lista);
    }

    //si vienen resultados
    choClasificacion.addItem("    ");
    if (listaClasificacion != null) {
      for (int i = 0; i < listaClasificacion.size(); i++) {
        DataListaEDOIndiv dataCl =
            (DataListaEDOIndiv) listaClasificacion.elementAt(i);
        String sCod = dataCl.getCodEnfermedad();
        String sDes = dataCl.getDesEnfermedad();
        choClasificacion.addItem(sCod + "   " + sDes);
      } //----------
    }
  }

  public String getCodClasif() {
    int index = choClasificacion.getSelectedIndex();
    /*index--;
         if (index >= 0) {
      DataListaEDOIndiv dataInd = (DataListaEDOIndiv) listaClasificacion.elementAt(index);
      return (dataInd.getCodEnfermedad());
         }
         return ""; */
    //mlm
    if (index > 0) {
      DataListaEDOIndiv dataInd = (DataListaEDOIndiv) listaClasificacion.
          elementAt(index - 1);
      return (dataInd.getCodEnfermedad());
    }
    return "";
  }

  public String getDesClasif() {
    int index = choClasificacion.getSelectedIndex();
    /* index--;
     if (index >= 0) {
       DataListaEDOIndiv dataInd = (DataListaEDOIndiv) listaClasificacion.elementAt(index);
       return (dataInd.getDesEnfermedad());
     }
     return ""; */
    //mlm
    if (index > 0) {
      DataListaEDOIndiv dataInd = (DataListaEDOIndiv) listaClasificacion.
          elementAt(index - 1);
      return (dataInd.getDesEnfermedad());
    }
    return "";
  }

  public String getItDiagMicro() {
    if (chkDgMicrobio.getState()) {
      return "S";
    }
    return "N";
  }

  public String getItDiagClin() {
    if (chkDgClinico.getState()) {
      return "S";
    }
    return "N";
  }

  public String getItDiagSero() {
    if (chkDgSerologico.getState()) {
      return "S";
    }
    return "N";
  }

  public String getItAsociado() {
    if (chkAsociadoCaso.getState()) {
      return "S";
    }
    return "N";
  }

  public String getItDerivado() {
    if (chkDerivado.getState()) {
      return "S";
    }
    return "N";
  }

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  private void ShowWarning(String sMessage) {
    dlgIndiv.ShowWarning(sMessage);
  }
}

// listener de los mousepressed en los choices
class PanelEDOIndivCasochkitemAdapter
    implements java.awt.event.ItemListener {
  PanelEDOIndivCaso adaptee;

  PanelEDOIndivCasochkitemAdapter(PanelEDOIndivCaso adaptee) {
    this.adaptee = adaptee;
  }

  public void itemStateChanged(ItemEvent e) {

    String sName = ( (Component) e.getSource()).getName();

    /// buscamos que bot�n es el que se ha pinchado y lo ejecutamos
    if (sName.equals("derivado")) {
      adaptee.chkDerivado_itemStateChanged(e);
    }
    else if (sName.equals("asociadocaso")) {
      adaptee.chkAsociadoCaso_itemStateChanged(e);
    }
  }
}
