package notindiv;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Locale;
import java.util.ResourceBundle;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import enfermo.DataEnfermo;
import enfermo.DialBusEnfermo;
import enfermo.Fechas;
import enfermo.comun;
import fechas.CFecha;
import notdata.DataCasoEDO;
import notdata.DataEnfermoBD;
import notdata.DataEntradaEDO;
import notdata.DataIndivEnfermedad;
import notdata.DataMunicipioEDO;
import notdata.DataTab;
import notdata.DataValoresEDO;
import notduplic.DialogCasoEDO;
import notduplic.DialogSelEnfermo;
import notutil.CListaNiveles1;
import notutil.Comunicador;
import notutil.UtilEDO;
import obj.CFechaSimple;
import sapp.StubSrvBD;
import suca.panelsuca;
import suca.zonificacionSanitaria;

public class PanelEDOIndivCabecera
    extends CPanel
    implements zonificacionSanitaria {

  //Almacen Zonificacion
  public String sN1Bk = "";
  ResourceBundle res;
  public String sN2Bk = "";

  //Antes de espera en suca Guardar el modo
  public int iAntesDeZona = 0;

  //control de activacion automatica de boton enfermo
  protected boolean bDeboLanzarEnfermo = true;

  // contenedor de im�genes
  protected CCargadorImagen imgs = null;

  //CA-prov
  protected boolean bCA_Cargada = false;
  protected boolean bProv_Cargada = false;

  //Guardar enfermo: (desde)
  // select,tstCodEnfermo_focuslost, tstCodEnfermo_focuslost,
  // btnlupa, btnEnfermo, btnCaso
  public String sStringBk = "";
  public String sEnfermedadBk = "";

  // modificacion jlt
  //public boolean enfermoborrado = false;

  //Datos del enfermo de la BD, EN TODO MOMENTO!!!!
  protected DataEnfermoBD dataEnfermoBD = null;

  //fechas
  public String sFechaRecepcion = "";
  public String sFechaNotificacion = "";

  SimpleDateFormat formato3 = new SimpleDateFormat("dd/MM/yyyy",
      new Locale("es", "ES"));

  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/enfermos.gif",
      "images/casos.gif"};

  /* MODOS DE OPERACI�N DEL PANEL */
  // modos de operaci�n del panel
  //final int modoNORMAL = 0;
  final int modoESPERA = 2;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;
  final int modoSOLODECLARANTES = 7;

  /* MODOS DE OPERACI�N DE LOS SERVLETS A LOS QUE SE LLAMA
    DESDE EL PANEL ACTUAL */

  // modos de operaci�n del servlet SrvEntradaEDO
  final int servletOBTENER_NIV1_X_CODIGO_TODAS_AUTORIZ = 19;
  final int servletOBTENER_NIV1_X_DESCRIPCION_TODAS_AUTORIZ = 20;
  final int servletSELECCION_NIV1_X_CODIGO_TODAS_AUTORIZ = 21;
  final int servletSELECCION_NIV1_X_DESCRIPCION_TODAS_AUTORIZ = 22;
  final int servletSELECCION_NIV2_X_CODIGO_TODAS_AUTORIZ = 23;
  final int servletOBTENER_NIV2_X_CODIGO_TODAS_AUTORIZ = 24;
  final int servletSELECCION_NIV2_X_DESCRIPCION_TODAS_AUTORIZ = 25;
  final int servletOBTENER_NIV2_X_DESCRIPCION_TODAS_AUTORIZ = 26;
  final int servletSELECCION_ZBS_X_CODIGO = 11;
  final int servletOBTENER_ZBS_X_CODIGO = 12;
  final int servletSELECCION_ZBS_X_DESCRIPCION = 13;
  final int servletOBTENER_ZBS_X_DESCRIPCION = 14;
  // ARG: Nuevos modos de operaci�n del servlet SrvEntradaEDO
  final int servletOBTENER_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ = 34;
  final int servletOBTENER_TODOS_NIV1_X_DESCRIPCION_TODAS_AUTORIZ = 35;
  final int servletSELECCION_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ = 36;
  final int servletSELECCION_TODOS_NIV1_X_DESCRIPCION_TODAS_AUTORIZ = 37;

  // modos de operaci�n del servlet SrvMunicipio
  final int servletSELECCION_MUNICIPIO_X_CODIGO = 9;
  final int servletOBTENER_MUNICIPIO_X_CODIGO = 10;
  final int servletSELECCION_MUNICIPIO_X_DESCRIPCION = 11;
  final int servletOBTENER_MUNICIPIO_X_DESCRIPCION = 12;

  // modos de operaci�n del servlet SrvEDOIndiv
  final int servletSELECCION_LISTA_ENFER = 9;
  final int servletSELECCION_LISTA_ENFER_TDOC_SEXO = 100;
  // modificacion jlt 27/11/2001
  // no pone condici�n de it_baja = N para la enfermedad
  final int servletSELECCION_LISTA_ENFER_TDOC_SEXO_M = 101;

  // modos de operaci�n del servlet SrvEnfermo
  final int modoDATOSENFERMO = 10;
  final int modoDATOSENFERMOTRAMERO = 320;

  /* CONSTANTES DEL PANEL */

  // constantes del panel
  final String strSERVLET = "servlet/SrvEntradaEDO";
  final String strSERVLET_EDOIND = "servlet/SrvEDOIndiv";
  final String strSERVLET_MUNICIPIO = "servlet/SrvMunicipio";
  final String strSERVLET_MUNICIPIO_CONT = "servlet/SrvMunicipioCont";
  final String strSERVLET_ENFERMEDAD = "servlet/SrvEnfermedad";
  final String strSERVLET_ENFERMO = "servlet/SrvEnfermo";
  final String sVigi = "I";

  /* PAR�METROS DEL PANEL */

  // stub que se inicializar� con el stub de DialogListaEDOIndiv
  protected StubSrvBD stubCliente = null;

  // modo de operaci�n del panel
  public int modoOperacion = modoALTA;
  // modo de entrada al panel (modoALTA / modoBAJA / modoMODIFICACION )

  // Listas generales de datos fijos que se obtienen al principio
  // Lista que contiene las enfermedades individualizadas
  protected CLista listaEnfermedad = null;
  protected CLista listaTipoDocumento = null;
  protected CLista listaSexo = null;
  protected CLista listaPaises = null;
  protected CLista listaCA = null;
  protected CLista listaTpVial = null;
  protected CLista listaTpNum = null;
  protected CLista listaCalNum = null;
  protected CLista listaProvincias = null;

  protected CLista listaTotal = new CLista();

  protected CLista listaPanelDeclarantes = null; //MIRAR SI SOBRA!!!
  protected CLista listaPanelCaso = null; //MIRAR SI SOBRA!!!

  // Lista que contiene los datos del enfermo
  protected CLista listaEnfermo = null;
  // listas auxiliares
  protected CLista parametros = null;
  CLista resultadoEnfermedad = null;
  protected CLista result = null;
  CLista data = new CLista();
  CLista appList = null;

  // sincronizaci�n
  protected boolean sinBloquear = true;

  // objeto que sirve de sincronizaci�n
  Object sincro = new Object();

  // indica si tiene permiso para ver los nombres o las siglas
  public boolean bFlagEnfermo = true; //si false, no puede ver normal

  // hashtable de NotifEDO
  Hashtable hashNotifEDO = null;
  // dialog que contiene a este panel
  DialogListaEDOIndiv dlgIndiv;

  //variable control
  //PARA NO DISPARAR 2 VECES LOSTFOCUS!!!!!!!
  boolean bFocoN1 = false;
  boolean bFocoN2 = false;
  boolean bFocoZBS = false;
  //boolean bFocoMun = false;

  boolean bSalir_del_dialogo = false;

  // variables booleanas que indican si el contenido de los campos
  // respectivos es correcto
  public boolean bNivel1Valid = false;
  public boolean bNivel2Valid = false;
  public boolean bNivelZBSValid = false;
  //public boolean bMunValid = false;

  // variable booleanas que indican si el nivel elegido se ha
  // seleccionado de la lista
  private boolean bNivel1SelDePopup = false;
  private boolean bNivel2SelDePopup = false;
  private boolean bNivelZBSSelDePopup = false;
  //private boolean bMunSelDePopup = false;

  /* LISTENERS */
  ActualizarZonaRiesgo actualizadorZonaRiesgo = new ActualizarZonaRiesgo(this);
  PanelEDOIndivCabeceraactionAdapter actionAdapter = new
      PanelEDOIndivCabeceraactionAdapter(this);
  PanelEDOIndivCabecerafocusAdapter focusAdapter = new
      PanelEDOIndivCabecerafocusAdapter(this);
  PanelEDOIndivCabecerachItemListener chItemListener = new
      PanelEDOIndivCabecerachItemListener(this);
  PanelEDOIndivCabeceratextAdapter KeyAdapter = new
      PanelEDOIndivCabeceratextAdapter(this);

  /* CONTROLES */
  XYLayout xYLayout = new XYLayout();
  Panel panel1 = new Panel();
  Label lEnfermedad = new Label();
  Choice choEnfermedad = new Choice();
  Label lblCaso = new Label();
  Label lblNCaso = new Label();
  Label lFechaNac = new Label();

  // ARG: en vez de CFechaSimple usamos CFecha
  // CFechaSimple txtFechaNac = new CFechaSimple("N");
  CFecha txtFechaNac = new CFecha("N");

  Label lEdad = new Label();
  TextField txtEdad = new TextField();
  Label label8 = new Label();
  Label lTelefono = new Label();
  TextField txtTelefono = new TextField();
  TextField txtCodEnfermo = new TextField();
  Label lCodEnfermo = new Label();
  TextField txtDesZBS = new TextField();
  TextField txtCodZBS = new TextField();
  TextField txtCodNivel1 = new TextField();
  TextField txtCodNivel2 = new TextField();
  ButtonControl btnZBS = new ButtonControl();
  Label lZBS = new Label();
  ButtonControl btnNivel1 = new ButtonControl();
  ButtonControl btnNivel2 = new ButtonControl();
  TextField txtDesNivel1 = new TextField();
  TextField txtDesNivel2 = new TextField();
  Label lNivel1 = new Label();
  Label lNivel2 = new Label();
  TextField txtApe1 = new TextField();
  TextField txtApe2 = new TextField();
  TextField txtNombre = new TextField();
  Label lApeNombre = new Label();
  Label lTipoDocumento = new Label();
  Choice choTipoDocumento = new Choice();
  Label lDocumento = new Label();
  TextField txtDocumento = new TextField();
  Choice choSexo = new Choice();
  Choice choEdad = new Choice();
  ButtonControl btnEnfermo = new ButtonControl();
  Label lTipoFechaNac = new Label();
  ButtonControl btnCaso = new ButtonControl();
  Label lFechaNotif = new Label();
  CFechaSimple CfechaNotif = new CFechaSimple("S");
  ButtonControl btnLupaEnfermo = new ButtonControl();

  // suca
  // JRM: P�blico para que lo vea DialogListaEDOIndiv. Rompe filosof�a OOP
  // pero esto se hab�a tenido que pensar antes.
  public panelsuca panSuca = null;

  boolean bTramero = false;
//  StatusBar statusBar1 = new StatusBar();

  /**
       *  esta funci�n devuelve la edad en a�os de una persona tomando como referencia
   *  la fecha fecReferencia
   *
   *  @param  fecNacimiento es la fecha de nacimiento de la persona
   *  @param  fecReferencia es la fecha sobre la que se calcula la edad
   *  @return  son los a�os que tiene
   */
  public int edadAniosConRef(java.util.Date fecNacimiento,
                             java.util.Date fecReferencia) {
    //Date hoy = new Date();
    if (fecNacimiento == null) {
      return 0;
    }

    long edad_mili = fecReferencia.getTime() - fecNacimiento.getTime();

    if (edad_mili > 0) {
      return (int) (edad_mili / Fechas.mili_anio);
    }
    else {
      return 0;
    }
  }

  /**
   *  esta funci�n devuelve la edad en meses de una persona
   *
   *  @param  fecNacimiento es la fecha de nacimiento de la persona
   *  @return  son los meses que tiene
   */
  public int edadMesesConRef(java.util.Date fecNacimiento,
                             java.util.Date fecReferencia) {
    //Date hoy = new Date();
    if (fecNacimiento == null) {
      return 0;
    }

    long edad_mili = fecReferencia.getTime() - fecNacimiento.getTime();

    if (edad_mili > 0) {
      return (int) (edad_mili / Fechas.mili_mes);
    }
    else {
      return 0;
    }
  }

  /* CONSTRUCTOR */
  public PanelEDOIndivCabecera(DialogListaEDOIndiv dlg, int modo,
                               Hashtable hashNotifEDO) {
    try {
      dlgIndiv = dlg;
      stubCliente = dlgIndiv.stubCliente;
      setApp(dlgIndiv.getCApp());
      res = ResourceBundle.getBundle("notindiv.Res" +
                                     dlgIndiv.getCApp().getIdioma());
      this.hashNotifEDO = hashNotifEDO;

      // inicializa SUCA
      // tramero
      boolean tramero = true;

      if (getApp().getIT_TRAMERO().equals("N")) {
        tramero = false;

        // visibilidad de datos sensibles
      }
      int modoSuca = panelsuca.modoINICIO;

      if (getApp().getIT_FG_ENFERMO().equals("N")) {
        modoSuca = panelsuca.modoCONFIDENCIAL;

      }
      panSuca = new panelsuca(getApp(), tramero, modoSuca, false, this);
      bTramero = panSuca.getModoTramero();

      jbInit();

      // modificacion jlt 27/11/2001
      modoOperacion = modo;

      RellenaListas(dlgIndiv.getListaPanelCabecera());

      if (modo == modoALTA) {
        txtDocumento.setVisible(false); //cris
        lDocumento.setVisible(false); //cris
      }

      modoOperacion = modo;
      Inicializar();
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

      // Arrancamos hilo de comprobaci�n de Zona de Riesgo
      actualizadorZonaRiesgo.start();
    }
    catch (Exception ex) {
      ex.printStackTrace();
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
  }

  void jbInit() throws Exception {

    // carga las im�genes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnLupaEnfermo.setImage(imgs.getImage(0));
//    statusBar1.setText("statusBar1");
    lblCaso.setText("");
    lblNCaso.setText("");
    btnNivel1.setImage(imgs.getImage(0));
    btnNivel2.setImage(imgs.getImage(0));
    btnZBS.setImage(imgs.getImage(0));
    btnEnfermo.setImage(imgs.getImage(1));
    btnCaso.setImage(imgs.getImage(2));

    //mlm
    this.setSize(new Dimension(745, 170));
    xYLayout.setHeight(365);
    xYLayout.setWidth(755);
    this.setLayout(xYLayout);

    lEnfermedad.setText(res.getString("msg58.Text"));
    choEnfermedad.setBackground(new Color(255, 255, 150));
    lTelefono.setText(res.getString("lTelefono.Text"));
    txtCodEnfermo.setBackground(new Color(255, 255, 150));
    lCodEnfermo.setText(res.getString("lCodEnfermo.Text"));
    txtDesZBS.setEditable(false);
    txtApe1.setBackground(new Color(255, 255, 150));
    lApeNombre.setText(res.getString("lApeNombre.Text"));
    lTipoDocumento.setText(res.getString("lTipoDocumento.Text"));
    lDocumento.setText(res.getString("lDocumento.Text"));
    lZBS.setText(this.app.getNivel3() + ":");

    txtDesNivel1.setEditable(false);
    txtDesNivel2.setEditable(false);

    lNivel1.setText(this.app.getNivel1() + ":");
    lNivel2.setText(this.app.getNivel2() + ":");
    lFechaNac.setText(res.getString("lFechaNac.Text"));
    lEdad.setText(res.getString("lEdad.Text"));
    btnEnfermo.setLabel(res.getString("btnEnfermo.Label"));
    btnCaso.setLabel(res.getString("btnCaso.Label"));
    lFechaNotif.setText(res.getString("lFechaNotif.Text"));
    label8.setText(res.getString("label8.Text"));

    this.add(txtCodEnfermo, new XYConstraints(135, 15, 86, -1));
    this
        .add(btnLupaEnfermo, new XYConstraints(227, 13, -1, -1));
    // modificacion jlt 20/11/2001
    // Pedro Bengoa no quiere esta funcionalidad
    //this.add(btnEnfermo, new XYConstraints(257, 13, 115, -1));
    this.add(lCodEnfermo, new XYConstraints(15, 15, 98, -1));
    this.add(lEnfermedad, new XYConstraints(15, 45, 81, -1));
    this.add(choEnfermedad, new XYConstraints(135, 45, 295, -1));
    this.add(btnCaso, new XYConstraints(436, 43, 115, -1));
    this.add(CfechaNotif, new XYConstraints(135, 75, 86, -1));
    this.add(choTipoDocumento, new XYConstraints(350, 75, 162, -1));
    this.add(txtDocumento, new XYConstraints(610, 75, 116, -1));
    this.add(txtApe1, new XYConstraints(135, 104, 145, -1));
    this.add(txtApe2, new XYConstraints(287, 104, 225, -1));
    this.add(txtNombre, new XYConstraints(517, 104, 224, -1));
    this.add(lFechaNac, new XYConstraints(15, 132, 113, -1));
    this.add(txtFechaNac, new XYConstraints(135, 132, 85, -1));
    this.add(lEdad, new XYConstraints(275, 132, 37, -1));
    this.add(txtEdad, new XYConstraints(313, 132, 35, -1));
    this.add(choEdad, new XYConstraints(353, 132, 81, -1));
    this.add(lTipoFechaNac, new XYConstraints(225, 132, 44, -1));
    this.add(choSexo, new XYConstraints(490, 132, 79, -1));
    this.add(txtTelefono, new XYConstraints(645, 132, 96, -1));
    this.add(label8, new XYConstraints(445, 132, 38, -1));
    this.add(lTelefono, new XYConstraints(580, 132, 59, -1));
    this.add(panSuca, new XYConstraints(13, 156, 596, 116));
    this.add(lNivel1, new XYConstraints(15, 275, 92, 19));
    this.add(txtCodNivel1, new XYConstraints(135, 273, 66, -1));
    this.add(btnNivel1, new XYConstraints(215, 273, -1, -1));
    this.add(txtDesNivel1, new XYConstraints(257, 273, 152, -1));
    this.add(lNivel2, new XYConstraints(15, 301, 92, 21));
    this.add(txtCodNivel2, new XYConstraints(135, 301, 66, -1));
    this.add(btnNivel2, new XYConstraints(215, 301, -1, -1));
    this.add(txtDesNivel2, new XYConstraints(257, 301, 152, -1));
    this.add(txtCodZBS, new XYConstraints(135, 329, 66, -1));
    this.add(btnZBS, new XYConstraints(215, 329, -1, -1));
    this.add(lZBS, new XYConstraints(15, 329, 92, -1));
    this.add(txtDesZBS, new XYConstraints(257, 329, 152, -1));
    this.add(lApeNombre, new XYConstraints(15, 104, 114, -1));
    this.add(lTipoDocumento, new XYConstraints(245, 75, 100, -1));
    this.add(lDocumento, new XYConstraints(535, 75, 73, -1));
    this.add(lFechaNotif, new XYConstraints(15, 75, 112, -1));
    this.add(lblCaso, new XYConstraints(584, 45, 70, 21));
    this.add(lblNCaso, new XYConstraints(671, 45, 70, 21));
//    this.add(statusBar1, new XYConstraints(329, 144, -1, -1));

    setBorde(false);

    btnLupaEnfermo.setActionCommand("enfermo");
    btnNivel1.setActionCommand("nivel1");
    btnNivel2.setActionCommand("nivel2");
    btnZBS.setActionCommand("zbs");
    btnEnfermo.setActionCommand("selenfermo");
    btnCaso.setActionCommand("datosenfermo");
    btnLupaEnfermo.addActionListener(actionAdapter);
    btnNivel1.addActionListener(actionAdapter);
    btnNivel2.addActionListener(actionAdapter);
    btnZBS.addActionListener(actionAdapter);
    btnEnfermo.addActionListener(actionAdapter);
    btnCaso.addActionListener(actionAdapter);

    txtApe1.setName("ape1");
    txtFechaNac.setName("fechanac");
    txtEdad.setName("edad");
    txtCodEnfermo.setName("enfermo");
    txtFechaNac.addFocusListener(focusAdapter);
    txtEdad.addFocusListener(focusAdapter);
    txtCodEnfermo.addFocusListener(focusAdapter);

    txtCodNivel1.setName("nivel1");
    txtCodNivel1.setBackground(new Color(255, 255, 150));
    txtCodNivel2.setName("nivel2");
    txtCodNivel2.setBackground(new Color(255, 255, 150));
    txtCodZBS.setName("zbs");
    txtCodZBS.setBackground(new Color(255, 255, 150));
    txtDocumento.setName("documento");

    txtDocumento.addFocusListener(focusAdapter);

    txtCodNivel1.addKeyListener(KeyAdapter);
    txtCodNivel2.addKeyListener(KeyAdapter);
    txtCodZBS.addKeyListener(KeyAdapter);

    txtCodNivel1.addFocusListener(focusAdapter);
    txtCodNivel2.addFocusListener(focusAdapter);
    txtCodZBS.addFocusListener(focusAdapter);

    choEnfermedad.setName("enfermedad");
    choTipoDocumento.setName("tipodoc");
    choSexo.setName("sexo");
    choEdad.setName("edad");

    choEdad.addItemListener(chItemListener);
    choEnfermedad.addItemListener(chItemListener);
    choTipoDocumento.addItemListener(chItemListener);
    choSexo.addItemListener(chItemListener);

    lDocumento.setVisible(false);
    txtDocumento.setVisible(false);
    CfechaNotif.setEnabled(false);
    CfechaNotif.setBackground(new Color(255, 255, 150));

//NOTA!!!!

    RellenaChoEdad();
    RellenaInicio();
  }

  public void RellenaListas(CLista lista) throws Exception {

    int modoservlet = 0;

    int modo = modoOperacion;

    // modificacion jlt 27/11/2001
    if (modo == modoMODIFICACION || modo == modoBAJA) {
      modoservlet = servletSELECCION_LISTA_ENFER_TDOC_SEXO_M;
    }
    else {
      modoservlet = servletSELECCION_LISTA_ENFER_TDOC_SEXO;

    }

    modoOperacion = modoESPERA;
    Inicializar();

    // se llama al servelt que rellena las listas de panel cabecera
    if (lista == null) {
      try {

        // enfermedad, y choices
        DataIndivEnfermedad dataEnf = new DataIndivEnfermedad(sVigi, "", "", "");
        parametros = new CLista();
        parametros.addElement(dataEnf);
        parametros.addElement(hashNotifEDO);
        parametros.setLogin(app.getLogin());
        parametros.setLortad(app.getParameter("LORTAD"));

        /*resultadoEnfermedad = Comunicador.Communicate(this.getApp(),
                            stubCliente,
                            servletSELECCION_LISTA_ENFER_TDOC_SEXO,
                            strSERVLET_ENFERMEDAD,
                            parametros);
         */

        resultadoEnfermedad = Comunicador.Communicate(this.getApp(),
            stubCliente,
            modoservlet,
            strSERVLET_ENFERMEDAD,
            parametros);

        if (resultadoEnfermedad != null && resultadoEnfermedad.size() == 3) {

          // relenamos las listas
          setListaEnfermedad( (CLista) resultadoEnfermedad.elementAt(0));
          setListaTipoDocumento( (CLista) resultadoEnfermedad.elementAt(1));
          setListaSexo( (CLista) resultadoEnfermedad.elementAt(2));
          // suca setListaPaises( (CLista) resultadoEnfermedad.elementAt(3));
          // suca setListaCA( (CLista) resultadoEnfermedad.elementAt(4));

          panSuca.setModoDeshabilitado(); //VER SI SOBRA!!!

          // Se ponen los campos de Provincia y Municipio a obligatorios (amarillo)
          panSuca.setObligatorios();

          setListaPaises(panSuca.getListaPaises());
          setListaCA(panSuca.getListaCA());
          setListaTpVial(panSuca.getListaTrameros());
          setListaTpNum(panSuca.getListaNumeros());
          setListaCalNum(panSuca.getListaCalificadores());

          listaTotal.addElement(listaEnfermedad);
          listaTotal.addElement(listaTipoDocumento);
          listaTotal.addElement(listaSexo);
          CLista aux = new CLista();
          aux.addElement(listaPaises);
          aux.addElement(listaCA);
          aux.addElement(listaTpVial);
          aux.addElement(listaTpNum);
          aux.addElement(listaCalNum);
          listaTotal.addElement(aux);

          // se cambia la lista en el dialogoindiv
          dlgIndiv.setListaPanelCabecera(listaTotal);

        }
      }
      catch (Exception e) {
        ShowWarning(e.toString());
      }
    } // endif listas != null
    else {
      //no se cambia la lista en el dialogoindiv
      setListaEnfermedad( (CLista) lista.elementAt(0));
      setListaTipoDocumento( (CLista) lista.elementAt(1));
      setListaSexo( (CLista) lista.elementAt(2));
      CLista lsuca = new CLista();
      lsuca = (CLista) lista.elementAt(3);

      setListaPaises( (CLista) lsuca.elementAt(0));
      setListaCA( (CLista) lsuca.elementAt(1));
      setListaTpVial( (CLista) lsuca.elementAt(2));
      setListaTpNum( (CLista) lsuca.elementAt(3));
      setListaCalNum( (CLista) lsuca.elementAt(4));

    }

    // rellena la lista de tdoc
    if (listaTipoDocumento != null) {
      comun.writeChoice(app, choTipoDocumento,
                        listaTipoDocumento, true, "DS_TIPODOC");
    }
    else {
      choTipoDocumento.addItem("          ");
    }

    // rellena la lista de enfermedad
    if (listaEnfermedad != null) {
      choEnfermedad.addItem("        ");

      //bucle pintado de la lista
      for (int i = 0; i < listaEnfermedad.size(); i++) {
        DataIndivEnfermedad dataEnfermedad =
            (DataIndivEnfermedad) listaEnfermedad.elementAt(i);

        //alinear enfermedad (6) *********************
        String cod_seis = Alinear_Cod_Enfermedad(dataEnfermedad.getCD_ENFCIE());
        //*********************  *********************
        choEnfermedad.addItem(cod_seis +
                              "  " + dataEnfermedad.getDS_PROCESO());
      }

    }

    // rellena la lista de sexo
    if (listaSexo != null) {
      comun.writeChoice(app, choSexo,
                        listaSexo, true, "DS_SEXO");
    }
    else {
      choSexo.addItem("         ");
    }

    modoOperacion = modo;
    Inicializar();
  }

  public String Alinear_Cod_Enfermedad(String cod) {
    //alinear a la izquierda el cod de enfermedad a seis caracteres
    int longitud = cod.length();
    String sAlmacen = cod;
    for (int i = longitud; i < 7; i++) {
      sAlmacen = sAlmacen + "_";
    }
    return (sAlmacen);
  }

  // permite inicializar el panel con el modoOperacion modo
  public void Inicializar(int modo) {

    modoOperacion = modo;
    Inicializar();

    //confidencialidad -----------
    if ( ( (String) hashNotifEDO.get("IT_FG_ENFERMO")) == null ||
        ! ( (String) hashNotifEDO.get("IT_FG_ENFERMO")).equals("S")) {
      setFlagEnfermo(false); //no LO VE
    }
    else if ( ( (String) hashNotifEDO.get("IT_FG_ENFERMO")).equals("S")) {
      setFlagEnfermo(true); //lo ve
      //-----------------------------

      //perfil 5: no ve botones, ni cod enfermo
    }
    if (getApp().getPerfil() == 5 &&
        modoOperacion == modoALTA) {
      btnCaso.setVisible(false);
      btnEnfermo.setVisible(false);
      btnLupaEnfermo.setVisible(false);
      lCodEnfermo.setVisible(false);
      txtCodEnfermo.setVisible(false);
      bDeboLanzarEnfermo = false;
    }
    else if (getApp().getPerfil() == 5 &&
             modoOperacion != modoALTA) {
      btnCaso.setVisible(true);
      btnEnfermo.setVisible(true);
      btnLupaEnfermo.setVisible(true);
      lCodEnfermo.setVisible(true);
      txtCodEnfermo.setVisible(true);

    }
    //-------------------------------------

  } //fin Inicializar

  // incializa el panel en funci�n del valor de modoOperacion
  public void Inicializar() {

    switch (modoOperacion) {

      case modoESPERA:

        //poner panel suca a espera
        panSuca.setModoEspera();

        choEdad.setEnabled(false);
        // suca choPais.setEnabled(false);
        // suca choCA.setEnabled(false);
        // suca choProvincia.setEnabled(false);
        // suca choTramero.setEnabled(false);
        choTipoDocumento.setEnabled(false);
        choSexo.setEnabled(false);
        choEnfermedad.setEnabled(false);
        btnLupaEnfermo.setEnabled(false);
        txtApe2.setEnabled(false);
        txtNombre.setEnabled(false);
        txtFechaNac.setEnabled(false);
        txtEdad.setEnabled(false);
        txtTelefono.setEnabled(false);
        // suca txtCodPostal.setEnabled(false);
        // suca txtCodMun.setEnabled(false);
        // suca btnMunicipio.setEnabled(false);
        //nO HACE FALTA, ESTAN SETEDITABLE A FALSE Y SE REFRESCAN MEJOR
        //txtDesMun.setEnabled(false);
        //txtDesZBS.setEnabled(false);
        //txtDesNivel1.setEnabled(false);
        //txtDesNivel2.setEnabled(false);
        // suca txtVia.setEnabled(false);
        // suca txtNo.setEnabled(false);
        // suca txtPiso.setEnabled(false);
        if (txtDocumento.isVisible()) {
          txtDocumento.setEnabled(false);
        }
        txtApe1.setEnabled(false);
        txtCodZBS.setEnabled(false);
        txtCodNivel1.setEnabled(false);
        txtCodNivel2.setEnabled(false);
        btnZBS.setEnabled(false);
        btnNivel1.setEnabled(false);
        btnNivel2.setEnabled(false);
        txtCodEnfermo.setEnabled(false); //SE PUEDE ESCRIBIR
        btnEnfermo.setEnabled(false);
        btnCaso.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.doLayout(); //POr que se puede escribir en codenfermo!!
        break;

      case modoALTA:

        //poner panel suca a espera
        panSuca.setModoNormal();

        txtCodEnfermo.setEnabled(true);
        txtCodEnfermo.setBackground(Color.white);
        btnLupaEnfermo.setEnabled(true);
        choEnfermedad.setEnabled(true);
        choTipoDocumento.setEnabled(true); //(false);
        // cris
        if (txtDocumento.isVisible()) {
          txtDocumento.setEnabled(true);

        }

        txtApe1.setEnabled(true); //(false);
        txtApe2.setEnabled(true); //(false);
        txtNombre.setEnabled(true); //(false);
        choSexo.setEnabled(true); //(false);
        txtFechaNac.setEnabled(true); //(false);
        txtEdad.setEnabled(true); //(false);
        choEdad.setEnabled(true); //(false);
        // suca choPais.setEnabled(false);
        //choCA.setEnabled(false);
        txtTelefono.setEnabled(true); //(false);
        CfechaNotif.setEnabled(false);
        btnEnfermo.setEnabled(true); //(false);
        btnCaso.setEnabled(true); //(false);
        //nO HACE FALTA, ESTAN SETEDITABLE A FALSE Y SE REFRESCAN MEJOR
        //txtDesMun.setEnabled(false);
        //txtDesZBS.setEnabled(false);
        //txtDesNivel1.setEnabled(false);
        //txtDesNivel2.setEnabled(false);
        // suca choTramero.setEnabled(false);
        txtCodZBS.setEnabled(true);
        txtCodNivel1.setEnabled(true);
        txtCodNivel2.setEnabled(true);
        btnZBS.setEnabled(true);
        btnNivel1.setEnabled(true);
        btnNivel2.setEnabled(true);
        // suca choProvincia.setEnabled(true);
        // suca txtCodPostal.setEnabled(true);
        // suca txtCodMun.setEnabled(true);
        // suca btnMunicipio.setEnabled(true);
        // suca txtVia.setEnabled(true);
        // suca txtNo.setEnabled(true);
        // suca txtPiso.setEnabled(true);
        txtCodZBS.setEnabled(true);
        btnZBS.setEnabled(true);
        txtCodNivel1.setEnabled(true);
        btnNivel1.setEnabled(true);
        txtCodNivel2.setEnabled(true);
        btnNivel2.setEnabled(true);
        // suca choCA.setEnabled(true);

        //CARGAR ENFERMO  ******* enfermedad
        sStringBk = txtCodEnfermo.getText();
        sEnfermedadBk = getCodEnfermedad();
        sN1Bk = txtCodNivel1.getText().trim();
        sN2Bk = txtCodNivel2.getText().trim();

        InicializaNivel2();
        InicializaNivelZBS();

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

        // modoMODIFICACI�N: no se permite la modificaci�n de:
        // c�digo de enfermo , enfermedad, fecnotif
      case modoMODIFICACION:

        //poner panel suca a espera
        panSuca.setModoNormal();

        //DESACTIVO AUTOMATISMO
        bDeboLanzarEnfermo = false;

        txtCodEnfermo.setEnabled(false);
        txtCodEnfermo.setBackground(new Color(255, 255, 150));
        choEnfermedad.setEnabled(false);
        choTipoDocumento.setEnabled(true);
        if (txtDocumento.isVisible()) {
          txtDocumento.setEnabled(true);

        }
        txtApe1.setEnabled(true);
        txtApe2.setEnabled(true);
        txtNombre.setEnabled(true);
        choSexo.setEnabled(true);
        txtFechaNac.setEnabled(true);
        txtEdad.setEnabled(true);
        choEdad.setEnabled(true);
        txtTelefono.setEnabled(true);
        CfechaNotif.setEnabled(false); //NO se cambia
        btnLupaEnfermo.setEnabled(false);
        btnEnfermo.setEnabled(false);
        btnCaso.setEnabled(false);
        //nO HACE FALTA, ESTAN SETEDITABLE A FALSE Y SE REFRESCAN MEJOR
        //txtDesMun.setEnabled(false);
        //txtDesZBS.setEnabled(false);
        //txtDesNivel1.setEnabled(false);
        //txtDesNivel2.setEnabled(false);
        // suca choTramero.setEnabled(false);//VIA; DE MOMENTO NADA
        // suca choPais.setEnabled(false);  // a false, siempre es Espa�a
        txtCodZBS.setEnabled(true);
        txtCodNivel1.setEnabled(true);
        txtCodNivel2.setEnabled(true);
        btnZBS.setEnabled(true);
        btnNivel1.setEnabled(true);
        btnNivel2.setEnabled(true);
        //choPais.setEnabled(true); // a true, si no, no se puede sel. CA
        // suca choCA.setEnabled(true); // a true, si no, no se puede sel. prov.
        // suca choProvincia.setEnabled(true);
        // suca txtCodPostal.setEnabled(true);
        // suca txtCodMun.setEnabled(true);
        // suca btnMunicipio.setEnabled(true);
        // suca txtVia.setEnabled(true);
        // suca txtNo.setEnabled(true);
        // suca txtPiso.setEnabled(true);
        txtCodZBS.setEnabled(true);
        btnZBS.setEnabled(true);
        txtCodNivel1.setEnabled(true);
        btnNivel1.setEnabled(true);
        txtCodNivel2.setEnabled(true);
        btnNivel2.setEnabled(true);
        //Inicializa niveles
        InicializaNivel2();
        InicializaNivelZBS();

        //Etiqueta con el Caso
        lblCaso.setText("N� Exped.:");
        lblNCaso.setText( (String) hashNotifEDO.get("NM_EDO"));
        lblNCaso.setFont(new Font("Dialog", 1, 12));

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        break;

        // modoBAJA: todos los campos deshabilitados
      case modoBAJA:
      case modoSOLODECLARANTES:
      case modoCONSULTA:

        //poner panel suca a espera
        panSuca.setModoDeshabilitado(); //ver si poner normal
        panSuca.doLayout();

        txtCodEnfermo.setEnabled(false);
        txtCodEnfermo.setBackground(new Color(255, 255, 150));
        btnLupaEnfermo.setEnabled(false);
        choEnfermedad.setEnabled(false);
        choTipoDocumento.setEnabled(false);
        if (txtDocumento.isVisible()) {
          txtDocumento.setEnabled(false);
        }
        txtApe1.setEnabled(false);
        txtApe2.setEnabled(false);
        txtNombre.setEnabled(false);
        choSexo.setEnabled(false);
        txtFechaNac.setEnabled(false);
        txtEdad.setEnabled(false);
        choEdad.setEnabled(false);
        // suca choPais.setEnabled(false);
        // suca choCA.setEnabled(false);
        txtTelefono.setEnabled(false);
        CfechaNotif.setEnabled(false);
        btnEnfermo.setEnabled(false);
        btnCaso.setEnabled(false);
        //nO HACE FALTA, ESTAN SETEDITABLE A FALSE Y SE REFRESCAN MEJOR
        //txtDesMun.setEnabled(false);
        //txtDesZBS.setEnabled(false);
        //txtDesNivel1.setEnabled(false);
        //txtDesNivel2.setEnabled(false);
        // suca choTramero.setEnabled(false);
        txtCodZBS.setEnabled(false);
        txtCodNivel1.setEnabled(false);
        txtCodNivel2.setEnabled(false);
        btnZBS.setEnabled(false);
        btnNivel1.setEnabled(false);
        btnNivel2.setEnabled(false);
        // suca choProvincia.setEnabled(false);
        // suca txtCodPostal.setEnabled(false);
        // suca txtCodMun.setEnabled(false);
        // suca btnMunicipio.setEnabled(false);
        // suca txtVia.setEnabled(false);
        // suca txtNo.setEnabled(false);
        // suca txtPiso.setEnabled(false);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }

    //aparte
    switch (modoOperacion) {
      case modoBAJA:

        //Etiqueta con el Caso
        lblCaso.setText(res.getString("lblCaso.Text"));
        lblNCaso.setText( (String) hashNotifEDO.get("NM_EDO"));
        lblNCaso.setFont(new Font("Dialog", 1, 12));
        break;
    } //fin segundo switch

    switch (modoOperacion) {
      case modoMODIFICACION:
      case modoBAJA:
      case modoCONSULTA:
      case modoSOLODECLARANTES:
      case modoESPERA:

        if (!getFlagEnfermo()) { //conf
          txtApe1.setEnabled(false);
        }
        break;
      case modoALTA:

        //conf y ya esta informado el enfermo(modif)
        if (!getFlagEnfermo()
            && !txtCodEnfermo.getText().trim().equals("")) {
          txtApe1.setEnabled(false);
        }
        else {
          txtApe1.setEnabled(true);
        }
        break;
    } //----tercer switch

  } //fin Inicializar

  // actualiza el estado del nivel2, condicionado al nivel1
  protected void InicializaNivel2() {
    if (txtDesNivel1.getText().trim().length() == 0) {
      txtCodNivel2.setText("");
      txtDesNivel2.setText("");
      btnNivel2.setEnabled(false);
    }
    else {
      btnNivel2.setEnabled(true);
    }

  }

  // actualiza el estado de la ZBS, condicionado al nivel2
  protected void InicializaNivelZBS() {
    if (txtDesNivel2.getText().trim().length() == 0) {

      txtCodZBS.setText("");
      txtDesZBS.setText("");
      btnZBS.setEnabled(false);
    }
    else {
      btnZBS.setEnabled(true);
    }

  }

  // mantiene una sincronizaci�n en los eventos
  public synchronized boolean bloquea() {
    if (sinBloquear) {
      // no hay nadie bloqueando pasamos a bloquear
      sinBloquear = false;
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      // ya est� bloqueado
      return false;
    }
  }

  // desbloquea el sistema
  public synchronized void desbloquea() {
    sinBloquear = true;
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // actualiza el flag de enfermo
  public void setFlagEnfermo(boolean bool) {
    bFlagEnfermo = bool;
  }

// recupera el flag de enfermo
//    (true si SIVE_USUARIO.IT_FG_ENFERMO = 'S')
//    (false en cualquier otro caso)
  public boolean getFlagEnfermo() {
    return bFlagEnfermo;
  }

// Restricci�n de confidencialidad
// si el usuario accede al modo modificaci�n y bFlagEnfermo = false:
// no puede ver el nombre / apellidos, se ver�n las iniciales
  public void Confidencialidad() {

    txtApe2.setVisible(false);
    txtNombre.setVisible(false);
    lApeNombre.setText(res.getString("lApeNombre.Text1"));
    txtApe1.setText(dataEnfermoBD.getSIGLAS());
    txtApe1.setVisible(true);
    txtApe1.setEnabled(false); //en Inicializar hay un refuerzo

    txtDocumento.setVisible(false);
    choTipoDocumento.setVisible(false);
    txtTelefono.setVisible(false);

    choSexo.setVisible(true);
    lDocumento.setVisible(false);
    lTipoDocumento.setVisible(false);
    lTelefono.setVisible(false);

    label8.setVisible(true);
    choSexo.setEnabled(false); //en Inicializar hay un refuerzo

  } //CONFIDENCIALIDAD********************************

  public void Visible(boolean bSi_No) {
    if (bSi_No) { //si (visible)
      txtApe2.setVisible(true);
      txtNombre.setVisible(true);
      txtApe1.setVisible(true);

      if (choTipoDocumento.getSelectedIndex() != 0
          && choTipoDocumento.getSelectedIndex() != -1) {
        txtDocumento.setVisible(true);
        lDocumento.setVisible(true);
      }
      //txtDocumento.setVisible (true);
      choTipoDocumento.setVisible(true);
      txtTelefono.setVisible(true);
      choSexo.setVisible(true);

    }
    else { //no (invisible)
      txtApe2.setVisible(false);
      txtNombre.setVisible(false);
      txtApe1.setVisible(false);

      txtDocumento.setVisible(false);
      lDocumento.setVisible(true);

      choTipoDocumento.setVisible(false);
      txtTelefono.setVisible(false);
      choSexo.setVisible(false);
    }
  } //fin de visible

//a�os o en meses
  private void RellenaChoEdad() {
    choEdad.addItem(res.getString("msg60.Text"));
    choEdad.addItem(res.getString("msg61.Text"));
  }

  // m�todo que rellena el panel con los datos que se propagan
  // del maestro detalle, en este caso:
  //      fecha de notificaci�n
  // adem�s, inicializa el panel actual con los siguientes datos:
  //    pa�s por defecto -> Espa�a (por ahora, c�digo 1)

  public void RellenaInicio() {

    //la que viene del panel Maestro  - de momento
    //Si se entra por ALTA******(en hashNotifEDO = fechas recep, notif)
    CfechaNotif.setText( (String) hashNotifEDO.get("FC_FECNOTIF"));

    // pa�s: por ahora, SIEMPRE ESPA�A
    /* suca
         Hashtable hash = new Hashtable();
         hash.put("CD_PAIS", "1");
         hash.put("DS_PAIS", "ESPA�A");
         CLista clista = new CLista();
         clista.addElement(hash);
         setListaPaises(clista);
         comun.writeChoice(getApp(), choPais, listaPaises, false, "DS_PAIS");
         if (choPais.getItemCount() > 0) {
      choPais.select(choPais.getItemCount() -1);
         }*/
  }

// m�todo que rellena los controles del panel con los datos
// de la estructura datatab ( a parti de SELECT*****)
//MODIFICACION-CONSULTA******
//BACKUP AL FINAL
  public void RellenaCabecera(DataTab datatab) {

    Hashtable hash;
    CLista clista;

    String sCD_TDOC = "", sDS_TDOC = "", sDS_NDOC = "";
    String sDS_APE2 = "", sDS_NOMBRE = "";
    String sCD_SEXO = "", sDS_SEXO = "";
    String sIT_CALC = "", sFC_NAC = "";
    String sCD_PROV_EDOIND = "", sDS_PROV_EDOIND = "", sCD_CA = "", sDS_CA = "";
    String sDS_TELEF = "", sCD_POSTAL_EDOIND = "";
    String sCD_MUN_EDOIND = "", sDS_MUN_EDOIND = "";
    String sCD_NIVEL_1_EDOIND = "", sDS_NIVEL_1_EDOIND = "";
    String sCD_NIVEL_2_EDOIND = "", sDS_NIVEL_2_EDOIND = "";
    String sCD_ZBS_EDOIND = "", sDS_ZBS_EDOIND = "";
    String sDS_CALLE = "", sDS_NMCALLE = "", sDS_PISO_EDOIND = "";
    String sFC_FECNOTIF = "";
    String sFC_RECEP = "";
    String sCD_ENFCIE = "";
    String sDS_PROCESO = "";
    String sCDVIAL = "";
    String sCDTVIA = "";
    String sCDTNUM = "";
    String sDSCALNUM = "";

    //cargamos la estructura de datos del enfermo
    dataEnfermoBD = new DataEnfermoBD(
        datatab.getCD_ENFERMO(), datatab.getCD_TDOC(),
        datatab.getDS_APE1(), datatab.getDS_APE2(),
        datatab.getDS_NOMBRE(), datatab.getDS_FONOAPE1(),
        datatab.getDS_FONOAPE2(), datatab.getDS_FONONOMBRE(),
        datatab.getIT_CALC(), datatab.getFC_NAC(),
        datatab.getCD_SEXO(), datatab.getDS_TELEF(),
        datatab.getCD_NIVEL_1(), datatab.getCD_NIVEL_2(),
        datatab.getCD_ZBS(), datatab.getCD_OPE(),
        datatab.getFC_ULTACT(), datatab.getDS_NDOC(),
        datatab.getSIGLAS());

    //cargamos el panel
    sFechaRecepcion = datatab.getFC_RECEP();
    sFechaNotificacion = datatab.getFC_FECNOTIF();

    CfechaNotif.setText(sFechaNotificacion);
    txtCodEnfermo.setText(datatab.getCD_ENFERMO());

    //enfermedad *****con set (PUEDE VENIR DE LA SELECT COMO NULL)
    if ( (datatab.getCD_ENFCIE()) != null) {
      sCD_ENFCIE = datatab.getCD_ENFCIE();

    }

    setEnfermedad(sCD_ENFCIE); //se encarga de ds_proceso

    //documento *****
    if ( (datatab.getCD_TDOC()) != null) {
      sCD_TDOC = datatab.getCD_TDOC();

    }
    setTipoDocumento(sCD_TDOC); //se encarga de iluminar el correspondiente ds

    //visible
    if (choTipoDocumento.getSelectedIndex() != 0
        && choTipoDocumento.getSelectedIndex() != -1) {
      txtDocumento.setVisible(true);
      lDocumento.setVisible(true);
    }
    else {
      txtDocumento.setVisible(false);
      lDocumento.setVisible(false);
    } //----------------------------------
    this.doLayout();

    if ( (datatab.getDS_NDOC()) != null) {
      sDS_NDOC = datatab.getDS_NDOC();
    }
    txtDocumento.setText(sDS_NDOC);

    //confidencialidad
    if (!getFlagEnfermo()) { //confid
      Visible(false);

    }

    txtApe1.setText(datatab.getDS_APE1());

    if ( (datatab.getDS_APE2()) != null) {
      sDS_APE2 = datatab.getDS_APE2();
    }
    txtApe2.setText(sDS_APE2);
    if ( (datatab.getDS_NOMBRE()) != null) {
      sDS_NOMBRE = datatab.getDS_NOMBRE();
    }
    txtNombre.setText(sDS_NOMBRE);

    // SEXO *****
    if ( (datatab.getCD_SEXO()) != null) {
      sCD_SEXO = datatab.getCD_SEXO();
    }
    setSexo(sCD_SEXO);

    //FECHA NACIMIENTO - it-calc
    if ( (datatab.getIT_CALC()) != null) {
      sIT_CALC = datatab.getIT_CALC();
      //fecha
    }
    if ( (datatab.getFC_NAC()) != null) {
      sFC_NAC = datatab.getFC_NAC();

    }
    txtFechaNac.setText(sFC_NAC);
    FechaNac_desde_BD(sIT_CALC);
    //---------------------------------

    // Tel�fono  ******
    if ( (datatab.getDS_TELEF()) != null) {
      sDS_TELEF = datatab.getDS_TELEF();

    }
    txtTelefono.setText(sDS_TELEF);

    // Rellenando datos al panel SUCA  -------------------------------------
    //panSuca.setModoDeshabilitado();

    // pa�s: por ahora, SIEMPRE ESPA�A
    // Comunidad Aut�noma
    if ( (datatab.getCD_CA_EDOIND()) != null) {
      sCD_CA = datatab.getCD_CA_EDOIND();
    }
    setCA(sCD_CA);

    // s�lo se rellena la lista de provincias si la Comunidad
    // Aut�noma se encuentra rellena
    if (!sCD_CA.equals("")) {

      if ( (datatab.getCD_PROV_EDOIND()) != null) {
        sCD_PROV_EDOIND = datatab.getCD_PROV_EDOIND();
      }
      setProvincia(sCD_PROV_EDOIND);
    }

    // C�digo de Municipio
    if ( (datatab.getCD_MUN_EDOIND()) != null) {
      sCD_MUN_EDOIND = datatab.getCD_MUN_EDOIND();
    }
    panSuca.setCD_MUN(sCD_MUN_EDOIND);

    // Descripci�n de Municipio
    if ( (datatab.getDS_MUN_EDOIND()) != null) {
      sDS_MUN_EDOIND = datatab.getDS_MUN_EDOIND();
    }
    panSuca.setDS_MUN(sDS_MUN_EDOIND);

    // c�digo de vial
    if ( (datatab.getCDVIAL()) != null) {
      sCDVIAL = datatab.getCDVIAL();
    }
    panSuca.setCDVIAL(sCDVIAL);

    // tipo de via
    if ( (datatab.getCDTVIA()) != null) {
      sCDTVIA = datatab.getCDTVIA();
    }
    panSuca.setCDTVIA(sCDTVIA);

    // tipo de numeraci�n
    if ( (datatab.getCDTNUM()) != null) {
      sCDTNUM = datatab.getCDTNUM();
    }
    panSuca.setCDTNUM(sCDTNUM);

    // tipo de calificador
    if ( (datatab.getDSCALNUM()) != null) {
      sDSCALNUM = datatab.getDSCALNUM();
    }
    panSuca.setDSCALNUM(sDSCALNUM);

    // C�digo Postal
    if ( (datatab.getCD_POSTAL_EDOIND()) != null) {
      sCD_POSTAL_EDOIND = datatab.getCD_POSTAL_EDOIND();
    }
    panSuca.setCD_POSTAL(sCD_POSTAL_EDOIND);

    // calle
    if (datatab.getDS_CALLE() != null) {
      sDS_CALLE = datatab.getDS_CALLE();
    }
    panSuca.setDS_DIREC(sDS_CALLE);

    // n�mero
    if (datatab.getDS_NMCALLE() != null) {
      sDS_NMCALLE = datatab.getDS_NMCALLE();
    }
    panSuca.setDS_NUM(sDS_NMCALLE);

    // piso
    if (datatab.getDS_PISO_EDOIND() != null) {
      sDS_PISO_EDOIND = datatab.getDS_PISO_EDOIND();
    }
    panSuca.setDS_PISO(sDS_PISO_EDOIND);

    //--------------------------------------------------------------------------

    // c�digo de nivel1
    if ( (datatab.getCD_NIVEL_1_EDOIND()) != null) {
      sCD_NIVEL_1_EDOIND = datatab.getCD_NIVEL_1_EDOIND();
    }
    txtCodNivel1.setText(sCD_NIVEL_1_EDOIND);
    if (!sCD_NIVEL_1_EDOIND.equals("")) {
      bNivel1Valid = true;
      bNivel1SelDePopup = true;
    }

    // descripci�n de nivel1
    if ( (datatab.getDS_NIVEL_1_EDOIND()) != null) {
      sDS_NIVEL_1_EDOIND = datatab.getDS_NIVEL_1_EDOIND();
    }
    txtDesNivel1.setText(sDS_NIVEL_1_EDOIND);

    // c�digo de nivel2
    if ( (datatab.getCD_NIVEL_2_EDOIND()) != null) {
      sCD_NIVEL_2_EDOIND = datatab.getCD_NIVEL_2_EDOIND();
    }
    txtCodNivel2.setText(sCD_NIVEL_2_EDOIND);
    if (!sCD_NIVEL_2_EDOIND.equals("")) {
      bNivel2Valid = true;
      bNivel2SelDePopup = true;
    }
    // descripci�n de nivel2
    if ( (datatab.getDS_NIVEL_2_EDOIND()) != null) {
      sDS_NIVEL_2_EDOIND = datatab.getDS_NIVEL_2_EDOIND();
    }
    txtDesNivel2.setText(sDS_NIVEL_2_EDOIND);

    // c�digo de ZBS
    if ( (datatab.getCD_ZBS_EDOIND()) != null) {
      sCD_ZBS_EDOIND = datatab.getCD_ZBS_EDOIND();
    }
    txtCodZBS.setText(sCD_ZBS_EDOIND);
    if (!sCD_ZBS_EDOIND.equals("")) {
      bNivelZBSValid = true;
      bNivelZBSSelDePopup = true;
    }
    // descripci�n de ZBS
    if ( (datatab.getDS_ZBS_EDOIND()) != null) {
      sDS_ZBS_EDOIND = datatab.getDS_ZBS_EDOIND();
    }
    txtDesZBS.setText(sDS_ZBS_EDOIND);

    //BACKUPs
    sEnfermedadBk = getCodEnfermedad(); //BACKUP
    sStringBk = datatab.getCD_ENFERMO(); //CodEnfermo Backup !!!!!!
    sN1Bk = sCD_NIVEL_1_EDOIND;
    sN2Bk = sCD_NIVEL_2_EDOIND;

    //confidencialidad -------------
    if (!getFlagEnfermo()) { //confid
      Confidencialidad();
    }
    else {
      Visible(true);
    } //---------------------------

  } //FIN RELLENA CABECERA  ****

  // recupera el c�digo del enfermo
  public String getCodEnfermo() {
    return (txtCodEnfermo.getText().trim());
  }

  // escribe el c�digo del enfermo
  public void setCodEnfermo(String sCod) {
    txtCodEnfermo.setText(sCod);
  }

  //cargamos los datos del enfermo que hay en la BD,
  //cuando hemos dado ALTA incluido el enfermo o al dar ALTA, modif enfermo
  public void Cargar_DataEnfermoBD(DataEnfermoBD datosEnfermodadoAlta) {
    dataEnfermoBD = (DataEnfermoBD) datosEnfermodadoAlta;
  }

  //PARA RECUPERAR EL DATAENFERMO DE LA BD
  public DataEnfermoBD getEnfermoBD() {

    return ( (DataEnfermoBD) dataEnfermoBD);
  } //fin de BD

  // recupera el apellido1
  public String getApe1() {
    return (txtApe1.getText().trim());
  }

  // recupera el apellido2
  public String getApe2() {
    return (txtApe2.getText().trim());
  }

  // recuepra el nombre
  public String getNombre() {
    return (txtNombre.getText().trim());
  }

  /* //ENFERMO CONFIDENVCIAL***********************************
   // recupera las iniciales (si el flag de enfermo est� a false)
   public String getSiglas() {
     if (!getFlagEnfermo())
         return (txtApe1.getText().trim());
     return "";
   }
   public String getAPE1() {
     if (!getFlagEnfermo())
         return ((String)hashNotifEDO.get("DS_APE1"));
     return "";
   }
   public String getAPE2() {
     if (!getFlagEnfermo())
         return ((String)hashNotifEDO.get("DS_APE2"));
     return "";
   }
   public String getNOMBRE() {
     if (!getFlagEnfermo())
          return ((String)hashNotifEDO.get("DS_NOMBRE"));
     return "";
   }
   public String getFONOAPE1() {
     if (!getFlagEnfermo())
         return ((String)hashNotifEDO.get("DS_FONOAPE1"));
     return "";
   }
   public String getFONOAPE2() {
     if (!getFlagEnfermo())
          return ((String)hashNotifEDO.get("DS_FONOAPE2"));
     return "";
   }
   public String getFONONOMBRE() {
     if (!getFlagEnfermo())
          return ((String)hashNotifEDO.get("DS_FONONOMBRE"));
     return "";
   }//*********************************************************
    // escribe las siglas
    public void setSiglas(String sSiglas) {
      if (getFlagEnfermo())
          txtApe1.setText(sSiglas);
    }   */

   // recupera la fecha de nacimiento del enfermo
   public String getFechaNac() {
     return (txtFechaNac.getText().trim());
   }

  // escribe la fecha de nacimiento del enfermo
  public void setFechaNac(String sFechaNac) {
    txtFechaNac.setText(sFechaNac);
  }

  //GET DE FECHAS PARA ESTADO MODIFICACION
  public String getFechaRecepcion() {
    return sFechaRecepcion;
  }

  public String getFechaNotificacion() {
    return sFechaNotificacion;
  }

  // recupera el c�digo de enfermedad correspondiente a la enfermedad
  // seleccionada en choEnfermedad
  // devuelve "" si la selecci�n no es v�lida
  public String getCodEnfermedad() {
    int indice = choEnfermedad.getSelectedIndex();

    if (indice > 0) {
      DataIndivEnfermedad dataEnf = (DataIndivEnfermedad) listaEnfermedad.
          elementAt(indice - 1);
      return dataEnf.getCD_ENFCIE();
    }

    return "";
  }

  //TVIGI
  public String getTVigiEnfermedad() {
    int indice = choEnfermedad.getSelectedIndex();
    if (indice > 0) {
      DataIndivEnfermedad dataEnf = (DataIndivEnfermedad) listaEnfermedad.
          elementAt(indice - 1);
      return dataEnf.getCD_TVIGI();
    }

    return "";

  } //TVIGI

  // recuepera la descripci�n de la enfermedad correspondiente a la
  // enfermedad que se ha seleccionado en choEnfermedad
  // devuelve "" si la selecci�n no es v�lida
  public String getDesEnfermedad() {
    int indice = choEnfermedad.getSelectedIndex();

    if (indice > 0) {
      DataIndivEnfermedad dataEnf = (DataIndivEnfermedad) listaEnfermedad.
          elementAt(indice - 1);
      return dataEnf.getDS_PROCESO();
    }
    return "";
  }

  // recupera el c�digo del documento correspondiente al documento
  // que se ha seleccionado en choTipoDocumento
  // devuelve "" si la selecci�n no es v�lida
  public String getCodTipoDocumento() {
    int indice = choTipoDocumento.getSelectedIndex();

    if (indice > 0) {
      return (String) ( (Hashtable) listaTipoDocumento.elementAt(indice - 1)).
          get("CD_TDOC");
    }
    return "";
  }

  // recupera la descripci�n del documento correspondiente al
  // documento que se ha seleccionado en choTipoDocumento
  // devuelve "" si la selecci�n no es v�lida
  public String getDesTipoDocumento() {
    int indice = choTipoDocumento.getSelectedIndex();

    if (indice > 0) {
      return (String) ( (Hashtable) listaTipoDocumento.elementAt(indice - 1)).
          get("DS_TDOC");
    }
    return "";
  }

  // recupera el c�digo del sexo correspondiente al sexo
  // que se ha seleccionado en choSexo
  // devuelve "" si la selecci�n no es v�lida
  public String getCodSexo() {
    int indice = choSexo.getSelectedIndex();

    if (indice > 0) {
      return (String) ( (Hashtable) listaSexo.elementAt(indice - 1)).get(
          "CD_SEXO");
    }
    return "";
  }

  // recupera la descripci�n del sexo correspondiente al sexo
  // que se ha seleccionado en choSexo
  // devuelve "" si la selecci�n no es v�lida
  public String getDesSexo() {
    int indice = choSexo.getSelectedIndex();

    if (indice > 0) {
      return (String) ( (Hashtable) listaSexo.elementAt(indice - 1)).get(
          "DS_SEXO");
    }
    return "";
  }

  // recupera el c�digo del pa�s correspondiente al pa�s
  // que se ha seleccionado en choPais
  // devuelve "" si la selecci�n no es v�lida
  public String getCodPais() {
    /* suca   int indice = choPais.getSelectedIndex();
      if (indice > 0) {
         return (String) ((Hashtable)listaPaises.elementAt(indice-1)).get("CD_PAIS");
      }
      return ""; */
    return panSuca.getCD_PAIS();
  }

  // recupera la descripci�n del pais correspondiente al pais
  // que se ha seleccionado en choPais
  // devuelve "" si la selecci�n no es v�lida

  /* suca
     public String getDesPais() {
    int indice = choPais.getSelectedIndex();
    if (indice > 0) {
       return (String) ((Hashtable)listaPaises.elementAt(indice-1)).get("DS_PAIS");
    }
    return "";
     }*/

  // recupera el c�digo de la CA
  // que se ha seleccionado en choCA
  // devuelve "" si la selecci�n no es v�lida
  public String getCodCA() {
    /* suca  int indice = choCA.getSelectedIndex();
      if (indice > 0) {
        return (String) ((Hashtable)listaCA.elementAt(indice-1)).get("CD_CA");
      }*/
    return panSuca.getCD_CA();
  }

  // recupera la descripci�n de la CA correspondiente a la
  // provincia que se ha seleccionado en choCA
  // devuelve "" si la selecci�n no es v�lida
  /* suca
     public String getDesCA() {
    int indice = choCA.getSelectedIndex();
    if (indice > 0) {
        return (String) ((Hashtable)listaCA.elementAt(indice-1)).get("DS_CA");
    }
    return "";
     } */

  // recupera el c�digo de la provincia correspondiente a la provincia
  // que se ha seleccionado en choProvincia
  // devuelve "" si la selecci�n no es v�lida
  public String getCodProvincia() {
    /* suca
      int indice = choProvincia.getSelectedIndex();
      if (indice > 0) {
         return (String) ((Hashtable)listaProvincias.elementAt(indice-1)).get("CD_PROV");
      } */
    return panSuca.getCD_PROV();
  }

  // recupera la descripci�n de la provincia correspondiente a la
  // provincia que se ha seleccionado en choProvincia
  // devuelve "" si la selecci�n no es v�lida
  /* suca
     public String getDesProvincia() {
    int indice = choProvincia.getSelectedIndex();
    if (indice > 0) {
       return (String) ((Hashtable)listaProvincias.elementAt(indice-1)).get("DS_PROV");
    }
    return "";
     } */

//CARGA CA CON UN DATO CONCRETO***************************

  public void setCA(String sCD_CA) {
    panSuca.setCD_CA(sCD_CA);
  } //CA********************

// escribe la provincia (c�digo + descripci�n) en choProvincia
  public void setProvincia(String sCD_PROV) {
    panSuca.setCD_PROV(sCD_PROV);
  } //provincias

  public void setSexo(String sCod) {
    Hashtable hash;

    // se comprueba que los datos se encuentren rellenos
    if (!sCod.equals("")) {

      if (getListaSexo() != null) {
        for (int i = 0; i < getListaSexo().size(); i++) {
          hash = (Hashtable) getListaSexo().elementAt(i);
          if ( ( (String) hash.get("CD_SEXO")).equals(sCod)) {
            choSexo.select(i + 1);
            break;
          }
        }
      }
    }
    else if ( (sCod.equals(""))) {
      choSexo.select(0);
    }

  } //SEXO

//TIPODOCUMENTO
  public void setTipoDocumento(String sCod) {
    Hashtable hash;

    // se comprueba que los datos se encuentren rellenos
    if (!sCod.equals("")) {

      if (getListaTipoDocumento() != null) {
        for (int i = 0; i < getListaTipoDocumento().size(); i++) {
          hash = (Hashtable) getListaTipoDocumento().elementAt(i);
          if ( ( (String) hash.get("CD_TDOC")).equals(sCod)) {
            choTipoDocumento.select(i + 1);
            txtDocumento.setVisible(true);
            lDocumento.setVisible(true);
            break;
          }
        }

      }
    }
    else if ( (sCod.equals(""))) {
      choTipoDocumento.select(0);
      txtDocumento.setVisible(false);
      lDocumento.setVisible(false);

    }
  }

//ENFERMEDAD ****** ***** ***** **** ****
  public void setEnfermedad(String sCod) {
    DataIndivEnfermedad dataE;

    // se comprueba que los datos se encuentren rellenos
    if (!sCod.equals("")) {

      if (getListaEnfermedad() != null) {
        for (int i = 0; i < getListaEnfermedad().size(); i++) {
          dataE = (DataIndivEnfermedad) getListaEnfermedad().elementAt(i);
          if (dataE.CD_ENFCIE.equals(sCod)) {
            choEnfermedad.select(i + 1);
            break;
          }
        } //for
      }
      //volcamos en la hashNotifEDO
      hashNotifEDO.put("DS_PROCESO", choEnfermedad.getSelectedItem());
    }
    else {
      hashNotifEDO.put("DS_PROCESO", "");
    }
  } //ENFERMEDAD

  /* funciones que devuelven los objetos globales */

  // recupera la CLista listaEnfermedad, que contiene la lista de los
  // c�digos y de las descripciones de las enfermedades individualizadas
  public CLista getListaEnfermedad() {
    synchronized (sincro) {
      return listaEnfermedad;
    }
  }

  // actualiza el contenido de listaEnfermedad con list
  public void setListaEnfermedad(CLista list) {
    synchronized (sincro) {
      listaEnfermedad = list;
    }
  }

  // recupera listaTipoDocumento
  public CLista getListaTipoDocumento() {
    synchronized (sincro) {
      return listaTipoDocumento;
    }
  }

  // actualiza listaTipoDocumento
  public void setListaTipoDocumento(CLista list) {
    synchronized (sincro) {
      listaTipoDocumento = list;
    }
  }

  // recupera listaSexo
  public CLista getListaSexo() {
    synchronized (sincro) {
      return listaSexo;
    }
  }

  // actualiza listaSexo
  public void setListaSexo(CLista list) {
    synchronized (sincro) {
      listaSexo = list;
    }
  }

  // recupera listaPaises
  public CLista getListaPaises() {
    synchronized (sincro) {
      return listaPaises;
    }
  }

  // actualiza listaPaises
  public void setListaPaises(CLista list) {
    synchronized (sincro) {
      listaPaises = list;
    }
  }

  // recupera listaCA
  public CLista getListaCA() {
    synchronized (sincro) {
      return listaCA;
    }
  }

  // actualiza listaCA
  public void setListaCA(CLista list) {
    synchronized (sincro) {
      listaCA = list;
    }
  }

  // recupera listaTpVial
  public CLista getListaTpVial() {
    synchronized (sincro) {
      return listaTpVial;
    }
  }

  // actualiza listaTpVial
  public void setListaTpVial(CLista list) {
    synchronized (sincro) {
      listaTpVial = list;
    }
  }

  // recupera listaTpNum
  public CLista getListaTpNum() {
    synchronized (sincro) {
      return listaTpNum;
    }
  }

  // actualiza listaTpNum
  public void setListaTpNum(CLista list) {
    synchronized (sincro) {
      listaTpNum = list;
    }
  }

  // recupera listaCalNum
  public CLista getListaCalNum() {
    synchronized (sincro) {
      return listaCalNum;
    }
  }

  // actualiza listaTpNum
  public void setListaCalNum(CLista list) {
    synchronized (sincro) {
      listaCalNum = list;
    }
  }

  // recuepera la lista de provincias
  public CLista getListaProvincias() {
    synchronized (sincro) {
      return listaProvincias;
    }
  }

  // actualiza la lista de provincias
  public void setListaProvincias(CLista list) {
    synchronized (sincro) {
      listaProvincias = list;
    }
  }

//COD LOSTFOCUS
  void txtCodEnfermo_focusLost() {

    String sStringDespues = txtCodEnfermo.getText();

    if (sStringBk.equals(sStringDespues)) {
      return;
    }

    //si difiere del anterior  ***************

    CMessage msgBox;

    //ahora VACIO--> el de antes
    if (txtCodEnfermo.getText().trim().equals("")) {
      int modo = modoOperacion;
      dlgIndiv.Inicializar(modoESPERA); //mlm nuevo
      Reset_Cabecera();
      modoOperacion = modo;
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

      bDeboLanzarEnfermo = true; //NOTA!! comenzar
      sStringBk = txtCodEnfermo.getText().trim();

      return;
    } //*********************

    //COMPROBAR NUMERICO ****
    boolean bEnf = true;
    try {
      Integer iCodEnfermo = new Integer(txtCodEnfermo.getText());
      if (iCodEnfermo.intValue() < 0) {
        bEnf = false;
      }
      if (iCodEnfermo.intValue() > 999999) {
        bEnf = false;
      }
    }
    catch (Exception e) {
      bEnf = false;
    }

    if (!bEnf) {
      msgBox = new CMessage(this.app,
                            CMessage.msgAVISO, res.getString("msg62.Text"));
      msgBox.show();
      msgBox = null;
      txtCodEnfermo.selectAll();
      txtCodEnfermo.setText(sStringBk);
      return;
    }
    //****************************

     int modo = modoOperacion; //lo guardo
    modoOperacion = modoESPERA; //general
    dlgIndiv.Inicializar(modoOperacion); //nuevo

    data = new CLista();
    DataEnfermo hash = new DataEnfermo("CD_ENFERMO");
    hash.put("CD_ENFERMO", getCodEnfermo());

    data.addElement(hash);

    try {
      data.setLogin(app.getLogin());
      data.setLortad(app.getParameter("LORTAD"));

      //segun tramero se llama a uno u otro
      if (app.getIT_TRAMERO().equals("N")) {
        listaEnfermo = Comunicador.Communicate(this.getApp(),
                                               stubCliente,
                                               modoDATOSENFERMO,
                                               strSERVLET_ENFERMO,
                                               data);
      }
      else {
        listaEnfermo = Comunicador.Communicate(this.getApp(),
                                               stubCliente,
                                               modoDATOSENFERMOTRAMERO,
                                               strSERVLET_ENFERMO,
                                               data);
      } //-------------------------------

      if (listaEnfermo != null) {
        //No existe el enfermo
        if (listaEnfermo.size() == 0) {
          ShowWarning(res.getString("msg63.Text"));
          //sin dato o sin permiso
          if (txtApe1.getText().trim().equals("")
              || !bDeboLanzarEnfermo) {
            txtCodEnfermo.setText(sStringBk);
            modoOperacion = modo;
            dlgIndiv.Inicializar(modoOperacion); //nuevo
            return;
          }
          if (!txtApe1.getText().trim().equals("")) {
            //Estoy en espera, con datos suficientes y con permiso de lanzar Automatismo
            int iBuscar = 0;
            iBuscar = Buscar_Enfermo(0);
            if (iBuscar == -1) { //No hay datos
              txtCodEnfermo.setText(sStringBk);
              modoOperacion = modo; //ALTA
              dlgIndiv.Inicializar(modoOperacion); //nuevo
              return;
            }
            else if (iBuscar == 0) { //cancelar
              txtCodEnfermo.setText(sStringBk);
              modoOperacion = modo; //ALTA
              dlgIndiv.Inicializar(modoOperacion); //nuevo
              return;
            }
            else if (iBuscar == 1) { //aceptar
              bDeboLanzarEnfermo = false;
              //que continue con a parte de caso
            }
          } //-------

        } //fin No existe enfermo
        //Existe el enfermo
        else {
          bDeboLanzarEnfermo = false;
          RellenaDatosEnfermo(listaEnfermo); //No se ha hecho backup del enfermo
        }

        //  protected int Buscar_Caso(){
        //----------------------------------------
        sStringDespues = txtCodEnfermo.getText(); //por si se vuelve a llenar
        if (choEnfermedad.getSelectedIndex() > 0) {
          int ibuscar = 0;
          ibuscar = Buscar_Caso();
          if (ibuscar == 0 || ibuscar == -1) {
            //paneles --- llaman a inicializar (ALTA (pCabecera))
            Paneles_txtCodEnfermo_focuslost(sStringBk, sStringDespues);

          }
          else if (ibuscar == 1) { //acepta caso
            //ya estan los paneles y en modificacion
          }

        } //if -----
        else { //sin enfermedad

          modoOperacion = modo;
          dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

        } //---------------------------------------

      }
      else if (listaEnfermo == null) { //no debe darse
        txtCodEnfermo.setText(sStringBk);
        modoOperacion = modo;
        dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
        return;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      txtCodEnfermo.setText(sStringBk);
      modoOperacion = modo;
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
    }

    //BACKUPS ****
    sStringBk = txtCodEnfermo.getText().trim();
    sEnfermedadBk = getCodEnfermedad();
    //en modificacion no se puede hacer lostfocus de cod enfermo, ptt no sN1Bk,sN2Bk

  } //FIN LOST COD ENFERMO

//PANELES --------------------
  protected void Paneles_Lupa(String sCodBk, String sCodDespues,
                              int iEnferAntes, int iEnferDespues) {

    //Si el enfermo estaba vacio, ahora lleno
    //y con enfermedad-->caso+proto
    if (sCodBk.equals("") &&
        !sCodDespues.equals("")) {

      if (choEnfermedad.getSelectedIndex() > 0) {
        dlgIndiv.Inicializar(modoESPERA);
        dlgIndiv.addCaso();
        dlgIndiv.Inicializar(modoESPERA);
        dlgIndiv.addProtocolo(); //ALTA (lupa activa solo en alta)
        dlgIndiv.Inicializar(modoALTA);
      }

      // modificacion jlt 13/09/01
      //else {
      //  dlgIndiv.Inicializar(modoALTA);
      //  enfermoborrado = true;
      //}
    }

    //si el enfermo estaba lleno, ahora lleno
    //si distintos--> cierro, si enefrmedad tb esta,  y abro
    //si iguales : enfer = , nada. enfer <>, cierro y si esta, abro

    if (!sCodBk.equals("") &&
        !sCodDespues.equals("")) {
      //distintos
      if (!sCodBk.equals(sCodDespues)) {
        if (dlgIndiv.pCaso != null) {
          dlgIndiv.borrarCaso();
        }
        if (dlgIndiv.pProtocolo != null) {
          dlgIndiv.borrarProtocolo();

        }
        if (choEnfermedad.getSelectedIndex() > 0) {
          dlgIndiv.Inicializar(modoESPERA);
          dlgIndiv.addCaso();
          dlgIndiv.Inicializar(modoESPERA);
          dlgIndiv.addProtocolo(); //ALTA  (lupa activa solo en alta)
          dlgIndiv.Inicializar(modoALTA);
        }
      }
      //iguales
      //si iguales : enfer = , nada. enfer <>, cierro y si esta, abro
      else {
        if (iEnferAntes != iEnferDespues) {
          if (dlgIndiv.pCaso != null) {
            dlgIndiv.borrarCaso();
          }
          if (dlgIndiv.pProtocolo != null) {
            dlgIndiv.borrarProtocolo();
          }
          if (choEnfermedad.getSelectedIndex() > 0) {
            dlgIndiv.Inicializar(modoESPERA);
            dlgIndiv.addCaso();
            dlgIndiv.Inicializar(modoESPERA);
            dlgIndiv.addProtocolo(); //ALTA  (lupa activa solo en alta)
            dlgIndiv.Inicializar(modoALTA);
          }
        }
      } //else
    }
    //-------------
  } //fin paneles_lupa

//PANELES---------------------------------------------
//Ya se llama sabiendo que txtCodEnfermo esta informado
  protected void Paneles_itemChanged(String sEnfdadAntes) {

    if (sEnfdadAntes.equals("")) { //primera vez
      dlgIndiv.Inicializar(modoESPERA);
      dlgIndiv.addCaso();
      dlgIndiv.Inicializar(modoESPERA);
      dlgIndiv.addProtocolo(); //ALTA
      dlgIndiv.Inicializar(modoALTA);
    }
    else { // habia enfermedad
      if (sEnfdadAntes.equals(getCodEnfermedad())) { //coinciden
        //nada
      }
      else if (!sEnfdadAntes.equals(getCodEnfermedad())) { //distinta enfermedad--> distinto caso y protocolo
        dlgIndiv.Inicializar(modoESPERA);
        dlgIndiv.borrarCaso();
        dlgIndiv.borrarProtocolo();
        dlgIndiv.addCaso();
        dlgIndiv.Inicializar(modoESPERA);
        dlgIndiv.addProtocolo(); //ALTA
        dlgIndiv.Inicializar(modoALTA);
      }
    } //fin de si primera - habia enfermedad

  } //fin paneles desde item

//CONTROL DE PANELES----------------------
  protected void Paneles_txtCodEnfermo_focuslost
      (String sCodBk, String sCodDespues) {

    //vacio--> lleno
    if (sCodBk.equals("") &&
        !sCodDespues.equals("")) {

      if (choEnfermedad.getSelectedIndex() > 0) {
        dlgIndiv.Inicializar(modoESPERA);
        dlgIndiv.addCaso();
        dlgIndiv.Inicializar(modoESPERA);
        dlgIndiv.addProtocolo(); //ALTA
        dlgIndiv.Inicializar(modoALTA);

      }

    } //fin
    //lleno--> lleno y diferentes
    if (!sCodBk.equals(sCodDespues) &&
        !sCodBk.equals("") &&
        !sCodDespues.equals("")) {
      if (dlgIndiv.pCaso != null) {
        dlgIndiv.borrarCaso();
      }
      if (dlgIndiv.pProtocolo != null) {
        dlgIndiv.borrarProtocolo();

      }
      if (choEnfermedad.getSelectedIndex() > 0) {
        dlgIndiv.Inicializar(modoESPERA);
        dlgIndiv.addCaso();
        dlgIndiv.Inicializar(modoESPERA);
        dlgIndiv.addProtocolo(); //ALTA
        dlgIndiv.Inicializar(modoALTA);
      }

    } //fin de enfermo distintos -informados
    //----------------------------------------

  } //fin funcion

// m�todo que se ejecuta en respuesta al focuslost en txtApe1
//MLMORA ?????? por que ???
  void txApe1_focusLost() {
    if (txtApe1.getText().trim().equals("")) {
      btnEnfermo.setEnabled(false);
    }
    else {
      btnEnfermo.setEnabled(true);
    }
  }

//FECHA NAC LOST FOCUS
  public void txtFechaNac_focusLost() {
    // ARS 04-06-01  Como resulta que el ValidarFecha, devuelve S
    // tanto si hay fecha correcta, como si no existe fecha, pues
    // si no metes fecha y pierdes el foco, salta todo.
    // P-al caso, que hay que comprobar previamente si hay o no algo
    // escrito.
    boolean hayFecha = false;

    if (txtFechaNac.getText().trim().length() > 0) {
      hayFecha = true;

    }
    if (hayFecha) {

      txtFechaNac.ValidarFecha(); //validamos

      //MAntengo, aunque sean iguales-----
      if ( (txtFechaNac.getText().trim().length() > 0) &&
          (txtFechaNac.getValid().equals("S"))) { //fecha ok, llena
        txtFechaNac.setText(txtFechaNac.getFecha()); //Cargo

        //fec nac <= dia Hoy
        int iLimite = 0;
        iLimite = UtilEDO.Compara_Fechas(UtilEDO.getFechaAct(),
                                         txtFechaNac.getText().trim());
        if (iLimite == 2) { //sin sentido!!
          txtFechaNac.setText("");
          //------------------
          //de cualquier modo
        }
        FechaNac_desde_BD("N"); //la metemos real
      }
      else { //ko, vacio
        // ARG: Se avisa de que la fecha no es valida
        CMessage msgBox = new CMessage(app, CMessage.msgERROR,
                                       res.getString("msg104.Text"));
        msgBox.show();
        msgBox = null;
        txtFechaNac.setText(txtFechaNac.getFecha()); //Cargo, vacio

        FechaNac_desde_BD("N"); //la metemos real
      } //------------------------------------

      //AUTOMATISMO ENFERMO-CASO   ---------------------

      int modo = modoOperacion;

      //sin dato o sin permiso
      if (!bDeboLanzarEnfermo
          || txtFechaNac.getText().trim().equals("")
          || txtApe1.getText().trim().equals("")) {
        return;
      }
      //con datos suficientes y con permiso de lanzar Automatismo
      int iBuscar = 0;

      iBuscar = Buscar_Enfermo(3);

      if (iBuscar == -1 || iBuscar == 0) { //No hay datos-cancelar
        return;
      }
      else if (iBuscar == 1) { //aceptar
        bDeboLanzarEnfermo = false;
        //se habra pintado el enfermo
        //que continue con a parte de caso
        //  protected int Buscar_Caso(){
        //----------------------------------------
        String sStringDespues = txtCodEnfermo.getText(); //por si se vuelve a llenar
        if (choEnfermedad.getSelectedIndex() > 0) {
          int ibuscar = 0;
          ibuscar = Buscar_Caso();
          if (ibuscar == 0 || ibuscar == -1) {
            //paneles --- llaman a inicializar (ALTA (pCabecera))
            Paneles_txtCodEnfermo_focuslost(sStringBk, sStringDespues);
          }
          else if (ibuscar == 1) { //acepta caso
            //ya estan los paneles y en modificacion
          }

        } //if -----
        else { //sin enfermedad
          modoOperacion = modo;
          dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
        } //---------------------------------------

        //BACKUPS ****
        sStringBk = txtCodEnfermo.getText();
        sEnfermedadBk = getCodEnfermedad();
        //Busqueda, solo en alta

      } //ACEPTAR
    } // Fin del no hay fecha

  } //fin txtlost

  public void FechaNac_desde_BD(String sIT_CALC) {
    //si esta llena (esta validada):
    //label segun el it
    //calculo la edad --> num, a/m
    //edad, cmbo
    if (!txtFechaNac.getText().equals("")) {

      //label
      if (sIT_CALC.equals("S")) { //fue calculada
        lTipoFechaNac.setText(res.getString("lTipoFechaNac.Text"));
      }
      if (sIT_CALC.equals("N")) { //fue metida real
        lTipoFechaNac.setText(res.getString("lTipoFechaNac.Text1"));
      } //-------

      //edad-----
      java.util.Date dFecha = Fechas.string2Date(txtFechaNac.getText()); //date
      //AIC
      java.util.Date dFechaNotificacion = string2DateCab(CfechaNotif.getText());
      //ShowWarning("Error al transformar la fecha: " + CfechaNotif.getText());

      if (Fechas.edadTipo(dFecha)) { //true-->A�OS
        choEdad.select(0);
        //AIC
        //txtEdad.setText( (new Integer(Fechas.edadAnios(dFecha))).toString()  );
        txtEdad.setText( (new Integer(edadAniosConRef(dFecha,
            dFechaNotificacion))).toString());
      }
      if (!Fechas.edadTipo(dFecha)) { //false-->MESES
        choEdad.select(1);
        //AIC
        //txtEdad.setText( (new Integer(Fechas.edadMeses(dFecha))).toString());
        txtEdad.setText( (new Integer(edadMesesConRef(dFecha,
            dFechaNotificacion))).toString());

      } //----------

    } //fin no vacio

    //si vacia
    //Label oculta, cmbo a�os, edad vacio
    if (txtFechaNac.getText().equals("")) {
      lTipoFechaNac.setText("");
      choEdad.select(0);
      txtEdad.setText("");
    }

  } //fin funcion

//adaptacion de las funciones de Fechas
  /**
   *  esta funcion formatea una fecha  a cadena
   *
   * @param a_date es la fecha a formatear
   * @return es la cadena de textocon el formato formatter
   */
  private String date2StringCab(java.util.Date a_date) {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    if (a_date == null) {
      return null;
    }

    try {
      return formatter.format(a_date);
    }
    catch (Exception exc) {
      return "";
    }

  }

  /**
   *  esta funcion formatea una fecha
   *
   * @param a_String es la cadena de texto a traducir a fecha
   * @return es la fecha que describ�a la cadena
   */
  private java.util.Date string2DateCab(String a_String) {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    if (a_String == null || a_String.trim().length() == 0) {
      return null;
    }

    try {
      ParsePosition pos = new ParsePosition(0);
      return formatter.parse(a_String, pos);
    }
    catch (Exception exc) {
      return null;
    }
  }

//adaptacion de las funciones de Fechas

//focuslost en txtEdad
  public void txtEdad_focusLost() {

    if (txtEdad.getText().equals("")) {
      //choEdad.select(0); //a�os
      return;
    }
    //edades backup
    //ACTUAL
    int valor = Integer.parseInt(txtEdad.getText());
    int tipoEdad = choEdad.getSelectedIndex();

    int valorBk = 0;
    int tipoEdadBk = 0;

    //calcular la edad a partir de la fecha
    //si coinciden, return   No coinciden, calcular la fecha y cambiar

    //calcular la edad a partir de la fecha
    //si coinciden, return   No coinciden, calcular la fecha y cambiar
    java.util.Date dFecha = string2DateCab(txtFechaNac.getText()); //date
    //AIC
    java.util.Date dFechaNotificacion = string2DateCab(CfechaNotif.getText());
    //ShowWarning("Error al transformar la fecha: " + CfechaNotif.getText());
    if (Fechas.edadTipo(dFecha)) { //true-->A�OS

      //AIC
      //valorBk = Fechas.edadAnios(dFecha);
      valorBk = edadAniosConRef(dFecha, dFechaNotificacion);
      tipoEdadBk = 0;
    }
    if (!Fechas.edadTipo(dFecha)) { //false-->MESES

      //AIC
      //valorBk = Fechas.edadMeses(dFecha);
      valorBk = edadMesesConRef(dFecha, dFechaNotificacion);

      tipoEdadBk = 1;
    } //----------
    if ( (valor == valorBk) &&
        (tipoEdad == tipoEdadBk)) { //IGUALES, es un lostfocus sin cambio
      return;
    }
    if ( (valor != valorBk) ||
        (tipoEdad != tipoEdadBk)) { //LOSTFOCUS VERDADERO  --> cambiar fecha y calc
      //a�os****
      if (choEdad.getSelectedIndex() == 0) {
        //calcualar la fecha  despues
        //AIC
        //java.util.Date result = Fechas.restaTiempo( valor ,false, new java.util.Date());
        // modificacion jlt 03/09/01
        // se calcula la fecha de nacimiento a partir de la
        // fecha de notificaci�n pero con el dia 15
        //java.util.Date result = Fechas.restaTiempo2( valor ,false, dFechaNotificacion);
        java.util.Date result = Fechas.restaTiempo(valor, false,
            dFechaNotificacion);
        ///////////////

        txtFechaNac.setText(date2StringCab(result));
        lTipoFechaNac.setText(res.getString("lTipoFechaNac.Text"));

      }
      //meses
      if (choEdad.getSelectedIndex() == 1) {
        //calcualar la fecha  despues
        //AIC
        //java.util.Date result = Fechas.restaTiempo( valor ,true, new java.util.Date());
        // modificacion jlt 03/09/01
        // se calcula la fecha de nacimiento a partir de la
        // fecha de notificaci�n pero con el dia 15
        //java.util.Date result = Fechas.restaTiempo2( valor ,true, dFechaNotificacion);
        java.util.Date result = Fechas.restaTiempo(valor, true,
            dFechaNotificacion);
        ///////////////

        txtFechaNac.setText(date2StringCab(result));
        lTipoFechaNac.setText(res.getString("lTipoFechaNac.Text"));

      }
    }

    //AUTOMATISMO ENFERMO-CASO   ---------------------

    int modo = modoOperacion;
    //sin dato o sin permiso
    if (!bDeboLanzarEnfermo
        || txtFechaNac.getText().trim().equals("")
        || txtApe1.getText().trim().equals("")) {
      return;
    }
    //con datos suficientes y con permiso de lanzar Automatismo
    int iBuscar = 0;
    iBuscar = Buscar_Enfermo(3);
    if (iBuscar == -1 || iBuscar == 0) { //No hay datos-cancelar
      return;
    }
    else if (iBuscar == 1) { //aceptar
      bDeboLanzarEnfermo = false;
      //se habra pintado el enfermo
      //que continue con a parte de caso
      //  protected int Buscar_Caso(){
      //----------------------------------------
      String sStringDespues = txtCodEnfermo.getText(); //por si se vuelve a llenar
      if (choEnfermedad.getSelectedIndex() > 0) {
        int ibuscar = 0;
        ibuscar = Buscar_Caso();
        if (ibuscar == 0 || ibuscar == -1) {
          //paneles --- llaman a inicializar (ALTA (pCabecera))
          Paneles_txtCodEnfermo_focuslost(sStringBk, sStringDespues);
        }
        else if (ibuscar == 1) { //acepta caso
          //ya estan los paneles y en modificacion
        }

      } //if -----
      else { //sin enfermedad
        modoOperacion = modo;
        dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
      } //---------------------------------------

      //BACKUPS ****
      sStringBk = txtCodEnfermo.getText();
      sEnfermedadBk = getCodEnfermedad();
      //busqueda solo en alta

    } //ACEPTAR

  } //fin focuslost en txtEdad

//ITEMCHANGED ****  ***** ***** ******
  void choEnfermedad_itemStateChanged() {

    int index = choEnfermedad.getSelectedIndex();

    //ITEMCHANGED SE usa solo DESDE ALTA!!!
    // a�adir la solapa de protocolo-caso
    if (index > 0) {

      if (!txtCodEnfermo.getText().equals("")) {
        //------------------------------------
        int ibuscar = 0;
        ibuscar = Buscar_Caso();
        if (ibuscar == 0 || ibuscar == -1) {

          // modificacion jlt
          //if (enfermoborrado)  {
          //enfermoborrado = false;
          //    dlgIndiv.Inicializar(modoALTA);
          //} else {
          Paneles_itemChanged(sEnfermedadBk); //inicializa  ALTA
          //}
        }
        else if (ibuscar == 1) { //acepta caso
          //ya estan los paneles y en modificacion
        }
        //----------------------------------

      } //enfermo informado!!!
    }
    else { //indice cero
      dlgIndiv.borrarCaso();
      dlgIndiv.borrarProtocolo();
    }
    sEnfermedadBk = getCodEnfermedad(); //BACKUP

  } //itemchanged******

  public void choTipoDocumento_itemStateChanged() {
    txtDocumento.setText("");
    if (choTipoDocumento.getSelectedIndex() > 0) {
      txtDocumento.setVisible(true);
      lDocumento.setVisible(true);
      validate();
    }
    else {
      txtDocumento.setVisible(false);
      lDocumento.setVisible(false);
    }
  }

  public void choEdad_itemStateChanged() {
    //Borro la edad (NADA MAS: label con su valor)
    //txtEdad.setText("");
    //jm hace perdida de foco(HACER)
    txtEdad_focusLost();

  } //fin edad****

//ENFERMO!!LUPA!!!!!!
//!!!!!!!!!!!!!!!!!!!
  void btnLupaEnfermo_actionPerformed() {

    int iEnferAntes = choEnfermedad.getSelectedIndex();

    //guardo el modo
    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    // ARG: dependiendo de IT_TRAMERO se llama a un constructor o a otro.
    DialBusEnfermo dlgEnfermo;
    if (bTramero == true) {

      // ARG: Se le pasa panSuca para poder obtener la lista de CA
      dlgEnfermo = new DialBusEnfermo(this.getApp(), panSuca);
    }
    else {
      dlgEnfermo = new DialBusEnfermo(this.getApp());

    }
    dlgEnfermo.show();

    CLista listaEnfermo = dlgEnfermo.getListaDatosEnfermo();
    // se piden los datos y rellenamos la tabla
    if (listaEnfermo != null) {

      if (listaEnfermo.size() != 0) {

        RellenaDatosEnfermo(listaEnfermo);
        //NOS TRAEMOS ENFERMO Y ENFERMEDAD

        String sStringDespues = txtCodEnfermo.getText();
        int iEnferDespues = choEnfermedad.getSelectedIndex();
        //-------------------------
        int ibuscar = 0;
        ibuscar = Buscar_Caso();
        if (ibuscar == 0 || ibuscar == -1) {
          //paneles --- llaman a inicializar (ALTA (pCabecera))
          Paneles_Lupa(sStringBk, sStringDespues,
                       iEnferAntes, iEnferDespues);
        }
        else if (ibuscar == 1) { //acepta caso
          //ya estan los paneles y en modificacion
        }
        //------------------------

      } //size
      else { //cancelar  (size=0)
        //No se ha elegido nada
        modoOperacion = modo;
        dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
      }
    } //null
    else { //(=null)
      modoOperacion = modo;
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
    }

    sStringBk = txtCodEnfermo.getText(); //ACTUALIZO BACKUP de enfermo
    sEnfermedadBk = getCodEnfermedad(); //BACKUP
    //en modif no hay lupa de enfermo. No procede sN1bk..

  } //FIN DE LUPA

// rellena todos los controles que corresponden a datos del enfermo,
// a partir de la CLista listaEnfermo
//NO LLEVA BACKUP, va en lostfocus y en lupa
  void RellenaDatosEnfermo(CLista listaEnfermo) {

    Hashtable hash = null;
    String dato = null;

    //desde el btn, sabemos llega lista con size1
    if (listaEnfermo != null) {
      hash = (Hashtable) listaEnfermo.firstElement();
    }
    if (hash == null) {
      return;
    } //*******************************************

    //TENEMOS LOS DATOS

    //confidencialidad ????
    if (!getFlagEnfermo()) { //confid
      Visible(false);

    }
    dato = (String) hash.get("DS_APE1");
    if (dato != null) {
      txtApe1.setText(dato);
    }
    else {
      txtApe1.setText("");

    }
    dato = (String) hash.get("DS_APE2");
    if (dato != null) {
      txtApe2.setText(dato);
    }
    else {
      txtApe2.setText("");

    }
    dato = (String) hash.get("DS_NOMBRE");
    if (dato != null) {
      txtNombre.setText(dato);
    }
    else {
      txtNombre.setText("");

    }
    dato = (hash.get("CD_ENFERMO")).toString();
    if (dato != null) {
      txtCodEnfermo.setText(dato);
    }
    else {
      txtCodEnfermo.setText("");

    }
    String sApe1 = "", sApe2 = "", sNombre = "";
    String sFonoApe1 = "", sFonoApe2 = "", sFonoNombre = "";

    sApe1 = (String) hash.get("DS_APE1");
    sApe2 = (String) hash.get("DS_APE2");
    sNombre = (String) hash.get("DS_NOMBRE");
    if (sApe2 == null) {
      sApe2 = ""; //COMUN no admite null
    }
    if (sNombre == null) {
      sNombre = "";

    }
    sFonoApe1 = comun.traduccionFonetica(sApe1);
    sFonoApe2 = comun.traduccionFonetica(sApe2);
    sFonoNombre = comun.traduccionFonetica(sNombre);

    dato = (String) hash.get("DS_TELEF");
    if (dato != null) {
      txtTelefono.setText(dato);
    }
    else {
      txtTelefono.setText("");
      //-----------------------------------------

      //FECHA NAC Y EDAD
    }
    String sIT_CALC = (String) hash.get("IT_CALC");
    String sFC_NAC = "";

    if ( (String) hash.get("FC_NAC") != null) {
      sFC_NAC = (String) hash.get("FC_NAC");
      sFC_NAC = sFC_NAC.trim();
      //controla el vacio
      txtFechaNac.setText(sFC_NAC);
      FechaNac_desde_BD(sIT_CALC); //no debe ser null IT_CALC
    }
    else { //resetear cajas
      txtFechaNac.setText("");
      FechaNac_desde_BD("");
    }
    //fin fecha--------

    // ponemos los choices
    //Hashtable hash;
    CLista clista;

    // enfermedad en set VIENE EN HASH (Jose Miguel)
    String sCodEnfer = "", sDesEnfer = "";
    if ( (String) hash.get("CD_ENFCIE") != null) {
      sCodEnfer = (String) hash.get("CD_ENFCIE");

    }
    setEnfermedad(sCodEnfer);
    //----------------------------------------------

    // tipo de documento
    String sCodTipoDoc = "", sDesTipoDoc = "";
    if ( (String) hash.get("CD_TDOC") != null) {
      sCodTipoDoc = (String) hash.get("CD_TDOC");
    }
    setTipoDocumento(sCodTipoDoc);

    if (choTipoDocumento.getSelectedIndex() != 0
        && choTipoDocumento.getSelectedIndex() != -1) {
      txtDocumento.setVisible(true);
      lDocumento.setVisible(true);

    }
    else {
      txtDocumento.setVisible(false);
      lDocumento.setVisible(false);

    } //----------------

    dato = (String) hash.get("DS_NDOC");
    if (dato != null) {
      txtDocumento.setText(dato);
    }
    else {
      txtDocumento.setText("");

      // sexo
    }
    String sCodSexo = "";
    if ( (String) hash.get("CD_SEXO") != null) {
      sCodSexo = (String) hash.get("CD_SEXO");
    }
    setSexo(sCodSexo);

    //SUCA -----------------------------------------------------------
    //panSuca.setModoDeshabilitado();

    // el pa�s es Espa�a
    // Comunidad Aut�noma
    String sCodCA = "";
    if ( (String) hash.get("CD_CA") != null) {
      sCodCA = (String) hash.get("CD_CA");
    }
    setCA(sCodCA);

    //prov
    if (!sCodCA.equals("")) {
      String sCodProv = "";
      if ( (String) hash.get("CD_PROV") != null) {
        sCodProv = (String) hash.get("CD_PROV");
      }
      setProvincia(sCodProv);
    }
    // el municipio
    dato = (String) hash.get("CD_MUN");
    if (dato != null) {
      panSuca.setCD_MUN(dato);
    }
    else {
      panSuca.setCD_MUN("");
    }
    dato = (String) hash.get("DS_MUN");
    if (dato != null) {
      panSuca.setDS_MUN(dato);
    }
    else {
      panSuca.setDS_MUN("");

      // los 4 nuevos
    }
    dato = (String) hash.get("CDTVIA");
    if (dato != null) {
      panSuca.setCDTVIA(dato);
    }
    else {
      panSuca.setCDTVIA("");

    }
    dato = (String) hash.get("CDVIAL");
    if (dato != null) {
      panSuca.setCDVIAL(dato);
    }
    else {
      panSuca.setCDVIAL("");

    }
    dato = (String) hash.get("CDTNUM");
    if (dato != null) {
      panSuca.setCDTNUM(dato);
    }
    else {
      panSuca.setCDTNUM("");

    }
    dato = (String) hash.get("DSCALNUM");
    if (dato != null) {
      panSuca.setDSCALNUM(dato);
    }
    else {
      panSuca.setDSCALNUM("");
      //los 4 ***

    }
    dato = (String) hash.get("DS_DIREC");
    if (dato != null) {
      panSuca.setDS_DIREC(dato);
    }
    else {
      panSuca.setDS_DIREC("");

    }
    dato = (String) hash.get("DS_NUM");
    if (dato != null) {
      panSuca.setDS_NUM(dato);
    }
    else {
      panSuca.setDS_NUM("");

    }
    dato = (String) hash.get("DS_PISO");
    if (dato != null) {
      panSuca.setDS_PISO(dato);
    }
    else {
      panSuca.setDS_PISO("");

    }
    dato = (String) hash.get("CD_POSTAL");
    if (dato != null) {
      panSuca.setCD_POSTAL(dato);
    }
    else {
      panSuca.setCD_POSTAL(dato);

      //niveles zonas ----

      //n1 y popup
    }
    dato = (String) hash.get("CD_NIVEL_1");
    if (dato != null) {
      txtCodNivel1.setText(dato);
      bNivel1Valid = true;
      bNivel1SelDePopup = true;
    }
    else {
      txtCodNivel1.setText("");
      //desc
    }
    dato = (String) hash.get("DS_NIVEL_1");
    if (dato != null) {
      txtDesNivel1.setText(dato);
    }
    else {
      txtDesNivel1.setText("");
      //---------

      //n2 y popup
    }
    dato = (String) hash.get("CD_NIVEL_2");
    if (dato != null) {
      txtCodNivel2.setText(dato);
      bNivel2Valid = true;
      bNivel2SelDePopup = true;
    }
    else {
      txtCodNivel2.setText("");
      //desc
    }
    dato = (String) hash.get("DS_NIVEL_2");
    if (dato != null) {
      txtDesNivel2.setText(dato);
    }
    else {
      txtDesNivel2.setText("");
      //---------

      //zbs y popup
    }
    dato = (String) hash.get("CD_ZBS");
    if (dato != null) {
      txtCodZBS.setText(dato);
      bNivelZBSValid = true;
      bNivelZBSSelDePopup = true;
    }
    else {
      txtCodZBS.setText("");
      //desc
    }
    dato = (String) hash.get("DS_ZBS");
    if (dato != null) {
      txtDesZBS.setText(dato);
    }
    else {
      txtDesZBS.setText("");
      //---------

    }

    this.doLayout();

    //cargamos la estructura de datos del enfermo
    String sFC_ULTACT = (String) hash.get("FC_ULTACT");

    dataEnfermoBD = new DataEnfermoBD(
        (hash.get("CD_ENFERMO")).toString(),
        (String) hash.get("CD_TDOC"),
        sApe1,
        sApe2,
        sNombre,
        sFonoApe1,
        sFonoApe2,
        sFonoNombre,
        sIT_CALC,
        sFC_NAC,
        (String) hash.get("CD_SEXO"),
        (String) hash.get("DS_TELEF"),
        (String) hash.get("CD_NIVEL_1"),
        (String) hash.get("CD_NIVEL_2"),
        (String) hash.get("CD_ZBS"),
        (String) hash.get("CD_OPE"),
        sFC_ULTACT,
        (String) hash.get("DS_NDOC"),
        (String) hash.get("SIGLAS"));
    //FIN DE GARGAR

    //confidencialidad -----------
    if (!getFlagEnfermo()) { //confid
      Confidencialidad();
    }
    else {
      Visible(true);
    } //---------------------------

  } //FIN RELLENADATOSENFERMO

  // se ejecuta en respuesta al actionPerformed en btnNivel1
  void btnNivel1_actionPerformed() {

    DataEntradaEDO data;

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    CListaNiveles1 lista = new CListaNiveles1(dlgIndiv.getCApp(),
                                              res.getString("msg65.Text"),
                                              stubCliente,
                                              strSERVLET,
        servletOBTENER_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ,
        servletOBTENER_TODOS_NIV1_X_DESCRIPCION_TODAS_AUTORIZ,
        servletSELECCION_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ,
        servletSELECCION_TODOS_NIV1_X_DESCRIPCION_TODAS_AUTORIZ);

    lista.show();
    data = (DataEntradaEDO) lista.getComponente();

    if (data != null) {
      txtCodNivel1.setText(data.getCod());
      txtDesNivel1.setText(data.getDes());
      txtCodNivel2.setText("");
      txtDesNivel2.setText("");
      txtCodZBS.setText("");
      txtDesZBS.setText("");
      bNivel1SelDePopup = true;
    }

    modoOperacion = modo;
    dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    if (bNivel1SelDePopup) {
      bNivel1Valid = true;
      Parche_Protocolo();
    }
  }

  // se ejecuta en respuesta al actionPerformed en btnNivel2
  void btnNivel2_actionPerformed() {
    DataEntradaEDO data;

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    // Se obliga a que antes se haya seleccionado un nivel1
    //if (bNivel1SelDePopup) {
    CListaNiveles2Indiv lista = new CListaNiveles2Indiv(this,
        res.getString("msg65.Text"),
        dlgIndiv.stubCliente,
        strSERVLET,
        servletOBTENER_NIV2_X_CODIGO_TODAS_AUTORIZ,
        servletOBTENER_NIV2_X_DESCRIPCION_TODAS_AUTORIZ,
        servletSELECCION_NIV2_X_CODIGO_TODAS_AUTORIZ,
        servletSELECCION_NIV2_X_DESCRIPCION_TODAS_AUTORIZ);
    lista.show();
    data = (DataEntradaEDO) lista.getComponente();

    if (data != null) {
      txtCodNivel2.setText(data.getCod());
      txtDesNivel2.setText(data.getDes());
      txtCodZBS.setText("");
      txtDesZBS.setText("");

      bNivel2SelDePopup = true;
    }

    modoOperacion = modo;
    dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    if (bNivel2SelDePopup) {
      bNivel2Valid = true;
      Parche_Protocolo();
    }
  }

  // se ejecuta en respuesta al actionPerformed en btnZBS
  void btnZBS_actionPerformed() {
    DataEntradaEDO data;

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    // Se obliga a que antes se haya seleccionado un nivel1
    //if (bNivel1SelDePopup) {
    CListaNiveles2Indiv lista = new CListaNiveles2Indiv(this,
        res.getString("msg65.Text"),
        dlgIndiv.stubCliente,
        strSERVLET,
        servletOBTENER_ZBS_X_CODIGO,
        servletOBTENER_ZBS_X_DESCRIPCION,
        servletSELECCION_ZBS_X_CODIGO,
        servletSELECCION_ZBS_X_DESCRIPCION);
    lista.show();
    data = (DataEntradaEDO) lista.getComponente();

    if (data != null) {
      txtCodZBS.setText(data.getCod());
      txtDesZBS.setText(data.getDes());

      bNivelZBSSelDePopup = true;
    }

    modoOperacion = modo;
    dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    if (bNivelZBSSelDePopup) {
      bNivelZBSValid = true;
    }
  }

//ENFERMO*****  BOTON GRANDE *****
  public void btnEnfermo_actionPerformed() {

    int modo = modoOperacion;
    int iBuscar = 0;

    //Al menos que este Ape1 informado???-> salen todos los enfermos
    int iDatosSuficientes = 0;
    iDatosSuficientes = Ape1_Y_QuizasOtros();
    if (iDatosSuficientes == 1 || iDatosSuficientes == 2) {
      //continuamos
    }
    else {
      ShowWarning(res.getString("msg66.Text"));
      return;
    }
    //-------------------------------------------------------------

    //Estudio de Autorizaciones-> de momento false (TODO) DENTRO DE BUSCAR

    iBuscar = Buscar_Enfermo(0);
    if (iBuscar == -1) { //No hay datos
      ShowWarning(res.getString("msg67.Text"));
      modoOperacion = modo; //ALTA
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
    }
    else if (iBuscar == 0) { //cancelar
      modoOperacion = modo; //ALTA
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
    }
    else if (iBuscar == 1) { //aceptar
      //caso-paneles
      String sStringDespues = txtCodEnfermo.getText();

      //0: sale por cancelar ALTA
      //-1: no hay datos, sacar mensaje si se hace desde boton caso ALTA
      //1: aceptar y con datos
      //llama a rellenadatos, y abre los paneles MODIFICAR
      //  protected int Buscar_Caso(){
      //----------------------------------------
      //hasta aqui tenemos el cd_enfermo
      if (choEnfermedad.getSelectedIndex() > 0) {
        int ibuscar = 0;
        ibuscar = Buscar_Caso();
        //repite busqueda y trae de nuevo datos de enfermo
        if (ibuscar == 0 || ibuscar == -1) {
          //paneles detras va a modo ALTA
          Paneles_txtCodEnfermo_focuslost(sStringBk, sStringDespues);
        }
        else if (ibuscar == 1) { //pasar a MODIFICAR
          //ya estan los paneles y en modificacion
        }
      } //if ------------------------------------
      else { //sin enfermedad
        modoOperacion = modo;
        dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
      }
      bDeboLanzarEnfermo = false;
    } //ACEPTAR

    sStringBk = txtCodEnfermo.getText();
    sEnfermedadBk = getCodEnfermedad(); //BACKUP  (aunque no tiene sentido)
    //boton no habilit en modif, ptt no procede sN1bk..

  } //fin BOTON enfermo

  //!!!!!  CASO !!!!!!!
  //!!!!!!!!!!!!!!!!!!!
  //CD_ENFERMO, CD_ENFERMEDAD*******
  public void btnCaso_actionPerformed() {
    int modo = modoOperacion;

    //Datos obligatorios***
    int indice = choEnfermedad.getSelectedIndex();
    if (indice <= 0 ||
        txtCodEnfermo.getText().trim().equals("")) {
      ShowWarning(res.getString("msg68.Text"));
      return;
    } //OBLIGATORIOS******

    //nota: los paneles estan abiertos, pero sin datos

    int iBuscar = 0;
    iBuscar = Buscar_Caso();
    if (iBuscar == -1) { //no hay datos
      ShowWarning(res.getString("msg67.Text"));
      modoOperacion = modo; //ALTA
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
    }
    else if (iBuscar == 0) { //cancelar
      modoOperacion = modo; //ALTA
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
    }
    else if (iBuscar == 1) { //acepta uno
      bDeboLanzarEnfermo = false;
      //abre paneles e Inicializa a Modificar
    }
    //no procede sN1Bk porque en modif  no se buscan casos

  } //FIN BOTON DE CASO *****

  String cadenaBarra(int tipo) {
    String cad = res.getString("msg69.Text");
    switch (tipo) {
      case 0:
        break;
      case 1:
        cad = cad + res.getString("msg71.Text");
        break;
      case 2:
        cad = cad + res.getString("msg72.Text");
        break;
      case 3:
        cad = cad + res.getString("msg73.Text");
        break;
    }
    return cad;
  }

  protected int Buscar_Enfermo(int iTipo) {
//llamada normal 0 (data: todo);
//llamada desde documento: 1 (data: tipo, txtDocumento)
//llamada desde via: 2 (data: ape1, ape2, nombre, via, mun, prov)
//llamada desde fechanac 3: (data: ape1, ape2, nombre, fechanac)

//0:	sale por cancelar No selecciono enfermo
//-1:	No hay datos, sacar mensaje si desde boton
//1:	acepta uno
//Entra en espera, pero no Inicializa al salir!!! (la salida siempre es ALTA)

    int iSalida = 0;
    if (getApp().getPerfil() == 5) {
      return (iSalida);
    }

    int modo = modoOperacion;
    if (modoOperacion != modoESPERA) {
      modoOperacion = modoESPERA;
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
    }

    //CONFIDENCIALIDAD --------------------------------------------
    //AUTORIZACIONES SOBRE EL DIALOGO: control (data: bFlag_Siglas)
    //si no esta autorizado, enviar en el data-> bFlag_Siglas =true
    boolean bFlag_Siglas = false;
    if (!getFlagEnfermo()) { //confid
      bFlag_Siglas = true;
    }
    //-------------------------------------------------------------

    DataValoresEDO dataVal = null;
    switch (iTipo) {
      case 0: // suca
        dataVal = new DataValoresEDO(txtApe1.getText().trim(),
                                     txtApe2.getText().trim(),
                                     txtNombre.getText().trim(),
                                     panSuca.getCD_MUN(),
                                     panSuca.getCD_PROV(),
                                     panSuca.getDS_DIREC(),
                                     txtFechaNac.getText().trim(),
                                     getCodSexo().trim(),
                                     getCodTipoDocumento().trim(),
                                     txtDocumento.getText().trim(),
                                     bFlag_Siglas);

        break;
      case 1:
        dataVal = new DataValoresEDO("",
                                     "",
                                     "",
                                     "",
                                     "",
                                     "",
                                     "",
                                     "",
                                     getCodTipoDocumento().trim(),
                                     txtDocumento.getText().trim(),
                                     bFlag_Siglas);
        break;
      case 2: //ape1, ape2, nombre, via, mun, prov) // suca

        if (panSuca.getCDVIAL().trim().equals("")) {
          dataVal = new DataValoresEDO(txtApe1.getText().trim(),
                                       txtApe2.getText().trim(),
                                       txtNombre.getText().trim(),
                                       panSuca.getCD_MUN(),
                                       getCodProvincia().trim(),
                                       panSuca.getDS_DIREC(),
                                       "",
                                       "",
                                       "",
                                       "",
                                       bFlag_Siglas);
        }
        else {
          dataVal = new DataValoresEDO(txtApe1.getText().trim(),
                                       txtApe2.getText().trim(),
                                       txtNombre.getText().trim(),
                                       panSuca.getCD_MUN(),
                                       getCodProvincia().trim(),
                                       "",
                                       "",
                                       "",
                                       "",
                                       "",
                                       bFlag_Siglas);
          //para buscar por... ira informado
          dataVal.CDVIAL = panSuca.getCDVIAL();
        }
        break;
      case 3:
        dataVal = new DataValoresEDO(txtApe1.getText().trim(),
                                     txtApe2.getText().trim(),
                                     txtNombre.getText().trim(),
                                     "",
                                     "",
                                     "",
                                     txtFechaNac.getText().trim(),
                                     "",
                                     "",
                                     "",
                                     bFlag_Siglas);
        break;

    }
    dlgIndiv.getBarra().setText(cadenaBarra(iTipo));

    DialogSelEnfermo dlg = new DialogSelEnfermo(this.getApp(),
                                                dataVal);

// hay datos --> lo muestro
    if (dlg.hayResultados()) {

      dlgIndiv.getBarra().setText("");
      dlg.show();

      //pulso OK!!!!
      //!!!!!!!!!!!!
      if (dlg.BotonSalida()) {

        iSalida = 1;
        txtCodEnfermo.setText(dlg.Resultados.sCodEnfermo);

        //confidencialidad
        if (!getFlagEnfermo()) { //confid
          Visible(false);

        }
        txtApe1.setText(dlg.Resultados.sApe1);
        txtApe2.setText(dlg.Resultados.sApe2);
        txtNombre.setText(dlg.Resultados.sNombre);

        //--------------------------------------
        setTipoDocumento(dlg.Resultados.sTDoc);

        //documento---
        txtDocumento.setText(dlg.Resultados.sNDoc);

        if (choTipoDocumento.getSelectedIndex() != 0
            && choTipoDocumento.getSelectedIndex() != -1) {
          txtDocumento.setVisible(true);
          lDocumento.setVisible(true);
        }
        else {
          txtDocumento.setVisible(false);
          lDocumento.setVisible(false);
        }
        this.doLayout();
        //---------

        setSexo(dlg.Resultados.sCodSexo);

        //NECESITO SABER SI ES CALCULADA  ---
        //sFechaNac viene vacia o informada. Nunca null.
        txtFechaNac.setText(dlg.Resultados.sFechaNac);
        FechaNac_desde_BD(dlg.Resultados.sItCalc);
        //------

        txtTelefono.setText(dlg.Resultados.sTelefono);

        //SUCA --------------------------------------
        //panSuca.setModoDeshabilitado();

        //pais espa�a
        //ca
        //(se llena la lista y el hash??)
        setCA(dlg.Resultados.sCodCA);
        if (!dlg.Resultados.sCodCA.equals("")) {
          setProvincia(dlg.Resultados.sCodProv);

        }
        panSuca.setCD_MUN(dlg.Resultados.sCodMun);
        panSuca.setDS_MUN(dlg.Resultados.sDesMun);

        panSuca.setCDTVIA(dlg.Resultados.CDTVIA);
        panSuca.setCDVIAL(dlg.Resultados.CDVIAL);
        panSuca.setCDTNUM(dlg.Resultados.CDTNUM);
        panSuca.setDSCALNUM(dlg.Resultados.DSCALNUM);

        panSuca.setDS_DIREC(dlg.Resultados.sDesCalle);
        panSuca.setCD_POSTAL(dlg.Resultados.sCPostal);
        panSuca.setDS_PISO(dlg.Resultados.sPiso);
        panSuca.setDS_NUM(dlg.Resultados.sNumero);

        txtCodNivel1.setText(dlg.Resultados.sCodNivel1);
        txtDesNivel1.setText(dlg.Resultados.sDesNivel1);
        if (!txtDesNivel1.getText().trim().equals("")) {
          bNivel1Valid = true;
          bNivel1SelDePopup = true;
        }
        txtCodNivel2.setText(dlg.Resultados.sCodNivel2);
        txtDesNivel2.setText(dlg.Resultados.sDesNivel2);
        if (!txtDesNivel2.getText().trim().equals("")) {
          bNivel2Valid = true;
          bNivel2SelDePopup = true;
        }
        txtCodZBS.setText(dlg.Resultados.sCodZBS);
        txtDesZBS.setText(dlg.Resultados.sDesZBS);
        if (!txtDesZBS.getText().trim().equals("")) {
          bNivelZBSValid = true;
          bNivelZBSSelDePopup = true;
        }

        //cargamos la estructura de datos del enfermo
        String sA1 = "", sFonoA1 = "";
        String sA2 = "", sFonoA2 = "";
        String sN = "", sFonoN = "";
        sA1 = dlg.Resultados.sApe1;
        sA2 = dlg.Resultados.sApe2;
        sN = dlg.Resultados.sNombre;
        sFonoA1 = comun.traduccionFonetica(sA1);
        sFonoA2 = comun.traduccionFonetica(sA2);
        sFonoN = comun.traduccionFonetica(sN);

        dataEnfermoBD = new DataEnfermoBD(
            dlg.Resultados.sCodEnfermo, dlg.Resultados.sTDoc,
            sA1, sA2, sN, sFonoA1, sFonoA2, sFonoN,
            dlg.Resultados.sItCalc, dlg.Resultados.sFechaNac,
            dlg.Resultados.sCodSexo, dlg.Resultados.sTelefono,
            dlg.Resultados.sCodNivel1, dlg.Resultados.sCodNivel2,
            dlg.Resultados.sCodZBS, "",
            "", dlg.Resultados.sNDoc,
            dlg.Resultados.sSiglas);

        //FIN DE CARGAR DATA
        //confidencialidad
        if (!getFlagEnfermo()) { //confid
          Confidencialidad();
        }
        else {
          Visible(true);
        } //---------------------------

      }
      //Abrio, lo vio y CANCELAR!!!
      //!!!!!!!!!!!
      else {
        //nada
        iSalida = 0;

        modoOperacion = modo;
        dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
      }

    } //NO HAY DATOS !!!!!
    else {
      dlgIndiv.getBarra().setText(res.getString("dlgIndiv.Barra.Text"));
      iSalida = -1;

      modoOperacion = modo;
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
    }

    //DE CUALQUIER MODO:
    dlg = null;

    return (iSalida);
  } //fin funcion ****************++++++++++++++++

//0: sale por cancelar  ALTA
//-1: no hay datos, sacar mensaje si se hace desde boton caso. ALTA
//1: aceptar y con datos
//llama a rellenadatos, y abre los paneles. MODIFICACION
//Esta funcion se queda en espera!!!!, poner detras
//de quien la llame, modo e inicializar (NO EN MODIFICACION; QUE YA LO HACE)
  protected int Buscar_Caso() {

    int result = 0;

    if (getApp().getPerfil() == 5) {
      return (result);
    }

    int iEnfAntes = choEnfermedad.getSelectedIndex();
    int modo = modoOperacion;
    if (modoOperacion != modoESPERA) {
      modoOperacion = modoESPERA;
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
    }

    DataCasoEDO dataCaso = new DataCasoEDO(
        txtCodEnfermo.getText().trim(),
        dataEnfermoBD.getDS_APE1(),
        dataEnfermoBD.getDS_APE2(),
        dataEnfermoBD.getDS_NOMBRE(),
        dataEnfermoBD.getSIGLAS(),
        0,
        getDesEnfermedad(),
        getCodEnfermedad(),
        false,
        false,
        getFlagEnfermo(),
        txtFechaNac.getText().trim(),
        CfechaNotif.getText().trim(),
        "", "", "",
        "", "", "", "", "", "");

    dlgIndiv.getBarra().setText(res.getString("BarraB.Text"));

    DialogCasoEDO dlg = new DialogCasoEDO(this.getApp(), dataCaso);

    // ahora se recoge el flag: si vuelve a true, es que se ha
    // cerrado con Aceptar y hay datos
    if (dlg.Resultados.bHayDatos) {
      dlgIndiv.getBarra().setText("");
      dlg.show();

      // si hay datos, y adem�s se han seleccionado y aceptado,
      // tenemos un CASO EXISTENTE -> pasar a modo MODIFICAR
      if (dlg.Resultados.bEsOK) {

        //si igual datos, No Solapas, pero si modificacion
        //si diferentes datos, cerrar y abrir , modificacion
        //PERO, las solapas se abren con NUEVO SELECT , ptt
        //SOLO modificacion

        //simula modificacion
        dlgIndiv.modoOperacionBk = modoMODIFICACION;
        dlgIndiv.modoOperacion = modoMODIFICACION;
        modo = modoMODIFICACION;

        //CASO
        //recuperar DATOS DEL CASO
        String nm_edo = "";
        nm_edo = (new Integer(dlg.Resultados.iCodCaso)).toString();
        hashNotifEDO.put("NM_EDO", nm_edo);

        dlgIndiv.Inicializar(modoESPERA);
        dlgIndiv.RellenaDatos(); //se encarga de abrir paneles
        dlgIndiv.Inicializar(modoMODIFICACION); //???

        result = 1; //modo modificacion
      } //ACEPTAR

      //CANCELAR
      else {
        //nada
        result = 0;
      }

    }
    else {
      dlgIndiv.getBarra().setText(res.getString("BarraC.Text"));
      result = -1;

    }

    dlg = null;

    //modoOperacion = modo; //reseteara pCabecera!!!! (ver si txtCodEnfermo amarillo)
    //Inicializar();

    return (result);

  } //fin buscar_caso

  public void Reset_Cabecera() {

    txtCodEnfermo.setText("");
    txtDocumento.setText("");
    choTipoDocumento.select(0);

    //visible
    txtDocumento.setVisible(false);
    lDocumento.setVisible(false);

    txtApe1.setText("");
    txtApe2.setText("");
    txtNombre.setText("");
    txtTelefono.setText("");
    // suca txtVia.setText("");
    // suca txtNo.setText("");
    // suca txtPiso.setText("");
    // suca txtCodPostal.setText("");
    panSuca.setDS_DIREC("");
    panSuca.setDS_NUM("");
    panSuca.setDS_PISO("");
    panSuca.setCD_POSTAL("");

    txtCodNivel1.setText("");
    txtDesNivel1.setText("");
    txtCodNivel2.setText("");
    txtDesNivel2.setText("");
    txtCodZBS.setText("");
    txtDesZBS.setText("");
    InicializaNivel2();
    InicializaNivelZBS();

    choSexo.select(0);

    txtFechaNac.setText("");
    txtEdad.setText("");
    lTipoFechaNac.setText("");

    // suca txtCodMun.setText("");
    // suca txtDesMun.setText("");

    // Rellenando datos al panel SUCA  -------------------------------------
    //panSuca.setModoEspera();

    panSuca.setCD_MUN("");
    panSuca.setDS_MUN("");

    // suca choCA.select(0); //nadie le quita el mouse, en prevision de poder cambiar PAIS
    // suca choProvincia.select(0); //nadie le quita el mouse
    panSuca.setCD_CA("");
    panSuca.setCD_PROV("");

    //panSuca.setModoNormal();

    //enfermedad no se toca!

    //confidencialidad
    lApeNombre.setText(res.getString("lApeNombre.Text")); //oportunidad de alta
    txtApe1.setEnabled(true);
    Visible(true);

    //paneles cerrarlos
    if (dlgIndiv.pCaso != null) {
      dlgIndiv.borrarCaso();
    }
    if (dlgIndiv.pProtocolo != null) {
      dlgIndiv.borrarProtocolo();

    }
  } //fin reset***********************************

  public int Ape1_Y_QuizasOtros() {

//nos se considera cdEnfermo
//1: al menos ape1 informado
//2: ape1 NO, pero algun otro que va en la query de Enfermo
//3: ape1 NO, NINGUN DATO de la query de Enfermo pero algun dato que no vale para la query
//4: sin contar: cdEnfermo, enfermedad y Pais, el resto, vacio
    int iResult = 0;

    if (!txtApe1.getText().trim().equals("")) {
      iResult = 1;
      return (iResult);
    }
    //Ape1 vacia
    //fecha y edad van juntos
    // suca
    if (!txtApe2.getText().trim().equals("")
        || !txtNombre.getText().trim().equals("")
        || !panSuca.getCD_MUN().trim().equals("")
        || !panSuca.getCDVIAL().trim().equals("")
        || !panSuca.getDS_DIREC().trim().equals("")
        || !txtFechaNac.getText().trim().equals("")
        || !txtDocumento.getText().trim().equals("")
        || !panSuca.getCD_PROV().trim().equals("")
        || !getCodSexo().trim().equals("")
        || !getCodTipoDocumento().trim().equals("")) {

      iResult = 2;
      return (iResult);
    }

    /*//LOS DATOS NECESARIOS PARA LA QUERY DE ENFERMO ESTAN DESINFORMADOS,
     //PERO BUSCO EL RESTO
    // suca
     if (!txtTelefono.getText().trim().equals("")
       || !panSuca.getDS_NUM().trim().equals("")
       || !panSuca.getDS_PISO().trim().equals("")
      || !panSuca.getCD_POSTAL().trim().equals("")
      || !txtCodNivel1.getText().trim().equals("")
      || !txtCodNivel2.getText().trim().equals("")
      || !txtCodZBS.getText().trim().equals("")
      || !panSuca.getCD_CA().trim().equals("")
      || !panSuca.getCD_PROV().trim().equals("")
    ){
    iResult  = 3;
    return(iResult);
     } */

   iResult = 4;
    return (iResult);

  } //fin **********************************

  // se ejecuta en respuesta al keyPressed en txtCodNivel1
  void txtCodNivel1_keyPressed() {

    txtDesNivel1.setText("");
    txtCodNivel2.setText("");
    txtDesNivel2.setText("");
    txtCodZBS.setText("");
    txtDesZBS.setText("");

    bNivel1SelDePopup = false;
    bNivel2SelDePopup = false;
    bNivelZBSSelDePopup = false;
    bNivel1Valid = false;
    bNivel2Valid = false;
    bNivelZBSValid = false;

    btnNivel2.setEnabled(false);
    btnZBS.setEnabled(false);

  }

  // se ejecuta en respuesta al keyPressed en txtCodNivel2
  void txtCodNivel2_keyPressed() {
    txtDesNivel2.setText("");
    txtCodZBS.setText("");
    txtDesZBS.setText("");

    bNivel2SelDePopup = false;
    bNivelZBSSelDePopup = false;
    bNivel2Valid = false;
    bNivelZBSValid = false;

    btnZBS.setEnabled(false);

  }

  // se ejecuta en respuesta al keyPressed en txtCodZBS
  void txtCodZBS_keyPressed() {
    txtDesZBS.setText("");
  }

  public void txtDocumento_focusLost() {

    int modo = modoOperacion;

    //sin dato o sin permiso
    if (!bDeboLanzarEnfermo
        || txtDocumento.getText().trim().equals("")) {
      return;
    }
    //con datos suficientes y con permiso de lanzar Automatismo
    int iBuscar = 0;
    iBuscar = Buscar_Enfermo(1);
    if (iBuscar == -1 || iBuscar == 0) { //No hay datos
      return;
    }
    else if (iBuscar == 1) { //aceptar
      bDeboLanzarEnfermo = false;
      //se habra pintado el enfermo
      //que continue con a parte de caso
      //  protected int Buscar_Caso(){
      //----------------------------------------
      String sStringDespues = txtCodEnfermo.getText(); //por si se vuelve a llenar
      if (choEnfermedad.getSelectedIndex() > 0) {
        int ibuscar = 0;
        ibuscar = Buscar_Caso();
        if (ibuscar == 0 || ibuscar == -1) {
          //paneles --- llaman a inicializar (ALTA (pCabecera))
          Paneles_txtCodEnfermo_focuslost(sStringBk, sStringDespues);
        }
        else if (ibuscar == 1) { //acepta caso
          //ya estan los paneles y en modificacion
        }

      } //if -----
      else { //sin enfermedad
        modoOperacion = modo;
        dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
      } //---------------------------------------

      //BACKUPS ****
      sStringBk = txtCodEnfermo.getText();
      sEnfermedadBk = getCodEnfermedad();

    } //ACEPTAR

  } //fin lostfocus de documento

  // se ejecuta en respuesta al focuslost en txtCodNivel1
  void txtCodNivel1_focusLost() {

    if (bSalir_del_dialogo) {
      return;
    }
    if (bFocoN1) {
      return;
    }

    if (bNivel1Valid) {
      return;
    }

    if (bNivel1SelDePopup) {
      return;
    }

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    if (txtCodNivel1.getText().trim().equals("")) {
      bFocoN1 = true;
      bFocoN1 = false;
      bNivel1Valid = false;
      bNivel2Valid = false;
      bNivelZBSValid = false;

      modoOperacion = modo;
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
      Parche_Protocolo();
      return;
    }

    // Comprueba que datos introducidos son v�lidos si no se han
    // seleccionado de los popup
    CLista data = null;
    Object componente;

    if (!bNivel1SelDePopup) {

      try {
        data = CompruebaNivel(
            servletOBTENER_TODOS_NIV1_X_CODIGO_TODAS_AUTORIZ,
            txtCodNivel1.getText().trim(), "", "");

        if (data.size() == 0) {

          bFocoN1 = true;
          ShowWarning(res.getString("msg70.Text"));
          bFocoN1 = false;
          txtCodNivel1.setText("");
          bNivel1Valid = false;
          bNivel2Valid = false;
          bNivelZBSValid = false;

          modoOperacion = modo;
          dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
          Parche_Protocolo();
          return;
        }

        else {
          DataEntradaEDO dataEDO = new DataEntradaEDO();
          dataEDO = (DataEntradaEDO) data.elementAt(0);
          txtDesNivel1.setText(dataEDO.getDes());
        }

      }
      catch (Exception e) { //acceso a base de datos mal

        bFocoN1 = true;
        ShowWarning(e.getMessage());
        bFocoN1 = false;

        bNivel1Valid = false;
        bNivel2Valid = false;
        bNivelZBSValid = false;

        //reseteo
        txtCodNivel1.setText("");
        txtCodNivel1.select(txtCodNivel1.getText().length(),
                            txtCodNivel1.getText().length());
        txtCodNivel1.requestFocus();

        modoOperacion = modo;
        dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
        Parche_Protocolo();
        return;
      } //fin del try

      //si todo fue bien
      componente = data.elementAt(0);
      txtDesNivel1.setText( ( (DataEntradaEDO) componente).getDes());
      bNivel1SelDePopup = true;
    }

    modoOperacion = modo;
    dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    // si no ha salido por ninguna de las condiciones anteriores,
    // el nivel1 el v�lido, y se pone su boolean a true
    bNivel1Valid = true;

    Parche_Protocolo();

  } //fin focuslostNivel1

  void Parche_Protocolo() {

    int modo = modoOperacion;

    //PARCHE: protocolo -----
    if (!txtCodNivel1.getText().trim().equals(sN1Bk) ||
        !txtCodNivel2.getText().trim().equals(sN2Bk)) {

      if (modo == modoMODIFICACION &&
          !dlgIndiv.YaSalioMsgEnferSinProtocolo) {

        dlgIndiv.borrarProtocolo();
        dlgIndiv.Inicializar(modoESPERA);
        dlgIndiv.getBarra().setText(res.getString("BarraD.Text"));
        dlgIndiv.addProtocolo();
        dlgIndiv.Inicializar(modo);
        dlgIndiv.getBarra().setText("");
      }
      else if (modo == modoALTA
               && dlgIndiv.pProtocolo != null) {
        dlgIndiv.borrarProtocolo();
        dlgIndiv.Inicializar(modoESPERA);
        dlgIndiv.getBarra().setText(res.getString("BarraD.Text"));
        dlgIndiv.addProtocolo();
        dlgIndiv.Inicializar(modo);
        dlgIndiv.getBarra().setText("");

      }
      sN1Bk = txtCodNivel1.getText().trim();
      sN2Bk = txtCodNivel2.getText().trim();
    }
    //-----------------------
  }

// se ejecuta en respuesta al focuslost en txtCodNivel2
  void txtCodNivel2_focusLost() {
    if (bSalir_del_dialogo) {
      return;
    }
    if (bFocoN2) {
      return;
    }

    if (bNivel2Valid) {
      return;
    }

    if (bNivel2SelDePopup) {
      return;
    }

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    CLista data = null;
    Object componente = null;

    //si lleno - y no se pulso popup
    if (txtCodNivel2.getText().trim().length() != 0
        && !bNivel2SelDePopup) {

      try {
        data = CompruebaNivel(
            servletOBTENER_NIV2_X_CODIGO_TODAS_AUTORIZ,
            txtCodNivel2.getText().trim(),
            txtCodNivel1.getText().trim(), "");

        if (data.size() == 0) {

          bFocoN2 = true;
          ShowWarning(res.getString("msg70.Text"));
          bFocoN2 = false;
          txtCodNivel2.setText("");
          txtDesNivel2.setText("");
          bNivel2Valid = false;
          bNivelZBSValid = false;

          modoOperacion = modo;
          dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
          Parche_Protocolo();
          return;
        }

        else {
          DataEntradaEDO dataEDO = new DataEntradaEDO();
          dataEDO = (DataEntradaEDO) data.elementAt(0);
          txtDesNivel2.setText(dataEDO.getDes());

          bNivel2SelDePopup = true;
        }

      }
      catch (Exception e) { //acceso a base de datos mal

        bFocoN2 = true;
        ShowWarning(e.getMessage());
        bFocoN2 = false;

        bNivel2Valid = false;
        bNivelZBSValid = false;

        //resetea
        txtCodNivel2.setText("");
        txtCodNivel2.select(txtCodNivel2.getText().length(),
                            txtCodNivel2.getText().length());
        txtCodNivel2.requestFocus();

        modoOperacion = modo;
        dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
        Parche_Protocolo();
        return;
      } //fin del try

      modoOperacion = modo;
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    } //fin del if

    // si no ha salido por ninguna de las condiciones anteriores,
    // el nivel2 es correcto, y se pone su boolean a true
    if (modoOperacion == modoESPERA) {
      //modoOperacion = modoNORMAL;
      modoOperacion = modo;
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
    }

    bNivel2Valid = true;
    Parche_Protocolo();

  } //fin de n2

  // se ejecuta en respuesta al focuslost en txtCodZBS
  void txtCodZBS_focusLost() {
    if (bSalir_del_dialogo) {
      return;
    }
    if (bFocoZBS) {
      return;
    }

    if (bNivelZBSValid) {
      return;
    }

    if (bNivelZBSSelDePopup) {
      return;
    }

    int modo = modoOperacion;
    modoOperacion = modoESPERA;
    dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    CLista data = null;
    Object componente = null;

    //si lleno - y no se pulso popup
    if (txtCodZBS.getText().trim().length() != 0
        && !bNivelZBSSelDePopup) {

      try {
        data = CompruebaNivel(
            servletOBTENER_ZBS_X_CODIGO,
            txtCodZBS.getText().trim(),
            txtCodNivel1.getText().trim(),
            txtCodNivel2.getText().trim());

        if (data.size() == 0) {

          bFocoZBS = true;
          ShowWarning(res.getString("msg70.Text"));
          bFocoZBS = false;
          txtCodZBS.setText("");
          txtDesZBS.setText("");
          bNivelZBSValid = false;

          modoOperacion = modo;
          dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
          return;
        }
        else {
          DataEntradaEDO dataEDO = new DataEntradaEDO();
          dataEDO = (DataEntradaEDO) data.elementAt(0);
          txtDesZBS.setText(dataEDO.getDes());

          bNivelZBSSelDePopup = true;
        }

      }
      catch (Exception e) { //acceso a base de datos mal

        bFocoZBS = true;
        ShowWarning(e.getMessage());
        bFocoZBS = false;

        bNivelZBSValid = false;

        //resetea
        txtCodZBS.setText("");
        txtCodZBS.select(txtCodZBS.getText().length(),
                         txtCodZBS.getText().length());
        txtCodZBS.requestFocus();

        modoOperacion = modo;
        dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
        return;

      } //fin del try

      modoOperacion = modo;
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    }
    // si no ha salido por ninguna de las condiciones anteriores,
    // el nivel2 es correcto, y se pone su boolean a true
    bNivelZBSValid = true;
    if (modoOperacion == modoESPERA) {
      modoOperacion = modo;
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
    }
  }

  public CLista CompruebaNivel(int iMode, String sCod, String sNivel1,
                               String sNivel2) throws Exception {

    CLista lista = null, data = null;
    data = new CLista();

    //JRM: A�adimos la informaci�n de �reas para que funcionen las p�rdidas
    // de foco.
    data.setVN1(getApp().getCD_NIVEL_1_AUTORIZACIONES());
    data.setVN2(getApp().getCD_NIVEL_2_AUTORIZACIONES());

    data.addElement(new DataEntradaEDO(sCod, "", sNivel1, sNivel2));

    data.setLogin(app.getLogin());
    data.setLortad(app.getParameter("LORTAD"));

    lista = Comunicador.Communicate(getApp(),
                                    stubCliente,
                                    iMode,
                                    strSERVLET,
                                    data);

    lista.trimToSize();
    return lista;
  }

  /*
     public CLista CompruebaMunicipio(int iMode,
       String sCodMun, String sProv) throws Exception {
    CLista lista = null, data = null;
    data = new CLista();
    data.addElement( new DataMunicipioEDO(sCodMun, sProv));
    lista = Comunicador.Communicate(getApp(),
                          stubCliente,
                          iMode,
                          strSERVLET_MUNICIPIO,
                          data);
    lista.trimToSize();
    return lista;
     }*/

  // Llama a CMessage para mostrar el mensaje de aviso sMessage
  private void ShowWarning(String sMessage) {
    dlgIndiv.ShowWarning(sMessage);
  }

// SUCA
  public void vialInformado() {

    int modo = modoOperacion;

    //sin datos o sin permiso
    // suca
    if (txtApe1.getText().trim().equals("")
        || !bDeboLanzarEnfermo
        || panSuca.getDS_DIREC().trim().equals("")
        || panSuca.getCD_MUN().trim().equals("")
        || panSuca.getCD_PROV().trim().equals("")
        ) {
      return;
    }
    //con datos suficientes y con permiso de lanzar Automatismo
    int iBuscar = 0;
    iBuscar = Buscar_Enfermo(2);
    if (iBuscar == -1 || iBuscar == 0) { //No hay datos
      return;
    }
    else if (iBuscar == 1) { //aceptar
      bDeboLanzarEnfermo = false;
      //se habra pintado el enfermo
      //que continue con a parte de caso
      //  protected int Buscar_Caso(){
      //----------------------------------------
      String sStringDespues = txtCodEnfermo.getText(); //por si se vuelve a llenar
      if (choEnfermedad.getSelectedIndex() > 0) {
        int ibuscar = 0;
        ibuscar = Buscar_Caso();
        if (ibuscar == 0 || ibuscar == -1) {
          //paneles --- llaman a inicializar (ALTA (pCabecera))
          Paneles_txtCodEnfermo_focuslost(sStringBk, sStringDespues);
        }
        else if (ibuscar == 1) { //acepta caso
          //ya estan los paneles y en modificacion
        }

      } //if -----
      else { //sin enfermedad
        modoOperacion = modo;
        dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
      } //---------------------------------------

      //BACKUPS ****
      sStringBk = txtCodEnfermo.getText();
      sEnfermedadBk = getCodEnfermedad();

    } //ACEPTAR
  }

  // b�squeda de la zonificaci�n sanitaria de un municipio
  public void setZonificacionSanitaria(String mun, String prov) { //DSR: parametro prov
    //Entra en modo ESPERA

    if (panSuca.getModoTramero()) {
      //actZonaRiesgo()
      ;
    }
    else {
      DataMunicipioEDO data = new DataMunicipioEDO(mun, prov); //DSR: parametro prov

      if (mun.trim().equals("")) {
        txtCodNivel1.setText("");
        txtDesNivel1.setText("");
        txtCodNivel2.setText("");
        txtDesNivel2.setText("");
        txtCodZBS.setText("");
        txtDesZBS.setText("");

      }
      else {

        try {
          CLista listadata = new CLista();
          listadata.addElement(data);
          listadata = Comunicador.Communicate(this.getApp(),
                                              stubCliente,
                                              0, //no lleva modo
                                              strSERVLET_MUNICIPIO_CONT,
                                              listadata);
          //listadata.size()==1
          data = (DataMunicipioEDO) listadata.firstElement();
        }
        catch (Exception e) {
          ShowWarning(res.getString("msg74.Text"));
          modoOperacion = iAntesDeZona;
          dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
          Parche_Protocolo();
          return;
        }

        txtCodNivel1.setText(data.getCodNivel1());
        txtDesNivel1.setText(data.getDesNivel1());
        txtCodNivel2.setText(data.getCodNivel2());
        txtDesNivel2.setText(data.getDesNivel2());
        txtCodZBS.setText(data.getCodZBS());
        txtDesZBS.setText(data.getDesZBS());
        if (!txtDesNivel1.getText().trim().equals("")) {
          bNivel1Valid = true;
          bNivel1SelDePopup = true;
        }
        if (!txtDesNivel2.getText().trim().equals("")) {
          bNivel2Valid = true;
          bNivel2SelDePopup = true;
        }
        if (!txtDesZBS.getText().trim().equals("")) {
          bNivelZBSValid = true;
          bNivelZBSSelDePopup = true;
        }

      }

      modoOperacion = iAntesDeZona;
      dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

      Parche_Protocolo();
    }
  }

  public void cambiaModoTramero(boolean b) {
    bTramero = b;
  }

  public int ponerEnEspera() {
    int m = modoOperacion;

    iAntesDeZona = m; //para saber en setZona...si alta o modif

    modoOperacion = modoESPERA;
    dlgIndiv.Inicializar(modoOperacion); //mlm nuevo

    return m;
  }

  public void ponerModo(int modo) {
    modoOperacion = modo;
    dlgIndiv.Inicializar(modoOperacion); //mlm nuevo
  }

}

// Actualiza la Zona de Riesgo
class ActualizarZonaRiesgo
    extends Thread {
  PanelEDOIndivCabecera panel = null;

  ActualizarZonaRiesgo(PanelEDOIndivCabecera panel) {
    this.panel = panel;
  }

  public void run() {

    while (true) {
      try {
        boolean actualizado = panel.panSuca.getCodigoPostalActualizado();
        if (actualizado) {
          panel.dlgIndiv.actZonaRiesgo();
          panel.panSuca.setCodigoPostalActualizado(false);
          //Thread.sleep(1000);
          // Cedemos el procesador a otro hilo
        }
        yield();
      }
      catch (Exception e) {
        ;
      }
    }
  }

}

// FOCO*******************************************************************
class PanelEDOIndivCabecerafocusAdapter
    implements java.awt.event.FocusListener, Runnable {
  PanelEDOIndivCabecera adaptee;
  FocusEvent evt;

  PanelEDOIndivCabecerafocusAdapter(PanelEDOIndivCabecera adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void focusGained(FocusEvent e) {

  }

  public void run() {
    if ( ( (TextField) evt.getSource()).getName().equals("fechanac")) {
      adaptee.txtFechaNac_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("edad")) {
      adaptee.txtEdad_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("ape1")) {
      adaptee.txApe1_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("nivel1")) {
      adaptee.txtCodNivel1_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("nivel2")) {
      adaptee.txtCodNivel2_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("zbs")) {
      adaptee.txtCodZBS_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("enfermo")) {
      adaptee.txtCodEnfermo_focusLost();
    }
    else if ( ( (TextField) evt.getSource()).getName().equals("documento")) {
      adaptee.txtDocumento_focusLost();
    }
  }

} //fin class

// escuchador para el pulsado de los botones
class PanelEDOIndivCabeceraactionAdapter
    implements java.awt.event.ActionListener, Runnable {
  PanelEDOIndivCabecera adaptee;
  ActionEvent evt;

  PanelEDOIndivCabeceraactionAdapter(PanelEDOIndivCabecera adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    evt = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    if (evt.getActionCommand().equals("enfermo")) {
      adaptee.btnLupaEnfermo_actionPerformed();
    }
    else if (evt.getActionCommand().equals("nivel1")) {
      adaptee.btnNivel1_actionPerformed();
    }
    else if (evt.getActionCommand().equals("nivel2")) {
      adaptee.btnNivel2_actionPerformed();
    }
    else if (evt.getActionCommand().equals("zbs")) {
      adaptee.btnZBS_actionPerformed();
    }
    else if (evt.getActionCommand().equals("selenfermo")) {
      adaptee.btnEnfermo_actionPerformed();
    }
    else if (evt.getActionCommand().equals("datosenfermo")) {
      adaptee.btnCaso_actionPerformed();
    }
  }
}

// escuchador de los cambios en los itemListener  de los choices
/**
 *  hay tres CListas que representan paises,comunidades y provincias
 *  cuando cambiamos el pais, se rellena la lista de comunidades
 *  y se vacia la lista de Provincias
 */
class PanelEDOIndivCabecerachItemListener
    implements java.awt.event.ItemListener, Runnable {
  PanelEDOIndivCabecera adaptee;
  ItemEvent e = null;

  PanelEDOIndivCabecerachItemListener(PanelEDOIndivCabecera adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void itemStateChanged(ItemEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;
      // lanzamos el thread que tratar� el evento
      new Thread(this).start();
    }
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {
    // quitamos los valores de los textfield de descripcion
    String name = ( (Component) e.getSource()).getName();

    /// buscamos que bot�n es el que se ha pinchado y lo ejecutamos
    if (name.equals("enfermedad")) {
      adaptee.choEnfermedad_itemStateChanged();
    }
    else if (name.equals("tipodoc")) {
      adaptee.choTipoDocumento_itemStateChanged();
    }
    else if (name.equals("edad")) {
      adaptee.choEdad_itemStateChanged();
    }
    // desbloquea la recepci�n  de eventos
    adaptee.desbloquea();
  }
}

// escuchador para las cajas de texto
class PanelEDOIndivCabeceratextAdapter
    extends java.awt.event.KeyAdapter {
  PanelEDOIndivCabecera adaptee;

  PanelEDOIndivCabeceratextAdapter(PanelEDOIndivCabecera adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    if ( ( (TextField) e.getSource()).getName().equals("nivel1")) {
      adaptee.txtCodNivel1_keyPressed();
    }
    else if ( ( (TextField) e.getSource()).getName().equals("nivel2")) {
      adaptee.txtCodNivel2_keyPressed();
    }
    else if ( ( (TextField) e.getSource()).getName().equals("zbs")) {
      adaptee.txtCodZBS_keyPressed();
    }
  }
}

// PRIMERA TRAMA DE LAS LISTAS/////////////////////////////////
// clase para obtener y seleccionar niveles2
class CListaNiveles2Indiv
    extends CListaValores {

  protected PanelEDOIndivCabecera panel;

  public CListaNiveles2Indiv(PanelEDOIndivCabecera p,
                             String title,
                             StubSrvBD stub,
                             String servlet,
                             int obtener_x_codigo,
                             int obtener_x_descricpcion,
                             int seleccion_x_codigo,
                             int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;

    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s, "",
                              panel.txtCodNivel1.getText(),
                              panel.txtCodNivel2.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

// clase para obtener y seleccionar zbs
class CListaNivelesZBSIndiv
    extends CListaValores {

  protected PanelEDOIndivCabecera panel;

  public CListaNivelesZBSIndiv(PanelEDOIndivCabecera p,
                               String title,
                               StubSrvBD stub,
                               String servlet,
                               int obtener_x_codigo,
                               int obtener_x_descricpcion,
                               int seleccion_x_codigo,
                               int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s, "", panel.txtCodNivel1.getText(),
                              panel.txtCodNivel2.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}
