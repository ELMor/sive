
package panniveles;

public interface usaPanelNiveles {

  public void cambioNivelAntesInformado(int nivel);

  public int ponerEnEspera();

  public void ponerModo(int modo);

}
