// Modificado el 02/12/1999 para hacer p�blico el m�todo
// lanzado al perderse el foco en Nivel 2

package panniveles;

import java.net.URL;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.TextEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import comun.DataEntradaEDO;
import comun.DataNivel1;
import comun.DataZBS;
import comun.DataZBS2;
import comun.constantes;
import sapp.StubSrvBD;

public class panelNiveles
    extends CPanel {

  private final int servletOBTENER_X_CODIGO = 3;
  private final int servletOBTENER_X_DESCRIPCION = 4;
  private final int servletSELECCION_X_CODIGO = 5;
  private final int servletSELECCION_X_DESCRIPCION = 6;

  private final int servletSELECCION_NIV2_X_CODIGO = 7;
  private final int servletOBTENER_NIV2_X_CODIGO = 8;
  private final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  private final int servletOBTENER_NIV2_X_DESCRIPCION = 10;

  /*
    // servlets para la b�squeda de niveles
    private final String strSERVLETNivel1 = "servlet/SrvNivel1";
    private final String strSERVLETNivel2 = "servlet/SrvZBS2";
    private final String strSERVLETZona = "servlet/SrvMun";
    private final String strSERVLETAut = "servlet/SrvEntradaEDO";
   */

  // servlets para la b�squeda de niveles
  private final String strSERVLETNivel1 = constantes.strSERVLET_NIVEL1;
  private final String strSERVLETNivel2 = constantes.strSERVLET_NIV2;
  private final String strSERVLETZona = constantes.strSERVLET_MUN;
  private final String strSERVLETAut = constantes.strSERVLET_NIV;

  // Para saber si se debe alinear a la derecha
  private boolean bDerecha = false;

  //conexion del panel con el panel que lo contiene
  usaPanelNiveles contenedor = null;
  int modoContenedor = -1;

  // conexi�n con los servlets
  protected StubSrvBD stubCliente = new StubSrvBD();

  // sincronizaci�n de eventos
  private boolean sinBloquear = true;

  // Informa de si el componente fue inhabilitado exteriormente.
  private boolean bHabilitado = true;

  // Modos de operaci�n del panel
  public static final int MODO_INICIO = 0;
  public static final int MODO_ESPERA = 1001;
  public static final int MODO_N1 = 1002;
  public static final int MODO_N2 = 1004;
  public static final int MODO_N3 = 1006;
  public static final int MODO_N4 = 1007;

  private int modoAnterior = 0;
  int modoOperacion = 0;

  // Indica si se debe tener en cuenta el perfil y el login del usuario
  // a la hora de acceder a los niveles 1 y 2.
  private boolean bAutorizacion = false;

  // Para saber qu� controles se visualizan
  public static final int TIPO_NIVEL_1 = 1; // �rea
  public static final int TIPO_NIVEL_2 = 2; // �rea + Distrito
  public static final int TIPO_NIVEL_3 = 3; // �rea + Distrito + Descripci�n del distrito
  public static final int TIPO_NIVEL_4 = 4; // �rea + Distrito + ZBS

  private int tipoOperacion = TIPO_NIVEL_4;

  // Para saber su distribuci�n
  public static final int LINEAS_1 = 1;
  public static final int LINEAS_2 = 2;
  public static final int LINEAS_3 = 3;

  //No puede darse el caso de TIPO_NIVEL_4 y COLUMNAS_2
  public static final int COLUMNAS_2 = 12; //una linea con dos columnas

  private int lineas = 0;

  // Para controlar el cambio de las cajas de texto
  private String strCDNivel1 = "";
  private String strCDNivel2 = "";
  private String strCDNivel4 = "";

  // Controles
  TextField txtNivel1 = null;
  TextField txtNivel2 = null;
  TextField txtNivel4 = null;

  TextField txtNivel1L = null;
  TextField txtNivel2L = null;
  TextField txtNivel4L = null;

  String antantTxtNivel1L = new String();
  String antantTxtNivel2L = new String();
  String antantTxtNivel4L = new String();

  String antTxtNivel1L = new String();
  String antTxtNivel2L = new String();
  String antTxtNivel4L = new String();

  ButtonControl btnNivel1 = null;
  ButtonControl btnNivel2 = null;
  ButtonControl btnNivel4 = null;

  Label lblNivel1 = null;
  Label lblNivel2 = null;
  Label lblNivel4 = null;

  // Gestores de eventos
  PanelNivelesBtnActionListener btnActionListener = new
      PanelNivelesBtnActionListener(this);
  PanelNivelesTextListener textListener = new PanelNivelesTextListener(this);
  PanelNivelesDesTextListener textDesListener = new PanelNivelesDesTextListener(this);
  PanelNivelesTextFocusListener txtFocusListener = new
      PanelNivelesTextFocusListener(this);
  XYLayout xyFondo = new XYLayout();

  // Interfaz
  /** Inhabilita todos los objetos y pone el cursor en modo de espera. */
  public void setModoEspera() {
    setModoDeshabilitado();
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
  }

  /** Inhabilita todos los objetos. */
  public void setModoDeshabilitado() {
    if (bHabilitado) {
      int modoTmp = modoOperacion;
      modoOperacion = MODO_ESPERA;
      Inicializar();
      modoOperacion = modoTmp;
      bHabilitado = false;
    }
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  public void setModoOperacion(int m) {
    modoOperacion = m;
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  /** Restaura el estado anterior del panel y pone el cursor en modo normal. */
  public void setModoNormal() {
    bHabilitado = true;
    Inicializar();
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // M�todos de lectura

  public String getCDNivel1() {
    return txtNivel1.getText().trim();
  }

  public String getCDNivel2() {
    String strValor = null;
    if (tipoOperacion >= TIPO_NIVEL_2) {
      strValor = txtNivel2.getText().trim();
    }
    return strValor;
  }

  public String getCDNivel4() {
    String strValor = null;
    if (tipoOperacion == TIPO_NIVEL_4) {
      strValor = txtNivel4.getText().trim();
    }
    return strValor;
  }

  public String getDSNivel1() {
    return txtNivel1L.getText().trim();
  }

  public String getDSNivel2() {
    String strValor = null;
    if (tipoOperacion >= TIPO_NIVEL_2) {
      strValor = txtNivel2L.getText().trim();
    }
    return strValor;
  }

  public String getDSNivel4() {
    String strValor = null;
    if (tipoOperacion == TIPO_NIVEL_4) {
      strValor = txtNivel4L.getText().trim();
    }
    return strValor;
  }

  // Escritura
  public void setNivelObligatorio(int iNivel, boolean bValor) {
    switch (iNivel) {
      case 1:
        txtNivel1.setBackground(bValor ? new Color(255, 255, 150) :
                                new Color(255, 255, 250));
        break;
      case 2:
        if (tipoOperacion >= TIPO_NIVEL_2) {
          txtNivel2.setBackground(bValor ? new Color(255, 255, 150) :
                                  new Color(255, 255, 250));
        }
        break;
      case 4:
        if (tipoOperacion == TIPO_NIVEL_4) {
          txtNivel2.setBackground(bValor ? new Color(255, 255, 150) :
                                  new Color(255, 255, 250));
        }
        break;
    }
    repaint();
  }

  public void setCDNivel1(String strValor) {
    sinBloquear = false;
    txtNivel1.setText(strValor.trim());
    sinBloquear = true;
  }

  // E Para cargar el nivel 1 y activar los controles
  // pertinentes
  public void setBuscaCDNivel1(String strValor) {
    sinBloquear = false;
    txtNivel1.setText(strValor.trim());
    txtNivel2.setEnabled(true);
    btnNivel2.setEnabled(true);
    modoOperacion = MODO_N1;
    Inicializar();
    sinBloquear = true;
  }

  public void setCDNivel2(String strValor) {
    sinBloquear = false;
    if (tipoOperacion >= TIPO_NIVEL_2) {
      txtNivel2.setText(strValor.trim());
    }
    sinBloquear = true;
  }

  public void setCDNivel4(String strValor) {
    sinBloquear = false;
    if (tipoOperacion == TIPO_NIVEL_4) {
      txtNivel4.setText(strValor.trim());
    }
    sinBloquear = true;
  }

  public void setDSNivel1(String strValor) {
    txtNivel1L.setText(strValor.trim());
  }

  public void setDSNivel2(String strValor) {
    if (tipoOperacion >= TIPO_NIVEL_2) {
      txtNivel2L.setText(strValor.trim());
    }
  }

  public void setDSNivel4(String strValor) {
    if (tipoOperacion == TIPO_NIVEL_4) {
      txtNivel4L.setText(strValor.trim());
    }
  }

  //****************************************************************

   public void Inicializar() {
     switch (modoOperacion) {
       case MODO_INICIO:
         txtNivel1.setEnabled(true);
         btnNivel1.setEnabled(true);
         //lblNivel1.setEnabled(true);
         if (tipoOperacion >= TIPO_NIVEL_2) {
           txtNivel2.setEnabled(false);
           btnNivel2.setEnabled(false);
           //lblNivel2.setEnabled(true);
           if (tipoOperacion == TIPO_NIVEL_4) {
             txtNivel4.setEnabled(false);
             btnNivel4.setEnabled(false);
             //lblNivel4.setEnabled(true);
           }
         }

         break;
       case MODO_N1:
         txtNivel1.setEnabled(true);
         btnNivel1.setEnabled(true);
         //lblNivel1.setEnabled(true);
         if (tipoOperacion >= TIPO_NIVEL_2) {
           txtNivel2.setEnabled(true);
           btnNivel2.setEnabled(true);
           //lblNivel2.setEnabled(true);
           if (tipoOperacion == TIPO_NIVEL_4) {
             txtNivel4.setEnabled(false);
             btnNivel4.setEnabled(false);
             //lblNivel4.setEnabled(true);
           }
         }
         break;
       case MODO_N2:
       case MODO_N4:
         txtNivel1.setEnabled(true);
         btnNivel1.setEnabled(true);
         //lblNivel1.setEnabled(true);
         if (tipoOperacion >= TIPO_NIVEL_2) {
           txtNivel2.setEnabled(true);
           btnNivel2.setEnabled(true);
           //lblNivel2.setEnabled(true);
           if (tipoOperacion == TIPO_NIVEL_4) {
             txtNivel4.setEnabled(true);
             btnNivel4.setEnabled(true);
             //lblNivel4.setEnabled(true);
           }
         }
         break;
       case MODO_ESPERA:
         txtNivel1.setEnabled(false);
         btnNivel1.setEnabled(false);
         //lblNivel1.setEnabled(false);
         if (tipoOperacion >= TIPO_NIVEL_2) {
           txtNivel2.setEnabled(false);
           btnNivel2.setEnabled(false);
           //lblNivel2.setEnabled(false);
           if (tipoOperacion == TIPO_NIVEL_4) {
             txtNivel4.setEnabled(false);
             btnNivel4.setEnabled(false);
             //lblNivel4.setEnabled(false);
           }
         }

         break;
     }
   }

  public panelNiveles(CApp a, usaPanelNiveles usuario, int modoOperacion,
                      int tipoOperacion, int lineas) {
    setApp(a);

    bDerecha = false;

    this.contenedor = usuario;

    this.modoOperacion = modoOperacion;
    this.tipoOperacion = tipoOperacion;
    this.lineas = lineas;

    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public panelNiveles(CApp a, usaPanelNiveles usuario, int modoOperacion,
                      int tipoOperacion, int lineas, boolean bAutorizacion,
                      boolean bDerecha) {
    this.bDerecha = bDerecha;

    setApp(a);

    this.contenedor = usuario;

    bDerecha = false;

    this.modoOperacion = modoOperacion;
    this.tipoOperacion = tipoOperacion;
    this.lineas = lineas;
    this.bAutorizacion = bAutorizacion;

    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

  public panelNiveles(CApp a, usaPanelNiveles usuario, int modoOperacion,
                      int tipoOperacion, int lineas, boolean bAutorizacion) {
    setApp(a);

    this.contenedor = usuario;

    bDerecha = false;

    this.modoOperacion = modoOperacion;
    this.tipoOperacion = tipoOperacion;
    this.lineas = lineas;
    this.bAutorizacion = bAutorizacion;

    try {
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    CCargadorImagen imgs = null;
    final String imgNAME[] = {
        "images/Magnify.gif"};

    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    this.setSize(new Dimension(510, 94));
    this.setBorde(false);
    this.setLayout(xyFondo);

    txtNivel1 = new TextField();
    txtNivel1.addFocusListener(txtFocusListener);
    txtNivel1.addTextListener(textListener);
    txtNivel1.setName("nivel1");

    btnNivel1 = new ButtonControl();
    btnNivel1.addActionListener(btnActionListener);
    btnNivel1.setActionCommand("nivel1");
    btnNivel1.setImage(imgs.getImage(0));

    txtNivel1L = new TextField();
    txtNivel1L.setEditable(false);
    txtNivel1L.setEnabled(false);
    txtNivel1L.addTextListener(textDesListener);
    txtNivel1L.setName("desNivel1");

    lblNivel1 = new Label();
    lblNivel1.setText(app.getNivel1());

    //
    if (bDerecha) {
      lblNivel1.setAlignment(java.awt.Label.RIGHT);
    }
    // E

    if (lineas == LINEAS_3) {
      if (bDerecha) {
        this.add(lblNivel1, new XYConstraints(4, 6, 77, 19));
      }
      else {
        this.add(lblNivel1, new XYConstraints(4, 6, 92, 19));
      }
      this.add(txtNivel1, new XYConstraints(103, 6, 66, 22));
      this.add(btnNivel1, new XYConstraints(179, 6, -1, -1));
      this.add(txtNivel1L, new XYConstraints(217, 6, 216, 22));
    }
    else if (lineas == LINEAS_2) {
      if (bDerecha) {
        this.add(lblNivel1, new XYConstraints(4, 6, 77, 19));
      }
      else {
        this.add(lblNivel1, new XYConstraints(4, 6, 92, 19));
      }
      this.add(txtNivel1, new XYConstraints(95, 6, 38, 22));
      this.add(btnNivel1, new XYConstraints(138, 6, -1, -1));
      this.add(txtNivel1L, new XYConstraints(170, 6, 195, 22));
    }
    else if (lineas == LINEAS_1) {
      this.add(lblNivel1, new XYConstraints(4, 6, 80, 19));
      this.add(txtNivel1, new XYConstraints(85, 6, 38, 22));
      this.add(btnNivel1, new XYConstraints(125, 6, -1, -1));
      this.add(txtNivel1L, new XYConstraints(155, 6, 1, 1));
      txtNivel1L.setVisible(false);
    }
    else if (lineas == COLUMNAS_2) {
      if (bDerecha) {
        this.add(lblNivel1, new XYConstraints(4, 6, 77, 19));
      }
      else {
        this.add(lblNivel1, new XYConstraints(4, 6, 92, 19));
      }
      this.add(txtNivel1, new XYConstraints(100, 6, 38, 22));
      this.add(btnNivel1, new XYConstraints(140, 6, -1, -1));
      this.add(txtNivel1L, new XYConstraints(175, 6, 1, 1));
      txtNivel1L.setVisible(false);
    }

    if (tipoOperacion >= TIPO_NIVEL_2) {

      txtNivel2 = new TextField();
      txtNivel2.addFocusListener(txtFocusListener);
      txtNivel2.addTextListener(textListener);
      txtNivel2.setName("nivel2");

      btnNivel2 = new ButtonControl();
      btnNivel2.addActionListener(btnActionListener);
      btnNivel2.setActionCommand("nivel2");
      btnNivel2.setImage(imgs.getImage(0));

      txtNivel2L = new TextField();
      txtNivel2L.setEditable(false);
      txtNivel2L.setEnabled(false);
      txtNivel2L.addTextListener(textDesListener);
      txtNivel2L.setName("desNivel2");

      lblNivel2 = new Label();
      lblNivel2.setText(app.getNivel2());

      //
      if (bDerecha) {
        lblNivel2.setAlignment(java.awt.Label.RIGHT);
      }
      //

      if (lineas == LINEAS_3) {
        this.add(lblNivel2, new XYConstraints(43, 35, 38, 21));
        this.add(txtNivel2, new XYConstraints(103, 35, 66, 22));
        this.add(btnNivel2, new XYConstraints(179, 35, -1, -1));
        this.add(txtNivel2L, new XYConstraints(217, 35, 216, 22));
      }
      else if (lineas == LINEAS_2) {
        this.add(lblNivel2, new XYConstraints(300 + 75 + 43, 6, 38, 21));
        this.add(txtNivel2, new XYConstraints(300 + 75 + 95, 6, 38, 22));
        this.add(btnNivel2, new XYConstraints(300 + 75 + 138, 6, -1, -1));
        this.add(txtNivel2L, new XYConstraints(300 + 75 + 170, 6, 195, 22));
      }
      else if (lineas == LINEAS_1) {
        this.add(lblNivel2, new XYConstraints(202, 6, 38, 21));
        this.add(txtNivel2, new XYConstraints(245, 6, 38, 22));
        this.add(btnNivel2, new XYConstraints(285, 6, 26, 26)); // E 26, 26

        if (tipoOperacion == TIPO_NIVEL_3) {
          this.add(txtNivel2L, new XYConstraints(315, 6, 195, 22));
          txtNivel2L.setVisible(true);
        }
        else {
          this.add(txtNivel2L, new XYConstraints(315, 6, 1, 1));
        }

      }
      else if (lineas == COLUMNAS_2) {
        this.add(lblNivel2, new XYConstraints(180, 6, 92, 21));
        this.add(txtNivel2, new XYConstraints(275, 6, 38, 22));
        this.add(btnNivel2, new XYConstraints(315, 6, -1, -1));
        this.add(txtNivel2L, new XYConstraints(345, 6, 195, 22));
      }

      if (tipoOperacion == TIPO_NIVEL_4) {

        txtNivel4 = new TextField();
        txtNivel4.addFocusListener(txtFocusListener);
        txtNivel4.addTextListener(textListener);
        txtNivel4.setName("nivel4");
        txtNivel4.setEnabled(false);

        btnNivel4 = new ButtonControl();
        btnNivel4.addActionListener(btnActionListener);
        btnNivel4.setActionCommand("nivel4");
        btnNivel4.setImage(imgs.getImage(0));

        lblNivel4 = new Label();
        lblNivel4.setText(app.getNivel3());

        txtNivel4L = new TextField();
        txtNivel4L.setEditable(false);
        txtNivel4L.setEnabled(false);
        txtNivel4L.addTextListener(textDesListener);
        txtNivel4L.setName("desNivel4");

        if (lineas == LINEAS_3) {
          this.add(lblNivel4, new XYConstraints(58, 64, 28, 21));
          this.add(txtNivel4, new XYConstraints(103, 64, 66, 22));
          this.add(btnNivel4, new XYConstraints(179, 64, -1, -1));
          this.add(txtNivel4L, new XYConstraints(217, 64, 217, 22));
        }
        else if (lineas == LINEAS_2) {
          this.add(lblNivel4, new XYConstraints(58, 35, 28, 21));
          this.add(txtNivel4, new XYConstraints(95, 35, 38, 22));
          this.add(btnNivel4, new XYConstraints(138, 35, -1, -1));
          this.add(txtNivel4L, new XYConstraints(170, 35, 195, 22));
        }
        else if (lineas == LINEAS_1) {
          this.add(lblNivel4, new XYConstraints(376, 6, 28, 21));
          this.add(txtNivel4, new XYConstraints(405, 6, 38, 22));
          this.add(btnNivel4, new XYConstraints(445, 6, -1, -1));
          this.add(txtNivel4L, new XYConstraints(475, 6, 175, 22));
        }
      }
    }

    if (lineas == LINEAS_3) {
      xyFondo.setHeight(91);
      xyFondo.setWidth(438);
    }
    else if (lineas == LINEAS_2) {
      xyFondo.setHeight(70);
      xyFondo.setWidth(755);
    }
    else if (lineas == LINEAS_1) {
      xyFondo.setHeight(40);
      xyFondo.setWidth(655);
    }
    else if (lineas == COLUMNAS_2) {
      xyFondo.setHeight(40);
      xyFondo.setWidth(550);
    }

    Inicializar();
  }

  // Sistema de bloqueo de eventos
  /** Este m�todo sirve para mantener una sincronizaci�n en los eventos */
  protected synchronized boolean bloquea() {
    if (sinBloquear) {
      sinBloquear = false;
      modoAnterior = modoOperacion;
      modoOperacion = MODO_ESPERA;

      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      return true;
    }
    else {
      return false;
    }
  }

  /** Este m�todo desbloquea el sistema */
  protected synchronized void desbloquea() {
    sinBloquear = true;
    if (modoOperacion == MODO_ESPERA) {
      modoOperacion = modoAnterior;
    }
    Inicializar();
    if (modoOperacion == MODO_ESPERA) {
      modoContenedor = contenedor.ponerEnEspera();
    }
    else if (modoContenedor != -1) {
      contenedor.ponerModo(modoContenedor);

    }
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  // Borrado de las cajas de texto
  private void borraNivel2() {
    if (tipoOperacion >= TIPO_NIVEL_2) {
      txtNivel2.setText("");
      txtValueChangeNivel2();
    }
  }

  private void borraNivel4() {
    if (tipoOperacion == TIPO_NIVEL_4) {
      txtNivel4.setText("");
      txtValueChangeNivel4();
    }
  }

//**************************************************************************
//          M�todos asociados a la gesti�n de eventos
//**************************************************************************

    // Para el interfaz "usaPanelNiveles"
    // informado ---> informado : resetear el suca
    // vacio ---> informado : nada
    // informado ---> vacio : nada

    protected void txtDesValueChangeNivel1() {
      //// System_out.println("  txtDesValueChangeNivel1 antes:" + antTxtNivel1L + " despues:" + txtNivel1L.getText());

      if (!antantTxtNivel1L.equals(new String())) { //antes informado
        if (!txtNivel1L.getText().equals(new String())) { //ahora informado
          contenedor.cambioNivelAntesInformado(1);
        }
      }
      antantTxtNivel1L = antTxtNivel1L;
      antTxtNivel1L = txtNivel1L.getText().trim();
    }

  protected void txtDesValueChangeNivel2() {

    //// System_out.println("  txtDesValueChangeNivel2 antes:" + antTxtNivel2L + " despues:" + txtNivel2L.getText());
    if (!antantTxtNivel2L.equals(new String())) { //antes informado
      if (!txtNivel2L.getText().equals(new String())) { //ahora informado
        contenedor.cambioNivelAntesInformado(2);
      }
    }
    antantTxtNivel2L = antTxtNivel2L;
    antTxtNivel2L = txtNivel2L.getText().trim();
  }

  protected void txtDesValueChangeNivel4() {
    //// System_out.println("  txtDesValueChangeNivel3 antes:" + antTxtNivel3L + " despues:" + txtNivel3L.getText());
    if (!antantTxtNivel4L.equals(new String())) { //antes informado
      if (!txtNivel4L.getText().equals(new String())) { //ahora informado
        contenedor.cambioNivelAntesInformado(3);
      }
    }
    antantTxtNivel4L = antTxtNivel4L;
    antTxtNivel4L = txtNivel4L.getText().trim();
  }

  // Operaciones realizadas al pulsar en las cajas de texto.
  protected void txtValueChangeNivel1() {
    if (!bHabilitado) {
      return;
    }
    if (txtNivel1.getText().trim().equals("")) {
      strCDNivel1 = "";
      contenedor.cambioNivelAntesInformado(1); //E 23/12/99
    }
    txtNivel1L.setText("");
    if (tipoOperacion >= TIPO_NIVEL_2) {
      txtNivel2.setText("");
      txtNivel2L.setText("");
      if (tipoOperacion == TIPO_NIVEL_4) {
        txtNivel4.setText("");
        txtNivel4L.setText("");
      }
    }
    modoOperacion = MODO_INICIO;
  }

  protected void txtValueChangeNivel2() {
    if (!bHabilitado) {
      return;
    }
    if (txtNivel1L.getText().equals("")) {
      return;
    }
    if (tipoOperacion >= TIPO_NIVEL_2) {
      if (txtNivel2.getText().trim().equals("")) {
        strCDNivel2 = "";
        contenedor.cambioNivelAntesInformado(2); //E 23/12/99
      }
      txtNivel2L.setText("");
      if (tipoOperacion == TIPO_NIVEL_4) {
        txtNivel4.setText("");
        txtNivel4L.setText("");
      }
    }
    modoOperacion = MODO_N1;
  }

  protected void txtValueChangeNivel4() {
    if (!bHabilitado) {
      return;
    }
    if (txtNivel2L.getText().equals("")) {
      return;
    }
    if (tipoOperacion == TIPO_NIVEL_4) {
      if (txtNivel4.getText().trim().equals("")) {
        strCDNivel4 = "";
      }
      txtNivel4L.setText("");
    }
  }

  // Nivel 1
  protected void btnActionNivel1() {
    DataNivel1 dataDN1 = null;
    DataEntradaEDO dataDEE = null;
    CMessage mensaje = null;

    try {
      if (bAutorizacion) {
        CListaNiveles1 lista = new CListaNiveles1(app,
                                                  "Selecci�n de " +
                                                  app.getNivel1(), stubCliente,
                                                  strSERVLETAut,
                                                  servletOBTENER_X_CODIGO,
                                                  servletOBTENER_X_DESCRIPCION,
                                                  servletSELECCION_X_CODIGO,
            servletSELECCION_X_DESCRIPCION);
        lista.show();
        dataDEE = (DataEntradaEDO) lista.getComponente();
      }
      else {
        CListaNivel1 lista = new CListaNivel1(app,
                                              "Selecci�n de " + app.getNivel1(),
                                              stubCliente,
                                              strSERVLETNivel1,
                                              servletOBTENER_X_CODIGO,
                                              servletOBTENER_X_DESCRIPCION,
                                              servletSELECCION_X_CODIGO,
                                              servletSELECCION_X_DESCRIPCION);
        lista.show();
        dataDN1 = (DataNivel1) lista.getComponente();
      }
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }

    borraNivel2(); // E 22/12/1999
    if (bAutorizacion) {
      if (dataDEE != null) {
        txtNivel1.setText(dataDEE.getCod());
        txtNivel1L.setText(dataDEE.getDes());
        //borraNivel2();
        modoOperacion = MODO_N1;
      }
      else {
        txtNivel1.setText("");
        txtNivel1L.setText("");
      }
    }
    else {
      if (dataDN1 != null) {
        txtNivel1.setText(dataDN1.getCod());
        txtNivel1L.setText(dataDN1.getDes());
        //borraNivel2();
        modoOperacion = MODO_N1;
      }
      else {
        txtNivel1.setText("");
        txtNivel1L.setText("");
      }
    }
  }

  public void txtFocusNivel1() {
    DataNivel1 dataDN1 = null;
    DataEntradaEDO dataDEE = null;

    CLista param = null;
    CMessage msg;

    if (!strCDNivel1.equals(txtNivel1.getText())) {
      contenedor.cambioNivelAntesInformado(1); //E 23/12/99
      if (txtNivel1.getText().trim().length() > 0) {
        param = new CLista();
        if (bAutorizacion) {
          param.setPerfil(app.getPerfil());
          param.setLogin(app.getLogin());
          param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES()); // E
          param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES()); // E

          param.addElement(new DataEntradaEDO(txtNivel1.getText()));
        }
        else {
          param.addElement(new DataNivel1(txtNivel1.getText()));
        }
      }

      if (param != null) {
        try {
          if (bAutorizacion) {
            stubCliente.setUrl(new URL(app.getURL() + strSERVLETAut));
          }
          else {
            stubCliente.setUrl(new URL(app.getURL() + strSERVLETNivel1));
          }
          param = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO, param);

          if (param.size() > 0) {
            if (bAutorizacion) {
              dataDEE = (DataEntradaEDO) param.firstElement();
              txtNivel1L.setText(dataDEE.getDes());
            }
            else {
              dataDN1 = (DataNivel1) param.firstElement();
              txtNivel1L.setText(dataDN1.getDes());
            }
            borraNivel2();
            modoOperacion = MODO_N1;
          }
          else {
            msg = new CMessage(this.app, CMessage.msgAVISO, "No hay datos.");
            msg.show();
            msg = null;
            txtNivel1.setText("");
            txtNivel1L.setText("");
          }
        }
        catch (Exception ex) {
          msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
          msg.show();
          msg = null;
        }
      }
    }
  }

  protected void txtGotFocusNivel1() {
    strCDNivel1 = txtNivel1.getText().trim();
  }

  protected void txtGotFocusNivel2() {
    strCDNivel2 = txtNivel2.getText().trim();
  }

  protected void txtGotFocusNivel4() {
    strCDNivel4 = txtNivel4.getText().trim();
  }

  //********** Nivel 2
   protected void btnActionNivel2() {
     DataZBS2 dataDN2 = null;
     DataEntradaEDO dataDEE = null;
     CMessage msgBox = null;

     try {
       if (bAutorizacion) {
         CListaNiveles2 lista = new CListaNiveles2(this,
             "Selecci�n de " + app.getNivel2(), stubCliente,
             strSERVLETAut, servletOBTENER_NIV2_X_CODIGO,
             servletOBTENER_NIV2_X_DESCRIPCION, servletSELECCION_NIV2_X_CODIGO,
             servletSELECCION_NIV2_X_DESCRIPCION);
         lista.show();
         dataDEE = (DataEntradaEDO) lista.getComponente();
       }
       else {
         CListaNivel2 lista = new CListaNivel2(this,
                                               "Selecci�n de " + app.getNivel2(),
                                               stubCliente,
                                               strSERVLETNivel2,
                                               servletOBTENER_NIV2_X_CODIGO,
             servletOBTENER_NIV2_X_DESCRIPCION, servletSELECCION_NIV2_X_CODIGO,
             servletSELECCION_NIV2_X_DESCRIPCION);
         lista.show();
         dataDN2 = (DataZBS2) lista.getComponente();
       }
     }
     catch (Exception er) {
       er.printStackTrace();
       msgBox = new CMessage(getApp(), CMessage.msgERROR, er.getMessage());
       msgBox.show();
       msgBox = null;
     }

     if (bAutorizacion) {
       if (dataDEE != null) {
         txtNivel2.setText(dataDEE.getCod());
         txtNivel2L.setText(dataDEE.getDes());
         //borraNivel3();
         modoOperacion = MODO_N2;
       }
       else {
         txtNivel2.setText("");
         txtNivel2L.setText("");
       }

     }
     else {
       if (dataDN2 != null) {
         txtNivel2.setText(dataDN2.getNiv2());
         txtNivel2L.setText(dataDN2.getDes());
         //borraNivel3();
         modoOperacion = MODO_N2;
       }
       else {
         txtNivel2.setText("");
         txtNivel2L.setText("");
       }
     }
   }

  public void txtFocusNivel2() {
    DataZBS2 dataDN2 = null;
    DataEntradaEDO dataDEE = null;
    CLista param = null;
    CMessage msg;

    if (!strCDNivel2.equals(txtNivel2.getText().trim())) {
      contenedor.cambioNivelAntesInformado(2); //E 23/12/99
      if (txtNivel2.getText().length() > 0) {
        param = new CLista();
        if (bAutorizacion) {
          param.setPerfil(app.getPerfil());
          param.setLogin(app.getLogin());

          param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES()); // E
          param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES()); // E

          param.addElement(new DataEntradaEDO(txtNivel2.getText(), "",
                                              txtNivel1.getText()));
        }
        else {
          param.addElement(new DataZBS2(txtNivel1.getText(), txtNivel2.getText(),
                                        "", ""));
        }
      }

      if (param != null) {
        try {
          if (bAutorizacion) {
            stubCliente.setUrl(new URL(app.getURL() + strSERVLETAut));
          }
          else {
            stubCliente.setUrl(new URL(app.getURL() + strSERVLETNivel2));
          }

          param = (CLista) stubCliente.doPost(servletOBTENER_NIV2_X_CODIGO,
                                              param);

          if (param.size() > 0) {
            if (bAutorizacion) {
              dataDEE = (DataEntradaEDO) param.firstElement();
              txtNivel2.setText(dataDEE.getCod());
              txtNivel2L.setText(dataDEE.getDes());
            }
            else {
              dataDN2 = (DataZBS2) param.firstElement();
              txtNivel2.setText(dataDN2.getNiv2());
              txtNivel2L.setText(dataDN2.getDes());
            }
            borraNivel4();
            modoOperacion = MODO_N2;
          }
          else {
            msg = new CMessage(this.app, CMessage.msgAVISO, "No hay datos.");
            msg.show();
            msg = null;
            txtNivel2.setText("");
            txtNivel2L.setText("");
          }
        }
        catch (Exception ex) {
          msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
          msg.show();
          msg = null;
        }
      }
    }
  }

  // Nivel 4
  protected void btnActionNivel4() {
    DataZBS data = null;
    CMessage msgBox = null;
    CListaNivel4 lista = null;

    try {
      lista = new CListaNivel4(this, "Selecci�n de " + app.getNivel3(),
                               stubCliente, strSERVLETZona,
                               servletOBTENER_NIV2_X_CODIGO,
                               servletOBTENER_NIV2_X_DESCRIPCION,
                               servletSELECCION_NIV2_X_CODIGO,
                               servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS) lista.getComponente();

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtNivel4.setText(data.getCod());
      txtNivel4L.setText(data.getDes());
      modoOperacion = MODO_N4;
    }
    else {
      txtNivel4.setText("");
      txtNivel4L.setText("");
    }
  }

  protected void txtFocusNivel4() {
    DataZBS zbs;
    CLista param = null;
    CMessage msg;

    if (!strCDNivel4.equals(txtNivel4.getText().trim())) {
      modoOperacion = MODO_N2;

      if (txtNivel4.getText().length() > 0) {
        param = new CLista();
        param.addElement(new DataZBS(txtNivel4.getText(), "", "",
                                     txtNivel1.getText(), txtNivel2.getText()));
      }
      if (param != null) {
        try {
          stubCliente.setUrl(new URL(app.getURL() + strSERVLETZona));
          param = (CLista) stubCliente.doPost(servletOBTENER_NIV2_X_CODIGO,
                                              param);

          if (param.size() > 0) {
            zbs = (DataZBS) param.firstElement();
            txtNivel4.setText(zbs.getCod());
            txtNivel4L.setText(zbs.getDes());
            modoOperacion = MODO_N4;
          }
          else {
            msg = new CMessage(this.app, CMessage.msgAVISO, "No hay datos.");
            msg.show();
            msg = null;
            txtNivel4.setText("");
            txtNivel4L.setText("");
          }
        }
        catch (Exception ex) {
          msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
          msg.show();
          msg = null;
        }
      }
    }
  }

} // FIN CLASE panelNiveles

//**************************************************************************
//          Clases para la gesti�n de eventos
//**************************************************************************

// Para comunicar al los contenedores de este panel, que implementan
// el interfaz usaPanelNiveles, que ha habido un cambio en alguno de los niveles
  class PanelNivelesDesTextListener
      implements java.awt.event.TextListener {
    panelNiveles adaptee = null;
    TextEvent e = null;

    PanelNivelesDesTextListener(panelNiveles adaptee) {
      this.adaptee = adaptee;
    }

    public void textValueChanged(java.awt.event.TextEvent e) {
      String name2 = ( (Component) e.getSource()).getName();

      if (name2.equals("desNivel1")) {
        adaptee.txtDesValueChangeNivel1();
      }
      else if (name2.equals("desNivel2")) {
        adaptee.txtDesValueChangeNivel2();
      }
      else if (name2.equals("desNivel4")) {
        adaptee.txtDesValueChangeNivel4();
      }

      adaptee.desbloquea();
    }

  }

class PanelNivelesTextListener
    implements java.awt.event.TextListener {
  panelNiveles adaptee = null;
  TextEvent e = null;

  PanelNivelesTextListener(panelNiveles adaptee) {
    this.adaptee = adaptee;
  }

  public void textValueChanged(java.awt.event.TextEvent e) {
    String name2 = ( (Component) e.getSource()).getName();

    if (name2.equals("nivel1")) {
      adaptee.txtValueChangeNivel1();
    }
    else if (name2.equals("nivel2")) {
      adaptee.txtValueChangeNivel2();
    }
    else if (name2.equals("nivel4")) {
      adaptee.txtValueChangeNivel4();
    }

    adaptee.desbloquea();
  }

}

// P�rdida de foco en las cajas de texto
class PanelNivelesTextFocusListener
    implements java.awt.event.FocusListener {
  panelNiveles adaptee;
  String name2 = null;
  protected StubSrvBD stub = new StubSrvBD();

  PanelNivelesTextFocusListener(panelNiveles adaptee) {
    this.adaptee = adaptee;
  }

  public void focusGained(FocusEvent e) {
    name2 = ( (Component) e.getSource()).getName();
    if (name2.equals("nivel1")) {
      adaptee.txtGotFocusNivel1();
    }
    else if (name2.equals("nivel2")) {
      adaptee.txtGotFocusNivel2();
    }
    else if (name2.equals("nivel4")) {
      adaptee.txtGotFocusNivel4();
    }
  }

  public void focusLost(FocusEvent e) {
    if (adaptee.bloquea()) {
      name2 = ( (Component) e.getSource()).getName();

      CLista lalista = null;
      String campo = null;
      CLista result = null, data = new CLista();

      try {
        adaptee.Inicializar();
        if (adaptee.modoOperacion == adaptee.MODO_ESPERA) {
          adaptee.modoContenedor = adaptee.contenedor.ponerEnEspera();
        }
        else if (adaptee.modoContenedor != -1) {
          adaptee.contenedor.ponerModo(adaptee.modoContenedor);
        }
        if (name2.equals("nivel1")) {
          adaptee.txtFocusNivel1();
        }
        else if (name2.equals("nivel2")) {
          adaptee.txtFocusNivel2();
        }
        else if (name2.equals("nivel4")) {
          adaptee.txtFocusNivel4();
        }
      }
      catch (Exception exc) {
        exc.printStackTrace();
      }
      adaptee.desbloquea();
    }
  }

} //_______________________________________________ END_CLASS

// Eventos sobre los botones
class PanelNivelesBtnActionListener
    implements ActionListener {
  panelNiveles adaptee = null;
  ActionEvent e = null;

  public PanelNivelesBtnActionListener(panelNiveles adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    if (adaptee.bloquea()) {
      this.e = e;

      String name = e.getActionCommand();
      CLista lalista = null;
      String campo = null;

      adaptee.Inicializar();
      if (adaptee.modoOperacion == adaptee.MODO_ESPERA) {
        adaptee.modoContenedor = adaptee.contenedor.ponerEnEspera();
      }
      else if (adaptee.modoContenedor != -1) {
        adaptee.contenedor.ponerModo(adaptee.modoContenedor);

      }
      if (name.equals("nivel1")) {
        adaptee.btnActionNivel1();
      }
      else if (name.equals("nivel2")) {
        adaptee.btnActionNivel2();
      }
      else if (name.equals("nivel4")) {
        adaptee.btnActionNivel4();
      }

      adaptee.desbloquea();
    }
  }

} //__________________________________________________ END_CLASS

class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a, String title, StubSrvBD stub, String servlet,
                      int obtener_x_codigo, int obtener_x_descricpcion,
                      int seleccion_x_codigo, int seleccion_x_descripcion) {

    super(a, title, stub, servlet, obtener_x_codigo, obtener_x_descricpcion,
          seleccion_x_codigo, seleccion_x_descripcion);

    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

class CListaNivel2
    extends CListaValores {

  protected panelNiveles panel;

  public CListaNivel2(panelNiveles p, String title, StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo, int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {

    super(p.getApp(), title, stub, servlet, obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo, seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtNivel1.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

class CListaNivel4
    extends CListaValores {

  protected panelNiveles panel;

  public CListaNivel4(panelNiveles p, String title, StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo, int obtener_x_descricpcion,
                      int seleccion_x_codigo, int seleccion_x_descripcion) {

    super(p.getApp(), title, stub, servlet, obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo, seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();
  }

  public Object setComponente(String s) {
    return new DataZBS(s, "", "", panel.txtNivel1.getText(),
                       panel.txtNivel2.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS) o).getDes());
  }
}

// Clases empleadas para acceder con bAutorizaciones = true
class CListaNiveles1
    extends CListaValores {

  public CListaNiveles1(CApp a,
                        String title,
                        StubSrvBD stub,
                        String servlet,
                        int obtener_x_codigo,
                        int obtener_x_descripcion,
                        int seleccion_x_codigo,
                        int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descripcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed();

  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}

class CListaNiveles2
    extends CListaValores {

  protected panelNiveles panel;

  public CListaNiveles2(panelNiveles p,
                        String title,
                        StubSrvBD stub,
                        String servlet,
                        int obtener_x_codigo,
                        int obtener_x_descricpcion,
                        int seleccion_x_codigo,
                        int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataEntradaEDO(s, "", panel.txtNivel1.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataEntradaEDO) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEntradaEDO) o).getDes());
  }
}
