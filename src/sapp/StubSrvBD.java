
package sapp;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.net.URLConnection;

import capp.CLista;

public class StubSrvBD {

  protected URL url = null;

  // contruye la url que apunta al servlet
  public StubSrvBD(URL u) {
    url = u;
  }

  public StubSrvBD() {
    url = null;
  }

  // ejecuta el servicio
  public Object doPost(int opmode, CLista param) throws Exception {
    Object data = null;
    URLConnection con = null;
    ObjectInputStream in = null;
    ObjectOutputStream out = null;

    // abre la conexi�n con el servlet
    con = url.openConnection();
    con.setRequestProperty("Content-type", "application/octet-stream");
    con.setDoOutput(true);
    con.setUseCaches(false);

    // invoca el modo de operaci�n del servlet
    out = new ObjectOutputStream(con.getOutputStream());
    out.writeInt(opmode);
    out.flush();

    // envia los par�metros
    param.trimToSize();
    out.writeObject(param);
    out.flush();
    out.close();

    // lee los par�metros de retorno
    in = new ObjectInputStream( (InputStream) con.getInputStream());
    data = in.readObject();

    // cierra los streams
    in.close();

    // levanta la excepci�n producida en el servlet
    if (data != null) {
      if (data.getClass().getName().equals("java.lang.Exception")) {
        throw (Exception) data;
      }
    }

    return (data);
  }

  // cambio de url
  public void setUrl(URL u) {
    url = u;
  }
}
