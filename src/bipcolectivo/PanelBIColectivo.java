package bipcolectivo;

import java.net.URL;
import java.util.Hashtable;
import java.util.ResourceBundle;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.ScrollPane;
import java.awt.TextField;

import bidata.DataBrote;
import bidata.DataCDDS;
import bidial.DialogInvesInfor;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import capp.CPanel;
import notdata.DataMunicipioEDO;
import notutil.Comunicador;
import pannivelesedo.panelNiveles;
import pannivelesedo.usaPanelNiveles;
import sapp.StubSrvBD;
import suca.panelsuca;
import suca.zonificacionSanitaria;

public class PanelBIColectivo
    extends CPanel
    implements zonificacionSanitaria,
    usaPanelNiveles {

  //Almacen Zonificacion
  public String sN1Bk = "";
  ResourceBundle res;
  public String sN2Bk = "";

  final int modoESPERA = 2;
  final int modoALTA = 3;
  final int modoMODIFICACION = 4;
  final int modoBAJA = 5;
  final int modoCONSULTA = 6;

  final String strSERVLET_MUNICIPIO_CONT = "servlet/SrvMunicipioCont";
  //Antes de espera en suca Guardar el modo
  public int iAntesDeZona = 0;

  protected int modoOperacion = modoALTA;
  protected StubSrvBD stubCliente = null;

  // dialog que contiene a este panel
  DialogInvesInfor dlgB;

  //suca
  panelsuca suca = null;
  CLista listaSuca = null;

  //niveles
  panelNiveles panNivCol = null;
  public panelNiveles panNivExp = null;

  //scroll
  ScrollPane panelScroll = new ScrollPane();
  Panel panel = new Panel();

  CLista listaColectivo = new CLista();

  // controles
  XYLayout xYLayout = new XYLayout();
  Label lColectivo = new Label();

  TextField txtColectivo = new TextField();
  XYLayout xYLayout1 = new XYLayout();
  XYLayout xYLayout2 = new XYLayout();

  Hashtable hash = null;

  Label lTelCol = new Label();
  TextField txtTelCol = new TextField();
  Label lTColec = new Label();

  Choice choColectivo = new Choice();

  CApp capp = null;
  Label lLocCol = new Label();
  Label lLocExp = new Label();

  // constructor
  public PanelBIColectivo(DialogInvesInfor dlg,
                          int modo,
                          Hashtable hsCompleta) {
    try {
      res = ResourceBundle.getBundle("bipcolectivo.Res" +
                                     dlg.getCApp().getIdioma());
      setApp(dlg.getCApp());
      dlgB = dlg;

      //el suca espera una lista con TODAS las listas
      listaSuca = new CLista();
      listaSuca.addElement( (CLista) hsCompleta.get("PAISES"));
      listaSuca.addElement( (CLista) hsCompleta.get("CA"));
      listaSuca.addElement(new CLista());
      listaSuca.addElement(new CLista());
      listaSuca.addElement(new CLista());
      //(CApp a, boolean bTramero, int modoOperacion, boolean bSeleccionPais, CLista datosListas, zonificacionSanitaria zs) {
      suca = new panelsuca(dlg.getCApp(), false, suca.modoINICIO, false,
                           listaSuca, this);

      panNivCol = new panelNiveles(dlg.getCApp(), this,
                                   panelNiveles.MODO_INICIO,
                                   panelNiveles.TIPO_NIVEL_3,
                                   panelNiveles.LINEAS_1, true);
      panNivExp = new panelNiveles(dlg.getCApp(), this,
                                   panelNiveles.MODO_INICIO,
                                   panelNiveles.TIPO_NIVEL_3,
                                   panelNiveles.LINEAS_1, true);

      listaColectivo = (CLista) hsCompleta.get("TCOLECTIVO");

      hash = hsCompleta;

      stubCliente = new StubSrvBD(new URL(this.app.getURL() +
                                          strSERVLET_MUNICIPIO_CONT));

      jbInit();
      modoOperacion = modo;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(755, 280));

    xYLayout.setHeight(270); //270
    xYLayout.setWidth(735);

    xYLayout2.setHeight(355);
    xYLayout2.setWidth(600);

    panelScroll.setSize(735, 270); //270
    panel.setBounds(new Rectangle(0, 2, 731, 355));
    panel.setSize(new Dimension(600, 530));

    DataCDDS aux = null;
    for (int i = 0; i < listaColectivo.size(); i++) {
      aux = (DataCDDS) listaColectivo.elementAt(i);
      choColectivo.addItem(aux.getCD() + " " + aux.getDS());
    }

    lColectivo.setText(res.getString("lColectivo.Text"));
    panel.setLayout(xYLayout2);
    lTelCol.setText(res.getString("lTelCol.Text"));
    choColectivo.setBackground(new Color(255, 255, 150));
    lLocCol.setForeground(Color.black);
    lLocCol.setFont(new Font("Dialog", 0, 12));
    lLocCol.setText(res.getString("lLocCol.Text"));
    lLocExp.setForeground(Color.black);
    lLocExp.setFont(new Font("Dialog", 0, 12));
    lLocExp.setText(res.getString("lLocExp.Text"));
    lTColec.setForeground(Color.black);
    lTColec.setFont(new Font("Dialog", 0, 12));
    lTColec.setText(res.getString("lTColec.Text"));
    this.setLayout(xYLayout);

    this.add(panelScroll, new XYConstraints(0, 0, 735, 270)); //270
    panelScroll.add(panel, new XYConstraints(0, 0, 600, 355));

    panel.add(lColectivo, new XYConstraints(9, 5, -1, -1));
    panel.add(txtColectivo, new XYConstraints(122, 7, 275, -1));
    panel.add(lTelCol, new XYConstraints(480, 7, 63, -1));
    panel.add(txtTelCol, new XYConstraints(549, 5, 130, -1));
    panel.add(lTColec, new XYConstraints(9, 35, 97, -1));
    panel.add(choColectivo, new XYConstraints(122, 35, 205, -1));
    panel.add(suca, new XYConstraints(9, 75, 596, 116));
    panel.add(lLocCol, new XYConstraints(9, 195, 220, -1));
    panel.add(panNivCol, new XYConstraints(9, 220, 655, 40));
    panel.add(lLocExp, new XYConstraints(9, 275, 217, -1));
    panel.add(panNivExp, new XYConstraints(9, 300, 655, 40));

    setBorde(false);

  }

//devuelve el cod del colectivo
  public String getColec() {

    String iG = "";
    iG = choColectivo.getSelectedItem().trim().substring(0, 1);

    return (iG);
  } //getColec

  public DataBrote recogerDatos(DataBrote resul) {

    resul.insert("DS_NOMCOL", txtColectivo.getText().trim());
    resul.insert("CD_TIPOCOL", getColec());
    resul.insert("DS_TELCOL", txtTelCol.getText().trim());

    resul.insert("CD_CACOL", suca.getCD_CA().trim());
    resul.insert("CD_PROVCOL", suca.getCD_PROV().trim());
    resul.insert("CD_MUNCOL", suca.getCD_MUN().trim());
    resul.insert("DS_MUNCOL", suca.getDS_MUN().trim());
    resul.insert("DS_DIRCOL", suca.getDS_DIREC().trim());
    resul.insert("DS_NMCALLE", suca.getDS_NUM().trim());
    resul.insert("DS_PISOCOL", suca.getDS_PISO());
    resul.insert("CD_POSTALCOL", suca.getCD_POSTAL().trim());

    resul.insert("CD_NIVEL_1_LCA", panNivCol.getCDNivel1());
    resul.insert("CD_NIVEL_2_LCA", panNivCol.getCDNivel2());
    resul.insert("CD_ZBS_LCA", panNivCol.getCDNivel3());

    resul.insert("CD_NIVEL_1_LE", panNivExp.getCDNivel1());
    resul.insert("CD_NIVEL_2_LE", panNivExp.getCDNivel2());
    resul.insert("CD_ZBS_LE", panNivExp.getCDNivel3());

    return resul;

  }

  boolean ValidarDatosSuca(panelsuca nombre) {

//1.Longitud
    if (nombre.getDS_DIREC().length() > 50) {
      ShowError(res.getString("msg1.Text"));
      nombre.setDS_DIREC("");
      nombre.requestFocus();
      return false;
    }
    if (nombre.getDS_NUM().length() > 6) {
      ShowError(res.getString("msg2.Text"));
      nombre.setDS_NUM("");
      nombre.requestFocus();
      return false;
    }
    if (nombre.getDS_PISO().length() > 4) {
      ShowError(res.getString("msg3.Text"));
      nombre.setDS_PISO("");
      nombre.requestFocus();
      return false;
    }
    if (nombre.getCD_POSTAL().length() > 5) {
      ShowError(res.getString("msg4.Text"));
      nombre.setCD_POSTAL("");
      nombre.requestFocus();
      return false;
    }
    /*//2.Numerica
     if (!nombre.getDS_NUM().equals("")){
      boolean b = isNum(nombre.getDS_NUM());
      if (!b){
          ShowError("El N�mero de la calle debe ser un valor Entero");
          nombre.setDS_NUM("");
          nombre.requestFocus();
          return false;
      }
     }
     //***
     if (!nombre.getDS_PISO().equals("")){
       boolean b = isNum(nombre.getDS_PISO());
       if (!b){
           ShowError("El Piso debe ser un valor Entero");
           nombre.setDS_PISO("");
           nombre.requestFocus();
           return false;
       }
      }
      //***
      //***
       if (!nombre.getCD_POSTAL().equals("")){
         boolean b = isNum(nombre.getCD_POSTAL());
         if (!b){
             ShowError("El C�digo Postal debe ser un valor Entero");
             nombre.setCD_POSTAL("");
             nombre.requestFocus();
             return false;
         }
        }
        //***  */
       return true;
  } //fin clase

//return: false, es negativo o no numero
//return: true, es ok
  boolean isNum(String Numero) {
    try {
      Integer I = new Integer(Numero);
      int i = I.intValue();
      if (i < 0) {
        return false;
      }
    }
    catch (Exception e) {
      return false;
    }
    return true;
  }

  private void ShowError(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgERROR, sMessage);
    msgBox.show();
    msgBox = null;
  }

  public boolean validarDatos() {

    boolean correcto = true;

    //1. Perdida de foco en municipio
    if (!suca.getCD_MUN().equals("")) {
      if (suca.getDS_MUN().equals("")) {
        suca.setCD_MUN("");
        suca.requestFocus();
        return false;
      }
    }

    if (!panNivCol.getCDNivel1().equals("")) {
      if (panNivCol.getDSNivel1().equals("")) {
        panNivCol.setDSNivel1("");
        panNivCol.requestFocus();
        return false;
      }
    }
    if (!panNivCol.getCDNivel2().equals("")) {
      if (panNivCol.getDSNivel2().equals("")) {
        panNivCol.setDSNivel2("");
        panNivCol.requestFocus();
        return false;
      }
    }
    if (!panNivCol.getCDNivel3().equals("")) {
      if (panNivCol.getDSNivel3().equals("")) {
        panNivCol.setDSNivel3("");
        panNivCol.requestFocus();
        return false;
      }
    }

    if (!panNivExp.getCDNivel1().equals("")) {
      if (panNivExp.getDSNivel1().equals("")) {
        panNivExp.setDSNivel1("");
        panNivExp.requestFocus();
        return false;
      }
    }
    if (!panNivExp.getCDNivel2().equals("")) {
      if (panNivExp.getDSNivel2().equals("")) {
        panNivExp.setDSNivel2("");
        panNivExp.requestFocus();
        return false;
      }
    }
    if (!panNivExp.getCDNivel3().equals("")) {
      if (panNivExp.getDSNivel3().equals("")) {
        panNivExp.setDSNivel3("");
        panNivExp.requestFocus();
        return false;
      }
    }
    //2. Longitudes de los campos
    if (txtColectivo.getText().length() > 50) {
      ShowError(res.getString("msg5.Text"));
      txtColectivo.setText("");
      txtColectivo.requestFocus();
      return false;
    }
    if (txtTelCol.getText().length() > 14) {
      ShowError(res.getString("msg6.Text"));
      txtTelCol.setText("");
      txtTelCol.requestFocus();
      return false;
    }

    //suca (numericos y longitudes)
    if (!ValidarDatosSuca(suca)) {
      return false;
    }

    //3. Valores numericos
    if (!txtTelCol.getText().equals("")) {
      boolean b = isNum(txtTelCol.getText());
      if (!b) {
        ShowError(res.getString("msg7.Text"));
        txtTelCol.setText("");
        txtTelCol.requestFocus();
        return false;
      }
    }

    //Campos obligatorios (combo sin opcion de vacio, ptt no hace falta)
    if (getColec().trim().equals("")) {
      ShowError(res.getString("msg8.Text"));
      this.requestFocus();
      choColectivo.requestFocus();
      return false;
    }

    return correcto;

  }

  public void Inicializar(int modo) {
    modoOperacion = modo;
    Inicializar();
  }

  public void Inicializar() {

    switch (modoOperacion) {

      case modoESPERA:
        enableControls(false);
        suca.setModoEspera();
        panNivCol.setModoEspera();
        panNivExp.setModoEspera();
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

      case modoALTA:
      case modoMODIFICACION:
        enableControls(true);
        suca.setModoNormal();
        panNivCol.setModoNormal();
        panNivExp.setModoNormal();

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
      case modoCONSULTA:
        enableControls(false);
        suca.setModoDeshabilitado();
        //primero lo pongo a modo normal para que bHAbilitado=true
        //y despues lo pongo en modo deshabilitado
        panNivCol.setModoNormal();
        panNivExp.setModoNormal();
        panNivCol.setModoDeshabilitado();
        panNivCol.doLayout();
        panNivExp.setModoDeshabilitado();
        panNivExp.doLayout();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;
    }
  }

  private void enableControls(boolean en) {

    txtColectivo.setEnabled(en);
    choColectivo.setEnabled(en);
    txtTelCol.setEnabled(en);

  }

//dado un cod, situar la Choice en ese registro
//(es obligatorio-> select (i))
  public void selectColec(String cd) {
    if (cd.equals("")) {
      choColectivo.select(0);
    }
    else {
      for (int i = 0; i < listaColectivo.size(); i++) {
        DataCDDS data = (DataCDDS) listaColectivo.elementAt(i);
        if (data.getCD().trim().equals(cd.trim())) {
          choColectivo.select(i);
          break;
        }
      }
    }
  }

  public void rellena(DataBrote data) {

    String cdTCol = "";
    if (data.getCD_TIPOCOL() != null) {
      cdTCol = data.getCD_TIPOCOL();
    }
    selectColec(cdTCol);

    String dsNomCol = "";
    if (data.getDS_NOMCOL() != null) {
      dsNomCol = data.getDS_NOMCOL();
    }
    txtColectivo.setText(dsNomCol);

    String dsTelCol = "";
    if (data.getDS_TELCOL() != null) {
      dsTelCol = data.getDS_TELCOL();
    }
    txtTelCol.setText(dsTelCol);

    String cdCA = "";
    String cdProv = "";
    String cdMun = "";
    String dsMun = "";
    String dsDirCol = "";
    String dsNMCalle = "";
    String dsPiso = "";
    String cdPostalCol = "";

    if (data.getCD_CACOL() != null) {
      cdCA = data.getCD_CACOL();
    }
    suca.setCD_CA(cdCA);
    if (data.getCD_PROVCOL() != null) {
      cdProv = data.getCD_PROVCOL();
    }
    suca.setCD_PROV(cdProv);
    if (data.getCD_MUNCOL() != null) {
      cdMun = data.getCD_MUNCOL();
    }
    suca.setCD_MUN(cdMun);
    if (data.getDS_MUNCOL() != null) {
      dsMun = data.getDS_MUNCOL();
    }
    suca.setDS_MUN(dsMun);
    if (data.getDS_DIRCOL() != null) {
      dsDirCol = data.getDS_DIRCOL();
    }
    suca.setDS_DIREC(dsDirCol);
    if (data.getDS_NMCALLE() != null) {
      dsNMCalle = data.getDS_NMCALLE();
    }
    suca.setDS_NUM(dsNMCalle);
    if (data.getDS_PISOCOL() != null) {
      dsPiso = data.getDS_PISOCOL();
    }
    suca.setDS_PISO(dsPiso);
    if (data.getCD_POSTALCOL() != null) {
      cdPostalCol = data.getCD_POSTALCOL();
    }
    suca.setCD_POSTAL(cdPostalCol);

    String cdNiv1Col = "";
    String cdNiv2Col = "";
    String cdZbsCol = "";

    //poner el modo adecuado en el panel de niveles de colectivo
    if (data.getCD_NIVEL_1_LCA() != null) {
      cdNiv1Col = data.getCD_NIVEL_1_LCA();
      panNivCol.setModoOperacion(panNivExp.MODO_N1);
    }
    panNivCol.setCDNivel1(cdNiv1Col);

    if (data.getCD_NIVEL_2_LCA() != null) {
      cdNiv2Col = data.getCD_NIVEL_2_LCA();
      panNivCol.setModoOperacion(panNivExp.MODO_N2);
    }
    panNivCol.setCDNivel2(cdNiv2Col);

    if (data.getCD_ZBS_LCA() != null) {
      cdZbsCol = data.getCD_ZBS_LCA();
      panNivCol.setModoOperacion(panNivExp.MODO_N3);
    }
    panNivCol.setCDNivel3(cdZbsCol);

    String dsNiv1Col = "";
    String dsNiv2Col = "";
    String dsZbsCol = "";

    if (data.getDS_NIVEL_1_LCA() != null) {
      dsNiv1Col = data.getDS_NIVEL_1_LCA();
    }
    panNivCol.setDSNivel1(dsNiv1Col);

    if (data.getDS_NIVEL_2_LCA() != null) {
      dsNiv2Col = data.getDS_NIVEL_2_LCA();
    }
    panNivCol.setDSNivel2(dsNiv2Col);

    if (data.getDS_ZBS_LCA() != null) {
      dsZbsCol = data.getDS_ZBS_LCA();
    }
    panNivCol.setDSNivel3(dsZbsCol);

    String cdNiv1Exp = "";
    String cdNiv2Exp = "";
    String cdZbsExp = "";
    String dsNiv1Exp = "";
    String dsNiv2Exp = "";
    String dsZbsExp = "";

    //poner el modo adecuado en el panel de niveles de exposicion
    if (data.getCD_NIVEL_1_LE() != null) {
      cdNiv1Exp = data.getCD_NIVEL_1_LE();
      panNivExp.setModoOperacion(panNivExp.MODO_N1);
    }
    panNivExp.setCDNivel1(cdNiv1Exp);

    if (data.getCD_NIVEL_2_LE() != null) {
      cdNiv2Exp = data.getCD_NIVEL_2_LE();
      panNivExp.setModoOperacion(panNivExp.MODO_N2);
    }
    panNivExp.setCDNivel2(cdNiv2Exp);

    if (data.getCD_ZBS_LE() != null) {
      cdZbsExp = data.getCD_ZBS_LE();
      panNivExp.setModoOperacion(panNivExp.MODO_N3);
    }
    panNivExp.setCDNivel3(cdZbsExp);

    if (data.getDS_NIVEL_1_LE() != null) {
      dsNiv1Exp = data.getDS_NIVEL_1_LE();
    }
    panNivExp.setDSNivel1(dsNiv1Exp);

    if (data.getDS_NIVEL_2_LE() != null) {
      dsNiv2Exp = data.getDS_NIVEL_2_LE();
    }
    panNivExp.setDSNivel2(dsNiv2Exp);

    if (data.getDS_ZBS_LE() != null) {
      dsZbsExp = data.getDS_ZBS_LE();
    }
    panNivExp.setDSNivel3(dsZbsExp);

    sN1Bk = panNivExp.getCDNivel1().trim();
    sN2Bk = panNivExp.getCDNivel2().trim();

  } //fin

  private void ShowWarning(String sMessage) {
    CMessage msgBox = new CMessage(app, CMessage.msgAVISO, sMessage);
    msgBox.show();
    msgBox = null;
  }

  // para el interfaz zonificacion sanitaria
  public void setZonificacionSanitaria(String mun) {

    DataMunicipioEDO data;

    if (mun.trim().equals("")) {
      panNivCol.setCDNivel1("");
      panNivCol.setCDNivel2("");
      panNivCol.setCDNivel3("");
      panNivCol.setDSNivel1("");
      panNivCol.setDSNivel2("");
      panNivCol.setDSNivel3("");
    }
    else {

      try {

        CLista lista = new CLista();
        data = new DataMunicipioEDO(mun, "");
        lista.addElement(data);

        lista = Comunicador.Communicate(this.getApp(),
                                        stubCliente,
                                        0,
                                        strSERVLET_MUNICIPIO_CONT,
                                        lista);
        data = (DataMunicipioEDO) lista.firstElement();
      }
      catch (Exception e) {
        ShowWarning(res.getString("msg9.Text"));
        modoOperacion = iAntesDeZona;
        dlgB.Inicializar(modoOperacion); //mlm nuevo
        return;
      }

      //NIVELES *********

      panNivCol.setCDNivel1(data.getCodNivel1());
      panNivCol.setCDNivel2(data.getCodNivel2());
      panNivCol.setCDNivel3(data.getCodZBS());
      panNivCol.setDSNivel1(data.getDesNivel1());
      panNivCol.setDSNivel2(data.getDesNivel2());
      panNivCol.setDSNivel3(data.getDesZBS());

    } //else
    modoOperacion = iAntesDeZona;
    dlgB.Inicializar(modoOperacion); //mlm nuevo

  }

  public void cambiaModoTramero(boolean b) {
    //no se implementa porque siempre va a estar en el modo sin tramero
  }

  public void setZonificacionSanitaria(String cdmun, String cdprov) {
  }

  public int ponerEnEspera() {
    int m = modoOperacion;
    iAntesDeZona = m; //para saber en setZona...si alta o modif
    modoOperacion = modoESPERA;
    dlgB.Inicializar(modoOperacion); //mlm nuevo
    return m;
  }

  //cuando el componente se pone a espera por perdida de foco
  //o pulse de boton
  public void ponerModo(int modo) {
    modoOperacion = modo;
    dlgB.Inicializar(modoOperacion); //mlm nuevo
    Parche_Protocolo();
  }

  public void vialInformado() {}

  public void cambioNivelAntesInformado(int nivel) {}

  void Parche_Protocolo() {

    int modo = modoOperacion;

    //PARCHE: protocolo -----
    if (!panNivExp.getCDNivel1().trim().equals(sN1Bk) ||
        !panNivExp.getCDNivel2().trim().equals(sN2Bk)) {

      if (modo == modoMODIFICACION &&
          !dlgB.YaSalioMsgEnferSinProtocolo) {

        dlgB.borrarProtocolo("Colectivo");
        dlgB.Inicializar(modoESPERA);
        dlgB.addProtocolo("Colectivo");
        dlgB.Inicializar(modo);
      }

      sN1Bk = panNivExp.getCDNivel1().trim();
      sN2Bk = panNivExp.getCDNivel2().trim();
    }
    //-----------------------
  }

} //clase
