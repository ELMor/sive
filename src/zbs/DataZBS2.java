
package zbs;

import java.io.Serializable;

public class DataZBS2
    implements Serializable {
  protected String sCod1 = "";
  protected String sCod2 = "";
  protected String sCodZBS = "";
  protected String sDes = "";

  public DataZBS2() {
  }

  public DataZBS2(String niv1, String niv2, String zbs, String des) {
    sCod1 = niv1;
    sCod2 = niv2;
    sCodZBS = zbs;
    sDes = des;
  }

  public String getNiv1() {
    return sCod1;
  }

  public String getNiv2() {
    return sCod2;
  }

  public String getZBS() {
    return sCodZBS;
  }

  public String getDes() {
    return sDes;
  }
}
