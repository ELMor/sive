
package zbs;

import java.io.Serializable;

public class DataZBS
    implements Serializable {
  protected String sCod = "";
  protected String sDes = "";
  protected String sDesL = "";
  protected String sCod1 = "";
  protected String sDes1 = "";
  protected String sCod2 = "";
  protected String sDes2 = "";

  public DataZBS() {
  }

  public DataZBS(String cod) {
    sCod = cod;
  }

  public DataZBS(String cod, String des, String desL, String cod1, String des1,
                 String cod2, String des2) {
    sCod = cod;
    sDes = des;
    if (sDesL != null) {
      sDesL = desL;
    }
    sCod1 = cod1;
    sDes1 = des1;
    sCod2 = cod2;
    sDes2 = des2;
  }

  public DataZBS(String cod, String des, String desL, String cod1, String cod2) {
    sCod = cod;
    sDes = des;
    if (sDesL != null) {
      sDesL = desL;
    }
    sCod1 = cod1;
    sDes1 = null;
    sCod2 = cod2;
    sDes2 = null;
  }

  public String getCod() {
    return sCod;
  }

  public String getDes() {
    return sDes;
  }

  public String getDesL() {
    String s;

    s = sDesL;
    if (s == null) {
      s = "";

    }
    return s;
  }

  public String getCod1() {
    return sCod1;
  }

  public String getDes1() {
    return sDes1;
  }

  public String getCod2() {
    return sCod2;
  }

  public String getDes2() {
    return sDes2;
  }
}
