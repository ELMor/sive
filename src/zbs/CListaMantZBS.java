
package zbs;

import capp.CApp;
import capp.CListaMantenimiento;
import sapp.StubSrvBD;

public class CListaMantZBS
    extends CListaMantenimiento {
  // modos de operaci�n del servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // constantes del panel
  final String strSERVLET = "servlet/SrvZBS";

  // variables nivel1 y nivel2
  public String elNivel1 = "";
  public String elNivel2 = "";

  public CListaMantZBS(ZBS a) {
    super( (CApp) a,
          4,
          "100\n100\n100\n260",
          a.getNivel1() + "\n" + a.getNivel2() + a.res.getString("msg1.Text"),
          new StubSrvBD(),
          "servlet/SrvZBS",
          11,
          12);
    this.setBorde(false);
  }

  // a�ade una l�nea
  public void btnAnadir_actionPerformed() {
    DataZBS cat;
    modoOperacion = modoESPERA;
    Inicializar();
    DialZBS dial = new DialZBS(this.app, DialZBS.modoALTA);
    ( (DialZBS) dial).show();

    if ( (dial.bAceptar) && (dial.valido)) {
      cat = new DataZBS(dial.txtCod.getText(),
                        dial.txtDes.getText(),
                        dial.txtDesL.getText(),
                        dial.txtCod1.getText(),
                        dial.txtCod2.getText());

      lista.addElement(cat);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // modifica l�nea de la lista
  public void btnModificar_actionPerformed(int i) {
    DataZBS cat;

    modoOperacion = modoESPERA;
    Inicializar();
    DialZBS dial = new DialZBS(this.app, DialZBS.modoMODIFICAR);

    // rellena los datos
    cat = (DataZBS) lista.elementAt(i);
    dial.txtCod.setText(cat.getCod());
    dial.txtDes.setText(cat.getDes());
    dial.txtDesL.setText(cat.getDesL());
    dial.txtCod1.setText(cat.getCod1());
    dial.txtCod2.setText(cat.getCod2());
    dial.traerDescripciones();

    ( (DialZBS) dial).show();

    if ( (dial.bAceptar) && (dial.valido)) {
      cat = new DataZBS(dial.txtCod.getText(),
                        dial.txtDes.getText(),
                        dial.txtDesL.getText(),
                        dial.txtCod1.getText(),
                        dial.txtCod2.getText());

      lista.removeElementAt(i);
      lista.insertElementAt(cat, i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // borra l�nea de la lista
  public void btnBorrar_actionPerformed(int i) {
    DataZBS cat;

    modoOperacion = modoESPERA;
    Inicializar();
    DialZBS dial = new DialZBS(this.app, DialZBS.modoBAJA);

    // rellena los datos
    cat = (DataZBS) lista.elementAt(i);
    dial.txtCod.setText(cat.getCod());
    dial.txtDes.setText(cat.getDes());
    dial.txtDesL.setText(cat.getDesL());
    dial.txtCod1.setText(cat.getCod1());
    dial.txtCod2.setText(cat.getCod2());
    dial.traerDescripciones();

    ( (DialZBS) dial).show();

    if (dial.bAceptar) {
      lista.removeElementAt(i);
      RellenaTabla();
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  // rellena los par�metros para enviar al servlet
  public Object setComponente(String s) {
    return new DataZBS(s, "", "", elNivel1, elNivel2);
  }

  // formatea el componente para ser insertado en una fila
  public String setLinea(Object o) {
    DataZBS cat = (DataZBS) o;

    return (

        cat.getCod1() + "&" + cat.getCod2() + "&" + cat.getCod() + "&" +
        cat.getDes());
  }

}