package zbs;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import capp.CApp;
import capp.CCampoCodigo;
import capp.CPanel;

public class Pan_ZBS
    extends CPanel {
  CApp app = null;
  BorderLayout borderLayout1 = new BorderLayout();
  CListaMantZBS PanAbajo = null;
  PanelSuperior PanArriba = null;

  public Pan_ZBS(CApp a) {

    try {
      this.app = a;
      PanArriba = new PanelSuperior(this.app);
//      PanAbajo = new CListaMantZBS(this.app);
      PanAbajo = new CListaMantZBS( (ZBS) (this.app));
      PanArriba.txtNiv1.addFocusListener(new escuchaFoco(this));
      PanArriba.txtNiv2.addFocusListener(new escuchaFoco(this));
      PanArriba.txtNiv1.addKeyListener(new escuchaTecla(this));
      PanArriba.txtNiv2.addKeyListener(new escuchaTecla(this));
      PanArriba.btnNiv1.addActionListener(new escuchaBoton(this));
      PanArriba.btnNiv2.addActionListener(new escuchaBoton(this));
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setLayout(borderLayout1);
    this.add(PanArriba, BorderLayout.NORTH);
    this.add(PanAbajo, BorderLayout.SOUTH);
  }

  public void Inicializar() {
  }
}

class escuchaBoton
    implements java.awt.event.ActionListener, Runnable {
  Pan_ZBS adaptee;
  ActionEvent e;

  escuchaBoton(Pan_ZBS adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.PanAbajo.modoOperacion = adaptee.PanAbajo.modoESPERA;
    adaptee.PanAbajo.Inicializar();
    if (e.getActionCommand() == "nivel1") {
      adaptee.PanArriba.btnNiv1_actionPerformed(e);
      if (adaptee.PanArriba.txtDesNiv1.getText().compareTo("") != 0) {
        adaptee.PanAbajo.elNivel1 = adaptee.PanArriba.txtNiv1.getText();
      }
      else {
        adaptee.PanAbajo.elNivel1 = "";
      }
      if (adaptee.PanArriba.txtDesNiv2.getText().compareTo("") != 0) {
        adaptee.PanAbajo.elNivel2 = adaptee.PanArriba.txtNiv2.getText();
      }
      else {
        adaptee.PanAbajo.elNivel2 = "";
      }
    }
    else if (e.getActionCommand() == "nivel2") {
      adaptee.PanArriba.btnNiv2_actionPerformed(e);
      adaptee.PanAbajo.elNivel1 = adaptee.PanArriba.txtNiv1.getText();
      if (adaptee.PanArriba.txtDesNiv2.getText().compareTo("") != 0) {
        adaptee.PanAbajo.elNivel2 = adaptee.PanArriba.txtNiv2.getText();
      }
      else {
        adaptee.PanAbajo.elNivel2 = "";
      }
      //Tras elegir niv2 Se pasa el foco al panel de abajo
      adaptee.PanAbajo.requestFocus();

    }
    adaptee.PanAbajo.modoOperacion = adaptee.PanAbajo.modoNORMAL;
    adaptee.PanAbajo.Inicializar();
  }
}

class escuchaTecla
    extends java.awt.event.KeyAdapter {
  Pan_ZBS adaptee;

  escuchaTecla(Pan_ZBS adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.PanAbajo.modoOperacion = adaptee.PanAbajo.modoESPERA;
    adaptee.PanAbajo.Inicializar();
//    if (e.getComponent().getName().compareTo("textfield3") == 0){

    if ( ( (CCampoCodigo) (e.getSource())).getName().compareTo("txtNiv1") == 0) {
      adaptee.PanArriba.txtNiv1_keyPressed(e);
      adaptee.PanAbajo.elNivel1 = "";
      adaptee.PanAbajo.elNivel2 = "";
    }
    else if ( ( (CCampoCodigo) (e.getSource())).getName().compareTo("txtNiv2") ==
             0) {
      adaptee.PanArriba.txtNiv2_keyPressed(e);
      adaptee.PanAbajo.elNivel2 = "";
    }
    adaptee.PanAbajo.modoOperacion = adaptee.PanAbajo.modoNORMAL;
    adaptee.PanAbajo.Inicializar();
  }
}

class escuchaFoco
    extends java.awt.event.FocusAdapter
    implements Runnable {
  Pan_ZBS adaptee;
  FocusEvent e;

  escuchaFoco(Pan_ZBS adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.PanAbajo.modoOperacion = adaptee.PanAbajo.modoESPERA;
    adaptee.PanAbajo.Inicializar();

//#// System_out.println("Thread focus lost************");
//    if (e.getComponent().getName().compareTo("textfield3") == 0){

    if ( ( (CCampoCodigo) (e.getSource())).getName().compareTo("txtNiv1") == 0) {
//#// System_out.println(" Therad Se mete en focus lost************");
      adaptee.PanArriba.txtNiv1_focusLost(e);

      if (adaptee.PanArriba.txtDesNiv1.getText().compareTo("") != 0) {
        adaptee.PanAbajo.elNivel1 = adaptee.PanArriba.txtNiv1.getText();
      }
      else {
        adaptee.PanAbajo.elNivel1 = "";
      }
      if (adaptee.PanArriba.txtDesNiv2.getText().compareTo("") != 0) {
        adaptee.PanAbajo.elNivel2 = adaptee.PanArriba.txtNiv2.getText();
      }
      else {
        adaptee.PanAbajo.elNivel2 = "";
      }
    }

    else if ( ( (CCampoCodigo) (e.getSource())).getName().compareTo("txtNiv2") ==
             0) {
      adaptee.PanArriba.txtNiv2_focusLost(e);
      if (adaptee.PanArriba.txtDesNiv1.getText().compareTo("") != 0) {
        adaptee.PanAbajo.elNivel1 = adaptee.PanArriba.txtNiv1.getText();
      }
      else {
        adaptee.PanAbajo.elNivel1 = "";
      }
      if (adaptee.PanArriba.txtDesNiv2.getText().compareTo("") != 0) {
        adaptee.PanAbajo.elNivel2 = adaptee.PanArriba.txtNiv2.getText();
        //Tras elegir niv2, si es distint de blanco  Se pasa el foco al panel de abajo
        adaptee.PanAbajo.requestFocus();
      }
      else {
        adaptee.PanAbajo.elNivel2 = "";
      }
    }
    adaptee.PanAbajo.modoOperacion = adaptee.PanAbajo.modoNORMAL;
    adaptee.PanAbajo.Inicializar();
  }
}
