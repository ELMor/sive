package zbs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvZBS
    extends DBServlet {

  // modos de operaci�n del servlet
  public static final int servletALTA = 0;
  public static final int servletMODIFICAR = 1;
  public static final int servletBAJA = 2;
  public static final int servletOBTENER_X_CODIGO = 3;
  public static final int servletOBTENER_X_DESCRIPCION = 4;
  public static final int servletSELECCION_X_CODIGO = 5;
  public static final int servletSELECCION_X_DESCRIPCION = 6;
  public static final int servletSELECCION_NIV2_X_CODIGO = 7;
  public static final int servletOBTENER_NIV2_X_CODIGO = 8;
  public static final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  public static final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  public static final int servletSELECCION_COMPLETA_CODIGO = 11;
  public static final int servletSELECCION_COMPLETA_DESCRIPCION = 12;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    CLista data = null;
    DataZBS zbs = null;

    // querys
    // ARG: uppers (8/5/02)
    // nuevo item
    final String sALTA = "insert into SIVE_ZONA_BASICA (CD_ZBS, CD_NIVEL_1, CD_NIVEL_2, DS_ZBS, DSL_ZBS) values (?, ?, ?, ?, ?)";

    // actualiza el item
    final String sACTUALIZA = "update SIVE_ZONA_BASICA SET CD_NIVEL_1 = ?, CD_NIVEL_2 = ?, DS_ZBS = ?, DSL_ZBS = ? where CD_ZBS = ? and CD_NIVEL_2 = ? and CD_NIVEL_1 = ?";

    // borra el item
    final String sBAJA = "delete from SIVE_ZONA_BASICA where CD_ZBS = ? and CD_NIVEL_2 = ? and CD_NIVEL_1 = ?";

    // obtiene lista de items
    final String sLISTADO = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_ZBS like ? order by CD_ZBS";

    final String sLISTADOdes = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and upper(a.DS_ZBS) like upper(?) order by DS_ZBS";

    // obtiene lista de items m�ltiple(nivel1 y nivel2)
    final String sLISTADOTodoN1N2 = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_NIVEL_1 =? and a.CD_NIVEL_2 =? and a.CD_ZBS like ? order by a.CD_ZBS";

    final String sLISTADOTododesN1N2 = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_NIVEL_1 =? and a.CD_NIVEL_2 =? and upper(a.DS_ZBS) like upper(?) order by a.DS_ZBS";

    // obtiene lista de items m�ltiple(nivel1)
    final String sLISTADOTodoN1 = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_NIVEL_1 =? and a.CD_ZBS like ? order by a.CD_ZBS";

    final String sLISTADOTododesN1 = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_NIVEL_1 =? and upper(a.DS_ZBS) like upper(?) order by a.DS_ZBS";

    // obtiene lista de items m�ltiple(nivel2)
    final String sLISTADOTodoN2 = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_NIVEL_2 = ? and a.CD_ZBS like ? order by a.CD_ZBS";

    final String sLISTADOTododesN2 = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_NIVEL_2 = ? and upper(a.DS_ZBS) like upper(?) order by a.DS_ZBS";

    // obtiene lista de items m�ltiple
    final String sLISTADOTodo = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_ZBS like ? order by a.CD_ZBS";

    final String sLISTADOTododes = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and upper(a.DS_ZBS) like upper(?) order by a.DS_ZBS";

    // obtiene lista de items
    final String sLISTADO2 = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_ZBS like ? and a.CD_ZBS > ? order by CD_ZBS";

    final String sLISTADO2des = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and upper(a.DS_ZBS) like upper(?) and upper(a.DS_ZBS) > upper(?) order by DS_ZBS";

    // obtiene lista de items m�ltiple(nivel1 y nivel2)
    final String sLISTADO2TodoN1N2 = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_NIVEL_1 = ? and a.CD_NIVEL_2 = ? and a.CD_ZBS like ? and a.CD_ZBS > ? order by a.CD_ZBS";

    final String sLISTADO2TododesN1N2 = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_NIVEL_1 = ? and a.CD_NIVEL_2 = ? and upper(a.DS_ZBS) like upper(?) and upper(a.DS_ZBS) > upper(?) order by a.DS_ZBS";

    // obtiene lista de items m�ltiple(nivel1)
    final String sLISTADO2TodoN1 = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_NIVEL_1 = ? and a.CD_ZBS like ? and a.CD_ZBS > ? order by a.CD_ZBS";

    final String sLISTADO2TododesN1 = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_NIVEL_1 = ? and upper(a.DS_ZBS) like upper(?) and upper(a.DS_ZBS) > upper(?) order by a.DS_ZBS";

    // obtiene lista de items m�ltiple(nivel2)
    final String sLISTADO2TodoN2 = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_NIVEL_2 = ? and a.CD_ZBS like ? and a.CD_ZBS > ? order by a.CD_ZBS";

    final String sLISTADO2TododesN2 = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_NIVEL_2 = ? and upper(a.DS_ZBS) like upper(?) and upper(a.DS_ZBS) > upper(?) order by a.DS_ZBS";

    // obtiene lista de items m�ltiple
    final String sLISTADO2Todo = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_ZBS like ? and a.CD_ZBS > ? order by a.CD_ZBS";

    final String sLISTADO2Tododes = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and upper(a.DS_ZBS) like upper(?) and upper(a.DS_ZBS) > upper(?) order by a.DS_ZBS";

    // busca un item
    final String sBUSQUEDA = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.CD_ZBS = ?";

    final String sBUSQUEDAdes = "select a.CD_ZBS, a.DS_ZBS, a.DSL_ZBS, a.CD_NIVEL_1, a.CD_NIVEL_2, b.DS_NIVEL_1, b.DSL_NIVEL_1, c.DS_NIVEL_2, c.DSL_NIVEL_2 from SIVE_ZONA_BASICA a, SIVE_NIVEL1_S b, SIVE_NIVEL2_S c where a.CD_NIVEL_1 = b.CD_NIVEL_1 and a.CD_NIVEL_2 = c.CD_NIVEL_2 and a.CD_NIVEL_1 = c.CD_NIVEL_1 and a.DS_ZBS = ?";

    // obtiene lista de niveles 2
    final String sLISTADO_NIV2 = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_2 like ? and CD_NIVEL_1 = ? order by CD_NIVEL_2";

    // obtiene lista de niveles 2
    final String sLISTADO_NIV22 = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_2 like ? and CD_NIVEL_1 = ? and CD_NIVEL_2 > ? order by CD_NIVEL_2";

    // busca un nivel 2
    final String sBUSQUEDA_NIV2 = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_2 = ? and CD_NIVEL_1 = ?";

    // campos
    String sCod;
    String sDes;
    String sDesL;
    String sCod1;
    String sDes1;
    String sDesL1;
    String sCod2;
    String sDes2;
    String sDesL2;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    zbs = (DataZBS) param.firstElement();

    // modos de operaci�n
    switch (opmode) {

      // alta
      case servletALTA:

        // prepara la query
        st = con.prepareStatement(sALTA);
        // codigo
        st.setString(1, zbs.getCod().trim());
        // codigo 1
        st.setString(2, zbs.getCod1().trim());
        // codigo 2
        st.setString(3, zbs.getCod2().trim());
        // descripci�n
        st.setString(4, zbs.getDes().trim());
        // descripci�n local
        if (zbs.getDesL().trim().length() > 0) {
          st.setString(5, zbs.getDesL().trim());
        }
        else {
          st.setNull(5, java.sql.Types.VARCHAR);

          // lanza la query
        }
        st.executeUpdate();
        st.close();

        break;

        // baja
      case servletBAJA:

        // lanza la query
        st = con.prepareStatement(sBAJA);
        st.setString(1, zbs.getCod().trim());
        st.setString(2, zbs.getCod2().trim());
        st.setString(3, zbs.getCod1().trim());
        st.executeUpdate();
        st.close();

        break;

      case servletSELECCION_COMPLETA_CODIGO:
      case servletSELECCION_COMPLETA_DESCRIPCION:

        // prepara la query
        if (param.getFilter().length() > 0) {
          if (opmode == servletSELECCION_COMPLETA_CODIGO) {
            if (zbs.getCod1().length() > 0) {
              if (zbs.getCod2().length() > 0) {
                st = con.prepareStatement(sLISTADO2TodoN1N2);
                // nivel 1
                st.setString(1, zbs.getCod1().trim());
                // nivel 2
                st.setString(2, zbs.getCod2().trim());
                // c�digo ZBS
                st.setString(3, zbs.getCod().trim() + "%");
                // paginaci�n
                st.setString(4, param.getFilter().trim());
              }
              else {
                st = con.prepareStatement(sLISTADO2TodoN1);
                // nivel 1
                st.setString(1, zbs.getCod1().trim());
                // c�digo ZBS
                st.setString(2, zbs.getCod().trim() + "%");
                // paginaci�n
                st.setString(3, param.getFilter().trim());

              }
            }
            else {
              if (zbs.getCod2().length() > 0) {
                st = con.prepareStatement(sLISTADO2TodoN2);
                // nivel 2
                st.setString(1, zbs.getCod2().trim());
                // c�digo ZBS
                st.setString(2, zbs.getCod().trim() + "%");
                // paginaci�n
                st.setString(3, param.getFilter().trim());
              }
              else {
                st = con.prepareStatement(sLISTADO2Todo);
                // c�digo ZBS
                st.setString(1, zbs.getCod().trim() + "%");
                // paginaci�n
                st.setString(2, param.getFilter().trim());
              }
            }
          }
          else {
            if (zbs.getCod1().length() > 0) {
              if (zbs.getCod2().length() > 0) {
                st = con.prepareStatement(sLISTADO2TododesN1N2);
                // nivel 1
                st.setString(1, zbs.getCod1().trim());
                // nivel 2
                st.setString(2, zbs.getCod2().trim());
                // c�digo ZBS
                st.setString(3, "%" + zbs.getCod().trim() + "%");
                // paginaci�n
                st.setString(4, param.getFilter().trim());
              }
              else {
                st = con.prepareStatement(sLISTADO2TododesN1);
                // nivel 1
                st.setString(1, zbs.getCod1().trim());
                // c�digo ZBS
                st.setString(2, "%" + zbs.getCod().trim() + "%");
                // paginaci�n
                st.setString(3, param.getFilter().trim());
              }
            }
            else {
              if (zbs.getCod2().length() > 0) {
                st = con.prepareStatement(sLISTADO2TododesN2);
                // nivel 2
                st.setString(1, zbs.getCod2().trim());
                // c�digo ZBS
                st.setString(2, "%" + zbs.getCod().trim() + "%");
                // paginaci�n
                st.setString(3, param.getFilter().trim());
              }
              else {
                st = con.prepareStatement(sLISTADO2Tododes);
                // c�digo ZBS
                st.setString(1, "%" + zbs.getCod().trim() + "%");
                // paginaci�n
                st.setString(2, param.getFilter().trim());
              }
            }

          }
        }
        else {
          if (opmode == servletSELECCION_COMPLETA_CODIGO) {
            if (zbs.getCod1().length() > 0) {
              if (zbs.getCod2().length() > 0) {
                st = con.prepareStatement(sLISTADOTodoN1N2);
                // nivel 1
                st.setString(1, zbs.getCod1().trim());
                // nivel 2
                st.setString(2, zbs.getCod2().trim());
                // c�digo ZBS
                st.setString(3, zbs.getCod().trim() + "%");

              }
              else {
                st = con.prepareStatement(sLISTADOTodoN1);
                // nivel 1
                st.setString(1, zbs.getCod1().trim());
                // c�digo ZBS
                st.setString(2, zbs.getCod().trim() + "%");
              }
            }
            else {
              if (zbs.getCod2().length() > 0) {
                st = con.prepareStatement(sLISTADOTodoN2);
                // nivel 2
                st.setString(1, zbs.getCod2().trim());
                // c�digo ZBS
                st.setString(2, zbs.getCod().trim() + "%");
              }
              else {
                st = con.prepareStatement(sLISTADOTodo);
                // c�digo ZBS
                st.setString(1, zbs.getCod().trim() + "%");
              }
            }
          }
          else {
            if (zbs.getCod1().length() > 0) {
              if (zbs.getCod2().length() > 0) {
                st = con.prepareStatement(sLISTADOTododesN1N2);
                // nivel 1
                st.setString(1, zbs.getCod1().trim());
                // nivel 2
                st.setString(2, zbs.getCod2().trim());
                // c�digo ZBS
                st.setString(3, "%" + zbs.getCod().trim() + "%");
              }
              else {
                st = con.prepareStatement(sLISTADOTododesN1);
                // nivel 1
                st.setString(1, zbs.getCod1().trim());
                // c�digo ZBS
                st.setString(2, "%" + zbs.getCod().trim() + "%");
              }
            }
            else {
              if (zbs.getCod2().length() > 0) {
                st = con.prepareStatement(sLISTADOTododesN2);
                // nivel 2
                st.setString(1, zbs.getCod2().trim());
                // c�digo ZBS
                st.setString(2, "%" + zbs.getCod().trim() + "%");
              }
              else {
                st = con.prepareStatement(sLISTADOTododes);
                // c�digo ZBS
                st.setString(1, "%" + zbs.getCod().trim() + "%");
              }
            }
          }
        }

        // prepara la lista de resultados
        data = new CLista();

        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {

          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataZBS) data.lastElement()).getCod());
            break;
          }

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }

          // obtiene los campos
          sCod = rs.getString("CD_ZBS");
          sDes = rs.getString("DS_ZBS");
          sDesL = rs.getString("DSL_ZBS");
          sCod1 = rs.getString("CD_NIVEL_1");
          sDes1 = rs.getString("DS_NIVEL_1");
          sDesL1 = rs.getString("DSL_NIVEL_1");
          sCod2 = rs.getString("CD_NIVEL_2");
          sDes2 = rs.getString("DS_NIVEL_2");
          sDesL2 = rs.getString("DSL_NIVEL_2");

          // obtiene la descripcion principal en funci�n del idioma si el cliente no es un mantenimiento
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (param.getMantenimiento() == false)) {
            if (sDesL != null) {
              sDes = sDesL;
            }
          }

          // obtiene la descripcion auxiliar en funci�n del idioma
          if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
            if (sDesL1 != null) {
              sDes1 = sDesL1;
            }
            if (sDesL2 != null) {
              sDes2 = sDesL2;
            }
          }

          // a�ade un nodo
          data.addElement(new DataZBS(sCod, sDes, sDesL, sCod1, sDes1, sCod2,
                                      sDes2));
          i++;
        }

        rs.close();
        st.close();

        break;

        // listado
      case servletSELECCION_X_CODIGO:
      case servletSELECCION_X_DESCRIPCION:

        // prepara la query
        if (param.getFilter().length() > 0) {
          if (opmode == servletSELECCION_X_CODIGO) {
            st = con.prepareStatement(sLISTADO2);
          }
          else {
            st = con.prepareStatement(sLISTADO2des);
          }
        }
        else {
          if (opmode == servletSELECCION_X_CODIGO) {
            st = con.prepareStatement(sLISTADO);
          }
          else {
            st = con.prepareStatement(sLISTADOdes);
          }
        }

        // prepara la lista de resultados
        data = new CLista();

        // filtro
        if (opmode == servletSELECCION_X_CODIGO) {
          st.setString(1, zbs.getCod().trim() + "%");
        }
        else {
          st.setString(1, "%" + zbs.getCod().trim() + "%");

          // paginaci�n
        }
        if (param.getFilter().length() > 0) {
          st.setString(2, param.getFilter().trim());
        }

        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {

          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataZBS) data.lastElement()).getCod());
            break;
          }

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }

          // obtiene los campos
          sCod = rs.getString("CD_ZBS");
          sDes = rs.getString("DS_ZBS");
          sDesL = rs.getString("DSL_ZBS");
          sCod1 = rs.getString("CD_NIVEL_1");
          sDes1 = rs.getString("DS_NIVEL_1");
          sDesL1 = rs.getString("DSL_NIVEL_1");
          sCod2 = rs.getString("CD_NIVEL_2");
          sDes2 = rs.getString("DS_NIVEL_2");
          sDesL2 = rs.getString("DSL_NIVEL_2");

          //___________________

          // obtiene la descripcion principal en funci�n del idioma si el cliente no es un mantenimiento
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (param.getMantenimiento() == false)) {
            if (sDesL != null) {
              sDes = sDesL;
            }
          }
          //___________________

          // obtiene la descripcion auxiliar en funci�n del idioma
          if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
            if (sDesL1 != null) {
              sDes1 = sDesL1;
            }
            if (sDesL2 != null) {
              sDes2 = sDesL2;
            }
          }

          // a�ade un nodo
          data.addElement(new DataZBS(sCod, sDes, sDesL, sCod1, sDes1, sCod2,
                                      sDes2));
          i++;
        }

        rs.close();
        st.close();

        break;

        // modificaci�n
      case servletMODIFICAR:

        // lanza la query
        st = con.prepareStatement(sACTUALIZA);

        // codigo 1
        st.setString(1, zbs.getCod1().trim());

        // codigo 2
        st.setString(2, zbs.getCod2().trim());

        // descripci�n
        st.setString(3, zbs.getDes().trim());

        // descripci�n local
        if (zbs.getDesL().trim().length() > 0) {
          st.setString(4, zbs.getDesL().trim());
        }
        else {
          st.setNull(4, java.sql.Types.VARCHAR);
          // codigo
        }
        st.setString(5, zbs.getCod().trim());
        st.setString(6, zbs.getCod2().trim());
        st.setString(7, zbs.getCod1().trim());

        st.executeUpdate();
        st.close();

        break;

      case servletOBTENER_X_CODIGO:
      case servletOBTENER_X_DESCRIPCION:

        // lanza la query
        if (opmode == servletOBTENER_X_CODIGO) {
          st = con.prepareStatement(sBUSQUEDA);
        }
        else {
          st = con.prepareStatement(sBUSQUEDAdes);
        }
        st.setString(1, zbs.getCod().trim());
        rs = st.executeQuery();

        data = new CLista();

        // extrae el registro encontrado
        while (rs.next()) {
          // obtiene los campos
          sCod = rs.getString("CD_ZBS");
          sDes = rs.getString("DS_ZBS");
          sDesL = rs.getString("DSL_ZBS");
          sCod1 = rs.getString("CD_NIVEL_1");
          sDes1 = rs.getString("DS_NIVEL_1");
          sDesL1 = rs.getString("DSL_NIVEL_1");
          sCod2 = rs.getString("CD_NIVEL_2");
          sDes2 = rs.getString("DS_NIVEL_2");
          sDesL2 = rs.getString("DSL_NIVEL_2");

          //___________________

          // obtiene la descripcion principal en funci�n del idioma si el cliente no es un mantenimiento
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) &&
              (param.getMantenimiento() == false)) {
            if (sDesL != null) {
              sDes = sDesL;
            }
          }

          //_____________________

          // obtiene la descripcion auxiliar en funci�n del idioma
          if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
            if (sDesL1 != null) {
              sDes1 = sDesL1;
            }
            if (sDesL2 != null) {
              sDes2 = sDesL2;
            }
          }

          // a�ade un nodo
          data.addElement(new DataZBS(sCod, sDes, sDesL, sCod1, sDes1, sCod2,
                                      sDes2));
        }
        rs.close();
        st.close();

        break;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }
}
