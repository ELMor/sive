package zbs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvZBS2
    extends DBServlet {

  // modos de operaci�n del servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    int i = 1;

    // objetos de datos
    CLista data = null;
    DataZBS2 zbs = null;

    // querys
    // ARG: uppers (8/5/02)
    // obtiene lista de niveles 2
    final String sLISTADO_NIV2 = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_2 like ? and CD_NIVEL_1 = ? order by CD_NIVEL_2";

    final String sLISTADO_NIV2des = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where upper(DS_NIVEL_2) like upper(?) and CD_NIVEL_1 = ? order by DS_NIVEL_2";

    // obtiene lista de niveles 2
    final String sLISTADO_NIV2_2 = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_2 like ? and CD_NIVEL_1 = ? and CD_NIVEL_2 > ? order by CD_NIVEL_2";

    final String sLISTADO_NIV2_2des = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where upper(DS_NIVEL_2) like upper(?) and CD_NIVEL_1 = ? and upper(DS_NIVEL_2) > upper(?) order by DS_NIVEL_2";

    // busca un nivel 2
    final String sBUSQUEDA_NIV2 = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where CD_NIVEL_2 = ? and CD_NIVEL_1 = ?";

    final String sBUSQUEDA_NIV2des = "select CD_NIVEL_2, DS_NIVEL_2, DSL_NIVEL_2 from SIVE_NIVEL2_S where DS_NIVEL_2 = ? and CD_NIVEL_1 = ?";

    // campos
    String sNiv1;
    String sNiv2;
    String sZBS;
    String sDes;
    String sDesL;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    zbs = (DataZBS2) param.firstElement();

    // modos de operaci�n
    switch (opmode) {
      // listado
      case servletSELECCION_NIV2_X_CODIGO:
      case servletSELECCION_NIV2_X_DESCRIPCION:

        // prepara la query
        if (param.getFilter().length() > 0) {
          if (opmode == servletSELECCION_NIV2_X_CODIGO) {
            st = con.prepareStatement(sLISTADO_NIV2_2);
          }
          else {
            st = con.prepareStatement(sLISTADO_NIV2_2des);
          }
        }
        else {
          if (opmode == servletSELECCION_NIV2_X_CODIGO) {
            st = con.prepareStatement(sLISTADO_NIV2);
          }
          else {
            st = con.prepareStatement(sLISTADO_NIV2des);
          }
        }

        // prepara la lista de resultados
        data = new CLista();

        // filtro
        if (opmode == servletSELECCION_NIV2_X_CODIGO) {
          st.setString(1, zbs.getNiv2().trim() + "%");
        }
        else {
          st.setString(1, "%" + zbs.getNiv2().trim() + "%");

        }
        st.setString(2, zbs.getNiv1().trim());

        // paginaci�n
        if (param.getFilter().length() > 0) {
          st.setString(3, param.getFilter().trim());
        }

        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {

          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataZBS2) data.lastElement()).getNiv2());
            break;
          }

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }

          // obtiene los campos
          sNiv2 = rs.getString("CD_NIVEL_2");
          sDes = rs.getString("DS_NIVEL_2");
          sDesL = rs.getString("DSL_NIVEL_2");

          // obtiene la descripcion auxiliar en funci�n del idioma
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesL != null)) {
            sDes = sDesL;

            // a�ade un nodo
          }
          data.addElement(new DataZBS2("", sNiv2, "", sDes));
          i++;
        }

        rs.close();
        st.close();

        break;

      case servletOBTENER_NIV2_X_CODIGO:
      case servletOBTENER_NIV2_X_DESCRIPCION:

        // lanza la query
        if (opmode == servletOBTENER_NIV2_X_CODIGO) {
          st = con.prepareStatement(sBUSQUEDA_NIV2);
        }
        else {
          st = con.prepareStatement(sBUSQUEDA_NIV2des);
        }
        st.setString(1, zbs.getNiv2().trim());
        st.setString(2, zbs.getNiv1().trim());
        rs = st.executeQuery();

        data = new CLista();

        // extrae el registro encontrado
        while (rs.next()) {
          // obtiene los campos
          sNiv2 = rs.getString("CD_NIVEL_2");
          sDes = rs.getString("DS_NIVEL_2");
          sDesL = rs.getString("DSL_NIVEL_2");

          // obtiene la descripcion auxiliar en funci�n del idioma
          if ( (param.getIdioma() != CApp.idiomaPORDEFECTO) && (sDesL != null)) {
            sDes = sDesL;

            // a�ade un nodo
          }
          data.addElement(new DataZBS2("", sNiv2, "", sDes));
        }
        rs.close();
        st.close();

        break;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }
}
