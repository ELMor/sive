
package zbs;

import java.util.ResourceBundle;

import capp.CApp;

public class ZBS
    extends CApp {

  // modos de operación del servlet
  final int servletOBTENER_X_CODIGO = 3;
  ResourceBundle res;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // constantes del panel
  final String strSERVLET = "servlet/SrvZBS";

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("zbs.Res" + this.getIdioma());
//    CApp a = (CApp) this;
    setTitulo(res.getString("msg10.Text"));
    VerPanel("", new Pan_ZBS(this));
  }
}
