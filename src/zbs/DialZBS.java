//Title:        ZBS
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Norsistemas S.A.
//Company:      Norsistemas, S.A.
//Description:

package zbs;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CDialog;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import catalogo.CListaCat;
import catalogo.Catalogo;
import catalogo.DataCat;
import sapp.StubSrvBD;

public class DialZBS
    extends CDialog {

  CApp app = null;
  ResourceBundle res;
  public boolean bAceptar = false;
  boolean valido = false;
  // modos de operaci�n de la ventana
  final static int modoALTA = 0;
  final static int modoMODIFICAR = 1;
  final static int modoBAJA = 2;
  final static int modoESPERA = 5;

  // modos de operaci�n del servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  // im�genes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/aceptar.gif",
      "images/cancelar.gif"};

  final String strSERVLET = "servlet/SrvZBS";
  final String strSERVLET_NIV1 = "servlet/SrvCat";
  final String strSERVLET_NIV2 = "servlet/SrvZBS2";

  // par�metros
  protected int modoOperacion = modoALTA;
  protected int modoOperacionBk = modoALTA;
  protected StubSrvBD stubCliente = null;
  protected boolean bBloquearCambio = false;

  // controles
  XYLayout xyLyt = new XYLayout();
  Label lbl1 = new Label();
  CCampoCodigo txtCod = new CCampoCodigo();
  Label lbl2 = new Label();
  CCampoCodigo txtCod1 = new CCampoCodigo();
  TextField txtDes1 = new TextField();
  Label lbl3 = new Label();
  CCampoCodigo txtCod2 = new CCampoCodigo();
  TextField txtDes2 = new TextField();
  Label lbl4 = new Label();
  TextField txtDes = new TextField();
  Label lbl5 = new Label();
  TextField txtDesL = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  ButtonControl btnCancelar = new ButtonControl();
  ButtonControl btnSearchNiv1 = new ButtonControl();
  ButtonControl btnSearchNiv2 = new ButtonControl();

  zbsActionListener btnActionListener = new zbsActionListener(this);
  zbsTxtCod1FocusAdapter txtCod1FocusAdapter = new zbsTxtCod1FocusAdapter(this);
  zbsTxtCod2FocusAdapter txtCod2FocusAdapter = new zbsTxtCod2FocusAdapter(this);

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {

      case modoALTA:

        // modo alta
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnSearchNiv1.setEnabled(true);
        txtCod.setEnabled(true);
        txtCod1.setEnabled(true);
        InicializaNivel2();
        txtDes.setEnabled(true);
        txtDesL.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoMODIFICAR:

        // modo modificaci�n
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnSearchNiv1.setEnabled(false);
        btnSearchNiv2.setEnabled(false);
        txtCod.setEnabled(false);
        txtCod1.setEnabled(false);
        txtDes1.setEnabled(false);
        txtCod2.setEnabled(false);
        txtDes2.setEnabled(false);
        txtDes.setEnabled(true);
        txtDesL.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoBAJA:
        btnAceptar.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnSearchNiv1.setEnabled(false);
        btnSearchNiv2.setEnabled(false);
        txtCod.setEnabled(false);
        txtCod1.setEnabled(false);
        txtDes1.setEnabled(false);
        txtCod2.setEnabled(false);
        txtDes2.setEnabled(false);
        txtDes.setEnabled(false);
        txtDesL.setEnabled(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        btnAceptar.setEnabled(false);
        btnCancelar.setEnabled(false);
        btnSearchNiv1.setEnabled(false);
        btnSearchNiv2.setEnabled(false);
        txtCod.setEnabled(false);
        txtCod1.setEnabled(false);
        txtDes1.setEnabled(false);
        txtCod2.setEnabled(false);
        txtDes2.setEnabled(false);
        txtDes.setEnabled(false);
        txtDesL.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  // actualiza el estado de selecci�n del nivel 2
  public void InicializaNivel2() {
    if (txtDes1.getText().length() == 0) {
      txtDes2.setText("");
      txtCod2.setText("");
      txtCod2.setEnabled(false);
      btnSearchNiv2.setEnabled(false);
    }
    else {
      txtCod2.setEnabled(true);
      btnSearchNiv2.setEnabled(true);
    }
  }

  // contructor
  public DialZBS(CApp a, int modo) {
    super(a);

    try {
      app = a;
      res = ResourceBundle.getBundle("zbs.Res" + app.getIdioma());
      this.setTitle(res.getString("this.Title"));
      modoOperacion = modo;
      stubCliente = new StubSrvBD(new URL(a.getURL() + strSERVLET));
      jbInit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // aspecto de la ventana
  private void jbInit() throws Exception {
    xyLyt.setHeight(277);
    xyLyt.setWidth(499);
    setSize(499, 325);
    lbl1.setText(res.getString("lbl1.Text"));
    lbl2.setText(this.app.getNivel1() + ":");
    lbl3.setText(this.app.getNivel2() + ":");
    lbl4.setText(res.getString("lbl4.Text"));
    txtDes.setBackground(new Color(255, 255, 150));
    lbl5.setText(res.getString("lbl5.Text"));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnCancelar.setLabel(res.getString("btnCancelar.Label"));

    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnSearchNiv1.setImage(imgs.getImage(0));
    btnSearchNiv2.setImage(imgs.getImage(0));
    btnAceptar.setImage(imgs.getImage(1));
    btnCancelar.setImage(imgs.getImage(2));

    btnSearchNiv1.setActionCommand("buscar1");
    btnSearchNiv2.setActionCommand("buscar2");

    btnAceptar.setActionCommand("aceptar");
    btnCancelar.setActionCommand("cancelar");
    txtCod.setName("cod");
    txtCod.setBackground(new Color(255, 255, 150));
    txtCod1.setName("cod1");
    txtDes1.setName("des1");
    txtCod1.setBackground(new Color(255, 255, 150));
    txtCod1.addKeyListener(new DialZBS_txtCod1_keyAdapter(this));
    txtCod2.setName("cod2");
    txtCod2.setBackground(new Color(255, 255, 150));
    txtCod2.addKeyListener(new DialZBS_txtCod2_keyAdapter(this));
    this.setLayout(xyLyt);

    this.add(lbl2, new XYConstraints(23, 27, -1, -1));
    this.add(txtCod1, new XYConstraints(117, 29, 77, -1));
    this.add(btnSearchNiv1, new XYConstraints(200, 28, -1, -1));
    this.add(txtDes1, new XYConstraints(234, 29, 236, -1));
    this.add(lbl3, new XYConstraints(23, 65, -1, -1));
    this.add(txtCod2, new XYConstraints(117, 67, 77, -1));
    this.add(btnSearchNiv2, new XYConstraints(200, 66, -1, -1));
    this.add(txtDes2, new XYConstraints(234, 67, 236, -1));
    this.add(lbl1, new XYConstraints(23, 103, -1, -1));
    this.add(txtCod, new XYConstraints(117, 105, 77, -1));
    this.add(lbl4, new XYConstraints(23, 144, -1, -1));
    this.add(txtDes, new XYConstraints(193, 146, 280, -1));
    this.add(lbl5, new XYConstraints(23, 186, -1, -1));
    this.add(txtDesL, new XYConstraints(193, 186, 280, -1));
    this.add(btnAceptar, new XYConstraints(307, 226, 80, 26));
    this.add(btnCancelar, new XYConstraints(392, 226, 80, 26));
//    this.add(groupBox1, new XYConstraints(6, 4, 485, 265));

    // establece los escuchadores
    btnSearchNiv1.addActionListener(btnActionListener);
    btnSearchNiv2.addActionListener(btnActionListener);
    btnAceptar.addActionListener(btnActionListener);
    btnCancelar.addActionListener(btnActionListener);

    txtCod1.addFocusListener(txtCod1FocusAdapter);
    txtCod2.addFocusListener(txtCod2FocusAdapter);

    // idioma local no es visible si no tengo
    if (app.getIdiomaLocal().length() == 0) {
      txtDesL.setVisible(false);
      lbl5.setVisible(false);
    }
    else {
      lbl5.setText(res.getString("lbl5.Text") + app.getIdiomaLocal() + "):");
    }

    // controles con estado fijo
    txtDes1.setEditable(false);
    txtDes2.setEditable(false);

    // establece el modo de operaci�n
    Inicializar();
  }

  public void traerDescripciones() {
    CMessage msgBox = null;
    CLista data = null;
    DataCat nivel1 = null;
    DataZBS2 nivel2 = null;
    CLista lista = null;

    // procesa
    try {
      //Va a por descripci�n del nivel1

      // obtiene y pinta la lista nueva
      data = new CLista();

      // idioma
      data.setIdioma(app.getIdioma());

      // perfil
      data.setPerfil(app.getPerfil());

      // login
      data.setLogin(app.getLogin());

      //
      data.addElement(new DataCat(txtCod1.getText()));
      stubCliente.setUrl(new URL(app.getURL() + strSERVLET_NIV1));
      lista = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO +
                                          Catalogo.catNIVEL1, data);

      nivel1 = (DataCat) lista.firstElement();

      txtDes1.setText(nivel1.getDes());
      data = null;

      //Va a por descripci�n del nivel2

      // obtiene y pinta la lista nueva
      data = new CLista();

      // idioma
      data.setIdioma(app.getIdioma());

      // perfil
      data.setPerfil(app.getPerfil());

      // login
      data.setLogin(app.getLogin());

      //
      data.addElement(new DataZBS2(txtCod1.getText(), txtCod2.getText(), "", ""));
      stubCliente.setUrl(new URL(app.getURL() + strSERVLET_NIV2));
      lista = (CLista) stubCliente.doPost(servletOBTENER_NIV2_X_CODIGO, data);

      nivel2 = (DataZBS2) lista.firstElement();

      txtDes2.setText(nivel2.getDes());
      data = null;

      // error al procesar
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }
  }

// procesa opci�n a�adir un item
  public void A�adir() {
    CMessage msgBox = null;
    CLista data = null;

    // determina si los datos est�n completos
    if (this.isDataValid()) {

      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        // prepara los par�metros
        data = new CLista();
        data.addElement(new DataZBS(txtCod.getText().toUpperCase(),
                                    txtDes.getText(), txtDesL.getText(),
                                    txtCod1.getText().toUpperCase(),
                                    txtCod2.getText().toUpperCase()));

        // apunta al servlet principal
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET));
        this.stubCliente.doPost(servletALTA, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoALTA;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
  }

  // procesa opci�n modificar
  public void Modificar() {
    CMessage msgBox = null;
    CLista data = null;

    if (this.isDataValid()) {

      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        // prepara los par�metros de env�o
        data = new CLista();
        data.addElement(new DataZBS(txtCod.getText(), txtDes.getText(),
                                    txtDesL.getText(), txtCod1.getText(),
                                    txtCod2.getText()));

        // apunta al servlet principal
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET));
        this.stubCliente.doPost(servletMODIFICAR, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoMODIFICAR;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }
    }
  }

  // procesa opci�n borrar
  public void Borrar() {
    CMessage msgBox = null;
    CLista data = null;

    msgBox = new CMessage(app, CMessage.msgPREGUNTA, res.getString("msg2.Text"));
    msgBox.show();

    // el usuario confirma la operaci�n
    if (msgBox.getResponse()) {

      msgBox = null;

      try {

        // modo espera
        this.modoOperacion = modoESPERA;
        Inicializar();

        // par�metros de env�o
        data = new CLista();
        data.addElement(new DataZBS(txtCod.getText(), "", "", txtCod1.getText(),
                                    "", txtCod2.getText(), ""));

        // apunta al servlet principal
        stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET));
        this.stubCliente.doPost(servletBAJA, data);

        // oculta el dialogo
        this.dispose();

        // error en el proceso
      }
      catch (Exception e) {
        this.modoOperacion = modoBAJA;
        Inicializar();
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

    }
    else {
      msgBox = null;
    }
  }

  // procesa opci�n seleccionar las listas de niveles 1
  public void btnSearchNiv1_actionPerformed(ActionEvent evt) {
    DataCat data;
    CMessage msgBox;

    int modo = modoOperacion;

    bBloquearCambio = true;

    try {
      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();

      CListaCat lista = new CListaCat(app,
                                      res.getString("msg3.Text") +
                                      app.getNivel1(),
                                      stubCliente,
                                      strSERVLET_NIV1,
                                      servletOBTENER_X_CODIGO +
                                      Catalogo.catNIVEL1,
                                      servletOBTENER_X_DESCRIPCION +
                                      Catalogo.catNIVEL1,
                                      servletSELECCION_X_CODIGO +
                                      Catalogo.catNIVEL1,
                                      servletSELECCION_X_DESCRIPCION +
                                      Catalogo.catNIVEL1);
      lista.show();
      data = (DataCat) lista.getComponente();
      modoOperacion = modo;
      if (data != null) {
        txtCod1.setText(data.getCod());
        txtDes1.setText(data.getDes());

        //Han cambiado datos de nivel1. Se inicializa el nivel 2
        //Se resetean las cajas de nivel2
        //NO SOLO ES NECESARIO VER QUE HAY DESCRIPCION DE N2 (inicializa nivel2) sino tambi�n poner cajas
        // a blanc pues puede ser que antes hubiera un N2, que ala cambiar N1 ya no ser� v�lido
        txtDes2.setText("");
        txtCod2.setText("");

        InicializaNivel2();
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    Inicializar();
    bBloquearCambio = false;
  }

  // procesa opci�n obtener las listas de niveles 2
  public void btnSearchNiv2_actionPerformed(ActionEvent evt) {

    DataZBS2 data;
    CMessage msgBox;

    int modo = modoOperacion;

    try {
      // apunta al servlet auxiliar
      stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_NIV2));
      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg4.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLET_NIV2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
      modoOperacion = modo;
      if (data != null) {
        txtDes2.setText(data.getDes());
        txtCod2.setText(data.getNiv2());
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    Inicializar();
  }

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;

    boolean b = false;

    // comprueba que esten informados los campos obligatorios
    if ( (txtCod.getText().length() > 0) && (txtDes.getText().length() > 0) &&
        (txtDes1.getText().length() > 0) && (txtDes2.getText().length() > 0)) {
      b = true;

      // comprueba la longitud m�xima de los campos
      if (txtCod.getText().length() > 2) {
        b = false;
        msg = res.getString("msg5.Text");
        txtCod.selectAll();
      }

      if (txtDes.getText().length() > 30) {
        b = false;
        msg = res.getString("msg5.Text");
        txtDes.selectAll();
      }

      if (txtDesL.getText().length() > 30) {
        b = false;
        msg = res.getString("msg5.Text");
        txtDesL.selectAll();
      }

      // advertencia de requerir datos
    }
    else {
      msg = res.getString("msg6.Text");
    }

    if (!b) {
      msgBox = new CMessage(app, CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }
    valido = b;
    return b;
  }

  void txtCod1_focusLost() {
    // datos de envio
    DataCat data;
    CLista param = null;
    CMessage msg;
    String sDes1;
    int modo = modoOperacion;

    if (txtCod1.getText().length() > 0) {

      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();

      try {
        param = new CLista();
        param.setIdioma(app.getIdioma());

        param.addElement(new DataCat(txtCod1.getText().toUpperCase()));

        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        // ARG: Para que funcione correctamente notservlet
//        param.setVN1(app.getCD_NIVEL_1_AUTORIZACIONES() );
//        param.setVN2(app.getCD_NIVEL_2_AUTORIZACIONES());

        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_NIV1));

        param = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO +
                                            Catalogo.catNIVEL1, param);

        // rellena los datos
        if (param.size() > 0) {
          data = (DataCat) param.elementAt(0);

          txtCod1.setText(data.getCod());
          sDes1 = data.getDes();

//REPASAR********************************************  Ver si interesa comparar DesL con "" o con null
          /*
               if ((this.app.getIdioma() != CApp.idiomaPORDEFECTO) && (data.getDesL() !=null))
                      sDes1 = data.getDesL();
           */
          txtDes1.setText(sDes1);

          //Se resetean las cajas de nivel2
          //NO SOLO ES NECESARIO VER QUE HAY DESCRIPCION DE N2 (inicializa nivel2) sino tambi�n poner cajas
          // a blanc pues puede ser que antes hubiera un N2, que ala cambiar N1 ya no ser� v�lido
          txtDes2.setText("");
          txtCod2.setText("");
          InicializaNivel2();

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg7.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
    // modo normal
    modoOperacion = modo;
    Inicializar();

  }

  void txtCod2_focusLost() {

    // datos de envio
    DataZBS2 data;
    CLista param = null;
    CMessage msg;
    String sDes2;
    int modo = modoOperacion;

    if (txtCod2.getText().length() > 0) {

      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();

      try {
        param = new CLista();
        param.setIdioma(app.getIdioma());
        param.addElement(new DataZBS2(txtCod1.getText().toUpperCase(),
                                      txtCod2.getText().toUpperCase(), "", ""));

        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_NIV2));
        param = (CLista) stubCliente.doPost(servletOBTENER_NIV2_X_CODIGO, param);

        // rellena los datos
        if (param.size() > 0) {
          data = (DataZBS2) param.elementAt(0);

          //         txtCod2.setEnabled(false);

          txtCod2.setText(data.getNiv2());
          sDes2 = data.getDes();

//REPASAR********************************************  Ver si interesa comparar DesL con "" o con null
          /*
               if ((this.app.getIdioma() != CApp.idiomaPORDEFECTO) && (data.getDesL() !=null))
                      sDes2 = data.getDesL();
           */
          txtDes2.setText(sDes2);

//          txtCod2.setEnabled(true);
        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg7.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
    // modo normal
    modoOperacion = modo;
    Inicializar();

  }

  void txtCod1_keyPressed(KeyEvent e) {
    txtDes1.setText("");
    InicializaNivel2();
  }

  void txtCod2_keyPressed(KeyEvent e) {
    txtDes2.setText("");
  }

} //CLASE

// action listener para los botones
class zbsActionListener
    implements ActionListener, Runnable {
  DialZBS adaptee = null;
  ActionEvent e = null;

  public zbsActionListener(DialZBS adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;

    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand() == "buscar1") { // lista nivel 1
      adaptee.btnSearchNiv1_actionPerformed(e);
    }
    else if (e.getActionCommand() == "buscar2") { // lista nivel 2
      adaptee.btnSearchNiv2_actionPerformed(e);

    }
    else if (e.getActionCommand() == "aceptar") { // aceptar
      adaptee.bAceptar = true;
      // realiza la operaci�n
      switch (adaptee.modoOperacion) {
        case DialZBS.modoALTA:
          adaptee.A�adir();
          break;
        case DialZBS.modoMODIFICAR:
          adaptee.Modificar();
          break;
        case DialZBS.modoBAJA:
          adaptee.Borrar();
          break;
      }
    }
    else if (e.getActionCommand() == "cancelar") { // cancelar
      adaptee.bAceptar = false;
      adaptee.dispose();
    }

  }
}

class zbsTxtCod1FocusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  DialZBS adaptee;

  zbsTxtCod1FocusAdapter(DialZBS adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {

    adaptee.txtCod1_focusLost();

  }
}

class zbsTxtCod2FocusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  DialZBS adaptee;

  zbsTxtCod2FocusAdapter(DialZBS adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {

    adaptee.txtCod2_focusLost();

  }
}

// lista de valores
class CListaZBS
    extends CListaValores {

  public CListaZBS(CApp a,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS) o).getDes());
  }
}

// lista de valores
class CListaZBS2
    extends CListaValores {

  protected DialZBS panel;

  public CListaZBS2(DialZBS p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.app,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtCod1.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

class DialZBS_txtCod1_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialZBS adaptee;

  DialZBS_txtCod1_keyAdapter(DialZBS adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCod1_keyPressed(e);
  }
}

class DialZBS_txtCod2_keyAdapter
    extends java.awt.event.KeyAdapter {
  DialZBS adaptee;

  DialZBS_txtCod2_keyAdapter(DialZBS adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtCod2_keyPressed(e);
  }
}
