package zbs;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Cursor;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo.CListaCat;
import catalogo.Catalogo;
import catalogo.DataCat;
import sapp.StubSrvBD;

public class PanelSuperior
    extends CPanel {
  CApp app = null;
  ResourceBundle res;
  // modos de operaci�n de la ventana
  final static int modoALTA = 0;
  final static int modoMODIFICAR = 1;
  final static int modoBAJA = 2;
  final static int modoESPERA = 5;

  // modos de operaci�n del servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;

  // contenedor de imagenes
  protected CCargadorImagen imgs = null;

  // im�genes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/aceptar.gif",
      "images/cancelar.gif"};

  final String strSERVLET = "servlet/SrvZBS";
  final String strSERVLET_NIV1 = "servlet/SrvCat";
  final String strSERVLET_NIV2 = "servlet/SrvZBS2";

  // par�metros
  protected int modoOperacion = modoALTA;
  protected int modoOperacionBk = modoALTA;
  protected StubSrvBD stubCliente = null;

  XYLayout xYLayout1 = new XYLayout();
  Label label1 = new Label();
  Label label2 = new Label();
  CCampoCodigo txtNiv2 = new CCampoCodigo();
  CCampoCodigo txtNiv1 = new CCampoCodigo();
  ButtonControl btnNiv1 = new ButtonControl();
  ButtonControl btnNiv2 = new ButtonControl();
  TextField txtDesNiv2 = new TextField();
  TextField txtDesNiv1 = new TextField();

  //C�digos de reserva
  String sNiv1Bk = "";
  String sNiv2Bk = "";

  public PanelSuperior(CApp a) {
    try {
      this.app = a;
      res = ResourceBundle.getBundle("zbs.Res" + app.getIdioma());
      stubCliente = new StubSrvBD(new URL(a.getURL() + strSERVLET));
      jbInit();
      this.setBorde(false);
      Inicializar();
      /* ******* AQUI METEMOS LA INICIACION DE ZONA y DISTRITO **** */
      this.txtNiv1.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesNiv1.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtNiv2.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesNiv2.setText(this.app.getDS_NIVEL2_DEFECTO());
      /********/
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    xYLayout1.setHeight(94);
    label1.setText(res.getString("label1.Text"));
    label2.setText(res.getString("label2.Text"));
    // carga las imagenes
    imgs = new CCargadorImagen(app, imgNAME);
    imgs.CargaImagenes();

    btnNiv1.setActionCommand("nivel1");
    btnNiv1.setImage(imgs.getImage(0));

    btnNiv2.setActionCommand("nivel2");
    btnNiv2.setImage(imgs.getImage(0));

    txtDesNiv2.setEditable(false);
    txtDesNiv1.setEditable(false);
    xYLayout1.setWidth(400);
    this.setLayout(xYLayout1);
    this.add(label1, new XYConstraints(12, 18, 54, -1));
    this.add(label2, new XYConstraints(12, 49, -1, -1));
    this.add(txtNiv2, new XYConstraints(79, 49, 49, -1));
    this.add(txtNiv1, new XYConstraints(79, 18, 49, -1));
    this.add(btnNiv1, new XYConstraints(135, 18, -1, -1));
    this.add(btnNiv2, new XYConstraints(135, 49, -1, -1));
    this.add(txtDesNiv2, new XYConstraints(174, 50, 226, -1));
    this.add(txtDesNiv1, new XYConstraints(174, 18, 227, -1));

    //Nombres para ver fuentes de eventos
    txtNiv1.setName("txtNiv1");
    txtNiv2.setName("txtNiv2");

  }

  public void Inicializar() {
    switch (modoOperacion) {

      case modoALTA:
        txtDesNiv1.setEnabled(true);
        txtDesNiv2.setEnabled(true);
        txtNiv1.setEnabled(true);
        txtNiv2.setEnabled(true);
        btnNiv1.setEnabled(true);
        if (txtDesNiv1.getText().length() > 0) {
          btnNiv2.setEnabled(true);
          txtNiv2.setEnabled(true);

        }
        else {
          btnNiv2.setEnabled(false);
          txtNiv2.setEnabled(false);
          txtNiv2.setText("");
          txtDesNiv2.setText("");
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtDesNiv1.setEnabled(false);
        txtDesNiv2.setEnabled(false);
        txtNiv1.setEnabled(false);
        txtNiv2.setEnabled(false);
        btnNiv1.setEnabled(false);
        btnNiv2.setEnabled(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
  }

  void btnNiv1_actionPerformed(ActionEvent e) {
//#// System_out.println(" btn Niv1 ActPerformed**********************");
    DataCat data;
    CMessage msgBox;

    int modo = modoOperacion;

    try {
      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();

      CListaCat lista = new CListaCat(app,
                                      res.getString("msg8.Text") +
                                      app.getNivel1(),
                                      stubCliente,
                                      strSERVLET_NIV1,
                                      servletOBTENER_X_CODIGO +
                                      Catalogo.catNIVEL1,
                                      servletOBTENER_X_DESCRIPCION +
                                      Catalogo.catNIVEL1,
                                      servletSELECCION_X_CODIGO +
                                      Catalogo.catNIVEL1,
                                      servletSELECCION_X_DESCRIPCION +
                                      Catalogo.catNIVEL1);
      lista.show();
      data = (DataCat) lista.getComponente();
      modoOperacion = modo;
      if (data != null) {
        txtNiv1.setText(data.getCod());
        txtDesNiv1.setText(data.getDes());

        //Se guarda el c�digo de reserva
        sNiv1Bk = txtNiv1.getText().trim();

        //Han cambiado datos de nivel1. Se inicializa el nivel 2
        //Se resetean las cajas de nivel2
        //NO SOLO ES NECESARIO VER QUE HAY DESCRIPCION DE N2 (al inicializar nivel2) sino tambi�n poner cajas
        // a blanc pues puede ser que antes hubiera un N2, que al cambiar N1 ya no ser� v�lido
        txtDesNiv2.setText("");
        txtNiv2.setText("");

      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
//    modoOperacion = modo;
    modoOperacion = modoALTA;
    Inicializar();
//#// System_out.println("FIN  btn Niv1 ActPerformed**********************");

  }

  void btnNiv2_actionPerformed(ActionEvent e) {
//#// System_out.println(" btn Niv2 ActPerformed**********************");
    DataZBS2 data;
    CMessage msgBox;

    int modo = modoOperacion;

    try {
      // apunta al servlet auxiliar
      stubCliente.setUrl(new URL(this.app.getURL() + strSERVLET_NIV2));
      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBSPanSup lista = new CListaZBSPanSup(this,
                                                  res.getString("msg8.Text") +
                                                  app.getNivel2(),
                                                  stubCliente,
                                                  strSERVLET_NIV2,
                                                  servletOBTENER_NIV2_X_CODIGO,
          servletOBTENER_NIV2_X_DESCRIPCION,
          servletSELECCION_NIV2_X_CODIGO,
          servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
      modoOperacion = modo;
      if (data != null) {
        txtDesNiv2.setText(data.getDes());
        txtNiv2.setText(data.getNiv2());

        //Se guarda el c�digo de reserva
        sNiv2Bk = txtNiv2.getText().trim(); //???????????????

      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
//    modoOperacion = modo;
    modoOperacion = modoALTA;
    Inicializar();
//#// System_out.println("FIN  btn Niv2 ActPerformed**********************");
  }

  void txtNiv1_keyPressed(KeyEvent e) {
    txtDesNiv1.setText("");
    txtNiv2.setText("");
    txtDesNiv2.setText("");
    txtNiv2.setEnabled(false);
    btnNiv2.setEnabled(false);

    //No hay c�digo validado
    sNiv1Bk = "";

  }

  void txtNiv2_keyPressed(KeyEvent e) {
    txtDesNiv2.setText("");
    //No hay c�digo validado
    sNiv2Bk = "";
  }

  void txtNiv1_focusLost(FocusEvent e) {
//#// System_out.println(" Niv1 focus lost**********************");
    DataCat data;
    CLista param = null;
    CMessage msg;
    String sDes1;
    int modo = modoOperacion;

    //Se quitan los blancos antes de comparar
    txtNiv1.setText(txtNiv1.getText().trim());

//    if (txtNiv1.getText().length() > 0) {

    //Codigo del focus lost solo se ejecuta si ha cambiado el valor de la caja respecto al guardado
    if (! (txtNiv1.getText().equals(sNiv1Bk))) {

      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();

      try {
        param = new CLista();
        param.setIdioma(app.getIdioma());
        param.addElement(new DataCat(txtNiv1.getText()));

        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_NIV1));
        param = (CLista) stubCliente.doPost(servletOBTENER_X_CODIGO +
                                            Catalogo.catNIVEL1, param);

//#// System_out.println(" Niv1 focus lost va a B.DATOS **********************");

        // rellena los datos
        if (param.size() > 0) {
          data = (DataCat) param.elementAt(0);

          txtNiv1.setText(data.getCod());
          sDes1 = data.getDes();
          txtDesNiv1.setText(sDes1);

          //Se guarda el c�digo de reserva
          sNiv1Bk = txtNiv1.getText().trim();

          //Han cambiado datos de nivel1. Se inicializa el nivel 2
          //Se resetean las cajas de nivel2
          //NO SOLO ES NECESARIO VER QUE HAY DESCRIPCION DE N2 (al inicializar nivel2) sino tambi�n poner cajas
          // a blanc pues puede ser que antes hubiera un N2, que ala cambiar N1 ya no ser� v�lido
          txtDesNiv2.setText("");
          txtNiv2.setText("");
          txtNiv2.setEnabled(true);
        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg9.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        ex.printStackTrace();
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
    // modo normal
//    modoOperacion = modo;
    modoOperacion = modoALTA;
    Inicializar();
//#// System_out.println(" FIN Niv1 focus lost**********************");

  }

  void txtNiv2_focusLost(FocusEvent e) {
    // datos de envio
    DataZBS2 data;
    CLista param = null;
    CMessage msg;
    String sDes2;
    int modo = modoOperacion;

//    if (txtNiv2.getText().length() > 0) {

    //Se quitan los blancos antes de comparar
    txtNiv2.setText(txtNiv2.getText().trim());

    //Codigo del focus lost solo se ejecuta si ha cambiado el valor de la caja respecto al guardado
    if (! (txtNiv2.getText().equals(sNiv2Bk))) {

      // apunta al servlet auxiliar
      modoOperacion = modoESPERA;
      Inicializar();

      try {
        param = new CLista();
        param.setIdioma(app.getIdioma());
        param.addElement(new DataZBS2(txtNiv1.getText().toUpperCase(),
                                      txtNiv2.getText().toUpperCase(), "", ""));

        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strSERVLET_NIV2));
        param = (CLista) stubCliente.doPost(servletOBTENER_NIV2_X_CODIGO, param);

        // rellena los datos
        if (param.size() > 0) {
          data = (DataZBS2) param.elementAt(0);
          txtNiv2.setText(data.getNiv2());
          sDes2 = data.getDes();
          txtNiv2.setEnabled(true);
          txtDesNiv2.setText(sDes2);

          //Se guarda el c�digo de reserva
          sNiv2Bk = txtNiv2.getText().trim(); //???????????????

        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg9.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        ex.printStackTrace();
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }
    }
    // modo normal
//    modoOperacion = modo;
    modoOperacion = modoALTA;
    Inicializar();
//#// System_out.println(" FIN Niv2 focus lost**********************");

  }

}

// lista de valores
class CListaZBSPanSup
    extends CListaValores {

  protected PanelSuperior panel;

  public CListaZBSPanSup(PanelSuperior p,
                         String title,
                         StubSrvBD stub,
                         String servlet,
                         int obtener_x_codigo,
                         int obtener_x_descricpcion,
                         int seleccion_x_codigo,
                         int seleccion_x_descripcion) {
    super(p.app,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed();

  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtNiv1.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}
