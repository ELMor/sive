//Title:        Volcado de casos individuales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Volcado de casos individuales (incluidas preguntas del protocolo) de una enfermedad a fichero ASCII.

package volCasosInd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.Vector;

import Registro.RegistroConsultas;
import capp.CLista;
import sapp.DBServlet;

public class SrvModPreg
    extends DBServlet {

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  final int modoINFOPREG = 7;
  final int modoVOLCADO = 8;

  final int MODELO = 10;
  final int PREGUNTA = 11;
  final int MOD_Y_PREG = 12;

  // Para el volcado de casos individuales sin preguntas
  final String NO_ENFER = "()";
  final String NO_PREGS = "('')";

  // ARG:
  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {
    // variables temporales
    String CD_TSIVE = "";
    String CD_MODELO = "";
    String DS_MODELO = "";
    String DSL_MODELO = "";
    String CD_NIVEL_1 = "";
    String CD_NIVEL_2 = "";
    String CD_CA = "";
    String IT_OK = "";
    String CD_ENFCIE = "";
    java.sql.Date FC_BAJAM = null;
    int numAux = 0;

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;
    int j = 1;

    // objetos de datos
    CLista data = new CLista();
    DataModPreg dModPreg = null;
    DataInfoPreg dInfoPreg = null;
    DataVolCasInd dVolCas = null;

    String fechaCadena = null;

    //para el formato de las fechas
    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;

      // establece la conexi�n con la base de datos
    }
    con = openConnection();

    /*
         if (aplicarLortad)
         {
       // Se obtiene la clave de acceso del usuario que se ha conectado
       passwd = sapp.Funciones.getPassword(con, st, rs, user);
       closeConnection(con);
       con=null;
       // Se abre una nueva conexion para el usuario
       con = openConnection(user, passwd);
         }
     */
    // Se crea un objeto RegistroConsultas para aplicar la Lortad
    registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

    con.setAutoCommit(true);

    ////////////////////////////////
    /*    String traze = "ALTER SESSION SQL_TRACE = TRUE";
        boolean resp;
        st = con.prepareStatement(traze);
        resp = st.execute();
     */

    data.setState(CLista.listaVACIA);
    data = null;
    try {

      // modos de operaci�n
      switch (opmode) {

        case modoVOLCADO:

          // // System_out.println(" SrvModPreg modoVOLCADO");

          dVolCas = (DataVolCasInd) param.firstElement();

          String sep = dVolCas.sep;

          if (dVolCas.enfer.equals(NO_ENFER)) {
            // No se ha rellenado el campo enfermedad y
            // se seleccionan todas las EDO
            String edos = new String();
            String queryEDO = "select CD_ENFCIE from SIVE_ENFEREDO";
            st = con.prepareStatement(queryEDO);
            rs = st.executeQuery();
            edos = "(";
            while (rs.next()) {
              edos += "'" + rs.getString("CD_ENFCIE") + "',";
            }
            rs.close();
            //st.close();
            st.close();
            rs = null;
            st = null;

            edos = edos.substring(0, edos.lastIndexOf(',')) + ")";
            //#// System_out.println(" SrvModPreg: edos " + edos);
            query = "select * from SIVE_EDOIND "
                + " where CD_ENFCIE in " + edos + " and ";
          }
          else {
            query = "select * from SIVE_EDOIND "
                + " where CD_ENFCIE = ? and ";
          }

          if (dVolCas.anoD.equals(dVolCas.anoH)) {
            query = query +
                " CD_ANOEPI = ? and CD_SEMEPI >= ? and CD_SEMEPI <= ? ";
          }
          else {
            query = query + " ((CD_ANOEPI = ? and CD_SEMEPI >= ?) or (CD_ANOEPI > ? and CD_ANOEPI < ?) or (CD_ANOEPI = ? and CD_SEMEPI <= ?)) ";

          }
          if (!dVolCas.area.equals("")) {
            query = query + " and CD_NIVEL_1 = ? ";
          }
          if (!dVolCas.dist.equals("")) {
            query = query + " and CD_NIVEL_2 = ? ";
          }
          if (!dVolCas.zbs.equals("")) {
            query = query + " and CD_ZBS = ? ";
          }
          if (!dVolCas.prov.equals("")) {
            query = query + " and CD_PROV = ? ";
          }
          if (!dVolCas.mun.equals("")) {
            query = query + " and CD_MUN = ? ";
          }

          // prepara la lista de resultados
          data = new CLista();

          //#// System_out.println("SrvModPreg : " + query);

          // lanza la query
          st = con.prepareStatement(query);
          int par = 1;
          if (!dVolCas.enfer.equals(NO_ENFER)) {
            st.setString(par, dVolCas.enfer);
            par++;
            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(dVolCas.enfer);
          }
          if (dVolCas.anoD.equals(dVolCas.anoH)) {
            st.setString(par, dVolCas.anoD);
            par++;
            st.setString(par, dVolCas.semD);
            par++;
            st.setString(par, dVolCas.semH);
            par++;
            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(dVolCas.anoD);
            registroConsultas.insertarParametro(dVolCas.semD);
            registroConsultas.insertarParametro(dVolCas.semH);
          }
          else {
            st.setString(par, dVolCas.anoD);
            par++;
            st.setString(par, dVolCas.semD);
            par++;
            st.setString(par, dVolCas.anoD);
            par++;
            st.setString(par, dVolCas.anoH);
            par++;
            st.setString(par, dVolCas.anoH);
            par++;
            st.setString(par, dVolCas.semH);
            par++;
            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(dVolCas.anoD);
            registroConsultas.insertarParametro(dVolCas.semD);
            registroConsultas.insertarParametro(dVolCas.anoD);
            registroConsultas.insertarParametro(dVolCas.anoH);
            registroConsultas.insertarParametro(dVolCas.anoH);
            registroConsultas.insertarParametro(dVolCas.semH);
          }
          if (!dVolCas.area.equals("")) {
            st.setString(par, dVolCas.area);
            par++;
            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(dVolCas.area);
          }
          if (!dVolCas.dist.equals("")) {
            st.setString(par, dVolCas.dist);
            par++;
            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(dVolCas.dist);
          }
          if (!dVolCas.zbs.equals("")) {
            st.setString(par, dVolCas.zbs);
            par++;
            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(dVolCas.zbs);
          }
          if (!dVolCas.prov.equals("")) {
            st.setString(par, dVolCas.prov);
            par++;
            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(dVolCas.prov);
          }
          if (!dVolCas.mun.equals("")) {
            st.setString(par, dVolCas.mun);
            par++;
            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro(dVolCas.mun);
          }

          rs = st.executeQuery();

          // ARG: Registro LORTAD
          registroConsultas.registrar("SIVE_EDOIND", query, "CD_ENFERMO",
                                      "", "SrvModPreg", true);

          // extrae la p�gina requerida
          i = 1;

          Vector enfermos = new Vector();
          Vector nm_edos = new Vector();

          while (rs.next()) {
            String linea = new String();

            String queryEnf = new String();
            String queryPreg = new String();
            String cd_enfermo = new String();
            cd_enfermo = rs.getString("CD_ENFERMO");
            enfermos.addElement(cd_enfermo);

            String nm_edo = new String();
            nm_edo = rs.getString("NM_EDO");
            nm_edos.addElement(nm_edo);

            //#// System_out.println ("SrvModPreg: cd_enfermo nm_edo " + cd_enfermo + nm_edo);
            // Para la edad en anos y en meses

            String edadA = new String();
            String edadM = new String();
            String cd_sexo = new String();

            /*
                       queryEnf = " select FC_NAC, CD_SEXO from SIVE_ENFERMO "
                     + " where CD_ENFERMO = ? ";
                       //#// System_out.println ("SrvModPreg: enfermo " + queryEnf);
                 PreparedStatement stAux = con.prepareStatement(queryEnf);
                       //#// System_out.println ("SrvModPreg: sentencia enfermo preparada");
                       stAux.setString(1, cd_enfermo);
                       //stAux.setString(1,cd_enfermo);
                       ResultSet rsAux = stAux.executeQuery();
                       //#// System_out.println ("SrvModPreg: sentencia enfermo ejecutada");
                       if (rsAux.next()) {
              //#// System_out.println ("SrvModPreg: hay un resultado en consulta Enf");
              java.util.Date dateNac = rsAux.getDate("FC_NAC");
              //#// System_out.println ("SrvModPreg : date " + dateNac);
              cd_sexo = rsAux.getString("CD_SEXO");
              if (cd_sexo==null)
                cd_sexo = " ";
              //#// System_out.println ("SrvModPreg : sexo " + cd_sexo);
              if (dateNac!=null){
                //#// System_out.println ("SrvModPreg : dateNac es distinto de null");
                ////#// System_out.println ("SrvModPreg : int fecha " + Fechas.edadAnios(dateNac));
                Integer aux ;//= new Integer(Fechas.edadAnios(dateNac));
                java.util.Date hoy = new java.util.Date();
                long mili_anio =  31557600000L;
                long mili_mes =  2626560000L;
                long edad_mili =  hoy.getTime() - dateNac.getTime();
                //#// System_out.println ("SrvModPreg :long edad_mili  " + edad_mili);
                if ( edad_mili > 0) {
                  aux = new Integer((int) (edad_mili / mili_anio));
                  //#// System_out.println ("SrvModPreg : int anos " + (int) (edad_mili / mili_anio));
                }else{
                  aux = new Integer(0);
                  //#// System_out.println ("SrvModPreg : int anos  " + aux);
                }
                edadA = aux.toString();
                //#// System_out.println ("SrvModPreg : edadA " + edadA);
                if ( edad_mili > 0) {
                  aux = new Integer((int) (edad_mili / mili_mes));
                  //#// System_out.println ("SrvModPreg : int meses " + (int) (edad_mili / mili_mes));
                }else{
                  aux = new Integer(0);
                  //#// System_out.println ("SrvModPreg : int meses  " + aux);
                }
                //aux =  new Integer(Fechas.edadMeses(dateNac));
                edadM = aux.toString();
                //#// System_out.println ("SrvModPreg : edadM " + edadM);
              }
              else {
                edadA = " ";
                edadM = " ";
                //#// System_out.println ("SrvModPreg : Me ha devuelto una fecha nula" );
              }
                       }
                       stAux.close();
                       rsAux.close();
                       stAux = null;
                       rsAux = null;
             */

            linea = nm_edo + sep;
            String aux = new String();
            aux = rs.getString("CD_NIVEL_1");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: CD_NIVEL_1 linea " + linea);

            }
            aux = rs.getString("CD_NIVEL_2");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: CD_NIVEL_2 linea " + linea);

            }
            aux = rs.getString("CD_ZBS");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: CD_ZBS linea " + linea);

            }
            aux = rs.getString("CD_PROV");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: CD_PROV linea " + linea);

            }
            aux = rs.getString("CD_MUN");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: CD_MUN linea " + linea);

            }
            linea = linea + rs.getString("CD_ENFCIE") + sep +
                rs.getString("CD_ANOEPI") + sep +
                rs.getString("CD_SEMEPI") + sep +
                "*@*" + sep +
                edadM + sep +
                "*@*" + sep;

            //// System_out.println ("SrvModPreg: hasta sexo linea " + linea);

            java.sql.Date dAux = rs.getDate("FC_INISNT");
            if (dAux == null) {
              //#// System_out.println ("SrvModPreg: FC_INISNT es nula" );
              linea = linea + " " + sep;
            }
            else {
              //#// System_out.println ("SrvModPreg: FC_INISNT NO nula" );
              linea = linea + formater.format(dAux) + sep;
            }
            /*
                       aux = rs.getString("FC_INISNT");
                       if (aux==null){
              //#// System_out.println ("SrvModPreg: FC_INISNT es nula" );
              linea = linea + " " + sep;
                       }
                       else{
              //#// System_out.println ("SrvModPreg: fecha leida BD " + aux);
              java.util.Date fAux = new java.util.Date(aux);
              //#// System_out.println ("SrvModPreg: fecha util a partir de string " + fAux.toString());
              java.sql.Date dAux = new java.sql.Date(fAux.getTime());
              //#// System_out.println ("SrvModPreg: fecha sql a partir de util " + dAux.toString());
              //#// System_out.println ("SrvModPreg: formater a partir de fecha sql " + formater.format(dAux) );
              linea = linea + formater.format(dAux) + sep;
                       }
             */
            aux = rs.getString("IT_ASOCIADO");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: IT_ASOCIADO linea " + linea);

            }
            aux = rs.getString("DS_COLECTIVO");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: DS_COLECTIVO linea " + linea);

            }
            aux = rs.getString("IT_DERIVADO");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: IT_DERIVADO linea " + linea);

            }
            aux = rs.getString("DS_CENTRODER");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: DS_DENTRODER linea " + linea);

            }
            aux = rs.getString("IT_DIAGCLI");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: IT_DIAGCLI linea " + linea);

            }
            aux = rs.getString("IT_DIAGSERO");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: IT_DIAGSERO linea " + linea);

            }
            aux = rs.getString("IT_DIAGMICRO");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: IT_DIAGMICRO linea " + linea);

            }
            aux = rs.getString("DS_DIAGOTROS");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: DS_DIAGOTROS linea " + linea);

            }
            aux = rs.getString("CD_CLASIFDIAG");
            if (aux == null) {
              linea = linea + " " + sep;
            }
            else {
              linea = linea + aux + sep;

              //// System_out.println ("SrvModPreg: CD_CLASIFDIAG linea " + linea);

            }
            dAux = rs.getDate("FC_RECEP");
            //#// System_out.println ("SrvModPreg: FC_RECEP formater " + formater.format(dAux));
            linea = linea + formater.format(dAux) + sep;
            /*
                       aux = rs.getString("FC_RECEP");
                       //#// System_out.println ("SrvModPreg: rs.getString(FC_RECEP) " +  aux);
                       java.util.Date fAux = new java.util.Date(aux);
                       //#// System_out.println ("SrvModPreg: FC_RECEP util.Date " + fAux.toString());
                       java.sql.Date dAux = new java.sql.Date(fAux.getTime());
                       //#// System_out.println ("SrvModPreg: FC_RECEP sql.Date " + dAux.toString());
                       //#// System_out.println ("SrvModPreg: FC_RECEP formater " + formater.format(dAux));
                       linea = linea + formater.format(dAux) + sep;
                       //linea = linea + rs.getString("FC_RECEP") + sep ;
             */

            /*
                       // Para las preguntas
                       if (!dVolCas.codsPreg.equals(NO_PREGS)) {
              queryPreg = "select DS_RESPUESTA from SIVE_RESP_EDO "
                      + " where CD_TSIVE = ? and "
                      + " NM_EDO = ? and "
                      + " CD_PREGUNTA in " + dVolCas.codsPreg ;
              //#// System_out.println ("SrvModPreg: preguntas " + queryPreg);
              stAux = con.prepareStatement(queryPreg);
              stAux.setString(1,dVolCas.tSive);
              //#// System_out.println ("SrvModPreg: tSive " + dVolCas.tSive);
              stAux.setString(2, nm_edo);
              //#// System_out.println ("SrvModPreg: nm_edo " + nm_edo);
              //stAux.setString(3, dVolCas.codsPreg);
              ////#// System_out.println ("SrvModPreg: codsPreg " + dVolCas.codsPreg);
              rsAux = stAux.executeQuery();
              //#// System_out.println ("SrvModPreg: he ejecutado la query ");
              String preg = new String();
              while (rsAux.next()) {
                preg = rsAux.getString("DS_RESPUESTA");
                if (preg != null)
                  linea = linea + preg + sep;
                else
                  linea = linea + " " + sep;
              }
              stAux.close();
              rsAux.close();
              stAux = null;
              rsAux = null;
                       }
             */

            data.addElement(linea);
          }
          rs.close();
          //st.close();
          st.close();
          rs = null;
          st = null;

          // System_out.println("SrvModPreg : data " + data.size()
          //                   + " enfermos " + enfermos.size()
          //                   + " nm_edos " + nm_edos.size());

          PreparedStatement stAux = null;
          ResultSet rsAux = null;
          for (int ji = 0; ji < data.size(); ji++) {
            String edadA = "";
            String edadM = "";
            String cd_sexo = "";

            String queryEnf = " select FC_NAC, CD_SEXO from SIVE_ENFERMO "
                + " where CD_ENFERMO = ? ";
            //// System_out.println ("SrvModPreg: enfermo " + queryEnf);
            stAux = con.prepareStatement(queryEnf);
            //#// System_out.println ("SrvModPreg: sentencia enfermo preparada");
            stAux.setString(1, (String) enfermos.elementAt(ji));
            //stAux.setString(1,cd_enfermo);

            // ARG: Se a�aden parametros
            registroConsultas.insertarParametro( (String) enfermos.elementAt(ji));

            rsAux = stAux.executeQuery();

            // ARG: Registro LORTAD
            registroConsultas.registrar("SIVE_ENFERMO", queryEnf, "CD_ENFERMO",
                                        (String) enfermos.elementAt(ji),
                                        "SrvModPreg", true);

            //// System_out.println ("SrvModPreg: sentencia enfermo ejecutada");
            if (rsAux.next()) {
              //// System_out.println ("SrvModPreg: hay un resultado en consulta Enf");

              java.util.Date dateNac = rsAux.getDate("FC_NAC");
              //// System_out.println ("SrvModPreg : date " + dateNac);
              cd_sexo = rsAux.getString("CD_SEXO");
              if (cd_sexo == null) {
                cd_sexo = " ";
                //// System_out.println ("SrvModPreg : sexo " + cd_sexo);
              }
              if (dateNac != null) {
                //// System_out.println ("SrvModPreg : dateNac es distinto de null");
                ////#// System_out.println ("SrvModPreg : int fecha " + Fechas.edadAnios(dateNac));
                Integer aux; //= new Integer(Fechas.edadAnios(dateNac));
                java.util.Date hoy = new java.util.Date();
                long mili_anio = 31557600000L;
                long mili_mes = 2626560000L;
                long edad_mili = hoy.getTime() - dateNac.getTime();
                //// System_out.println ("SrvModPreg :long edad_mili  " + edad_mili);
                if (edad_mili > 0) {
                  aux = new Integer( (int) (edad_mili / mili_anio));
                  //// System_out.println ("SrvModPreg : int anos " + (int) (edad_mili / mili_anio));
                }
                else {
                  aux = new Integer(0);
                  //// System_out.println ("SrvModPreg : int anos  " + aux);
                }

                edadA = aux.toString();
                //// System_out.println ("SrvModPreg : edadA " + edadA);

                if (edad_mili > 0) {
                  aux = new Integer( (int) (edad_mili / mili_mes));
                  //// System_out.println ("SrvModPreg : int meses " + (int) (edad_mili / mili_mes));
                }
                else {
                  aux = new Integer(0);
                  //// System_out.println ("SrvModPreg : int meses  " + aux);
                }

                //aux =  new Integer(Fechas.edadMeses(dateNac));
                edadM = aux.toString();
                //// System_out.println ("SrvModPreg : edadM " + edadM);
              }
              else {
                edadA = " ";
                edadM = " ";
                //// System_out.println ("SrvModPreg : Me ha devuelto una fecha nula" );
              }
            }

            /*stAux.close();*/
            rsAux.close();
            stAux.close();
            stAux = null;
            rsAux = null;

            String lin = (String) data.elementAt(ji);
            lin = lin.substring(0, lin.indexOf("*@*")) +
                edadA + sep +
                edadM + sep +
                cd_sexo + lin.substring(lin.lastIndexOf("*@*") + 3);
            data.setElementAt(lin, ji);
            // System_out.println ("SrvModPreg : " + lin);
          }

          /*
                  // Para las preguntas
                  if (!dVolCas.codsPreg.equals(NO_PREGS)) {
                    for (int xi=0; xi<data.size(); xi++){
               String queryPreg = "select DS_RESPUESTA from SIVE_RESP_EDO "
                              + " where CD_TSIVE = ? and "
                              + " NM_EDO = ? and "
                              + " CD_PREGUNTA in " + dVolCas.codsPreg ;
                      // System_out.println ("SrvModPreg: preguntas " + queryPreg);
                      stAux = con.prepareStatement(queryPreg);
                      stAux.setString(1,dVolCas.tSive);
                      //#// System_out.println ("SrvModPreg: tSive " + dVolCas.tSive);
                      stAux.setString(2, (String)nm_edos.elementAt(xi));
                      //#// System_out.println ("SrvModPreg: nm_edo " + nm_edo);
                      //stAux.setString(3, dVolCas.codsPreg);
                      ////#// System_out.println ("SrvModPreg: codsPreg " + dVolCas.codsPreg);
                      rsAux = stAux.executeQuery();
                      //#// System_out.println ("SrvModPreg: he ejecutado la query ");
                      String preg = new String();
                      String lin = (String) data.elementAt(xi);
                      while (rsAux.next()) {
                        preg = rsAux.getString("DS_RESPUESTA");
                        if (preg != null)
                          lin = lin + preg + sep;
                        else
                          lin = lin + " " + sep;
                      }
                      // System_out.println ("SrvModPreg : " + lin);
                      data.setElementAt(lin,xi);
                      //stAux.close();
                      rsAux.close();
                      stAux = null;
                      rsAux = null;
                    }
                  }
           */

          // Para las preguntas
          if (!dVolCas.codsPreg.equals(NO_PREGS)) {
            Vector vCodsPreg = dVolCas.vCodsPreg;
            String sCodsPreg = null;
            for (int xi = 0; xi < data.size(); xi++) {
              String lin = (String) data.elementAt(xi);
              for (int v = 0; v < vCodsPreg.size(); v++) {
                sCodsPreg = (String) vCodsPreg.elementAt(v);

                String queryPreg = "select DS_RESPUESTA from SIVE_RESP_EDO "
                    + " where CD_TSIVE = ? and "
                    + " NM_EDO = ? and "
                    + " CD_PREGUNTA = '" + sCodsPreg + "'";

                stAux = con.prepareStatement(queryPreg);
                stAux.setString(1, dVolCas.tSive);

                stAux.setString(2, (String) nm_edos.elementAt(xi));
                rsAux = stAux.executeQuery();

                String preg = new String();
                if (!rsAux.next()) {
                  lin = lin + " " + sep;
                }
                else {
                  preg = rsAux.getString("DS_RESPUESTA");
                  if (preg != null) {
                    lin = lin + preg + sep;
                  }
                  else {
                    lin = lin + " " + sep;
                  }
                }
                rsAux.close();
                stAux.close();
                stAux = null;
                rsAux = null;
              }

              data.setElementAt(lin, xi);
            }
          }

          // Se construye la cabecera
          String strCabecera = "";
          strCabecera = "NM_EDO" + sep + "CD_NIVEL_1" + sep + "CD_NIVEL_2" +
              sep + "CD_ZBS" + sep + "CD_PROV" + sep +
              "CD_MUN" + sep + "CD_ENFCIE" + sep + "CD_ANOEPI" + sep +
              "CD_SEMEPI" + sep + "EDAD_ANOS" + sep +
              "EDAD_MESES" + sep + "CD_SEXO" + sep + "FC_INISNT" + sep +
              "IT_ASOCIADO" + sep +
              "DS_COLECTIVO" + sep + "IT_DERIVADO" + sep + "DS_CENTRODER" + sep +
              "IT_DIAGCLI" + sep +
              "IT_DIAGSERO" + sep + "IT_DIAGMICRO" + sep + "DS_DIAGOTROS" + sep +
              "CD_CLASIFDIAG" + sep + "FC_RECEP";

          Vector vCodsPreg = dVolCas.vCodsPreg;
          for (int v = 0; v < vCodsPreg.size(); v++) {
            String sCodsPreg = (String) vCodsPreg.elementAt(v);
            strCabecera = strCabecera + sep + sCodsPreg;
          }

          // Se a�ade la cabecera al final del data
          data.addElement(strCabecera);

          break;

        case modoINFOPREG:
          dInfoPreg = (DataInfoPreg) param.firstElement();
          // Se obtiene informacion de una pregunta
          //#// System_out.println("SrvModPreg: modoINFOPREG");
          query = "select distinct a.CD_MODELO, a.DS_MODELO, a.FC_ALTAM, a.FC_BAJAM, b.NM_LIN, c.DS_TEXTO "
              + " from SIVE_MODELO a, SIVE_LINEA_ITEM b, SIVE_LINEASM c "
              + " where a.CD_TSIVE = ? and "
              + " a.CD_MODELO = b.CD_MODELO and "
              + " b.CD_PREGUNTA = ? and "
              + " b.NM_LIN = c.NM_LIN ";
          if (param.getFilter().length() > 0) {
            query = query + " and b.CD_MODELO > ? ";
          }

          // prepara la lista de resultados
          data = new CLista();

          //#// System_out.println("SrvModPreg : " + query);

          // lanza la query
          st = con.prepareStatement(query);
          st.setString(1, dInfoPreg.tSive);
          //#// System_out.println("SrvModPreg : tSive " + dInfoPreg.tSive);
          st.setString(2, dInfoPreg.codPreg);
          //#// System_out.println("SrvModPreg : tSive " + dInfoPreg.codPreg);

          if (param.getFilter().length() > 0) {
            st.setString(3, dInfoPreg.codPreg);
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          i = 1;

          while (rs.next()) {

            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              data.setFilter( ( (DataInfoPreg) data.lastElement()).codMod);
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            SimpleDateFormat dateF = new SimpleDateFormat("dd/MM/yyyy");
            StringBuffer sBF = new StringBuffer();
            String fA = new String();
            String fB = new String();
            java.util.Date dateAux = rs.getDate("FC_ALTAM");
            if (dateAux != null) {
              sBF = dateF.format(dateAux, sBF, new FieldPosition(0));
              fA = sBF.toString();
            }
            else {
              fA = "-";

            }
            sBF = new StringBuffer();
            dateAux = rs.getDate("FC_BAJAM");
            if (dateAux != null) {
              sBF = dateF.format(dateAux, sBF, new FieldPosition(0));
              fB = sBF.toString();
            }
            else {
              fB = "-";

            }

            data.addElement(new DataInfoPreg(rs.getString(1), rs.getString(2),
                                             fA, fB, //rs.getString(3),rs.getString(4),
                                             rs.getString(5), dInfoPreg.codPreg,
                                             rs.getString(6), dInfoPreg.tSive));

          }
          rs.close();
          //st.close();
          st.close();
          rs = null;
          st = null;
          break;

          // b�squeda de modelo
        case servletSELECCION_X_CODIGO:
          dModPreg = (DataModPreg) param.firstElement();
          int caso = 0;

          //#// System_out.println("SrvModPreg: servletSELECCION_X_CODIGO");

          switch (dModPreg.modo) {
            case MODELO:

              //#// System_out.println("SrvModPreg: MODELO");
              query = "select CD_MODELO, DS_MODELO from SIVE_MODELO "
                  + " where CD_TSIVE = ? and ";
              if (param.getFilter().length() > 0) {
                query = query +
                    " CD_MODELO like ? and CD_MODELO > ? order by CD_MODELO";
              }
              else {
                query = query + " CD_MODELO like ? order by CD_MODELO";
              }
              break;
            case PREGUNTA:

              //#// System_out.println("SrvModPreg: PREGUNTA");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and ";
              if (param.getFilter().length() > 0) {
                query = query +
                    " CD_PREGUNTA like ? and CD_PREGUNTA > ? order by CD_PREGUNTA";
              }
              else {
                query = query + " CD_PREGUNTA like ? order by CD_PREGUNTA";
              }
              break;
            case MOD_Y_PREG:

              //#// System_out.println("SrvModPreg: MOD_Y_PREG");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and "
                  + " CD_PREGUNTA in (select CD_PREGUNTA from SIVE_LINEA_ITEM where CD_MODELO = ?) and ";
              if (param.getFilter().length() > 0) {
                query = query +
                    " CD_PREGUNTA like ? and CD_PREGUNTA > ? order by CD_PREGUNTA";
              }
              else {
                query = query + " CD_PREGUNTA like ? order by CD_PREGUNTA";
              }
              break;
          }

          // prepara la lista de resultados
          data = new CLista();

          //#// System_out.println("SrvModPreg : " + query);

          // lanza la query
          st = con.prepareStatement(query);
          j = 1;
          st.setString(j, dModPreg.tSive);
          //#// System_out.println("SrvModPreg : tSive " + dModPreg.tSive + " " + j);
          j++;

          // Da valor a los parametros

          switch (dModPreg.modo) {
            case MODELO:
              st.setString(j, dModPreg.codMod + '%');
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.codMod + '%' + " " + j);
              j++;
              break;
            case PREGUNTA:
              st.setString(j, dModPreg.codPreg + '%');
              //#// System_out.println("SrvModPreg : codPreg " + dModPreg.codPreg + '%' + " " + j);
              j++;
              break;
            case MOD_Y_PREG:
              st.setString(j, dModPreg.codMod);
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.codMod + " " + j);
              j++;
              st.setString(j, dModPreg.codPreg + '%');
              //#// System_out.println("SrvModPreg : codPreg " + dModPreg.codPreg + '%' + " " + j);
              j++;
              break;
          }

          if (param.getFilter().length() > 0) {
            st.setString(j, param.getFilter().trim());
            j++;
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          i = 1;

          while (rs.next()) {

            ////#// System_out.println("SrvModPreg: entro en el while");

            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (dModPreg.modo == MODELO) {
                data.setFilter( ( (DataModPreg) data.lastElement()).codMod);
              }
              else {
                data.setFilter( ( (DataModPreg) data.lastElement()).codPreg);
              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            CD_MODELO = (rs.getString(1));
            DS_MODELO = (rs.getString(2));

            ////#// System_out.println("SrvModPreg: CD y DS " + CD_MODELO + " " + DS_MODELO);

            if (dModPreg.modo == MODELO) {
              data.addElement(new DataModPreg(CD_MODELO, DS_MODELO,
                                              "", "", dModPreg.tSive));
              i++;
            }
            else {
              data.addElement(new DataModPreg("", "",
                                              CD_MODELO, DS_MODELO,
                                              dModPreg.tSive));
              i++;
            }
          }
          rs.close();
          //st.close();
          st.close();
          rs = null;
          st = null;
          break;

        case servletSELECCION_X_DESCRIPCION:
          dModPreg = (DataModPreg) param.firstElement();
          //#// System_out.println("SrvModPreg: servletSELECCION_X_DESCRIPCION");
          caso = 0;

          switch (dModPreg.modo) {
            // ARG: upper (21/5/02)
            case MODELO:

              //#// System_out.println("SrvModPreg: MODELO");
              query = "select CD_MODELO, DS_MODELO from SIVE_MODELO "
                  + " where CD_TSIVE = ? and ";
              if (param.getFilter().length() > 0) {
                query = query + " upper(DS_MODELO) like upper(?) and upper(DS_MODELO) > upper(?) order by DS_MODELO";
              }
              else {
                query = query +
                    " upper(DS_MODELO) like upper(?) order by DS_MODELO";
              }
              break;
            case PREGUNTA:

              //#// System_out.println("SrvModPreg: PREGUNTA ");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and ";
              if (param.getFilter().length() > 0) {
                query = query + " upper(DS_PREGUNTA) like upper(?) and upper(DS_PREGUNTA) > upper(?) order by DS_PREGUNTA";
              }
              else {
                query = query +
                    " upper(DS_PREGUNTA) like upper(?) order by DS_PREGUNTA";
              }
              break;
            case MOD_Y_PREG:

              //#// System_out.println("SrvModPreg: MOD_Y_PREG");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and "
                  + " CD_PREGUNTA in (select CD_PREGUNTA from SIVE_LINEA_ITEM where CD_MODELO = ?) and ";
              if (param.getFilter().length() > 0) {
                query = query + " upper(DS_PREGUNTA) like upper(?) and upper(DS_PREGUNTA) > upper(?) order by DS_PREGUNTA";
              }
              else {
                query = query +
                    " upper(DS_PREGUNTA) like upper(?) order by DS_PREGUNTA";
              }
              break;
          }

          // prepara la lista de resultados
          data = new CLista();

          //#// System_out.println("SrvModPreg : " + query);

          // lanza la query
          st = con.prepareStatement(query);
          j = 1;
          st.setString(j, dModPreg.tSive);
          //#// System_out.println("SrvModPreg : tSive " + dModPreg.tSive + " " + j);
          j++;

          // Da valor a los parametros

          switch (dModPreg.modo) {
            case MODELO:
              st.setString(j, '%' + dModPreg.desMod + '%');
              //#// System_out.println("SrvModPreg : desMod " + dModPreg.desMod + '%'+ " " + j);
              j++;
              break;
            case PREGUNTA:
              st.setString(j, '%' + dModPreg.desPreg + '%');
              //#// System_out.println("SrvModPreg : desPreg " + dModPreg.desPreg + '%'+ " " + j);
              j++;
              break;
            case MOD_Y_PREG:
              st.setString(j, dModPreg.codMod);
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.desMod + " " + j);
              j++;
              st.setString(j, '%' + dModPreg.desPreg + '%');
              //#// System_out.println("SrvModPreg : desPreg " + dModPreg.desPreg + '%'+ " " + j);
              j++;
              break;
          }

          if (param.getFilter().length() > 0) {
            st.setString(j, param.getFilter().trim());
            j++;
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          i = 1;

          while (rs.next()) {
            ////#// System_out.println("SrvModPreg : entro en el while ");
            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              if (dModPreg.modo == MODELO) {
                data.setFilter( ( (DataModPreg) data.lastElement()).desMod);
              }
              else {
                data.setFilter( ( (DataModPreg) data.lastElement()).desPreg);
              }
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            CD_MODELO = (rs.getString(1));
            DS_MODELO = (rs.getString(2));

            ////#// System_out.println("SrvModPreg: CD y DS " + CD_MODELO + " " + DS_MODELO);

            if (dModPreg.modo == MODELO) {
              data.addElement(new DataModPreg(CD_MODELO, DS_MODELO,
                                              "", "", dModPreg.tSive));
              i++;
            }
            else {
              data.addElement(new DataModPreg("", "",
                                              CD_MODELO, DS_MODELO,
                                              dModPreg.tSive));
              i++;
            }
          }
          rs.close();
          //st.close();
          st.close();
          rs = null;
          st = null;
          break;

        case servletOBTENER_X_CODIGO:
          dModPreg = (DataModPreg) param.firstElement();
          //#// System_out.println("SrvModPreg: servletOBTENER_X_CODIGO");
          caso = 0;

          switch (dModPreg.modo) {
            case MODELO:

              //#// System_out.println("SrvModPreg: MODELO");
              caso = MODELO;
              query = "select CD_MODELO, DS_MODELO from SIVE_MODELO "
                  + " where CD_TSIVE = ? and ";
              query = query + " CD_MODELO = ? order by CD_MODELO";
              break;
            case PREGUNTA:

              //#// System_out.println("SrvModPreg: PREGUNTA");
              caso = PREGUNTA;
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and ";
              query = query + " CD_PREGUNTA = ? order by CD_PREGUNTA";
              break;
            case MOD_Y_PREG:

              //#// System_out.println("SrvModPreg: MOD_Y_PREG");
              caso = MOD_Y_PREG;
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and "
                  + " CD_PREGUNTA in (select CD_PREGUNTA from SIVE_LINEA_ITEM where CD_MODELO = ?) and ";
              query = query + " CD_PREGUNTA = ? order by CD_PREGUNTA";
              break;
          }

          // prepara la lista de resultados
          data = new CLista();

          //#// System_out.println("SrvModPreg : " + query);

          // lanza la query
          st = con.prepareStatement(query);
          j = 1;
          st.setString(j, dModPreg.tSive);
          j++;

          // Da valor a los parametros

          switch (dModPreg.modo) {
            case MODELO:
              st.setString(j, dModPreg.codMod);
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.codMod);
              j++;
              break;
            case PREGUNTA:
              st.setString(j, dModPreg.codPreg);
              //#// System_out.println("SrvModPreg : codPreg " + dModPreg.codPreg);
              j++;
              break;
            case MOD_Y_PREG:
              st.setString(j, dModPreg.codMod);
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.codMod);
              j++;
              st.setString(j, dModPreg.codPreg);
              //#// System_out.println("SrvModPreg : codPreg " + dModPreg.codPreg);
              j++;
              break;
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          i = 1;

          while (rs.next()) {
            ////#// System_out.println("SrvModPreg: dentro de while (rs.next())");

            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            CD_MODELO = (rs.getString(1));
            DS_MODELO = (rs.getString(2));

            ////#// System_out.println("SrvModPreg: cd y ds " + CD_MODELO + " " + DS_MODELO);

            if (dModPreg.modo == MODELO) {
              data.addElement(new DataModPreg(CD_MODELO, DS_MODELO,
                                              "", "", dModPreg.tSive));
              i++;
            }
            else {
              data.addElement(new DataModPreg("", "",
                                              CD_MODELO, DS_MODELO,
                                              dModPreg.tSive));
              i++;
            }
          }
          rs.close();
          //st.close();
          st.close();
          rs = null;
          st = null;
          break;

        case servletOBTENER_X_DESCRIPCION:
          dModPreg = (DataModPreg) param.firstElement();
          caso = 0;
          //#// System_out.println("SrvModPreg: OBTENER_X_DESCRIPCIOM");

          switch (dModPreg.modo) {
            case MODELO:
              caso = MODELO;
              //#// System_out.println("SrvModPreg: MODELO");
              query = "select CD_MODELO, DS_MODELO from SIVE_MODELO "
                  + " where CD_TSIVE = ? and ";
              query = query + " DS_MODELO = ? order by DS_MODELO";
              break;
            case PREGUNTA:
              caso = PREGUNTA;
              //#// System_out.println("SrvModPreg: PREGUNTA");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and ";
              query = query + " DS_PREGUNTA = ? order by DS_PREGUNTA";
              break;
            case MOD_Y_PREG:
              caso = MOD_Y_PREG;
              //#// System_out.println("SrvModPreg: MOD_Y_PREG ");
              query = "select CD_PREGUNTA, DS_PREGUNTA from SIVE_PREGUNTA "
                  + " where CD_TSIVE = ? and "
                  + " CD_PREGUNTA in (select CD_PREGUNTA from SIVE_LINEA_ITEM where CD_MODELO = ?) and ";
              query = query + " DS_PREGUNTA = ? order by DS_PREGUNTA";
              break;
          }

          // prepara la lista de resultados
          data = new CLista();

          //#// System_out.println("SrvModPreg : " + query);

          // lanza la query
          st = con.prepareStatement(query);
          j = 1;
          st.setString(j, dModPreg.tSive);
          //#// System_out.println("SrvModPreg : tSive " + dModPreg.tSive + " " + j);
          j++;

          // Da valor a los parametros

          switch (dModPreg.modo) {
            case MODELO:
              st.setString(j, dModPreg.desMod);
              //#// System_out.println("SrvModPreg : desMod " + dModPreg.desMod + " " + j);
              j++;
              break;
            case PREGUNTA:
              st.setString(j, dModPreg.desPreg);
              //#// System_out.println("SrvModPreg : desPreg " + dModPreg.desPreg + " " + j);
              j++;
              break;
            case MOD_Y_PREG:
              st.setString(j, dModPreg.codMod);
              //#// System_out.println("SrvModPreg : codMod " + dModPreg.codMod + " " + j);
              j++;
              st.setString(j, dModPreg.desPreg);
              //#// System_out.println("SrvModPreg : desPreg " + dModPreg.desPreg + " " + j);
              j++;
              break;
          }

          rs = st.executeQuery();

          // extrae la p�gina requerida
          i = 1;

          while (rs.next()) {
            ////#// System_out.println("SrvModPreg : entro en while");

            // control de tama�o
            if (i > DBServlet.maxSIZE) {
              data.setState(CLista.listaINCOMPLETA);
              break;
            }
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }

            CD_MODELO = (rs.getString(1));
            DS_MODELO = (rs.getString(2));

            ////#// System_out.println("SrvModPreg: CD y DS " + CD_MODELO + " " + DS_MODELO);

            if (dModPreg.modo == MODELO) {
              data.addElement(new DataModPreg(CD_MODELO, DS_MODELO,
                                              "", "", dModPreg.tSive));
              i++;
            }
            else {
              data.addElement(new DataModPreg("", "",
                                              CD_MODELO, DS_MODELO,
                                              dModPreg.tSive));
              i++;
            }
          }
          rs.close();
          //st.close();
          st.close();
          rs = null;
          st = null;
          break;

      }
      //con.commit();
    }
    catch (Exception exs) {
      //con.rollback();

      String mensa = exs.getMessage();
      //if (mensa.indexOf("ORA-00000")==-1){
      exs.printStackTrace();
      throw exs;
      //}
    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      //#// System_out.println("SrvModPreg: data no es null " + data.size());
      data.trimToSize();
    }

    return data;
  }
}
