
package volCasosInd;

import java.io.Serializable;
import java.util.Vector;

public class DataVolCasInd
    implements Serializable {

  public String anoD = new String();
  public String semD = new String();
  public String anoH = new String();
  public String semH = new String();
  public String enfer = new String();
  public String area = new String();
  public String dist = new String();
  public String zbs = new String();
  public String prov = new String();
  public String mun = new String();
  public String sep = "$";
  public String codsPreg = new String();
  // ARG: Los codigos de enfermedad tambien se meten en un vector (22/5/02)
  public Vector vCodsPreg = new Vector();
  public String tSive = new String();

  public DataVolCasInd() {
  }

  public DataVolCasInd(String aD, String sD, String aH, String sH,
                       String e) {
    anoD = aD;
    semD = sD;
    anoH = aH;
    semH = sH;
    enfer = e;
  }

}