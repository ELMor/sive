//Title:        Volcado de casos individuales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Volcado de casos individuales
//              (incluidas preguntas del protocolo)
//              de una enfermedad a fichero ASCII.

package volCasosInd;

import java.util.ResourceBundle;

import capp.CApp;

public class volCasosInd
    extends CApp {

  //public volCasosInd() {
  //}

  ResourceBundle res;

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("volCasosInd.Res" + this.getIdioma());
    setTitulo(res.getString("msg19.Text"));
    CApp a = (CApp)this;
    PanelListaPreg listaPreg = new PanelListaPreg(a);
    PanelConsVolCasosInd consVol = new PanelConsVolCasosInd(a, listaPreg);
    VerPanel(res.getString("msg19.Text"), consVol, true);
    VerPanel(res.getString("msg20.Text"), listaPreg, true);
    VerPanel(res.getString("msg19.Text"));
  }

}
