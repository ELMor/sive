//Title:        Volcado de casos individuales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Volcado de casos individuales (incluidas preguntas del protocolo) de una enfermedad a fichero ASCII.

package volCasosInd;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CCargadorImagen;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo.DataCat;
import catalogo2.DataCat2;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
import utilidades.PanFechas;
import vCasIn.DataMun;
import zbs.DataZBS;
import zbs.DataZBS2;

public class PanelConsVolCasosInd
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  final int modoVOLCADO = 8;

  //rutas imagenes
  final String imgNAME[] = {
      "images/Magnify.gif",
      "images/vaciar.gif",
      "images/salvar.gif"};

  protected StubSrvBD stubCliente = new StubSrvBD();

  public ParVolCasosInd parCons;

  protected int modoOperacion = modoNORMAL;

  protected PanelListaPreg panelListaPreg;

  // servlet
  String sUrlServlet;

  File miFichero = null;
  FileWriter fStream = null;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETMun = "servlet/SrvMun";
  final String strSERVLETProv = "servlet/SrvCat2";
  final String strSERVLETEnf = "servlet/SrvEnfVigi";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  XYLayout xYLayout = new XYLayout();

  Label lblHasta = new Label();
  Label lblDesde = new Label();
  Label lblProvincia = new Label();
  CCampoCodigo txtCodPro = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarPro = new ButtonControl();
  TextField txtDesPro = new TextField();
  Label lblMunicipio = new Label();
  CCampoCodigo txtCodMun = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarMun = new ButtonControl();
  TextField txtDesMun = new TextField();
  Label lblArea = new Label();
  CCampoCodigo txtCodAre = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarAre = new ButtonControl();
  TextField txtDesAre = new TextField();
  Label lblDistrito = new Label();
  CCampoCodigo txtCodDis = new CCampoCodigo(); /*E*/
  TextField txtDesDis = new TextField();
  Label lblZonaBasica = new Label();
  CCampoCodigo txtCodZBS = new CCampoCodigo(); /*E*/
  ButtonControl btnCtrlBuscarDis = new ButtonControl();
  ButtonControl btnCtrlBuscarZBS = new ButtonControl();
  TextField txtDesZBS = new TextField();

  PanFechas fechasDesde;
  PanFechas fechasHasta;

  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  capp.CFileName panFichero;

  focusAdapter txtFocusAdapter = new focusAdapter(this);

  txt_keyAdapter txtKeyAdapter = new txt_keyAdapter(this);
  actionListener btnActionListener = new actionListener(this);
  Label lblEnfermedad = new Label();
  CCampoCodigo txtEnfer = new CCampoCodigo(); /*E*/
  TextField txtDesEnfer = new TextField();
  ButtonControl btnCtrlBuscarEnfer = new ButtonControl();
  Label lblSeparador = new Label();
  TextField txtSeparador = new TextField();

  protected CCargadorImagen imgs = null;

  public PanelConsVolCasosInd(CApp a, PanelListaPreg p) {
    try {
      sUrlServlet = "servlet/SrvModPreg";
      setApp(a);
      res = ResourceBundle.getBundle("volCasosInd.Res" + app.getIdioma());
      panelListaPreg = p;
      fechasDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      fechasHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      panFichero = new capp.CFileName(a);
      jbInit();
      lblArea.setText(this.app.getNivel1() + ":");
      lblDistrito.setText(this.app.getNivel2() + ":");
      this.txtCodAre.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesAre.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtCodDis.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesDis.setText(this.app.getDS_NIVEL2_DEFECTO());
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(447);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));

    // gestores de eventos
    btnCtrlBuscarEnfer.addActionListener(btnActionListener);
    btnCtrlBuscarPro.addActionListener(btnActionListener);
    btnCtrlBuscarMun.addActionListener(btnActionListener);
    btnCtrlBuscarAre.addActionListener(btnActionListener);
    btnCtrlBuscarDis.addActionListener(btnActionListener);
    btnCtrlBuscarZBS.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    txtEnfer.addKeyListener(txtKeyAdapter);
    txtCodPro.addKeyListener(txtKeyAdapter);
    txtCodMun.addKeyListener(txtKeyAdapter);
    txtCodAre.addKeyListener(txtKeyAdapter);
    txtCodDis.addKeyListener(txtKeyAdapter);
    txtCodZBS.addKeyListener(txtKeyAdapter);

    txtEnfer.addFocusListener(txtFocusAdapter);
    txtCodPro.addFocusListener(txtFocusAdapter);
    txtCodMun.addFocusListener(txtFocusAdapter);
    txtCodAre.addFocusListener(txtFocusAdapter);
    txtCodDis.addFocusListener(txtFocusAdapter);
    txtCodZBS.addFocusListener(txtFocusAdapter);

    lblSeparador.setText(res.getString("lblSeparador.Text"));
    txtSeparador.addKeyListener(txtKeyAdapter);
    txtSeparador.addFocusListener(txtFocusAdapter);
    txtSeparador.setText("$");

    btnCtrlBuscarEnfer.setActionCommand("buscarEnfer");
    txtDesEnfer.setEditable(false);
    txtDesEnfer.setEnabled(false); /*E*/
    btnCtrlBuscarPro.setActionCommand("buscarPro");
    txtDesPro.setEditable(false);
    txtDesPro.setEnabled(false); /*E*/
    btnCtrlBuscarMun.setActionCommand("buscarMun");
    txtDesMun.setEditable(false);
    txtDesMun.setEnabled(false); /*E*/
    btnCtrlBuscarAre.setActionCommand("buscarAre");
    txtDesAre.setEditable(false);
    txtDesAre.setEnabled(false); /*E*/
    btnCtrlBuscarDis.setActionCommand("buscarDis");
    txtDesDis.setEditable(false);
    txtDesDis.setEnabled(false); /*E*/
    btnCtrlBuscarZBS.setActionCommand("buscarZBS");
    txtDesZBS.setEditable(false);
    txtDesZBS.setEnabled(false); /*E*/
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    txtEnfer.setName("txtEnfer");
    txtCodPro.setName("txtCodPro");
    txtCodMun.setName("txtCodMun");
    txtCodAre.setName("txtCodAre");
    txtCodDis.setName("txtCodDis");
    txtSeparador.setName("txtSeparador");
    txtCodZBS.setName("txtCodZBS");

    lblEnfermedad.setText(res.getString("lblEnfermedad.Text"));
    lblHasta.setText(res.getString("lblHasta.Text"));
    lblProvincia.setText(res.getString("lblProvincia.Text"));
    lblMunicipio.setText(res.getString("lblMunicipio.Text"));
    lblZonaBasica.setText(res.getString("lblZonaBasica.Text"));
    lblArea.setText(res.getString("lblArea.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));

    txtEnfer.setBackground(new Color(255, 255, 150));
    xYLayout.setWidth(596);

    this.setLayout(xYLayout);

    this.add(lblDesde, new XYConstraints(26, 5, 51, -1));
    this.add(fechasDesde, new XYConstraints(40, 5, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 35, -1, -1));
    this.add(fechasHasta, new XYConstraints(40, 35, -1, -1));
    this.add(lblEnfermedad, new XYConstraints(26, 65, 75, -1));
    this.add(txtEnfer, new XYConstraints(143, 65, 77, -1));
    this.add(btnCtrlBuscarEnfer, new XYConstraints(225, 65, -1, -1));
    this.add(txtDesEnfer, new XYConstraints(254, 65, 287, -1));
    this.add(lblProvincia, new XYConstraints(26, 95, 64, -1));
    this.add(txtCodPro, new XYConstraints(143, 95, 77, -1));
    this.add(btnCtrlBuscarPro, new XYConstraints(225, 95, -1, -1));
    this.add(txtDesPro, new XYConstraints(254, 95, 287, -1));
    this.add(lblMunicipio, new XYConstraints(26, 125, 66, -1));
    this.add(txtCodMun, new XYConstraints(143, 125, 77, -1));
    this.add(btnCtrlBuscarMun, new XYConstraints(225, 125, -1, -1));
    this.add(txtDesMun, new XYConstraints(254, 125, 287, -1));
    this.add(lblArea, new XYConstraints(26, 155, -1, -1));
    this.add(txtCodAre, new XYConstraints(143, 155, 77, -1));
    this.add(btnCtrlBuscarAre, new XYConstraints(225, 155, -1, -1));
    this.add(txtDesAre, new XYConstraints(254, 155, 287, -1));
    this.add(lblDistrito, new XYConstraints(26, 185, 52, -1));
    this.add(txtCodDis, new XYConstraints(143, 185, 77, -1));
    this.add(btnCtrlBuscarDis, new XYConstraints(225, 185, -1, -1));
    this.add(txtDesDis, new XYConstraints(254, 185, 287, -1));
    this.add(lblZonaBasica, new XYConstraints(26, 215, 77, -1));
    this.add(txtCodZBS, new XYConstraints(143, 215, 77, -1));
    this.add(btnCtrlBuscarZBS, new XYConstraints(225, 215, -1, -1));
    this.add(txtDesZBS, new XYConstraints(254, 215, 287, -1));
    this.add(panFichero, new XYConstraints(70, 245, -1, -1));
    this.add(lblSeparador, new XYConstraints(26, 295, 129, -1));
    this.add(txtSeparador, new XYConstraints(165, 295, -1, -1));
    this.add(btnLimpiar, new XYConstraints(325, 325, -1, -1));
    this.add(btnInforme, new XYConstraints(425, 325, -1, -1));

    imgs = new CCargadorImagen(app, imgNAME);
    lblDistrito.setText(res.getString("lblDistrito.Text"));
    txtSeparador.setBackground(new Color(255, 255, 150));
    imgs.CargaImagenes();

    btnCtrlBuscarEnfer.setImage(imgs.getImage(0));
    btnCtrlBuscarPro.setImage(imgs.getImage(0));
    btnCtrlBuscarMun.setImage(imgs.getImage(0));
    btnCtrlBuscarMun.addActionListener(new
        PanelConsVolCasosInd_btnCtrlBuscarMun_actionAdapter(this));
    btnCtrlBuscarAre.setImage(imgs.getImage(0));
    btnCtrlBuscarDis.setImage(imgs.getImage(0));
    btnCtrlBuscarZBS.setImage(imgs.getImage(0));
    btnLimpiar.setImage(imgs.getImage(1));
    btnInforme.setImage(imgs.getImage(2));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    // Comprobacion de las fechas
    if ( (fechasDesde.txtAno.getText().length() == 0) ||
        (fechasHasta.txtAno.getText().length() == 0) ||
        (fechasDesde.txtCodSem.getText().length() == 0) ||
        (fechasHasta.txtCodSem.getText().length() == 0)) {
      bDatosCompletos = false;
    }

    // Comprobacion de la descripcion de la enfermedad
    if (txtDesEnfer.getText().length() == 0) {
      bDatosCompletos = false;

      // Comprobacion del separador
    }
    if (txtSeparador.getText().length() == 0) {
      bDatosCompletos = false;

      // Comprobacion del fichero
    }
    if (panFichero.txtFile.getText().length() == 0) {
      bDatosCompletos = false;

    }
    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtEnfer.setEnabled(true);
        txtCodPro.setEnabled(true);
        txtCodAre.setEnabled(true);
        //txtDesEnfer.setEnabled(true); /*E*/
        //txtDesPro.setEnabled(true); /*E*/
        //txtDesMun.setEnabled(true);
        //txtDesAre.setEnabled(true); /*E*/
        //txtDesDis.setEnabled(true);
        //txtDesZBS.setEnabled(true);

        btnCtrlBuscarEnfer.setEnabled(true);

        btnCtrlBuscarPro.setEnabled(true);
        // control municipio
        if (!txtDesPro.getText().equals("")) {
          btnCtrlBuscarMun.setEnabled(true);
          txtCodMun.setEnabled(true);
          //txtDesMun.setEnabled(true); /*E*/
        }
        else {
          btnCtrlBuscarMun.setEnabled(false);
          txtCodMun.setEnabled(false);
          //txtDesMun.setEnabled(false); /*E*/
        }

        btnCtrlBuscarAre.setEnabled(true);
        // control area
        if (!txtDesAre.getText().equals("")) {
          btnCtrlBuscarDis.setEnabled(true);
          txtCodDis.setEnabled(true);
          //txtDesDis.setEnabled(true); /*E*/
          //txtDesZBS.setEnabled(true); /*E*/
        }
        else {
          btnCtrlBuscarDis.setEnabled(false);
          txtCodDis.setEnabled(false);
          //txtDesDis.setEnabled(false); /*E*/
          //txtDesZBS.setEnabled(false); /*E*/
        }

        // control distrito
        if (!txtDesDis.getText().equals("")) {
          btnCtrlBuscarZBS.setEnabled(true);
          txtCodZBS.setEnabled(true);
          //txtDesZBS.setEnabled(true); /*E*/
        }
        else {
          btnCtrlBuscarZBS.setEnabled(false);
          txtCodZBS.setEnabled(false);
          //txtDesZBS.setEnabled(false); /*E*/
        }

        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtEnfer.setEnabled(false);
        txtCodPro.setEnabled(false);
        txtCodMun.setEnabled(false);
        txtCodAre.setEnabled(false);
        txtCodDis.setEnabled(false);
        txtCodZBS.setEnabled(false);
        btnCtrlBuscarEnfer.setEnabled(false);
        btnCtrlBuscarPro.setEnabled(false);
        btnCtrlBuscarMun.setEnabled(false);
        btnCtrlBuscarAre.setEnabled(false);
        btnCtrlBuscarDis.setEnabled(false);
        btnCtrlBuscarZBS.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //busca la enfermedad
  void btnCtrlBuscarEnfer_actionPerformed(ActionEvent evt) {
    DataCat datosPantalla = null;
    CMessage msgBox = null;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      CListaEnfEDO lista = new CListaEnfEDO(app,
                                            res.getString("msg9.Text"),
                                            stubCliente,
                                            strSERVLETEnf,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      datosPantalla = (DataCat) lista.getComponente();

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (datosPantalla != null) {
      txtEnfer.removeKeyListener(txtKeyAdapter);
      txtEnfer.setText(datosPantalla.getCod());
      txtDesEnfer.setText(datosPantalla.getDes());
      txtEnfer.addKeyListener(txtKeyAdapter);
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca la provincia
  void btnCtrlbuscarPro_actionPerformed(ActionEvent evt) {
    DataCat2 data = null;
    DataCat2 datProv = null;
    CMessage msgBox = null;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      catalogo2.CListaCat2 lista = new catalogo2.CListaCat2(app,
          res.getString("msg10.Text"),
          stubCliente,
          strSERVLETProv,
          servletOBTENER_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA,
          servletOBTENER_X_DESCRIPCION + catalogo2.Catalogo2.catPROVINCIA,
          servletSELECCION_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA,
          servletSELECCION_X_DESCRIPCION + catalogo2.Catalogo2.catPROVINCIA);

      lista.show();
      datProv = (DataCat2) lista.getComponente();
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (datProv != null) {
      txtCodPro.removeKeyListener(txtKeyAdapter);
      txtCodPro.setText(datProv.getCod());
      txtDesPro.setText(datProv.getDes());
      txtCodPro.addKeyListener(txtKeyAdapter);
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca el municipio
  void btnCtrlbuscarMun_actionPerformed(ActionEvent evt) {
    DataMun data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaMun lista = new CListaMun(this,
                                      res.getString("msg11.Text"),
                                      stubCliente,
                                      strSERVLETMun,
                                      servletOBTENER_X_CODIGO,
                                      servletOBTENER_X_DESCRIPCION,
                                      servletSELECCION_X_CODIGO,
                                      servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataMun) lista.getComponente();

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodMun.removeKeyListener(txtKeyAdapter);
      txtCodMun.setText(data.getMunicipio());
      txtDesMun.setText(data.getDescMun());
      txtCodMun.addKeyListener(txtKeyAdapter);
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //Busca los niveles1 (area de salud)
  void btnCtrlbuscarAre_actionPerformed(ActionEvent evt) {
    DataNivel1 data = null;
    CMessage mensaje = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaNivel1 lista = new CListaNivel1(app,
                                            res.getString("msg12.Text") +
                                            this.app.getNivel1(),
                                            stubCliente,
                                            strSERVLETNivel1,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }

    if (data != null) {
      txtCodAre.removeKeyListener(txtKeyAdapter);
      txtCodAre.setText(data.getCod());
      txtDesAre.setText(data.getDes());
      txtCodAre.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca el distrito (Niv2)
  void btnCtrlbuscarDis_actionPerformed(ActionEvent evt) {

    DataZBS2 data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg12.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLETNivel2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodDis.removeKeyListener(txtKeyAdapter);
      txtCodDis.setText(data.getNiv2());
      txtDesDis.setText(data.getDes());
      txtCodDis.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnCtrlbuscarZBS_actionPerformed(ActionEvent evt) {
    DataZBS data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZona lista = new CListaZona(this,
                                        res.getString("msg13.Text"),
                                        stubCliente,
                                        strSERVLETZona,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS) lista.getComponente();

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    if (data != null) {
      txtCodZBS.removeKeyListener(txtKeyAdapter);
      txtCodZBS.setText(data.getCod());
      txtDesZBS.setText(data.getDes());
      txtCodZBS.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    txtEnfer.setText("");
    txtDesEnfer.setText("");
    txtCodAre.setText("");
    txtDesAre.setText("");
    txtCodDis.setText("");
    txtDesDis.setText("");
    txtCodZBS.setText("");
    txtDesZBS.setText("");
    txtCodPro.setText("");
    txtDesPro.setText("");
    txtCodMun.setText("");
    txtDesMun.setText("");
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {

    CMessage msgBox;
    DataVolCasInd par = new DataVolCasInd();

    if (isDataValid()) {
      if (!txtDesEnfer.getText().equals("")) {
        par.enfer = txtEnfer.getText();
      }
      if (!txtDesPro.getText().equals("")) {
        par.prov = txtCodPro.getText();
      }
      if (!txtDesMun.getText().equals("")) {
        par.mun = txtCodMun.getText();
      }
      if (!txtDesAre.getText().equals("")) {
        par.area = txtCodAre.getText();
      }
      if (!txtDesDis.getText().equals("")) {
        par.dist = txtCodDis.getText();
      }
      if (!txtDesZBS.getText().equals("")) {
        par.zbs = txtCodZBS.getText();

      }
      par.anoD = fechasDesde.txtAno.getText();
      par.anoH = fechasHasta.txtAno.getText();
      if (fechasDesde.txtCodSem.getText().length() == 1) {
        par.semD = "0" + fechasDesde.txtCodSem.getText();
      }
      else {
        par.semD = fechasDesde.txtCodSem.getText();
      }
      if (fechasHasta.txtCodSem.getText().length() == 1) {
        par.semH = "0" + fechasHasta.txtCodSem.getText();
      }
      else {
        par.semH = fechasHasta.txtCodSem.getText();

        // Compruebo si se calculan las tasas
        // tasas si area o todo vacio
        // de momento lo pongo a false
      }
      par.sep = txtSeparador.getText();
      par.tSive = getApp().getTSive();
      par.codsPreg = panelListaPreg.obtenerCodPreg();
      // ARG: Tambien se obtienen los codigos de las preguntas en un vector
      par.vCodsPreg = panelListaPreg.obtenerVectorCodPreg();

      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();
      try {
        CLista data = new CLista();

        // idioma
        data.setIdioma(app.getIdioma());

        // estado
        data.setState(CLista.listaINCOMPLETA);

        // ARG: Se introduce el login
        data.setLogin(app.getLogin());

        // ARG: Se introduce la lortad
        data.setLortad(app.getParameter("LORTAD"));

        data.addElement(par);

        stubCliente.setUrl(new URL(app.getURL() + sUrlServlet));

//        data = (CLista) stubCliente.doPost(this .modoVOLCADO, data);

        SrvModPreg servlet = new SrvModPreg();
        servlet.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                                   "jdbc:oracle:thin:@10.160.12.54:1521:ORCL",
                                   "pista",
                                   "loteb98");
        data = (CLista) servlet.doDebug(this.modoVOLCADO, data);

        // Guardar en fichero
        String sLinea = new String();
        if (data.size() > 0) {
          try {
            miFichero = new File(this.panFichero.txtFile.getText());
            fStream = new FileWriter(miFichero);

            // Se escribe la cabecera, que esta al final del data
            sLinea = (String) data.elementAt(data.size() - 1);
            fStream.write(sLinea);
            fStream.write("\r");
            fStream.write("\n");

            for (int j = 0; j < data.size() - 1; j++) {
              sLinea = (String) data.elementAt(j);
              fStream.write(sLinea);
              fStream.write("\r");
              fStream.write("\n");
            }
            fStream.close();
            fStream = null;
            miFichero = null;
            msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                  res.getString("msg14.Text"));
            msgBox.show();
            msgBox = null;
          }
          catch (IOException ioEx) {
            ioEx.printStackTrace();
            msgBox = new CMessage(this.app, CMessage.msgERROR,
                                  res.getString("msg15.Text"));
            msgBox.show();
            msgBox = null;
          }
        } //if
        else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO,
                                res.getString("msg16.Text"));
          msgBox.show();
          msgBox = null;
        }

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg17.Text"));
      msgBox.show();
      msgBox = null;
    }

  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    // datos de envio
    DataNivel1 nivel1;
    DataZBS2 nivel2;
    DataZBS zbs;
    DataMun mun;
    DataCat2 prov;
    DataCat enf;

    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    //#// System_out.println(" Foco perdido: " +  txt.getName());

    // gestion de datos
    if ( (txt.getName().equals("txtEnfer")) && (txtEnfer.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataCat(txtEnfer.getText()));
      strServlet = strSERVLETEnf;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodAre")) &&
             (txtCodAre.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataNivel1(txtCodAre.getText()));
      strServlet = strSERVLETNivel1;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodDis")) &&
             (txtCodDis.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS2(txtCodAre.getText(), txtCodDis.getText(),
                                    "", ""));
      strServlet = strSERVLETNivel2;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodZBS")) &&
             (txtCodZBS.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS(txtCodZBS.getText(), "", "",
                                   txtCodAre.getText(), txtCodDis.getText()));
      strServlet = strSERVLETZona;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodPro")) &&
             (txtCodPro.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataCat2(txtCodPro.getText()));
      strServlet = strSERVLETProv;
      modoServlet = servletOBTENER_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA;

    }
    else if ( (txt.getName().equals("txtCodMun")) &&
             (txtCodMun.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataMun(txtCodPro.getText(), txtCodMun.getText(), "",
                                   "", "", ""));
      strServlet = strSERVLETMun;
      modoServlet = servletOBTENER_X_CODIGO;

    }

    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtEnfer")) {
            enf = (DataCat) param.firstElement();
            txtEnfer.removeKeyListener(txtKeyAdapter);
            txtEnfer.setText(enf.getCod());
            txtDesEnfer.setText(enf.getDes());
            txtEnfer.addKeyListener(txtKeyAdapter);

          }
          else if (txt.getName().equals("txtCodAre")) {
            nivel1 = (DataNivel1) param.firstElement();
            txtCodAre.removeKeyListener(txtKeyAdapter);
            txtCodAre.setText(nivel1.getCod());
            txtDesAre.setText(nivel1.getDes());
            txtCodAre.addKeyListener(txtKeyAdapter);

          }
          else if (txt.getName().equals("txtCodDis")) {
            nivel2 = (DataZBS2) param.firstElement();
            txtCodDis.removeKeyListener(txtKeyAdapter);
            txtCodDis.setText(nivel2.getNiv2());
            txtDesDis.setText(nivel2.getDes());
            txtCodDis.addKeyListener(txtKeyAdapter);

          }
          else if (txt.getName().equals("txtCodZBS")) {
            zbs = (DataZBS) param.firstElement();
            txtCodZBS.removeKeyListener(txtKeyAdapter);
            txtCodZBS.setText(zbs.getCod());
            txtDesZBS.setText(zbs.getDes());
            txtCodZBS.addKeyListener(txtKeyAdapter);

          }
          else if (txt.getName().equals("txtCodPro")) {
            prov = (DataCat2) param.firstElement();
            txtCodPro.removeKeyListener(txtKeyAdapter);
            txtCodPro.setText(prov.getCod());
            txtDesPro.setText(prov.getDes());
            txtCodPro.addKeyListener(txtKeyAdapter);

          }
          else if (txt.getName().equals("txtCodMun")) {
            mun = (DataMun) param.firstElement();
            txtCodMun.removeKeyListener(txtKeyAdapter);
            txtCodMun.setText(mun.getMunicipio());
            txtDesMun.setText(mun.getDescMun());
            txtCodMun.addKeyListener(txtKeyAdapter);

          }
        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg18.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtEnfer")) &&
        (txtDesEnfer.getText().length() > 0)) {
      txtEnfer.setText("");
      txtDesEnfer.setText("");
    }
    else if ( (txt.getName().equals("txtCodAre")) &&
             (txtDesAre.getText().length() > 0)) {
      txtCodAre.setText("");
      txtCodDis.setText("");
      txtDesAre.setText("");
    }
    else if ( (txt.getName().equals("txtCodDis")) &&
             (txtDesDis.getText().length() > 0)) {
      txtCodDis.setText("");
      txtCodZBS.setText("");
      txtDesDis.setText("");
    }
    else if ( (txt.getName().equals("txtCodZBS")) &&
             (txtDesZBS.getText().length() > 0)) {
      txtCodZBS.setText("");
      txtDesZBS.setText("");
    }
    else if ( (txt.getName().equals("txtCodPro")) &&
             (txtDesPro.getText().length() > 0)) {
      txtCodPro.setText("");
      txtCodMun.setText("");
      txtDesPro.setText("");
    }
    else if ( (txt.getName().equals("txtCodMun")) &&
             (txtDesMun.getText().length() > 0)) {
      txtCodMun.setText("");
      txtDesMun.setText("");
    }

    Inicializar();
  }

  void btnCtrlBuscarMun_actionPerformed(ActionEvent e) {

  }
} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  PanelConsVolCasosInd adaptee = null;
  ActionEvent e = null;

  public actionListener(PanelConsVolCasosInd adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarEnfer")) {
      adaptee.btnCtrlBuscarEnfer_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarPro")) {
      adaptee.btnCtrlbuscarPro_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarMun")) {
      adaptee.btnCtrlbuscarMun_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarAre")) {
      adaptee.btnCtrlbuscarAre_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarDis")) {
      adaptee.btnCtrlbuscarDis_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarZBS")) {
      adaptee.btnCtrlbuscarZBS_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanelConsVolCasosInd adaptee;
  FocusEvent event;

  focusAdapter(PanelConsVolCasosInd adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

// cambio en una caja de codigos
class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  PanelConsVolCasosInd adaptee;

  txt_keyAdapter(PanelConsVolCasosInd adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}

////////////////////// Clases para listas

class CListaEnfEDO
    extends CListaValores {

  public CListaEnfEDO(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataCat(s);

  }

  public String getCodigo(Object o) {
    return ( ( (DataCat) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCat) o).getDes());
  }
}

// lista de valores
class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

// lista de valores
class CListaZBS2
    extends CListaValores {

  protected PanelConsVolCasosInd panel;

  public CListaZBS2(PanelConsVolCasosInd p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtCodAre.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

// lista de valores
class CListaZona
    extends CListaValores {

  protected PanelConsVolCasosInd panel;

  public CListaZona(PanelConsVolCasosInd p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS(s, "", "", panel.txtCodAre.getText(),
                       panel.txtCodDis.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS) o).getDes());
  }
}

class CListaMun
    extends CListaValores {

  protected PanelConsVolCasosInd panel;

  public CListaMun(PanelConsVolCasosInd p,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataMun(panel.txtCodPro.getText(), s, "", "", "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataMun) o).getMunicipio());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataMun) o).getDescMun());
  }
}

class PanelConsVolCasosInd_btnCtrlBuscarMun_actionAdapter
    implements java.awt.event.ActionListener {
  PanelConsVolCasosInd adaptee;

  PanelConsVolCasosInd_btnCtrlBuscarMun_actionAdapter(PanelConsVolCasosInd
      adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnCtrlBuscarMun_actionPerformed(e);
  }
}
