//Title:        Volcado de casos individuales
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      Norsistemas
//Description:  Volcado de casos individuales (incluidas preguntas del protocolo) de una enfermedad a fichero ASCII.

package volCasosInd;

import java.io.Serializable;

public class DataModPreg
    implements Serializable {
// Clase para recuperar el codigo y la descripcion
// de modelos y preguntas

  public String codMod;
  public String codPreg;
  public String desMod;
  public String desPreg;

  public String tSive;

  public int modo;

  public DataModPreg() {
  }

  public DataModPreg(String cMod, String dMod,
                     String cPreg, String dPreg,
                     String tS) {
    codMod = cMod;
    desMod = dMod;
    codPreg = cPreg;
    desPreg = dPreg;

    tSive = tS;
  }

}