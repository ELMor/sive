//Title:        Notificacion de alarmas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Durante la notificacion de los casos que se han dado de una enfermedad,
//muestra los indicadores para los que se ha superado el umbral.

package notalarmas;

import java.net.URL;
import java.util.Vector;

import capp.CApp;
import capp.CLista;
import capp.CMessage;
import sapp.StubSrvBD;

public class miraIndicadores {

  // modos de operación
  final int servletIND_SUP = 1;

  String sUrlServlet = "servlet/SrvIndSupNot";
  CApp app = null;

  protected StubSrvBD stubCliente = new StubSrvBD();

  diaSuperados dialogo = null;

  public miraIndicadores(CApp a) {
    app = a;
    dialogo = new diaSuperados(a);
  }

  boolean datosValidos(parNotifAlar par) {
    boolean datosCompletos = true;
    if (par.enfermedad().equals("")) {
      return false;
    }
    if (par.ano().equals("")) {
      return false;
    }
    if (par.semana().equals("")) {
      return false;
    }
    //AIC
    if (par.cAutonoma().equals("")) {
      return false;
    }
    return datosCompletos;

  }

  public void compruebaIndSuperados(parNotifAlar par) {
    CMessage msgBox;
    if (datosValidos(par)) {
      try {
        CLista data = new CLista();

        // idioma y perfil
        data.setIdioma(app.getIdioma());
        data.setPerfil(app.getPerfil());
        // estado
        data.setState(CLista.listaINCOMPLETA);
        // ARG: Se introduce el login
        data.setLogin(app.getLogin());
        // ARG: Se introduce la lortad
        data.setLortad(app.getParameter("LORTAD"));

        data.addElement(par);
        stubCliente.setUrl(new URL(app.getURL() + sUrlServlet));
        data = (CLista) stubCliente.doPost(servletIND_SUP, data);
        /*
         SrvIndSupNot srv = new SrvIndSupNot();
         data = (CLista)srv.doPrueba(servletIND_SUP, data);
         srv = null;
         */

        //Mostrar el dialogo con la tabla de indicadores superados
        if (data.size() > 0) {
          Vector inds = new Vector();
          inds = (Vector) data.elementAt(0);
          Vector cabecera = (Vector) data.elementAt(1);
          //cabecera
          if (cabecera.size() > 0) {
            par.enfermedad( (String) cabecera.elementAt(0));
            if (!par.nivel1().equals("")) {
              par.nivel1(par.nivel1() + " - " + (String) cabecera.elementAt(1));
            }
            else {
              par.nivel1("");
            }
            if (!par.nivel1().equals("")) {
              par.nivel2(par.nivel2() + " - " + (String) cabecera.elementAt(2));
            }
            else {
              par.nivel1("");
            }
            dialogo.ponerCabecera(par);
          }
          //AIC
          //msgBox = new CMessage(app, CMessage.msgERROR, "Valores devueltos" + String.valueOf(inds.size()));
          //msgBox.show();
          //msgBox = null;
          if (inds.size() > 0) {
            dialogo.rellenarTabla(inds);
            dialogo.show();
          }
        } //if
        /*else {
          msgBox = new CMessage(this.app, CMessage.msgAVISO, "No hay indicadores superados");
          msgBox.show();
          msgBox = null;
                 }*/
      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
      }

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            "Faltan campos obligatorios.");
      msgBox.show();
      msgBox = null;
    }
  }

  /*
     public static void main(String[] args) {
    miraIndicadores miraIndicadores = new miraIndicadores();
    miraIndicadores.invokedStandalone = true;
     }
     private boolean invokedStandalone = false;
   */
}