//Title:        Notificacion de alarmas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Durante la notificacion de los casos que se han dado de una enfermedad,
//muestra los indicadores para los que se ha superado el umbral.

package notalarmas;

import capp.CApp;

public class appPrueba
    extends CApp {

  public appPrueba() {
  }

  public void init() {
    super.init();
  }

  public void start() {

    setTitulo("Prueba para mostrar indicadores superados en una notificacion");
    CApp a = (CApp)this;

    miraIndicadores miraInd = new miraIndicadores(a);
    VerPanel("Prueba para mostrar indicadores superados en una notificacion");
    parNotifAlar par = new parNotifAlar("0-0-0", "1999", "10", "1", "0", "13");
    miraInd.compruebaIndSuperados(par);

  }
}