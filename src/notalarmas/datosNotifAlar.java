//Title:        Notificacion de alarmas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Durante la notificacion de los casos que se han dado de una enfermedad,
//muestra los indicadores para los que se ha superado el umbral.

package notalarmas;

import java.io.Serializable;

public class datosNotifAlar
    implements Serializable {

  String cd_ind = "";
  String ds_ind = "";
  double coef = 0;
  double valor = 0;
  double umbral = 0;

  public datosNotifAlar(String cd, String ds,
                        double c, double v, double u) {
    cd_ind = cd;
    ds_ind = ds;
    coef = c;
    valor = v;
    umbral = u;
  }

  public datosNotifAlar() {
  }

  public String cd_ind() {
    return cd_ind;
  }

  public String ds_ind() {
    return ds_ind;
  }

  public double coef() {
    return coef;
  }

  public double valor() {
    return valor;
  }

  public double umbral() {
    return umbral;
  }

  public void cd_ind(String cd) {
    cd_ind = cd;
  }

  public void ds_ind(String ds) {
    ds_ind = ds;
  }

  public void coef(double c) {
    coef = c;
  }

  public void valor(double v) {
    valor = v;
  }

  public void umbral(double u) {
    umbral = u;
  }

}
