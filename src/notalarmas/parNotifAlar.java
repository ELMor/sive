//Title:        Notificacion de alarmas
//Version:
//Copyright:    Copyright (c) 1998
//Author:       Cristina Gayan Asensio
//Company:      NorSistemas
//Description:  Durante la notificacion de los casos que se han dado de una enfermedad,
//muestra los indicadores para los que se ha superado el umbral.

package notalarmas;

import java.io.Serializable;

public class parNotifAlar
    implements Serializable {

  String enfermedad = "";
  String ano = "";
  String semana = "";
  String nivel1 = "";
  String nivel2 = "";
  String cAutonoma = "";

  public parNotifAlar(String enf, String a, String s,
                      String n1, String n2, String ca) {
    //AIC
    //public parNotifAlar ( String enf, String a, String s,
    //                           String n1, String n2 ) {
    enfermedad = enf;
    ano = a;
    semana = s;
    nivel1 = n1;
    nivel2 = n2;
    cAutonoma = ca;
  }

  public parNotifAlar() {
  }

  public String enfermedad() {
    return enfermedad;
  }

  public String ano() {
    return ano;
  }

  public String semana() {
    return semana;
  }

  public String nivel1() {
    return nivel1;
  }

  public String nivel2() {
    return nivel2;
  }

  //AIC
  public String cAutonoma() {
    return cAutonoma;
  }

  //AIC
  public void cAutonoma(String ca) {
    cAutonoma = ca;
  }

  public void enfermedad(String enf) {
    enfermedad = enf;
  }

  public void ano(String a) {
    ano = a;
  }

  public void semana(String s) {
    semana = s;
  }

  public void nivel1(String n1) {
    nivel1 = n1;
  }

  public void nivel2(String n2) {
    nivel2 = n2;
  }

}