package vCasIn;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import alarmas.DataEnferedo;
import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.GroupBox;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo2.CListaCat2;
import catalogo2.DataCat2;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
import zbs.DataZBS;
import zbs.DataZBS2;

public class Pan_vCasIn
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoALTA = 0;
  ResourceBundle res;
  final int modoMODIFICAR = 1;
  final int modoESPERA = 2;

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  protected int modoOperacion = modoALTA;

  // constantes del panel
  final String imgLUPA = "images/Magnify.gif";
  final String imgALTA = "images/alta.gif";

  // variables del panel
  protected String enfermedadActual = "";
  protected String provinciaActual = "";
  protected String municipioActual = "";
  protected String nivel1Actual = "";
  protected String nivel2Actual = "";
  protected String zonaActual = "";

  // par�metros
  protected CLista listaResultadoEnfCie = null;
  protected CLista listaResultadoNivel1 = null;
  protected CLista listaResultadoNivel2 = null;
  protected CLista listaResultadoZona = null;
  protected CLista listaResultadoMunicipio = null;

  // stub's
  final String strSERVLETEnfCie = "servlet/SrvEnferedo";
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETMun = "servlet/SrvMun";
  final String strSERVLETProv = "servlet/SrvCat2";

  protected StubSrvBD stubEnfCie = null;
  protected StubSrvBD stubProvincia = null;
  protected StubSrvBD stubMunicipio = null;
  protected StubSrvBD stubNivel1 = null;
  protected StubSrvBD stubNivel2 = null;
  protected StubSrvBD stubZona = null;

  Label lblDesde = new Label();
  XYLayout xYLayout1 = new XYLayout();
  Label lblAnDesde = new Label();
  TextField txtAnDesde = new TextField();
  TextField txtSeDesde = new TextField();
  Label lblSeDesde = new Label();
  TextField txtSeHasta = new TextField();
  Label lblHasta = new Label();
  Label lblAnHasta = new Label();
  TextField txtAnHasta = new TextField();
  Label lblSeHasta = new Label();
  Label lblEnfermedad = new Label();
  TextField txtEnfCie = new TextField();
  ButtonControl btnEnfCie = new ButtonControl();
  TextField txtNiv1 = new TextField();
  ButtonControl btnNiv1 = new ButtonControl();
  Label lblNiv1 = new Label();
  Label lblNiv2 = new Label();
  ButtonControl btnNiv2 = new ButtonControl();
  Label lblZona = new Label();
  ButtonControl btnZona = new ButtonControl();
  Label lblMunicipio = new Label();
  ButtonControl btnMunicipio = new ButtonControl();
  TextField txtDistrito = new TextField();
  TextField txtZona = new TextField();
  TextField txtmunicipio = new TextField();
  ButtonControl btnAceptar = new ButtonControl();
  Label lblProvincia = new Label();
  TextField txtProvincia = new TextField();
  ButtonControl btnProvincia = new ButtonControl();
  GroupBox groupBox1 = new GroupBox();

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoALTA:
        txtAnDesde.setEnabled(true);
        txtAnHasta.setEnabled(true);
        txtDistrito.setEnabled(true);
        txtEnfCie.setEnabled(true);
        txtProvincia.setEnabled(true);
        txtmunicipio.setEnabled(true);
        txtNiv1.setEnabled(true);
        txtSeDesde.setEnabled(true);
        txtSeHasta.setEnabled(true);
        txtZona.setEnabled(true);

        btnAceptar.setEnabled(true);
        btnEnfCie.setEnabled(true);
        btnProvincia.setEnabled(true);

        if (provinciaActual.compareTo("") != 0) {
          btnMunicipio.setEnabled(true);
        }
        else {
          btnMunicipio.setEnabled(false);

        }
        btnNiv1.setEnabled(true);
        if (nivel1Actual.compareTo("") != 0) {
          btnNiv2.setEnabled(true);
        }
        else {
          btnNiv2.setEnabled(false);

        }
        if (nivel2Actual.compareTo("") != 0) {
          btnZona.setEnabled(true);
        }
        else {
          btnZona.setEnabled(false);

        }
        btnProvincia.setEnabled(true);

        btnEnfCie.setSelected(false);
        btnMunicipio.setSelected(false);
        btnNiv1.setSelected(false);
        btnNiv2.setSelected(false);
        btnZona.setSelected(false);
        btnProvincia.setSelected(false);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtAnDesde.setEnabled(false);
        txtAnHasta.setEnabled(false);
        txtDistrito.setEnabled(false);
        txtEnfCie.setEnabled(false);
        txtmunicipio.setEnabled(false);
        txtNiv1.setEnabled(false);
        txtSeDesde.setEnabled(false);
        txtSeHasta.setEnabled(false);
        txtZona.setEnabled(false);

        btnAceptar.setEnabled(false);
        btnEnfCie.setEnabled(false);
        btnMunicipio.setEnabled(false);
        btnNiv1.setEnabled(false);
        btnNiv2.setEnabled(false);
        btnZona.setEnabled(false);
        btnProvincia.setEnabled(false);

        btnEnfCie.setSelected(false);
        btnMunicipio.setSelected(false);
        btnNiv1.setSelected(false);
        btnNiv2.setSelected(false);
        btnZona.setSelected(false);
        btnProvincia.setSelected(false);

        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;
    }
    this.doLayout();
  }

  // contructor
  public Pan_vCasIn(CApp a) {
    try {
      setApp(a);
      res = ResourceBundle.getBundle("vCasIn.Res" + app.getIdioma());
      listaResultadoEnfCie = new CLista();
      listaResultadoNivel1 = new CLista();
      listaResultadoNivel2 = new CLista();
      listaResultadoZona = new CLista();
      listaResultadoMunicipio = new CLista();
      jbInit();

      stubEnfCie = new StubSrvBD(new URL(this.app.getURL() + strSERVLETEnfCie));
      stubNivel1 = new StubSrvBD(new URL(this.app.getURL() + strSERVLETNivel1));
      stubNivel2 = new StubSrvBD(new URL(this.app.getURL() + strSERVLETNivel2));
      stubZona = new StubSrvBD(new URL(this.app.getURL() + strSERVLETZona));
      stubMunicipio = new StubSrvBD(new URL(this.app.getURL() + strSERVLETMun));
      stubProvincia = new StubSrvBD(new URL(this.app.getURL() + strSERVLETProv));

      listaResultadoEnfCie.setState(CLista.listaNOVALIDA);
      listaResultadoNivel1.setState(CLista.listaNOVALIDA);
      listaResultadoNivel2.setState(CLista.listaNOVALIDA);
      listaResultadoZona.setState(CLista.listaNOVALIDA);
      listaResultadoMunicipio.setState(CLista.listaNOVALIDA);

      btnEnfCie.setImageURL(new URL(app.getCodeBase(), imgLUPA));
      btnNiv1.setImageURL(new URL(app.getCodeBase(), imgLUPA));
      btnNiv2.setImageURL(new URL(app.getCodeBase(), imgLUPA));
      btnZona.setImageURL(new URL(app.getCodeBase(), imgLUPA));
      btnMunicipio.setImageURL(new URL(app.getCodeBase(), imgLUPA));
      btnProvincia.setImageURL(new URL(app.getCodeBase(), imgLUPA));
      btnAceptar.setImageURL(new URL(app.getCodeBase(), imgALTA));

      lblNiv1.setText(this.app.getNivel1() + ":");
      lblNiv2.setText(this.app.getNivel2() + ":");

      enfermedadActual = "";
      nivel1Actual = "";
      nivel2Actual = "";
      zonaActual = "";
      provinciaActual = "";
      municipioActual = "";

      modoOperacion = modoALTA;
      Inicializar();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  // aspecto de la ventana
  private void jbInit() throws Exception {
    this.setLayout(xYLayout1);
    lblDesde.setFont(new Font("Dialog", 1, 12));
    lblDesde.setText(res.getString("lblDesde.Text"));
    xYLayout1.setHeight(335);
    xYLayout1.setWidth(311);
    lblAnDesde.setText(res.getString("lblAnDesde.Text"));
    txtAnDesde.setBackground(new Color(255, 255, 150));
    txtAnDesde.setColumns(4);
    txtSeDesde.setBackground(new Color(255, 255, 150));
    lblSeDesde.setText(res.getString("lblSeDesde.Text"));
    txtSeHasta.setBackground(new Color(255, 255, 150));
    lblHasta.setFont(new Font("Dialog", 1, 12));
    lblHasta.setText(res.getString("lblHasta.Text"));
    lblAnHasta.setText(res.getString("lblAnDesde.Text"));
    txtAnHasta.setBackground(new Color(255, 255, 150));
    lblSeHasta.setText(res.getString("lblSeDesde.Text"));
    lblEnfermedad.setText(res.getString("lblEnfermedad.Text"));
    txtEnfCie.addKeyListener(new Pan_vCasIn_txtEnfCie_keyAdapter(this));
    btnEnfCie.setLabel("");
    txtNiv1.addKeyListener(new Pan_vCasIn_txtNiv1_keyAdapter(this));
    btnEnfCie.addActionListener(new Pan_vCasIn_btnEnfCie_actionAdapter(this));
    btnNiv1.setLabel("");
    btnNiv1.addActionListener(new Pan_vCasIn_btnNiv1_actionAdapter(this));
    lblNiv2.setText(res.getString("lblNiv2.Text"));
    btnNiv2.setLabel("");
    btnNiv2.addActionListener(new Pan_vCasIn_btnNiv2_actionAdapter(this));
    lblZona.setText(res.getString("lblZona.Text"));
    btnZona.setLabel("");
    btnZona.addActionListener(new Pan_vCasIn_btnZona_actionAdapter(this));
    lblMunicipio.setText(res.getString("lblMunicipio.Text"));
    btnMunicipio.setLabel("");
    txtZona.addKeyListener(new Pan_vCasIn_txtZona_keyAdapter(this));
    txtDistrito.addKeyListener(new Pan_vCasIn_txtDistrito_keyAdapter(this));
    btnMunicipio.addActionListener(new Pan_vCasIn_btnMunicipio_actionAdapter(this));
    btnAceptar.setLabel(res.getString("btnAceptar.Label"));
    btnAceptar.addActionListener(new Pan_vCasIn_btnAceptar_actionAdapter(this));
    lblProvincia.setText(res.getString("lblProvincia.Text"));
    txtProvincia.addKeyListener(new Pan_vCasIn_txtProvincia_keyAdapter(this));
    btnProvincia.setLabel("");
    btnProvincia.addActionListener(new Pan_vCasIn_btnProvincia_actionAdapter(this));
    groupBox1.setForeground(new Color(155, 0, 0));
    groupBox1.setFont(new Font("Dialog", 3, 12));
    groupBox1.setLabel(res.getString("groupBox1.Label"));
    // btnProvincia.addActionListener(new Pan_vCasIn_btnProvincia_actionAdapter(this));
    // establece el modo de operaci�n
    Inicializar();
    this.add(lblDesde, new XYConstraints(24, 29, 43, -1));
    this.add(lblAnDesde, new XYConstraints(78, 29, 31, -1));
    this.add(txtAnDesde, new XYConstraints(112, 29, -1, -1));
    this.add(txtSeDesde, new XYConstraints(233, 29, 52, -1));
    this.add(lblSeDesde, new XYConstraints(177, 29, 52, -1));
    this.add(lblHasta, new XYConstraints(24, 62, 43, -1));
    this.add(lblAnHasta, new XYConstraints(78, 62, 31, -1));
    this.add(txtAnHasta, new XYConstraints(112, 62, 52, -1));
    this.add(txtSeHasta, new XYConstraints(233, 62, 52, -1));
    this.add(lblSeHasta, new XYConstraints(177, 62, 52, -1));
    this.add(lblEnfermedad, new XYConstraints(24, 111, -1, -1));
    this.add(txtEnfCie, new XYConstraints(112, 111, 116, -1));
    this.add(btnEnfCie, new XYConstraints(233, 111, -1, -1));
    this.add(lblProvincia, new XYConstraints(24, 140, 85, -1));
    this.add(txtProvincia, new XYConstraints(112, 140, 116, -1));
    this.add(btnProvincia, new XYConstraints(233, 140, -1, -1));
    this.add(lblMunicipio, new XYConstraints(24, 169, 85, -1));
    this.add(txtmunicipio, new XYConstraints(112, 169, 116, -1));
    this.add(btnMunicipio, new XYConstraints(233, 169, -1, -1));
    this.add(lblNiv1, new XYConstraints(24, 198, 85, -1));
    this.add(txtNiv1, new XYConstraints(112, 198, 116, -1));
    this.add(btnNiv1, new XYConstraints(233, 198, -1, -1));
    this.add(lblNiv2, new XYConstraints(24, 227, 85, -1));
    this.add(txtDistrito, new XYConstraints(112, 227, 116, -1));
    this.add(btnNiv2, new XYConstraints(233, 227, -1, -1));
    this.add(lblZona, new XYConstraints(24, 256, 85, -1));
    this.add(txtZona, new XYConstraints(112, 256, 116, -1));
    this.add(btnZona, new XYConstraints(233, 256, -1, -1));
    this.add(btnAceptar, new XYConstraints(204, 290, 80, 26));
    this.add(groupBox1, new XYConstraints(5, 7, 300, 321));
  }

  // comprueba que los datos sean v�lidos
  protected boolean isDataValid() {
    CMessage msgBox;
    String msg = null;

    boolean b = false;
    msg = new String("");

    // comprueba que esten informados los campos obligatorios
    if ( (txtAnDesde.getText().length() > 0) &&
        (txtAnHasta.getText().length() > 0) &&
        (txtSeDesde.getText().length() > 0) &&
        (txtSeHasta.getText().length() > 0)) {
      b = true;

      if (txtAnDesde.getText().length() > 4) {
        b = false;
        msg = res.getString("msg1.Text");
        txtAnDesde.selectAll();
      }

      if (txtAnHasta.getText().length() > 4) {
        b = false;
        msg = res.getString("msg1.Text");
        txtAnHasta.selectAll();
      }

      if (txtAnDesde.getText().length() > 4) {
        b = false;
        msg = res.getString("msg1.Text");
        txtSeDesde.selectAll();
      }

      if (txtAnHasta.getText().length() > 4) {
        b = false;
        msg = res.getString("msg1.Text");
        txtSeHasta.selectAll();
      }
      // advertencia de requerir datos
    }
    else {
      msg = res.getString("msg2.Text");
    }

    if (!b) {
      msgBox = new CMessage(app, CMessage.msgAVISO, msg);
      msgBox.show();
      msgBox = null;
    }

    return b;
  }

  void btnEnfCie_actionPerformed(ActionEvent e) {
    DataEnferedo data;
    CMessage mensaje = null;
    int modoAnt = 0;

    modoAnt = modoOperacion;
    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaEnfCie lista = new CListaEnfCie(app, res.getString("msg3.Text"),
                                            stubEnfCie, strSERVLETEnfCie,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataEnferedo) lista.getComponente();
      if (data != null) {
        enfermedadActual = data.getCod();
        txtEnfCie.setText(data.getDes());
      }
      modoOperacion = modoAnt;
      Inicializar();
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
      modoOperacion = modoAnt;
      Inicializar();
    }
  }

  void btnNiv1_actionPerformed(ActionEvent e) {
    DataNivel1 data;
    int modo = modoOperacion;
    CMessage mensaje = null;

    CLista lr;
    CLista datos;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaNivel1 lista = new CListaNivel1(app,
                                            res.getString("msg4.Text") +
                                            this.app.getNivel1(),
                                            stubNivel1,
                                            strSERVLETNivel1,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();
      modoOperacion = modo;
      if (data != null) {
        //borraTodo();
        nivel1Actual = data.getCod();
        txtNiv1.setText(data.getDes());
        btnNiv2.setEnabled(true);
      }
      Inicializar();
    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
      modoOperacion = modo;
      Inicializar();
    }
  }

  void btnNiv2_actionPerformed(ActionEvent e) {
    DataZBS2 data;
    CMessage msgBox;

    int modo = modoOperacion;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg4.Text") +
                                        app.getNivel2(),
                                        stubNivel2,
                                        strSERVLETNivel2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
      modoOperacion = modo;
      if (data != null) {
        nivel2Actual = data.getNiv2();
        zonaActual = "";
        txtDistrito.setText(data.getDes());
        btnNiv2.setEnabled(true);
        modoOperacion = modo;
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
      modoOperacion = modo;
    }
    Inicializar();
  }

  void btnZona_actionPerformed(ActionEvent e) {
    DataZBS data;
    CMessage msgBox;

    int modo = modoOperacion;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZona lista = new CListaZona(this,
                                        res.getString("msg5.Text"),
                                        stubZona,
                                        strSERVLETZona,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS) lista.getComponente();
      modoOperacion = modo;
      if (data != null) {
        zonaActual = data.getCod();
        txtZona.setText(data.getDes());
        modoOperacion = modo;
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
      modoOperacion = modo;
    }
    Inicializar();
  }

  void btnMunicipio_actionPerformed(ActionEvent e) {
    DataMun data;
    CMessage msgBox;

    int modo = modoOperacion;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaMun lista = new CListaMun(this,
                                      res.getString("msg6.Text"),
                                      stubMunicipio,
                                      strSERVLETMun,
                                      servletOBTENER_X_CODIGO,
                                      servletOBTENER_X_DESCRIPCION,
                                      servletSELECCION_X_CODIGO,
                                      servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataMun) lista.getComponente();
      modoOperacion = modo;
      if (data != null) {
        municipioActual = data.getMunicipio();
        txtmunicipio.setText(data.getDescMun());
        btnMunicipio.setEnabled(true);
        modoOperacion = modo;
      }
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
      modoOperacion = modo;
    }
    Inicializar();
  }

  void txtNiv1_keyPressed(KeyEvent e) {
    //txtNiv1.setText("");
    txtDistrito.setText("");
    txtZona.setText("");
    nivel1Actual = "";
    nivel2Actual = "";
    zonaActual = "";
    btnNiv2.setEnabled(false);
    btnZona.setEnabled(false);
    this.doLayout();
  }

  void txtDistrito_keyPressed(KeyEvent e) {
    //txtDistrito.setText("");
    txtZona.setText("");
    nivel2Actual = "";
    zonaActual = "";
    btnZona.setEnabled(false);
    this.doLayout();
  }

  void txtZona_keyPressed(KeyEvent e) {
    //txtZona.setText("");
    zonaActual = "";
    this.doLayout();
  }

  void txtEnfCie_keyPressed(KeyEvent e) {
    //txtEnfCie.setText("");
    enfermedadActual = "";
  }

  void txtProvincia_keyPressed(KeyEvent e) {
    provinciaActual = "";
    municipioActual = "";
    txtmunicipio.setText("");
    btnMunicipio.setEnabled(false);
    this.doLayout();
  }

  void btnProvincia_actionPerformed(ActionEvent e) {

    DataCat2 data;
    DataCat2 datosBK;
    int modo = modoOperacion;

    modoOperacion = modoESPERA;
    Inicializar();

    CListaCat2 lista = new CListaCat2(app,
                                      res.getString("msg7.Text"),
                                      stubProvincia,
                                      strSERVLETProv,
                                      servletOBTENER_X_CODIGO +
                                      catalogo2.Catalogo2.catPROVINCIA,
                                      servletOBTENER_X_DESCRIPCION +
                                      catalogo2.Catalogo2.catPROVINCIA,
                                      servletSELECCION_X_CODIGO +
                                      catalogo2.Catalogo2.catPROVINCIA,
                                      servletSELECCION_X_DESCRIPCION +
                                      catalogo2.Catalogo2.catPROVINCIA);
    lista.show();
    datosBK = (DataCat2) lista.getComponente();

    if (datosBK != null) {
      txtProvincia.setText(datosBK.getDes());
      provinciaActual = datosBK.getCod();

    }
    modoOperacion = modo;
    Inicializar();

  }

  void btnAceptar_actionPerformed(ActionEvent e) {
    DataCasIn datos = null;
    CLista listaResul = null;
    CLista listaPar = null;
    Date fecha;
    CMessage mensaje = null;

    //if (isDataValid()){
    if (true) {
      try {
        listaResul = new CLista();
        listaPar = new CLista();
        fecha = new Date();

        listaPar.addElement(new DataCasIn(txtAnDesde.getText(),
                                          txtAnHasta.getText(),
                                          txtSeDesde.getText(),
                                          txtSeHasta.getText(),
                                          enfermedadActual, provinciaActual,
                                          municipioActual, nivel1Actual,
                                          nivel2Actual, zonaActual, "", ""));

        listaResul = (CLista) stubMunicipio.doPost(
            servletSELECCION_INDIVIDUAL_X_CODIGO,
            listaPar);
        if (listaResul != null) {
          datos = (DataCasIn) listaResul.firstElement();
        }
      }
      catch (Exception erf) {
        erf.printStackTrace();
        mensaje = new CMessage(this.app, CMessage.msgERROR, erf.getMessage());
        mensaje.show();
      }
    }
  }

}

class Pan_vCasIn_btnEnfCie_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_vCasIn adaptee;
  ActionEvent e;

  Pan_vCasIn_btnEnfCie_actionAdapter(Pan_vCasIn adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnEnfCie_actionPerformed(e);
  }
}

class Pan_vCasIn_btnNiv1_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_vCasIn adaptee;
  ActionEvent e;

  Pan_vCasIn_btnNiv1_actionAdapter(Pan_vCasIn adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnNiv1_actionPerformed(e);
  }
}

class Pan_vCasIn_btnNiv2_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_vCasIn adaptee;
  ActionEvent e;

  Pan_vCasIn_btnNiv2_actionAdapter(Pan_vCasIn adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnNiv2_actionPerformed(e);
  }
}

class Pan_vCasIn_btnZona_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_vCasIn adaptee;
  ActionEvent e;

  Pan_vCasIn_btnZona_actionAdapter(Pan_vCasIn adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnZona_actionPerformed(e);
  }
}

class Pan_vCasIn_btnMunicipio_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_vCasIn adaptee;
  ActionEvent e;

  Pan_vCasIn_btnMunicipio_actionAdapter(Pan_vCasIn adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnMunicipio_actionPerformed(e);
  }
}

// lista de valores
class CListaEnfCie
    extends CListaValores {

  public CListaEnfCie(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataEnferedo(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataEnferedo) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataEnferedo) o).getDes());
  }
}

// lista de valores
class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

// lista de valores
class CListaZBS2
    extends CListaValores {

  protected Pan_vCasIn panel;

  public CListaZBS2(Pan_vCasIn p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.nivel1Actual, s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

// lista de valores
class CListaZona
    extends CListaValores {

  protected Pan_vCasIn panel;

  public CListaZona(Pan_vCasIn p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS(s, "", "", panel.nivel1Actual, panel.nivel2Actual);
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS) o).getDes());
  }
}

// lista de valores
class CListaMun
    extends CListaValores {

  protected Pan_vCasIn panel;

  public CListaMun(Pan_vCasIn p,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataMun(panel.provinciaActual, s, "", "", "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataMun) o).getMunicipio());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataMun) o).getDescMun());
  }
}

class Pan_vCasIn_txtNiv1_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_vCasIn adaptee;

  Pan_vCasIn_txtNiv1_keyAdapter(Pan_vCasIn adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtNiv1_keyPressed(e);
  }
}

class Pan_vCasIn_txtDistrito_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_vCasIn adaptee;

  Pan_vCasIn_txtDistrito_keyAdapter(Pan_vCasIn adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtDistrito_keyPressed(e);
  }
}

class Pan_vCasIn_txtZona_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_vCasIn adaptee;

  Pan_vCasIn_txtZona_keyAdapter(Pan_vCasIn adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtZona_keyPressed(e);
  }
}

class Pan_vCasIn_txtEnfCie_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_vCasIn adaptee;

  Pan_vCasIn_txtEnfCie_keyAdapter(Pan_vCasIn adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtEnfCie_keyPressed(e);
  }
}

class Pan_vCasIn_txtProvincia_keyAdapter
    extends java.awt.event.KeyAdapter {
  Pan_vCasIn adaptee;

  Pan_vCasIn_txtProvincia_keyAdapter(Pan_vCasIn adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txtProvincia_keyPressed(e);
  }
}

class Pan_vCasIn_btnProvincia_actionAdapter
    implements java.awt.event.ActionListener, Runnable {
  Pan_vCasIn adaptee;
  ActionEvent e;

  Pan_vCasIn_btnProvincia_actionAdapter(Pan_vCasIn adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  public void run() {
    adaptee.btnProvincia_actionPerformed(e);
  }
}

class Pan_vCasIn_btnAceptar_actionAdapter
    implements java.awt.event.ActionListener {
  Pan_vCasIn adaptee;

  Pan_vCasIn_btnAceptar_actionAdapter(Pan_vCasIn adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.btnAceptar_actionPerformed(e);
  }
}
