package vCasIn;

import java.io.Serializable;

public class DataCasIn
    implements Serializable {

  protected String sCD_ANODESDE = "";
  protected String sCD_ANOHASTA = "";
  protected String sCD_SEMDESDE = "";
  protected String sCD_SEMHASTA = "";
  protected String sCD_ENFCIE = "";
  protected String sCD_PROV = "";
  protected String sCD_MUN = "";
  protected String sCD_NIV1 = "";
  protected String sCD_NIV2 = "";
  protected String sCD_ZBS = "";
  protected String dFC_NAC = "";
  protected String sCD_SEXO = "";

  public DataCasIn(String anoDesde,
                   String anoHasta,
                   String semDesde,
                   String semHasta,
                   String enfcie,
                   String prov,
                   String muni,
                   String niv1,
                   String niv2,
                   String ZBS,
                   String FC_NAC,
                   String sexo) {

    sCD_ANODESDE = anoDesde;
    sCD_ANOHASTA = anoHasta;
    sCD_SEMDESDE = semDesde;
    sCD_SEMHASTA = semHasta;
    sCD_ENFCIE = enfcie;
    sCD_PROV = prov;
    sCD_MUN = muni;
    sCD_NIV1 = niv1;
    sCD_NIV2 = niv2;
    sCD_ZBS = ZBS;
    dFC_NAC = FC_NAC;
    sCD_SEXO = sexo;
  }

  public String getAnoDesde() {
    return sCD_ANODESDE;
  }

  public String getAnoHasta() {
    return sCD_ANOHASTA;
  }

  public String getSemDesde() {
    return sCD_SEMDESDE;
  }

  public String getSemHasta() {
    return sCD_SEMHASTA;
  }

  public String getEnfCie() {
    return sCD_ENFCIE;
  }

  public String getProvincia() {
    return sCD_PROV;
  }

  public String getMun() {
    return sCD_MUN;
  }

  public String getNiv1() {
    return sCD_NIV1;
  }

  public String getNiv2() {
    return sCD_NIV2;
  }

  public String getZBS() {
    return sCD_ZBS;
  }

  public String getFechaNac() {
    return dFC_NAC;
  }

  public String getSexo() {
    return sCD_SEXO;
  }
}
