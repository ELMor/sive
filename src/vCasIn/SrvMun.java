package vCasIn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CApp;
import capp.CLista;
import sapp.DBServlet;
import zbs.DataZBS;

public class SrvMun
    extends DBServlet {

  //Modos de operaci�n del Servlet
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int i = 1;
    int pos = -1;
    int posEnfCie = -1;
    int posProvincia = -1;
    int posMunicipio = -1;
    int posNivel1 = -1;
    int posNivel2 = -1;
    int posZBS = -1;

    // objetos de datos
    CLista data = new CLista();
    DataMun datos = null;
    DataZBS datos2 = null;
    DataCasIn datos3 = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(true);

    String sDesZbs = "";
    String sDesZbsL = "";

    // modos de operaci�n
    switch (opmode) {

      // alta
      case servletALTA:

        // prepara la query
        query = "insert into  () values (?)";
        st = con.prepareStatement(query);
        // codigo
        //st.setString(1, datos.get().trim());
        // lanza la query
        st.executeUpdate();
        st.close();

        break;

        // baja
      case servletBAJA:

        // prepara la query
        query = "DELETE FROM  WHERE CD_ = ?";

        // lanza la query
        st = con.prepareStatement(query);
        //st.setString(1, datos.get().trim());
        st.executeUpdate();
        st.close();

        break;

        // b�squeda
      case servletSELECCION_X_CODIGO:
      case servletSELECCION_X_DESCRIPCION:

        datos = (DataMun) param.firstElement();
        // prepara la query
        // ARG: upper (13/5/02)
        if (param.getFilter().length() > 0) {
          if (opmode == servletSELECCION_X_CODIGO) {

            /*
                 query = "select * from sive_municipio where CD_PROV like ? and "+
                        "CD_NIVEL_1 like ? and CD_NIVEL_2 like ? and "+
                 "CD_ZBS like ? and CD_MUN like ? and CD_MUN > ? order by CD_MUN";
             */
            query = "select * from sive_municipio where CD_PROV  = ? and " +
                "CD_MUN like ? and CD_MUN > ? order by CD_MUN";

          }
          else {

            /*
                 query = "select * from sive_municipio where CD_PROV like ? and "+
                        "CD_NIVEL_1 like ? and CD_NIVEL_2 like ? and "+
                 "CD_ZBS like ? and DS_MUN like ? and CD_MUN > ? order by CD_MUN";
             */
            query = "select * from sive_municipio where CD_PROV = ? and " +
                "upper(DS_MUN) like upper(?) and CD_MUN > ? order by CD_MUN";

          }
        }
        else {
          if (opmode == servletSELECCION_X_CODIGO) {

            /*
                 query = "select * from sive_municipio where CD_PROV like ? and "+
                        "CD_NIVEL_1 like ? and CD_NIVEL_2 like ? and "+
                        "CD_ZBS like ? and CD_MUN like ? order by CD_MUN";
             */
            query = "select * from sive_municipio where CD_PROV  = ? and " +
                "CD_MUN like ? order by CD_MUN";

          }
          else {

            /*
                 query = "select * from sive_municipio where CD_PROV like ? and "+
                        "CD_NIVEL_1 like ? and CD_NIVEL_2 like ? and "+
                        "CD_ZBS like ? and DS_MUN like ? order by CD_MUN";
             */
            query = "select * from sive_municipio where CD_PROV = ? and " +
                "upper(DS_MUN) like upper(?) order by CD_MUN";

          }
        }

        // prepara la lista de resultados
        data = new CLista();

        // lanza la query
        st = con.prepareStatement(query);
        /*
                // c�digo provincia
                st.setString(1, datos.getProvincia().trim() + "%");
                // c�digo nivel 1
                st.setString(2, datos.getNivel1().trim() + "%");
                // c�digo nivel 2
                st.setString(3, datos.getNivel2().trim() + "%");
                // c�digo zona b�sica
                st.setString(4, datos.getZBS().trim() + "%");
                // c�digo municipio
                st.setString(5, datos.getMunicipio().trim() + "%");
         */

        // c�digo provincia
        st.setString(1, datos.getProvincia().trim());
        // c�digo municipio
        if (opmode == servletSELECCION_X_CODIGO) {
          st.setString(2, datos.getMunicipio().trim() + "%");
        }
        else {
          st.setString(2, "%" + datos.getMunicipio().trim() + "%");

          // paginaci�n
        }
        if (param.getFilter().length() > 0) {
//          st.setString(6, param.getFilter().trim());
          st.setString(3, param.getFilter().trim());
        }
        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {
          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
//            data.setFilter( ((DataMun)data.lastElement()).getProvincia() );
            data.setFilter( ( (DataMun) data.lastElement()).getMunicipio());
            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          // a�ade un nodo
          data.addElement(new DataMun(rs.getString("CD_PROV"),
                                      rs.getString("CD_MUN"),
                                      rs.getString("CD_NIVEL_1"),
                                      rs.getString("CD_NIVEL_2"),
                                      rs.getString("CD_ZBS"),
                                      rs.getString("DS_MUN")));
          i++;
        }

        rs.close();
        st.close();

        break;

      case servletSELECCION_NIV2_X_CODIGO:
      case servletSELECCION_NIV2_X_DESCRIPCION:

        datos2 = (DataZBS) param.firstElement();
        // prepara la query
        // ARG: upper (14/5/02)
        if (param.getFilter().length() > 0) {
          if (opmode == servletSELECCION_NIV2_X_CODIGO) {
            query = "select * from sive_zona_basica where " +
                "CD_NIVEL_1 = ? and CD_NIVEL_2 = ? and " +
                "CD_ZBS like ? and CD_ZBS > ? order by CD_ZBS";
          }
          else {
            query = "select * from sive_zona_basica where " +
                "CD_NIVEL_1 = ? and CD_NIVEL_2 = ? and " +
                "upper(DS_ZBS) like upper(?) and CD_ZBS > ? order by CD_ZBS";
          }
        }
        else {
          if (opmode == servletSELECCION_NIV2_X_CODIGO) {
            query = "select * from sive_zona_basica where " +
                "CD_NIVEL_1 = ? and CD_NIVEL_2 = ? and " +
                "DS_ZBS like ? order by CD_ZBS";
          }
          else {
            query = "select * from sive_zona_basica where " +
                "CD_NIVEL_1 = ? and CD_NIVEL_2 = ? and " +
                "upper(DS_ZBS) like upper(?) order by CD_ZBS";
          }
        }

        // prepara la lista de resultados
        data = new CLista();

        // lanza la query
        st = con.prepareStatement(query);

        // c�digo nivel 1
        st.setString(1, datos2.getCod1().trim());
        // c�digo nivel 2
        st.setString(2, datos2.getCod2().trim());
        // c�digo zona b�sica
        if (opmode == servletSELECCION_NIV2_X_CODIGO) {
          st.setString(3, datos2.getCod().trim() + "%");
        }
        else {
          st.setString(3, "%" + datos2.getCod().trim() + "%");

          // paginaci�n
        }
        if (param.getFilter().length() > 0) {
          st.setString(4, param.getFilter().trim());
        }
        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {
          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataZBS) data.lastElement()).getCod());
            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          sDesZbs = rs.getString("DS_ZBS");
          sDesZbsL = rs.getString("DSL_ZBS");

          // obtiene la descripcion auxiliar en funci�n del idioma
          if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
            if (sDesZbsL != null) {
              sDesZbs = sDesZbsL;
            }
          }

          // a�ade un nodo
          data.addElement(new DataZBS(rs.getString("CD_ZBS"), sDesZbs,
                                      sDesZbsL, rs.getString("CD_NIVEL_1"),
                                      rs.getString("CD_NIVEL_2")));
          i++;
        }

        rs.close();
        st.close();

        break;

        // b�squeda
      case servletOBTENER_X_CODIGO:
      case servletOBTENER_X_DESCRIPCION:

        datos = (DataMun) param.firstElement();
        // prepara la query
        if (opmode == servletOBTENER_X_CODIGO) {

          /*
               query = "select * from sive_municipio where CD_PROV like ? and "+
                      "CD_NIVEL_1 like ? and CD_NIVEL_2 like ? and "+
                      "CD_ZBS like ? and CD_MUN = ? order by CD_MUN";
           */
          query = "select * from sive_municipio where CD_PROV = ? and " +
              "CD_MUN = ? order by CD_MUN";

        }
        else {

          /*
               query = "select * from sive_municipio where CD_PROV like ? and "+
                      "CD_NIVEL_1 like ? and CD_NIVEL_2 like ? and "+
                      "CD_ZBS like ? and DS_MUN = ? order by CD_MUN";
           */
          query = "select * from sive_municipio where CD_PROV = ? and " +
              "DS_MUN = ? order by CD_MUN";

          // prepara la lista de resultados
        }
        data = new CLista();

        // lanza la query
        st = con.prepareStatement(query);
        // c�digo provincia
        st.setString(1, datos.getProvincia().trim());
        /*
                // c�digo nivel 1
                st.setString(2, datos.getNivel1().trim() + "%");
                // c�digo nivel 2
                st.setString(3, datos.getNivel2().trim() + "%");
                // c�digo zona b�sica
                st.setString(4, datos.getZBS().trim() + "%");
                // c�digo municipio
                st.setString(5, datos.getMunicipio().trim());
         */

        // c�digo municipio
        st.setString(2, datos.getMunicipio().trim());

        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {
          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataMun) data.lastElement()).getMunicipio());
            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          // a�ade un nodo
          data.addElement(new DataMun(rs.getString("CD_PROV"),
                                      rs.getString("CD_MUN"),
                                      rs.getString("CD_NIVEL_1"),
                                      rs.getString("CD_NIVEL_2"),
                                      rs.getString("CD_ZBS"),
                                      rs.getString("DS_MUN")));
          i++;
        }

        rs.close();
        st.close();

        break;

        // b�squeda
      case servletOBTENER_NIV2_X_CODIGO:
      case servletOBTENER_NIV2_X_DESCRIPCION:

        datos2 = (DataZBS) param.firstElement();
        // prepara la query
        if (opmode == servletOBTENER_NIV2_X_CODIGO) {
          query = "select * from sive_zona_basica where CD_NIVEL_1 = ? and " +
              "CD_NIVEL_2 = ? and CD_ZBS = ? order by CD_ZBS";
        }
        else {
          query = "select * from sive_zona_basica where CD_NIVEL_1 = ? and " +
              "CD_NIVEL_2 = ? and DS_ZBS = ? order by CD_ZBS";

          // prepara la lista de resultados
        }
        data = new CLista();

        // lanza la query
        st = con.prepareStatement(query);

        // c�digo nivel 1
        st.setString(1, datos2.getCod1().trim());
        // c�digo nivel 2
        st.setString(2, datos2.getCod2().trim());
        // c�digo zona b�sica
        st.setString(3, datos2.getCod().trim());

        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {
          // control de tama�o
          if (i > DBServlet.maxSIZE) {
            data.setState(CLista.listaINCOMPLETA);
            data.setFilter( ( (DataZBS) data.lastElement()).getCod());
            break;
          }
          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }

          sDesZbs = rs.getString("DS_ZBS");
          sDesZbsL = rs.getString("DSL_ZBS");

          // obtiene la descripcion auxiliar en funci�n del idioma
          if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
            if (sDesZbsL != null) {
              sDesZbs = sDesZbsL;
            }
          }

          // a�ade un nodo
          data.addElement(new DataZBS(rs.getString("CD_ZBS"), sDesZbs,
                                      sDesZbsL, rs.getString("CD_NIVEL_1"),
                                      rs.getString("CD_NIVEL_2")));
          i++;
        }

        rs.close();
        st.close();

        break;

        // modificaci�n
      case servletMODIFICAR:

        // prepara la query
        query = "UPDATE  SET =?, WHERE CD_=?";

        // lanza la query
        st = con.prepareStatement(query);
        //st.setString(1, datos.get().trim());

        st.executeUpdate();
        st.close();

        break;

      case servletSELECCION_INDIVIDUAL_X_CODIGO:

        datos3 = (DataCasIn) param.firstElement();

        // prepara la query b�sica
        query = "select * from sive_enfermo b, sive_edoind a " +
            "where a.CD_ENFERMO=b.CD_ENFERMO and a.CD_ANOEPI>=?" +
            " and a.CD_ANOEPI<=? and a.CD_SEMEPI>=? and a.CD_SEMEPI<=?";

        // prepara el resto de la query
        pos = 5;
        if (datos3.getEnfCie() != null) {
          posEnfCie = pos;
          query += " and a.CD_ENFCIE = ?";
          pos++;
        }
        if (datos3.getMun() != null) {
          posMunicipio = pos;
          query += " and a.CD_MUN = ?";
          pos++;
        }
        if (datos3.getNiv1() != null) {
          posNivel1 = pos;
          query += " and a.CD_NIVEL_1 = ?";
          pos++;
        }
        if (datos3.getNiv2() != null) {
          posNivel2 = pos;
          query += " and a.CD_NIVEL_2 = ?";
          pos++;
        }
        if (datos3.getProvincia() != null) {
          posProvincia = pos;
          query += " and a.CD_PROV = ?";
          pos++;
        }
        if (datos3.getZBS() != null) {
          posZBS = pos;
          query += " and a.CD_ZBS = ?";
          pos++;
        }

        // prepara la lista de resultados
        data = new CLista();

        // lanza la query
        st = con.prepareStatement(query);
        // c�digo a�o desde
        st.setString(1, datos3.getAnoDesde().trim());
        // c�digo a�o hasta
        st.setString(2, datos3.getAnoHasta().trim());
        // c�digo semana desde
        st.setString(3, datos3.getSemDesde().trim());
        // c�digo semana hasta
        st.setString(4, datos3.getSemHasta().trim());

        // Ahora los c�digos opcionales
        // c�digo enfcie
        if (posEnfCie != -1) {
          st.setString(posEnfCie, datos3.getEnfCie().trim());
          // c�digo municipio
        }
        if (posMunicipio != -1) {
          st.setString(posMunicipio, datos3.getMun().trim());
          // c�digo nivel 1
        }
        if (posNivel1 != -1) {
          st.setString(posNivel1, datos3.getNiv1().trim());
          // c�digo nivel 2
        }
        if (posNivel2 != -1) {
          st.setString(posNivel2, datos3.getNiv2().trim());
          // c�digo provincia
        }
        if (posProvincia != -1) {
          st.setString(posProvincia, datos3.getProvincia().trim());
          // c�digo zona b�sica de salud
        }
        if (posZBS != -1) {
          st.setString(posZBS, datos3.getZBS().trim());

        }
        rs = st.executeQuery();

        // extrae la p�gina requerida
        while (rs.next()) {

          // control de estado
          if (data.getState() == CLista.listaVACIA) {
            data.setState(CLista.listaLLENA);
          }
          // a�ade un nodo
          data.addElement(new DataCasIn(rs.getString("CD_ANOEPI"), "",
                                        rs.getString("CD_SEMEPI"), "",
                                        rs.getString("CD_ENFCIE"),
                                        rs.getString("CD_PROV"),
                                        rs.getString("CD_MUN"),
                                        rs.getString("CD_NIVEL_1"),
                                        rs.getString("CD_NIVEL_2"),
                                        rs.getString("CD_ZBS"),
                                        rs.getDate("FC_NAC").toString(),
                                        rs.getString("CD_SEXO")));
          i++;
        }

        rs.close();
        st.close();

        break;

    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    if (data != null) {
      data.trimToSize();

    }
    return data;
  }
}
