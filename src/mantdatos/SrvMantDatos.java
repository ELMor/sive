package mantdatos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import capp.CLista;
import param.DataParam;
import sapp.DBServlet;

public class SrvMantDatos
    extends DBServlet {

  // modos de operaci�n
  final int servletALTA = 0;
  final int servletMODIFICAR = 1;
  final int servletBAJA = 2;
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;

  // funcionalidad del servlet
  protected CLista doWork(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    ResultSet rs = null;
    int iValor = 0;
    DataParam dman;
    int contador;

    // objetos de datos
    CLista data = null;
    DataMantDatos dParam = null;

    // establece la conexi�n con la base de datos
    con = openConnection();
    con.setAutoCommit(false);

    try {
      // modos de operaci�n
      switch (opmode) {

        // alta de lista de valores
        case servletALTA:

          // primero borramos todo
          query = "DELETE FROM SIVE_SERVICIO_EPI";
          st = con.prepareStatement(query);
          st.executeUpdate();
          st.close();
          st = null;

          // prepara la query
          dParam = (DataMantDatos) param.firstElement();
          query =
              "insert into sive_servicio_epi (cd_ca,ds_consej,ds_direcg,ds_serv,ds_direc" +
              ",cd_postal,ds_pob,ds_jefeserv,ds_telef,ds_fax,ds_email) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
          st = con.prepareStatement(query);
          // codigo CCAA
          st.setString(1, dParam.getCCAA());
          // consejer�a
          st.setString(2, dParam.getCodConsejero().trim());
          // direcci�n general
          st.setString(3, dParam.getDirGeneral().trim());
          // descripci�n servicio
          st.setString(4, dParam.getServicio().trim());
          // direcci�n
          st.setString(5, dParam.getDireccion().trim());
          // c�digo postal
          st.setString(6, dParam.getCP().trim());
          // descripci�n poblaci�n (municipio)
          st.setString(7, dParam.getPoblacion());
          // Jefe de servicio
          st.setString(8, dParam.getJefeServ().trim());
          // tel�fono
          if (dParam.getTelefono().trim().length() > 0) {
            st.setString(9, dParam.getTelefono());
          }
          else {
            st.setNull(9, java.sql.Types.VARCHAR);
            // fax
          }
          if (dParam.getFax().trim().length() > 0) {
            st.setString(10, dParam.getFax());
          }
          else {
            st.setNull(10, java.sql.Types.VARCHAR);
            // e-mail
          }
          if (dParam.getEMail().trim().length() > 0) {
            st.setString(11, dParam.getEMail().trim());
          }
          else {
            st.setNull(11, java.sql.Types.VARCHAR);
            // lanza la query
          }
          st.executeUpdate();
          st.close();
          data = new CLista();
          data.addElement(new DataParam("", "", "", "", "", "", "", "", "", "",
                                        "", "", ""));
          break;

          // Baja de par�metros
        case servletBAJA:
          query = "DELETE FROM CA_PARAMETROS";
          st = con.prepareStatement(query);
          st.executeUpdate();
          st.close();
          data = new CLista();
          data.addElement(new DataParam("", "", "", "", "", "", "", "", "", "",
                                        "", "", ""));
          break;

          // b�squeda de par�metros
        case servletOBTENER_X_CODIGO:

          query =
              "select * from sive_servicio_epi a, sive_com_aut b where a.CD_CA=b.CD_CA";
          // prepara la lista de resultados
          data = new CLista();

          // lanza la query
          st = con.prepareStatement(query);

          // paginaci�n

          rs = st.executeQuery();

          // extrae la p�gina requerida
          while (rs.next()) {
            // control de estado
            if (data.getState() == CLista.listaVACIA) {
              data.setState(CLista.listaLLENA);
            }
            // A�ade un nodo
            data.addElement(new DataMantDatos(rs.getString("CD_CA"),
                                              rs.getString("DS_CONSEJ"),
                                              rs.getString("DS_DIRECG"),
                                              rs.getString("DS_SERV"),
                                              rs.getString("DS_DIREC"),
                                              rs.getString("CD_POSTAL"),
                                              rs.getString("DS_POB"),
                                              rs.getString("DS_JEFESERV"),
                                              rs.getString("DS_TELEF"),
                                              rs.getString("DS_FAX"),
                                              rs.getString("DS_EMAIL")));
          }
          rs.close();
          st.close();

          break;
      }
      con.commit();
    }
    catch (Exception ef) {
      con.rollback();
      throw ef;
    }

    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    data.trimToSize();
    return data;
  }
}
