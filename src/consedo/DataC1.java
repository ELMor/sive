package consedo;

import java.io.Serializable;

public class DataC1
    implements Serializable {

  public String DS_PROCESO;
  //AIC se cambia para que el n�mero de casos aparezca correctamente en los informes
  //public int CASOS;
  public String CASOS;

  //AIC
  //formatea el entero para darle formato a los  superiores a 1.000
  /*public static String formatearEntero(int entero)
     {
    String resultado = "";
    String resto;
    while((entero / 1000) > 0)
    {
       int restoInt = entero % 1000;
       if(restoInt == 0)
       {
         resto = "000";
       }
       else
       {
         resto = String.valueOf(entero % 1000);
         for(int i = 0; i < 3 - resto.length(); i++)
         {
           resto = resto + "0";
         }
       }
       resultado = "." + resto + resultado;
       entero = (int)entero / 1000;
    }
    resultado = String.valueOf(entero) + resultado;
    return(resultado);
     } */

  // hay que modificar este m�todo pues calcula mal algunos valores
  // 1570 en vez de 1057
  public static String formatearEntero(int entero) {
    String resultado = "";
    String resto;
    String resto2;
    while ( (entero / 1000) > 0) {
      int restoInt = entero % 1000;
      if (restoInt == 0) {
        resto = "000";
      }
      else {
        resto = String.valueOf(entero % 1000);
        resto2 = String.valueOf(entero % 1000);
        for (int i = 0; i < 3 - resto2.length(); i++) {
          //resto = resto + "0";
          resto = "0" + resto;
        }
      }

      resultado = "." + resto + resultado;
      entero = (int) entero / 1000;
    }

    resultado = String.valueOf(entero) + resultado;

    return (resultado);

  }

  //AIC
  //m�todo para asignar el valor al campo CASOS;
  public void setCasos(int i) {
    CASOS = formatearEntero(i);
  }

  public DataC1(String d,
                int i) {
    DS_PROCESO = d;
    CASOS = formatearEntero(i);
  }
}
