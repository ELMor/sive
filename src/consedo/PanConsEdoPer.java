package consedo;

import java.net.URL;
import java.util.ResourceBundle;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import com.borland.jbcl.control.ButtonControl;
import com.borland.jbcl.control.LabelControl;
import com.borland.jbcl.layout.XYConstraints;
import com.borland.jbcl.layout.XYLayout;
import capp.CApp;
import capp.CCampoCodigo;
import capp.CLista;
import capp.CListaValores;
import capp.CMessage;
import capp.CPanel;
import catalogo2.DataCat2;
import cn.DataCN;
import eqNot.DataEqNot;
import nivel1.DataNivel1;
import sapp.StubSrvBD;
import utilidades.PanFechas;
import vCasIn.DataMun;
import zbs.DataZBS;
import zbs.DataZBS2;

//import graf.*;

public class PanConsEdoPer
    extends CPanel {

  //Modos de operaci�n de la ventana
  final int modoNORMAL = 0;
  ResourceBundle res;
  final int modoESPERA = 2;

  //rutas imagenes
  final String imgLUPA = "images/Magnify.gif";
  final String imgLIMPIAR = "images/vaciar.gif";
  final String imgGENERAR = "images/grafico.gif";

  protected StubSrvBD stubCliente = new StubSrvBD();

  public ParamC1 paramC1;

  protected int modoOperacion = modoNORMAL;

  protected PanelInforme informe;

  // stub's
  final String strSERVLETNivel1 = "servlet/SrvNivel1";
  final String strSERVLETNivel2 = "servlet/SrvZBS2";
  final String strSERVLETZona = "servlet/SrvMun";
  final String strSERVLETMun = "servlet/SrvMun";
  //final String strSERVLETMun = "servlet/SrvMunicipio";
  final String strSERVLETProv = "servlet/SrvCat2";
  final String strSERVLETEquipo = "servlet/SrvEqNotCen";
  final String strSERVLETCentro = "servlet/SrvCN";

  //Modos de operaci�n del Servlet
  final int servletOBTENER_X_CODIGO = 3;
  final int servletOBTENER_X_DESCRIPCION = 4;
  final int servletSELECCION_X_CODIGO = 5;
  final int servletSELECCION_X_DESCRIPCION = 6;
  final int servletSELECCION_NIV2_X_CODIGO = 7;
  final int servletOBTENER_NIV2_X_CODIGO = 8;
  final int servletSELECCION_NIV2_X_DESCRIPCION = 9;
  final int servletOBTENER_NIV2_X_DESCRIPCION = 10;
  final int servletSELECCION_INDIVIDUAL_X_CODIGO = 11;

  XYLayout xYLayout = new XYLayout();

  Label lblHasta = new Label();
  Label lblDesde = new Label();
  Label lblProvincia = new Label();
  CCampoCodigo txtCodPro = new CCampoCodigo();
  ButtonControl btnCtrlBuscarPro = new ButtonControl();
  TextField txtDesPro = new TextField();
  Label lblMunicipio = new Label();
  CCampoCodigo txtCodMun = new CCampoCodigo();
  ButtonControl btnCtrlBuscarMun = new ButtonControl();
  TextField txtDesMun = new TextField();
  Label lblArea = new Label();
  CCampoCodigo txtCodAre = new CCampoCodigo();
  ButtonControl btnCtrlBuscarAre = new ButtonControl();
  TextField txtDesAre = new TextField();
  Label lblDistrito = new Label();
  CCampoCodigo txtCodDis = new CCampoCodigo();
  TextField txtDesDis = new TextField();
  Label lblZonaBasica = new Label();
  CCampoCodigo txtCodZBS = new CCampoCodigo();
  ButtonControl btnCtrlBuscarDis = new ButtonControl();
  ButtonControl btnCtrlBuscarZBS = new ButtonControl();
  TextField txtDesZBS = new TextField();
  Label lblCenNot = new Label();
  CCampoCodigo txtCodCenNot = new CCampoCodigo();
  ButtonControl btnCtrlBuscarCenNot = new ButtonControl();
  TextField txtDesCenNot = new TextField();
  Label lblEquNot = new Label();
  CCampoCodigo txtCodEquNot = new CCampoCodigo();
  ButtonControl btnCtrlBuscarEquNot = new ButtonControl();
  TextField txtDesEquNot = new TextField();
  PanFechas fechasDesde;
  PanFechas fechasHasta;
  ButtonControl btnLimpiar = new ButtonControl();
  ButtonControl btnInforme = new ButtonControl();

  focusAdapter txtFocusAdapter = new focusAdapter(this);
  txt_keyAdapter txtKeyAdapter = new txt_keyAdapter(this);
  actionListener btnActionListener = new actionListener(this);
  LabelControl labelControl1 = new LabelControl();
  LabelControl labelControl2 = new LabelControl();

  public PanConsEdoPer(CApp a) {
    try {

      setApp(a);
      res = ResourceBundle.getBundle("consedo.Res" + a.getIdioma());

      informe = new PanelInforme(a, this);
      fechasDesde = new PanFechas(this, PanFechas.modoINICIO_SEMANA, true);
      fechasHasta = new PanFechas(this, PanFechas.modoSEMANA_ACTUAL, true);
      jbInit();
      informe.setEnabled(false);
      lblArea.setText(this.app.getNivel1() + ":");
      lblDistrito.setText(this.app.getNivel2() + ":");
      this.txtCodAre.setText(this.app.getCD_NIVEL1_DEFECTO());
      this.txtDesAre.setText(this.app.getDS_NIVEL1_DEFECTO());
      this.txtCodDis.setText(this.app.getCD_NIVEL2_DEFECTO());
      this.txtDesDis.setText(this.app.getDS_NIVEL2_DEFECTO());
      modoOperacion = modoNORMAL;
      Inicializar();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {
    this.setSize(new Dimension(569, 300));
    xYLayout.setHeight(445);
    btnLimpiar.setLabel(res.getString("btnLimpiar.Label"));
    btnInforme.setLabel(res.getString("btnInforme.Label"));
    labelControl1.setText(res.getString("labelControl1.Text"));
    labelControl2.setText(res.getString("labelControl2.Text") + " " +
                          res.getString("msg3.Text"));
    xYLayout.setWidth(596);

    // gestores de eventos
    btnCtrlBuscarPro.addActionListener(btnActionListener);
    btnCtrlBuscarMun.addActionListener(btnActionListener);
    btnCtrlBuscarAre.addActionListener(btnActionListener);
    btnCtrlBuscarDis.addActionListener(btnActionListener);
    btnCtrlBuscarZBS.addActionListener(btnActionListener);
    btnCtrlBuscarCenNot.addActionListener(btnActionListener);
    btnCtrlBuscarEquNot.addActionListener(btnActionListener);
    btnLimpiar.addActionListener(btnActionListener);
    btnInforme.addActionListener(btnActionListener);

    txtCodPro.addFocusListener(txtFocusAdapter);
    txtCodMun.addFocusListener(txtFocusAdapter);
    txtCodAre.addFocusListener(txtFocusAdapter);
    txtCodDis.addFocusListener(txtFocusAdapter);
    txtCodZBS.addFocusListener(txtFocusAdapter);
    txtCodCenNot.addFocusListener(txtFocusAdapter);
    txtCodEquNot.addFocusListener(txtFocusAdapter);

    btnCtrlBuscarPro.setActionCommand("buscarPro");
    txtDesPro.setEditable(false);
    txtDesPro.setEnabled(false); /*E*/
    btnCtrlBuscarMun.setActionCommand("buscarMun");
    txtDesMun.setEditable(false);
    txtDesMun.setEnabled(false); /*E*/
    btnCtrlBuscarAre.setActionCommand("buscarAre");
    txtDesAre.setEditable(false);
    txtDesAre.setEnabled(false); /*E*/
    btnCtrlBuscarDis.setActionCommand("buscarDis");
    btnCtrlBuscarZBS.setActionCommand("buscarZBS");
    txtDesZBS.setEditable(false);
    txtDesZBS.setEnabled(false); /*E*/
    btnCtrlBuscarCenNot.setActionCommand("buscarCenNot");
    txtDesCenNot.setEditable(false);
    txtDesCenNot.setEnabled(false); /*E*/
    btnCtrlBuscarEquNot.setActionCommand("buscarEquNot");
    txtDesEquNot.setEditable(false);
    txtDesEquNot.setEnabled(false); /*E*/
    btnInforme.setActionCommand("generar");
    btnLimpiar.setActionCommand("limpiar");

    txtCodPro.setName("txtCodPro");
    txtCodMun.setName("txtCodMun");
    txtCodAre.setName("txtCodAre");
    txtCodDis.setName("txtCodDis");
    txtDesDis.setEditable(false);
    txtDesDis.setEnabled(false); /*E*/
    txtCodZBS.setName("txtCodZBS");
    txtCodCenNot.setName("txtCodCenNot");
    txtCodEquNot.setName("txtCodEquNot");

    lblHasta.setText(res.getString("lblHasta.Text"));
    lblProvincia.setText(res.getString("lblProvincia.Text"));
    lblMunicipio.setText(res.getString("lblMunicipio.Text"));
    lblZonaBasica.setText(res.getString("lblZonaBasica.Text"));
    lblCenNot.setText(res.getString("lblCenNot.Text"));
    lblEquNot.setText(res.getString("lblEquNot.Text"));
    lblArea.setText(res.getString("lblArea.Text"));
    lblDesde.setText(res.getString("lblDesde.Text"));
    this.setLayout(xYLayout);

    this.add(lblDesde, new XYConstraints(26, 5, 51, -1));
    this.add(fechasDesde, new XYConstraints(40, 5, -1, -1));
    this.add(lblHasta, new XYConstraints(26, 35, 51, -1));
    this.add(fechasHasta, new XYConstraints(40, 35, -1, -1));
    this.add(lblProvincia, new XYConstraints(26, 65, 64, -1));
    this.add(txtCodPro, new XYConstraints(143, 65, 77, -1));
    this.add(btnCtrlBuscarPro, new XYConstraints(225, 65, -1, -1));
    this.add(txtDesPro, new XYConstraints(254, 65, 287, -1));

    this.add(lblMunicipio, new XYConstraints(26, 95, 66, -1));
    this.add(txtCodMun, new XYConstraints(143, 95, 77, -1));
    this.add(btnCtrlBuscarMun, new XYConstraints(225, 95, -1, -1));
    this.add(txtDesMun, new XYConstraints(254, 95, 287, -1));
    this.add(lblArea, new XYConstraints(26, 125, -1, -1));
    this.add(txtCodAre, new XYConstraints(143, 125, 77, -1));
    this.add(btnCtrlBuscarAre, new XYConstraints(225, 125, -1, -1));
    this.add(txtDesAre, new XYConstraints(254, 125, 287, -1));

    this.add(lblDistrito, new XYConstraints(26, 155, 52, -1));
    this.add(txtCodDis, new XYConstraints(143, 155, 77, -1));
    this.add(btnCtrlBuscarDis, new XYConstraints(225, 155, -1, -1));
    this.add(txtDesDis, new XYConstraints(254, 155, 287, -1));

    this.add(lblZonaBasica, new XYConstraints(26, 185, 77, -1));
    this.add(txtCodZBS, new XYConstraints(143, 185, 77, -1));
    this.add(btnCtrlBuscarZBS, new XYConstraints(225, 185, -1, -1));
    this.add(txtDesZBS, new XYConstraints(254, 185, 287, -1));
    this.add(lblCenNot, new XYConstraints(26, 215, 101, -1));
    this.add(txtCodCenNot, new XYConstraints(143, 215, 77, -1));
    this.add(btnCtrlBuscarCenNot, new XYConstraints(225, 215, -1, -1));
    this.add(txtDesCenNot, new XYConstraints(254, 215, 287, -1));

    this.add(lblEquNot, new XYConstraints(26, 245, 110, -1));
    this.add(txtCodEquNot, new XYConstraints(143, 245, 77, -1));
    this.add(btnCtrlBuscarEquNot, new XYConstraints(225, 245, -1, -1));
    this.add(txtDesEquNot, new XYConstraints(254, 245, 287, -1));
    this.add(btnLimpiar, new XYConstraints(399, 275, -1, -1));
    this.add(btnInforme, new XYConstraints(475, 275, -1, -1));
    this.add(labelControl1, new XYConstraints(24, 275, 341, -1));
    this.add(labelControl2, new XYConstraints(24, 295, 430, -1));

    /*
        Situador sit = new Situador();
        Point p = sit.getOrigen();
        this.add(lblDesde, new XYConstraints(p.x, p.y, 51, -1));
        p = sit.avanzaHor(lblDesde);
        this.add(fechasDesde, new XYConstraints(p.x, p.y, -1, -1));
        p = sit.avanzaVer(lblDesde);
        this.add(lblHasta, new XYConstraints(p.x, p.y, 51, -1));
        p = sit.avanzaHor(lblHasta);
        this.add(fechasHasta, new XYConstraints(p.x, p.y, -1, -1));
        p = sit.avanzaVer(lblHasta);
        this.add(lblProvincia, new XYConstraints(p.x, p.y, 64, -1));
        p = sit.avanzaHor(lblProvincia);
        this.add(txtCodPro, new XYConstraints(p.x, p.y, 77, -1));
        p = sit.avanzaHor(txtCodPro);
        this.add(btnCtrlBuscarPro, new XYConstraints(p.x, p.y, -1, -1));
        p = sit.avanzaHor(btnCtrlBuscarPro);
        this.add(txtDesPro, new XYConstraints(p.x, p.y, 287, -1));
        this.add(lblMunicipio, new XYConstraints(26, 157, 66, -1));
        this.add(txtCodMun, new XYConstraints(143, 157, 77, -1));
        this.add(btnCtrlBuscarMun, new XYConstraints(225, 157, -1, -1));
        this.add(txtDesMun, new XYConstraints(254, 157, 287, -1));
        this.add(lblArea, new XYConstraints(26, 196, -1, -1));
        this.add(txtCodAre, new XYConstraints(143, 196, 77, -1));
        this.add(btnCtrlBuscarAre, new XYConstraints(225, 196, -1, -1));
        this.add(txtDesAre, new XYConstraints(254, 196, 287, -1));
        this.add(lblDistrito, new XYConstraints(26, 234, 52, -1));
        this.add(txtCodDis, new XYConstraints(143, 234, 77, -1));
        this.add(btnCtrlBuscarDis, new XYConstraints(225, 234, -1, -1));
        this.add(txtDesDis, new XYConstraints(254, 234, 287, -1));
        this.add(lblZonaBasica, new XYConstraints(26, 272, 77, -1));
        this.add(txtCodZBS, new XYConstraints(143, 272, 77, -1));
        this.add(btnCtrlBuscarZBS, new XYConstraints(225, 272, -1, -1));
        this.add(txtDesZBS, new XYConstraints(254, 272, 287, -1));
        this.add(lblCenNot, new XYConstraints(26, 311, 101, -1));
        this.add(txtCodCenNot, new XYConstraints(143, 311, 77, -1));
        this.add(btnCtrlBuscarCenNot, new XYConstraints(225, 311, -1, -1));
        this.add(txtDesCenNot, new XYConstraints(254, 311, 287, -1));
        this.add(lblEquNot, new XYConstraints(26, 349, 110, -1));
        this.add(txtCodEquNot, new XYConstraints(143, 349, 77, -1));
        this.add(btnCtrlBuscarEquNot, new XYConstraints(225, 349, -1, -1));
        this.add(txtDesEquNot, new XYConstraints(254, 349, 287, -1));
        this.add(btnLimpiar, new XYConstraints(399, 384, -1, -1));
        this.add(btnInforme, new XYConstraints(475, 384, -1, -1));
        this.add(labelControl1, new XYConstraints(24, 386, 341, -1));
        this.add(labelControl2, new XYConstraints(24, 409, 430, -1));
     */

    btnCtrlBuscarPro.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarMun.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarAre.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarDis.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarZBS.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarCenNot.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnCtrlBuscarEquNot.setImageURL(new URL(app.getCodeBase(), imgLUPA));
    btnLimpiar.setImageURL(new URL(app.getCodeBase(), imgLIMPIAR));
    btnInforme.setImageURL(new URL(app.getCodeBase(), imgGENERAR));

    this.modoOperacion = modoNORMAL;
    Inicializar();
  }

  // valida datos m�nimos de envio
  public boolean isDataValid() {
    boolean bDatosCompletos = true;

    if ( (fechasDesde.txtAno.getText().length() == 0) ||
        (fechasHasta.txtAno.getText().length() == 0) ||
        (fechasDesde.txtCodSem.getText().length() == 0) ||
        (fechasHasta.txtCodSem.getText().length() == 0)) {
      bDatosCompletos = false;

    }
    return bDatosCompletos;
  }

  // configura los controles seg�n el modo de operaci�n
  public void Inicializar() {

    switch (modoOperacion) {
      case modoNORMAL:
        txtCodPro.setEnabled(true);
        txtCodAre.setEnabled(true);
        txtCodCenNot.setEnabled(true);
        //txtDesPro.setEnabled(true);
        //txtDesMun.setEnabled(true);
        //txtDesAre.setEnabled(true);
        //txtDesDis.setEnabled(true);
        //txtDesZBS.setEnabled(true);
        //txtDesCenNot.setEnabled(true);
        //txtDesEquNot.setEnabled(true);
        btnCtrlBuscarPro.setEnabled(true);

        // control municipio
        if (!txtDesPro.getText().equals("")) {
          btnCtrlBuscarMun.setEnabled(true);
          txtCodMun.setEnabled(true);
        }
        else {
          btnCtrlBuscarMun.setEnabled(false);
          txtCodMun.setEnabled(false);
        }

        btnCtrlBuscarAre.setEnabled(true);
        // control area
        if (!txtDesAre.getText().equals("")) {
          btnCtrlBuscarDis.setEnabled(true);
          txtCodDis.setEnabled(true);
        }
        else {
          btnCtrlBuscarDis.setEnabled(false);
          txtCodDis.setEnabled(false);
        }

        // control distrito
        if (!txtDesDis.getText().equals("")) {
          btnCtrlBuscarZBS.setEnabled(true);
          txtCodZBS.setEnabled(true);
        }
        else {
          btnCtrlBuscarZBS.setEnabled(false);
          txtCodZBS.setEnabled(false);
        }

        // control eq
        if (!txtDesCenNot.getText().equals("")) {
          btnCtrlBuscarEquNot.setEnabled(true);
          txtCodEquNot.setEnabled(true);
        }
        else {
          txtCodEquNot.setEnabled(false);
          btnCtrlBuscarEquNot.setEnabled(false);
        }

        btnCtrlBuscarCenNot.setEnabled(true);
        btnLimpiar.setEnabled(true);
        btnInforme.setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        break;

      case modoESPERA:
        txtCodPro.setEnabled(false);
        txtCodMun.setEnabled(false);
        txtCodAre.setEnabled(false);
        txtCodDis.setEnabled(false);
        txtCodZBS.setEnabled(false);
        txtCodCenNot.setEnabled(false);
        txtCodEquNot.setEnabled(false);
        btnCtrlBuscarPro.setEnabled(false);
        btnCtrlBuscarMun.setEnabled(false);
        btnCtrlBuscarAre.setEnabled(false);
        btnCtrlBuscarDis.setEnabled(false);
        btnCtrlBuscarZBS.setEnabled(false);
        btnCtrlBuscarCenNot.setEnabled(false);
        btnCtrlBuscarEquNot.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnInforme.setEnabled(false);
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        break;

    }
    this.doLayout();
  }

  //busca la provincia
  void btnCtrlbuscarPro_actionPerformed(ActionEvent evt) {
    DataCat2 data = null;
    DataCat2 datProv = null;
    CMessage msgBox = null;

    try {
      modoOperacion = modoESPERA;
      Inicializar();

      catalogo2.CListaCat2 lista = new catalogo2.CListaCat2(app,
          res.getString("msg4.Text"),
          stubCliente,
          strSERVLETProv,
          servletOBTENER_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA,
          servletOBTENER_X_DESCRIPCION + catalogo2.Catalogo2.catPROVINCIA,
          servletSELECCION_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA,
          servletSELECCION_X_DESCRIPCION + catalogo2.Catalogo2.catPROVINCIA);
      lista.show();
      datProv = (DataCat2) lista.getComponente();
    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (datProv != null) {
      txtCodPro.removeKeyListener(txtKeyAdapter);
      txtCodPro.setText(datProv.getCod());
      txtDesPro.setText(datProv.getDes());

      txtCodMun.setText("");
      txtDesMun.setText("");

      txtCodPro.addKeyListener(txtKeyAdapter);
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca el municipio
  void btnCtrlbuscarMun_actionPerformed(ActionEvent evt) {
    DataMun data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaMun lista = new CListaMun(this,
                                      res.getString("msg5.Text"),
                                      stubCliente,
                                      strSERVLETMun,
                                      servletOBTENER_X_CODIGO,
                                      servletOBTENER_X_DESCRIPCION,
                                      servletSELECCION_X_CODIGO,
                                      servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataMun) lista.getComponente();

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodMun.removeKeyListener(txtKeyAdapter);
      txtCodMun.setText(data.getMunicipio());
      txtDesMun.setText(data.getDescMun());
      txtCodMun.addKeyListener(txtKeyAdapter);
    }

    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //Busca los niveles1 (area de salud)
  void btnCtrlbuscarAre_actionPerformed(ActionEvent evt) {
    DataNivel1 data = null;
    CMessage mensaje = null;

    modoOperacion = modoESPERA;
    Inicializar();
    try {
      CListaNivel1 lista = new CListaNivel1(app,
                                            res.getString("msg6.Text") +
                                            this.app.getNivel1(),
                                            stubCliente,
                                            strSERVLETNivel1,
                                            servletOBTENER_X_CODIGO,
                                            servletOBTENER_X_DESCRIPCION,
                                            servletSELECCION_X_CODIGO,
                                            servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataNivel1) lista.getComponente();

    }
    catch (Exception excepc) {
      excepc.printStackTrace();
      mensaje = new CMessage(this.app, CMessage.msgERROR, excepc.getMessage());
      mensaje.show();
    }

    if (data != null) {
      txtCodAre.removeKeyListener(txtKeyAdapter);
      txtCodAre.setText(data.getCod());
      txtDesAre.setText(data.getDes());

      txtCodDis.setText("");
      txtDesDis.setText("");
      txtCodZBS.setText("");
      txtDesZBS.setText("");

      txtCodAre.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  //busca el distrito (Niv2)
  void btnCtrlbuscarDis_actionPerformed(ActionEvent evt) {

    DataZBS2 data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZBS2 lista = new CListaZBS2(this,
                                        res.getString("msg6.Text") +
                                        app.getNivel2(),
                                        stubCliente,
                                        strSERVLETNivel2,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS2) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodDis.removeKeyListener(txtKeyAdapter);
      txtCodDis.setText(data.getNiv2());
      txtDesDis.setText(data.getDes());

      txtCodZBS.setText("");
      txtDesZBS.setText("");

      txtCodDis.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnCtrlbuscarZBS_actionPerformed(ActionEvent evt) {
    DataZBS data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaZona lista = new CListaZona(this,
                                        res.getString("msg7.Text"),
                                        stubCliente,
                                        strSERVLETZona,
                                        servletOBTENER_NIV2_X_CODIGO,
                                        servletOBTENER_NIV2_X_DESCRIPCION,
                                        servletSELECCION_NIV2_X_CODIGO,
                                        servletSELECCION_NIV2_X_DESCRIPCION);
      lista.show();
      data = (DataZBS) lista.getComponente();

    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }
    if (data != null) {
      txtCodZBS.removeKeyListener(txtKeyAdapter);
      txtCodZBS.setText(data.getCod());
      txtDesZBS.setText(data.getDes());
      txtCodZBS.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnCtrlbuscarCenNot_actionPerformed(ActionEvent evt) {
    DataCN data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      CListaCN lista = new CListaCN(this.app,
                                    res.getString("msg8.Text"),
                                    stubCliente,
                                    strSERVLETCentro,
                                    servletOBTENER_X_CODIGO,
                                    servletOBTENER_X_DESCRIPCION,
                                    servletSELECCION_X_CODIGO,
                                    servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataCN) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodCenNot.removeKeyListener(txtKeyAdapter);
      txtCodCenNot.setText(data.getCodCentro());
      txtDesCenNot.setText(data.getCentroDesc());

      txtCodEquNot.setText("");
      txtDesEquNot.setText("");

      txtCodCenNot.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnCtrlbuscarEquNot_actionPerformed(ActionEvent evt) {

    DataEqNot data = null;
    CMessage msgBox = null;

    try {

      modoOperacion = modoESPERA;
      Inicializar();

      consedo.CListaEqNot lista = new consedo.CListaEqNot(this,
          res.getString("msg9.Text"),
          stubCliente,
          strSERVLETEquipo,
          servletOBTENER_X_CODIGO,
          servletOBTENER_X_DESCRIPCION,
          servletSELECCION_X_CODIGO,
          servletSELECCION_X_DESCRIPCION);
      lista.show();
      data = (DataEqNot) lista.getComponente();
    }
    catch (Exception er) {
      er.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, er.getMessage());
      msgBox.show();
      msgBox = null;
    }

    if (data != null) {
      txtCodEquNot.removeKeyListener(txtKeyAdapter);
      txtCodEquNot.setText(data.getEquipo());
      txtDesEquNot.setText(data.getDesEquipo());
      txtCodEquNot.addKeyListener(txtKeyAdapter);
    }
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnLimpiar_actionPerformed(ActionEvent evt) {
    txtCodAre.setText("");
    txtDesAre.setText("");
    txtCodDis.setText("");
    txtDesDis.setText("");
    txtCodZBS.setText("");
    txtDesZBS.setText("");
    txtCodPro.setText("");
    txtDesPro.setText("");
    txtCodMun.setText("");
    txtDesMun.setText("");
    txtCodEquNot.setText("");
    txtDesEquNot.setText("");
    txtCodCenNot.setText("");
    txtDesCenNot.setText("");
    modoOperacion = modoNORMAL;
    Inicializar();
  }

  void btnGenerar_actionPerformed(ActionEvent evt) {
    CMessage msgBox;
    informe.paramC1 = new ParamC1();

    if (isDataValid()) {

      if (!txtDesPro.getText().equals("")) {
        informe.paramC1.sProvincia = txtCodPro.getText();
      }
      if (!txtDesMun.getText().equals("")) {
        informe.paramC1.sMunicipio = txtCodMun.getText();
      }
      if (!txtDesAre.getText().equals("")) {
        informe.paramC1.sNivel_1 = txtCodAre.getText();
      }
      if (!txtDesDis.getText().equals("")) {
        informe.paramC1.sNivel_2 = txtCodDis.getText();
      }
      if (!txtDesZBS.getText().equals("")) {
        informe.paramC1.sZBS = txtCodZBS.getText();
      }
      if (!txtDesCenNot.getText().equals("")) {
        informe.paramC1.sCN = txtCodCenNot.getText();
      }
      if (!txtDesEquNot.getText().equals("")) {
        informe.paramC1.sEN = txtCodEquNot.getText();

      }
      informe.paramC1.sAnoI = fechasDesde.txtAno.getText();
      informe.paramC1.sAnoF = fechasHasta.txtAno.getText();
      informe.paramC1.sSemanaI = fechasDesde.txtCodSem.getText();
      informe.paramC1.sSemanaF = fechasHasta.txtCodSem.getText();

      // habilita el panel
      modoOperacion = modoESPERA;
      Inicializar();
      informe.setEnabled(true);
      boolean hay = informe.GenerarInforme();
      if (hay) {
        informe.show();
      }
      modoOperacion = modoNORMAL;
      Inicializar();

    }
    else {
      msgBox = new CMessage(this.app, CMessage.msgADVERTENCIA,
                            res.getString("msg10.Text"));
      msgBox.show();
      msgBox = null;
    }
  }

  // perdida de foco de cajas de c�digos
  void focusLost(FocusEvent e) {

    // datos de envio
    DataNivel1 nivel1;
    DataZBS2 nivel2;
    DataZBS zbs;
    DataMun mun;
    DataEqNot eqnot;
    DataCN cnnot;
    DataCat2 prov;
    CLista param = null;
    CMessage msg;
    String strServlet = null;
    int modoServlet = 0;

    TextField txt = (TextField) e.getSource();

    // gestion de datos
    if ( (txt.getName().equals("txtCodAre")) &&
        (txtCodAre.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataNivel1(txtCodAre.getText()));
      strServlet = strSERVLETNivel1;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodDis")) &&
             (txtCodDis.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS2(txtCodAre.getText(), txtCodDis.getText(),
                                    "", ""));
      strServlet = strSERVLETNivel2;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodZBS")) &&
             (txtCodZBS.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataZBS(txtCodZBS.getText(), "", "",
                                   txtCodAre.getText(), txtCodDis.getText()));
      strServlet = strSERVLETZona;
      modoServlet = servletOBTENER_NIV2_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodPro")) &&
             (txtCodPro.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataCat2(txtCodPro.getText()));
      strServlet = strSERVLETProv;
      modoServlet = servletOBTENER_X_CODIGO + catalogo2.Catalogo2.catPROVINCIA;

    }
    else if ( (txt.getName().equals("txtCodMun")) &&
             (txtCodMun.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataMun(txtCodPro.getText(), txtCodMun.getText(), "",
                                   "", "", ""));
      strServlet = strSERVLETMun;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodEquNot")) &&
             (txtCodEquNot.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataEqNot(txtCodEquNot.getText(), "",
                                     txtCodCenNot.getText(), "", "", "", "", "",
                                     "", "",
                                     "", 0, "", "", false));
      strServlet = strSERVLETEquipo;
      modoServlet = servletOBTENER_X_CODIGO;

    }
    else if ( (txt.getName().equals("txtCodCenNot")) &&
             (txtCodCenNot.getText().length() > 0)) {
      param = new CLista();
      param.setIdioma(app.getIdioma());
      param.addElement(new DataCN(txtCodCenNot.getText(), "", "", "", "", "",
                                  "",
                                  "", "", "", "", "", "", "", "", "", ""));
      strServlet = strSERVLETCentro;
      modoServlet = servletSELECCION_X_CODIGO;

    }

    // busca el item
    if (param != null) {

      try {
        // consulta en modo espera
        modoOperacion = modoESPERA;
        Inicializar();

        stubCliente.setUrl(new URL(app.getURL() + strServlet));
        param = (CLista) stubCliente.doPost(modoServlet, param);

        // rellena los datos
        if (param.size() > 0) {

          // realiza el cast y rellena los datos
          if (txt.getName().equals("txtCodAre")) {
            nivel1 = (DataNivel1) param.firstElement();
            txtCodAre.removeKeyListener(txtKeyAdapter);
            txtCodAre.setText(nivel1.getCod());
            txtDesAre.setText(nivel1.getDes());
            txtCodAre.addKeyListener(txtKeyAdapter);

          }
          else if (txt.getName().equals("txtCodDis")) {
            nivel2 = (DataZBS2) param.firstElement();
            txtCodDis.removeKeyListener(txtKeyAdapter);
            txtCodDis.setText(nivel2.getNiv2());
            txtDesDis.setText(nivel2.getDes());
            txtCodDis.addKeyListener(txtKeyAdapter);

          }
          else if (txt.getName().equals("txtCodZBS")) {
            zbs = (DataZBS) param.firstElement();
            txtCodZBS.removeKeyListener(txtKeyAdapter);
            txtCodZBS.setText(zbs.getCod());
            txtDesZBS.setText(zbs.getDes());
            txtCodZBS.addKeyListener(txtKeyAdapter);

          }
          else if (txt.getName().equals("txtCodPro")) {
            prov = (DataCat2) param.firstElement();
            txtCodPro.removeKeyListener(txtKeyAdapter);
            txtCodPro.setText(prov.getCod());
            txtDesPro.setText(prov.getDes());
            txtCodPro.addKeyListener(txtKeyAdapter);

          }
          else if (txt.getName().equals("txtCodMun")) {
            mun = (DataMun) param.firstElement();
            txtCodMun.removeKeyListener(txtKeyAdapter);
            txtCodMun.setText(mun.getMunicipio());
            txtDesMun.setText(mun.getDescMun());
            txtCodMun.addKeyListener(txtKeyAdapter);

          }
          else if (txt.getName().equals("txtCodEquNot")) {
            eqnot = (DataEqNot) param.firstElement();
            txtCodEquNot.removeKeyListener(txtKeyAdapter);
            txtCodEquNot.setText(eqnot.getEquipo());
            txtDesEquNot.setText(eqnot.getDesEquipo());
            txtCodEquNot.addKeyListener(txtKeyAdapter);

          }
          else if (txt.getName().equals("txtCodCenNot")) {
            cnnot = (DataCN) param.firstElement();
            txtCodCenNot.removeKeyListener(txtKeyAdapter);
            txtCodCenNot.setText(cnnot.getCodCentro());
            txtDesCenNot.setText(cnnot.getCentroDesc());
            txtCodCenNot.addKeyListener(txtKeyAdapter);
          }
        }
        // no hay datos
        else {
          msg = new CMessage(this.app, CMessage.msgAVISO,
                             res.getString("msg11.Text"));
          msg.show();
          msg = null;
        }

      }
      catch (Exception ex) {
        msg = new CMessage(this.app, CMessage.msgERROR, ex.toString());
        msg.show();
        msg = null;
      }

      // consulta en modo normal
      modoOperacion = modoNORMAL;
      Inicializar();
    }

  }

  // cambio en una caja de texto
  void txt_keyPressed(KeyEvent e) {
    TextField txt = (TextField) e.getSource();
    // gestion de datos
    if ( (txt.getName().equals("txtCodAre")) &&
        (txtDesAre.getText().length() > 0)) {
      txtCodDis.setText("");
      txtCodZBS.setText("");
      txtDesAre.setText("");
      txtDesDis.setText("");
      txtDesZBS.setText("");
    }
    else if ( (txt.getName().equals("txtCodDis")) &&
             (txtDesDis.getText().length() > 0)) {
      txtCodZBS.setText("");
      txtDesDis.setText("");
      txtDesZBS.setText("");
    }
    else if ( (txt.getName().equals("txtCodZBS")) &&
             (txtDesZBS.getText().length() > 0)) {
      txtDesZBS.setText("");
    }
    else if ( (txt.getName().equals("txtCodPro")) &&
             (txtDesPro.getText().length() > 0)) {
      txtCodMun.setText("");
      txtDesPro.setText("");
      txtDesMun.setText("");
    }
    else if ( (txt.getName().equals("txtCodMun")) &&
             (txtDesMun.getText().length() > 0)) {
      txtDesMun.setText("");
    }
    else if ( (txt.getName().equals("txtCodEquNot")) &&
             (txtDesEquNot.getText().length() > 0)) {
      txtDesEquNot.setText("");
    }
    else if ( (txt.getName().equals("txtCodCenNot")) &&
             (txtDesCenNot.getText().length() > 0)) {
      txtCodEquNot.setText("");
      txtDesCenNot.setText("");
      txtDesEquNot.setText("");
    }

    Inicializar();
  }
} //clase

// action listener para los botones
class actionListener
    implements ActionListener, Runnable {
  PanConsEdoPer adaptee = null;
  ActionEvent e = null;

  public actionListener(PanConsEdoPer adaptee) {
    this.adaptee = adaptee;
  }

  // evento
  public void actionPerformed(ActionEvent e) {
    this.e = e;
    new Thread(this).start();
  }

  // hilo de ejecuci�n para servir el evento
  public void run() {

    if (e.getActionCommand().equals("buscarPro")) {
      adaptee.btnCtrlbuscarPro_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarMun")) {
      adaptee.btnCtrlbuscarMun_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarAre")) {
      adaptee.btnCtrlbuscarAre_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarDis")) {
      adaptee.btnCtrlbuscarDis_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarZBS")) {
      adaptee.btnCtrlbuscarZBS_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarCenNot")) {
      adaptee.btnCtrlbuscarCenNot_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("buscarEquNot")) {
      adaptee.btnCtrlbuscarEquNot_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("generar")) {
      adaptee.btnGenerar_actionPerformed(e);
    }
    else if (e.getActionCommand().equals("limpiar")) {
      adaptee.btnLimpiar_actionPerformed(e);
    }
  }
}

// perdida del foco de una caja de codigo
class focusAdapter
    extends java.awt.event.FocusAdapter
    implements Runnable {
  PanConsEdoPer adaptee;
  FocusEvent event;

  focusAdapter(PanConsEdoPer adaptee) {
    this.adaptee = adaptee;
  }

  public void focusLost(FocusEvent e) {
    event = e;
    Thread th = new Thread(this);
    th.start();
  }

  public void run() {
    adaptee.focusLost(event);
  }
}

/*
// cambio en una caja de codigos
 class textAdapter implements java.awt.event.TextListener {
  PanConsEdoPer adaptee;
  textAdapter(PanConsEdoPer adaptee) {
    this.adaptee = adaptee;
  }
  public void textValueChanged(TextEvent e) {
    adaptee.textValueChanged(e);
  }
 }
 */

////////////////////// Clases para listas

// lista de valores
class CListaNivel1
    extends CListaValores {

  public CListaNivel1(CApp a,
                      String title,
                      StubSrvBD stub,
                      String servlet,
                      int obtener_x_codigo,
                      int obtener_x_descricpcion,
                      int seleccion_x_codigo,
                      int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataNivel1(s);
  }

  public String getCodigo(Object o) {
    return ( ( (DataNivel1) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataNivel1) o).getDes());
  }
}

// lista de valores
class CListaZBS2
    extends CListaValores {

  protected PanConsEdoPer panel;

  public CListaZBS2(PanConsEdoPer p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS2(panel.txtCodAre.getText(), s, "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS2) o).getNiv2());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS2) o).getDes());
  }
}

// lista de valores
class CListaZona
    extends CListaValores {

  protected PanConsEdoPer panel;

  public CListaZona(PanConsEdoPer p,
                    String title,
                    StubSrvBD stub,
                    String servlet,
                    int obtener_x_codigo,
                    int obtener_x_descricpcion,
                    int seleccion_x_codigo,
                    int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataZBS(s, "", "", panel.txtCodAre.getText(),
                       panel.txtCodDis.getText());
  }

  public String getCodigo(Object o) {
    return ( ( (DataZBS) o).getCod());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataZBS) o).getDes());
  }
}

class CListaMun
    extends CListaValores {

  protected PanConsEdoPer panel;

  public CListaMun(PanConsEdoPer p,
                   String title,
                   StubSrvBD stub,
                   String servlet,
                   int obtener_x_codigo,
                   int obtener_x_descricpcion,
                   int seleccion_x_codigo,
                   int seleccion_x_descripcion) {
    super(p.getApp(),
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);

    panel = p;
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataMun(panel.txtCodPro.getText(), s, "", "", "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataMun) o).getMunicipio());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataMun) o).getDescMun());
  }
}

// lista de valores
class CListaCN
    extends CListaValores {

  public CListaCN(CApp a,
                  String title,
                  StubSrvBD stub,
                  String servlet,
                  int obtener_x_codigo,
                  int obtener_x_descricpcion,
                  int seleccion_x_codigo,
                  int seleccion_x_descripcion) {
    super(a,
          title,
          stub,
          servlet,
          obtener_x_codigo,
          obtener_x_descricpcion,
          seleccion_x_codigo,
          seleccion_x_descripcion);
    btnSearch_actionPerformed(); //E
  }

  public Object setComponente(String s) {
    return new DataCN(s, "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                      "", "");
  }

  public String getCodigo(Object o) {
    return ( ( (DataCN) o).getCodCentro());
  }

  public String getDescripcion(Object o) {
    return ( ( (DataCN) o).getCentroDesc());
  }
}

class txt_keyAdapter
    extends java.awt.event.KeyAdapter {
  PanConsEdoPer adaptee;

  txt_keyAdapter(PanConsEdoPer adaptee) {
    this.adaptee = adaptee;
  }

  public void keyPressed(KeyEvent e) {
    adaptee.txt_keyPressed(e);
  }
}
