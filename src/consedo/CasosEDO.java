package consedo;

import java.util.ResourceBundle;

import capp.CApp;

public class CasosEDO
    extends CApp {
  public PanelInforme informe;
  ResourceBundle res;
  public PanConsEdoPer parametros;

  public CasosEDO() {
  }

  public void init() {
    super.init();
  }

  public void start() {
    res = ResourceBundle.getBundle("consedo.Res" + this.getIdioma());
    setTitulo(res.getString("msg1.Text"));
    CApp a = (CApp)this;
    parametros = new PanConsEdoPer(this);
    VerPanel(res.getString("msg2.Text"), parametros, false);
    VerPanel(res.getString("msg2.Text"));
  }
}
