package consedo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

// LORTAD
import Registro.RegistroConsultas;
import capp.CApp;
import capp.CLista;
import sapp.DBServlet;

public class SrvConsEdoPer
    extends DBServlet {

  // informes
  final int erwCASOS_EDO_RESUMEN = 1;
  final int erwCASOS_EDO_REAL = 2;

  /** Flag que indica si hay que aplicar la LORTAD */
  private boolean aplicarLortad;
  /** Objeto RegistroConsultas para registrar las consultas */
  private RegistroConsultas registroConsultas = null;

  // modificacion jlt para usar fechas en las selects
  /*  private java.sql.Timestamp cadena_a_timestamp(String sFecha){
      int dd = (new Integer(sFecha.substring(0,2))).intValue();
      int mm = (new Integer(sFecha.substring(3,5))).intValue();
      int yyyy = (new Integer(sFecha.substring(6,10))).intValue();
      int hh = (new Integer(sFecha.substring(11,13))).intValue();
      int mi = (new Integer(sFecha.substring(14,16))).intValue();
      int ss = (new Integer(sFecha.substring(17,19))).intValue();
      java.sql.Timestamp TSFec = new java.sql.Timestamp(yyyy-1900, mm-1, dd,
                                                        hh, mi, ss, 0);
      return TSFec;
    }*/

  protected CLista doWork(int opmode, CLista param) throws Exception {
    // funcionalidad del servlet(int opmode, CLista param) throws Exception {

    // objetos JDBC
    Connection con = null;
    PreparedStatement st = null;
    String query = null;
    String where = null;
    ResultSet rs = null;

    // control
    int i;
    int iEstado;
    String sCodigos = null;
    String sTipo = null;
    String sCodAgrup = null;

    // parametros de consulta
    ParamC1 paramC1;

    // buffers
    String sCod;
    int iNum;
    DataC1 dat = null;
    DataC1 c1;

    // objetos de datos
    CLista data = new CLista();
    Vector agrup = new Vector();
    Vector registros = new Vector();
    Vector totales = new Vector();

    //Para descripciones
    String sDesPro = "";
    String sDesProL = "";

    // modificacion jlt recupero fecha para compararla con la
    // fecha de ultima actualizacion
    // tener en cuenta no usar to_char
    Vector vfecha = new Vector();
    String fechain = new String();
    String ano = new String();
    int ind = 0;
    String mes = new String();
    String dia = new String();
    //java.sql.Timestamp fecin = null;

    /****************** APLICACION DE LORTAD ********************************/
    String user = "";
    String passwd = "";
    boolean lortad;

    // ARG: Leemos el user y el parametro lortad
    user = param.getLogin();
    lortad = param.getLortad();

    // ARG: Se comprueba si hay que aplicar la Lortad
    aplicarLortad = false;
    if (user != null) {
      aplicarLortad = (!user.trim().equals("")) && lortad;
          /****************** APLICACION DE LORTAD ********************************/

    }

    try {

      // establece la conexi�n con la base de datos
      con = openConnection();
      /*
           if (aplicarLortad)
           {
         // Se obtiene la clave de acceso del usuario que se ha conectado
         passwd = sapp.Funciones.getPassword(con, st, rs, user);
         closeConnection(con);
         con=null;
         // Se abre una nueva conexion para el usuario
         con = openConnection(user, passwd);
       }
       */

      // Se crea un objeto RegistroConsultas para aplicar la Lortad
      registroConsultas = new RegistroConsultas(con, aplicarLortad, user);

      con.setAutoCommit(true);

      paramC1 = (ParamC1) param.firstElement();

      // modificacion jlt
      // no sacamos enfermedades que est�n dadas de baja
      // y que la fecha de baja (fc_ultact) sea menor que la
      // fecha de inicio de la consulta
      vfecha = (Vector) param.elementAt(1);
      fechain = (String) vfecha.firstElement();
      ind = fechain.indexOf("/");
      dia = fechain.substring(0, ind);
      fechain = fechain.substring(ind + 1);
      ind = 0;
      ind = fechain.indexOf("/");
      mes = fechain.substring(0, ind);
      ano = fechain.substring(ind + 1);

      fechain = ano + mes + dia;

      //fechain = ((String)vfecha.firstElement()).trim() + " 00:00:00";
      //fecin  = (java.sql.Timestamp)cadena_a_timestamp(fechain);
      //.setTimestamp(4,fec);

      // calcula la agrupacion
      //st = con.prepareStatement("select a.cd_enfcie, a.cd_tvigi, b.ds_proceso from sive_enferedo a, sive_procesos b where a.it_baja<>'S' and a.cd_enfcie = b.cd_denfcie)");
      //AIC order by....

      // modificacion jlt
      // se quita la condici�n de baja
      //st = con.prepareStatement("select a.cd_enfcie, a.cd_tvigi, b.ds_proceso, b.dsl_proceso from sive_enferedo a, sive_procesos b where a.it_baja<>'S' and a.cd_enfcie = b.cd_enfcie order by b.ds_proceso ");
      //st = con.prepareStatement("select a.cd_enfcie, a.cd_tvigi, b.ds_proceso, b.dsl_proceso from sive_enferedo a, sive_procesos b where a.cd_enfcie = b.cd_enfcie order by b.ds_proceso ");

      st = con.prepareStatement("select a.cd_enfcie, a.cd_tvigi, b.ds_proceso, b.dsl_proceso from sive_enferedo a, sive_procesos b "
                                + " where (a.it_baja<>'S' or (a.it_baja = 'S' and to_char(fc_ultact, 'YYYYMMDD') > ? )) "
                                +
          " and a.cd_enfcie = b.cd_enfcie order by b.ds_proceso ");

      st.setString(1, fechain);

      //st.setDate(1,new java.sql.Date(d.getTime()));
      //new java.sql.Date(d.getTime())
      rs = st.executeQuery();
      // se desplaza ha la pagina solicitada
      for (i = 0; i < paramC1.iPagina * DBServlet.maxSIZE; i++) {
        rs.next();

      }
      i = 1;
      iEstado = CLista.listaVACIA;
      while (rs.next()) {

        // control de tama�o
        if ( (i > DBServlet.maxSIZE) && (! (paramC1.bInformeCompleto))) {
          iEstado = CLista.listaINCOMPLETA;
          break;
        }

        // control de estado
        if (iEstado == CLista.listaVACIA) {
          iEstado = CLista.listaLLENA;

          //Recogemos datos agrupacion
        }
        agrup.addElement(rs.getString("CD_ENFCIE"));
        agrup.addElement(rs.getString("CD_TVIGI"));

        sDesPro = rs.getString("DS_PROCESO");
        sDesProL = rs.getString("DSL_PROCESO");

        // obtiene la descripcion auxiliar en funci�n del idioma
        if (param.getIdioma() != CApp.idiomaPORDEFECTO) {
          if (sDesProL != null) {
            sDesPro = sDesProL;
          }
        }
        registros.addElement(new DataC1(sDesPro, 0));

//E      //#// System_out.println("SrvConsEdoPer:  un regist");

        i++;
      } //while que recorre el ResultSet

      rs.close();
      rs = null;
      st.close();
      st = null;

      DataC1 nada = null;
      for (int s = 0; s < registros.size(); s++) {
        nada = (DataC1) registros.elementAt(s);
        // System_out.println("SrvConsEdoPer: proceso " + nada.DS_PROCESO + " casos " +  nada.CASOS);
      }

      /*
           for (int s=0; s<agrup.size(); s++){
        // System_out.println("SrvConsEdoPer: agrup " + (String) agrup.elementAt(s));
           }
       */

      // recupera el conteo de casos
      if (agrup.size() > 0) {

//E      //# // System_out.println("SrvConsEdoPer: todavia quedan enfermedades " + registros.size() );
//E      //# // System_out.println("SrvConsEdoPer: opmode " + Integer.toString(opmode));
        // modos de operaci�n
        switch (opmode) {

          // consulta sobre resumen edo
          case erwCASOS_EDO_RESUMEN:

// System_out.println("SrvConsEdoPer: CASOS_EDO_RESUMEN ");
            // query
            query =
                "select CD_ENFCIE, SUM(NM_CASOS) from SIVE_RESUMEN_EDOS where ";
            where = "cd_enfcie in (";
            ////# // System_out.println("*+Antes del for");
            for (i = 0; i < agrup.size(); i++) {
              if (i > 0) {
                where = where + ",";
              }
              where = where + "'" + agrup.elementAt(i) + "'";
              i++; // para saltar el tipo de vigilancia
            }

            ////# // System_out.println("*+Despu�s del for");
            where = where + ")";

            // agrupaci�n temporal
            if (paramC1.sAnoF.equals(paramC1.sAnoI)) {
              where = where +
                  " and (CD_ANOEPI = ? and CD_SEMEPI >= ? and CD_SEMEPI <= ?)";
            }
            else {
              where = where + " and ((CD_ANOEPI = ? and CD_SEMEPI >= ?) or (CD_ANOEPI > ? and CD_ANOEPI < ?) or (CD_ANOEPI = ? and CD_SEMEPI <= ?))";

              // nivel 1
            }
            if (paramC1.sNivel_1 != null) {
              where = where + " and CD_NIVEL_1 = ?";

              // nivel 2
            }
            if (paramC1.sNivel_2 != null) {
              where = where + " and CD_NIVEL_2 = ?";

              // zbs
            }
            if (paramC1.sZBS != null) {
              where = where + " and CD_ZBS = ?";

              // provincia
            }
            if (paramC1.sProvincia != null) {
              where = where + " and CD_PROV = ?";

              // municipio
            }
            if (paramC1.sMunicipio != null) {
              where = where + " and CD_MUN = ?";

// System_out.println("SrvConsEdoPer: " + query + where + " group by CD_ENFCIE");

              // calcula los datos
            }
            st = con.prepareStatement(query + where + " group by CD_ENFCIE");
            ////# // System_out.println("*+Consulta: "+query+where);
            // parametros temporales
            int iParam = 1;

            // agrupaci�n temporal

            if (paramC1.sSemanaI.length() < 2) {
              paramC1.sSemanaI = "0" + paramC1.sSemanaI;
            }
            if (paramC1.sSemanaF.length() < 2) {
              paramC1.sSemanaF = "0" + paramC1.sSemanaF;
            }
            if (paramC1.sAnoF.equals(paramC1.sAnoI)) {
              st.setString(iParam, paramC1.sAnoI);
              iParam++;
              st.setString(iParam, paramC1.sSemanaI);
              iParam++;
              st.setString(iParam, paramC1.sSemanaF);
              iParam++;
// System_out.println("SrvConsEdoPer: " + paramC1.sAnoI + " "
//                                                 + paramC1.sSemanaI + " "
//                                                 + paramC1.sSemanaF + " ");
            }
            else {
              st.setString(iParam, paramC1.sAnoI);
              iParam++;
              st.setString(iParam, paramC1.sSemanaI);
              iParam++;
              st.setString(iParam, paramC1.sAnoI);
              iParam++;
              st.setString(iParam, paramC1.sAnoF);
              iParam++;
              st.setString(iParam, paramC1.sAnoF);
              iParam++;
              st.setString(iParam, paramC1.sSemanaF);
              iParam++;
            }

            // Parametros de los criterios de seleccion
            if (paramC1.sNivel_1 != null) {
              st.setString(iParam, paramC1.sNivel_1);
              iParam++;
            }
            if (paramC1.sNivel_2 != null) {
              st.setString(iParam, paramC1.sNivel_2);
              iParam++;
            }
            if (paramC1.sZBS != null) {
              st.setString(iParam, paramC1.sZBS);
              iParam++;
            }
            if (paramC1.sProvincia != null) {
              st.setString(iParam, paramC1.sProvincia);
              iParam++;
            }
            if (paramC1.sMunicipio != null) {
              st.setString(iParam, paramC1.sMunicipio);
              iParam++;
            }

            ////# // System_out.println("ya hemos parametrizado");
            rs = st.executeQuery();
            ////# // System_out.println("ya hemos ejecutado la query");
            // pone los datos en la matriz de salida

            while (rs.next()) {

              sCod = rs.getString(1);
              iNum = rs.getInt(2);
              i = 0; //para que busque en el vector desde el principio
              sCodAgrup = (String) agrup.elementAt(i);
              // System_out.println(" codigo " + sCod + " num " + iNum);
//E            //#

              // se mueve a la enfermedad con datos
              while ( (!sCodAgrup.equals(sCod)) && (i < agrup.size())) {
                i++;
                // System_out.println("Valor de i: "+Integer.toString(i));
                if (i < agrup.size()) {
                  sCodAgrup = (String) agrup.elementAt(i);
                }
              }

              // obtiene la linea para rellenar
              if (i < agrup.size()) {
                c1 = (DataC1) registros.elementAt( (i / 2));
                //AIC
                //c1.CASOS = iNum;
                if (c1.DS_PROCESO.equals("PAROTIDITIS")) {
                  c1.setCasos(iNum);

                }
                c1.setCasos(iNum);

                registros.setElementAt(c1, (i / 2));

              }

// System_out.println("SrvConsEdoPer: casos  del elemento del vector " + ((DataC1) registros.elementAt(i/2)).CASOS);

            }

            rs.close();
            rs = null;
            st.close();
            st = null;

            nada = null;
            for (int s = 0; s < registros.size(); s++) {
              nada = (DataC1) registros.elementAt(s);
              // System_out.println("SrvConsEdoPer: proceso " + nada.DS_PROCESO + " casos " +  nada.CASOS);
            }

            break;

            // consulta sobre datos reales
          case erwCASOS_EDO_REAL:

            int j = 0;

            ////# // System_out.println("*+Antes del for2");
            for (i = 0; i < agrup.size(); i++) {

              // obtiene la linea para rellenar
              c1 = (DataC1) registros.elementAt(j);
              j++;

              sCod = (String) agrup.elementAt(i);

              i++;
              sTipo = (String) agrup.elementAt(i);

              // EDOI
              if (sTipo.equals("I")) {
                query = "select COUNT(NM_EDO) from SIVE_EDOIND where CD_ENFCIE = ? and NM_EDO in (select NM_EDO from SIVE_NOTIF_EDOI where CD_FUENTE = 'E' and CD_E_NOTIF";
                //EDON
              }
              else {
                query =
                    "select SUM(NM_CASOS) from SIVE_EDONUM where CD_ENFCIE = ? and (CD_E_NOTIF";
              }

              // eq o cn
              if (paramC1.sEN != null) {
                query = query + " = ?)";
              }
              else {
                query = query +
                    " in (select CD_E_NOTIF from SIVE_E_NOTIF where CD_CENTRO = ?))";

                // agrupaci�n temporal
              }
              if (paramC1.sAnoF.equals(paramC1.sAnoI)) {
                query = query +
                    " and (CD_ANOEPI = ? and CD_SEMEPI >= ? and CD_SEMEPI <= ?)";
              }
              else {
                query = query + " and ((CD_ANOEPI = ? and CD_SEMEPI >= ?) or (CD_ANOEPI > ? and CD_ANOEPI < ?) or (CD_ANOEPI = ? and CD_SEMEPI <= ?))";

//E          //# // System_out.println("SrvConsEdoPer: " + query);

                // calcula los datos
              }
              st = con.prepareStatement(query);

              if (sTipo.equals("I")) {
                registroConsultas.insertarParametro(sCod);
              }
              st.setString(1, sCod);

              // Parametros de los criterios de seleccion
              if (paramC1.sEN != null) {
                if (sTipo.equals("I")) {
                  registroConsultas.insertarParametro(paramC1.sEN);
                }
                st.setString(2, paramC1.sEN);
              }
              else {
                if (sTipo.equals("I")) {
                  registroConsultas.insertarParametro(paramC1.sCN);
                }
                st.setString(2, paramC1.sCN);
              }
              // agrupaci�n temporal
              if (paramC1.sAnoF.equals(paramC1.sAnoI)) {
                if (sTipo.equals("I")) {
                  registroConsultas.insertarParametro(paramC1.sAnoI);
                }
                st.setString(3, paramC1.sAnoI);

                if (sTipo.equals("I")) {
                  registroConsultas.insertarParametro(paramC1.sSemanaI);
                }
                st.setString(4, paramC1.sSemanaI);

                if (sTipo.equals("I")) {
                  registroConsultas.insertarParametro(paramC1.sSemanaF);
                }
                st.setString(5, paramC1.sSemanaF);
              }
              else {

                if (sTipo.equals("I")) {
                  registroConsultas.insertarParametro(paramC1.sAnoI);
                }
                st.setString(3, paramC1.sAnoI);

                if (sTipo.equals("I")) {
                  registroConsultas.insertarParametro(paramC1.sSemanaI);
                }
                st.setString(4, paramC1.sSemanaI);

                if (sTipo.equals("I")) {
                  registroConsultas.insertarParametro(paramC1.sAnoI);
                }
                st.setString(5, paramC1.sAnoI);

                if (sTipo.equals("I")) {
                  registroConsultas.insertarParametro(paramC1.sAnoF);
                }
                st.setString(6, paramC1.sAnoF);

                if (sTipo.equals("I")) {
                  registroConsultas.insertarParametro(paramC1.sAnoF);
                }
                st.setString(7, paramC1.sAnoF);

                if (sTipo.equals("I")) {
                  registroConsultas.insertarParametro(paramC1.sSemanaF);
                }
                st.setString(8, paramC1.sSemanaF);
              }

              if (sTipo.equals("I")) {
                registroConsultas.registrar("SIVE_EDOIND",
                                            query,
                                            "C�d. Enf",
                                            "",
                                            "SrvConsEdoPer",
                                            true);
              }

              ////// Creo que es esto //////////
              rs = st.executeQuery();

              if (rs.next()) {
                iNum = rs.getInt(1);
                //AIC
                //c1.CASOS = iNum;
                c1.setCasos(iNum);
              }
              rs.close();
              rs = null;
              st.close();
              st = null;
            }

            ////# // System_out.println("*+Antes del for2");
            break;
        }

        // totales
        // A la linea siguiente le sobra un parentesis
        //st = con.prepareStatement("select count(cd_enfcie) from sive_enferedo where it_baja<>'S')");

        // modificacion jlt
        //st = con.prepareStatement("select count(cd_enfcie) from sive_enferedo where it_baja<>'S'");
        st = con.prepareStatement("select count(cd_enfcie) from sive_enferedo "
                                  + " where (it_baja<>'S' or (it_baja = 'S' and to_char(fc_ultact, 'YYYYMMDD') > ? ))");

        //st.setTimestamp(1,fecin);
        st.setString(1, fechain);

        rs = st.executeQuery();

        // totales
        if (rs.next()) {
          totales.addElement(new Integer(rs.getInt(1)));

        }
        rs.close();
        rs = null;
        st.close();
        st = null;
      }

      // prepara los datos
      registros.trimToSize();
      data.addElement(registros);
      totales.trimToSize();
      data.addElement(totales);
      data.setState(iEstado);
      data.trimToSize();

    }
    catch (Exception exc) {
      String dd = exc.toString();
//        // System_out.println("SrvDemSem Error: "+xc.toString());
      throw exc; //A�adido############

    }

    // Se borra de la tabla de registro de sesion de usuario
    registroConsultas.borrarSesion();
    // cierra la conexion y acaba el procedimiento doWork
    closeConnection(con);

    return data;
  }
}
