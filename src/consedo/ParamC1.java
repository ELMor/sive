package consedo;

import java.io.Serializable;

public class ParamC1
    implements Serializable {
  public String sAnoI = null;
  public String sAnoF = null;
  public String sSemanaI = null;
  public String sSemanaF = null;
  public String sProvincia = null;
  public String sMunicipio = null;
  public String sNivel_1 = null;
  public String sNivel_2 = null;
  public String sZBS = null;
  public String sEN = null;
  public String sCN = null;
  public int iPagina = 0;
  public boolean bInformeCompleto = false;

  public ParamC1() {
  }
}