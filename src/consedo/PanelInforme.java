package consedo;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

import java.awt.BorderLayout;
import java.awt.Cursor;

import EnterpriseSoft.ERW.ERW;
import EnterpriseSoft.ERW.TemplateManager;
import EnterpriseSoft.ERW.Default.DataSrcMgrs.AppDataSrc.AppDataHandler;
import EnterpriseSoft.ERWClient.ERWClient;
import capp.CApp;
import capp.CLista;
import capp.CMessage;
import eapp.EPanel;
import sapp.StubSrvBD;

public class PanelInforme
    extends EPanel {

  PanConsEdoPer pan;
  ResourceBundle res;

  final int modoNORMAL = 0;
  final int modoESPERA = 1;

  final String strSERVLET = "servlet/SrvConsEdoPer";
  final String strLOGO_CCAA = "images/ccaa.gif";

  final int erwCASOS_EDO_RESUMEN = 1;
  final int erwCASOS_EDO_REAL = 2;

  // estructuras de datos
  protected Vector vCasos;
  protected Vector vTotales;
  protected CLista lista;

  // modo de operaci�n
  protected int modoOperacion;
  protected int modoServlet;

  // conexion con el servlet
  protected StubSrvBD stub;

  // parametros de consulta
  ParamC1 paramC1 = null;

  // report
  ERW erw = null;
  ERWClient erwClient = null;
  AppDataHandler dataHandler = null;

  public PanelInforme(CApp a, PanConsEdoPer miPanel) {
    super(a);
    res = ResourceBundle.getBundle("consedo.Res" + a.getIdioma());
    pan = miPanel;
    try {
      stub = new StubSrvBD();
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  // carga la plantilla
  void jbInit() throws Exception {

    // plantilla
    erw = new ERW();
    erw.ReadTemplate(app.getCodeBase().toString() + "erw/ERWCasosEDO.erw");

    // data
    dataHandler = new AppDataHandler();
    erw.SetDataSource(dataHandler);

    // configura el cliente ERW
    erwClient = new ERWClient();

    // zona del report
    this.add(erwClient.getPreviewDevice(), BorderLayout.CENTER);

    // iniciliza el panel
    modoOperacion = modoNORMAL;
  }

  // inicializar
  public void Inicializar() {
    switch (modoOperacion) {
      case modoNORMAL:
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        this.setEnabled(true);
        break;

      case modoESPERA:
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setEnabled(false);
        break;
    }
  }

  //mostrar como un dialogo
  public void mostrar() {

    //erwClient.refreshReport(true);
    this.setModal(false);
    this.show();
    LlamadaInforme(false);
  }

  // siguiente trama
  public void MasDatos() {
    CMessage msgBox;
    CLista param = new CLista();
    Vector v;

    // jlt
    // modificaci�n para hacer que no salgan las enfermedades
    // dadas de baja seg�n la fecha desde
    Vector fecha = new Vector();
    String fechadesde = new String();
    fechadesde = (String) pan.fechasDesde.txtFecSem.getText();
    fecha.addElement( (String) fechadesde);

//E    //# System_Out.println(lista.getState());
    if (lista.getState() == CLista.listaINCOMPLETA) {

      modoOperacion = modoESPERA;
      Inicializar();
      this.setGenerandoInforme();

      try {
        PrepararInforme();

        // obtiene los datos del servidor
        paramC1.iPagina++;
        param.addElement(paramC1);
        param.setIdioma(app.getIdioma());
        param.trimToSize();
        stub.setUrl(new URL(app.getURL() + strSERVLET));

        param.setLogin(app.getLogin());
        param.setLortad(app.getParameter("LORTAD"));

        // modificacion jlt para lo de la fecha de baja
        param.addElement(fecha);

        lista = (CLista) stub.doPost(modoServlet, param);
        v = (Vector) lista.elementAt(0);
        vTotales = (Vector) lista.elementAt(1);

        for (int j = 0; j < v.size(); j++) {
          vCasos.addElement(v.elementAt(j));

          // control de registros
        }
        this.setTotalRegistros(vTotales.elementAt(0).toString());
        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // repintado
        erwClient.refreshReport(true);

      }
      catch (Exception e) {
        e.printStackTrace();
        msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();
      }

      modoOperacion = modoNORMAL;
      Inicializar();
    }
  }

  // informe comleto
  public void InformeCompleto() {
    LlamadaInforme(true);
  }

  // genera el informe
  public boolean GenerarInforme() {
    return LlamadaInforme(false);
  }

  protected boolean LlamadaInforme(boolean conTodos) {
    CMessage msgBox;
    CLista param = new CLista();
    // jlt
    // modificaci�n para hacer que no salgan las enfermedades
    // dadas de baja seg�n la fecha desde
    Vector fecha = new Vector();
    String fechadesde = new String();
    fechadesde = (String) pan.fechasDesde.txtFecSem.getText();
    fecha.addElement( (String) fechadesde);

    modoOperacion = modoESPERA;
    Inicializar();
    this.setGenerandoInforme();

    try {
      PrepararInforme();

      // obtiene los datos del servidor
      paramC1.iPagina = 0;
      paramC1.bInformeCompleto = conTodos;
      param.setIdioma(app.getIdioma());
      param.addElement(paramC1);
      param.trimToSize();
      if ( (paramC1.sCN != null) ||
          (paramC1.sEN != null)) {
        modoServlet = erwCASOS_EDO_REAL;
      }
      else {
        modoServlet = erwCASOS_EDO_RESUMEN;
      }
      stub.setUrl(new URL(app.getURL() + strSERVLET));

      param.setLogin(app.getLogin());
      param.setLortad(app.getParameter("LORTAD"));

      // modificacion jlt
      param.addElement(fecha);

      lista = (CLista) stub.doPost(modoServlet, param);

      /*SrvConsEdoPer srv = new SrvConsEdoPer();
            srv.setJdbcEnvironment("oracle.jdbc.driver.OracleDriver",
                              "jdbc:oracle:thin:@194.140.66.208:1521:ORCL",
                              "sive_desa",
                              "sive_desa");
            lista = srv.doDebug(modoServlet, param);*/

      vCasos = (Vector) lista.elementAt(0);
      vTotales = (Vector) lista.elementAt(1);

      // control de registros
      if (vTotales.size() == 0) {
        msgBox = new CMessage(this.app, CMessage.msgAVISO,
                              res.getString("msg13.Text"));
        msgBox.show();
        msgBox = null;
        this.setLimpiarStatus();

        modoOperacion = modoNORMAL;
        Inicializar();
        return false;
      }
      else {

        this.setTotalRegistros(vTotales.elementAt(0).toString());

        this.setRegistrosMostrados( (new Integer(vCasos.size())).toString());

        // establece las matrices de datos
        Vector retval = new Vector();
        retval.addElement("DS_PROCESO = DS_PROCESO");
        retval.addElement("CASOS = CASOS");
        dataHandler.RegisterTable(vCasos, "SIVE_C1", retval, null);
        erwClient.setInputProperties(erw.GetTemplateManager(), erw.getDATReader(true));

        // repintado
        //AIC
        this.pack();
        erwClient.prv_setActivePage(0);
        //erwClient.refreshReport(true);

        modoOperacion = modoNORMAL;
        Inicializar();
        return true;
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
      this.setLimpiarStatus();

      modoOperacion = modoNORMAL;
      Inicializar();
      return false;
    }

  }

  // este informe no tiene grafica
  public void MostrarGrafica() {
  }

  protected void PrepararInforme() throws Exception {
    String sBuffer;
    int iEtiqueta = 0;
    CMessage msgBox;
    /*
        String sEtiqueta[] = {"LAB008",
                               "LAB009",
                               "LAB010",
                               "LAB011",
                               "LAB012"};
     */

    String sEtiqueta[] = {
        "CRITERIO1",
        "CRITERIO2",
        "CRITERIO3",
        "CRITERIO4"};

    try {

      // plantilla
      TemplateManager tm = erw.GetTemplateManager();

      /*if(tm.IsZone("PG_FTR"))
           {
          //msgBox = new CMessage(this.app, CMessage.msgERROR, "PG_FTR es una zona");
          //msgBox.show();
          //msgBox = null;
           }
           if(tm.GetSkipPageAfterPrint("PG_FTR") || tm.GetSkipPageBeforePrint("PG_FTR"))
           {
          //msgBox = new CMessage(this.app, CMessage.msgERROR, "PG_FTR tiene salto de p�gina");
          //msgBox.show();
          //msgBox = null;
           }
           tm.SetVisible("PG_FTR",true);
           tm.SetVisible("RP_FTR",false);
           tm.SetSkipPageAfterPrint("RP_FTR",false);
           tm.SetSkipPageBeforePrint("RP_FTR",false);
           tm.SetSkipPageAfterPrint("PG_FTR",false);
           tm.SetSkipPageBeforePrint("PG_FTR",false);
       */

      // carga los logos
      tm.SetImageURL("IMA000", app.getCodeBase().toString() + strLOGO_CCAA);
      tm.SetImageURL("IMA001", app.getCodeBase().toString() + strLOGO_CCAA);

      // carga los citerios

      tm.SetLabel(sEtiqueta[iEtiqueta],
                  res.getString("msg14.Text") + paramC1.sAnoI + " " +
                  res.getString("msg15.Text") + paramC1.sSemanaI + " , " +
                  res.getString("msg16.Text") + paramC1.sAnoF + " " +
                  res.getString("msg17.Text") + paramC1.sSemanaF);
      iEtiqueta++;

      //    tm.SetLabel("LAB007", res.getString("msg18.Text") + paramC1.sAnoF + res.getString("msg19.Text") + paramC1.sSemanaF);
//    tm.SetLabel("CRIT1", EPanel.PERIODO +  paramC1.sAnoI + EPanel.SEPARADOR_ANO_SEM + paramC1.sSemanaI
//                      + EPanel.HASTA + paramC1.sAnoF + EPanel.SEPARADOR_ANO_SEM + paramC1.sSemanaF);

      if ( (paramC1.sCN != null) || (paramC1.sEN != null)) {
        // cn
        if (paramC1.sCN != null) {
          tm.SetLabel(sEtiqueta[iEtiqueta],
                      res.getString("msg20.Text") + paramC1.sCN + " " +
                      pan.txtDesCenNot.getText() + " ");
          // en
          if (paramC1.sEN != null) {
            tm.SetLabel(sEtiqueta[iEtiqueta],
                        tm.GetLabel(sEtiqueta[iEtiqueta]) +
                        res.getString("msg21.Text") + paramC1.sCN
                        + " " + pan.txtDesEquNot.getText() + " ");
          }
          iEtiqueta++;
        }
      }
//    else
//    {
      // nivel1
      if (paramC1.sNivel_1 != null) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    app.getNivel1() + ": " + paramC1.sNivel_1 + " " +
                    pan.txtDesAre.getText());
        // nivel2
        if (paramC1.sNivel_2 != null) {
          tm.SetLabel(sEtiqueta[iEtiqueta],
                      tm.GetLabel(sEtiqueta[iEtiqueta]) + " " + app.getNivel2() +
                      ": " + paramC1.sNivel_2 + " " + pan.txtDesDis.getText());
          // zbs
          if (paramC1.sZBS != null) {
            tm.SetLabel(sEtiqueta[iEtiqueta],
                        tm.GetLabel(sEtiqueta[iEtiqueta]) + " " +
//                      res.getString("msg22.Text") + ": " + paramC1.sZBS );
// ARS 26-06-01 Modificaci�n hecha para que muestre la descripci�n de la ZBS
                        res.getString("msg22.Text") + ": " + paramC1.sZBS + " " +
                        pan.txtDesZBS.getText());
          }
        }
        iEtiqueta++;
      }

      // prov
      if (paramC1.sProvincia != null) {
        tm.SetLabel(sEtiqueta[iEtiqueta],
                    res.getString("msg23.Text") + paramC1.sProvincia + " " +
                    pan.txtDesPro.getText());
        //mun
        if (paramC1.sMunicipio != null) {
          tm.SetLabel(sEtiqueta[iEtiqueta],
                      tm.GetLabel(sEtiqueta[iEtiqueta]) + " " +
                      res.getString("msg24.Text") + paramC1.sMunicipio + " " +
                      pan.txtDesMun.getText());
        }
        iEtiqueta++;
      }
      //  }

      // oculta criterios no informados
      for (int i = iEtiqueta; i < 4; i++) {
        tm.SetLabel(sEtiqueta[i], "");
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      msgBox = new CMessage(this.app, CMessage.msgERROR, e.getMessage());
      msgBox.show();
      msgBox = null;
    }

  }

}
