CREATE OR REPLACE PACKAGE SIVE_PAQ_LOG AS

   FUNCTION crear_reg_log
           ( vi_COD_APLICACION    IN  varchar2 ,
             vi_DSFICH_LOGICO     IN  varchar2 ,
             vi_CDTPOPER_LOGICO   IN  varchar2 ,
             vi_CDTPACCESO_LOGICO IN  varchar2 ,
             vi_DSTABLA_FISICA    IN  varchar2 ,
             vi_CDTPOPER_FISICA   IN  varchar2 ,
             vi_CDCLAVE           IN  varchar2 ,
             vi_CDCLAVE_VALOR     IN  varchar2 ,
             vi_CDIDENT           IN  varchar2 ,
             vi_CDIDENTDUP        IN  varchar2 ,
             vi_CDPROCESO         IN  varchar2 ,
             vi_DSPROCESO         IN  varchar2 ,
             vi_DSFICH_FISICO     IN  varchar2 default null ,
             vi_DSSQL             IN  varchar2 default null ,
             vi_DSURL             IN  varchar2 default null ,
             vi_DSPARAM_1         IN  varchar2 default null ,
             vi_DSPARAM_1_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_2         IN  varchar2 default null ,
             vi_DSPARAM_2_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_3         IN  varchar2 default null ,
             vi_DSPARAM_3_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_4         IN  varchar2 default null ,
             vi_DSPARAM_4_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_5         IN  varchar2 default null ,
             vi_DSPARAM_5_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_6         IN  varchar2 default null ,
             vi_DSPARAM_6_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_7         IN  varchar2 default null ,
             vi_DSPARAM_7_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_8         IN  varchar2 default null ,
             vi_DSPARAM_8_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_9         IN  varchar2 default null ,
             vi_DSPARAM_9_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_10        IN  varchar2 default null ,
             vi_DSPARAM_10_VALOR  IN  varchar2 default null ,
             vi_DSOBSERV          IN  varchar2 default null ) RETURN NUMBER;
   FUNCTION enviar_reg_log
          ( command VARCHAR2         ,
            timeout NUMBER DEFAULT 5 ) RETURN NUMBER;
   FUNCTION stop
          ( timeout NUMBER DEFAULT 5 ) RETURN NUMBER;


END SIVE_PAQ_LOG;
/



CREATE OR REPLACE PACKAGE BODY SIVE_PAQ_LOG AS
   FUNCTION crear_reg_log
           ( vi_COD_APLICACION    IN  varchar2 ,
             vi_DSFICH_LOGICO     IN  varchar2 ,
             vi_CDTPOPER_LOGICO   IN  varchar2 ,
             vi_CDTPACCESO_LOGICO IN  varchar2 ,
             vi_DSTABLA_FISICA    IN  varchar2 ,
             vi_CDTPOPER_FISICA   IN  varchar2 ,
             vi_CDCLAVE           IN  varchar2 ,
             vi_CDCLAVE_VALOR     IN  varchar2 ,
             vi_CDIDENT           IN  varchar2 ,
             vi_CDIDENTDUP        IN  varchar2 ,
             vi_CDPROCESO         IN  varchar2 ,
             vi_DSPROCESO         IN  varchar2 ,
             vi_DSFICH_FISICO     IN  varchar2 default null ,
             vi_DSSQL             IN  varchar2 default null ,
             vi_DSURL             IN  varchar2 default null ,
             vi_DSPARAM_1         IN  varchar2 default null ,
             vi_DSPARAM_1_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_2         IN  varchar2 default null ,
             vi_DSPARAM_2_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_3         IN  varchar2 default null ,
             vi_DSPARAM_3_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_4         IN  varchar2 default null ,
             vi_DSPARAM_4_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_5         IN  varchar2 default null ,
             vi_DSPARAM_5_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_6         IN  varchar2 default null ,
             vi_DSPARAM_6_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_7         IN  varchar2 default null ,
             vi_DSPARAM_7_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_8         IN  varchar2 default null ,
             vi_DSPARAM_8_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_9         IN  varchar2 default null ,
             vi_DSPARAM_9_VALOR   IN  varchar2 default null ,
             vi_DSPARAM_10        IN  varchar2 default null ,
             vi_DSPARAM_10_VALOR  IN  varchar2 default null ,
             vi_DSOBSERV          IN  varchar2 default null ) RETURN NUMBER AS
      --
	w_dbname   varchar2(100);
      w_DBuser   varchar2(100);
      w_OSuser   varchar2(100);
      w_machine  varchar2(100);
      w_terminal varchar2(100);
      w_sid      number;
      w_program  varchar2(100);
      w_reglog   varchar2(2500);
      w_usuario  varchar2(20);
      status     number;
   BEGIN
     -------------------------------------------------------------------
     -- Inicializar variables.                                        --
     -------------------------------------------------------------------
     -- w_reglog    := null;
     status 	  := 1;

     -------------------------------------------------------------------
     -- Obtener datos asociados a la sessi�n.                         --
     -------------------------------------------------------------------
     SELECT VALUE
       INTO w_DBNAME
       FROM sis_nombre_bd;

     --------------------------------------------------------------------
     -- Select para obtener la m�quina en la que reside la Base de datos --
     -- (icm45, hac3, etc); el TTY (si se accede por Telnet) o el nombre --
     -- del PC (si se accede desde windows); y el c�digo de proceso ORACLE --
     --------------------------------------------------------------------
     select S.Username ,
            S.OSuser   ,
            substr(S.MACHINE,1,20)  ,
            S.TERMINAL ,
            S.SID      ,
            S.PROGRAM
       into w_DBuser   ,
            w_OSuser   ,
            w_machine  ,
            w_terminal ,
            w_sid      ,
            w_program
       from sis_log_process P ,
            sis_log_session S
      where P.Addr   = S.Paddr
        and S.Audsid = USERENV('SESSIONID');

     -------------------------------------------------------------------
     -- Componer registro de Log.
     -------------------------------------------------------------------
     status := sis_func_control_sesion('C',w_usuario);
     if (status > 0) then
		return(1);
     end if;
	

     w_reglog  := '^' || w_DBNAME       || '^~^' ||
                   w_machine            || '^~^' ||
                   vi_COD_APLICACION    || '^~^' ||
                   w_usuario                 || '^~^' ||
                   to_char (SYSDATE,'YYYY-MM-DD HH24:mi:ss') || '^~^' ||
                   vi_DSFICH_LOGICO     || '^~^' ||
                   vi_CDTPOPER_LOGICO   || '^~^' ||
                   vi_CDTPACCESO_LOGICO || '^~^' ||
                   vi_DSTABLA_FISICA    || '^~^' ||
                   vi_CDTPOPER_FISICA   || '^~^' ||
                   vi_CDCLAVE           || '^~^' ||
                   vi_CDCLAVE_VALOR     || '^~^' ||
                   vi_CDIDENT           || '^~^' ||
                   vi_CDIDENTDUP        || '^~^' ||
                   w_terminal           || '^~^' ||
                   vi_CDPROCESO         || '^~^' ||
                   vi_DSPROCESO         || '^~^' ||
                   w_program            || '^~^' ||
                   ''                   || '^~^' ||
                   vi_DSFICH_FISICO     || '^~^' ||
                   ''                   || '^~^' ||
                   substr (vi_DSSQL,1,500) || '^~^' ||
                   vi_DSURL             || '^~^' ||
                   vi_DSPARAM_1         || '^~^' ||
                   vi_DSPARAM_1_VALOR   || '^~^' ||
                   vi_DSPARAM_2         || '^~^' ||
                   vi_DSPARAM_2_VALOR   || '^~^' ||
                   vi_DSPARAM_3         || '^~^' ||
                   vi_DSPARAM_3_VALOR   || '^~^' ||
                   vi_DSPARAM_4         || '^~^' ||
                   vi_DSPARAM_4_VALOR   || '^~^' ||
                   vi_DSPARAM_5         || '^~^' ||
                   vi_DSPARAM_5_VALOR   || '^~^' ||
                   vi_DSPARAM_6         || '^~^' ||
                   vi_DSPARAM_6_VALOR   || '^~^' ||
                   vi_DSPARAM_7         || '^~^' ||
                   vi_DSPARAM_7_VALOR   || '^~^' ||
                   vi_DSPARAM_8         || '^~^' ||
                   vi_DSPARAM_8_VALOR   || '^~^' ||
                   vi_DSPARAM_9         || '^~^' ||
                   vi_DSPARAM_9_VALOR   || '^~^' ||
                   vi_DSPARAM_10        || '^~^' ||
                   vi_DSPARAM_10_VALOR  || '^~^' ||
                   vi_DSOBSERV          || '^~' ;



     --
     status := enviar_reg_log(w_reglog);
   

     RETURN (status);
      --
   EXCEPTION
      WHEN OTHERS THEN RETURN (w_reglog);

   END crear_reg_log;

   FUNCTION enviar_reg_log
          ( command VARCHAR2,
            timeout NUMBER DEFAULT 10)  RETURN NUMBER IS
      status       NUMBER;
      pipe_name    VARCHAR2(30);
   BEGIN
     pipe_name := DBMS_PIPE.UNIQUE_SESSION_NAME;
     --
     --DBMS_PIPE.PACK_MESSAGE('INIREGLOG');
     --DBMS_PIPE.PACK_MESSAGE(pipe_name);
     DBMS_PIPE.PACK_MESSAGE(command);
     --DBMS_PIPE.PACK_MESSAGE('FINREGLOG');
     --
     status := DBMS_PIPE.SEND_MESSAGE('sive_pipe', timeout);
     IF status <> 0 
     THEN
        RAISE_APPLICATION_ERROR(-20010,
        'Execute_system: Error while sending. Status = ' || status);
     END IF;
     --
     RETURN status;
   END enviar_reg_log;


   FUNCTION stop (timeout NUMBER DEFAULT 10) RETURN NUMBER IS
      status NUMBER;
   BEGIN
      DBMS_PIPE.PACK_MESSAGE('STOP');
      status := DBMS_PIPE.SEND_MESSAGE('sive_pipe', timeout);
      IF status <> 0
      THEN
         RAISE_APPLICATION_ERROR(-20030,
         'stop: error while sending.  status = ' || status);
      END IF;
      --
      RETURN status;
   END stop;
END SIVE_PAQ_LOG;

/

