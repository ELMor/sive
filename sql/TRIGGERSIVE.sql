


CREATE OR REPLACE TRIGGER SIVE_AG_CAUSALES_TRIG_A_D_1 after DELETE on SIVE_AG_CAUSALES for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- DELETE trigger on SIVE_AG_CAUSALES 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_AG_CAUSALES  SIVE_AGCAUSAL_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_AGCAUSAL_BROTE
      where
        /*  %JoinFKPK(SIVE_AGCAUSAL_BROTE,:%Old," = "," and") */
        SIVE_AGCAUSAL_BROTE.CD_AGENTC = :old.CD_AGENTC and
        SIVE_AGCAUSAL_BROTE.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_AG_CAUSALES" because "SIVE_AGCAUSAL_BROTE" exists.'*/
       'no se puede borrar de SIVE_AG_CAUSALES
        porque hay registros en SIVE_AGCAUSAL_BROTE con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_AG_CAUSALES_TRIG_A_I_1 after INSERT on SIVE_AG_CAUSALES for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- INSERT trigger on SIVE_AG_CAUSALES 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_GRUPO_BROTE  SIVE_AG_CAUSALES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_GRUPO_BROTE
      where
        /* %JoinFKPK(:%New,SIVE_GRUPO_BROTE," = "," and") */
        :new.CD_GRUPO = SIVE_GRUPO_BROTE.CD_GRUPO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AG_CAUSALES" because "SIVE_GRUPO_BROTE" does not exist.'*/
          'no se puede grabar en SIVE_AG_CAUSALES
           porque no existe la clave en SIVE_GRUPO_BROTE'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_AG_CAUSALES_TRIG_A_U_1 after UPDATE on SIVE_AG_CAUSALES for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- UPDATE trigger on SIVE_AG_CAUSALES 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_AG_CAUSALES  SIVE_AGCAUSAL_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_AGENTC <> :new.CD_AGENTC or 
    :old.CD_GRUPO <> :new.CD_GRUPO
  then
    select count(*) into numrows
      from SIVE_AGCAUSAL_BROTE
      where
        /*  %JoinFKPK(SIVE_AGCAUSAL_BROTE,:%Old," = "," and") */
        SIVE_AGCAUSAL_BROTE.CD_AGENTC = :old.CD_AGENTC and
        SIVE_AGCAUSAL_BROTE.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_AG_CAUSALES" because "SIVE_AGCAUSAL_BROTE" exists.'*/
       'no se puede modificar SIVE_AG_CAUSALES
        porque hay registros en SIVE_AGCAUSAL_BROTE con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_GRUPO_BROTE  SIVE_AG_CAUSALES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_GRUPO_BROTE
    where
      /* %JoinFKPK(:%New,SIVE_GRUPO_BROTE," = "," and") */
      :new.CD_GRUPO = SIVE_GRUPO_BROTE.CD_GRUPO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AG_CAUSALES" because "SIVE_GRUPO_BROTE" does not exist.'*/
         'no se puede actualizar SIVE_AG_CAUSALES
          porque no existe la clave en SIVE_GRUPO_BROTE'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_AGCAUSAL_BROTE_TRIG_A_I_1 after INSERT on SIVE_AGCAUSAL_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- INSERT trigger on SIVE_AGCAUSAL_BROTE 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_AG_CAUSALES  SIVE_AGCAUSAL_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_AG_CAUSALES
      where
        /* %JoinFKPK(:%New,SIVE_AG_CAUSALES," = "," and") */
        :new.CD_AGENTC = SIVE_AG_CAUSALES.CD_AGENTC and
        :new.CD_GRUPO = SIVE_AG_CAUSALES.CD_GRUPO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AGCAUSAL_BROTE" because "SIVE_AG_CAUSALES" does not exist.'*/
          'no se puede grabar en SIVE_AGCAUSAL_BROTE
           porque no existe la clave en SIVE_AG_CAUSALES'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_BROTES  SIVE_AGCAUSAL_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
        :new.CD_ANO = SIVE_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AGCAUSAL_BROTE" because "SIVE_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_AGCAUSAL_BROTE
           porque no existe la clave en SIVE_BROTES'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_AGCAUSAL_BROTE_TRIG_A_U_1 after UPDATE on SIVE_AGCAUSAL_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- UPDATE trigger on SIVE_AGCAUSAL_BROTE 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_AG_CAUSALES  SIVE_AGCAUSAL_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_AG_CAUSALES
    where
      /* %JoinFKPK(:%New,SIVE_AG_CAUSALES," = "," and") */
      :new.CD_AGENTC = SIVE_AG_CAUSALES.CD_AGENTC and
      :new.CD_GRUPO = SIVE_AG_CAUSALES.CD_GRUPO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AGCAUSAL_BROTE" because "SIVE_AG_CAUSALES" does not exist.'*/
         'no se puede actualizar SIVE_AGCAUSAL_BROTE
          porque no existe la clave en SIVE_AG_CAUSALES'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_BROTES  SIVE_AGCAUSAL_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
      :new.CD_ANO = SIVE_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AGCAUSAL_BROTE" because "SIVE_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_AGCAUSAL_BROTE
          porque no existe la clave en SIVE_BROTES'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_AGENTE_TOXICO_TRIG_A_D_1 after DELETE on SIVE_AGENTE_TOXICO for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- DELETE trigger on SIVE_AGENTE_TOXICO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_AGENTE_TOXICO  SIVE_MICROTOX_MUESTRAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MICROTOX_MUESTRAS
      where
        /*  %JoinFKPK(SIVE_MICROTOX_MUESTRAS,:%Old," = "," and") */
        SIVE_MICROTOX_MUESTRAS.CD_GRUPO = :old.CD_GRUPO and
        SIVE_MICROTOX_MUESTRAS.CD_AGENTET = :old.CD_AGENTET;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_AGENTE_TOXICO" because "SIVE_MICROTOX_MUESTRAS" exists.'*/
       'no se puede borrar de SIVE_AGENTE_TOXICO
        porque hay registros en SIVE_MICROTOX_MUESTRAS con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_AGENTE_TOXICO_TRIG_A_I_1 after INSERT on SIVE_AGENTE_TOXICO for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- INSERT trigger on SIVE_AGENTE_TOXICO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_GRUPO_BROTE  SIVE_AGENTE_TOXICO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_GRUPO_BROTE
      where
        /* %JoinFKPK(:%New,SIVE_GRUPO_BROTE," = "," and") */
        :new.CD_GRUPO = SIVE_GRUPO_BROTE.CD_GRUPO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AGENTE_TOXICO" because "SIVE_GRUPO_BROTE" does not exist.'*/
          'no se puede grabar en SIVE_AGENTE_TOXICO
           porque no existe la clave en SIVE_GRUPO_BROTE'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_AGENTE_TOXICO_TRIG_A_U_1 after UPDATE on SIVE_AGENTE_TOXICO for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- UPDATE trigger on SIVE_AGENTE_TOXICO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_AGENTE_TOXICO  SIVE_MICROTOX_MUESTRAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GRUPO <> :new.CD_GRUPO or 
    :old.CD_AGENTET <> :new.CD_AGENTET
  then
    select count(*) into numrows
      from SIVE_MICROTOX_MUESTRAS
      where
        /*  %JoinFKPK(SIVE_MICROTOX_MUESTRAS,:%Old," = "," and") */
        SIVE_MICROTOX_MUESTRAS.CD_GRUPO = :old.CD_GRUPO and
        SIVE_MICROTOX_MUESTRAS.CD_AGENTET = :old.CD_AGENTET;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_AGENTE_TOXICO" because "SIVE_MICROTOX_MUESTRAS" exists.'*/
       'no se puede modificar SIVE_AGENTE_TOXICO
        porque hay registros en SIVE_MICROTOX_MUESTRAS con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_GRUPO_BROTE  SIVE_AGENTE_TOXICO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_GRUPO_BROTE
    where
      /* %JoinFKPK(:%New,SIVE_GRUPO_BROTE," = "," and") */
      :new.CD_GRUPO = SIVE_GRUPO_BROTE.CD_GRUPO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AGENTE_TOXICO" because "SIVE_GRUPO_BROTE" does not exist.'*/
         'no se puede actualizar SIVE_AGENTE_TOXICO
          porque no existe la clave en SIVE_GRUPO_BROTE'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ALARMA_TRIG_A_I_1 after INSERT on SIVE_ALARMA for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- INSERT trigger on SIVE_ALARMA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_SEMANA_EPI R/287 SIVE_ALARMA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEMANA_EPI
      where
        /* %JoinFKPK(:%New,SIVE_SEMANA_EPI," = "," and") */
        :new.CD_ANOEPI = SIVE_SEMANA_EPI.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_SEMANA_EPI.CD_SEMEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALARMA" because "SIVE_SEMANA_EPI" does not exist.'*/
          'no se puede grabar en SIVE_ALARMA
           porque no existe la clave en SIVE_SEMANA_EPI'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_IND_ANO R/266 SIVE_ALARMA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_IND_ANO
      where
        /* %JoinFKPK(:%New,SIVE_IND_ANO," = "," and") */
        :new.CD_INDALAR = SIVE_IND_ANO.CD_INDALAR and
        :new.CD_ANOEPI = SIVE_IND_ANO.CD_ANOEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALARMA" because "SIVE_IND_ANO" does not exist.'*/
          'no se puede grabar en SIVE_ALARMA
           porque no existe la clave en SIVE_IND_ANO'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ALARMA_TRIG_A_U_1 after UPDATE on SIVE_ALARMA for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- UPDATE trigger on SIVE_ALARMA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_SEMANA_EPI R/287 SIVE_ALARMA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEMANA_EPI
    where
      /* %JoinFKPK(:%New,SIVE_SEMANA_EPI," = "," and") */
      :new.CD_ANOEPI = SIVE_SEMANA_EPI.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_SEMANA_EPI.CD_SEMEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALARMA" because "SIVE_SEMANA_EPI" does not exist.'*/
         'no se puede actualizar SIVE_ALARMA
          porque no existe la clave en SIVE_SEMANA_EPI'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_IND_ANO R/266 SIVE_ALARMA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_IND_ANO
    where
      /* %JoinFKPK(:%New,SIVE_IND_ANO," = "," and") */
      :new.CD_INDALAR = SIVE_IND_ANO.CD_INDALAR and
      :new.CD_ANOEPI = SIVE_IND_ANO.CD_ANOEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALARMA" because "SIVE_IND_ANO" does not exist.'*/
         'no se puede actualizar SIVE_ALARMA
          porque no existe la clave en SIVE_IND_ANO'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ALARMA_RMC_TRIG_A_I_1 after INSERT on SIVE_ALARMA_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- INSERT trigger on SIVE_ALARMA_RMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_SEMANA_EPI_RMC  SIVE_ALARMA_RMC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEMANA_EPI_RMC
      where
        /* %JoinFKPK(:%New,SIVE_SEMANA_EPI_RMC," = "," and") */
        :new.CD_ANOEPI = SIVE_SEMANA_EPI_RMC.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_SEMANA_EPI_RMC.CD_SEMEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALARMA_RMC" because "SIVE_SEMANA_EPI_RMC" does not exist.'*/
          'no se puede grabar en SIVE_ALARMA_RMC
           porque no existe la clave en SIVE_SEMANA_EPI_RMC'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_IND_ALARRMC_ANO  SIVE_ALARMA_RMC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_IND_ALARRMC_ANO
      where
        /* %JoinFKPK(:%New,SIVE_IND_ALARRMC_ANO," = "," and") */
        :new.CD_INDALAR = SIVE_IND_ALARRMC_ANO.CD_INDALAR and
        :new.CD_ANOEPI = SIVE_IND_ALARRMC_ANO.CD_ANOEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALARMA_RMC" because "SIVE_IND_ALARRMC_ANO" does not exist.'*/
          'no se puede grabar en SIVE_ALARMA_RMC
           porque no existe la clave en SIVE_IND_ALARRMC_ANO'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ALARMA_RMC_TRIG_A_U_1 after UPDATE on SIVE_ALARMA_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- UPDATE trigger on SIVE_ALARMA_RMC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_SEMANA_EPI_RMC  SIVE_ALARMA_RMC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEMANA_EPI_RMC
    where
      /* %JoinFKPK(:%New,SIVE_SEMANA_EPI_RMC," = "," and") */
      :new.CD_ANOEPI = SIVE_SEMANA_EPI_RMC.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_SEMANA_EPI_RMC.CD_SEMEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALARMA_RMC" because "SIVE_SEMANA_EPI_RMC" does not exist.'*/
         'no se puede actualizar SIVE_ALARMA_RMC
          porque no existe la clave en SIVE_SEMANA_EPI_RMC'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_IND_ALARRMC_ANO  SIVE_ALARMA_RMC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_IND_ALARRMC_ANO
    where
      /* %JoinFKPK(:%New,SIVE_IND_ALARRMC_ANO," = "," and") */
      :new.CD_INDALAR = SIVE_IND_ALARRMC_ANO.CD_INDALAR and
      :new.CD_ANOEPI = SIVE_IND_ALARRMC_ANO.CD_ANOEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALARMA_RMC" because "SIVE_IND_ALARRMC_ANO" does not exist.'*/
         'no se puede actualizar SIVE_ALARMA_RMC
          porque no existe la clave en SIVE_IND_ALARRMC_ANO'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ALERTA_ADIC_TRIG_A_I_1 after INSERT on SIVE_ALERTA_ADIC for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- INSERT trigger on SIVE_ALERTA_ADIC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_T_NOTIFICADOR  SIVE_ALERTA_ADIC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_T_NOTIFICADOR
      where
        /* %JoinFKPK(:%New,SIVE_T_NOTIFICADOR," = "," and") */
        :new.CD_TNOTIF = SIVE_T_NOTIFICADOR.CD_TNOTIF;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_ADIC" because "SIVE_T_NOTIFICADOR" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_ADIC
           porque no existe la clave en SIVE_T_NOTIFICADOR'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_MUNICIPIO  SIVE_ALERTA_ADIC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
        :new.CD_MINPROV = SIVE_MUNICIPIO.CD_PROV and
        :new.CD_MINMUN = SIVE_MUNICIPIO.CD_MUN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_MINPROV is not null and
      :new.CD_MINMUN is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_ADIC" because "SIVE_MUNICIPIO" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_ADIC
           porque no existe la clave en SIVE_MUNICIPIO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_PROVINCIA  SIVE_ALERTA_ADIC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PROVINCIA
      where
        /* %JoinFKPK(:%New,SIVE_PROVINCIA," = "," and") */
        :new.CD_MINPROV = SIVE_PROVINCIA.CD_PROV;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_MINPROV is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_ADIC" because "SIVE_PROVINCIA" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_ADIC
           porque no existe la clave en SIVE_PROVINCIA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_MUNICIPIO  SIVE_ALERTA_ADIC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
        :new.CD_NOTIFPROV = SIVE_MUNICIPIO.CD_PROV and
        :new.CD_NOTIFMUN = SIVE_MUNICIPIO.CD_MUN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NOTIFPROV is not null and
      :new.CD_NOTIFMUN is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_ADIC" because "SIVE_MUNICIPIO" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_ADIC
           porque no existe la clave en SIVE_MUNICIPIO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_PROVINCIA  SIVE_ALERTA_ADIC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PROVINCIA
      where
        /* %JoinFKPK(:%New,SIVE_PROVINCIA," = "," and") */
        :new.CD_NOTIFPROV = SIVE_PROVINCIA.CD_PROV;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NOTIFPROV is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_ADIC" because "SIVE_PROVINCIA" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_ADIC
           porque no existe la clave en SIVE_PROVINCIA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_ALERTA_BROTES  SIVE_ALERTA_ADIC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_ALERTA_BROTES," = "," and") */
        :new.CD_ANO = SIVE_ALERTA_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_ALERTA_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_ADIC" because "SIVE_ALERTA_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_ADIC
           porque no existe la clave en SIVE_ALERTA_BROTES'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ALERTA_ADIC_TRIG_A_U_1 after UPDATE on SIVE_ALERTA_ADIC for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- UPDATE trigger on SIVE_ALERTA_ADIC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_T_NOTIFICADOR  SIVE_ALERTA_ADIC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_T_NOTIFICADOR
    where
      /* %JoinFKPK(:%New,SIVE_T_NOTIFICADOR," = "," and") */
      :new.CD_TNOTIF = SIVE_T_NOTIFICADOR.CD_TNOTIF;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_ADIC" because "SIVE_T_NOTIFICADOR" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_ADIC
          porque no existe la clave en SIVE_T_NOTIFICADOR'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_MUNICIPIO  SIVE_ALERTA_ADIC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MUNICIPIO
    where
      /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
      :new.CD_MINPROV = SIVE_MUNICIPIO.CD_PROV and
      :new.CD_MINMUN = SIVE_MUNICIPIO.CD_MUN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_MINPROV is not null and
    :new.CD_MINMUN is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_ADIC" because "SIVE_MUNICIPIO" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_ADIC
          porque no existe la clave en SIVE_MUNICIPIO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_PROVINCIA  SIVE_ALERTA_ADIC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PROVINCIA
    where
      /* %JoinFKPK(:%New,SIVE_PROVINCIA," = "," and") */
      :new.CD_MINPROV = SIVE_PROVINCIA.CD_PROV;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_MINPROV is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_ADIC" because "SIVE_PROVINCIA" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_ADIC
          porque no existe la clave en SIVE_PROVINCIA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_MUNICIPIO  SIVE_ALERTA_ADIC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MUNICIPIO
    where
      /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
      :new.CD_NOTIFPROV = SIVE_MUNICIPIO.CD_PROV and
      :new.CD_NOTIFMUN = SIVE_MUNICIPIO.CD_MUN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NOTIFPROV is not null and
    :new.CD_NOTIFMUN is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_ADIC" because "SIVE_MUNICIPIO" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_ADIC
          porque no existe la clave en SIVE_MUNICIPIO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_PROVINCIA  SIVE_ALERTA_ADIC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PROVINCIA
    where
      /* %JoinFKPK(:%New,SIVE_PROVINCIA," = "," and") */
      :new.CD_NOTIFPROV = SIVE_PROVINCIA.CD_PROV;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NOTIFPROV is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_ADIC" because "SIVE_PROVINCIA" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_ADIC
          porque no existe la clave en SIVE_PROVINCIA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_ALERTA_BROTES  SIVE_ALERTA_ADIC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ALERTA_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_ALERTA_BROTES," = "," and") */
      :new.CD_ANO = SIVE_ALERTA_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_ALERTA_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_ADIC" because "SIVE_ALERTA_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_ADIC
          porque no existe la clave en SIVE_ALERTA_BROTES'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ALERTA_BROTES_TRIG_A_D_1 after DELETE on SIVE_ALERTA_BROTES for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- DELETE trigger on SIVE_ALERTA_BROTES 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_ALERTA_BROTES  SIVE_ALERTA_ADIC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_ADIC
      where
        /*  %JoinFKPK(SIVE_ALERTA_ADIC,:%Old," = "," and") */
        SIVE_ALERTA_ADIC.CD_ANO = :old.CD_ANO and
        SIVE_ALERTA_ADIC.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ALERTA_BROTES" because "SIVE_ALERTA_ADIC" exists.'*/
       'no se puede borrar de SIVE_ALERTA_BROTES
        porque hay registros en SIVE_ALERTA_ADIC con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_ALERTA_BROTES  SIVE_ALERTA_COLEC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_COLEC
      where
        /*  %JoinFKPK(SIVE_ALERTA_COLEC,:%Old," = "," and") */
        SIVE_ALERTA_COLEC.CD_ANO = :old.CD_ANO and
        SIVE_ALERTA_COLEC.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ALERTA_BROTES" because "SIVE_ALERTA_COLEC" exists.'*/
       'no se puede borrar de SIVE_ALERTA_BROTES
        porque hay registros en SIVE_ALERTA_COLEC con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_ALERTA_BROTES  SIVE_BROTES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /*  %JoinFKPK(SIVE_BROTES,:%Old," = "," and") */
        SIVE_BROTES.CD_ANO = :old.CD_ANO and
        SIVE_BROTES.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ALERTA_BROTES" because "SIVE_BROTES" exists.'*/
       'no se puede borrar de SIVE_ALERTA_BROTES
        porque hay registros en SIVE_BROTES con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_ALERTA_BROTES  SIVE_HOSPITAL_ALERT ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_HOSPITAL_ALERT
      where
        /*  %JoinFKPK(SIVE_HOSPITAL_ALERT,:%Old," = "," and") */
        SIVE_HOSPITAL_ALERT.CD_ANO = :old.CD_ANO and
        SIVE_HOSPITAL_ALERT.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ALERTA_BROTES" because "SIVE_HOSPITAL_ALERT" exists.'*/
       'no se puede borrar de SIVE_ALERTA_BROTES
        porque hay registros en SIVE_HOSPITAL_ALERT con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ALERTA_BROTES_TRIG_A_I_1 after INSERT on SIVE_ALERTA_BROTES for each row
-- ERwin Builtin Wed Oct 31 16:28:45 2001
-- INSERT trigger on SIVE_ALERTA_BROTES 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_PROVINCIA  SIVE_ALERTA_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PROVINCIA
      where
        /* %JoinFKPK(:%New,SIVE_PROVINCIA," = "," and") */
        :new.CD_LEXPROV = SIVE_PROVINCIA.CD_PROV;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_LEXPROV is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_BROTES" because "SIVE_PROVINCIA" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_BROTES
           porque no existe la clave en SIVE_PROVINCIA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_ZONA_BASICA  SIVE_ALERTA_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ZONA_BASICA
      where
        /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
        :new.CD_NIVEL_1_EX = SIVE_ZONA_BASICA.CD_NIVEL_1 and
        :new.CD_NIVEL_2_EX = SIVE_ZONA_BASICA.CD_NIVEL_2 and
        :new.CD_ZBS_EX = SIVE_ZONA_BASICA.CD_ZBS;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1_EX is not null and
      :new.CD_NIVEL_2_EX is not null and
      :new.CD_ZBS_EX is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_BROTES" because "SIVE_ZONA_BASICA" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_BROTES
           porque no existe la clave en SIVE_ZONA_BASICA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_E_NOTIF  SIVE_ALERTA_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_E_NOTIF
      where
        /* %JoinFKPK(:%New,SIVE_E_NOTIF," = "," and") */
        :new.CD_E_NOTIF = SIVE_E_NOTIF.CD_E_NOTIF;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_E_NOTIF is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_BROTES" because "SIVE_E_NOTIF" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_BROTES
           porque no existe la clave en SIVE_E_NOTIF'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_NIVEL1_S  SIVE_ALERTA_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL1_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_BROTES" because "SIVE_NIVEL1_S" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_BROTES
           porque no existe la clave en SIVE_NIVEL1_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_GRUPO_BROTE  SIVE_ALERTA_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_GRUPO_BROTE
      where
        /* %JoinFKPK(:%New,SIVE_GRUPO_BROTE," = "," and") */
        :new.CD_GRUPO = SIVE_GRUPO_BROTE.CD_GRUPO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_BROTES" because "SIVE_GRUPO_BROTE" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_BROTES
           porque no existe la clave en SIVE_GRUPO_BROTE'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_NIVEL2_S  SIVE_ALERTA_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL2_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
        :new.CD_NIVEL_1_EX = SIVE_NIVEL2_S.CD_NIVEL_1 and
        :new.CD_NIVEL_2_EX = SIVE_NIVEL2_S.CD_NIVEL_2;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1_EX is not null and
      :new.CD_NIVEL_2_EX is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_BROTES" because "SIVE_NIVEL2_S" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_BROTES
           porque no existe la clave en SIVE_NIVEL2_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_NIVEL1_S  SIVE_ALERTA_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL1_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
        :new.CD_NIVEL_1_EX = SIVE_NIVEL1_S.CD_NIVEL_1;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1_EX is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_BROTES" because "SIVE_NIVEL1_S" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_BROTES
           porque no existe la clave en SIVE_NIVEL1_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_T_SIT_ALERTA  SIVE_ALERTA_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_T_SIT_ALERTA
      where
        /* %JoinFKPK(:%New,SIVE_T_SIT_ALERTA," = "," and") */
        :new.CD_SITALERBRO = SIVE_T_SIT_ALERTA.CD_SITALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_BROTES" because "SIVE_T_SIT_ALERTA" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_BROTES
           porque no existe la clave en SIVE_T_SIT_ALERTA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
    /* SIVE_MUNICIPIO  SIVE_ALERTA_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
        :new.CD_LEXPROV = SIVE_MUNICIPIO.CD_PROV and
        :new.CD_LEXMUN = SIVE_MUNICIPIO.CD_MUN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_LEXPROV is not null and
      :new.CD_LEXMUN is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_BROTES" because "SIVE_MUNICIPIO" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_BROTES
           porque no existe la clave en SIVE_MUNICIPIO'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:45 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ALERTA_BROTES_TRIG_A_U_1 after UPDATE on SIVE_ALERTA_BROTES for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- UPDATE trigger on SIVE_ALERTA_BROTES 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_ALERTA_BROTES  SIVE_ALERTA_ADIC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_ALERTA_ADIC
      where
        /*  %JoinFKPK(SIVE_ALERTA_ADIC,:%Old," = "," and") */
        SIVE_ALERTA_ADIC.CD_ANO = :old.CD_ANO and
        SIVE_ALERTA_ADIC.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ALERTA_BROTES" because "SIVE_ALERTA_ADIC" exists.'*/
       'no se puede modificar SIVE_ALERTA_BROTES
        porque hay registros en SIVE_ALERTA_ADIC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_ALERTA_BROTES  SIVE_ALERTA_COLEC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_ALERTA_COLEC
      where
        /*  %JoinFKPK(SIVE_ALERTA_COLEC,:%Old," = "," and") */
        SIVE_ALERTA_COLEC.CD_ANO = :old.CD_ANO and
        SIVE_ALERTA_COLEC.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ALERTA_BROTES" because "SIVE_ALERTA_COLEC" exists.'*/
       'no se puede modificar SIVE_ALERTA_BROTES
        porque hay registros en SIVE_ALERTA_COLEC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_ALERTA_BROTES  SIVE_BROTES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_BROTES
      where
        /*  %JoinFKPK(SIVE_BROTES,:%Old," = "," and") */
        SIVE_BROTES.CD_ANO = :old.CD_ANO and
        SIVE_BROTES.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ALERTA_BROTES" because "SIVE_BROTES" exists.'*/
       'no se puede modificar SIVE_ALERTA_BROTES
        porque hay registros en SIVE_BROTES con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_ALERTA_BROTES  SIVE_HOSPITAL_ALERT ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_HOSPITAL_ALERT
      where
        /*  %JoinFKPK(SIVE_HOSPITAL_ALERT,:%Old," = "," and") */
        SIVE_HOSPITAL_ALERT.CD_ANO = :old.CD_ANO and
        SIVE_HOSPITAL_ALERT.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ALERTA_BROTES" because "SIVE_HOSPITAL_ALERT" exists.'*/
       'no se puede modificar SIVE_ALERTA_BROTES
        porque hay registros en SIVE_HOSPITAL_ALERT con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_PROVINCIA  SIVE_ALERTA_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PROVINCIA
    where
      /* %JoinFKPK(:%New,SIVE_PROVINCIA," = "," and") */
      :new.CD_LEXPROV = SIVE_PROVINCIA.CD_PROV;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_LEXPROV is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_BROTES" because "SIVE_PROVINCIA" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_BROTES
          porque no existe la clave en SIVE_PROVINCIA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_ZONA_BASICA  SIVE_ALERTA_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ZONA_BASICA
    where
      /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
      :new.CD_NIVEL_1_EX = SIVE_ZONA_BASICA.CD_NIVEL_1 and
      :new.CD_NIVEL_2_EX = SIVE_ZONA_BASICA.CD_NIVEL_2 and
      :new.CD_ZBS_EX = SIVE_ZONA_BASICA.CD_ZBS;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1_EX is not null and
    :new.CD_NIVEL_2_EX is not null and
    :new.CD_ZBS_EX is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_BROTES" because "SIVE_ZONA_BASICA" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_BROTES
          porque no existe la clave en SIVE_ZONA_BASICA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_E_NOTIF  SIVE_ALERTA_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_E_NOTIF
    where
      /* %JoinFKPK(:%New,SIVE_E_NOTIF," = "," and") */
      :new.CD_E_NOTIF = SIVE_E_NOTIF.CD_E_NOTIF;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_E_NOTIF is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_BROTES" because "SIVE_E_NOTIF" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_BROTES
          porque no existe la clave en SIVE_E_NOTIF'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:45 2001 */
  /* SIVE_NIVEL1_S  SIVE_ALERTA_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL1_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_BROTES" because "SIVE_NIVEL1_S" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_BROTES
          porque no existe la clave en SIVE_NIVEL1_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_GRUPO_BROTE  SIVE_ALERTA_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_GRUPO_BROTE
    where
      /* %JoinFKPK(:%New,SIVE_GRUPO_BROTE," = "," and") */
      :new.CD_GRUPO = SIVE_GRUPO_BROTE.CD_GRUPO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_BROTES" because "SIVE_GRUPO_BROTE" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_BROTES
          porque no existe la clave en SIVE_GRUPO_BROTE'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_NIVEL2_S  SIVE_ALERTA_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL2_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
      :new.CD_NIVEL_1_EX = SIVE_NIVEL2_S.CD_NIVEL_1 and
      :new.CD_NIVEL_2_EX = SIVE_NIVEL2_S.CD_NIVEL_2;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1_EX is not null and
    :new.CD_NIVEL_2_EX is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_BROTES" because "SIVE_NIVEL2_S" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_BROTES
          porque no existe la clave en SIVE_NIVEL2_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_NIVEL1_S  SIVE_ALERTA_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL1_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
      :new.CD_NIVEL_1_EX = SIVE_NIVEL1_S.CD_NIVEL_1;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1_EX is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_BROTES" because "SIVE_NIVEL1_S" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_BROTES
          porque no existe la clave en SIVE_NIVEL1_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_T_SIT_ALERTA  SIVE_ALERTA_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_T_SIT_ALERTA
    where
      /* %JoinFKPK(:%New,SIVE_T_SIT_ALERTA," = "," and") */
      :new.CD_SITALERBRO = SIVE_T_SIT_ALERTA.CD_SITALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_BROTES" because "SIVE_T_SIT_ALERTA" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_BROTES
          porque no existe la clave en SIVE_T_SIT_ALERTA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_MUNICIPIO  SIVE_ALERTA_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MUNICIPIO
    where
      /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
      :new.CD_LEXPROV = SIVE_MUNICIPIO.CD_PROV and
      :new.CD_LEXMUN = SIVE_MUNICIPIO.CD_MUN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_LEXPROV is not null and
    :new.CD_LEXMUN is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_BROTES" because "SIVE_MUNICIPIO" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_BROTES
          porque no existe la clave en SIVE_MUNICIPIO'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ALERTA_COLEC_TRIG_A_I_1 after INSERT on SIVE_ALERTA_COLEC for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- INSERT trigger on SIVE_ALERTA_COLEC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_MUNICIPIO  SIVE_ALERTA_COLEC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
        :new.CD_PROV = SIVE_MUNICIPIO.CD_PROV and
        :new.CD_MUN = SIVE_MUNICIPIO.CD_MUN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_PROV is not null and
      :new.CD_MUN is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_COLEC" because "SIVE_MUNICIPIO" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_COLEC
           porque no existe la clave en SIVE_MUNICIPIO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_PROVINCIA  SIVE_ALERTA_COLEC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PROVINCIA
      where
        /* %JoinFKPK(:%New,SIVE_PROVINCIA," = "," and") */
        :new.CD_PROV = SIVE_PROVINCIA.CD_PROV;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_PROV is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_COLEC" because "SIVE_PROVINCIA" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_COLEC
           porque no existe la clave en SIVE_PROVINCIA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_ALERTA_BROTES  SIVE_ALERTA_COLEC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_ALERTA_BROTES," = "," and") */
        :new.CD_ANO = SIVE_ALERTA_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_ALERTA_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALERTA_COLEC" because "SIVE_ALERTA_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_ALERTA_COLEC
           porque no existe la clave en SIVE_ALERTA_BROTES'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ALERTA_COLEC_TRIG_A_U_1 after UPDATE on SIVE_ALERTA_COLEC for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- UPDATE trigger on SIVE_ALERTA_COLEC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_MUNICIPIO  SIVE_ALERTA_COLEC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MUNICIPIO
    where
      /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
      :new.CD_PROV = SIVE_MUNICIPIO.CD_PROV and
      :new.CD_MUN = SIVE_MUNICIPIO.CD_MUN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_PROV is not null and
    :new.CD_MUN is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_COLEC" because "SIVE_MUNICIPIO" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_COLEC
          porque no existe la clave en SIVE_MUNICIPIO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_PROVINCIA  SIVE_ALERTA_COLEC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PROVINCIA
    where
      /* %JoinFKPK(:%New,SIVE_PROVINCIA," = "," and") */
      :new.CD_PROV = SIVE_PROVINCIA.CD_PROV;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_PROV is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_COLEC" because "SIVE_PROVINCIA" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_COLEC
          porque no existe la clave en SIVE_PROVINCIA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_ALERTA_BROTES  SIVE_ALERTA_COLEC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ALERTA_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_ALERTA_BROTES," = "," and") */
      :new.CD_ANO = SIVE_ALERTA_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_ALERTA_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALERTA_COLEC" because "SIVE_ALERTA_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_ALERTA_COLEC
          porque no existe la clave en SIVE_ALERTA_BROTES'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ALI_BROTE_TRIG_A_I_1 after INSERT on SIVE_ALI_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- INSERT trigger on SIVE_ALI_BROTE 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_MUNICIPIO R/362 SIVE_ALI_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
        :new.CD_PROV_LPREP = SIVE_MUNICIPIO.CD_PROV and
        :new.CD_MUN_LPREP = SIVE_MUNICIPIO.CD_MUN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_PROV_LPREP is not null and
      :new.CD_MUN_LPREP is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALI_BROTE" because "SIVE_MUNICIPIO" does not exist.'*/
          'no se puede grabar en SIVE_ALI_BROTE
           porque no existe la clave en SIVE_MUNICIPIO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_TRAT_PREVIO_ALI R/318 SIVE_ALI_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_TRAT_PREVIO_ALI
      where
        /* %JoinFKPK(:%New,SIVE_TRAT_PREVIO_ALI," = "," and") */
        :new.CD_TRATPREVALI = SIVE_TRAT_PREVIO_ALI.CD_TRATPREVALI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_TRATPREVALI is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALI_BROTE" because "SIVE_TRAT_PREVIO_ALI" does not exist.'*/
          'no se puede grabar en SIVE_ALI_BROTE
           porque no existe la clave en SIVE_TRAT_PREVIO_ALI'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_TALIMENTO R/317 SIVE_ALI_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_TALIMENTO
      where
        /* %JoinFKPK(:%New,SIVE_TALIMENTO," = "," and") */
        :new.CD_TALIMENTO = SIVE_TALIMENTO.CD_TALIMENTO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALI_BROTE" because "SIVE_TALIMENTO" does not exist.'*/
          'no se puede grabar en SIVE_ALI_BROTE
           porque no existe la clave en SIVE_TALIMENTO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_T_IMPLICACION R/316 SIVE_ALI_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_T_IMPLICACION
      where
        /* %JoinFKPK(:%New,SIVE_T_IMPLICACION," = "," and") */
        :new.CD_TIMPLICACION = SIVE_T_IMPLICACION.CD_TIMPLICACION;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_TIMPLICACION is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALI_BROTE" because "SIVE_T_IMPLICACION" does not exist.'*/
          'no se puede grabar en SIVE_ALI_BROTE
           porque no existe la clave en SIVE_T_IMPLICACION'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_MCOMERC_ALI R/315 SIVE_ALI_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MCOMERC_ALI
      where
        /* %JoinFKPK(:%New,SIVE_MCOMERC_ALI," = "," and") */
        :new.CD_MCOMERCALI = SIVE_MCOMERC_ALI.CD_MCOMERCALI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_MCOMERCALI is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALI_BROTE" because "SIVE_MCOMERC_ALI" does not exist.'*/
          'no se puede grabar en SIVE_ALI_BROTE
           porque no existe la clave en SIVE_MCOMERC_ALI'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_LPREP_ALI R/314 SIVE_ALI_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_LPREP_ALI
      where
        /* %JoinFKPK(:%New,SIVE_LPREP_ALI," = "," and") */
        :new.CD_LPREPALI = SIVE_LPREP_ALI.CD_LPREPALI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_LPREPALI is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALI_BROTE" because "SIVE_LPREP_ALI" does not exist.'*/
          'no se puede grabar en SIVE_ALI_BROTE
           porque no existe la clave en SIVE_LPREP_ALI'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_LCONTAMI_ALI R/313 SIVE_ALI_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_LCONTAMI_ALI
      where
        /* %JoinFKPK(:%New,SIVE_LCONTAMI_ALI," = "," and") */
        :new.CD_LCONTAMIALI = SIVE_LCONTAMI_ALI.CD_LCONTAMIALI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_LCONTAMIALI is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALI_BROTE" because "SIVE_LCONTAMI_ALI" does not exist.'*/
          'no se puede grabar en SIVE_ALI_BROTE
           porque no existe la clave en SIVE_LCONTAMI_ALI'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_LCONSUMO_ALI R/312 SIVE_ALI_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_LCONSUMO_ALI
      where
        /* %JoinFKPK(:%New,SIVE_LCONSUMO_ALI," = "," and") */
        :new.CD_CONSUMOALI = SIVE_LCONSUMO_ALI.CD_CONSUMOALI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_CONSUMOALI is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALI_BROTE" because "SIVE_LCONSUMO_ALI" does not exist.'*/
          'no se puede grabar en SIVE_ALI_BROTE
           porque no existe la clave en SIVE_LCONSUMO_ALI'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_F_INGERIR_ALI R/311 SIVE_ALI_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_F_INGERIR_ALI
      where
        /* %JoinFKPK(:%New,SIVE_F_INGERIR_ALI," = "," and") */
        :new.CD_FINGERIRALI = SIVE_F_INGERIR_ALI.CD_FINGERIRALI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_FINGERIRALI is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALI_BROTE" because "SIVE_F_INGERIR_ALI" does not exist.'*/
          'no se puede grabar en SIVE_ALI_BROTE
           porque no existe la clave en SIVE_F_INGERIR_ALI'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BROTES R/288 SIVE_ALI_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
        :new.CD_ANO = SIVE_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ALI_BROTE" because "SIVE_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_ALI_BROTE
           porque no existe la clave en SIVE_BROTES'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ALI_BROTE_TRIG_A_U_1 after UPDATE on SIVE_ALI_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- UPDATE trigger on SIVE_ALI_BROTE 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_MUNICIPIO R/362 SIVE_ALI_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MUNICIPIO
    where
      /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
      :new.CD_PROV_LPREP = SIVE_MUNICIPIO.CD_PROV and
      :new.CD_MUN_LPREP = SIVE_MUNICIPIO.CD_MUN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_PROV_LPREP is not null and
    :new.CD_MUN_LPREP is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALI_BROTE" because "SIVE_MUNICIPIO" does not exist.'*/
         'no se puede actualizar SIVE_ALI_BROTE
          porque no existe la clave en SIVE_MUNICIPIO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_TRAT_PREVIO_ALI R/318 SIVE_ALI_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_TRAT_PREVIO_ALI
    where
      /* %JoinFKPK(:%New,SIVE_TRAT_PREVIO_ALI," = "," and") */
      :new.CD_TRATPREVALI = SIVE_TRAT_PREVIO_ALI.CD_TRATPREVALI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_TRATPREVALI is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALI_BROTE" because "SIVE_TRAT_PREVIO_ALI" does not exist.'*/
         'no se puede actualizar SIVE_ALI_BROTE
          porque no existe la clave en SIVE_TRAT_PREVIO_ALI'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_TALIMENTO R/317 SIVE_ALI_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_TALIMENTO
    where
      /* %JoinFKPK(:%New,SIVE_TALIMENTO," = "," and") */
      :new.CD_TALIMENTO = SIVE_TALIMENTO.CD_TALIMENTO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALI_BROTE" because "SIVE_TALIMENTO" does not exist.'*/
         'no se puede actualizar SIVE_ALI_BROTE
          porque no existe la clave en SIVE_TALIMENTO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_T_IMPLICACION R/316 SIVE_ALI_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_T_IMPLICACION
    where
      /* %JoinFKPK(:%New,SIVE_T_IMPLICACION," = "," and") */
      :new.CD_TIMPLICACION = SIVE_T_IMPLICACION.CD_TIMPLICACION;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_TIMPLICACION is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALI_BROTE" because "SIVE_T_IMPLICACION" does not exist.'*/
         'no se puede actualizar SIVE_ALI_BROTE
          porque no existe la clave en SIVE_T_IMPLICACION'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_MCOMERC_ALI R/315 SIVE_ALI_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MCOMERC_ALI
    where
      /* %JoinFKPK(:%New,SIVE_MCOMERC_ALI," = "," and") */
      :new.CD_MCOMERCALI = SIVE_MCOMERC_ALI.CD_MCOMERCALI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_MCOMERCALI is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALI_BROTE" because "SIVE_MCOMERC_ALI" does not exist.'*/
         'no se puede actualizar SIVE_ALI_BROTE
          porque no existe la clave en SIVE_MCOMERC_ALI'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_LPREP_ALI R/314 SIVE_ALI_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_LPREP_ALI
    where
      /* %JoinFKPK(:%New,SIVE_LPREP_ALI," = "," and") */
      :new.CD_LPREPALI = SIVE_LPREP_ALI.CD_LPREPALI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_LPREPALI is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALI_BROTE" because "SIVE_LPREP_ALI" does not exist.'*/
         'no se puede actualizar SIVE_ALI_BROTE
          porque no existe la clave en SIVE_LPREP_ALI'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_LCONTAMI_ALI R/313 SIVE_ALI_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_LCONTAMI_ALI
    where
      /* %JoinFKPK(:%New,SIVE_LCONTAMI_ALI," = "," and") */
      :new.CD_LCONTAMIALI = SIVE_LCONTAMI_ALI.CD_LCONTAMIALI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_LCONTAMIALI is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALI_BROTE" because "SIVE_LCONTAMI_ALI" does not exist.'*/
         'no se puede actualizar SIVE_ALI_BROTE
          porque no existe la clave en SIVE_LCONTAMI_ALI'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_LCONSUMO_ALI R/312 SIVE_ALI_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_LCONSUMO_ALI
    where
      /* %JoinFKPK(:%New,SIVE_LCONSUMO_ALI," = "," and") */
      :new.CD_CONSUMOALI = SIVE_LCONSUMO_ALI.CD_CONSUMOALI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_CONSUMOALI is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALI_BROTE" because "SIVE_LCONSUMO_ALI" does not exist.'*/
         'no se puede actualizar SIVE_ALI_BROTE
          porque no existe la clave en SIVE_LCONSUMO_ALI'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_F_INGERIR_ALI R/311 SIVE_ALI_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_F_INGERIR_ALI
    where
      /* %JoinFKPK(:%New,SIVE_F_INGERIR_ALI," = "," and") */
      :new.CD_FINGERIRALI = SIVE_F_INGERIR_ALI.CD_FINGERIRALI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_FINGERIRALI is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALI_BROTE" because "SIVE_F_INGERIR_ALI" does not exist.'*/
         'no se puede actualizar SIVE_ALI_BROTE
          porque no existe la clave en SIVE_F_INGERIR_ALI'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_BROTES R/288 SIVE_ALI_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
      :new.CD_ANO = SIVE_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ALI_BROTE" because "SIVE_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_ALI_BROTE
          porque no existe la clave en SIVE_BROTES'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

create trigger SIVE_ANO_EPI_TRIG_A_D
  AFTER DELETE
  on SIVE_ANO_EPI
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Tue Dec 28 10:19:06 1999 */
    /* SIVE_ANO_EPI R/130 SIVE_SEMANA_EPI ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_SEMANA_EPI
      where
        /*  %JoinFKPK(SIVE_SEMANA_EPI,:%Old," = "," and") */
        SIVE_SEMANA_EPI.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_ANO_EPI" debido a que existe en "SIVE_SEMANA_EPI".'
      );
    end if;


-- ERwin Builtin Tue Dec 28 10:19:06 1999
end;
/


create trigger SIVE_ANO_EPI_TRIG_A_U
  AFTER UPDATE
  on SIVE_ANO_EPI
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Tue Dec 28 10:19:06 1999 */
  /* SIVE_ANO_EPI R/130 SIVE_SEMANA_EPI ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANOEPI <> :new.CD_ANOEPI
  then
    select count(*) into numrows
      from SIVE_SEMANA_EPI
      where
        /*  %JoinFKPK(SIVE_SEMANA_EPI,:%Old," = "," and") */
        SIVE_SEMANA_EPI.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_ANO_EPI" debido a que existe "SIVE_SEMANA_EPI".'
      );
    end if;
  end if;


-- ERwin Builtin Tue Dec 28 10:19:06 1999
end;
/


CREATE OR REPLACE TRIGGER SIVE_ANO_EPI_RMC_TRIG_A_D_1 after DELETE on SIVE_ANO_EPI_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- DELETE trigger on SIVE_ANO_EPI_RMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_ANO_EPI_RMC  SIVE_IND_ALARRMC_ANO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_IND_ALARRMC_ANO
      where
        /*  %JoinFKPK(SIVE_IND_ALARRMC_ANO,:%Old," = "," and") */
        SIVE_IND_ALARRMC_ANO.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ANO_EPI_RMC" because "SIVE_IND_ALARRMC_ANO" exists.'*/
       'no se puede borrar de SIVE_ANO_EPI_RMC
        porque hay registros en SIVE_IND_ALARRMC_ANO con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_ANO_EPI_RMC  SIVE_POBLAC_TS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_POBLAC_TS
      where
        /*  %JoinFKPK(SIVE_POBLAC_TS,:%Old," = "," and") */
        SIVE_POBLAC_TS.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ANO_EPI_RMC" because "SIVE_POBLAC_TS" exists.'*/
       'no se puede borrar de SIVE_ANO_EPI_RMC
        porque hay registros en SIVE_POBLAC_TS con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_ANO_EPI_RMC  SIVE_RANGO_ANO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RANGO_ANO
      where
        /*  %JoinFKPK(SIVE_RANGO_ANO,:%Old," = "," and") */
        SIVE_RANGO_ANO.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ANO_EPI_RMC" because "SIVE_RANGO_ANO" exists.'*/
       'no se puede borrar de SIVE_ANO_EPI_RMC
        porque hay registros en SIVE_RANGO_ANO con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_ANO_EPI_RMC  SIVE_SEMANA_EPI_RMC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_SEMANA_EPI_RMC
      where
        /*  %JoinFKPK(SIVE_SEMANA_EPI_RMC,:%Old," = "," and") */
        SIVE_SEMANA_EPI_RMC.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ANO_EPI_RMC" because "SIVE_SEMANA_EPI_RMC" exists.'*/
       'no se puede borrar de SIVE_ANO_EPI_RMC
        porque hay registros en SIVE_SEMANA_EPI_RMC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ANO_EPI_RMC_TRIG_A_U_1 after UPDATE on SIVE_ANO_EPI_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- UPDATE trigger on SIVE_ANO_EPI_RMC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_ANO_EPI_RMC  SIVE_IND_ALARRMC_ANO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANOEPI <> :new.CD_ANOEPI
  then
    select count(*) into numrows
      from SIVE_IND_ALARRMC_ANO
      where
        /*  %JoinFKPK(SIVE_IND_ALARRMC_ANO,:%Old," = "," and") */
        SIVE_IND_ALARRMC_ANO.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ANO_EPI_RMC" because "SIVE_IND_ALARRMC_ANO" exists.'*/
       'no se puede modificar SIVE_ANO_EPI_RMC
        porque hay registros en SIVE_IND_ALARRMC_ANO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_ANO_EPI_RMC  SIVE_POBLAC_TS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANOEPI <> :new.CD_ANOEPI
  then
    select count(*) into numrows
      from SIVE_POBLAC_TS
      where
        /*  %JoinFKPK(SIVE_POBLAC_TS,:%Old," = "," and") */
        SIVE_POBLAC_TS.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ANO_EPI_RMC" because "SIVE_POBLAC_TS" exists.'*/
       'no se puede modificar SIVE_ANO_EPI_RMC
        porque hay registros en SIVE_POBLAC_TS con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_ANO_EPI_RMC  SIVE_RANGO_ANO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANOEPI <> :new.CD_ANOEPI
  then
    select count(*) into numrows
      from SIVE_RANGO_ANO
      where
        /*  %JoinFKPK(SIVE_RANGO_ANO,:%Old," = "," and") */
        SIVE_RANGO_ANO.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ANO_EPI_RMC" because "SIVE_RANGO_ANO" exists.'*/
       'no se puede modificar SIVE_ANO_EPI_RMC
        porque hay registros en SIVE_RANGO_ANO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_ANO_EPI_RMC  SIVE_SEMANA_EPI_RMC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANOEPI <> :new.CD_ANOEPI
  then
    select count(*) into numrows
      from SIVE_SEMANA_EPI_RMC
      where
        /*  %JoinFKPK(SIVE_SEMANA_EPI_RMC,:%Old," = "," and") */
        SIVE_SEMANA_EPI_RMC.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ANO_EPI_RMC" because "SIVE_SEMANA_EPI_RMC" exists.'*/
       'no se puede modificar SIVE_ANO_EPI_RMC
        porque hay registros en SIVE_SEMANA_EPI_RMC con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

create trigger SIVE_AUTORIZACI_BROTE_TRIG_A_I
  AFTER INSERT
  on SIVE_AUTORIZACIONES_BROTE
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Apr 05 11:29:42 2000 */
    /* SIVE_USUARIO_BROTE R/238 SIVE_AUTORIZACIONES_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_USUARIO_BROTE
      where
        /* %JoinFKPK(:%New,SIVE_USUARIO_BROTE," = "," and") */
        :new.CD_USUARIO = SIVE_USUARIO_BROTE.CD_USUARIO and
        :new.CD_CA = SIVE_USUARIO_BROTE.CD_CA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AUTORIZACIONES_BROTE" because "SIVE_USUARIO_BROTE" does not exist.'*/
          'no se puede grabar en SIVE_AUTORIZACIONES_BROTE
           porque no existe la clave en SIVE_USUARIO_BROTE'
      );
    end if;


-- ERwin Builtin Wed Apr 05 11:29:42 2000
end;
/


create trigger SIVE_AUTORIZACI_BROTE_TRIG_A_U
  AFTER UPDATE
  on SIVE_AUTORIZACIONES_BROTE
  
  for each row
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Apr 05 11:29:42 2000 */
  /* SIVE_USUARIO_BROTE R/238 SIVE_AUTORIZACIONES_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_USUARIO_BROTE
    where
      /* %JoinFKPK(:%New,SIVE_USUARIO_BROTE," = "," and") */
      :new.CD_USUARIO = SIVE_USUARIO_BROTE.CD_USUARIO and
      :new.CD_CA = SIVE_USUARIO_BROTE.CD_CA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AUTORIZACIONES_BROTE" because "SIVE_USUARIO_BROTE" does not exist.'*/
         'no se puede actualizar SIVE_AUTORIZACIONES_BROTE
          porque no existe la clave en SIVE_USUARIO_BROTE'
    );
  end if;


-- ERwin Builtin Wed Apr 05 11:29:42 2000
end;
/


CREATE OR REPLACE TRIGGER SIVE_AUTORIZA_PISTA_TRIG_A_I_1 after INSERT on SIVE_AUTORIZACIONES_PISTA for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- INSERT trigger on SIVE_AUTORIZACIONES_PISTA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_NIVEL1_S R/278 SIVE_AUTORIZACIONES_PISTA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL1_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AUTORIZACIONES_PISTA" because "SIVE_NIVEL1_S" does not exist.'*/
          'no se puede grabar en SIVE_AUTORIZACIONES_PISTA
           porque no existe la clave en SIVE_NIVEL1_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_NIVEL1_S  SIVE_AUTORIZACIONES_PISTA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL1_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AUTORIZACIONES_PISTA" because "SIVE_NIVEL1_S" does not exist.'*/
          'no se puede grabar en SIVE_AUTORIZACIONES_PISTA
           porque no existe la clave en SIVE_NIVEL1_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_NIVEL2_S  SIVE_AUTORIZACIONES_PISTA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL2_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      :new.CD_NIVEL_2 is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AUTORIZACIONES_PISTA" because "SIVE_NIVEL2_S" does not exist.'*/
          'no se puede grabar en SIVE_AUTORIZACIONES_PISTA
           porque no existe la clave en SIVE_NIVEL2_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_USUARIO_PISTA  SIVE_AUTORIZACIONES_PISTA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_USUARIO_PISTA
      where
        /* %JoinFKPK(:%New,SIVE_USUARIO_PISTA," = "," and") */
        :new.CD_USUARIO = SIVE_USUARIO_PISTA.CD_USUARIO and
        :new.CD_CA = SIVE_USUARIO_PISTA.CD_CA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AUTORIZACIONES_PISTA" because "SIVE_USUARIO_PISTA" does not exist.'*/
          'no se puede grabar en SIVE_AUTORIZACIONES_PISTA
           porque no existe la clave en SIVE_USUARIO_PISTA'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_AUTORIZA_PISTA_TRIG_A_U_1 after UPDATE on SIVE_AUTORIZACIONES_PISTA for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- UPDATE trigger on SIVE_AUTORIZACIONES_PISTA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_NIVEL1_S R/278 SIVE_AUTORIZACIONES_PISTA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL1_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AUTORIZACIONES_PISTA" because "SIVE_NIVEL1_S" does not exist.'*/
         'no se puede actualizar SIVE_AUTORIZACIONES_PISTA
          porque no existe la clave en SIVE_NIVEL1_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_NIVEL1_S  SIVE_AUTORIZACIONES_PISTA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL1_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AUTORIZACIONES_PISTA" because "SIVE_NIVEL1_S" does not exist.'*/
         'no se puede actualizar SIVE_AUTORIZACIONES_PISTA
          porque no existe la clave en SIVE_NIVEL1_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_NIVEL2_S  SIVE_AUTORIZACIONES_PISTA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL2_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
      :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    :new.CD_NIVEL_2 is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AUTORIZACIONES_PISTA" because "SIVE_NIVEL2_S" does not exist.'*/
         'no se puede actualizar SIVE_AUTORIZACIONES_PISTA
          porque no existe la clave en SIVE_NIVEL2_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_USUARIO_PISTA  SIVE_AUTORIZACIONES_PISTA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_USUARIO_PISTA
    where
      /* %JoinFKPK(:%New,SIVE_USUARIO_PISTA," = "," and") */
      :new.CD_USUARIO = SIVE_USUARIO_PISTA.CD_USUARIO and
      :new.CD_CA = SIVE_USUARIO_PISTA.CD_CA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AUTORIZACIONES_PISTA" because "SIVE_USUARIO_PISTA" does not exist.'*/
         'no se puede actualizar SIVE_AUTORIZACIONES_PISTA
          porque no existe la clave en SIVE_USUARIO_PISTA'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_AUTORIZAC_RTBC_TRIG_A_I_1 after INSERT on SIVE_AUTORIZACIONES_RTBC for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- INSERT trigger on SIVE_AUTORIZACIONES_RTBC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_NIVEL2_S R/297 SIVE_AUTORIZACIONES_RTBC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL2_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      :new.CD_NIVEL_2 is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AUTORIZACIONES_RTBC" because "SIVE_NIVEL2_S" does not exist.'*/
          'no se puede grabar en SIVE_AUTORIZACIONES_RTBC
           porque no existe la clave en SIVE_NIVEL2_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_NIVEL1_S R/296 SIVE_AUTORIZACIONES_RTBC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL1_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AUTORIZACIONES_RTBC" because "SIVE_NIVEL1_S" does not exist.'*/
          'no se puede grabar en SIVE_AUTORIZACIONES_RTBC
           porque no existe la clave en SIVE_NIVEL1_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_USUARIO_RTBC  SIVE_AUTORIZACIONES_RTBC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_USUARIO_RTBC
      where
        /* %JoinFKPK(:%New,SIVE_USUARIO_RTBC," = "," and") */
        :new.CD_USUARIO = SIVE_USUARIO_RTBC.CD_USUARIO and
        :new.CD_CA = SIVE_USUARIO_RTBC.CD_CA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AUTORIZACIONES_RTBC" because "SIVE_USUARIO_RTBC" does not exist.'*/
          'no se puede grabar en SIVE_AUTORIZACIONES_RTBC
           porque no existe la clave en SIVE_USUARIO_RTBC'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_AUTORIZAC_RTBC_TRIG_A_U_1 after UPDATE on SIVE_AUTORIZACIONES_RTBC for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- UPDATE trigger on SIVE_AUTORIZACIONES_RTBC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_NIVEL2_S R/297 SIVE_AUTORIZACIONES_RTBC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL2_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
      :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    :new.CD_NIVEL_2 is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AUTORIZACIONES_RTBC" because "SIVE_NIVEL2_S" does not exist.'*/
         'no se puede actualizar SIVE_AUTORIZACIONES_RTBC
          porque no existe la clave en SIVE_NIVEL2_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_NIVEL1_S R/296 SIVE_AUTORIZACIONES_RTBC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL1_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AUTORIZACIONES_RTBC" because "SIVE_NIVEL1_S" does not exist.'*/
         'no se puede actualizar SIVE_AUTORIZACIONES_RTBC
          porque no existe la clave en SIVE_NIVEL1_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_USUARIO_RTBC  SIVE_AUTORIZACIONES_RTBC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_USUARIO_RTBC
    where
      /* %JoinFKPK(:%New,SIVE_USUARIO_RTBC," = "," and") */
      :new.CD_USUARIO = SIVE_USUARIO_RTBC.CD_USUARIO and
      :new.CD_CA = SIVE_USUARIO_RTBC.CD_CA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AUTORIZACIONES_RTBC" because "SIVE_USUARIO_RTBC" does not exist.'*/
         'no se puede actualizar SIVE_AUTORIZACIONES_RTBC
          porque no existe la clave en SIVE_USUARIO_RTBC'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_AVISOS_TRIG_A_I_1 after INSERT on SIVE_AVISOS for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- INSERT trigger on SIVE_AVISOS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_MCENTINELA R/253 SIVE_AVISOS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MCENTINELA
      where
        /* %JoinFKPK(:%New,SIVE_MCENTINELA," = "," and") */
        :new.CD_PCENTI = SIVE_MCENTINELA.CD_PCENTI and
        :new.CD_MEDCEN = SIVE_MCENTINELA.CD_MEDCEN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AVISOS" because "SIVE_MCENTINELA" does not exist.'*/
          'no se puede grabar en E/332
           porque no existe la clave en SIVE_MCENTINELA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_PLANTILLA_RMC R/251 SIVE_AVISOS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PLANTILLA_RMC
      where
        /* %JoinFKPK(:%New,SIVE_PLANTILLA_RMC," = "," and") */
        :new.CD_PLANTILLA = SIVE_PLANTILLA_RMC.CD_PLANTILLA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_AVISOS" because "SIVE_PLANTILLA_RMC" does not exist.'*/
          'no se puede grabar en E/332
           porque no existe la clave en E/330'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_AVISOS_TRIG_A_U_1 after UPDATE on SIVE_AVISOS for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- UPDATE trigger on SIVE_AVISOS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_MCENTINELA R/253 SIVE_AVISOS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MCENTINELA
    where
      /* %JoinFKPK(:%New,SIVE_MCENTINELA," = "," and") */
      :new.CD_PCENTI = SIVE_MCENTINELA.CD_PCENTI and
      :new.CD_MEDCEN = SIVE_MCENTINELA.CD_MEDCEN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AVISOS" because "SIVE_MCENTINELA" does not exist.'*/
         'no se puede actualizar E/332
          porque no existe la clave en SIVE_MCENTINELA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_PLANTILLA_RMC R/251 SIVE_AVISOS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PLANTILLA_RMC
    where
      /* %JoinFKPK(:%New,SIVE_PLANTILLA_RMC," = "," and") */
      :new.CD_PLANTILLA = SIVE_PLANTILLA_RMC.CD_PLANTILLA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_AVISOS" because "SIVE_PLANTILLA_RMC" does not exist.'*/
         'no se puede actualizar E/332
          porque no existe la clave en E/330'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

create trigger SIVE_BAJASEDOIND_TRIG_A_DIU
  AFTER DELETE or INSERT or UPDATE
  on SIVE_BAJASEDOIND
  
  for each row
DECLARE 
   w_CDTPOPER_LOGICO varchar2(2); 
   w_CDTPOPER_FISICA varchar2(2); 
   w_CDCLAVE_VALOR   varchar2(20); 
   w_CDIDENT         varchar2(20); 
   w_DSPROCESO       varchar2(60); 
   w_Estado          number; 
   w_CD_ENFERMO      number; 
BEGIN 
   --------------------------------------------------------------------- 
   --                                                                 -- 
   --------------------------------------------------------------------- 
   w_DSPROCESO := 'TRIGGER SIVE_BAJASEDOIND_TRIG_A_DIU'; 
   -- 
   IF DELETING THEN 
      -- 
      w_CDTPOPER_LOGICO := 'B'; 
      w_CDTPOPER_FISICA := 'B'; 
      w_CD_ENFERMO       := :old.CD_ENFERMO; 
      -- 
   ELSIF INSERTING THEN 
         -- 
         w_CDTPOPER_LOGICO := 'A'; 
         w_CDTPOPER_FISICA := 'A'; 
         w_CD_ENFERMO       := :new.CD_ENFERMO; 
         -- 
   ELSIF UPDATING  THEN 
         -- 
         w_CDTPOPER_LOGICO := 'M'; 
         w_CDTPOPER_FISICA := 'M'; 
         w_CD_ENFERMO       := :old.CD_ENFERMO; 
         -- 
   ELSE /* La manipulaci�n es un UPDATE general */ 
         null; 
   END IF; 
   -------------------------------------------------------------------- 
   -- Obtenemos datos a registrar 
   -------------------------------------------------------------------- 
   w_CDCLAVE_VALOR := lpad (to_char (w_CD_ENFERMO),6,'0'); 
   select DS_NDOC 
     into w_CDIDENT 
     from SIVE_ENFERMO 
     where CD_ENFERMO = w_CD_ENFERMO; 
   -------------------------------------------------------------------- 
   -- Crear Registrar Registro de Log a PIPE. 
   -------------------------------------------------------------------- 
   w_Estado := dba_sive.sive_paq_log.crear_reg_log( 
                 vi_cod_aplicacion    => 'SIVE', 
                 vi_dsfich_logico     => 'SIVE_BAJASEDOIND', /* DSFICH_LOGICO: Fichero L�gico declarado en la APD */ 
                 vi_cdtpoper_logico   => w_CDTPOPER_LOGICO, 
                 vi_cdtpacceso_logico => 'I', 
                 vi_dstabla_fisica    => 'SIVE_BAJASEDOIND', 
                 vi_cdtpoper_fisica   => w_CDTPOPER_FISICA, 
                 vi_cdclave           => 'Cod. Enfermo.', /* CDCLAVE: Clave de Acceso */ 
                 vi_cdclave_valor     => w_CDCLAVE_VALOR, 
                 vi_cdident           => w_CDIDENT, 
                 vi_cdidentdup        => null,  /* CDIDENTDUP: Duplicado de Identificaci�n */ 
                 vi_cdproceso         => null,  /* CDPROCESO: C�digo del proceso */ 
                 vi_dsproceso         => w_DSPROCESO, /* DSPROCESO: Descripci�n del proceso */ 
                 vi_DSOBSERV          => null ); 
   -- 
END;
/



CREATE OR REPLACE TRIGGER SIVE_BREGISTROTBC_TRIG_A_D_1 after DELETE on SIVE_BREGISTROTBC for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- DELETE trigger on SIVE_BREGISTROTBC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BREGISTROTBC R/363 SIVE_CASOCONTAC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_CASOCONTAC
      where
        /*  %JoinFKPK(SIVE_CASOCONTAC,:%Old," = "," and") */
        SIVE_CASOCONTAC.CD_ARTBC = :old.CD_ARTBC and
        SIVE_CASOCONTAC.CD_NRTBC = :old.CD_NRTBC;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_BREGISTROTBC" because "SIVE_CASOCONTAC" exists.'*/
       'no se puede borrar de SIVE_BREGISTROTBC
        porque hay registros en SIVE_CASOCONTAC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_BREGISTROTBC_TRIG_A_U_1 after UPDATE on SIVE_BREGISTROTBC for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- UPDATE trigger on SIVE_BREGISTROTBC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
  /* SIVE_BREGISTROTBC R/363 SIVE_CASOCONTAC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ARTBC <> :new.CD_ARTBC or 
    :old.CD_NRTBC <> :new.CD_NRTBC
  then
    select count(*) into numrows
      from SIVE_CASOCONTAC
      where
        /*  %JoinFKPK(SIVE_CASOCONTAC,:%Old," = "," and") */
        SIVE_CASOCONTAC.CD_ARTBC = :old.CD_ARTBC and
        SIVE_CASOCONTAC.CD_NRTBC = :old.CD_NRTBC;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_BREGISTROTBC" because "SIVE_CASOCONTAC" exists.'*/
       'no se puede modificar SIVE_BREGISTROTBC
        porque hay registros en SIVE_CASOCONTAC con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_BROTES_TRIG_A_D_1 after DELETE on SIVE_BROTES for each row
-- ERwin Builtin Wed Oct 31 16:28:46 2001
-- DELETE trigger on SIVE_BROTES 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BROTES R/288 SIVE_ALI_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_ANO = :old.CD_ANO and
        SIVE_ALI_BROTE.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_BROTES" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede borrar de SIVE_BROTES
        porque hay registros en SIVE_ALI_BROTE con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BROTES  SIVE_AGCAUSAL_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_AGCAUSAL_BROTE
      where
        /*  %JoinFKPK(SIVE_AGCAUSAL_BROTE,:%Old," = "," and") */
        SIVE_AGCAUSAL_BROTE.CD_ANO = :old.CD_ANO and
        SIVE_AGCAUSAL_BROTE.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_BROTES" because "SIVE_AGCAUSAL_BROTE" exists.'*/
       'no se puede borrar de SIVE_BROTES
        porque hay registros en SIVE_AGCAUSAL_BROTE con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BROTES  SIVE_BROTES_DISTGEDAD ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES_DISTGEDAD
      where
        /*  %JoinFKPK(SIVE_BROTES_DISTGEDAD,:%Old," = "," and") */
        SIVE_BROTES_DISTGEDAD.CD_ANO = :old.CD_ANO and
        SIVE_BROTES_DISTGEDAD.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_BROTES" because "SIVE_BROTES_DISTGEDAD" exists.'*/
       'no se puede borrar de SIVE_BROTES
        porque hay registros en SIVE_BROTES_DISTGEDAD con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BROTES  SIVE_BROTES_FACCONT ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES_FACCONT
      where
        /*  %JoinFKPK(SIVE_BROTES_FACCONT,:%Old," = "," and") */
        SIVE_BROTES_FACCONT.CD_ANO = :old.CD_ANO and
        SIVE_BROTES_FACCONT.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_BROTES" because "SIVE_BROTES_FACCONT" exists.'*/
       'no se puede borrar de SIVE_BROTES
        porque hay registros en SIVE_BROTES_FACCONT con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BROTES  SIVE_BROTES_MED ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES_MED
      where
        /*  %JoinFKPK(SIVE_BROTES_MED,:%Old," = "," and") */
        SIVE_BROTES_MED.CD_ANO = :old.CD_ANO and
        SIVE_BROTES_MED.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_BROTES" because "SIVE_BROTES_MED" exists.'*/
       'no se puede borrar de SIVE_BROTES
        porque hay registros en SIVE_BROTES_MED con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BROTES  SIVE_FACTOR_RIESGO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_FACTOR_RIESGO
      where
        /*  %JoinFKPK(SIVE_FACTOR_RIESGO,:%Old," = "," and") */
        SIVE_FACTOR_RIESGO.CD_ANO = :old.CD_ANO and
        SIVE_FACTOR_RIESGO.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_BROTES" because "SIVE_FACTOR_RIESGO" exists.'*/
       'no se puede borrar de SIVE_BROTES
        porque hay registros en SIVE_FACTOR_RIESGO con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BROTES  SIVE_INVESTIGADOR ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_INVESTIGADOR
      where
        /*  %JoinFKPK(SIVE_INVESTIGADOR,:%Old," = "," and") */
        SIVE_INVESTIGADOR.CD_ANO = :old.CD_ANO and
        SIVE_INVESTIGADOR.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_BROTES" because "SIVE_INVESTIGADOR" exists.'*/
       'no se puede borrar de SIVE_BROTES
        porque hay registros en SIVE_INVESTIGADOR con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BROTES  SIVE_MODELO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MODELO
      where
        /*  %JoinFKPK(SIVE_MODELO,:%Old," = "," and") */
        SIVE_MODELO.CD_ANO = :old.CD_ANO and
        SIVE_MODELO.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_BROTES" because "SIVE_MODELO" exists.'*/
       'no se puede borrar de SIVE_BROTES
        porque hay registros en SIVE_MODELO con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BROTES  SIVE_MUESTRAS_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MUESTRAS_BROTE
      where
        /*  %JoinFKPK(SIVE_MUESTRAS_BROTE,:%Old," = "," and") */
        SIVE_MUESTRAS_BROTE.CD_ANO = :old.CD_ANO and
        SIVE_MUESTRAS_BROTE.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_BROTES" because "SIVE_MUESTRAS_BROTE" exists.'*/
       'no se puede borrar de SIVE_BROTES
        porque hay registros en SIVE_MUESTRAS_BROTE con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BROTES  SIVE_RESP_BROTES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESP_BROTES
      where
        /*  %JoinFKPK(SIVE_RESP_BROTES,:%Old," = "," and") */
        SIVE_RESP_BROTES.CD_ANO = :old.CD_ANO and
        SIVE_RESP_BROTES.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_BROTES" because "SIVE_RESP_BROTES" exists.'*/
       'no se puede borrar de SIVE_BROTES
        porque hay registros en SIVE_RESP_BROTES con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BROTES  SIVE_SINTOMAS_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_SINTOMAS_BROTE
      where
        /*  %JoinFKPK(SIVE_SINTOMAS_BROTE,:%Old," = "," and") */
        SIVE_SINTOMAS_BROTE.CD_ANO = :old.CD_ANO and
        SIVE_SINTOMAS_BROTE.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_BROTES" because "SIVE_SINTOMAS_BROTE" exists.'*/
       'no se puede borrar de SIVE_BROTES
        porque hay registros en SIVE_SINTOMAS_BROTE con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:46 2001 */
    /* SIVE_BROTES  SIVE_TATAQ_ENFVAC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_TATAQ_ENFVAC
      where
        /*  %JoinFKPK(SIVE_TATAQ_ENFVAC,:%Old," = "," and") */
        SIVE_TATAQ_ENFVAC.CD_ANO = :old.CD_ANO and
        SIVE_TATAQ_ENFVAC.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_BROTES" because "SIVE_TATAQ_ENFVAC" exists.'*/
       'no se puede borrar de SIVE_BROTES
        porque hay registros en SIVE_TATAQ_ENFVAC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:46 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_BROTES_TRIG_A_I_1 after INSERT on SIVE_BROTES for each row
-- ERwin Builtin Wed Oct 31 16:28:47 2001
-- INSERT trigger on SIVE_BROTES 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_ZONA_BASICA  SIVE_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ZONA_BASICA
      where
        /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
        :new.CD_NIVEL_1_LE = SIVE_ZONA_BASICA.CD_NIVEL_1 and
        :new.CD_NIVEL_2_LE = SIVE_ZONA_BASICA.CD_NIVEL_2 and
        :new.CD_ZBS_LE = SIVE_ZONA_BASICA.CD_ZBS;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1_LE is not null and
      :new.CD_NIVEL_2_LE is not null and
      :new.CD_ZBS_LE is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES" because "SIVE_ZONA_BASICA" does not exist.'*/
          'no se puede grabar en SIVE_BROTES
           porque no existe la clave en SIVE_ZONA_BASICA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_NIVEL2_S  SIVE_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL2_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
        :new.CD_NIVEL_1_LE = SIVE_NIVEL2_S.CD_NIVEL_1 and
        :new.CD_NIVEL_2_LE = SIVE_NIVEL2_S.CD_NIVEL_2;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1_LE is not null and
      :new.CD_NIVEL_2_LE is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES" because "SIVE_NIVEL2_S" does not exist.'*/
          'no se puede grabar en SIVE_BROTES
           porque no existe la clave en SIVE_NIVEL2_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_NIVEL1_S  SIVE_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL1_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
        :new.CD_NIVEL_1_LE = SIVE_NIVEL1_S.CD_NIVEL_1;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1_LE is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES" because "SIVE_NIVEL1_S" does not exist.'*/
          'no se puede grabar en SIVE_BROTES
           porque no existe la clave en SIVE_NIVEL1_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_NIVEL1_S  SIVE_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL1_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
        :new.CD_NIVEL_1_LCA = SIVE_NIVEL1_S.CD_NIVEL_1;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1_LCA is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES" because "SIVE_NIVEL1_S" does not exist.'*/
          'no se puede grabar en SIVE_BROTES
           porque no existe la clave en SIVE_NIVEL1_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_NIVEL2_S  SIVE_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL2_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
        :new.CD_NIVEL_1_LCA = SIVE_NIVEL2_S.CD_NIVEL_1 and
        :new.CD_NIVEL_2_LCA = SIVE_NIVEL2_S.CD_NIVEL_2;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1_LCA is not null and
      :new.CD_NIVEL_2_LCA is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES" because "SIVE_NIVEL2_S" does not exist.'*/
          'no se puede grabar en SIVE_BROTES
           porque no existe la clave en SIVE_NIVEL2_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_ZONA_BASICA  SIVE_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ZONA_BASICA
      where
        /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
        :new.CD_NIVEL_1_LCA = SIVE_ZONA_BASICA.CD_NIVEL_1 and
        :new.CD_NIVEL_2_LCA = SIVE_ZONA_BASICA.CD_NIVEL_2 and
        :new.CD_ZBS_LCA = SIVE_ZONA_BASICA.CD_ZBS;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1_LCA is not null and
      :new.CD_NIVEL_2_LCA is not null and
      :new.CD_ZBS_LCA is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES" because "SIVE_ZONA_BASICA" does not exist.'*/
          'no se puede grabar en SIVE_BROTES
           porque no existe la clave en SIVE_ZONA_BASICA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_MUNICIPIO  SIVE_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
        :new.CD_PROVCOL = SIVE_MUNICIPIO.CD_PROV and
        :new.CD_MUNCOL = SIVE_MUNICIPIO.CD_MUN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_PROVCOL is not null and
      :new.CD_MUNCOL is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES" because "SIVE_MUNICIPIO" does not exist.'*/
          'no se puede grabar en SIVE_BROTES
           porque no existe la clave en SIVE_MUNICIPIO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_PROVINCIA  SIVE_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PROVINCIA
      where
        /* %JoinFKPK(:%New,SIVE_PROVINCIA," = "," and") */
        :new.CD_PROVCOL = SIVE_PROVINCIA.CD_PROV;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_PROVCOL is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES" because "SIVE_PROVINCIA" does not exist.'*/
          'no se puede grabar en SIVE_BROTES
           porque no existe la clave en SIVE_PROVINCIA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_T_NOTIFICADOR  SIVE_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_T_NOTIFICADOR
      where
        /* %JoinFKPK(:%New,SIVE_T_NOTIFICADOR," = "," and") */
        :new.CD_TNOTIF = SIVE_T_NOTIFICADOR.CD_TNOTIF;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES" because "SIVE_T_NOTIFICADOR" does not exist.'*/
          'no se puede grabar en SIVE_BROTES
           porque no existe la clave en SIVE_T_NOTIFICADOR'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_MEC_TRANS  SIVE_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MEC_TRANS
      where
        /* %JoinFKPK(:%New,SIVE_MEC_TRANS," = "," and") */
        :new.CD_TRANSMIS = SIVE_MEC_TRANS.CD_TRANSMIS;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_TRANSMIS is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES" because "SIVE_MEC_TRANS" does not exist.'*/
          'no se puede grabar en SIVE_BROTES
           porque no existe la clave en SIVE_MEC_TRANS'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_TIPO_BROTE  SIVE_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_TIPO_BROTE
      where
        /* %JoinFKPK(:%New,SIVE_TIPO_BROTE," = "," and") */
        :new.CD_GRUPO = SIVE_TIPO_BROTE.CD_GRUPO and
        :new.CD_TBROTE = SIVE_TIPO_BROTE.CD_TBROTE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES" because "SIVE_TIPO_BROTE" does not exist.'*/
          'no se puede grabar en SIVE_BROTES
           porque no existe la clave en SIVE_TIPO_BROTE'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_TIPOCOL  SIVE_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_TIPOCOL
      where
        /* %JoinFKPK(:%New,SIVE_TIPOCOL," = "," and") */
        :new.CD_TIPOCOL = SIVE_TIPOCOL.CD_TIPOCOL;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES" because "SIVE_TIPOCOL" does not exist.'*/
          'no se puede grabar en SIVE_BROTES
           porque no existe la clave en SIVE_TIPOCOL'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_ALERTA_BROTES  SIVE_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_ALERTA_BROTES," = "," and") */
        :new.CD_ANO = SIVE_ALERTA_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_ALERTA_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES" because "SIVE_ALERTA_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_BROTES
           porque no existe la clave en SIVE_ALERTA_BROTES'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:47 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_BROTES_TRIG_A_U_1 after UPDATE on SIVE_BROTES for each row
-- ERwin Builtin Wed Oct 31 16:28:47 2001
-- UPDATE trigger on SIVE_BROTES 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES R/288 SIVE_ALI_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_ANO = :old.CD_ANO and
        SIVE_ALI_BROTE.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede modificar SIVE_BROTES
        porque hay registros en SIVE_ALI_BROTE con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_AGCAUSAL_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_AGCAUSAL_BROTE
      where
        /*  %JoinFKPK(SIVE_AGCAUSAL_BROTE,:%Old," = "," and") */
        SIVE_AGCAUSAL_BROTE.CD_ANO = :old.CD_ANO and
        SIVE_AGCAUSAL_BROTE.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_AGCAUSAL_BROTE" exists.'*/
       'no se puede modificar SIVE_BROTES
        porque hay registros en SIVE_AGCAUSAL_BROTE con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_BROTES_DISTGEDAD ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_BROTES_DISTGEDAD
      where
        /*  %JoinFKPK(SIVE_BROTES_DISTGEDAD,:%Old," = "," and") */
        SIVE_BROTES_DISTGEDAD.CD_ANO = :old.CD_ANO and
        SIVE_BROTES_DISTGEDAD.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_BROTES_DISTGEDAD" exists.'*/
       'no se puede modificar SIVE_BROTES
        porque hay registros en SIVE_BROTES_DISTGEDAD con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_BROTES_FACCONT ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_BROTES_FACCONT
      where
        /*  %JoinFKPK(SIVE_BROTES_FACCONT,:%Old," = "," and") */
        SIVE_BROTES_FACCONT.CD_ANO = :old.CD_ANO and
        SIVE_BROTES_FACCONT.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_BROTES_FACCONT" exists.'*/
       'no se puede modificar SIVE_BROTES
        porque hay registros en SIVE_BROTES_FACCONT con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_BROTES_MED ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_BROTES_MED
      where
        /*  %JoinFKPK(SIVE_BROTES_MED,:%Old," = "," and") */
        SIVE_BROTES_MED.CD_ANO = :old.CD_ANO and
        SIVE_BROTES_MED.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_BROTES_MED" exists.'*/
       'no se puede modificar SIVE_BROTES
        porque hay registros en SIVE_BROTES_MED con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_FACTOR_RIESGO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_FACTOR_RIESGO
      where
        /*  %JoinFKPK(SIVE_FACTOR_RIESGO,:%Old," = "," and") */
        SIVE_FACTOR_RIESGO.CD_ANO = :old.CD_ANO and
        SIVE_FACTOR_RIESGO.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_FACTOR_RIESGO" exists.'*/
       'no se puede modificar SIVE_BROTES
        porque hay registros en SIVE_FACTOR_RIESGO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_INVESTIGADOR ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_INVESTIGADOR
      where
        /*  %JoinFKPK(SIVE_INVESTIGADOR,:%Old," = "," and") */
        SIVE_INVESTIGADOR.CD_ANO = :old.CD_ANO and
        SIVE_INVESTIGADOR.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_INVESTIGADOR" exists.'*/
       'no se puede modificar SIVE_BROTES
        porque hay registros en SIVE_INVESTIGADOR con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_MODELO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_MODELO
      where
        /*  %JoinFKPK(SIVE_MODELO,:%Old," = "," and") */
        SIVE_MODELO.CD_ANO = :old.CD_ANO and
        SIVE_MODELO.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_MODELO" exists.'*/
       'no se puede modificar SIVE_BROTES
        porque hay registros en SIVE_MODELO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_MUESTRAS_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_MUESTRAS_BROTE
      where
        /*  %JoinFKPK(SIVE_MUESTRAS_BROTE,:%Old," = "," and") */
        SIVE_MUESTRAS_BROTE.CD_ANO = :old.CD_ANO and
        SIVE_MUESTRAS_BROTE.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_MUESTRAS_BROTE" exists.'*/
       'no se puede modificar SIVE_BROTES
        porque hay registros en SIVE_MUESTRAS_BROTE con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_RESP_BROTES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_RESP_BROTES
      where
        /*  %JoinFKPK(SIVE_RESP_BROTES,:%Old," = "," and") */
        SIVE_RESP_BROTES.CD_ANO = :old.CD_ANO and
        SIVE_RESP_BROTES.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_RESP_BROTES" exists.'*/
       'no se puede modificar SIVE_BROTES
        porque hay registros en SIVE_RESP_BROTES con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_SINTOMAS_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_SINTOMAS_BROTE
      where
        /*  %JoinFKPK(SIVE_SINTOMAS_BROTE,:%Old," = "," and") */
        SIVE_SINTOMAS_BROTE.CD_ANO = :old.CD_ANO and
        SIVE_SINTOMAS_BROTE.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_SINTOMAS_BROTE" exists.'*/
       'no se puede modificar SIVE_BROTES
        porque hay registros en SIVE_SINTOMAS_BROTE con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_TATAQ_ENFVAC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANO <> :new.CD_ANO or 
    :old.NM_ALERBRO <> :new.NM_ALERBRO
  then
    select count(*) into numrows
      from SIVE_TATAQ_ENFVAC
      where
        /*  %JoinFKPK(SIVE_TATAQ_ENFVAC,:%Old," = "," and") */
        SIVE_TATAQ_ENFVAC.CD_ANO = :old.CD_ANO and
        SIVE_TATAQ_ENFVAC.NM_ALERBRO = :old.NM_ALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_TATAQ_ENFVAC" exists.'*/
       'no se puede modificar SIVE_BROTES
        porque hay registros en SIVE_TATAQ_ENFVAC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_ZONA_BASICA  SIVE_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ZONA_BASICA
    where
      /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
      :new.CD_NIVEL_1_LE = SIVE_ZONA_BASICA.CD_NIVEL_1 and
      :new.CD_NIVEL_2_LE = SIVE_ZONA_BASICA.CD_NIVEL_2 and
      :new.CD_ZBS_LE = SIVE_ZONA_BASICA.CD_ZBS;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1_LE is not null and
    :new.CD_NIVEL_2_LE is not null and
    :new.CD_ZBS_LE is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_ZONA_BASICA" does not exist.'*/
         'no se puede actualizar SIVE_BROTES
          porque no existe la clave en SIVE_ZONA_BASICA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_NIVEL2_S  SIVE_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL2_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
      :new.CD_NIVEL_1_LE = SIVE_NIVEL2_S.CD_NIVEL_1 and
      :new.CD_NIVEL_2_LE = SIVE_NIVEL2_S.CD_NIVEL_2;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1_LE is not null and
    :new.CD_NIVEL_2_LE is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_NIVEL2_S" does not exist.'*/
         'no se puede actualizar SIVE_BROTES
          porque no existe la clave en SIVE_NIVEL2_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_NIVEL1_S  SIVE_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL1_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
      :new.CD_NIVEL_1_LE = SIVE_NIVEL1_S.CD_NIVEL_1;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1_LE is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_NIVEL1_S" does not exist.'*/
         'no se puede actualizar SIVE_BROTES
          porque no existe la clave en SIVE_NIVEL1_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_NIVEL1_S  SIVE_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL1_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
      :new.CD_NIVEL_1_LCA = SIVE_NIVEL1_S.CD_NIVEL_1;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1_LCA is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_NIVEL1_S" does not exist.'*/
         'no se puede actualizar SIVE_BROTES
          porque no existe la clave en SIVE_NIVEL1_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_NIVEL2_S  SIVE_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL2_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
      :new.CD_NIVEL_1_LCA = SIVE_NIVEL2_S.CD_NIVEL_1 and
      :new.CD_NIVEL_2_LCA = SIVE_NIVEL2_S.CD_NIVEL_2;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1_LCA is not null and
    :new.CD_NIVEL_2_LCA is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_NIVEL2_S" does not exist.'*/
         'no se puede actualizar SIVE_BROTES
          porque no existe la clave en SIVE_NIVEL2_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_ZONA_BASICA  SIVE_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ZONA_BASICA
    where
      /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
      :new.CD_NIVEL_1_LCA = SIVE_ZONA_BASICA.CD_NIVEL_1 and
      :new.CD_NIVEL_2_LCA = SIVE_ZONA_BASICA.CD_NIVEL_2 and
      :new.CD_ZBS_LCA = SIVE_ZONA_BASICA.CD_ZBS;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1_LCA is not null and
    :new.CD_NIVEL_2_LCA is not null and
    :new.CD_ZBS_LCA is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_ZONA_BASICA" does not exist.'*/
         'no se puede actualizar SIVE_BROTES
          porque no existe la clave en SIVE_ZONA_BASICA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_MUNICIPIO  SIVE_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MUNICIPIO
    where
      /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
      :new.CD_PROVCOL = SIVE_MUNICIPIO.CD_PROV and
      :new.CD_MUNCOL = SIVE_MUNICIPIO.CD_MUN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_PROVCOL is not null and
    :new.CD_MUNCOL is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_MUNICIPIO" does not exist.'*/
         'no se puede actualizar SIVE_BROTES
          porque no existe la clave en SIVE_MUNICIPIO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_PROVINCIA  SIVE_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PROVINCIA
    where
      /* %JoinFKPK(:%New,SIVE_PROVINCIA," = "," and") */
      :new.CD_PROVCOL = SIVE_PROVINCIA.CD_PROV;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_PROVCOL is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_PROVINCIA" does not exist.'*/
         'no se puede actualizar SIVE_BROTES
          porque no existe la clave en SIVE_PROVINCIA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_T_NOTIFICADOR  SIVE_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_T_NOTIFICADOR
    where
      /* %JoinFKPK(:%New,SIVE_T_NOTIFICADOR," = "," and") */
      :new.CD_TNOTIF = SIVE_T_NOTIFICADOR.CD_TNOTIF;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_T_NOTIFICADOR" does not exist.'*/
         'no se puede actualizar SIVE_BROTES
          porque no existe la clave en SIVE_T_NOTIFICADOR'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_MEC_TRANS  SIVE_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MEC_TRANS
    where
      /* %JoinFKPK(:%New,SIVE_MEC_TRANS," = "," and") */
      :new.CD_TRANSMIS = SIVE_MEC_TRANS.CD_TRANSMIS;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_TRANSMIS is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_MEC_TRANS" does not exist.'*/
         'no se puede actualizar SIVE_BROTES
          porque no existe la clave en SIVE_MEC_TRANS'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_TIPO_BROTE  SIVE_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_TIPO_BROTE
    where
      /* %JoinFKPK(:%New,SIVE_TIPO_BROTE," = "," and") */
      :new.CD_GRUPO = SIVE_TIPO_BROTE.CD_GRUPO and
      :new.CD_TBROTE = SIVE_TIPO_BROTE.CD_TBROTE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_TIPO_BROTE" does not exist.'*/
         'no se puede actualizar SIVE_BROTES
          porque no existe la clave en SIVE_TIPO_BROTE'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_TIPOCOL  SIVE_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_TIPOCOL
    where
      /* %JoinFKPK(:%New,SIVE_TIPOCOL," = "," and") */
      :new.CD_TIPOCOL = SIVE_TIPOCOL.CD_TIPOCOL;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_TIPOCOL" does not exist.'*/
         'no se puede actualizar SIVE_BROTES
          porque no existe la clave en SIVE_TIPOCOL'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_ALERTA_BROTES  SIVE_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ALERTA_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_ALERTA_BROTES," = "," and") */
      :new.CD_ANO = SIVE_ALERTA_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_ALERTA_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES" because "SIVE_ALERTA_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_BROTES
          porque no existe la clave en SIVE_ALERTA_BROTES'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:47 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_BROTES_DISTGED_TRIG_A_I_1 after INSERT on SIVE_BROTES_DISTGEDAD for each row
-- ERwin Builtin Wed Oct 31 16:28:47 2001
-- INSERT trigger on SIVE_BROTES_DISTGEDAD 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_BROTES  SIVE_BROTES_DISTGEDAD ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
        :new.CD_ANO = SIVE_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES_DISTGEDAD" because "SIVE_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_BROTES_DISTGEDAD
           porque no existe la clave en SIVE_BROTES'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_DISTR_GEDAD  SIVE_BROTES_DISTGEDAD ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_DISTR_GEDAD
      where
        /* %JoinFKPK(:%New,SIVE_DISTR_GEDAD," = "," and") */
        :new.CD_GEDAD = SIVE_DISTR_GEDAD.CD_GEDAD and
        :new.DIST_EDAD_I = SIVE_DISTR_GEDAD.DIST_EDAD_I and
        :new.DIST_EDAD_F = SIVE_DISTR_GEDAD.DIST_EDAD_F;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_GEDAD is not null and
      :new.DIST_EDAD_I is not null and
      :new.DIST_EDAD_F is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES_DISTGEDAD" because "SIVE_DISTR_GEDAD" does not exist.'*/
          'no se puede grabar en SIVE_BROTES_DISTGEDAD
           porque no existe la clave en SIVE_DISTR_GEDAD'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:47 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_BROTES_DISTGED_TRIG_A_U_1 after UPDATE on SIVE_BROTES_DISTGEDAD for each row
-- ERwin Builtin Wed Oct 31 16:28:47 2001
-- UPDATE trigger on SIVE_BROTES_DISTGEDAD 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_BROTES_DISTGEDAD ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
      :new.CD_ANO = SIVE_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES_DISTGEDAD" because "SIVE_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_BROTES_DISTGEDAD
          porque no existe la clave en SIVE_BROTES'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_DISTR_GEDAD  SIVE_BROTES_DISTGEDAD ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_DISTR_GEDAD
    where
      /* %JoinFKPK(:%New,SIVE_DISTR_GEDAD," = "," and") */
      :new.CD_GEDAD = SIVE_DISTR_GEDAD.CD_GEDAD and
      :new.DIST_EDAD_I = SIVE_DISTR_GEDAD.DIST_EDAD_I and
      :new.DIST_EDAD_F = SIVE_DISTR_GEDAD.DIST_EDAD_F;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_GEDAD is not null and
    :new.DIST_EDAD_I is not null and
    :new.DIST_EDAD_F is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES_DISTGEDAD" because "SIVE_DISTR_GEDAD" does not exist.'*/
         'no se puede actualizar SIVE_BROTES_DISTGEDAD
          porque no existe la clave en SIVE_DISTR_GEDAD'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:47 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_BROTES_FACCONT_TRIG_A_I_1 after INSERT on SIVE_BROTES_FACCONT for each row
-- ERwin Builtin Wed Oct 31 16:28:47 2001
-- INSERT trigger on SIVE_BROTES_FACCONT 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_BROTES  SIVE_BROTES_FACCONT ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
        :new.CD_ANO = SIVE_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES_FACCONT" because "SIVE_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_BROTES_FACCONT
           porque no existe la clave en SIVE_BROTES'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_FACT_CONTRIB  SIVE_BROTES_FACCONT ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_FACT_CONTRIB
      where
        /* %JoinFKPK(:%New,SIVE_FACT_CONTRIB," = "," and") */
        :new.CD_FACCONT = SIVE_FACT_CONTRIB.CD_FACCONT and
        :new.CD_GRUPO = SIVE_FACT_CONTRIB.CD_GRUPO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES_FACCONT" because "SIVE_FACT_CONTRIB" does not exist.'*/
          'no se puede grabar en SIVE_BROTES_FACCONT
           porque no existe la clave en SIVE_FACT_CONTRIB'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:47 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_BROTES_FACCONT_TRIG_A_U_1 after UPDATE on SIVE_BROTES_FACCONT for each row
-- ERwin Builtin Wed Oct 31 16:28:47 2001
-- UPDATE trigger on SIVE_BROTES_FACCONT 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_BROTES_FACCONT ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
      :new.CD_ANO = SIVE_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES_FACCONT" because "SIVE_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_BROTES_FACCONT
          porque no existe la clave en SIVE_BROTES'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_FACT_CONTRIB  SIVE_BROTES_FACCONT ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_FACT_CONTRIB
    where
      /* %JoinFKPK(:%New,SIVE_FACT_CONTRIB," = "," and") */
      :new.CD_FACCONT = SIVE_FACT_CONTRIB.CD_FACCONT and
      :new.CD_GRUPO = SIVE_FACT_CONTRIB.CD_GRUPO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES_FACCONT" because "SIVE_FACT_CONTRIB" does not exist.'*/
         'no se puede actualizar SIVE_BROTES_FACCONT
          porque no existe la clave en SIVE_FACT_CONTRIB'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:47 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_BROTES_MED_TRIG_A_I_1 after INSERT on SIVE_BROTES_MED for each row
-- ERwin Builtin Wed Oct 31 16:28:47 2001
-- INSERT trigger on SIVE_BROTES_MED 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_BROTES  SIVE_BROTES_MED ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
        :new.CD_ANO = SIVE_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES_MED" because "SIVE_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_BROTES_MED
           porque no existe la clave en SIVE_BROTES'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_MEDIDAS  SIVE_BROTES_MED ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MEDIDAS
      where
        /* %JoinFKPK(:%New,SIVE_MEDIDAS," = "," and") */
        :new.CD_MEDIDA = SIVE_MEDIDAS.CD_MEDIDA and
        :new.CD_GRUPO = SIVE_MEDIDAS.CD_GRUPO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_BROTES_MED" because "SIVE_MEDIDAS" does not exist.'*/
          'no se puede grabar en SIVE_BROTES_MED
           porque no existe la clave en SIVE_MEDIDAS'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:47 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_BROTES_MED_TRIG_A_U_1 after UPDATE on SIVE_BROTES_MED for each row
-- ERwin Builtin Wed Oct 31 16:28:47 2001
-- UPDATE trigger on SIVE_BROTES_MED 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_BROTES  SIVE_BROTES_MED ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
      :new.CD_ANO = SIVE_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES_MED" because "SIVE_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_BROTES_MED
          porque no existe la clave en SIVE_BROTES'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
  /* SIVE_MEDIDAS  SIVE_BROTES_MED ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MEDIDAS
    where
      /* %JoinFKPK(:%New,SIVE_MEDIDAS," = "," and") */
      :new.CD_MEDIDA = SIVE_MEDIDAS.CD_MEDIDA and
      :new.CD_GRUPO = SIVE_MEDIDAS.CD_GRUPO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_BROTES_MED" because "SIVE_MEDIDAS" does not exist.'*/
         'no se puede actualizar SIVE_BROTES_MED
          porque no existe la clave en SIVE_MEDIDAS'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:47 2001
END;
/

create trigger SIVE_C_HOSP_TRIG_A_I
  AFTER INSERT
  on SIVE_C_HOSP
  
  for each row
declare numrows INTEGER;
begin

    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_C_NOTIF R/43 SIVE_C_HOSP ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_C_NOTIF
      where
        /* %JoinFKPK(:%New,SIVE_C_NOTIF," = "," and") */
        :new.CD_CENTRO = SIVE_C_NOTIF.CD_CENTRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_C_HOSP" because a que no existe en "SIVE_C_NOTIF".'
      );
    end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_C_HOSP_TRIG_A_D
  AFTER DELETE
  on SIVE_C_HOSP
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_C_HOSP R/48 SIVE_RES_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_HOSP = :old.CD_HOSP;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_C_HOSP" debido a que existe en "SIVE_RES_URGENCIAS".'
      );
    end if;

    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_C_HOSP R/41 SIVE_ESPECIALIDAD ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ESPECIALIDAD
      where
        /*  %JoinFKPK(SIVE_ESPECIALIDAD,:%Old," = "," and") */
        SIVE_ESPECIALIDAD.CD_HOSP = :old.CD_HOSP;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_C_HOSP" debido a que existe en "SIVE_ESPECIALIDAD".'
      );
    end if;

    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_C_HOSP R/35 SIVE_FICHEROS_URG ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_FICHEROS_URG
      where
        /*  %JoinFKPK(SIVE_FICHEROS_URG,:%Old," = "," and") */
        SIVE_FICHEROS_URG.CD_HOSP = :old.CD_HOSP;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_C_HOSP" debido a que existe en "SIVE_FICHEROS_URG".'
      );
    end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_C_HOSP_TRIG_A_U
  AFTER UPDATE
  on SIVE_C_HOSP
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_C_HOSP R/48 SIVE_RES_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_HOSP <> :new.CD_HOSP
  then
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_HOSP = :old.CD_HOSP;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_C_HOSP" debido a que existe "SIVE_RES_URGENCIAS".'
      );
    end if;
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_C_HOSP R/41 SIVE_ESPECIALIDAD ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_HOSP <> :new.CD_HOSP
  then
    select count(*) into numrows
      from SIVE_ESPECIALIDAD
      where
        /*  %JoinFKPK(SIVE_ESPECIALIDAD,:%Old," = "," and") */
        SIVE_ESPECIALIDAD.CD_HOSP = :old.CD_HOSP;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_C_HOSP" debido a que existe "SIVE_ESPECIALIDAD".'
      );
    end if;
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_C_HOSP R/35 SIVE_FICHEROS_URG ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_HOSP <> :new.CD_HOSP
  then
    select count(*) into numrows
      from SIVE_FICHEROS_URG
      where
        /*  %JoinFKPK(SIVE_FICHEROS_URG,:%Old," = "," and") */
        SIVE_FICHEROS_URG.CD_HOSP = :old.CD_HOSP;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_C_HOSP" debido a que existe "SIVE_FICHEROS_URG".'
      );
    end if;
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_C_NOTIF R/43 SIVE_C_HOSP ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_C_NOTIF
    where
      /* %JoinFKPK(:%New,SIVE_C_NOTIF," = "," and") */
      :new.CD_CENTRO = SIVE_C_NOTIF.CD_CENTRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_C_HOSP" debido a que no existe en "SIVE_C_NOTIF".'
    );
  end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_C_NOTIF_TRIG_A_D
  AFTER DELETE
  on SIVE_C_NOTIF
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Tue Dec 28 10:18:50 1999 */
    /* SIVE_C_NOTIF R/43 SIVE_C_HOSP ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_C_HOSP
      where
        /*  %JoinFKPK(SIVE_C_HOSP,:%Old," = "," and") */
        SIVE_C_HOSP.CD_CENTRO = :old.CD_CENTRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_C_NOTIF" debido a que existe en "SIVE_C_HOSP".'
      );
    end if;


-- ERwin Builtin Tue Dec 28 10:18:50 1999
end;
/


create trigger SIVE_C_NOTIF_TRIG_A_U
  AFTER UPDATE
  on SIVE_C_NOTIF
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Tue Dec 28 10:18:53 1999 */
  /* SIVE_C_NOTIF R/43 SIVE_C_HOSP ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_CENTRO <> :new.CD_CENTRO
  then
    select count(*) into numrows
      from SIVE_C_HOSP
      where
        /*  %JoinFKPK(SIVE_C_HOSP,:%Old," = "," and") */
        SIVE_C_HOSP.CD_CENTRO = :old.CD_CENTRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_C_NOTIF" debido a que existe "SIVE_C_HOSP".'
      );
    end if;
  end if;


-- ERwin Builtin Tue Dec 28 10:18:53 1999
end;
/


CREATE OR REPLACE TRIGGER SIVE_C_NOTIF_TRIG_A_I_1 after INSERT on SIVE_C_NOTIF for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_C_NOTIF 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_NIV_ASIST  SIVE_C_NOTIF ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIV_ASIST
      where
        /* %JoinFKPK(:%New,SIVE_NIV_ASIST," = "," and") */
        :new.CD_NIVASIS = SIVE_NIV_ASIST.CD_NIVASIS;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_C_NOTIF" because "SIVE_NIV_ASIST" does not exist.'*/
          'no se puede grabar en SIVE_C_NOTIF
           porque no existe la clave en SIVE_NIV_ASIST'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:47 2001 */
    /* SIVE_MUNICIPIO  SIVE_C_NOTIF ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
        :new.CD_PROV = SIVE_MUNICIPIO.CD_PROV and
        :new.CD_MUN = SIVE_MUNICIPIO.CD_MUN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_C_NOTIF" because "SIVE_MUNICIPIO" does not exist.'*/
          'no se puede grabar en SIVE_C_NOTIF
           porque no existe la clave en SIVE_MUNICIPIO'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CA_PARAMETROS_TRIG_A_I_1 after INSERT on SIVE_CA_PARAMETROS for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_CA_PARAMETROS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_COM_AUT R/267 SIVE_CA_PARAMETROS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_COM_AUT
      where
        /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
        :new.CD_CA = SIVE_COM_AUT.CD_CA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_CA_PARAMETROS" because "SIVE_COM_AUT" does not exist.'*/
          'no se puede grabar en SIVE_CA_PARAMETROS
           porque no existe la clave en SIVE_COM_AUT'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CA_PARAMETROS_TRIG_A_U_1 after UPDATE on SIVE_CA_PARAMETROS for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_CA_PARAMETROS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_COM_AUT R/267 SIVE_CA_PARAMETROS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_COM_AUT
    where
      /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
      :new.CD_CA = SIVE_COM_AUT.CD_CA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_CA_PARAMETROS" because "SIVE_COM_AUT" does not exist.'*/
         'no se puede actualizar SIVE_CA_PARAMETROS
          porque no existe la clave en SIVE_COM_AUT'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CA_PARAMETS_BR_TRIG_A_I_1 after INSERT on SIVE_CA_PARAMETROS_BR for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_CA_PARAMETROS_BR 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_COM_AUT R/290 SIVE_CA_PARAMETROS_BR ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_COM_AUT
      where
        /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
        :new.CD_CA = SIVE_COM_AUT.CD_CA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_CA_PARAMETROS_BR" because "SIVE_COM_AUT" does not exist.'*/
          'no se puede grabar en SIVE_CA_PARAMETROS_BR
           porque no existe la clave en SIVE_COM_AUT'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CA_PARAMETS_BR_TRIG_A_U_1 after UPDATE on SIVE_CA_PARAMETROS_BR for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_CA_PARAMETROS_BR 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_COM_AUT R/290 SIVE_CA_PARAMETROS_BR ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_COM_AUT
    where
      /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
      :new.CD_CA = SIVE_COM_AUT.CD_CA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_CA_PARAMETROS_BR" because "SIVE_COM_AUT" does not exist.'*/
         'no se puede actualizar SIVE_CA_PARAMETROS_BR
          porque no existe la clave en SIVE_COM_AUT'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CA_PARAMETS_TB_TRIG_A_I_1 after INSERT on SIVE_CA_PARAMETROS_TB for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_CA_PARAMETROS_TB 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_COM_AUT R/294 SIVE_CA_PARAMETROS_TB ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_COM_AUT
      where
        /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
        :new.CD_CA = SIVE_COM_AUT.CD_CA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_CA_PARAMETROS_TB" because "SIVE_COM_AUT" does not exist.'*/
          'no se puede grabar en SIVE_CA_PARAMETROS_TB
           porque no existe la clave en SIVE_COM_AUT'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CA_PARAMETS_TB_TRIG_A_U_1 after UPDATE on SIVE_CA_PARAMETROS_TB for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_CA_PARAMETROS_TB 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_COM_AUT R/294 SIVE_CA_PARAMETROS_TB ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_COM_AUT
    where
      /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
      :new.CD_CA = SIVE_COM_AUT.CD_CA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_CA_PARAMETROS_TB" because "SIVE_COM_AUT" does not exist.'*/
         'no se puede actualizar SIVE_CA_PARAMETROS_TB
          porque no existe la clave en SIVE_COM_AUT'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CASOCONTAC_TRIG_A_D_1 after DELETE on SIVE_CASOCONTAC for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_CASOCONTAC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_CASOCONTAC R/295 SIVE_RESPCONTACTO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESPCONTACTO
      where
        /*  %JoinFKPK(SIVE_RESPCONTACTO,:%Old," = "," and") */
        SIVE_RESPCONTACTO.CD_ENFERMO = :old.CD_ENFERMO and
        SIVE_RESPCONTACTO.CD_ARTBC = :old.CD_ARTBC and
        SIVE_RESPCONTACTO.CD_NRTBC = :old.CD_NRTBC;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_CASOCONTAC" because "SIVE_RESPCONTACTO" exists.'*/
       'no se puede borrar de SIVE_CASOCONTAC
        porque hay registros en SIVE_RESPCONTACTO con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CASOCONTAC_TRIG_A_I_1 after INSERT on SIVE_CASOCONTAC for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_CASOCONTAC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_BREGISTROTBC R/363 SIVE_CASOCONTAC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_BREGISTROTBC
      where
        /* %JoinFKPK(:%New,SIVE_BREGISTROTBC," = "," and") */
        :new.CD_ARTBC = SIVE_BREGISTROTBC.CD_ARTBC and
        :new.CD_NRTBC = SIVE_BREGISTROTBC.CD_NRTBC;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_CASOCONTAC" because "SIVE_BREGISTROTBC" does not exist.'*/
          'no se puede grabar en SIVE_CASOCONTAC
           porque no existe la clave en SIVE_BREGISTROTBC'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_REGISTROTBC  SIVE_CASOCONTAC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_REGISTROTBC
      where
        /* %JoinFKPK(:%New,SIVE_REGISTROTBC," = "," and") */
        :new.CD_ARTBC = SIVE_REGISTROTBC.CD_ARTBC and
        :new.CD_NRTBC = SIVE_REGISTROTBC.CD_NRTBC;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_CASOCONTAC" because "SIVE_REGISTROTBC" does not exist.'*/
          'no se puede grabar en SIVE_CASOCONTAC
           porque no existe la clave en SIVE_REGISTROTBC'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENFERMO  SIVE_CASOCONTAC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ENFERMO
      where
        /* %JoinFKPK(:%New,SIVE_ENFERMO," = "," and") */
        :new.CD_ENFERMO = SIVE_ENFERMO.CD_ENFERMO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_CASOCONTAC" because "SIVE_ENFERMO" does not exist.'*/
          'no se puede grabar en SIVE_CASOCONTAC
           porque no existe la clave en SIVE_ENFERMO'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CASOCONTAC_TRIG_A_U_1 after UPDATE on SIVE_CASOCONTAC for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_CASOCONTAC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_CASOCONTAC R/295 SIVE_RESPCONTACTO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFERMO <> :new.CD_ENFERMO or 
    :old.CD_ARTBC <> :new.CD_ARTBC or 
    :old.CD_NRTBC <> :new.CD_NRTBC
  then
    select count(*) into numrows
      from SIVE_RESPCONTACTO
      where
        /*  %JoinFKPK(SIVE_RESPCONTACTO,:%Old," = "," and") */
        SIVE_RESPCONTACTO.CD_ENFERMO = :old.CD_ENFERMO and
        SIVE_RESPCONTACTO.CD_ARTBC = :old.CD_ARTBC and
        SIVE_RESPCONTACTO.CD_NRTBC = :old.CD_NRTBC;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_CASOCONTAC" because "SIVE_RESPCONTACTO" exists.'*/
       'no se puede modificar SIVE_CASOCONTAC
        porque hay registros en SIVE_RESPCONTACTO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_BREGISTROTBC R/363 SIVE_CASOCONTAC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_BREGISTROTBC
    where
      /* %JoinFKPK(:%New,SIVE_BREGISTROTBC," = "," and") */
      :new.CD_ARTBC = SIVE_BREGISTROTBC.CD_ARTBC and
      :new.CD_NRTBC = SIVE_BREGISTROTBC.CD_NRTBC;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_CASOCONTAC" because "SIVE_BREGISTROTBC" does not exist.'*/
         'no se puede actualizar SIVE_CASOCONTAC
          porque no existe la clave en SIVE_BREGISTROTBC'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_REGISTROTBC  SIVE_CASOCONTAC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_REGISTROTBC
    where
      /* %JoinFKPK(:%New,SIVE_REGISTROTBC," = "," and") */
      :new.CD_ARTBC = SIVE_REGISTROTBC.CD_ARTBC and
      :new.CD_NRTBC = SIVE_REGISTROTBC.CD_NRTBC;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_CASOCONTAC" because "SIVE_REGISTROTBC" does not exist.'*/
         'no se puede actualizar SIVE_CASOCONTAC
          porque no existe la clave en SIVE_REGISTROTBC'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENFERMO  SIVE_CASOCONTAC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ENFERMO
    where
      /* %JoinFKPK(:%New,SIVE_ENFERMO," = "," and") */
      :new.CD_ENFERMO = SIVE_ENFERMO.CD_ENFERMO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_CASOCONTAC" because "SIVE_ENFERMO" does not exist.'*/
         'no se puede actualizar SIVE_CASOCONTAC
          porque no existe la clave en SIVE_ENFERMO'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CASOS_CENT_TRIG_A_D_1 after DELETE on SIVE_CASOS_CENT for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_CASOS_CENT 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_CASOS_CENT  SIVE_RESP_CENTI ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESP_CENTI
      where
        /*  %JoinFKPK(SIVE_RESP_CENTI,:%Old," = "," and") */
        SIVE_RESP_CENTI.CD_ENFCIE = :old.CD_ENFCIE and
        SIVE_RESP_CENTI.CD_PCENTI = :old.CD_PCENTI and
        SIVE_RESP_CENTI.CD_MEDCEN = :old.CD_MEDCEN and
        SIVE_RESP_CENTI.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_RESP_CENTI.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_RESP_CENTI.NM_ORDEN = :old.NM_ORDEN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_CASOS_CENT" because "SIVE_RESP_CENTI" exists.'*/
       'no se puede borrar de SIVE_CASOS_CENT
        porque hay registros en SIVE_RESP_CENTI con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CASOS_CENT_TRIG_A_I_1 after INSERT on SIVE_CASOS_CENT for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_CASOS_CENT 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_NOTIF_RMC  SIVE_CASOS_CENT ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIF_RMC
      where
        /* %JoinFKPK(:%New,SIVE_NOTIF_RMC," = "," and") */
        :new.CD_ENFCIE = SIVE_NOTIF_RMC.CD_ENFCIE and
        :new.CD_PCENTI = SIVE_NOTIF_RMC.CD_PCENTI and
        :new.CD_MEDCEN = SIVE_NOTIF_RMC.CD_MEDCEN and
        :new.CD_ANOEPI = SIVE_NOTIF_RMC.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_NOTIF_RMC.CD_SEMEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_CASOS_CENT" because "SIVE_NOTIF_RMC" does not exist.'*/
          'no se puede grabar en SIVE_CASOS_CENT
           porque no existe la clave en SIVE_NOTIF_RMC'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CASOS_CENT_TRIG_A_U_1 after UPDATE on SIVE_CASOS_CENT for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_CASOS_CENT 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_CASOS_CENT  SIVE_RESP_CENTI ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE or 
    :old.CD_PCENTI <> :new.CD_PCENTI or 
    :old.CD_MEDCEN <> :new.CD_MEDCEN or 
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI or 
    :old.NM_ORDEN <> :new.NM_ORDEN
  then
    select count(*) into numrows
      from SIVE_RESP_CENTI
      where
        /*  %JoinFKPK(SIVE_RESP_CENTI,:%Old," = "," and") */
        SIVE_RESP_CENTI.CD_ENFCIE = :old.CD_ENFCIE and
        SIVE_RESP_CENTI.CD_PCENTI = :old.CD_PCENTI and
        SIVE_RESP_CENTI.CD_MEDCEN = :old.CD_MEDCEN and
        SIVE_RESP_CENTI.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_RESP_CENTI.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_RESP_CENTI.NM_ORDEN = :old.NM_ORDEN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_CASOS_CENT" because "SIVE_RESP_CENTI" exists.'*/
       'no se puede modificar SIVE_CASOS_CENT
        porque hay registros en SIVE_RESP_CENTI con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_NOTIF_RMC  SIVE_CASOS_CENT ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NOTIF_RMC
    where
      /* %JoinFKPK(:%New,SIVE_NOTIF_RMC," = "," and") */
      :new.CD_ENFCIE = SIVE_NOTIF_RMC.CD_ENFCIE and
      :new.CD_PCENTI = SIVE_NOTIF_RMC.CD_PCENTI and
      :new.CD_MEDCEN = SIVE_NOTIF_RMC.CD_MEDCEN and
      :new.CD_ANOEPI = SIVE_NOTIF_RMC.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_NOTIF_RMC.CD_SEMEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_CASOS_CENT" because "SIVE_NOTIF_RMC" does not exist.'*/
         'no se puede actualizar SIVE_CASOS_CENT
          porque no existe la clave en SIVE_NOTIF_RMC'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

create trigger SIVE_CAUSAURG_TRIG_A_U
  AFTER UPDATE
  on SIVE_CAUSAURG
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Fri Feb 04 10:04:39 2000 */
  /* SIVE_CAUSAURG R/52 SIVE_RES_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_CAUSAURG <> :new.CD_CAUSAURG
  then
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_CAUSAURG = :old.CD_CAUSAURG;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_CAUSAURG" debido a que existe "SIVE_RES_URGENCIAS".'
      );
    end if;
  end if;

  /* ERwin Builtin Fri Feb 04 10:04:39 2000 */
  /* SIVE_CAUSAURG R/31 SIVE_CODIFICADOR ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_CAUSAURG <> :new.CD_CAUSAURG
  then
    select count(*) into numrows
      from SIVE_CODIFICADOR
      where
        /*  %JoinFKPK(SIVE_CODIFICADOR,:%Old," = "," and") */
        SIVE_CODIFICADOR.CD_CAUSAURG = :old.CD_CAUSAURG;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_CAUSAURG" debido a que existe "SIVE_CODIFICADOR".'
      );
    end if;
  end if;

  /* ERwin Builtin Fri Feb 04 10:04:39 2000 */
  /* SIVE_CAUSAURG R/30 SIVE_MOV_URGDIAG ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_CAUSAURG <> :new.CD_CAUSAURG
  then
    select count(*) into numrows
      from SIVE_MOV_URGDIAG
      where
        /*  %JoinFKPK(SIVE_MOV_URGDIAG,:%Old," = "," and") */
        SIVE_MOV_URGDIAG.CD_CAUSAURG = :old.CD_CAUSAURG;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_CAUSAURG" debido a que existe "SIVE_MOV_URGDIAG".'
      );
    end if;
  end if;

  /* ERwin Builtin Fri Feb 04 10:04:39 2000 */
  /* SIVE_CAUSAURG R/29 SIVE_URGDIAG ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_CAUSAURG <> :new.CD_CAUSAURG
  then
    select count(*) into numrows
      from SIVE_URGDIAG
      where
        /*  %JoinFKPK(SIVE_URGDIAG,:%Old," = "," and") */
        SIVE_URGDIAG.CD_CAUSAURG = :old.CD_CAUSAURG;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_CAUSAURG" debido a que existe "SIVE_URGDIAG".'
      );
    end if;
  end if;


-- ERwin Builtin Fri Feb 04 10:04:39 2000
end;
/


create trigger SIVE_CAUSAURG_TRIG_A_D
  AFTER DELETE
  on SIVE_CAUSAURG
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Fri Feb 04 10:04:39 2000 */
    /* SIVE_CAUSAURG R/52 SIVE_RES_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_CAUSAURG = :old.CD_CAUSAURG;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_CAUSAURG" debido a que existe en "SIVE_RES_URGENCIAS".'
      );
    end if;

    /* ERwin Builtin Fri Feb 04 10:04:39 2000 */
    /* SIVE_CAUSAURG R/31 SIVE_CODIFICADOR ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_CODIFICADOR
      where
        /*  %JoinFKPK(SIVE_CODIFICADOR,:%Old," = "," and") */
        SIVE_CODIFICADOR.CD_CAUSAURG = :old.CD_CAUSAURG;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_CAUSAURG" debido a que existe en "SIVE_CODIFICADOR".'
      );
    end if;

    /* ERwin Builtin Fri Feb 04 10:04:39 2000 */
    /* SIVE_CAUSAURG R/30 SIVE_MOV_URGDIAG ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MOV_URGDIAG
      where
        /*  %JoinFKPK(SIVE_MOV_URGDIAG,:%Old," = "," and") */
        SIVE_MOV_URGDIAG.CD_CAUSAURG = :old.CD_CAUSAURG;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_CAUSAURG" debido a que existe en "SIVE_MOV_URGDIAG".'
      );
    end if;

    /* ERwin Builtin Fri Feb 04 10:04:39 2000 */
    /* SIVE_CAUSAURG R/29 SIVE_URGDIAG ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_URGDIAG
      where
        /*  %JoinFKPK(SIVE_URGDIAG,:%Old," = "," and") */
        SIVE_URGDIAG.CD_CAUSAURG = :old.CD_CAUSAURG;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_CAUSAURG" debido a que existe en "SIVE_URGDIAG".'
      );
    end if;


-- ERwin Builtin Fri Feb 04 10:04:39 2000
end;
/


CREATE OR REPLACE TRIGGER SIVE_CLASIF_EDO_TRIG_A_D_1 after DELETE on SIVE_CLASIF_EDO for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_CLASIF_EDO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_CLASIF_EDO  SIVE_EDOIND ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_EDOIND
      where
        /*  %JoinFKPK(SIVE_EDOIND,:%Old," = "," and") */
        SIVE_EDOIND.CD_CLASIFDIAG = :old.CD_CLASIFDIAG;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_CLASIF_EDO" because "SIVE_EDOIND" exists.'*/
       'no se puede borrar de SIVE_CLASIF_EDO
        porque hay registros en SIVE_EDOIND con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CLASIF_EDO_TRIG_A_U_1 after UPDATE on SIVE_CLASIF_EDO for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_CLASIF_EDO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_CLASIF_EDO  SIVE_EDOIND ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_CLASIFDIAG <> :new.CD_CLASIFDIAG
  then
    select count(*) into numrows
      from SIVE_EDOIND
      where
        /*  %JoinFKPK(SIVE_EDOIND,:%Old," = "," and") */
        SIVE_EDOIND.CD_CLASIFDIAG = :old.CD_CLASIFDIAG;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_CLASIF_EDO" because "SIVE_EDOIND" exists.'*/
       'no se puede modificar SIVE_CLASIF_EDO
        porque hay registros en SIVE_EDOIND con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

create trigger SIVE_CODIFICADOR_TRIG_A_U
  AFTER UPDATE
  on SIVE_CODIFICADOR
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_CAUSAURG R/31 SIVE_CODIFICADOR ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_CAUSAURG
    where
      /* %JoinFKPK(:%New,SIVE_CAUSAURG," = "," and") */
      :new.CD_CAUSAURG = SIVE_CAUSAURG.CD_CAUSAURG;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_CODIFICADOR" debido a que no existe en "SIVE_CAUSAURG".'
    );
  end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_CODIFICADOR_TRIG_B_I
  BEFORE INSERT
  on SIVE_CODIFICADOR
  
  for each row
declare numrows INTEGER;
begin
/* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_CAUSAURG R/31 SIVE_CODIFICADOR ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_CAUSAURG
      where
        /* %JoinFKPK(:%New,SIVE_CAUSAURG," = "," and") */
        :new.CD_CAUSAURG = SIVE_CAUSAURG.CD_CAUSAURG;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_CODIFICADOR" because a que no existe en "SIVE_CAUSAURG".'
      );
    end if;






/* Secuenciador */
select SIVE_s_CODIFICADOR.NextVal
into :new.NM_CODIF
from Dual;




end;
/


create trigger SIVE_COM_AUT_TRIG_A_U
  AFTER UPDATE
  on SIVE_COM_AUT
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Tue Dec 28 10:19:04 1999 */
  /* SIVE_COM_AUT R/21 SIVE_PROVINCIA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_CA <> :new.CD_CA
  then
    select count(*) into numrows
      from SIVE_PROVINCIA
      where
        /*  %JoinFKPK(SIVE_PROVINCIA,:%Old," = "," and") */
        SIVE_PROVINCIA.CD_CA = :old.CD_CA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_COM_AUT" debido a que existe "SIVE_PROVINCIA".'
      );
    end if;
  end if;


-- ERwin Builtin Tue Dec 28 10:19:04 1999
end;
/


create trigger SIVE_COM_AUT_TRIG_A_D
  AFTER DELETE
  on SIVE_COM_AUT
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Tue Dec 28 10:19:04 1999 */
    /* SIVE_COM_AUT R/21 SIVE_PROVINCIA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_PROVINCIA
      where
        /*  %JoinFKPK(SIVE_PROVINCIA,:%Old," = "," and") */
        SIVE_PROVINCIA.CD_CA = :old.CD_CA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_COM_AUT" debido a que existe en "SIVE_PROVINCIA".'
      );
    end if;


-- ERwin Builtin Tue Dec 28 10:19:04 1999
end;
/


CREATE OR REPLACE TRIGGER SIVE_CONGLOM_TRIG_A_D_1 after DELETE on SIVE_CONGLOM for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_CONGLOM 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_CONGLOM  SIVE_PCENTINELA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_PCENTINELA
      where
        /*  %JoinFKPK(SIVE_PCENTINELA,:%Old," = "," and") */
        SIVE_PCENTINELA.CD_CONGLO = :old.CD_CONGLO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_CONGLOM" because "SIVE_PCENTINELA" exists.'*/
       'no se puede borrar de SIVE_CONGLOM
        porque hay registros en SIVE_PCENTINELA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_CONGLOM_TRIG_A_U_1 after UPDATE on SIVE_CONGLOM for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_CONGLOM 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_CONGLOM  SIVE_PCENTINELA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_CONGLO <> :new.CD_CONGLO
  then
    select count(*) into numrows
      from SIVE_PCENTINELA
      where
        /*  %JoinFKPK(SIVE_PCENTINELA,:%Old," = "," and") */
        SIVE_PCENTINELA.CD_CONGLO = :old.CD_CONGLO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_CONGLOM" because "SIVE_PCENTINELA" exists.'*/
       'no se puede modificar SIVE_CONGLOM
        porque hay registros en SIVE_PCENTINELA con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

create trigger SIVE_DERIVACION_TRIG_A_D
  AFTER DELETE
  on SIVE_DERIVACION
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_DERIVACION R/46 SIVE_RES_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_DERIVACION = :old.CD_DERIVACION;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_DERIVACION" debido a que existe en "SIVE_RES_URGENCIAS".'
      );
    end if;

    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_DERIVACION R/39 SIVE_MOV_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MOV_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_MOV_URGENCIAS,:%Old," = "," and") */
        SIVE_MOV_URGENCIAS.CD_DERIVACION = :old.CD_DERIVACION;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_DERIVACION" debido a que existe en "SIVE_MOV_URGENCIAS".'
      );
    end if;

    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_DERIVACION R/38 SIVE_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_URGENCIAS,:%Old," = "," and") */
        SIVE_URGENCIAS.CD_DERIVACION = :old.CD_DERIVACION;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_DERIVACION" debido a que existe en "SIVE_URGENCIAS".'
      );
    end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_DERIVACION_TRIG_A_U
  AFTER UPDATE
  on SIVE_DERIVACION
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_DERIVACION R/46 SIVE_RES_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_DERIVACION <> :new.CD_DERIVACION
  then
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_DERIVACION = :old.CD_DERIVACION;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_DERIVACION" debido a que existe "SIVE_RES_URGENCIAS".'
      );
    end if;
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_DERIVACION R/39 SIVE_MOV_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_DERIVACION <> :new.CD_DERIVACION
  then
    select count(*) into numrows
      from SIVE_MOV_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_MOV_URGENCIAS,:%Old," = "," and") */
        SIVE_MOV_URGENCIAS.CD_DERIVACION = :old.CD_DERIVACION;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_DERIVACION" debido a que existe "SIVE_MOV_URGENCIAS".'
      );
    end if;
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_DERIVACION R/38 SIVE_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_DERIVACION <> :new.CD_DERIVACION
  then
    select count(*) into numrows
      from SIVE_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_URGENCIAS,:%Old," = "," and") */
        SIVE_URGENCIAS.CD_DERIVACION = :old.CD_DERIVACION;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_DERIVACION" debido a que existe "SIVE_URGENCIAS".'
      );
    end if;
  end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_DICCIONARIO_TRIG_B_I
  BEFORE INSERT
  on SIVE_DICCIONARIO
  
  for each row
declare numrows INTEGER;
begin





/* Secuenciador */
select SIVE_s_DICCIONARIO.NextVal
into :new.NM_SECDIC
from Dual;



end;
/


CREATE OR REPLACE TRIGGER SIVE_DISTR_GEDAD_TRIG_A_D_1 after DELETE on SIVE_DISTR_GEDAD for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_DISTR_GEDAD 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_DISTR_GEDAD  SIVE_BROTES_DISTGEDAD ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES_DISTGEDAD
      where
        /*  %JoinFKPK(SIVE_BROTES_DISTGEDAD,:%Old," = "," and") */
        SIVE_BROTES_DISTGEDAD.CD_GEDAD = :old.CD_GEDAD and
        SIVE_BROTES_DISTGEDAD.DIST_EDAD_I = :old.DIST_EDAD_I and
        SIVE_BROTES_DISTGEDAD.DIST_EDAD_F = :old.DIST_EDAD_F;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_DISTR_GEDAD" because "SIVE_BROTES_DISTGEDAD" exists.'*/
       'no se puede borrar de SIVE_DISTR_GEDAD
        porque hay registros en SIVE_BROTES_DISTGEDAD con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_DISTR_GEDAD  SIVE_TATAQ_ENFVAC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_TATAQ_ENFVAC
      where
        /*  %JoinFKPK(SIVE_TATAQ_ENFVAC,:%Old," = "," and") */
        SIVE_TATAQ_ENFVAC.CD_GEDAD = :old.CD_GEDAD and
        SIVE_TATAQ_ENFVAC.DIST_EDAD_I = :old.DIST_EDAD_I and
        SIVE_TATAQ_ENFVAC.DIST_EDAD_F = :old.DIST_EDAD_F;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_DISTR_GEDAD" because "SIVE_TATAQ_ENFVAC" exists.'*/
       'no se puede borrar de SIVE_DISTR_GEDAD
        porque hay registros en SIVE_TATAQ_ENFVAC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_DISTR_GEDAD_TRIG_A_I_1 after INSERT on SIVE_DISTR_GEDAD for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_DISTR_GEDAD 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_GEDAD  SIVE_DISTR_GEDAD ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_GEDAD
      where
        /* %JoinFKPK(:%New,SIVE_GEDAD," = "," and") */
        :new.CD_GEDAD = SIVE_GEDAD.CD_GEDAD;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_DISTR_GEDAD" because "SIVE_GEDAD" does not exist.'*/
          'no se puede grabar en SIVE_DISTR_GEDAD
           porque no existe la clave en SIVE_GEDAD'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_DISTR_GEDAD_TRIG_A_U_1 after UPDATE on SIVE_DISTR_GEDAD for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_DISTR_GEDAD 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_DISTR_GEDAD  SIVE_BROTES_DISTGEDAD ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GEDAD <> :new.CD_GEDAD or 
    :old.DIST_EDAD_I <> :new.DIST_EDAD_I or 
    :old.DIST_EDAD_F <> :new.DIST_EDAD_F
  then
    select count(*) into numrows
      from SIVE_BROTES_DISTGEDAD
      where
        /*  %JoinFKPK(SIVE_BROTES_DISTGEDAD,:%Old," = "," and") */
        SIVE_BROTES_DISTGEDAD.CD_GEDAD = :old.CD_GEDAD and
        SIVE_BROTES_DISTGEDAD.DIST_EDAD_I = :old.DIST_EDAD_I and
        SIVE_BROTES_DISTGEDAD.DIST_EDAD_F = :old.DIST_EDAD_F;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_DISTR_GEDAD" because "SIVE_BROTES_DISTGEDAD" exists.'*/
       'no se puede modificar SIVE_DISTR_GEDAD
        porque hay registros en SIVE_BROTES_DISTGEDAD con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_DISTR_GEDAD  SIVE_TATAQ_ENFVAC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GEDAD <> :new.CD_GEDAD or 
    :old.DIST_EDAD_I <> :new.DIST_EDAD_I or 
    :old.DIST_EDAD_F <> :new.DIST_EDAD_F
  then
    select count(*) into numrows
      from SIVE_TATAQ_ENFVAC
      where
        /*  %JoinFKPK(SIVE_TATAQ_ENFVAC,:%Old," = "," and") */
        SIVE_TATAQ_ENFVAC.CD_GEDAD = :old.CD_GEDAD and
        SIVE_TATAQ_ENFVAC.DIST_EDAD_I = :old.DIST_EDAD_I and
        SIVE_TATAQ_ENFVAC.DIST_EDAD_F = :old.DIST_EDAD_F;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_DISTR_GEDAD" because "SIVE_TATAQ_ENFVAC" exists.'*/
       'no se puede modificar SIVE_DISTR_GEDAD
        porque hay registros en SIVE_TATAQ_ENFVAC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_GEDAD  SIVE_DISTR_GEDAD ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_GEDAD
    where
      /* %JoinFKPK(:%New,SIVE_GEDAD," = "," and") */
      :new.CD_GEDAD = SIVE_GEDAD.CD_GEDAD;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_DISTR_GEDAD" because "SIVE_GEDAD" does not exist.'*/
         'no se puede actualizar SIVE_DISTR_GEDAD
          porque no existe la clave en SIVE_GEDAD'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

create trigger SIVE_DISTR_GEDAD__TRIG_A_I
  AFTER INSERT
  on SIVE_DISTR_GEDAD_ADU
  
  for each row
declare numrows INTEGER;
begin

    /* ERwin Builtin Mon Feb 07 12:32:57 2000 */
    /* SIVE_GEDAD_ADU R/224 SIVE_DISTR_GEDAD_ADU ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_GEDAD_ADU
      where
        /* %JoinFKPK(:%New,SIVE_GEDAD_ADU," = "," and") */
        :new.CD_GEDAD = SIVE_GEDAD_ADU.CD_GEDAD;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_DISTR_GEDAD_ADU" because a que no existe en "SIVE_GEDAD_ADU".'
      );
    end if;


-- ERwin Builtin Mon Feb 07 12:32:57 2000
end;
/


create trigger SIVE_DISTR_GEDAD__TRIG_A_D
  AFTER DELETE
  on SIVE_DISTR_GEDAD_ADU
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Mon Feb 07 12:32:57 2000 */
    /* SIVE_DISTR_GEDAD_ADU R/55 SIVE_RES_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_GEDAD = :old.CD_GEDAD and
        SIVE_RES_URGENCIAS.DIST_EDAD_I = :old.DIST_EDAD_I and
        SIVE_RES_URGENCIAS.DIST_EDAD_F = :old.DIST_EDAD_F;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_DISTR_GEDAD_ADU" debido a que existe en "SIVE_RES_URGENCIAS".'
      );
    end if;


-- ERwin Builtin Mon Feb 07 12:32:57 2000
end;
/


create trigger SIVE_DISTR_GEDAD__TRIG_A_U
  AFTER UPDATE
  on SIVE_DISTR_GEDAD_ADU
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Mon Feb 07 12:32:57 2000 */
  /* SIVE_DISTR_GEDAD_ADU R/55 SIVE_RES_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GEDAD <> :new.CD_GEDAD or 
    :old.DIST_EDAD_I <> :new.DIST_EDAD_I or 
    :old.DIST_EDAD_F <> :new.DIST_EDAD_F
  then
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_GEDAD = :old.CD_GEDAD and
        SIVE_RES_URGENCIAS.DIST_EDAD_I = :old.DIST_EDAD_I and
        SIVE_RES_URGENCIAS.DIST_EDAD_F = :old.DIST_EDAD_F;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_DISTR_GEDAD_ADU" debido a que existe "SIVE_RES_URGENCIAS".'
      );
    end if;
  end if;

  /* ERwin Builtin Mon Feb 07 12:32:57 2000 */
  /* SIVE_GEDAD_ADU R/224 SIVE_DISTR_GEDAD_ADU ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_GEDAD_ADU
    where
      /* %JoinFKPK(:%New,SIVE_GEDAD_ADU," = "," and") */
      :new.CD_GEDAD = SIVE_GEDAD_ADU.CD_GEDAD;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_DISTR_GEDAD_ADU" debido a que no existe en "SIVE_GEDAD_ADU".'
    );
  end if;


-- ERwin Builtin Mon Feb 07 12:32:58 2000
end;
/


CREATE OR REPLACE TRIGGER SIVE_DIS_GRU_ED_RMC_TRIG_A_I_1 after INSERT on SIVE_DISTR_GRUPO_EDAD_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_DISTR_GRUPO_EDAD_RMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_GRUPO_EDAD_RMC  SIVE_DISTR_GRUPO_EDAD_RMC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_GRUPO_EDAD_RMC
      where
        /* %JoinFKPK(:%New,SIVE_GRUPO_EDAD_RMC," = "," and") */
        :new.CD_GEDAD = SIVE_GRUPO_EDAD_RMC.CD_GEDAD;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_DISTR_GRUPO_EDAD_RMC" because "SIVE_GRUPO_EDAD_RMC" does not exist.'*/
          'no se puede grabar en SIVE_DISTR_GRUPO_EDAD_RMC
           porque no existe la clave en SIVE_GRUPO_EDAD_RMC'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_DIS_GRU_ED_RMC_TRIG_A_U_1 after UPDATE on SIVE_DISTR_GRUPO_EDAD_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_DISTR_GRUPO_EDAD_RMC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_GRUPO_EDAD_RMC  SIVE_DISTR_GRUPO_EDAD_RMC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_GRUPO_EDAD_RMC
    where
      /* %JoinFKPK(:%New,SIVE_GRUPO_EDAD_RMC," = "," and") */
      :new.CD_GEDAD = SIVE_GRUPO_EDAD_RMC.CD_GEDAD;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_DISTR_GRUPO_EDAD_RMC" because "SIVE_GRUPO_EDAD_RMC" does not exist.'*/
         'no se puede actualizar SIVE_DISTR_GRUPO_EDAD_RMC
          porque no existe la clave en SIVE_GRUPO_EDAD_RMC'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_E_NOTIF_TRIG_A_D_1 after DELETE on SIVE_E_NOTIF for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_E_NOTIF 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_E_NOTIF  SIVE_ALERTA_BROTES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_BROTES
      where
        /*  %JoinFKPK(SIVE_ALERTA_BROTES,:%Old," = "," and") */
        SIVE_ALERTA_BROTES.CD_E_NOTIF = :old.CD_E_NOTIF;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_E_NOTIF" because "SIVE_ALERTA_BROTES" exists.'*/
       'no se puede borrar de SIVE_E_NOTIF
        porque hay registros en SIVE_ALERTA_BROTES con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_E_NOTIF  SIVE_MCENTINELA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MCENTINELA
      where
        /*  %JoinFKPK(SIVE_MCENTINELA,:%Old," = "," and") */
        SIVE_MCENTINELA.CD_E_NOTIF = :old.CD_E_NOTIF;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_E_NOTIF" because "SIVE_MCENTINELA" exists.'*/
       'no se puede borrar de SIVE_E_NOTIF
        porque hay registros en SIVE_MCENTINELA con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_E_NOTIF  SIVE_NOTIF_SEM ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIF_SEM
      where
        /*  %JoinFKPK(SIVE_NOTIF_SEM,:%Old," = "," and") */
        SIVE_NOTIF_SEM.CD_E_NOTIF = :old.CD_E_NOTIF;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_E_NOTIF" because "SIVE_NOTIF_SEM" exists.'*/
       'no se puede borrar de SIVE_E_NOTIF
        porque hay registros en SIVE_NOTIF_SEM con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_E_NOTIF  SIVE_REGISTROTBC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_REGISTROTBC
      where
        /*  %JoinFKPK(SIVE_REGISTROTBC,:%Old," = "," and") */
        SIVE_REGISTROTBC.CD_E_NOTIF = :old.CD_E_NOTIF;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_E_NOTIF" because "SIVE_REGISTROTBC" exists.'*/
       'no se puede borrar de SIVE_E_NOTIF
        porque hay registros en SIVE_REGISTROTBC con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_E_NOTIF  SIVE_USUARIO_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_USUARIO_BROTE
      where
        /*  %JoinFKPK(SIVE_USUARIO_BROTE,:%Old," = "," and") */
        SIVE_USUARIO_BROTE.CD_E_NOTIF = :old.CD_E_NOTIF;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_E_NOTIF" because "SIVE_USUARIO_BROTE" exists.'*/
       'no se puede borrar de SIVE_E_NOTIF
        porque hay registros en SIVE_USUARIO_BROTE con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_E_NOTIF R_269 SIVE_USUARIO_PISTA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_USUARIO_PISTA
      where
        /*  %JoinFKPK(SIVE_USUARIO_PISTA,:%Old," = "," and") */
        SIVE_USUARIO_PISTA.CD_E_NOTIF = :old.CD_E_NOTIF;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_E_NOTIF" because "SIVE_USUARIO_PISTA" exists.'*/
       'no se puede borrar de SIVE_E_NOTIF
        porque hay registros en SIVE_USUARIO_PISTA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_E_NOTIF_TRIG_A_I_1 after INSERT on SIVE_E_NOTIF for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_E_NOTIF 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_SEMANA_EPI  SIVE_E_NOTIF ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEMANA_EPI
      where
        /* %JoinFKPK(:%New,SIVE_SEMANA_EPI," = "," and") */
        :new.CD_ANOEPI = SIVE_SEMANA_EPI.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_SEMANA_EPI.CD_SEMEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_E_NOTIF" because "SIVE_SEMANA_EPI" does not exist.'*/
          'no se puede grabar en SIVE_E_NOTIF
           porque no existe la clave en SIVE_SEMANA_EPI'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ZONA_BASICA  SIVE_E_NOTIF ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ZONA_BASICA
      where
        /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
        :new.CD_NIVEL_1 = SIVE_ZONA_BASICA.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_ZONA_BASICA.CD_NIVEL_2 and
        :new.CD_ZBS = SIVE_ZONA_BASICA.CD_ZBS;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_E_NOTIF" because "SIVE_ZONA_BASICA" does not exist.'*/
          'no se puede grabar en SIVE_E_NOTIF
           porque no existe la clave en SIVE_ZONA_BASICA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_C_NOTIF  SIVE_E_NOTIF ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_C_NOTIF
      where
        /* %JoinFKPK(:%New,SIVE_C_NOTIF," = "," and") */
        :new.CD_CENTRO = SIVE_C_NOTIF.CD_CENTRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_E_NOTIF" because "SIVE_C_NOTIF" does not exist.'*/
          'no se puede grabar en SIVE_E_NOTIF
           porque no existe la clave en SIVE_C_NOTIF'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_E_NOTIF_TRIG_A_U_1 after UPDATE on SIVE_E_NOTIF for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_E_NOTIF 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_E_NOTIF  SIVE_ALERTA_BROTES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF
  then
    select count(*) into numrows
      from SIVE_ALERTA_BROTES
      where
        /*  %JoinFKPK(SIVE_ALERTA_BROTES,:%Old," = "," and") */
        SIVE_ALERTA_BROTES.CD_E_NOTIF = :old.CD_E_NOTIF;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_E_NOTIF" because "SIVE_ALERTA_BROTES" exists.'*/
       'no se puede modificar SIVE_E_NOTIF
        porque hay registros en SIVE_ALERTA_BROTES con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_E_NOTIF  SIVE_MCENTINELA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF
  then
    select count(*) into numrows
      from SIVE_MCENTINELA
      where
        /*  %JoinFKPK(SIVE_MCENTINELA,:%Old," = "," and") */
        SIVE_MCENTINELA.CD_E_NOTIF = :old.CD_E_NOTIF;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_E_NOTIF" because "SIVE_MCENTINELA" exists.'*/
       'no se puede modificar SIVE_E_NOTIF
        porque hay registros en SIVE_MCENTINELA con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_E_NOTIF  SIVE_NOTIF_SEM ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF
  then
    select count(*) into numrows
      from SIVE_NOTIF_SEM
      where
        /*  %JoinFKPK(SIVE_NOTIF_SEM,:%Old," = "," and") */
        SIVE_NOTIF_SEM.CD_E_NOTIF = :old.CD_E_NOTIF;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_E_NOTIF" because "SIVE_NOTIF_SEM" exists.'*/
       'no se puede modificar SIVE_E_NOTIF
        porque hay registros en SIVE_NOTIF_SEM con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_E_NOTIF  SIVE_REGISTROTBC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF
  then
    select count(*) into numrows
      from SIVE_REGISTROTBC
      where
        /*  %JoinFKPK(SIVE_REGISTROTBC,:%Old," = "," and") */
        SIVE_REGISTROTBC.CD_E_NOTIF = :old.CD_E_NOTIF;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_E_NOTIF" because "SIVE_REGISTROTBC" exists.'*/
       'no se puede modificar SIVE_E_NOTIF
        porque hay registros en SIVE_REGISTROTBC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_E_NOTIF  SIVE_USUARIO_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF
  then
    select count(*) into numrows
      from SIVE_USUARIO_BROTE
      where
        /*  %JoinFKPK(SIVE_USUARIO_BROTE,:%Old," = "," and") */
        SIVE_USUARIO_BROTE.CD_E_NOTIF = :old.CD_E_NOTIF;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_E_NOTIF" because "SIVE_USUARIO_BROTE" exists.'*/
       'no se puede modificar SIVE_E_NOTIF
        porque hay registros en SIVE_USUARIO_BROTE con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_E_NOTIF R_269 SIVE_USUARIO_PISTA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF
  then
    select count(*) into numrows
      from SIVE_USUARIO_PISTA
      where
        /*  %JoinFKPK(SIVE_USUARIO_PISTA,:%Old," = "," and") */
        SIVE_USUARIO_PISTA.CD_E_NOTIF = :old.CD_E_NOTIF;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_E_NOTIF" because "SIVE_USUARIO_PISTA" exists.'*/
       'no se puede modificar SIVE_E_NOTIF
        porque hay registros en SIVE_USUARIO_PISTA con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_SEMANA_EPI  SIVE_E_NOTIF ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEMANA_EPI
    where
      /* %JoinFKPK(:%New,SIVE_SEMANA_EPI," = "," and") */
      :new.CD_ANOEPI = SIVE_SEMANA_EPI.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_SEMANA_EPI.CD_SEMEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_E_NOTIF" because "SIVE_SEMANA_EPI" does not exist.'*/
         'no se puede actualizar SIVE_E_NOTIF
          porque no existe la clave en SIVE_SEMANA_EPI'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ZONA_BASICA  SIVE_E_NOTIF ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ZONA_BASICA
    where
      /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
      :new.CD_NIVEL_1 = SIVE_ZONA_BASICA.CD_NIVEL_1 and
      :new.CD_NIVEL_2 = SIVE_ZONA_BASICA.CD_NIVEL_2 and
      :new.CD_ZBS = SIVE_ZONA_BASICA.CD_ZBS;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_E_NOTIF" because "SIVE_ZONA_BASICA" does not exist.'*/
         'no se puede actualizar SIVE_E_NOTIF
          porque no existe la clave en SIVE_ZONA_BASICA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_C_NOTIF  SIVE_E_NOTIF ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_C_NOTIF
    where
      /* %JoinFKPK(:%New,SIVE_C_NOTIF," = "," and") */
      :new.CD_CENTRO = SIVE_C_NOTIF.CD_CENTRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_E_NOTIF" because "SIVE_C_NOTIF" does not exist.'*/
         'no se puede actualizar SIVE_E_NOTIF
          porque no existe la clave en SIVE_C_NOTIF'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_EDO_CNE_TRIG_A_D_1 after DELETE on SIVE_EDO_CNE for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_EDO_CNE 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_EDO_CNE R_158 SIVE_ENFEREDO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ENFEREDO
      where
        /*  %JoinFKPK(SIVE_ENFEREDO,:%Old," = "," and") */
        SIVE_ENFEREDO.CD_ENFERE = :old.CD_ENFEREDO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_EDO_CNE" because "SIVE_ENFEREDO" exists.'*/
       'no se puede borrar de SIVE_EDO_CNE
        porque hay registros en SIVE_ENFEREDO con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_EDO_CNE_TRIG_A_U_1 after UPDATE on SIVE_EDO_CNE for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_EDO_CNE 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_EDO_CNE R_158 SIVE_ENFEREDO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFEREDO <> :new.CD_ENFEREDO
  then
    select count(*) into numrows
      from SIVE_ENFEREDO
      where
        /*  %JoinFKPK(SIVE_ENFEREDO,:%Old," = "," and") */
        SIVE_ENFEREDO.CD_ENFERE = :old.CD_ENFEREDO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_EDO_CNE" because "SIVE_ENFEREDO" exists.'*/
       'no se puede modificar SIVE_EDO_CNE
        porque hay registros en SIVE_ENFEREDO con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_EDO_DADIC_TRIG_A_D_1 after DELETE on SIVE_EDO_DADIC for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_EDO_DADIC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_EDO_DADIC R/268 SIVE_RESP_ADIC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESP_ADIC
      where
        /*  %JoinFKPK(SIVE_RESP_ADIC,:%Old," = "," and") */
        SIVE_RESP_ADIC.CD_ENFCIE = :old.CD_ENFCIE and
        SIVE_RESP_ADIC.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_RESP_ADIC.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_RESP_ADIC.SEQ_CODIGO = :old.SEQ_CODIGO and
        SIVE_RESP_ADIC.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_RESP_ADIC.FC_RECEP = :old.FC_RECEP and
        SIVE_RESP_ADIC.FC_FECNOTIF = :old.FC_FECNOTIF;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_EDO_DADIC" because "SIVE_RESP_ADIC" exists.'*/
       'no se puede borrar de SIVE_EDO_DADIC
        porque hay registros en SIVE_RESP_ADIC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_EDO_DADIC_TRIG_A_I_1 after INSERT on SIVE_EDO_DADIC for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_EDO_DADIC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENFEREDO R/273 SIVE_EDO_DADIC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ENFEREDO
      where
        /* %JoinFKPK(:%New,SIVE_ENFEREDO," = "," and") */
        :new.CD_ENFCIE = SIVE_ENFEREDO.CD_ENFCIE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_EDO_DADIC" because "SIVE_ENFEREDO" does not exist.'*/
          'no se puede grabar en SIVE_EDO_DADIC
           porque no existe la clave en SIVE_ENFEREDO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_EDONUM R/269 SIVE_EDO_DADIC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_EDONUM
      where
        /* %JoinFKPK(:%New,SIVE_EDONUM," = "," and") */
        :new.CD_ENFCIE = SIVE_EDONUM.CD_ENFCIE and
        :new.CD_SEMEPI = SIVE_EDONUM.CD_SEMEPI and
        :new.CD_ANOEPI = SIVE_EDONUM.CD_ANOEPI and
        :new.CD_E_NOTIF = SIVE_EDONUM.CD_E_NOTIF and
        :new.FC_RECEP = SIVE_EDONUM.FC_RECEP and
        :new.FC_FECNOTIF = SIVE_EDONUM.FC_FECNOTIF;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_EDO_DADIC" because "SIVE_EDONUM" does not exist.'*/
          'no se puede grabar en SIVE_EDO_DADIC
           porque no existe la clave en SIVE_EDONUM'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_EDO_DADIC_TRIG_A_U_1 after UPDATE on SIVE_EDO_DADIC for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_EDO_DADIC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_EDO_DADIC R/268 SIVE_RESP_ADIC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI or 
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.SEQ_CODIGO <> :new.SEQ_CODIGO or 
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF or 
    :old.FC_RECEP <> :new.FC_RECEP or 
    :old.FC_FECNOTIF <> :new.FC_FECNOTIF
  then
    select count(*) into numrows
      from SIVE_RESP_ADIC
      where
        /*  %JoinFKPK(SIVE_RESP_ADIC,:%Old," = "," and") */
        SIVE_RESP_ADIC.CD_ENFCIE = :old.CD_ENFCIE and
        SIVE_RESP_ADIC.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_RESP_ADIC.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_RESP_ADIC.SEQ_CODIGO = :old.SEQ_CODIGO and
        SIVE_RESP_ADIC.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_RESP_ADIC.FC_RECEP = :old.FC_RECEP and
        SIVE_RESP_ADIC.FC_FECNOTIF = :old.FC_FECNOTIF;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_EDO_DADIC" because "SIVE_RESP_ADIC" exists.'*/
       'no se puede modificar SIVE_EDO_DADIC
        porque hay registros en SIVE_RESP_ADIC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENFEREDO R/273 SIVE_EDO_DADIC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ENFEREDO
    where
      /* %JoinFKPK(:%New,SIVE_ENFEREDO," = "," and") */
      :new.CD_ENFCIE = SIVE_ENFEREDO.CD_ENFCIE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_EDO_DADIC" because "SIVE_ENFEREDO" does not exist.'*/
         'no se puede actualizar SIVE_EDO_DADIC
          porque no existe la clave en SIVE_ENFEREDO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_EDONUM R/269 SIVE_EDO_DADIC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_EDONUM
    where
      /* %JoinFKPK(:%New,SIVE_EDONUM," = "," and") */
      :new.CD_ENFCIE = SIVE_EDONUM.CD_ENFCIE and
      :new.CD_SEMEPI = SIVE_EDONUM.CD_SEMEPI and
      :new.CD_ANOEPI = SIVE_EDONUM.CD_ANOEPI and
      :new.CD_E_NOTIF = SIVE_EDONUM.CD_E_NOTIF and
      :new.FC_RECEP = SIVE_EDONUM.FC_RECEP and
      :new.FC_FECNOTIF = SIVE_EDONUM.FC_FECNOTIF;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_EDO_DADIC" because "SIVE_EDONUM" does not exist.'*/
         'no se puede actualizar SIVE_EDO_DADIC
          porque no existe la clave en SIVE_EDONUM'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

create trigger SIVE_EDOIND_TRIG_A_DIU
  AFTER DELETE or INSERT or UPDATE
  on SIVE_EDOIND
  
  for each row
DECLARE 
   w_CDTPOPER_LOGICO varchar2(2); 
   w_CDTPOPER_FISICA varchar2(2); 
   w_CDCLAVE_VALOR   varchar2(20); 
   w_CDIDENT         varchar2(20); 
   w_DSPROCESO       varchar2(60); 
   w_Estado          number; 
   w_CD_ENFERMO      number; 
BEGIN 
   --------------------------------------------------------------------- 
   --                                                                 -- 
   --------------------------------------------------------------------- 
   w_DSPROCESO := 'TRIGGER SIVE_EDOIND_TRIG_A_DIU'; 
   -- 
   IF DELETING THEN 
      -- 
      w_CDTPOPER_LOGICO := 'B'; 
      w_CDTPOPER_FISICA := 'B'; 
      w_CD_ENFERMO       := :old.CD_ENFERMO; 
      -- 
   ELSIF INSERTING THEN 
         -- 
         w_CDTPOPER_LOGICO := 'A'; 
         w_CDTPOPER_FISICA := 'A'; 
         w_CD_ENFERMO       := :new.CD_ENFERMO; 
         -- 
   ELSIF UPDATING  THEN 
         -- 
         w_CDTPOPER_LOGICO := 'M'; 
         w_CDTPOPER_FISICA := 'M'; 
         w_CD_ENFERMO       := :old.CD_ENFERMO; 
         -- 
   ELSE /* La manipulaci�n es un UPDATE general */ 
         null; 
   END IF; 
   -------------------------------------------------------------------- 
   -- Obtenemos datos a registrar 
   -------------------------------------------------------------------- 
   w_CDCLAVE_VALOR := lpad (to_char (w_CD_ENFERMO),6,'0'); 
   select DS_NDOC 
     into w_CDIDENT 
     from SIVE_ENFERMO 
     where CD_ENFERMO = w_CD_ENFERMO; 
   -------------------------------------------------------------------- 
   -- Crear Registrar Registro de Log a PIPE. 
   -------------------------------------------------------------------- 
   w_Estado := dba_sive.sive_paq_log.crear_reg_log( 
                 vi_cod_aplicacion    => 'SIVE', 
                 vi_dsfich_logico     => 'SIVE_EDOIND', /* DSFICH_LOGICO: Fichero L�gico declarado en la APD */ 
                 vi_cdtpoper_logico   => w_CDTPOPER_LOGICO, 
                 vi_cdtpacceso_logico => 'I', 
                 vi_dstabla_fisica    => 'SIVE_EDOIND', 
                 vi_cdtpoper_fisica   => w_CDTPOPER_FISICA, 
                 vi_cdclave           => 'Cod. Enfermo.', /* CDCLAVE: Clave de Acceso */ 
                 vi_cdclave_valor     => w_CDCLAVE_VALOR, 
                 vi_cdident           => w_CDIDENT, 
                 vi_cdidentdup        => null,  /* CDIDENTDUP: Duplicado de Identificaci�n */ 
                 vi_cdproceso         => null,  /* CDPROCESO: C�digo del proceso */ 
                 vi_dsproceso         => w_DSPROCESO, /* DSPROCESO: Descripci�n del proceso */ 
                 vi_DSOBSERV          => null ); 
   -- 
END;
/



CREATE OR REPLACE TRIGGER SIVE_EDONUM_TRIG_A_D_1 after DELETE on SIVE_EDONUM for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_EDONUM 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_EDONUM R/269 SIVE_EDO_DADIC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_EDO_DADIC
      where
        /*  %JoinFKPK(SIVE_EDO_DADIC,:%Old," = "," and") */
        SIVE_EDO_DADIC.CD_ENFCIE = :old.CD_ENFCIE and
        SIVE_EDO_DADIC.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_EDO_DADIC.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_EDO_DADIC.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_EDO_DADIC.FC_RECEP = :old.FC_RECEP and
        SIVE_EDO_DADIC.FC_FECNOTIF = :old.FC_FECNOTIF;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_EDONUM" because "SIVE_EDO_DADIC" exists.'*/
       'no se puede borrar de SIVE_EDONUM
        porque hay registros en SIVE_EDO_DADIC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_EDONUM_TRIG_A_I_1 after INSERT on SIVE_EDONUM for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_EDONUM 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENFEREDO R/319 SIVE_EDONUM ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ENFEREDO
      where
        /* %JoinFKPK(:%New,SIVE_ENFEREDO," = "," and") */
        :new.CD_ENFCIE = SIVE_ENFEREDO.CD_ENFCIE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_EDONUM" because "SIVE_ENFEREDO" does not exist.'*/
          'no se puede grabar en SIVE_EDONUM
           porque no existe la clave en SIVE_ENFEREDO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENFEREDO R/271 SIVE_EDONUM ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ENFEREDO
      where
        /* %JoinFKPK(:%New,SIVE_ENFEREDO," = "," and") */
        :new.CD_ENFCIE = SIVE_ENFEREDO.CD_ENFCIE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_EDONUM" because "SIVE_ENFEREDO" does not exist.'*/
          'no se puede grabar en SIVE_EDONUM
           porque no existe la clave en SIVE_ENFEREDO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_NOTIFEDO R/270 SIVE_EDONUM ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIFEDO
      where
        /* %JoinFKPK(:%New,SIVE_NOTIFEDO," = "," and") */
        :new.CD_E_NOTIF = SIVE_NOTIFEDO.CD_E_NOTIF and
        :new.CD_ANOEPI = SIVE_NOTIFEDO.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_NOTIFEDO.CD_SEMEPI and
        :new.FC_RECEP = SIVE_NOTIFEDO.FC_RECEP and
        :new.FC_FECNOTIF = SIVE_NOTIFEDO.FC_FECNOTIF;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_EDONUM" because "SIVE_NOTIFEDO" does not exist.'*/
          'no se puede grabar en SIVE_EDONUM
           porque no existe la clave en SIVE_NOTIFEDO'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_EDONUM_TRIG_A_U_1 after UPDATE on SIVE_EDONUM for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_EDONUM 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_EDONUM R/269 SIVE_EDO_DADIC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI or 
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF or 
    :old.FC_RECEP <> :new.FC_RECEP or 
    :old.FC_FECNOTIF <> :new.FC_FECNOTIF
  then
    select count(*) into numrows
      from SIVE_EDO_DADIC
      where
        /*  %JoinFKPK(SIVE_EDO_DADIC,:%Old," = "," and") */
        SIVE_EDO_DADIC.CD_ENFCIE = :old.CD_ENFCIE and
        SIVE_EDO_DADIC.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_EDO_DADIC.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_EDO_DADIC.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_EDO_DADIC.FC_RECEP = :old.FC_RECEP and
        SIVE_EDO_DADIC.FC_FECNOTIF = :old.FC_FECNOTIF;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_EDONUM" because "SIVE_EDO_DADIC" exists.'*/
       'no se puede modificar SIVE_EDONUM
        porque hay registros en SIVE_EDO_DADIC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENFEREDO R/319 SIVE_EDONUM ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ENFEREDO
    where
      /* %JoinFKPK(:%New,SIVE_ENFEREDO," = "," and") */
      :new.CD_ENFCIE = SIVE_ENFEREDO.CD_ENFCIE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_EDONUM" because "SIVE_ENFEREDO" does not exist.'*/
         'no se puede actualizar SIVE_EDONUM
          porque no existe la clave en SIVE_ENFEREDO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENFEREDO R/271 SIVE_EDONUM ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ENFEREDO
    where
      /* %JoinFKPK(:%New,SIVE_ENFEREDO," = "," and") */
      :new.CD_ENFCIE = SIVE_ENFEREDO.CD_ENFCIE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_EDONUM" because "SIVE_ENFEREDO" does not exist.'*/
         'no se puede actualizar SIVE_EDONUM
          porque no existe la clave en SIVE_ENFEREDO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_NOTIFEDO R/270 SIVE_EDONUM ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NOTIFEDO
    where
      /* %JoinFKPK(:%New,SIVE_NOTIFEDO," = "," and") */
      :new.CD_E_NOTIF = SIVE_NOTIFEDO.CD_E_NOTIF and
      :new.CD_ANOEPI = SIVE_NOTIFEDO.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_NOTIFEDO.CD_SEMEPI and
      :new.FC_RECEP = SIVE_NOTIFEDO.FC_RECEP and
      :new.FC_FECNOTIF = SIVE_NOTIFEDO.FC_FECNOTIF;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_EDONUM" because "SIVE_NOTIFEDO" does not exist.'*/
         'no se puede actualizar SIVE_EDONUM
          porque no existe la clave en SIVE_NOTIFEDO'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ENF_CENTI_TRIG_A_D_1 after DELETE on SIVE_ENF_CENTI for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_ENF_CENTI 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENF_CENTI  SIVE_IND_ALAR_RMC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_IND_ALAR_RMC
      where
        /*  %JoinFKPK(SIVE_IND_ALAR_RMC,:%Old," = "," and") */
        SIVE_IND_ALAR_RMC.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ENF_CENTI" because "SIVE_IND_ALAR_RMC" exists.'*/
       'no se puede borrar de SIVE_ENF_CENTI
        porque hay registros en SIVE_IND_ALAR_RMC con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENF_CENTI  SIVE_NOTIF_RMC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIF_RMC
      where
        /*  %JoinFKPK(SIVE_NOTIF_RMC,:%Old," = "," and") */
        SIVE_NOTIF_RMC.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ENF_CENTI" because "SIVE_NOTIF_RMC" exists.'*/
       'no se puede borrar de SIVE_ENF_CENTI
        porque hay registros en SIVE_NOTIF_RMC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ENF_CENTI_TRIG_A_I_1 after INSERT on SIVE_ENF_CENTI for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_ENF_CENTI 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_PROCESOS  SIVE_ENF_CENTI ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PROCESOS
      where
        /* %JoinFKPK(:%New,SIVE_PROCESOS," = "," and") */
        :new.CD_ENFCIE = SIVE_PROCESOS.CD_ENFCIE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ENF_CENTI" because "SIVE_PROCESOS" does not exist.'*/
          'no se puede grabar en SIVE_ENF_CENTI
           porque no existe la clave en SIVE_PROCESOS'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ENF_CENTI_TRIG_A_U_1 after UPDATE on SIVE_ENF_CENTI for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_ENF_CENTI 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENF_CENTI  SIVE_IND_ALAR_RMC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE
  then
    select count(*) into numrows
      from SIVE_IND_ALAR_RMC
      where
        /*  %JoinFKPK(SIVE_IND_ALAR_RMC,:%Old," = "," and") */
        SIVE_IND_ALAR_RMC.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ENF_CENTI" because "SIVE_IND_ALAR_RMC" exists.'*/
       'no se puede modificar SIVE_ENF_CENTI
        porque hay registros en SIVE_IND_ALAR_RMC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENF_CENTI  SIVE_NOTIF_RMC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE
  then
    select count(*) into numrows
      from SIVE_NOTIF_RMC
      where
        /*  %JoinFKPK(SIVE_NOTIF_RMC,:%Old," = "," and") */
        SIVE_NOTIF_RMC.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ENF_CENTI" because "SIVE_NOTIF_RMC" exists.'*/
       'no se puede modificar SIVE_ENF_CENTI
        porque hay registros en SIVE_NOTIF_RMC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_PROCESOS  SIVE_ENF_CENTI ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PROCESOS
    where
      /* %JoinFKPK(:%New,SIVE_PROCESOS," = "," and") */
      :new.CD_ENFCIE = SIVE_PROCESOS.CD_ENFCIE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ENF_CENTI" because "SIVE_PROCESOS" does not exist.'*/
         'no se puede actualizar SIVE_ENF_CENTI
          porque no existe la clave en SIVE_PROCESOS'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ENFER_CIE_TRIG_A_D_1 after DELETE on SIVE_ENFER_CIE for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_ENFER_CIE 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENFER_CIE  SIVE_PREG_FIJA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_PREG_FIJA
      where
        /*  %JoinFKPK(SIVE_PREG_FIJA,:%Old," = "," and") */
        SIVE_PREG_FIJA.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ENFER_CIE" because "SIVE_PREG_FIJA" exists.'*/
       'no se puede borrar de SIVE_ENFER_CIE
        porque hay registros en SIVE_PREG_FIJA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ENFER_CIE_TRIG_A_U_1 after UPDATE on SIVE_ENFER_CIE for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_ENFER_CIE 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENFER_CIE  SIVE_PREG_FIJA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE
  then
    select count(*) into numrows
      from SIVE_PREG_FIJA
      where
        /*  %JoinFKPK(SIVE_PREG_FIJA,:%Old," = "," and") */
        SIVE_PREG_FIJA.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ENFER_CIE" because "SIVE_PREG_FIJA" exists.'*/
       'no se puede modificar SIVE_ENFER_CIE
        porque hay registros en SIVE_PREG_FIJA con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ENFEREDO_TRIG_A_D_1 after DELETE on SIVE_ENFEREDO for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_ENFEREDO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENFEREDO R/319 SIVE_EDONUM ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_EDONUM
      where
        /*  %JoinFKPK(SIVE_EDONUM,:%Old," = "," and") */
        SIVE_EDONUM.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ENFEREDO" because "SIVE_EDONUM" exists.'*/
       'no se puede borrar de SIVE_ENFEREDO
        porque hay registros en SIVE_EDONUM con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENFEREDO R/275 SIVE_INDICADOR ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_INDICADOR
      where
        /*  %JoinFKPK(SIVE_INDICADOR,:%Old," = "," and") */
        SIVE_INDICADOR.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ENFEREDO" because "SIVE_INDICADOR" exists.'*/
       'no se puede borrar de SIVE_ENFEREDO
        porque hay registros en SIVE_INDICADOR con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENFEREDO R/273 SIVE_EDO_DADIC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_EDO_DADIC
      where
        /*  %JoinFKPK(SIVE_EDO_DADIC,:%Old," = "," and") */
        SIVE_EDO_DADIC.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ENFEREDO" because "SIVE_EDO_DADIC" exists.'*/
       'no se puede borrar de SIVE_ENFEREDO
        porque hay registros en SIVE_EDO_DADIC con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENFEREDO R/272 SIVE_RESUMEN_EDOS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESUMEN_EDOS
      where
        /*  %JoinFKPK(SIVE_RESUMEN_EDOS,:%Old," = "," and") */
        SIVE_RESUMEN_EDOS.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ENFEREDO" because "SIVE_RESUMEN_EDOS" exists.'*/
       'no se puede borrar de SIVE_ENFEREDO
        porque hay registros en SIVE_RESUMEN_EDOS con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENFEREDO R/271 SIVE_EDONUM ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_EDONUM
      where
        /*  %JoinFKPK(SIVE_EDONUM,:%Old," = "," and") */
        SIVE_EDONUM.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ENFEREDO" because "SIVE_EDONUM" exists.'*/
       'no se puede borrar de SIVE_ENFEREDO
        porque hay registros en SIVE_EDONUM con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENFEREDO  SIVE_EDOIND ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_EDOIND
      where
        /*  %JoinFKPK(SIVE_EDOIND,:%Old," = "," and") */
        SIVE_EDOIND.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ENFEREDO" because "SIVE_EDOIND" exists.'*/
       'no se puede borrar de SIVE_ENFEREDO
        porque hay registros en SIVE_EDOIND con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ENFEREDO_TRIG_A_I_1 after INSERT on SIVE_ENFEREDO for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_ENFEREDO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_PROCESOS R/298 SIVE_ENFEREDO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PROCESOS
      where
        /* %JoinFKPK(:%New,SIVE_PROCESOS," = "," and") */
        :new.CD_ENFCIE = SIVE_PROCESOS.CD_ENFCIE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ENFEREDO" because "SIVE_PROCESOS" does not exist.'*/
          'no se puede grabar en SIVE_ENFEREDO
           porque no existe la clave en SIVE_PROCESOS'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_EDO_CNE R_158 SIVE_ENFEREDO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_EDO_CNE
      where
        /* %JoinFKPK(:%New,SIVE_EDO_CNE," = "," and") */
        :new.CD_ENFERE = SIVE_EDO_CNE.CD_ENFEREDO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ENFEREDO" because "SIVE_EDO_CNE" does not exist.'*/
          'no se puede grabar en SIVE_ENFEREDO
           porque no existe la clave en SIVE_EDO_CNE'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_T_VIGILANCIA R_131 SIVE_ENFEREDO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_T_VIGILANCIA
      where
        /* %JoinFKPK(:%New,SIVE_T_VIGILANCIA," = "," and") */
        :new.CD_TVIGI = SIVE_T_VIGILANCIA.CD_TVIGI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ENFEREDO" because "SIVE_T_VIGILANCIA" does not exist.'*/
          'no se puede grabar en SIVE_ENFEREDO
           porque no existe la clave en SIVE_T_VIGILANCIA'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ENFEREDO_TRIG_A_U_1 after UPDATE on SIVE_ENFEREDO for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_ENFEREDO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENFEREDO R/319 SIVE_EDONUM ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE
  then
    select count(*) into numrows
      from SIVE_EDONUM
      where
        /*  %JoinFKPK(SIVE_EDONUM,:%Old," = "," and") */
        SIVE_EDONUM.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ENFEREDO" because "SIVE_EDONUM" exists.'*/
       'no se puede modificar SIVE_ENFEREDO
        porque hay registros en SIVE_EDONUM con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENFEREDO R/275 SIVE_INDICADOR ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE
  then
    select count(*) into numrows
      from SIVE_INDICADOR
      where
        /*  %JoinFKPK(SIVE_INDICADOR,:%Old," = "," and") */
        SIVE_INDICADOR.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ENFEREDO" because "SIVE_INDICADOR" exists.'*/
       'no se puede modificar SIVE_ENFEREDO
        porque hay registros en SIVE_INDICADOR con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENFEREDO R/273 SIVE_EDO_DADIC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE
  then
    select count(*) into numrows
      from SIVE_EDO_DADIC
      where
        /*  %JoinFKPK(SIVE_EDO_DADIC,:%Old," = "," and") */
        SIVE_EDO_DADIC.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ENFEREDO" because "SIVE_EDO_DADIC" exists.'*/
       'no se puede modificar SIVE_ENFEREDO
        porque hay registros en SIVE_EDO_DADIC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENFEREDO R/272 SIVE_RESUMEN_EDOS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE
  then
    select count(*) into numrows
      from SIVE_RESUMEN_EDOS
      where
        /*  %JoinFKPK(SIVE_RESUMEN_EDOS,:%Old," = "," and") */
        SIVE_RESUMEN_EDOS.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ENFEREDO" because "SIVE_RESUMEN_EDOS" exists.'*/
       'no se puede modificar SIVE_ENFEREDO
        porque hay registros en SIVE_RESUMEN_EDOS con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENFEREDO R/271 SIVE_EDONUM ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE
  then
    select count(*) into numrows
      from SIVE_EDONUM
      where
        /*  %JoinFKPK(SIVE_EDONUM,:%Old," = "," and") */
        SIVE_EDONUM.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ENFEREDO" because "SIVE_EDONUM" exists.'*/
       'no se puede modificar SIVE_ENFEREDO
        porque hay registros en SIVE_EDONUM con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENFEREDO  SIVE_EDOIND ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE
  then
    select count(*) into numrows
      from SIVE_EDOIND
      where
        /*  %JoinFKPK(SIVE_EDOIND,:%Old," = "," and") */
        SIVE_EDOIND.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ENFEREDO" because "SIVE_EDOIND" exists.'*/
       'no se puede modificar SIVE_ENFEREDO
        porque hay registros en SIVE_EDOIND con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_PROCESOS R/298 SIVE_ENFEREDO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PROCESOS
    where
      /* %JoinFKPK(:%New,SIVE_PROCESOS," = "," and") */
      :new.CD_ENFCIE = SIVE_PROCESOS.CD_ENFCIE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ENFEREDO" because "SIVE_PROCESOS" does not exist.'*/
         'no se puede actualizar SIVE_ENFEREDO
          porque no existe la clave en SIVE_PROCESOS'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_EDO_CNE R_158 SIVE_ENFEREDO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_EDO_CNE
    where
      /* %JoinFKPK(:%New,SIVE_EDO_CNE," = "," and") */
      :new.CD_ENFERE = SIVE_EDO_CNE.CD_ENFEREDO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ENFEREDO" because "SIVE_EDO_CNE" does not exist.'*/
         'no se puede actualizar SIVE_ENFEREDO
          porque no existe la clave en SIVE_EDO_CNE'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_T_VIGILANCIA R_131 SIVE_ENFEREDO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_T_VIGILANCIA
    where
      /* %JoinFKPK(:%New,SIVE_T_VIGILANCIA," = "," and") */
      :new.CD_TVIGI = SIVE_T_VIGILANCIA.CD_TVIGI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ENFEREDO" because "SIVE_T_VIGILANCIA" does not exist.'*/
         'no se puede actualizar SIVE_ENFEREDO
          porque no existe la clave en SIVE_T_VIGILANCIA'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

create trigger SIVE_ENFERMO_TRIG_A_DIU
  AFTER DELETE or INSERT or UPDATE
  on SIVE_ENFERMO
  
  for each row
DECLARE 
   w_CDTPOPER_LOGICO varchar2(2); 
   w_CDTPOPER_FISICA varchar2(2); 
   w_CDCLAVE_VALOR   varchar2(20); 
   w_CDIDENT         varchar2(20); 
   w_DSPROCESO       varchar2(60); 
   w_Estado          number; 
	w_CD_ENFERMO      number; 
BEGIN 
   --------------------------------------------------------------------- 
   -- DS_NDOC: Es el DNI o Tarjeta Sanitaria                          -- 
   --------------------------------------------------------------------- 
   w_DSPROCESO := 'TRIGGER SIVE_ENFERMO_TRIG_A_DIU'; 
   -- 
   IF DELETING THEN 
      -- 
      w_CDTPOPER_LOGICO := 'B'; 
      w_CDTPOPER_FISICA := 'B'; 
      w_CD_ENFERMO      := :old.CD_ENFERMO; 
		w_CDIDENT			:= :old.DS_NDOC;  
      -- 
   ELSIF INSERTING THEN 
         -- 
         w_CDTPOPER_LOGICO := 'A'; 
         w_CDTPOPER_FISICA := 'A'; 
         w_CD_ENFERMO      := :new.CD_ENFERMO; 
			w_CDIDENT         := :new.DS_NDOC; 
         -- 
   ELSIF UPDATING  THEN 
         -- 
         w_CDTPOPER_LOGICO := 'M'; 
         w_CDTPOPER_FISICA := 'M'; 
         w_CD_ENFERMO      := :old.CD_ENFERMO; 
			w_CDIDENT         := :old.DS_NDOC; 
         -- 
   ELSE /* La manipulaci�n es un UPDATE general */ 
         null; 
   END IF; 
   -------------------------------------------------------------------- 
   -- Obtenemos datos a registrar 
   -------------------------------------------------------------------- 
   w_CDCLAVE_VALOR := lpad (to_char (w_CD_ENFERMO),6,'0'); 
 
   -------------------------------------------------------------------- 
   -- Crear Registrar Registro de Log a PIPE. 
   -------------------------------------------------------------------- 
   w_Estado := dba_sive.sive_paq_log.crear_reg_log( 
                 vi_cod_aplicacion    => 'SIVE', 
                 vi_dsfich_logico     => 'SIVE_ENFERMO', /* DSFICH_LOGICO: Fichero L�gico declarado en la APD */ 
                 vi_cdtpoper_logico   => w_CDTPOPER_LOGICO, 
                 vi_cdtpacceso_logico => 'I', 
                 vi_dstabla_fisica    => 'SIVE_ENFERMO', 
                 vi_cdtpoper_fisica   => w_CDTPOPER_FISICA, 
                 vi_cdclave           => 'Cod. Enfermo.', /* CDCLAVE: Clave de Acceso */ 
                 vi_cdclave_valor     => w_CDCLAVE_VALOR, 
                 vi_cdident           => w_CDIDENT, 
                 vi_cdidentdup        => null,  /* CDIDENTDUP: Duplicado de Identificaci�n */ 
                 vi_cdproceso         => null,  /* CDPROCESO: C�digo del proceso */ 
                 vi_dsproceso         => w_DSPROCESO, /* DSPROCESO: Descripci�n del proceso */ 
                 vi_DSOBSERV          => null ); 
   -- 
END;
/



CREATE OR REPLACE TRIGGER SIVE_ENV_OTRCOM_TRIG_A_D_1 after DELETE on SIVE_ENV_OTRCOM for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_ENV_OTRCOM 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENV_OTRCOM  SIVE_EDOIND ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_EDOIND
      where
        /*  %JoinFKPK(SIVE_EDOIND,:%Old," = "," and") */
        SIVE_EDOIND.CD_ANOOTRC = :old.CD_ANOOTRC and
        SIVE_EDOIND.NM_ENVOTRC = :old.NM_ENVOTRC;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ENV_OTRCOM" because "SIVE_EDOIND" exists.'*/
       'no se puede borrar de SIVE_ENV_OTRCOM
        porque hay registros en SIVE_EDOIND con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ENV_OTRCOM_TRIG_A_U_1 after UPDATE on SIVE_ENV_OTRCOM for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_ENV_OTRCOM 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENV_OTRCOM  SIVE_EDOIND ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANOOTRC <> :new.CD_ANOOTRC or 
    :old.NM_ENVOTRC <> :new.NM_ENVOTRC
  then
    select count(*) into numrows
      from SIVE_EDOIND
      where
        /*  %JoinFKPK(SIVE_EDOIND,:%Old," = "," and") */
        SIVE_EDOIND.CD_ANOOTRC = :old.CD_ANOOTRC and
        SIVE_EDOIND.NM_ENVOTRC = :old.NM_ENVOTRC;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ENV_OTRCOM" because "SIVE_EDOIND" exists.'*/
       'no se puede modificar SIVE_ENV_OTRCOM
        porque hay registros en SIVE_EDOIND con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ENVURG_TRIG_A_D_1 after DELETE on SIVE_ENVURG for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_ENVURG 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ENVURG  SIVE_EDOIND ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_EDOIND
      where
        /*  %JoinFKPK(SIVE_EDOIND,:%Old," = "," and") */
        SIVE_EDOIND.CD_ANOURG = :old.CD_ANOURG and
        SIVE_EDOIND.NM_ENVIOURGSEM = :old.NM_ENVIOURGSEM;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ENVURG" because "SIVE_EDOIND" exists.'*/
       'no se puede borrar de SIVE_ENVURG
        porque hay registros en SIVE_EDOIND con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ENVURG_TRIG_A_U_1 after UPDATE on SIVE_ENVURG for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_ENVURG 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ENVURG  SIVE_EDOIND ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANOURG <> :new.CD_ANOURG or 
    :old.NM_ENVIOURGSEM <> :new.NM_ENVIOURGSEM
  then
    select count(*) into numrows
      from SIVE_EDOIND
      where
        /*  %JoinFKPK(SIVE_EDOIND,:%Old," = "," and") */
        SIVE_EDOIND.CD_ANOURG = :old.CD_ANOURG and
        SIVE_EDOIND.NM_ENVIOURGSEM = :old.NM_ENVIOURGSEM;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ENVURG" because "SIVE_EDOIND" exists.'*/
       'no se puede modificar SIVE_ENVURG
        porque hay registros en SIVE_EDOIND con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

create trigger SIVE_ESPECI_URGEN_TRIG_A_U
  AFTER UPDATE
  on SIVE_ESPECI_URGENCIA
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_ESPECI_URGENCIA R/5 SIVE_ESPECIALIDAD ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ESPEURG <> :new.CD_ESPEURG
  then
    select count(*) into numrows
      from SIVE_ESPECIALIDAD
      where
        /*  %JoinFKPK(SIVE_ESPECIALIDAD,:%Old," = "," and") */
        SIVE_ESPECIALIDAD.CD_ESPEURG = :old.CD_ESPEURG;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_ESPECI_URGENCIA" debido a que existe "SIVE_ESPECIALIDAD".'
      );
    end if;
  end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_ESPECI_URGEN_TRIG_A_D
  AFTER DELETE
  on SIVE_ESPECI_URGENCIA
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_ESPECI_URGENCIA R/5 SIVE_ESPECIALIDAD ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ESPECIALIDAD
      where
        /*  %JoinFKPK(SIVE_ESPECIALIDAD,:%Old," = "," and") */
        SIVE_ESPECIALIDAD.CD_ESPEURG = :old.CD_ESPEURG;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_ESPECI_URGENCIA" debido a que existe en "SIVE_ESPECIALIDAD".'
      );
    end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


CREATE OR REPLACE TRIGGER SIVE_ESPECIALIDAD_TRIG_A_D_1 after DELETE on SIVE_ESPECIALIDAD for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_ESPECIALIDAD 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ESPECIALIDAD R/344 SIVE_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_URGENCIAS,:%Old," = "," and") */
        SIVE_URGENCIAS.CD_ESPECI = :old.CD_ESPECI and
        SIVE_URGENCIAS.CD_HOSP = :old.CD_HOSP;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ESPECIALIDAD" because "SIVE_URGENCIAS" exists.'*/
       'no se puede borrar de ESPECIALIDAD
        porque hay registros en SIVE_URGENCIAS con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ESPECIALIDAD R/343 SIVE_MOV_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MOV_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_MOV_URGENCIAS,:%Old," = "," and") */
        SIVE_MOV_URGENCIAS.CD_ESPECI = :old.CD_ESPECI and
        SIVE_MOV_URGENCIAS.CD_HOSP = :old.CD_HOSP;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ESPECIALIDAD" because "SIVE_MOV_URGENCIAS" exists.'*/
       'no se puede borrar de ESPECIALIDAD
        porque hay registros en SIVE_MOV_URGENCIAS con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ESPECIALIDAD_TRIG_A_I_1 after INSERT on SIVE_ESPECIALIDAD for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_ESPECIALIDAD 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ESPECI_URGENCIA R/342 SIVE_ESPECIALIDAD ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ESPECI_URGENCIA
      where
        /* %JoinFKPK(:%New,SIVE_ESPECI_URGENCIA," = "," and") */
        :new.CD_ESPEURG = SIVE_ESPECI_URGENCIA.CD_ESPEURG;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ESPECIALIDAD" because "SIVE_ESPECI_URGENCIA" does not exist.'*/
          'no se puede grabar en ESPECIALIDAD
           porque no existe la clave en SIVE_ESPECI_URGENCIA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_C_HOSP R/332 SIVE_ESPECIALIDAD ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_C_HOSP
      where
        /* %JoinFKPK(:%New,SIVE_C_HOSP," = "," and") */
        :new.CD_HOSP = SIVE_C_HOSP.CD_HOSP;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_ESPECIALIDAD" because "SIVE_C_HOSP" does not exist.'*/
          'no se puede grabar en ESPECIALIDAD
           porque no existe la clave en SIVE_C_HOSP'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ESPECIALIDAD_TRIG_A_U_1 after UPDATE on SIVE_ESPECIALIDAD for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_ESPECIALIDAD 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ESPECIALIDAD R/344 SIVE_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ESPECI <> :new.CD_ESPECI or 
    :old.CD_HOSP <> :new.CD_HOSP
  then
    select count(*) into numrows
      from SIVE_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_URGENCIAS,:%Old," = "," and") */
        SIVE_URGENCIAS.CD_ESPECI = :old.CD_ESPECI and
        SIVE_URGENCIAS.CD_HOSP = :old.CD_HOSP;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ESPECIALIDAD" because "SIVE_URGENCIAS" exists.'*/
       'no se puede modificar ESPECIALIDAD
        porque hay registros en SIVE_URGENCIAS con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ESPECIALIDAD R/343 SIVE_MOV_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ESPECI <> :new.CD_ESPECI or 
    :old.CD_HOSP <> :new.CD_HOSP
  then
    select count(*) into numrows
      from SIVE_MOV_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_MOV_URGENCIAS,:%Old," = "," and") */
        SIVE_MOV_URGENCIAS.CD_ESPECI = :old.CD_ESPECI and
        SIVE_MOV_URGENCIAS.CD_HOSP = :old.CD_HOSP;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ESPECIALIDAD" because "SIVE_MOV_URGENCIAS" exists.'*/
       'no se puede modificar ESPECIALIDAD
        porque hay registros en SIVE_MOV_URGENCIAS con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ESPECI_URGENCIA R/342 SIVE_ESPECIALIDAD ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ESPECI_URGENCIA
    where
      /* %JoinFKPK(:%New,SIVE_ESPECI_URGENCIA," = "," and") */
      :new.CD_ESPEURG = SIVE_ESPECI_URGENCIA.CD_ESPEURG;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ESPECIALIDAD" because "SIVE_ESPECI_URGENCIA" does not exist.'*/
         'no se puede actualizar ESPECIALIDAD
          porque no existe la clave en SIVE_ESPECI_URGENCIA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_C_HOSP R/332 SIVE_ESPECIALIDAD ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_C_HOSP
    where
      /* %JoinFKPK(:%New,SIVE_C_HOSP," = "," and") */
      :new.CD_HOSP = SIVE_C_HOSP.CD_HOSP;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_ESPECIALIDAD" because "SIVE_C_HOSP" does not exist.'*/
         'no se puede actualizar ESPECIALIDAD
          porque no existe la clave en SIVE_C_HOSP'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ESTUDIORESIS_TRIG_A_D_1 after DELETE on SIVE_ESTUDIORESIS for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_ESTUDIORESIS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_ESTUDIORESIS  SIVE_RES_RESISTENCIA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RES_RESISTENCIA
      where
        /*  %JoinFKPK(SIVE_RES_RESISTENCIA,:%Old," = "," and") */
        SIVE_RES_RESISTENCIA.CD_ESTREST = :old.CD_ESTREST;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ESTUDIORESIS" because "SIVE_RES_RESISTENCIA" exists.'*/
       'no se puede borrar de SIVE_ESTUDIORESIS
        porque hay registros en SIVE_RES_RESISTENCIA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ESTUDIORESIS_TRIG_A_U_1 after UPDATE on SIVE_ESTUDIORESIS for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_ESTUDIORESIS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_ESTUDIORESIS  SIVE_RES_RESISTENCIA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ESTREST <> :new.CD_ESTREST
  then
    select count(*) into numrows
      from SIVE_RES_RESISTENCIA
      where
        /*  %JoinFKPK(SIVE_RES_RESISTENCIA,:%Old," = "," and") */
        SIVE_RES_RESISTENCIA.CD_ESTREST = :old.CD_ESTREST;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ESTUDIORESIS" because "SIVE_RES_RESISTENCIA" exists.'*/
       'no se puede modificar SIVE_ESTUDIORESIS
        porque hay registros en SIVE_RES_RESISTENCIA con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_F_INGERIR_ALI_TRIG_A_D_1 after DELETE on SIVE_F_INGERIR_ALI for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_F_INGERIR_ALI 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_F_INGERIR_ALI R/311 SIVE_ALI_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_FINGERIRALI = :old.CD_FINGERIRALI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_F_INGERIR_ALI" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede borrar de SIVE_F_INGERIR_ALI
        porque hay registros en SIVE_ALI_BROTE con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_F_INGERIR_ALI_TRIG_A_U_1 after UPDATE on SIVE_F_INGERIR_ALI for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_F_INGERIR_ALI 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_F_INGERIR_ALI R/311 SIVE_ALI_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_FINGERIRALI <> :new.CD_FINGERIRALI
  then
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_FINGERIRALI = :old.CD_FINGERIRALI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_F_INGERIR_ALI" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede modificar SIVE_F_INGERIR_ALI
        porque hay registros en SIVE_ALI_BROTE con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_FACT_CONTRIB_TRIG_A_D_1 after DELETE on SIVE_FACT_CONTRIB for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_FACT_CONTRIB 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_FACT_CONTRIB  SIVE_BROTES_FACCONT ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES_FACCONT
      where
        /*  %JoinFKPK(SIVE_BROTES_FACCONT,:%Old," = "," and") */
        SIVE_BROTES_FACCONT.CD_FACCONT = :old.CD_FACCONT and
        SIVE_BROTES_FACCONT.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_FACT_CONTRIB" because "SIVE_BROTES_FACCONT" exists.'*/
       'no se puede borrar de SIVE_FACT_CONTRIB
        porque hay registros en SIVE_BROTES_FACCONT con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_FACT_CONTRIB_TRIG_A_I_1 after INSERT on SIVE_FACT_CONTRIB for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_FACT_CONTRIB 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_GRUPO_BROTE  SIVE_FACT_CONTRIB ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_GRUPO_BROTE
      where
        /* %JoinFKPK(:%New,SIVE_GRUPO_BROTE," = "," and") */
        :new.CD_GRUPO = SIVE_GRUPO_BROTE.CD_GRUPO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_FACT_CONTRIB" because "SIVE_GRUPO_BROTE" does not exist.'*/
          'no se puede grabar en SIVE_FACT_CONTRIB
           porque no existe la clave en SIVE_GRUPO_BROTE'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_FACT_CONTRIB_TRIG_A_U_1 after UPDATE on SIVE_FACT_CONTRIB for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_FACT_CONTRIB 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_FACT_CONTRIB  SIVE_BROTES_FACCONT ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_FACCONT <> :new.CD_FACCONT or 
    :old.CD_GRUPO <> :new.CD_GRUPO
  then
    select count(*) into numrows
      from SIVE_BROTES_FACCONT
      where
        /*  %JoinFKPK(SIVE_BROTES_FACCONT,:%Old," = "," and") */
        SIVE_BROTES_FACCONT.CD_FACCONT = :old.CD_FACCONT and
        SIVE_BROTES_FACCONT.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_FACT_CONTRIB" because "SIVE_BROTES_FACCONT" exists.'*/
       'no se puede modificar SIVE_FACT_CONTRIB
        porque hay registros en SIVE_BROTES_FACCONT con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_GRUPO_BROTE  SIVE_FACT_CONTRIB ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_GRUPO_BROTE
    where
      /* %JoinFKPK(:%New,SIVE_GRUPO_BROTE," = "," and") */
      :new.CD_GRUPO = SIVE_GRUPO_BROTE.CD_GRUPO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_FACT_CONTRIB" because "SIVE_GRUPO_BROTE" does not exist.'*/
         'no se puede actualizar SIVE_FACT_CONTRIB
          porque no existe la clave en SIVE_GRUPO_BROTE'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_FACTOR_RIESGO_TRIG_A_D_1 after DELETE on SIVE_FACTOR_RIESGO for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_FACTOR_RIESGO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_FACTOR_RIESGO  SIVE_VALOR_FACRI ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_VALOR_FACRI
      where
        /*  %JoinFKPK(SIVE_VALOR_FACRI,:%Old," = "," and") */
        SIVE_VALOR_FACRI.NM_FACRI = :old.NM_FACRI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_FACTOR_RIESGO" because "SIVE_VALOR_FACRI" exists.'*/
       'no se puede borrar de SIVE_FACTOR_RIESGO
        porque hay registros en SIVE_VALOR_FACRI con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_FACTOR_RIESGO_TRIG_A_I_1 after INSERT on SIVE_FACTOR_RIESGO for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- INSERT trigger on SIVE_FACTOR_RIESGO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_PREGUNTA  SIVE_FACTOR_RIESGO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PREGUNTA
      where
        /* %JoinFKPK(:%New,SIVE_PREGUNTA," = "," and") */
        :new.CD_TSIVE = SIVE_PREGUNTA.CD_TSIVE and
        :new.CD_PREGUNTA = SIVE_PREGUNTA.CD_PREGUNTA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_TSIVE is not null and
      :new.CD_PREGUNTA is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_FACTOR_RIESGO" because "SIVE_PREGUNTA" does not exist.'*/
          'no se puede grabar en SIVE_FACTOR_RIESGO
           porque no existe la clave en SIVE_PREGUNTA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_BROTES  SIVE_FACTOR_RIESGO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
        :new.CD_ANO = SIVE_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_ANO is not null and
      :new.NM_ALERBRO is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_FACTOR_RIESGO" because "SIVE_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_FACTOR_RIESGO
           porque no existe la clave en SIVE_BROTES'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_FACTOR_RIESGO_TRIG_A_U_1 after UPDATE on SIVE_FACTOR_RIESGO for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_FACTOR_RIESGO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_FACTOR_RIESGO  SIVE_VALOR_FACRI ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.NM_FACRI <> :new.NM_FACRI
  then
    select count(*) into numrows
      from SIVE_VALOR_FACRI
      where
        /*  %JoinFKPK(SIVE_VALOR_FACRI,:%Old," = "," and") */
        SIVE_VALOR_FACRI.NM_FACRI = :old.NM_FACRI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_FACTOR_RIESGO" because "SIVE_VALOR_FACRI" exists.'*/
       'no se puede modificar SIVE_FACTOR_RIESGO
        porque hay registros en SIVE_VALOR_FACRI con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_PREGUNTA  SIVE_FACTOR_RIESGO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PREGUNTA
    where
      /* %JoinFKPK(:%New,SIVE_PREGUNTA," = "," and") */
      :new.CD_TSIVE = SIVE_PREGUNTA.CD_TSIVE and
      :new.CD_PREGUNTA = SIVE_PREGUNTA.CD_PREGUNTA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_TSIVE is not null and
    :new.CD_PREGUNTA is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_FACTOR_RIESGO" because "SIVE_PREGUNTA" does not exist.'*/
         'no se puede actualizar SIVE_FACTOR_RIESGO
          porque no existe la clave en SIVE_PREGUNTA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_BROTES  SIVE_FACTOR_RIESGO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
      :new.CD_ANO = SIVE_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_ANO is not null and
    :new.NM_ALERBRO is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_FACTOR_RIESGO" because "SIVE_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_FACTOR_RIESGO
          porque no existe la clave en SIVE_BROTES'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

create trigger SIVE_FICHEROS_URG_TRIG_A_U
  AFTER UPDATE
  on SIVE_FICHEROS_URG
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_C_HOSP R/35 SIVE_FICHEROS_URG ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_C_HOSP
    where
      /* %JoinFKPK(:%New,SIVE_C_HOSP," = "," and") */
      :new.CD_HOSP = SIVE_C_HOSP.CD_HOSP;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_FICHEROS_URG" debido a que no existe en "SIVE_C_HOSP".'
    );
  end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_FICHEROS_URG_TRIG_B_I
  BEFORE INSERT
  on SIVE_FICHEROS_URG
  
  for each row
declare numrows INTEGER;
begin
/* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_C_HOSP R/35 SIVE_FICHEROS_URG ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_C_HOSP
      where
        /* %JoinFKPK(:%New,SIVE_C_HOSP," = "," and") */
        :new.CD_HOSP = SIVE_C_HOSP.CD_HOSP;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_FICHEROS_URG" because a que no existe en "SIVE_C_HOSP".'
      );
    end if;




/* Secuenciador */
select SIVE_s_FICHERO.NextVal
into :new.NM_FICHERO
from Dual;



end;
/


CREATE OR REPLACE TRIGGER SIVE_FUENTES_NOTIF_TRIG_A_D_1 after DELETE on SIVE_FUENTES_NOTIF for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_FUENTES_NOTIF 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_FUENTES_NOTIF  SIVE_NOTIF_EDOI ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIF_EDOI
      where
        /*  %JoinFKPK(SIVE_NOTIF_EDOI,:%Old," = "," and") */
        SIVE_NOTIF_EDOI.CD_FUENTE = :old.CD_FUENTE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_FUENTES_NOTIF" because "SIVE_NOTIF_EDOI" exists.'*/
       'no se puede borrar de SIVE_FUENTES_NOTIF
        porque hay registros en SIVE_NOTIF_EDOI con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_FUENTES_NOTIF_TRIG_A_U_1 after UPDATE on SIVE_FUENTES_NOTIF for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_FUENTES_NOTIF 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_FUENTES_NOTIF  SIVE_NOTIF_EDOI ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_FUENTE <> :new.CD_FUENTE
  then
    select count(*) into numrows
      from SIVE_NOTIF_EDOI
      where
        /*  %JoinFKPK(SIVE_NOTIF_EDOI,:%Old," = "," and") */
        SIVE_NOTIF_EDOI.CD_FUENTE = :old.CD_FUENTE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_FUENTES_NOTIF" because "SIVE_NOTIF_EDOI" exists.'*/
       'no se puede modificar SIVE_FUENTES_NOTIF
        porque hay registros en SIVE_NOTIF_EDOI con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_GEDAD_TRIG_A_D_1 after DELETE on SIVE_GEDAD for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_GEDAD 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_GEDAD  SIVE_DISTR_GEDAD ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_DISTR_GEDAD
      where
        /*  %JoinFKPK(SIVE_DISTR_GEDAD,:%Old," = "," and") */
        SIVE_DISTR_GEDAD.CD_GEDAD = :old.CD_GEDAD;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_GEDAD" because "SIVE_DISTR_GEDAD" exists.'*/
       'no se puede borrar de SIVE_GEDAD
        porque hay registros en SIVE_DISTR_GEDAD con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_GEDAD_TRIG_A_U_1 after UPDATE on SIVE_GEDAD for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_GEDAD 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_GEDAD  SIVE_DISTR_GEDAD ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GEDAD <> :new.CD_GEDAD
  then
    select count(*) into numrows
      from SIVE_DISTR_GEDAD
      where
        /*  %JoinFKPK(SIVE_DISTR_GEDAD,:%Old," = "," and") */
        SIVE_DISTR_GEDAD.CD_GEDAD = :old.CD_GEDAD;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_GEDAD" because "SIVE_DISTR_GEDAD" exists.'*/
       'no se puede modificar SIVE_GEDAD
        porque hay registros en SIVE_DISTR_GEDAD con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

create trigger SIVE_GEDAD_ADU_TRIG_A_U
  AFTER UPDATE
  on SIVE_GEDAD_ADU
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Mon Feb 07 12:32:58 2000 */
  /* SIVE_GEDAD_ADU R/224 SIVE_DISTR_GEDAD_ADU ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GEDAD <> :new.CD_GEDAD
  then
    select count(*) into numrows
      from SIVE_DISTR_GEDAD_ADU
      where
        /*  %JoinFKPK(SIVE_DISTR_GEDAD_ADU,:%Old," = "," and") */
        SIVE_DISTR_GEDAD_ADU.CD_GEDAD = :old.CD_GEDAD;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_GEDAD_ADU" debido a que existe "SIVE_DISTR_GEDAD_ADU".'
      );
    end if;
  end if;


-- ERwin Builtin Mon Feb 07 12:32:58 2000
end;
/


create trigger SIVE_GEDAD_ADU_TRIG_A_D
  AFTER DELETE
  on SIVE_GEDAD_ADU
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Mon Feb 07 12:32:58 2000 */
    /* SIVE_GEDAD_ADU R/224 SIVE_DISTR_GEDAD_ADU ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_DISTR_GEDAD_ADU
      where
        /*  %JoinFKPK(SIVE_DISTR_GEDAD_ADU,:%Old," = "," and") */
        SIVE_DISTR_GEDAD_ADU.CD_GEDAD = :old.CD_GEDAD;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_GEDAD_ADU" debido a que existe en "SIVE_DISTR_GEDAD_ADU".'
      );
    end if;


-- ERwin Builtin Mon Feb 07 12:32:58 2000
end;
/


CREATE OR REPLACE TRIGGER SIVE_GRUPO_BROTE_TRIG_A_D_1 after DELETE on SIVE_GRUPO_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_GRUPO_BROTE 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_GRUPO_BROTE  SIVE_AGENTE_TOXICO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_AGENTE_TOXICO
      where
        /*  %JoinFKPK(SIVE_AGENTE_TOXICO,:%Old," = "," and") */
        SIVE_AGENTE_TOXICO.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_GRUPO_BROTE" because "SIVE_AGENTE_TOXICO" exists.'*/
       'no se puede borrar de SIVE_GRUPO_BROTE
        porque hay registros en SIVE_AGENTE_TOXICO con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_GRUPO_BROTE  SIVE_AG_CAUSALES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_AG_CAUSALES
      where
        /*  %JoinFKPK(SIVE_AG_CAUSALES,:%Old," = "," and") */
        SIVE_AG_CAUSALES.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_GRUPO_BROTE" because "SIVE_AG_CAUSALES" exists.'*/
       'no se puede borrar de SIVE_GRUPO_BROTE
        porque hay registros en SIVE_AG_CAUSALES con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_GRUPO_BROTE  SIVE_ALERTA_BROTES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_BROTES
      where
        /*  %JoinFKPK(SIVE_ALERTA_BROTES,:%Old," = "," and") */
        SIVE_ALERTA_BROTES.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_GRUPO_BROTE" because "SIVE_ALERTA_BROTES" exists.'*/
       'no se puede borrar de SIVE_GRUPO_BROTE
        porque hay registros en SIVE_ALERTA_BROTES con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_GRUPO_BROTE  SIVE_FACT_CONTRIB ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_FACT_CONTRIB
      where
        /*  %JoinFKPK(SIVE_FACT_CONTRIB,:%Old," = "," and") */
        SIVE_FACT_CONTRIB.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_GRUPO_BROTE" because "SIVE_FACT_CONTRIB" exists.'*/
       'no se puede borrar de SIVE_GRUPO_BROTE
        porque hay registros en SIVE_FACT_CONTRIB con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_GRUPO_BROTE  SIVE_MEDIDAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MEDIDAS
      where
        /*  %JoinFKPK(SIVE_MEDIDAS,:%Old," = "," and") */
        SIVE_MEDIDAS.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_GRUPO_BROTE" because "SIVE_MEDIDAS" exists.'*/
       'no se puede borrar de SIVE_GRUPO_BROTE
        porque hay registros en SIVE_MEDIDAS con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_GRUPO_BROTE  SIVE_TIPO_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_TIPO_BROTE
      where
        /*  %JoinFKPK(SIVE_TIPO_BROTE,:%Old," = "," and") */
        SIVE_TIPO_BROTE.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_GRUPO_BROTE" because "SIVE_TIPO_BROTE" exists.'*/
       'no se puede borrar de SIVE_GRUPO_BROTE
        porque hay registros en SIVE_TIPO_BROTE con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_GRUPO_BROTE_TRIG_A_U_1 after UPDATE on SIVE_GRUPO_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_GRUPO_BROTE 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_GRUPO_BROTE  SIVE_AGENTE_TOXICO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GRUPO <> :new.CD_GRUPO
  then
    select count(*) into numrows
      from SIVE_AGENTE_TOXICO
      where
        /*  %JoinFKPK(SIVE_AGENTE_TOXICO,:%Old," = "," and") */
        SIVE_AGENTE_TOXICO.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_GRUPO_BROTE" because "SIVE_AGENTE_TOXICO" exists.'*/
       'no se puede modificar SIVE_GRUPO_BROTE
        porque hay registros en SIVE_AGENTE_TOXICO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_GRUPO_BROTE  SIVE_AG_CAUSALES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GRUPO <> :new.CD_GRUPO
  then
    select count(*) into numrows
      from SIVE_AG_CAUSALES
      where
        /*  %JoinFKPK(SIVE_AG_CAUSALES,:%Old," = "," and") */
        SIVE_AG_CAUSALES.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_GRUPO_BROTE" because "SIVE_AG_CAUSALES" exists.'*/
       'no se puede modificar SIVE_GRUPO_BROTE
        porque hay registros en SIVE_AG_CAUSALES con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_GRUPO_BROTE  SIVE_ALERTA_BROTES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GRUPO <> :new.CD_GRUPO
  then
    select count(*) into numrows
      from SIVE_ALERTA_BROTES
      where
        /*  %JoinFKPK(SIVE_ALERTA_BROTES,:%Old," = "," and") */
        SIVE_ALERTA_BROTES.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_GRUPO_BROTE" because "SIVE_ALERTA_BROTES" exists.'*/
       'no se puede modificar SIVE_GRUPO_BROTE
        porque hay registros en SIVE_ALERTA_BROTES con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_GRUPO_BROTE  SIVE_FACT_CONTRIB ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GRUPO <> :new.CD_GRUPO
  then
    select count(*) into numrows
      from SIVE_FACT_CONTRIB
      where
        /*  %JoinFKPK(SIVE_FACT_CONTRIB,:%Old," = "," and") */
        SIVE_FACT_CONTRIB.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_GRUPO_BROTE" because "SIVE_FACT_CONTRIB" exists.'*/
       'no se puede modificar SIVE_GRUPO_BROTE
        porque hay registros en SIVE_FACT_CONTRIB con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_GRUPO_BROTE  SIVE_MEDIDAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GRUPO <> :new.CD_GRUPO
  then
    select count(*) into numrows
      from SIVE_MEDIDAS
      where
        /*  %JoinFKPK(SIVE_MEDIDAS,:%Old," = "," and") */
        SIVE_MEDIDAS.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_GRUPO_BROTE" because "SIVE_MEDIDAS" exists.'*/
       'no se puede modificar SIVE_GRUPO_BROTE
        porque hay registros en SIVE_MEDIDAS con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_GRUPO_BROTE  SIVE_TIPO_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GRUPO <> :new.CD_GRUPO
  then
    select count(*) into numrows
      from SIVE_TIPO_BROTE
      where
        /*  %JoinFKPK(SIVE_TIPO_BROTE,:%Old," = "," and") */
        SIVE_TIPO_BROTE.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_GRUPO_BROTE" because "SIVE_TIPO_BROTE" exists.'*/
       'no se puede modificar SIVE_GRUPO_BROTE
        porque hay registros en SIVE_TIPO_BROTE con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_GRUPO_EDAD_RMC_TRIG_A_D_1 after DELETE on SIVE_GRUPO_EDAD_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- DELETE trigger on SIVE_GRUPO_EDAD_RMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_GRUPO_EDAD_RMC  SIVE_DISTR_GRUPO_EDAD_RMC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_DISTR_GRUPO_EDAD_RMC
      where
        /*  %JoinFKPK(SIVE_DISTR_GRUPO_EDAD_RMC,:%Old," = "," and") */
        SIVE_DISTR_GRUPO_EDAD_RMC.CD_GEDAD = :old.CD_GEDAD;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_GRUPO_EDAD_RMC" because "SIVE_DISTR_GRUPO_EDAD_RMC" exists.'*/
       'no se puede borrar de SIVE_GRUPO_EDAD_RMC
        porque hay registros en SIVE_DISTR_GRUPO_EDAD_RMC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_GRUPO_EDAD_RMC_TRIG_A_U_1 after UPDATE on SIVE_GRUPO_EDAD_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:48 2001
-- UPDATE trigger on SIVE_GRUPO_EDAD_RMC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
  /* SIVE_GRUPO_EDAD_RMC  SIVE_DISTR_GRUPO_EDAD_RMC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GEDAD <> :new.CD_GEDAD
  then
    select count(*) into numrows
      from SIVE_DISTR_GRUPO_EDAD_RMC
      where
        /*  %JoinFKPK(SIVE_DISTR_GRUPO_EDAD_RMC,:%Old," = "," and") */
        SIVE_DISTR_GRUPO_EDAD_RMC.CD_GEDAD = :old.CD_GEDAD;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_GRUPO_EDAD_RMC" because "SIVE_DISTR_GRUPO_EDAD_RMC" exists.'*/
       'no se puede modificar SIVE_GRUPO_EDAD_RMC
        porque hay registros en SIVE_DISTR_GRUPO_EDAD_RMC con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:48 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_HIST_BROTES_TRIG_A_I_1 after INSERT on SIVE_HIST_BROTES for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_HIST_BROTES 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:48 2001 */
    /* SIVE_MOTIVO_BAJA R/291 SIVE_HIST_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MOTIVO_BAJA
      where
        /* %JoinFKPK(:%New,SIVE_MOTIVO_BAJA," = "," and") */
        :new.CD_MOTBAJA = SIVE_MOTIVO_BAJA.CD_MOTBAJA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_MOTBAJA is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_HIST_BROTES" because "SIVE_MOTIVO_BAJA" does not exist.'*/
          'no se puede grabar en SIVE_HIST_BROTES
           porque no existe la clave en SIVE_MOTIVO_BAJA'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_HIST_BROTES_TRIG_A_U_1 after UPDATE on SIVE_HIST_BROTES for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_HIST_BROTES 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MOTIVO_BAJA R/291 SIVE_HIST_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MOTIVO_BAJA
    where
      /* %JoinFKPK(:%New,SIVE_MOTIVO_BAJA," = "," and") */
      :new.CD_MOTBAJA = SIVE_MOTIVO_BAJA.CD_MOTBAJA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_MOTBAJA is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_HIST_BROTES" because "SIVE_MOTIVO_BAJA" does not exist.'*/
         'no se puede actualizar SIVE_HIST_BROTES
          porque no existe la clave en SIVE_MOTIVO_BAJA'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_HOSPITAL_ALERT_TRIG_A_I_1 after INSERT on SIVE_HOSPITAL_ALERT for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_HOSPITAL_ALERT 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_ALERTA_BROTES  SIVE_HOSPITAL_ALERT ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_ALERTA_BROTES," = "," and") */
        :new.CD_ANO = SIVE_ALERTA_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_ALERTA_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_HOSPITAL_ALERT" because "SIVE_ALERTA_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_HOSPITAL_ALERT
           porque no existe la clave en SIVE_ALERTA_BROTES'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_HOSPITAL_ALERT_TRIG_A_U_1 after UPDATE on SIVE_HOSPITAL_ALERT for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_HOSPITAL_ALERT 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_ALERTA_BROTES  SIVE_HOSPITAL_ALERT ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ALERTA_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_ALERTA_BROTES," = "," and") */
      :new.CD_ANO = SIVE_ALERTA_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_ALERTA_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_HOSPITAL_ALERT" because "SIVE_ALERTA_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_HOSPITAL_ALERT
          porque no existe la clave en SIVE_ALERTA_BROTES'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_IND_ALAR_RMC_TRIG_A_D_1 after DELETE on SIVE_IND_ALAR_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_IND_ALAR_RMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_IND_ALAR_RMC  SIVE_IND_ALARRMC_ANO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_IND_ALARRMC_ANO
      where
        /*  %JoinFKPK(SIVE_IND_ALARRMC_ANO,:%Old," = "," and") */
        SIVE_IND_ALARRMC_ANO.CD_INDALAR = :old.CD_INDALAR;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_IND_ALAR_RMC" because "SIVE_IND_ALARRMC_ANO" exists.'*/
       'no se puede borrar de SIVE_IND_ALAR_RMC
        porque hay registros en SIVE_IND_ALARRMC_ANO con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_IND_ALAR_RMC_TRIG_A_I_1 after INSERT on SIVE_IND_ALAR_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_IND_ALAR_RMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_ENF_CENTI  SIVE_IND_ALAR_RMC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ENF_CENTI
      where
        /* %JoinFKPK(:%New,SIVE_ENF_CENTI," = "," and") */
        :new.CD_ENFCIE = SIVE_ENF_CENTI.CD_ENFCIE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_IND_ALAR_RMC" because "SIVE_ENF_CENTI" does not exist.'*/
          'no se puede grabar en SIVE_IND_ALAR_RMC
           porque no existe la clave en SIVE_ENF_CENTI'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_IND_ALAR_RMC_TRIG_A_U_1 after UPDATE on SIVE_IND_ALAR_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_IND_ALAR_RMC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_IND_ALAR_RMC  SIVE_IND_ALARRMC_ANO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_INDALAR <> :new.CD_INDALAR
  then
    select count(*) into numrows
      from SIVE_IND_ALARRMC_ANO
      where
        /*  %JoinFKPK(SIVE_IND_ALARRMC_ANO,:%Old," = "," and") */
        SIVE_IND_ALARRMC_ANO.CD_INDALAR = :old.CD_INDALAR;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_IND_ALAR_RMC" because "SIVE_IND_ALARRMC_ANO" exists.'*/
       'no se puede modificar SIVE_IND_ALAR_RMC
        porque hay registros en SIVE_IND_ALARRMC_ANO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_ENF_CENTI  SIVE_IND_ALAR_RMC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ENF_CENTI
    where
      /* %JoinFKPK(:%New,SIVE_ENF_CENTI," = "," and") */
      :new.CD_ENFCIE = SIVE_ENF_CENTI.CD_ENFCIE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_IND_ALAR_RMC" because "SIVE_ENF_CENTI" does not exist.'*/
         'no se puede actualizar SIVE_IND_ALAR_RMC
          porque no existe la clave en SIVE_ENF_CENTI'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_IND_ALARMC_ANO_TRIG_A_D_1 after DELETE on SIVE_IND_ALARRMC_ANO for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_IND_ALARRMC_ANO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_IND_ALARRMC_ANO  SIVE_ALARMA_RMC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALARMA_RMC
      where
        /*  %JoinFKPK(SIVE_ALARMA_RMC,:%Old," = "," and") */
        SIVE_ALARMA_RMC.CD_INDALAR = :old.CD_INDALAR and
        SIVE_ALARMA_RMC.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_IND_ALARRMC_ANO" because "SIVE_ALARMA_RMC" exists.'*/
       'no se puede borrar de SIVE_IND_ALARRMC_ANO
        porque hay registros en SIVE_ALARMA_RMC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_IND_ALARMC_ANO_TRIG_A_I_1 after INSERT on SIVE_IND_ALARRMC_ANO for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_IND_ALARRMC_ANO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_IND_ALAR_RMC  SIVE_IND_ALARRMC_ANO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_IND_ALAR_RMC
      where
        /* %JoinFKPK(:%New,SIVE_IND_ALAR_RMC," = "," and") */
        :new.CD_INDALAR = SIVE_IND_ALAR_RMC.CD_INDALAR;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_IND_ALARRMC_ANO" because "SIVE_IND_ALAR_RMC" does not exist.'*/
          'no se puede grabar en SIVE_IND_ALARRMC_ANO
           porque no existe la clave en SIVE_IND_ALAR_RMC'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_ANO_EPI_RMC  SIVE_IND_ALARRMC_ANO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ANO_EPI_RMC
      where
        /* %JoinFKPK(:%New,SIVE_ANO_EPI_RMC," = "," and") */
        :new.CD_ANOEPI = SIVE_ANO_EPI_RMC.CD_ANOEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_IND_ALARRMC_ANO" because "SIVE_ANO_EPI_RMC" does not exist.'*/
          'no se puede grabar en SIVE_IND_ALARRMC_ANO
           porque no existe la clave en SIVE_ANO_EPI_RMC'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_IND_ALARMC_ANO_TRIG_A_U_1 after UPDATE on SIVE_IND_ALARRMC_ANO for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_IND_ALARRMC_ANO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_IND_ALARRMC_ANO  SIVE_ALARMA_RMC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_INDALAR <> :new.CD_INDALAR or 
    :old.CD_ANOEPI <> :new.CD_ANOEPI
  then
    select count(*) into numrows
      from SIVE_ALARMA_RMC
      where
        /*  %JoinFKPK(SIVE_ALARMA_RMC,:%Old," = "," and") */
        SIVE_ALARMA_RMC.CD_INDALAR = :old.CD_INDALAR and
        SIVE_ALARMA_RMC.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_IND_ALARRMC_ANO" because "SIVE_ALARMA_RMC" exists.'*/
       'no se puede modificar SIVE_IND_ALARRMC_ANO
        porque hay registros en SIVE_ALARMA_RMC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_IND_ALAR_RMC  SIVE_IND_ALARRMC_ANO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_IND_ALAR_RMC
    where
      /* %JoinFKPK(:%New,SIVE_IND_ALAR_RMC," = "," and") */
      :new.CD_INDALAR = SIVE_IND_ALAR_RMC.CD_INDALAR;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_IND_ALARRMC_ANO" because "SIVE_IND_ALAR_RMC" does not exist.'*/
         'no se puede actualizar SIVE_IND_ALARRMC_ANO
          porque no existe la clave en SIVE_IND_ALAR_RMC'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_ANO_EPI_RMC  SIVE_IND_ALARRMC_ANO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ANO_EPI_RMC
    where
      /* %JoinFKPK(:%New,SIVE_ANO_EPI_RMC," = "," and") */
      :new.CD_ANOEPI = SIVE_ANO_EPI_RMC.CD_ANOEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_IND_ALARRMC_ANO" because "SIVE_ANO_EPI_RMC" does not exist.'*/
         'no se puede actualizar SIVE_IND_ALARRMC_ANO
          porque no existe la clave en SIVE_ANO_EPI_RMC'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_IND_ANO_TRIG_A_D_1 after DELETE on SIVE_IND_ANO for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_IND_ANO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_IND_ANO R/266 SIVE_ALARMA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALARMA
      where
        /*  %JoinFKPK(SIVE_ALARMA,:%Old," = "," and") */
        SIVE_ALARMA.CD_INDALAR = :old.CD_INDALAR and
        SIVE_ALARMA.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_IND_ANO" because "SIVE_ALARMA" exists.'*/
       'no se puede borrar de SIVE_IND_ANO
        porque hay registros en SIVE_ALARMA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_IND_ANO_TRIG_A_I_1 after INSERT on SIVE_IND_ANO for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_IND_ANO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_ANO_EPI R/149 SIVE_IND_ANO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ANO_EPI
      where
        /* %JoinFKPK(:%New,SIVE_ANO_EPI," = "," and") */
        :new.CD_ANOEPI = SIVE_ANO_EPI.CD_ANOEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_IND_ANO" because "SIVE_ANO_EPI" does not exist.'*/
          'no se puede grabar en SIVE_IND_ANO
           porque no existe la clave en SIVE_ANO_EPI'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_INDICADOR R/111 SIVE_IND_ANO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_INDICADOR
      where
        /* %JoinFKPK(:%New,SIVE_INDICADOR," = "," and") */
        :new.CD_INDALAR = SIVE_INDICADOR.CD_INDALAR;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_IND_ANO" because "SIVE_INDICADOR" does not exist.'*/
          'no se puede grabar en SIVE_IND_ANO
           porque no existe la clave en SIVE_INDICADOR'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_IND_ANO_TRIG_A_U_1 after UPDATE on SIVE_IND_ANO for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_IND_ANO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_IND_ANO R/266 SIVE_ALARMA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_INDALAR <> :new.CD_INDALAR or 
    :old.CD_ANOEPI <> :new.CD_ANOEPI
  then
    select count(*) into numrows
      from SIVE_ALARMA
      where
        /*  %JoinFKPK(SIVE_ALARMA,:%Old," = "," and") */
        SIVE_ALARMA.CD_INDALAR = :old.CD_INDALAR and
        SIVE_ALARMA.CD_ANOEPI = :old.CD_ANOEPI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_IND_ANO" because "SIVE_ALARMA" exists.'*/
       'no se puede modificar SIVE_IND_ANO
        porque hay registros en SIVE_ALARMA con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_ANO_EPI R/149 SIVE_IND_ANO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ANO_EPI
    where
      /* %JoinFKPK(:%New,SIVE_ANO_EPI," = "," and") */
      :new.CD_ANOEPI = SIVE_ANO_EPI.CD_ANOEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_IND_ANO" because "SIVE_ANO_EPI" does not exist.'*/
         'no se puede actualizar SIVE_IND_ANO
          porque no existe la clave en SIVE_ANO_EPI'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_INDICADOR R/111 SIVE_IND_ANO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_INDICADOR
    where
      /* %JoinFKPK(:%New,SIVE_INDICADOR," = "," and") */
      :new.CD_INDALAR = SIVE_INDICADOR.CD_INDALAR;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_IND_ANO" because "SIVE_INDICADOR" does not exist.'*/
         'no se puede actualizar SIVE_IND_ANO
          porque no existe la clave en SIVE_INDICADOR'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_INDACTMC_TRIG_A_D_1 after DELETE on SIVE_INDACTMC for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_INDACTMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_INDACTMC  SIVE_INDACTMC_MEDCEN ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_INDACTMC_MEDCEN
      where
        /*  %JoinFKPK(SIVE_INDACTMC_MEDCEN,:%Old," = "," and") */
        SIVE_INDACTMC_MEDCEN.CD_INDRMC = :old.CD_INDRMC;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_INDACTMC" because "SIVE_INDACTMC_MEDCEN" exists.'*/
       'no se puede borrar de SIVE_INDACTMC
        porque hay registros en SIVE_INDACTMC_MEDCEN con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_INDACTMC_TRIG_A_U_1 after UPDATE on SIVE_INDACTMC for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_INDACTMC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_INDACTMC  SIVE_INDACTMC_MEDCEN ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_INDRMC <> :new.CD_INDRMC
  then
    select count(*) into numrows
      from SIVE_INDACTMC_MEDCEN
      where
        /*  %JoinFKPK(SIVE_INDACTMC_MEDCEN,:%Old," = "," and") */
        SIVE_INDACTMC_MEDCEN.CD_INDRMC = :old.CD_INDRMC;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_INDACTMC" because "SIVE_INDACTMC_MEDCEN" exists.'*/
       'no se puede modificar SIVE_INDACTMC
        porque hay registros en SIVE_INDACTMC_MEDCEN con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_INDACTM_MEDCEN_TRIG_A_I_1 after INSERT on SIVE_INDACTMC_MEDCEN for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_INDACTMC_MEDCEN 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MCENTINELA  SIVE_INDACTMC_MEDCEN ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MCENTINELA
      where
        /* %JoinFKPK(:%New,SIVE_MCENTINELA," = "," and") */
        :new.CD_PCENTI = SIVE_MCENTINELA.CD_PCENTI and
        :new.CD_MEDCEN = SIVE_MCENTINELA.CD_MEDCEN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_INDACTMC_MEDCEN" because "SIVE_MCENTINELA" does not exist.'*/
          'no se puede grabar en SIVE_INDACTMC_MEDCEN
           porque no existe la clave en SIVE_MCENTINELA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_INDACTMC  SIVE_INDACTMC_MEDCEN ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_INDACTMC
      where
        /* %JoinFKPK(:%New,SIVE_INDACTMC," = "," and") */
        :new.CD_INDRMC = SIVE_INDACTMC.CD_INDRMC;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_INDACTMC_MEDCEN" because "SIVE_INDACTMC" does not exist.'*/
          'no se puede grabar en SIVE_INDACTMC_MEDCEN
           porque no existe la clave en SIVE_INDACTMC'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_INDACTM_MEDCEN_TRIG_A_U_1 after UPDATE on SIVE_INDACTMC_MEDCEN for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_INDACTMC_MEDCEN 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MCENTINELA  SIVE_INDACTMC_MEDCEN ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MCENTINELA
    where
      /* %JoinFKPK(:%New,SIVE_MCENTINELA," = "," and") */
      :new.CD_PCENTI = SIVE_MCENTINELA.CD_PCENTI and
      :new.CD_MEDCEN = SIVE_MCENTINELA.CD_MEDCEN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_INDACTMC_MEDCEN" because "SIVE_MCENTINELA" does not exist.'*/
         'no se puede actualizar SIVE_INDACTMC_MEDCEN
          porque no existe la clave en SIVE_MCENTINELA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_INDACTMC  SIVE_INDACTMC_MEDCEN ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_INDACTMC
    where
      /* %JoinFKPK(:%New,SIVE_INDACTMC," = "," and") */
      :new.CD_INDRMC = SIVE_INDACTMC.CD_INDRMC;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_INDACTMC_MEDCEN" because "SIVE_INDACTMC" does not exist.'*/
         'no se puede actualizar SIVE_INDACTMC_MEDCEN
          porque no existe la clave en SIVE_INDACTMC'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_INDICADOR_TRIG_A_D_1 after DELETE on SIVE_INDICADOR for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_INDICADOR 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_INDICADOR R/111 SIVE_IND_ANO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_IND_ANO
      where
        /*  %JoinFKPK(SIVE_IND_ANO,:%Old," = "," and") */
        SIVE_IND_ANO.CD_INDALAR = :old.CD_INDALAR;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_INDICADOR" because "SIVE_IND_ANO" exists.'*/
       'no se puede borrar de SIVE_INDICADOR
        porque hay registros en SIVE_IND_ANO con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_INDICADOR_TRIG_A_I_1 after INSERT on SIVE_INDICADOR for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_INDICADOR 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_NIVEL2_S R/280 SIVE_INDICADOR ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL2_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      :new.CD_NIVEL_2 is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_INDICADOR" because "SIVE_NIVEL2_S" does not exist.'*/
          'no se puede grabar en SIVE_INDICADOR
           porque no existe la clave en SIVE_NIVEL2_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_NIVEL1_S R/279 SIVE_INDICADOR ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL1_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_INDICADOR" because "SIVE_NIVEL1_S" does not exist.'*/
          'no se puede grabar en SIVE_INDICADOR
           porque no existe la clave en SIVE_NIVEL1_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_ENFEREDO R/275 SIVE_INDICADOR ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ENFEREDO
      where
        /* %JoinFKPK(:%New,SIVE_ENFEREDO," = "," and") */
        :new.CD_ENFCIE = SIVE_ENFEREDO.CD_ENFCIE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_ENFCIE is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_INDICADOR" because "SIVE_ENFEREDO" does not exist.'*/
          'no se puede grabar en SIVE_INDICADOR
           porque no existe la clave en SIVE_ENFEREDO'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_INDICADOR_TRIG_A_U_1 after UPDATE on SIVE_INDICADOR for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_INDICADOR 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_INDICADOR R/111 SIVE_IND_ANO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_INDALAR <> :new.CD_INDALAR
  then
    select count(*) into numrows
      from SIVE_IND_ANO
      where
        /*  %JoinFKPK(SIVE_IND_ANO,:%Old," = "," and") */
        SIVE_IND_ANO.CD_INDALAR = :old.CD_INDALAR;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_INDICADOR" because "SIVE_IND_ANO" exists.'*/
       'no se puede modificar SIVE_INDICADOR
        porque hay registros en SIVE_IND_ANO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_NIVEL2_S R/280 SIVE_INDICADOR ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL2_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
      :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    :new.CD_NIVEL_2 is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_INDICADOR" because "SIVE_NIVEL2_S" does not exist.'*/
         'no se puede actualizar SIVE_INDICADOR
          porque no existe la clave en SIVE_NIVEL2_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_NIVEL1_S R/279 SIVE_INDICADOR ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL1_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_INDICADOR" because "SIVE_NIVEL1_S" does not exist.'*/
         'no se puede actualizar SIVE_INDICADOR
          porque no existe la clave en SIVE_NIVEL1_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_ENFEREDO R/275 SIVE_INDICADOR ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ENFEREDO
    where
      /* %JoinFKPK(:%New,SIVE_ENFEREDO," = "," and") */
      :new.CD_ENFCIE = SIVE_ENFEREDO.CD_ENFCIE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_ENFCIE is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_INDICADOR" because "SIVE_ENFEREDO" does not exist.'*/
         'no se puede actualizar SIVE_INDICADOR
          porque no existe la clave en SIVE_ENFEREDO'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_INDMEDRMC_TRIG_A_I_1 after INSERT on SIVE_INDMEDRMC for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_INDMEDRMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MCENTINELA  SIVE_INDMEDRMC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MCENTINELA
      where
        /* %JoinFKPK(:%New,SIVE_MCENTINELA," = "," and") */
        :new.CD_PCENTI = SIVE_MCENTINELA.CD_PCENTI and
        :new.CD_MEDCEN = SIVE_MCENTINELA.CD_MEDCEN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_INDMEDRMC" because "SIVE_MCENTINELA" does not exist.'*/
          'no se puede grabar en SIVE_INDMEDRMC
           porque no existe la clave en SIVE_MCENTINELA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_INDRMC  SIVE_INDMEDRMC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_INDRMC
      where
        /* %JoinFKPK(:%New,SIVE_INDRMC," = "," and") */
        :new.CD_INDRMC = SIVE_INDRMC.CD_INDRMC;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_INDMEDRMC" because "SIVE_INDRMC" does not exist.'*/
          'no se puede grabar en SIVE_INDMEDRMC
           porque no existe la clave en SIVE_INDRMC'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_INDMEDRMC_TRIG_A_U_1 after UPDATE on SIVE_INDMEDRMC for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_INDMEDRMC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MCENTINELA  SIVE_INDMEDRMC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MCENTINELA
    where
      /* %JoinFKPK(:%New,SIVE_MCENTINELA," = "," and") */
      :new.CD_PCENTI = SIVE_MCENTINELA.CD_PCENTI and
      :new.CD_MEDCEN = SIVE_MCENTINELA.CD_MEDCEN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_INDMEDRMC" because "SIVE_MCENTINELA" does not exist.'*/
         'no se puede actualizar SIVE_INDMEDRMC
          porque no existe la clave en SIVE_MCENTINELA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_INDRMC  SIVE_INDMEDRMC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_INDRMC
    where
      /* %JoinFKPK(:%New,SIVE_INDRMC," = "," and") */
      :new.CD_INDRMC = SIVE_INDRMC.CD_INDRMC;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_INDMEDRMC" because "SIVE_INDRMC" does not exist.'*/
         'no se puede actualizar SIVE_INDMEDRMC
          porque no existe la clave en SIVE_INDRMC'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_INDRMC_TRIG_A_D_1 after DELETE on SIVE_INDRMC for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_INDRMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_INDRMC  SIVE_INDMEDRMC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_INDMEDRMC
      where
        /*  %JoinFKPK(SIVE_INDMEDRMC,:%Old," = "," and") */
        SIVE_INDMEDRMC.CD_INDRMC = :old.CD_INDRMC;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_INDRMC" because "SIVE_INDMEDRMC" exists.'*/
       'no se puede borrar de SIVE_INDRMC
        porque hay registros en SIVE_INDMEDRMC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_INDRMC_TRIG_A_U_1 after UPDATE on SIVE_INDRMC for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_INDRMC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_INDRMC  SIVE_INDMEDRMC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_INDRMC <> :new.CD_INDRMC
  then
    select count(*) into numrows
      from SIVE_INDMEDRMC
      where
        /*  %JoinFKPK(SIVE_INDMEDRMC,:%Old," = "," and") */
        SIVE_INDMEDRMC.CD_INDRMC = :old.CD_INDRMC;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_INDRMC" because "SIVE_INDMEDRMC" exists.'*/
       'no se puede modificar SIVE_INDRMC
        porque hay registros en SIVE_INDMEDRMC con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_INVESTIGADOR_TRIG_A_I_1 after INSERT on SIVE_INVESTIGADOR for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_INVESTIGADOR 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_USUARIO_BROTE R/292 SIVE_INVESTIGADOR ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_USUARIO_BROTE
      where
        /* %JoinFKPK(:%New,SIVE_USUARIO_BROTE," = "," and") */
        :new.CD_USUARIO = SIVE_USUARIO_BROTE.CD_USUARIO and
        :new.CD_CA = SIVE_USUARIO_BROTE.CD_CA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_INVESTIGADOR" because "SIVE_USUARIO_BROTE" does not exist.'*/
          'no se puede grabar en SIVE_INVESTIGADOR
           porque no existe la clave en SIVE_USUARIO_BROTE'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_BROTES  SIVE_INVESTIGADOR ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
        :new.CD_ANO = SIVE_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_INVESTIGADOR" because "SIVE_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_INVESTIGADOR
           porque no existe la clave en SIVE_BROTES'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_INVESTIGADOR_TRIG_A_U_1 after UPDATE on SIVE_INVESTIGADOR for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_INVESTIGADOR 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_USUARIO_BROTE R/292 SIVE_INVESTIGADOR ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_USUARIO_BROTE
    where
      /* %JoinFKPK(:%New,SIVE_USUARIO_BROTE," = "," and") */
      :new.CD_USUARIO = SIVE_USUARIO_BROTE.CD_USUARIO and
      :new.CD_CA = SIVE_USUARIO_BROTE.CD_CA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_INVESTIGADOR" because "SIVE_USUARIO_BROTE" does not exist.'*/
         'no se puede actualizar SIVE_INVESTIGADOR
          porque no existe la clave en SIVE_USUARIO_BROTE'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_BROTES  SIVE_INVESTIGADOR ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
      :new.CD_ANO = SIVE_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_INVESTIGADOR" because "SIVE_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_INVESTIGADOR
          porque no existe la clave en SIVE_BROTES'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LCONSUMO_ALI_TRIG_A_D_1 after DELETE on SIVE_LCONSUMO_ALI for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_LCONSUMO_ALI 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_LCONSUMO_ALI R/312 SIVE_ALI_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_CONSUMOALI = :old.CD_CONSUMOALI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_LCONSUMO_ALI" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede borrar de SIVE_LCONSUMO_ALI
        porque hay registros en SIVE_ALI_BROTE con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LCONSUMO_ALI_TRIG_A_U_1 after UPDATE on SIVE_LCONSUMO_ALI for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_LCONSUMO_ALI 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_LCONSUMO_ALI R/312 SIVE_ALI_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_CONSUMOALI <> :new.CD_CONSUMOALI
  then
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_CONSUMOALI = :old.CD_CONSUMOALI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_LCONSUMO_ALI" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede modificar SIVE_LCONSUMO_ALI
        porque hay registros en SIVE_ALI_BROTE con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LCONTAMI_ALI_TRIG_A_D_1 after DELETE on SIVE_LCONTAMI_ALI for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_LCONTAMI_ALI 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_LCONTAMI_ALI R/313 SIVE_ALI_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_LCONTAMIALI = :old.CD_LCONTAMIALI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_LCONTAMI_ALI" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede borrar de SIVE_LCONTAMI_ALI
        porque hay registros en SIVE_ALI_BROTE con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LCONTAMI_ALI_TRIG_A_U_1 after UPDATE on SIVE_LCONTAMI_ALI for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_LCONTAMI_ALI 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_LCONTAMI_ALI R/313 SIVE_ALI_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_LCONTAMIALI <> :new.CD_LCONTAMIALI
  then
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_LCONTAMIALI = :old.CD_LCONTAMIALI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_LCONTAMI_ALI" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede modificar SIVE_LCONTAMI_ALI
        porque hay registros en SIVE_ALI_BROTE con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LINEA_ITEM_TRIG_A_D_1 after DELETE on SIVE_LINEA_ITEM for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_LINEA_ITEM 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_LINEA_ITEM R/276 SIVE_RESP_ADIC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESP_ADIC
      where
        /*  %JoinFKPK(SIVE_RESP_ADIC,:%Old," = "," and") */
        SIVE_RESP_ADIC.CD_TSIVE = :old.CD_TSIVE and
        SIVE_RESP_ADIC.CD_MODELO = :old.CD_MODELO and
        SIVE_RESP_ADIC.NM_LIN = :old.NM_LIN and
        SIVE_RESP_ADIC.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_LINEA_ITEM" because "SIVE_RESP_ADIC" exists.'*/
       'no se puede borrar de SIVE_LINEA_ITEM
        porque hay registros en SIVE_RESP_ADIC con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_LINEA_ITEM  SIVE_RESPCONTACTO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESPCONTACTO
      where
        /*  %JoinFKPK(SIVE_RESPCONTACTO,:%Old," = "," and") */
        SIVE_RESPCONTACTO.CD_TSIVE = :old.CD_TSIVE and
        SIVE_RESPCONTACTO.CD_MODELO = :old.CD_MODELO and
        SIVE_RESPCONTACTO.NM_LIN = :old.NM_LIN and
        SIVE_RESPCONTACTO.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_LINEA_ITEM" because "SIVE_RESPCONTACTO" exists.'*/
       'no se puede borrar de SIVE_LINEA_ITEM
        porque hay registros en SIVE_RESPCONTACTO con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_LINEA_ITEM  SIVE_RESP_BROTES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESP_BROTES
      where
        /*  %JoinFKPK(SIVE_RESP_BROTES,:%Old," = "," and") */
        SIVE_RESP_BROTES.CD_TSIVE = :old.CD_TSIVE and
        SIVE_RESP_BROTES.CD_MODELO = :old.CD_MODELO and
        SIVE_RESP_BROTES.NM_LIN = :old.NM_LIN and
        SIVE_RESP_BROTES.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_LINEA_ITEM" because "SIVE_RESP_BROTES" exists.'*/
       'no se puede borrar de SIVE_LINEA_ITEM
        porque hay registros en SIVE_RESP_BROTES con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_LINEA_ITEM  SIVE_RESP_CENTI ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESP_CENTI
      where
        /*  %JoinFKPK(SIVE_RESP_CENTI,:%Old," = "," and") */
        SIVE_RESP_CENTI.CD_TSIVE = :old.CD_TSIVE and
        SIVE_RESP_CENTI.CD_MODELO = :old.CD_MODELO and
        SIVE_RESP_CENTI.NM_LIN = :old.NM_LIN and
        SIVE_RESP_CENTI.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_LINEA_ITEM" because "SIVE_RESP_CENTI" exists.'*/
       'no se puede borrar de SIVE_LINEA_ITEM
        porque hay registros en SIVE_RESP_CENTI con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_LINEA_ITEM  SIVE_RESP_EDO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESP_EDO
      where
        /*  %JoinFKPK(SIVE_RESP_EDO,:%Old," = "," and") */
        SIVE_RESP_EDO.CD_TSIVE = :old.CD_TSIVE and
        SIVE_RESP_EDO.CD_MODELO = :old.CD_MODELO and
        SIVE_RESP_EDO.NM_LIN = :old.NM_LIN and
        SIVE_RESP_EDO.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_LINEA_ITEM" because "SIVE_RESP_EDO" exists.'*/
       'no se puede borrar de SIVE_LINEA_ITEM
        porque hay registros en SIVE_RESP_EDO con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LINEA_ITEM_TRIG_A_I_1 after INSERT on SIVE_LINEA_ITEM for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_LINEA_ITEM 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_LINEASM  SIVE_LINEA_ITEM ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_LINEASM
      where
        /* %JoinFKPK(:%New,SIVE_LINEASM," = "," and") */
        :new.CD_TSIVE = SIVE_LINEASM.CD_TSIVE and
        :new.CD_MODELO = SIVE_LINEASM.CD_MODELO and
        :new.NM_LIN = SIVE_LINEASM.NM_LIN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_LINEA_ITEM" because "SIVE_LINEASM" does not exist.'*/
          'no se puede grabar en SIVE_LINEA_ITEM
           porque no existe la clave en SIVE_LINEASM'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_PREGUNTA  SIVE_LINEA_ITEM ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PREGUNTA
      where
        /* %JoinFKPK(:%New,SIVE_PREGUNTA," = "," and") */
        :new.CD_TSIVE = SIVE_PREGUNTA.CD_TSIVE and
        :new.CD_PREGUNTA = SIVE_PREGUNTA.CD_PREGUNTA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_LINEA_ITEM" because "SIVE_PREGUNTA" does not exist.'*/
          'no se puede grabar en SIVE_LINEA_ITEM
           porque no existe la clave en SIVE_PREGUNTA'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LINEA_ITEM_TRIG_A_U_1 after UPDATE on SIVE_LINEA_ITEM for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_LINEA_ITEM 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_LINEA_ITEM R/276 SIVE_RESP_ADIC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TSIVE <> :new.CD_TSIVE or 
    :old.CD_MODELO <> :new.CD_MODELO or 
    :old.NM_LIN <> :new.NM_LIN or 
    :old.CD_PREGUNTA <> :new.CD_PREGUNTA
  then
    select count(*) into numrows
      from SIVE_RESP_ADIC
      where
        /*  %JoinFKPK(SIVE_RESP_ADIC,:%Old," = "," and") */
        SIVE_RESP_ADIC.CD_TSIVE = :old.CD_TSIVE and
        SIVE_RESP_ADIC.CD_MODELO = :old.CD_MODELO and
        SIVE_RESP_ADIC.NM_LIN = :old.NM_LIN and
        SIVE_RESP_ADIC.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_LINEA_ITEM" because "SIVE_RESP_ADIC" exists.'*/
       'no se puede modificar SIVE_LINEA_ITEM
        porque hay registros en SIVE_RESP_ADIC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_LINEA_ITEM  SIVE_RESPCONTACTO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TSIVE <> :new.CD_TSIVE or 
    :old.CD_MODELO <> :new.CD_MODELO or 
    :old.NM_LIN <> :new.NM_LIN or 
    :old.CD_PREGUNTA <> :new.CD_PREGUNTA
  then
    select count(*) into numrows
      from SIVE_RESPCONTACTO
      where
        /*  %JoinFKPK(SIVE_RESPCONTACTO,:%Old," = "," and") */
        SIVE_RESPCONTACTO.CD_TSIVE = :old.CD_TSIVE and
        SIVE_RESPCONTACTO.CD_MODELO = :old.CD_MODELO and
        SIVE_RESPCONTACTO.NM_LIN = :old.NM_LIN and
        SIVE_RESPCONTACTO.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_LINEA_ITEM" because "SIVE_RESPCONTACTO" exists.'*/
       'no se puede modificar SIVE_LINEA_ITEM
        porque hay registros en SIVE_RESPCONTACTO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_LINEA_ITEM  SIVE_RESP_BROTES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TSIVE <> :new.CD_TSIVE or 
    :old.CD_MODELO <> :new.CD_MODELO or 
    :old.NM_LIN <> :new.NM_LIN or 
    :old.CD_PREGUNTA <> :new.CD_PREGUNTA
  then
    select count(*) into numrows
      from SIVE_RESP_BROTES
      where
        /*  %JoinFKPK(SIVE_RESP_BROTES,:%Old," = "," and") */
        SIVE_RESP_BROTES.CD_TSIVE = :old.CD_TSIVE and
        SIVE_RESP_BROTES.CD_MODELO = :old.CD_MODELO and
        SIVE_RESP_BROTES.NM_LIN = :old.NM_LIN and
        SIVE_RESP_BROTES.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_LINEA_ITEM" because "SIVE_RESP_BROTES" exists.'*/
       'no se puede modificar SIVE_LINEA_ITEM
        porque hay registros en SIVE_RESP_BROTES con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_LINEA_ITEM  SIVE_RESP_CENTI ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TSIVE <> :new.CD_TSIVE or 
    :old.CD_MODELO <> :new.CD_MODELO or 
    :old.NM_LIN <> :new.NM_LIN or 
    :old.CD_PREGUNTA <> :new.CD_PREGUNTA
  then
    select count(*) into numrows
      from SIVE_RESP_CENTI
      where
        /*  %JoinFKPK(SIVE_RESP_CENTI,:%Old," = "," and") */
        SIVE_RESP_CENTI.CD_TSIVE = :old.CD_TSIVE and
        SIVE_RESP_CENTI.CD_MODELO = :old.CD_MODELO and
        SIVE_RESP_CENTI.NM_LIN = :old.NM_LIN and
        SIVE_RESP_CENTI.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_LINEA_ITEM" because "SIVE_RESP_CENTI" exists.'*/
       'no se puede modificar SIVE_LINEA_ITEM
        porque hay registros en SIVE_RESP_CENTI con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_LINEA_ITEM  SIVE_RESP_EDO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TSIVE <> :new.CD_TSIVE or 
    :old.CD_MODELO <> :new.CD_MODELO or 
    :old.NM_LIN <> :new.NM_LIN or 
    :old.CD_PREGUNTA <> :new.CD_PREGUNTA
  then
    select count(*) into numrows
      from SIVE_RESP_EDO
      where
        /*  %JoinFKPK(SIVE_RESP_EDO,:%Old," = "," and") */
        SIVE_RESP_EDO.CD_TSIVE = :old.CD_TSIVE and
        SIVE_RESP_EDO.CD_MODELO = :old.CD_MODELO and
        SIVE_RESP_EDO.NM_LIN = :old.NM_LIN and
        SIVE_RESP_EDO.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_LINEA_ITEM" because "SIVE_RESP_EDO" exists.'*/
       'no se puede modificar SIVE_LINEA_ITEM
        porque hay registros en SIVE_RESP_EDO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_LINEASM  SIVE_LINEA_ITEM ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_LINEASM
    where
      /* %JoinFKPK(:%New,SIVE_LINEASM," = "," and") */
      :new.CD_TSIVE = SIVE_LINEASM.CD_TSIVE and
      :new.CD_MODELO = SIVE_LINEASM.CD_MODELO and
      :new.NM_LIN = SIVE_LINEASM.NM_LIN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_LINEA_ITEM" because "SIVE_LINEASM" does not exist.'*/
         'no se puede actualizar SIVE_LINEA_ITEM
          porque no existe la clave en SIVE_LINEASM'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_PREGUNTA  SIVE_LINEA_ITEM ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PREGUNTA
    where
      /* %JoinFKPK(:%New,SIVE_PREGUNTA," = "," and") */
      :new.CD_TSIVE = SIVE_PREGUNTA.CD_TSIVE and
      :new.CD_PREGUNTA = SIVE_PREGUNTA.CD_PREGUNTA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_LINEA_ITEM" because "SIVE_PREGUNTA" does not exist.'*/
         'no se puede actualizar SIVE_LINEA_ITEM
          porque no existe la clave en SIVE_PREGUNTA'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LINEASM_TRIG_A_D_1 after DELETE on SIVE_LINEASM for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_LINEASM 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_LINEASM  SIVE_LINEA_ITEM ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_LINEA_ITEM
      where
        /*  %JoinFKPK(SIVE_LINEA_ITEM,:%Old," = "," and") */
        SIVE_LINEA_ITEM.CD_TSIVE = :old.CD_TSIVE and
        SIVE_LINEA_ITEM.CD_MODELO = :old.CD_MODELO and
        SIVE_LINEA_ITEM.NM_LIN = :old.NM_LIN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_LINEASM" because "SIVE_LINEA_ITEM" exists.'*/
       'no se puede borrar de SIVE_LINEASM
        porque hay registros en SIVE_LINEA_ITEM con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LINEASM_TRIG_A_I_1 after INSERT on SIVE_LINEASM for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_LINEASM 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MODELO  SIVE_LINEASM ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MODELO
      where
        /* %JoinFKPK(:%New,SIVE_MODELO," = "," and") */
        :new.CD_TSIVE = SIVE_MODELO.CD_TSIVE and
        :new.CD_MODELO = SIVE_MODELO.CD_MODELO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_LINEASM" because "SIVE_MODELO" does not exist.'*/
          'no se puede grabar en SIVE_LINEASM
           porque no existe la clave en SIVE_MODELO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_TLINEA  SIVE_LINEASM ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_TLINEA
      where
        /* %JoinFKPK(:%New,SIVE_TLINEA," = "," and") */
        :new.CD_TLINEA = SIVE_TLINEA.CD_TLINEA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_LINEASM" because "SIVE_TLINEA" does not exist.'*/
          'no se puede grabar en SIVE_LINEASM
           porque no existe la clave en SIVE_TLINEA'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LINEASM_TRIG_A_U_1 after UPDATE on SIVE_LINEASM for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_LINEASM 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_LINEASM  SIVE_LINEA_ITEM ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TSIVE <> :new.CD_TSIVE or 
    :old.CD_MODELO <> :new.CD_MODELO or 
    :old.NM_LIN <> :new.NM_LIN
  then
    select count(*) into numrows
      from SIVE_LINEA_ITEM
      where
        /*  %JoinFKPK(SIVE_LINEA_ITEM,:%Old," = "," and") */
        SIVE_LINEA_ITEM.CD_TSIVE = :old.CD_TSIVE and
        SIVE_LINEA_ITEM.CD_MODELO = :old.CD_MODELO and
        SIVE_LINEA_ITEM.NM_LIN = :old.NM_LIN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_LINEASM" because "SIVE_LINEA_ITEM" exists.'*/
       'no se puede modificar SIVE_LINEASM
        porque hay registros en SIVE_LINEA_ITEM con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MODELO  SIVE_LINEASM ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MODELO
    where
      /* %JoinFKPK(:%New,SIVE_MODELO," = "," and") */
      :new.CD_TSIVE = SIVE_MODELO.CD_TSIVE and
      :new.CD_MODELO = SIVE_MODELO.CD_MODELO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_LINEASM" because "SIVE_MODELO" does not exist.'*/
         'no se puede actualizar SIVE_LINEASM
          porque no existe la clave en SIVE_MODELO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_TLINEA  SIVE_LINEASM ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_TLINEA
    where
      /* %JoinFKPK(:%New,SIVE_TLINEA," = "," and") */
      :new.CD_TLINEA = SIVE_TLINEA.CD_TLINEA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_LINEASM" because "SIVE_TLINEA" does not exist.'*/
         'no se puede actualizar SIVE_LINEASM
          porque no existe la clave en SIVE_TLINEA'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LISTA_VALORES_TRIG_A_I_1 after INSERT on SIVE_LISTA_VALORES for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_LISTA_VALORES 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_LISTAS  SIVE_LISTA_VALORES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_LISTAS
      where
        /* %JoinFKPK(:%New,SIVE_LISTAS," = "," and") */
        :new.CD_LISTA = SIVE_LISTAS.CD_LISTA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_LISTA_VALORES" because "SIVE_LISTAS" does not exist.'*/
          'no se puede grabar en SIVE_LISTA_VALORES
           porque no existe la clave en SIVE_LISTAS'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LISTA_VALORES_TRIG_A_U_1 after UPDATE on SIVE_LISTA_VALORES for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_LISTA_VALORES 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_LISTAS  SIVE_LISTA_VALORES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_LISTAS
    where
      /* %JoinFKPK(:%New,SIVE_LISTAS," = "," and") */
      :new.CD_LISTA = SIVE_LISTAS.CD_LISTA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_LISTA_VALORES" because "SIVE_LISTAS" does not exist.'*/
         'no se puede actualizar SIVE_LISTA_VALORES
          porque no existe la clave en SIVE_LISTAS'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LISTAS_TRIG_A_D_1 after DELETE on SIVE_LISTAS for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_LISTAS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_LISTAS  SIVE_LISTA_VALORES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_LISTA_VALORES
      where
        /*  %JoinFKPK(SIVE_LISTA_VALORES,:%Old," = "," and") */
        SIVE_LISTA_VALORES.CD_LISTA = :old.CD_LISTA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_LISTAS" because "SIVE_LISTA_VALORES" exists.'*/
       'no se puede borrar de SIVE_LISTAS
        porque hay registros en SIVE_LISTA_VALORES con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_LISTAS  SIVE_PREGUNTA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_PREGUNTA
      where
        /*  %JoinFKPK(SIVE_PREGUNTA,:%Old," = "," and") */
        SIVE_PREGUNTA.CD_LISTA = :old.CD_LISTA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_LISTAS" because "SIVE_PREGUNTA" exists.'*/
       'no se puede borrar de SIVE_LISTAS
        porque hay registros en SIVE_PREGUNTA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LISTAS_TRIG_A_U_1 after UPDATE on SIVE_LISTAS for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_LISTAS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_LISTAS  SIVE_LISTA_VALORES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_LISTA <> :new.CD_LISTA
  then
    select count(*) into numrows
      from SIVE_LISTA_VALORES
      where
        /*  %JoinFKPK(SIVE_LISTA_VALORES,:%Old," = "," and") */
        SIVE_LISTA_VALORES.CD_LISTA = :old.CD_LISTA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_LISTAS" because "SIVE_LISTA_VALORES" exists.'*/
       'no se puede modificar SIVE_LISTAS
        porque hay registros en SIVE_LISTA_VALORES con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_LISTAS  SIVE_PREGUNTA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_LISTA <> :new.CD_LISTA
  then
    select count(*) into numrows
      from SIVE_PREGUNTA
      where
        /*  %JoinFKPK(SIVE_PREGUNTA,:%Old," = "," and") */
        SIVE_PREGUNTA.CD_LISTA = :old.CD_LISTA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_LISTAS" because "SIVE_PREGUNTA" exists.'*/
       'no se puede modificar SIVE_LISTAS
        porque hay registros en SIVE_PREGUNTA con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LPREP_ALI_TRIG_A_D_1 after DELETE on SIVE_LPREP_ALI for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_LPREP_ALI 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_LPREP_ALI R/314 SIVE_ALI_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_LPREPALI = :old.CD_LPREPALI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_LPREP_ALI" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede borrar de SIVE_LPREP_ALI
        porque hay registros en SIVE_ALI_BROTE con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_LPREP_ALI_TRIG_A_U_1 after UPDATE on SIVE_LPREP_ALI for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_LPREP_ALI 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_LPREP_ALI R/314 SIVE_ALI_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_LPREPALI <> :new.CD_LPREPALI
  then
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_LPREPALI = :old.CD_LPREPALI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_LPREP_ALI" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede modificar SIVE_LPREP_ALI
        porque hay registros en SIVE_ALI_BROTE con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MASISTENCIA_TRIG_A_D_1 after DELETE on SIVE_MASISTENCIA for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_MASISTENCIA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MASISTENCIA  SIVE_MCENTINELA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MCENTINELA
      where
        /*  %JoinFKPK(SIVE_MCENTINELA,:%Old," = "," and") */
        SIVE_MCENTINELA.CD_MASIST = :old.CD_MASIST;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MASISTENCIA" because "SIVE_MCENTINELA" exists.'*/
       'no se puede borrar de SIVE_MASISTENCIA
        porque hay registros en SIVE_MCENTINELA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MASISTENCIA_TRIG_A_U_1 after UPDATE on SIVE_MASISTENCIA for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_MASISTENCIA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MASISTENCIA  SIVE_MCENTINELA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_MASIST <> :new.CD_MASIST
  then
    select count(*) into numrows
      from SIVE_MCENTINELA
      where
        /*  %JoinFKPK(SIVE_MCENTINELA,:%Old," = "," and") */
        SIVE_MCENTINELA.CD_MASIST = :old.CD_MASIST;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MASISTENCIA" because "SIVE_MCENTINELA" exists.'*/
       'no se puede modificar SIVE_MASISTENCIA
        porque hay registros en SIVE_MCENTINELA con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MCENTINELA_TRIG_A_D_1 after DELETE on SIVE_MCENTINELA for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_MCENTINELA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MCENTINELA R/253 SIVE_AVISOS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_AVISOS
      where
        /*  %JoinFKPK(SIVE_AVISOS,:%Old," = "," and") */
        SIVE_AVISOS.CD_PCENTI = :old.CD_PCENTI and
        SIVE_AVISOS.CD_MEDCEN = :old.CD_MEDCEN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MCENTINELA" because "SIVE_AVISOS" exists.'*/
       'no se puede borrar de SIVE_MCENTINELA
        porque hay registros en E/332 con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MCENTINELA  SIVE_INDACTMC_MEDCEN ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_INDACTMC_MEDCEN
      where
        /*  %JoinFKPK(SIVE_INDACTMC_MEDCEN,:%Old," = "," and") */
        SIVE_INDACTMC_MEDCEN.CD_PCENTI = :old.CD_PCENTI and
        SIVE_INDACTMC_MEDCEN.CD_MEDCEN = :old.CD_MEDCEN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MCENTINELA" because "SIVE_INDACTMC_MEDCEN" exists.'*/
       'no se puede borrar de SIVE_MCENTINELA
        porque hay registros en SIVE_INDACTMC_MEDCEN con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MCENTINELA  SIVE_INDMEDRMC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_INDMEDRMC
      where
        /*  %JoinFKPK(SIVE_INDMEDRMC,:%Old," = "," and") */
        SIVE_INDMEDRMC.CD_PCENTI = :old.CD_PCENTI and
        SIVE_INDMEDRMC.CD_MEDCEN = :old.CD_MEDCEN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MCENTINELA" because "SIVE_INDMEDRMC" exists.'*/
       'no se puede borrar de SIVE_MCENTINELA
        porque hay registros en SIVE_INDMEDRMC con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MCENTINELA  SIVE_NOTIF_RMC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIF_RMC
      where
        /*  %JoinFKPK(SIVE_NOTIF_RMC,:%Old," = "," and") */
        SIVE_NOTIF_RMC.CD_PCENTI = :old.CD_PCENTI and
        SIVE_NOTIF_RMC.CD_MEDCEN = :old.CD_MEDCEN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MCENTINELA" because "SIVE_NOTIF_RMC" exists.'*/
       'no se puede borrar de SIVE_MCENTINELA
        porque hay registros en SIVE_NOTIF_RMC con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MCENTINELA  SIVE_POBLAC_TS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_POBLAC_TS
      where
        /*  %JoinFKPK(SIVE_POBLAC_TS,:%Old," = "," and") */
        SIVE_POBLAC_TS.CD_PCENTI = :old.CD_PCENTI and
        SIVE_POBLAC_TS.CD_MEDCEN = :old.CD_MEDCEN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MCENTINELA" because "SIVE_POBLAC_TS" exists.'*/
       'no se puede borrar de SIVE_MCENTINELA
        porque hay registros en SIVE_POBLAC_TS con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MCENTINELA_TRIG_A_I_1 after INSERT on SIVE_MCENTINELA for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_MCENTINELA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MOTIVO_BAJA  SIVE_MCENTINELA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MOTIVO_BAJA
      where
        /* %JoinFKPK(:%New,SIVE_MOTIVO_BAJA," = "," and") */
        :new.CD_MOTBAJA = SIVE_MOTIVO_BAJA.CD_MOTBAJA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_MOTBAJA is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MCENTINELA" because "SIVE_MOTIVO_BAJA" does not exist.'*/
          'no se puede grabar en SIVE_MCENTINELA
           porque no existe la clave en SIVE_MOTIVO_BAJA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_SEXO  SIVE_MCENTINELA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEXO
      where
        /* %JoinFKPK(:%New,SIVE_SEXO," = "," and") */
        :new.CD_SEXO = SIVE_SEXO.CD_SEXO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MCENTINELA" because "SIVE_SEXO" does not exist.'*/
          'no se puede grabar en SIVE_MCENTINELA
           porque no existe la clave en SIVE_SEXO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MASISTENCIA  SIVE_MCENTINELA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MASISTENCIA
      where
        /* %JoinFKPK(:%New,SIVE_MASISTENCIA," = "," and") */
        :new.CD_MASIST = SIVE_MASISTENCIA.CD_MASIST;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MCENTINELA" because "SIVE_MASISTENCIA" does not exist.'*/
          'no se puede grabar en SIVE_MCENTINELA
           porque no existe la clave en SIVE_MASISTENCIA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_E_NOTIF  SIVE_MCENTINELA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_E_NOTIF
      where
        /* %JoinFKPK(:%New,SIVE_E_NOTIF," = "," and") */
        :new.CD_E_NOTIF = SIVE_E_NOTIF.CD_E_NOTIF;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MCENTINELA" because "SIVE_E_NOTIF" does not exist.'*/
          'no se puede grabar en SIVE_MCENTINELA
           porque no existe la clave en SIVE_E_NOTIF'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_PCENTINELA  SIVE_MCENTINELA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PCENTINELA
      where
        /* %JoinFKPK(:%New,SIVE_PCENTINELA," = "," and") */
        :new.CD_PCENTI = SIVE_PCENTINELA.CD_PCENTI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MCENTINELA" because "SIVE_PCENTINELA" does not exist.'*/
          'no se puede grabar en SIVE_MCENTINELA
           porque no existe la clave en SIVE_PCENTINELA'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MCENTINELA_TRIG_A_U_1 after UPDATE on SIVE_MCENTINELA for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_MCENTINELA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MCENTINELA R/253 SIVE_AVISOS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PCENTI <> :new.CD_PCENTI or 
    :old.CD_MEDCEN <> :new.CD_MEDCEN
  then
    select count(*) into numrows
      from SIVE_AVISOS
      where
        /*  %JoinFKPK(SIVE_AVISOS,:%Old," = "," and") */
        SIVE_AVISOS.CD_PCENTI = :old.CD_PCENTI and
        SIVE_AVISOS.CD_MEDCEN = :old.CD_MEDCEN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MCENTINELA" because "SIVE_AVISOS" exists.'*/
       'no se puede modificar SIVE_MCENTINELA
        porque hay registros en E/332 con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MCENTINELA  SIVE_INDACTMC_MEDCEN ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PCENTI <> :new.CD_PCENTI or 
    :old.CD_MEDCEN <> :new.CD_MEDCEN
  then
    select count(*) into numrows
      from SIVE_INDACTMC_MEDCEN
      where
        /*  %JoinFKPK(SIVE_INDACTMC_MEDCEN,:%Old," = "," and") */
        SIVE_INDACTMC_MEDCEN.CD_PCENTI = :old.CD_PCENTI and
        SIVE_INDACTMC_MEDCEN.CD_MEDCEN = :old.CD_MEDCEN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MCENTINELA" because "SIVE_INDACTMC_MEDCEN" exists.'*/
       'no se puede modificar SIVE_MCENTINELA
        porque hay registros en SIVE_INDACTMC_MEDCEN con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MCENTINELA  SIVE_INDMEDRMC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PCENTI <> :new.CD_PCENTI or 
    :old.CD_MEDCEN <> :new.CD_MEDCEN
  then
    select count(*) into numrows
      from SIVE_INDMEDRMC
      where
        /*  %JoinFKPK(SIVE_INDMEDRMC,:%Old," = "," and") */
        SIVE_INDMEDRMC.CD_PCENTI = :old.CD_PCENTI and
        SIVE_INDMEDRMC.CD_MEDCEN = :old.CD_MEDCEN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MCENTINELA" because "SIVE_INDMEDRMC" exists.'*/
       'no se puede modificar SIVE_MCENTINELA
        porque hay registros en SIVE_INDMEDRMC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MCENTINELA  SIVE_NOTIF_RMC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PCENTI <> :new.CD_PCENTI or 
    :old.CD_MEDCEN <> :new.CD_MEDCEN
  then
    select count(*) into numrows
      from SIVE_NOTIF_RMC
      where
        /*  %JoinFKPK(SIVE_NOTIF_RMC,:%Old," = "," and") */
        SIVE_NOTIF_RMC.CD_PCENTI = :old.CD_PCENTI and
        SIVE_NOTIF_RMC.CD_MEDCEN = :old.CD_MEDCEN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MCENTINELA" because "SIVE_NOTIF_RMC" exists.'*/
       'no se puede modificar SIVE_MCENTINELA
        porque hay registros en SIVE_NOTIF_RMC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MCENTINELA  SIVE_POBLAC_TS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PCENTI <> :new.CD_PCENTI or 
    :old.CD_MEDCEN <> :new.CD_MEDCEN
  then
    select count(*) into numrows
      from SIVE_POBLAC_TS
      where
        /*  %JoinFKPK(SIVE_POBLAC_TS,:%Old," = "," and") */
        SIVE_POBLAC_TS.CD_PCENTI = :old.CD_PCENTI and
        SIVE_POBLAC_TS.CD_MEDCEN = :old.CD_MEDCEN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MCENTINELA" because "SIVE_POBLAC_TS" exists.'*/
       'no se puede modificar SIVE_MCENTINELA
        porque hay registros en SIVE_POBLAC_TS con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MOTIVO_BAJA  SIVE_MCENTINELA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MOTIVO_BAJA
    where
      /* %JoinFKPK(:%New,SIVE_MOTIVO_BAJA," = "," and") */
      :new.CD_MOTBAJA = SIVE_MOTIVO_BAJA.CD_MOTBAJA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_MOTBAJA is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MCENTINELA" because "SIVE_MOTIVO_BAJA" does not exist.'*/
         'no se puede actualizar SIVE_MCENTINELA
          porque no existe la clave en SIVE_MOTIVO_BAJA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_SEXO  SIVE_MCENTINELA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEXO
    where
      /* %JoinFKPK(:%New,SIVE_SEXO," = "," and") */
      :new.CD_SEXO = SIVE_SEXO.CD_SEXO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MCENTINELA" because "SIVE_SEXO" does not exist.'*/
         'no se puede actualizar SIVE_MCENTINELA
          porque no existe la clave en SIVE_SEXO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MASISTENCIA  SIVE_MCENTINELA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MASISTENCIA
    where
      /* %JoinFKPK(:%New,SIVE_MASISTENCIA," = "," and") */
      :new.CD_MASIST = SIVE_MASISTENCIA.CD_MASIST;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MCENTINELA" because "SIVE_MASISTENCIA" does not exist.'*/
         'no se puede actualizar SIVE_MCENTINELA
          porque no existe la clave en SIVE_MASISTENCIA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_E_NOTIF  SIVE_MCENTINELA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_E_NOTIF
    where
      /* %JoinFKPK(:%New,SIVE_E_NOTIF," = "," and") */
      :new.CD_E_NOTIF = SIVE_E_NOTIF.CD_E_NOTIF;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MCENTINELA" because "SIVE_E_NOTIF" does not exist.'*/
         'no se puede actualizar SIVE_MCENTINELA
          porque no existe la clave en SIVE_E_NOTIF'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_PCENTINELA  SIVE_MCENTINELA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PCENTINELA
    where
      /* %JoinFKPK(:%New,SIVE_PCENTINELA," = "," and") */
      :new.CD_PCENTI = SIVE_PCENTINELA.CD_PCENTI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MCENTINELA" because "SIVE_PCENTINELA" does not exist.'*/
         'no se puede actualizar SIVE_MCENTINELA
          porque no existe la clave en SIVE_PCENTINELA'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MCOMERC_ALI_TRIG_A_D_1 after DELETE on SIVE_MCOMERC_ALI for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_MCOMERC_ALI 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MCOMERC_ALI R/315 SIVE_ALI_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_MCOMERCALI = :old.CD_MCOMERCALI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MCOMERC_ALI" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede borrar de SIVE_MCOMERC_ALI
        porque hay registros en SIVE_ALI_BROTE con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MCOMERC_ALI_TRIG_A_U_1 after UPDATE on SIVE_MCOMERC_ALI for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_MCOMERC_ALI 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MCOMERC_ALI R/315 SIVE_ALI_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_MCOMERCALI <> :new.CD_MCOMERCALI
  then
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_MCOMERCALI = :old.CD_MCOMERCALI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MCOMERC_ALI" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede modificar SIVE_MCOMERC_ALI
        porque hay registros en SIVE_ALI_BROTE con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MEC_TRANS_TRIG_A_D_1 after DELETE on SIVE_MEC_TRANS for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_MEC_TRANS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MEC_TRANS  SIVE_BROTES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /*  %JoinFKPK(SIVE_BROTES,:%Old," = "," and") */
        SIVE_BROTES.CD_TRANSMIS = :old.CD_TRANSMIS;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MEC_TRANS" because "SIVE_BROTES" exists.'*/
       'no se puede borrar de SIVE_MEC_TRANS
        porque hay registros en SIVE_BROTES con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MEC_TRANS_TRIG_A_U_1 after UPDATE on SIVE_MEC_TRANS for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_MEC_TRANS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MEC_TRANS  SIVE_BROTES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TRANSMIS <> :new.CD_TRANSMIS
  then
    select count(*) into numrows
      from SIVE_BROTES
      where
        /*  %JoinFKPK(SIVE_BROTES,:%Old," = "," and") */
        SIVE_BROTES.CD_TRANSMIS = :old.CD_TRANSMIS;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MEC_TRANS" because "SIVE_BROTES" exists.'*/
       'no se puede modificar SIVE_MEC_TRANS
        porque hay registros en SIVE_BROTES con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MEDIDAS_TRIG_A_D_1 after DELETE on SIVE_MEDIDAS for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_MEDIDAS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MEDIDAS  SIVE_BROTES_MED ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES_MED
      where
        /*  %JoinFKPK(SIVE_BROTES_MED,:%Old," = "," and") */
        SIVE_BROTES_MED.CD_MEDIDA = :old.CD_MEDIDA and
        SIVE_BROTES_MED.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MEDIDAS" because "SIVE_BROTES_MED" exists.'*/
       'no se puede borrar de SIVE_MEDIDAS
        porque hay registros en SIVE_BROTES_MED con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MEDIDAS_TRIG_A_I_1 after INSERT on SIVE_MEDIDAS for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_MEDIDAS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_GRUPO_BROTE  SIVE_MEDIDAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_GRUPO_BROTE
      where
        /* %JoinFKPK(:%New,SIVE_GRUPO_BROTE," = "," and") */
        :new.CD_GRUPO = SIVE_GRUPO_BROTE.CD_GRUPO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MEDIDAS" because "SIVE_GRUPO_BROTE" does not exist.'*/
          'no se puede grabar en SIVE_MEDIDAS
           porque no existe la clave en SIVE_GRUPO_BROTE'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MEDIDAS_TRIG_A_U_1 after UPDATE on SIVE_MEDIDAS for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_MEDIDAS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MEDIDAS  SIVE_BROTES_MED ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_MEDIDA <> :new.CD_MEDIDA or 
    :old.CD_GRUPO <> :new.CD_GRUPO
  then
    select count(*) into numrows
      from SIVE_BROTES_MED
      where
        /*  %JoinFKPK(SIVE_BROTES_MED,:%Old," = "," and") */
        SIVE_BROTES_MED.CD_MEDIDA = :old.CD_MEDIDA and
        SIVE_BROTES_MED.CD_GRUPO = :old.CD_GRUPO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MEDIDAS" because "SIVE_BROTES_MED" exists.'*/
       'no se puede modificar SIVE_MEDIDAS
        porque hay registros en SIVE_BROTES_MED con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_GRUPO_BROTE  SIVE_MEDIDAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_GRUPO_BROTE
    where
      /* %JoinFKPK(:%New,SIVE_GRUPO_BROTE," = "," and") */
      :new.CD_GRUPO = SIVE_GRUPO_BROTE.CD_GRUPO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MEDIDAS" because "SIVE_GRUPO_BROTE" does not exist.'*/
         'no se puede actualizar SIVE_MEDIDAS
          porque no existe la clave en SIVE_GRUPO_BROTE'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MICROTOX_MUEST_TRIG_A_I_1 after INSERT on SIVE_MICROTOX_MUESTRAS for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- INSERT trigger on SIVE_MICROTOX_MUESTRAS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_AGENTE_TOXICO  SIVE_MICROTOX_MUESTRAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_AGENTE_TOXICO
      where
        /* %JoinFKPK(:%New,SIVE_AGENTE_TOXICO," = "," and") */
        :new.CD_GRUPO = SIVE_AGENTE_TOXICO.CD_GRUPO and
        :new.CD_AGENTET = SIVE_AGENTE_TOXICO.CD_AGENTET;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MICROTOX_MUESTRAS" because "SIVE_AGENTE_TOXICO" does not exist.'*/
          'no se puede grabar en SIVE_MICROTOX_MUESTRAS
           porque no existe la clave en SIVE_AGENTE_TOXICO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MUESTRAS_BROTE  SIVE_MICROTOX_MUESTRAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUESTRAS_BROTE
      where
        /* %JoinFKPK(:%New,SIVE_MUESTRAS_BROTE," = "," and") */
        :new.NM_MUESTRA = SIVE_MUESTRAS_BROTE.NM_MUESTRA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MICROTOX_MUESTRAS" because "SIVE_MUESTRAS_BROTE" does not exist.'*/
          'no se puede grabar en SIVE_MICROTOX_MUESTRAS
           porque no existe la clave en SIVE_MUESTRAS_BROTE'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MICROTOX_MUEST_TRIG_A_U_1 after UPDATE on SIVE_MICROTOX_MUESTRAS for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- UPDATE trigger on SIVE_MICROTOX_MUESTRAS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_AGENTE_TOXICO  SIVE_MICROTOX_MUESTRAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_AGENTE_TOXICO
    where
      /* %JoinFKPK(:%New,SIVE_AGENTE_TOXICO," = "," and") */
      :new.CD_GRUPO = SIVE_AGENTE_TOXICO.CD_GRUPO and
      :new.CD_AGENTET = SIVE_AGENTE_TOXICO.CD_AGENTET;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MICROTOX_MUESTRAS" because "SIVE_AGENTE_TOXICO" does not exist.'*/
         'no se puede actualizar SIVE_MICROTOX_MUESTRAS
          porque no existe la clave en SIVE_AGENTE_TOXICO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
  /* SIVE_MUESTRAS_BROTE  SIVE_MICROTOX_MUESTRAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MUESTRAS_BROTE
    where
      /* %JoinFKPK(:%New,SIVE_MUESTRAS_BROTE," = "," and") */
      :new.NM_MUESTRA = SIVE_MUESTRAS_BROTE.NM_MUESTRA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MICROTOX_MUESTRAS" because "SIVE_MUESTRAS_BROTE" does not exist.'*/
         'no se puede actualizar SIVE_MICROTOX_MUESTRAS
          porque no existe la clave en SIVE_MUESTRAS_BROTE'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MODELO_TRIG_A_D_1 after DELETE on SIVE_MODELO for each row
-- ERwin Builtin Wed Oct 31 16:28:49 2001
-- DELETE trigger on SIVE_MODELO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_MODELO  SIVE_LINEASM ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_LINEASM
      where
        /*  %JoinFKPK(SIVE_LINEASM,:%Old," = "," and") */
        SIVE_LINEASM.CD_TSIVE = :old.CD_TSIVE and
        SIVE_LINEASM.CD_MODELO = :old.CD_MODELO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MODELO" because "SIVE_LINEASM" exists.'*/
       'no se puede borrar de SIVE_MODELO
        porque hay registros en SIVE_LINEASM con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:49 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MODELO_TRIG_A_I_1 after INSERT on SIVE_MODELO for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- INSERT trigger on SIVE_MODELO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_NIVEL2_S R/293 SIVE_MODELO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL2_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      :new.CD_NIVEL_2 is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MODELO" because "SIVE_NIVEL2_S" does not exist.'*/
          'no se puede grabar en SIVE_MODELO
           porque no existe la clave en SIVE_NIVEL2_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_BROTES  SIVE_MODELO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
        :new.CD_ANO = SIVE_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_ANO is not null and
      :new.NM_ALERBRO is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MODELO" because "SIVE_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_MODELO
           porque no existe la clave en SIVE_BROTES'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_COM_AUT  SIVE_MODELO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_COM_AUT
      where
        /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
        :new.CD_CA = SIVE_COM_AUT.CD_CA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_CA is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MODELO" because "SIVE_COM_AUT" does not exist.'*/
          'no se puede grabar en SIVE_MODELO
           porque no existe la clave en SIVE_COM_AUT'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_PROCESOS  SIVE_MODELO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PROCESOS
      where
        /* %JoinFKPK(:%New,SIVE_PROCESOS," = "," and") */
        :new.CD_ENFCIE = SIVE_PROCESOS.CD_ENFCIE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_ENFCIE is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MODELO" because "SIVE_PROCESOS" does not exist.'*/
          'no se puede grabar en SIVE_MODELO
           porque no existe la clave en SIVE_PROCESOS'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_NIVEL1_S  SIVE_MODELO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL1_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MODELO" because "SIVE_NIVEL1_S" does not exist.'*/
          'no se puede grabar en SIVE_MODELO
           porque no existe la clave en SIVE_NIVEL1_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:49 2001 */
    /* SIVE_TSIVE  SIVE_MODELO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_TSIVE
      where
        /* %JoinFKPK(:%New,SIVE_TSIVE," = "," and") */
        :new.CD_TSIVE = SIVE_TSIVE.CD_TSIVE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MODELO" because "SIVE_TSIVE" does not exist.'*/
          'no se puede grabar en SIVE_MODELO
           porque no existe la clave en SIVE_TSIVE'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MODELO_TRIG_A_U_1 after UPDATE on SIVE_MODELO for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_MODELO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_MODELO  SIVE_LINEASM ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TSIVE <> :new.CD_TSIVE or 
    :old.CD_MODELO <> :new.CD_MODELO
  then
    select count(*) into numrows
      from SIVE_LINEASM
      where
        /*  %JoinFKPK(SIVE_LINEASM,:%Old," = "," and") */
        SIVE_LINEASM.CD_TSIVE = :old.CD_TSIVE and
        SIVE_LINEASM.CD_MODELO = :old.CD_MODELO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MODELO" because "SIVE_LINEASM" exists.'*/
       'no se puede modificar SIVE_MODELO
        porque hay registros en SIVE_LINEASM con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NIVEL2_S R/293 SIVE_MODELO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL2_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
      :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    :new.CD_NIVEL_2 is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MODELO" because "SIVE_NIVEL2_S" does not exist.'*/
         'no se puede actualizar SIVE_MODELO
          porque no existe la clave en SIVE_NIVEL2_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_BROTES  SIVE_MODELO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
      :new.CD_ANO = SIVE_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_ANO is not null and
    :new.NM_ALERBRO is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MODELO" because "SIVE_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_MODELO
          porque no existe la clave en SIVE_BROTES'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_COM_AUT  SIVE_MODELO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_COM_AUT
    where
      /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
      :new.CD_CA = SIVE_COM_AUT.CD_CA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_CA is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MODELO" because "SIVE_COM_AUT" does not exist.'*/
         'no se puede actualizar SIVE_MODELO
          porque no existe la clave en SIVE_COM_AUT'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_PROCESOS  SIVE_MODELO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PROCESOS
    where
      /* %JoinFKPK(:%New,SIVE_PROCESOS," = "," and") */
      :new.CD_ENFCIE = SIVE_PROCESOS.CD_ENFCIE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_ENFCIE is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MODELO" because "SIVE_PROCESOS" does not exist.'*/
         'no se puede actualizar SIVE_MODELO
          porque no existe la clave en SIVE_PROCESOS'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NIVEL1_S  SIVE_MODELO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL1_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MODELO" because "SIVE_NIVEL1_S" does not exist.'*/
         'no se puede actualizar SIVE_MODELO
          porque no existe la clave en SIVE_NIVEL1_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_TSIVE  SIVE_MODELO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_TSIVE
    where
      /* %JoinFKPK(:%New,SIVE_TSIVE," = "," and") */
      :new.CD_TSIVE = SIVE_TSIVE.CD_TSIVE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MODELO" because "SIVE_TSIVE" does not exist.'*/
         'no se puede actualizar SIVE_MODELO
          porque no existe la clave en SIVE_TSIVE'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MOTIVO_BAJA_TRIG_A_D_1 after DELETE on SIVE_MOTIVO_BAJA for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_MOTIVO_BAJA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_MOTIVO_BAJA R/291 SIVE_HIST_BROTES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_HIST_BROTES
      where
        /*  %JoinFKPK(SIVE_HIST_BROTES,:%Old," = "," and") */
        SIVE_HIST_BROTES.CD_MOTBAJA = :old.CD_MOTBAJA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MOTIVO_BAJA" because "SIVE_HIST_BROTES" exists.'*/
       'no se puede borrar de SIVE_MOTIVO_BAJA
        porque hay registros en SIVE_HIST_BROTES con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_MOTIVO_BAJA  SIVE_ENFERMO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ENFERMO
      where
        /*  %JoinFKPK(SIVE_ENFERMO,:%Old," = "," and") */
        SIVE_ENFERMO.CD_MOTBAJA = :old.CD_MOTBAJA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MOTIVO_BAJA" because "SIVE_ENFERMO" exists.'*/
       'no se puede borrar de SIVE_MOTIVO_BAJA
        porque hay registros en SIVE_ENFERMO con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_MOTIVO_BAJA  SIVE_MCENTINELA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MCENTINELA
      where
        /*  %JoinFKPK(SIVE_MCENTINELA,:%Old," = "," and") */
        SIVE_MCENTINELA.CD_MOTBAJA = :old.CD_MOTBAJA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MOTIVO_BAJA" because "SIVE_MCENTINELA" exists.'*/
       'no se puede borrar de SIVE_MOTIVO_BAJA
        porque hay registros en SIVE_MCENTINELA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MOTIVO_BAJA_TRIG_A_U_1 after UPDATE on SIVE_MOTIVO_BAJA for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_MOTIVO_BAJA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_MOTIVO_BAJA R/291 SIVE_HIST_BROTES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_MOTBAJA <> :new.CD_MOTBAJA
  then
    select count(*) into numrows
      from SIVE_HIST_BROTES
      where
        /*  %JoinFKPK(SIVE_HIST_BROTES,:%Old," = "," and") */
        SIVE_HIST_BROTES.CD_MOTBAJA = :old.CD_MOTBAJA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MOTIVO_BAJA" because "SIVE_HIST_BROTES" exists.'*/
       'no se puede modificar SIVE_MOTIVO_BAJA
        porque hay registros en SIVE_HIST_BROTES con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_MOTIVO_BAJA  SIVE_ENFERMO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_MOTBAJA <> :new.CD_MOTBAJA
  then
    select count(*) into numrows
      from SIVE_ENFERMO
      where
        /*  %JoinFKPK(SIVE_ENFERMO,:%Old," = "," and") */
        SIVE_ENFERMO.CD_MOTBAJA = :old.CD_MOTBAJA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MOTIVO_BAJA" because "SIVE_ENFERMO" exists.'*/
       'no se puede modificar SIVE_MOTIVO_BAJA
        porque hay registros en SIVE_ENFERMO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_MOTIVO_BAJA  SIVE_MCENTINELA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_MOTBAJA <> :new.CD_MOTBAJA
  then
    select count(*) into numrows
      from SIVE_MCENTINELA
      where
        /*  %JoinFKPK(SIVE_MCENTINELA,:%Old," = "," and") */
        SIVE_MCENTINELA.CD_MOTBAJA = :old.CD_MOTBAJA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MOTIVO_BAJA" because "SIVE_MCENTINELA" exists.'*/
       'no se puede modificar SIVE_MOTIVO_BAJA
        porque hay registros en SIVE_MCENTINELA con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MOTIVO_TRAT_TRIG_A_D_1 after DELETE on SIVE_MOTIVO_TRAT for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_MOTIVO_TRAT 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_MOTIVO_TRAT  SIVE_TRATAMIENTOS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_TRATAMIENTOS
      where
        /*  %JoinFKPK(SIVE_TRATAMIENTOS,:%Old," = "," and") */
        SIVE_TRATAMIENTOS.CD_MOTRATINI = :old.CD_MOTRAT;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MOTIVO_TRAT" because "SIVE_TRATAMIENTOS" exists.'*/
       'no se puede borrar de SIVE_MOTIVO_TRAT
        porque hay registros en SIVE_TRATAMIENTOS con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_MOTIVO_TRAT  SIVE_TRATAMIENTOS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_TRATAMIENTOS
      where
        /*  %JoinFKPK(SIVE_TRATAMIENTOS,:%Old," = "," and") */
        SIVE_TRATAMIENTOS.CD_MOTRATFIN = :old.CD_MOTRAT;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MOTIVO_TRAT" because "SIVE_TRATAMIENTOS" exists.'*/
       'no se puede borrar de SIVE_MOTIVO_TRAT
        porque hay registros en SIVE_TRATAMIENTOS con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MOTIVO_TRAT_TRIG_A_U_1 after UPDATE on SIVE_MOTIVO_TRAT for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_MOTIVO_TRAT 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_MOTIVO_TRAT  SIVE_TRATAMIENTOS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_MOTRAT <> :new.CD_MOTRAT
  then
    select count(*) into numrows
      from SIVE_TRATAMIENTOS
      where
        /*  %JoinFKPK(SIVE_TRATAMIENTOS,:%Old," = "," and") */
        SIVE_TRATAMIENTOS.CD_MOTRATINI = :old.CD_MOTRAT;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MOTIVO_TRAT" because "SIVE_TRATAMIENTOS" exists.'*/
       'no se puede modificar SIVE_MOTIVO_TRAT
        porque hay registros en SIVE_TRATAMIENTOS con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_MOTIVO_TRAT  SIVE_TRATAMIENTOS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_MOTRAT <> :new.CD_MOTRAT
  then
    select count(*) into numrows
      from SIVE_TRATAMIENTOS
      where
        /*  %JoinFKPK(SIVE_TRATAMIENTOS,:%Old," = "," and") */
        SIVE_TRATAMIENTOS.CD_MOTRATFIN = :old.CD_MOTRAT;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MOTIVO_TRAT" because "SIVE_TRATAMIENTOS" exists.'*/
       'no se puede modificar SIVE_MOTIVO_TRAT
        porque hay registros en SIVE_TRATAMIENTOS con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MOTIVO_SALRTBC_TRIG_A_D_1 after DELETE on SIVE_MOTIVOS_SALRTBC for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_MOTIVOS_SALRTBC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_MOTIVOS_SALRTBC  SIVE_REGISTROTBC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_REGISTROTBC
      where
        /*  %JoinFKPK(SIVE_REGISTROTBC,:%Old," = "," and") */
        SIVE_REGISTROTBC.CD_MOTSALRTBC = :old.CD_MOTSALRTBC;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MOTIVOS_SALRTBC" because "SIVE_REGISTROTBC" exists.'*/
       'no se puede borrar de SIVE_MOTIVOS_SALRTBC
        porque hay registros en SIVE_REGISTROTBC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MOTIVO_SALRTBC_TRIG_A_U_1 after UPDATE on SIVE_MOTIVOS_SALRTBC for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_MOTIVOS_SALRTBC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_MOTIVOS_SALRTBC  SIVE_REGISTROTBC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_MOTSALRTBC <> :new.CD_MOTSALRTBC
  then
    select count(*) into numrows
      from SIVE_REGISTROTBC
      where
        /*  %JoinFKPK(SIVE_REGISTROTBC,:%Old," = "," and") */
        SIVE_REGISTROTBC.CD_MOTSALRTBC = :old.CD_MOTSALRTBC;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MOTIVOS_SALRTBC" because "SIVE_REGISTROTBC" exists.'*/
       'no se puede modificar SIVE_MOTIVOS_SALRTBC
        porque hay registros en SIVE_REGISTROTBC con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MOV_ENFERMO_TRIG_A_I_1 after INSERT on SIVE_MOV_ENFERMO for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- INSERT trigger on SIVE_MOV_ENFERMO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_ENFERMO R_80 SIVE_MOV_ENFERMO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ENFERMO
      where
        /* %JoinFKPK(:%New,SIVE_ENFERMO," = "," and") */
        :new.CD_ENFERMO = SIVE_ENFERMO.CD_ENFERMO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MOV_ENFERMO" because "SIVE_ENFERMO" does not exist.'*/
          'no se puede grabar en SIVE_MOV_ENFERMO
           porque no existe la clave en SIVE_ENFERMO'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MOV_ENFERMO_TRIG_A_U_1 after UPDATE on SIVE_MOV_ENFERMO for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_MOV_ENFERMO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_ENFERMO R_80 SIVE_MOV_ENFERMO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ENFERMO
    where
      /* %JoinFKPK(:%New,SIVE_ENFERMO," = "," and") */
      :new.CD_ENFERMO = SIVE_ENFERMO.CD_ENFERMO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MOV_ENFERMO" because "SIVE_ENFERMO" does not exist.'*/
         'no se puede actualizar SIVE_MOV_ENFERMO
          porque no existe la clave en SIVE_ENFERMO'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

create trigger SIVE_MOV_URGDIAG_TRIG_A_I
  AFTER INSERT
  on SIVE_MOV_URGDIAG
  
  for each row
declare numrows INTEGER;
begin

    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_CAUSAURG R/30 SIVE_MOV_URGDIAG ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_CAUSAURG
      where
        /* %JoinFKPK(:%New,SIVE_CAUSAURG," = "," and") */
        :new.CD_CAUSAURG = SIVE_CAUSAURG.CD_CAUSAURG;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_MOV_URGDIAG" because a que no existe en "SIVE_CAUSAURG".'
      );
    end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_MOV_URGDIAG_TRIG_A_U
  AFTER UPDATE
  on SIVE_MOV_URGDIAG
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_MOV_URGENCIAS R/34 SIVE_MOV_URGDIAG ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MOV_URGENCIAS
    where
      /* %JoinFKPK(:%New,SIVE_MOV_URGENCIAS," = "," and") */
      :new.NM_MOVURG = SIVE_MOV_URGENCIAS.NM_MOVURG;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_MOV_URGDIAG" debido a que no existe en "SIVE_MOV_URGENCIAS".'
    );
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_CAUSAURG R/30 SIVE_MOV_URGDIAG ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_CAUSAURG
    where
      /* %JoinFKPK(:%New,SIVE_CAUSAURG," = "," and") */
      :new.CD_CAUSAURG = SIVE_CAUSAURG.CD_CAUSAURG;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_MOV_URGDIAG" debido a que no existe en "SIVE_CAUSAURG".'
    );
  end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_MOV_URGENCIAS_TRIG_A_I
  AFTER INSERT
  on SIVE_MOV_URGENCIAS
  
  for each row
declare numrows INTEGER;
        v_Contador NUMBER;
        Longitud   NUMBER;
begin
/* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_ZONA_BASICA R/60 SIVE_MOV_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ZONA_BASICA
      where
        /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
        :new.CD_NIVEL_1 = SIVE_ZONA_BASICA.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_ZONA_BASICA.CD_NIVEL_2 and
        :new.CD_ZBS = SIVE_ZONA_BASICA.CD_ZBS;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      :new.CD_NIVEL_2 is not null and
      :new.CD_ZBS is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_MOV_URGENCIAS" because a que no existe en "SIVE_ZONA_BASICA".'
      );
    end if;

/* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_MUNI_URGEN R/58 SIVE_MOV_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUNI_URGEN
      where
        /* %JoinFKPK(:%New,SIVE_MUNI_URGEN," = "," and") */
        :new.CD_MUNI = SIVE_MUNI_URGEN.CD_MUNI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_MUNI is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_MOV_URGENCIAS" because a que no existe en "SIVE_MUNI_URGEN".'
      );
    end if;

/* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_SEXO R/51 SIVE_MOV_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEXO
      where
        /* %JoinFKPK(:%New,SIVE_SEXO," = "," and") */
        :new.CD_SEXO = SIVE_SEXO.CD_SEXO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_MOV_URGENCIAS" because a que no existe en "SIVE_SEXO".'
      );
    end if;

/* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_ESPECIALIDAD R/40 SIVE_MOV_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ESPECIALIDAD
      where
        /* %JoinFKPK(:%New,SIVE_ESPECIALIDAD," = "," and") */
        :new.CD_ESPECI = SIVE_ESPECIALIDAD.CD_ESPECI and
        :new.CD_HOSP = SIVE_ESPECIALIDAD.CD_HOSP;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_MOV_URGENCIAS" because a que no existe en "SIVE_ESPECIALIDAD".'
      );
    end if;

/* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_DERIVACION R/39 SIVE_MOV_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_DERIVACION
      where
        /* %JoinFKPK(:%New,SIVE_DERIVACION," = "," and") */
        :new.CD_DERIVACION = SIVE_DERIVACION.CD_DERIVACION;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_MOV_URGENCIAS" because a que no existe en "SIVE_DERIVACION".'
      );
    end if;




 IF :new.IT_PROCESADO is null THEN
    IF :new.DS_DIAGDUP <> :new.DS_DIAGNOSTICO THEN
      /* Insertamos las causas de urgencias que vienen en el campo DS_DIAGDUP en c�digos 
        de cuatro caracteres sin separaciones de blancos */
     v_Contador := 1;
     Longitud   := LENGTH(:new.DS_DIAGDUP); /* Debe ser m�ltiplo de 4 */
     WHILE v_Contador <= Longitud LOOP
       insert into SIVE_MOV_URGDIAG (NM_MOVURG,CD_CAUSAURG)
       values (:new.NM_MOVURG, SUBSTR(:new.DS_DIAGDUP,v_Contador,4)); 
       v_Contador := v_Contador + 4;
     END LOOP; 
    END IF;
  END IF;

end;
/




create trigger SIVE_MOV_URGENCIAS_TRIG_B_I
  BEFORE INSERT
  on SIVE_MOV_URGENCIAS
  
  for each row
declare numrows INTEGER;
begin
/* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_ZONA_BASICA R/60 SIVE_MOV_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ZONA_BASICA
      where
        /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
        :new.CD_NIVEL_1 = SIVE_ZONA_BASICA.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_ZONA_BASICA.CD_NIVEL_2 and
        :new.CD_ZBS = SIVE_ZONA_BASICA.CD_ZBS;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      :new.CD_NIVEL_2 is not null and
      :new.CD_ZBS is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_MOV_URGENCIAS" because a que no existe en "SIVE_ZONA_BASICA".'
      );
    end if;

/* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_MUNI_URGEN R/58 SIVE_MOV_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUNI_URGEN
      where
        /* %JoinFKPK(:%New,SIVE_MUNI_URGEN," = "," and") */
        :new.CD_MUNI = SIVE_MUNI_URGEN.CD_MUNI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_MUNI is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_MOV_URGENCIAS" because a que no existe en "SIVE_MUNI_URGEN".'
      );
    end if;

/* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_SEXO R/51 SIVE_MOV_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEXO
      where
        /* %JoinFKPK(:%New,SIVE_SEXO," = "," and") */
        :new.CD_SEXO = SIVE_SEXO.CD_SEXO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_MOV_URGENCIAS" because a que no existe en "SIVE_SEXO".'
      );
    end if;

/* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_ESPECIALIDAD R/40 SIVE_MOV_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ESPECIALIDAD
      where
        /* %JoinFKPK(:%New,SIVE_ESPECIALIDAD," = "," and") */
        :new.CD_ESPECI = SIVE_ESPECIALIDAD.CD_ESPECI and
        :new.CD_HOSP = SIVE_ESPECIALIDAD.CD_HOSP;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_MOV_URGENCIAS" because a que no existe en "SIVE_ESPECIALIDAD".'
      );
    end if;

/* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_DERIVACION R/39 SIVE_MOV_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_DERIVACION
      where
        /* %JoinFKPK(:%New,SIVE_DERIVACION," = "," and") */
        :new.CD_DERIVACION = SIVE_DERIVACION.CD_DERIVACION;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_MOV_URGENCIAS" because a que no existe en "SIVE_DERIVACION".'
      );
    end if;





/* Secuenciador. Si no nos dan valor, le asignamos nosotros uno */
IF :new.NM_MOVURG is null THEN
 select SIVE_s_URGENCIAS.NextVal
 into :new.NM_MOVURG
 from Dual;
END IF;


end;
/


create trigger SIVE_MOV_URGENCIA_TRIG_A_U
  AFTER UPDATE
  on SIVE_MOV_URGENCIAS
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_MOV_URGENCIAS R/34 SIVE_MOV_URGDIAG ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.NM_MOVURG <> :new.NM_MOVURG
  then
    select count(*) into numrows
      from SIVE_MOV_URGDIAG
      where
        /*  %JoinFKPK(SIVE_MOV_URGDIAG,:%Old," = "," and") */
        SIVE_MOV_URGDIAG.NM_MOVURG = :old.NM_MOVURG;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_MOV_URGENCIAS" debido a que existe "SIVE_MOV_URGDIAG".'
      );
    end if;
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_ZONA_BASICA R/60 SIVE_MOV_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ZONA_BASICA
    where
      /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
      :new.CD_NIVEL_1 = SIVE_ZONA_BASICA.CD_NIVEL_1 and
      :new.CD_NIVEL_2 = SIVE_ZONA_BASICA.CD_NIVEL_2 and
      :new.CD_ZBS = SIVE_ZONA_BASICA.CD_ZBS;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    :new.CD_NIVEL_2 is not null and
    :new.CD_ZBS is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_MOV_URGENCIAS" debido a que no existe en "SIVE_ZONA_BASICA".'
    );
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_MUNI_URGEN R/58 SIVE_MOV_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MUNI_URGEN
    where
      /* %JoinFKPK(:%New,SIVE_MUNI_URGEN," = "," and") */
      :new.CD_MUNI = SIVE_MUNI_URGEN.CD_MUNI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_MUNI is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_MOV_URGENCIAS" debido a que no existe en "SIVE_MUNI_URGEN".'
    );
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_SEXO R/51 SIVE_MOV_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEXO
    where
      /* %JoinFKPK(:%New,SIVE_SEXO," = "," and") */
      :new.CD_SEXO = SIVE_SEXO.CD_SEXO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_MOV_URGENCIAS" debido a que no existe en "SIVE_SEXO".'
    );
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_ESPECIALIDAD R/40 SIVE_MOV_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ESPECIALIDAD
    where
      /* %JoinFKPK(:%New,SIVE_ESPECIALIDAD," = "," and") */
      :new.CD_ESPECI = SIVE_ESPECIALIDAD.CD_ESPECI and
      :new.CD_HOSP = SIVE_ESPECIALIDAD.CD_HOSP;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_MOV_URGENCIAS" debido a que no existe en "SIVE_ESPECIALIDAD".'
    );
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_DERIVACION R/39 SIVE_MOV_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_DERIVACION
    where
      /* %JoinFKPK(:%New,SIVE_DERIVACION," = "," and") */
      :new.CD_DERIVACION = SIVE_DERIVACION.CD_DERIVACION;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_MOV_URGENCIAS" debido a que no existe en "SIVE_DERIVACION".'
    );
  end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


CREATE OR REPLACE TRIGGER SIVE_MOV_URGENCIAS_TRIG_A_D_1 after DELETE on SIVE_MOV_URGENCIAS for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_MOV_URGENCIAS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_MOV_URGENCIAS R/346 SIVE_MOV_URGDIAG ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MOV_URGDIAG
      where
        /*  %JoinFKPK(SIVE_MOV_URGDIAG,:%Old," = "," and") */
        SIVE_MOV_URGDIAG.NM_MOVURG = :old.NM_MOVURG;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MOV_URGENCIAS" because "SIVE_MOV_URGDIAG" exists.'*/
       'no se puede borrar de SIVE_MOV_URGENCIAS
        porque hay registros en SIVE_MOV_URGDIAG con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MOVRESPEDO_TRIG_A_I_1 after INSERT on SIVE_MOVRESPEDO for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- INSERT trigger on SIVE_MOVRESPEDO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_NOTIF_EDOI R/321 SIVE_MOVRESPEDO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIF_EDOI
      where
        /* %JoinFKPK(:%New,SIVE_NOTIF_EDOI," = "," and") */
        :new.NM_EDO = SIVE_NOTIF_EDOI.NM_EDO and
        :new.CD_E_NOTIF = SIVE_NOTIF_EDOI.CD_E_NOTIF and
        :new.CD_ANOEPI = SIVE_NOTIF_EDOI.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_NOTIF_EDOI.CD_SEMEPI and
        :new.FC_RECEP = SIVE_NOTIF_EDOI.FC_RECEP and
        :new.FC_FECNOTIF = SIVE_NOTIF_EDOI.FC_FECNOTIF and
        :new.CD_FUENTE = SIVE_NOTIF_EDOI.CD_FUENTE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MOVRESPEDO" because "SIVE_NOTIF_EDOI" does not exist.'*/
          'no se puede grabar en SIVE_MOVRESPEDO
           porque no existe la clave en SIVE_NOTIF_EDOI'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_RESP_EDO  SIVE_MOVRESPEDO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_RESP_EDO
      where
        /* %JoinFKPK(:%New,SIVE_RESP_EDO," = "," and") */
        :new.CD_TSIVE = SIVE_RESP_EDO.CD_TSIVE and
        :new.NM_EDO = SIVE_RESP_EDO.NM_EDO and
        :new.CD_MODELO = SIVE_RESP_EDO.CD_MODELO and
        :new.NM_LIN = SIVE_RESP_EDO.NM_LIN and
        :new.CD_PREGUNTA = SIVE_RESP_EDO.CD_PREGUNTA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MOVRESPEDO" because "SIVE_RESP_EDO" does not exist.'*/
          'no se puede grabar en SIVE_MOVRESPEDO
           porque no existe la clave en SIVE_RESP_EDO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_NOTIF_EDOI  SIVE_MOVRESPEDO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIF_EDOI
      where
        /* %JoinFKPK(:%New,SIVE_NOTIF_EDOI," = "," and") */
        :new.NM_EDO = SIVE_NOTIF_EDOI.NM_EDO and
        :new.CD_E_NOTIF = SIVE_NOTIF_EDOI.CD_E_NOTIF and
        :new.CD_ANOEPI = SIVE_NOTIF_EDOI.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_NOTIF_EDOI.CD_SEMEPI and
        :new.FC_RECEP = SIVE_NOTIF_EDOI.FC_RECEP and
        :new.FC_FECNOTIF = SIVE_NOTIF_EDOI.FC_FECNOTIF and
        :new.CD_FUENTE = SIVE_NOTIF_EDOI.CD_FUENTE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MOVRESPEDO" because "SIVE_NOTIF_EDOI" does not exist.'*/
          'no se puede grabar en SIVE_MOVRESPEDO
           porque no existe la clave en SIVE_NOTIF_EDOI'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MOVRESPEDO_TRIG_A_U_1 after UPDATE on SIVE_MOVRESPEDO for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_MOVRESPEDO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NOTIF_EDOI R/321 SIVE_MOVRESPEDO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NOTIF_EDOI
    where
      /* %JoinFKPK(:%New,SIVE_NOTIF_EDOI," = "," and") */
      :new.NM_EDO = SIVE_NOTIF_EDOI.NM_EDO and
      :new.CD_E_NOTIF = SIVE_NOTIF_EDOI.CD_E_NOTIF and
      :new.CD_ANOEPI = SIVE_NOTIF_EDOI.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_NOTIF_EDOI.CD_SEMEPI and
      :new.FC_RECEP = SIVE_NOTIF_EDOI.FC_RECEP and
      :new.FC_FECNOTIF = SIVE_NOTIF_EDOI.FC_FECNOTIF and
      :new.CD_FUENTE = SIVE_NOTIF_EDOI.CD_FUENTE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MOVRESPEDO" because "SIVE_NOTIF_EDOI" does not exist.'*/
         'no se puede actualizar SIVE_MOVRESPEDO
          porque no existe la clave en SIVE_NOTIF_EDOI'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_RESP_EDO  SIVE_MOVRESPEDO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_RESP_EDO
    where
      /* %JoinFKPK(:%New,SIVE_RESP_EDO," = "," and") */
      :new.CD_TSIVE = SIVE_RESP_EDO.CD_TSIVE and
      :new.NM_EDO = SIVE_RESP_EDO.NM_EDO and
      :new.CD_MODELO = SIVE_RESP_EDO.CD_MODELO and
      :new.NM_LIN = SIVE_RESP_EDO.NM_LIN and
      :new.CD_PREGUNTA = SIVE_RESP_EDO.CD_PREGUNTA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MOVRESPEDO" because "SIVE_RESP_EDO" does not exist.'*/
         'no se puede actualizar SIVE_MOVRESPEDO
          porque no existe la clave en SIVE_RESP_EDO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NOTIF_EDOI  SIVE_MOVRESPEDO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NOTIF_EDOI
    where
      /* %JoinFKPK(:%New,SIVE_NOTIF_EDOI," = "," and") */
      :new.NM_EDO = SIVE_NOTIF_EDOI.NM_EDO and
      :new.CD_E_NOTIF = SIVE_NOTIF_EDOI.CD_E_NOTIF and
      :new.CD_ANOEPI = SIVE_NOTIF_EDOI.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_NOTIF_EDOI.CD_SEMEPI and
      :new.FC_RECEP = SIVE_NOTIF_EDOI.FC_RECEP and
      :new.FC_FECNOTIF = SIVE_NOTIF_EDOI.FC_FECNOTIF and
      :new.CD_FUENTE = SIVE_NOTIF_EDOI.CD_FUENTE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MOVRESPEDO" because "SIVE_NOTIF_EDOI" does not exist.'*/
         'no se puede actualizar SIVE_MOVRESPEDO
          porque no existe la clave en SIVE_NOTIF_EDOI'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MUESTRA_LAB_TRIG_A_D_1 after DELETE on SIVE_MUESTRA_LAB for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_MUESTRA_LAB 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_MUESTRA_LAB  SIVE_RESLAB ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESLAB
      where
        /*  %JoinFKPK(SIVE_RESLAB,:%Old," = "," and") */
        SIVE_RESLAB.CD_MUESTRA = :old.CD_MUESTRA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUESTRA_LAB" because "SIVE_RESLAB" exists.'*/
       'no se puede borrar de SIVE_MUESTRA_LAB
        porque hay registros en SIVE_RESLAB con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MUESTRA_LAB_TRIG_A_U_1 after UPDATE on SIVE_MUESTRA_LAB for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_MUESTRA_LAB 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_MUESTRA_LAB  SIVE_RESLAB ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_MUESTRA <> :new.CD_MUESTRA
  then
    select count(*) into numrows
      from SIVE_RESLAB
      where
        /*  %JoinFKPK(SIVE_RESLAB,:%Old," = "," and") */
        SIVE_RESLAB.CD_MUESTRA = :old.CD_MUESTRA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUESTRA_LAB" because "SIVE_RESLAB" exists.'*/
       'no se puede modificar SIVE_MUESTRA_LAB
        porque hay registros en SIVE_RESLAB con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MUESTRAS_BROTE_TRIG_A_D_1 after DELETE on SIVE_MUESTRAS_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_MUESTRAS_BROTE 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_MUESTRAS_BROTE  SIVE_MICROTOX_MUESTRAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MICROTOX_MUESTRAS
      where
        /*  %JoinFKPK(SIVE_MICROTOX_MUESTRAS,:%Old," = "," and") */
        SIVE_MICROTOX_MUESTRAS.NM_MUESTRA = :old.NM_MUESTRA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUESTRAS_BROTE" because "SIVE_MICROTOX_MUESTRAS" exists.'*/
       'no se puede borrar de SIVE_MUESTRAS_BROTE
        porque hay registros en SIVE_MICROTOX_MUESTRAS con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MUESTRAS_BROTE_TRIG_A_I_1 after INSERT on SIVE_MUESTRAS_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- INSERT trigger on SIVE_MUESTRAS_BROTE 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_BROTES  SIVE_MUESTRAS_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
        :new.CD_ANO = SIVE_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MUESTRAS_BROTE" because "SIVE_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_MUESTRAS_BROTE
           porque no existe la clave en SIVE_BROTES'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_ORIGEN_MUESTRA  SIVE_MUESTRAS_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ORIGEN_MUESTRA
      where
        /* %JoinFKPK(:%New,SIVE_ORIGEN_MUESTRA," = "," and") */
        :new.CD_MUESTRA = SIVE_ORIGEN_MUESTRA.CD_MUESTRA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MUESTRAS_BROTE" because "SIVE_ORIGEN_MUESTRA" does not exist.'*/
          'no se puede grabar en SIVE_MUESTRAS_BROTE
           porque no existe la clave en SIVE_ORIGEN_MUESTRA'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_MUESTRAS_BROTE_TRIG_A_U_1 after UPDATE on SIVE_MUESTRAS_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_MUESTRAS_BROTE 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_MUESTRAS_BROTE  SIVE_MICROTOX_MUESTRAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.NM_MUESTRA <> :new.NM_MUESTRA
  then
    select count(*) into numrows
      from SIVE_MICROTOX_MUESTRAS
      where
        /*  %JoinFKPK(SIVE_MICROTOX_MUESTRAS,:%Old," = "," and") */
        SIVE_MICROTOX_MUESTRAS.NM_MUESTRA = :old.NM_MUESTRA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUESTRAS_BROTE" because "SIVE_MICROTOX_MUESTRAS" exists.'*/
       'no se puede modificar SIVE_MUESTRAS_BROTE
        porque hay registros en SIVE_MICROTOX_MUESTRAS con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_BROTES  SIVE_MUESTRAS_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
      :new.CD_ANO = SIVE_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MUESTRAS_BROTE" because "SIVE_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_MUESTRAS_BROTE
          porque no existe la clave en SIVE_BROTES'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_ORIGEN_MUESTRA  SIVE_MUESTRAS_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ORIGEN_MUESTRA
    where
      /* %JoinFKPK(:%New,SIVE_ORIGEN_MUESTRA," = "," and") */
      :new.CD_MUESTRA = SIVE_ORIGEN_MUESTRA.CD_MUESTRA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MUESTRAS_BROTE" because "SIVE_ORIGEN_MUESTRA" does not exist.'*/
         'no se puede actualizar SIVE_MUESTRAS_BROTE
          porque no existe la clave en SIVE_ORIGEN_MUESTRA'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

create trigger SIVE_MUNI_URGEN_TRIG_A_I
  AFTER INSERT
  on SIVE_MUNI_URGEN
  
  for each row
declare numrows INTEGER;
begin

    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_MUNICIPIO R/57 SIVE_MUNI_URGEN ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
        :new.CD_MUN = SIVE_MUNICIPIO.CD_MUN and
        :new.CD_PROV = SIVE_MUNICIPIO.CD_PROV;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_MUN is not null and
      :new.CD_PROV is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_MUNI_URGEN" because a que no existe en "SIVE_MUNICIPIO".'
      );
    end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_MUNI_URGEN_TRIG_A_D
  AFTER DELETE
  on SIVE_MUNI_URGEN
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_MUNI_URGEN R/59 SIVE_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_URGENCIAS,:%Old," = "," and") */
        SIVE_URGENCIAS.CD_MUNI = :old.CD_MUNI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_MUNI_URGEN" debido a que existe en "SIVE_URGENCIAS".'
      );
    end if;

    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_MUNI_URGEN R/58 SIVE_MOV_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MOV_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_MOV_URGENCIAS,:%Old," = "," and") */
        SIVE_MOV_URGENCIAS.CD_MUNI = :old.CD_MUNI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_MUNI_URGEN" debido a que existe en "SIVE_MOV_URGENCIAS".'
      );
    end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_MUNI_URGEN_TRIG_A_U
  AFTER UPDATE
  on SIVE_MUNI_URGEN
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_MUNI_URGEN R/59 SIVE_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_MUNI <> :new.CD_MUNI
  then
    select count(*) into numrows
      from SIVE_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_URGENCIAS,:%Old," = "," and") */
        SIVE_URGENCIAS.CD_MUNI = :old.CD_MUNI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_MUNI_URGEN" debido a que existe "SIVE_URGENCIAS".'
      );
    end if;
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_MUNI_URGEN R/58 SIVE_MOV_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_MUNI <> :new.CD_MUNI
  then
    select count(*) into numrows
      from SIVE_MOV_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_MOV_URGENCIAS,:%Old," = "," and") */
        SIVE_MOV_URGENCIAS.CD_MUNI = :old.CD_MUNI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_MUNI_URGEN" debido a que existe "SIVE_MOV_URGENCIAS".'
      );
    end if;
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_MUNICIPIO R/57 SIVE_MUNI_URGEN ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MUNICIPIO
    where
      /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
      :new.CD_MUN = SIVE_MUNICIPIO.CD_MUN and
      :new.CD_PROV = SIVE_MUNICIPIO.CD_PROV;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_MUN is not null and
    :new.CD_PROV is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_MUNI_URGEN" debido a que no existe en "SIVE_MUNICIPIO".'
    );
  end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


CREATE OR REPLACE TRIGGER SIVE_MUNICIPIO_TRIG_A_D_1 after DELETE on SIVE_MUNICIPIO for each row
-- ERwin Builtin Wed Jan 02 20:28:37 2002
-- DELETE trigger on SIVE_MUNICIPIO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_MUNICIPIO R/362 SIVE_ALI_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_PROV_LPREP = :old.CD_PROV and
        SIVE_ALI_BROTE.CD_MUN_LPREP = :old.CD_MUN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUNICIPIO" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede borrar de SIVE_MUNICIPIO
        porque hay registros en SIVE_ALI_BROTE con su clave'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_MUNICIPIO R/349 SIVE_MUNI_URGEN ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MUNI_URGEN
      where
        /*  %JoinFKPK(SIVE_MUNI_URGEN,:%Old," = "," and") */
        SIVE_MUNI_URGEN.CD_PROV = :old.CD_PROV and
        SIVE_MUNI_URGEN.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUNICIPIO" because "SIVE_MUNI_URGEN" exists.'*/
       'no se puede borrar de SIVE_MUNICIPIO
        porque hay registros en SIVE_MUNI_URGEN con su clave'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_MUNICIPIO R/320 SIVE_ENFERMO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ENFERMO
      where
        /*  %JoinFKPK(SIVE_ENFERMO,:%Old," = "," and") */
        SIVE_ENFERMO.CD_PROV = :old.CD_PROV and
        SIVE_ENFERMO.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUNICIPIO" because "SIVE_ENFERMO" exists.'*/
       'no se puede borrar de SIVE_MUNICIPIO
        porque hay registros en SIVE_ENFERMO con su clave'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_MUNICIPIO R/277 SIVE_RESUMEN_EDOS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESUMEN_EDOS
      where
        /*  %JoinFKPK(SIVE_RESUMEN_EDOS,:%Old," = "," and") */
        SIVE_RESUMEN_EDOS.CD_PROV = :old.CD_PROV and
        SIVE_RESUMEN_EDOS.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUNICIPIO" because "SIVE_RESUMEN_EDOS" exists.'*/
       'no se puede borrar de SIVE_MUNICIPIO
        porque hay registros en SIVE_RESUMEN_EDOS con su clave'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_MUNICIPIO R/264 SIVE_POBLACION_NG ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_POBLACION_NG
      where
        /*  %JoinFKPK(SIVE_POBLACION_NG,:%Old," = "," and") */
        SIVE_POBLACION_NG.CD_PROV = :old.CD_PROV and
        SIVE_POBLACION_NG.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUNICIPIO" because "SIVE_POBLACION_NG" exists.'*/
       'no se puede borrar de SIVE_MUNICIPIO
        porque hay registros en SIVE_POBLACION_NG con su clave'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_MUNICIPIO  SIVE_ALERTA_ADIC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_ADIC
      where
        /*  %JoinFKPK(SIVE_ALERTA_ADIC,:%Old," = "," and") */
        SIVE_ALERTA_ADIC.CD_MINPROV = :old.CD_PROV and
        SIVE_ALERTA_ADIC.CD_MINMUN = :old.CD_MUN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUNICIPIO" because "SIVE_ALERTA_ADIC" exists.'*/
       'no se puede borrar de SIVE_MUNICIPIO
        porque hay registros en SIVE_ALERTA_ADIC con su clave'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_MUNICIPIO  SIVE_ALERTA_ADIC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_ADIC
      where
        /*  %JoinFKPK(SIVE_ALERTA_ADIC,:%Old," = "," and") */
        SIVE_ALERTA_ADIC.CD_NOTIFPROV = :old.CD_PROV and
        SIVE_ALERTA_ADIC.CD_NOTIFMUN = :old.CD_MUN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUNICIPIO" because "SIVE_ALERTA_ADIC" exists.'*/
       'no se puede borrar de SIVE_MUNICIPIO
        porque hay registros en SIVE_ALERTA_ADIC con su clave'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_MUNICIPIO  SIVE_ALERTA_BROTES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_BROTES
      where
        /*  %JoinFKPK(SIVE_ALERTA_BROTES,:%Old," = "," and") */
        SIVE_ALERTA_BROTES.CD_LEXPROV = :old.CD_PROV and
        SIVE_ALERTA_BROTES.CD_LEXMUN = :old.CD_MUN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUNICIPIO" because "SIVE_ALERTA_BROTES" exists.'*/
       'no se puede borrar de SIVE_MUNICIPIO
        porque hay registros en SIVE_ALERTA_BROTES con su clave'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_MUNICIPIO  SIVE_ALERTA_COLEC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_COLEC
      where
        /*  %JoinFKPK(SIVE_ALERTA_COLEC,:%Old," = "," and") */
        SIVE_ALERTA_COLEC.CD_PROV = :old.CD_PROV and
        SIVE_ALERTA_COLEC.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUNICIPIO" because "SIVE_ALERTA_COLEC" exists.'*/
       'no se puede borrar de SIVE_MUNICIPIO
        porque hay registros en SIVE_ALERTA_COLEC con su clave'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_MUNICIPIO  SIVE_BROTES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /*  %JoinFKPK(SIVE_BROTES,:%Old," = "," and") */
        SIVE_BROTES.CD_PROVCOL = :old.CD_PROV and
        SIVE_BROTES.CD_MUNCOL = :old.CD_MUN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUNICIPIO" because "SIVE_BROTES" exists.'*/
       'no se puede borrar de SIVE_MUNICIPIO
        porque hay registros en SIVE_BROTES con su clave'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_MUNICIPIO  SIVE_C_NOTIF ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_C_NOTIF
      where
        /*  %JoinFKPK(SIVE_C_NOTIF,:%Old," = "," and") */
        SIVE_C_NOTIF.CD_PROV = :old.CD_PROV and
        SIVE_C_NOTIF.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUNICIPIO" because "SIVE_C_NOTIF" exists.'*/
       'no se puede borrar de SIVE_MUNICIPIO
        porque hay registros en SIVE_C_NOTIF con su clave'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_MUNICIPIO  SIVE_EDOIND ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_EDOIND
      where
        /*  %JoinFKPK(SIVE_EDOIND,:%Old," = "," and") */
        SIVE_EDOIND.CD_PROV = :old.CD_PROV and
        SIVE_EDOIND.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_MUNICIPIO" because "SIVE_EDOIND" exists.'*/
       'no se puede borrar de SIVE_MUNICIPIO
        porque hay registros en SIVE_EDOIND con su clave'
      );
    end if;


-- ERwin Builtin Wed Jan 02 20:28:37 2002
END;
/

CREATE OR REPLACE TRIGGER SIVE_MUNICIPIO_TRIG_A_I_1 after INSERT on SIVE_MUNICIPIO for each row
-- ERwin Builtin Wed Jan 02 20:28:37 2002
-- INSERT trigger on SIVE_MUNICIPIO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_NIVEL2_S R/281 SIVE_MUNICIPIO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL2_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      :new.CD_NIVEL_2 is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MUNICIPIO" because "SIVE_NIVEL2_S" does not exist.'*/
          'no se puede grabar en SIVE_MUNICIPIO
           porque no existe la clave en SIVE_NIVEL2_S'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_PROVINCIA  SIVE_MUNICIPIO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PROVINCIA
      where
        /* %JoinFKPK(:%New,SIVE_PROVINCIA," = "," and") */
        :new.CD_PROV = SIVE_PROVINCIA.CD_PROV;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MUNICIPIO" because "SIVE_PROVINCIA" does not exist.'*/
          'no se puede grabar en SIVE_MUNICIPIO
           porque no existe la clave en SIVE_PROVINCIA'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_ZONA_BASICA  SIVE_MUNICIPIO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ZONA_BASICA
      where
        /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
        :new.CD_NIVEL_1 = SIVE_ZONA_BASICA.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_ZONA_BASICA.CD_NIVEL_2 and
        :new.CD_ZBS = SIVE_ZONA_BASICA.CD_ZBS;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      :new.CD_NIVEL_2 is not null and
      :new.CD_ZBS is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MUNICIPIO" because "SIVE_ZONA_BASICA" does not exist.'*/
          'no se puede grabar en SIVE_MUNICIPIO
           porque no existe la clave en SIVE_ZONA_BASICA'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_NIVEL1_S  SIVE_MUNICIPIO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL1_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MUNICIPIO" because "SIVE_NIVEL1_S" does not exist.'*/
          'no se puede grabar en SIVE_MUNICIPIO
           porque no existe la clave en SIVE_NIVEL1_S'
      );
    end if;

    /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
    /* SIVE_NIVEL2_S  SIVE_MUNICIPIO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL2_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      :new.CD_NIVEL_2 is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_MUNICIPIO" because "SIVE_NIVEL2_S" does not exist.'*/
          'no se puede grabar en SIVE_MUNICIPIO
           porque no existe la clave en SIVE_NIVEL2_S'
      );
    end if;


-- ERwin Builtin Wed Jan 02 20:28:37 2002
END;
/


CREATE OR REPLACE TRIGGER SIVE_MUNICIPIO_TRIG_A_U_1 after UPDATE on SIVE_MUNICIPIO for each row
-- ERwin Builtin Wed Jan 02 20:28:38 2002
-- UPDATE trigger on SIVE_MUNICIPIO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
  /* SIVE_MUNICIPIO R/362 SIVE_ALI_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PROV <> :new.CD_PROV or 
    :old.CD_MUN <> :new.CD_MUN
  then
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_PROV_LPREP = :old.CD_PROV and
        SIVE_ALI_BROTE.CD_MUN_LPREP = :old.CD_MUN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede modificar SIVE_MUNICIPIO
        porque hay registros en SIVE_ALI_BROTE con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
  /* SIVE_MUNICIPIO R/349 SIVE_MUNI_URGEN ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PROV <> :new.CD_PROV or 
    :old.CD_MUN <> :new.CD_MUN
  then
    select count(*) into numrows
      from SIVE_MUNI_URGEN
      where
        /*  %JoinFKPK(SIVE_MUNI_URGEN,:%Old," = "," and") */
        SIVE_MUNI_URGEN.CD_PROV = :old.CD_PROV and
        SIVE_MUNI_URGEN.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_MUNI_URGEN" exists.'*/
       'no se puede modificar SIVE_MUNICIPIO
        porque hay registros en SIVE_MUNI_URGEN con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
  /* SIVE_MUNICIPIO R/320 SIVE_ENFERMO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PROV <> :new.CD_PROV or 
    :old.CD_MUN <> :new.CD_MUN
  then
    select count(*) into numrows
      from SIVE_ENFERMO
      where
        /*  %JoinFKPK(SIVE_ENFERMO,:%Old," = "," and") */
        SIVE_ENFERMO.CD_PROV = :old.CD_PROV and
        SIVE_ENFERMO.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_ENFERMO" exists.'*/
       'no se puede modificar SIVE_MUNICIPIO
        porque hay registros en SIVE_ENFERMO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
  /* SIVE_MUNICIPIO R/277 SIVE_RESUMEN_EDOS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PROV <> :new.CD_PROV or 
    :old.CD_MUN <> :new.CD_MUN
  then
    select count(*) into numrows
      from SIVE_RESUMEN_EDOS
      where
        /*  %JoinFKPK(SIVE_RESUMEN_EDOS,:%Old," = "," and") */
        SIVE_RESUMEN_EDOS.CD_PROV = :old.CD_PROV and
        SIVE_RESUMEN_EDOS.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_RESUMEN_EDOS" exists.'*/
       'no se puede modificar SIVE_MUNICIPIO
        porque hay registros en SIVE_RESUMEN_EDOS con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
  /* SIVE_MUNICIPIO R/264 SIVE_POBLACION_NG ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PROV <> :new.CD_PROV or 
    :old.CD_MUN <> :new.CD_MUN
  then
    select count(*) into numrows
      from SIVE_POBLACION_NG
      where
        /*  %JoinFKPK(SIVE_POBLACION_NG,:%Old," = "," and") */
        SIVE_POBLACION_NG.CD_PROV = :old.CD_PROV and
        SIVE_POBLACION_NG.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_POBLACION_NG" exists.'*/
       'no se puede modificar SIVE_MUNICIPIO
        porque hay registros en SIVE_POBLACION_NG con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
  /* SIVE_MUNICIPIO  SIVE_ALERTA_ADIC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PROV <> :new.CD_PROV or 
    :old.CD_MUN <> :new.CD_MUN
  then
    select count(*) into numrows
      from SIVE_ALERTA_ADIC
      where
        /*  %JoinFKPK(SIVE_ALERTA_ADIC,:%Old," = "," and") */
        SIVE_ALERTA_ADIC.CD_MINPROV = :old.CD_PROV and
        SIVE_ALERTA_ADIC.CD_MINMUN = :old.CD_MUN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_ALERTA_ADIC" exists.'*/
       'no se puede modificar SIVE_MUNICIPIO
        porque hay registros en SIVE_ALERTA_ADIC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
  /* SIVE_MUNICIPIO  SIVE_ALERTA_ADIC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PROV <> :new.CD_PROV or 
    :old.CD_MUN <> :new.CD_MUN
  then
    select count(*) into numrows
      from SIVE_ALERTA_ADIC
      where
        /*  %JoinFKPK(SIVE_ALERTA_ADIC,:%Old," = "," and") */
        SIVE_ALERTA_ADIC.CD_NOTIFPROV = :old.CD_PROV and
        SIVE_ALERTA_ADIC.CD_NOTIFMUN = :old.CD_MUN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_ALERTA_ADIC" exists.'*/
       'no se puede modificar SIVE_MUNICIPIO
        porque hay registros en SIVE_ALERTA_ADIC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:37 2002 */
  /* SIVE_MUNICIPIO  SIVE_ALERTA_BROTES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PROV <> :new.CD_PROV or 
    :old.CD_MUN <> :new.CD_MUN
  then
    select count(*) into numrows
      from SIVE_ALERTA_BROTES
      where
        /*  %JoinFKPK(SIVE_ALERTA_BROTES,:%Old," = "," and") */
        SIVE_ALERTA_BROTES.CD_LEXPROV = :old.CD_PROV and
        SIVE_ALERTA_BROTES.CD_LEXMUN = :old.CD_MUN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_ALERTA_BROTES" exists.'*/
       'no se puede modificar SIVE_MUNICIPIO
        porque hay registros en SIVE_ALERTA_BROTES con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:38 2002 */
  /* SIVE_MUNICIPIO  SIVE_ALERTA_COLEC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PROV <> :new.CD_PROV or 
    :old.CD_MUN <> :new.CD_MUN
  then
    select count(*) into numrows
      from SIVE_ALERTA_COLEC
      where
        /*  %JoinFKPK(SIVE_ALERTA_COLEC,:%Old," = "," and") */
        SIVE_ALERTA_COLEC.CD_PROV = :old.CD_PROV and
        SIVE_ALERTA_COLEC.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_ALERTA_COLEC" exists.'*/
       'no se puede modificar SIVE_MUNICIPIO
        porque hay registros en SIVE_ALERTA_COLEC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:38 2002 */
  /* SIVE_MUNICIPIO  SIVE_BROTES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PROV <> :new.CD_PROV or 
    :old.CD_MUN <> :new.CD_MUN
  then
    select count(*) into numrows
      from SIVE_BROTES
      where
        /*  %JoinFKPK(SIVE_BROTES,:%Old," = "," and") */
        SIVE_BROTES.CD_PROVCOL = :old.CD_PROV and
        SIVE_BROTES.CD_MUNCOL = :old.CD_MUN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_BROTES" exists.'*/
       'no se puede modificar SIVE_MUNICIPIO
        porque hay registros en SIVE_BROTES con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:38 2002 */
  /* SIVE_MUNICIPIO  SIVE_C_NOTIF ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PROV <> :new.CD_PROV or 
    :old.CD_MUN <> :new.CD_MUN
  then
    select count(*) into numrows
      from SIVE_C_NOTIF
      where
        /*  %JoinFKPK(SIVE_C_NOTIF,:%Old," = "," and") */
        SIVE_C_NOTIF.CD_PROV = :old.CD_PROV and
        SIVE_C_NOTIF.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_C_NOTIF" exists.'*/
       'no se puede modificar SIVE_MUNICIPIO
        porque hay registros en SIVE_C_NOTIF con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:38 2002 */
  /* SIVE_MUNICIPIO  SIVE_EDOIND ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PROV <> :new.CD_PROV or 
    :old.CD_MUN <> :new.CD_MUN
  then
    select count(*) into numrows
      from SIVE_EDOIND
      where
        /*  %JoinFKPK(SIVE_EDOIND,:%Old," = "," and") */
        SIVE_EDOIND.CD_PROV = :old.CD_PROV and
        SIVE_EDOIND.CD_MUN = :old.CD_MUN;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_EDOIND" exists.'*/
       'no se puede modificar SIVE_MUNICIPIO
        porque hay registros en SIVE_EDOIND con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:38 2002 */
  /* SIVE_NIVEL2_S R/281 SIVE_MUNICIPIO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL2_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
      :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    :new.CD_NIVEL_2 is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_NIVEL2_S" does not exist.'*/
         'no se puede actualizar SIVE_MUNICIPIO
          porque no existe la clave en SIVE_NIVEL2_S'
    );
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:38 2002 */
  /* SIVE_PROVINCIA  SIVE_MUNICIPIO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PROVINCIA
    where
      /* %JoinFKPK(:%New,SIVE_PROVINCIA," = "," and") */
      :new.CD_PROV = SIVE_PROVINCIA.CD_PROV;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_PROVINCIA" does not exist.'*/
         'no se puede actualizar SIVE_MUNICIPIO
          porque no existe la clave en SIVE_PROVINCIA'
    );
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:38 2002 */
  /* SIVE_ZONA_BASICA  SIVE_MUNICIPIO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ZONA_BASICA
    where
      /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
      :new.CD_NIVEL_1 = SIVE_ZONA_BASICA.CD_NIVEL_1 and
      :new.CD_NIVEL_2 = SIVE_ZONA_BASICA.CD_NIVEL_2 and
      :new.CD_ZBS = SIVE_ZONA_BASICA.CD_ZBS;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    :new.CD_NIVEL_2 is not null and
    :new.CD_ZBS is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_ZONA_BASICA" does not exist.'*/
         'no se puede actualizar SIVE_MUNICIPIO
          porque no existe la clave en SIVE_ZONA_BASICA'
    );
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:38 2002 */
  /* SIVE_NIVEL1_S  SIVE_MUNICIPIO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL1_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_NIVEL1_S" does not exist.'*/
         'no se puede actualizar SIVE_MUNICIPIO
          porque no existe la clave en SIVE_NIVEL1_S'
    );
  end if;

  /* ERwin Builtin Wed Jan 02 20:28:38 2002 */
  /* SIVE_NIVEL2_S  SIVE_MUNICIPIO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL2_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
      :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    :new.CD_NIVEL_2 is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_MUNICIPIO" because "SIVE_NIVEL2_S" does not exist.'*/
         'no se puede actualizar SIVE_MUNICIPIO
          porque no existe la clave en SIVE_NIVEL2_S'
    );
  end if;


-- ERwin Builtin Wed Jan 02 20:28:38 2002
END;
/


CREATE OR REPLACE TRIGGER SIVE_NIV_ASIST_TRIG_A_D_1 after DELETE on SIVE_NIV_ASIST for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_NIV_ASIST 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_NIV_ASIST  SIVE_C_NOTIF ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_C_NOTIF
      where
        /*  %JoinFKPK(SIVE_C_NOTIF,:%Old," = "," and") */
        SIVE_C_NOTIF.CD_NIVASIS = :old.CD_NIVASIS;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_NIV_ASIST" because "SIVE_C_NOTIF" exists.'*/
       'no se puede borrar de SIVE_NIV_ASIST
        porque hay registros en SIVE_C_NOTIF con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_NIV_ASIST_TRIG_A_U_1 after UPDATE on SIVE_NIV_ASIST for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_NIV_ASIST 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NIV_ASIST  SIVE_C_NOTIF ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_NIVASIS <> :new.CD_NIVASIS
  then
    select count(*) into numrows
      from SIVE_C_NOTIF
      where
        /*  %JoinFKPK(SIVE_C_NOTIF,:%Old," = "," and") */
        SIVE_C_NOTIF.CD_NIVASIS = :old.CD_NIVASIS;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_NIV_ASIST" because "SIVE_C_NOTIF" exists.'*/
       'no se puede modificar SIVE_NIV_ASIST
        porque hay registros en SIVE_C_NOTIF con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

create trigger SIVE_NIVEL1_S_TRIG_A_D
  AFTER DELETE
  on SIVE_NIVEL1_S
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Tue Dec 28 10:18:59 1999 */
    /* SIVE_NIVEL1_S R/47 SIVE_RES_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_NIVEL_1 = :old.CD_NIVEL_1;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_NIVEL1_S" debido a que existe en "SIVE_RES_URGENCIAS".'
      );
    end if;

    /* ERwin Builtin Tue Dec 28 10:18:59 1999 */
    /* SIVE_NIVEL1_S R/28 SIVE_NIVEL2_S ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL2_S
      where
        /*  %JoinFKPK(SIVE_NIVEL2_S,:%Old," = "," and") */
        SIVE_NIVEL2_S.CD_NIVEL_1 = :old.CD_NIVEL_1;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_NIVEL1_S" debido a que existe en "SIVE_NIVEL2_S".'
      );
    end if;


-- ERwin Builtin Tue Dec 28 10:18:59 1999
end;
/


create trigger SIVE_NIVEL1_S_TRIG_A_U
  AFTER UPDATE
  on SIVE_NIVEL1_S
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Tue Dec 28 10:18:59 1999 */
  /* SIVE_NIVEL1_S R/47 SIVE_RES_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_NIVEL_1 <> :new.CD_NIVEL_1
  then
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_NIVEL_1 = :old.CD_NIVEL_1;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_NIVEL1_S" debido a que existe "SIVE_RES_URGENCIAS".'
      );
    end if;
  end if;

  /* ERwin Builtin Tue Dec 28 10:18:59 1999 */
  /* SIVE_NIVEL1_S R/28 SIVE_NIVEL2_S ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_NIVEL_1 <> :new.CD_NIVEL_1
  then
    select count(*) into numrows
      from SIVE_NIVEL2_S
      where
        /*  %JoinFKPK(SIVE_NIVEL2_S,:%Old," = "," and") */
        SIVE_NIVEL2_S.CD_NIVEL_1 = :old.CD_NIVEL_1;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_NIVEL1_S" debido a que existe "SIVE_NIVEL2_S".'
      );
    end if;
  end if;


-- ERwin Builtin Tue Dec 28 10:18:59 1999
end;
/


create trigger SIVE_NIVEL2_S_TRIG_A_D
  AFTER DELETE
  on SIVE_NIVEL2_S
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Tue Dec 28 10:18:59 1999 */
    /* SIVE_NIVEL2_S R/18 SIVE_ZONA_BASICA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ZONA_BASICA
      where
        /*  %JoinFKPK(SIVE_ZONA_BASICA,:%Old," = "," and") */
        SIVE_ZONA_BASICA.CD_NIVEL_1 = :old.CD_NIVEL_1 and
        SIVE_ZONA_BASICA.CD_NIVEL_2 = :old.CD_NIVEL_2;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_NIVEL2_S" debido a que existe en "SIVE_ZONA_BASICA".'
      );
    end if;


-- ERwin Builtin Tue Dec 28 10:18:59 1999
end;
/


create trigger SIVE_NIVEL2_S_TRIG_A_I
  AFTER INSERT
  on SIVE_NIVEL2_S
  
  for each row
declare numrows INTEGER;
begin

    /* ERwin Builtin Tue Dec 28 10:18:59 1999 */
    /* SIVE_NIVEL1_S R/28 SIVE_NIVEL2_S ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL1_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_NIVEL2_S" because a que no existe en "SIVE_NIVEL1_S".'
      );
    end if;


-- ERwin Builtin Tue Dec 28 10:18:59 1999
end;
/


create trigger SIVE_NIVEL2_S_TRIG_A_U
  AFTER UPDATE
  on SIVE_NIVEL2_S
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Tue Dec 28 10:18:59 1999 */
  /* SIVE_NIVEL2_S R/18 SIVE_ZONA_BASICA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_NIVEL_1 <> :new.CD_NIVEL_1 or 
    :old.CD_NIVEL_2 <> :new.CD_NIVEL_2
  then
    select count(*) into numrows
      from SIVE_ZONA_BASICA
      where
        /*  %JoinFKPK(SIVE_ZONA_BASICA,:%Old," = "," and") */
        SIVE_ZONA_BASICA.CD_NIVEL_1 = :old.CD_NIVEL_1 and
        SIVE_ZONA_BASICA.CD_NIVEL_2 = :old.CD_NIVEL_2;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_NIVEL2_S" debido a que existe "SIVE_ZONA_BASICA".'
      );
    end if;
  end if;

  /* ERwin Builtin Tue Dec 28 10:18:59 1999 */
  /* SIVE_NIVEL1_S R/28 SIVE_NIVEL2_S ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL1_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_NIVEL2_S" debido a que no existe en "SIVE_NIVEL1_S".'
    );
  end if;


-- ERwin Builtin Tue Dec 28 10:18:59 1999
end;
/


CREATE OR REPLACE TRIGGER SIVE_NOTIF_EDOI_TRIG_A_D_1 after DELETE on SIVE_NOTIF_EDOI for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_NOTIF_EDOI 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_NOTIF_EDOI R/321 SIVE_MOVRESPEDO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MOVRESPEDO
      where
        /*  %JoinFKPK(SIVE_MOVRESPEDO,:%Old," = "," and") */
        SIVE_MOVRESPEDO.NM_EDO = :old.NM_EDO and
        SIVE_MOVRESPEDO.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_MOVRESPEDO.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_MOVRESPEDO.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_MOVRESPEDO.FC_RECEP = :old.FC_RECEP and
        SIVE_MOVRESPEDO.FC_FECNOTIF = :old.FC_FECNOTIF and
        SIVE_MOVRESPEDO.CD_FUENTE = :old.CD_FUENTE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_NOTIF_EDOI" because "SIVE_MOVRESPEDO" exists.'*/
       'no se puede borrar de SIVE_NOTIF_EDOI
        porque hay registros en SIVE_MOVRESPEDO con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_NOTIF_EDOI  SIVE_MOVRESPEDO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MOVRESPEDO
      where
        /*  %JoinFKPK(SIVE_MOVRESPEDO,:%Old," = "," and") */
        SIVE_MOVRESPEDO.NM_EDO = :old.NM_EDO and
        SIVE_MOVRESPEDO.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_MOVRESPEDO.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_MOVRESPEDO.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_MOVRESPEDO.FC_RECEP = :old.FC_RECEP and
        SIVE_MOVRESPEDO.FC_FECNOTIF = :old.FC_FECNOTIF and
        SIVE_MOVRESPEDO.CD_FUENTE = :old.CD_FUENTE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_NOTIF_EDOI" because "SIVE_MOVRESPEDO" exists.'*/
       'no se puede borrar de SIVE_NOTIF_EDOI
        porque hay registros en SIVE_MOVRESPEDO con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_NOTIF_EDOI  SIVE_RESLAB ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESLAB
      where
        /*  %JoinFKPK(SIVE_RESLAB,:%Old," = "," and") */
        SIVE_RESLAB.NM_EDO = :old.NM_EDO and
        SIVE_RESLAB.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_RESLAB.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_RESLAB.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_RESLAB.FC_RECEP = :old.FC_RECEP and
        SIVE_RESLAB.FC_FECNOTIF = :old.FC_FECNOTIF and
        SIVE_RESLAB.CD_FUENTE = :old.CD_FUENTE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_NOTIF_EDOI" because "SIVE_RESLAB" exists.'*/
       'no se puede borrar de SIVE_NOTIF_EDOI
        porque hay registros en SIVE_RESLAB con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_NOTIF_EDOI  SIVE_TRATAMIENTOS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_TRATAMIENTOS
      where
        /*  %JoinFKPK(SIVE_TRATAMIENTOS,:%Old," = "," and") */
        SIVE_TRATAMIENTOS.NM_EDO = :old.NM_EDO and
        SIVE_TRATAMIENTOS.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_TRATAMIENTOS.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_TRATAMIENTOS.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_TRATAMIENTOS.FC_RECEP = :old.FC_RECEP and
        SIVE_TRATAMIENTOS.FC_FECNOTIF = :old.FC_FECNOTIF and
        SIVE_TRATAMIENTOS.CD_FUENTE = :old.CD_FUENTE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_NOTIF_EDOI" because "SIVE_TRATAMIENTOS" exists.'*/
       'no se puede borrar de SIVE_NOTIF_EDOI
        porque hay registros en SIVE_TRATAMIENTOS con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_NOTIF_EDOI_TRIG_A_I_1 after INSERT on SIVE_NOTIF_EDOI for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- INSERT trigger on SIVE_NOTIF_EDOI 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_EDOIND  SIVE_NOTIF_EDOI ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_EDOIND
      where
        /* %JoinFKPK(:%New,SIVE_EDOIND," = "," and") */
        :new.NM_EDO = SIVE_EDOIND.NM_EDO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_NOTIF_EDOI" because "SIVE_EDOIND" does not exist.'*/
          'no se puede grabar en SIVE_NOTIF_EDOI
           porque no existe la clave en SIVE_EDOIND'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_NOTIFEDO  SIVE_NOTIF_EDOI ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIFEDO
      where
        /* %JoinFKPK(:%New,SIVE_NOTIFEDO," = "," and") */
        :new.CD_E_NOTIF = SIVE_NOTIFEDO.CD_E_NOTIF and
        :new.CD_ANOEPI = SIVE_NOTIFEDO.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_NOTIFEDO.CD_SEMEPI and
        :new.FC_RECEP = SIVE_NOTIFEDO.FC_RECEP and
        :new.FC_FECNOTIF = SIVE_NOTIFEDO.FC_FECNOTIF;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_NOTIF_EDOI" because "SIVE_NOTIFEDO" does not exist.'*/
          'no se puede grabar en SIVE_NOTIF_EDOI
           porque no existe la clave en SIVE_NOTIFEDO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_FUENTES_NOTIF  SIVE_NOTIF_EDOI ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_FUENTES_NOTIF
      where
        /* %JoinFKPK(:%New,SIVE_FUENTES_NOTIF," = "," and") */
        :new.CD_FUENTE = SIVE_FUENTES_NOTIF.CD_FUENTE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_NOTIF_EDOI" because "SIVE_FUENTES_NOTIF" does not exist.'*/
          'no se puede grabar en SIVE_NOTIF_EDOI
           porque no existe la clave en SIVE_FUENTES_NOTIF'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_NOTIF_EDOI_TRIG_A_U_1 after UPDATE on SIVE_NOTIF_EDOI for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_NOTIF_EDOI 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NOTIF_EDOI R/321 SIVE_MOVRESPEDO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.NM_EDO <> :new.NM_EDO or 
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF or 
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI or 
    :old.FC_RECEP <> :new.FC_RECEP or 
    :old.FC_FECNOTIF <> :new.FC_FECNOTIF or 
    :old.CD_FUENTE <> :new.CD_FUENTE
  then
    select count(*) into numrows
      from SIVE_MOVRESPEDO
      where
        /*  %JoinFKPK(SIVE_MOVRESPEDO,:%Old," = "," and") */
        SIVE_MOVRESPEDO.NM_EDO = :old.NM_EDO and
        SIVE_MOVRESPEDO.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_MOVRESPEDO.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_MOVRESPEDO.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_MOVRESPEDO.FC_RECEP = :old.FC_RECEP and
        SIVE_MOVRESPEDO.FC_FECNOTIF = :old.FC_FECNOTIF and
        SIVE_MOVRESPEDO.CD_FUENTE = :old.CD_FUENTE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_NOTIF_EDOI" because "SIVE_MOVRESPEDO" exists.'*/
       'no se puede modificar SIVE_NOTIF_EDOI
        porque hay registros en SIVE_MOVRESPEDO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NOTIF_EDOI  SIVE_MOVRESPEDO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.NM_EDO <> :new.NM_EDO or 
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF or 
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI or 
    :old.FC_RECEP <> :new.FC_RECEP or 
    :old.FC_FECNOTIF <> :new.FC_FECNOTIF or 
    :old.CD_FUENTE <> :new.CD_FUENTE
  then
    select count(*) into numrows
      from SIVE_MOVRESPEDO
      where
        /*  %JoinFKPK(SIVE_MOVRESPEDO,:%Old," = "," and") */
        SIVE_MOVRESPEDO.NM_EDO = :old.NM_EDO and
        SIVE_MOVRESPEDO.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_MOVRESPEDO.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_MOVRESPEDO.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_MOVRESPEDO.FC_RECEP = :old.FC_RECEP and
        SIVE_MOVRESPEDO.FC_FECNOTIF = :old.FC_FECNOTIF and
        SIVE_MOVRESPEDO.CD_FUENTE = :old.CD_FUENTE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_NOTIF_EDOI" because "SIVE_MOVRESPEDO" exists.'*/
       'no se puede modificar SIVE_NOTIF_EDOI
        porque hay registros en SIVE_MOVRESPEDO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NOTIF_EDOI  SIVE_RESLAB ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.NM_EDO <> :new.NM_EDO or 
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF or 
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI or 
    :old.FC_RECEP <> :new.FC_RECEP or 
    :old.FC_FECNOTIF <> :new.FC_FECNOTIF or 
    :old.CD_FUENTE <> :new.CD_FUENTE
  then
    select count(*) into numrows
      from SIVE_RESLAB
      where
        /*  %JoinFKPK(SIVE_RESLAB,:%Old," = "," and") */
        SIVE_RESLAB.NM_EDO = :old.NM_EDO and
        SIVE_RESLAB.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_RESLAB.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_RESLAB.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_RESLAB.FC_RECEP = :old.FC_RECEP and
        SIVE_RESLAB.FC_FECNOTIF = :old.FC_FECNOTIF and
        SIVE_RESLAB.CD_FUENTE = :old.CD_FUENTE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_NOTIF_EDOI" because "SIVE_RESLAB" exists.'*/
       'no se puede modificar SIVE_NOTIF_EDOI
        porque hay registros en SIVE_RESLAB con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NOTIF_EDOI  SIVE_TRATAMIENTOS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.NM_EDO <> :new.NM_EDO or 
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF or 
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI or 
    :old.FC_RECEP <> :new.FC_RECEP or 
    :old.FC_FECNOTIF <> :new.FC_FECNOTIF or 
    :old.CD_FUENTE <> :new.CD_FUENTE
  then
    select count(*) into numrows
      from SIVE_TRATAMIENTOS
      where
        /*  %JoinFKPK(SIVE_TRATAMIENTOS,:%Old," = "," and") */
        SIVE_TRATAMIENTOS.NM_EDO = :old.NM_EDO and
        SIVE_TRATAMIENTOS.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_TRATAMIENTOS.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_TRATAMIENTOS.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_TRATAMIENTOS.FC_RECEP = :old.FC_RECEP and
        SIVE_TRATAMIENTOS.FC_FECNOTIF = :old.FC_FECNOTIF and
        SIVE_TRATAMIENTOS.CD_FUENTE = :old.CD_FUENTE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_NOTIF_EDOI" because "SIVE_TRATAMIENTOS" exists.'*/
       'no se puede modificar SIVE_NOTIF_EDOI
        porque hay registros en SIVE_TRATAMIENTOS con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_EDOIND  SIVE_NOTIF_EDOI ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_EDOIND
    where
      /* %JoinFKPK(:%New,SIVE_EDOIND," = "," and") */
      :new.NM_EDO = SIVE_EDOIND.NM_EDO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_NOTIF_EDOI" because "SIVE_EDOIND" does not exist.'*/
         'no se puede actualizar SIVE_NOTIF_EDOI
          porque no existe la clave en SIVE_EDOIND'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NOTIFEDO  SIVE_NOTIF_EDOI ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NOTIFEDO
    where
      /* %JoinFKPK(:%New,SIVE_NOTIFEDO," = "," and") */
      :new.CD_E_NOTIF = SIVE_NOTIFEDO.CD_E_NOTIF and
      :new.CD_ANOEPI = SIVE_NOTIFEDO.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_NOTIFEDO.CD_SEMEPI and
      :new.FC_RECEP = SIVE_NOTIFEDO.FC_RECEP and
      :new.FC_FECNOTIF = SIVE_NOTIFEDO.FC_FECNOTIF;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_NOTIF_EDOI" because "SIVE_NOTIFEDO" does not exist.'*/
         'no se puede actualizar SIVE_NOTIF_EDOI
          porque no existe la clave en SIVE_NOTIFEDO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_FUENTES_NOTIF  SIVE_NOTIF_EDOI ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_FUENTES_NOTIF
    where
      /* %JoinFKPK(:%New,SIVE_FUENTES_NOTIF," = "," and") */
      :new.CD_FUENTE = SIVE_FUENTES_NOTIF.CD_FUENTE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_NOTIF_EDOI" because "SIVE_FUENTES_NOTIF" does not exist.'*/
         'no se puede actualizar SIVE_NOTIF_EDOI
          porque no existe la clave en SIVE_FUENTES_NOTIF'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_NOTIF_RMC_TRIG_A_D_1 after DELETE on SIVE_NOTIF_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_NOTIF_RMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_NOTIF_RMC  SIVE_CASOS_CENT ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_CASOS_CENT
      where
        /*  %JoinFKPK(SIVE_CASOS_CENT,:%Old," = "," and") */
        SIVE_CASOS_CENT.CD_ENFCIE = :old.CD_ENFCIE and
        SIVE_CASOS_CENT.CD_PCENTI = :old.CD_PCENTI and
        SIVE_CASOS_CENT.CD_MEDCEN = :old.CD_MEDCEN and
        SIVE_CASOS_CENT.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_CASOS_CENT.CD_SEMEPI = :old.CD_SEMEPI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_NOTIF_RMC" because "SIVE_CASOS_CENT" exists.'*/
       'no se puede borrar de SIVE_NOTIF_RMC
        porque hay registros en SIVE_CASOS_CENT con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_NOTIF_RMC_TRIG_A_I_1 after INSERT on SIVE_NOTIF_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- INSERT trigger on SIVE_NOTIF_RMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_SEMANA_EPI_RMC  SIVE_NOTIF_RMC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEMANA_EPI_RMC
      where
        /* %JoinFKPK(:%New,SIVE_SEMANA_EPI_RMC," = "," and") */
        :new.CD_ANOEPI = SIVE_SEMANA_EPI_RMC.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_SEMANA_EPI_RMC.CD_SEMEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_NOTIF_RMC" because "SIVE_SEMANA_EPI_RMC" does not exist.'*/
          'no se puede grabar en SIVE_NOTIF_RMC
           porque no existe la clave en SIVE_SEMANA_EPI_RMC'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_MCENTINELA  SIVE_NOTIF_RMC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MCENTINELA
      where
        /* %JoinFKPK(:%New,SIVE_MCENTINELA," = "," and") */
        :new.CD_PCENTI = SIVE_MCENTINELA.CD_PCENTI and
        :new.CD_MEDCEN = SIVE_MCENTINELA.CD_MEDCEN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_NOTIF_RMC" because "SIVE_MCENTINELA" does not exist.'*/
          'no se puede grabar en SIVE_NOTIF_RMC
           porque no existe la clave en SIVE_MCENTINELA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_ENF_CENTI  SIVE_NOTIF_RMC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ENF_CENTI
      where
        /* %JoinFKPK(:%New,SIVE_ENF_CENTI," = "," and") */
        :new.CD_ENFCIE = SIVE_ENF_CENTI.CD_ENFCIE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_NOTIF_RMC" because "SIVE_ENF_CENTI" does not exist.'*/
          'no se puede grabar en SIVE_NOTIF_RMC
           porque no existe la clave en SIVE_ENF_CENTI'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_NOTIF_RMC_TRIG_A_U_1 after UPDATE on SIVE_NOTIF_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_NOTIF_RMC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NOTIF_RMC  SIVE_CASOS_CENT ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE or 
    :old.CD_PCENTI <> :new.CD_PCENTI or 
    :old.CD_MEDCEN <> :new.CD_MEDCEN or 
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI
  then
    select count(*) into numrows
      from SIVE_CASOS_CENT
      where
        /*  %JoinFKPK(SIVE_CASOS_CENT,:%Old," = "," and") */
        SIVE_CASOS_CENT.CD_ENFCIE = :old.CD_ENFCIE and
        SIVE_CASOS_CENT.CD_PCENTI = :old.CD_PCENTI and
        SIVE_CASOS_CENT.CD_MEDCEN = :old.CD_MEDCEN and
        SIVE_CASOS_CENT.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_CASOS_CENT.CD_SEMEPI = :old.CD_SEMEPI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_NOTIF_RMC" because "SIVE_CASOS_CENT" exists.'*/
       'no se puede modificar SIVE_NOTIF_RMC
        porque hay registros en SIVE_CASOS_CENT con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_SEMANA_EPI_RMC  SIVE_NOTIF_RMC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEMANA_EPI_RMC
    where
      /* %JoinFKPK(:%New,SIVE_SEMANA_EPI_RMC," = "," and") */
      :new.CD_ANOEPI = SIVE_SEMANA_EPI_RMC.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_SEMANA_EPI_RMC.CD_SEMEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_NOTIF_RMC" because "SIVE_SEMANA_EPI_RMC" does not exist.'*/
         'no se puede actualizar SIVE_NOTIF_RMC
          porque no existe la clave en SIVE_SEMANA_EPI_RMC'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_MCENTINELA  SIVE_NOTIF_RMC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MCENTINELA
    where
      /* %JoinFKPK(:%New,SIVE_MCENTINELA," = "," and") */
      :new.CD_PCENTI = SIVE_MCENTINELA.CD_PCENTI and
      :new.CD_MEDCEN = SIVE_MCENTINELA.CD_MEDCEN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_NOTIF_RMC" because "SIVE_MCENTINELA" does not exist.'*/
         'no se puede actualizar SIVE_NOTIF_RMC
          porque no existe la clave en SIVE_MCENTINELA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_ENF_CENTI  SIVE_NOTIF_RMC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ENF_CENTI
    where
      /* %JoinFKPK(:%New,SIVE_ENF_CENTI," = "," and") */
      :new.CD_ENFCIE = SIVE_ENF_CENTI.CD_ENFCIE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_NOTIF_RMC" because "SIVE_ENF_CENTI" does not exist.'*/
         'no se puede actualizar SIVE_NOTIF_RMC
          porque no existe la clave en SIVE_ENF_CENTI'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_NOTIF_SEM_TRIG_A_D_1 after DELETE on SIVE_NOTIF_SEM for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_NOTIF_SEM 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_NOTIF_SEM  SIVE_NOTIFEDO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIFEDO
      where
        /*  %JoinFKPK(SIVE_NOTIFEDO,:%Old," = "," and") */
        SIVE_NOTIFEDO.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_NOTIFEDO.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_NOTIFEDO.CD_SEMEPI = :old.CD_SEMEPI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_NOTIF_SEM" because "SIVE_NOTIFEDO" exists.'*/
       'no se puede borrar de SIVE_NOTIF_SEM
        porque hay registros en SIVE_NOTIFEDO con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_NOTIF_SEM_TRIG_A_I_1 after INSERT on SIVE_NOTIF_SEM for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- INSERT trigger on SIVE_NOTIF_SEM 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_E_NOTIF  SIVE_NOTIF_SEM ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_E_NOTIF
      where
        /* %JoinFKPK(:%New,SIVE_E_NOTIF," = "," and") */
        :new.CD_E_NOTIF = SIVE_E_NOTIF.CD_E_NOTIF;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_NOTIF_SEM" because "SIVE_E_NOTIF" does not exist.'*/
          'no se puede grabar en SIVE_NOTIF_SEM
           porque no existe la clave en SIVE_E_NOTIF'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_SEMANA_EPI  SIVE_NOTIF_SEM ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEMANA_EPI
      where
        /* %JoinFKPK(:%New,SIVE_SEMANA_EPI," = "," and") */
        :new.CD_ANOEPI = SIVE_SEMANA_EPI.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_SEMANA_EPI.CD_SEMEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_NOTIF_SEM" because "SIVE_SEMANA_EPI" does not exist.'*/
          'no se puede grabar en SIVE_NOTIF_SEM
           porque no existe la clave en SIVE_SEMANA_EPI'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_NOTIF_SEM_TRIG_A_U_1 after UPDATE on SIVE_NOTIF_SEM for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_NOTIF_SEM 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NOTIF_SEM  SIVE_NOTIFEDO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF or 
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI
  then
    select count(*) into numrows
      from SIVE_NOTIFEDO
      where
        /*  %JoinFKPK(SIVE_NOTIFEDO,:%Old," = "," and") */
        SIVE_NOTIFEDO.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_NOTIFEDO.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_NOTIFEDO.CD_SEMEPI = :old.CD_SEMEPI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_NOTIF_SEM" because "SIVE_NOTIFEDO" exists.'*/
       'no se puede modificar SIVE_NOTIF_SEM
        porque hay registros en SIVE_NOTIFEDO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_E_NOTIF  SIVE_NOTIF_SEM ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_E_NOTIF
    where
      /* %JoinFKPK(:%New,SIVE_E_NOTIF," = "," and") */
      :new.CD_E_NOTIF = SIVE_E_NOTIF.CD_E_NOTIF;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_NOTIF_SEM" because "SIVE_E_NOTIF" does not exist.'*/
         'no se puede actualizar SIVE_NOTIF_SEM
          porque no existe la clave en SIVE_E_NOTIF'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_SEMANA_EPI  SIVE_NOTIF_SEM ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEMANA_EPI
    where
      /* %JoinFKPK(:%New,SIVE_SEMANA_EPI," = "," and") */
      :new.CD_ANOEPI = SIVE_SEMANA_EPI.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_SEMANA_EPI.CD_SEMEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_NOTIF_SEM" because "SIVE_SEMANA_EPI" does not exist.'*/
         'no se puede actualizar SIVE_NOTIF_SEM
          porque no existe la clave en SIVE_SEMANA_EPI'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_NOTIFEDO_TRIG_A_D_1 after DELETE on SIVE_NOTIFEDO for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_NOTIFEDO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_NOTIFEDO R/270 SIVE_EDONUM ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_EDONUM
      where
        /*  %JoinFKPK(SIVE_EDONUM,:%Old," = "," and") */
        SIVE_EDONUM.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_EDONUM.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_EDONUM.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_EDONUM.FC_RECEP = :old.FC_RECEP and
        SIVE_EDONUM.FC_FECNOTIF = :old.FC_FECNOTIF;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_NOTIFEDO" because "SIVE_EDONUM" exists.'*/
       'no se puede borrar de SIVE_NOTIFEDO
        porque hay registros en SIVE_EDONUM con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_NOTIFEDO  SIVE_NOTIF_EDOI ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIF_EDOI
      where
        /*  %JoinFKPK(SIVE_NOTIF_EDOI,:%Old," = "," and") */
        SIVE_NOTIF_EDOI.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_NOTIF_EDOI.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_NOTIF_EDOI.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_NOTIF_EDOI.FC_RECEP = :old.FC_RECEP and
        SIVE_NOTIF_EDOI.FC_FECNOTIF = :old.FC_FECNOTIF;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_NOTIFEDO" because "SIVE_NOTIF_EDOI" exists.'*/
       'no se puede borrar de SIVE_NOTIFEDO
        porque hay registros en SIVE_NOTIF_EDOI con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_NOTIFEDO_TRIG_A_I_1 after INSERT on SIVE_NOTIFEDO for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- INSERT trigger on SIVE_NOTIFEDO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_NOTIF_SEM  SIVE_NOTIFEDO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIF_SEM
      where
        /* %JoinFKPK(:%New,SIVE_NOTIF_SEM," = "," and") */
        :new.CD_E_NOTIF = SIVE_NOTIF_SEM.CD_E_NOTIF and
        :new.CD_ANOEPI = SIVE_NOTIF_SEM.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_NOTIF_SEM.CD_SEMEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_NOTIFEDO" because "SIVE_NOTIF_SEM" does not exist.'*/
          'no se puede grabar en SIVE_NOTIFEDO
           porque no existe la clave en SIVE_NOTIF_SEM'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_NOTIFEDO_TRIG_A_U_1 after UPDATE on SIVE_NOTIFEDO for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_NOTIFEDO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NOTIFEDO R/270 SIVE_EDONUM ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF or 
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI or 
    :old.FC_RECEP <> :new.FC_RECEP or 
    :old.FC_FECNOTIF <> :new.FC_FECNOTIF
  then
    select count(*) into numrows
      from SIVE_EDONUM
      where
        /*  %JoinFKPK(SIVE_EDONUM,:%Old," = "," and") */
        SIVE_EDONUM.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_EDONUM.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_EDONUM.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_EDONUM.FC_RECEP = :old.FC_RECEP and
        SIVE_EDONUM.FC_FECNOTIF = :old.FC_FECNOTIF;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_NOTIFEDO" because "SIVE_EDONUM" exists.'*/
       'no se puede modificar SIVE_NOTIFEDO
        porque hay registros en SIVE_EDONUM con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NOTIFEDO  SIVE_NOTIF_EDOI ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_E_NOTIF <> :new.CD_E_NOTIF or 
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI or 
    :old.FC_RECEP <> :new.FC_RECEP or 
    :old.FC_FECNOTIF <> :new.FC_FECNOTIF
  then
    select count(*) into numrows
      from SIVE_NOTIF_EDOI
      where
        /*  %JoinFKPK(SIVE_NOTIF_EDOI,:%Old," = "," and") */
        SIVE_NOTIF_EDOI.CD_E_NOTIF = :old.CD_E_NOTIF and
        SIVE_NOTIF_EDOI.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_NOTIF_EDOI.CD_SEMEPI = :old.CD_SEMEPI and
        SIVE_NOTIF_EDOI.FC_RECEP = :old.FC_RECEP and
        SIVE_NOTIF_EDOI.FC_FECNOTIF = :old.FC_FECNOTIF;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_NOTIFEDO" because "SIVE_NOTIF_EDOI" exists.'*/
       'no se puede modificar SIVE_NOTIFEDO
        porque hay registros en SIVE_NOTIF_EDOI con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_NOTIF_SEM  SIVE_NOTIFEDO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NOTIF_SEM
    where
      /* %JoinFKPK(:%New,SIVE_NOTIF_SEM," = "," and") */
      :new.CD_E_NOTIF = SIVE_NOTIF_SEM.CD_E_NOTIF and
      :new.CD_ANOEPI = SIVE_NOTIF_SEM.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_NOTIF_SEM.CD_SEMEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_NOTIFEDO" because "SIVE_NOTIF_SEM" does not exist.'*/
         'no se puede actualizar SIVE_NOTIFEDO
          porque no existe la clave en SIVE_NOTIF_SEM'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ORIGEN_MUESTRA_TRIG_A_D_1 after DELETE on SIVE_ORIGEN_MUESTRA for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_ORIGEN_MUESTRA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_ORIGEN_MUESTRA  SIVE_MUESTRAS_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MUESTRAS_BROTE
      where
        /*  %JoinFKPK(SIVE_MUESTRAS_BROTE,:%Old," = "," and") */
        SIVE_MUESTRAS_BROTE.CD_MUESTRA = :old.CD_MUESTRA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_ORIGEN_MUESTRA" because "SIVE_MUESTRAS_BROTE" exists.'*/
       'no se puede borrar de SIVE_ORIGEN_MUESTRA
        porque hay registros en SIVE_MUESTRAS_BROTE con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_ORIGEN_MUESTRA_TRIG_A_U_1 after UPDATE on SIVE_ORIGEN_MUESTRA for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_ORIGEN_MUESTRA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_ORIGEN_MUESTRA  SIVE_MUESTRAS_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_MUESTRA <> :new.CD_MUESTRA
  then
    select count(*) into numrows
      from SIVE_MUESTRAS_BROTE
      where
        /*  %JoinFKPK(SIVE_MUESTRAS_BROTE,:%Old," = "," and") */
        SIVE_MUESTRAS_BROTE.CD_MUESTRA = :old.CD_MUESTRA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_ORIGEN_MUESTRA" because "SIVE_MUESTRAS_BROTE" exists.'*/
       'no se puede modificar SIVE_ORIGEN_MUESTRA
        porque hay registros en SIVE_MUESTRAS_BROTE con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

create or replace trigger SIVE_PARAM_RES_EDO_TRIG_A_U after UPDATE on SIVE_PARAM_RES_EDO for each row
-- ERwin Builtin Thu Jan 18 19:07:01 2001
-- UPDATE trigger on SIVE_PARAM_RES_EDO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Thu Jan 18 19:07:01 2001 */
  /* SIVE_SEMANA_EPI R/251 SIVE_PARAM_RES_EDO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEMANA_EPI
    where
      /* JoinFKPK" = "," and") */
      :new.CD_ANOEPI_FIN = SIVE_SEMANA_EPI.CD_ANOEPI and
      :new.CD_SEMEPI_FIN = SIVE_SEMANA_EPI.CD_SEMEPI;
  if (
    /* NotnullFK */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'Cannot UPDATE "SIVE_PARAM_RES_EDO" because "SIVE_SEMANA_EPI" does not exist.'
    );
  end if;

  /* ERwin Builtin Thu Jan 18 19:07:01 2001 */
  /* SIVE_SEMANA_EPI R/250 SIVE_PARAM_RES_EDO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEMANA_EPI
    where
      /* JoinFKPK" = "," and") */
      :new.CD_ANOEPI_INI = SIVE_SEMANA_EPI.CD_ANOEPI and
      :new.CD_SEMEPI_INI = SIVE_SEMANA_EPI.CD_SEMEPI;
  if (
    /* NotnullFK */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'Cannot UPDATE "SIVE_PARAM_RES_EDO" because "SIVE_SEMANA_EPI" does not exist.'
    );
  end if;


-- ERwin Builtin Thu Jan 18 19:07:01 2001
end;
/







create or replace trigger SIVE_PARAM_RES_EDO_TRIG_B_I
  BEFORE INSERT
  on SIVE_PARAM_RES_EDO
  
  for each row
/* ERwin Builtin Wed Oct 31 16:28:50 2001 */
/* default body for SIVE_PARAM_RES_EDO_TRIG_B_I */
declare  
   numrows INTEGER;
   Error   VARCHAR2(255);

begin
/* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_SEMANA_EPI  SIVE_PARAM_RES_EDO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEMANA_EPI
      where
        /* %JoinFKPK(:%New,SIVE_SEMANA_EPI," = "," and") */
        :new.CD_ANOEPI_FIN = SIVE_SEMANA_EPI.CD_ANOEPI and
        :new.CD_SEMEPI_FIN = SIVE_SEMANA_EPI.CD_SEMEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_PARAM_RES_EDO" because "SIVE_SEMANA_EPI" does not exist.'*/
          'no se puede grabar en SIVE_PARAM_RES_EDO
           porque no existe la clave en SIVE_SEMANA_EPI'
      );
    end if;

/* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_SEMANA_EPI  SIVE_PARAM_RES_EDO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEMANA_EPI
      where
        /* %JoinFKPK(:%New,SIVE_SEMANA_EPI," = "," and") */
        :new.CD_ANOEPI_INI = SIVE_SEMANA_EPI.CD_ANOEPI and
        :new.CD_SEMEPI_INI = SIVE_SEMANA_EPI.CD_SEMEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_PARAM_RES_EDO" because "SIVE_SEMANA_EPI" does not exist.'*/
          'no se puede grabar en SIVE_PARAM_RES_EDO
           porque no existe la clave en SIVE_SEMANA_EPI'
      );
    end if;






/* Secuenciador */
select SIVE_s_PARAM_RESEDO.NextVal
into :new.NM_PETICION
from Dual;

/* Fecha de inserci�n del registro  */
select TO_DATE(TO_CHAR(sysdate,'DD/MM/YYYY'),'DD/MM/YYYY')
into :new.FC_INICIO
from Dual;

/* Iniciamos el proceso de resumen EDOs */
:new.IT_ESTADO := 'P';


/* Borramos la informaci�n del resumen EDOs. (La m�s r�pida posible) */
delete 
from   SIVE_RESUMEN_EDOS
where  TO_NUMBER(:new.CD_ANOEPI_INI || :new.CD_SEMEPI_INI) <=TO_NUMBER(CD_ANOEPI || CD_SEMEPI)  and 
       TO_NUMBER(:new.CD_ANOEPI_FIN || :new.CD_SEMEPI_FIN) >= TO_NUMBER(CD_ANOEPI || CD_SEMEPI);


/* Inserta en SIVE_RESUMEN_EDOS las enfermedades cuya vigilancia es:
   N: N�merica.
   X: Num�rica con datos adicionales. 
   A: Num�rica e individualizada.
   Se dejan los tipos A:num�rica e individualizada e I:individualizada para insertar datos desde las 
   enfermedades individualizadas.
   Se tienen en cuenta las condiciones de tiempo
 */
insert into SIVE_RESUMEN_EDOS (CD_ENFCIE,CD_ANOEPI,CD_SEMEPI,CD_NIVEL_1,CD_NIVEL_2,CD_ZBS,CD_PROV,CD_MUN,NM_CASOS)
select en.CD_ENFCIE, en.CD_ANOEPI, en.CD_SEMEPI, 
       eno.CD_NIVEL_1, eno.CD_NIVEL_2, eno.CD_ZBS,
       cn.CD_PROV, cn.CD_MUN, sum(en.NM_CASOS) as NM_CASOS
from   SIVE_EDONUM en, 
       SIVE_E_NOTIF eno, 
       SIVE_C_NOTIF cn,
       SIVE_ENFEREDO ee
where  en.CD_E_NOTIF  = eno.CD_E_NOTIF and
       eno.CD_CENTRO  = cn.CD_CENTRO and 
       en.CD_ENFCIE   = ee.CD_ENFCIE and
     (
      (ee.CD_TVIGI    = 'N') OR
      (ee.CD_TVIGI    = 'X') OR 
      (ee.CD_TVIGI    = 'A') 
     ) and 
     TO_NUMBER(:new.CD_ANOEPI_INI || :new.CD_SEMEPI_INI) <=TO_NUMBER(en.CD_ANOEPI || en.CD_SEMEPI)  and 
     TO_NUMBER(:new.CD_ANOEPI_FIN || :new.CD_SEMEPI_FIN) >= TO_NUMBER(en.CD_ANOEPI || en.CD_SEMEPI)
group by en.CD_ENFCIE, en.CD_ANOEPI, en.CD_SEMEPI, eno.CD_NIVEL_1, eno.CD_NIVEL_2, eno.CD_ZBS, cn.CD_PROV, cn.CD_MUN;

/* Realizamos la cuenta de casos de notificaciones individuales.
   Tiene en cuenta que la vigilancia de las enfermedades sea I(individualizada),
   justo las dos que no se cuentan en as num�ricas.
*/
insert into SIVE_RESUMEN_EDOS (CD_ENFCIE,CD_ANOEPI,CD_SEMEPI,CD_NIVEL_1,CD_NIVEL_2,CD_ZBS,CD_PROV,CD_MUN,NM_CASOS)
select ein.CD_ENFCIE, ein.CD_ANOEPI, ein.CD_SEMEPI, 
       ein.CD_NIVEL_1, ein.CD_NIVEL_2, ein.CD_ZBS,
       ein.CD_PROV, ein.CD_MUN, count(*) as NM_CASOS
from   SIVE_EDOIND ein, 
       SIVE_ENFEREDO ee
where  ein.CD_ENFCIE  = ee.CD_ENFCIE and
       ee.CD_TVIGI    = 'I'          and 
       ein.NM_EDO in (select NM_EDO from SIVE_NOTIF_EDOI where CD_FUENTE = 'E' and IT_PRIMERO = 'S') and  
       TO_NUMBER(:new.CD_ANOEPI_INI || :new.CD_SEMEPI_INI) <= TO_NUMBER(ein.CD_ANOEPI || ein.CD_SEMEPI)  and 
       TO_NUMBER(:new.CD_ANOEPI_FIN || :new.CD_SEMEPI_FIN) >= TO_NUMBER(ein.CD_ANOEPI || ein.CD_SEMEPI)
group by ein.CD_ENFCIE, ein.CD_ANOEPI, ein.CD_SEMEPI, ein.CD_NIVEL_1, ein.CD_NIVEL_2, ein.CD_ZBS, ein.CD_PROV, ein.CD_MUN;


/* Comprobamos si se ha terminado bien el proceso */
:new.IT_ESTADO := 'T';


/* Fecha de finalizaci�n del proceso  */
select TO_DATE(TO_CHAR(sysdate,'DD/MM/YYYY'),'DD/MM/YYYY')
into :new.FC_FIN
from Dual;


EXCEPTION
 WHEN OTHERS THEN
         :new.IT_ESTADO := 'E';
         
         /* Fecha de finalizaci�n del proceso  */
         select TO_DATE(TO_CHAR(sysdate,'DD/MM/YYYY'),'DD/MM/YYYY')  
         into :new.FC_FIN
         from Dual;

         Error := SUBSTR(SQLERRM,1,255);         
         insert into SIVE_PARAM_RESEDO_ERRORES values (:new.NM_PETICION,0,Error);
         
end;
/





































































CREATE OR REPLACE TRIGGER SIVE_PARAM_RES_EDO_TRIG_A_D_1 after DELETE on SIVE_PARAM_RES_EDO for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_PARAM_RES_EDO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_PARAM_RES_EDO  SIVE_PARAM_RESEDO_ERRORES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_PARAM_RESEDO_ERRORES
      where
        /*  %JoinFKPK(SIVE_PARAM_RESEDO_ERRORES,:%Old," = "," and") */
        SIVE_PARAM_RESEDO_ERRORES.NM_PETICION = :old.NM_PETICION;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_PARAM_RES_EDO" because "SIVE_PARAM_RESEDO_ERRORES" exists.'*/
       'no se puede borrar de SIVE_PARAM_RES_EDO
        porque hay registros en SIVE_PARAM_RESEDO_ERRORES con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PARAM_RESE_ERR_TRIG_A_I_1 after INSERT on SIVE_PARAM_RESEDO_ERRORES for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- INSERT trigger on SIVE_PARAM_RESEDO_ERRORES 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_PARAM_RES_EDO  SIVE_PARAM_RESEDO_ERRORES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PARAM_RES_EDO
      where
        /* %JoinFKPK(:%New,SIVE_PARAM_RES_EDO," = "," and") */
        :new.NM_PETICION = SIVE_PARAM_RES_EDO.NM_PETICION;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_PARAM_RESEDO_ERRORES" because "SIVE_PARAM_RES_EDO" does not exist.'*/
          'no se puede grabar en SIVE_PARAM_RESEDO_ERRORES
           porque no existe la clave en SIVE_PARAM_RES_EDO'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PARAM_RESE_ERR_TRIG_A_U_1 after UPDATE on SIVE_PARAM_RESEDO_ERRORES for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_PARAM_RESEDO_ERRORES 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_PARAM_RES_EDO  SIVE_PARAM_RESEDO_ERRORES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PARAM_RES_EDO
    where
      /* %JoinFKPK(:%New,SIVE_PARAM_RES_EDO," = "," and") */
      :new.NM_PETICION = SIVE_PARAM_RES_EDO.NM_PETICION;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_PARAM_RESEDO_ERRORES" because "SIVE_PARAM_RES_EDO" does not exist.'*/
         'no se puede actualizar SIVE_PARAM_RESEDO_ERRORES
          porque no existe la clave en SIVE_PARAM_RES_EDO'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PCENTINELA_TRIG_A_D_1 after DELETE on SIVE_PCENTINELA for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_PCENTINELA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_PCENTINELA  SIVE_MCENTINELA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MCENTINELA
      where
        /*  %JoinFKPK(SIVE_MCENTINELA,:%Old," = "," and") */
        SIVE_MCENTINELA.CD_PCENTI = :old.CD_PCENTI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_PCENTINELA" because "SIVE_MCENTINELA" exists.'*/
       'no se puede borrar de SIVE_PCENTINELA
        porque hay registros en SIVE_MCENTINELA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PCENTINELA_TRIG_A_I_1 after INSERT on SIVE_PCENTINELA for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- INSERT trigger on SIVE_PCENTINELA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_TPCENTINELA  SIVE_PCENTINELA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_TPCENTINELA
      where
        /* %JoinFKPK(:%New,SIVE_TPCENTINELA," = "," and") */
        :new.CD_TPCENTI = SIVE_TPCENTINELA.CD_TPCENTI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_PCENTINELA" because "SIVE_TPCENTINELA" does not exist.'*/
          'no se puede grabar en SIVE_PCENTINELA
           porque no existe la clave en SIVE_TPCENTINELA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_CONGLOM  SIVE_PCENTINELA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_CONGLOM
      where
        /* %JoinFKPK(:%New,SIVE_CONGLOM," = "," and") */
        :new.CD_CONGLO = SIVE_CONGLOM.CD_CONGLO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_PCENTINELA" because "SIVE_CONGLOM" does not exist.'*/
          'no se puede grabar en SIVE_PCENTINELA
           porque no existe la clave en SIVE_CONGLOM'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PCENTINELA_TRIG_A_U_1 after UPDATE on SIVE_PCENTINELA for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_PCENTINELA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_PCENTINELA  SIVE_MCENTINELA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PCENTI <> :new.CD_PCENTI
  then
    select count(*) into numrows
      from SIVE_MCENTINELA
      where
        /*  %JoinFKPK(SIVE_MCENTINELA,:%Old," = "," and") */
        SIVE_MCENTINELA.CD_PCENTI = :old.CD_PCENTI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_PCENTINELA" because "SIVE_MCENTINELA" exists.'*/
       'no se puede modificar SIVE_PCENTINELA
        porque hay registros en SIVE_MCENTINELA con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_TPCENTINELA  SIVE_PCENTINELA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_TPCENTINELA
    where
      /* %JoinFKPK(:%New,SIVE_TPCENTINELA," = "," and") */
      :new.CD_TPCENTI = SIVE_TPCENTINELA.CD_TPCENTI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_PCENTINELA" because "SIVE_TPCENTINELA" does not exist.'*/
         'no se puede actualizar SIVE_PCENTINELA
          porque no existe la clave en SIVE_TPCENTINELA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_CONGLOM  SIVE_PCENTINELA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_CONGLOM
    where
      /* %JoinFKPK(:%New,SIVE_CONGLOM," = "," and") */
      :new.CD_CONGLO = SIVE_CONGLOM.CD_CONGLO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_PCENTINELA" because "SIVE_CONGLOM" does not exist.'*/
         'no se puede actualizar SIVE_PCENTINELA
          porque no existe la clave en SIVE_CONGLOM'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PLANTILLA_RMC_TRIG_A_D_1 after DELETE on SIVE_PLANTILLA_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- DELETE trigger on SIVE_PLANTILLA_RMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_PLANTILLA_RMC R/251 SIVE_AVISOS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_AVISOS
      where
        /*  %JoinFKPK(SIVE_AVISOS,:%Old," = "," and") */
        SIVE_AVISOS.CD_PLANTILLA = :old.CD_PLANTILLA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_PLANTILLA_RMC" because "SIVE_AVISOS" exists.'*/
       'no se puede borrar de E/330
        porque hay registros en E/332 con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PLANTILLA_RMC_TRIG_A_U_1 after UPDATE on SIVE_PLANTILLA_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_PLANTILLA_RMC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_PLANTILLA_RMC R/251 SIVE_AVISOS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PLANTILLA <> :new.CD_PLANTILLA
  then
    select count(*) into numrows
      from SIVE_AVISOS
      where
        /*  %JoinFKPK(SIVE_AVISOS,:%Old," = "," and") */
        SIVE_AVISOS.CD_PLANTILLA = :old.CD_PLANTILLA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_PLANTILLA_RMC" because "SIVE_AVISOS" exists.'*/
       'no se puede modificar E/330
        porque hay registros en E/332 con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_POBLAC_TS_TRIG_A_I_1 after INSERT on SIVE_POBLAC_TS for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- INSERT trigger on SIVE_POBLAC_TS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_ANO_EPI_RMC  SIVE_POBLAC_TS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ANO_EPI_RMC
      where
        /* %JoinFKPK(:%New,SIVE_ANO_EPI_RMC," = "," and") */
        :new.CD_ANOEPI = SIVE_ANO_EPI_RMC.CD_ANOEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_POBLAC_TS" because "SIVE_ANO_EPI_RMC" does not exist.'*/
          'no se puede grabar en SIVE_POBLAC_TS
           porque no existe la clave en SIVE_ANO_EPI_RMC'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_SEXO  SIVE_POBLAC_TS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEXO
      where
        /* %JoinFKPK(:%New,SIVE_SEXO," = "," and") */
        :new.CD_SEXO = SIVE_SEXO.CD_SEXO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_POBLAC_TS" because "SIVE_SEXO" does not exist.'*/
          'no se puede grabar en SIVE_POBLAC_TS
           porque no existe la clave en SIVE_SEXO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_MCENTINELA  SIVE_POBLAC_TS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MCENTINELA
      where
        /* %JoinFKPK(:%New,SIVE_MCENTINELA," = "," and") */
        :new.CD_PCENTI = SIVE_MCENTINELA.CD_PCENTI and
        :new.CD_MEDCEN = SIVE_MCENTINELA.CD_MEDCEN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_POBLAC_TS" because "SIVE_MCENTINELA" does not exist.'*/
          'no se puede grabar en SIVE_POBLAC_TS
           porque no existe la clave en SIVE_MCENTINELA'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_POBLAC_TS_TRIG_A_U_1 after UPDATE on SIVE_POBLAC_TS for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- UPDATE trigger on SIVE_POBLAC_TS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_ANO_EPI_RMC  SIVE_POBLAC_TS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ANO_EPI_RMC
    where
      /* %JoinFKPK(:%New,SIVE_ANO_EPI_RMC," = "," and") */
      :new.CD_ANOEPI = SIVE_ANO_EPI_RMC.CD_ANOEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_POBLAC_TS" because "SIVE_ANO_EPI_RMC" does not exist.'*/
         'no se puede actualizar SIVE_POBLAC_TS
          porque no existe la clave en SIVE_ANO_EPI_RMC'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_SEXO  SIVE_POBLAC_TS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEXO
    where
      /* %JoinFKPK(:%New,SIVE_SEXO," = "," and") */
      :new.CD_SEXO = SIVE_SEXO.CD_SEXO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_POBLAC_TS" because "SIVE_SEXO" does not exist.'*/
         'no se puede actualizar SIVE_POBLAC_TS
          porque no existe la clave en SIVE_SEXO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_MCENTINELA  SIVE_POBLAC_TS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MCENTINELA
    where
      /* %JoinFKPK(:%New,SIVE_MCENTINELA," = "," and") */
      :new.CD_PCENTI = SIVE_MCENTINELA.CD_PCENTI and
      :new.CD_MEDCEN = SIVE_MCENTINELA.CD_MEDCEN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_POBLAC_TS" because "SIVE_MCENTINELA" does not exist.'*/
         'no se puede actualizar SIVE_POBLAC_TS
          porque no existe la clave en SIVE_MCENTINELA'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_POBLACION_NG_TRIG_A_I_1 after INSERT on SIVE_POBLACION_NG for each row
-- ERwin Builtin Wed Oct 31 16:28:50 2001
-- INSERT trigger on SIVE_POBLACION_NG 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_MUNICIPIO R/264 SIVE_POBLACION_NG ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
        :new.CD_PROV = SIVE_MUNICIPIO.CD_PROV and
        :new.CD_MUN = SIVE_MUNICIPIO.CD_MUN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_POBLACION_NG" because "SIVE_MUNICIPIO" does not exist.'*/
          'no se puede grabar en SIVE_POBLACION_NG
           porque no existe la clave en SIVE_MUNICIPIO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
    /* SIVE_ANO_EPI R/263 SIVE_POBLACION_NG ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ANO_EPI
      where
        /* %JoinFKPK(:%New,SIVE_ANO_EPI," = "," and") */
        :new.CD_ANOEPI = SIVE_ANO_EPI.CD_ANOEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_POBLACION_NG" because "SIVE_ANO_EPI" does not exist.'*/
          'no se puede grabar en SIVE_POBLACION_NG
           porque no existe la clave en SIVE_ANO_EPI'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:50 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_POBLACION_NG_TRIG_A_U_1 after UPDATE on SIVE_POBLACION_NG for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_POBLACION_NG 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_MUNICIPIO R/264 SIVE_POBLACION_NG ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MUNICIPIO
    where
      /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
      :new.CD_PROV = SIVE_MUNICIPIO.CD_PROV and
      :new.CD_MUN = SIVE_MUNICIPIO.CD_MUN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_POBLACION_NG" because "SIVE_MUNICIPIO" does not exist.'*/
         'no se puede actualizar SIVE_POBLACION_NG
          porque no existe la clave en SIVE_MUNICIPIO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:50 2001 */
  /* SIVE_ANO_EPI R/263 SIVE_POBLACION_NG ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ANO_EPI
    where
      /* %JoinFKPK(:%New,SIVE_ANO_EPI," = "," and") */
      :new.CD_ANOEPI = SIVE_ANO_EPI.CD_ANOEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_POBLACION_NG" because "SIVE_ANO_EPI" does not exist.'*/
         'no se puede actualizar SIVE_POBLACION_NG
          porque no existe la clave en SIVE_ANO_EPI'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_POBLACION_NS_TRIG_A_I_1 after INSERT on SIVE_POBLACION_NS for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_POBLACION_NS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_ZONA_BASICA R/284 SIVE_POBLACION_NS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ZONA_BASICA
      where
        /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
        :new.CD_NIVEL_1 = SIVE_ZONA_BASICA.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_ZONA_BASICA.CD_NIVEL_2 and
        :new.CD_ZBS = SIVE_ZONA_BASICA.CD_ZBS;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_POBLACION_NS" because "SIVE_ZONA_BASICA" does not exist.'*/
          'no se puede grabar en SIVE_POBLACION_NS
           porque no existe la clave en SIVE_ZONA_BASICA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_ANO_EPI R/283 SIVE_POBLACION_NS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ANO_EPI
      where
        /* %JoinFKPK(:%New,SIVE_ANO_EPI," = "," and") */
        :new.CD_ANOEPI = SIVE_ANO_EPI.CD_ANOEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_POBLACION_NS" because "SIVE_ANO_EPI" does not exist.'*/
          'no se puede grabar en SIVE_POBLACION_NS
           porque no existe la clave en SIVE_ANO_EPI'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_POBLACION_NS_TRIG_A_U_1 after UPDATE on SIVE_POBLACION_NS for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_POBLACION_NS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_ZONA_BASICA R/284 SIVE_POBLACION_NS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ZONA_BASICA
    where
      /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
      :new.CD_NIVEL_1 = SIVE_ZONA_BASICA.CD_NIVEL_1 and
      :new.CD_NIVEL_2 = SIVE_ZONA_BASICA.CD_NIVEL_2 and
      :new.CD_ZBS = SIVE_ZONA_BASICA.CD_ZBS;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_POBLACION_NS" because "SIVE_ZONA_BASICA" does not exist.'*/
         'no se puede actualizar SIVE_POBLACION_NS
          porque no existe la clave en SIVE_ZONA_BASICA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_ANO_EPI R/283 SIVE_POBLACION_NS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ANO_EPI
    where
      /* %JoinFKPK(:%New,SIVE_ANO_EPI," = "," and") */
      :new.CD_ANOEPI = SIVE_ANO_EPI.CD_ANOEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_POBLACION_NS" because "SIVE_ANO_EPI" does not exist.'*/
         'no se puede actualizar SIVE_POBLACION_NS
          porque no existe la clave en SIVE_ANO_EPI'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PREG_FIJA_TRIG_A_I_1 after INSERT on SIVE_PREG_FIJA for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_PREG_FIJA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_ENFER_CIE  SIVE_PREG_FIJA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ENFER_CIE
      where
        /* %JoinFKPK(:%New,SIVE_ENFER_CIE," = "," and") */
        :new.CD_ENFCIE = SIVE_ENFER_CIE.CD_ENFCIE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_PREG_FIJA" because "SIVE_ENFER_CIE" does not exist.'*/
          'no se puede grabar en SIVE_PREG_FIJA
           porque no existe la clave en SIVE_ENFER_CIE'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_PREGUNTA  SIVE_PREG_FIJA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_PREGUNTA
      where
        /* %JoinFKPK(:%New,SIVE_PREGUNTA," = "," and") */
        :new.CD_TSIVE = SIVE_PREGUNTA.CD_TSIVE and
        :new.CD_PREGUNTA = SIVE_PREGUNTA.CD_PREGUNTA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_PREG_FIJA" because "SIVE_PREGUNTA" does not exist.'*/
          'no se puede grabar en SIVE_PREG_FIJA
           porque no existe la clave en SIVE_PREGUNTA'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PREG_FIJA_TRIG_A_U_1 after UPDATE on SIVE_PREG_FIJA for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_PREG_FIJA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_ENFER_CIE  SIVE_PREG_FIJA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ENFER_CIE
    where
      /* %JoinFKPK(:%New,SIVE_ENFER_CIE," = "," and") */
      :new.CD_ENFCIE = SIVE_ENFER_CIE.CD_ENFCIE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_PREG_FIJA" because "SIVE_ENFER_CIE" does not exist.'*/
         'no se puede actualizar SIVE_PREG_FIJA
          porque no existe la clave en SIVE_ENFER_CIE'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_PREGUNTA  SIVE_PREG_FIJA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_PREGUNTA
    where
      /* %JoinFKPK(:%New,SIVE_PREGUNTA," = "," and") */
      :new.CD_TSIVE = SIVE_PREGUNTA.CD_TSIVE and
      :new.CD_PREGUNTA = SIVE_PREGUNTA.CD_PREGUNTA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_PREG_FIJA" because "SIVE_PREGUNTA" does not exist.'*/
         'no se puede actualizar SIVE_PREG_FIJA
          porque no existe la clave en SIVE_PREGUNTA'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PREGUNTA_TRIG_A_D_1 after DELETE on SIVE_PREGUNTA for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- DELETE trigger on SIVE_PREGUNTA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_PREGUNTA  SIVE_FACTOR_RIESGO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_FACTOR_RIESGO
      where
        /*  %JoinFKPK(SIVE_FACTOR_RIESGO,:%Old," = "," and") */
        SIVE_FACTOR_RIESGO.CD_TSIVE = :old.CD_TSIVE and
        SIVE_FACTOR_RIESGO.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_PREGUNTA" because "SIVE_FACTOR_RIESGO" exists.'*/
       'no se puede borrar de SIVE_PREGUNTA
        porque hay registros en SIVE_FACTOR_RIESGO con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_PREGUNTA  SIVE_LINEA_ITEM ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_LINEA_ITEM
      where
        /*  %JoinFKPK(SIVE_LINEA_ITEM,:%Old," = "," and") */
        SIVE_LINEA_ITEM.CD_TSIVE = :old.CD_TSIVE and
        SIVE_LINEA_ITEM.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_PREGUNTA" because "SIVE_LINEA_ITEM" exists.'*/
       'no se puede borrar de SIVE_PREGUNTA
        porque hay registros en SIVE_LINEA_ITEM con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_PREGUNTA  SIVE_PREG_FIJA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_PREG_FIJA
      where
        /*  %JoinFKPK(SIVE_PREG_FIJA,:%Old," = "," and") */
        SIVE_PREG_FIJA.CD_TSIVE = :old.CD_TSIVE and
        SIVE_PREG_FIJA.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_PREGUNTA" because "SIVE_PREG_FIJA" exists.'*/
       'no se puede borrar de SIVE_PREGUNTA
        porque hay registros en SIVE_PREG_FIJA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PREGUNTA_TRIG_A_I_1 after INSERT on SIVE_PREGUNTA for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_PREGUNTA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_TSIVE  SIVE_PREGUNTA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_TSIVE
      where
        /* %JoinFKPK(:%New,SIVE_TSIVE," = "," and") */
        :new.CD_TSIVE = SIVE_TSIVE.CD_TSIVE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_PREGUNTA" because "SIVE_TSIVE" does not exist.'*/
          'no se puede grabar en SIVE_PREGUNTA
           porque no existe la clave en SIVE_TSIVE'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_LISTAS  SIVE_PREGUNTA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_LISTAS
      where
        /* %JoinFKPK(:%New,SIVE_LISTAS," = "," and") */
        :new.CD_LISTA = SIVE_LISTAS.CD_LISTA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_LISTA is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_PREGUNTA" because "SIVE_LISTAS" does not exist.'*/
          'no se puede grabar en SIVE_PREGUNTA
           porque no existe la clave en SIVE_LISTAS'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_TPREGUNTA  SIVE_PREGUNTA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_TPREGUNTA
      where
        /* %JoinFKPK(:%New,SIVE_TPREGUNTA," = "," and") */
        :new.CD_TPREG = SIVE_TPREGUNTA.CD_TPREG;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_PREGUNTA" because "SIVE_TPREGUNTA" does not exist.'*/
          'no se puede grabar en SIVE_PREGUNTA
           porque no existe la clave en SIVE_TPREGUNTA'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PREGUNTA_TRIG_A_U_1 after UPDATE on SIVE_PREGUNTA for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_PREGUNTA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_PREGUNTA  SIVE_FACTOR_RIESGO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TSIVE <> :new.CD_TSIVE or 
    :old.CD_PREGUNTA <> :new.CD_PREGUNTA
  then
    select count(*) into numrows
      from SIVE_FACTOR_RIESGO
      where
        /*  %JoinFKPK(SIVE_FACTOR_RIESGO,:%Old," = "," and") */
        SIVE_FACTOR_RIESGO.CD_TSIVE = :old.CD_TSIVE and
        SIVE_FACTOR_RIESGO.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_PREGUNTA" because "SIVE_FACTOR_RIESGO" exists.'*/
       'no se puede modificar SIVE_PREGUNTA
        porque hay registros en SIVE_FACTOR_RIESGO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_PREGUNTA  SIVE_LINEA_ITEM ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TSIVE <> :new.CD_TSIVE or 
    :old.CD_PREGUNTA <> :new.CD_PREGUNTA
  then
    select count(*) into numrows
      from SIVE_LINEA_ITEM
      where
        /*  %JoinFKPK(SIVE_LINEA_ITEM,:%Old," = "," and") */
        SIVE_LINEA_ITEM.CD_TSIVE = :old.CD_TSIVE and
        SIVE_LINEA_ITEM.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_PREGUNTA" because "SIVE_LINEA_ITEM" exists.'*/
       'no se puede modificar SIVE_PREGUNTA
        porque hay registros en SIVE_LINEA_ITEM con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_PREGUNTA  SIVE_PREG_FIJA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TSIVE <> :new.CD_TSIVE or 
    :old.CD_PREGUNTA <> :new.CD_PREGUNTA
  then
    select count(*) into numrows
      from SIVE_PREG_FIJA
      where
        /*  %JoinFKPK(SIVE_PREG_FIJA,:%Old," = "," and") */
        SIVE_PREG_FIJA.CD_TSIVE = :old.CD_TSIVE and
        SIVE_PREG_FIJA.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_PREGUNTA" because "SIVE_PREG_FIJA" exists.'*/
       'no se puede modificar SIVE_PREGUNTA
        porque hay registros en SIVE_PREG_FIJA con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_TSIVE  SIVE_PREGUNTA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_TSIVE
    where
      /* %JoinFKPK(:%New,SIVE_TSIVE," = "," and") */
      :new.CD_TSIVE = SIVE_TSIVE.CD_TSIVE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_PREGUNTA" because "SIVE_TSIVE" does not exist.'*/
         'no se puede actualizar SIVE_PREGUNTA
          porque no existe la clave en SIVE_TSIVE'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_LISTAS  SIVE_PREGUNTA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_LISTAS
    where
      /* %JoinFKPK(:%New,SIVE_LISTAS," = "," and") */
      :new.CD_LISTA = SIVE_LISTAS.CD_LISTA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_LISTA is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_PREGUNTA" because "SIVE_LISTAS" does not exist.'*/
         'no se puede actualizar SIVE_PREGUNTA
          porque no existe la clave en SIVE_LISTAS'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_TPREGUNTA  SIVE_PREGUNTA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_TPREGUNTA
    where
      /* %JoinFKPK(:%New,SIVE_TPREGUNTA," = "," and") */
      :new.CD_TPREG = SIVE_TPREGUNTA.CD_TPREG;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_PREGUNTA" because "SIVE_TPREGUNTA" does not exist.'*/
         'no se puede actualizar SIVE_PREGUNTA
          porque no existe la clave en SIVE_TPREGUNTA'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PROCESOS_TRIG_A_D_1 after DELETE on SIVE_PROCESOS for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- DELETE trigger on SIVE_PROCESOS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_PROCESOS R/298 SIVE_ENFEREDO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ENFEREDO
      where
        /*  %JoinFKPK(SIVE_ENFEREDO,:%Old," = "," and") */
        SIVE_ENFEREDO.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_PROCESOS" because "SIVE_ENFEREDO" exists.'*/
       'no se puede borrar de SIVE_PROCESOS
        porque hay registros en SIVE_ENFEREDO con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_PROCESOS  SIVE_ENF_CENTI ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ENF_CENTI
      where
        /*  %JoinFKPK(SIVE_ENF_CENTI,:%Old," = "," and") */
        SIVE_ENF_CENTI.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_PROCESOS" because "SIVE_ENF_CENTI" exists.'*/
       'no se puede borrar de SIVE_PROCESOS
        porque hay registros en SIVE_ENF_CENTI con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_PROCESOS  SIVE_MODELO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MODELO
      where
        /*  %JoinFKPK(SIVE_MODELO,:%Old," = "," and") */
        SIVE_MODELO.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_PROCESOS" because "SIVE_MODELO" exists.'*/
       'no se puede borrar de SIVE_PROCESOS
        porque hay registros en SIVE_MODELO con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_PROCESOS_TRIG_A_U_1 after UPDATE on SIVE_PROCESOS for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_PROCESOS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_PROCESOS R/298 SIVE_ENFEREDO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE
  then
    select count(*) into numrows
      from SIVE_ENFEREDO
      where
        /*  %JoinFKPK(SIVE_ENFEREDO,:%Old," = "," and") */
        SIVE_ENFEREDO.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_PROCESOS" because "SIVE_ENFEREDO" exists.'*/
       'no se puede modificar SIVE_PROCESOS
        porque hay registros en SIVE_ENFEREDO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_PROCESOS  SIVE_ENF_CENTI ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE
  then
    select count(*) into numrows
      from SIVE_ENF_CENTI
      where
        /*  %JoinFKPK(SIVE_ENF_CENTI,:%Old," = "," and") */
        SIVE_ENF_CENTI.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_PROCESOS" because "SIVE_ENF_CENTI" exists.'*/
       'no se puede modificar SIVE_PROCESOS
        porque hay registros en SIVE_ENF_CENTI con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_PROCESOS  SIVE_MODELO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ENFCIE <> :new.CD_ENFCIE
  then
    select count(*) into numrows
      from SIVE_MODELO
      where
        /*  %JoinFKPK(SIVE_MODELO,:%Old," = "," and") */
        SIVE_MODELO.CD_ENFCIE = :old.CD_ENFCIE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_PROCESOS" because "SIVE_MODELO" exists.'*/
       'no se puede modificar SIVE_PROCESOS
        porque hay registros en SIVE_MODELO con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

create trigger SIVE_PROVINCIA_TRIG_A_U
  AFTER UPDATE
  on SIVE_PROVINCIA
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Tue Dec 28 10:19:03 1999 */
  /* SIVE_PROVINCIA R/129 SIVE_MUNICIPIO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_PROV <> :new.CD_PROV
  then
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /*  %JoinFKPK(SIVE_MUNICIPIO,:%Old," = "," and") */
        SIVE_MUNICIPIO.CD_PROV = :old.CD_PROV;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_PROVINCIA" debido a que existe "SIVE_MUNICIPIO".'
      );
    end if;
  end if;

  /* ERwin Builtin Tue Dec 28 10:19:03 1999 */
  /* SIVE_COM_AUT R/21 SIVE_PROVINCIA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_COM_AUT
    where
      /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
      :new.CD_CA = SIVE_COM_AUT.CD_CA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_PROVINCIA" debido a que no existe en "SIVE_COM_AUT".'
    );
  end if;


-- ERwin Builtin Tue Dec 28 10:19:03 1999
end;
/


create trigger SIVE_PROVINCIA_TRIG_A_D
  AFTER DELETE
  on SIVE_PROVINCIA
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Tue Dec 28 10:19:00 1999 */
    /* SIVE_PROVINCIA R/129 SIVE_MUNICIPIO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /*  %JoinFKPK(SIVE_MUNICIPIO,:%Old," = "," and") */
        SIVE_MUNICIPIO.CD_PROV = :old.CD_PROV;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_PROVINCIA" debido a que existe en "SIVE_MUNICIPIO".'
      );
    end if;


-- ERwin Builtin Tue Dec 28 10:19:00 1999
end;
/


create trigger SIVE_PROVINCIA_TRIG_A_I
  AFTER INSERT
  on SIVE_PROVINCIA
  
  for each row
declare numrows INTEGER;
begin

    /* ERwin Builtin Tue Dec 28 10:19:03 1999 */
    /* SIVE_COM_AUT R/21 SIVE_PROVINCIA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_COM_AUT
      where
        /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
        :new.CD_CA = SIVE_COM_AUT.CD_CA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_PROVINCIA" because a que no existe en "SIVE_COM_AUT".'
      );
    end if;


-- ERwin Builtin Tue Dec 28 10:19:03 1999
end;
/


CREATE OR REPLACE TRIGGER SIVE_RANGO_ANO_TRIG_A_I_1 after INSERT on SIVE_RANGO_ANO for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_RANGO_ANO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_ANO_EPI_RMC  SIVE_RANGO_ANO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ANO_EPI_RMC
      where
        /* %JoinFKPK(:%New,SIVE_ANO_EPI_RMC," = "," and") */
        :new.CD_ANOEPI = SIVE_ANO_EPI_RMC.CD_ANOEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RANGO_ANO" because "SIVE_ANO_EPI_RMC" does not exist.'*/
          'no se puede grabar en SIVE_RANGO_ANO
           porque no existe la clave en SIVE_ANO_EPI_RMC'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_TIPO_RANGO  SIVE_RANGO_ANO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_TIPO_RANGO
      where
        /* %JoinFKPK(:%New,SIVE_TIPO_RANGO," = "," and") */
        :new.CD_TIPORANGO = SIVE_TIPO_RANGO.CD_TIPORANGO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RANGO_ANO" because "SIVE_TIPO_RANGO" does not exist.'*/
          'no se puede grabar en SIVE_RANGO_ANO
           porque no existe la clave en SIVE_TIPO_RANGO'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RANGO_ANO_TRIG_A_U_1 after UPDATE on SIVE_RANGO_ANO for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_RANGO_ANO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_ANO_EPI_RMC  SIVE_RANGO_ANO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ANO_EPI_RMC
    where
      /* %JoinFKPK(:%New,SIVE_ANO_EPI_RMC," = "," and") */
      :new.CD_ANOEPI = SIVE_ANO_EPI_RMC.CD_ANOEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RANGO_ANO" because "SIVE_ANO_EPI_RMC" does not exist.'*/
         'no se puede actualizar SIVE_RANGO_ANO
          porque no existe la clave en SIVE_ANO_EPI_RMC'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_TIPO_RANGO  SIVE_RANGO_ANO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_TIPO_RANGO
    where
      /* %JoinFKPK(:%New,SIVE_TIPO_RANGO," = "," and") */
      :new.CD_TIPORANGO = SIVE_TIPO_RANGO.CD_TIPORANGO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RANGO_ANO" because "SIVE_TIPO_RANGO" does not exist.'*/
         'no se puede actualizar SIVE_RANGO_ANO
          porque no existe la clave en SIVE_TIPO_RANGO'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_REGISTROTBC_TRIG_A_D_1 after DELETE on SIVE_REGISTROTBC for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- DELETE trigger on SIVE_REGISTROTBC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_REGISTROTBC  SIVE_CASOCONTAC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_CASOCONTAC
      where
        /*  %JoinFKPK(SIVE_CASOCONTAC,:%Old," = "," and") */
        SIVE_CASOCONTAC.CD_ARTBC = :old.CD_ARTBC and
        SIVE_CASOCONTAC.CD_NRTBC = :old.CD_NRTBC;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_REGISTROTBC" because "SIVE_CASOCONTAC" exists.'*/
       'no se puede borrar de SIVE_REGISTROTBC
        porque hay registros en SIVE_CASOCONTAC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_REGISTROTBC_TRIG_A_I_1 after INSERT on SIVE_REGISTROTBC for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_REGISTROTBC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_NIVEL2_S  SIVE_REGISTROTBC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL2_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
        :new.CD_NIVEL_1_GE = SIVE_NIVEL2_S.CD_NIVEL_1 and
        :new.CD_NIVEL_2_GE = SIVE_NIVEL2_S.CD_NIVEL_2;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_REGISTROTBC" because "SIVE_NIVEL2_S" does not exist.'*/
          'no se puede grabar en SIVE_REGISTROTBC
           porque no existe la clave en SIVE_NIVEL2_S'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_MOTIVOS_SALRTBC  SIVE_REGISTROTBC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MOTIVOS_SALRTBC
      where
        /* %JoinFKPK(:%New,SIVE_MOTIVOS_SALRTBC," = "," and") */
        :new.CD_MOTSALRTBC = SIVE_MOTIVOS_SALRTBC.CD_MOTSALRTBC;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_MOTSALRTBC is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_REGISTROTBC" because "SIVE_MOTIVOS_SALRTBC" does not exist.'*/
          'no se puede grabar en SIVE_REGISTROTBC
           porque no existe la clave en SIVE_MOTIVOS_SALRTBC'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_E_NOTIF  SIVE_REGISTROTBC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_E_NOTIF
      where
        /* %JoinFKPK(:%New,SIVE_E_NOTIF," = "," and") */
        :new.CD_E_NOTIF = SIVE_E_NOTIF.CD_E_NOTIF;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_E_NOTIF is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_REGISTROTBC" because "SIVE_E_NOTIF" does not exist.'*/
          'no se puede grabar en SIVE_REGISTROTBC
           porque no existe la clave en SIVE_E_NOTIF'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_EDOIND  SIVE_REGISTROTBC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_EDOIND
      where
        /* %JoinFKPK(:%New,SIVE_EDOIND," = "," and") */
        :new.NM_EDO = SIVE_EDOIND.NM_EDO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.NM_EDO is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_REGISTROTBC" because "SIVE_EDOIND" does not exist.'*/
          'no se puede grabar en SIVE_REGISTROTBC
           porque no existe la clave en SIVE_EDOIND'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_NIVEL1_S  SIVE_REGISTROTBC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL1_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
        :new.CD_NIVEL_1_GE = SIVE_NIVEL1_S.CD_NIVEL_1;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_REGISTROTBC" because "SIVE_NIVEL1_S" does not exist.'*/
          'no se puede grabar en SIVE_REGISTROTBC
           porque no existe la clave en SIVE_NIVEL1_S'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_REGISTROTBC_TRIG_A_U_1 after UPDATE on SIVE_REGISTROTBC for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_REGISTROTBC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_REGISTROTBC  SIVE_CASOCONTAC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ARTBC <> :new.CD_ARTBC or 
    :old.CD_NRTBC <> :new.CD_NRTBC
  then
    select count(*) into numrows
      from SIVE_CASOCONTAC
      where
        /*  %JoinFKPK(SIVE_CASOCONTAC,:%Old," = "," and") */
        SIVE_CASOCONTAC.CD_ARTBC = :old.CD_ARTBC and
        SIVE_CASOCONTAC.CD_NRTBC = :old.CD_NRTBC;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_REGISTROTBC" because "SIVE_CASOCONTAC" exists.'*/
       'no se puede modificar SIVE_REGISTROTBC
        porque hay registros en SIVE_CASOCONTAC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_NIVEL2_S  SIVE_REGISTROTBC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL2_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
      :new.CD_NIVEL_1_GE = SIVE_NIVEL2_S.CD_NIVEL_1 and
      :new.CD_NIVEL_2_GE = SIVE_NIVEL2_S.CD_NIVEL_2;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_REGISTROTBC" because "SIVE_NIVEL2_S" does not exist.'*/
         'no se puede actualizar SIVE_REGISTROTBC
          porque no existe la clave en SIVE_NIVEL2_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_MOTIVOS_SALRTBC  SIVE_REGISTROTBC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MOTIVOS_SALRTBC
    where
      /* %JoinFKPK(:%New,SIVE_MOTIVOS_SALRTBC," = "," and") */
      :new.CD_MOTSALRTBC = SIVE_MOTIVOS_SALRTBC.CD_MOTSALRTBC;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_MOTSALRTBC is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_REGISTROTBC" because "SIVE_MOTIVOS_SALRTBC" does not exist.'*/
         'no se puede actualizar SIVE_REGISTROTBC
          porque no existe la clave en SIVE_MOTIVOS_SALRTBC'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_E_NOTIF  SIVE_REGISTROTBC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_E_NOTIF
    where
      /* %JoinFKPK(:%New,SIVE_E_NOTIF," = "," and") */
      :new.CD_E_NOTIF = SIVE_E_NOTIF.CD_E_NOTIF;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_E_NOTIF is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_REGISTROTBC" because "SIVE_E_NOTIF" does not exist.'*/
         'no se puede actualizar SIVE_REGISTROTBC
          porque no existe la clave en SIVE_E_NOTIF'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_EDOIND  SIVE_REGISTROTBC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_EDOIND
    where
      /* %JoinFKPK(:%New,SIVE_EDOIND," = "," and") */
      :new.NM_EDO = SIVE_EDOIND.NM_EDO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.NM_EDO is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_REGISTROTBC" because "SIVE_EDOIND" does not exist.'*/
         'no se puede actualizar SIVE_REGISTROTBC
          porque no existe la clave en SIVE_EDOIND'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_NIVEL1_S  SIVE_REGISTROTBC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL1_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
      :new.CD_NIVEL_1_GE = SIVE_NIVEL1_S.CD_NIVEL_1;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_REGISTROTBC" because "SIVE_NIVEL1_S" does not exist.'*/
         'no se puede actualizar SIVE_REGISTROTBC
          porque no existe la clave en SIVE_NIVEL1_S'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RES_RESISTENCI_TRIG_A_I_1 after INSERT on SIVE_RES_RESISTENCIA for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_RES_RESISTENCIA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_ESTUDIORESIS  SIVE_RES_RESISTENCIA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ESTUDIORESIS
      where
        /* %JoinFKPK(:%New,SIVE_ESTUDIORESIS," = "," and") */
        :new.CD_ESTREST = SIVE_ESTUDIORESIS.CD_ESTREST;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RES_RESISTENCIA" because "SIVE_ESTUDIORESIS" does not exist.'*/
          'no se puede grabar en SIVE_RES_RESISTENCIA
           porque no existe la clave en SIVE_ESTUDIORESIS'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_RESLAB  SIVE_RES_RESISTENCIA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_RESLAB
      where
        /* %JoinFKPK(:%New,SIVE_RESLAB," = "," and") */
        :new.NM_RESLAB = SIVE_RESLAB.NM_RESLAB;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RES_RESISTENCIA" because "SIVE_RESLAB" does not exist.'*/
          'no se puede grabar en SIVE_RES_RESISTENCIA
           porque no existe la clave en SIVE_RESLAB'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RES_RESISTENCI_TRIG_A_U_1 after UPDATE on SIVE_RES_RESISTENCIA for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_RES_RESISTENCIA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_ESTUDIORESIS  SIVE_RES_RESISTENCIA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ESTUDIORESIS
    where
      /* %JoinFKPK(:%New,SIVE_ESTUDIORESIS," = "," and") */
      :new.CD_ESTREST = SIVE_ESTUDIORESIS.CD_ESTREST;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RES_RESISTENCIA" because "SIVE_ESTUDIORESIS" does not exist.'*/
         'no se puede actualizar SIVE_RES_RESISTENCIA
          porque no existe la clave en SIVE_ESTUDIORESIS'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_RESLAB  SIVE_RES_RESISTENCIA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_RESLAB
    where
      /* %JoinFKPK(:%New,SIVE_RESLAB," = "," and") */
      :new.NM_RESLAB = SIVE_RESLAB.NM_RESLAB;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RES_RESISTENCIA" because "SIVE_RESLAB" does not exist.'*/
         'no se puede actualizar SIVE_RES_RESISTENCIA
          porque no existe la clave en SIVE_RESLAB'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

create trigger SIVE_RES_URGENCIAS_TRIG_B_I
  BEFORE INSERT
  on SIVE_RES_URGENCIAS
  
  for each row
/* ERwin Builtin Wed Oct 31 16:28:51 2001 */
/* default body for SIVE_RES_URGENCIAS_TRIG_B_I */
declare numrows INTEGER;
begin
/* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_SEXO R/353 SIVE_RES_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEXO
      where
        /* %JoinFKPK(:%New,SIVE_SEXO," = "," and") */
        :new.CD_SEXO = SIVE_SEXO.CD_SEXO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RES_URGENCIAS" because "SIVE_SEXO" does not exist.'*/
          'no se puede grabar en RES_URGENCIAS
           porque no existe la clave en SIVE_SEXO'
      );
    end if;

/* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_NIVEL1_S R/350 SIVE_RES_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL1_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RES_URGENCIAS" because "SIVE_NIVEL1_S" does not exist.'*/
          'no se puede grabar en RES_URGENCIAS
           porque no existe la clave en SIVE_NIVEL1_S'
      );
    end if;

/* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_DISTR_GEDAD_ADU R/341 SIVE_RES_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_DISTR_GEDAD_ADU
      where
        /* %JoinFKPK(:%New,SIVE_DISTR_GEDAD_ADU," = "," and") */
        :new.CD_GEDAD = SIVE_DISTR_GEDAD_ADU.CD_GEDAD and
        :new.DIST_EDAD_I = SIVE_DISTR_GEDAD_ADU.DIST_EDAD_I and
        :new.DIST_EDAD_F = SIVE_DISTR_GEDAD_ADU.DIST_EDAD_F;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RES_URGENCIAS" because "SIVE_DISTR_GEDAD_ADU" does not exist.'*/
          'no se puede grabar en RES_URGENCIAS
           porque no existe la clave en SIVE_DISTR_GEDAD_ADU'
      );
    end if;

/* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_DERIVACION R/340 SIVE_RES_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_DERIVACION
      where
        /* %JoinFKPK(:%New,SIVE_DERIVACION," = "," and") */
        :new.CD_DERIVACION = SIVE_DERIVACION.CD_DERIVACION;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RES_URGENCIAS" because "SIVE_DERIVACION" does not exist.'*/
          'no se puede grabar en RES_URGENCIAS
           porque no existe la clave en SIVE_DERIVACION'
      );
    end if;

/* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_CAUSAURG R/335 SIVE_RES_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_CAUSAURG
      where
        /* %JoinFKPK(:%New,SIVE_CAUSAURG," = "," and") */
        :new.CD_CAUSAURG = SIVE_CAUSAURG.CD_CAUSAURG;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RES_URGENCIAS" because "SIVE_CAUSAURG" does not exist.'*/
          'no se puede grabar en RES_URGENCIAS
           porque no existe la clave en SIVE_CAUSAURG'
      );
    end if;

/* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_C_HOSP R/331 SIVE_RES_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_C_HOSP
      where
        /* %JoinFKPK(:%New,SIVE_C_HOSP," = "," and") */
        :new.CD_HOSP = SIVE_C_HOSP.CD_HOSP;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_HOSP is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RES_URGENCIAS" because "SIVE_C_HOSP" does not exist.'*/
          'no se puede grabar en RES_URGENCIAS
           porque no existe la clave en SIVE_C_HOSP'
      );
    end if;





/* Secuenciador */
select SIVE_s_RESUMEN.NextVal
into :new.NM_RESURG
from Dual;



end;
/







CREATE OR REPLACE TRIGGER SIVE_RES_URGENCIAS_TRIG_A_U_1 after UPDATE on SIVE_RES_URGENCIAS for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_RES_URGENCIAS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_SEXO R/353 SIVE_RES_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEXO
    where
      /* %JoinFKPK(:%New,SIVE_SEXO," = "," and") */
      :new.CD_SEXO = SIVE_SEXO.CD_SEXO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RES_URGENCIAS" because "SIVE_SEXO" does not exist.'*/
         'no se puede actualizar RES_URGENCIAS
          porque no existe la clave en SIVE_SEXO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_NIVEL1_S R/350 SIVE_RES_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL1_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL1_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL1_S.CD_NIVEL_1;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RES_URGENCIAS" because "SIVE_NIVEL1_S" does not exist.'*/
         'no se puede actualizar RES_URGENCIAS
          porque no existe la clave en SIVE_NIVEL1_S'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_DISTR_GEDAD_ADU R/341 SIVE_RES_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_DISTR_GEDAD_ADU
    where
      /* %JoinFKPK(:%New,SIVE_DISTR_GEDAD_ADU," = "," and") */
      :new.CD_GEDAD = SIVE_DISTR_GEDAD_ADU.CD_GEDAD and
      :new.DIST_EDAD_I = SIVE_DISTR_GEDAD_ADU.DIST_EDAD_I and
      :new.DIST_EDAD_F = SIVE_DISTR_GEDAD_ADU.DIST_EDAD_F;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RES_URGENCIAS" because "SIVE_DISTR_GEDAD_ADU" does not exist.'*/
         'no se puede actualizar RES_URGENCIAS
          porque no existe la clave en SIVE_DISTR_GEDAD_ADU'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_DERIVACION R/340 SIVE_RES_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_DERIVACION
    where
      /* %JoinFKPK(:%New,SIVE_DERIVACION," = "," and") */
      :new.CD_DERIVACION = SIVE_DERIVACION.CD_DERIVACION;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RES_URGENCIAS" because "SIVE_DERIVACION" does not exist.'*/
         'no se puede actualizar RES_URGENCIAS
          porque no existe la clave en SIVE_DERIVACION'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_CAUSAURG R/335 SIVE_RES_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_CAUSAURG
    where
      /* %JoinFKPK(:%New,SIVE_CAUSAURG," = "," and") */
      :new.CD_CAUSAURG = SIVE_CAUSAURG.CD_CAUSAURG;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RES_URGENCIAS" because "SIVE_CAUSAURG" does not exist.'*/
         'no se puede actualizar RES_URGENCIAS
          porque no existe la clave en SIVE_CAUSAURG'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_C_HOSP R/331 SIVE_RES_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_C_HOSP
    where
      /* %JoinFKPK(:%New,SIVE_C_HOSP," = "," and") */
      :new.CD_HOSP = SIVE_C_HOSP.CD_HOSP;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_HOSP is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RES_URGENCIAS" because "SIVE_C_HOSP" does not exist.'*/
         'no se puede actualizar RES_URGENCIAS
          porque no existe la clave en SIVE_C_HOSP'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESLAB_TRIG_A_D_1 after DELETE on SIVE_RESLAB for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- DELETE trigger on SIVE_RESLAB 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_RESLAB  SIVE_RES_RESISTENCIA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RES_RESISTENCIA
      where
        /*  %JoinFKPK(SIVE_RES_RESISTENCIA,:%Old," = "," and") */
        SIVE_RES_RESISTENCIA.NM_RESLAB = :old.NM_RESLAB;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_RESLAB" because "SIVE_RES_RESISTENCIA" exists.'*/
       'no se puede borrar de SIVE_RESLAB
        porque hay registros en SIVE_RES_RESISTENCIA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESLAB_TRIG_A_I_1 after INSERT on SIVE_RESLAB for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_RESLAB 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_NOTIF_EDOI  SIVE_RESLAB ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIF_EDOI
      where
        /* %JoinFKPK(:%New,SIVE_NOTIF_EDOI," = "," and") */
        :new.NM_EDO = SIVE_NOTIF_EDOI.NM_EDO and
        :new.CD_E_NOTIF = SIVE_NOTIF_EDOI.CD_E_NOTIF and
        :new.CD_ANOEPI = SIVE_NOTIF_EDOI.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_NOTIF_EDOI.CD_SEMEPI and
        :new.FC_RECEP = SIVE_NOTIF_EDOI.FC_RECEP and
        :new.FC_FECNOTIF = SIVE_NOTIF_EDOI.FC_FECNOTIF and
        :new.CD_FUENTE = SIVE_NOTIF_EDOI.CD_FUENTE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESLAB" because "SIVE_NOTIF_EDOI" does not exist.'*/
          'no se puede grabar en SIVE_RESLAB
           porque no existe la clave en SIVE_NOTIF_EDOI'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_VALOR_MUESTRA  SIVE_RESLAB ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_VALOR_MUESTRA
      where
        /* %JoinFKPK(:%New,SIVE_VALOR_MUESTRA," = "," and") */
        :new.CD_VMUESTRA = SIVE_VALOR_MUESTRA.CD_VMUESTRA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_VMUESTRA is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESLAB" because "SIVE_VALOR_MUESTRA" does not exist.'*/
          'no se puede grabar en SIVE_RESLAB
           porque no existe la clave en SIVE_VALOR_MUESTRA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_MUESTRA_LAB  SIVE_RESLAB ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUESTRA_LAB
      where
        /* %JoinFKPK(:%New,SIVE_MUESTRA_LAB," = "," and") */
        :new.CD_MUESTRA = SIVE_MUESTRA_LAB.CD_MUESTRA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESLAB" because "SIVE_MUESTRA_LAB" does not exist.'*/
          'no se puede grabar en SIVE_RESLAB
           porque no existe la clave en SIVE_MUESTRA_LAB'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_TIPO_TECNICALAB  SIVE_RESLAB ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_TIPO_TECNICALAB
      where
        /* %JoinFKPK(:%New,SIVE_TIPO_TECNICALAB," = "," and") */
        :new.CD_TTECLAB = SIVE_TIPO_TECNICALAB.CD_TTECLAB;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESLAB" because "SIVE_TIPO_TECNICALAB" does not exist.'*/
          'no se puede grabar en SIVE_RESLAB
           porque no existe la clave en SIVE_TIPO_TECNICALAB'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_TMICOBACTERIA  SIVE_RESLAB ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_TMICOBACTERIA
      where
        /* %JoinFKPK(:%New,SIVE_TMICOBACTERIA," = "," and") */
        :new.CD_TMICOB = SIVE_TMICOBACTERIA.CD_TMICOB;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESLAB" because "SIVE_TMICOBACTERIA" does not exist.'*/
          'no se puede grabar en SIVE_RESLAB
           porque no existe la clave en SIVE_TMICOBACTERIA'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESLAB_TRIG_A_U_1 after UPDATE on SIVE_RESLAB for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_RESLAB 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_RESLAB  SIVE_RES_RESISTENCIA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.NM_RESLAB <> :new.NM_RESLAB
  then
    select count(*) into numrows
      from SIVE_RES_RESISTENCIA
      where
        /*  %JoinFKPK(SIVE_RES_RESISTENCIA,:%Old," = "," and") */
        SIVE_RES_RESISTENCIA.NM_RESLAB = :old.NM_RESLAB;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_RESLAB" because "SIVE_RES_RESISTENCIA" exists.'*/
       'no se puede modificar SIVE_RESLAB
        porque hay registros en SIVE_RES_RESISTENCIA con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_NOTIF_EDOI  SIVE_RESLAB ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NOTIF_EDOI
    where
      /* %JoinFKPK(:%New,SIVE_NOTIF_EDOI," = "," and") */
      :new.NM_EDO = SIVE_NOTIF_EDOI.NM_EDO and
      :new.CD_E_NOTIF = SIVE_NOTIF_EDOI.CD_E_NOTIF and
      :new.CD_ANOEPI = SIVE_NOTIF_EDOI.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_NOTIF_EDOI.CD_SEMEPI and
      :new.FC_RECEP = SIVE_NOTIF_EDOI.FC_RECEP and
      :new.FC_FECNOTIF = SIVE_NOTIF_EDOI.FC_FECNOTIF and
      :new.CD_FUENTE = SIVE_NOTIF_EDOI.CD_FUENTE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESLAB" because "SIVE_NOTIF_EDOI" does not exist.'*/
         'no se puede actualizar SIVE_RESLAB
          porque no existe la clave en SIVE_NOTIF_EDOI'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_VALOR_MUESTRA  SIVE_RESLAB ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_VALOR_MUESTRA
    where
      /* %JoinFKPK(:%New,SIVE_VALOR_MUESTRA," = "," and") */
      :new.CD_VMUESTRA = SIVE_VALOR_MUESTRA.CD_VMUESTRA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_VMUESTRA is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESLAB" because "SIVE_VALOR_MUESTRA" does not exist.'*/
         'no se puede actualizar SIVE_RESLAB
          porque no existe la clave en SIVE_VALOR_MUESTRA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_MUESTRA_LAB  SIVE_RESLAB ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MUESTRA_LAB
    where
      /* %JoinFKPK(:%New,SIVE_MUESTRA_LAB," = "," and") */
      :new.CD_MUESTRA = SIVE_MUESTRA_LAB.CD_MUESTRA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESLAB" because "SIVE_MUESTRA_LAB" does not exist.'*/
         'no se puede actualizar SIVE_RESLAB
          porque no existe la clave en SIVE_MUESTRA_LAB'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_TIPO_TECNICALAB  SIVE_RESLAB ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_TIPO_TECNICALAB
    where
      /* %JoinFKPK(:%New,SIVE_TIPO_TECNICALAB," = "," and") */
      :new.CD_TTECLAB = SIVE_TIPO_TECNICALAB.CD_TTECLAB;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESLAB" because "SIVE_TIPO_TECNICALAB" does not exist.'*/
         'no se puede actualizar SIVE_RESLAB
          porque no existe la clave en SIVE_TIPO_TECNICALAB'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_TMICOBACTERIA  SIVE_RESLAB ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_TMICOBACTERIA
    where
      /* %JoinFKPK(:%New,SIVE_TMICOBACTERIA," = "," and") */
      :new.CD_TMICOB = SIVE_TMICOBACTERIA.CD_TMICOB;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESLAB" because "SIVE_TMICOBACTERIA" does not exist.'*/
         'no se puede actualizar SIVE_RESLAB
          porque no existe la clave en SIVE_TMICOBACTERIA'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESP_ADIC_TRIG_A_I_1 after INSERT on SIVE_RESP_ADIC for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_RESP_ADIC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_LINEA_ITEM R/276 SIVE_RESP_ADIC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_LINEA_ITEM
      where
        /* %JoinFKPK(:%New,SIVE_LINEA_ITEM," = "," and") */
        :new.CD_TSIVE = SIVE_LINEA_ITEM.CD_TSIVE and
        :new.CD_MODELO = SIVE_LINEA_ITEM.CD_MODELO and
        :new.NM_LIN = SIVE_LINEA_ITEM.NM_LIN and
        :new.CD_PREGUNTA = SIVE_LINEA_ITEM.CD_PREGUNTA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESP_ADIC" because "SIVE_LINEA_ITEM" does not exist.'*/
          'no se puede grabar en SIVE_RESP_ADIC
           porque no existe la clave en SIVE_LINEA_ITEM'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_EDO_DADIC R/268 SIVE_RESP_ADIC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_EDO_DADIC
      where
        /* %JoinFKPK(:%New,SIVE_EDO_DADIC," = "," and") */
        :new.CD_ENFCIE = SIVE_EDO_DADIC.CD_ENFCIE and
        :new.CD_SEMEPI = SIVE_EDO_DADIC.CD_SEMEPI and
        :new.CD_ANOEPI = SIVE_EDO_DADIC.CD_ANOEPI and
        :new.SEQ_CODIGO = SIVE_EDO_DADIC.SEQ_CODIGO and
        :new.CD_E_NOTIF = SIVE_EDO_DADIC.CD_E_NOTIF and
        :new.FC_RECEP = SIVE_EDO_DADIC.FC_RECEP and
        :new.FC_FECNOTIF = SIVE_EDO_DADIC.FC_FECNOTIF;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESP_ADIC" because "SIVE_EDO_DADIC" does not exist.'*/
          'no se puede grabar en SIVE_RESP_ADIC
           porque no existe la clave en SIVE_EDO_DADIC'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESP_ADIC_TRIG_A_U_1 after UPDATE on SIVE_RESP_ADIC for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_RESP_ADIC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_LINEA_ITEM R/276 SIVE_RESP_ADIC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_LINEA_ITEM
    where
      /* %JoinFKPK(:%New,SIVE_LINEA_ITEM," = "," and") */
      :new.CD_TSIVE = SIVE_LINEA_ITEM.CD_TSIVE and
      :new.CD_MODELO = SIVE_LINEA_ITEM.CD_MODELO and
      :new.NM_LIN = SIVE_LINEA_ITEM.NM_LIN and
      :new.CD_PREGUNTA = SIVE_LINEA_ITEM.CD_PREGUNTA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESP_ADIC" because "SIVE_LINEA_ITEM" does not exist.'*/
         'no se puede actualizar SIVE_RESP_ADIC
          porque no existe la clave en SIVE_LINEA_ITEM'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_EDO_DADIC R/268 SIVE_RESP_ADIC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_EDO_DADIC
    where
      /* %JoinFKPK(:%New,SIVE_EDO_DADIC," = "," and") */
      :new.CD_ENFCIE = SIVE_EDO_DADIC.CD_ENFCIE and
      :new.CD_SEMEPI = SIVE_EDO_DADIC.CD_SEMEPI and
      :new.CD_ANOEPI = SIVE_EDO_DADIC.CD_ANOEPI and
      :new.SEQ_CODIGO = SIVE_EDO_DADIC.SEQ_CODIGO and
      :new.CD_E_NOTIF = SIVE_EDO_DADIC.CD_E_NOTIF and
      :new.FC_RECEP = SIVE_EDO_DADIC.FC_RECEP and
      :new.FC_FECNOTIF = SIVE_EDO_DADIC.FC_FECNOTIF;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESP_ADIC" because "SIVE_EDO_DADIC" does not exist.'*/
         'no se puede actualizar SIVE_RESP_ADIC
          porque no existe la clave en SIVE_EDO_DADIC'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESP_BROTES_TRIG_A_I_1 after INSERT on SIVE_RESP_BROTES for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_RESP_BROTES 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_LINEA_ITEM  SIVE_RESP_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_LINEA_ITEM
      where
        /* %JoinFKPK(:%New,SIVE_LINEA_ITEM," = "," and") */
        :new.CD_TSIVE = SIVE_LINEA_ITEM.CD_TSIVE and
        :new.CD_MODELO = SIVE_LINEA_ITEM.CD_MODELO and
        :new.NM_LIN = SIVE_LINEA_ITEM.NM_LIN and
        :new.CD_PREGUNTA = SIVE_LINEA_ITEM.CD_PREGUNTA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESP_BROTES" because "SIVE_LINEA_ITEM" does not exist.'*/
          'no se puede grabar en SIVE_RESP_BROTES
           porque no existe la clave en SIVE_LINEA_ITEM'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_BROTES  SIVE_RESP_BROTES ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
        :new.CD_ANO = SIVE_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESP_BROTES" because "SIVE_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_RESP_BROTES
           porque no existe la clave en SIVE_BROTES'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESP_BROTES_TRIG_A_U_1 after UPDATE on SIVE_RESP_BROTES for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_RESP_BROTES 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_LINEA_ITEM  SIVE_RESP_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_LINEA_ITEM
    where
      /* %JoinFKPK(:%New,SIVE_LINEA_ITEM," = "," and") */
      :new.CD_TSIVE = SIVE_LINEA_ITEM.CD_TSIVE and
      :new.CD_MODELO = SIVE_LINEA_ITEM.CD_MODELO and
      :new.NM_LIN = SIVE_LINEA_ITEM.NM_LIN and
      :new.CD_PREGUNTA = SIVE_LINEA_ITEM.CD_PREGUNTA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESP_BROTES" because "SIVE_LINEA_ITEM" does not exist.'*/
         'no se puede actualizar SIVE_RESP_BROTES
          porque no existe la clave en SIVE_LINEA_ITEM'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_BROTES  SIVE_RESP_BROTES ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
      :new.CD_ANO = SIVE_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESP_BROTES" because "SIVE_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_RESP_BROTES
          porque no existe la clave en SIVE_BROTES'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESP_CENTI_TRIG_A_I_1 after INSERT on SIVE_RESP_CENTI for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_RESP_CENTI 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_LINEA_ITEM  SIVE_RESP_CENTI ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_LINEA_ITEM
      where
        /* %JoinFKPK(:%New,SIVE_LINEA_ITEM," = "," and") */
        :new.CD_TSIVE = SIVE_LINEA_ITEM.CD_TSIVE and
        :new.CD_MODELO = SIVE_LINEA_ITEM.CD_MODELO and
        :new.NM_LIN = SIVE_LINEA_ITEM.NM_LIN and
        :new.CD_PREGUNTA = SIVE_LINEA_ITEM.CD_PREGUNTA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESP_CENTI" because "SIVE_LINEA_ITEM" does not exist.'*/
          'no se puede grabar en SIVE_RESP_CENTI
           porque no existe la clave en SIVE_LINEA_ITEM'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_CASOS_CENT  SIVE_RESP_CENTI ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_CASOS_CENT
      where
        /* %JoinFKPK(:%New,SIVE_CASOS_CENT," = "," and") */
        :new.CD_ENFCIE = SIVE_CASOS_CENT.CD_ENFCIE and
        :new.CD_PCENTI = SIVE_CASOS_CENT.CD_PCENTI and
        :new.CD_MEDCEN = SIVE_CASOS_CENT.CD_MEDCEN and
        :new.CD_ANOEPI = SIVE_CASOS_CENT.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_CASOS_CENT.CD_SEMEPI and
        :new.NM_ORDEN = SIVE_CASOS_CENT.NM_ORDEN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESP_CENTI" because "SIVE_CASOS_CENT" does not exist.'*/
          'no se puede grabar en SIVE_RESP_CENTI
           porque no existe la clave en SIVE_CASOS_CENT'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESP_CENTI_TRIG_A_U_1 after UPDATE on SIVE_RESP_CENTI for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_RESP_CENTI 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_LINEA_ITEM  SIVE_RESP_CENTI ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_LINEA_ITEM
    where
      /* %JoinFKPK(:%New,SIVE_LINEA_ITEM," = "," and") */
      :new.CD_TSIVE = SIVE_LINEA_ITEM.CD_TSIVE and
      :new.CD_MODELO = SIVE_LINEA_ITEM.CD_MODELO and
      :new.NM_LIN = SIVE_LINEA_ITEM.NM_LIN and
      :new.CD_PREGUNTA = SIVE_LINEA_ITEM.CD_PREGUNTA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESP_CENTI" because "SIVE_LINEA_ITEM" does not exist.'*/
         'no se puede actualizar SIVE_RESP_CENTI
          porque no existe la clave en SIVE_LINEA_ITEM'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_CASOS_CENT  SIVE_RESP_CENTI ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_CASOS_CENT
    where
      /* %JoinFKPK(:%New,SIVE_CASOS_CENT," = "," and") */
      :new.CD_ENFCIE = SIVE_CASOS_CENT.CD_ENFCIE and
      :new.CD_PCENTI = SIVE_CASOS_CENT.CD_PCENTI and
      :new.CD_MEDCEN = SIVE_CASOS_CENT.CD_MEDCEN and
      :new.CD_ANOEPI = SIVE_CASOS_CENT.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_CASOS_CENT.CD_SEMEPI and
      :new.NM_ORDEN = SIVE_CASOS_CENT.NM_ORDEN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESP_CENTI" because "SIVE_CASOS_CENT" does not exist.'*/
         'no se puede actualizar SIVE_RESP_CENTI
          porque no existe la clave en SIVE_CASOS_CENT'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESP_EDO_TRIG_A_D_1 after DELETE on SIVE_RESP_EDO for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- DELETE trigger on SIVE_RESP_EDO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_RESP_EDO  SIVE_MOVRESPEDO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MOVRESPEDO
      where
        /*  %JoinFKPK(SIVE_MOVRESPEDO,:%Old," = "," and") */
        SIVE_MOVRESPEDO.CD_TSIVE = :old.CD_TSIVE and
        SIVE_MOVRESPEDO.NM_EDO = :old.NM_EDO and
        SIVE_MOVRESPEDO.CD_MODELO = :old.CD_MODELO and
        SIVE_MOVRESPEDO.NM_LIN = :old.NM_LIN and
        SIVE_MOVRESPEDO.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_RESP_EDO" because "SIVE_MOVRESPEDO" exists.'*/
       'no se puede borrar de SIVE_RESP_EDO
        porque hay registros en SIVE_MOVRESPEDO con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESP_EDO_TRIG_A_I_1 after INSERT on SIVE_RESP_EDO for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_RESP_EDO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_LINEA_ITEM  SIVE_RESP_EDO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_LINEA_ITEM
      where
        /* %JoinFKPK(:%New,SIVE_LINEA_ITEM," = "," and") */
        :new.CD_TSIVE = SIVE_LINEA_ITEM.CD_TSIVE and
        :new.CD_MODELO = SIVE_LINEA_ITEM.CD_MODELO and
        :new.NM_LIN = SIVE_LINEA_ITEM.NM_LIN and
        :new.CD_PREGUNTA = SIVE_LINEA_ITEM.CD_PREGUNTA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESP_EDO" because "SIVE_LINEA_ITEM" does not exist.'*/
          'no se puede grabar en SIVE_RESP_EDO
           porque no existe la clave en SIVE_LINEA_ITEM'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_EDOIND  SIVE_RESP_EDO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_EDOIND
      where
        /* %JoinFKPK(:%New,SIVE_EDOIND," = "," and") */
        :new.NM_EDO = SIVE_EDOIND.NM_EDO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESP_EDO" because "SIVE_EDOIND" does not exist.'*/
          'no se puede grabar en SIVE_RESP_EDO
           porque no existe la clave en SIVE_EDOIND'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESP_EDO_TRIG_A_U_1 after UPDATE on SIVE_RESP_EDO for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_RESP_EDO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_RESP_EDO  SIVE_MOVRESPEDO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TSIVE <> :new.CD_TSIVE or 
    :old.NM_EDO <> :new.NM_EDO or 
    :old.CD_MODELO <> :new.CD_MODELO or 
    :old.NM_LIN <> :new.NM_LIN or 
    :old.CD_PREGUNTA <> :new.CD_PREGUNTA
  then
    select count(*) into numrows
      from SIVE_MOVRESPEDO
      where
        /*  %JoinFKPK(SIVE_MOVRESPEDO,:%Old," = "," and") */
        SIVE_MOVRESPEDO.CD_TSIVE = :old.CD_TSIVE and
        SIVE_MOVRESPEDO.NM_EDO = :old.NM_EDO and
        SIVE_MOVRESPEDO.CD_MODELO = :old.CD_MODELO and
        SIVE_MOVRESPEDO.NM_LIN = :old.NM_LIN and
        SIVE_MOVRESPEDO.CD_PREGUNTA = :old.CD_PREGUNTA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_RESP_EDO" because "SIVE_MOVRESPEDO" exists.'*/
       'no se puede modificar SIVE_RESP_EDO
        porque hay registros en SIVE_MOVRESPEDO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_LINEA_ITEM  SIVE_RESP_EDO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_LINEA_ITEM
    where
      /* %JoinFKPK(:%New,SIVE_LINEA_ITEM," = "," and") */
      :new.CD_TSIVE = SIVE_LINEA_ITEM.CD_TSIVE and
      :new.CD_MODELO = SIVE_LINEA_ITEM.CD_MODELO and
      :new.NM_LIN = SIVE_LINEA_ITEM.NM_LIN and
      :new.CD_PREGUNTA = SIVE_LINEA_ITEM.CD_PREGUNTA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESP_EDO" because "SIVE_LINEA_ITEM" does not exist.'*/
         'no se puede actualizar SIVE_RESP_EDO
          porque no existe la clave en SIVE_LINEA_ITEM'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_EDOIND  SIVE_RESP_EDO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_EDOIND
    where
      /* %JoinFKPK(:%New,SIVE_EDOIND," = "," and") */
      :new.NM_EDO = SIVE_EDOIND.NM_EDO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESP_EDO" because "SIVE_EDOIND" does not exist.'*/
         'no se puede actualizar SIVE_RESP_EDO
          porque no existe la clave en SIVE_EDOIND'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESPCONTACTO_TRIG_A_I_1 after INSERT on SIVE_RESPCONTACTO for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_RESPCONTACTO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_CASOCONTAC R/295 SIVE_RESPCONTACTO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_CASOCONTAC
      where
        /* %JoinFKPK(:%New,SIVE_CASOCONTAC," = "," and") */
        :new.CD_ENFERMO = SIVE_CASOCONTAC.CD_ENFERMO and
        :new.CD_ARTBC = SIVE_CASOCONTAC.CD_ARTBC and
        :new.CD_NRTBC = SIVE_CASOCONTAC.CD_NRTBC;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESPCONTACTO" because "SIVE_CASOCONTAC" does not exist.'*/
          'no se puede grabar en SIVE_RESPCONTACTO
           porque no existe la clave en SIVE_CASOCONTAC'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_LINEA_ITEM  SIVE_RESPCONTACTO ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_LINEA_ITEM
      where
        /* %JoinFKPK(:%New,SIVE_LINEA_ITEM," = "," and") */
        :new.CD_TSIVE = SIVE_LINEA_ITEM.CD_TSIVE and
        :new.CD_MODELO = SIVE_LINEA_ITEM.CD_MODELO and
        :new.NM_LIN = SIVE_LINEA_ITEM.NM_LIN and
        :new.CD_PREGUNTA = SIVE_LINEA_ITEM.CD_PREGUNTA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESPCONTACTO" because "SIVE_LINEA_ITEM" does not exist.'*/
          'no se puede grabar en SIVE_RESPCONTACTO
           porque no existe la clave en SIVE_LINEA_ITEM'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESPCONTACTO_TRIG_A_U_1 after UPDATE on SIVE_RESPCONTACTO for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_RESPCONTACTO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_CASOCONTAC R/295 SIVE_RESPCONTACTO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_CASOCONTAC
    where
      /* %JoinFKPK(:%New,SIVE_CASOCONTAC," = "," and") */
      :new.CD_ENFERMO = SIVE_CASOCONTAC.CD_ENFERMO and
      :new.CD_ARTBC = SIVE_CASOCONTAC.CD_ARTBC and
      :new.CD_NRTBC = SIVE_CASOCONTAC.CD_NRTBC;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESPCONTACTO" because "SIVE_CASOCONTAC" does not exist.'*/
         'no se puede actualizar SIVE_RESPCONTACTO
          porque no existe la clave en SIVE_CASOCONTAC'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_LINEA_ITEM  SIVE_RESPCONTACTO ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_LINEA_ITEM
    where
      /* %JoinFKPK(:%New,SIVE_LINEA_ITEM," = "," and") */
      :new.CD_TSIVE = SIVE_LINEA_ITEM.CD_TSIVE and
      :new.CD_MODELO = SIVE_LINEA_ITEM.CD_MODELO and
      :new.NM_LIN = SIVE_LINEA_ITEM.NM_LIN and
      :new.CD_PREGUNTA = SIVE_LINEA_ITEM.CD_PREGUNTA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESPCONTACTO" because "SIVE_LINEA_ITEM" does not exist.'*/
         'no se puede actualizar SIVE_RESPCONTACTO
          porque no existe la clave en SIVE_LINEA_ITEM'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESUMEN_EDOS_TRIG_A_I_1 after INSERT on SIVE_RESUMEN_EDOS for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_RESUMEN_EDOS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_SEMANA_EPI R/286 SIVE_RESUMEN_EDOS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEMANA_EPI
      where
        /* %JoinFKPK(:%New,SIVE_SEMANA_EPI," = "," and") */
        :new.CD_ANOEPI = SIVE_SEMANA_EPI.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_SEMANA_EPI.CD_SEMEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESUMEN_EDOS" because "SIVE_SEMANA_EPI" does not exist.'*/
          'no se puede grabar en SIVE_RESUMEN_EDOS
           porque no existe la clave en SIVE_SEMANA_EPI'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_ZONA_BASICA R/285 SIVE_RESUMEN_EDOS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ZONA_BASICA
      where
        /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
        :new.CD_NIVEL_1 = SIVE_ZONA_BASICA.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_ZONA_BASICA.CD_NIVEL_2 and
        :new.CD_ZBS = SIVE_ZONA_BASICA.CD_ZBS;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESUMEN_EDOS" because "SIVE_ZONA_BASICA" does not exist.'*/
          'no se puede grabar en SIVE_RESUMEN_EDOS
           porque no existe la clave en SIVE_ZONA_BASICA'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_MUNICIPIO R/277 SIVE_RESUMEN_EDOS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
        :new.CD_PROV = SIVE_MUNICIPIO.CD_PROV and
        :new.CD_MUN = SIVE_MUNICIPIO.CD_MUN;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESUMEN_EDOS" because "SIVE_MUNICIPIO" does not exist.'*/
          'no se puede grabar en SIVE_RESUMEN_EDOS
           porque no existe la clave en SIVE_MUNICIPIO'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_ENFEREDO R/272 SIVE_RESUMEN_EDOS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ENFEREDO
      where
        /* %JoinFKPK(:%New,SIVE_ENFEREDO," = "," and") */
        :new.CD_ENFCIE = SIVE_ENFEREDO.CD_ENFCIE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_RESUMEN_EDOS" because "SIVE_ENFEREDO" does not exist.'*/
          'no se puede grabar en SIVE_RESUMEN_EDOS
           porque no existe la clave en SIVE_ENFEREDO'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_RESUMEN_EDOS_TRIG_A_U_1 after UPDATE on SIVE_RESUMEN_EDOS for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_RESUMEN_EDOS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_SEMANA_EPI R/286 SIVE_RESUMEN_EDOS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEMANA_EPI
    where
      /* %JoinFKPK(:%New,SIVE_SEMANA_EPI," = "," and") */
      :new.CD_ANOEPI = SIVE_SEMANA_EPI.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_SEMANA_EPI.CD_SEMEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESUMEN_EDOS" because "SIVE_SEMANA_EPI" does not exist.'*/
         'no se puede actualizar SIVE_RESUMEN_EDOS
          porque no existe la clave en SIVE_SEMANA_EPI'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_ZONA_BASICA R/285 SIVE_RESUMEN_EDOS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ZONA_BASICA
    where
      /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
      :new.CD_NIVEL_1 = SIVE_ZONA_BASICA.CD_NIVEL_1 and
      :new.CD_NIVEL_2 = SIVE_ZONA_BASICA.CD_NIVEL_2 and
      :new.CD_ZBS = SIVE_ZONA_BASICA.CD_ZBS;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESUMEN_EDOS" because "SIVE_ZONA_BASICA" does not exist.'*/
         'no se puede actualizar SIVE_RESUMEN_EDOS
          porque no existe la clave en SIVE_ZONA_BASICA'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_MUNICIPIO R/277 SIVE_RESUMEN_EDOS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MUNICIPIO
    where
      /* %JoinFKPK(:%New,SIVE_MUNICIPIO," = "," and") */
      :new.CD_PROV = SIVE_MUNICIPIO.CD_PROV and
      :new.CD_MUN = SIVE_MUNICIPIO.CD_MUN;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESUMEN_EDOS" because "SIVE_MUNICIPIO" does not exist.'*/
         'no se puede actualizar SIVE_RESUMEN_EDOS
          porque no existe la clave en SIVE_MUNICIPIO'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_ENFEREDO R/272 SIVE_RESUMEN_EDOS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ENFEREDO
    where
      /* %JoinFKPK(:%New,SIVE_ENFEREDO," = "," and") */
      :new.CD_ENFCIE = SIVE_ENFEREDO.CD_ENFCIE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_RESUMEN_EDOS" because "SIVE_ENFEREDO" does not exist.'*/
         'no se puede actualizar SIVE_RESUMEN_EDOS
          porque no existe la clave en SIVE_ENFEREDO'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

create trigger SIVE_SEMANA_EPI_TRIG_A_U
  AFTER UPDATE
  on SIVE_SEMANA_EPI
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Tue Dec 28 10:19:05 1999 */
  /* SIVE_SEMANA_EPI R/45 SIVE_RES_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI
  then
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_RES_URGENCIAS.CD_SEMEPI = :old.CD_SEMEPI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_SEMANA_EPI" debido a que existe "SIVE_RES_URGENCIAS".'
      );
    end if;
  end if;

  /* ERwin Builtin Tue Dec 28 10:19:05 1999 */
  /* SIVE_SEMANA_EPI R/23 SIVE_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI
  then
    select count(*) into numrows
      from SIVE_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_URGENCIAS,:%Old," = "," and") */
        SIVE_URGENCIAS.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_URGENCIAS.CD_SEMEPI = :old.CD_SEMEPI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_SEMANA_EPI" debido a que existe "SIVE_URGENCIAS".'
      );
    end if;
  end if;

  /* ERwin Builtin Tue Dec 28 10:19:05 1999 */
  /* SIVE_ANO_EPI R/130 SIVE_SEMANA_EPI ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ANO_EPI
    where
      /* %JoinFKPK(:%New,SIVE_ANO_EPI," = "," and") */
      :new.CD_ANOEPI = SIVE_ANO_EPI.CD_ANOEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_SEMANA_EPI" debido a que no existe en "SIVE_ANO_EPI".'
    );
  end if;


-- ERwin Builtin Tue Dec 28 10:19:05 1999
end;
/


create trigger SIVE_SEMANA_EPI_TRIG_A_D
  AFTER DELETE
  on SIVE_SEMANA_EPI
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Tue Dec 28 10:19:05 1999 */
    /* SIVE_SEMANA_EPI R/45 SIVE_RES_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_RES_URGENCIAS.CD_SEMEPI = :old.CD_SEMEPI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_SEMANA_EPI" debido a que existe en "SIVE_RES_URGENCIAS".'
      );
    end if;

    /* ERwin Builtin Tue Dec 28 10:19:05 1999 */
    /* SIVE_SEMANA_EPI R/23 SIVE_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_URGENCIAS,:%Old," = "," and") */
        SIVE_URGENCIAS.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_URGENCIAS.CD_SEMEPI = :old.CD_SEMEPI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_SEMANA_EPI" debido a que existe en "SIVE_URGENCIAS".'
      );
    end if;


-- ERwin Builtin Tue Dec 28 10:19:05 1999
end;
/


create trigger SIVE_SEMANA_EPI_TRIG_A_I
  AFTER INSERT
  on SIVE_SEMANA_EPI
  
  for each row
declare numrows INTEGER;
begin

    /* ERwin Builtin Tue Dec 28 10:19:05 1999 */
    /* SIVE_ANO_EPI R/130 SIVE_SEMANA_EPI ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ANO_EPI
      where
        /* %JoinFKPK(:%New,SIVE_ANO_EPI," = "," and") */
        :new.CD_ANOEPI = SIVE_ANO_EPI.CD_ANOEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_SEMANA_EPI" because a que no existe en "SIVE_ANO_EPI".'
      );
    end if;


-- ERwin Builtin Tue Dec 28 10:19:05 1999
end;
/


CREATE OR REPLACE TRIGGER SIVE_SEMANA_EPI_RMC_TRIG_A_D_1 after DELETE on SIVE_SEMANA_EPI_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- DELETE trigger on SIVE_SEMANA_EPI_RMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_SEMANA_EPI_RMC  SIVE_ALARMA_RMC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALARMA_RMC
      where
        /*  %JoinFKPK(SIVE_ALARMA_RMC,:%Old," = "," and") */
        SIVE_ALARMA_RMC.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_ALARMA_RMC.CD_SEMEPI = :old.CD_SEMEPI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_SEMANA_EPI_RMC" because "SIVE_ALARMA_RMC" exists.'*/
       'no se puede borrar de SIVE_SEMANA_EPI_RMC
        porque hay registros en SIVE_ALARMA_RMC con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_SEMANA_EPI_RMC  SIVE_NOTIF_RMC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIF_RMC
      where
        /*  %JoinFKPK(SIVE_NOTIF_RMC,:%Old," = "," and") */
        SIVE_NOTIF_RMC.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_NOTIF_RMC.CD_SEMEPI = :old.CD_SEMEPI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_SEMANA_EPI_RMC" because "SIVE_NOTIF_RMC" exists.'*/
       'no se puede borrar de SIVE_SEMANA_EPI_RMC
        porque hay registros en SIVE_NOTIF_RMC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_SEMANA_EPI_RMC_TRIG_A_I_1 after INSERT on SIVE_SEMANA_EPI_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- INSERT trigger on SIVE_SEMANA_EPI_RMC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_ANO_EPI_RMC  SIVE_SEMANA_EPI_RMC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ANO_EPI_RMC
      where
        /* %JoinFKPK(:%New,SIVE_ANO_EPI_RMC," = "," and") */
        :new.CD_ANOEPI = SIVE_ANO_EPI_RMC.CD_ANOEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_SEMANA_EPI_RMC" because "SIVE_ANO_EPI_RMC" does not exist.'*/
          'no se puede grabar en SIVE_SEMANA_EPI_RMC
           porque no existe la clave en SIVE_ANO_EPI_RMC'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_SEMANA_EPI_RMC_TRIG_A_U_1 after UPDATE on SIVE_SEMANA_EPI_RMC for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_SEMANA_EPI_RMC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_SEMANA_EPI_RMC  SIVE_ALARMA_RMC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI
  then
    select count(*) into numrows
      from SIVE_ALARMA_RMC
      where
        /*  %JoinFKPK(SIVE_ALARMA_RMC,:%Old," = "," and") */
        SIVE_ALARMA_RMC.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_ALARMA_RMC.CD_SEMEPI = :old.CD_SEMEPI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_SEMANA_EPI_RMC" because "SIVE_ALARMA_RMC" exists.'*/
       'no se puede modificar SIVE_SEMANA_EPI_RMC
        porque hay registros en SIVE_ALARMA_RMC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_SEMANA_EPI_RMC  SIVE_NOTIF_RMC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_ANOEPI <> :new.CD_ANOEPI or 
    :old.CD_SEMEPI <> :new.CD_SEMEPI
  then
    select count(*) into numrows
      from SIVE_NOTIF_RMC
      where
        /*  %JoinFKPK(SIVE_NOTIF_RMC,:%Old," = "," and") */
        SIVE_NOTIF_RMC.CD_ANOEPI = :old.CD_ANOEPI and
        SIVE_NOTIF_RMC.CD_SEMEPI = :old.CD_SEMEPI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_SEMANA_EPI_RMC" because "SIVE_NOTIF_RMC" exists.'*/
       'no se puede modificar SIVE_SEMANA_EPI_RMC
        porque hay registros en SIVE_NOTIF_RMC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_ANO_EPI_RMC  SIVE_SEMANA_EPI_RMC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ANO_EPI_RMC
    where
      /* %JoinFKPK(:%New,SIVE_ANO_EPI_RMC," = "," and") */
      :new.CD_ANOEPI = SIVE_ANO_EPI_RMC.CD_ANOEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_SEMANA_EPI_RMC" because "SIVE_ANO_EPI_RMC" does not exist.'*/
         'no se puede actualizar SIVE_SEMANA_EPI_RMC
          porque no existe la clave en SIVE_ANO_EPI_RMC'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

create trigger SIVE_SEXO_TRIG_A_U
  AFTER UPDATE
  on SIVE_SEXO
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Tue Dec 28 10:18:31 1999 */
  /* SIVE_SEXO R/51 SIVE_MOV_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_SEXO <> :new.CD_SEXO
  then
    select count(*) into numrows
      from SIVE_MOV_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_MOV_URGENCIAS,:%Old," = "," and") */
        SIVE_MOV_URGENCIAS.CD_SEXO = :old.CD_SEXO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_SEXO" debido a que existe "SIVE_MOV_URGENCIAS".'
      );
    end if;
  end if;

  /* ERwin Builtin Tue Dec 28 10:18:31 1999 */
  /* SIVE_SEXO R/50 SIVE_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_SEXO <> :new.CD_SEXO
  then
    select count(*) into numrows
      from SIVE_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_URGENCIAS,:%Old," = "," and") */
        SIVE_URGENCIAS.CD_SEXO = :old.CD_SEXO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_SEXO" debido a que existe "SIVE_URGENCIAS".'
      );
    end if;
  end if;

  /* ERwin Builtin Tue Dec 28 10:18:31 1999 */
  /* SIVE_SEXO R/49 SIVE_RES_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_SEXO <> :new.CD_SEXO
  then
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_SEXO = :old.CD_SEXO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_SEXO" debido a que existe "SIVE_RES_URGENCIAS".'
      );
    end if;
  end if;


-- ERwin Builtin Tue Dec 28 10:18:31 1999
end;
/


create trigger SIVE_SEXO_TRIG_A_D
  AFTER DELETE
  on SIVE_SEXO
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Tue Dec 28 10:18:31 1999 */
    /* SIVE_SEXO R/51 SIVE_MOV_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MOV_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_MOV_URGENCIAS,:%Old," = "," and") */
        SIVE_MOV_URGENCIAS.CD_SEXO = :old.CD_SEXO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_SEXO" debido a que existe en "SIVE_MOV_URGENCIAS".'
      );
    end if;

    /* ERwin Builtin Tue Dec 28 10:18:31 1999 */
    /* SIVE_SEXO R/50 SIVE_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_URGENCIAS,:%Old," = "," and") */
        SIVE_URGENCIAS.CD_SEXO = :old.CD_SEXO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_SEXO" debido a que existe en "SIVE_URGENCIAS".'
      );
    end if;

    /* ERwin Builtin Tue Dec 28 10:18:31 1999 */
    /* SIVE_SEXO R/49 SIVE_RES_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RES_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_RES_URGENCIAS,:%Old," = "," and") */
        SIVE_RES_URGENCIAS.CD_SEXO = :old.CD_SEXO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_SEXO" debido a que existe en "SIVE_RES_URGENCIAS".'
      );
    end if;


-- ERwin Builtin Tue Dec 28 10:18:31 1999
end;
/


CREATE OR REPLACE TRIGGER SIVE_SINTOMAS_TRIG_A_D_1 after DELETE on SIVE_SINTOMAS for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- DELETE trigger on SIVE_SINTOMAS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
    /* SIVE_SINTOMAS  SIVE_SINTOMAS_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_SINTOMAS_BROTE
      where
        /*  %JoinFKPK(SIVE_SINTOMAS_BROTE,:%Old," = "," and") */
        SIVE_SINTOMAS_BROTE.CD_SINTOMA = :old.CD_SINTOMA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_SINTOMAS" because "SIVE_SINTOMAS_BROTE" exists.'*/
       'no se puede borrar de SIVE_SINTOMAS
        porque hay registros en SIVE_SINTOMAS_BROTE con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_SINTOMAS_TRIG_A_U_1 after UPDATE on SIVE_SINTOMAS for each row
-- ERwin Builtin Wed Oct 31 16:28:51 2001
-- UPDATE trigger on SIVE_SINTOMAS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:51 2001 */
  /* SIVE_SINTOMAS  SIVE_SINTOMAS_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_SINTOMA <> :new.CD_SINTOMA
  then
    select count(*) into numrows
      from SIVE_SINTOMAS_BROTE
      where
        /*  %JoinFKPK(SIVE_SINTOMAS_BROTE,:%Old," = "," and") */
        SIVE_SINTOMAS_BROTE.CD_SINTOMA = :old.CD_SINTOMA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_SINTOMAS" because "SIVE_SINTOMAS_BROTE" exists.'*/
       'no se puede modificar SIVE_SINTOMAS
        porque hay registros en SIVE_SINTOMAS_BROTE con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:51 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_SINTOMAS_BROTE_TRIG_A_I_1 after INSERT on SIVE_SINTOMAS_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- INSERT trigger on SIVE_SINTOMAS_BROTE 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_BROTES  SIVE_SINTOMAS_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
        :new.CD_ANO = SIVE_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_SINTOMAS_BROTE" because "SIVE_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_SINTOMAS_BROTE
           porque no existe la clave en SIVE_BROTES'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_SINTOMAS  SIVE_SINTOMAS_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SINTOMAS
      where
        /* %JoinFKPK(:%New,SIVE_SINTOMAS," = "," and") */
        :new.CD_SINTOMA = SIVE_SINTOMAS.CD_SINTOMA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_SINTOMAS_BROTE" because "SIVE_SINTOMAS" does not exist.'*/
          'no se puede grabar en SIVE_SINTOMAS_BROTE
           porque no existe la clave en SIVE_SINTOMAS'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_SINTOMAS_BROTE_TRIG_A_U_1 after UPDATE on SIVE_SINTOMAS_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_SINTOMAS_BROTE 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_BROTES  SIVE_SINTOMAS_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
      :new.CD_ANO = SIVE_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_SINTOMAS_BROTE" because "SIVE_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_SINTOMAS_BROTE
          porque no existe la clave en SIVE_BROTES'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_SINTOMAS  SIVE_SINTOMAS_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SINTOMAS
    where
      /* %JoinFKPK(:%New,SIVE_SINTOMAS," = "," and") */
      :new.CD_SINTOMA = SIVE_SINTOMAS.CD_SINTOMA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_SINTOMAS_BROTE" because "SIVE_SINTOMAS" does not exist.'*/
         'no se puede actualizar SIVE_SINTOMAS_BROTE
          porque no existe la clave en SIVE_SINTOMAS'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_T_DOC_TRIG_A_D_1 after DELETE on SIVE_T_DOC for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_T_DOC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_T_DOC  SIVE_ENFERMO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ENFERMO
      where
        /*  %JoinFKPK(SIVE_ENFERMO,:%Old," = "," and") */
        SIVE_ENFERMO.CD_TDOC = :old.CD_TDOC;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_T_DOC" because "SIVE_ENFERMO" exists.'*/
       'no se puede borrar de SIVE_T_DOC
        porque hay registros en SIVE_ENFERMO con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_T_DOC_TRIG_A_U_1 after UPDATE on SIVE_T_DOC for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_T_DOC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_T_DOC  SIVE_ENFERMO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TDOC <> :new.CD_TDOC
  then
    select count(*) into numrows
      from SIVE_ENFERMO
      where
        /*  %JoinFKPK(SIVE_ENFERMO,:%Old," = "," and") */
        SIVE_ENFERMO.CD_TDOC = :old.CD_TDOC;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_T_DOC" because "SIVE_ENFERMO" exists.'*/
       'no se puede modificar SIVE_T_DOC
        porque hay registros en SIVE_ENFERMO con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_T_IMPLICACION_TRIG_A_D_1 after DELETE on SIVE_T_IMPLICACION for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_T_IMPLICACION 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_T_IMPLICACION R/316 SIVE_ALI_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_TIMPLICACION = :old.CD_TIMPLICACION;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_T_IMPLICACION" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede borrar de SIVE_T_IMPLICACION
        porque hay registros en SIVE_ALI_BROTE con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_T_IMPLICACION_TRIG_A_U_1 after UPDATE on SIVE_T_IMPLICACION for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_T_IMPLICACION 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_T_IMPLICACION R/316 SIVE_ALI_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TIMPLICACION <> :new.CD_TIMPLICACION
  then
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_TIMPLICACION = :old.CD_TIMPLICACION;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_T_IMPLICACION" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede modificar SIVE_T_IMPLICACION
        porque hay registros en SIVE_ALI_BROTE con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_T_NOTIFICADOR_TRIG_A_D_1 after DELETE on SIVE_T_NOTIFICADOR for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_T_NOTIFICADOR 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_T_NOTIFICADOR  SIVE_ALERTA_ADIC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_ADIC
      where
        /*  %JoinFKPK(SIVE_ALERTA_ADIC,:%Old," = "," and") */
        SIVE_ALERTA_ADIC.CD_TNOTIF = :old.CD_TNOTIF;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_T_NOTIFICADOR" because "SIVE_ALERTA_ADIC" exists.'*/
       'no se puede borrar de SIVE_T_NOTIFICADOR
        porque hay registros en SIVE_ALERTA_ADIC con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_T_NOTIFICADOR  SIVE_BROTES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /*  %JoinFKPK(SIVE_BROTES,:%Old," = "," and") */
        SIVE_BROTES.CD_TNOTIF = :old.CD_TNOTIF;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_T_NOTIFICADOR" because "SIVE_BROTES" exists.'*/
       'no se puede borrar de SIVE_T_NOTIFICADOR
        porque hay registros en SIVE_BROTES con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_T_NOTIFICADOR_TRIG_A_U_1 after UPDATE on SIVE_T_NOTIFICADOR for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_T_NOTIFICADOR 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_T_NOTIFICADOR  SIVE_ALERTA_ADIC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TNOTIF <> :new.CD_TNOTIF
  then
    select count(*) into numrows
      from SIVE_ALERTA_ADIC
      where
        /*  %JoinFKPK(SIVE_ALERTA_ADIC,:%Old," = "," and") */
        SIVE_ALERTA_ADIC.CD_TNOTIF = :old.CD_TNOTIF;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_T_NOTIFICADOR" because "SIVE_ALERTA_ADIC" exists.'*/
       'no se puede modificar SIVE_T_NOTIFICADOR
        porque hay registros en SIVE_ALERTA_ADIC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_T_NOTIFICADOR  SIVE_BROTES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TNOTIF <> :new.CD_TNOTIF
  then
    select count(*) into numrows
      from SIVE_BROTES
      where
        /*  %JoinFKPK(SIVE_BROTES,:%Old," = "," and") */
        SIVE_BROTES.CD_TNOTIF = :old.CD_TNOTIF;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_T_NOTIFICADOR" because "SIVE_BROTES" exists.'*/
       'no se puede modificar SIVE_T_NOTIFICADOR
        porque hay registros en SIVE_BROTES con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_T_SIT_ALERTA_TRIG_A_D_1 after DELETE on SIVE_T_SIT_ALERTA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_T_SIT_ALERTA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_T_SIT_ALERTA  SIVE_ALERTA_BROTES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALERTA_BROTES
      where
        /*  %JoinFKPK(SIVE_ALERTA_BROTES,:%Old," = "," and") */
        SIVE_ALERTA_BROTES.CD_SITALERBRO = :old.CD_SITALERBRO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_T_SIT_ALERTA" because "SIVE_ALERTA_BROTES" exists.'*/
       'no se puede borrar de SIVE_T_SIT_ALERTA
        porque hay registros en SIVE_ALERTA_BROTES con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_T_SIT_ALERTA_TRIG_A_U_1 after UPDATE on SIVE_T_SIT_ALERTA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_T_SIT_ALERTA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_T_SIT_ALERTA  SIVE_ALERTA_BROTES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_SITALERBRO <> :new.CD_SITALERBRO
  then
    select count(*) into numrows
      from SIVE_ALERTA_BROTES
      where
        /*  %JoinFKPK(SIVE_ALERTA_BROTES,:%Old," = "," and") */
        SIVE_ALERTA_BROTES.CD_SITALERBRO = :old.CD_SITALERBRO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_T_SIT_ALERTA" because "SIVE_ALERTA_BROTES" exists.'*/
       'no se puede modificar SIVE_T_SIT_ALERTA
        porque hay registros en SIVE_ALERTA_BROTES con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_T_VIGILANCIA_TRIG_A_D_1 after DELETE on SIVE_T_VIGILANCIA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_T_VIGILANCIA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_T_VIGILANCIA R_131 SIVE_ENFEREDO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ENFEREDO
      where
        /*  %JoinFKPK(SIVE_ENFEREDO,:%Old," = "," and") */
        SIVE_ENFEREDO.CD_TVIGI = :old.CD_TVIGI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_T_VIGILANCIA" because "SIVE_ENFEREDO" exists.'*/
       'no se puede borrar de SIVE_T_VIGILANCIA
        porque hay registros en SIVE_ENFEREDO con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_T_VIGILANCIA_TRIG_A_U_1 after UPDATE on SIVE_T_VIGILANCIA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_T_VIGILANCIA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_T_VIGILANCIA R_131 SIVE_ENFEREDO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TVIGI <> :new.CD_TVIGI
  then
    select count(*) into numrows
      from SIVE_ENFEREDO
      where
        /*  %JoinFKPK(SIVE_ENFEREDO,:%Old," = "," and") */
        SIVE_ENFEREDO.CD_TVIGI = :old.CD_TVIGI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_T_VIGILANCIA" because "SIVE_ENFEREDO" exists.'*/
       'no se puede modificar SIVE_T_VIGILANCIA
        porque hay registros en SIVE_ENFEREDO con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TALIMENTO_TRIG_A_D_1 after DELETE on SIVE_TALIMENTO for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_TALIMENTO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_TALIMENTO R/317 SIVE_ALI_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_TALIMENTO = :old.CD_TALIMENTO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_TALIMENTO" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede borrar de SIVE_TALIMENTO
        porque hay registros en SIVE_ALI_BROTE con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TALIMENTO_TRIG_A_U_1 after UPDATE on SIVE_TALIMENTO for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_TALIMENTO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_TALIMENTO R/317 SIVE_ALI_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TALIMENTO <> :new.CD_TALIMENTO
  then
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_TALIMENTO = :old.CD_TALIMENTO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_TALIMENTO" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede modificar SIVE_TALIMENTO
        porque hay registros en SIVE_ALI_BROTE con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TATAQ_ENFVAC_TRIG_A_I_1 after INSERT on SIVE_TATAQ_ENFVAC for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- INSERT trigger on SIVE_TATAQ_ENFVAC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_BROTES  SIVE_TATAQ_ENFVAC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
        :new.CD_ANO = SIVE_BROTES.CD_ANO and
        :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_TATAQ_ENFVAC" because "SIVE_BROTES" does not exist.'*/
          'no se puede grabar en SIVE_TATAQ_ENFVAC
           porque no existe la clave en SIVE_BROTES'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_DISTR_GEDAD  SIVE_TATAQ_ENFVAC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_DISTR_GEDAD
      where
        /* %JoinFKPK(:%New,SIVE_DISTR_GEDAD," = "," and") */
        :new.CD_GEDAD = SIVE_DISTR_GEDAD.CD_GEDAD and
        :new.DIST_EDAD_I = SIVE_DISTR_GEDAD.DIST_EDAD_I and
        :new.DIST_EDAD_F = SIVE_DISTR_GEDAD.DIST_EDAD_F;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_GEDAD is not null and
      :new.DIST_EDAD_I is not null and
      :new.DIST_EDAD_F is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_TATAQ_ENFVAC" because "SIVE_DISTR_GEDAD" does not exist.'*/
          'no se puede grabar en SIVE_TATAQ_ENFVAC
           porque no existe la clave en SIVE_DISTR_GEDAD'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TATAQ_ENFVAC_TRIG_A_U_1 after UPDATE on SIVE_TATAQ_ENFVAC for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_TATAQ_ENFVAC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_BROTES  SIVE_TATAQ_ENFVAC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_BROTES
    where
      /* %JoinFKPK(:%New,SIVE_BROTES," = "," and") */
      :new.CD_ANO = SIVE_BROTES.CD_ANO and
      :new.NM_ALERBRO = SIVE_BROTES.NM_ALERBRO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_TATAQ_ENFVAC" because "SIVE_BROTES" does not exist.'*/
         'no se puede actualizar SIVE_TATAQ_ENFVAC
          porque no existe la clave en SIVE_BROTES'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_DISTR_GEDAD  SIVE_TATAQ_ENFVAC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_DISTR_GEDAD
    where
      /* %JoinFKPK(:%New,SIVE_DISTR_GEDAD," = "," and") */
      :new.CD_GEDAD = SIVE_DISTR_GEDAD.CD_GEDAD and
      :new.DIST_EDAD_I = SIVE_DISTR_GEDAD.DIST_EDAD_I and
      :new.DIST_EDAD_F = SIVE_DISTR_GEDAD.DIST_EDAD_F;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_GEDAD is not null and
    :new.DIST_EDAD_I is not null and
    :new.DIST_EDAD_F is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_TATAQ_ENFVAC" because "SIVE_DISTR_GEDAD" does not exist.'*/
         'no se puede actualizar SIVE_TATAQ_ENFVAC
          porque no existe la clave en SIVE_DISTR_GEDAD'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TIPO_BROTE_TRIG_A_D_1 after DELETE on SIVE_TIPO_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_TIPO_BROTE 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_TIPO_BROTE  SIVE_BROTES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /*  %JoinFKPK(SIVE_BROTES,:%Old," = "," and") */
        SIVE_BROTES.CD_GRUPO = :old.CD_GRUPO and
        SIVE_BROTES.CD_TBROTE = :old.CD_TBROTE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_TIPO_BROTE" because "SIVE_BROTES" exists.'*/
       'no se puede borrar de SIVE_TIPO_BROTE
        porque hay registros en SIVE_BROTES con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TIPO_BROTE_TRIG_A_I_1 after INSERT on SIVE_TIPO_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- INSERT trigger on SIVE_TIPO_BROTE 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_GRUPO_BROTE  SIVE_TIPO_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_GRUPO_BROTE
      where
        /* %JoinFKPK(:%New,SIVE_GRUPO_BROTE," = "," and") */
        :new.CD_GRUPO = SIVE_GRUPO_BROTE.CD_GRUPO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_TIPO_BROTE" because "SIVE_GRUPO_BROTE" does not exist.'*/
          'no se puede grabar en SIVE_TIPO_BROTE
           porque no existe la clave en SIVE_GRUPO_BROTE'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TIPO_BROTE_TRIG_A_U_1 after UPDATE on SIVE_TIPO_BROTE for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_TIPO_BROTE 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_TIPO_BROTE  SIVE_BROTES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_GRUPO <> :new.CD_GRUPO or 
    :old.CD_TBROTE <> :new.CD_TBROTE
  then
    select count(*) into numrows
      from SIVE_BROTES
      where
        /*  %JoinFKPK(SIVE_BROTES,:%Old," = "," and") */
        SIVE_BROTES.CD_GRUPO = :old.CD_GRUPO and
        SIVE_BROTES.CD_TBROTE = :old.CD_TBROTE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_TIPO_BROTE" because "SIVE_BROTES" exists.'*/
       'no se puede modificar SIVE_TIPO_BROTE
        porque hay registros en SIVE_BROTES con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_GRUPO_BROTE  SIVE_TIPO_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_GRUPO_BROTE
    where
      /* %JoinFKPK(:%New,SIVE_GRUPO_BROTE," = "," and") */
      :new.CD_GRUPO = SIVE_GRUPO_BROTE.CD_GRUPO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_TIPO_BROTE" because "SIVE_GRUPO_BROTE" does not exist.'*/
         'no se puede actualizar SIVE_TIPO_BROTE
          porque no existe la clave en SIVE_GRUPO_BROTE'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TIPO_RANGO_TRIG_A_D_1 after DELETE on SIVE_TIPO_RANGO for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_TIPO_RANGO 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_TIPO_RANGO  SIVE_RANGO_ANO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RANGO_ANO
      where
        /*  %JoinFKPK(SIVE_RANGO_ANO,:%Old," = "," and") */
        SIVE_RANGO_ANO.CD_TIPORANGO = :old.CD_TIPORANGO;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_TIPO_RANGO" because "SIVE_RANGO_ANO" exists.'*/
       'no se puede borrar de SIVE_TIPO_RANGO
        porque hay registros en SIVE_RANGO_ANO con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TIPO_RANGO_TRIG_A_U_1 after UPDATE on SIVE_TIPO_RANGO for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_TIPO_RANGO 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_TIPO_RANGO  SIVE_RANGO_ANO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TIPORANGO <> :new.CD_TIPORANGO
  then
    select count(*) into numrows
      from SIVE_RANGO_ANO
      where
        /*  %JoinFKPK(SIVE_RANGO_ANO,:%Old," = "," and") */
        SIVE_RANGO_ANO.CD_TIPORANGO = :old.CD_TIPORANGO;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_TIPO_RANGO" because "SIVE_RANGO_ANO" exists.'*/
       'no se puede modificar SIVE_TIPO_RANGO
        porque hay registros en SIVE_RANGO_ANO con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TIPO_TECNICALA_TRIG_A_D_1 after DELETE on SIVE_TIPO_TECNICALAB for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_TIPO_TECNICALAB 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_TIPO_TECNICALAB  SIVE_RESLAB ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESLAB
      where
        /*  %JoinFKPK(SIVE_RESLAB,:%Old," = "," and") */
        SIVE_RESLAB.CD_TTECLAB = :old.CD_TTECLAB;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_TIPO_TECNICALAB" because "SIVE_RESLAB" exists.'*/
       'no se puede borrar de SIVE_TIPO_TECNICALAB
        porque hay registros en SIVE_RESLAB con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TIPO_TECNICALA_TRIG_A_U_1 after UPDATE on SIVE_TIPO_TECNICALAB for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_TIPO_TECNICALAB 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_TIPO_TECNICALAB  SIVE_RESLAB ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TTECLAB <> :new.CD_TTECLAB
  then
    select count(*) into numrows
      from SIVE_RESLAB
      where
        /*  %JoinFKPK(SIVE_RESLAB,:%Old," = "," and") */
        SIVE_RESLAB.CD_TTECLAB = :old.CD_TTECLAB;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_TIPO_TECNICALAB" because "SIVE_RESLAB" exists.'*/
       'no se puede modificar SIVE_TIPO_TECNICALAB
        porque hay registros en SIVE_RESLAB con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TIPOCOL_TRIG_A_D_1 after DELETE on SIVE_TIPOCOL for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_TIPOCOL 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_TIPOCOL  SIVE_BROTES ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_BROTES
      where
        /*  %JoinFKPK(SIVE_BROTES,:%Old," = "," and") */
        SIVE_BROTES.CD_TIPOCOL = :old.CD_TIPOCOL;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_TIPOCOL" because "SIVE_BROTES" exists.'*/
       'no se puede borrar de SIVE_TIPOCOL
        porque hay registros en SIVE_BROTES con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TIPOCOL_TRIG_A_U_1 after UPDATE on SIVE_TIPOCOL for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_TIPOCOL 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_TIPOCOL  SIVE_BROTES ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TIPOCOL <> :new.CD_TIPOCOL
  then
    select count(*) into numrows
      from SIVE_BROTES
      where
        /*  %JoinFKPK(SIVE_BROTES,:%Old," = "," and") */
        SIVE_BROTES.CD_TIPOCOL = :old.CD_TIPOCOL;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_TIPOCOL" because "SIVE_BROTES" exists.'*/
       'no se puede modificar SIVE_TIPOCOL
        porque hay registros en SIVE_BROTES con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TLINEA_TRIG_A_D_1 after DELETE on SIVE_TLINEA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_TLINEA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_TLINEA  SIVE_LINEASM ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_LINEASM
      where
        /*  %JoinFKPK(SIVE_LINEASM,:%Old," = "," and") */
        SIVE_LINEASM.CD_TLINEA = :old.CD_TLINEA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_TLINEA" because "SIVE_LINEASM" exists.'*/
       'no se puede borrar de SIVE_TLINEA
        porque hay registros en SIVE_LINEASM con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TLINEA_TRIG_A_U_1 after UPDATE on SIVE_TLINEA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_TLINEA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_TLINEA  SIVE_LINEASM ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TLINEA <> :new.CD_TLINEA
  then
    select count(*) into numrows
      from SIVE_LINEASM
      where
        /*  %JoinFKPK(SIVE_LINEASM,:%Old," = "," and") */
        SIVE_LINEASM.CD_TLINEA = :old.CD_TLINEA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_TLINEA" because "SIVE_LINEASM" exists.'*/
       'no se puede modificar SIVE_TLINEA
        porque hay registros en SIVE_LINEASM con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TMICOBACTERIA_TRIG_A_D_1 after DELETE on SIVE_TMICOBACTERIA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_TMICOBACTERIA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_TMICOBACTERIA  SIVE_RESLAB ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESLAB
      where
        /*  %JoinFKPK(SIVE_RESLAB,:%Old," = "," and") */
        SIVE_RESLAB.CD_TMICOB = :old.CD_TMICOB;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_TMICOBACTERIA" because "SIVE_RESLAB" exists.'*/
       'no se puede borrar de SIVE_TMICOBACTERIA
        porque hay registros en SIVE_RESLAB con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TMICOBACTERIA_TRIG_A_U_1 after UPDATE on SIVE_TMICOBACTERIA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_TMICOBACTERIA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_TMICOBACTERIA  SIVE_RESLAB ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TMICOB <> :new.CD_TMICOB
  then
    select count(*) into numrows
      from SIVE_RESLAB
      where
        /*  %JoinFKPK(SIVE_RESLAB,:%Old," = "," and") */
        SIVE_RESLAB.CD_TMICOB = :old.CD_TMICOB;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_TMICOBACTERIA" because "SIVE_RESLAB" exists.'*/
       'no se puede modificar SIVE_TMICOBACTERIA
        porque hay registros en SIVE_RESLAB con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TPCENTINELA_TRIG_A_D_1 after DELETE on SIVE_TPCENTINELA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_TPCENTINELA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_TPCENTINELA  SIVE_PCENTINELA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_PCENTINELA
      where
        /*  %JoinFKPK(SIVE_PCENTINELA,:%Old," = "," and") */
        SIVE_PCENTINELA.CD_TPCENTI = :old.CD_TPCENTI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_TPCENTINELA" because "SIVE_PCENTINELA" exists.'*/
       'no se puede borrar de SIVE_TPCENTINELA
        porque hay registros en SIVE_PCENTINELA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TPCENTINELA_TRIG_A_U_1 after UPDATE on SIVE_TPCENTINELA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_TPCENTINELA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_TPCENTINELA  SIVE_PCENTINELA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TPCENTI <> :new.CD_TPCENTI
  then
    select count(*) into numrows
      from SIVE_PCENTINELA
      where
        /*  %JoinFKPK(SIVE_PCENTINELA,:%Old," = "," and") */
        SIVE_PCENTINELA.CD_TPCENTI = :old.CD_TPCENTI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_TPCENTINELA" because "SIVE_PCENTINELA" exists.'*/
       'no se puede modificar SIVE_TPCENTINELA
        porque hay registros en SIVE_PCENTINELA con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TPREGUNTA_TRIG_A_D_1 after DELETE on SIVE_TPREGUNTA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_TPREGUNTA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_TPREGUNTA  SIVE_PREGUNTA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_PREGUNTA
      where
        /*  %JoinFKPK(SIVE_PREGUNTA,:%Old," = "," and") */
        SIVE_PREGUNTA.CD_TPREG = :old.CD_TPREG;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_TPREGUNTA" because "SIVE_PREGUNTA" exists.'*/
       'no se puede borrar de SIVE_TPREGUNTA
        porque hay registros en SIVE_PREGUNTA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TPREGUNTA_TRIG_A_U_1 after UPDATE on SIVE_TPREGUNTA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_TPREGUNTA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_TPREGUNTA  SIVE_PREGUNTA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TPREG <> :new.CD_TPREG
  then
    select count(*) into numrows
      from SIVE_PREGUNTA
      where
        /*  %JoinFKPK(SIVE_PREGUNTA,:%Old," = "," and") */
        SIVE_PREGUNTA.CD_TPREG = :old.CD_TPREG;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_TPREGUNTA" because "SIVE_PREGUNTA" exists.'*/
       'no se puede modificar SIVE_TPREGUNTA
        porque hay registros en SIVE_PREGUNTA con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TRAT_PREVI_ALI_TRIG_A_D_1  after DELETE on SIVE_TRAT_PREVIO_ALI for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_TRAT_PREVIO_ALI 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_TRAT_PREVIO_ALI R/318 SIVE_ALI_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_TRATPREVALI = :old.CD_TRATPREVALI;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_TRAT_PREVIO_ALI" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede borrar de SIVE_TRAT_PREVIO_ALI
        porque hay registros en SIVE_ALI_BROTE con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TRAT_PREVI_ALI_TRIG_A_U_1 after UPDATE on SIVE_TRAT_PREVIO_ALI for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_TRAT_PREVIO_ALI 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_TRAT_PREVIO_ALI R/318 SIVE_ALI_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TRATPREVALI <> :new.CD_TRATPREVALI
  then
    select count(*) into numrows
      from SIVE_ALI_BROTE
      where
        /*  %JoinFKPK(SIVE_ALI_BROTE,:%Old," = "," and") */
        SIVE_ALI_BROTE.CD_TRATPREVALI = :old.CD_TRATPREVALI;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_TRAT_PREVIO_ALI" because "SIVE_ALI_BROTE" exists.'*/
       'no se puede modificar SIVE_TRAT_PREVIO_ALI
        porque hay registros en SIVE_ALI_BROTE con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TRATAMIENTOS_TRIG_A_I_1 after INSERT on SIVE_TRATAMIENTOS for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- INSERT trigger on SIVE_TRATAMIENTOS 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_MOTIVO_TRAT  SIVE_TRATAMIENTOS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MOTIVO_TRAT
      where
        /* %JoinFKPK(:%New,SIVE_MOTIVO_TRAT," = "," and") */
        :new.CD_MOTRATINI = SIVE_MOTIVO_TRAT.CD_MOTRAT;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_MOTRATINI is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_TRATAMIENTOS" because "SIVE_MOTIVO_TRAT" does not exist.'*/
          'no se puede grabar en SIVE_TRATAMIENTOS
           porque no existe la clave en SIVE_MOTIVO_TRAT'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_MOTIVO_TRAT  SIVE_TRATAMIENTOS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MOTIVO_TRAT
      where
        /* %JoinFKPK(:%New,SIVE_MOTIVO_TRAT," = "," and") */
        :new.CD_MOTRATFIN = SIVE_MOTIVO_TRAT.CD_MOTRAT;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_MOTRATFIN is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_TRATAMIENTOS" because "SIVE_MOTIVO_TRAT" does not exist.'*/
          'no se puede grabar en SIVE_TRATAMIENTOS
           porque no existe la clave en SIVE_MOTIVO_TRAT'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_NOTIF_EDOI  SIVE_TRATAMIENTOS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NOTIF_EDOI
      where
        /* %JoinFKPK(:%New,SIVE_NOTIF_EDOI," = "," and") */
        :new.NM_EDO = SIVE_NOTIF_EDOI.NM_EDO and
        :new.CD_E_NOTIF = SIVE_NOTIF_EDOI.CD_E_NOTIF and
        :new.CD_ANOEPI = SIVE_NOTIF_EDOI.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_NOTIF_EDOI.CD_SEMEPI and
        :new.FC_RECEP = SIVE_NOTIF_EDOI.FC_RECEP and
        :new.FC_FECNOTIF = SIVE_NOTIF_EDOI.FC_FECNOTIF and
        :new.CD_FUENTE = SIVE_NOTIF_EDOI.CD_FUENTE;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_TRATAMIENTOS" because "SIVE_NOTIF_EDOI" does not exist.'*/
          'no se puede grabar en SIVE_TRATAMIENTOS
           porque no existe la clave en SIVE_NOTIF_EDOI'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TRATAMIENTOS_TRIG_A_U_1 after UPDATE on SIVE_TRATAMIENTOS for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_TRATAMIENTOS 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_MOTIVO_TRAT  SIVE_TRATAMIENTOS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MOTIVO_TRAT
    where
      /* %JoinFKPK(:%New,SIVE_MOTIVO_TRAT," = "," and") */
      :new.CD_MOTRATINI = SIVE_MOTIVO_TRAT.CD_MOTRAT;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_MOTRATINI is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_TRATAMIENTOS" because "SIVE_MOTIVO_TRAT" does not exist.'*/
         'no se puede actualizar SIVE_TRATAMIENTOS
          porque no existe la clave en SIVE_MOTIVO_TRAT'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_MOTIVO_TRAT  SIVE_TRATAMIENTOS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MOTIVO_TRAT
    where
      /* %JoinFKPK(:%New,SIVE_MOTIVO_TRAT," = "," and") */
      :new.CD_MOTRATFIN = SIVE_MOTIVO_TRAT.CD_MOTRAT;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_MOTRATFIN is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_TRATAMIENTOS" because "SIVE_MOTIVO_TRAT" does not exist.'*/
         'no se puede actualizar SIVE_TRATAMIENTOS
          porque no existe la clave en SIVE_MOTIVO_TRAT'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_NOTIF_EDOI  SIVE_TRATAMIENTOS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NOTIF_EDOI
    where
      /* %JoinFKPK(:%New,SIVE_NOTIF_EDOI," = "," and") */
      :new.NM_EDO = SIVE_NOTIF_EDOI.NM_EDO and
      :new.CD_E_NOTIF = SIVE_NOTIF_EDOI.CD_E_NOTIF and
      :new.CD_ANOEPI = SIVE_NOTIF_EDOI.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_NOTIF_EDOI.CD_SEMEPI and
      :new.FC_RECEP = SIVE_NOTIF_EDOI.FC_RECEP and
      :new.FC_FECNOTIF = SIVE_NOTIF_EDOI.FC_FECNOTIF and
      :new.CD_FUENTE = SIVE_NOTIF_EDOI.CD_FUENTE;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_TRATAMIENTOS" because "SIVE_NOTIF_EDOI" does not exist.'*/
         'no se puede actualizar SIVE_TRATAMIENTOS
          porque no existe la clave en SIVE_NOTIF_EDOI'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TSIVE_TRIG_A_D_1 after DELETE on SIVE_TSIVE for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_TSIVE 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_TSIVE  SIVE_MODELO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MODELO
      where
        /*  %JoinFKPK(SIVE_MODELO,:%Old," = "," and") */
        SIVE_MODELO.CD_TSIVE = :old.CD_TSIVE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_TSIVE" because "SIVE_MODELO" exists.'*/
       'no se puede borrar de SIVE_TSIVE
        porque hay registros en SIVE_MODELO con su clave'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_TSIVE  SIVE_PREGUNTA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_PREGUNTA
      where
        /*  %JoinFKPK(SIVE_PREGUNTA,:%Old," = "," and") */
        SIVE_PREGUNTA.CD_TSIVE = :old.CD_TSIVE;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_TSIVE" because "SIVE_PREGUNTA" exists.'*/
       'no se puede borrar de SIVE_TSIVE
        porque hay registros en SIVE_PREGUNTA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_TSIVE_TRIG_A_U_1 after UPDATE on SIVE_TSIVE for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_TSIVE 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_TSIVE  SIVE_MODELO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TSIVE <> :new.CD_TSIVE
  then
    select count(*) into numrows
      from SIVE_MODELO
      where
        /*  %JoinFKPK(SIVE_MODELO,:%Old," = "," and") */
        SIVE_MODELO.CD_TSIVE = :old.CD_TSIVE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_TSIVE" because "SIVE_MODELO" exists.'*/
       'no se puede modificar SIVE_TSIVE
        porque hay registros en SIVE_MODELO con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_TSIVE  SIVE_PREGUNTA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_TSIVE <> :new.CD_TSIVE
  then
    select count(*) into numrows
      from SIVE_PREGUNTA
      where
        /*  %JoinFKPK(SIVE_PREGUNTA,:%Old," = "," and") */
        SIVE_PREGUNTA.CD_TSIVE = :old.CD_TSIVE;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_TSIVE" because "SIVE_PREGUNTA" exists.'*/
       'no se puede modificar SIVE_TSIVE
        porque hay registros en SIVE_PREGUNTA con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

create trigger SIVE_URGDIAG_TRIG_A_U
  AFTER UPDATE
  on SIVE_URGDIAG
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_CAUSAURG R/29 SIVE_URGDIAG ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_CAUSAURG
    where
      /* %JoinFKPK(:%New,SIVE_CAUSAURG," = "," and") */
      :new.CD_CAUSAURG = SIVE_CAUSAURG.CD_CAUSAURG;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_URGDIAG" debido a que no existe en "SIVE_CAUSAURG".'
    );
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_URGENCIAS R/11 SIVE_URGDIAG ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_URGENCIAS
    where
      /* %JoinFKPK(:%New,SIVE_URGENCIAS," = "," and") */
      :new.NM_URG = SIVE_URGENCIAS.NM_URG;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_URGDIAG" debido a que no existe en "SIVE_URGENCIAS".'
    );
  end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_URGDIAG_TRIG_A_I
  AFTER INSERT
  on SIVE_URGDIAG
  
  for each row
declare numrows INTEGER;
begin

    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_CAUSAURG R/29 SIVE_URGDIAG ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_CAUSAURG
      where
        /* %JoinFKPK(:%New,SIVE_CAUSAURG," = "," and") */
        :new.CD_CAUSAURG = SIVE_CAUSAURG.CD_CAUSAURG;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_URGDIAG" because a que no existe en "SIVE_CAUSAURG".'
      );
    end if;

    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_URGENCIAS R/11 SIVE_URGDIAG ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_URGENCIAS
      where
        /* %JoinFKPK(:%New,SIVE_URGENCIAS," = "," and") */
        :new.NM_URG = SIVE_URGENCIAS.NM_URG;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_URGDIAG" because a que no existe en "SIVE_URGENCIAS".'
      );
    end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_URGENCIAS_TRIG_A_D
  AFTER DELETE
  on SIVE_URGENCIAS
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
    /* SIVE_URGENCIAS R/11 SIVE_URGDIAG ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_URGDIAG
      where
        /*  %JoinFKPK(SIVE_URGDIAG,:%Old," = "," and") */
        SIVE_URGDIAG.NM_URG = :old.NM_URG;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_URGENCIAS" debido a que existe en "SIVE_URGDIAG".'
      );
    end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create trigger SIVE_URGENCIAS_TRIG_A_U
  AFTER UPDATE
  on SIVE_URGENCIAS
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_URGENCIAS R/11 SIVE_URGDIAG ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.NM_URG <> :new.NM_URG
  then
    select count(*) into numrows
      from SIVE_URGDIAG
      where
        /*  %JoinFKPK(SIVE_URGDIAG,:%Old," = "," and") */
        SIVE_URGDIAG.NM_URG = :old.NM_URG;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_URGENCIAS" debido a que existe "SIVE_URGDIAG".'
      );
    end if;
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_MUNI_URGEN R/59 SIVE_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_MUNI_URGEN
    where
      /* %JoinFKPK(:%New,SIVE_MUNI_URGEN," = "," and") */
      :new.CD_MUNI = SIVE_MUNI_URGEN.CD_MUNI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_MUNI is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_URGENCIAS" debido a que no existe en "SIVE_MUNI_URGEN".'
    );
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_SEXO R/50 SIVE_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEXO
    where
      /* %JoinFKPK(:%New,SIVE_SEXO," = "," and") */
      :new.CD_SEXO = SIVE_SEXO.CD_SEXO;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_URGENCIAS" debido a que no existe en "SIVE_SEXO".'
    );
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_DERIVACION R/38 SIVE_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_DERIVACION
    where
      /* %JoinFKPK(:%New,SIVE_DERIVACION," = "," and") */
      :new.CD_DERIVACION = SIVE_DERIVACION.CD_DERIVACION;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_URGENCIAS" debido a que no existe en "SIVE_DERIVACION".'
    );
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_ZONA_BASICA R/33 SIVE_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ZONA_BASICA
    where
      /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
      :new.CD_NIVEL_1 = SIVE_ZONA_BASICA.CD_NIVEL_1 and
      :new.CD_NIVEL_2 = SIVE_ZONA_BASICA.CD_NIVEL_2 and
      :new.CD_ZBS = SIVE_ZONA_BASICA.CD_ZBS;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_NIVEL_1 is not null and
    :new.CD_NIVEL_2 is not null and
    :new.CD_ZBS is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_URGENCIAS" debido a que no existe en "SIVE_ZONA_BASICA".'
    );
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_SEMANA_EPI R/23 SIVE_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_SEMANA_EPI
    where
      /* %JoinFKPK(:%New,SIVE_SEMANA_EPI," = "," and") */
      :new.CD_ANOEPI = SIVE_SEMANA_EPI.CD_ANOEPI and
      :new.CD_SEMEPI = SIVE_SEMANA_EPI.CD_SEMEPI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_ANOEPI is not null and
    :new.CD_SEMEPI is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_URGENCIAS" debido a que no existe en "SIVE_SEMANA_EPI".'
    );
  end if;

  /* ERwin Builtin Thu Jan 27 13:05:38 2000 */
  /* SIVE_ESPECIALIDAD R/7 SIVE_URGENCIAS ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_ESPECIALIDAD
    where
      /* %JoinFKPK(:%New,SIVE_ESPECIALIDAD," = "," and") */
      :new.CD_ESPECI = SIVE_ESPECIALIDAD.CD_ESPECI and
      :new.CD_HOSP = SIVE_ESPECIALIDAD.CD_HOSP;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_URGENCIAS" debido a que no existe en "SIVE_ESPECIALIDAD".'
    );
  end if;


-- ERwin Builtin Thu Jan 27 13:05:38 2000
end;
/


create or replace trigger SIVE_URGENCIAS_TRIG_B_I
  BEFORE INSERT
  on SIVE_URGENCIAS
  
  for each row
/* ERwin Builtin Wed Oct 31 16:28:52 2001 */
/* default body for SIVE_URGENCIAS_TRIG_B_I */
declare 
  numrows    INTEGER;
  AnnoSem    NUMBER;
  strAnnoSem varchar(6); 
begin
/* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_ZONA_BASICA R/356 SIVE_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ZONA_BASICA
      where
        /* %JoinFKPK(:%New,SIVE_ZONA_BASICA," = "," and") */
        :new.CD_NIVEL_1 = SIVE_ZONA_BASICA.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_ZONA_BASICA.CD_NIVEL_2 and
        :new.CD_ZBS = SIVE_ZONA_BASICA.CD_ZBS;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_NIVEL_1 is not null and
      :new.CD_NIVEL_2 is not null and
      :new.CD_ZBS is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_URGENCIAS" because "SIVE_ZONA_BASICA" does not exist.'*/
          'no se puede grabar en SIVE_URGENCIAS
           porque no existe la clave en SIVE_ZONA_BASICA'
      );
    end if;

/* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_SEXO R/352 SIVE_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEXO
      where
        /* %JoinFKPK(:%New,SIVE_SEXO," = "," and") */
        :new.CD_SEXO = SIVE_SEXO.CD_SEXO;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_URGENCIAS" because "SIVE_SEXO" does not exist.'*/
          'no se puede grabar en SIVE_URGENCIAS
           porque no existe la clave en SIVE_SEXO'
      );
    end if;

/* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_SEMANA_EPI R/351 SIVE_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_SEMANA_EPI
      where
        /* %JoinFKPK(:%New,SIVE_SEMANA_EPI," = "," and") */
        :new.CD_ANOEPI = SIVE_SEMANA_EPI.CD_ANOEPI and
        :new.CD_SEMEPI = SIVE_SEMANA_EPI.CD_SEMEPI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_ANOEPI is not null and
      :new.CD_SEMEPI is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_URGENCIAS" because "SIVE_SEMANA_EPI" does not exist.'*/
          'no se puede grabar en SIVE_URGENCIAS
           porque no existe la clave en SIVE_SEMANA_EPI'
      );
    end if;

/* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_MUNI_URGEN R/348 SIVE_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_MUNI_URGEN
      where
        /* %JoinFKPK(:%New,SIVE_MUNI_URGEN," = "," and") */
        :new.CD_MUNI = SIVE_MUNI_URGEN.CD_MUNI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_MUNI is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_URGENCIAS" because "SIVE_MUNI_URGEN" does not exist.'*/
          'no se puede grabar en SIVE_URGENCIAS
           porque no existe la clave en SIVE_MUNI_URGEN'
      );
    end if;

/* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_ESPECIALIDAD R/344 SIVE_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_ESPECIALIDAD
      where
        /* %JoinFKPK(:%New,SIVE_ESPECIALIDAD," = "," and") */
        :new.CD_ESPECI = SIVE_ESPECIALIDAD.CD_ESPECI and
        :new.CD_HOSP = SIVE_ESPECIALIDAD.CD_HOSP;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_URGENCIAS" because "SIVE_ESPECIALIDAD" does not exist.'*/
          'no se puede grabar en SIVE_URGENCIAS
           porque no existe la clave en ESPECIALIDAD'
      );
    end if;

/* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_DERIVACION R/338 SIVE_URGENCIAS ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_DERIVACION
      where
        /* %JoinFKPK(:%New,SIVE_DERIVACION," = "," and") */
        :new.CD_DERIVACION = SIVE_DERIVACION.CD_DERIVACION;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_URGENCIAS" because "SIVE_DERIVACION" does not exist.'*/
          'no se puede grabar en SIVE_URGENCIAS
           porque no existe la clave en SIVE_DERIVACION'
      );
    end if;





select  min((TO_NUMBER(CD_ANOEPI) * 100)+TO_NUMBER(CD_SEMEPI)) as CODIGO
into    AnnoSem
from    SIVE_SEMANA_EPI se
where TO_DATE(TO_CHAR(FC_FINEPI,'DD/MM/YYYY'),'DD/MM/YYYY') >= 
      TO_DATE(TO_CHAR(:new.FC_URGENCIA,'DD/MM/YYYY'),'DD/MM/YYYY');

strAnnoSem     := TO_CHAR(AnnoSem);

:new.CD_ANOEPI := SUBSTR(strAnnoSem,1,4);  
:new.CD_SEMEPI := SUBSTR(strAnnoSem,5,2);
 

end;
/















































create trigger SIVE_USUARIO_BR_TRIG_A_D_1
  AFTER DELETE
  on SIVE_USUARIO_BROTE
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Thu Apr 13 15:34:06 2000 */
    /* SIVE_USUARIO_BROTE R/238 SIVE_AUTORIZACIONES_BROTE ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_AUTORIZACIONES_BROTE
      where
        /*  %JoinFKPK(SIVE_AUTORIZACIONES_BROTE,:%Old," = "," and") */
        SIVE_AUTORIZACIONES_BROTE.CD_USUARIO = :old.CD_USUARIO and
        SIVE_AUTORIZACIONES_BROTE.CD_CA = :old.CD_CA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_USUARIO_BROTE" because "SIVE_AUTORIZACIONES_BROTE" exists.'*/
       'no se puede borrar de SIVE_USUARIO_BROTE
        porque hay registros en SIVE_AUTORIZACIONES_BROTE con su clave'
      );
    end if;


-- ERwin Builtin Thu Apr 13 15:34:06 2000
end;
/


create trigger SIVE_USUARIO_BR_TRIG_A_I_1
  AFTER INSERT
  on SIVE_USUARIO_BROTE
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Thu Apr 13 15:34:06 2000 */
    /* SIVE_COM_AUT R/242 SIVE_USUARIO_BROTE ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_COM_AUT
      where
        /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
        :new.CD_CA = SIVE_COM_AUT.CD_CA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_USUARIO_BROTE" because "SIVE_COM_AUT" does not exist.'*/
          'no se puede grabar en SIVE_USUARIO_BROTE
           porque no existe la clave en SIVE_COM_AUT'
      );
    end if;


-- ERwin Builtin Thu Apr 13 15:34:06 2000
end;
/


create trigger SIVE_USUARIO_BR_TRIG_A_U_1
  AFTER UPDATE
  on SIVE_USUARIO_BROTE
  
  for each row
declare numrows INTEGER;
begin
  /* ERwin Builtin Thu Apr 13 15:34:06 2000 */
  /* SIVE_USUARIO_BROTE R/238 SIVE_AUTORIZACIONES_BROTE ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_USUARIO <> :new.CD_USUARIO or 
    :old.CD_CA <> :new.CD_CA
  then
    select count(*) into numrows
      from SIVE_AUTORIZACIONES_BROTE
      where
        /*  %JoinFKPK(SIVE_AUTORIZACIONES_BROTE,:%Old," = "," and") */
        SIVE_AUTORIZACIONES_BROTE.CD_USUARIO = :old.CD_USUARIO and
        SIVE_AUTORIZACIONES_BROTE.CD_CA = :old.CD_CA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_USUARIO_BROTE" because "SIVE_AUTORIZACIONES_BROTE" exists.'*/
       'no se puede modificar SIVE_USUARIO_BROTE
        porque hay registros en SIVE_AUTORIZACIONES_BROTE con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Thu Apr 13 15:34:06 2000 */
  /* SIVE_COM_AUT R/242 SIVE_USUARIO_BROTE ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_COM_AUT
    where
      /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
      :new.CD_CA = SIVE_COM_AUT.CD_CA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_USUARIO_BROTE" because "SIVE_COM_AUT" does not exist.'*/
         'no se puede actualizar SIVE_USUARIO_BROTE
          porque no existe la clave en SIVE_COM_AUT'
    );
  end if;


-- ERwin Builtin Thu Apr 13 15:34:06 2000
end;
/


CREATE OR REPLACE TRIGGER SIVE_USUARIO_PISTA_TRIG_A_D_1 after DELETE on SIVE_USUARIO_PISTA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_USUARIO_PISTA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_USUARIO_PISTA  SIVE_AUTORIZACIONES_PISTA ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_AUTORIZACIONES_PISTA
      where
        /*  %JoinFKPK(SIVE_AUTORIZACIONES_PISTA,:%Old," = "," and") */
        SIVE_AUTORIZACIONES_PISTA.CD_USUARIO = :old.CD_USUARIO and
        SIVE_AUTORIZACIONES_PISTA.CD_CA = :old.CD_CA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_USUARIO_PISTA" because "SIVE_AUTORIZACIONES_PISTA" exists.'*/
       'no se puede borrar de SIVE_USUARIO_PISTA
        porque hay registros en SIVE_AUTORIZACIONES_PISTA con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_USUARIO_PISTA_TRIG_A_I_1 after INSERT on SIVE_USUARIO_PISTA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- INSERT trigger on SIVE_USUARIO_PISTA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_E_NOTIF R_269 SIVE_USUARIO_PISTA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_E_NOTIF
      where
        /* %JoinFKPK(:%New,SIVE_E_NOTIF," = "," and") */
        :new.CD_E_NOTIF = SIVE_E_NOTIF.CD_E_NOTIF;
    if (
      /* %NotnullFK(:%New," is not null and") */
      :new.CD_E_NOTIF is not null and
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_USUARIO_PISTA" because "SIVE_E_NOTIF" does not exist.'*/
          'no se puede grabar en SIVE_USUARIO_PISTA
           porque no existe la clave en SIVE_E_NOTIF'
      );
    end if;

    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_COM_AUT R_271 SIVE_USUARIO_PISTA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_COM_AUT
      where
        /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
        :new.CD_CA = SIVE_COM_AUT.CD_CA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_USUARIO_PISTA" because "SIVE_COM_AUT" does not exist.'*/
          'no se puede grabar en SIVE_USUARIO_PISTA
           porque no existe la clave en SIVE_COM_AUT'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_USUARIO_PISTA_TRIG_A_U_1 after UPDATE on SIVE_USUARIO_PISTA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_USUARIO_PISTA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_USUARIO_PISTA  SIVE_AUTORIZACIONES_PISTA ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_USUARIO <> :new.CD_USUARIO or 
    :old.CD_CA <> :new.CD_CA
  then
    select count(*) into numrows
      from SIVE_AUTORIZACIONES_PISTA
      where
        /*  %JoinFKPK(SIVE_AUTORIZACIONES_PISTA,:%Old," = "," and") */
        SIVE_AUTORIZACIONES_PISTA.CD_USUARIO = :old.CD_USUARIO and
        SIVE_AUTORIZACIONES_PISTA.CD_CA = :old.CD_CA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_USUARIO_PISTA" because "SIVE_AUTORIZACIONES_PISTA" exists.'*/
       'no se puede modificar SIVE_USUARIO_PISTA
        porque hay registros en SIVE_AUTORIZACIONES_PISTA con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_E_NOTIF R_269 SIVE_USUARIO_PISTA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_E_NOTIF
    where
      /* %JoinFKPK(:%New,SIVE_E_NOTIF," = "," and") */
      :new.CD_E_NOTIF = SIVE_E_NOTIF.CD_E_NOTIF;
  if (
    /* %NotnullFK(:%New," is not null and") */
    :new.CD_E_NOTIF is not null and
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_USUARIO_PISTA" because "SIVE_E_NOTIF" does not exist.'*/
         'no se puede actualizar SIVE_USUARIO_PISTA
          porque no existe la clave en SIVE_E_NOTIF'
    );
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_COM_AUT R_271 SIVE_USUARIO_PISTA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_COM_AUT
    where
      /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
      :new.CD_CA = SIVE_COM_AUT.CD_CA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_USUARIO_PISTA" because "SIVE_COM_AUT" does not exist.'*/
         'no se puede actualizar SIVE_USUARIO_PISTA
          porque no existe la clave en SIVE_COM_AUT'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_USUARIO_RTBC_TRIG_A_D_1 after DELETE on SIVE_USUARIO_RTBC for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_USUARIO_RTBC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_USUARIO_RTBC  SIVE_AUTORIZACIONES_RTBC ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_AUTORIZACIONES_RTBC
      where
        /*  %JoinFKPK(SIVE_AUTORIZACIONES_RTBC,:%Old," = "," and") */
        SIVE_AUTORIZACIONES_RTBC.CD_USUARIO = :old.CD_USUARIO and
        SIVE_AUTORIZACIONES_RTBC.CD_CA = :old.CD_CA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_USUARIO_RTBC" because "SIVE_AUTORIZACIONES_RTBC" exists.'*/
       'no se puede borrar de SIVE_USUARIO_RTBC
        porque hay registros en SIVE_AUTORIZACIONES_RTBC con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_USUARIO_RTBC_TRIG_A_I_1 after INSERT on SIVE_USUARIO_RTBC for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- INSERT trigger on SIVE_USUARIO_RTBC 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_COM_AUT  SIVE_USUARIO_RTBC ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_COM_AUT
      where
        /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
        :new.CD_CA = SIVE_COM_AUT.CD_CA;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_USUARIO_RTBC" because "SIVE_COM_AUT" does not exist.'*/
          'no se puede grabar en SIVE_USUARIO_RTBC
           porque no existe la clave en SIVE_COM_AUT'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_USUARIO_RTBC_TRIG_A_U_1 after UPDATE on SIVE_USUARIO_RTBC for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_USUARIO_RTBC 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_USUARIO_RTBC  SIVE_AUTORIZACIONES_RTBC ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_USUARIO <> :new.CD_USUARIO or 
    :old.CD_CA <> :new.CD_CA
  then
    select count(*) into numrows
      from SIVE_AUTORIZACIONES_RTBC
      where
        /*  %JoinFKPK(SIVE_AUTORIZACIONES_RTBC,:%Old," = "," and") */
        SIVE_AUTORIZACIONES_RTBC.CD_USUARIO = :old.CD_USUARIO and
        SIVE_AUTORIZACIONES_RTBC.CD_CA = :old.CD_CA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_USUARIO_RTBC" because "SIVE_AUTORIZACIONES_RTBC" exists.'*/
       'no se puede modificar SIVE_USUARIO_RTBC
        porque hay registros en SIVE_AUTORIZACIONES_RTBC con esa clave'

      );
    end if;
  end if;

  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_COM_AUT  SIVE_USUARIO_RTBC ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_COM_AUT
    where
      /* %JoinFKPK(:%New,SIVE_COM_AUT," = "," and") */
      :new.CD_CA = SIVE_COM_AUT.CD_CA;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_USUARIO_RTBC" because "SIVE_COM_AUT" does not exist.'*/
         'no se puede actualizar SIVE_USUARIO_RTBC
          porque no existe la clave en SIVE_COM_AUT'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_VALOR_FACRI_TRIG_A_I_1 after INSERT on SIVE_VALOR_FACRI for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- INSERT trigger on SIVE_VALOR_FACRI 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_FACTOR_RIESGO  SIVE_VALOR_FACRI ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_FACTOR_RIESGO
      where
        /* %JoinFKPK(:%New,SIVE_FACTOR_RIESGO," = "," and") */
        :new.NM_FACRI = SIVE_FACTOR_RIESGO.NM_FACRI;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        /*'Cannot INSERT "SIVE_VALOR_FACRI" because "SIVE_FACTOR_RIESGO" does not exist.'*/
          'no se puede grabar en SIVE_VALOR_FACRI
           porque no existe la clave en SIVE_FACTOR_RIESGO'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_VALOR_FACRI_TRIG_A_U_1 after UPDATE on SIVE_VALOR_FACRI for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_VALOR_FACRI 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_FACTOR_RIESGO  SIVE_VALOR_FACRI ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_FACTOR_RIESGO
    where
      /* %JoinFKPK(:%New,SIVE_FACTOR_RIESGO," = "," and") */
      :new.NM_FACRI = SIVE_FACTOR_RIESGO.NM_FACRI;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      /*'Cannot UPDATE "SIVE_VALOR_FACRI" because "SIVE_FACTOR_RIESGO" does not exist.'*/
         'no se puede actualizar SIVE_VALOR_FACRI
          porque no existe la clave en SIVE_FACTOR_RIESGO'
    );
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_VALOR_MUESTRA_TRIG_A_D_1 after DELETE on SIVE_VALOR_MUESTRA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- DELETE trigger on SIVE_VALOR_MUESTRA 
declare numrows INTEGER;
begin
    /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
    /* SIVE_VALOR_MUESTRA  SIVE_RESLAB ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_RESLAB
      where
        /*  %JoinFKPK(SIVE_RESLAB,:%Old," = "," and") */
        SIVE_RESLAB.CD_VMUESTRA = :old.CD_VMUESTRA;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
       /*'Cannot DELETE "SIVE_VALOR_MUESTRA" because "SIVE_RESLAB" exists.'*/
       'no se puede borrar de SIVE_VALOR_MUESTRA
        porque hay registros en SIVE_RESLAB con su clave'
      );
    end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

CREATE OR REPLACE TRIGGER SIVE_VALOR_MUESTRA_TRIG_A_U_1 after UPDATE on SIVE_VALOR_MUESTRA for each row
-- ERwin Builtin Wed Oct 31 16:28:52 2001
-- UPDATE trigger on SIVE_VALOR_MUESTRA 
declare numrows INTEGER;
begin
  /* ERwin Builtin Wed Oct 31 16:28:52 2001 */
  /* SIVE_VALOR_MUESTRA  SIVE_RESLAB ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_VMUESTRA <> :new.CD_VMUESTRA
  then
    select count(*) into numrows
      from SIVE_RESLAB
      where
        /*  %JoinFKPK(SIVE_RESLAB,:%Old," = "," and") */
        SIVE_RESLAB.CD_VMUESTRA = :old.CD_VMUESTRA;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
       /*'Cannot UPDATE "SIVE_VALOR_MUESTRA" because "SIVE_RESLAB" exists.'*/
       'no se puede modificar SIVE_VALOR_MUESTRA
        porque hay registros en SIVE_RESLAB con esa clave'

      );
    end if;
  end if;


-- ERwin Builtin Wed Oct 31 16:28:52 2001
END;
/

create trigger SIVE_ZONA_BASICA_TRIG_A_U
  AFTER UPDATE
  on SIVE_ZONA_BASICA
  
  for each row
declare numrows INTEGER;
begin

  /* ERwin Builtin Tue Dec 28 10:18:59 1999 */
  /* SIVE_ZONA_BASICA R/33 SIVE_URGENCIAS ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_NIVEL_1 <> :new.CD_NIVEL_1 or 
    :old.CD_NIVEL_2 <> :new.CD_NIVEL_2 or 
    :old.CD_ZBS <> :new.CD_ZBS
  then
    select count(*) into numrows
      from SIVE_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_URGENCIAS,:%Old," = "," and") */
        SIVE_URGENCIAS.CD_NIVEL_1 = :old.CD_NIVEL_1 and
        SIVE_URGENCIAS.CD_NIVEL_2 = :old.CD_NIVEL_2 and
        SIVE_URGENCIAS.CD_ZBS = :old.CD_ZBS;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_ZONA_BASICA" debido a que existe "SIVE_URGENCIAS".'
      );
    end if;
  end if;

  /* ERwin Builtin Tue Dec 28 10:18:59 1999 */
  /* SIVE_ZONA_BASICA R/34 SIVE_MUNICIPIO ON PARENT UPDATE RESTRICT */
  if
    /* %JoinPKPK(:%Old,:%New," <> "," or ") */
    :old.CD_NIVEL_1 <> :new.CD_NIVEL_1 or 
    :old.CD_NIVEL_2 <> :new.CD_NIVEL_2 or 
    :old.CD_ZBS <> :new.CD_ZBS
  then
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /*  %JoinFKPK(SIVE_MUNICIPIO,:%Old," = "," and") */
        SIVE_MUNICIPIO.CD_NIVEL_1 = :old.CD_NIVEL_1 and
        SIVE_MUNICIPIO.CD_NIVEL_2 = :old.CD_NIVEL_2 and
        SIVE_MUNICIPIO.CD_ZBS = :old.CD_ZBS;
    if (numrows > 0)
    then 
      raise_application_error(
        -20005,
        'no se puede ACTUALIZAR en "SIVE_ZONA_BASICA" debido a que existe "SIVE_MUNICIPIO".'
      );
    end if;
  end if;

  /* ERwin Builtin Tue Dec 28 10:18:59 1999 */
  /* SIVE_NIVEL2_S R/18 SIVE_ZONA_BASICA ON CHILD UPDATE RESTRICT */
  select count(*) into numrows
    from SIVE_NIVEL2_S
    where
      /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
      :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
      :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
  if (
    /* %NotnullFK(:%New," is not null and") */
    
    numrows = 0
  )
  then
    raise_application_error(
      -20007,
      'No se puede ACTUALIZAR en "SIVE_ZONA_BASICA" debido a que no existe en "SIVE_NIVEL2_S".'
    );
  end if;


-- ERwin Builtin Tue Dec 28 10:18:59 1999
end;
/


create trigger SIVE_ZONA_BASICA_TRIG_A_I
  AFTER INSERT
  on SIVE_ZONA_BASICA
  
  for each row
declare numrows INTEGER;
begin

    /* ERwin Builtin Tue Dec 28 10:18:58 1999 */
    /* SIVE_NIVEL2_S R/18 SIVE_ZONA_BASICA ON CHILD INSERT RESTRICT */
    select count(*) into numrows
      from SIVE_NIVEL2_S
      where
        /* %JoinFKPK(:%New,SIVE_NIVEL2_S," = "," and") */
        :new.CD_NIVEL_1 = SIVE_NIVEL2_S.CD_NIVEL_1 and
        :new.CD_NIVEL_2 = SIVE_NIVEL2_S.CD_NIVEL_2;
    if (
      /* %NotnullFK(:%New," is not null and") */
      
      numrows = 0
    )
    then
      raise_application_error(
        -20002,
        'No se puede INSERTAR en "SIVE_ZONA_BASICA" because a que no existe en "SIVE_NIVEL2_S".'
      );
    end if;


-- ERwin Builtin Tue Dec 28 10:18:58 1999
end;
/


create trigger SIVE_ZONA_BASICA_TRIG_A_D
  AFTER DELETE
  on SIVE_ZONA_BASICA
  
  for each row
declare numrows INTEGER;
begin
    /* ERwin Builtin Tue Dec 28 10:18:58 1999 */
    /* SIVE_ZONA_BASICA R/33 SIVE_URGENCIAS ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_URGENCIAS
      where
        /*  %JoinFKPK(SIVE_URGENCIAS,:%Old," = "," and") */
        SIVE_URGENCIAS.CD_NIVEL_1 = :old.CD_NIVEL_1 and
        SIVE_URGENCIAS.CD_NIVEL_2 = :old.CD_NIVEL_2 and
        SIVE_URGENCIAS.CD_ZBS = :old.CD_ZBS;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_ZONA_BASICA" debido a que existe en "SIVE_URGENCIAS".'
      );
    end if;

    /* ERwin Builtin Tue Dec 28 10:18:58 1999 */
    /* SIVE_ZONA_BASICA R/34 SIVE_MUNICIPIO ON PARENT DELETE RESTRICT */
    select count(*) into numrows
      from SIVE_MUNICIPIO
      where
        /*  %JoinFKPK(SIVE_MUNICIPIO,:%Old," = "," and") */
        SIVE_MUNICIPIO.CD_NIVEL_1 = :old.CD_NIVEL_1 and
        SIVE_MUNICIPIO.CD_NIVEL_2 = :old.CD_NIVEL_2 and
        SIVE_MUNICIPIO.CD_ZBS = :old.CD_ZBS;
    if (numrows > 0)
    then
      raise_application_error(
        -20001,
        'No se puede BORRAR en "SIVE_ZONA_BASICA" debido a que existe en "SIVE_MUNICIPIO".'
      );
    end if;


-- ERwin Builtin Tue Dec 28 10:18:58 1999
end;
/


